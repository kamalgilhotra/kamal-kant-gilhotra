/*
||==========================================================================================
|| Application: PowerPlan
|| Module:      PCM
|| File Name:   maint_042193_pcm_new_est_attributes_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     1/15/15    MARDIS              Added 10 new estimate attributes
||==========================================================================================
*/

alter table wo_est_monthly add budget_id number(22);
alter table wo_est_monthly add attribute01_id varchar2(254);
alter table wo_est_monthly add attribute02_id varchar2(254);
alter table wo_est_monthly add attribute03_id varchar2(254);
alter table wo_est_monthly add attribute04_id varchar2(254);
alter table wo_est_monthly add attribute05_id varchar2(254);
alter table wo_est_monthly add attribute06_id varchar2(254);
alter table wo_est_monthly add attribute07_id varchar2(254);
alter table wo_est_monthly add attribute08_id varchar2(254);
alter table wo_est_monthly add attribute09_id varchar2(254);
alter table wo_est_monthly add attribute10_id varchar2(254);

comment on column wo_est_monthly.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_monthly.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_monthly_fy_temp add budget_id number(22);
alter table wo_est_monthly_fy_temp add attribute01_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute02_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute03_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute04_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute05_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute06_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute07_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute08_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute09_id varchar2(254);
alter table wo_est_monthly_fy_temp add attribute10_id varchar2(254);

comment on column wo_est_monthly_fy_temp.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_monthly_fy_temp.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_fy_temp.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_processing_transpose add attribute01_id varchar2(254);
alter table wo_est_processing_transpose add attribute02_id varchar2(254);
alter table wo_est_processing_transpose add attribute03_id varchar2(254);
alter table wo_est_processing_transpose add attribute04_id varchar2(254);
alter table wo_est_processing_transpose add attribute05_id varchar2(254);
alter table wo_est_processing_transpose add attribute06_id varchar2(254);
alter table wo_est_processing_transpose add attribute07_id varchar2(254);
alter table wo_est_processing_transpose add attribute08_id varchar2(254);
alter table wo_est_processing_transpose add attribute09_id varchar2(254);
alter table wo_est_processing_transpose add attribute10_id varchar2(254);

comment on column wo_est_processing_transpose.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_transpose.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table budget_monthly_data add attribute01_id varchar2(254);
alter table budget_monthly_data add attribute02_id varchar2(254);
alter table budget_monthly_data add attribute03_id varchar2(254);
alter table budget_monthly_data add attribute04_id varchar2(254);
alter table budget_monthly_data add attribute05_id varchar2(254);
alter table budget_monthly_data add attribute06_id varchar2(254);
alter table budget_monthly_data add attribute07_id varchar2(254);
alter table budget_monthly_data add attribute08_id varchar2(254);
alter table budget_monthly_data add attribute09_id varchar2(254);
alter table budget_monthly_data add attribute10_id varchar2(254);

comment on column budget_monthly_data.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_processing_mo_id add attribute01_id varchar2(254);
alter table wo_est_processing_mo_id add attribute02_id varchar2(254);
alter table wo_est_processing_mo_id add attribute03_id varchar2(254);
alter table wo_est_processing_mo_id add attribute04_id varchar2(254);
alter table wo_est_processing_mo_id add attribute05_id varchar2(254);
alter table wo_est_processing_mo_id add attribute06_id varchar2(254);
alter table wo_est_processing_mo_id add attribute07_id varchar2(254);
alter table wo_est_processing_mo_id add attribute08_id varchar2(254);
alter table wo_est_processing_mo_id add attribute09_id varchar2(254);
alter table wo_est_processing_mo_id add attribute10_id varchar2(254);

comment on column wo_est_processing_mo_id.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_processing_mo_id.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table clearing_wo_control_bdg add load_by_attr01 number(22);
alter table clearing_wo_control_bdg add load_by_attr02 number(22);
alter table clearing_wo_control_bdg add load_by_attr03 number(22);
alter table clearing_wo_control_bdg add load_by_attr04 number(22);
alter table clearing_wo_control_bdg add load_by_attr05 number(22);
alter table clearing_wo_control_bdg add load_by_attr06 number(22);
alter table clearing_wo_control_bdg add load_by_attr07 number(22);
alter table clearing_wo_control_bdg add load_by_attr08 number(22);
alter table clearing_wo_control_bdg add load_by_attr09 number(22);
alter table clearing_wo_control_bdg add load_by_attr10 number(22);

comment on column clearing_wo_control_bdg.load_by_attr01 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr02 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr03 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr04 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr05 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr06 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr07 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr08 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr09 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column clearing_wo_control_bdg.load_by_attr10 is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_slide_results_temp add budget_id number(22);
alter table wo_est_slide_results_temp add attribute01_id varchar2(254);
alter table wo_est_slide_results_temp add attribute02_id varchar2(254);
alter table wo_est_slide_results_temp add attribute03_id varchar2(254);
alter table wo_est_slide_results_temp add attribute04_id varchar2(254);
alter table wo_est_slide_results_temp add attribute05_id varchar2(254);
alter table wo_est_slide_results_temp add attribute06_id varchar2(254);
alter table wo_est_slide_results_temp add attribute07_id varchar2(254);
alter table wo_est_slide_results_temp add attribute08_id varchar2(254);
alter table wo_est_slide_results_temp add attribute09_id varchar2(254);
alter table wo_est_slide_results_temp add attribute10_id varchar2(254);

comment on column wo_est_slide_results_temp.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_slide_results_temp.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_slide_results_temp.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_transpose_temp add budget_id number(22);
alter table wo_est_transpose_temp add attribute01_id varchar2(254);
alter table wo_est_transpose_temp add attribute02_id varchar2(254);
alter table wo_est_transpose_temp add attribute03_id varchar2(254);
alter table wo_est_transpose_temp add attribute04_id varchar2(254);
alter table wo_est_transpose_temp add attribute05_id varchar2(254);
alter table wo_est_transpose_temp add attribute06_id varchar2(254);
alter table wo_est_transpose_temp add attribute07_id varchar2(254);
alter table wo_est_transpose_temp add attribute08_id varchar2(254);
alter table wo_est_transpose_temp add attribute09_id varchar2(254);
alter table wo_est_transpose_temp add attribute10_id varchar2(254);

comment on column wo_est_transpose_temp.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_transpose_temp.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_transpose_temp.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

create or replace view wo_est_monthly_deescalation_vw as
select A.EST_MONTHLY_ID,
       A.TIME_STAMP,
       A.USER_ID,
       A.WORK_ORDER_ID,
       A.REVISION,
       A.YEAR,
       EXPENDITURE_TYPE_ID,
       A.EST_CHG_TYPE_ID,
       A.DEPARTMENT_ID,
       NVL(A.JANUARY, 0) - NVL(V.JANUARY, 0) JANUARY,
       NVL(A.FEBRUARY, 0) - NVL(V.FEBRUARY, 0) FEBRUARY,
       NVL(A.MARCH, 0) - NVL(V.MARCH, 0) MARCH,
       NVL(A.APRIL, 0) - NVL(V.APRIL, 0) APRIL,
       NVL(A.MAY, 0) - NVL(V.MAY, 0) MAY,
       NVL(A.JUNE, 0) - NVL(V.JUNE, 0) JUNE,
       NVL(A.JULY, 0) - NVL(V.JULY, 0) JULY,
       NVL(A.AUGUST, 0) - NVL(V.AUGUST, 0) AUGUST,
       NVL(A.SEPTEMBER, 0) - NVL(V.SEPTEMBER, 0) SEPTEMBER,
       NVL(A.OCTOBER, 0) - NVL(V.OCTOBER, 0) OCTOBER,
       NVL(A.NOVEMBER, 0) - NVL(V.NOVEMBER, 0) NOVEMBER,
       NVL(A.DECEMBER, 0) - NVL(V.DECEMBER, 0) DECEMBER,
       NVL(A.TOTAL, 0) - NVL(V.TOTAL, 0) TOTAL,
       A.UTILITY_ACCOUNT_ID,
       A.LONG_DESCRIPTION,
       NVL(A.FUTURE_DOLLARS, 0) FUTURE_DOLLARS,
       NVL(A.HIST_ACTUALS, 0) HIST_ACTUALS,
       A.WO_WORK_ORDER_ID,
       A.JOB_TASK_ID,
       A.SUBSTITUTION_ID,
       A.BUDGET_ID,
       A.ATTRIBUTE01_ID,
       A.ATTRIBUTE02_ID,
       A.ATTRIBUTE03_ID,
       A.ATTRIBUTE04_ID,
       A.ATTRIBUTE05_ID,
       A.ATTRIBUTE06_ID,
       A.ATTRIBUTE07_ID,
       A.ATTRIBUTE08_ID,
       A.ATTRIBUTE09_ID,
       A.ATTRIBUTE10_ID,
       B.FACTOR_ID,
       B.CURVE_ID,
       B.RATE_TYPE_ID,
       NVL(A.HRS_JAN, 0) HRS_JAN,
       NVL(A.HRS_FEB, 0) HRS_FEB,
       NVL(A.HRS_MAR, 0) HRS_MAR,
       NVL(A.HRS_APR, 0) HRS_APR,
       NVL(A.HRS_MAY, 0) HRS_MAY,
       NVL(A.HRS_JUN, 0) HRS_JUN,
       NVL(A.HRS_JUL, 0) HRS_JUL,
       NVL(A.HRS_AUG, 0) HRS_AUG,
       NVL(A.HRS_SEP, 0) HRS_SEP,
       NVL(A.HRS_OCT, 0) HRS_OCT,
       NVL(A.HRS_NOV, 0) HRS_NOV,
       NVL(A.HRS_DEC, 0) HRS_DEC,
       NVL(A.HRS_TOTAL, 0) HRS_TOTAL,
       NVL(A.QTY_JAN, 0) QTY_JAN,
       NVL(A.QTY_FEB, 0) QTY_FEB,
       NVL(A.QTY_MAR, 0) QTY_MAR,
       NVL(A.QTY_APR, 0) QTY_APR,
       NVL(A.QTY_MAY, 0) QTY_MAY,
       NVL(A.QTY_JUN, 0) QTY_JUN,
       NVL(A.QTY_JUL, 0) QTY_JUL,
       NVL(A.QTY_AUG, 0) QTY_AUG,
       NVL(A.QTY_SEP, 0) QTY_SEP,
       NVL(A.QTY_OCT, 0) QTY_OCT,
       NVL(A.QTY_NOV, 0) QTY_NOV,
       NVL(A.QTY_DEC, 0) QTY_DEC,
       NVL(A.QTY_TOTAL, 0) QTY_TOTAL
  from WO_EST_MONTHLY A, WO_EST_MONTHLY_ESCALATION V, WO_EST_MONTHLY_SPREAD B
 where A.EST_MONTHLY_ID = V.EST_MONTHLY_ID(+)
   and A.YEAR = V.YEAR(+)
   and A.EST_MONTHLY_ID = B.EST_MONTHLY_ID(+);

create or replace view wo_est_monthly_deescalated_vw as
select W.WORK_ORDER_ID,
       W.REVISION,
       W.EST_CHG_TYPE_ID,
       TO_NUMBER(W.YEAR || LPAD(B.MONTH_NUM, 2, '0')) MONTH_NUMBER,
       W.EXPENDITURE_TYPE_ID,
       W.DEPARTMENT_ID,
       NVL(sum(DECODE(TO_NUMBER(B.MONTH_NUM),
                      1,
                      W.JANUARY,
                      2,
                      W.FEBRUARY,
                      3,
                      W.MARCH,
                      4,
                      W.APRIL,
                      5,
                      W.MAY,
                      6,
                      W.JUNE,
                      7,
                      W.JULY,
                      8,
                      W.AUGUST,
                      9,
                      W.SEPTEMBER,
                      10,
                      W.OCTOBER,
                      11,
                      W.NOVEMBER,
                      12,
                      W.DECEMBER)),
           0) AMOUNT,
       NVL(sum(DECODE(TO_NUMBER(B.MONTH_NUM),
                      1,
                      W.HRS_JAN,
                      2,
                      W.HRS_FEB,
                      3,
                      W.HRS_MAR,
                      4,
                      W.HRS_APR,
                      5,
                      W.HRS_MAY,
                      6,
                      W.HRS_JUN,
                      7,
                      W.HRS_JUL,
                      8,
                      W.HRS_AUG,
                      9,
                      W.HRS_SEP,
                      10,
                      W.HRS_OCT,
                      11,
                      W.HRS_NOV,
                      12,
                      W.HRS_DEC)),
           0) HRS,
       NVL(sum(DECODE(TO_NUMBER(B.MONTH_NUM),
                      1,
                      W.QTY_JAN,
                      2,
                      W.QTY_FEB,
                      3,
                      W.QTY_MAR,
                      4,
                      W.QTY_APR,
                      5,
                      W.QTY_MAY,
                      6,
                      W.QTY_JUN,
                      7,
                      W.QTY_JUL,
                      8,
                      W.QTY_AUG,
                      9,
                      W.QTY_SEP,
                      10,
                      W.QTY_OCT,
                      11,
                      W.QTY_NOV,
                      12,
                      W.QTY_DEC)),
           0) QTY,
       0 HIST_ACTUALS,
       W.UTILITY_ACCOUNT_ID,
       W.WO_WORK_ORDER_ID,
       W.JOB_TASK_ID,
       W.SUBSTITUTION_ID,
       W.BUDGET_ID,
       W.ATTRIBUTE01_ID,
       W.ATTRIBUTE02_ID,
       W.ATTRIBUTE03_ID,
       W.ATTRIBUTE04_ID,
       W.ATTRIBUTE05_ID,
       W.ATTRIBUTE06_ID,
       W.ATTRIBUTE07_ID,
       W.ATTRIBUTE08_ID,
       W.ATTRIBUTE09_ID,
       W.ATTRIBUTE10_ID,
       W.FACTOR_ID,
       W.CURVE_ID,
       W.RATE_TYPE_ID
  from WO_EST_MONTHLY_DEESCALATION_VW W, PP_TABLE_MONTHS B
 group by W.WORK_ORDER_ID,
          W.REVISION,
          W.EST_CHG_TYPE_ID,
          TO_NUMBER(W.YEAR || LPAD(B.MONTH_NUM, 2, '0')),
          W.EXPENDITURE_TYPE_ID,
          W.DEPARTMENT_ID,
          0,
          W.UTILITY_ACCOUNT_ID,
          W.WO_WORK_ORDER_ID,
          W.JOB_TASK_ID,
          W.SUBSTITUTION_ID,
          W.BUDGET_ID,
          W.ATTRIBUTE01_ID,
          W.ATTRIBUTE02_ID,
          W.ATTRIBUTE03_ID,
          W.ATTRIBUTE04_ID,
          W.ATTRIBUTE05_ID,
          W.ATTRIBUTE06_ID,
          W.ATTRIBUTE07_ID,
          W.ATTRIBUTE08_ID,
          W.ATTRIBUTE09_ID,
          W.ATTRIBUTE10_ID,
          W.FACTOR_ID,
          W.CURVE_ID,
          W.RATE_TYPE_ID;


create or replace view wo_est_monthly_deesc_sv as
select W.EST_MONTHLY_ID EST_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.REVISION REVISION,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) - NVL(WE.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) - NVL(WE.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) - NVL(WE.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) - NVL(WE.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) - NVL(WE.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) - NVL(WE.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) - NVL(WE.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) - NVL(WE.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) - NVL(WE.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) - NVL(WE.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) - NVL(WE.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) - NVL(WE.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) - NVL(WE.TOTAL, 0) TOTAL,
       W.UTILITY_ACCOUNT_ID UTILITY_ACCOUNT_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WO_WORK_ORDER_ID WO_WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID,
       W.SUBSTITUTION_ID SUBSTITUTION_ID,
       W.BUDGET_ID BUDGET_ID,
       W.ATTRIBUTE01_ID ATTRIBUTE01_ID,
       W.ATTRIBUTE02_ID ATTRIBUTE02_ID,
       W.ATTRIBUTE03_ID ATTRIBUTE03_ID,
       W.ATTRIBUTE04_ID ATTRIBUTE04_ID,
       W.ATTRIBUTE05_ID ATTRIBUTE05_ID,
       W.ATTRIBUTE06_ID ATTRIBUTE06_ID,
       W.ATTRIBUTE07_ID ATTRIBUTE07_ID,
       W.ATTRIBUTE08_ID ATTRIBUTE08_ID,
       W.ATTRIBUTE09_ID ATTRIBUTE09_ID,
       W.ATTRIBUTE10_ID ATTRIBUTE10_ID,
       NVL(W.HRS_JAN, 0) HRS_PERIOD1,
       NVL(W.HRS_FEB, 0) HRS_PERIOD2,
       NVL(W.HRS_MAR, 0) HRS_PERIOD3,
       NVL(W.HRS_APR, 0) HRS_PERIOD4,
       NVL(W.HRS_MAY, 0) HRS_PERIOD5,
       NVL(W.HRS_JUN, 0) HRS_PERIOD6,
       NVL(W.HRS_JUL, 0) HRS_PERIOD7,
       NVL(W.HRS_AUG, 0) HRS_PERIOD8,
       NVL(W.HRS_SEP, 0) HRS_PERIOD9,
       NVL(W.HRS_OCT, 0) HRS_PERIOD10,
       NVL(W.HRS_NOV, 0) HRS_PERIOD11,
       NVL(W.HRS_DEC, 0) HRS_PERIOD12,
       NVL(W.HRS_TOTAL, 0) HRS_TOTAL,
       NVL(W.QTY_JAN, 0) QTY_PERIOD1,
       NVL(W.QTY_FEB, 0) QTY_PERIOD2,
       NVL(W.QTY_MAR, 0) QTY_PERIOD3,
       NVL(W.QTY_APR, 0) QTY_PERIOD4,
       NVL(W.QTY_MAY, 0) QTY_PERIOD5,
       NVL(W.QTY_JUN, 0) QTY_PERIOD6,
       NVL(W.QTY_JUL, 0) QTY_PERIOD7,
       NVL(W.QTY_AUG, 0) QTY_PERIOD8,
       NVL(W.QTY_SEP, 0) QTY_PERIOD9,
       NVL(W.QTY_OCT, 0) QTY_PERIOD10,
       NVL(W.QTY_NOV, 0) QTY_PERIOD11,
       NVL(W.QTY_DEC, 0) QTY_PERIOD12,
       NVL(W.QTY_TOTAL, 0) QTY_TOTAL
  from WO_EST_MONTHLY W, WO_EST_MONTHLY_ESCALATION WE, ESTIMATE_CHARGE_TYPE ECT
 where W.EST_MONTHLY_ID = WE.EST_MONTHLY_ID(+)
   and W.YEAR = WE.YEAR(+)
   and W.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and W.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and ECT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(ECT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0;


create or replace view wo_est_monthly_deesc_vw_fy as
select w.est_monthly_id est_monthly_id, max(w.time_stamp) time_stamp, max(w.user_id) user_id, w.work_order_id, w.revision, p.fiscal_year year,
       w.expenditure_type_id, w.est_chg_type_id, w.department_id,
       sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) january,
       sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) february,
       sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) march,
       sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) april,
       sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) may,
       sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) june,
       sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) july,
       sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) august,
       sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) september,
       sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) october,
       sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) november,
       sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) december,
       sum(
          nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)
          ) total,
       w.utility_account_id, w.long_description, 0 future_dollars, 0 hist_actuals, w.wo_work_order_id, w.job_task_id, w.substitution_id, w.budget_id,
       w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id,
       sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jan,
       sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_feb,
       sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_mar,
       sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_apr,
       sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_may,
       sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jun,
       sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jul,
       sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_aug,
       sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_sep,
       sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_oct,
       sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_nov,
       sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_dec,
       sum(
          nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)
          ) hrs_total,
       sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jan,
       sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_feb,
       sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_mar,
       sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_apr,
       sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_may,
       sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jun,
       sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jul,
       sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_aug,
       sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_sep,
       sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_oct,
       sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_nov,
       sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_dec,
       sum(
          nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_total
  from PP_CALENDAR P, WO_EST_MONTHLY_DEESCALATION_VW W
 where P.YEAR = W.YEAR
 group by W.EST_MONTHLY_ID, W.WORK_ORDER_ID, W.REVISION, P.FISCAL_YEAR,
          W.EXPENDITURE_TYPE_ID, W.EST_CHG_TYPE_ID, W.DEPARTMENT_ID,
          W.UTILITY_ACCOUNT_ID, W.LONG_DESCRIPTION, W.WO_WORK_ORDER_ID,
          W.JOB_TASK_ID, W.SUBSTITUTION_ID, w.budget_id,
          w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id;


create or replace view wo_est_monthly_fy as
select EST_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       WORK_ORDER_ID,
       REVISION,
       year,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       JANUARY,
       FEBRUARY,
       MARCH,
       APRIL,
       MAY,
       JUNE,
       JULY,
       AUGUST,
       SEPTEMBER,
       OCTOBER,
       NOVEMBER,
       DECEMBER,
       JANUARY + FEBRUARY + MARCH + APRIL + MAY + JUNE + JULY + AUGUST + SEPTEMBER + OCTOBER + NOVEMBER + DECEMBER TOTAL,
       UTILITY_ACCOUNT_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID,
       BUDGET_ID,
       ATTRIBUTE01_ID,
       ATTRIBUTE02_ID,
       ATTRIBUTE03_ID,
       ATTRIBUTE04_ID,
       ATTRIBUTE05_ID,
       ATTRIBUTE06_ID,
       ATTRIBUTE07_ID,
       ATTRIBUTE08_ID,
       ATTRIBUTE09_ID,
       ATTRIBUTE10_ID,
       HRS_JAN,
       HRS_FEB,
       HRS_MAR,
       HRS_APR,
       HRS_MAY,
       HRS_JUN,
       HRS_JUL,
       HRS_AUG,
       HRS_SEP,
       HRS_OCT,
       HRS_NOV,
       HRS_DEC,
       HRS_JAN + HRS_FEB + HRS_MAR + HRS_APR + HRS_MAY + HRS_JUN + HRS_JUL + HRS_AUG + HRS_SEP + HRS_OCT + HRS_NOV + HRS_DEC HRS_TOTAL,
       QTY_JAN,
       QTY_FEB,
       QTY_MAR,
       QTY_APR,
       QTY_MAY,
       QTY_JUN,
       QTY_JUL,
       QTY_AUG,
       QTY_SEP,
       QTY_OCT,
       QTY_NOV,
       QTY_DEC,
       QTY_JAN + QTY_FEB + QTY_MAR + QTY_APR + QTY_MAY + QTY_JUN + QTY_JUL + QTY_AUG + QTY_SEP + QTY_OCT + QTY_NOV + QTY_DEC QTY_TOTAL
    from (select W.EST_MONTHLY_ID EST_MONTHLY_ID,
                 max(W.TIME_STAMP) TIME_STAMP,
                 max(W.USER_ID) USER_ID,
                 W.WORK_ORDER_ID WORK_ORDER_ID,
                 W.REVISION REVISION,
                 P.FISCAL_YEAR year,
                 max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
                 max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
                 max(W.DEPARTMENT_ID) DEPARTMENT_ID,
                 sum(nvl(decode(P.FISCAL_MONTH,1, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) JANUARY,
                 sum(nvl(decode(P.FISCAL_MONTH,2, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) FEBRUARY,
                 sum(nvl(decode(P.FISCAL_MONTH,3, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) MARCH,
                 sum(nvl(decode(P.FISCAL_MONTH,4, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) APRIL,
                 sum(nvl(decode(P.FISCAL_MONTH,5, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) MAY,
                 sum(nvl(decode(P.FISCAL_MONTH,6, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) JUNE,
                 sum(nvl(decode(P.FISCAL_MONTH,7, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) JULY,
                 sum(nvl(decode(P.FISCAL_MONTH,8, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) AUGUST,
                 sum(nvl(decode(P.FISCAL_MONTH,9, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) SEPTEMBER,
                 sum(nvl(decode(P.FISCAL_MONTH,10,decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) OCTOBER,
                 sum(nvl(decode(P.FISCAL_MONTH,11,decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) NOVEMBER,
                 sum(nvl(decode(P.FISCAL_MONTH,12,decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) DECEMBER,
                 max(W.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
                 max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
                 0 FUTURE_DOLLARS,
                 0 HIST_ACTUALS,
                 max(W.WO_WORK_ORDER_ID) WO_WORK_ORDER_ID,
                 max(W.JOB_TASK_ID) JOB_TASK_ID,
                 max(W.SUBSTITUTION_ID) SUBSTITUTION_ID,
                 max(W.BUDGET_ID) BUDGET_ID,
                 max(W.ATTRIBUTE01_ID) ATTRIBUTE01_ID,
                 max(W.ATTRIBUTE02_ID) ATTRIBUTE02_ID,
                 max(W.ATTRIBUTE03_ID) ATTRIBUTE03_ID,
                 max(W.ATTRIBUTE04_ID) ATTRIBUTE04_ID,
                 max(W.ATTRIBUTE05_ID) ATTRIBUTE05_ID,
                 max(W.ATTRIBUTE06_ID) ATTRIBUTE06_ID,
                 max(W.ATTRIBUTE07_ID) ATTRIBUTE07_ID,
                 max(W.ATTRIBUTE08_ID) ATTRIBUTE08_ID,
                 max(W.ATTRIBUTE09_ID) ATTRIBUTE09_ID,
                 max(W.ATTRIBUTE10_ID) ATTRIBUTE10_ID,
                 sum(nvl(decode(P.FISCAL_MONTH,1, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_JAN,
                 sum(nvl(decode(P.FISCAL_MONTH,2, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_FEB,
                 sum(nvl(decode(P.FISCAL_MONTH,3, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_MAR,
                 sum(nvl(decode(P.FISCAL_MONTH,4, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_APR,
                 sum(nvl(decode(P.FISCAL_MONTH,5, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_MAY,
                 sum(nvl(decode(P.FISCAL_MONTH,6, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_JUN,
                 sum(nvl(decode(P.FISCAL_MONTH,7, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_JUL,
                 sum(nvl(decode(P.FISCAL_MONTH,8, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_AUG,
                 sum(nvl(decode(P.FISCAL_MONTH,9, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_SEP,
                 sum(nvl(decode(P.FISCAL_MONTH,10,decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_OCT,
                 sum(nvl(decode(P.FISCAL_MONTH,11,decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_NOV,
                 sum(nvl(decode(P.FISCAL_MONTH,12,decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_DEC,
                 sum(nvl(decode(P.FISCAL_MONTH,1, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_JAN,
                 sum(nvl(decode(P.FISCAL_MONTH,2, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_FEB,
                 sum(nvl(decode(P.FISCAL_MONTH,3, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_MAR,
                 sum(nvl(decode(P.FISCAL_MONTH,4, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_APR,
                 sum(nvl(decode(P.FISCAL_MONTH,5, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_MAY,
                 sum(nvl(decode(P.FISCAL_MONTH,6, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_JUN,
                 sum(nvl(decode(P.FISCAL_MONTH,7, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_JUL,
                 sum(nvl(decode(P.FISCAL_MONTH,8, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_AUG,
                 sum(nvl(decode(P.FISCAL_MONTH,9, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_SEP,
                 sum(nvl(decode(P.FISCAL_MONTH,10,decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_OCT,
                 sum(nvl(decode(P.FISCAL_MONTH,11,decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_NOV,
                 sum(nvl(decode(P.FISCAL_MONTH,12,decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_DEC
            from PP_CALENDAR P, WO_EST_MONTHLY W
           where P.YEAR = W.YEAR
           group by W.EST_MONTHLY_ID, P.FISCAL_YEAR, W.WORK_ORDER_ID, W.REVISION
         );


create or replace view wo_est_monthly_fy_bdg_v as
select EST_MONTHLY_ID,
        TIME_STAMP,
        USER_ID,
        WORK_ORDER_ID,
        REVISION,
        year,
        EXPENDITURE_TYPE_ID,
        EST_CHG_TYPE_ID,
        DEPARTMENT_ID,
        JANUARY,
        FEBRUARY,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULY,
        AUGUST,
        SEPTEMBER,
        OCTOBER,
        NOVEMBER,
        DECEMBER,
        JANUARY + FEBRUARY + MARCH + APRIL + MAY + JUNE + JULY + AUGUST + SEPTEMBER + OCTOBER +
        NOVEMBER + DECEMBER TOTAL,
        UTILITY_ACCOUNT_ID,
        LONG_DESCRIPTION,
        FUTURE_DOLLARS,
        HIST_ACTUALS,
        WO_WORK_ORDER_ID,
        JOB_TASK_ID,
        SUBSTITUTION_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
        HRS_JAN,
        HRS_FEB,
        HRS_MAR,
        HRS_APR,
        HRS_MAY,
        HRS_JUN,
        HRS_JUL,
        HRS_AUG,
        HRS_SEP,
        HRS_OCT,
        HRS_NOV,
        HRS_DEC,
        HRS_JAN + HRS_FEB + HRS_MAR + HRS_APR + HRS_MAY + HRS_JUN + HRS_JUL + HRS_AUG + HRS_SEP +
        HRS_OCT + HRS_NOV + HRS_DEC HRS_TOTAL,
        QTY_JAN,
        QTY_FEB,
        QTY_MAR,
        QTY_APR,
        QTY_MAY,
        QTY_JUN,
        QTY_JUL,
        QTY_AUG,
        QTY_SEP,
        QTY_OCT,
        QTY_NOV,
        QTY_DEC,
        QTY_JAN + QTY_FEB + QTY_MAR + QTY_APR + QTY_MAY + QTY_JUN + QTY_JUL + QTY_AUG + QTY_SEP +
        QTY_OCT + QTY_NOV + QTY_DEC QTY_TOTAL
   from (select W.EST_MONTHLY_ID EST_MONTHLY_ID,
                max(W.TIME_STAMP) TIME_STAMP,
                max(W.USER_ID) USER_ID,
                W.WORK_ORDER_ID WORK_ORDER_ID,
                W.REVISION REVISION,
                P.FISCAL_YEAR year,
                max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
                max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
                max(W.DEPARTMENT_ID) DEPARTMENT_ID,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '01', W.JANUARY, 0), 0)) JANUARY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '02', W.FEBRUARY, 0), 0)) FEBRUARY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '03', W.MARCH, 0), 0)) MARCH,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '04', W.APRIL, 0), 0)) APRIL,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '05', W.MAY, 0), 0)) MAY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '06', W.JUNE, 0), 0)) JUNE,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '07', W.JULY, 0), 0)) JULY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '08', W.AUGUST, 0), 0)) AUGUST,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '09', W.SEPTEMBER, 0), 0)) SEPTEMBER,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '10', W.OCTOBER, 0), 0)) OCTOBER,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '11', W.NOVEMBER, 0), 0)) NOVEMBER,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '12', W.DECEMBER, 0), 0)) DECEMBER,
                max(W.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
                max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
                0 FUTURE_DOLLARS,
                0 HIST_ACTUALS,
                max(W.WO_WORK_ORDER_ID) WO_WORK_ORDER_ID,
                max(W.JOB_TASK_ID) JOB_TASK_ID,
                max(W.SUBSTITUTION_ID) SUBSTITUTION_ID,
                max(W.BUDGET_ID) BUDGET_ID,
                max(W.ATTRIBUTE01_ID) ATTRIBUTE01_ID,
                max(W.ATTRIBUTE02_ID) ATTRIBUTE02_ID,
                max(W.ATTRIBUTE03_ID) ATTRIBUTE03_ID,
                max(W.ATTRIBUTE04_ID) ATTRIBUTE04_ID,
                max(W.ATTRIBUTE05_ID) ATTRIBUTE05_ID,
                max(W.ATTRIBUTE06_ID) ATTRIBUTE06_ID,
                max(W.ATTRIBUTE07_ID) ATTRIBUTE07_ID,
                max(W.ATTRIBUTE08_ID) ATTRIBUTE08_ID,
                max(W.ATTRIBUTE09_ID) ATTRIBUTE09_ID,
                max(W.ATTRIBUTE10_ID) ATTRIBUTE10_ID,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '01', W.HRS_JAN, 0), 0)) HRS_JAN,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '02', W.HRS_FEB, 0), 0)) HRS_FEB,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '03', W.HRS_MAR, 0), 0)) HRS_MAR,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '04', W.HRS_APR, 0), 0)) HRS_APR,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '05', W.HRS_MAY, 0), 0)) HRS_MAY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '06', W.HRS_JUN, 0), 0)) HRS_JUN,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '07', W.HRS_JUL, 0), 0)) HRS_JUL,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '08', W.HRS_AUG, 0), 0)) HRS_AUG,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '09', W.HRS_SEP, 0), 0)) HRS_SEP,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '10', W.HRS_OCT, 0), 0)) HRS_OCT,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '11', W.HRS_NOV, 0), 0)) HRS_NOV,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '12', W.HRS_DEC, 0), 0)) HRS_DEC,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '01', W.QTY_JAN, 0), 0)) QTY_JAN,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '02', W.QTY_FEB, 0), 0)) QTY_FEB,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '03', W.QTY_MAR, 0), 0)) QTY_MAR,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '04', W.QTY_APR, 0), 0)) QTY_APR,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '05', W.QTY_MAY, 0), 0)) QTY_MAY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '06', W.QTY_JUN, 0), 0)) QTY_JUN,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '07', W.QTY_JUL, 0), 0)) QTY_JUL,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '08', W.QTY_AUG, 0), 0)) QTY_AUG,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '09', W.QTY_SEP, 0), 0)) QTY_SEP,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '10', W.QTY_OCT, 0), 0)) QTY_OCT,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '11', W.QTY_NOV, 0), 0)) QTY_NOV,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '12', W.QTY_DEC, 0), 0)) QTY_DEC
           from PP_CALENDAR P, WO_EST_MONTHLY W
          where P.YEAR = W.YEAR
          group by W.EST_MONTHLY_ID, P.FISCAL_YEAR, W.WORK_ORDER_ID, W.REVISION);


create or replace view wo_est_monthly_fy_deesc_sv as
select EST_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       WORK_ORDER_ID,
       REVISION,
       YEAR,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       AMOUNT_PERIOD1,
       AMOUNT_PERIOD2,
       AMOUNT_PERIOD3,
       AMOUNT_PERIOD4,
       AMOUNT_PERIOD5,
       AMOUNT_PERIOD6,
       AMOUNT_PERIOD7,
       AMOUNT_PERIOD8,
       AMOUNT_PERIOD9,
       AMOUNT_PERIOD10,
       AMOUNT_PERIOD11,
       AMOUNT_PERIOD12,
       AMOUNT_PERIOD1 + AMOUNT_PERIOD2 + AMOUNT_PERIOD3 + AMOUNT_PERIOD4 + AMOUNT_PERIOD5 +
       AMOUNT_PERIOD6 + AMOUNT_PERIOD7 + AMOUNT_PERIOD8 + AMOUNT_PERIOD9 + AMOUNT_PERIOD10 +
       AMOUNT_PERIOD11 + AMOUNT_PERIOD12 TOTAL,
       UTILITY_ACCOUNT_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID,
       BUDGET_ID,
       ATTRIBUTE01_ID,
       ATTRIBUTE02_ID,
       ATTRIBUTE03_ID,
       ATTRIBUTE04_ID,
       ATTRIBUTE05_ID,
       ATTRIBUTE06_ID,
       ATTRIBUTE07_ID,
       ATTRIBUTE08_ID,
       ATTRIBUTE09_ID,
       ATTRIBUTE10_ID,
       HRS_PERIOD1,
       HRS_PERIOD2,
       HRS_PERIOD3,
       HRS_PERIOD4,
       HRS_PERIOD5,
       HRS_PERIOD6,
       HRS_PERIOD7,
       HRS_PERIOD8,
       HRS_PERIOD9,
       HRS_PERIOD10,
       HRS_PERIOD11,
       HRS_PERIOD12,
       HRS_PERIOD1 + HRS_PERIOD2 + HRS_PERIOD3 + HRS_PERIOD4 + HRS_PERIOD5 + HRS_PERIOD6 +
       HRS_PERIOD7 + HRS_PERIOD8 + HRS_PERIOD9 + HRS_PERIOD10 + HRS_PERIOD11 + HRS_PERIOD12 HRS_TOTAL,
       QTY_PERIOD1,
       QTY_PERIOD2,
       QTY_PERIOD3,
       QTY_PERIOD4,
       QTY_PERIOD5,
       QTY_PERIOD6,
       QTY_PERIOD7,
       QTY_PERIOD8,
       QTY_PERIOD9,
       QTY_PERIOD10,
       QTY_PERIOD11,
       QTY_PERIOD12,
       QTY_PERIOD1 + QTY_PERIOD2 + QTY_PERIOD3 + QTY_PERIOD4 + QTY_PERIOD5 + QTY_PERIOD6 +
       QTY_PERIOD7 + QTY_PERIOD8 + QTY_PERIOD9 + QTY_PERIOD10 + QTY_PERIOD11 + QTY_PERIOD12 QTY_TOTAL
  from (
   select W.EST_MONTHLY_ID EST_MONTHLY_ID,
          max(W.TIME_STAMP) TIME_STAMP,
          max(W.USER_ID) USER_ID,
          W.WORK_ORDER_ID WORK_ORDER_ID,
          W.REVISION REVISION,
          decode(sign(p.fiscal_period - p.month_num),1,w.year,w.year + 1) year,
          max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
          max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
          max(W.DEPARTMENT_ID) DEPARTMENT_ID,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period12,
          max(W.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
          max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
          0 FUTURE_DOLLARS,
          0 HIST_ACTUALS,
          max(W.WO_WORK_ORDER_ID) WO_WORK_ORDER_ID,
          max(W.JOB_TASK_ID) JOB_TASK_ID,
          max(W.SUBSTITUTION_ID) SUBSTITUTION_ID,
          max(W.BUDGET_ID) BUDGET_ID,
          max(W.ATTRIBUTE01_ID) ATTRIBUTE01_ID,
          max(W.ATTRIBUTE02_ID) ATTRIBUTE02_ID,
          max(W.ATTRIBUTE03_ID) ATTRIBUTE03_ID,
          max(W.ATTRIBUTE04_ID) ATTRIBUTE04_ID,
          max(W.ATTRIBUTE05_ID) ATTRIBUTE05_ID,
          max(W.ATTRIBUTE06_ID) ATTRIBUTE06_ID,
          max(W.ATTRIBUTE07_ID) ATTRIBUTE07_ID,
          max(W.ATTRIBUTE08_ID) ATTRIBUTE08_ID,
          max(W.ATTRIBUTE09_ID) ATTRIBUTE09_ID,
          max(W.ATTRIBUTE10_ID) ATTRIBUTE10_ID,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period12,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period12
     from WO_EST_MONTHLY W, PP_TABLE_MONTHS P, WO_EST_MONTHLY_ESCALATION WE, ESTIMATE_CHARGE_TYPE ECT
    where W.EST_MONTHLY_ID = WE.EST_MONTHLY_ID (+)
      and W.YEAR = WE.YEAR (+)
      and W.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
      and W.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
      and ECT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
      and NVL(ECT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
    group by W.EST_MONTHLY_ID, W.WORK_ORDER_ID, W.REVISION, decode(sign(P.FISCAL_PERIOD - P.MONTH_NUM),1,W.YEAR,W.YEAR + 1)
   );


create or replace view wo_est_monthly_fy_sv as
select EST_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       WORK_ORDER_ID,
       REVISION,
       year,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       AMOUNT_PERIOD1,
       AMOUNT_PERIOD2,
       AMOUNT_PERIOD3,
       AMOUNT_PERIOD4,
       AMOUNT_PERIOD5,
       AMOUNT_PERIOD6,
       AMOUNT_PERIOD7,
       AMOUNT_PERIOD8,
       AMOUNT_PERIOD9,
       AMOUNT_PERIOD10,
       AMOUNT_PERIOD11,
       AMOUNT_PERIOD12,
       AMOUNT_PERIOD1 + AMOUNT_PERIOD2 + AMOUNT_PERIOD3 + AMOUNT_PERIOD4 + AMOUNT_PERIOD5 +
       AMOUNT_PERIOD6 + AMOUNT_PERIOD7 + AMOUNT_PERIOD8 + AMOUNT_PERIOD9 + AMOUNT_PERIOD10 +
       AMOUNT_PERIOD11 + AMOUNT_PERIOD12 TOTAL,
       UTILITY_ACCOUNT_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID,
       BUDGET_ID,
       ATTRIBUTE01_ID,
       ATTRIBUTE02_ID,
       ATTRIBUTE03_ID,
       ATTRIBUTE04_ID,
       ATTRIBUTE05_ID,
       ATTRIBUTE06_ID,
       ATTRIBUTE07_ID,
       ATTRIBUTE08_ID,
       ATTRIBUTE09_ID,
       ATTRIBUTE10_ID,
       HRS_PERIOD1,
       HRS_PERIOD2,
       HRS_PERIOD3,
       HRS_PERIOD4,
       HRS_PERIOD5,
       HRS_PERIOD6,
       HRS_PERIOD7,
       HRS_PERIOD8,
       HRS_PERIOD9,
       HRS_PERIOD10,
       HRS_PERIOD11,
       HRS_PERIOD12,
       HRS_PERIOD1 + HRS_PERIOD2 + HRS_PERIOD3 + HRS_PERIOD4 + HRS_PERIOD5 + HRS_PERIOD6 +
       HRS_PERIOD7 + HRS_PERIOD8 + HRS_PERIOD9 + HRS_PERIOD10 + HRS_PERIOD11 + HRS_PERIOD12 HRS_TOTAL,
       QTY_PERIOD1,
       QTY_PERIOD2,
       QTY_PERIOD3,
       QTY_PERIOD4,
       QTY_PERIOD5,
       QTY_PERIOD6,
       QTY_PERIOD7,
       QTY_PERIOD8,
       QTY_PERIOD9,
       QTY_PERIOD10,
       QTY_PERIOD11,
       QTY_PERIOD12,
       QTY_PERIOD1 + QTY_PERIOD2 + QTY_PERIOD3 + QTY_PERIOD4 + QTY_PERIOD5 + QTY_PERIOD6 +
       QTY_PERIOD7 + QTY_PERIOD8 + QTY_PERIOD9 + QTY_PERIOD10 + QTY_PERIOD11 + QTY_PERIOD12 QTY_TOTAL
  from (
   select W.EST_MONTHLY_ID EST_MONTHLY_ID,
          max(W.TIME_STAMP) TIME_STAMP,
          max(W.USER_ID) USER_ID,
          W.WORK_ORDER_ID WORK_ORDER_ID,
          W.REVISION REVISION,
          decode(sign(p.fiscal_period - p.month_num),1,w.year,w.year + 1) year,
          max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
          max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
          max(W.DEPARTMENT_ID) DEPARTMENT_ID,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period12,
          max(W.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
          max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
          0 FUTURE_DOLLARS,
          0 HIST_ACTUALS,
          max(W.WO_WORK_ORDER_ID) WO_WORK_ORDER_ID,
          max(W.JOB_TASK_ID) JOB_TASK_ID,
          max(W.SUBSTITUTION_ID) SUBSTITUTION_ID,
          max(W.BUDGET_ID) BUDGET_ID,
          max(W.ATTRIBUTE01_ID) ATTRIBUTE01_ID,
          max(W.ATTRIBUTE02_ID) ATTRIBUTE02_ID,
          max(W.ATTRIBUTE03_ID) ATTRIBUTE03_ID,
          max(W.ATTRIBUTE04_ID) ATTRIBUTE04_ID,
          max(W.ATTRIBUTE05_ID) ATTRIBUTE05_ID,
          max(W.ATTRIBUTE06_ID) ATTRIBUTE06_ID,
          max(W.ATTRIBUTE07_ID) ATTRIBUTE07_ID,
          max(W.ATTRIBUTE08_ID) ATTRIBUTE08_ID,
          max(W.ATTRIBUTE09_ID) ATTRIBUTE09_ID,
          max(W.ATTRIBUTE10_ID) ATTRIBUTE10_ID,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period12,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period12
     from WO_EST_MONTHLY W, PP_TABLE_MONTHS P, ESTIMATE_CHARGE_TYPE ECT
    where W.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
      and W.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
      and ECT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
      and NVL(ECT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
    group by W.EST_MONTHLY_ID, W.WORK_ORDER_ID, W.REVISION, decode(sign(P.FISCAL_PERIOD - P.MONTH_NUM),1,W.YEAR,W.YEAR + 1)
   );


create or replace view wo_est_monthly_fy_temp_cal as
select max(w.updated) updated, w.est_monthly_id est_monthly_id, max(w.time_stamp) time_stamp, max(w.user_id) user_id, w.work_order_id, w.revision, p.year,
	w.expenditure_type_id, w.est_chg_type_id, w.job_task_id, w.utility_account_id, w.department_id, w.long_description,
	0 future_dollars, 0 hist_actuals, w.substitution_id, w.wo_work_order_id, w.budget_id,
   w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id,
	sum(nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) january,
	sum(nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) february,
	sum(nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) march,
	sum(nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) april,
	sum(nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) may,
	sum(nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) june,
	sum(nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) july,
	sum(nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) august,
	sum(nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) september,
	sum(nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) october,
	sum(nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) november,
	sum(nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) december,
	sum(
		nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0) +
		nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)
		) total,
	sum(nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_jan,
	sum(nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_feb,
	sum(nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_mar,
	sum(nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_apr,
	sum(nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_may,
	sum(nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_jun,
	sum(nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_jul,
	sum(nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_aug,
	sum(nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_sep,
	sum(nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_oct,
	sum(nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_nov,
	sum(nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_dec,
	sum(
		nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)
		) hrs_total,
	sum(nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_jan,
	sum(nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_feb,
	sum(nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_mar,
	sum(nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_apr,
	sum(nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_may,
	sum(nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_jun,
	sum(nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_jul,
	sum(nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_aug,
	sum(nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_sep,
	sum(nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_oct,
	sum(nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_nov,
	sum(nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_dec,
	sum(
		nvl(decode(substr(p.month_number,5,2),'01',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'02',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'03',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'04',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'05',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'06',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'07',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'08',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'09',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'10',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'11',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0) +
		nvl(decode(substr(p.month_number,5,2),'12',decode(p.fiscal_month,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_total
from pp_calendar p, wo_est_monthly_fy_temp w
where p.fiscal_year = w.year
group by w.est_monthly_id, w.work_order_id, w.revision, p.year,
	w.expenditure_type_id, w.est_chg_type_id, w.department_id,
	w.utility_account_id, w.long_description, w.job_task_id, w.substitution_id, w.wo_work_order_id, w.budget_id,
   w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id;


create or replace view wo_est_monthly_month_number as
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '01') MONTH_NUMBER,
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JANUARY) AMOUNT,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where "TEMP_WORK_ORDER"."SESSION_ID" = USERENV('SESSIONID')
   and UPPER("TEMP_WORK_ORDER"."USER_ID") = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and JANUARY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
        EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '02'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(FEBRUARY),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and FEBRUARY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '03'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(MARCH),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and MARCH <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '04'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(APRIL),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and APRIL <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '05'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(MAY),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and MAY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '06'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JUNE),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and JUNE <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '07'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JULY),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and JULY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '08'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(AUGUST),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and AUGUST <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '09'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(SEPTEMBER),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and SEPTEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '10'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(OCTOBER),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and OCTOBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '11'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(NOVEMBER),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and NOVEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '12'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(DECEMBER),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and DECEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID,
          EST_MONTHLY_ID;


create or replace view wo_est_monthly_sv as
select W.EST_MONTHLY_ID EST_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.REVISION REVISION,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) TOTAL,
       W.UTILITY_ACCOUNT_ID UTILITY_ACCOUNT_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WO_WORK_ORDER_ID WO_WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID,
       W.SUBSTITUTION_ID SUBSTITUTION_ID,
       W.BUDGET_ID BUDGET_ID,
       W.ATTRIBUTE01_ID ATTRIBUTE01_ID,
       W.ATTRIBUTE02_ID ATTRIBUTE02_ID,
       W.ATTRIBUTE03_ID ATTRIBUTE03_ID,
       W.ATTRIBUTE04_ID ATTRIBUTE04_ID,
       W.ATTRIBUTE05_ID ATTRIBUTE05_ID,
       W.ATTRIBUTE06_ID ATTRIBUTE06_ID,
       W.ATTRIBUTE07_ID ATTRIBUTE07_ID,
       W.ATTRIBUTE08_ID ATTRIBUTE08_ID,
       W.ATTRIBUTE09_ID ATTRIBUTE09_ID,
       W.ATTRIBUTE10_ID ATTRIBUTE10_ID,
       NVL(W.HRS_JAN, 0) HRS_PERIOD1,
       NVL(W.HRS_FEB, 0) HRS_PERIOD2,
       NVL(W.HRS_MAR, 0) HRS_PERIOD3,
       NVL(W.HRS_APR, 0) HRS_PERIOD4,
       NVL(W.HRS_MAY, 0) HRS_PERIOD5,
       NVL(W.HRS_JUN, 0) HRS_PERIOD6,
       NVL(W.HRS_JUL, 0) HRS_PERIOD7,
       NVL(W.HRS_AUG, 0) HRS_PERIOD8,
       NVL(W.HRS_SEP, 0) HRS_PERIOD9,
       NVL(W.HRS_OCT, 0) HRS_PERIOD10,
       NVL(W.HRS_NOV, 0) HRS_PERIOD11,
       NVL(W.HRS_DEC, 0) HRS_PERIOD12,
       NVL(W.HRS_TOTAL, 0) HRS_TOTAL,
       NVL(W.QTY_JAN, 0) QTY_PERIOD1,
       NVL(W.QTY_FEB, 0) QTY_PERIOD2,
       NVL(W.QTY_MAR, 0) QTY_PERIOD3,
       NVL(W.QTY_APR, 0) QTY_PERIOD4,
       NVL(W.QTY_MAY, 0) QTY_PERIOD5,
       NVL(W.QTY_JUN, 0) QTY_PERIOD6,
       NVL(W.QTY_JUL, 0) QTY_PERIOD7,
       NVL(W.QTY_AUG, 0) QTY_PERIOD8,
       NVL(W.QTY_SEP, 0) QTY_PERIOD9,
       NVL(W.QTY_OCT, 0) QTY_PERIOD10,
       NVL(W.QTY_NOV, 0) QTY_PERIOD11,
       NVL(W.QTY_DEC, 0) QTY_PERIOD12,
       NVL(W.QTY_TOTAL, 0) QTY_TOTAL
  from WO_EST_MONTHLY W, ESTIMATE_CHARGE_TYPE ECT
 where W.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and W.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and ECT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(ECT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0;


create or replace view wo_est_monthly_view as
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       year * 100 + MONTH_NUM MONTH_NUMBER,
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(DECODE(MONTH_NUM,
                  1,
                  NVL(JANUARY, 0),
                  2,
                  NVL(FEBRUARY, 0),
                  3,
                  NVL(MARCH, 0),
                  4,
                  NVL(APRIL, 0),
                  5,
                  NVL(MAY, 0),
                  6,
                  NVL(JUNE, 0),
                  7,
                  NVL(JULY, 0),
                  8,
                  NVL(AUGUST, 0),
                  9,
                  NVL(SEPTEMBER, 0),
                  10,
                  NVL(OCTOBER, 0),
                  11,
                  NVL(NOVEMBER, 0),
                  12,
                  NVL(DECEMBER, 0),
                  0)) AMOUNT,
       sum(DECODE(MONTH_NUM,
                  1,
                  NVL(HRS_JAN, 0),
                  2,
                  NVL(HRS_FEB, 0),
                  3,
                  NVL(HRS_MAR, 0),
                  4,
                  NVL(HRS_APR, 0),
                  5,
                  NVL(HRS_MAY, 0),
                  6,
                  NVL(HRS_JUN, 0),
                  7,
                  NVL(HRS_JUL, 0),
                  8,
                  NVL(HRS_AUG, 0),
                  9,
                  NVL(HRS_SEP, 0),
                  10,
                  NVL(HRS_OCT, 0),
                  11,
                  NVL(HRS_NOV, 0),
                  12,
                  NVL(HRS_DEC, 0),
                  0)) HRS,
       sum(DECODE(MONTH_NUM,
                  1,
                  NVL(QTY_JAN, 0),
                  2,
                  NVL(QTY_FEB, 0),
                  3,
                  NVL(QTY_MAR, 0),
                  4,
                  NVL(QTY_APR, 0),
                  5,
                  NVL(QTY_MAY, 0),
                  6,
                  NVL(QTY_JUN, 0),
                  7,
                  NVL(QTY_JUL, 0),
                  8,
                  NVL(QTY_AUG, 0),
                  9,
                  NVL(QTY_SEP, 0),
                  10,
                  NVL(QTY_OCT, 0),
                  11,
                  NVL(QTY_NOV, 0),
                  12,
                  NVL(QTY_DEC, 0),
                  0)) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID,
      BUDGET_ID,
      ATTRIBUTE01_ID,
      ATTRIBUTE02_ID,
      ATTRIBUTE03_ID,
      ATTRIBUTE04_ID,
      ATTRIBUTE05_ID,
      ATTRIBUTE06_ID,
      ATTRIBUTE07_ID,
      ATTRIBUTE08_ID,
      ATTRIBUTE09_ID,
      ATTRIBUTE10_ID
  from WO_EST_MONTHLY, PP_TABLE_MONTHS
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year * 100 + MONTH_NUM,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID,
        BUDGET_ID,
        ATTRIBUTE01_ID,
        ATTRIBUTE02_ID,
        ATTRIBUTE03_ID,
        ATTRIBUTE04_ID,
        ATTRIBUTE05_ID,
        ATTRIBUTE06_ID,
        ATTRIBUTE07_ID,
        ATTRIBUTE08_ID,
        ATTRIBUTE09_ID,
        ATTRIBUTE10_ID
having((sum(DECODE(MONTH_NUM, 1, NVL(JANUARY, 0), 2, NVL(FEBRUARY, 0), 3, NVL(MARCH, 0), 4, NVL(APRIL, 0), 5, NVL(MAY, 0), 6, NVL(JUNE, 0), 7, NVL(JULY, 0), 8, NVL(AUGUST, 0), 9, NVL(SEPTEMBER, 0), 10, NVL(OCTOBER, 0), 11, NVL(NOVEMBER, 0), 12, NVL(DECEMBER, 0), 0))) <> 0);


create or replace view wo_est_monthly_view_temp as
select WO_EST_MONTHLY.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       year * 100 + MONTH_NUM MONTH_NUMBER,
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(DECODE(MONTH_NUM,
                  1,
                  NVL(JANUARY, 0),
                  2,
                  NVL(FEBRUARY, 0),
                  3,
                  NVL(MARCH, 0),
                  4,
                  NVL(APRIL, 0),
                  5,
                  NVL(MAY, 0),
                  6,
                  NVL(JUNE, 0),
                  7,
                  NVL(JULY, 0),
                  8,
                  NVL(AUGUST, 0),
                  9,
                  NVL(SEPTEMBER, 0),
                  10,
                  NVL(OCTOBER, 0),
                  11,
                  NVL(NOVEMBER, 0),
                  12,
                  NVL(DECEMBER, 0),
                  0)) AMOUNT,
       sum(DECODE(MONTH_NUM,
                  1,
                  NVL(HRS_JAN, 0),
                  2,
                  NVL(HRS_FEB, 0),
                  3,
                  NVL(HRS_MAR, 0),
                  4,
                  NVL(HRS_APR, 0),
                  5,
                  NVL(HRS_MAY, 0),
                  6,
                  NVL(HRS_JUN, 0),
                  7,
                  NVL(HRS_JUL, 0),
                  8,
                  NVL(HRS_AUG, 0),
                  9,
                  NVL(HRS_SEP, 0),
                  10,
                  NVL(HRS_OCT, 0),
                  11,
                  NVL(HRS_NOV, 0),
                  12,
                  NVL(HRS_DEC, 0),
                  0)) HRS,
       sum(DECODE(MONTH_NUM,
                  1,
                  NVL(QTY_JAN, 0),
                  2,
                  NVL(QTY_FEB, 0),
                  3,
                  NVL(QTY_MAR, 0),
                  4,
                  NVL(QTY_APR, 0),
                  5,
                  NVL(QTY_MAY, 0),
                  6,
                  NVL(QTY_JUN, 0),
                  7,
                  NVL(QTY_JUL, 0),
                  8,
                  NVL(QTY_AUG, 0),
                  9,
                  NVL(QTY_SEP, 0),
                  10,
                  NVL(QTY_OCT, 0),
                  11,
                  NVL(QTY_NOV, 0),
                  12,
                  NVL(QTY_DEC, 0),
                  0)) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID,
       BUDGET_ID,
       ATTRIBUTE01_ID,
       ATTRIBUTE02_ID,
       ATTRIBUTE03_ID,
       ATTRIBUTE04_ID,
       ATTRIBUTE05_ID,
       ATTRIBUTE06_ID,
       ATTRIBUTE07_ID,
       ATTRIBUTE08_ID,
       ATTRIBUTE09_ID,
       ATTRIBUTE10_ID
  from WO_EST_MONTHLY, PP_TABLE_MONTHS, TEMP_WORK_ORDER
 where WO_EST_MONTHLY.WORK_ORDER_ID = TEMP_WORK_ORDER.WORK_ORDER_ID
   and TEMP_WORK_ORDER.USER_ID = user
   and TEMP_WORK_ORDER.SESSION_ID = USERENV('sessionid')
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
 group by WO_EST_MONTHLY.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year * 100 + MONTH_NUM,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID,
          BUDGET_ID,
          ATTRIBUTE01_ID,
          ATTRIBUTE02_ID,
          ATTRIBUTE03_ID,
          ATTRIBUTE04_ID,
          ATTRIBUTE05_ID,
          ATTRIBUTE06_ID,
          ATTRIBUTE07_ID,
          ATTRIBUTE08_ID,
          ATTRIBUTE09_ID,
          ATTRIBUTE10_ID
having((sum(DECODE(MONTH_NUM, 1, NVL(JANUARY, 0), 2, NVL(FEBRUARY, 0), 3, NVL(MARCH, 0), 4, NVL(APRIL, 0), 5, NVL(MAY, 0), 6, NVL(JUNE, 0), 7, NVL(JULY, 0), 8, NVL(AUGUST, 0), 9, NVL(SEPTEMBER, 0), 10, NVL(OCTOBER, 0), 11, NVL(NOVEMBER, 0), 12, NVL(DECEMBER, 0), 0))) <> 0);


create or replace view wo_est_summary_view as
select WO_EST_MONTHLY.EST_MONTHLY_ID EST_SUMMARY_ID,
       WO_EST_MONTHLY.EST_CHG_TYPE_ID,
       WO_EST_MONTHLY.EXPENDITURE_TYPE_ID,
       WO_EST_MONTHLY.WORK_ORDER_ID,
       WO_EST_MONTHLY.REVISION,
       WO_EST_MONTHLY.UTILITY_ACCOUNT_ID,
       WO_EST_MONTHLY.DEPARTMENT_ID,
       WO_EST_MONTHLY.HIST_ACTUALS,
       WO_EST_MONTHLY.WO_WORK_ORDER_ID,
       WO_EST_MONTHLY.JOB_TASK_ID,
       WO_EST_MONTHLY.LONG_DESCRIPTION,
       WO_EST_MONTHLY.BUDGET_ID,
       WO_EST_MONTHLY.ATTRIBUTE01_ID,
       WO_EST_MONTHLY.ATTRIBUTE02_ID,
       WO_EST_MONTHLY.ATTRIBUTE03_ID,
       WO_EST_MONTHLY.ATTRIBUTE04_ID,
       WO_EST_MONTHLY.ATTRIBUTE05_ID,
       WO_EST_MONTHLY.ATTRIBUTE06_ID,
       WO_EST_MONTHLY.ATTRIBUTE07_ID,
       WO_EST_MONTHLY.ATTRIBUTE08_ID,
       WO_EST_MONTHLY.ATTRIBUTE09_ID,
       WO_EST_MONTHLY.ATTRIBUTE10_ID,
       sum(WO_EST_MONTHLY.TOTAL) APPROVED_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR, TOTAL, 0)) CURRENT_YEAR_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 1, TOTAL, 0)) YEAR2_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 2, TOTAL, 0)) YEAR3_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 3, TOTAL, 0)) YEAR4_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 4, TOTAL, 0)) YEAR5_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 5, TOTAL, 0)) YEAR6_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 6, TOTAL, 0)) YEAR7_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 7, TOTAL, 0)) YEAR8_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 8, TOTAL, 0)) YEAR9_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 9, TOTAL, 0)) YEAR10_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 10, TOTAL, 0)) YEAR11_DOLLARS,
       sum(DECODE(SIGN(WO_EST_MONTHLY.YEAR - (BUDGET_VERSION.START_YEAR + 10)), 1, TOTAL, 0)) FUTURE_DOLLARS
  from WO_EST_MONTHLY, BUDGET_VERSION, BUDGET_VERSION_FUND_PROJ
 where WO_EST_MONTHLY.REVISION = BUDGET_VERSION_FUND_PROJ.REVISION
   and WO_EST_MONTHLY.WORK_ORDER_ID = BUDGET_VERSION_FUND_PROJ.WORK_ORDER_ID
   and BUDGET_VERSION_FUND_PROJ.ACTIVE = 1
   and BUDGET_VERSION_FUND_PROJ.BUDGET_VERSION_ID =
       (select max(BUDGET_VERSION_ID)
          from BUDGET_VERSION_FUND_PROJ A
         where A.WORK_ORDER_ID = BUDGET_VERSION_FUND_PROJ.WORK_ORDER_ID
           and A.REVISION = BUDGET_VERSION_FUND_PROJ.REVISION
           and A.ACTIVE = 1)
   and BUDGET_VERSION.BUDGET_VERSION_ID = BUDGET_VERSION_FUND_PROJ.BUDGET_VERSION_ID
   and exists (select 1
          from WORK_ORDER_CONTROL B
         where B.WORK_ORDER_ID = WO_EST_MONTHLY.WORK_ORDER_ID
           and B.FUNDING_WO_INDICATOR = 1)
 group by WO_EST_MONTHLY.EST_MONTHLY_ID,
          WO_EST_MONTHLY.EST_CHG_TYPE_ID,
          WO_EST_MONTHLY.EXPENDITURE_TYPE_ID,
          WO_EST_MONTHLY.WORK_ORDER_ID,
          WO_EST_MONTHLY.REVISION,
          WO_EST_MONTHLY.UTILITY_ACCOUNT_ID,
          WO_EST_MONTHLY.DEPARTMENT_ID,
          WO_EST_MONTHLY.HIST_ACTUALS,
          WO_EST_MONTHLY.WO_WORK_ORDER_ID,
          WO_EST_MONTHLY.JOB_TASK_ID,
          WO_EST_MONTHLY.LONG_DESCRIPTION,
          WO_EST_MONTHLY.BUDGET_ID,
          WO_EST_MONTHLY.ATTRIBUTE01_ID,
          WO_EST_MONTHLY.ATTRIBUTE02_ID,
          WO_EST_MONTHLY.ATTRIBUTE03_ID,
          WO_EST_MONTHLY.ATTRIBUTE04_ID,
          WO_EST_MONTHLY.ATTRIBUTE05_ID,
          WO_EST_MONTHLY.ATTRIBUTE06_ID,
          WO_EST_MONTHLY.ATTRIBUTE07_ID,
          WO_EST_MONTHLY.ATTRIBUTE08_ID,
          WO_EST_MONTHLY.ATTRIBUTE09_ID,
          WO_EST_MONTHLY.ATTRIBUTE10_ID
union all
select WO_EST_MONTHLY.EST_MONTHLY_ID EST_SUMMARY_ID,
       WO_EST_MONTHLY.EST_CHG_TYPE_ID,
       WO_EST_MONTHLY.EXPENDITURE_TYPE_ID,
       WO_EST_MONTHLY.WORK_ORDER_ID,
       WO_EST_MONTHLY.REVISION,
       WO_EST_MONTHLY.UTILITY_ACCOUNT_ID,
       WO_EST_MONTHLY.DEPARTMENT_ID,
       WO_EST_MONTHLY.HIST_ACTUALS,
       WO_EST_MONTHLY.WO_WORK_ORDER_ID,
       WO_EST_MONTHLY.JOB_TASK_ID,
       WO_EST_MONTHLY.LONG_DESCRIPTION,
       WO_EST_MONTHLY.BUDGET_ID,
       WO_EST_MONTHLY.ATTRIBUTE01_ID,
       WO_EST_MONTHLY.ATTRIBUTE02_ID,
       WO_EST_MONTHLY.ATTRIBUTE03_ID,
       WO_EST_MONTHLY.ATTRIBUTE04_ID,
       WO_EST_MONTHLY.ATTRIBUTE05_ID,
       WO_EST_MONTHLY.ATTRIBUTE06_ID,
       WO_EST_MONTHLY.ATTRIBUTE07_ID,
       WO_EST_MONTHLY.ATTRIBUTE08_ID,
       WO_EST_MONTHLY.ATTRIBUTE09_ID,
       WO_EST_MONTHLY.ATTRIBUTE10_ID,
       sum(WO_EST_MONTHLY.TOTAL) APPROVED_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')), WO_EST_MONTHLY.YEAR, TOTAL, 0)) CURRENT_YEAR_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 1, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR2_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 2, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR3_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 3, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR4_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 4, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR5_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 5, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR6_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 6, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR7_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 7, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR8_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 8, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR9_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 9, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR10_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 10, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR11_DOLLARS,
       sum(DECODE(SIGN(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) - (WO_EST_MONTHLY.YEAR + 10)),
                  1,
                  TOTAL,
                  0)) FUTURE_DOLLARS
  from WO_EST_MONTHLY, WORK_ORDER_APPROVAL WOA
 where WO_EST_MONTHLY.WORK_ORDER_ID = WOA.WORK_ORDER_ID
   and WO_EST_MONTHLY.REVISION = WOA.REVISION
   and exists (select 1
          from WORK_ORDER_CONTROL B
         where B.WORK_ORDER_ID = WO_EST_MONTHLY.WORK_ORDER_ID
           and B.FUNDING_WO_INDICATOR = 0)
 group by WO_EST_MONTHLY.EST_MONTHLY_ID,
          WO_EST_MONTHLY.EST_CHG_TYPE_ID,
          WO_EST_MONTHLY.EXPENDITURE_TYPE_ID,
          WO_EST_MONTHLY.WORK_ORDER_ID,
          WO_EST_MONTHLY.REVISION,
          WO_EST_MONTHLY.UTILITY_ACCOUNT_ID,
          WO_EST_MONTHLY.DEPARTMENT_ID,
          WO_EST_MONTHLY.HIST_ACTUALS,
          WO_EST_MONTHLY.WO_WORK_ORDER_ID,
          WO_EST_MONTHLY.JOB_TASK_ID,
          WO_EST_MONTHLY.LONG_DESCRIPTION,
          WO_EST_MONTHLY.BUDGET_ID,
          WO_EST_MONTHLY.ATTRIBUTE01_ID,
          WO_EST_MONTHLY.ATTRIBUTE02_ID,
          WO_EST_MONTHLY.ATTRIBUTE03_ID,
          WO_EST_MONTHLY.ATTRIBUTE04_ID,
          WO_EST_MONTHLY.ATTRIBUTE05_ID,
          WO_EST_MONTHLY.ATTRIBUTE06_ID,
          WO_EST_MONTHLY.ATTRIBUTE07_ID,
          WO_EST_MONTHLY.ATTRIBUTE08_ID,
          WO_EST_MONTHLY.ATTRIBUTE09_ID,
          WO_EST_MONTHLY.ATTRIBUTE10_ID;


alter table wo_est_actuals_temp add attribute01_id varchar2(254);
alter table wo_est_actuals_temp add attribute02_id varchar2(254);
alter table wo_est_actuals_temp add attribute03_id varchar2(254);
alter table wo_est_actuals_temp add attribute04_id varchar2(254);
alter table wo_est_actuals_temp add attribute05_id varchar2(254);
alter table wo_est_actuals_temp add attribute06_id varchar2(254);
alter table wo_est_actuals_temp add attribute07_id varchar2(254);
alter table wo_est_actuals_temp add attribute08_id varchar2(254);
alter table wo_est_actuals_temp add attribute09_id varchar2(254);
alter table wo_est_actuals_temp add attribute10_id varchar2(254);

comment on column wo_est_actuals_temp.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_actuals_temp2 add attribute01_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute02_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute03_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute04_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute05_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute06_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute07_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute08_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute09_id varchar2(254);
alter table wo_est_actuals_temp2 add attribute10_id varchar2(254);

comment on column wo_est_actuals_temp2.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_actuals_temp2.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_actuals_temp add include_attr01 number(22);
alter table wo_est_actuals_temp add include_attr02 number(22);
alter table wo_est_actuals_temp add include_attr03 number(22);
alter table wo_est_actuals_temp add include_attr04 number(22);
alter table wo_est_actuals_temp add include_attr05 number(22);
alter table wo_est_actuals_temp add include_attr06 number(22);
alter table wo_est_actuals_temp add include_attr07 number(22);
alter table wo_est_actuals_temp add include_attr08 number(22);
alter table wo_est_actuals_temp add include_attr09 number(22);
alter table wo_est_actuals_temp add include_attr10 number(22);

comment on column wo_est_actuals_temp.include_attr01 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr02 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr03 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr04 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr05 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr06 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr07 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr08 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr09 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp.include_attr10 is 'Is this additional, optional estimate attribute included in the update with actuals process?';

alter table wo_est_actuals_temp2 add include_attr01 number(22);
alter table wo_est_actuals_temp2 add include_attr02 number(22);
alter table wo_est_actuals_temp2 add include_attr03 number(22);
alter table wo_est_actuals_temp2 add include_attr04 number(22);
alter table wo_est_actuals_temp2 add include_attr05 number(22);
alter table wo_est_actuals_temp2 add include_attr06 number(22);
alter table wo_est_actuals_temp2 add include_attr07 number(22);
alter table wo_est_actuals_temp2 add include_attr08 number(22);
alter table wo_est_actuals_temp2 add include_attr09 number(22);
alter table wo_est_actuals_temp2 add include_attr10 number(22);

comment on column wo_est_actuals_temp2.include_attr01 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr02 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr03 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr04 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr05 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr06 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr07 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr08 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr09 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_actuals_temp2.include_attr10 is 'Is this additional, optional estimate attribute included in the update with actuals process?';

alter table wo_est_update_with_act_temp add include_attr01 number(22);
alter table wo_est_update_with_act_temp add include_attr02 number(22);
alter table wo_est_update_with_act_temp add include_attr03 number(22);
alter table wo_est_update_with_act_temp add include_attr04 number(22);
alter table wo_est_update_with_act_temp add include_attr05 number(22);
alter table wo_est_update_with_act_temp add include_attr06 number(22);
alter table wo_est_update_with_act_temp add include_attr07 number(22);
alter table wo_est_update_with_act_temp add include_attr08 number(22);
alter table wo_est_update_with_act_temp add include_attr09 number(22);
alter table wo_est_update_with_act_temp add include_attr10 number(22);

comment on column wo_est_update_with_act_temp.include_attr01 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr02 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr03 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr04 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr05 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr06 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr07 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr08 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr09 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_update_with_act_temp.include_attr10 is 'Is this additional, optional estimate attribute included in the update with actuals process?';

alter table wo_est_hierarchy add include_attr01 number(22);
alter table wo_est_hierarchy add include_attr02 number(22);
alter table wo_est_hierarchy add include_attr03 number(22);
alter table wo_est_hierarchy add include_attr04 number(22);
alter table wo_est_hierarchy add include_attr05 number(22);
alter table wo_est_hierarchy add include_attr06 number(22);
alter table wo_est_hierarchy add include_attr07 number(22);
alter table wo_est_hierarchy add include_attr08 number(22);
alter table wo_est_hierarchy add include_attr09 number(22);
alter table wo_est_hierarchy add include_attr10 number(22);

comment on column wo_est_hierarchy.include_attr01 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr02 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr03 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr04 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr05 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr06 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr07 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr08 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr09 is 'Is this additional, optional estimate attribute included in the update with actuals process?';
comment on column wo_est_hierarchy.include_attr10 is 'Is this additional, optional estimate attribute included in the update with actuals process?';

alter table wo_est_monthly add uwa_include_attr01 number(22);
alter table wo_est_monthly add uwa_include_attr02 number(22);
alter table wo_est_monthly add uwa_include_attr03 number(22);
alter table wo_est_monthly add uwa_include_attr04 number(22);
alter table wo_est_monthly add uwa_include_attr05 number(22);
alter table wo_est_monthly add uwa_include_attr06 number(22);
alter table wo_est_monthly add uwa_include_attr07 number(22);
alter table wo_est_monthly add uwa_include_attr08 number(22);
alter table wo_est_monthly add uwa_include_attr09 number(22);
alter table wo_est_monthly add uwa_include_attr10 number(22);

comment on column wo_est_monthly.uwa_include_attr01 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr02 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr03 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr04 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr05 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr06 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr07 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr08 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr09 is 'Column used by update with actuals process.';
comment on column wo_est_monthly.uwa_include_attr10 is 'Column used by update with actuals process.';

/*
create table update_with_actuals_addl_attr (
   "LEVEL" number(22),
   user_id varchar2(18),
   time_stampe date,
   attribute01_column varchar2(35),
   attribute02_column varchar2(35),
   attribute03_column varchar2(35),
   attribute04_column varchar2(35),
   attribute05_column varchar2(35),
   attribute06_column varchar2(35),
   attribute07_column varchar2(35),
   attribute08_column varchar2(35),
   attribute09_column varchar2(35),
   attribute10_column varchar2(35)
   );
alter table update_with_actuals_addl_attr add constraint update_with_act_addl_pk primary key ("LEVEL");
*/

alter table wo_est_cotenant_spread_temp rename column budget_id to id;


alter table wo_interface_monthly_temp add budget_id number(22);
alter table wo_interface_monthly_temp add attribute01_id varchar2(254);
alter table wo_interface_monthly_temp add attribute02_id varchar2(254);
alter table wo_interface_monthly_temp add attribute03_id varchar2(254);
alter table wo_interface_monthly_temp add attribute04_id varchar2(254);
alter table wo_interface_monthly_temp add attribute05_id varchar2(254);
alter table wo_interface_monthly_temp add attribute06_id varchar2(254);
alter table wo_interface_monthly_temp add attribute07_id varchar2(254);
alter table wo_interface_monthly_temp add attribute08_id varchar2(254);
alter table wo_interface_monthly_temp add attribute09_id varchar2(254);
alter table wo_interface_monthly_temp add attribute10_id varchar2(254);

comment on column wo_interface_monthly_temp.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_interface_monthly_temp.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_temp.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_interface_monthly_ids add budget_id number(22);
alter table wo_interface_monthly_ids add attribute01_id varchar2(254);
alter table wo_interface_monthly_ids add attribute02_id varchar2(254);
alter table wo_interface_monthly_ids add attribute03_id varchar2(254);
alter table wo_interface_monthly_ids add attribute04_id varchar2(254);
alter table wo_interface_monthly_ids add attribute05_id varchar2(254);
alter table wo_interface_monthly_ids add attribute06_id varchar2(254);
alter table wo_interface_monthly_ids add attribute07_id varchar2(254);
alter table wo_interface_monthly_ids add attribute08_id varchar2(254);
alter table wo_interface_monthly_ids add attribute09_id varchar2(254);
alter table wo_interface_monthly_ids add attribute10_id varchar2(254);

comment on column wo_interface_monthly_ids.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_interface_monthly_ids.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_ids.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_monthly_upload add budget_id varchar2(35);
alter table wo_est_monthly_upload add attribute01_id varchar2(254);
alter table wo_est_monthly_upload add attribute02_id varchar2(254);
alter table wo_est_monthly_upload add attribute03_id varchar2(254);
alter table wo_est_monthly_upload add attribute04_id varchar2(254);
alter table wo_est_monthly_upload add attribute05_id varchar2(254);
alter table wo_est_monthly_upload add attribute06_id varchar2(254);
alter table wo_est_monthly_upload add attribute07_id varchar2(254);
alter table wo_est_monthly_upload add attribute08_id varchar2(254);
alter table wo_est_monthly_upload add attribute09_id varchar2(254);
alter table wo_est_monthly_upload add attribute10_id varchar2(254);
alter table wo_est_monthly_upload add mapped_budget_id number(22);

comment on column wo_est_monthly_upload.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_monthly_upload.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload.mapped_budget_id is 'For future use. This is the budget id related to this estimate.';

alter table wo_est_monthly_upload_arch add budget_id number(22);
alter table wo_est_monthly_upload_arch add attribute01_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute02_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute03_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute04_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute05_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute06_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute07_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute08_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute09_id varchar2(254);
alter table wo_est_monthly_upload_arch add attribute10_id varchar2(254);

comment on column wo_est_monthly_upload_arch.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_monthly_upload_arch.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_upload_arch.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_interface_monthly add ext_budget_number varchar2(254);
alter table wo_interface_monthly add budget_id number(22);
alter table wo_interface_monthly_arc add ext_budget_number varchar2(254);
alter table wo_interface_monthly_arc add budget_id number(22);

comment on column wo_interface_monthly.ext_budget_number is 'For future use. This is the budget number used for mapping the budget id related to this estimate.';
comment on column wo_interface_monthly.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_interface_monthly_arc.ext_budget_number is 'For future use. This is the budget number used for mapping the budget id related to this estimate.';
comment on column wo_interface_monthly_arc.budget_id is 'For future use. This is the budget id related to this estimate.';


alter table wo_interface_monthly add ext_attribute01 varchar2(254);
alter table wo_interface_monthly add ext_attribute02 varchar2(254);
alter table wo_interface_monthly add ext_attribute03 varchar2(254);
alter table wo_interface_monthly add ext_attribute04 varchar2(254);
alter table wo_interface_monthly add ext_attribute05 varchar2(254);
alter table wo_interface_monthly add ext_attribute06 varchar2(254);
alter table wo_interface_monthly add ext_attribute07 varchar2(254);
alter table wo_interface_monthly add ext_attribute08 varchar2(254);
alter table wo_interface_monthly add ext_attribute09 varchar2(254);
alter table wo_interface_monthly add ext_attribute10 varchar2(254);
alter table wo_interface_monthly add attribute01_id varchar2(254);
alter table wo_interface_monthly add attribute02_id varchar2(254);
alter table wo_interface_monthly add attribute03_id varchar2(254);
alter table wo_interface_monthly add attribute04_id varchar2(254);
alter table wo_interface_monthly add attribute05_id varchar2(254);
alter table wo_interface_monthly add attribute06_id varchar2(254);
alter table wo_interface_monthly add attribute07_id varchar2(254);
alter table wo_interface_monthly add attribute08_id varchar2(254);
alter table wo_interface_monthly add attribute09_id varchar2(254);
alter table wo_interface_monthly add attribute10_id varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute01 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute02 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute03 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute04 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute05 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute06 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute07 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute08 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute09 varchar2(254);
alter table wo_interface_monthly_arc add ext_attribute10 varchar2(254);
alter table wo_interface_monthly_arc add attribute01_id varchar2(254);
alter table wo_interface_monthly_arc add attribute02_id varchar2(254);
alter table wo_interface_monthly_arc add attribute03_id varchar2(254);
alter table wo_interface_monthly_arc add attribute04_id varchar2(254);
alter table wo_interface_monthly_arc add attribute05_id varchar2(254);
alter table wo_interface_monthly_arc add attribute06_id varchar2(254);
alter table wo_interface_monthly_arc add attribute07_id varchar2(254);
alter table wo_interface_monthly_arc add attribute08_id varchar2(254);
alter table wo_interface_monthly_arc add attribute09_id varchar2(254);
alter table wo_interface_monthly_arc add attribute10_id varchar2(254);

comment on column wo_interface_monthly.ext_attribute01 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute02 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute03 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute04 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute05 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute06 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute07 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute08 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute09 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.ext_attribute10 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute01 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute02 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute03 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute04 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute05 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute06 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute07 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute08 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute09 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.ext_attribute10 is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_interface_monthly_arc.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_monthly_default add budget_id number(22);
alter table wo_est_monthly_default add attribute01_id varchar2(254);
alter table wo_est_monthly_default add attribute02_id varchar2(254);
alter table wo_est_monthly_default add attribute03_id varchar2(254);
alter table wo_est_monthly_default add attribute04_id varchar2(254);
alter table wo_est_monthly_default add attribute05_id varchar2(254);
alter table wo_est_monthly_default add attribute06_id varchar2(254);
alter table wo_est_monthly_default add attribute07_id varchar2(254);
alter table wo_est_monthly_default add attribute08_id varchar2(254);
alter table wo_est_monthly_default add attribute09_id varchar2(254);
alter table wo_est_monthly_default add attribute10_id varchar2(254);

comment on column wo_est_monthly_default.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_monthly_default.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_default.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_monthly_cr add budget_id number(22);
alter table wo_est_monthly_cr add attribute01_id varchar2(254);
alter table wo_est_monthly_cr add attribute02_id varchar2(254);
alter table wo_est_monthly_cr add attribute03_id varchar2(254);
alter table wo_est_monthly_cr add attribute04_id varchar2(254);
alter table wo_est_monthly_cr add attribute05_id varchar2(254);
alter table wo_est_monthly_cr add attribute06_id varchar2(254);
alter table wo_est_monthly_cr add attribute07_id varchar2(254);
alter table wo_est_monthly_cr add attribute08_id varchar2(254);
alter table wo_est_monthly_cr add attribute09_id varchar2(254);
alter table wo_est_monthly_cr add attribute10_id varchar2(254);

comment on column wo_est_monthly_cr.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_monthly_cr.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_cr.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table budget_monthly_data_cr add attribute01_id varchar2(254);
alter table budget_monthly_data_cr add attribute02_id varchar2(254);
alter table budget_monthly_data_cr add attribute03_id varchar2(254);
alter table budget_monthly_data_cr add attribute04_id varchar2(254);
alter table budget_monthly_data_cr add attribute05_id varchar2(254);
alter table budget_monthly_data_cr add attribute06_id varchar2(254);
alter table budget_monthly_data_cr add attribute07_id varchar2(254);
alter table budget_monthly_data_cr add attribute08_id varchar2(254);
alter table budget_monthly_data_cr add attribute09_id varchar2(254);
alter table budget_monthly_data_cr add attribute10_id varchar2(254);

comment on column budget_monthly_data_cr.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_cr.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';


alter table wo_est_monthly_arch add hrs_jan number(22,4);
alter table wo_est_monthly_arch add hrs_feb number(22,4);
alter table wo_est_monthly_arch add hrs_mar number(22,4);
alter table wo_est_monthly_arch add hrs_apr number(22,4);
alter table wo_est_monthly_arch add hrs_may number(22,4);
alter table wo_est_monthly_arch add hrs_jun number(22,4);
alter table wo_est_monthly_arch add hrs_jul number(22,4);
alter table wo_est_monthly_arch add hrs_aug number(22,4);
alter table wo_est_monthly_arch add hrs_sep number(22,4);
alter table wo_est_monthly_arch add hrs_oct number(22,4);
alter table wo_est_monthly_arch add hrs_nov number(22,4);
alter table wo_est_monthly_arch add hrs_dec number(22,4);
alter table wo_est_monthly_arch add hrs_total number(22,4);
alter table wo_est_monthly_arch add qty_jan number(22,4);
alter table wo_est_monthly_arch add qty_feb number(22,4);
alter table wo_est_monthly_arch add qty_mar number(22,4);
alter table wo_est_monthly_arch add qty_apr number(22,4);
alter table wo_est_monthly_arch add qty_may number(22,4);
alter table wo_est_monthly_arch add qty_jun number(22,4);
alter table wo_est_monthly_arch add qty_jul number(22,4);
alter table wo_est_monthly_arch add qty_aug number(22,4);
alter table wo_est_monthly_arch add qty_sep number(22,4);
alter table wo_est_monthly_arch add qty_oct number(22,4);
alter table wo_est_monthly_arch add qty_nov number(22,4);
alter table wo_est_monthly_arch add qty_dec number(22,4);
alter table wo_est_monthly_arch add qty_total number(22,4);
alter table wo_est_monthly_arch add wo_work_order_id number(22);
alter table wo_est_monthly_arch add budget_id number(22);
alter table wo_est_monthly_arch add attribute01_id varchar2(254);
alter table wo_est_monthly_arch add attribute02_id varchar2(254);
alter table wo_est_monthly_arch add attribute03_id varchar2(254);
alter table wo_est_monthly_arch add attribute04_id varchar2(254);
alter table wo_est_monthly_arch add attribute05_id varchar2(254);
alter table wo_est_monthly_arch add attribute06_id varchar2(254);
alter table wo_est_monthly_arch add attribute07_id varchar2(254);
alter table wo_est_monthly_arch add attribute08_id varchar2(254);
alter table wo_est_monthly_arch add attribute09_id varchar2(254);
alter table wo_est_monthly_arch add attribute10_id varchar2(254);

comment on column wo_est_monthly_arch.hrs_jan is 'Estimated hours for January.';
comment on column wo_est_monthly_arch.hrs_feb is 'Estimated hours for February.';
comment on column wo_est_monthly_arch.hrs_mar is 'Estimated hours for March.';
comment on column wo_est_monthly_arch.hrs_apr is 'Estimated hours for April.';
comment on column wo_est_monthly_arch.hrs_may is 'Estimated hours for May.';
comment on column wo_est_monthly_arch.hrs_jun is 'Estimated hours for June.';
comment on column wo_est_monthly_arch.hrs_jul is 'Estimated hours for July.';
comment on column wo_est_monthly_arch.hrs_aug is 'Estimated hours for August.';
comment on column wo_est_monthly_arch.hrs_sep is 'Estimated hours for September.';
comment on column wo_est_monthly_arch.hrs_oct is 'Estimated hours for October.';
comment on column wo_est_monthly_arch.hrs_nov is 'Estimated hours for November.';
comment on column wo_est_monthly_arch.hrs_dec is 'Estimated hours for December.';
comment on column wo_est_monthly_arch.hrs_total is 'Estimated hours for the year.';
comment on column wo_est_monthly_arch.qty_jan is 'Estimated quantity for January.';
comment on column wo_est_monthly_arch.qty_feb is 'Estimated quantity for February.';
comment on column wo_est_monthly_arch.qty_mar is 'Estimated quantity for March.';
comment on column wo_est_monthly_arch.qty_apr is 'Estimated quantity for April.';
comment on column wo_est_monthly_arch.qty_may is 'Estimated quantity for May.';
comment on column wo_est_monthly_arch.qty_jun is 'Estimated quantity for June.';
comment on column wo_est_monthly_arch.qty_jul is 'Estimated quantity for July.';
comment on column wo_est_monthly_arch.qty_aug is 'Estimated quantity for August.';
comment on column wo_est_monthly_arch.qty_sep is 'Estimated quantity for September.';
comment on column wo_est_monthly_arch.qty_oct is 'Estimated quantity for October.';
comment on column wo_est_monthly_arch.qty_nov is 'Estimated quantity for November.';
comment on column wo_est_monthly_arch.qty_dec is 'Estimated quantity for December.';
comment on column wo_est_monthly_arch.qty_total is 'Estimated quantity for the year.';
comment on column wo_est_monthly_arch.wo_work_order_id is 'For Funding Project estimates this is the work order id related to this estimate.';
comment on column wo_est_monthly_arch.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_monthly_arch.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_monthly_arch.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';


alter table budget_monthly_data_arch add attribute01_id varchar2(254);
alter table budget_monthly_data_arch add attribute02_id varchar2(254);
alter table budget_monthly_data_arch add attribute03_id varchar2(254);
alter table budget_monthly_data_arch add attribute04_id varchar2(254);
alter table budget_monthly_data_arch add attribute05_id varchar2(254);
alter table budget_monthly_data_arch add attribute06_id varchar2(254);
alter table budget_monthly_data_arch add attribute07_id varchar2(254);
alter table budget_monthly_data_arch add attribute08_id varchar2(254);
alter table budget_monthly_data_arch add attribute09_id varchar2(254);
alter table budget_monthly_data_arch add attribute10_id varchar2(254);

comment on column budget_monthly_data_arch.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_monthly_data_arch.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table budget_approval_data add attribute01_id varchar2(254);
alter table budget_approval_data add attribute02_id varchar2(254);
alter table budget_approval_data add attribute03_id varchar2(254);
alter table budget_approval_data add attribute04_id varchar2(254);
alter table budget_approval_data add attribute05_id varchar2(254);
alter table budget_approval_data add attribute06_id varchar2(254);
alter table budget_approval_data add attribute07_id varchar2(254);
alter table budget_approval_data add attribute08_id varchar2(254);
alter table budget_approval_data add attribute09_id varchar2(254);
alter table budget_approval_data add attribute10_id varchar2(254);

comment on column budget_approval_data.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table budget_approval_data_arch add attribute01_id varchar2(254);
alter table budget_approval_data_arch add attribute02_id varchar2(254);
alter table budget_approval_data_arch add attribute03_id varchar2(254);
alter table budget_approval_data_arch add attribute04_id varchar2(254);
alter table budget_approval_data_arch add attribute05_id varchar2(254);
alter table budget_approval_data_arch add attribute06_id varchar2(254);
alter table budget_approval_data_arch add attribute07_id varchar2(254);
alter table budget_approval_data_arch add attribute08_id varchar2(254);
alter table budget_approval_data_arch add attribute09_id varchar2(254);
alter table budget_approval_data_arch add attribute10_id varchar2(254);

comment on column budget_approval_data_arch.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column budget_approval_data_arch.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_rate_default add budget_id number(22);
alter table wo_est_rate_default add attribute01_id varchar2(254);
alter table wo_est_rate_default add attribute02_id varchar2(254);
alter table wo_est_rate_default add attribute03_id varchar2(254);
alter table wo_est_rate_default add attribute04_id varchar2(254);
alter table wo_est_rate_default add attribute05_id varchar2(254);
alter table wo_est_rate_default add attribute06_id varchar2(254);
alter table wo_est_rate_default add attribute07_id varchar2(254);
alter table wo_est_rate_default add attribute08_id varchar2(254);
alter table wo_est_rate_default add attribute09_id varchar2(254);
alter table wo_est_rate_default add attribute10_id varchar2(254);

comment on column wo_est_rate_default.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_rate_default.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_default.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table wo_est_rate_filter add budget_id number(22);
alter table wo_est_rate_filter add attribute01_id varchar2(254);
alter table wo_est_rate_filter add attribute02_id varchar2(254);
alter table wo_est_rate_filter add attribute03_id varchar2(254);
alter table wo_est_rate_filter add attribute04_id varchar2(254);
alter table wo_est_rate_filter add attribute05_id varchar2(254);
alter table wo_est_rate_filter add attribute06_id varchar2(254);
alter table wo_est_rate_filter add attribute07_id varchar2(254);
alter table wo_est_rate_filter add attribute08_id varchar2(254);
alter table wo_est_rate_filter add attribute09_id varchar2(254);
alter table wo_est_rate_filter add attribute10_id varchar2(254);

comment on column wo_est_rate_filter.budget_id is 'For future use. This is the budget id related to this estimate.';
comment on column wo_est_rate_filter.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column wo_est_rate_filter.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';


create or replace view budget_approval_data_fy as
select w.budget_monthly_id budget_monthly_id, w.approval_number, max(w.time_stamp) time_stamp, max(w.user_id) user_id, w.budget_id, w.budget_version_id, p.fiscal_year year,
	w.expenditure_type_id, w.est_chg_type_id, w.budget_plant_class_id, w.job_task_id, w.department_id,
   w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id,
   w.long_description,
	0 future_dollars, 0 hist_actuals, w.work_order_id,
	sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) january,
	sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) february,
	sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) march,
	sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) april,
	sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) may,
	sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) june,
	sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) july,
	sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) august,
	sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) september,
	sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) october,
	sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) november,
	sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) december,
	sum(
		nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)
		) total,
	sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) jan_local,
	sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) feb_local,
	sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) mar_local,
	sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) apr_local,
	sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) may_local,
	sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) jun_local,
	sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) jul_local,
	sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) aug_local,
	sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) sep_local,
	sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) oct_local,
	sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) nov_local,
	sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) dec_local,
	sum(
		nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)
		) tot_local
from pp_calendar p, budget_approval_data w
where p.year = w.year
group by w.budget_monthly_id, w.approval_number, w.budget_id, w.budget_version_id, p.fiscal_year,
	w.expenditure_type_id, w.est_chg_type_id, w.budget_plant_class_id, w.job_task_id, w.department_id,
   w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id,
   w.long_description, w.work_order_id;


create or replace view budget_monthly_data_fy as
select w.budget_monthly_id budget_monthly_id, max(w.time_stamp) time_stamp, max(w.user_id) user_id, w.budget_id, w.budget_version_id, p.fiscal_year year,
	w.expenditure_type_id, w.est_chg_type_id, w.budget_plant_class_id, w.job_task_id, w.department_id,
   w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id,
   w.long_description,
	0 future_dollars, 0 hist_actuals, w.work_order_id,
	sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) january,
	sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) february,
	sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) march,
	sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) april,
	sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) may,
	sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) june,
	sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) july,
	sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) august,
	sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) september,
	sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) october,
	sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) november,
	sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) december,
	sum(
		nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
		nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)
		) total,
	sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) jan_local,
	sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) feb_local,
	sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) mar_local,
	sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) apr_local,
	sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) may_local,
	sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) jun_local,
	sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) jul_local,
	sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) aug_local,
	sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) sep_local,
	sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) oct_local,
	sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) nov_local,
	sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)) dec_local,
	sum(
		nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0) +
		nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.jan_local,'02',w.feb_local,'03',w.mar_local,'04',w.apr_local,'05',w.may_local,'06',w.jun_local,'07',w.jul_local,'08',w.aug_local,'09',w.sep_local,'10',w.oct_local,'11',w.nov_local,'12',w.dec_local),0),0)
		) tot_local
from pp_calendar p, budget_monthly_data w
where p.year = w.year
group by w.budget_monthly_id, w.budget_id, w.budget_version_id, p.fiscal_year,
	w.expenditure_type_id, w.est_chg_type_id, w.budget_plant_class_id, w.job_task_id, w.department_id,
   w.attribute01_id, w.attribute02_id, w.attribute03_id, w.attribute04_id, w.attribute05_id, w.attribute06_id, w.attribute07_id, w.attribute08_id, w.attribute09_id, w.attribute10_id,
   w.long_description, w.work_order_id;


create or replace view budg_monthly_data_deesc_sv as
select W.BUDGET_MONTHLY_ID BUDGET_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.BUDGET_ID BUDGET_ID,
       W.BUDGET_VERSION_ID BUDGET_VERSION_ID,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) - NVL(WE.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) - NVL(WE.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) - NVL(WE.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) - NVL(WE.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) - NVL(WE.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) - NVL(WE.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) - NVL(WE.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) - NVL(WE.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) - NVL(WE.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) - NVL(WE.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) - NVL(WE.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) - NVL(WE.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) - NVL(WE.TOTAL, 0) TOTAL,
       W.BUDGET_PLANT_CLASS_ID BUDGET_PLANT_CLASS_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID,
       W.ATTRIBUTE01_ID ATTRIBUTE01_ID,
       W.ATTRIBUTE02_ID ATTRIBUTE02_ID,
       W.ATTRIBUTE03_ID ATTRIBUTE03_ID,
       W.ATTRIBUTE04_ID ATTRIBUTE04_ID,
       W.ATTRIBUTE05_ID ATTRIBUTE05_ID,
       W.ATTRIBUTE06_ID ATTRIBUTE06_ID,
       W.ATTRIBUTE07_ID ATTRIBUTE07_ID,
       W.ATTRIBUTE08_ID ATTRIBUTE08_ID,
       W.ATTRIBUTE09_ID ATTRIBUTE09_ID,
       W.ATTRIBUTE10_ID ATTRIBUTE10_ID
  from BUDGET_MONTHLY_DATA W, BUDGET_MONTHLY_DATA_ESCALATION WE
 where W.BUDGET_MONTHLY_ID = WE.BUDGET_MONTHLY_ID(+)
   and W.YEAR = WE.YEAR(+);


create or replace view budg_monthly_data_fy_deesc_sv as
select BUDGET_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       BUDGET_ID,
       BUDGET_VERSION_ID,
       year,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       AMOUNT_PERIOD1,
       AMOUNT_PERIOD2,
       AMOUNT_PERIOD3,
       AMOUNT_PERIOD4,
       AMOUNT_PERIOD5,
       AMOUNT_PERIOD6,
       AMOUNT_PERIOD7,
       AMOUNT_PERIOD8,
       AMOUNT_PERIOD9,
       AMOUNT_PERIOD10,
       AMOUNT_PERIOD11,
       AMOUNT_PERIOD12,
       AMOUNT_PERIOD1 + AMOUNT_PERIOD2 + AMOUNT_PERIOD3 + AMOUNT_PERIOD4 + AMOUNT_PERIOD5 +
       AMOUNT_PERIOD6 + AMOUNT_PERIOD7 + AMOUNT_PERIOD8 + AMOUNT_PERIOD9 + AMOUNT_PERIOD10 +
       AMOUNT_PERIOD11 + AMOUNT_PERIOD12 TOTAL,
       BUDGET_PLANT_CLASS_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WORK_ORDER_ID,
       JOB_TASK_ID,
       ATTRIBUTE01_ID,
       ATTRIBUTE02_ID,
       ATTRIBUTE03_ID,
       ATTRIBUTE04_ID,
       ATTRIBUTE05_ID,
       ATTRIBUTE06_ID,
       ATTRIBUTE07_ID,
       ATTRIBUTE08_ID,
       ATTRIBUTE09_ID,
       ATTRIBUTE10_ID
  from (select W.BUDGET_MONTHLY_ID BUDGET_MONTHLY_ID,
               max(W.TIME_STAMP) TIME_STAMP,
               max(W.USER_ID) USER_ID,
               W.BUDGET_ID BUDGET_ID,
               W.BUDGET_VERSION_ID BUDGET_VERSION_ID,
               DECODE(SIGN(P.FISCAL_PERIOD - P.MONTH_NUM), 1, W.YEAR, W.YEAR + 1) year,
               max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
               max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
               max(W.DEPARTMENT_ID) DEPARTMENT_ID,
               sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period1,
               sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period2,
               sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period3,
               sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period4,
               sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period5,
               sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period6,
               sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period7,
               sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period8,
               sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period9,
               sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period10,
               sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period11,
               sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period12,
               max(W.BUDGET_PLANT_CLASS_ID) BUDGET_PLANT_CLASS_ID,
               max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
               0 FUTURE_DOLLARS,
               0 HIST_ACTUALS,
               max(W.WORK_ORDER_ID) WORK_ORDER_ID,
               max(W.JOB_TASK_ID) JOB_TASK_ID,
               max(W.ATTRIBUTE01_ID) ATTRIBUTE01_ID,
               max(W.ATTRIBUTE02_ID) ATTRIBUTE02_ID,
               max(W.ATTRIBUTE03_ID) ATTRIBUTE03_ID,
               max(W.ATTRIBUTE04_ID) ATTRIBUTE04_ID,
               max(W.ATTRIBUTE05_ID) ATTRIBUTE05_ID,
               max(W.ATTRIBUTE06_ID) ATTRIBUTE06_ID,
               max(W.ATTRIBUTE07_ID) ATTRIBUTE07_ID,
               max(W.ATTRIBUTE08_ID) ATTRIBUTE08_ID,
               max(W.ATTRIBUTE09_ID) ATTRIBUTE09_ID,
               max(W.ATTRIBUTE10_ID) ATTRIBUTE10_ID
          from BUDGET_MONTHLY_DATA W, PP_TABLE_MONTHS P, BUDGET_MONTHLY_DATA_ESCALATION WE
         where W.BUDGET_MONTHLY_ID = WE.BUDGET_MONTHLY_ID(+)
           and W.YEAR = WE.YEAR(+)
         group by W.BUDGET_MONTHLY_ID,
                  W.BUDGET_ID,
                  W.BUDGET_VERSION_ID,
                  DECODE(SIGN(P.FISCAL_PERIOD - P.MONTH_NUM), 1, W.YEAR, W.YEAR + 1));


create or replace view budg_monthly_data_fy_sv as
select BUDGET_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       BUDGET_ID,
       BUDGET_VERSION_ID,
       year,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       AMOUNT_PERIOD1,
       AMOUNT_PERIOD2,
       AMOUNT_PERIOD3,
       AMOUNT_PERIOD4,
       AMOUNT_PERIOD5,
       AMOUNT_PERIOD6,
       AMOUNT_PERIOD7,
       AMOUNT_PERIOD8,
       AMOUNT_PERIOD9,
       AMOUNT_PERIOD10,
       AMOUNT_PERIOD11,
       AMOUNT_PERIOD12,
       AMOUNT_PERIOD1 + AMOUNT_PERIOD2 + AMOUNT_PERIOD3 + AMOUNT_PERIOD4 + AMOUNT_PERIOD5 +
       AMOUNT_PERIOD6 + AMOUNT_PERIOD7 + AMOUNT_PERIOD8 + AMOUNT_PERIOD9 + AMOUNT_PERIOD10 +
       AMOUNT_PERIOD11 + AMOUNT_PERIOD12 TOTAL,
       BUDGET_PLANT_CLASS_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WORK_ORDER_ID,
       JOB_TASK_ID,
       ATTRIBUTE01_ID,
       ATTRIBUTE02_ID,
       ATTRIBUTE03_ID,
       ATTRIBUTE04_ID,
       ATTRIBUTE05_ID,
       ATTRIBUTE06_ID,
       ATTRIBUTE07_ID,
       ATTRIBUTE08_ID,
       ATTRIBUTE09_ID,
       ATTRIBUTE10_ID
  from (select W.BUDGET_MONTHLY_ID BUDGET_MONTHLY_ID,
               max(W.TIME_STAMP) TIME_STAMP,
               max(W.USER_ID) USER_ID,
               W.BUDGET_ID BUDGET_ID,
               W.BUDGET_VERSION_ID BUDGET_VERSION_ID,
               DECODE(SIGN(P.FISCAL_PERIOD - P.MONTH_NUM), 1, W.YEAR, W.YEAR + 1) year,
               max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
               max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
               max(W.DEPARTMENT_ID) DEPARTMENT_ID,
               sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period1,
               sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period2,
               sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period3,
               sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period4,
               sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period5,
               sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period6,
               sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period7,
               sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period8,
               sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period9,
               sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period10,
               sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period11,
               sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period12,
               max(W.BUDGET_PLANT_CLASS_ID) BUDGET_PLANT_CLASS_ID,
               max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
               0 FUTURE_DOLLARS,
               0 HIST_ACTUALS,
               max(W.WORK_ORDER_ID) WORK_ORDER_ID,
               max(W.JOB_TASK_ID) JOB_TASK_ID,
               max(W.ATTRIBUTE01_ID) ATTRIBUTE01_ID,
               max(W.ATTRIBUTE02_ID) ATTRIBUTE02_ID,
               max(W.ATTRIBUTE03_ID) ATTRIBUTE03_ID,
               max(W.ATTRIBUTE04_ID) ATTRIBUTE04_ID,
               max(W.ATTRIBUTE05_ID) ATTRIBUTE05_ID,
               max(W.ATTRIBUTE06_ID) ATTRIBUTE06_ID,
               max(W.ATTRIBUTE07_ID) ATTRIBUTE07_ID,
               max(W.ATTRIBUTE08_ID) ATTRIBUTE08_ID,
               max(W.ATTRIBUTE09_ID) ATTRIBUTE09_ID,
               max(W.ATTRIBUTE10_ID) ATTRIBUTE10_ID
          from BUDGET_MONTHLY_DATA W, PP_TABLE_MONTHS P
         group by W.BUDGET_MONTHLY_ID,
                  W.BUDGET_ID,
                  W.BUDGET_VERSION_ID,
                  DECODE(SIGN(P.FISCAL_PERIOD - P.MONTH_NUM), 1, W.YEAR, W.YEAR + 1));


create or replace view budg_monthly_data_sv as
select W.BUDGET_MONTHLY_ID BUDGET_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.BUDGET_ID BUDGET_ID,
       W.BUDGET_VERSION_ID BUDGET_VERSION_ID,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) TOTAL,
       W.BUDGET_PLANT_CLASS_ID BUDGET_PLANT_CLASS_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID,
       W.ATTRIBUTE01_ID,
       W.ATTRIBUTE02_ID,
       W.ATTRIBUTE03_ID,
       W.ATTRIBUTE04_ID,
       W.ATTRIBUTE05_ID,
       W.ATTRIBUTE06_ID,
       W.ATTRIBUTE07_ID,
       W.ATTRIBUTE08_ID,
       W.ATTRIBUTE09_ID,
       W.ATTRIBUTE10_ID
  from BUDGET_MONTHLY_DATA W;


alter table cwip_charge add attribute01_id varchar2(254);
alter table cwip_charge add attribute02_id varchar2(254);
alter table cwip_charge add attribute03_id varchar2(254);
alter table cwip_charge add attribute04_id varchar2(254);
alter table cwip_charge add attribute05_id varchar2(254);
alter table cwip_charge add attribute06_id varchar2(254);
alter table cwip_charge add attribute07_id varchar2(254);
alter table cwip_charge add attribute08_id varchar2(254);
alter table cwip_charge add attribute09_id varchar2(254);
alter table cwip_charge add attribute10_id varchar2(254);

comment on column cwip_charge.attribute01_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute02_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute03_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute04_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute05_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute06_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute07_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute08_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute09_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';
comment on column cwip_charge.attribute10_id is 'Identifier/Description of an additional, optional charge attribute. Used by Budget Update with Actuals process.';

alter table commitments add attribute01_id varchar2(254);
alter table commitments add attribute02_id varchar2(254);
alter table commitments add attribute03_id varchar2(254);
alter table commitments add attribute04_id varchar2(254);
alter table commitments add attribute05_id varchar2(254);
alter table commitments add attribute06_id varchar2(254);
alter table commitments add attribute07_id varchar2(254);
alter table commitments add attribute08_id varchar2(254);
alter table commitments add attribute09_id varchar2(254);
alter table commitments add attribute10_id varchar2(254);

comment on column commitments.attribute01_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute02_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute03_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute04_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute05_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute06_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute07_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute08_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute09_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';
comment on column commitments.attribute10_id is 'Identifier/Description of an additional, optional commitment attribute. Used by Budget for Forecasting and Reporting.';

create or replace view commitments_bi_fy_sv as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       CV.AMOUNT,
       CV.HOURS,
       CV.QUANTITY,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       C.COMMITMENT_ID,
       C.COMMITMENT_TYPE_ID,
       C.EST_CHG_TYPE_ID COMM_EST_CHG_TYPE_ID,
       C.DESCRIPTION,
       C.EST_START_DATE,
       C.EST_COMP_DATE,
       C.PO_NUMBER,
       C.EXPENDITURE_TYPE_ID,
       C.JOB_TASK_ID,
       C.NOTES,
       C.ID_NUMBER,
       C.UNITS,
       C.REFERENCE_NUMBER,
       C.VENDOR_INFORMATION,
       C.WORK_ORDER_ID,
       C.BUS_SEGMENT_ID,
       C.DEPARTMENT_ID,
       C.COMPANY_ID,
       C.TIME_STAMP,
       C.USER_ID,
       C.STCK_KEEP_UNIT_ID,
       C.RETIREMENT_UNIT_ID,
       C.CHARGE_MO_YR,
       C.UTILITY_ACCOUNT_ID,
       C.SUB_ACCOUNT_ID,
       C.PAYMENT_DATE,
       C.LOADING_AMOUNT,
       C.CHARGE_AUDIT_ID,
       C.JOURNAL_CODE,
       C.COST_ELEMENT_ID,
       C.EXTERNAL_GL_ACCOUNT,
       C.GL_ACCOUNT_ID,
       C.STATUS,
       C.NON_UNITIZED_STATUS,
       C.EXCLUDE_FROM_OVERHEADS,
       C.CLOSED_MONTH_NUMBER,
       C.ORIGINAL_CURRENCY,
       C.ORIGINAL_AMOUNT,
       C.SERIAL_NUMBER,
       C.ATTRIBUTE01_ID,
       C.ATTRIBUTE02_ID,
       C.ATTRIBUTE03_ID,
       C.ATTRIBUTE04_ID,
       C.ATTRIBUTE05_ID,
       C.ATTRIBUTE06_ID,
       C.ATTRIBUTE07_ID,
       C.ATTRIBUTE08_ID,
       C.ATTRIBUTE09_ID,
       C.ATTRIBUTE10_ID
  from COMMITMENTS C,
       COMMITMENT_NET_VIEW CV,
       WORK_ORDER_CONTROL WOC,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB,
       PP_CALENDAR PP
 where C.COMMITMENT_ID = CV.COMMITMENT_ID
   and C.COMMITMENT_TYPE_ID in (1, 2)
   and C.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and C.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CT.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and C.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and C.MONTH_NUMBER = PP.MONTH_NUMBER
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where C.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 2)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where C.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 2);


create or replace view commitments_bi_sv as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE     EST_CHG_TYPE_ID,
       CV.AMOUNT,
       CV.HOURS,
       CV.QUANTITY,
       C.MONTH_NUMBER,
       C.COMMITMENT_ID,
       C.COMMITMENT_TYPE_ID,
       C.EST_CHG_TYPE_ID        COMM_EST_CHG_TYPE_ID,
       C.DESCRIPTION,
       C.EST_START_DATE,
       C.EST_COMP_DATE,
       C.PO_NUMBER,
       C.EXPENDITURE_TYPE_ID,
       C.JOB_TASK_ID,
       C.NOTES,
       C.ID_NUMBER,
       C.UNITS,
       C.REFERENCE_NUMBER,
       C.VENDOR_INFORMATION,
       C.WORK_ORDER_ID,
       C.BUS_SEGMENT_ID,
       C.DEPARTMENT_ID,
       C.COMPANY_ID,
       C.TIME_STAMP,
       C.USER_ID,
       C.STCK_KEEP_UNIT_ID,
       C.RETIREMENT_UNIT_ID,
       C.CHARGE_MO_YR,
       C.UTILITY_ACCOUNT_ID,
       C.SUB_ACCOUNT_ID,
       C.PAYMENT_DATE,
       C.LOADING_AMOUNT,
       C.CHARGE_AUDIT_ID,
       C.JOURNAL_CODE,
       C.COST_ELEMENT_ID,
       C.EXTERNAL_GL_ACCOUNT,
       C.GL_ACCOUNT_ID,
       C.STATUS,
       C.NON_UNITIZED_STATUS,
       C.EXCLUDE_FROM_OVERHEADS,
       C.CLOSED_MONTH_NUMBER,
       C.ORIGINAL_CURRENCY,
       C.ORIGINAL_AMOUNT,
       C.SERIAL_NUMBER,
       C.ATTRIBUTE01_ID,
       C.ATTRIBUTE02_ID,
       C.ATTRIBUTE03_ID,
       C.ATTRIBUTE04_ID,
       C.ATTRIBUTE05_ID,
       C.ATTRIBUTE06_ID,
       C.ATTRIBUTE07_ID,
       C.ATTRIBUTE08_ID,
       C.ATTRIBUTE09_ID,
       C.ATTRIBUTE10_ID
  from COMMITMENTS C,
       COMMITMENT_NET_VIEW CV,
       WORK_ORDER_CONTROL WOC,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB
 where C.COMMITMENT_ID = CV.COMMITMENT_ID
   and C.COMMITMENT_TYPE_ID in (1, 2)
   and C.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and C.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CT.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and C.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where C.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 2)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where C.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 2);


create or replace view commitments_fp_fy_sv as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       CV.AMOUNT,
       CV.HOURS,
       CV.QUANTITY,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       C.COMMITMENT_ID,
       C.COMMITMENT_TYPE_ID,
       C.EST_CHG_TYPE_ID COMM_EST_CHG_TYPE_ID,
       C.DESCRIPTION,
       C.EST_START_DATE,
       C.EST_COMP_DATE,
       C.PO_NUMBER,
       C.EXPENDITURE_TYPE_ID,
       C.JOB_TASK_ID,
       C.NOTES,
       C.ID_NUMBER,
       C.UNITS,
       C.REFERENCE_NUMBER,
       C.VENDOR_INFORMATION,
       C.WORK_ORDER_ID,
       C.BUS_SEGMENT_ID,
       C.DEPARTMENT_ID,
       C.COMPANY_ID,
       C.TIME_STAMP,
       C.USER_ID,
       C.STCK_KEEP_UNIT_ID,
       C.RETIREMENT_UNIT_ID,
       C.CHARGE_MO_YR,
       C.UTILITY_ACCOUNT_ID,
       C.SUB_ACCOUNT_ID,
       C.PAYMENT_DATE,
       C.LOADING_AMOUNT,
       C.CHARGE_AUDIT_ID,
       C.JOURNAL_CODE,
       C.COST_ELEMENT_ID,
       C.EXTERNAL_GL_ACCOUNT,
       C.GL_ACCOUNT_ID,
       C.STATUS,
       C.NON_UNITIZED_STATUS,
       C.EXCLUDE_FROM_OVERHEADS,
       C.CLOSED_MONTH_NUMBER,
       C.ORIGINAL_CURRENCY,
       C.ORIGINAL_AMOUNT,
       C.SERIAL_NUMBER,
       C.ATTRIBUTE01_ID,
       C.ATTRIBUTE02_ID,
       C.ATTRIBUTE03_ID,
       C.ATTRIBUTE04_ID,
       C.ATTRIBUTE05_ID,
       C.ATTRIBUTE06_ID,
       C.ATTRIBUTE07_ID,
       C.ATTRIBUTE08_ID,
       C.ATTRIBUTE09_ID,
       C.ATTRIBUTE10_ID
  from COMMITMENTS C,
       COMMITMENT_NET_VIEW CV,
       WORK_ORDER_CONTROL WOC,
       WORK_ORDER_ACCOUNT FPA,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       WO_EST_HIERARCHY_MAP WEH_MAP,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB,
       PP_CALENDAR PP
 where C.COMMITMENT_ID = CV.COMMITMENT_ID
   and C.COMMITMENT_TYPE_ID in (1, 2)
   and C.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.FUNDING_WO_ID = FPA.WORK_ORDER_ID
   and FPA.WO_EST_HIERARCHY_ID = WEH_MAP.WO_EST_HIERARCHY_ID
   and C.COST_ELEMENT_ID = WEH_MAP.COST_ELEMENT_ID
   and WEH_MAP.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and C.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and C.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and C.MONTH_NUMBER = PP.MONTH_NUMBER
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where C.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 1)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where C.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 1);


create or replace view commitments_fp_sv as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE     EST_CHG_TYPE_ID,
       CV.AMOUNT,
       CV.HOURS,
       CV.QUANTITY,
       C.MONTH_NUMBER,
       C.COMMITMENT_ID,
       C.COMMITMENT_TYPE_ID,
       C.EST_CHG_TYPE_ID        COMM_EST_CHG_TYPE_ID,
       C.DESCRIPTION,
       C.EST_START_DATE,
       C.EST_COMP_DATE,
       C.PO_NUMBER,
       C.EXPENDITURE_TYPE_ID,
       C.JOB_TASK_ID,
       C.NOTES,
       C.ID_NUMBER,
       C.UNITS,
       C.REFERENCE_NUMBER,
       C.VENDOR_INFORMATION,
       C.WORK_ORDER_ID,
       C.BUS_SEGMENT_ID,
       C.DEPARTMENT_ID,
       C.COMPANY_ID,
       C.TIME_STAMP,
       C.USER_ID,
       C.STCK_KEEP_UNIT_ID,
       C.RETIREMENT_UNIT_ID,
       C.CHARGE_MO_YR,
       C.UTILITY_ACCOUNT_ID,
       C.SUB_ACCOUNT_ID,
       C.PAYMENT_DATE,
       C.LOADING_AMOUNT,
       C.CHARGE_AUDIT_ID,
       C.JOURNAL_CODE,
       C.COST_ELEMENT_ID,
       C.EXTERNAL_GL_ACCOUNT,
       C.GL_ACCOUNT_ID,
       C.STATUS,
       C.NON_UNITIZED_STATUS,
       C.EXCLUDE_FROM_OVERHEADS,
       C.CLOSED_MONTH_NUMBER,
       C.ORIGINAL_CURRENCY,
       C.ORIGINAL_AMOUNT,
       C.SERIAL_NUMBER,
       C.ATTRIBUTE01_ID,
       C.ATTRIBUTE02_ID,
       C.ATTRIBUTE03_ID,
       C.ATTRIBUTE04_ID,
       C.ATTRIBUTE05_ID,
       C.ATTRIBUTE06_ID,
       C.ATTRIBUTE07_ID,
       C.ATTRIBUTE08_ID,
       C.ATTRIBUTE09_ID,
       C.ATTRIBUTE10_ID
  from COMMITMENTS C,
       COMMITMENT_NET_VIEW CV,
       WORK_ORDER_CONTROL WOC,
       WORK_ORDER_ACCOUNT FPA,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       WO_EST_HIERARCHY_MAP WEH_MAP,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB
 where C.COMMITMENT_ID = CV.COMMITMENT_ID
   and C.COMMITMENT_TYPE_ID in (1, 2)
   and C.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.FUNDING_WO_ID = FPA.WORK_ORDER_ID
   and FPA.WO_EST_HIERARCHY_ID = WEH_MAP.WO_EST_HIERARCHY_ID
   and C.COST_ELEMENT_ID = WEH_MAP.COST_ELEMENT_ID
   and WEH_MAP.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and C.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and C.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where C.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 1)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where C.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 1);


create or replace view cwip_bi_fy_sv as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       CC.CHARGE_ID,
       CC.CHARGE_TYPE_ID,
       CC.JOB_TASK_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.WORK_ORDER_ID,
       CC.TIME_STAMP,
       CC.USER_ID,
       CC.STCK_KEEP_UNIT_ID,
       CC.RETIREMENT_UNIT_ID,
       CC.CHARGE_MO_YR,
       CC.DESCRIPTION,
       CC.QUANTITY,
       CC.UTILITY_ACCOUNT_ID,
       CC.BUS_SEGMENT_ID,
       CC.AMOUNT,
       CC.SUB_ACCOUNT_ID,
       CC.HOURS,
       CC.PAYMENT_DATE,
       CC.NOTES,
       CC.ID_NUMBER,
       CC.UNITS,
       CC.LOADING_AMOUNT,
       CC.REFERENCE_NUMBER,
       CC.VENDOR_INFORMATION,
       CC.DEPARTMENT_ID,
       CC.CHARGE_AUDIT_ID,
       CC.JOURNAL_CODE,
       CC.COST_ELEMENT_ID,
       CC.EXTERNAL_GL_ACCOUNT,
       CC.GL_ACCOUNT_ID,
       CC.STATUS,
       CC.COMPANY_ID,
       CC.NON_UNITIZED_STATUS,
       CC.EXCLUDE_FROM_OVERHEADS,
       CC.CLOSED_MONTH_NUMBER,
       CC.ORIGINAL_CURRENCY,
       CC.ORIGINAL_AMOUNT,
       CC.ASSET_LOCATION_ID,
       CC.SERIAL_NUMBER,
       CC.PO_NUMBER,
       CC.UNIT_CLOSED_MONTH_NUMBER,
       CC.TAX_ORIG_MONTH_NUMBER,
       CC.ATTRIBUTE01_ID,
       CC.ATTRIBUTE02_ID,
       CC.ATTRIBUTE03_ID,
       CC.ATTRIBUTE04_ID,
       CC.ATTRIBUTE05_ID,
       CC.ATTRIBUTE06_ID,
       CC.ATTRIBUTE07_ID,
       CC.ATTRIBUTE08_ID,
       CC.ATTRIBUTE09_ID,
       CC.ATTRIBUTE10_ID
  from CWIP_CHARGE CC,
       WORK_ORDER_CONTROL WOC,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB,
       PP_CALENDAR PP
 where CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and CC.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CT.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and CC.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and CC.MONTH_NUMBER = PP.MONTH_NUMBER
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where CC.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 2)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where CC.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 2);


create or replace view cwip_bi_sv as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       CC.MONTH_NUMBER,
       CC.CHARGE_ID,
       CC.CHARGE_TYPE_ID,
       CC.JOB_TASK_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.WORK_ORDER_ID,
       CC.TIME_STAMP,
       CC.USER_ID,
       CC.STCK_KEEP_UNIT_ID,
       CC.RETIREMENT_UNIT_ID,
       CC.CHARGE_MO_YR,
       CC.DESCRIPTION,
       CC.QUANTITY,
       CC.UTILITY_ACCOUNT_ID,
       CC.BUS_SEGMENT_ID,
       CC.AMOUNT,
       CC.SUB_ACCOUNT_ID,
       CC.HOURS,
       CC.PAYMENT_DATE,
       CC.NOTES,
       CC.ID_NUMBER,
       CC.UNITS,
       CC.LOADING_AMOUNT,
       CC.REFERENCE_NUMBER,
       CC.VENDOR_INFORMATION,
       CC.DEPARTMENT_ID,
       CC.CHARGE_AUDIT_ID,
       CC.JOURNAL_CODE,
       CC.COST_ELEMENT_ID,
       CC.EXTERNAL_GL_ACCOUNT,
       CC.GL_ACCOUNT_ID,
       CC.STATUS,
       CC.COMPANY_ID,
       CC.NON_UNITIZED_STATUS,
       CC.EXCLUDE_FROM_OVERHEADS,
       CC.CLOSED_MONTH_NUMBER,
       CC.ORIGINAL_CURRENCY,
       CC.ORIGINAL_AMOUNT,
       CC.ASSET_LOCATION_ID,
       CC.SERIAL_NUMBER,
       CC.PO_NUMBER,
       CC.UNIT_CLOSED_MONTH_NUMBER,
       CC.TAX_ORIG_MONTH_NUMBER,
       CC.ATTRIBUTE01_ID,
       CC.ATTRIBUTE02_ID,
       CC.ATTRIBUTE03_ID,
       CC.ATTRIBUTE04_ID,
       CC.ATTRIBUTE05_ID,
       CC.ATTRIBUTE06_ID,
       CC.ATTRIBUTE07_ID,
       CC.ATTRIBUTE08_ID,
       CC.ATTRIBUTE09_ID,
       CC.ATTRIBUTE10_ID
  from CWIP_CHARGE CC,
       WORK_ORDER_CONTROL WOC,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB
 where CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and CC.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CT.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and CC.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where CC.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 2)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where CC.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 2);


create or replace view cwip_charge_sob_view as
select CWIP_CHARGE.CHARGE_ID,
       CWIP_CHARGE.CHARGE_TYPE_ID,
       CWIP_CHARGE.JOB_TASK_ID,
       CWIP_CHARGE.EXPENDITURE_TYPE_ID,
       CWIP_CHARGE.WORK_ORDER_ID,
       CWIP_CHARGE.TIME_STAMP,
       CWIP_CHARGE.USER_ID,
       CWIP_CHARGE.STCK_KEEP_UNIT_ID,
       CWIP_CHARGE.RETIREMENT_UNIT_ID,
       CWIP_CHARGE.CHARGE_MO_YR,
       CWIP_CHARGE.DESCRIPTION,
       CWIP_CHARGE.QUANTITY,
       CWIP_CHARGE.UTILITY_ACCOUNT_ID,
       CWIP_CHARGE.BUS_SEGMENT_ID,
       CWIP_CHARGE.AMOUNT,
       CWIP_CHARGE.SUB_ACCOUNT_ID,
       CWIP_CHARGE.HOURS,
       CWIP_CHARGE.PAYMENT_DATE,
       CWIP_CHARGE.NOTES,
       CWIP_CHARGE.ID_NUMBER,
       CWIP_CHARGE.UNITS,
       CWIP_CHARGE.LOADING_AMOUNT,
       CWIP_CHARGE.REFERENCE_NUMBER,
       CWIP_CHARGE.VENDOR_INFORMATION,
       CWIP_CHARGE.DEPARTMENT_ID,
       CWIP_CHARGE.CHARGE_AUDIT_ID,
       CWIP_CHARGE.JOURNAL_CODE,
       CWIP_CHARGE.COST_ELEMENT_ID,
       CWIP_CHARGE.EXTERNAL_GL_ACCOUNT,
       CWIP_CHARGE.GL_ACCOUNT_ID,
       CWIP_CHARGE.STATUS,
       CWIP_CHARGE.COMPANY_ID,
       CWIP_CHARGE.MONTH_NUMBER,
       CWIP_CHARGE.NON_UNITIZED_STATUS,
       CWIP_CHARGE.EXCLUDE_FROM_OVERHEADS,
       CWIP_CHARGE.CLOSED_MONTH_NUMBER,
       CWIP_CHARGE.ORIGINAL_CURRENCY,
       CWIP_CHARGE.ORIGINAL_AMOUNT,
       CWIP_CHARGE.ASSET_LOCATION_ID,
       CWIP_CHARGE.SERIAL_NUMBER,
       CWIP_CHARGE.UNIT_CLOSED_MONTH_NUMBER,
       CWIP_CHARGE.PO_NUMBER,
       CWIP_CHARGE.TAX_ORIG_MONTH_NUMBER,
       CWIP_CHARGE.ATTRIBUTE01_ID,
       CWIP_CHARGE.ATTRIBUTE02_ID,
       CWIP_CHARGE.ATTRIBUTE03_ID,
       CWIP_CHARGE.ATTRIBUTE04_ID,
       CWIP_CHARGE.ATTRIBUTE05_ID,
       CWIP_CHARGE.ATTRIBUTE06_ID,
       CWIP_CHARGE.ATTRIBUTE07_ID,
       CWIP_CHARGE.ATTRIBUTE08_ID,
       CWIP_CHARGE.ATTRIBUTE09_ID,
       CWIP_CHARGE.ATTRIBUTE10_ID
  from CWIP_CHARGE, TEMP_SET_OF_BOOKS
 where CWIP_CHARGE.CHARGE_TYPE_ID = TEMP_SET_OF_BOOKS.CHARGE_TYPE_ID;


create or replace view cwip_fp_fy_sv as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       CC.CHARGE_ID,
       CC.CHARGE_TYPE_ID,
       CC.JOB_TASK_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.WORK_ORDER_ID,
       CC.TIME_STAMP,
       CC.USER_ID,
       CC.STCK_KEEP_UNIT_ID,
       CC.RETIREMENT_UNIT_ID,
       CC.CHARGE_MO_YR,
       CC.DESCRIPTION,
       CC.QUANTITY,
       CC.UTILITY_ACCOUNT_ID,
       CC.BUS_SEGMENT_ID,
       CC.AMOUNT,
       CC.SUB_ACCOUNT_ID,
       CC.HOURS,
       CC.PAYMENT_DATE,
       CC.NOTES,
       CC.ID_NUMBER,
       CC.UNITS,
       CC.LOADING_AMOUNT,
       CC.REFERENCE_NUMBER,
       CC.VENDOR_INFORMATION,
       CC.DEPARTMENT_ID,
       CC.CHARGE_AUDIT_ID,
       CC.JOURNAL_CODE,
       CC.COST_ELEMENT_ID,
       CC.EXTERNAL_GL_ACCOUNT,
       CC.GL_ACCOUNT_ID,
       CC.STATUS,
       CC.COMPANY_ID,
       CC.NON_UNITIZED_STATUS,
       CC.EXCLUDE_FROM_OVERHEADS,
       CC.CLOSED_MONTH_NUMBER,
       CC.ORIGINAL_CURRENCY,
       CC.ORIGINAL_AMOUNT,
       CC.ASSET_LOCATION_ID,
       CC.SERIAL_NUMBER,
       CC.PO_NUMBER,
       CC.UNIT_CLOSED_MONTH_NUMBER,
       CC.TAX_ORIG_MONTH_NUMBER,
       CC.ATTRIBUTE01_ID,
       CC.ATTRIBUTE02_ID,
       CC.ATTRIBUTE03_ID,
       CC.ATTRIBUTE04_ID,
       CC.ATTRIBUTE05_ID,
       CC.ATTRIBUTE06_ID,
       CC.ATTRIBUTE07_ID,
       CC.ATTRIBUTE08_ID,
       CC.ATTRIBUTE09_ID,
       CC.ATTRIBUTE10_ID
  from CWIP_CHARGE CC,
       WORK_ORDER_CONTROL WOC,
       WORK_ORDER_ACCOUNT FPA,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       WO_EST_HIERARCHY_MAP WEH_MAP,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB,
       PP_CALENDAR PP
 where CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.FUNDING_WO_ID = FPA.WORK_ORDER_ID
   and FPA.WO_EST_HIERARCHY_ID = WEH_MAP.WO_EST_HIERARCHY_ID
   and CC.COST_ELEMENT_ID = WEH_MAP.COST_ELEMENT_ID
   and WEH_MAP.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and CC.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CC.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and CC.MONTH_NUMBER = PP.MONTH_NUMBER
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where CC.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 1)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where CC.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 1);


create or replace view cwip_fp_sv as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       CC.MONTH_NUMBER,
       CC.CHARGE_ID,
       CC.CHARGE_TYPE_ID,
       CC.JOB_TASK_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.WORK_ORDER_ID,
       CC.TIME_STAMP,
       CC.USER_ID,
       CC.STCK_KEEP_UNIT_ID,
       CC.RETIREMENT_UNIT_ID,
       CC.CHARGE_MO_YR,
       CC.DESCRIPTION,
       CC.QUANTITY,
       CC.UTILITY_ACCOUNT_ID,
       CC.BUS_SEGMENT_ID,
       CC.AMOUNT,
       CC.SUB_ACCOUNT_ID,
       CC.HOURS,
       CC.PAYMENT_DATE,
       CC.NOTES,
       CC.ID_NUMBER,
       CC.UNITS,
       CC.LOADING_AMOUNT,
       CC.REFERENCE_NUMBER,
       CC.VENDOR_INFORMATION,
       CC.DEPARTMENT_ID,
       CC.CHARGE_AUDIT_ID,
       CC.JOURNAL_CODE,
       CC.COST_ELEMENT_ID,
       CC.EXTERNAL_GL_ACCOUNT,
       CC.GL_ACCOUNT_ID,
       CC.STATUS,
       CC.COMPANY_ID,
       CC.NON_UNITIZED_STATUS,
       CC.EXCLUDE_FROM_OVERHEADS,
       CC.CLOSED_MONTH_NUMBER,
       CC.ORIGINAL_CURRENCY,
       CC.ORIGINAL_AMOUNT,
       CC.ASSET_LOCATION_ID,
       CC.SERIAL_NUMBER,
       CC.PO_NUMBER,
       CC.UNIT_CLOSED_MONTH_NUMBER,
       CC.TAX_ORIG_MONTH_NUMBER,
       CC.ATTRIBUTE01_ID,
       CC.ATTRIBUTE02_ID,
       CC.ATTRIBUTE03_ID,
       CC.ATTRIBUTE04_ID,
       CC.ATTRIBUTE05_ID,
       CC.ATTRIBUTE06_ID,
       CC.ATTRIBUTE07_ID,
       CC.ATTRIBUTE08_ID,
       CC.ATTRIBUTE09_ID,
       CC.ATTRIBUTE10_ID
  from CWIP_CHARGE CC,
       WORK_ORDER_CONTROL WOC,
       WORK_ORDER_ACCOUNT FPA,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       WO_EST_HIERARCHY_MAP WEH_MAP,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB
 where CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.FUNDING_WO_ID = FPA.WORK_ORDER_ID
   and FPA.WO_EST_HIERARCHY_ID = WEH_MAP.WO_EST_HIERARCHY_ID
   and CC.COST_ELEMENT_ID = WEH_MAP.COST_ELEMENT_ID
   and WEH_MAP.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and CC.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CC.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where CC.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 1)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where CC.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 1);


alter table CR_COMMITMENTS_SUM add attribute01_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute02_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute03_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute04_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute05_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute06_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute07_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute08_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute09_id varchar2(254);
alter table CR_COMMITMENTS_SUM add attribute10_id varchar2(254);

comment on column cr_commitments_sum.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_sum.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table CR_COMMITMENTS_translate add attribute01_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute02_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute03_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute04_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute05_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute06_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute07_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute08_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute09_id varchar2(254);
alter table CR_COMMITMENTS_translate add attribute10_id varchar2(254);

comment on column cr_commitments_translate.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_commitments_translate.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table CR_CWIP_CHARGE_SUM add attribute01_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute02_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute03_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute04_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute05_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute06_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute07_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute08_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute09_id varchar2(254);
alter table CR_CWIP_CHARGE_SUM add attribute10_id varchar2(254);

comment on column cr_cwip_charge_sum.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_sum.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';

alter table CR_CWIP_CHARGE_translate add attribute01_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute02_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute03_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute04_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute05_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute06_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute07_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute08_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute09_id varchar2(254);
alter table CR_CWIP_CHARGE_translate add attribute10_id varchar2(254);

comment on column cr_cwip_charge_translate.attribute01_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute02_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute03_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute04_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute05_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute06_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute07_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute08_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute09_id is 'Identifier/Description of an additional, optional estimate attribute.';
comment on column cr_cwip_charge_translate.attribute10_id is 'Identifier/Description of an additional, optional estimate attribute.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2179, 0, 2015, 1, 0, 0, 42193, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042193_pcm_new_est_attributes_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;