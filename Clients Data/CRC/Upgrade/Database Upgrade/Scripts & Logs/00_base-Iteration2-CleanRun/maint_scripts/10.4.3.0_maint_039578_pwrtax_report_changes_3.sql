/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_039578_pwrtax_report_changes_3.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/17/2014 Anand Rajashekar    changes to pp_report_filter_id 81, 82
||========================================================================================
*/

-- changes to pp_report_filter_id 81, 82

update PP_DYNAMIC_FILTER_MAPPING
   set FILTER_ID = 157
 where PP_REPORT_FILTER_ID = 81
   and FILTER_ID = 101;

update PP_DYNAMIC_FILTER_MAPPING
   set FILTER_ID = 157
 where PP_REPORT_FILTER_ID = 82
   and FILTER_ID = 101;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1430, 0, 10, 4, 3, 0, 39578, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039578_pwrtax_report_changes_3.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;