/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031428_prov.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/26/2013 Nathan Hollis  Point Release
||============================================================================
*/

--------------------------------------------------------------------------------
--add consolidating flag to 51004 report (it is not present in many databases)--
--------------------------------------------------------------------------------
update PP_REPORTS set SPECIAL_NOTE = 'CONSOLIDATING_FED_CURR' where REPORT_ID = 51004;

--------------------------------------------------------------------------------
--fix subtotaling by forcing application to manually calculate subtotals--------
--------------------------------------------------------------------------------
update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = null
 where REP_CONS_TYPE_ID in (2,3)
   and ROW_ID = 13;

--------------------------------------------------------------------------------
--fix header sources for company report-----------------------------------------
--------------------------------------------------------------------------------
UPDATE tax_accrual_rep_cons_rows SET row_value = 'book_y_total' WHERE row_id = 7 and rep_cons_type_id = 7;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'tax_items_total' WHERE row_id = 8 and rep_cons_type_id = 7;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'pretax_book_y_total' WHERE row_id = 9 and rep_cons_type_id = 7;

--------------------------------------------------------------------------------
--fix header sources for monthtype report---------------------------------------
--------------------------------------------------------------------------------
UPDATE tax_accrual_rep_cons_rows SET row_value = 'book_y_total' WHERE row_id = 7 and rep_cons_type_id = 3;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'tax_items_total' WHERE row_id = 8 and rep_cons_type_id = 3;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'pretax_book_y_total' WHERE row_id = 9 and rep_cons_type_id = 3;

--------------------------------------------------------------------------------
--fix header sources for month  report------------------------------------------
--------------------------------------------------------------------------------
UPDATE tax_accrual_rep_cons_rows SET row_value = 'book_y_total' WHERE row_id = 7 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'tax_items_total' WHERE row_id = 8 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'pretax_book_y_total' WHERE row_id = 9 and rep_cons_type_id = 2;

--------------------------------------------------------------------------------
--fix header sources for oper  report-------------------------------------------
--------------------------------------------------------------------------------
UPDATE tax_accrual_rep_cons_rows SET row_value = 'book_y_total' WHERE row_id = 7 and rep_cons_type_id = 1;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'tax_items_total' WHERE row_id = 8 and rep_cons_type_id = 1;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'pretax_book_y_total' WHERE row_id = 9 and rep_cons_type_id = 1;

--------------------------------------------------------------------------------
--fix footers sources for month report------------------------------------------
--------------------------------------------------------------------------------
UPDATE tax_accrual_rep_cons_rows SET row_value = 'pre_apportion_taxable_y_tot' WHERE row_id = 15 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'state_tax_deductible_tot' WHERE row_id = 16 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'total_taxable_y' WHERE row_id = 17 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'calc_stat_rate_tot' WHERE row_id = 18 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'calc_current_tax_tot' WHERE row_id = 19 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'curr_month_trueup_tot' WHERE row_id = 20 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'total_tax_b4_credits' WHERE row_id = 21 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'credits_tot' WHERE row_id = 22 and rep_cons_type_id = 2;
UPDATE tax_accrual_rep_cons_rows SET row_value = 'current_tax_tot' WHERE row_id = 23 and rep_cons_type_id = 2;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (536, 0, 10, 4, 1, 0, 31428, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031428_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
