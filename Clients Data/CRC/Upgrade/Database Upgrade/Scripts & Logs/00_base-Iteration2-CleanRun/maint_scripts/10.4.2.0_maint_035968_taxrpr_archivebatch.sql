/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035968_taxrpr_archivebatch.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/21/2014 Alex P.
||============================================================================
*/

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, IS_BASE_OPTION)
values
   ('Repairs - Stranded Batch Archive',
    '"No" leaves all stranded batches in the table for audit purposes. "Yes" deletes stranded batches to improve performance.',
    0, 'Yes', 1);

update PPBASE_SYSTEM_OPTIONS
   set OPTION_VALUE =
        (select DECODE(LOWER(trim(CONTROL_VALUE)), 'yes', 'No', 'Yes')
           from PP_SYSTEM_CONTROL_COMPANY
          where CONTROL_NAME = 'REPAIRS-Fail Zero Qty Minors')
 where SYSTEM_OPTION_ID = 'Repairs - Stranded Batch Archive';

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - Stranded Batch Archive', 'Yes');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - Stranded Batch Archive', 'No');

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Repairs - Stranded Batch Archive', 'REPAIRS');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (983, 0, 10, 4, 2, 0, 35968, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035968_taxrpr_archivebatch.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;