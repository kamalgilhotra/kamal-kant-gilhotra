/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047281_06_lease_add_asset_schedule_depr_view_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/24/2017 Jared Watkins    add view to retrieve Lease asset schedule fields and depr fields together
||============================================================================
*/

CREATE OR REPLACE VIEW v_multicurrency_ls_asset_inner AS 
WITH depr AS (
  SELECT  /*+ materialize */ ilr_id, revision, set_of_books_id, 
         month, sum(depr_expense) as depr_expense,
         sum(begin_reserve) as begin_reserve,
         sum(end_reserve) as end_reserve,
         sum(depr_exp_alloc_adjust) as depr_exp_alloc_adjust
  FROM ls_asset la, ls_depr_forecast ldf
  WHERE la.ls_asset_id = ldf.ls_asset_id
)
SELECT /*+ NO_MERGE */ 
  la.ilr_id,
  las.ls_asset_id,
  las.revision,
  las.set_of_books_id,
  las.MONTH,
  la.contract_currency_id,
  las.beg_capital_cost,
  las.end_capital_cost,
  las.beg_obligation,
  las.end_obligation,
  las.beg_lt_obligation,
  las.end_lt_obligation,
  las.interest_accrual,
  las.principal_accrual,
  las.interest_paid,
  las.principal_paid,
  las.executory_accrual1,
  las.executory_accrual2,
  las.executory_accrual3,
  las.executory_accrual4,
  las.executory_accrual5,
  las.executory_accrual6,
  las.executory_accrual7,
  las.executory_accrual8,
  las.executory_accrual9,
  las.executory_accrual10,
  las.executory_paid1,
  las.executory_paid2,
  las.executory_paid3,
  las.executory_paid4,
  las.executory_paid5,
  las.executory_paid6,
  las.executory_paid7,
  las.executory_paid8,
  las.executory_paid9,
  las.executory_paid10,
  las.contingent_accrual1,
  las.contingent_accrual2,
  las.contingent_accrual3,
  las.contingent_accrual4,
  las.contingent_accrual5,
  las.contingent_accrual6,
  las.contingent_accrual7,
  las.contingent_accrual8,
  las.contingent_accrual9,
  las.contingent_accrual10,
  las.contingent_paid1,
  las.contingent_paid2,
  las.contingent_paid3,
  las.contingent_paid4,
  las.contingent_paid5,
  las.contingent_paid6,
  las.contingent_paid7,
  las.contingent_paid8,
  las.contingent_paid9,
  las.contingent_paid10,
  las.current_lease_cost,
  ldf.depr_expense, 
  ldf.begin_reserve, 
  ldf.end_reserve,
  ldf.depr_exp_alloc_adjust, 
  la.company_id, 
  opt.in_service_exchange_rate
FROM ls_asset_schedule las
INNER JOIN ls_asset la
  ON las.ls_asset_id = la.ls_asset_id
INNER JOIN ls_ilr_options opt
  ON la.ilr_id = opt.ilr_id
  AND las.revision = opt.revision
LEFT OUTER JOIN ls_depr_forecast ldf
  ON las.ls_asset_id = ldf.ls_asset_id
  AND las.revision = ldf.revision
  AND las.set_of_books_id = ldf.set_of_books_id
  AND las.month = ldf.month
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3449, 0, 2017, 1, 0, 0, 47281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047281_06_lease_add_asset_schedule_depr_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
