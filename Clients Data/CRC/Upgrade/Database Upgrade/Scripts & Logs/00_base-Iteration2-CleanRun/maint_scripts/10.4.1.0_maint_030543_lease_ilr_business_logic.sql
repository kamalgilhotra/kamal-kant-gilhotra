/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030543_lease_ilr_business_logic.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/09/2013 Kyle Peterson  Point release
||============================================================================
*/

alter table LS_ILR drop (NPV_METHOD_ID, OPERATING_EXPENSE_METHOD_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (445, 0, 10, 4, 1, 0, 30543, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030543_lease_ilr_business_logic.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
