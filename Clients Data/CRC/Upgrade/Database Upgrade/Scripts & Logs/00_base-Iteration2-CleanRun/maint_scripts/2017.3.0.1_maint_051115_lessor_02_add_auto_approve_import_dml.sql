/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051115_lessor_02_add_auto_approve_import_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.3.0.1  05/09/2018 Crystal Yura    Add Auto Approve columns for Lessor MLA Import Template
||============================================================================
*/

insert into pp_import_column
(import_Type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table, is_on_table, parent_table_pk_column)
select import_Type_id, 'auto_approve', 'Auto Approve MLA?', 'auto_approve_xlate',0,1,'number(22,0)','yes_no',1,'yes_no_id' 
from pp_import_type a where lower(import_table_name) = 'lsr_import_lease'
and not exists (select 1 from pp_import_column b where a.import_type_id = b.import_type_id and lower(b.column_name) = 'auto_approve');

insert into pp_import_column_lookup
(import_Type_id, column_name, import_lookup_id)
select 
a.import_type_id, 
'auto_approve', 
(select min(import_lookup_id) From pp_import_lookup where lower(lookup_table_name) = 'yes_no' and lower(lookup_column_name) = 'description') 
from pp_import_type a where lower(import_table_name) = 'lsr_import_lease'
and not exists (select 1 from pp_import_column_lookup b where a.import_type_id = b.import_type_id and lower(b.column_name) = 'auto_approve');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(6585, 0, 2017, 3, 0, 1, 51115, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051115_lessor_02_add_auto_approve_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;