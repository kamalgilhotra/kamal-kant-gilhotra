/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010730_sys_ctrl.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/20/2012 Aaron Smith    Point Release
||============================================================================
*/

-- AJS: 14-AUG-2012: Maint 10730

delete from PP_SYSTEM_CONTROL_COMPANY where UPPER(CONTROL_NAME) = 'ASSET TRANSFER WIZARD';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (198, 0, 10, 3, 5, 0, 10730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010730_sys_ctrl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;