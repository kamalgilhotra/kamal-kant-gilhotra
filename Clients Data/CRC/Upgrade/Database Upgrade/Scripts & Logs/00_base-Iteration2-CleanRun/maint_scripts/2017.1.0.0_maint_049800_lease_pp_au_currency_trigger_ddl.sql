/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049800_lease_pp_au_currency_trigger_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/22/2017 A. Smith       Update CURRENCY audit trigger to include ISO Code.
||============================================================================
*/

CREATE OR REPLACE TRIGGER
PP_AU_CURRENCY AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF CURRENCY_DISPLAY_FACTOR, CURRENCY_DISPLAY_SYMBOL, CURRENCY_ID, DESCRIPTION, EXTERNAL_CURRENCY_CODE, ISO_CODE
 ON CURRENCY FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := substr( 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal), 1 , 250 );
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING THEN
   IF :old.CURRENCY_DISPLAY_FACTOR <> :new.CURRENCY_DISPLAY_FACTOR OR
     (:old.CURRENCY_DISPLAY_FACTOR is null AND :new.CURRENCY_DISPLAY_FACTOR is not null) OR
     (:new.CURRENCY_DISPLAY_FACTOR is null AND :old.CURRENCY_DISPLAY_FACTOR is not null) THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_DISPLAY_FACTOR',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :old.CURRENCY_DISPLAY_FACTOR, :new.CURRENCY_DISPLAY_FACTOR,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.CURRENCY_DISPLAY_SYMBOL <> :new.CURRENCY_DISPLAY_SYMBOL OR
     (:old.CURRENCY_DISPLAY_SYMBOL is null AND :new.CURRENCY_DISPLAY_SYMBOL is not null) OR
     (:new.CURRENCY_DISPLAY_SYMBOL is null AND :old.CURRENCY_DISPLAY_SYMBOL is not null) THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_DISPLAY_SYMBOL',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :old.CURRENCY_DISPLAY_SYMBOL, :new.CURRENCY_DISPLAY_SYMBOL,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.CURRENCY_ID <> :new.CURRENCY_ID OR
     (:old.CURRENCY_ID is null AND :new.CURRENCY_ID is not null) OR
     (:new.CURRENCY_ID is null AND :old.CURRENCY_ID is not null) THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_ID',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :old.CURRENCY_ID, :new.CURRENCY_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.DESCRIPTION <> :new.DESCRIPTION OR
     (:old.DESCRIPTION is null AND :new.DESCRIPTION is not null) OR
     (:new.DESCRIPTION is null AND :old.DESCRIPTION is not null) THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'DESCRIPTION',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :old.DESCRIPTION, :new.DESCRIPTION,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.EXTERNAL_CURRENCY_CODE <> :new.EXTERNAL_CURRENCY_CODE OR
     (:old.EXTERNAL_CURRENCY_CODE is null AND :new.EXTERNAL_CURRENCY_CODE is not null) OR
     (:new.EXTERNAL_CURRENCY_CODE is null AND :old.EXTERNAL_CURRENCY_CODE is not null) THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'EXTERNAL_CURRENCY_CODE',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :old.EXTERNAL_CURRENCY_CODE, :new.EXTERNAL_CURRENCY_CODE,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.ISO_CODE <> :new.ISO_CODE OR
     (:old.ISO_CODE is null AND :new.ISO_CODE is not null) OR
     (:new.ISO_CODE is null AND :old.ISO_CODE is not null) THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'ISO_CODE',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :old.ISO_CODE, :new.ISO_CODE,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
END IF;
IF INSERTING THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_DISPLAY_FACTOR',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :new.CURRENCY_DISPLAY_FACTOR,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_DISPLAY_SYMBOL',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :new.CURRENCY_DISPLAY_SYMBOL,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_ID',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :new.CURRENCY_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'DESCRIPTION',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :new.DESCRIPTION,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'EXTERNAL_CURRENCY_CODE',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :new.EXTERNAL_CURRENCY_CODE,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'ISO_CODE',
        ''|| 'CURRENCY_ID=' || :new.CURRENCY_ID||'; ', pk_lookup, :new.ISO_CODE,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
IF DELETING THEN
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_DISPLAY_FACTOR',
        ''|| 'CURRENCY_ID=' || :old.CURRENCY_ID||'; ', pk_lookup, :old.CURRENCY_DISPLAY_FACTOR,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_DISPLAY_SYMBOL',
        ''|| 'CURRENCY_ID=' || :old.CURRENCY_ID||'; ', pk_lookup, :old.CURRENCY_DISPLAY_SYMBOL,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'CURRENCY_ID',
        ''|| 'CURRENCY_ID=' || :old.CURRENCY_ID||'; ', pk_lookup, :old.CURRENCY_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'DESCRIPTION',
        ''|| 'CURRENCY_ID=' || :old.CURRENCY_ID||'; ', pk_lookup, :old.DESCRIPTION,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'EXTERNAL_CURRENCY_CODE',
        ''|| 'CURRENCY_ID=' || :old.CURRENCY_ID||'; ', pk_lookup, :old.EXTERNAL_CURRENCY_CODE,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_TABLES_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'CURRENCY', 'ISO_CODE',
        ''|| 'CURRENCY_ID=' || :old.CURRENCY_ID||'; ', pk_lookup, :old.ISO_CODE,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3999, 0, 2017, 1, 0, 0, 49800, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049800_lease_pp_au_currency_trigger_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;