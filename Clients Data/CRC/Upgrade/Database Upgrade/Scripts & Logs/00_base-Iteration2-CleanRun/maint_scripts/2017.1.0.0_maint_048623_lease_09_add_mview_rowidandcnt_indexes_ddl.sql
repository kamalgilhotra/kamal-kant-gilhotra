/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048623_lease_09_add_mview_rowidandcnt_indexes_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/15/2017 Andrew Hill      Add indices to mview rowids/counts to improve refresh performance
||============================================================================
*/

create index mv_mc_ls_asset_in_rowidlas_idx on mv_multicurr_ls_asset_inner(rowidlas) tablespace pwrplant_idx compute statistics;
create index mv_mc_ls_asset_in_rowidla_idx on mv_multicurr_ls_asset_inner(rowidla) tablespace pwrplant_idx compute statistics;
create index mv_mc_ls_asset_in_rowidopt_idx on mv_multicurr_ls_asset_inner(rowidopt) tablespace pwrplant_idx compute statistics;
create index mv_mc_ls_asset_in_rowidldf_idx on mv_multicurr_ls_asset_inner(rowidldf) tablespace pwrplant_idx compute statistics;

create index mv_mc_lis_in_amt_lisrowid_idx on mv_multicurr_lis_inner_amounts(lisrowid) tablespace pwrplant_idx compute statistics;
create index mv_mc_lis_in_amt_optrowid_idx on mv_multicurr_lis_inner_amounts(optrowid) tablespace pwrplant_idx compute statistics;
create index mv_mc_lis_in_amt_lsobrowid_idx on mv_multicurr_lis_inner_amounts(liasobrowid) tablespace pwrplant_idx compute statistics;
create index mv_mc_lis_in_amt_ilrrowid_idx on mv_multicurr_lis_inner_amounts(ilrrowid) tablespace pwrplant_idx compute statistics;
create index mv_mc_lis_in_amt_lsrowid_idx on mv_multicurr_lis_inner_amounts(leaserowid) tablespace pwrplant_idx compute statistics;

create index mv_mc_lis_in_dpr_cntdexp_idx on mv_multicurr_lis_inner_depr(count_depr_expense) tablespace pwrplant_idx compute statistics;
create index mv_mc_lis_in_dpr_cntbrsv_idx on mv_multicurr_lis_inner_depr(count_begin_reserve) tablespace pwrplant_idx compute statistics;
create index mv_mc_lis_in_dpr_cntersv_idx on mv_multicurr_lis_inner_depr(count_end_reserve) tablespace pwrplant_idx compute statistics;
create index mv_mc_lis_in_dpr_cntdeaa_idx on mv_multicurr_lis_inner_depr(count_depr_exp_alloc_adjust) tablespace pwrplant_idx compute statistics;
create index mv_mc_lis_in_dpr_cntall_idx on mv_multicurr_lis_inner_depr(count_all) tablespace pwrplant_idx compute statistics;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3654, 0, 2017, 1, 0, 0, 48623, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048623_lease_09_add_mview_rowidandcnt_indexes_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
