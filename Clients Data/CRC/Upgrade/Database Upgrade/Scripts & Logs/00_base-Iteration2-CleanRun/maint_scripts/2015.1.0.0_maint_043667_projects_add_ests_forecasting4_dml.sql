/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043667_projects_add_ests_forecasting4_dml.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     04/25/2015 Chris Mardis   Additional estimate attributes requires flexible number of detail levels
||============================================================================
*/

insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active)
values ('Estimate Description', 'wem.long_description', 'wem.long_description', 'wem.long_description', 'est_description', 'wem.long_description', 'wem.long_description', 'wem.long_description', 'Est Description', null, 'long_description', 1);

insert into wo_est_forecast_options (users, current_version, current_year, dollar_format, detail_levels, previous_version, previous_column, previous_version_id, show_commit, show_actuals, show_totals, show_totals_year, font_size, group_hrs_qty_first, show_hrs_qty, open_to_options, apply_rates, rate_option_dollars, rate_option_hours, rate_option_quantity, exclude_supp_data, summary_report_datawindow, plug_totals, lock_totals, lock_supp_totals, sort_above_curr_bv, template_name, used_last, template_level, template_locked, subtotal_details)
values ('xxx', null, -1, '#,##0', 'Funding Project||Expenditure Type||Estimated Charge Type||Department||Budget Plant Class||Work Order||Job Task (FP)||Job Task (WO)||Job Task (List)||Attribute 01||Attribute 02||Attribute 03||Attribute 04||Attribute 05||Attribute 06||Attribute 07||Attribute 08||Attribute 09||Attribute 10||Estimate Description||N/A', 0, 0, null, 0, 0, 0, 1, 2, 0, 1, 0, 1, 1, 1, 1, 0, null, 0, 0, 0, 0, 'Detailed Grid Estimates', 0, 'fp', 1, '1||0||0||0||0||0||0||0||0||0||0||0||0||0||0||0||0||0||0||0||0');

insert into wo_est_forecast_options (users, current_version, current_year, dollar_format, detail_levels, previous_version, previous_column, previous_version_id, show_commit, show_actuals, show_totals, show_totals_year, font_size, group_hrs_qty_first, show_hrs_qty, open_to_options, apply_rates, rate_option_dollars, rate_option_hours, rate_option_quantity, exclude_supp_data, summary_report_datawindow, plug_totals, lock_totals, lock_supp_totals, sort_above_curr_bv, template_name, used_last, template_level, template_locked, subtotal_details)
select new_record_users, current_version, current_year, dollar_format, detail_levels, previous_version, previous_column, previous_version_id, show_commit, show_actuals, show_totals, show_totals_year, font_size, group_hrs_qty_first, show_hrs_qty, open_to_options, apply_rates, rate_option_dollars, rate_option_hours, rate_option_quantity, exclude_supp_data, summary_report_datawindow, plug_totals, lock_totals, lock_supp_totals, sort_above_curr_bv, template_name, used_last, new_template_level, template_locked, subtotal_details
from wo_est_forecast_options a,
   (select distinct users new_record_users from wo_est_forecast_options) b,
   (select 'jt' new_template_level from dual union all select 'wo' new_template_level from dual union all select 'fp' new_template_level from dual union all select 'bv' new_template_level from dual) c
where users = 'xxx'
and template_name = 'Detailed Grid Estimates'
and template_level = 'fp'
and not (new_record_users = 'xxx' and new_template_level = 'fp');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2532, 0, 2015, 1, 0, 0, 43667, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043667_projects_add_ests_forecasting4_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;