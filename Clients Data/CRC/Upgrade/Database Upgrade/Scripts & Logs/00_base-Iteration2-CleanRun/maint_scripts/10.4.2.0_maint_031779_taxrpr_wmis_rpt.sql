/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031779_taxrpr_wmis_rpt.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/27/2014 Alex Pivoshenko
||============================================================================
*/

update REPAIR_WO_SEG_REPORTING
   set MY_EVALUATE = 0
 where PRETEST_EXPLAIN_ID = 6
   and MY_EVALUATE = 1;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER,
    OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: UOP Work Task Run Results',
          'UOP Work Task Repairs:  Process Run Results for Non-Related Work Orders for a specified batch.  Displays quantity values tested for each work order number, work request, and repair unit,as well the amount of additions expensed and retirements reversed.',
          null,
          'dw_rpr_rpt_netwk_process_wt',
          null,
          null,
          null,
          'UOPWT - 3000',
          null,
          null,
          null,
          13,
          103,
          43,
          33,
          1,
          3,
          null,
          null,
          TO_DATE('26-01-2014 21:14:30', 'dd-mm-yyyy hh24:mi:ss'),
          null,
          null,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (909, 0, 10, 4, 2, 0, 31779, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_031779_taxrpr_wmis_rpt.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;