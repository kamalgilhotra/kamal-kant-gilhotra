/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010631_version-updates.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.2   07/05/2012 AYP            10.3.4.2 version
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 10.3.4.0',
       PP_PATCH   = 'Patch: 10.3.4.2',
       POWERTAX_VERSION = '10.3.4.2',
       DATE_INSTALLED = sysdate,
       POST_VERSION = 'Version 10.3.4.0, April 11, 2012'
where PP_VERSION_ID = 1;


insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'automatedreview.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'batreporter.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'budget_allocations.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_allocations.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_balances.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_batch_derivation.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_batch_derivation_bdg.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_budget_entry_calc_all.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_build_combos.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_build_deriver.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_delete_budget_allocations.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_financial_reports_build.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_flatten_structures.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_man_jrnl_reversals.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_posting.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_posting_bdg.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_sap_trueup.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_sum.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_sum_ab.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_to_commitments.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cwip_charge.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'populate_matlrec.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'ppmetricbatch.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'pptocr.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'ppverify.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'pp_integration.exe', '10.3.4.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.4.2', 'cr_derivation_trueup.exe', '10.3.4.0');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (171, 0, 10, 3, 4, 2, 10631, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.2_maint_010631_version-updates.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'),
    SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
