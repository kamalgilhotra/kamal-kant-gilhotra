/*
||============================================================================================
|| Application: PowerPlan
|| File Name: maint_047549_lease_mc_add_curr_type_ddl.sql
||============================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------- ---------------------------------------------------
|| 2017.1.0.0 04/19/2017 Johnny Sisouphanh Create a table to store types of currency
||============================================================================================
*/ 

create table ls_lease_currency_type (
  ls_currency_type_id  number(22) not null,
  description   	varchar(35) not null,
  user_id				varchar2(18),
  time_stamp			date
);

alter table ls_lease_currency_type 
  add constraint ls_lease_currency_typepk
  primary key (ls_currency_type_id)
  using index tablespace pwrplant_idx;
  
comment on table ls_lease_currency_type is '(F) [06] The LS Currency Type table is a table that stores the types of currency. ';
comment on column ls_lease_currency_type.ls_currency_type_id is 'A reference to the type of currency. 1-Contract Currency Type, 2-Company Currency Type.';
comment on column ls_lease_currency_type.description is 'The description for the type of currency.';
comment on column ls_lease_currency_type.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_lease_currency_type.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3457, 0, 2017, 1, 0, 0, 47549, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047549_lease_mc_add_curr_type_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;