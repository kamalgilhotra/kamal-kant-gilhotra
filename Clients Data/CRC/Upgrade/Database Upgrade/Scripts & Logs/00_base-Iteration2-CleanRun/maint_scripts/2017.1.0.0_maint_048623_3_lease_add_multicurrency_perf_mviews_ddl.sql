/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048623_3_lease_add_multicurrency_perf_mviews_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2017.1.0.0 08/08/2017 Andrew Hill         Add materialized views & indexes to improve reporting performance
||
||============================================================================
*/ 


CREATE MATERIALIZED VIEW LOG ON ls_asset_schedule WITH ROWID INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW LOG ON ls_asset WITH ROWID, sequence(ilr_id, ls_asset_id) INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW LOG ON ls_ilr_options WITH ROWID INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW LOG ON ls_depr_forecast WITH ROWID, SEQUENCE(ls_asset_id, revision, set_of_books_id, MONTH, depr_expense, begin_reserve, end_reserve, depr_exp_alloc_adjust) INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW LOG ON ls_ilr_schedule WITH ROWID INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW LOG ON ls_ilr_amounts_set_of_books WITH ROWID INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW LOG ON ls_ilr WITH ROWID INCLUDING NEW VALUES;
create materialized view log on ls_lease with rowid including new values;

create materialized view mv_multicurr_ls_asset_inner refresh fast on commit as
select 
  la.ilr_id,
  las.ls_asset_id,
  las.revision,
  las.set_of_books_id,
  las.MONTH,
  la.contract_currency_id,
  las.residual_amount,
  las.term_penalty,
  las.bpo_price,
  las.beg_capital_cost,
  las.end_capital_cost,
  las.beg_obligation,
  las.end_obligation,
  las.beg_lt_obligation,
  las.end_lt_obligation,
  las.interest_accrual,
  las.principal_accrual,
  las.interest_paid,
  las.principal_paid,
  las.executory_accrual1,
  las.executory_accrual2,
  las.executory_accrual3,
  las.executory_accrual4,
  las.executory_accrual5,
  las.executory_accrual6,
  las.executory_accrual7,
  las.executory_accrual8,
  las.executory_accrual9,
  las.executory_accrual10,
  las.executory_paid1,
  las.executory_paid2,
  las.executory_paid3,
  las.executory_paid4,
  las.executory_paid5,
  las.executory_paid6,
  las.executory_paid7,
  las.executory_paid8,
  las.executory_paid9,
  las.executory_paid10,
  las.contingent_accrual1,
  las.contingent_accrual2,
  las.contingent_accrual3,
  las.contingent_accrual4,
  las.contingent_accrual5,
  las.contingent_accrual6,
  las.contingent_accrual7,
  las.contingent_accrual8,
  las.contingent_accrual9,
  las.contingent_accrual10,
  las.contingent_paid1,
  las.contingent_paid2,
  las.contingent_paid3,
  las.contingent_paid4,
  las.contingent_paid5,
  las.contingent_paid6,
  las.contingent_paid7,
  las.contingent_paid8,
  las.contingent_paid9,
  las.contingent_paid10,
  las.current_lease_cost,
  ldf.depr_expense,
  ldf.begin_reserve,
  ldf.end_reserve,
  ldf.depr_exp_alloc_adjust,
  la.company_id,
  opt.in_service_exchange_rate,
  la.description as asset_description,
  la.leased_asset_number,
  la.fmv,
  las.is_om,
  la.approved_revision,
  opt.lease_cap_type_id,
  la.ls_asset_status_id,
  la.retirement_date,
  las.ROWID rowidlas,
  la.rowid rowidla,
  opt.ROWID rowidopt,
  ldf.rowid rowidldf
FROM ls_asset_schedule las,
     ls_asset la,
     ls_ilr_options opt,
     ls_depr_forecast ldf
WHERE las.ls_asset_id = la.ls_asset_id
AND la.ilr_id = opt.ilr_id
AND las.revision = opt.revision
AND las.ls_asset_id = ldf.ls_asset_id (+)
AND las.revision = ldf.revision (+)
AND las.set_of_books_id = ldf.set_of_books_id (+)
AND las.month = ldf.month (+);

CREATE INDEX mv_mc_ls_asset_in_ctc_cur_idx ON mv_multicurr_ls_asset_inner (contract_currency_id) TABLESPACE pwrplant_idx compute statistics;
CREATE INDEX mv_mc_ls_asset_in_cmpy_cur_idx on mv_multicurr_ls_asset_inner (company_id, contract_currency_id) tablespace pwrplant_idx compute statistics;
CREATE INDEX mv_mc_ls_asset_in_sob_idx ON mv_multicurr_ls_asset_inner (set_of_books_id) TABLESPACE pwrplant_idx compute statistics;
CREATE INDEX mv_mc_ls_asset_in_msob_idx ON mv_multicurr_ls_asset_inner(MONTH, set_of_books_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_ls_asset_in_many_idx ON mv_multicurr_ls_asset_inner (contract_currency_id, MONTH, company_id, set_of_books_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_ls_asset_in_astid_idx ON mv_multicurr_ls_asset_inner (ls_asset_id) TABLESPACE pwrplant_idx compute statistics;

create materialized view mv_multicurr_lis_inner_depr refresh fast on commit as
SELECT  la.ilr_id, 
        ldf.revision, 
        ldf.set_of_books_id,
        ldf.MONTH, 
        SUM(ldf.depr_expense) AS depr_expense,
        COUNT(ldf.depr_expense) AS count_depr_expense,
        SUM(ldf.begin_reserve) AS begin_reserve,
        count(ldf.begin_reserve) as count_begin_reserve,
        SUM(ldf.end_reserve) AS end_reserve,
        COUNT(ldf.end_reserve) AS count_end_reserve,
        SUM(ldf.depr_exp_alloc_adjust) AS depr_exp_alloc_adjust,
        count(ldf.depr_exp_alloc_adjust) as count_depr_exp_alloc_adjust,
        count(*) as count_all
FROM  ls_asset la, 
      ls_depr_forecast ldf
WHERE la.ls_asset_id = ldf.ls_asset_id
GROUP BY  la.ilr_id, 
          ldf.revision, 
          ldf.set_of_books_id, 
          ldf.MONTH;

create index mv_mc_lis_inner_depr_idx on mv_multicurr_lis_inner_depr(ilr_id, revision, month, set_of_books_id) tablespace pwrplant_idx compute statistics;

create materialized view mv_multicurr_lis_inner_amounts refresh fast on commit as
SELECT  LIS.ilr_id,
        ilr.ilr_number,
        ilr.current_revision,
        lis.revision,
        lis.set_of_books_id,
        lis.MONTH,
        lis.beg_capital_cost,
        lis.end_capital_cost,
        lis.beg_obligation,
        lis.end_obligation,
        lis.beg_lt_obligation,
        lis.end_lt_obligation,
        lis.interest_accrual,
        lis.principal_accrual,
        lis.interest_paid,
        lis.principal_paid,
        lis.executory_accrual1,
        lis.executory_accrual2,
        lis.executory_accrual3,
        lis.executory_accrual4,
        lis.executory_accrual5,
        lis.executory_accrual6,
        lis.executory_accrual7,
        lis.executory_accrual8,
        lis.executory_accrual9,
        lis.executory_accrual10,
        lis.executory_paid1,
        lis.executory_paid2,
        lis.executory_paid3,
        lis.executory_paid4,
        lis.executory_paid5,
        lis.executory_paid6,
        lis.executory_paid7,
        lis.executory_paid8,
        lis.executory_paid9,
        lis.executory_paid10,
        lis.contingent_accrual1,
        lis.contingent_accrual2,
        lis.contingent_accrual3,
        lis.contingent_accrual4,
        lis.contingent_accrual5,
        lis.contingent_accrual6,
        lis.contingent_accrual7,
        lis.contingent_accrual8,
        lis.contingent_accrual9,
        lis.contingent_accrual10,
        lis.contingent_paid1,
        lis.contingent_paid2,
        lis.contingent_paid3,
        lis.contingent_paid4,
        lis.contingent_paid5,
        lis.contingent_paid6,
        lis.contingent_paid7,
        lis.contingent_paid8,
        lis.contingent_paid9,
        lis.contingent_paid10,
        lis.current_lease_cost,
        ilr.lease_id,
        ilr.company_id,
        opt.in_service_exchange_rate,
        opt.purchase_option_amt,
        opt.termination_amt,
        liasob.net_present_value,
        liasob.capital_cost,
        LIS.is_om,
        LIS.ROWID AS lisrowid,
        opt.ROWID AS optrowid,
        liasob.ROWID AS liasobrowid,
        ilr.ROWID AS ilrrowid,
        lease.rowid as leaserowid
FROM  ls_ilr_schedule LIS,
      ls_ilr_options opt,
      ls_ilr_amounts_set_of_books liasob,
      ls_ilr ilr,
      ls_lease lease
WHERE LIS.ilr_id = opt.ilr_id
      AND lis.revision = opt.revision
AND LIS.ilr_id = liasob.ilr_id
    AND lis.revision = liasob.revision
    AND LIS.set_of_books_id = liasob.set_of_books_id
AND LIS.ilr_id = ilr.ilr_id
AND ilr.lease_id = lease.lease_id;

CREATE INDEX mv_mc_lis_inner_amounts_idx ON mv_multicurr_lis_inner_amounts(ilr_id, revision, MONTH, set_of_books_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lis_inner_amounts_rev_id ON mv_multicurr_lis_inner_amounts(revision) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lis_inner_amts_month_idx ON mv_multicurr_lis_inner_amounts(MONTH) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lis_inner_amts_idx2 ON mv_multicurr_lis_inner_amounts(company_id, lease_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lis_inner_amts_lease_idx ON mv_multicurr_lis_inner_amounts(lease_id) TABLESPACE pwrplant_idx compute statistics;

CREATE OR REPLACE VIEW v_multicurrency_lis_inner AS
SELECT
  A.ilr_id,
  A.ilr_number,
  A.current_revision,
  a.revision,
  a.set_of_books_id,
  a.MONTH,
  A.beg_capital_cost,
  a.end_capital_cost,
  a.beg_obligation,
  a.end_obligation,
  a.beg_lt_obligation,
  a.end_lt_obligation,
  a.interest_accrual,
  a.principal_accrual,
  a.interest_paid,
  a.principal_paid,
  a.executory_accrual1,
  a.executory_accrual2,
  a.executory_accrual3,
  a.executory_accrual4,
  a.executory_accrual5,
  a.executory_accrual6,
  a.executory_accrual7,
  a.executory_accrual8,
  a.executory_accrual9,
  a.executory_accrual10,
  a.executory_paid1,
  a.executory_paid2,
  a.executory_paid3,
  a.executory_paid4,
  a.executory_paid5,
  a.executory_paid6,
  a.executory_paid7,
  a.executory_paid8,
  a.executory_paid9,
  a.executory_paid10,
  a.contingent_accrual1,
  a.contingent_accrual2,
  a.contingent_accrual3,
  a.contingent_accrual4,
  a.contingent_accrual5,
  a.contingent_accrual6,
  a.contingent_accrual7,
  a.contingent_accrual8,
  a.contingent_accrual9,
  a.contingent_accrual10,
  a.contingent_paid1,
  a.contingent_paid2,
  a.contingent_paid3,
  a.contingent_paid4,
  a.contingent_paid5,
  a.contingent_paid6,
  a.contingent_paid7,
  a.contingent_paid8,
  a.contingent_paid9,
  a.contingent_paid10,
  a.current_lease_cost,
  depr.depr_expense,
  depr.begin_reserve,
  depr.end_reserve,
  depr.depr_exp_alloc_adjust,
  a.lease_id,
  a.company_id,
  A.in_service_exchange_rate,
  a.purchase_option_amt,
  a.termination_amt,
  a.net_present_value,
  a.capital_cost,
  A.is_om
FROM mv_multicurr_lis_inner_amounts A
LEFT OUTER JOIN mv_multicurr_lis_inner_depr depr
  ON a.ilr_id = depr.ilr_id
  AND a.revision = depr.revision
  AND A.set_of_books_id = depr.set_of_books_id
  AND a.month = depr.month;
  

CREATE OR REPLACE VIEW v_multicurrency_ls_asset_inner AS
SELECT
  ilr_id,
  ls_asset_id,
  revision,
  set_of_books_id,
  MONTH,
  contract_currency_id,
  residual_amount,
  term_penalty,
  bpo_price,
  beg_capital_cost,
  end_capital_cost,
  beg_obligation,
  end_obligation,
  beg_lt_obligation,
  end_lt_obligation,
  interest_accrual,
  principal_accrual,
  interest_paid,
  principal_paid,
  executory_accrual1,
  executory_accrual2,
  executory_accrual3,
  executory_accrual4,
  executory_accrual5,
  executory_accrual6,
  executory_accrual7,
  executory_accrual8,
  executory_accrual9,
  executory_accrual10,
  executory_paid1,
  executory_paid2,
  executory_paid3,
  executory_paid4,
  executory_paid5,
  executory_paid6,
  executory_paid7,
  executory_paid8,
  executory_paid9,
  executory_paid10,
  contingent_accrual1,
  contingent_accrual2,
  contingent_accrual3,
  contingent_accrual4,
  contingent_accrual5,
  contingent_accrual6,
  contingent_accrual7,
  contingent_accrual8,
  contingent_accrual9,
  contingent_accrual10,
  contingent_paid1,
  contingent_paid2,
  contingent_paid3,
  contingent_paid4,
  contingent_paid5,
  contingent_paid6,
  contingent_paid7,
  contingent_paid8,
  contingent_paid9,
  contingent_paid10,
  current_lease_cost,
  depr_expense,
  begin_reserve,
  end_reserve,
  depr_exp_alloc_adjust,
  company_id,
  in_service_exchange_rate,
  asset_description,
  leased_asset_number,
  fmv,
  is_om,
  approved_revision,
  lease_cap_type_id,
  ls_asset_status_id,
  retirement_date
FROM mv_multicurr_ls_asset_inner;

CREATE OR REPLACE VIEW v_ls_asset_schedule_fx_vw as
WITH
cur AS (
  SELECT ls_cur_type, currency_id, currency_display_symbol, iso_code, contract_approval_rate
  FROM (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
      contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 contract_approval_rate
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id,
      company_cur.currency_display_symbol, company_cur.iso_code, NULL
    FROM currency company_cur
  )
),
open_month AS (
  SELECT company_id, Min(gl_posting_mo_yr) open_month
  FROM ls_process_control
  WHERE open_next IS NULL
  GROUP BY company_id
),
cr AS (
  SELECT exchange_date, currency_from, currency_to, rate, add_months(exchange_date, -1) exchange_date_less_one
  FROM currency_rate_default a
  WHERE exchange_date = (select max(exchange_date)
                          from currency_rate_default b
                          where to_char(a.exchange_date, 'yyyymm') = to_char(b.exchange_date, 'yyyymm')
                          and a.currency_from = b.currency_from and a.currency_to = b.currency_to)
),
calc_rate AS (
  SELECT a.company_id, a.contract_currency_id, a.company_currency_id,
    a.accounting_month, a.exchange_date, a.rate, b.rate prev_rate
  FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b
  ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = Add_Months(b.accounting_month, 1)
),
cr_now as (
  SELECT a.exchange_date, a.currency_from, a.currency_to, a.rate
  FROM currency_rate_default a
  WHERE a.exchange_date = (
    SELECT Max(b.exchange_date)
    FROM currency_rate_default b
    WHERE a.currency_from = b.currency_from
    AND a.currency_to = b.currency_to
    AND To_Char(b.exchange_date, 'YYYYMMDD') <= To_Char(SYSDATE, 'YYYYMMDD')
  )
)
SELECT
  las.ilr_id ilr_id,
  las.ls_asset_id ls_asset_id,
  las.revision revision,
  las.set_of_books_id set_of_books_id,
  las.MONTH MONTH,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  cr.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  las.contract_currency_id,
  cur.currency_id,
  cr.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  las.residual_amount * nvl(calc_rate.rate, cr.rate) residual_amount,
  las.term_penalty * nvl(calc_rate.rate, cr.rate) term_penalty,
  las.bpo_price * nvl(calc_rate.rate, cr.rate) bpo_price,
  las.beg_capital_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) beg_capital_cost,
  las.end_capital_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) end_capital_cost,
  las.beg_obligation * nvl(calc_rate.rate, cr.rate) beg_obligation,
  las.end_obligation * nvl(calc_rate.rate, cr.rate) end_obligation,
  las.beg_lt_obligation * nvl(calc_rate.rate, cr.rate) beg_lt_obligation,
  las.end_lt_obligation * nvl(calc_rate.rate, cr.rate) end_lt_obligation,
  las.interest_accrual * nvl(calc_rate.rate, cr.rate) interest_accrual,
  las.principal_accrual * nvl(calc_rate.rate, cr.rate) principal_accrual,
  las.interest_paid * nvl(calc_rate.rate, cr.rate) interest_paid,
  las.principal_paid * nvl(calc_rate.rate, cr.rate) principal_paid,
  las.executory_accrual1 * nvl(calc_rate.rate, cr.rate) executory_accrual1,
  las.executory_accrual2 * nvl(calc_rate.rate, cr.rate) executory_accrual2,
  las.executory_accrual3 * nvl(calc_rate.rate, cr.rate) executory_accrual3,
  las.executory_accrual4 * nvl(calc_rate.rate, cr.rate) executory_accrual4,
  las.executory_accrual5 * nvl(calc_rate.rate, cr.rate) executory_accrual5,
  las.executory_accrual6 * nvl(calc_rate.rate, cr.rate) executory_accrual6,
  las.executory_accrual7 * nvl(calc_rate.rate, cr.rate) executory_accrual7,
  las.executory_accrual8 * nvl(calc_rate.rate, cr.rate) executory_accrual8,
  las.executory_accrual9 * nvl(calc_rate.rate, cr.rate) executory_accrual9,
  las.executory_accrual10 * nvl(calc_rate.rate, cr.rate) executory_accrual10,
  las.executory_paid1 * nvl(calc_rate.rate, cr.rate) executory_paid1,
  las.executory_paid2 * nvl(calc_rate.rate, cr.rate) executory_paid2,
  las.executory_paid3 * nvl(calc_rate.rate, cr.rate) executory_paid3,
  las.executory_paid4 * nvl(calc_rate.rate, cr.rate) executory_paid4,
  las.executory_paid5 * nvl(calc_rate.rate, cr.rate) executory_paid5,
  las.executory_paid6 * nvl(calc_rate.rate, cr.rate) executory_paid6,
  las.executory_paid7 * nvl(calc_rate.rate, cr.rate) executory_paid7,
  las.executory_paid8 * nvl(calc_rate.rate, cr.rate) executory_paid8,
  las.executory_paid9 * nvl(calc_rate.rate, cr.rate) executory_paid9,
  las.executory_paid10 * nvl(calc_rate.rate, cr.rate) executory_paid10,
  las.contingent_accrual1 * nvl(calc_rate.rate, cr.rate) contingent_accrual1,
  las.contingent_accrual2 * nvl(calc_rate.rate, cr.rate) contingent_accrual2,
  las.contingent_accrual3 * nvl(calc_rate.rate, cr.rate) contingent_accrual3,
  las.contingent_accrual4 * nvl(calc_rate.rate, cr.rate) contingent_accrual4,
  las.contingent_accrual5 * nvl(calc_rate.rate, cr.rate) contingent_accrual5,
  las.contingent_accrual6 * nvl(calc_rate.rate, cr.rate) contingent_accrual6,
  las.contingent_accrual7 * nvl(calc_rate.rate, cr.rate) contingent_accrual7,
  las.contingent_accrual8 * nvl(calc_rate.rate, cr.rate) contingent_accrual8,
  las.contingent_accrual9 * nvl(calc_rate.rate, cr.rate) contingent_accrual9,
  las.contingent_accrual10 * nvl(calc_rate.rate, cr.rate) contingent_accrual10,
  las.contingent_paid1 * nvl(calc_rate.rate, cr.rate) contingent_paid1,
  las.contingent_paid2 * nvl(calc_rate.rate, cr.rate) contingent_paid2,
  las.contingent_paid3 * nvl(calc_rate.rate, cr.rate) contingent_paid3,
  las.contingent_paid4 * nvl(calc_rate.rate, cr.rate) contingent_paid4,
  las.contingent_paid5 * nvl(calc_rate.rate, cr.rate) contingent_paid5,
  las.contingent_paid6 * nvl(calc_rate.rate, cr.rate) contingent_paid6,
  las.contingent_paid7 * nvl(calc_rate.rate, cr.rate) contingent_paid7,
  las.contingent_paid8 * nvl(calc_rate.rate, cr.rate) contingent_paid8,
  las.contingent_paid9 * nvl(calc_rate.rate, cr.rate) contingent_paid9,
  las.contingent_paid10 * nvl(calc_rate.rate, cr.rate) contingent_paid10,
  las.current_lease_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) current_lease_cost,
  decode(calc_rate.rate, NULL, 0, ( las.beg_obligation * ( calc_rate.rate - coalesce(calc_rate.prev_rate, las.in_service_exchange_rate, calc_rate.rate, 0 ) ) ) ) gain_loss_fx,
  las.depr_expense * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) depr_expense,
  las.begin_reserve * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) begin_reserve,
  las.end_reserve * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) end_reserve,
  las.depr_exp_alloc_adjust * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) depr_exp_alloc_adjust,
  las.asset_description,
  las.leased_asset_number,
  las.fmv * nvl(calc_rate.rate, cr.rate) fmv,
  las.is_om,
  las.approved_revision,
  las.lease_cap_type_id,
  las.ls_asset_status_id,
  las.retirement_date
FROM mv_multicurr_ls_asset_inner las
INNER JOIN currency_schema cs
  ON las.company_id = cs.company_id
INNER JOIN cur
  ON (cur.ls_cur_type = 1 AND cur.currency_id = las.contract_currency_id)
  OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
INNER JOIN open_month
  ON las.company_id = open_month.company_id
INNER JOIN cr
  ON cur.currency_id = cr.currency_to
  AND las.contract_currency_id = cr.currency_from
  AND cr.exchange_date_less_one < las.month
INNER JOIN cr_now
  ON cur.currency_id = cr_now.currency_to
  AND las.contract_currency_id = cr_now.currency_from
LEFT OUTER JOIN calc_rate
  ON las.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND las.company_id = calc_rate.company_id
  AND las.MONTH = calc_rate.accounting_month
WHERE cs.currency_type_id = 1;


CREATE OR REPLACE VIEW v_ls_ilr_schedule_fx_vw as
WITH
cur AS (
  select ls_cur_type, currency_id, currency_display_symbol, iso_code, contract_approval_rate
  from (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
      contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 contract_approval_rate
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id,
      company_cur.currency_display_symbol, company_cur.iso_code, NULL
    FROM currency company_cur
  )
),
open_month AS (
  SELECT company_id, Min(gl_posting_mo_yr) open_month
  FROM ls_process_control
  WHERE open_next IS NULL
  GROUP BY company_id
),
cr AS (
  SELECT exchange_date, currency_from, currency_to, rate, add_months(exchange_date, -1) as exchange_date_less_one
  FROM currency_rate_default a
  WHERE exchange_date = (select max(exchange_date)
                         from currency_rate_default b
                         where to_char(a.exchange_date, 'yyyymm') = to_char(b.exchange_date, 'yyyymm')
                         and a.currency_from = b.currency_from and a.currency_to = b.currency_to)
),
calc_rate AS (
  SELECT a.company_id, a.contract_currency_id, a.company_currency_id, a.accounting_month, a.exchange_date, a.rate, b.rate prev_rate
  FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b
  ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = Add_Months(b.accounting_month, 1)
),
cr_now as (
	SELECT a.exchange_date, a.currency_from, a.currency_to, a.rate
	FROM currency_rate_default a
	WHERE a.exchange_date = (
		SELECT Max(b.exchange_date)
		FROM currency_rate_default b
		WHERE a.currency_from = b.currency_from
		AND a.currency_to = b.currency_to
		AND To_Char(b.exchange_date, 'YYYYMMDD') <= To_Char(SYSDATE, 'YYYYMMDD')
	)
)
SELECT
  lis.ilr_id ilr_id,
  lis.ilr_number,
  lease.lease_id,
  lease.lease_number,
  lis.current_revision,
  lis.revision revision,
  lis.set_of_books_id set_of_books_id,
  lis.MONTH MONTH,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  cr.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  lease.contract_currency_id,
  cur.currency_id display_currency_id,
  cr.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  lis.is_om,
  lis.purchase_option_amt * nvl(calc_rate.rate, cr.rate) purchase_option_amt,
  lis.termination_amt * nvl(calc_rate.rate, cr.rate) termination_amt,
  lis.net_present_value * nvl(calc_rate.rate, cr.rate) net_present_value,
  lis.capital_cost * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) capital_cost,
  lis.beg_capital_cost * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) beg_capital_cost,
  lis.end_capital_cost * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) end_capital_cost,
  lis.beg_obligation * nvl(calc_rate.rate, cr.rate) beg_obligation,
  lis.end_obligation * nvl(calc_rate.rate, cr.rate) end_obligation,
  lis.beg_lt_obligation * nvl(calc_rate.rate, cr.rate) beg_lt_obligation,
  lis.end_lt_obligation * nvl(calc_rate.rate, cr.rate) end_lt_obligation,
  lis.interest_accrual * nvl(calc_rate.rate, cr.rate) interest_accrual,
  lis.principal_accrual * nvl(calc_rate.rate, cr.rate) principal_accrual,
  lis.interest_paid * nvl(calc_rate.rate, cr.rate) interest_paid,
  lis.principal_paid * nvl(calc_rate.rate, cr.rate) principal_paid,
  lis.executory_accrual1 * nvl(calc_rate.rate, cr.rate) executory_accrual1,
  lis.executory_accrual2 * nvl(calc_rate.rate, cr.rate) executory_accrual2,
  lis.executory_accrual3 * nvl(calc_rate.rate, cr.rate) executory_accrual3,
  lis.executory_accrual4 * nvl(calc_rate.rate, cr.rate) executory_accrual4,
  lis.executory_accrual5 * nvl(calc_rate.rate, cr.rate) executory_accrual5,
  lis.executory_accrual6 * nvl(calc_rate.rate, cr.rate) executory_accrual6,
  lis.executory_accrual7 * nvl(calc_rate.rate, cr.rate) executory_accrual7,
  lis.executory_accrual8 * nvl(calc_rate.rate, cr.rate) executory_accrual8,
  lis.executory_accrual9 * nvl(calc_rate.rate, cr.rate) executory_accrual9,
  lis.executory_accrual10 * nvl(calc_rate.rate, cr.rate) executory_accrual10,
  lis.executory_paid1 * nvl(calc_rate.rate, cr.rate) executory_paid1,
  lis.executory_paid2 * nvl(calc_rate.rate, cr.rate) executory_paid2,
  lis.executory_paid3 * nvl(calc_rate.rate, cr.rate) executory_paid3,
  lis.executory_paid4 * nvl(calc_rate.rate, cr.rate) executory_paid4,
  lis.executory_paid5 * nvl(calc_rate.rate, cr.rate) executory_paid5,
  lis.executory_paid6 * nvl(calc_rate.rate, cr.rate) executory_paid6,
  lis.executory_paid7 * nvl(calc_rate.rate, cr.rate) executory_paid7,
  lis.executory_paid8 * nvl(calc_rate.rate, cr.rate) executory_paid8,
  lis.executory_paid9 * nvl(calc_rate.rate, cr.rate) executory_paid9,
  lis.executory_paid10 * nvl(calc_rate.rate, cr.rate) executory_paid10,
  lis.contingent_accrual1 * nvl(calc_rate.rate, cr.rate) contingent_accrual1,
  lis.contingent_accrual2 * nvl(calc_rate.rate, cr.rate) contingent_accrual2,
  lis.contingent_accrual3 * nvl(calc_rate.rate, cr.rate) contingent_accrual3,
  lis.contingent_accrual4 * nvl(calc_rate.rate, cr.rate) contingent_accrual4,
  lis.contingent_accrual5 * nvl(calc_rate.rate, cr.rate) contingent_accrual5,
  lis.contingent_accrual6 * nvl(calc_rate.rate, cr.rate) contingent_accrual6,
  lis.contingent_accrual7 * nvl(calc_rate.rate, cr.rate) contingent_accrual7,
  lis.contingent_accrual8 * nvl(calc_rate.rate, cr.rate) contingent_accrual8,
  lis.contingent_accrual9 * nvl(calc_rate.rate, cr.rate) contingent_accrual9,
  lis.contingent_accrual10 * nvl(calc_rate.rate, cr.rate) contingent_accrual10,
  lis.contingent_paid1 * nvl(calc_rate.rate, cr.rate) contingent_paid1,
  lis.contingent_paid2 * nvl(calc_rate.rate, cr.rate) contingent_paid2,
  lis.contingent_paid3 * nvl(calc_rate.rate, cr.rate) contingent_paid3,
  lis.contingent_paid4 * nvl(calc_rate.rate, cr.rate) contingent_paid4,
  lis.contingent_paid5 * nvl(calc_rate.rate, cr.rate) contingent_paid5,
  lis.contingent_paid6 * nvl(calc_rate.rate, cr.rate) contingent_paid6,
  lis.contingent_paid7 * nvl(calc_rate.rate, cr.rate) contingent_paid7,
  lis.contingent_paid8 * nvl(calc_rate.rate, cr.rate) contingent_paid8,
  lis.contingent_paid9 * nvl(calc_rate.rate, cr.rate) contingent_paid9,
  lis.contingent_paid10 * nvl(calc_rate.rate, cr.rate) contingent_paid10,
  lis.current_lease_cost * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) current_lease_cost,
  decode(calc_rate.rate, NULL, 0, ( lis.beg_obligation * ( calc_rate.rate - coalesce(calc_rate.prev_rate, lis.in_service_exchange_rate, calc_rate.rate, 0 ) ) ) ) gain_loss_fx,
  lis.depr_expense * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) depr_expense,
  lis.begin_reserve * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) begin_reserve,
  lis.end_reserve * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) end_reserve,
  lis.depr_exp_alloc_adjust * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) depr_exp_alloc_adjust
FROM v_multicurrency_lis_inner lis
INNER JOIN ls_lease lease
  ON lis.lease_id = lease.lease_id
INNER JOIN currency_schema cs
  ON lis.company_id = cs.company_id
INNER JOIN cur
  ON (cur.ls_cur_type = 1 AND cur.currency_id = lease.contract_currency_id)
  OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
INNER JOIN open_month
  ON lis.company_id = open_month.company_id
INNER JOIN cr
  ON cur.currency_id = cr.currency_to
  AND lease.contract_currency_id = cr.currency_from
  AND cr.exchange_date_less_one < lis.month
INNER JOIN cr_now
  ON cur.currency_id = cr_now.currency_to
  AND lease.contract_currency_id = cr_now.currency_from
LEFT OUTER JOIN calc_rate
  ON lease.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND lis.company_id = calc_rate.company_id
  AND lis.month = calc_rate.accounting_month
AND cs.currency_type_id = 1;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3634, 0, 2017, 1, 0, 0, 48623, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048623_3_lease_add_multicurrency_perf_mviews_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;