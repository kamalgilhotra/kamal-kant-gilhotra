SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_041066_sys_load_version_data.sql
|| Description: 
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.1 10/08/2014 Alex Pivoshenko  Patch Release
||============================================================================
*/

begin
   begin
      execute immediate 'create table PP_SCHEMA_UPGRADE
                         (
                          BASE_ID                  number(22) not null,
                          BASE_SKIPPED             number(22) default 0 not null,
                          BASE_MAJOR_VERSION       number(22) not null,
                          BASE_MINOR_VERSION       number(22) not null,
                          BASE_POINT_VERSION       number(22) not null,
                          BASE_PATCH_VERSION       number(22) not null,
                          BASE_MAINT_NUM           number(22) not null,
                          BASE_SCRIPT_PATH         varchar2(512) not null,
                          BASE_SCRIPT_NAME         varchar2(512) not null,
                          BASE_SCRIPT_REVISION     number(22) default 1 not null,
                          BASE_DATE_APPLIED        timestamp(6) default systimestamp not null,
                          BASE_OS_USER             varchar2(512),
                          BASE_TERMINAL            varchar2(512),
                          BASE_SERVICE_NAME        varchar2(512),
                          BASE_USER_ID             varchar2(18),
                          BASE_TIME_STAMP          date,
                          BASE_SCRIPT_LOG          clob,
                          RELEASED_VERSION         number(22),
                          RELEASED_ID              number(22),
                          RELEASED_SKIPPED         number(22) default 0 not null,
                          RELEASED_MAJOR_VERSION   number(22),
                          RELEASED_MINOR_VERSION   number(22),
                          RELEASED_POINT_VERSION   number(22),
                          RELEASED_PATCH_VERSION   number(22),
                          RELEASED_MAINT_NUM       number(22),
                          RELEASED_SCRIPT_PATH     varchar2(512),
                          RELEASED_SCRIPT_NAME     varchar2(512),
                          RELEASED_SCRIPT_REVISION number(22) default 1 not null,
                          RELEASED_DATE_APPLIED    timestamp(6) default systimestamp,
                          RELEASED_OS_USER         varchar2(512),
                          RELEASED_TERMINAL        varchar2(512),
                          RELEASED_SERVICE_NAME    varchar2(512),
                          RELEASED_USER_ID         varchar2(18),
                          RELEASED_TIME_STAMP      date,
                          RELEASED_SCRIPT_LOG      clob,
                          RELEASED_MODULE          varchar2(30),
                          RELEASED_NOT_NEEDED      number default 0 not null
                         )';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_SCHEMA_UPGRADE table already exists.');
   end;

   begin
      execute immediate 'alter table PP_SCHEMA_UPGRADE
                            add constraint PP_SCHEMA_UPGRADE_PK
                                primary key (RELEASED_ID, RELEASED_SCRIPT_REVISION)
                                using index tablespace PWRPLANT_IDX';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_SCHEMA_UPGRADE Primary Key already exists.');
   end;

   begin
      execute immediate 'alter table PP_SCHEMA_UPGRADE ADD SKIPPED_INSERT_STATEMENT varchar2(4000)';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_SCHEMA_UPGRADE already has SKIPPED_INSERT_STATEMENT column.');
   end;

   -- Create PP_SCHEMA_CHANGE_LOG if it doesn't exist. Ignore errors.
   begin
      execute immediate 'create table PP_SCHEMA_CHANGE_LOG
                        (
                         ID              number(22) not null,
                         SKIPPED         number(22) default 0 not null,
                         MAJOR_VERSION   number(22) not null,
                         MINOR_VERSION   number(22) not null,
                         POINT_VERSION   number(22) not null,
                         PATCH_VERSION   number(22) not null,
                         MAINT_NUM       number(22) not null,
                         SCRIPT_PATH     varchar2(256) not null,
                         SCRIPT_NAME     varchar2(256) not null,
                         SCRIPT_REVISION number(22) default 1 not null,
                         DATE_APPLIED    timestamp default systimestamp not null,
                         OS_USER         varchar2(256),
                         TERMINAL        varchar2(256),
                         SERVICE_NAME    varchar2(18),
                         USER_ID         varchar2(18),
                         TIME_STAMP      date
                        )';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_SCHEMA_CHANGE_LOG table already exists.');
   end;

   begin
      execute immediate 'alter table PP_SCHEMA_CHANGE_LOG
                            add constraint PP_SCHEMA_CHANGE_LOG_PK
                                primary key (ID, SCRIPT_REVISION)
                                using index tablespace PWRPLANT_IDX';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_SCHEMA_CHANGE_LOG Primary Key already exists.');
   end;

   begin
      execute immediate 'create or replace trigger PP_SCHEMA_CHANGE_LOG
                            before update or insert on PWRPLANT.PP_SCHEMA_CHANGE_LOG
                            for each row
                         begin
                            :NEW.USER_ID    := SYS_CONTEXT(''USERENV'', ''SESSION_USER'');
                            :NEW.TIME_STAMP := sysdate;
                         end;';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_SCHEMA_CHANGE_LOG table already exists.');
   end;
end;
/

/* Get Base Values
select BASE_ID,
       BASE_SKIPPED,
       BASE_MAJOR_VERSION,
       BASE_MINOR_VERSION,
       BASE_POINT_VERSION,
       BASE_PATCH_VERSION,
       BASE_MAINT_NUM,
       BASE_SCRIPT_PATH,
       BASE_SCRIPT_NAME,
       BASE_SCRIPT_REVISION,
       BASE_DATE_APPLIED,
       BASE_OS_USER,
       BASE_TERMINAL,
       BASE_SERVICE_NAME,
       BASE_USER_ID,
       BASE_TIME_STAMP,
       RELEASED_ID,
       RELEASED_SKIPPED,
       RELEASED_MAJOR_VERSION,
       RELEASED_MINOR_VERSION,
       RELEASED_POINT_VERSION,
       RELEASED_PATCH_VERSION,
       RELEASED_MAINT_NUM,
       RELEASED_SCRIPT_PATH,
       RELEASED_SCRIPT_NAME,
       RELEASED_SCRIPT_REVISION,
       RELEASED_DATE_APPLIED,
       RELEASED_OS_USER,
       RELEASED_TERMINAL,
       RELEASED_SERVICE_NAME,
       RELEASED_USER_ID,
       RELEASED_TIME_STAMP,
       RELEASED_NOT_NEEDED,
       RELEASED_MODULE,
       RELEASED_VERSION,
       SKIPPED_INSERT_STATEMENT
  from PP_SCHEMA_UPGRADE
 order by BASE_MAJOR_VERSION,
          BASE_MINOR_VERSION,
          BASE_POINT_VERSION,
          BASE_PATCH_VERSION,
          BASE_DATE_APPLIED;
*/
-- Fill in script data
--delete from PP_SCHEMA_UPGRADE;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2040, 0, 10, 4, 3, 1, 41066, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_041066_sys_load_version_data.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;