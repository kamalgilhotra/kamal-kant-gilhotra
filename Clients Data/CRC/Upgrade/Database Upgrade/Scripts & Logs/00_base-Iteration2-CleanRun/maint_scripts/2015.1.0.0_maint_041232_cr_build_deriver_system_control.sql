/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041232_cr_build_deriver_system_conyrol.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   11/21/2014 Anand Rajashekar    create a new cr system control CR BUILD DERIVER - DEBUG
||========================================================================================
*/

insert into CR_SYSTEM_CONTROL ( CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, LONG_DESCRIPTION ) values
(270, 'CR Build Deriver - Debug', 'No', sysdate, 'PWRPLANT', '"Yes" will turn on debugging and the underlying derivation SQL will be recorded in the online logs. "No" will turn off debugging.  The default is "No".') ;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2053, 0, 2015, 1, 0, 0, 41232, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041232_cr_build_deriver_system_control.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;