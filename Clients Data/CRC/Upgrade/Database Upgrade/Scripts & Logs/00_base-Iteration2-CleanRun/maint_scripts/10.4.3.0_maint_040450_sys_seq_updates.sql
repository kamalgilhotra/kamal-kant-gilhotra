/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040450_sys_seq_updates.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.3.0
|| 1.1		08/07/2015 Ken Streit	     ORA-30009 caused by large diff in start & cur
||========================================================================================
*/

--CAT: 11/7/15: There were some blank lines here previously that were causing errors, as well as the fact that the script wasn't written to accomodate for if the
--sequence didn't exist.
--PCD: 11/9/15: Logic no_sequence check failed when no_sequnce was null. Declared as 0 to start.

declare
   START_ID number;
   CURR_ID  number;
   the_count NUMBER;
   no_sequence NUMBER;
   create_sql VARCHAR2(2000);
   drop_sql VARCHAR2(2000);
begin
   no_sequence := 0;
   select NVL(max(OCCURRENCE_ID), 0) into START_ID from PP_PROCESSES_OCCURRENCES;

   --CAT: 11/7/2015: See if there IS a record in all sequences first.
   SELECT Count(*)
   INTO the_count
   FROM ALL_sequences
   WHERE sequence_name = 'PP_PROCESSES_OCURRENCES_SEQ';

   IF the_count = 0 THEN
     curr_id := 0;
     no_sequence := 1;
   ELSE
     SELECT last_number into CURR_ID FROM all_sequences WHERE sequence_name = 'PP_PROCESSES_OCURRENCES_SEQ';
   END IF;

   --SELECT last_number into CURR_ID FROM all_sequences WHERE sequence_name = 'PP_PROCESSES_OCURRENCES_SEQ';


   if START_ID > CURR_ID THEN
      drop_sql := 'DROP SEQUENCE PP_PROCESSES_OCURRENCES_SEQ';

	  --CAT: 11/7/15: There was a slash at the end here that previously wasn't working.  Removed.
      create_sql := 'CREATE SEQUENCE PP_PROCESSES_OCURRENCES_SEQ
        start with ' || to_char(START_ID + 1) || '
        INCREMENT BY 1
        NOCYCLE
        NOORDER
        NOCACHE';
	  --PCD added to_char() for error fix

      --We dont want to try to drop the sequence if it doesn't already exist.
      IF no_sequence <> 1 THEN
        EXECUTE IMMEDIATE drop_sql;
      END IF;

      EXECUTE IMMEDIATE create_sql;

   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1555, 0, 10, 4, 3, 0, 40450, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040450_sys_seq_updates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;