/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038647_reg_REG_FORECAST_LEDGER_SV.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.7 07/11/2014 Ryan Olivera   Added recon adj ammount/comment
||========================================================================================
*/

create or replace view REG_FORECAST_LEDGER_SV
(REG_COMPANY, REG_ACCOUNT, REG_SOURCE, FORECAST_LEDGER, GL_MONTH,
 FCST_AMOUNT, ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH, RECON_ADJ_AMOUNT, RECON_ADJ_COMMENT)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_SOURCE,
       FORECAST_LEDGER,
       GL_MONTH,
       FCST_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       RECON_ADJ_AMOUNT,
       RECON_ADJ_COMMENT
  from (select C.DESCRIPTION       REG_COMPANY,
               M.DESCRIPTION       REG_ACCOUNT,
               R.DESCRIPTION       REG_SOURCE,
               V.DESCRIPTION       FORECAST_LEDGER,
               L.GL_MONTH          GL_MONTH,
               L.FCST_AMOUNT       FCST_AMOUNT,
               L.ANNUALIZED_AMT    ANNUALIZED_AMT,
               L.ADJ_AMOUNT        ADJ_AMOUNT,
               L.ADJ_MONTH         ADJ_MONTH,
               L.RECON_ADJ_AMOUNT  RECON_ADJ_AMOUNT,
               L.RECON_ADJ_COMMENT RECON_ADJ_COMMENT
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_FORECAST_VERSION V,
               REG_FORECAST_LEDGER L,
               REG_SOURCE R
         where L.FORECAST_VERSION_ID = V.FORECAST_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1287, 0, 10, 4, 2, 7, 38647, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038647_reg_REG_FORECAST_LEDGER_SV.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;