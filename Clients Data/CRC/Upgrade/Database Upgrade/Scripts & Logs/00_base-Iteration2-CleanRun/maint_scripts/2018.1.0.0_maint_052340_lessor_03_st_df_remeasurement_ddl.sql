/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052340_lessor_03_st_df_remeasurement_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2018.1.0.0 10/22/2018 Anand R           make LSR_ILR_SCHEDULE_SALES_DIRECT.unguaran_residual_remeasure non null-able column
||============================================================================
*/

alter table LSR_ILR_SCHEDULE_SALES_DIRECT
   modify unguaran_residual_remeasure not null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10624, 0, 2018, 1, 0, 0, 52340, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052340_lessor_03_st_df_remeasurement_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;