/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045712_sys_add_rows_ppbase_alerts_dd_2_dml.sql.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2016.1.0  06/14/2016 Anand R       update text for actions drop down for Alerts
||============================================================================
*/

update ppbase_actions_windows set ACTION_TEXT = 'Details' where ID = 36;

update ppbase_actions_windows set ACTION_TEXT = 'Dashboard' where ID = 37;

update ppbase_actions_windows set ACTION_TEXT = 'Completion' where ID = 38;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3219, 0, 2016, 1, 0, 0, 045712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045712_sys_add_rows_ppbase_alerts_dd_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;