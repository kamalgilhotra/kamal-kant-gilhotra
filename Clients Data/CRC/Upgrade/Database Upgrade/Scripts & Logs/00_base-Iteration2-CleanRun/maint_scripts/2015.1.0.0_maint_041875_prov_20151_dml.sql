/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041875_prov_20151_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1   03/27/2015 Jarrett Skov   Changes to allow proper functionality of 51014 Current Provision Grid Report
||============================================================================
*/ 

-- Delete existing data


delete from tax_accrual_rep_cons_rows where rep_cons_type_id in (41,42,43,44);

delete from tax_accrual_rep_cons_cols where rep_cons_type_id in (41,42,43,44);

delete from tax_accrual_rep_cons_type where rep_cons_type_id in (41,42,43,44);

delete from tax_accrual_rep_special_note where description = 'CONSOLIDATED-OPER-CONS';

delete from pp_reports where report_id = 51014;



-- Insert new data

-- Old version of the report exists for some reason in Master DB
DELETE FROM pp_reports WHERE datawindow = 'dw_tax_accrual_state_prov_all_cons';

INSERT INTO pp_reports 
(	report_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, 
	pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw)
VALUES (	51014,	'Current Provision-All - Grid',	
	'Current Provision Report that starts with pre-tax income (not Federal Taxable Income) for all entities - Components across the top of the grid.',	'Tax Accrual',	'dw_tax_accrual_state_prov_all_cons',
	'CONSOLIDATED-OPER-CONS',	NULL,	NULL,	'Tax Accrual - 51014',	NULL,	NULL,	NULL,	NULL,	3,	0,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);


INSERT INTO tax_accrual_rep_special_note 
(	special_note_id, description, report_option_id, month_required, compare_case, m_rollup_required, calc_rates, report_type, window_name, dw_parameter_list, je_temp_table_ind, ns2fas109_temp_table_ind, 
	rep_rollup_group_id, default_acct_rollup, rep_cons_category_id)
VALUES (	76,	'CONSOLIDATED-OPER-CONS',	34,	1,	0,	0,	0,	0,	NULL,	6,	0,	0,	12,	NULL,	6);



-- Insert rows for rep_cons_type

INSERT INTO tax_accrual_rep_cons_type 
( rep_cons_type_id, description, datawindow, sort_spec, rep_cons_category_id, balances_ind)
VALUES (	41,	'Current By Oper',	'dw_tax_accrual_state_prov_all_cons',	'oper_ind, m_type_id, tax_return_key, detail_descr',	6,	0);

INSERT INTO tax_accrual_rep_cons_type 
( rep_cons_type_id, description, datawindow, sort_spec, rep_cons_category_id, balances_ind)
VALUES (	42,	'Current By Month',	'dw_tax_accrual_state_prov_all_cons',	NULL,	6,	0);

INSERT INTO tax_accrual_rep_cons_type 
( rep_cons_type_id, description, datawindow, sort_spec, rep_cons_category_id, balances_ind)
VALUES (	43,	'Current By Month Rollup',	'dw_tax_accrual_state_prov_all_cons',	NULL,	6,	0);

INSERT INTO tax_accrual_rep_cons_type 
( rep_cons_type_id, description, datawindow, sort_spec, rep_cons_category_id, balances_ind)
VALUES (	44,	'Current By Company',	'dw_tax_accrual_state_prov_all_cons',	NULL,	6,	0);


-- Insert rows for rep_cons_cols

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	41,	'oper_ind',	1,	'number',	'oper_descr',	0);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	41,	'detail_description',	1,	'string',	'detail_description',	1);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	41,	'm_type_id',	1,	'number',	'rollup_descr',	2);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	41,	'post_apport_ind',	1,	'number',	'post_apport_ind',	3);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	41,	'post_apport_ind2',	1,	'number',	'post_apport_ind2',	4);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	41,	'post_apport_ind3',	1,	'number',	'post_apport_ind3',	5);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	41,	'post_apport_ind4',	1,	'number',	'post_apport_ind4',	6);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	42,	'month_id',	1,	'number',	'month',	0);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	42,	'detail_description',	1,	'string',	'detail_description',	1);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	42,	'm_type_id',	1,	'number',	'rollup_descr',	2);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	42,	'post_apport_ind',	1,	'number',	'post_apport_ind',	3);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	42,	'post_apport_ind2',	1,	'number',	'post_apport_ind2',	4);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	42,	'post_apport_ind3',	1,	'number',	'post_apport_ind3',	5);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	42,	'post_apport_ind4',	1,	'number',	'post_apport_ind4',	6);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	43,	'month_id',	1,	'number',	'month',	0);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	43,	'detail_description',	1,	'string',	'detail_description',	1);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	43,	'm_type_id',	1,	'number',	'rollup_descr',	2);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	43,	'post_apport_ind',	1,	'number',	'post_apport_ind',	3);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	43,	'post_apport_ind2',	1,	'number',	'post_apport_ind2',	4);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	43,	'post_apport_ind3',	1,	'number',	'post_apport_ind3',	5);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	43,	'post_apport_ind4',	1,	'number',	'post_apport_ind4',	6);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	44,	'consol_desc',	1,	'string',	'consol_desc',	0);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	44,	'detail_description',	1,	'string',	'detail_description',	1);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	44,	'm_type_id',	1,	'number',	'rollup_descr',	2);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	44,	'post_apport_ind',	1,	'number',	'post_apport_ind',	3);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	44,	'post_apport_ind2',	1,	'number',	'post_apport_ind2',	4);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	44,	'post_apport_ind3',	1,	'number',	'post_apport_ind3',	5);

INSERT INTO tax_accrual_rep_cons_cols 
( rep_cons_type_id, cons_colname, rank, cons_coltype, cons_descr_colname, group_id)
VALUES (	44,	'post_apport_ind4',	1,	'number',	'post_apport_ind4',	6);


-- Insert rows for rep_cons_rows



INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	1,	'H',	-1,	1,	1,	'TEXT',	1,	'Current Consolidating Report by Oper',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	2,	'H',	-1,	2,	1,	'USER_TITLE',	0,	NULL,	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	3,	'H',	-1,	3,	1,	'COL_VALUE_TEXT',	0,	'case_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	4,	'H',	-1,	4,	1,	'COL_VALUE_TEXT',	0,	'consol_desc',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	5,	'H',	-1,	5,	1,	'COL_VALUE_TEXT',	0,	'entity_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	6,	'H',	-1,	6,	1,	'COL_VALUE_TEXT',	0,	'month_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	7,	'H',	-1,	7,	0,	'FILLER',	0,	NULL,	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	8,	'H',	0,	1,	0,	'COL_VALUE_NUM',	0,	'bi_cp',	'TEXT',	'Book Income',	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	9,	'H',	0,	2,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	10,	'H',	3,	2,	0,	'TEXT',	0,	NULL,	'TEXT',	'Tax Items',	'T',	'cons_tax_items_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	11,	'D',	1,	2,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_tax_items_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	12,	'F',	3,	1,	1,	'COL_VALUE_NUM',	0,	'tax_items_total_cm',	'TEXT',	'Tax Items',	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	13,	'F',	3,	2,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	14,	'F',	3,	3,	1,	'COL_VALUE_TRANS',	0,	'bi_cp + tax_items_total_cm',	'TEXT',	'Book Income Before Tax',	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	15,	'F',	3,	4,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	16,	'H',	2,	1,	0,	'TEXT',	0,	NULL,	'COL_VALUE_TEXT',	'rollup_descr',	'T',	'cons_regular_m_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	17,	'D',	1,	1,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_regular_m_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	18,	'F',	2,	2,	1,	'COL_VALUE_NUM',	0,	'm_rollup_total_cm',	'COL_VALUE_TEXT',	'rollup_descr',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	19,	'F',	2,	3,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	20,	'F',	3,	5,	1,	'COL_VALUE_NUM',	0,	'ti_b4_deductions_cp',	'TEXT',	'   Taxable Income Before Deductions',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	21,	'F',	3,	6,	0,	'COL_VALUE_NUM',	0,	'deductions_cp',	'TEXT',	'      Deduction for Fed/Other States',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	22,	'F',	3,	7,	1,	'COL_VALUE_NUM',	0,	'ti_b4_apport_cp',	'TEXT',	'   Taxable Income Before Apportionment',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	23,	'F',	3,	8,	0,	'COL_VALUE_NUM',	0,	'apportionment_cp',	'TEXT',	'      Apportionment Factor',	'PA',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	24,	'F',	3,	9,	1,	'COL_VALUE_NUM',	0,	'apport_ti_cp',	'TEXT',	'   Taxable Income After Apportioment',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	25,	'H',	4,	10,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	26,	'H',	4,	11,	0,	'TEXT',	0,	NULL,	'TEXT',	'Post Apportionment Items',	'T',	'cons_post_apport_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	27,	'D',	1,	12,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_post_apport_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	28,	'F',	4,	13,	1,	'COL_VALUE_NUM',	0,	'post_apport_total_cm',	'TEXT',	'Post Apportionment Items',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	29,	'F',	4,	14,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	30,	'F',	4,	15,	1,	'COL_VALUE_NUM',	0,	'ti_with_nols_cp',	'TEXT',	'   Taxable Income After Post Apport Adj',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	31,	'F',	4,	16,	0,	'COL_VALUE_NUM',	0,	'calc_stat_rate_cp',	'TEXT',	'      Statutory Tax Rate',	'P',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	32,	'F',	4,	17,	0,	'COL_VALUE_NUM',	0,	'ct_calc_one_rate_cp',	'TEXT',	'   Calculated Tax Before Credits',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	33,	'F',	4,	18,	0,	'COL_VALUE_NUM',	0,	'rate_diff_cp',	'TEXT',	'      Rate Differential',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	34,	'F',	4,	19,	0,	'COL_VALUE_NUM',	0,	'ct_calc_individual_rates_cp',	'TEXT',	'   Calculated Tax After Rate Differential',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	35,	'F',	4,	20,	0,	'COL_VALUE_TRANS',	0,	'tax_b4_credits_cp-ct_calc_individual_rates_cp',	'TEXT',	'   Current Period True-Up',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	36,	'F',	4,	21,	1,	'COL_VALUE_NUM',	0,	'tax_b4_credits_cp',	'TEXT',	'   Tax Before Credits',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	37,	'H',	5,	22,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	38,	'H',	5,	23,	0,	'TEXT',	0,	NULL,	'TEXT',	'Tax Credits and Adjustments',	'T',	'cons_tax_credit_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	39,	'D',	1,	24,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_tax_credit_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	40,	'F',	5,	25,	1,	'COL_VALUE_NUM',	0,	'tax_credit_total_cm',	'TEXT',	'Tax Credits and Adjustments',	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	41,	'F',	5,	26,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	42,	'F',	5,	27,	1,	'COL_VALUE_NUM',	0,	'current_tax_cp',	'TEXT',	'Current Tax',	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	43,	'H',	6,	28,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_other_adjust_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	44,	'H',	6,	29,	0,	'TEXT',	0,	NULL,	'TEXT',	'Other Items Impacting Current Tax',	'T',	'cons_other_adjust_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	45,	'D',	1,	30,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_other_adjust_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	46,	'F',	6,	31,	1,	'COL_VALUE_NUM',	0,	'other_adjust_total_cm',	'TEXT',	'Other Items Impacting Current Tax',	'T',	'cons_other_adjust_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	47,	'F',	99,	32,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	41,	48,	'F',	99,	33,	1,	'COL_VALUE_TRANS',	0,	'current_tax_cp + other_adjust_total_cm',	'TEXT',	'Total Current Tax With Other Adjustments',	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	1,	'H',	-1,	1,	1,	'TEXT',	1,	'Current Consolidating Report by Month',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	2,	'H',	-1,	2,	1,	'USER_TITLE',	0,	NULL,	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	3,	'H',	-1,	3,	1,	'COL_VALUE_TEXT',	0,	'case_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	4,	'H',	-1,	4,	1,	'COL_VALUE_TEXT',	0,	'consol_desc',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	5,	'H',	-1,	5,	1,	'COL_VALUE_TEXT',	0,	'entity_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	6,	'H',	-1,	6,	1,	'COL_VALUE_TEXT',	0,	'month_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	7,	'H',	-1,	7,	0,	'FILLER',	0,	NULL,	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	8,	'H',	0,	1,	0,	'COL_VALUE_NUM',	0,	'bi_cp',	'TEXT',	'Book Income',	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	9,	'H',	0,	2,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	10,	'H',	3,	2,	0,	'TEXT',	0,	NULL,	'TEXT',	'Tax Items',	'T',	'cons_tax_items_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	11,	'D',	1,	2,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_tax_items_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	12,	'F',	3,	1,	1,	'COL_VALUE_NUM',	0,	'tax_items_total_cm',	'TEXT',	'Tax Items',	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	13,	'F',	3,	2,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	14,	'F',	3,	3,	1,	'COL_VALUE_TRANS',	0,	'bi_cp + tax_items_total_cm',	'TEXT',	'Book Income Before Tax',	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	15,	'F',	3,	4,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	16,	'H',	2,	1,	0,	'TEXT',	0,	NULL,	'COL_VALUE_TEXT',	'rollup_descr',	'T',	'cons_regular_m_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	17,	'D',	1,	1,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_regular_m_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	18,	'F',	2,	2,	1,	'COL_VALUE_NUM',	0,	'm_rollup_total_cm',	'COL_VALUE_TEXT',	'rollup_descr',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	19,	'F',	2,	3,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	20,	'F',	3,	5,	1,	'COL_VALUE_NUM',	0,	'ti_b4_deductions_cp',	'TEXT',	'   Taxable Income Before Deductions',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	21,	'F',	3,	6,	0,	'COL_VALUE_NUM',	0,	'deductions_cp',	'TEXT',	'      Deduction for Fed/Other States',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	22,	'F',	3,	7,	1,	'COL_VALUE_NUM',	0,	'ti_b4_apport_cp',	'TEXT',	'   Taxable Income Before Apportionment',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	23,	'F',	3,	8,	0,	'COL_VALUE_NUM',	0,	'apportionment_month',	'TEXT',	'      Apportionment Factor',	'PA',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	24,	'F',	3,	9,	1,	'COL_VALUE_NUM',	0,	'apport_ti_cp_m',	'TEXT',	'   Taxable Income After Apportioment',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	25,	'H',	4,	10,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	26,	'H',	4,	11,	0,	'TEXT',	0,	NULL,	'TEXT',	'Post Apportionment Items',	'T',	'cons_post_apport_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	27,	'D',	1,	12,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_post_apport_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	28,	'F',	4,	13,	1,	'COL_VALUE_NUM',	0,	'post_apport_total_cm',	'TEXT',	'Post Apportionment Items',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	29,	'F',	4,	14,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	30,	'F',	4,	15,	1,	'COL_VALUE_NUM',	0,	'ti_with_nols_cp_m',	'TEXT',	'   Taxable Income After Other Adjustments',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	31,	'F',	4,	16,	0,	'COL_VALUE_NUM',	0,	'statutory_rate_month',	'TEXT',	'      Statutory Tax Rate',	'P',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	32,	'F',	4,	17,	0,	'COL_VALUE_NUM',	0,	'ct_calc_one_rate_cp_m',	'TEXT',	'   Calculated Tax Before Credits',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	33,	'F',	4,	18,	0,	'COL_VALUE_NUM',	0,	'rate_diff_cp',	'TEXT',	'      Rate Differential',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	34,	'F',	4,	19,	0,	'COL_VALUE_NUM',	0,	'ct_calc_individual_rates_cp_m',	'TEXT',	'   Calculated Tax After Rate Differential',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	35,	'F',	4,	20,	0,	'COL_VALUE_TRANS',	0,	'tax_b4_credits_cp-ct_calc_individual_rates_cp_m',	'TEXT',	'   Current Period True-Up',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	36,	'F',	4,	21,	1,	'COL_VALUE_NUM',	0,	'tax_b4_credits_cp',	'TEXT',	'   Tax Before Credits',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	37,	'H',	5,	22,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	38,	'H',	5,	23,	0,	'TEXT',	0,	NULL,	'TEXT',	'Tax Credits and Adjustments',	'T',	'cons_tax_credit_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	39,	'D',	1,	24,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_tax_credit_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	40,	'F',	5,	25,	1,	'COL_VALUE_NUM',	0,	'tax_credit_total_cm',	'TEXT',	'Tax Credits and Adjustments',	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	41,	'F',	5,	26,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	42,	'F',	5,	27,	1,	'COL_VALUE_NUM',	0,	'current_tax_cp',	'TEXT',	'Current Tax',	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	43,	'H',	6,	28,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_other_adjust_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	44,	'H',	6,	29,	0,	'TEXT',	0,	NULL,	'TEXT',	'Other Items Impacting Current Tax',	'T',	'cons_other_adjust_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	45,	'D',	1,	30,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_other_adjust_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	46,	'F',	6,	31,	1,	'COL_VALUE_NUM',	0,	'other_adjust_total_cm',	'TEXT',	'Other Items Impacting Current Tax',	'T',	'cons_other_adjust_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	47,	'F',	99,	32,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	42,	48,	'F',	99,	33,	1,	'COL_VALUE_TRANS',	0,	'current_tax_cp + other_adjust_total_cm',	'TEXT',	'Total Current Tax With Other Adjustments',	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	1,	'H',	-1,	1,	1,	'TEXT',	1,	'Current Consolidating Report by Month Rollup',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	2,	'H',	-1,	2,	1,	'USER_TITLE',	0,	NULL,	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	3,	'H',	-1,	3,	1,	'COL_VALUE_TEXT',	0,	'case_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	4,	'H',	-1,	4,	1,	'COL_VALUE_TEXT',	0,	'consol_desc',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	5,	'H',	-1,	5,	1,	'COL_VALUE_TEXT',	0,	'entity_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	6,	'H',	-1,	6,	1,	'COL_VALUE_TEXT',	0,	'month_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	7,	'H',	-1,	7,	0,	'FILLER',	0,	NULL,	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	8,	'H',	0,	1,	0,	'COL_VALUE_NUM',	0,	'bi_cp',	'TEXT',	'Book Income',	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	9,	'H',	0,	2,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	10,	'H',	3,	2,	0,	'TEXT',	0,	NULL,	'TEXT',	'Tax Items',	'T',	'cons_tax_items_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	11,	'D',	1,	2,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_tax_items_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	12,	'F',	3,	1,	1,	'COL_VALUE_NUM',	0,	'tax_items_total_cm',	'TEXT',	'Tax Items',	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	13,	'F',	3,	2,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	14,	'F',	3,	3,	1,	'COL_VALUE_TRANS',	0,	'bi_cp + tax_items_total_cm',	'TEXT',	'Book Income Before Tax',	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	15,	'F',	3,	4,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	16,	'H',	2,	1,	0,	'TEXT',	0,	NULL,	'COL_VALUE_TEXT',	'rollup_descr',	'T',	'cons_regular_m_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	17,	'D',	1,	1,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_regular_m_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	18,	'F',	2,	2,	1,	'COL_VALUE_NUM',	0,	'm_rollup_total_cm',	'COL_VALUE_TEXT',	'rollup_descr',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	19,	'F',	2,	3,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	20,	'F',	3,	5,	1,	'COL_VALUE_NUM',	0,	'ti_b4_deductions_cp',	'TEXT',	'   Taxable Income Before Deductions',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	21,	'F',	3,	6,	0,	'COL_VALUE_NUM',	0,	'deductions_cp',	'TEXT',	'      Deduction for Fed/Other States',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	22,	'F',	3,	7,	1,	'COL_VALUE_NUM',	0,	'ti_b4_apport_cp',	'TEXT',	'   Taxable Income Before Apportionment',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	23,	'F',	3,	8,	0,	'COL_VALUE_NUM',	0,	'apportionment_cp',	'TEXT',	'      Apportionment Factor',	'PA',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	24,	'F',	3,	9,	1,	'COL_VALUE_NUM',	0,	'apport_ti_cp',	'TEXT',	'   Taxable Income After Apportioment',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	25,	'H',	4,	10,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	26,	'H',	4,	11,	0,	'TEXT',	0,	NULL,	'TEXT',	'Post Apportionment Items',	'T',	'cons_post_apport_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	27,	'D',	1,	12,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_post_apport_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	28,	'F',	4,	13,	1,	'COL_VALUE_NUM',	0,	'post_apport_total_cm',	'TEXT',	'Post Apportionment Items',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	29,	'F',	4,	14,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	30,	'F',	4,	15,	1,	'COL_VALUE_NUM',	0,	'ti_with_nols_cp',	'TEXT',	'   Taxable Income After Other Adjustments',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	31,	'F',	4,	16,	0,	'COL_VALUE_NUM',	0,	'calc_stat_rate_cp',	'TEXT',	'      Statutory Tax Rate',	'P',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	32,	'F',	4,	17,	0,	'COL_VALUE_NUM',	0,	'ct_calc_one_rate_cp',	'TEXT',	'   Calculated Tax Before Credits',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	33,	'F',	4,	18,	0,	'COL_VALUE_NUM',	0,	'rate_diff_cp',	'TEXT',	'      Rate Differential',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	34,	'F',	4,	19,	0,	'COL_VALUE_NUM',	0,	'ct_calc_individual_rates_cp',	'TEXT',	'   Calculated Tax After Rate Differential',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	35,	'F',	4,	20,	0,	'COL_VALUE_TRANS',	0,	'tax_b4_credits_cp-ct_calc_individual_rates_cp',	'TEXT',	'   Current Period True-Up',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	36,	'F',	4,	21,	1,	'COL_VALUE_NUM',	0,	'tax_b4_credits_cp',	'TEXT',	'   Tax Before Credits',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	37,	'H',	5,	22,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	38,	'H',	5,	23,	0,	'TEXT',	0,	NULL,	'TEXT',	'Tax Credits and Adjustments',	'T',	'cons_tax_credit_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	39,	'D',	1,	24,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_tax_credit_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	40,	'F',	5,	25,	1,	'COL_VALUE_NUM',	0,	'tax_credit_total_cm',	'TEXT',	'Tax Credits and Adjustments',	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	41,	'F',	5,	26,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	42,	'F',	5,	27,	1,	'COL_VALUE_NUM',	0,	'current_tax_cp',	'TEXT',	'Current Tax',	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	43,	'H',	6,	28,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_other_adjust_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	44,	'H',	6,	29,	0,	'TEXT',	0,	NULL,	'TEXT',	'Other Items Impacting Current Tax',	'T',	'cons_other_adjust_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	45,	'D',	1,	30,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_other_adjust_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	46,	'F',	6,	31,	1,	'COL_VALUE_NUM',	0,	'other_adjust_total_cm',	'TEXT',	'Other Items Impacting Current Tax',	'T',	'cons_other_adjust_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	47,	'F',	99,	32,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	43,	48,	'F',	99,	33,	1,	'COL_VALUE_TRANS',	0,	'current_tax_cp + other_adjust_total_cm',	'TEXT',	'Total Current Tax With Other Adjustments',	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	1,	'H',	-1,	1,	1,	'TEXT',	1,	'Current Consolidating Report by Company',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	2,	'H',	-1,	2,	1,	'USER_TITLE',	0,	NULL,	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	3,	'H',	-1,	3,	1,	'COL_VALUE_TEXT',	0,	'case_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	4,	'H',	-1,	4,	1,	'COL_VALUE_TEXT',	0,	'consol_desc',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	5,	'H',	-1,	5,	1,	'COL_VALUE_TEXT',	0,	'entity_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	6,	'H',	-1,	6,	1,	'COL_VALUE_TEXT',	0,	'month_descr',	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	7,	'H',	-1,	7,	0,	'FILLER',	0,	NULL,	NULL,	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	8,	'H',	0,	1,	0,	'COL_VALUE_NUM',	0,	'bi_cp',	'TEXT',	'Book Income',	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	9,	'H',	0,	2,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	10,	'H',	3,	2,	0,	'TEXT',	0,	NULL,	'TEXT',	'Tax Items',	'T',	'cons_tax_items_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	11,	'D',	1,	2,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_tax_items_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	12,	'F',	3,	1,	1,	'COL_VALUE_NUM',	0,	'tax_items_total_cm',	'TEXT',	'Tax Items',	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	13,	'F',	3,	2,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	14,	'F',	3,	3,	1,	'COL_VALUE_TRANS',	0,	'bi_cp + tax_items_total_cm',	'TEXT',	'Book Income Before Tax',	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	15,	'F',	3,	4,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_items_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	16,	'H',	2,	1,	0,	'TEXT',	0,	NULL,	'COL_VALUE_TEXT',	'rollup_descr',	'T',	'cons_regular_m_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	17,	'D',	1,	1,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_regular_m_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	18,	'F',	2,	2,	1,	'COL_VALUE_NUM',	0,	'm_rollup_total_cm',	'COL_VALUE_TEXT',	'rollup_descr',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	19,	'F',	2,	3,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	20,	'F',	3,	5,	1,	'COL_VALUE_NUM',	0,	'ti_b4_deductions_cp',	'TEXT',	'   Taxable Income Before Deductions',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	21,	'F',	3,	6,	0,	'COL_VALUE_NUM',	0,	'deductions_cp',	'TEXT',	'      Deduction for Fed/Other States',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	22,	'F',	3,	7,	1,	'COL_VALUE_NUM',	0,	'ti_b4_apport_cp',	'TEXT',	'   Taxable Income Before Apportionment',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	23,	'F',	3,	8,	0,	'COL_VALUE_NUM',	0,	'apportionment_cp',	'TEXT',	'      Apportionment Factor',	'PA',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	24,	'F',	3,	9,	1,	'COL_VALUE_NUM',	0,	'apport_ti_cp',	'TEXT',	'   Taxable Income After Apportioment',	'T',	'cons_regular_m_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	25,	'H',	4,	10,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	26,	'H',	4,	11,	0,	'TEXT',	0,	NULL,	'TEXT',	'Post Apportionment Items',	'T',	'cons_post_apport_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	27,	'D',	1,	12,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_post_apport_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	28,	'F',	4,	13,	1,	'COL_VALUE_NUM',	0,	'post_apport_total_cm',	'TEXT',	'Post Apportionment Items',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	29,	'F',	4,	14,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	30,	'F',	4,	15,	1,	'COL_VALUE_NUM',	0,	'ti_with_nols_cp',	'TEXT',	'   Taxable Income After Post Apport Adj',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	31,	'F',	4,	16,	0,	'COL_VALUE_NUM',	0,	'calc_stat_rate_cp',	'TEXT',	'      Statutory Tax Rate',	'P',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	32,	'F',	4,	17,	0,	'COL_VALUE_NUM',	0,	'ct_calc_one_rate_cp',	'TEXT',	'   Calculated Tax Before Credits',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	33,	'F',	4,	18,	0,	'COL_VALUE_NUM',	0,	'rate_diff_cp',	'TEXT',	'      Rate Differential',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	34,	'F',	4,	19,	0,	'COL_VALUE_NUM',	0,	'ct_calc_individual_rates_cp',	'TEXT',	'   Calculated Tax After Rate Differential',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	35,	'F',	4,	20,	0,	'COL_VALUE_TRANS',	0,	'tax_b4_credits_cp-ct_calc_individual_rates_cp',	'TEXT',	'   Current Period True-Up',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	36,	'F',	4,	21,	1,	'COL_VALUE_NUM',	0,	'tax_b4_credits_cp',	'TEXT',	'   Tax Before Credits',	'T',	'cons_post_apport_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	37,	'H',	5,	22,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	38,	'H',	5,	23,	0,	'TEXT',	0,	NULL,	'TEXT',	'Tax Credits and Adjustments',	'T',	'cons_tax_credit_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	39,	'D',	1,	24,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_tax_credit_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	40,	'F',	5,	25,	1,	'COL_VALUE_NUM',	0,	'tax_credit_total_cm',	'TEXT',	'Tax Credits and Adjustments',	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	41,	'F',	5,	26,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	42,	'F',	5,	27,	1,	'COL_VALUE_NUM',	0,	'current_tax_cp',	'TEXT',	'Current Tax',	'T',	'cons_tax_credit_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	43,	'H',	6,	28,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	'cons_other_adjust_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	44,	'H',	6,	29,	0,	'TEXT',	0,	NULL,	'TEXT',	'Other Items Impacting Current Tax',	'T',	'cons_other_adjust_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	45,	'D',	1,	30,	0,	'COL_VALUE_NUM',	0,	'm_amount_cm',	'COL_VALUE_TEXT',	'detail_description',	'T',	'cons_other_adjust_visible',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	46,	'F',	6,	31,	1,	'COL_VALUE_NUM',	0,	'other_adjust_total_cm',	'TEXT',	'Other Items Impacting Current Tax',	'T',	'cons_other_adjust_visible_foot',	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	47,	'F',	99,	32,	0,	'TEXT',	0,	NULL,	'FILLER',	NULL,	'T',	NULL,	NULL);

INSERT INTO tax_accrual_rep_cons_rows 
( rep_cons_type_id, row_id, row_type, group_id, rank, color_id, value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname)
VALUES (	44,	48,	'F',	99,	33,	1,	'COL_VALUE_TRANS',	0,	'current_tax_cp + other_adjust_total_cm',	'TEXT',	'Total Current Tax With Other Adjustments',	'T',	NULL,	NULL);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2464, 0, 2015, 1, 0, 0, 41875, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041875_prov_20151_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;