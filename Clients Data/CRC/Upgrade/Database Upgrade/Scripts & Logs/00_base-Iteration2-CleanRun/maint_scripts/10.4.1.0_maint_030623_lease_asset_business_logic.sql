/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030623_lease_asset_business_logic.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/10/2013 Kyle Peterson  Point release
||============================================================================
*/

update LS_ASSET_STATUS
   set DESCRIPTION = 'Initiated', LONG_DESCRIPTION = 'Initiated'
 where LS_ASSET_STATUS_ID = 1;

update LS_ASSET_STATUS
   set DESCRIPTION = 'Pending Approval', LONG_DESCRIPTION = 'Pending Approval'
 where LS_ASSET_STATUS_ID = 2;

update LS_ASSET_STATUS
   set DESCRIPTION = 'In-Service', LONG_DESCRIPTION = 'In-Service'
 where LS_ASSET_STATUS_ID = 3;

update LS_ASSET_STATUS
   set DESCRIPTION = 'Retired', LONG_DESCRIPTION = 'Retired'
 where LS_ASSET_STATUS_ID = 4;

delete from LS_ASSET_STATUS where LS_ASSET_STATUS_ID = 5;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (462, 0, 10, 4, 1, 0, 30623, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030623_lease_asset_business_logic.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
