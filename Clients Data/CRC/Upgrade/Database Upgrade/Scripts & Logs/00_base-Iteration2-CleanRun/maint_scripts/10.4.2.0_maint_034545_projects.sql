/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034545_projects.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/13/2014 Chris Mardis   Point Release
||============================================================================
*/

update POWERPLANT_DDDW
   set DROPDOWN_NAME = 'workflow_type_dw'
 where DROPDOWN_NAME = 'workflow_type';

update POWERPLANT_COLUMNS
   set DROPDOWN_NAME = 'workflow_type_dw'
 where TABLE_NAME = 'work_order_type'
   and COLUMN_NAME = 'approval_type_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (850, 0, 10, 4, 2, 0, 34545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034545_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;