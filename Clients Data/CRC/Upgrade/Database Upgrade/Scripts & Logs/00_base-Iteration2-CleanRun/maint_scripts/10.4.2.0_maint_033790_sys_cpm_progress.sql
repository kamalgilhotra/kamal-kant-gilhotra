/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033790_sys_cpm_progress.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/08/2014 Stephen Motter Point Release
||============================================================================
*/

alter table PP_PROCESSES_OCCURRENCES add PCT_COMPLETE number(22,0);
comment on column PP_PROCESSES_OCCURRENCES.PCT_COMPLETE is 'Number between 1 and 100. Drives the progress bar on the Client Process Monitor.';

alter table PP_JOB_REQUEST
   add (WINDOWS_PROCESS_ID number(22,0),
        PROCESS_MESSAGE    varchar2(4000));
comment on column PP_JOB_REQUEST.WINDOWS_PROCESS_ID is 'Holds the value of the PID that Windows assigns to this process. This information allows the user to kill the process if they wish.';
comment on column PP_JOB_REQUEST.PROCESS_MESSAGE is 'This serves as a return value and a way to display information to the user that is not included in the process logs.';

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE)
   select max(PROCESS_ID) + 1, 'Budget Automatic Processing', 'Budget Automatic Processing', 'budget_one_button.exe'
     from PP_PROCESSES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (854, 0, 10, 4, 2, 0, 33790, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033790_sys_cpm_progress.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;