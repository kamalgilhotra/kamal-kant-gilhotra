/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_051711_lessee_02_purchase_option_payment_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2017.4.0.0 06/07/2018 Sarah Byers      New for Purchase Option Payments
||============================================================================
*/

ALTER TABLE LS_ASSET ADD (PAY_PURCHASE_OPTION   NUMBER(22,0) NULL,
                          ACTUAL_PURCHASE_AMOUNT NUMBER(22,2) NULL);
     
comment on column ls_asset.pay_purchase_option is 'Indicates whether Purchase Option Payment Lines should be generated for a retired asset (1 = Yes, 0 = No).';
comment on column ls_asset.actual_purchase_amount is 'Indicates the dollar amount of the purchase price paid for the asset.';

ALTER TABLE LS_ILR_ACCOUNT ADD (PO_DEBIT_ACCOUNT_ID NUMBER(22,0) NULL);

comment on column ls_asset.pay_purchase_option is 'The Purchase Option Debit Account.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7467, 0, 2017, 4, 0, 0, 51711, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051711_lessee_02_purchase_option_payment_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
