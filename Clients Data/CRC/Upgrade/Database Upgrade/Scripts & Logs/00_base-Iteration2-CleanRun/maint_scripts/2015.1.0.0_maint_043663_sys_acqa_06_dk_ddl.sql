/*
 ||============================================================================
 || Application: acqaider_app
 || File Name: maint_043663_sys_acqa_06_dk_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2015.1.0.0 04/17/2015 Derrick Kemph  <Create Tables for wo estimates, etc import tool for acqaider app>
 ||============================================================================
 */

CREATE TABLE wo_interface_staging_imp_stg (
  import_run_id           NUMBER(22,0)   NOT NULL,
  line_id                 NUMBER(22,0)   NOT NULL,
  error_message           VARCHAR2(4000) NULL,
  time_stamp              DATE           NULL,
  user_id                 VARCHAR2(18)   NULL,
  work_order_id           NUMBER(22,0)   NULL,
  company_id              NUMBER(22,0)   NULL,
  bus_segment_id          NUMBER(22,0)   NULL,
  budget_id               NUMBER(22,0)   NULL,
  budget_version_id       NUMBER(22,0)   NULL,
  work_order_number       VARCHAR2(35)   NOT NULL,
  reason_cd_id            NUMBER(22,0)   NULL,
  work_order_type_id      NUMBER(22,0)   NULL,
  funding_wo_id           NUMBER(22,0)   NULL,
  funding_proj_number     VARCHAR2(35)   NULL,
  major_location_id       NUMBER(22,0)   NULL,
  asset_location_id       NUMBER(22,0)   NULL,
  description             VARCHAR2(35)   NULL,
  long_description        VARCHAR2(2000) NULL,
  funding_wo_indicator    NUMBER(22,0)   NULL,
  est_start_date          VARCHAR2(35)   NULL,
  est_complete_date       VARCHAR2(35)   NULL,
  notes                   VARCHAR2(2000) NULL,
  department_id           NUMBER(22,0)   NULL,
  est_in_service_date     VARCHAR2(35)   NULL,
  work_order_grp_id       NUMBER(22,0)   NULL,
  wo_status_id            NUMBER(22,0)   NULL,
  class_code1             VARCHAR2(254)  NULL,
  class_value1            VARCHAR2(254)  NULL,
  class_code2             VARCHAR2(254)  NULL,
  class_value2            VARCHAR2(254)  NULL,
  class_code3             VARCHAR2(254)  NULL,
  class_value3            VARCHAR2(254)  NULL,
  class_code4             VARCHAR2(254)  NULL,
  class_value4            VARCHAR2(254)  NULL,
  class_code5             VARCHAR2(254)  NULL,
  class_value5            VARCHAR2(254)  NULL,
  class_code6             VARCHAR2(254)  NULL,
  class_value6            VARCHAR2(254)  NULL,
  class_code7             VARCHAR2(254)  NULL,
  class_value7            VARCHAR2(254)  NULL,
  class_code8             VARCHAR2(254)  NULL,
  class_value8            VARCHAR2(254)  NULL,
  class_code9             VARCHAR2(254)  NULL,
  class_value9            VARCHAR2(254)  NULL,
  class_code10            VARCHAR2(254)  NULL,
  class_value10           VARCHAR2(254)  NULL,
  status                  NUMBER(22,0)   NULL,
  action                  VARCHAR2(1)    NULL,
  new_budget              NUMBER(22,0)   NULL,
  in_service_date         VARCHAR2(35)   NULL,
  completion_date         VARCHAR2(35)   NULL,
  initiation_date         VARCHAR2(35)   NULL,
  row_id                  NUMBER(22,0)   NULL,
  reason_for_work         VARCHAR2(2000) NULL,
  close_date              VARCHAR2(35)   NULL,
  engineer                VARCHAR2(18)   NULL,
  engineer_xlate          VARCHAR2(254)  NULL,
  plant_accountant        VARCHAR2(18)   NULL,
  plant_accountant_xlate  VARCHAR2(254)  NULL,
  project_manager         VARCHAR2(18)   NULL,
  project_manager_xlate   VARCHAR2(254)  NULL,
  contract_admin          VARCHAR2(18)   NULL,
  contract_admin_xlate    VARCHAR2(254)  NULL,
  other_contact           VARCHAR2(18)   NULL,
  other_contact_xlate     VARCHAR2(254)  NULL,
  class_code11            VARCHAR2(254)  NULL,
  class_value11           VARCHAR2(254)  NULL,
  class_code12            VARCHAR2(254)  NULL,
  class_value12           VARCHAR2(254)  NULL,
  class_code13            VARCHAR2(254)  NULL,
  class_value13           VARCHAR2(254)  NULL,
  class_code14            VARCHAR2(254)  NULL,
  class_value14           VARCHAR2(254)  NULL,
  class_code15            VARCHAR2(254)  NULL,
  class_value15           VARCHAR2(254)  NULL,
  class_code16            VARCHAR2(254)  NULL,
  class_value16           VARCHAR2(254)  NULL,
  class_code17            VARCHAR2(254)  NULL,
  class_value17           VARCHAR2(254)  NULL,
  class_code18            VARCHAR2(254)  NULL,
  class_value18           VARCHAR2(254)  NULL,
  class_code19            VARCHAR2(254)  NULL,
  class_value19           VARCHAR2(254)  NULL,
  class_code20            VARCHAR2(254)  NULL,
  class_value20           VARCHAR2(254)  NULL,
  class_code21            VARCHAR2(254)  NULL,
  class_value21           VARCHAR2(254)  NULL,
  reimbursable_type_id    NUMBER(22,0)   NULL,
  reimbursable_type_xlate VARCHAR2(254)  NULL,
  wo_approval_group_id    NUMBER(22,0)   NULL,
  suspended_date          VARCHAR2(35)   NULL,
  out_of_service_date     VARCHAR2(35)   NULL,
  initiator               VARCHAR2(18)   NULL,
  future_projects         VARCHAR2(2000) NULL,
  financial_analysis      VARCHAR2(2000) NULL,
  external_wo_number      VARCHAR2(35)   NULL,
  est_annual_rev          NUMBER(22,0)   NULL,
  class_value22           VARCHAR2(254)  NULL,
  class_value23           VARCHAR2(254)  NULL,
  class_value24           VARCHAR2(254)  NULL,
  class_value25           VARCHAR2(254)  NULL,
  class_value26           VARCHAR2(254)  NULL,
  class_value27           VARCHAR2(254)  NULL,
  class_value28           VARCHAR2(254)  NULL,
  class_value29           VARCHAR2(254)  NULL,
  class_value30           VARCHAR2(254)  NULL,
  class_code22            VARCHAR2(254)  NULL,
  class_code23            VARCHAR2(254)  NULL,
  class_code24            VARCHAR2(254)  NULL,
  class_code25            VARCHAR2(254)  NULL,
  class_code26            VARCHAR2(254)  NULL,
  class_code27            VARCHAR2(254)  NULL,
  class_code28            VARCHAR2(254)  NULL,
  class_code29            VARCHAR2(254)  NULL,
  class_code30            VARCHAR2(254)  NULL,
  base_year               NUMBER(22,0)   NULL,
  background              VARCHAR2(2000) NULL,
  alternatives            VARCHAR2(2000) NULL,
  afudc_stop_date         VARCHAR2(35)   NULL,
  afudc_start_date        VARCHAR2(35)   NULL,
  agreement_id            NUMBER(22,0)   NULL,
  batch_id                VARCHAR2(60)   NULL,
  company_xlate           VARCHAR2(254)  NULL,
  work_order_type_xlate   VARCHAR2(254)  NULL,
  major_location_xlate    VARCHAR2(254)  NULL,
  budget_xlate            VARCHAR2(254)  NULL,
  budget_company_id       NUMBER(22,0)   NULL,
  budget_company_xlate    VARCHAR2(254)  NULL,
  budget_version_xlate    VARCHAR2(254)  NULL,
  department_xlate        VARCHAR2(254)  NULL,
  work_order_grp_xlate    VARCHAR2(254)  NULL,
  asset_location_xlate    VARCHAR2(254)  NULL,
  seq_id                  NUMBER(22,0)   DEFAULT 0 NULL,
  old_wo_status_id        NUMBER(22,0)   NULL,
  wo_status_xlate         VARCHAR2(254)  NULL,
  reason_cd_xlate         VARCHAR2(254)  NULL,
  wo_approval_group_xlate VARCHAR2(254)  NULL,
  agreement_xlate         VARCHAR2(254)  NULL,
  bus_segment_xlate       VARCHAR2(254)  NULL,
  auto_approved           NUMBER(22,0)   NULL,
  funding_wo_xlate        VARCHAR2(254)  NULL
)
/

CREATE INDEX wo_interface_stg_imp_stg_per1
  ON wo_interface_staging_imp_stg (
    import_run_id,
    work_order_number
  )
  TABLESPACE pwrplant_idx
/

CREATE INDEX wo_interface_stg_imp_stg_run
  ON wo_interface_staging_imp_stg (
    import_run_id
  )
  TABLESPACE pwrplant_idx
/

ALTER TABLE wo_interface_staging_imp_stg
  ADD CONSTRAINT wo_interface_stg_imp_stg_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

COMMENT ON TABLE WO_INTERFACE_STAGING_IMP_STG        is '(S) [11] The staging table for data from the front end FP/WO Loader.';

COMMENT ON COLUMN wo_interface_staging_imp_stg.error_message IS 'String containing an inclusive list of all errors applicable to a given record during processing of the wo interface API';
COMMENT ON COLUMN wo_interface_staging_imp_stg.time_stamp IS 'Standard System-assigned time stamp used for audit purposes.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.work_order_id IS 'Map work order id on various table, can be left blank the user object will generate the next sequence number';
COMMENT ON COLUMN wo_interface_staging_imp_stg.company_id IS 'The company id that will be mapped into work_order_control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.bus_segment_id IS 'The business segment id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.budget_id IS 'The budget id that will be mapped into work order control for funding projects this can be left blank and the system will auto generate a new budget item';
COMMENT ON COLUMN wo_interface_staging_imp_stg.budget_version_id IS 'The budget version to place this work order or funding project into';
COMMENT ON COLUMN wo_interface_staging_imp_stg.work_order_number IS 'The number of work order or funding project being generated';
COMMENT ON COLUMN wo_interface_staging_imp_stg.reason_cd_id IS 'The reason code id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.work_order_type_id IS 'The work order type that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.funding_wo_id IS 'The id of the funding project being assigned to this work order (used by work orders only)';
COMMENT ON COLUMN wo_interface_staging_imp_stg.funding_proj_number IS 'The number of the funding project being assigned to this work order (used by work orders only)';
COMMENT ON COLUMN wo_interface_staging_imp_stg.major_location_id IS 'The major location id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.asset_location_id IS 'The asset location that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.description IS 'The description that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.long_description IS 'The long description that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.funding_wo_indicator IS 'not ull 1 = funding project 0 = work order';
COMMENT ON COLUMN wo_interface_staging_imp_stg.est_start_date IS 'estimated date that will be mapped into work order control and work order approval';
COMMENT ON COLUMN wo_interface_staging_imp_stg.est_complete_date IS 'estimated date that will be mapped into work order control and work order approval';
COMMENT ON COLUMN wo_interface_staging_imp_stg.notes IS 'The notes that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.department_id IS 'The department id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.est_in_service_date IS 'estimated date that will be mapped into work order control and work order approval';
COMMENT ON COLUMN wo_interface_staging_imp_stg.work_order_grp_id IS 'The work order group id that will be mapped into work order account table';
COMMENT ON COLUMN wo_interface_staging_imp_stg.wo_status_id IS 'The work order status that will be mapped into work order contorl if left blank the user object contains logic based on the dates to derive the status';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code1 IS 'Varchar2(254)  The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value1 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code2 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value2 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code3 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value3 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code4 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value4 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code5 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value5 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code6 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value6 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code7 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value7 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code8 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value8 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code9 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value9 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code10 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value10 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.status IS 'The status of this row in the a negative number indicates an error occured';
COMMENT ON COLUMN wo_interface_staging_imp_stg.action IS 'I = Insert new work order or funding project U = Update existing work order or funding project';
COMMENT ON COLUMN wo_interface_staging_imp_stg.new_budget IS '1 = create new budget item 0 = do not create new budget item, this field is filled in by the user object and should not be populated';
COMMENT ON COLUMN wo_interface_staging_imp_stg.in_service_date IS 'The actual in-service date that will be mapped to work_order_control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.completion_date IS 'The actual complete date that will be mapped to work_order_control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.initiation_date IS 'The date this work order or funding project was created and will be mapped to work_order_initiator. If left blank the system date will be used';
COMMENT ON COLUMN wo_interface_staging_imp_stg.row_id IS 'Unique identifier for this staging table';
COMMENT ON COLUMN wo_interface_staging_imp_stg.reason_for_work IS 'freeform text field that will be mapped into the funding justification table';
COMMENT ON COLUMN wo_interface_staging_imp_stg.close_date IS 'The actual close date that will be mapped into work_order_control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.engineer IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_stg.plant_accountant IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_stg.project_manager IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_stg.contract_admin IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_stg.other_contact IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code11 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value11 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code12 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value12 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code13 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value13 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code14 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value14 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code15 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value15 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code16 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value16 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code17 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value17 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code18 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value18 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code19 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value19 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code20 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value20 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code21 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value21 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.reimbursable_type_id IS 'reimbursable type that will be mapped into work order account';
COMMENT ON COLUMN wo_interface_staging_imp_stg.wo_approval_group_id IS 'work order approval group id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.suspended_date IS 'actual suspended date that will be mapped to work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.out_of_service_date IS 'actual out of service date that will be mapped to work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.initiator IS 'The initiator of this work order or funding project if left blank the current user will be used';
COMMENT ON COLUMN wo_interface_staging_imp_stg.future_projects IS 'freeform text field that will be mapped to the funding_justification table';
COMMENT ON COLUMN wo_interface_staging_imp_stg.financial_analysis IS 'freeform text field that will be mapped to the funding_justification table';
COMMENT ON COLUMN wo_interface_staging_imp_stg.external_wo_number IS 'mapped to external wo number on work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.est_annual_rev IS 'mapped to est annual rev field on work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value22 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value23 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value24 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value25 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value26 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value27 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value28 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value29 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_value30 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code22 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code23 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code24 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code25 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code26 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code27 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code28 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code29 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.class_code30 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_stg.base_year IS 'Year in YYYY format that the dollar for this project will be estimated in';
COMMENT ON COLUMN wo_interface_staging_imp_stg.background IS 'Freeform text field mapped to funding justification';
COMMENT ON COLUMN wo_interface_staging_imp_stg.alternatives IS 'Freeform text field mapped to funding justification';
COMMENT ON COLUMN wo_interface_staging_imp_stg.afudc_stop_date IS 'mapped to afudc stop date on work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.afudc_start_date IS 'mapped to afudc start date on work order control';
COMMENT ON COLUMN wo_interface_staging_imp_stg.agreement_id IS 'mapped to agreement id on work order account';
COMMENT ON COLUMN wo_interface_staging_imp_stg.batch_id IS 'Batch identifier used to segregate records from multiple instances of the job task interface.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.company_xlate IS 'External value used to map to PowerPlan companies.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.work_order_type_xlate IS 'External value used to map to PowerPlan work order types.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.major_location_xlate IS 'External value used to map to PowerPlan major locations.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.budget_xlate IS 'Budget number used to translate to the budget id.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.budget_company_id IS 'Company of the budget number.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.department_xlate IS 'External value used to map to PowerPlan major departments.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.work_order_grp_xlate IS 'External value used to map to PowerPlan major work order groups.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.asset_location_xlate IS 'External value used to map to PowerPlan major asset locations.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.seq_id IS 'Incremental identifier used to indicate which records are the most recent.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.old_wo_status_id IS 'Work order status of already existing work orders or funding projects being processed by the wo interface API';
COMMENT ON COLUMN wo_interface_staging_imp_stg.wo_status_xlate IS 'External value used for mapping values from other systems into values in the work_order_status table.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.reason_cd_xlate IS 'External value used for mapping values from other systems into values in the reason_code table.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.wo_approval_group_xlate IS 'External value used for mapping values from other systems into values in the wo_approval_group table.';
COMMENT ON COLUMN wo_interface_staging_imp_stg.agreement_xlate IS 'External value used for mapping values from other systems into values in the co_tenancy_agreement table.';

CREATE TABLE wo_interface_staging_imp_arc (
  import_run_id           NUMBER(22,0)   NOT NULL,
  line_id                 NUMBER(22,0)   NOT NULL,
  error_message           VARCHAR2(4000) NULL,
  time_stamp              DATE           NULL,
  user_id                 VARCHAR2(18)   NULL,
  work_order_id           NUMBER(22,0)   NULL,
  company_id              NUMBER(22,0)   NULL,
  bus_segment_id          NUMBER(22,0)   NULL,
  budget_id               NUMBER(22,0)   NULL,
  budget_version_id       NUMBER(22,0)   NULL,
  work_order_number       VARCHAR2(35)   NOT NULL,
  reason_cd_id            NUMBER(22,0)   NULL,
  work_order_type_id      NUMBER(22,0)   NULL,
  funding_wo_id           NUMBER(22,0)   NULL,
  funding_proj_number     VARCHAR2(35)   NULL,
  major_location_id       NUMBER(22,0)   NULL,
  asset_location_id       NUMBER(22,0)   NULL,
  description             VARCHAR2(35)   NULL,
  long_description        VARCHAR2(2000) NULL,
  funding_wo_indicator    NUMBER(22,0)   NULL,
  est_start_date          VARCHAR2(35)   NULL,
  est_complete_date       VARCHAR2(35)   NULL,
  notes                   VARCHAR2(2000) NULL,
  department_id           NUMBER(22,0)   NULL,
  est_in_service_date     VARCHAR2(35)   NULL,
  work_order_grp_id       NUMBER(22,0)   NULL,
  wo_status_id            NUMBER(22,0)   NULL,
  class_code1             VARCHAR2(254)  NULL,
  class_value1            VARCHAR2(254)  NULL,
  class_code2             VARCHAR2(254)  NULL,
  class_value2            VARCHAR2(254)  NULL,
  class_code3             VARCHAR2(254)  NULL,
  class_value3            VARCHAR2(254)  NULL,
  class_code4             VARCHAR2(254)  NULL,
  class_value4            VARCHAR2(254)  NULL,
  class_code5             VARCHAR2(254)  NULL,
  class_value5            VARCHAR2(254)  NULL,
  class_code6             VARCHAR2(254)  NULL,
  class_value6            VARCHAR2(254)  NULL,
  class_code7             VARCHAR2(254)  NULL,
  class_value7            VARCHAR2(254)  NULL,
  class_code8             VARCHAR2(254)  NULL,
  class_value8            VARCHAR2(254)  NULL,
  class_code9             VARCHAR2(254)  NULL,
  class_value9            VARCHAR2(254)  NULL,
  class_code10            VARCHAR2(254)  NULL,
  class_value10           VARCHAR2(254)  NULL,
  status                  NUMBER(22,0)   NULL,
  action                  VARCHAR2(1)    NULL,
  new_budget              NUMBER(22,0)   NULL,
  in_service_date         VARCHAR2(35)   NULL,
  completion_date         VARCHAR2(35)   NULL,
  initiation_date         VARCHAR2(35)   NULL,
  row_id                  NUMBER(22,0)   NULL,
  reason_for_work         VARCHAR2(2000) NULL,
  close_date              VARCHAR2(35)   NULL,
  engineer                VARCHAR2(18)   NULL,
  engineer_xlate          VARCHAR2(254)  NULL,
  plant_accountant        VARCHAR2(18)   NULL,
  plant_accountant_xlate  VARCHAR2(254)  NULL,
  project_manager         VARCHAR2(18)   NULL,
  project_manager_xlate   VARCHAR2(254)  NULL,
  contract_admin          VARCHAR2(18)   NULL,
  contract_admin_xlate    VARCHAR2(254)  NULL,
  other_contact           VARCHAR2(18)   NULL,
  other_contact_xlate     VARCHAR2(254)  NULL,
  class_code11            VARCHAR2(254)  NULL,
  class_value11           VARCHAR2(254)  NULL,
  class_code12            VARCHAR2(254)  NULL,
  class_value12           VARCHAR2(254)  NULL,
  class_code13            VARCHAR2(254)  NULL,
  class_value13           VARCHAR2(254)  NULL,
  class_code14            VARCHAR2(254)  NULL,
  class_value14           VARCHAR2(254)  NULL,
  class_code15            VARCHAR2(254)  NULL,
  class_value15           VARCHAR2(254)  NULL,
  class_code16            VARCHAR2(254)  NULL,
  class_value16           VARCHAR2(254)  NULL,
  class_code17            VARCHAR2(254)  NULL,
  class_value17           VARCHAR2(254)  NULL,
  class_code18            VARCHAR2(254)  NULL,
  class_value18           VARCHAR2(254)  NULL,
  class_code19            VARCHAR2(254)  NULL,
  class_value19           VARCHAR2(254)  NULL,
  class_code20            VARCHAR2(254)  NULL,
  class_value20           VARCHAR2(254)  NULL,
  class_code21            VARCHAR2(254)  NULL,
  class_value21           VARCHAR2(254)  NULL,
  reimbursable_type_id    NUMBER(22,0)   NULL,
  reimbursable_type_xlate VARCHAR2(254)  NULL,
  wo_approval_group_id    NUMBER(22,0)   NULL,
  suspended_date          VARCHAR2(35)   NULL,
  out_of_service_date     VARCHAR2(35)   NULL,
  initiator               VARCHAR2(18)   NULL,
  future_projects         VARCHAR2(2000) NULL,
  financial_analysis      VARCHAR2(2000) NULL,
  external_wo_number      VARCHAR2(35)   NULL,
  est_annual_rev          NUMBER(22,0)   NULL,
  class_value22           VARCHAR2(254)  NULL,
  class_value23           VARCHAR2(254)  NULL,
  class_value24           VARCHAR2(254)  NULL,
  class_value25           VARCHAR2(254)  NULL,
  class_value26           VARCHAR2(254)  NULL,
  class_value27           VARCHAR2(254)  NULL,
  class_value28           VARCHAR2(254)  NULL,
  class_value29           VARCHAR2(254)  NULL,
  class_value30           VARCHAR2(254)  NULL,
  class_code22            VARCHAR2(254)  NULL,
  class_code23            VARCHAR2(254)  NULL,
  class_code24            VARCHAR2(254)  NULL,
  class_code25            VARCHAR2(254)  NULL,
  class_code26            VARCHAR2(254)  NULL,
  class_code27            VARCHAR2(254)  NULL,
  class_code28            VARCHAR2(254)  NULL,
  class_code29            VARCHAR2(254)  NULL,
  class_code30            VARCHAR2(254)  NULL,
  base_year               NUMBER(22,0)   NULL,
  background              VARCHAR2(2000) NULL,
  alternatives            VARCHAR2(2000) NULL,
  afudc_stop_date         VARCHAR2(35)   NULL,
  afudc_start_date        VARCHAR2(35)   NULL,
  agreement_id            NUMBER(22,0)   NULL,
  batch_id                VARCHAR2(60)   NULL,
  company_xlate           VARCHAR2(254)  NULL,
  work_order_type_xlate   VARCHAR2(254)  NULL,
  major_location_xlate    VARCHAR2(254)  NULL,
  budget_xlate            VARCHAR2(254)  NULL,
  budget_company_id       NUMBER(22,0)   NULL,
  budget_company_xlate    VARCHAR2(254)  NULL,
  budget_version_xlate    VARCHAR2(254)  NULL,
  department_xlate        VARCHAR2(254)  NULL,
  work_order_grp_xlate    VARCHAR2(254)  NULL,
  asset_location_xlate    VARCHAR2(254)  NULL,
  seq_id                  NUMBER(22,0)   DEFAULT 0 NULL,
  old_wo_status_id        NUMBER(22,0)   NULL,
  wo_status_xlate         VARCHAR2(254)  NULL,
  reason_cd_xlate         VARCHAR2(254)  NULL,
  wo_approval_group_xlate VARCHAR2(254)  NULL,
  agreement_xlate         VARCHAR2(254)  NULL,
  bus_segment_xlate       VARCHAR2(254)  NULL,
  auto_approved           NUMBER(22,0)   NULL,
  funding_wo_xlate        VARCHAR2(254)  NULL
)
/

CREATE INDEX wo_interface_stg_imp_arc_per1
  ON wo_interface_staging_imp_arc (
    import_run_id,
    work_order_number
  )
  TABLESPACE pwrplant_idx
/

CREATE INDEX wo_interface_stg_imp_arc_run
  ON wo_interface_staging_imp_arc (
    import_run_id
  )
  TABLESPACE pwrplant_idx
/

ALTER TABLE wo_interface_staging_imp_arc
  ADD CONSTRAINT wo_interface_stg_imp_arc_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

COMMENT ON TABLE WO_INTERFACE_STAGING_IMP_ARC        is '(S) [11] The archive table for data from the front end FP/WO Loader.';

COMMENT ON COLUMN wo_interface_staging_imp_arc.error_message IS 'String containing an inclusive list of all errors applicable to a given record during processing of the wo interface API';
COMMENT ON COLUMN wo_interface_staging_imp_arc.time_stamp IS 'Standard System-assigned time stamp used for audit purposes.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.work_order_id IS 'Map work order id on various table, can be left blank the user object will generate the next sequence number';
COMMENT ON COLUMN wo_interface_staging_imp_arc.company_id IS 'The company id that will be mapped into work_order_control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.bus_segment_id IS 'The business segment id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.budget_id IS 'The budget id that will be mapped into work order control for funding projects this can be left blank and the system will auto generate a new budget item';
COMMENT ON COLUMN wo_interface_staging_imp_arc.budget_version_id IS 'The budget version to place this work order or funding project into';
COMMENT ON COLUMN wo_interface_staging_imp_arc.work_order_number IS 'The number of work order or funding project being generated';
COMMENT ON COLUMN wo_interface_staging_imp_arc.reason_cd_id IS 'The reason code id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.work_order_type_id IS 'The work order type that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.funding_wo_id IS 'The id of the funding project being assigned to this work order (used by work orders only)';
COMMENT ON COLUMN wo_interface_staging_imp_arc.funding_proj_number IS 'The number of the funding project being assigned to this work order (used by work orders only)';
COMMENT ON COLUMN wo_interface_staging_imp_arc.major_location_id IS 'The major location id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.asset_location_id IS 'The asset location that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.description IS 'The description that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.long_description IS 'The long description that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.funding_wo_indicator IS 'not ull 1 = funding project 0 = work order';
COMMENT ON COLUMN wo_interface_staging_imp_arc.est_start_date IS 'estimated date that will be mapped into work order control and work order approval';
COMMENT ON COLUMN wo_interface_staging_imp_arc.est_complete_date IS 'estimated date that will be mapped into work order control and work order approval';
COMMENT ON COLUMN wo_interface_staging_imp_arc.notes IS 'The notes that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.department_id IS 'The department id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.est_in_service_date IS 'estimated date that will be mapped into work order control and work order approval';
COMMENT ON COLUMN wo_interface_staging_imp_arc.work_order_grp_id IS 'The work order group id that will be mapped into work order account table';
COMMENT ON COLUMN wo_interface_staging_imp_arc.wo_status_id IS 'The work order status that will be mapped into work order contorl if left blank the user object contains logic based on the dates to derive the status';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code1 IS 'Varchar2(254)  The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value1 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code2 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value2 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code3 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value3 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code4 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value4 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code5 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value5 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code6 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value6 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code7 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value7 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code8 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value8 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code9 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value9 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code10 IS 'The name of a class code that will map to a class code id in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value10 IS 'The value of this class code that will map to the value field in work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.status IS 'The status of this row in the a negative number indicates an error occured';
COMMENT ON COLUMN wo_interface_staging_imp_arc.action IS 'I = Insert new work order or funding project U = Update existing work order or funding project';
COMMENT ON COLUMN wo_interface_staging_imp_arc.new_budget IS '1 = create new budget item 0 = do not create new budget item, this field is filled in by the user object and should not be populated';
COMMENT ON COLUMN wo_interface_staging_imp_arc.in_service_date IS 'The actual in-service date that will be mapped to work_order_control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.completion_date IS 'The actual complete date that will be mapped to work_order_control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.initiation_date IS 'The date this work order or funding project was created and will be mapped to work_order_initiator. If left blank the system date will be used';
COMMENT ON COLUMN wo_interface_staging_imp_arc.row_id IS 'Unique identifier for this staging table';
COMMENT ON COLUMN wo_interface_staging_imp_arc.reason_for_work IS 'freeform text field that will be mapped into the funding justification table';
COMMENT ON COLUMN wo_interface_staging_imp_arc.close_date IS 'The actual close date that will be mapped into work_order_control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.engineer IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_arc.plant_accountant IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_arc.project_manager IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_arc.contract_admin IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_arc.other_contact IS 'optional work order or funding project contact that will be mapped into work order initiator';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code11 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value11 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code12 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value12 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code13 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value13 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code14 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value14 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code15 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value15 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code16 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value16 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code17 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value17 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code18 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value18 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code19 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value19 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code20 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value20 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code21 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value21 IS 'The value of this class code that will map to the value field in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.reimbursable_type_id IS 'reimbursable type that will be mapped into work order account';
COMMENT ON COLUMN wo_interface_staging_imp_arc.wo_approval_group_id IS 'work order approval group id that will be mapped into work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.suspended_date IS 'actual suspended date that will be mapped to work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.out_of_service_date IS 'actual out of service date that will be mapped to work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.initiator IS 'The initiator of this work order or funding project if left blank the current user will be used';
COMMENT ON COLUMN wo_interface_staging_imp_arc.future_projects IS 'freeform text field that will be mapped to the funding_justification table';
COMMENT ON COLUMN wo_interface_staging_imp_arc.financial_analysis IS 'freeform text field that will be mapped to the funding_justification table';
COMMENT ON COLUMN wo_interface_staging_imp_arc.external_wo_number IS 'mapped to external wo number on work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.est_annual_rev IS 'mapped to est annual rev field on work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value22 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value23 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value24 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value25 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value26 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value27 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value28 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value29 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_value30 IS 'The value of this class code that will map to the value field in the work order class code.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code22 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code23 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code24 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code25 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code26 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code27 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code28 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code29 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.class_code30 IS 'The name of a class code that will map to a class code id in work order class code';
COMMENT ON COLUMN wo_interface_staging_imp_arc.base_year IS 'Year in YYYY format that the dollar for this project will be estimated in';
COMMENT ON COLUMN wo_interface_staging_imp_arc.background IS 'Freeform text field mapped to funding justification';
COMMENT ON COLUMN wo_interface_staging_imp_arc.alternatives IS 'Freeform text field mapped to funding justification';
COMMENT ON COLUMN wo_interface_staging_imp_arc.afudc_stop_date IS 'mapped to afudc stop date on work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.afudc_start_date IS 'mapped to afudc start date on work order control';
COMMENT ON COLUMN wo_interface_staging_imp_arc.agreement_id IS 'mapped to agreement id on work order account';
COMMENT ON COLUMN wo_interface_staging_imp_arc.batch_id IS 'Batch identifier used to segregate records from multiple instances of the job task interface.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.company_xlate IS 'External value used to map to PowerPlan companies.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.work_order_type_xlate IS 'External value used to map to PowerPlan work order types.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.major_location_xlate IS 'External value used to map to PowerPlan major locations.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.budget_xlate IS 'Budget number used to translate to the budget id.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.budget_company_id IS 'Company of the budget number.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.department_xlate IS 'External value used to map to PowerPlan major departments.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.work_order_grp_xlate IS 'External value used to map to PowerPlan major work order groups.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.asset_location_xlate IS 'External value used to map to PowerPlan major asset locations.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.seq_id IS 'Incremental identifier used to indicate which records are the most recent.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.old_wo_status_id IS 'Work order status of already existing work orders or funding projects being processed by the wo interface API';
COMMENT ON COLUMN wo_interface_staging_imp_arc.wo_status_xlate IS 'External value used for mapping values from other systems into values in the work_order_status table.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.reason_cd_xlate IS 'External value used for mapping values from other systems into values in the reason_code table.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.wo_approval_group_xlate IS 'External value used for mapping values from other systems into values in the wo_approval_group table.';
COMMENT ON COLUMN wo_interface_staging_imp_arc.agreement_xlate IS 'External value used for mapping values from other systems into values in the co_tenancy_agreement table.';

CREATE TABLE job_task_int_stg_imp_stg (
  import_run_id          NUMBER(22,0)   NOT NULL,
  line_id                NUMBER(22,0)   NOT NULL,
  error_message          VARCHAR2(4000) NULL,
  time_stamp             DATE           NULL,
  user_id                VARCHAR2(18)   NULL,
  job_task_id            VARCHAR2(35)   NOT NULL,
  work_order_id          NUMBER(22,0)   NULL,
  work_order_number      VARCHAR2(35)   NULL,
  description            VARCHAR2(35)   NULL,
  long_description       VARCHAR2(254)  NULL,
  est_start_date         VARCHAR2(35)   NULL,
  est_complete_date      VARCHAR2(35)   NULL,
  completion_date        VARCHAR2(35)   NULL,
  status_code_id         NUMBER(22,0)   NULL,
  percent_complete       NUMBER(22,8)   NULL,
  parent_job_task        VARCHAR2(35)   NULL,
  chargeable             NUMBER(22,0)   NULL,
  forecast               NUMBER(22,0)   NULL,
  estimate               NUMBER(22,0)   NULL,
  level_number           NUMBER(22,0)   NULL,
  work_effort            NUMBER(22,2)   NULL,
  field1                 VARCHAR2(35)   NULL,
  field2                 VARCHAR2(35)   NULL,
  field3                 VARCHAR2(35)   NULL,
  field4                 VARCHAR2(35)   NULL,
  field5                 VARCHAR2(35)   NULL,
  priority_code_id       NUMBER(22,0)   NULL,
  external_job_task      VARCHAR2(35)   NULL,
  job_task_status_id     NUMBER(22,0)   NULL,
  system_id              NUMBER(22,0)   NULL,
  group_id               NUMBER(22,0)   NULL,
  initiator              VARCHAR2(18)   NULL,
  task_owner             VARCHAR2(18)   NULL,
  budget_analyst         VARCHAR2(18)   NULL,
  project_manager        VARCHAR2(18)   NULL,
  engineer               VARCHAR2(18)   NULL,
  other_contact          VARCHAR2(18)   NULL,
  notes                  VARCHAR2(4000) NULL,
  class_code1            VARCHAR2(254)  NULL,
  class_value1           VARCHAR2(254)  NULL,
  class_code2            VARCHAR2(254)  NULL,
  class_value2           VARCHAR2(254)  NULL,
  class_code3            VARCHAR2(254)  NULL,
  class_value3           VARCHAR2(254)  NULL,
  class_code4            VARCHAR2(254)  NULL,
  class_value4           VARCHAR2(254)  NULL,
  class_code5            VARCHAR2(254)  NULL,
  class_value5           VARCHAR2(254)  NULL,
  class_code6            VARCHAR2(254)  NULL,
  class_value6           VARCHAR2(254)  NULL,
  class_code7            VARCHAR2(254)  NULL,
  class_value7           VARCHAR2(254)  NULL,
  class_code8            VARCHAR2(254)  NULL,
  class_value8           VARCHAR2(254)  NULL,
  class_code9            VARCHAR2(254)  NULL,
  class_value9           VARCHAR2(254)  NULL,
  class_code10           VARCHAR2(254)  NULL,
  class_value10          VARCHAR2(254)  NULL,
  class_code11           VARCHAR2(254)  NULL,
  class_value11          VARCHAR2(254)  NULL,
  class_code12           VARCHAR2(254)  NULL,
  class_value12          VARCHAR2(254)  NULL,
  class_code13           VARCHAR2(254)  NULL,
  class_value13          VARCHAR2(254)  NULL,
  class_code14           VARCHAR2(254)  NULL,
  class_value14          VARCHAR2(254)  NULL,
  class_code15           VARCHAR2(254)  NULL,
  class_value15          VARCHAR2(254)  NULL,
  class_code16           VARCHAR2(254)  NULL,
  class_value16          VARCHAR2(254)  NULL,
  class_code17           VARCHAR2(254)  NULL,
  class_value17          VARCHAR2(254)  NULL,
  class_code18           VARCHAR2(254)  NULL,
  class_value18          VARCHAR2(254)  NULL,
  class_code19           VARCHAR2(254)  NULL,
  class_value19          VARCHAR2(254)  NULL,
  class_code20           VARCHAR2(254)  NULL,
  class_value20          VARCHAR2(254)  NULL,
  setup_template         VARCHAR2(35)   NULL,
  setup_template_id      NUMBER(22,0)   NULL,
  status                 NUMBER(22,0)   NULL,
  action                 VARCHAR2(1)    NULL,
  row_id                 NUMBER(22,0)   NULL,
  priority_code_descr    VARCHAR2(35)   NULL,
  system_descr           VARCHAR2(35)   NULL,
  group_descr            VARCHAR2(35)   NULL,
  job_task_status_descr  VARCHAR2(35)   NULL,
  company_id             NUMBER(22,0)   NULL,
  funding_task_indicator NUMBER(22,0)   NULL,
  reason_for_work        VARCHAR2(4000) NULL,
  background             VARCHAR2(4000) NULL,
  future_projects        VARCHAR2(4000) NULL,
  alternatives           VARCHAR2(4000) NULL,
  financial_analysis     VARCHAR2(4000) NULL,
  batch_id               VARCHAR2(60)   NULL,
  seq_id                 NUMBER(22,0)   DEFAULT 0 NULL,
  company_xlate          VARCHAR2(254)  NULL,
  priority_code_xlate    VARCHAR2(254)  NULL,
  job_task_status_xlate  VARCHAR2(254)  NULL,
  setup_template_xlate   VARCHAR2(254)  NULL,
  work_order_xlate       VARCHAR2(254)  NULL,
  task_owner_xlate       VARCHAR2(254)  NULL,
  budget_analyst_xlate   VARCHAR2(254)  NULL,
  project_manager_xlate  VARCHAR2(254)  NULL,
  engineer_xlate         VARCHAR2(254)  NULL,
  other_contact_xlate    VARCHAR2(254)  NULL,
  status_code_xlate      VARCHAR2(254)  NULL,
  chargeable_xlate       VARCHAR2(254)  NULL,
  forecast_xlate         VARCHAR2(254)  NULL,
  estimate_xlate         VARCHAR2(254)  NULL,
  parent_job_task_xlate  VARCHAR2(254)  NULL
)
/

CREATE INDEX job_task_int_stg_imp_stg_run
  ON job_task_int_stg_imp_stg (
    import_run_id
  )
  TABLESPACE pwrplant_idx
/

ALTER TABLE job_task_int_stg_imp_stg
  ADD CONSTRAINT job_task_int_stg_imp_stg_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

COMMENT ON TABLE job_task_int_stg_imp_stg IS '(C)  [02]
The Job Task Interface Staging Import table is an staging table in the Project Management Loader to load data to the API tables through the front end.';

COMMENT ON COLUMN job_task_int_stg_imp_stg.error_message IS 'Error message encountered while importing/translating/loading this row of data during the import process.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.job_task_id IS 'Identifier of a particular job task.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.work_order_id IS 'System-assigned identifier of a particular work order.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.work_order_number IS 'User defined number or name of a work order will be translated into a work order id by the API';
COMMENT ON COLUMN job_task_int_stg_imp_stg.description IS 'Records a short description of the job task.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.long_description IS 'Records an optional detailed description of the job task.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.est_start_date IS 'Estimated start date of task';
COMMENT ON COLUMN job_task_int_stg_imp_stg.est_complete_date IS 'Estimated completion date of the task';
COMMENT ON COLUMN job_task_int_stg_imp_stg.completion_date IS 'Actual Complete date of a task';
COMMENT ON COLUMN job_task_int_stg_imp_stg.status_code_id IS 'Status: ''0'' = Active, ''1'' = Inactive.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.percent_complete IS 'Percentage of task complete, entered as a decimal.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.parent_job_task IS 'number Pointer to parent task on this table.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.chargeable IS 'Yes / No, denoting whether this task is chargeable, e.g. a parent might not be chargeable.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.forecast IS 'Yes / No field denoting whether this task can be used for forecasting purposes';
COMMENT ON COLUMN job_task_int_stg_imp_stg.estimate IS 'Yes / No field denoting whether this task can be used for budgeting purposes';
COMMENT ON COLUMN job_task_int_stg_imp_stg.level_number IS 'Tree or tier level used in displaying or reporting tasks. Level 1 is left most (highest). The other levels (e.g. 2, 3, 4, etc.) are indented or sublevels.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.work_effort IS 'Optional quantity field related to the task, such as hours, units, etc.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.field1 IS 'field 1 User defined field, e.g. class code, list from a standard table, CR master element, etc.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.field2 IS 'field 2 Additional Optional field (see field 1).';
COMMENT ON COLUMN job_task_int_stg_imp_stg.field3 IS 'field 3 Additional Optional field (see field 1).';
COMMENT ON COLUMN job_task_int_stg_imp_stg.field4 IS 'field 4 Additional Optional field (see field 1).';
COMMENT ON COLUMN job_task_int_stg_imp_stg.field5 IS 'field 5 Additional Optional field (see field 1).';
COMMENT ON COLUMN job_task_int_stg_imp_stg.priority_code_id IS 'System-assigned identifier of a user-defined Job Task Priority';
COMMENT ON COLUMN job_task_int_stg_imp_stg.external_job_task IS 'Optional code for linking the job task to a job task in an external system.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.job_task_status_id IS 'System-assigned identifier of a user-defined Job Task Status.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.system_id IS 'System-assigned identifier of a user-defined system.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.group_id IS 'System-assigned identifier of a user defined job task group.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.initiator IS 'Optional field for recording the initiator of the job task.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.task_owner IS 'Optional field for recording the individual responsible for the task.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.project_manager IS 'Optional field for recording the budget analyst responsible for the task.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.engineer IS 'Optional field for recording another contact individual.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.other_contact IS 'Optional field for recording another contact individual.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.notes IS 'Optional notes, explanation or documentation field.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code1 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value1 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code2 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value2 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code3 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value3 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code4 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value4 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code5 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value5 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code6 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value6 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code7 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value7 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code8 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value8 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code9 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value9 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code10 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value10 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code11 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value11 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code12 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value12 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code13 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value13 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code14 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value14 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code15 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value15 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code16 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value16 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code17 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value17 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code18 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value18 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code19 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value19 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_code20 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.class_value20 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.setup_template IS 'Description of the setup template can be filled in instead of setup_template_id the API will translate this to the setup template id';
COMMENT ON COLUMN job_task_int_stg_imp_stg.setup_template_id IS 'System assigned identifier of a job task setup template';
COMMENT ON COLUMN job_task_int_stg_imp_stg.status IS 'identifies records in this table that have encountered an error and will not be loaded';
COMMENT ON COLUMN job_task_int_stg_imp_stg.action IS 'I = Insert new Job Task U = Update existing job task';
COMMENT ON COLUMN job_task_int_stg_imp_stg.row_id IS 'Not used';
COMMENT ON COLUMN job_task_int_stg_imp_stg.priority_code_descr IS 'Description of the Priority Code can be filled in instead of priority_code_id the API will translate this to the priority_code_id';
COMMENT ON COLUMN job_task_int_stg_imp_stg.system_descr IS 'Description of the System can be filled in instead of system_id the API will translate this to the system_id';
COMMENT ON COLUMN job_task_int_stg_imp_stg.group_descr IS 'Description of the Job Task Group can be filled in instead of group_id the API will translate this to the group_id';
COMMENT ON COLUMN job_task_int_stg_imp_stg.job_task_status_descr IS 'Description of the Job Task Status can be filled in instead of job_task_status_id the API will translate this to the job_task_status_id';
COMMENT ON COLUMN job_task_int_stg_imp_stg.company_id IS 'Company id of the work order in the staging table, this field is required is filling in a work order number rather then a work order id';
COMMENT ON COLUMN job_task_int_stg_imp_stg.funding_task_indicator IS 'not used';
COMMENT ON COLUMN job_task_int_stg_imp_stg.reason_for_work IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.background IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.future_projects IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.alternatives IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.financial_analysis IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.batch_id IS 'Batch identifier used to segregate records from multiple instances of the job task interface.';
COMMENT ON COLUMN job_task_int_stg_imp_stg.seq_id IS 'Incremental identifier used to indicate which records are the most recent.';

CREATE TABLE job_task_int_stg_imp_arc (
  import_run_id          NUMBER(22,0)   NOT NULL,
  line_id                NUMBER(22,0)   NOT NULL,
  error_message          VARCHAR2(4000) NULL,
  time_stamp             DATE           NULL,
  user_id                VARCHAR2(18)   NULL,
  job_task_id            VARCHAR2(35)   NOT NULL,
  work_order_id          NUMBER(22,0)   NULL,
  work_order_number      VARCHAR2(35)   NULL,
  description            VARCHAR2(35)   NULL,
  long_description       VARCHAR2(254)  NULL,
  est_start_date         VARCHAR2(35)   NULL,
  est_complete_date      VARCHAR2(35)   NULL,
  completion_date        VARCHAR2(35)   NULL,
  status_code_id         NUMBER(22,0)   NULL,
  percent_complete       NUMBER(22,8)   NULL,
  parent_job_task        VARCHAR2(35)   NULL,
  chargeable             NUMBER(22,0)   NULL,
  forecast               NUMBER(22,0)   NULL,
  estimate               NUMBER(22,0)   NULL,
  level_number           NUMBER(22,0)   NULL,
  work_effort            NUMBER(22,2)   NULL,
  field1                 VARCHAR2(35)   NULL,
  field2                 VARCHAR2(35)   NULL,
  field3                 VARCHAR2(35)   NULL,
  field4                 VARCHAR2(35)   NULL,
  field5                 VARCHAR2(35)   NULL,
  priority_code_id       NUMBER(22,0)   NULL,
  external_job_task      VARCHAR2(35)   NULL,
  job_task_status_id     NUMBER(22,0)   NULL,
  system_id              NUMBER(22,0)   NULL,
  group_id               NUMBER(22,0)   NULL,
  initiator              VARCHAR2(18)   NULL,
  task_owner             VARCHAR2(18)   NULL,
  budget_analyst         VARCHAR2(18)   NULL,
  project_manager        VARCHAR2(18)   NULL,
  engineer               VARCHAR2(18)   NULL,
  other_contact          VARCHAR2(18)   NULL,
  notes                  VARCHAR2(4000) NULL,
  class_code1            VARCHAR2(254)  NULL,
  class_value1           VARCHAR2(254)  NULL,
  class_code2            VARCHAR2(254)  NULL,
  class_value2           VARCHAR2(254)  NULL,
  class_code3            VARCHAR2(254)  NULL,
  class_value3           VARCHAR2(254)  NULL,
  class_code4            VARCHAR2(254)  NULL,
  class_value4           VARCHAR2(254)  NULL,
  class_code5            VARCHAR2(254)  NULL,
  class_value5           VARCHAR2(254)  NULL,
  class_code6            VARCHAR2(254)  NULL,
  class_value6           VARCHAR2(254)  NULL,
  class_code7            VARCHAR2(254)  NULL,
  class_value7           VARCHAR2(254)  NULL,
  class_code8            VARCHAR2(254)  NULL,
  class_value8           VARCHAR2(254)  NULL,
  class_code9            VARCHAR2(254)  NULL,
  class_value9           VARCHAR2(254)  NULL,
  class_code10           VARCHAR2(254)  NULL,
  class_value10          VARCHAR2(254)  NULL,
  class_code11           VARCHAR2(254)  NULL,
  class_value11          VARCHAR2(254)  NULL,
  class_code12           VARCHAR2(254)  NULL,
  class_value12          VARCHAR2(254)  NULL,
  class_code13           VARCHAR2(254)  NULL,
  class_value13          VARCHAR2(254)  NULL,
  class_code14           VARCHAR2(254)  NULL,
  class_value14          VARCHAR2(254)  NULL,
  class_code15           VARCHAR2(254)  NULL,
  class_value15          VARCHAR2(254)  NULL,
  class_code16           VARCHAR2(254)  NULL,
  class_value16          VARCHAR2(254)  NULL,
  class_code17           VARCHAR2(254)  NULL,
  class_value17          VARCHAR2(254)  NULL,
  class_code18           VARCHAR2(254)  NULL,
  class_value18          VARCHAR2(254)  NULL,
  class_code19           VARCHAR2(254)  NULL,
  class_value19          VARCHAR2(254)  NULL,
  class_code20           VARCHAR2(254)  NULL,
  class_value20          VARCHAR2(254)  NULL,
  setup_template         VARCHAR2(35)   NULL,
  setup_template_id      NUMBER(22,0)   NULL,
  status                 NUMBER(22,0)   NULL,
  action                 VARCHAR2(1)    NULL,
  row_id                 NUMBER(22,0)   NULL,
  priority_code_descr    VARCHAR2(35)   NULL,
  system_descr           VARCHAR2(35)   NULL,
  group_descr            VARCHAR2(35)   NULL,
  job_task_status_descr  VARCHAR2(35)   NULL,
  company_id             NUMBER(22,0)   NULL,
  funding_task_indicator NUMBER(22,0)   NULL,
  reason_for_work        VARCHAR2(4000) NULL,
  background             VARCHAR2(4000) NULL,
  future_projects        VARCHAR2(4000) NULL,
  alternatives           VARCHAR2(4000) NULL,
  financial_analysis     VARCHAR2(4000) NULL,
  batch_id               VARCHAR2(60)   NULL,
  seq_id                 NUMBER(22,0)   DEFAULT 0 NULL,
  company_xlate          VARCHAR2(254)  NULL,
  priority_code_xlate    VARCHAR2(254)  NULL,
  job_task_status_xlate  VARCHAR2(254)  NULL,
  setup_template_xlate   VARCHAR2(254)  NULL,
  work_order_xlate       VARCHAR2(254)  NULL,
  task_owner_xlate       VARCHAR2(254)  NULL,
  budget_analyst_xlate   VARCHAR2(254)  NULL,
  project_manager_xlate  VARCHAR2(254)  NULL,
  engineer_xlate         VARCHAR2(254)  NULL,
  other_contact_xlate    VARCHAR2(254)  NULL,
  status_code_xlate      VARCHAR2(254)  NULL,
  chargeable_xlate       VARCHAR2(254)  NULL,
  forecast_xlate         VARCHAR2(254)  NULL,
  estimate_xlate         VARCHAR2(254)  NULL,
  parent_job_task_xlate  VARCHAR2(254)  NULL
)
/

CREATE INDEX job_task_int_stg_imp_arc_run
  ON job_task_int_stg_imp_arc (
    import_run_id
  )
  TABLESPACE pwrplant_idx
/

ALTER TABLE job_task_int_stg_imp_arc
  ADD CONSTRAINT job_task_int_stg_imp_arc_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/


COMMENT ON TABLE job_task_int_stg_imp_arc IS '(C)  [02]
The Job Task Interface Staging Import table is an staging table in the Project Management Loader to load data to the API tables through the front end.';

COMMENT ON COLUMN job_task_int_stg_imp_arc.error_message IS 'Error message encountered while importing/translating/loading this row of data during the import process.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.job_task_id IS 'Identifier of a particular job task.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.work_order_id IS 'System-assigned identifier of a particular work order.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.work_order_number IS 'User defined number or name of a work order will be translated into a work order id by the API';
COMMENT ON COLUMN job_task_int_stg_imp_arc.description IS 'Records a short description of the job task.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.long_description IS 'Records an optional detailed description of the job task.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.est_start_date IS 'Estimated start date of task';
COMMENT ON COLUMN job_task_int_stg_imp_arc.est_complete_date IS 'Estimated completion date of the task';
COMMENT ON COLUMN job_task_int_stg_imp_arc.completion_date IS 'Actual Complete date of a task';
COMMENT ON COLUMN job_task_int_stg_imp_arc.status_code_id IS 'Status: ''0'' = Active, ''1'' = Inactive.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.percent_complete IS 'Percentage of task complete, entered as a decimal.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.parent_job_task IS 'number Pointer to parent task on this table.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.chargeable IS 'Yes / No, denoting whether this task is chargeable, e.g. a parent might not be chargeable.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.forecast IS 'Yes / No field denoting whether this task can be used for forecasting purposes';
COMMENT ON COLUMN job_task_int_stg_imp_arc.estimate IS 'Yes / No field denoting whether this task can be used for budgeting purposes';
COMMENT ON COLUMN job_task_int_stg_imp_arc.level_number IS 'Tree or tier level used in displaying or reporting tasks. Level 1 is left most (highest). The other levels (e.g. 2, 3, 4, etc.) are indented or sublevels.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.work_effort IS 'Optional quantity field related to the task, such as hours, units, etc.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.field1 IS 'field 1 User defined field, e.g. class code, list from a standard table, CR master element, etc.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.field2 IS 'field 2 Additional Optional field (see field 1).';
COMMENT ON COLUMN job_task_int_stg_imp_arc.field3 IS 'field 3 Additional Optional field (see field 1).';
COMMENT ON COLUMN job_task_int_stg_imp_arc.field4 IS 'field 4 Additional Optional field (see field 1).';
COMMENT ON COLUMN job_task_int_stg_imp_arc.field5 IS 'field 5 Additional Optional field (see field 1).';
COMMENT ON COLUMN job_task_int_stg_imp_arc.priority_code_id IS 'System-assigned identifier of a user-defined Job Task Priority';
COMMENT ON COLUMN job_task_int_stg_imp_arc.external_job_task IS 'Optional code for linking the job task to a job task in an external system.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.job_task_status_id IS 'System-assigned identifier of a user-defined Job Task Status.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.system_id IS 'System-assigned identifier of a user-defined system.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.group_id IS 'System-assigned identifier of a user defined job task group.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.initiator IS 'Optional field for recording the initiator of the job task.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.task_owner IS 'Optional field for recording the individual responsible for the task.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.project_manager IS 'Optional field for recording the budget analyst responsible for the task.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.engineer IS 'Optional field for recording another contact individual.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.other_contact IS 'Optional field for recording another contact individual.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.notes IS 'Optional notes, explanation or documentation field.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code1 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value1 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code2 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value2 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code3 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value3 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code4 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value4 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code5 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value5 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code6 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value6 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code7 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value7 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code8 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value8 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code9 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value9 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code10 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value10 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code11 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value11 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code12 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value12 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code13 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value13 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code14 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value14 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code15 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value15 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code16 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value16 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code17 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value17 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code18 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value18 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code19 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value19 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_code20 IS 'Job Task Class Code Name.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.class_value20 IS 'Job Task Class Code Value.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.setup_template IS 'Description of the setup template can be filled in instead of setup_template_id the API will translate this to the setup template id';
COMMENT ON COLUMN job_task_int_stg_imp_arc.setup_template_id IS 'System assigned identifier of a job task setup template';
COMMENT ON COLUMN job_task_int_stg_imp_arc.status IS 'identifies records in this table that have encountered an error and will not be loaded';
COMMENT ON COLUMN job_task_int_stg_imp_arc.action IS 'I = Insert new Job Task U = Update existing job task';
COMMENT ON COLUMN job_task_int_stg_imp_arc.row_id IS 'Not used';
COMMENT ON COLUMN job_task_int_stg_imp_arc.priority_code_descr IS 'Description of the Priority Code can be filled in instead of priority_code_id the API will translate this to the priority_code_id';
COMMENT ON COLUMN job_task_int_stg_imp_arc.system_descr IS 'Description of the System can be filled in instead of system_id the API will translate this to the system_id';
COMMENT ON COLUMN job_task_int_stg_imp_arc.group_descr IS 'Description of the Job Task Group can be filled in instead of group_id the API will translate this to the group_id';
COMMENT ON COLUMN job_task_int_stg_imp_arc.job_task_status_descr IS 'Description of the Job Task Status can be filled in instead of job_task_status_id the API will translate this to the job_task_status_id';
COMMENT ON COLUMN job_task_int_stg_imp_arc.company_id IS 'Company id of the work order in the staging table, this field is required is filling in a work order number rather then a work order id';
COMMENT ON COLUMN job_task_int_stg_imp_arc.funding_task_indicator IS 'not used';
COMMENT ON COLUMN job_task_int_stg_imp_arc.reason_for_work IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.background IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.future_projects IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.alternatives IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.financial_analysis IS 'Optional free form descriptive field.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.batch_id IS 'Batch identifier used to segregate records from multiple instances of the job task interface.';
COMMENT ON COLUMN job_task_int_stg_imp_arc.seq_id IS 'Incremental identifier used to indicate which records are the most recent.';


CREATE TABLE wo_interface_unit_imp_stg (
  import_run_id           NUMBER(22,0)   NOT NULL,
  line_id                 NUMBER(22,0)   NOT NULL,
  error_message           VARCHAR2(4000) NULL,
  time_stamp              DATE           NULL,
  user_id                 VARCHAR2(18)   NULL,
  work_order_number       VARCHAR2(35)   NOT NULL,
  estimate_id             NUMBER(22,0)   NULL,
  revision                NUMBER(22,0)   NULL,
  work_order_id           NUMBER(22,0)   NULL,
  expenditure_type_id     NUMBER(22,0)   NULL,
  job_task_id             VARCHAR2(35)   NULL,
  utility_account_id      NUMBER(22,0)   NULL,
  est_chg_type_id         NUMBER(22,0)   NULL,
  sub_account_id          NUMBER(22,0)   NULL,
  stck_keep_unit_id       NUMBER(22,0)   NULL,
  retirement_unit_id      NUMBER(22,0)   NULL,
  bus_segment_id          NUMBER(22,0)   NULL,
  asset_id                NUMBER(22,0)   NULL,
  quantity                NUMBER(22,2)   NULL,
  hours                   NUMBER(22,2)   NULL,
  amount                  NUMBER(22,2)   NULL,
  notes                   VARCHAR2(254)  NULL,
  department_id           NUMBER(22,0)   NULL,
  property_group_id       NUMBER(22,0)   NULL,
  asset_location_id       NUMBER(22,0)   NULL,
  replacement_amount      NUMBER(22,2)   NULL,
  serial_number           VARCHAR2(35)   NULL,
  wo_est_trans_type_id    NUMBER(22,0)   NULL,
  est_in_service_date     DATE           NULL,
  status                  NUMBER(22,0)   NULL,
  retire_vintage          NUMBER(22,0)   NULL,
  unit_desc               VARCHAR2(35)   NULL,
  unit_long_desc          VARCHAR2(254)  NULL,
  percent                 NUMBER(22,8)   NULL,
  field_1                 VARCHAR2(35)   NULL,
  field_2                 VARCHAR2(35)   NULL,
  field_3                 VARCHAR2(35)   NULL,
  field_4                 VARCHAR2(35)   NULL,
  field_5                 VARCHAR2(35)   NULL,
  company_id              NUMBER(22,0)   NULL,
  batch_id                VARCHAR2(60)   NULL,
  company_xlate           VARCHAR2(35)   NULL,
  funding_wo_indicator    NUMBER(22,0)   NULL,
  expenditure_type_xlate  VARCHAR2(254)  NULL,
  est_chg_type_xlate      VARCHAR2(254)  NULL,
  stck_keep_unit_xlate    VARCHAR2(254)  NULL,
  wo_est_trans_type_xlate VARCHAR2(35)   NULL,
  department_xlate        VARCHAR2(254)  NULL,
  job_task_xlate          VARCHAR2(254)  NULL,
  bus_segment_xlate       VARCHAR2(254)  NULL,
  utility_account_xlate   VARCHAR2(254)  NULL,
  sub_account_xlate       VARCHAR2(254)  NULL,
  property_group_xlate    VARCHAR2(254)  NULL,
  retirement_unit_xlate   VARCHAR2(254)  NULL,
  asset_location_xlate    VARCHAR2(254)  NULL,
  asset_id_xlate          VARCHAR2(254)  NULL,
  process_level           VARCHAR2(35)   NULL,
  seq_id                  NUMBER(22,0)   DEFAULT 0 NULL,
  row_id                  NUMBER(22,0)   NULL,
  replace_estimates       VARCHAR2(35)   NULL
)
/

ALTER TABLE wo_interface_unit_imp_stg
  ADD CONSTRAINT wo_interface_unit_imp_stg_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

COMMENT ON TABLE wo_interface_unit_imp_stg IS '(C)  [13]
The WO Interface Unit Import Staging table is an table that loads date into the API for unit and as-built estimates.';

COMMENT ON COLUMN wo_interface_unit_imp_stg.error_message IS 'String containing an inclusive list of all errors applicable to a given record during processing of the wo unit estimate interface API.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.time_stamp IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.work_order_number IS 'Work order number.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.estimate_id IS 'Estimate row number.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.revision IS 'Revision of estimate for this work order.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.work_order_id IS 'Unique identifier of a work order in PowerPlan.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.expenditure_type_id IS 'One of the four expenditure types (Add, retire, etc).';
COMMENT ON COLUMN wo_interface_unit_imp_stg.job_task_id IS 'Optional job task identifier associated with the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.utility_account_id IS 'Utility (plant) account id.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.est_chg_type_id IS 'Estimate charge type (e.g. labor).';
COMMENT ON COLUMN wo_interface_unit_imp_stg.sub_account_id IS 'Sub account of the plant account.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.stck_keep_unit_id IS 'Stock keeping unit for a material charge.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.retirement_unit_id IS 'Retirement unit of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.bus_segment_id IS 'Business egment of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.asset_id IS 'Asset id of the estimate (e.g. if a retirement).';
COMMENT ON COLUMN wo_interface_unit_imp_stg.quantity IS 'Quanitfy of the retirement unit (estimate).';
COMMENT ON COLUMN wo_interface_unit_imp_stg.hours IS 'Hours of a labor estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.amount IS 'Dollar amount of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.notes IS 'Optional notes fields.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.department_id IS 'Department of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.property_group_id IS 'Property group of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.asset_location_id IS 'Asset location of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.replacement_amount IS 'Replacement dollar amount for a retirement if using HW indexed retirements.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.serial_number IS 'Serial number of the equipment.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.wo_est_trans_type_id IS 'Identifier of work order estimate transaction type.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.est_in_service_date IS 'Estimated in-service (out of service) date.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.status IS 'Status of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.retire_vintage IS 'Used to identify specific vintage retirements for mass property.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.unit_desc IS 'Unit Desc to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.unit_long_desc IS 'Unit Long Desc to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.percent IS 'Percent to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.field_1 IS 'Field 1 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.field_2 IS 'Field 2 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.field_3 IS 'Field 3 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.field_4 IS 'Field 4 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.field_5 IS 'Field 5 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.company_id IS 'Company of the work order being loaded.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.batch_id IS 'Batch identifier used to segregate records from multiple instances of the job task interface.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.company_xlate IS 'External value used to map to PowerPlan companies.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.funding_wo_indicator IS '1 or 0 to indicate whether estimates are being loaded to work orders or funding projects.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.expenditure_type_xlate IS 'External value used to map to PowerPlan expenditure types.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.est_chg_type_xlate IS 'External value used to map to PowerPlan est charge types.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.stck_keep_unit_xlate IS 'External value used to map to PowerPlan stock keeping units.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.wo_est_trans_type_xlate IS 'External value used to map to PowerPlan wo est trans type.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.department_xlate IS 'External value used to map to PowerPlan department.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.job_task_xlate IS 'External value used to map to PowerPlan job task.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.bus_segment_xlate IS 'External value used to map to PowerPlan business segment.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.utility_account_xlate IS 'External value used to map to PowerPlan utility account.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.sub_account_xlate IS 'External value used to map to PowerPlan sub account.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.property_group_xlate IS 'External value used to map to PowerPlan property group.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.retirement_unit_xlate IS 'External value used to map to PowerPlan retirement unit.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.asset_location_xlate IS 'External value used to map to PowerPlan asset location.';
COMMENT ON COLUMN wo_interface_unit_imp_stg.process_level IS 'WO or TASK to indicate if estimates are being loaded for an entire work order or for a specific task.';


CREATE TABLE wo_interface_unit_imp_arc (
  import_run_id           NUMBER(22,0)   NOT NULL,
  line_id                 NUMBER(22,0)   NOT NULL,
  error_message           VARCHAR2(4000) NULL,
  time_stamp              DATE           NULL,
  user_id                 VARCHAR2(18)   NULL,
  work_order_number       VARCHAR2(35)   NOT NULL,
  estimate_id             NUMBER(22,0)   NULL,
  revision                NUMBER(22,0)   NULL,
  work_order_id           NUMBER(22,0)   NULL,
  expenditure_type_id     NUMBER(22,0)   NULL,
  job_task_id             VARCHAR2(35)   NULL,
  utility_account_id      NUMBER(22,0)   NULL,
  est_chg_type_id         NUMBER(22,0)   NULL,
  sub_account_id          NUMBER(22,0)   NULL,
  stck_keep_unit_id       NUMBER(22,0)   NULL,
  retirement_unit_id      NUMBER(22,0)   NULL,
  bus_segment_id          NUMBER(22,0)   NULL,
  asset_id                NUMBER(22,0)   NULL,
  quantity                NUMBER(22,2)   NULL,
  hours                   NUMBER(22,2)   NULL,
  amount                  NUMBER(22,2)   NULL,
  notes                   VARCHAR2(254)  NULL,
  department_id           NUMBER(22,0)   NULL,
  property_group_id       NUMBER(22,0)   NULL,
  asset_location_id       NUMBER(22,0)   NULL,
  replacement_amount      NUMBER(22,2)   NULL,
  serial_number           VARCHAR2(35)   NULL,
  wo_est_trans_type_id    NUMBER(22,0)   NULL,
  est_in_service_date     DATE           NULL,
  status                  NUMBER(22,0)   NULL,
  retire_vintage          NUMBER(22,0)   NULL,
  unit_desc               VARCHAR2(35)   NULL,
  unit_long_desc          VARCHAR2(254)  NULL,
  percent                 NUMBER(22,8)   NULL,
  field_1                 VARCHAR2(35)   NULL,
  field_2                 VARCHAR2(35)   NULL,
  field_3                 VARCHAR2(35)   NULL,
  field_4                 VARCHAR2(35)   NULL,
  field_5                 VARCHAR2(35)   NULL,
  company_id              NUMBER(22,0)   NULL,
  batch_id                VARCHAR2(60)   NULL,
  company_xlate           VARCHAR2(35)   NULL,
  funding_wo_indicator    NUMBER(22,0)   NULL,
  expenditure_type_xlate  VARCHAR2(254)  NULL,
  est_chg_type_xlate      VARCHAR2(254)  NULL,
  stck_keep_unit_xlate    VARCHAR2(254)  NULL,
  wo_est_trans_type_xlate VARCHAR2(35)   NULL,
  department_xlate        VARCHAR2(254)  NULL,
  job_task_xlate          VARCHAR2(254)  NULL,
  bus_segment_xlate       VARCHAR2(254)  NULL,
  utility_account_xlate   VARCHAR2(254)  NULL,
  sub_account_xlate       VARCHAR2(254)  NULL,
  property_group_xlate    VARCHAR2(254)  NULL,
  retirement_unit_xlate   VARCHAR2(254)  NULL,
  asset_location_xlate    VARCHAR2(254)  NULL,
  asset_id_xlate          VARCHAR2(254)  NULL,
  process_level           VARCHAR2(35)   NULL,
  seq_id                  NUMBER(22,0)   DEFAULT 0 NULL,
  row_id                  NUMBER(22,0)   NULL,
  replace_estimates       VARCHAR2(35)   NULL
)
/

ALTER TABLE wo_interface_unit_imp_arc
  ADD CONSTRAINT wo_interface_unit_imp_arc_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/


COMMENT ON TABLE wo_interface_unit_imp_arc IS '(C)  [13]
The WO Interface Unit Import Staging table is an table that loads date into the API for unit and as-built estimates.';

COMMENT ON COLUMN wo_interface_unit_imp_arc.error_message IS 'String containing an inclusive list of all errors applicable to a given record during processing of the wo unit estimate interface API.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.time_stamp IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.work_order_number IS 'Work order number.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.estimate_id IS 'Estimate row number.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.revision IS 'Revision of estimate for this work order.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.work_order_id IS 'Unique identifier of a work order in PowerPlan.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.expenditure_type_id IS 'One of the four expenditure types (Add, retire, etc).';
COMMENT ON COLUMN wo_interface_unit_imp_arc.job_task_id IS 'Optional job task identifier associated with the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.utility_account_id IS 'Utility (plant) account id.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.est_chg_type_id IS 'Estimate charge type (e.g. labor).';
COMMENT ON COLUMN wo_interface_unit_imp_arc.sub_account_id IS 'Sub account of the plant account.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.stck_keep_unit_id IS 'Stock keeping unit for a material charge.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.retirement_unit_id IS 'Retirement unit of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.bus_segment_id IS 'Business egment of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.asset_id IS 'Asset id of the estimate (e.g. if a retirement).';
COMMENT ON COLUMN wo_interface_unit_imp_arc.quantity IS 'Quanitfy of the retirement unit (estimate).';
COMMENT ON COLUMN wo_interface_unit_imp_arc.hours IS 'Hours of a labor estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.amount IS 'Dollar amount of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.notes IS 'Optional notes fields.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.department_id IS 'Department of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.property_group_id IS 'Property group of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.asset_location_id IS 'Asset location of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.replacement_amount IS 'Replacement dollar amount for a retirement if using HW indexed retirements.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.serial_number IS 'Serial number of the equipment.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.wo_est_trans_type_id IS 'Identifier of work order estimate transaction type.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.est_in_service_date IS 'Estimated in-service (out of service) date.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.status IS 'Status of the estimate.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.retire_vintage IS 'Used to identify specific vintage retirements for mass property.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.unit_desc IS 'Unit Desc to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.unit_long_desc IS 'Unit Long Desc to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.percent IS 'Percent to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.field_1 IS 'Field 1 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.field_2 IS 'Field 2 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.field_3 IS 'Field 3 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.field_4 IS 'Field 4 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.field_5 IS 'Field 5 to be mapped into the unit estimates.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.company_id IS 'Company of the work order being loaded.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.batch_id IS 'Batch identifier used to segregate records from multiple instances of the job task interface.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.company_xlate IS 'External value used to map to PowerPlan companies.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.funding_wo_indicator IS '1 or 0 to indicate whether estimates are being loaded to work orders or funding projects.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.expenditure_type_xlate IS 'External value used to map to PowerPlan expenditure types.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.est_chg_type_xlate IS 'External value used to map to PowerPlan est charge types.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.stck_keep_unit_xlate IS 'External value used to map to PowerPlan stock keeping units.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.wo_est_trans_type_xlate IS 'External value used to map to PowerPlan wo est trans type.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.department_xlate IS 'External value used to map to PowerPlan department.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.job_task_xlate IS 'External value used to map to PowerPlan job task.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.bus_segment_xlate IS 'External value used to map to PowerPlan business segment.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.utility_account_xlate IS 'External value used to map to PowerPlan utility account.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.sub_account_xlate IS 'External value used to map to PowerPlan sub account.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.property_group_xlate IS 'External value used to map to PowerPlan property group.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.retirement_unit_xlate IS 'External value used to map to PowerPlan retirement unit.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.asset_location_xlate IS 'External value used to map to PowerPlan asset location.';
COMMENT ON COLUMN wo_interface_unit_imp_arc.process_level IS 'WO or TASK to indicate if estimates are being loaded for an entire work order or for a specific task.';


CREATE TABLE wo_interface_monthly_imp_stg (
  import_run_id              NUMBER(22,0)   NOT NULL,
  line_id                    NUMBER(22,0)   NOT NULL,
  error_message              VARCHAR2(4000) NULL,
  time_stamp                 DATE           NULL,
  user_id                    VARCHAR2(18)   NULL,
  row_id                     NUMBER(22,0)   NOT NULL,
  work_order_number          VARCHAR2(35)   NULL,
  company_xlate              VARCHAR2(254)  NULL,
  expenditure_type_xlate     VARCHAR2(254)  NULL,
  est_chg_type_xlate         VARCHAR2(254)  NULL,
  department_xlate           VARCHAR2(254)  NULL,
  job_task_xlate             VARCHAR2(254)  NULL,
  utility_account_xlate      VARCHAR2(254)  NULL,
  long_description           VARCHAR2(254)  NULL,
  wo_work_order_number_xlate VARCHAR2(254)  NULL,
  "YEAR"                       NUMBER(22,0)   NOT NULL,
  january                    NUMBER(22,2)   NULL,
  february                   NUMBER(22,2)   NULL,
  march                      NUMBER(22,2)   NULL,
  april                      NUMBER(22,2)   NULL,
  may                        NUMBER(22,2)   NULL,
  june                       NUMBER(22,2)   NULL,
  july                       NUMBER(22,2)   NULL,
  august                     NUMBER(22,2)   NULL,
  september                  NUMBER(22,2)   NULL,
  october                    NUMBER(22,2)   NULL,
  november                   NUMBER(22,2)   NULL,
  december                   NUMBER(22,2)   NULL,
  total                      NUMBER(22,2)   NULL,
  hrs_jan                    NUMBER(22,4)   NULL,
  hrs_feb                    NUMBER(22,4)   NULL,
  hrs_mar                    NUMBER(22,4)   NULL,
  hrs_apr                    NUMBER(22,4)   NULL,
  hrs_may                    NUMBER(22,4)   NULL,
  hrs_jun                    NUMBER(22,4)   NULL,
  hrs_jul                    NUMBER(22,4)   NULL,
  hrs_aug                    NUMBER(22,4)   NULL,
  hrs_sep                    NUMBER(22,4)   NULL,
  hrs_oct                    NUMBER(22,4)   NULL,
  hrs_nov                    NUMBER(22,4)   NULL,
  hrs_dec                    NUMBER(22,4)   NULL,
  hrs_total                  NUMBER(22,4)   NULL,
  qty_jan                    NUMBER(22,4)   NULL,
  qty_feb                    NUMBER(22,4)   NULL,
  qty_mar                    NUMBER(22,4)   NULL,
  qty_apr                    NUMBER(22,4)   NULL,
  qty_may                    NUMBER(22,4)   NULL,
  qty_jun                    NUMBER(22,4)   NULL,
  qty_jul                    NUMBER(22,4)   NULL,
  qty_aug                    NUMBER(22,4)   NULL,
  qty_sep                    NUMBER(22,4)   NULL,
  qty_oct                    NUMBER(22,4)   NULL,
  qty_nov                    NUMBER(22,4)   NULL,
  qty_dec                    NUMBER(22,4)   NULL,
  qty_total                  NUMBER(22,4)   NULL,
  budget_version_xlate       VARCHAR2(254)  NULL,
  work_order_id              NUMBER(22,0)   NULL,
  company_id                 NUMBER(22,0)   NULL,
  expenditure_type_id        NUMBER(22,0)   NULL,
  est_chg_type_id            NUMBER(22,0)   NULL,
  department_id              NUMBER(22,0)   NULL,
  job_task_id                VARCHAR2(35)   NULL,
  utility_account_id         NUMBER(22,0)   NULL,
  wo_work_order_id           NUMBER(22,0)   NULL,
  budget_version_id          NUMBER(22,0)   NULL,
  est_monthly_id             NUMBER(22,0)   NULL,
  batch_id                   VARCHAR2(35)   NULL,
  funding_wo_indicator       NUMBER(22,0)   NULL,
  seq_id                     NUMBER(22,0)   NULL,
  process_level              VARCHAR2(35)   NULL,
  status                     NUMBER(22,0)   NULL,
  old_revision               NUMBER(22,0)   NULL,
  new_revision               NUMBER(22,0)   NULL,
  revision                   NUMBER(22,0)   NULL,
  replace_estimates          VARCHAR2(35)   NULL,
  replace_year               VARCHAR2(35)   NULL
)
/

ALTER TABLE wo_interface_monthly_imp_stg
  ADD CONSTRAINT wo_interface_monimp_stg_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/


COMMENT ON TABLE wo_interface_monthly_imp_stg IS '(O)  [13]
The WO Interface Monthly table is an table that loads date into the API for dollar estimates';

COMMENT ON COLUMN wo_interface_monthly_imp_stg.time_stamp IS 'Standard System-assigned time stamp used for audit purposes.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.work_order_number IS 'Work order or funding project number';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.long_description IS 'Long Description for estimate or interface comments';
COMMENT ON COLUMN wo_interface_monthly_imp_stg."YEAR" IS 'Four digit year (YYYY) for the estimate';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.january IS 'Estimated dollars for January';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.february IS 'Estimated dollars for February';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.march IS 'Estimated dollars for March';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.april IS 'Estimated dollars for April';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.may IS 'Estimated dollars for May';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.june IS 'Estimated dollars for June';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.july IS 'Estimated dollars for July';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.august IS 'Estimated dollars for August';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.september IS 'Estimated dollars for September';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.october IS 'Estimated dollars for October';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.november IS 'Estimated dollars for November';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.december IS 'Estimated dollars for December';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.total IS 'Estimated dollars in total (this is also computed based on the months)';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.hrs_jan IS 'Estimated hours for January.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.hrs_dec IS 'Estimated hours for December.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.hrs_total IS 'Estimated hours for the year.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.qty_jan IS 'Estimated quanity for January.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.qty_dec IS 'Estimated quantity for December.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.qty_total IS 'Estimated quantity for the year.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.work_order_id IS 'System identifier for a work order';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.company_id IS 'System identifier for a company';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.expenditure_type_id IS 'System identifier for an expenditure type';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.est_chg_type_id IS 'System identifier for an estimate charge type';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.department_id IS 'System identifier for a department';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.job_task_id IS 'System identifier for a job task';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.utility_account_id IS 'System identifier for a utility account or budget plant class';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.wo_work_order_id IS 'Work Order id when estimating at the funding project level.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.budget_version_id IS 'System identifier for a budget version';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.est_monthly_id IS 'System-assigned identifier for a monthly estimate record';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.status IS 'Status of processing of this record.  New records should be inserted with a value of 0.';
COMMENT ON COLUMN wo_interface_monthly_imp_stg.revision IS 'System identifier for a revision (computed during upload process)';

CREATE TABLE wo_interface_monthly_imp_arc (
  import_run_id              NUMBER(22,0)   NOT NULL,
  line_id                    NUMBER(22,0)   NOT NULL,
  error_message              VARCHAR2(4000) NULL,
  time_stamp                 DATE           NULL,
  user_id                    VARCHAR2(18)   NULL,
  row_id                     NUMBER(22,0)   NOT NULL,
  work_order_number          VARCHAR2(35)   NULL,
  company_xlate              VARCHAR2(254)  NULL,
  expenditure_type_xlate     VARCHAR2(254)  NULL,
  est_chg_type_xlate         VARCHAR2(254)  NULL,
  department_xlate           VARCHAR2(254)  NULL,
  job_task_xlate             VARCHAR2(254)  NULL,
  utility_account_xlate      VARCHAR2(254)  NULL,
  long_description           VARCHAR2(254)  NULL,
  wo_work_order_number_xlate VARCHAR2(254)  NULL,
  "YEAR"                       NUMBER(22,0)   NOT NULL,
  january                    NUMBER(22,2)   NULL,
  february                   NUMBER(22,2)   NULL,
  march                      NUMBER(22,2)   NULL,
  april                      NUMBER(22,2)   NULL,
  may                        NUMBER(22,2)   NULL,
  june                       NUMBER(22,2)   NULL,
  july                       NUMBER(22,2)   NULL,
  august                     NUMBER(22,2)   NULL,
  september                  NUMBER(22,2)   NULL,
  october                    NUMBER(22,2)   NULL,
  november                   NUMBER(22,2)   NULL,
  december                   NUMBER(22,2)   NULL,
  total                      NUMBER(22,2)   NULL,
  hrs_jan                    NUMBER(22,4)   NULL,
  hrs_feb                    NUMBER(22,4)   NULL,
  hrs_mar                    NUMBER(22,4)   NULL,
  hrs_apr                    NUMBER(22,4)   NULL,
  hrs_may                    NUMBER(22,4)   NULL,
  hrs_jun                    NUMBER(22,4)   NULL,
  hrs_jul                    NUMBER(22,4)   NULL,
  hrs_aug                    NUMBER(22,4)   NULL,
  hrs_sep                    NUMBER(22,4)   NULL,
  hrs_oct                    NUMBER(22,4)   NULL,
  hrs_nov                    NUMBER(22,4)   NULL,
  hrs_dec                    NUMBER(22,4)   NULL,
  hrs_total                  NUMBER(22,4)   NULL,
  qty_jan                    NUMBER(22,4)   NULL,
  qty_feb                    NUMBER(22,4)   NULL,
  qty_mar                    NUMBER(22,4)   NULL,
  qty_apr                    NUMBER(22,4)   NULL,
  qty_may                    NUMBER(22,4)   NULL,
  qty_jun                    NUMBER(22,4)   NULL,
  qty_jul                    NUMBER(22,4)   NULL,
  qty_aug                    NUMBER(22,4)   NULL,
  qty_sep                    NUMBER(22,4)   NULL,
  qty_oct                    NUMBER(22,4)   NULL,
  qty_nov                    NUMBER(22,4)   NULL,
  qty_dec                    NUMBER(22,4)   NULL,
  qty_total                  NUMBER(22,4)   NULL,
  budget_version_xlate       VARCHAR2(254)  NULL,
  work_order_id              NUMBER(22,0)   NULL,
  company_id                 NUMBER(22,0)   NULL,
  expenditure_type_id        NUMBER(22,0)   NULL,
  est_chg_type_id            NUMBER(22,0)   NULL,
  department_id              NUMBER(22,0)   NULL,
  job_task_id                VARCHAR2(35)   NULL,
  utility_account_id         NUMBER(22,0)   NULL,
  wo_work_order_id           NUMBER(22,0)   NULL,
  budget_version_id          NUMBER(22,0)   NULL,
  est_monthly_id             NUMBER(22,0)   NULL,
  batch_id                   VARCHAR2(35)   NULL,
  funding_wo_indicator       NUMBER(22,0)   NULL,
  seq_id                     NUMBER(22,0)   NULL,
  process_level              VARCHAR2(35)   NULL,
  status                     NUMBER(22,0)   NULL,
  old_revision               NUMBER(22,0)   NULL,
  new_revision               NUMBER(22,0)   NULL,
  revision                   NUMBER(22,0)   NULL,
  replace_estimates          VARCHAR2(35)   NULL,
  replace_year               VARCHAR2(35)   NULL
)
/

ALTER TABLE wo_interface_monthly_imp_arc
  ADD CONSTRAINT wo_interface_monimp_arc_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

COMMENT ON TABLE wo_interface_monthly_imp_arc IS '(O)  [13]
The WO Interface Monthly table is an table that loads date into the API for dollar estimates';

COMMENT ON COLUMN wo_interface_monthly_imp_arc.time_stamp IS 'Standard System-assigned time stamp used for audit purposes.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.work_order_number IS 'Work order or funding project number';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.long_description IS 'Long Description for estimate or interface comments';
COMMENT ON COLUMN wo_interface_monthly_imp_arc."YEAR" IS 'Four digit year (YYYY) for the estimate';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.january IS 'Estimated dollars for January';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.february IS 'Estimated dollars for February';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.march IS 'Estimated dollars for March';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.april IS 'Estimated dollars for April';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.may IS 'Estimated dollars for May';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.june IS 'Estimated dollars for June';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.july IS 'Estimated dollars for July';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.august IS 'Estimated dollars for August';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.september IS 'Estimated dollars for September';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.october IS 'Estimated dollars for October';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.november IS 'Estimated dollars for November';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.december IS 'Estimated dollars for December';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.total IS 'Estimated dollars in total (this is also computed based on the months)';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.hrs_jan IS 'Estimated hours for January.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.hrs_dec IS 'Estimated hours for December.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.hrs_total IS 'Estimated hours for the year.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.qty_jan IS 'Estimated quanity for January.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.qty_dec IS 'Estimated quantity for December.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.qty_total IS 'Estimated quantity for the year.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.work_order_id IS 'System identifier for a work order';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.company_id IS 'System identifier for a company';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.expenditure_type_id IS 'System identifier for an expenditure type';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.est_chg_type_id IS 'System identifier for an estimate charge type';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.department_id IS 'System identifier for a department';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.job_task_id IS 'System identifier for a job task';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.utility_account_id IS 'System identifier for a utility account or budget plant class';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.wo_work_order_id IS 'Work Order id when estimating at the funding project level.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.budget_version_id IS 'System identifier for a budget version';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.est_monthly_id IS 'System-assigned identifier for a monthly estimate record';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.status IS 'Status of processing of this record.  New records should be inserted with a value of 0.';
COMMENT ON COLUMN wo_interface_monthly_imp_arc.revision IS 'System identifier for a revision (computed during upload process)';

CREATE TABLE wo_interface_ocr_imp_stg (
  import_run_id         NUMBER(22,0)   NOT NULL,
  line_id               NUMBER(22,0)   NOT NULL,
  error_message         VARCHAR2(4000) NULL,
  time_stamp            DATE           NULL,
  user_id               VARCHAR2(18)   NULL,
  company_id            NUMBER(22,0)   NULL,
  company_xlate         VARCHAR2(254)  NULL,
  work_order_id         NUMBER(22,0)   NULL,
  work_order_number     VARCHAR2(35)   NULL,
  work_order_xlate      VARCHAR2(254)  NULL,
  asset_id              NUMBER(22,0)   NULL,
  bus_segment_id        NUMBER(22,0)   NULL,
  bus_segment_xlate     VARCHAR2(254)  NULL,
  utility_account_id    NUMBER(22,0)   NULL,
  utility_account_xlate VARCHAR2(254)  NULL,
  sub_account_id        NUMBER(22,0)   NULL,
  sub_account_xlate     VARCHAR2(254)  NULL,
  property_group_id     NUMBER(22,0)   NULL,
  property_group_xlate  VARCHAR2(254)  NULL,
  retirement_unit_id    NUMBER(22,0)   NULL,
  retirement_unit_xlate VARCHAR2(254)  NULL,
  asset_location_id     NUMBER(22,0)   NULL,
  asset_location_xlate  VARCHAR2(254)  NULL,
  gl_account_id         NUMBER(22,0)   NULL,
  gl_account_xlate      VARCHAR2(254)  NULL,
  serial_number         VARCHAR2(35)   NULL,
  vintage               NUMBER(4,0)    NULL,
  quantity              NUMBER(22,2)   NULL,
  amount                NUMBER(22,2)   NULL,
  upload_indicator      VARCHAR2(1)    NULL,
  validation_message    VARCHAR2(2000) NULL,
  batch_id              VARCHAR2(60)   NULL,
  estimate_id           NUMBER(22,0)   NULL,
  row_id                NUMBER(22,0)   NULL
)
/

ALTER TABLE wo_interface_ocr_imp_stg
  ADD CONSTRAINT wo_interface_ocr_imp_stg_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/


COMMENT ON TABLE wo_interface_ocr_imp_stg IS 'This table is used to stage data for the Original Cost Retirements API.';

COMMENT ON COLUMN wo_interface_ocr_imp_stg.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.company_id IS 'System-assigned identifier of a particular company.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.company_xlate IS 'The external system-assigned identifier of a particular company.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.work_order_id IS 'The system-assigned identifier of a work order.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.work_order_number IS 'User assigned work order number.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.asset_id IS 'System-assigned identifier of a particular asset recorded on the CPR Ledger. Not used for mass retirements.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.bus_segment_id IS 'System-assigned identifier of a particular business Segment.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.bus_segment_xlate IS 'The external system-assigned identifier of a particular business Segment.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.utility_account_id IS 'System-assigned identifier of a particular utility account.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.utility_account_xlate IS 'The external system-assigned identifier of a particular utility account.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.sub_account_id IS 'User-designated values to further detail a particular utility account.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.sub_account_xlate IS 'The external systems user-designated values to further detail a particular utility account.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.property_group_id IS 'System-assigned identifier of a particular property group for the associated asset.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.property_group_xlate IS 'The external system-assigned identifier of a particular property group for the associated asset.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.retirement_unit_id IS 'System-assigned identifier of a particular retirement unit.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.retirement_unit_xlate IS 'The external system-assigned identifier of a particular retirement unit.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.asset_location_id IS 'System-assigned identifier of a particular detailed asset location.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.asset_location_xlate IS 'The external system-assigned identifier of a particular detailed asset location.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.gl_account_id IS 'System-assigned identifier of a particular GL Account.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.gl_account_xlate IS 'The external system-assigned identifier of a particular GL Account.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.serial_number IS 'Serial number of the asset.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.vintage IS 'Initial year in service (Engineering in service date). Null for Mass Ret. Require for specific Ret.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.quantity IS 'Records the quantity associated with the retirement. Required to be non-zero for mass retirements.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.amount IS 'Records the amount associated with the retirement.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.upload_indicator IS 'Indicates rows current status of upload: I In-Progress, E Error, D Done.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.validation_message IS 'Concatenates validations messages if it cannot derive accounting key fields.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.batch_id IS 'System-assigned identifier of a particular batch.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.estimate_id IS 'System-assigned identifier of a particular wo_estimate record.';
COMMENT ON COLUMN wo_interface_ocr_imp_stg.row_id IS 'Unique id number for each row.';

CREATE TABLE wo_interface_ocr_imp_arc (
  import_run_id         NUMBER(22,0)   NOT NULL,
  line_id               NUMBER(22,0)   NOT NULL,
  error_message         VARCHAR2(4000) NULL,
  time_stamp            DATE           NULL,
  user_id               VARCHAR2(18)   NULL,
  company_id            NUMBER(22,0)   NULL,
  company_xlate         VARCHAR2(254)  NULL,
  work_order_id         NUMBER(22,0)   NULL,
  work_order_number     VARCHAR2(35)   NULL,
  work_order_xlate      VARCHAR2(254)  NULL,
  asset_id              NUMBER(22,0)   NULL,
  bus_segment_id        NUMBER(22,0)   NULL,
  bus_segment_xlate     VARCHAR2(254)  NULL,
  utility_account_id    NUMBER(22,0)   NULL,
  utility_account_xlate VARCHAR2(254)  NULL,
  sub_account_id        NUMBER(22,0)   NULL,
  sub_account_xlate     VARCHAR2(254)  NULL,
  property_group_id     NUMBER(22,0)   NULL,
  property_group_xlate  VARCHAR2(254)  NULL,
  retirement_unit_id    NUMBER(22,0)   NULL,
  retirement_unit_xlate VARCHAR2(254)  NULL,
  asset_location_id     NUMBER(22,0)   NULL,
  asset_location_xlate  VARCHAR2(254)  NULL,
  gl_account_id         NUMBER(22,0)   NULL,
  gl_account_xlate      VARCHAR2(254)  NULL,
  serial_number         VARCHAR2(35)   NULL,
  vintage               NUMBER(4,0)    NULL,
  quantity              NUMBER(22,2)   NULL,
  amount                NUMBER(22,2)   NULL,
  upload_indicator      VARCHAR2(1)    NULL,
  validation_message    VARCHAR2(2000) NULL,
  batch_id              VARCHAR2(60)   NULL,
  estimate_id           NUMBER(22,0)   NULL,
  row_id                NUMBER(22,0)   NULL
)
/

ALTER TABLE wo_interface_ocr_imp_arc
  ADD CONSTRAINT wo_interface_ocr_imp_arc_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/


COMMENT ON TABLE wo_interface_ocr_imp_arc IS 'This table is used to stage data for the Original Cost Retirements API.';

COMMENT ON COLUMN wo_interface_ocr_imp_arc.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.company_id IS 'System-assigned identifier of a particular company.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.company_xlate IS 'The external system-assigned identifier of a particular company.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.work_order_id IS 'The system-assigned identifier of a work order.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.work_order_number IS 'User assigned work order number.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.asset_id IS 'System-assigned identifier of a particular asset recorded on the CPR Ledger. Not used for mass retirements.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.bus_segment_id IS 'System-assigned identifier of a particular business Segment.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.bus_segment_xlate IS 'The external system-assigned identifier of a particular business Segment.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.utility_account_id IS 'System-assigned identifier of a particular utility account.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.utility_account_xlate IS 'The external system-assigned identifier of a particular utility account.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.sub_account_id IS 'User-designated values to further detail a particular utility account.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.sub_account_xlate IS 'The external systems user-designated values to further detail a particular utility account.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.property_group_id IS 'System-assigned identifier of a particular property group for the associated asset.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.property_group_xlate IS 'The external system-assigned identifier of a particular property group for the associated asset.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.retirement_unit_id IS 'System-assigned identifier of a particular retirement unit.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.retirement_unit_xlate IS 'The external system-assigned identifier of a particular retirement unit.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.asset_location_id IS 'System-assigned identifier of a particular detailed asset location.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.asset_location_xlate IS 'The external system-assigned identifier of a particular detailed asset location.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.gl_account_id IS 'System-assigned identifier of a particular GL Account.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.gl_account_xlate IS 'The external system-assigned identifier of a particular GL Account.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.serial_number IS 'Serial number of the asset.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.vintage IS 'Initial year in service (Engineering in service date). Null for Mass Ret. Require for specific Ret.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.quantity IS 'Records the quantity associated with the retirement. Required to be non-zero for mass retirements.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.amount IS 'Records the amount associated with the retirement.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.upload_indicator IS 'Indicates rows current status of upload: I In-Progress, E Error, D Done.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.validation_message IS 'Concatenates validations messages if it cannot derive accounting key fields.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.batch_id IS 'System-assigned identifier of a particular batch.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.estimate_id IS 'System-assigned identifier of a particular wo_estimate record.';
COMMENT ON COLUMN wo_interface_ocr_imp_arc.row_id IS 'Unique id number for each row.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2513, 0, 2015, 1, 0, 0, 43663, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043663_sys_acqa_06_dk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
