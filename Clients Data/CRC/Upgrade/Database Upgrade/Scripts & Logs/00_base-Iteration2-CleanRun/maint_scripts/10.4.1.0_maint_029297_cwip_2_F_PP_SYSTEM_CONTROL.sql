/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029297_cwip_2_F_PP_SYSTEM_CONTROL.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/20/2013 Joseph King    Point Release
||============================================================================
*/

create or replace function F_PP_SYSTEM_CONTROL(A_COMPANY_ID   number,
                                               A_CONTROL_NAME varchar2) return varchar2
                                               RESULT_CACHE RELIES_ON(PP_SYSTEM_CONTROL_COMPANY) is

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: F_PP_SYSTEM_CONTROL
   || Description: Lookup a control value.
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Initial create
   ||============================================================================
   */

   R_CONTROL_VALUE varchar2(254);
   COUNTER         number;

begin

   select count(*), min(TO_NUMBER(CONTROL_VALUE))
     into COUNTER, R_CONTROL_VALUE
     from PP_SYSTEM_CONTROL_COMPANY
    where UPPER(trim(CONTROL_NAME)) = UPPER(trim(A_CONTROL_NAME))
      and COMPANY_ID = A_COMPANY_ID;

   if COUNTER = 0 and A_COMPANY_ID <> -1 then
      select count(*), min(TO_NUMBER(CONTROL_VALUE))
        into COUNTER, R_CONTROL_VALUE
        from PP_SYSTEM_CONTROL_COMPANY
       where UPPER(trim(CONTROL_NAME)) = UPPER(trim(A_CONTROL_NAME))
         and COMPANY_ID = -1;

      if COUNTER = 0 then
         R_CONTROL_VALUE := '';
      end if;
   elsif COUNTER = 0 then
      R_CONTROL_VALUE := '';
   end if;

   return R_CONTROL_VALUE;

end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (330, 0, 10, 4, 1, 0, 29297, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029297_cwip_2_F_PP_SYSTEM_CONTROL.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
