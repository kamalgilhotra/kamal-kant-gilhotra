/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036212_sys_budget_exes.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/05/2014 Stephen Motter
||============================================================================
*/

update PP_PROCESSES set EXECUTABLE_FILE = 'ssp_budget.exe Esc'   where DESCRIPTION = 'Budget Escalations';
update PP_PROCESSES set EXECUTABLE_FILE = 'ssp_budget.exe Load'  where DESCRIPTION = 'Load Budget Dollars';
update PP_PROCESSES set EXECUTABLE_FILE = 'ssp_budget.exe OH'    where DESCRIPTION = 'Budget Overheads';
update PP_PROCESSES set EXECUTABLE_FILE = 'ssp_budget.exe CR'    where DESCRIPTION = 'Budget To CR';
update PP_PROCESSES set EXECUTABLE_FILE = 'ssp_budget.exe WIP'   where DESCRIPTION = 'Budget WIP Comp';
update PP_PROCESSES set EXECUTABLE_FILE = 'ssp_budget.exe AFUDC' where DESCRIPTION = 'Budget AFUDC';
update PP_PROCESSES set EXECUTABLE_FILE = 'ssp_budget.exe UWA'   where DESCRIPTION = 'Budget Update with Actuals';
update PP_PROCESSES set EXECUTABLE_FILE = 'ssp_budget.exe'       where DESCRIPTION = 'Budget Automatic Processing';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (945, 0, 10, 4, 2, 0, 36212, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036212_sys_budget_exes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;