/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009888_prov_dmi_process_info.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date            Revised By          Reason for Change
|| -------- --------------- ------------------- ------------------------------
|| 10.4.2.0 12/20/2013      Ron Ferentini
||============================================================================
*/

alter table TAX_ACCRUAL_DMI_PROCESS_INFO modify PROVISIONENDMONTH number(2,2);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (821, 0, 10, 4, 2, 0, 9888, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_009888_prov_dmi_process_info.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;