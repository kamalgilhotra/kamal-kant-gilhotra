/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040477_lease_reports.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.3.0 10/22/2014 Daniel Motter        PP-40476
||========================================================================================
*/

update PP_REPORTS
   set PP_REPORT_FILTER_ID = 42
 where DATAWINDOW = 'dw_ls_rpt_future_est_exec_cost';

update PP_REPORTS
   set DESCRIPTION = replace(DESCRIPTION,'Principle','Principal'),
       LONG_DESCRIPTION = replace(LONG_DESCRIPTION,'Principle','Principal')
 where DATAWINDOW = 'dw_ls_rpt_principle_interest_breakdown';

delete from PP_REPORTS
 where DATAWINDOW = 'dw_ls_rpt_lease_calc_adjustment';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1565, 0, 10, 4, 3, 0, 40477, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040477_lease_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;