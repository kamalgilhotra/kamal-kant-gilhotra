/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030295_system_basegui.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/28/2013 Julia Breuer   Point release
||============================================================================
*/

insert into POWERPLANT_DDDW (DROPDOWN_NAME, TIME_STAMP, USER_ID, TABLE_NAME, CODE_COL, DISPLAY_COL) values ('pp_company_filter', sysdate, user, 'company_setup', 'company_id', 'description');
insert into POWERPLANT_DDDW (DROPDOWN_NAME, TIME_STAMP, USER_ID, TABLE_NAME, CODE_COL, DISPLAY_COL) values ('pp_county_filter', sysdate, user, 'county', 'state_id_county_id', 'description');
insert into POWERPLANT_DDDW (DROPDOWN_NAME, TIME_STAMP, USER_ID, TABLE_NAME, CODE_COL, DISPLAY_COL) values ('pp_spread_factor_filter', sysdate, user, 'spread_factor', 'factor_id', 'description');
insert into POWERPLANT_DDDW (DROPDOWN_NAME, TIME_STAMP, USER_ID, TABLE_NAME, CODE_COL, DISPLAY_COL) values ('pp_state_filter', sysdate, user, 'state', 'state_id', 'description');
insert into POWERPLANT_DDDW (DROPDOWN_NAME, TIME_STAMP, USER_ID, TABLE_NAME, CODE_COL, DISPLAY_COL) values ('pp_status_code_filter', sysdate, user, 'status_code', 'status_code_id', 'description');
insert into POWERPLANT_DDDW (DROPDOWN_NAME, TIME_STAMP, USER_ID, TABLE_NAME, CODE_COL, DISPLAY_COL) values ('pp_yes_no_filter', sysdate, user, 'yes_no', 'yes_no_id', 'description');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (423, 0, 10, 4, 1, 0, 30295, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030295_system_basegui.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
