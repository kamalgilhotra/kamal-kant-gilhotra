/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039984_cwip_fast101.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/3/2014 Sunjin Cone
||============================================================================
*/

drop table UNITIZE_ALLOC_LATE_ADD_STATS;
drop table UNITIZE_ALLOC_LATE_RWIP_STATS;
drop table UNITIZE_ALLOC_UNIT_ITEM_STATS;
drop table UNITIZE_ALLOC_EST_STATS;
drop table UNITIZE_ALLOC_STND_STATS;
drop table UNITIZE_ALLOC_ERRORS;
drop table UNITIZE_ALLOC_CGC_INSERTS;
drop table UNITIZE_ALLOC_LATE_BP_TEMP;


alter table UNITIZE_WO_STG_UNITS_TEMP add MIN_RWIP_BATCH_UNIT_ID number(22,0);
alter table UNITIZE_ELIG_UNITS_TEMP   add MIN_RWIP_BATCH_UNIT_ID number(22,0);

comment on column UNITIZE_WO_STG_UNITS_TEMP.MIN_RWIP_BATCH_UNIT_ID is 'This field has the min(BATCH_UNIT_ITEM_ID) from CHARGE_GROUP_CONTROL for the first time RWIP charges unitized for the work order.';
comment on column UNITIZE_ELIG_UNITS_TEMP.MIN_RWIP_BATCH_UNIT_ID is 'This field has the min(BATCH_UNIT_ITEM_ID) from CHARGE_GROUP_CONTROL for the first time RWIP charges unitized for the work order.';


create global temporary table UNITIZE_RATIO_LATE_ADD_TEMP
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 BOOK_SUMMARY_ID        number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 UNITIZE_BY_ACCOUNT     number(22,0),
 UTILITY_ACCOUNT_ID     number(22,0),
 BUS_SEGMENT_ID         number(22,0),
 SUB_ACCOUNT_ID         number(22,0),
 COST_SOURCE            varchar2(35),
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_UT      number(22,8),
 ALLOC_RATIO_BY_SUB     number(22,8)
) on commit preserve rows;

comment on table  UNITIZE_RATIO_LATE_ADD_TEMP is '(C) [04] Global temporary table for allocation information used to unitize late Additions charges for Late Close using the CPR dollars.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the addition expenditure type.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.BOOK_SUMMARY_ID is 'System-assigned identifier of the book summary of the late charge.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.COST_SOURCE is 'This field will indicate the ALLOC_STATISTIC being sourced from one of the following:  CPR_LEDGER, CPR_ACTIVITY, CPR_LDG_BASIS, or CPR_ACT_BASIS.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_RATIO_LATE_ADD_TEMP.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';


create global temporary table UNITIZE_RATIO_LATE_RWIP_TEMP
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 PROCESSING_TYPE_ID     number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 UNITIZE_BY_ACCOUNT     number(22,0),
 UTILITY_ACCOUNT_ID     number(22,0),
 BUS_SEGMENT_ID         number(22,0),
 SUB_ACCOUNT_ID         number(22,0),
 COST_SOURCE            varchar2(35),
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_UT      number(22,8),
 ALLOC_RATIO_BY_SUB     number(22,8),
 MIN_RWIP_BATCH_UNIT_ID number(22,0)
) on commit preserve rows;

comment on table  UNITIZE_RATIO_LATE_RWIP_TEMP is '(C) [04] Global temporary table for allocation information used to unitize late RWIP charges for Late Close using the first time unitized RWIP dollars.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the retirement expenditure type.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.PROCESSING_TYPE_ID is 'System-assigned identifier of the processing type. The processing type of the late charge determines if the late charge is Cost of Removal, Salvage Cash, etc.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.COST_SOURCE is 'This field will indicate the ALLOC_STATISTIC being sourced from one of the following, MATCHED, ALL, where MATCHED indicates same processing type from prior unitization used. ALL indicates total RWIP dollars used.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';
comment on column UNITIZE_RATIO_LATE_RWIP_TEMP.MIN_RWIP_BATCH_UNIT_ID is 'This field has the min(BATCH_UNIT_ITEM_ID) from CHARGE_GROUP_CONTROL for the first time RWIP charges unitized for the work order.';



create global temporary table UNITIZE_RATIO_UNITS_TEMP
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 ALLOC_ID               number(22,0) not null,
 ALLOC_PRIORITY         number(22,8) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 ALLOC_TYPE             varchar2(35),
 ALLOC_BASIS            varchar2(35),
 UNITIZE_BY_ACCOUNT     number(22,0),
 UTILITY_ACCOUNT_ID     number(22,0),
 BUS_SEGMENT_ID         number(22,0),
 SUB_ACCOUNT_ID         number(22,0),
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_UT      number(22,8),
 ALLOC_RATIO_BY_SUB     number(22,8)
) on commit preserve rows;

comment on table  UNITIZE_RATIO_UNITS_TEMP is '(C) [04] Global temporary table  of the allocation information used to auto unitize charge types configured for Actuals, Estimate, and Standards based allocation.';
comment on column UNITIZE_RATIO_UNITS_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_RATIO_UNITS_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_RATIO_UNITS_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_RATIO_UNITS_TEMP.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_RATIO_UNITS_TEMP.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_RATIO_UNITS_TEMP.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_RATIO_UNITS_TEMP.ALLOC_TYPE is 'Allocation type: actuals.';
comment on column UNITIZE_RATIO_UNITS_TEMP.ALLOC_BASIS is 'Allocation basis: dollars, quantity.';
comment on column UNITIZE_RATIO_UNITS_TEMP.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_RATIO_UNITS_TEMP.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_UNITS_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_UNITS_TEMP.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_UNITS_TEMP.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_RATIO_UNITS_TEMP.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_RATIO_UNITS_TEMP.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_RATIO_UNITS_TEMP.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';


create global temporary table UNITIZE_ALLOC_EST_TEMP
(
 COMPANY_ID          number(22,0) not null,
 WORK_ORDER_ID       number(22,0) not null,
 UNIT_ITEM_ID        number(22,0) not null,
 EXPENDITURE_TYPE_ID number(22,0) not null,
 ALLOC_ID            number(22,0) not null,
 ALLOC_TYPE          varchar2(35),
 ALLOC_BASIS         varchar2(35),
 UNITIZE_BY_ACCOUNT  number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 ALLOC_STATISTIC     number(22,2)
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_EST_TEMP is '(C) [04] Global temporary table of Summarized and One for One Estimate information used to auto unitize charge types configured for Estimates based allocation.';
comment on column UNITIZE_ALLOC_EST_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_EST_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_EST_TEMP.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_EST_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_EST_TEMP.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_EST_TEMP.ALLOC_TYPE is 'Allocation type: estimates.';
comment on column UNITIZE_ALLOC_EST_TEMP.ALLOC_BASIS is 'Allocation basis: dollars, quantity.';
comment on column UNITIZE_ALLOC_EST_TEMP.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_ALLOC_EST_TEMP.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_EST_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_ALLOC_EST_TEMP.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_ALLOC_EST_TEMP.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';

create global temporary table UNITIZE_ALLOC_STND_TEMP
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 ALLOC_ID               number(22,0) not null,
 ALLOC_PRIORITY         number(22,0) not null,
 ALLOC_TYPE             varchar2(35),
 ALLOC_BASIS            varchar2(35),
 UNITIZE_BY_ACCOUNT     number(22,0),
 UTILITY_ACCOUNT_ID     number(22,0),
 BUS_SEGMENT_ID         number(22,0),
 SUB_ACCOUNT_ID         number(22,0),
 ALLOC_STATISTIC        number(22,2),
 UNIT_QUANTITY          number(22,2)
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_STND_TEMP is '(C) [04] Global temporary table of the allocation information used to auto unitize charge types configured for Standards based allocation.';
comment on column UNITIZE_ALLOC_STND_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_STND_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_STND_TEMP.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_STND_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_STND_TEMP.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_STND_TEMP.ALLOC_TYPE is 'Allocation type: standards.';
comment on column UNITIZE_ALLOC_STND_TEMP.ALLOC_BASIS is 'Allocation basis: standard material cost, standard labor_cost, standard labor hours, standard cor cost, standard cor hours.';
comment on column UNITIZE_ALLOC_STND_TEMP.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_ALLOC_STND_TEMP.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_STND_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_STND_TEMP.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_STND_TEMP.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_STND_TEMP.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_STND_TEMP.UNIT_QUANTITY is 'Unit Item quantity.';

create global temporary table UNITIZE_ALLOC_ERRORS_TEMP
(
 COMPANY_ID           number(22,0) not null,
 WORK_ORDER_ID        number(22,0) not null,
 ERROR_MSG            varchar2(2000) not null
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_ERRORS_TEMP is '(C) [04] Global temporary table of the allocation errors saved for work orders being auto unitized.';
comment on column UNITIZE_ALLOC_ERRORS_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_ERRORS_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_ERRORS_TEMP.ERROR_MSG is 'Description of the error message.';

create global temporary table UNITIZE_ALLOC_CGC_INSERTS_TEMP
(
 ID                   number(22,0),
 COMPANY_ID           number(22,0) not null,
 ORIG_CHARGE_GROUP_ID number(22,0) not null,
 NEW_CHARGE_GROUP_ID  number(22,0),
 WORK_ORDER_ID        number(22,0) not null,
 ALLOC_ID             number(22,0),
 UNIT_ITEM_ID         number(22,0),
 UTILITY_ACCOUNT_ID   number(22,0),
 BUS_SEGMENT_ID       number(22,0),
 SUB_ACCOUNT_ID       number(22,0),
 CHARGE_TYPE_ID       number(22,0),
 EXPENDITURE_TYPE_ID  number(22,0),
 RETIREMENT_UNIT_ID   number(22,0),
 PROPERTY_GROUP_ID    number(22,0),
 ASSET_LOCATION_ID    number(22,0),
 SERIAL_NUMBER        varchar2(35),
 DESCRIPTION          varchar2(35),
 GROUP_INDICATOR      number(22,0),
 ALLOCATION_PRIORITY  number(22,0),
 UNITIZATION_SUMMARY  varchar2(35),
 BOOK_SUMMARY_NAME    varchar2(35),
 AMOUNT               number(22,2) default 0,
 QUANTITY             number(22,2) default 0
) on commit preserve rows;

comment on table UNITIZE_ALLOC_CGC_INSERTS_TEMP is '(C) [04] Global temporary table of the auto unitization allocation results to be inserted into the CHARGE_GROUP_CONTROL table.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.ID is 'System-assigned identifier of a particular record in the table.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.ORIG_CHARGE_GROUP_ID is 'System-assigned identifier of the charge_group_id being allocated.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.NEW_CHARGE_GROUP_ID is 'System-assigned identifier of the new charge_group_id created for the allocation results.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.UNIT_ITEM_ID is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utility account.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.CHARGE_TYPE_ID is 'System-assigned identifier of the charge type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.PROPERTY_GROUP_ID is 'System-assigned identifier of the property group.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.ASSET_LOCATION_ID is 'System-assigned identifier of the asset location.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.SERIAL_NUMBER is 'Serial Number.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.DESCRIPTION is 'Records a short description of the charge group (or charge).';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.GROUP_INDICATOR is 'Use null value for these records to indicate they are from allocation results, not from CWIP_CHARGE.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.ALLOCATION_PRIORITY is 'User-defined order (from 1 to n) that was used when the charge types were allocated.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.UNITIZATION_SUMMARY is 'Summary Name from BOOK_SUMMARY associated with the charge type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.BOOK_SUMMARY_NAME is 'Summary Name from BOOK_SUMMARY associated with the charge type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.AMOUNT is 'Dollar amount of allocated records.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.QUANTITY is 'Use 0 for quantity of the allocated records.';


create global temporary table UNITIZE_ALLOC_LATE_BK_TEMP
(
 COMPANY_ID      number(22,0) not null,
 WORK_ORDER_ID   number(22,0) not null,
 BOOK_SUMMARY_ID number(22,0)
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_LATE_BK_TEMP is '(C) [04] A global temp table that stores the list of distinct Book Summaries of the Addition late charges for the work order(s) being auto unitized.';
comment on column UNITIZE_ALLOC_LATE_BK_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LATE_BK_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_LATE_BK_TEMP.BOOK_SUMMARY_ID is 'System-assigned identifier of the book summary of the Addition late charge.';

create global temporary table UNITIZE_ALLOC_LATE_PT_TEMP
(
 COMPANY_ID         number(22,0) not null,
 WORK_ORDER_ID      number(22,0) not null,
 PROCESSING_TYPE_ID number(22,0)
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_LATE_PT_TEMP is '(C) [04] A global temp table that stores the list of distinct Processing Types of the RWIP late charges for the work order(s) being auto unitized.';
comment on column UNITIZE_ALLOC_LATE_PT_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LATE_PT_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_LATE_PT_TEMP.PROCESSING_TYPE_ID is 'System-assigned identifier of the processing type of the RWIP late charge.';

create table UNITIZE_WO_LIST_ARC
(
 UNIT_CLOSED_MONTH_NUMBER number(22,0) not null,
 RUN_TIME_STRING          varchar2(35) not null,
 RUN                      number(22,0) not null,
 WORK_ORDER_ID            number(22,0) not null,
 WORK_ORDER_NUMBER        varchar2(35) not null,
 COMPANY_ID               number(22,0) not null,
 BUS_SEGMENT_ID           number(22,0) not null,
 MAJOR_LOCATION_ID        number(22,0) not null,
 ASSET_LOCATION_ID        number(22,0),
 WO_STATUS_ID             number(22,0) not null,
 IN_SERVICE_DATE          date,
 COMPLETION_DATE          date,
 LATE_CHG_WAIT_PERIOD     number(22,0),
 FUNDING_WO_ID            number(22,0),
 DESCRIPTION              varchar2(35),
 LONG_DESCRIPTION         varchar2(254),
 CLOSING_OPTION_ID        number(22,0) not null,
 CWIP_GL_ACCOUNT          number(22,0) not null,
 NON_UNITIZED_GL_ACCOUNT  number(22,0) not null,
 UNITIZED_GL_ACCOUNT      number(22,0) not null,
 ALLOC_METHOD_TYPE_ID     number(22,0) not null,
 ACCRUAL_TYPE_ID          number(22,0),
 RWIP_YES_NO              varchar2(3),
 EST_UNIT_ITEM_OPTION     number(22,0),
 UNIT_ITEM_FROM_ESTIMATE  number(22,0),
 UNIT_ITEM_SOURCE         varchar2(35),
 UNITIZE_BY_ACCOUNT       number(22,0),
 TOLERANCE_ID             number(22,0),
 TOLERANCE_REVISION       number(22,0),
 TOLERANCE_THRESHOLD      number(22,2),
 TOLERANCE_PCT            number(22,4),
 TOLERANCE_ESTIMATE_AMT   number(22,2),
 TOLERANCE_ACTUAL_AMT     number(22,2),
 TOLERANCE_EST_ACT_DIFF   number(22,2),
 MAX_CHARGE_GROUP_ID      number(22,0),
 MAX_REVISION             number(22,0),
 MAX_UNIT_ITEM_ID         number(22,0),
 RTN_CODE                 number(22,0),
 ERROR_MSG                varchar2(2000),
 UNITIZATION_TYPE         varchar2(35),
 LATE_MONTH_NUMBER        number(22,0) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

comment on table  UNITIZE_WO_LIST_ARC is '(C) [04] Archive table of the data from the UNITIZE_WO_LIST_TEMP global temp table.';
comment on column UNITIZE_WO_LIST_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_WO_LIST_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_WO_LIST_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_WO_LIST_ARC.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_LIST_ARC.WORK_ORDER_NUMBER is 'User-defined number associated with a Work Order.  This is usually a reference to an external numbering system.';
comment on column UNITIZE_WO_LIST_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_LIST_ARC.BUS_SEGMENT_ID is 'System-assigned identifier of a particular business segment.';
comment on column UNITIZE_WO_LIST_ARC.MAJOR_LOCATION_ID is 'System-assigned identifier of a particular major location.';
comment on column UNITIZE_WO_LIST_ARC.ASSET_LOCATION_ID is 'System-assigned identifier of a particular asset location.';
comment on column UNITIZE_WO_LIST_ARC.WO_STATUS_ID is 'System-assigned identifier of a particular work order status.';
comment on column UNITIZE_WO_LIST_ARC.IN_SERVICE_DATE is 'Actual (day, month, year) date in which facility constructed is in service; AFUDC should cease and the accounting may now be in 106 or 101.';
comment on column UNITIZE_WO_LIST_ARC.COMPLETION_DATE is 'Month/year/date that the job is complete and charges are in; at this point it is waiting unitization and closing.';
comment on column UNITIZE_WO_LIST_ARC.LATE_CHG_WAIT_PERIOD is 'Number of months that a Work Order remains after the completion date before it is available for automatic unitization.';
comment on column UNITIZE_WO_LIST_ARC.FUNDING_WO_ID is 'Refers to another work order identifier that is the funding work order to which this work order has been assigned.  Funding work  orders, if used, can be set up using a special Work Order Type on the Work Order Type table.';
comment on column UNITIZE_WO_LIST_ARC.DESCRIPTION is 'Brief description of the Work Order.';
comment on column UNITIZE_WO_LIST_ARC.LONG_DESCRIPTION is 'Detailed description of the Work Order.';
comment on column UNITIZE_WO_LIST_ARC.CLOSING_OPTION_ID is 'System-assigned identifier of a particular closing option.';
comment on column UNITIZE_WO_LIST_ARC.CWIP_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account for relieving the CWIP side of the work order (e.g., 107, 183 when posting to 106 or 101.';
comment on column UNITIZE_WO_LIST_ARC.NON_UNITIZED_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account used when posting in a non-unitized manner (e.g., 106).';
comment on column UNITIZE_WO_LIST_ARC.UNITIZED_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account to which unitized entries are.';
comment on column UNITIZE_WO_LIST_ARC.ALLOC_METHOD_TYPE_ID is 'System-assigned identifier of a particular Allocation Method Type.';
comment on column UNITIZE_WO_LIST_ARC.ACCRUAL_TYPE_ID is 'This field indicates which accrual type is assigned to the work order/funding project.';
comment on column UNITIZE_WO_LIST_ARC.RWIP_YES_NO is 'Yes = Unitization includes RWIP charges.  No = Unitization excludes RWIP Charges.';
comment on column UNITIZE_WO_LIST_ARC.EST_UNIT_ITEM_OPTION is 'Summarize (=0) ? Force unitization to create unit items from the summarized estimates (default). One for One (=1) = Allow unitization to create unit items from non-summarized estimate.';
comment on column UNITIZE_WO_LIST_ARC.UNIT_ITEM_FROM_ESTIMATE is 'Yes (=1) ? Force unitization to create unit items from the estimates only and ignore actuals. No (=0) = Allow unitization to decide how to create unit items based on the presence of actuals first and then estimate (default).';
comment on column UNITIZE_WO_LIST_ARC.UNIT_ITEM_SOURCE is 'This field indicates if the source of the unit item creation is Actuals, Estimates, or CPR.';
comment on column UNITIZE_WO_LIST_ARC.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_WO_LIST_ARC.TOLERANCE_ID is 'System-assigned identifier of a tolerance test used in unitization and set on the unitization tolerance table.';
comment on column UNITIZE_WO_LIST_ARC.TOLERANCE_REVISION is 'Estimate revision associated with the unitization tolerance validation.';
comment on column UNITIZE_WO_LIST_ARC.TOLERANCE_THRESHOLD is 'Estimate dollar VS Actual dollar threshold for the unitization tolerance.';
comment on column UNITIZE_WO_LIST_ARC.TOLERANCE_PCT is 'Estimate vs Actual dollar difference vs Estimate dollar percent threshold for the unitization tolerance.';
comment on column UNITIZE_WO_LIST_ARC.TOLERANCE_ESTIMATE_AMT is 'Estimate dollar amount for the work order.';
comment on column UNITIZE_WO_LIST_ARC.TOLERANCE_ACTUAL_AMT is 'Actual dollar amount for the work order.';
comment on column UNITIZE_WO_LIST_ARC.TOLERANCE_EST_ACT_DIFF is 'Absoulte value of the difference of Estimate vs Actual dollar amounts for the work order.';
comment on column UNITIZE_WO_LIST_ARC.MAX_CHARGE_GROUP_ID is 'The maximum charge_group_id from CHARGE_GROUP_CONTROL table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST_ARC.MAX_REVISION is 'The maximum revision from WO_ESTIMATE table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST_ARC.MAX_UNIT_ITEM_ID is 'The maximum unit_item_id from UNITIZED_WORK_ORDER table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST_ARC.RTN_CODE is 'System-assigned identifier of a particular return code saved to Auto101 errors log(WO_AUTO101_CONTROL).';
comment on column UNITIZE_WO_LIST_ARC.ERROR_MSG is 'Error messages saved to Auto101 errors log(WO_AUTO101_CONTROL).';
comment on column UNITIZE_WO_LIST_ARC.UNITIZATION_TYPE is 'Unitization can be run in muliptlie ways:  AUTO, AUTO LATE, AUTO WO, MANUAL';
comment on column UNITIZE_WO_LIST_ARC.LATE_MONTH_NUMBER is 'The late month_number used for Late Charge Auto Unitization.';
comment on column UNITIZE_WO_LIST_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_WO_LIST_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_ALLOC_LIST_ARC
(
 UNIT_CLOSED_MONTH_NUMBER number(22,0) not null,
 RUN_TIME_STRING          varchar2(35) not null,
 RUN                      number(22,0) not null,
 COMPANY_ID               number(22,0) not null,
 ALLOC_ID                 number(22,0) not null,
 ALLOC_PRIORITY           number(22,0) not null,
 ALLOC_TYPE               varchar2(35) not null,
 ALLOC_BASIS              varchar2(35) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

comment on table  UNITIZE_ALLOC_LIST_ARC is '(C) [04] Archive table of the data from the UNITIZE_ALLOC_LIST_TEMP global temp table.';
comment on column UNITIZE_ALLOC_LIST_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_ALLOC_LIST_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_ALLOC_LIST_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_ALLOC_LIST_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LIST_ARC.ALLOC_ID is 'System-assigned identifier of the unit allocation type.';
comment on column UNITIZE_ALLOC_LIST_ARC.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_LIST_ARC.ALLOC_TYPE is 'Indicates whether the unitization basis will be on Actual, Estimate, or Standards.';
comment on column UNITIZE_ALLOC_LIST_ARC.ALLOC_BASIS is 'Defines the allocation methodology given by allocation type, ie dollars, quantity, material cost, etc.';
comment on column UNITIZE_ALLOC_LIST_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_LIST_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_ELIG_UNITS_ARC
(
 UNIT_CLOSED_MONTH_NUMBER  number(22,0) not null,
 RUN_TIME_STRING           varchar2(35) not null,
 RUN                       number(22,0) not null,
 COMPANY_ID                number(22,0) not null,
 WORK_ORDER_ID             number(22,0) not null,
 UNIT_ITEM_ID              number(22,0) not null,
 EXPENDITURE_TYPE_ID       number(22,0) not null,
 ACTIVITY_CODE             varchar2(8),
 GL_ACCOUNT_ID             number(22,0),
 UTILITY_ACCOUNT_ID        number(22,0),
 BUS_SEGMENT_ID            number(22,0),
 SUB_ACCOUNT_ID            number(22,0),
 RETIREMENT_UNIT_ID        number(22,0),
 ASSET_LOCATION_ID         number(22,0),
 PROPERTY_GROUP_ID         number(22,0),
 SERIAL_NUMBER             varchar2(35),
 LDG_ASSET_ID              number(22,0),
 UNIT_ESTIMATE_ID          number(22,0),
 STATUS                    number(22,0),
 EXCLUDE_FROM_ALLOCATIONS  number(22,0),
 QUANTITY                  number(22,2),
 TIME_STAMP                date,
 USER_ID                   varchar2(18)
);

comment on table  UNITIZE_ELIG_UNITS_ARC is '(C) [04] Archive table of the data from the UNITIZE_ELIG_UNITS_TEMP global temp table.';
comment on column UNITIZE_ELIG_UNITS_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_ELIG_UNITS_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_ELIG_UNITS_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_ELIG_UNITS_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ELIG_UNITS_ARC.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ELIG_UNITS_ARC.UNIT_ITEM_ID is 'System-assigned identifier of the Unit Item.';
comment on column UNITIZE_ELIG_UNITS_ARC.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.  UADD and MADD are set to 1; all others are set to 2.';
comment on column UNITIZE_ELIG_UNITS_ARC.ACTIVITY_CODE is 'System-assigned identifier of the Activity Code.';
comment on column UNITIZE_ELIG_UNITS_ARC.GL_ACCOUNT_ID is  'System-assigned identifier of a gl account.';
comment on column UNITIZE_ELIG_UNITS_ARC.RETIREMENT_UNIT_ID is  'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_ELIG_UNITS_ARC.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.  It can be a FERC utility plant account such as 314 or the Companys own account number structure.';
comment on column UNITIZE_ELIG_UNITS_ARC.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_ELIG_UNITS_ARC.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_ELIG_UNITS_ARC.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_ELIG_UNITS_ARC.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_ELIG_UNITS_ARC.SERIAL_NUMBER is 'User input serial number.';
comment on column UNITIZE_ELIG_UNITS_ARC.LDG_ASSET_ID is 'System-assigned identifier of an asset.';
comment on column UNITIZE_ELIG_UNITS_ARC.UNIT_ESTIMATE_ID is 'The associated ESTIMATE_ID for Unit Items created from One for One estimates.';
comment on column UNITIZE_ELIG_UNITS_ARC.EXCLUDE_FROM_ALLOCATIONS is '1 indicates the record is excluded from Unitization Allocation.';
comment on column UNITIZE_ELIG_UNITS_ARC.QUANTITY is 'The summarized estimate record quantity.';
comment on column UNITIZE_ELIG_UNITS_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ELIG_UNITS_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_RATIO_UNITS_ARC
(
 UNIT_CLOSED_MONTH_NUMBER  number(22,0) not null,
 RUN_TIME_STRING           varchar2(35) not null,
 RUN                       number(22,0) not null,
 COMPANY_ID                number(22,0) not null,
 WORK_ORDER_ID             number(22,0) not null,
 EXPENDITURE_TYPE_ID       number(22,0) not null,
 ALLOC_ID                  number(22,0) not null,
 ALLOC_PRIORITY            number(22,8) not null,
 UNIT_ITEM_ID              number(22,0) not null,
 ALLOC_TYPE                varchar2(35),
 ALLOC_BASIS               varchar2(35),
 UNITIZE_BY_ACCOUNT        number(22,0),
 UTILITY_ACCOUNT_ID        number(22,0),
 BUS_SEGMENT_ID            number(22,0),
 SUB_ACCOUNT_ID            number(22,0),
 ALLOC_STATISTIC           number(22,2),
 ALLOC_RATIO               number(22,8),
 ALLOC_RATIO_BY_UT         number(22,8),
 ALLOC_RATIO_BY_SUB        number(22,8),
 TIME_STAMP                date,
 USER_ID                   varchar2(18)
);

comment on table  UNITIZE_RATIO_UNITS_ARC is '(C) [04] Archive table of the data from the UNITIZE_RATIO_UNITS_TEMP global temp table.';
comment on column UNITIZE_RATIO_UNITS_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_RATIO_UNITS_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_RATIO_UNITS_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_RATIO_UNITS_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_RATIO_UNITS_ARC.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_RATIO_UNITS_ARC.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_RATIO_UNITS_ARC.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_RATIO_UNITS_ARC.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_RATIO_UNITS_ARC.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_RATIO_UNITS_ARC.ALLOC_TYPE is 'Allocation type: actuals.';
comment on column UNITIZE_RATIO_UNITS_ARC.ALLOC_BASIS is 'Allocation basis: dollars, quantity.';
comment on column UNITIZE_RATIO_UNITS_ARC.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_RATIO_UNITS_ARC.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_UNITS_ARC.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_UNITS_ARC.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_UNITS_ARC.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_RATIO_UNITS_ARC.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_RATIO_UNITS_ARC.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_RATIO_UNITS_ARC.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';
comment on column UNITIZE_RATIO_UNITS_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_RATIO_UNITS_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_RATIO_LATE_ADD_ARC
(
 UNIT_CLOSED_MONTH_NUMBER number(22,0) not null,
 RUN_TIME_STRING          varchar2(35) not null,
 RUN                      number(22,0) not null,
 COMPANY_ID               number(22,0) not null,
 WORK_ORDER_ID            number(22,0) not null,
 EXPENDITURE_TYPE_ID      number(22,0) not null,
 BOOK_SUMMARY_ID          number(22,0) not null,
 UNIT_ITEM_ID             number(22,0) not null,
 UNITIZE_BY_ACCOUNT       number(22,0),
 UTILITY_ACCOUNT_ID       number(22,0),
 BUS_SEGMENT_ID           number(22,0),
 SUB_ACCOUNT_ID           number(22,0),
 COST_SOURCE              varchar2(35),
 ALLOC_STATISTIC          number(22,2),
 ALLOC_RATIO              number(22,8),
 ALLOC_RATIO_BY_UT        number(22,8),
 ALLOC_RATIO_BY_SUB       number(22,8),
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

comment on table  UNITIZE_RATIO_LATE_ADD_ARC is '(C) [04] Archive table of the data from the UNITIZE_RATIO_LATE_ADD_TEMP global temp table.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the addition expenditure type.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.BOOK_SUMMARY_ID is 'System-assigned identifier of the book summary of the late charge.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.COST_SOURCE is 'This field will indicate the ALLOC_STATISTIC being sourced from one of the following:  CPR_LEDGER, CPR_ACTIVITY, CPR_LDG_BASIS, or CPR_ACT_BASIS.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_RATIO_LATE_ADD_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_RATIO_LATE_RWIP_ARC
(
 UNIT_CLOSED_MONTH_NUMBER number(22,0) not null,
 RUN_TIME_STRING          varchar2(35) not null,
 RUN                      number(22,0) not null,
 COMPANY_ID               number(22,0) not null,
 WORK_ORDER_ID            number(22,0) not null,
 EXPENDITURE_TYPE_ID      number(22,0) not null,
 PROCESSING_TYPE_ID       number(22,0) not null,
 UNIT_ITEM_ID             number(22,0) not null,
 UNITIZE_BY_ACCOUNT       number(22,0),
 UTILITY_ACCOUNT_ID       number(22,0),
 BUS_SEGMENT_ID           number(22,0),
 SUB_ACCOUNT_ID           number(22,0),
 COST_SOURCE              varchar2(35),
 ALLOC_STATISTIC          number(22,2),
 ALLOC_RATIO              number(22,8),
 ALLOC_RATIO_BY_UT        number(22,8),
 ALLOC_RATIO_BY_SUB       number(22,8),
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

comment on table  UNITIZE_RATIO_LATE_RWIP_ARC is '(C) [04] Archive table of the data from the UNITIZE_RATIO_LATE_RWIP_TEMP global temp table.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the retirement expenditure type.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.PROCESSING_TYPE_ID is 'System-assigned identifier of the processing type. The processing type of the late charge determines if the late charge is Cost of Removal, Salvage Cash, etc.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.COST_SOURCE is 'This field will indicate the ALLOC_STATISTIC being sourced from one of the following, MATCHED, ALL, where MATCHED indicates same processing type from prior unitization used. ALL indicates total RWIP dollars used.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_RATIO_LATE_RWIP_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_ALLOC_LATE_BK_ARC
(
 UNIT_CLOSED_MONTH_NUMBER number(22,0) not null,
 RUN_TIME_STRING          varchar2(35) not null,
 RUN                      number(22,0) not null,
 COMPANY_ID               number(22,0) not null,
 WORK_ORDER_ID            number(22,0) not null,
 BOOK_SUMMARY_ID          number(22,0),
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

comment on table  UNITIZE_ALLOC_LATE_BK_ARC is '(C) [04] Archive table of the data from the UNITIZE_ALLOC_LATE_BK_TEMP global temp table.';
comment on column UNITIZE_ALLOC_LATE_BK_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_ALLOC_LATE_BK_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_ALLOC_LATE_BK_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_ALLOC_LATE_BK_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LATE_BK_ARC.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_LATE_BK_ARC.BOOK_SUMMARY_ID is 'System-assigned identifier of the book summary of the Addition late charge.';
comment on column UNITIZE_ALLOC_LATE_BK_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_LATE_BK_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_ALLOC_LATE_PT_ARC
(
 UNIT_CLOSED_MONTH_NUMBER number(22,0) not null,
 RUN_TIME_STRING          varchar2(35) not null,
 RUN                      number(22,0) not null,
 COMPANY_ID               number(22,0) not null,
 WORK_ORDER_ID            number(22,0) not null,
 PROCESSING_TYPE_ID       number(22,0),
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

comment on table  UNITIZE_ALLOC_LATE_PT_ARC is '(C) [04] Archive table of the data from the UNITIZE_ALLOC_LATE_PT_TEMP global temp table.';
comment on column UNITIZE_ALLOC_LATE_PT_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_ALLOC_LATE_PT_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_ALLOC_LATE_PT_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_ALLOC_LATE_PT_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LATE_PT_ARC.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_LATE_PT_ARC.PROCESSING_TYPE_ID is 'System-assigned identifier of the processing type of the RWIP late charge.';
comment on column UNITIZE_ALLOC_LATE_PT_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_LATE_PT_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_ALLOC_CGC_INSERTS_ARC
(
 UNIT_CLOSED_MONTH_NUMBER number(22,0) not null,
 RUN_TIME_STRING          varchar2(35) not null,
 RUN                      number(22,0) not null,
 ID                       number(22,0),
 COMPANY_ID               number(22,0) not null,
 ORIG_CHARGE_GROUP_ID     number(22,0) not null,
 NEW_CHARGE_GROUP_ID      number(22,0),
 WORK_ORDER_ID            number(22,0) not null,
 ALLOC_ID                 number(22,0),
 UNIT_ITEM_ID             number(22,0),
 UTILITY_ACCOUNT_ID       number(22,0),
 BUS_SEGMENT_ID           number(22,0),
 SUB_ACCOUNT_ID           number(22,0),
 CHARGE_TYPE_ID           number(22,0),
 EXPENDITURE_TYPE_ID      number(22,0),
 RETIREMENT_UNIT_ID       number(22,0),
 PROPERTY_GROUP_ID        number(22,0),
 ASSET_LOCATION_ID        number(22,0),
 SERIAL_NUMBER            varchar2(35),
 DESCRIPTION              varchar2(35),
 GROUP_INDICATOR          number(22,0),
 ALLOCATION_PRIORITY      number(22,0),
 UNITIZATION_SUMMARY      varchar2(35),
 BOOK_SUMMARY_NAME        varchar2(35),
 AMOUNT                   number(22,2) default 0,
 QUANTITY                 number(22,2) default 0,
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

comment on table UNITIZE_ALLOC_CGC_INSERTS_ARC is '(C) [04] Archive table of the data from the UNITIZE_ALLOC_CGC_INSERTS_TEMP global temp table.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.UNIT_CLOSED_MONTH_NUMBER is 'The YYYYMM format of the close month.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.RUN_TIME_STRING is 'The date and run time in string formate of the auto unitization run.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.RUN is 'The sequence number of the auto unitization run matching the Auto101 Errors.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.ID is 'System-assigned identifier of a particular record in the table.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.ORIG_CHARGE_GROUP_ID is 'System-assigned identifier of the charge_group_id being allocated.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.NEW_CHARGE_GROUP_ID is 'System-assigned identifier of the new charge_group_id created for the allocation results.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.UNIT_ITEM_ID is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utility account.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.CHARGE_TYPE_ID is 'System-assigned identifier of the charge type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.PROPERTY_GROUP_ID is 'System-assigned identifier of the property group.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.ASSET_LOCATION_ID is 'System-assigned identifier of the asset location.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.SERIAL_NUMBER is 'Serial Number.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.DESCRIPTION is 'Records a short description of the charge group (or charge).';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.GROUP_INDICATOR is 'Use null value for these records to indicate they are from allocation results, not from CWIP_CHARGE.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.ALLOCATION_PRIORITY is 'User-defined order (from 1 to n) that was used when the charge types were allocated.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.UNITIZATION_SUMMARY is 'Summary Name from BOOK_SUMMARY associated with the charge type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.BOOK_SUMMARY_NAME is 'Summary Name from BOOK_SUMMARY associated with the charge type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.AMOUNT is 'Dollar amount of allocated records.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.QUANTITY is 'Use 0 for quantity of the allocated records.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';


update PP_SYSTEM_CONTROL_COMPANY SET CONTROL_VALUE = 'Yes' where UPPER(CONTROL_NAME) = 'AUTO101 - FAST NO LOOP';

delete from PP_SYSTEM_CONTROL_COMPANY
 where UPPER(CONTROL_NAME) in ('UNITIZATION - NO UI FROM ACTUALS', 'PREGROUP CHARGES');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1495, 0, 10, 4, 3, 0, 39984, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039984_cwip_fast101.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
