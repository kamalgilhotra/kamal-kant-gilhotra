/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045083_lease_fix_co_lkup_impt_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 10/14/2015 Will Davis     Fix Lease Company Lookups
||============================================================================
*/

update pp_import_template_fields a
set import_lookup_id =
(select import_lookup_id from pp_import_lookup where upper(description) = 'LEASE COMPANY.DESCRIPTION')
where
import_type_id in (select import_type_id from pp_import_type_subsystem where import_subsystem_id =
                    (select IMPORT_SUBSYSTEM_ID from pp_import_subsystem where upper(description) = 'LESSEE'))
and column_name = 'company_id'
and not exists
(select 1
 from pp_import_type_subsystem s, pp_import_subsystem z
 where s.import_subsystem_id = z.import_subsystem_id
  and s.import_type_id = a.import_type_id
  and UPPER(z.description) <> 'LESSEE');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2920, 0, 2015, 2, 0, 0, 45083, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045083_lease_fix_co_lkup_impt_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
