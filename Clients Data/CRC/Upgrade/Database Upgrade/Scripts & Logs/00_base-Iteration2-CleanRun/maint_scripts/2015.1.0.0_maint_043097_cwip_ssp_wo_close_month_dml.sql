/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043097_cwip_ssp_wo_close_month_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 3/04/2015  Sarah Byers		creation of ssp process - Work order Close Month 
||============================================================================
*/

-- Use merge on the off chance that they already have a record called Work Order Close Month
MERGE INTO pp_processes p1
USING (
	SELECT Nvl(Max(process_id),0) + 1 AS process_id, 'Work Order Close Month' AS description,
		'Work Order Close Month Server Side Process' AS long_description, 'ssp_wo_close_month.exe' AS executable_file,
		'2015.1.0.0' AS version, 0 AS allow_concurrent, 1 AS wo_month_end_flag
	FROM pp_processes) p2
ON (Lower(Trim(p1.description)) = Lower(Trim(p2.description)))
WHEN MATCHED THEN
	UPDATE SET p1.long_description = p2.long_description,
		p1.executable_file = p2.executable_file,
		p1.version = p2.version,
		p1.allow_concurrent = p2.allow_concurrent,
		p1.wo_month_end_flag = p2.wo_month_end_flag
WHEN NOT MATCHED THEN
	INSERT (process_id, description, long_description, executable_file, version, allow_concurrent, wo_month_end_flag)
	VALUES (p2.process_id, p2.description, p2.long_description, p2.executable_file, p2.version, p2.allow_concurrent, p2.wo_month_end_flag)
;


INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
process_id,
1 AS option_id,
'Continue if Overheads and AFUDC have not been calculated?',
'If ''Yes'', the Work Order Close PowerPlant process will continue even if Overheads and AFUDC have not been calculated. If ''No'', Overheads and AFUDC must complete successfully prior to the month being closed, unless a user manually overrides this option via a prompt on the month end window.',
'No',
Sys_Context('USERENV','SESSION_USER'),
SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_close_month.exe';

INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
process_id,
2 AS option_id,
'Continue if Auto Unitization has not been completed?',
'If ''Yes'', the Work Order Close PowerPlant process will continue even if Auto Unitization has not been completed. If ''No'', Auto Unitization must complete successfully prior to the month being closed, unless a user manually overrides this option via a prompt on the month end window.',
'No',
Sys_Context('USERENV','SESSION_USER'),
SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_close_month.exe';

insert into wo_validation_type (
	wo_validation_type_id, description, long_description, "FUNCTION", find_company, col1, col2, hard_edit)
values (
	1060, 'WO - Close Month', 'Work Order Control - Close Month', 'various', 'select company_id from company_setup where company_id = <arg2>', 'month', 'company_id', 1);




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2349, 0, 2015, 1, 0, 0, 043097, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043097_cwip_ssp_wo_close_month_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;