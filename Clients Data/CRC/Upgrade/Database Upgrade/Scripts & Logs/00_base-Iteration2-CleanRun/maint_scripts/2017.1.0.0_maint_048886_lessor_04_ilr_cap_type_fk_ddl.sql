/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048886_lessor_04_ilr_cap_type_fk_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date      Revised By      Reason for Change
|| ---------- --------- --------------  ------------------------------------
|| 2017.1.0.0 9/25/2017 Shane "C" Ward  Rebuild FK to reference correct table
||============================================================================
*/

ALTER TABLE lsr_ilr_options DROP constraint fk_lsr_ilr_opt_cap_type;

ALTER TABLE lsr_ilr_options
  ADD CONSTRAINT fk_lsr_ilr_opt_cap_type FOREIGN KEY (
    lease_cap_type_id
  ) REFERENCES lsr_cap_type (
    cap_type_id
  )
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3741, 0, 2017, 1, 0, 0, 48886, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048886_lessor_04_ilr_cap_type_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;