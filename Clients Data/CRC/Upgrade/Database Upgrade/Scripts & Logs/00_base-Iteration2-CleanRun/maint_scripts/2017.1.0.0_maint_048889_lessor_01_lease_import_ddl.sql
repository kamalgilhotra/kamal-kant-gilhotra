/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048889_lessor_01_lease_import_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/04/2017 Shane "C" Ward		Set up Lessor Lease Import Tables
||============================================================================
*/

CREATE TABLE lsr_import_lease (
  import_run_id                NUMBER(22,0)   NOT NULL,
  line_id                      NUMBER(22,0)   NOT NULL,
  time_stamp                   DATE           NULL,
  user_id                      VARCHAR2(18)   NULL,
  lease_id                     NUMBER(22,0)   NULL,
  lease_number                 VARCHAR2(35)   NULL,
  description                  VARCHAR2(35)   NULL,
  long_description             VARCHAR2(254)  NULL,
  lessee_xlate                 VARCHAR2(254)  NULL,
  lessee_id                    NUMBER(22,0)   NULL,
  lease_type_xlate             VARCHAR2(254)  NULL,
  lease_type_id                NUMBER(22,0)   NULL,
  payment_due_day              NUMBER(22,0)   NULL,
  pre_payment_sw               VARCHAR2(35)   NULL,
  lease_group_xlate            VARCHAR2(254)  NULL,
  lease_group_id               NUMBER(22,0)   NULL,
  lease_cap_type_xlate         VARCHAR2(254)  NULL,
  lease_cap_type_id            NUMBER(22,0)   NULL,
  workflow_type_xlate          VARCHAR2(254)  NULL,
  workflow_type_id             NUMBER(22,0)   NULL,
  master_agreement_date        VARCHAR2(254)  NULL,
  notes                        VARCHAR2(4000) NULL,
  purchase_option_type_xlate   VARCHAR2(254)  NULL,
  purchase_option_type_id      NUMBER(22,0)   NULL,
  purchase_option_amt          NUMBER(22,2)   NULL,
  renewal_option_type_xlate    VARCHAR2(254)  NULL,
  renewal_option_type_id       NUMBER(22,0)   NULL,
  cancelable_type_xlate        VARCHAR2(254)  NULL,
  cancelable_type_id           NUMBER(22,0)   NULL,
  itc_sw                       VARCHAR2(35)   NULL,
  partial_retire_sw            VARCHAR2(35)   NULL,
  sublet_sw                    VARCHAR2(35)   NULL,
  company_xlate                VARCHAR2(254)  NULL,
  company_id                   NUMBER(22,0)   NULL,
  class_code_xlate1            VARCHAR2(254)  NULL,
  class_code_id1               NUMBER(22,0)   NULL,
  class_code_value1            VARCHAR2(254)  NULL,
  class_code_xlate2            VARCHAR2(254)  NULL,
  class_code_id2               NUMBER(22,0)   NULL,
  class_code_value2            VARCHAR2(254)  NULL,
  class_code_xlate3            VARCHAR2(254)  NULL,
  class_code_id3               NUMBER(22,0)   NULL,
  class_code_value3            VARCHAR2(254)  NULL,
  class_code_xlate4            VARCHAR2(254)  NULL,
  class_code_id4               NUMBER(22,0)   NULL,
  class_code_value4            VARCHAR2(254)  NULL,
  class_code_xlate5            VARCHAR2(254)  NULL,
  class_code_id5               NUMBER(22,0)   NULL,
  class_code_value5            VARCHAR2(254)  NULL,
  class_code_xlate6            VARCHAR2(254)  NULL,
  class_code_id6               NUMBER(22,0)   NULL,
  class_code_value6            VARCHAR2(254)  NULL,
  class_code_xlate7            VARCHAR2(254)  NULL,
  class_code_id7               NUMBER(22,0)   NULL,
  class_code_value7            VARCHAR2(254)  NULL,
  class_code_xlate8            VARCHAR2(254)  NULL,
  class_code_id8               NUMBER(22,0)   NULL,
  class_code_value8            VARCHAR2(254)  NULL,
  class_code_xlate9            VARCHAR2(254)  NULL,
  class_code_id9               NUMBER(22,0)   NULL,
  class_code_value9            VARCHAR2(254)  NULL,
  class_code_xlate10           VARCHAR2(254)  NULL,
  class_code_id10              NUMBER(22,0)   NULL,
  class_code_value10           VARCHAR2(254)  NULL,
  class_code_xlate11           VARCHAR2(254)  NULL,
  class_code_id11              NUMBER(22,0)   NULL,
  class_code_value11           VARCHAR2(254)  NULL,
  class_code_xlate12           VARCHAR2(254)  NULL,
  class_code_id12              NUMBER(22,0)   NULL,
  class_code_value12           VARCHAR2(254)  NULL,
  class_code_xlate13           VARCHAR2(254)  NULL,
  class_code_id13              NUMBER(22,0)   NULL,
  class_code_value13           VARCHAR2(254)  NULL,
  class_code_xlate14           VARCHAR2(254)  NULL,
  class_code_id14              NUMBER(22,0)   NULL,
  class_code_value14           VARCHAR2(254)  NULL,
  class_code_xlate15           VARCHAR2(254)  NULL,
  class_code_id15              NUMBER(22,0)   NULL,
  class_code_value15           VARCHAR2(254)  NULL,
  class_code_xlate16           VARCHAR2(254)  NULL,
  class_code_id16              NUMBER(22,0)   NULL,
  class_code_value16           VARCHAR2(254)  NULL,
  class_code_xlate17           VARCHAR2(254)  NULL,
  class_code_id17              NUMBER(22,0)   NULL,
  class_code_value17           VARCHAR2(254)  NULL,
  class_code_xlate18           VARCHAR2(254)  NULL,
  class_code_id18              NUMBER(22,0)   NULL,
  class_code_value18           VARCHAR2(254)  NULL,
  class_code_xlate19           VARCHAR2(254)  NULL,
  class_code_id19              NUMBER(22,0)   NULL,
  class_code_value19           VARCHAR2(254)  NULL,
  class_code_xlate20           VARCHAR2(254)  NULL,
  class_code_id20              NUMBER(22,0)   NULL,
  class_code_value20           VARCHAR2(254)  NULL,
  loaded                       NUMBER(22,0)   NULL,
  is_modified                  NUMBER(22,0)   NULL,
  error_message                VARCHAR2(4000) NULL,
  unique_lease_identifier      VARCHAR2(35)   NULL,
  days_in_year                 NUMBER(3,0)    NULL,
  cut_off_day                  NUMBER(2,0)    NULL,
  lease_end_date               VARCHAR2(254)  NULL,
  days_in_month_sw             VARCHAR2(35)   NULL,
  contract_currency_xlate      VARCHAR2(254)  NULL,
  contract_currency_id         NUMBER(22,0)   NULL,
  likely_to_collect           number(22,0) NULL,
  likely_to_collect_xlate VARCHAR2(35),
  specialized_asset NUMBER(22,0) NULL,
  specialized_asset_xlate VARCHAR2(35),
  intent_to_purchase_xlate VARCHAR2(35),
  intent_to_purchase NUMBER(22,0),
  sublease          NUMBER(22,0),
  sublease_xlate VARCHAR2(35),
  sublease_lease_id NUMBER(22,0),
  sublease_lease_xlate VARCHAR2(35)
);

ALTER TABLE lsr_import_lease
  ADD CONSTRAINT lsr_import_lease_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
;

PROMPT ALTER TABLE lsr_import_lease ADD CONSTRAINT lsr_import_lease_run_fk FOREIGN KEY
ALTER TABLE lsr_import_lease
  ADD CONSTRAINT lsr_import_lease_run_fk FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  );

COMMENT ON TABLE lsr_import_lease IS '(S)  [06]
The LSR Import Lease table is an API table used to import MLAs.';

COMMENT ON COLUMN lsr_import_lease.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN lsr_import_lease.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN lsr_import_lease.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_import_lease.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_import_lease.lease_id IS 'The internal lease id within PowerPlant .';
COMMENT ON COLUMN lsr_import_lease.lease_number IS 'User-defined number associated with an MLA. This is usually a reference to an external numbering system.';
COMMENT ON COLUMN lsr_import_lease.description IS 'a description field for the table.';
COMMENT ON COLUMN lsr_import_lease.long_description IS 'a long description field for the table.';
COMMENT ON COLUMN lsr_import_lease.lessee_xlate IS 'Translation field for determining the lessee.';
COMMENT ON COLUMN lsr_import_lease.lessee_id IS 'The internal lessee id within PowerPlant .';
COMMENT ON COLUMN lsr_import_lease.lease_type_xlate IS 'Translation field for determining the lease type.';
COMMENT ON COLUMN lsr_import_lease.lease_type_id IS 'The lease type.';
COMMENT ON COLUMN lsr_import_lease.payment_due_day IS 'The day of the month that payments are due under this MLA.';
COMMENT ON COLUMN lsr_import_lease.pre_payment_sw IS 'Indicates if pre payment of rent is allowable on this MLA.';
COMMENT ON COLUMN lsr_import_lease.lease_group_xlate IS 'Translation field for determining the lease group.';
COMMENT ON COLUMN lsr_import_lease.lease_group_id IS 'The lease group.';
COMMENT ON COLUMN lsr_import_lease.lease_cap_type_xlate IS 'Translation field for determining the lease capitalization type.';
COMMENT ON COLUMN lsr_import_lease.lease_cap_type_id IS 'Identifies how a lease is mapped to cpr basis buckets';
COMMENT ON COLUMN lsr_import_lease.workflow_type_xlate IS 'Translation field for determining the workflow type.';
COMMENT ON COLUMN lsr_import_lease.workflow_type_id IS 'An internal id used for routing records to approval.';
COMMENT ON COLUMN lsr_import_lease.master_agreement_date IS 'Effective start date for this MLA.';
COMMENT ON COLUMN lsr_import_lease.notes IS 'A freeform field for entering notes.';
COMMENT ON COLUMN lsr_import_lease.purchase_option_type_xlate IS 'Translation field for determining the purchase option type.';
COMMENT ON COLUMN lsr_import_lease.purchase_option_type_id IS 'Represents if this ILR has a purchase option.';
COMMENT ON COLUMN lsr_import_lease.purchase_option_amt IS 'The amount of the purchase option.';
COMMENT ON COLUMN lsr_import_lease.renewal_option_type_xlate IS 'Translation field for determining the renewal option type.';
COMMENT ON COLUMN lsr_import_lease.renewal_option_type_id IS 'Represents if this ILR has a renewal option.';
COMMENT ON COLUMN lsr_import_lease.cancelable_type_xlate IS 'Translation field for determining the cancelable type.';
COMMENT ON COLUMN lsr_import_lease.cancelable_type_id IS 'A PowerPlant internal id representing whether or not a lease can be cancelled before the lease terms are up.';
COMMENT ON COLUMN lsr_import_lease.itc_sw IS 'A flag identifying whether this ILR has investment tax credits associate with it.';
COMMENT ON COLUMN lsr_import_lease.partial_retire_sw IS 'A flag identifying whether or not partial retirements are allowed.';
COMMENT ON COLUMN lsr_import_lease.sublet_sw IS 'A flag identifying if the lease allows subletting.';
COMMENT ON COLUMN lsr_import_lease.company_xlate IS 'Translation field for determining the company.';
COMMENT ON COLUMN lsr_import_lease.company_id IS 'The company.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate1 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id1 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value1 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate2 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id2 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value2 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate3 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id3 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value3 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate4 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id4 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value4 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate5 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id5 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value5 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate6 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id6 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value6 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate7 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id7 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value7 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate8 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id8 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value8 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate9 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id9 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value9 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate10 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id10 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value10 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate11 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id11 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value11 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate12 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id12 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value12 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate13 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id13 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value13 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate14 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id14 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value14 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate15 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id15 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value15 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate16 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id16 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value16 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate17 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id17 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value17 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate18 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id18 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value18 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate19 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id19 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value19 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.class_code_xlate20 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_id20 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease.class_code_value20 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease.loaded IS 'System-assigned number to specify if this row has been loaded into its table.';
COMMENT ON COLUMN lsr_import_lease.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';
COMMENT ON COLUMN lsr_import_lease.error_message IS 'Error messages resulting from data valdiation in the import process.';
COMMENT ON COLUMN lsr_import_lease.unique_lease_identifier IS 'Unique identifier for this MLA, for this import run.  Used to link rows in this table that are under the same MLA.';
COMMENT ON COLUMN lsr_import_lease.days_in_year IS 'The number of days in year.';
COMMENT ON COLUMN lsr_import_lease.cut_off_day IS 'The cut-off day.';
COMMENT ON COLUMN lsr_import_lease.lease_end_date IS 'End date for any associated ILR payment terms to be within.';
COMMENT ON COLUMN lsr_import_lease.days_in_month_sw IS 'Switch indicating number of days in month used for interim interest. 0 is 30 days, 1 is actual days in month';
COMMENT ON COLUMN lsr_import_lease.likely_to_collect IS 'Switch indicating whether lease is likely to be collected';
COMMENT ON COLUMN lsr_import_lease.likely_to_collect_xlate IS 'Translation for switch indicating whether lease is likely to be collected';
COMMENT ON COLUMN lsr_import_lease.specialized_asset IS 'Switch indicating whether lease contains a specialized asset';
COMMENT ON COLUMN lsr_import_lease.specialized_asset_xlate IS 'Translation for witch indicating whether lease contains a specialized asset';
COMMENT ON COLUMN lsr_import_lease.intent_to_purchase_xlate IS 'Translation for switch indicating whether Lessee intends to Purchase at end of Lease';
COMMENT ON COLUMN lsr_import_lease.intent_to_purchase IS 'Switch indicating whether Lessee intends to Purchase at end of Lease';
COMMENT ON COLUMN lsr_import_lease.sublease IS 'Switch indicating whether lease is a Sublease';
COMMENT ON COLUMN lsr_import_lease.sublease_xlate IS 'Translation for switch indicating whether lease a Sublease';
COMMENT ON COLUMN lsr_import_lease.sublease_lease_id IS 'ID of lease that is the sublease of this Lease';
COMMENT ON COLUMN lsr_import_lease.sublease_lease_xlate IS 'Translation for ID of lease that is the sublease of this Lease';



CREATE TABLE lsr_import_lease_archive (
  import_run_id                NUMBER(22,0)   NOT NULL,
  line_id                      NUMBER(22,0)   NOT NULL,
  time_stamp                   DATE           NULL,
  user_id                      VARCHAR2(18)   NULL,
  lease_id                     NUMBER(22,0)   NULL,
  lease_number                 VARCHAR2(35)   NULL,
  description                  VARCHAR2(35)   NULL,
  long_description             VARCHAR2(254)  NULL,
  lessee_xlate                 VARCHAR2(254)  NULL,
  lessee_id                    NUMBER(22,0)   NULL,
  lease_type_xlate             VARCHAR2(254)  NULL,
  lease_type_id                NUMBER(22,0)   NULL,
  payment_due_day              NUMBER(22,0)   NULL,
  pre_payment_sw               VARCHAR2(35)   NULL,
  lease_group_xlate            VARCHAR2(254)  NULL,
  lease_group_id               NUMBER(22,0)   NULL,
  lease_cap_type_xlate         VARCHAR2(254)  NULL,
  lease_cap_type_id            NUMBER(22,0)   NULL,
  workflow_type_xlate          VARCHAR2(254)  NULL,
  workflow_type_id             NUMBER(22,0)   NULL,
  master_agreement_date        VARCHAR2(254)  NULL,
  notes                        VARCHAR2(4000) NULL,
  purchase_option_type_xlate   VARCHAR2(254)  NULL,
  purchase_option_type_id      NUMBER(22,0)   NULL,
  purchase_option_amt          NUMBER(22,2)   NULL,
  renewal_option_type_xlate    VARCHAR2(254)  NULL,
  renewal_option_type_id       NUMBER(22,0)   NULL,
  cancelable_type_xlate        VARCHAR2(254)  NULL,
  cancelable_type_id           NUMBER(22,0)   NULL,
  itc_sw                       VARCHAR2(35)   NULL,
  partial_retire_sw            VARCHAR2(35)   NULL,
  sublet_sw                    VARCHAR2(35)   NULL,
  company_xlate                VARCHAR2(254)  NULL,
  company_id                   NUMBER(22,0)   NULL,
  class_code_xlate1            VARCHAR2(254)  NULL,
  class_code_id1               NUMBER(22,0)   NULL,
  class_code_value1            VARCHAR2(254)  NULL,
  class_code_xlate2            VARCHAR2(254)  NULL,
  class_code_id2               NUMBER(22,0)   NULL,
  class_code_value2            VARCHAR2(254)  NULL,
  class_code_xlate3            VARCHAR2(254)  NULL,
  class_code_id3               NUMBER(22,0)   NULL,
  class_code_value3            VARCHAR2(254)  NULL,
  class_code_xlate4            VARCHAR2(254)  NULL,
  class_code_id4               NUMBER(22,0)   NULL,
  class_code_value4            VARCHAR2(254)  NULL,
  class_code_xlate5            VARCHAR2(254)  NULL,
  class_code_id5               NUMBER(22,0)   NULL,
  class_code_value5            VARCHAR2(254)  NULL,
  class_code_xlate6            VARCHAR2(254)  NULL,
  class_code_id6               NUMBER(22,0)   NULL,
  class_code_value6            VARCHAR2(254)  NULL,
  class_code_xlate7            VARCHAR2(254)  NULL,
  class_code_id7               NUMBER(22,0)   NULL,
  class_code_value7            VARCHAR2(254)  NULL,
  class_code_xlate8            VARCHAR2(254)  NULL,
  class_code_id8               NUMBER(22,0)   NULL,
  class_code_value8            VARCHAR2(254)  NULL,
  class_code_xlate9            VARCHAR2(254)  NULL,
  class_code_id9               NUMBER(22,0)   NULL,
  class_code_value9            VARCHAR2(254)  NULL,
  class_code_xlate10           VARCHAR2(254)  NULL,
  class_code_id10              NUMBER(22,0)   NULL,
  class_code_value10           VARCHAR2(254)  NULL,
  class_code_xlate11           VARCHAR2(254)  NULL,
  class_code_id11              NUMBER(22,0)   NULL,
  class_code_value11           VARCHAR2(254)  NULL,
  class_code_xlate12           VARCHAR2(254)  NULL,
  class_code_id12              NUMBER(22,0)   NULL,
  class_code_value12           VARCHAR2(254)  NULL,
  class_code_xlate13           VARCHAR2(254)  NULL,
  class_code_id13              NUMBER(22,0)   NULL,
  class_code_value13           VARCHAR2(254)  NULL,
  class_code_xlate14           VARCHAR2(254)  NULL,
  class_code_id14              NUMBER(22,0)   NULL,
  class_code_value14           VARCHAR2(254)  NULL,
  class_code_xlate15           VARCHAR2(254)  NULL,
  class_code_id15              NUMBER(22,0)   NULL,
  class_code_value15           VARCHAR2(254)  NULL,
  class_code_xlate16           VARCHAR2(254)  NULL,
  class_code_id16              NUMBER(22,0)   NULL,
  class_code_value16           VARCHAR2(254)  NULL,
  class_code_xlate17           VARCHAR2(254)  NULL,
  class_code_id17              NUMBER(22,0)   NULL,
  class_code_value17           VARCHAR2(254)  NULL,
  class_code_xlate18           VARCHAR2(254)  NULL,
  class_code_id18              NUMBER(22,0)   NULL,
  class_code_value18           VARCHAR2(254)  NULL,
  class_code_xlate19           VARCHAR2(254)  NULL,
  class_code_id19              NUMBER(22,0)   NULL,
  class_code_value19           VARCHAR2(254)  NULL,
  class_code_xlate20           VARCHAR2(254)  NULL,
  class_code_id20              NUMBER(22,0)   NULL,
  class_code_value20           VARCHAR2(254)  NULL,
  loaded                       NUMBER(22,0)   NULL,
  is_modified                  NUMBER(22,0)   NULL,
  error_message                VARCHAR2(4000) NULL,
  unique_lease_identifier      VARCHAR2(35)   NULL,
  days_in_year                 NUMBER(3,0)    NULL,
  cut_off_day                  NUMBER(2,0)    NULL,
  lease_end_date               VARCHAR2(254)  NULL,
  days_in_month_sw             VARCHAR2(35)   NULL,
  contract_currency_xlate      VARCHAR2(254)  NULL,
  contract_currency_id         NUMBER(22,0)   NULL,
  likely_to_collect           number(22,0) NULL,
  likely_to_collect_xlate VARCHAR2(35),
  specialized_asset NUMBER(22,0) NULL,
  specialized_asset_xlate VARCHAR2(35),
  intent_to_purchase_xlate VARCHAR2(35),
  intent_to_purchase NUMBER(22,0),
  sublease          NUMBER(22,0),
  sublease_xlate VARCHAR2(35),
  sublease_lease_id NUMBER(22,0),
  sublease_lease_xlate VARCHAR2(35)
);

ALTER TABLE lsr_import_lease_archive
  ADD CONSTRAINT lsr_import_lease_arc_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
;

PROMPT ALTER TABLE lsr_import_lease_archive ADD CONSTRAINT lsr_import_lease_archive_run_fk FOREIGN KEY
ALTER TABLE lsr_import_lease_archive
  ADD CONSTRAINT lsr_import_lease_arc_run_fk FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  );

COMMENT ON TABLE lsr_import_lease_archive IS '(S)  [06]
The LSR Import Lease table is an API table used to archive imported MLAs.';

COMMENT ON COLUMN lsr_import_lease_archive.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN lsr_import_lease_archive.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN lsr_import_lease_archive.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_import_lease_archive.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_import_lease_archive.lease_id IS 'The internal lease id within PowerPlant .';
COMMENT ON COLUMN lsr_import_lease_archive.lease_number IS 'User-defined number associated with an MLA. This is usually a reference to an external numbering system.';
COMMENT ON COLUMN lsr_import_lease_archive.description IS 'a description field for the table.';
COMMENT ON COLUMN lsr_import_lease_archive.long_description IS 'a long description field for the table.';
COMMENT ON COLUMN lsr_import_lease_archive.lessee_xlate IS 'Translation field for determining the lessee.';
COMMENT ON COLUMN lsr_import_lease_archive.lessee_id IS 'The internal lessee id within PowerPlant .';
COMMENT ON COLUMN lsr_import_lease_archive.lease_type_xlate IS 'Translation field for determining the lease type.';
COMMENT ON COLUMN lsr_import_lease_archive.lease_type_id IS 'The lease type.';
COMMENT ON COLUMN lsr_import_lease_archive.payment_due_day IS 'The day of the month that payments are due under this MLA.';
COMMENT ON COLUMN lsr_import_lease_archive.pre_payment_sw IS 'Indicates if pre payment of rent is allowable on this MLA.';
COMMENT ON COLUMN lsr_import_lease_archive.lease_group_xlate IS 'Translation field for determining the lease group.';
COMMENT ON COLUMN lsr_import_lease_archive.lease_group_id IS 'The lease group.';
COMMENT ON COLUMN lsr_import_lease_archive.lease_cap_type_xlate IS 'Translation field for determining the lease capitalization type.';
COMMENT ON COLUMN lsr_import_lease_archive.lease_cap_type_id IS 'Identifies how a lease is mapped to cpr basis buckets';
COMMENT ON COLUMN lsr_import_lease_archive.workflow_type_xlate IS 'Translation field for determining the workflow type.';
COMMENT ON COLUMN lsr_import_lease_archive.workflow_type_id IS 'An internal id used for routing records to approval.';
COMMENT ON COLUMN lsr_import_lease_archive.master_agreement_date IS 'Effective start date for this MLA.';
COMMENT ON COLUMN lsr_import_lease_archive.notes IS 'A freeform field for entering notes.';
COMMENT ON COLUMN lsr_import_lease_archive.purchase_option_type_xlate IS 'Translation field for determining the purchase option type.';
COMMENT ON COLUMN lsr_import_lease_archive.purchase_option_type_id IS 'Represents if this ILR has a purchase option.';
COMMENT ON COLUMN lsr_import_lease_archive.purchase_option_amt IS 'The amount of the purchase option.';
COMMENT ON COLUMN lsr_import_lease_archive.renewal_option_type_xlate IS 'Translation field for determining the renewal option type.';
COMMENT ON COLUMN lsr_import_lease_archive.renewal_option_type_id IS 'Represents if this ILR has a renewal option.';
COMMENT ON COLUMN lsr_import_lease_archive.cancelable_type_xlate IS 'Translation field for determining the cancelable type.';
COMMENT ON COLUMN lsr_import_lease_archive.cancelable_type_id IS 'A PowerPlant internal id representing whether or not a lease can be cancelled before the lease terms are up.';
COMMENT ON COLUMN lsr_import_lease_archive.itc_sw IS 'A flag identifying whether this ILR has investment tax credits associate with it.';
COMMENT ON COLUMN lsr_import_lease_archive.partial_retire_sw IS 'A flag identifying whether or not partial retirements are allowed.';
COMMENT ON COLUMN lsr_import_lease_archive.sublet_sw IS 'A flag identifying if the lease allows subletting.';
COMMENT ON COLUMN lsr_import_lease_archive.company_xlate IS 'Translation field for determining the company.';
COMMENT ON COLUMN lsr_import_lease_archive.company_id IS 'The company.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate1 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id1 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value1 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate2 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id2 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value2 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate3 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id3 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value3 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate4 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id4 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value4 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate5 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id5 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value5 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate6 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id6 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value6 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate7 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id7 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value7 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate8 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id8 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value8 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate9 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id9 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value9 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate10 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id10 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value10 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate11 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id11 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value11 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate12 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id12 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value12 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate13 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id13 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value13 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate14 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id14 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value14 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate15 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id15 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value15 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate16 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id16 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value16 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate17 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id17 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value17 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate18 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id18 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value18 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate19 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id19 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value19 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_xlate20 IS 'Translation field for determining the class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_id20 IS 'The class code.';
COMMENT ON COLUMN lsr_import_lease_archive.class_code_value20 IS 'The class code value for this record.';
COMMENT ON COLUMN lsr_import_lease_archive.loaded IS 'System-assigned number to specify if this row has been loaded into its table.';
COMMENT ON COLUMN lsr_import_lease_archive.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';
COMMENT ON COLUMN lsr_import_lease_archive.error_message IS 'Error messages resulting from data valdiation in the import process.';
COMMENT ON COLUMN lsr_import_lease_archive.unique_lease_identifier IS 'Unique identifier for this MLA, for this import run.  Used to link rows in this table that are under the same MLA.';
COMMENT ON COLUMN lsr_import_lease_archive.days_in_year IS 'The number of days in year.';
COMMENT ON COLUMN lsr_import_lease_archive.cut_off_day IS 'The cut-off day.';
COMMENT ON COLUMN lsr_import_lease_archive.lease_end_date IS 'End date for any associated ILR payment terms to be within.';
COMMENT ON COLUMN lsr_import_lease_archive.days_in_month_sw IS 'Switch indicating number of days in month used for interim interest. 0 is 30 days, 1 is actual days in month';
COMMENT ON COLUMN lsr_import_lease_archive.likely_to_collect IS 'Switch indicating whether lease is likely to be collected';
COMMENT ON COLUMN lsr_import_lease_archive.likely_to_collect_xlate IS 'Translation for switch indicating whether lease is likely to be collected';
COMMENT ON COLUMN lsr_import_lease_archive.specialized_asset IS 'Switch indicating whether lease contains a specialized asset';
COMMENT ON COLUMN lsr_import_lease_archive.specialized_asset_xlate IS 'Translation for witch indicating whether lease contains a specialized asset';
COMMENT ON COLUMN lsr_import_lease_archive.intent_to_purchase_xlate IS 'Translation for switch indicating whether Lessee intends to Purchase at end of Lease';
COMMENT ON COLUMN lsr_import_lease_archive.intent_to_purchase IS 'Switch indicating whether Lessee intends to Purchase at end of Lease';
COMMENT ON COLUMN lsr_import_lease_archive.sublease IS 'Switch indicating whether lease is a Sublease';
COMMENT ON COLUMN lsr_import_lease_archive.sublease_xlate IS 'Translation for switch indicating whether lease a Sublease';
COMMENT ON COLUMN lsr_import_lease_archive.sublease_lease_id IS 'ID of lease that is the sublease of this Lease';
COMMENT ON COLUMN lsr_import_lease_archive.sublease_lease_xlate IS 'Translation for ID of lease that is the sublease of this Lease';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3761, 0, 2017, 1, 0, 0, 48889, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048889_lessor_01_lease_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
