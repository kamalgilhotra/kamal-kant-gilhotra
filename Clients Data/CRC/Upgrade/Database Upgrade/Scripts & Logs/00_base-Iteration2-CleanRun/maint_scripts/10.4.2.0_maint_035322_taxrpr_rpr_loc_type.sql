/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035322_taxrpr_rpr_loc_type.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/13/2014 Alex P.        Point Release
||============================================================================
*/

create table REPAIR_LOCATION_TYPE
(
 repair_location_type_id number(22) not null,
 description             varchar2(35) not null,
 time_stamp              date,
 user_id                 varchar2(18)
);

alter table REPAIR_LOCATION_TYPE
   add constraint PK_RL_REPAIR_LOCATION_TYPE
       primary key (REPAIR_LOCATION_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

insert into REPAIR_LOCATION_TYPE
   (REPAIR_LOCATION_TYPE_ID, DESCRIPTION)
values
   (0, 'Non-Circuit Related');

insert into REPAIR_LOCATION_TYPE
   (REPAIR_LOCATION_TYPE_ID, DESCRIPTION)
values
   (1, 'Circuit Related');

comment on table REPAIR_LOCATION_TYPE is '(S) [18] The Repair Location Type table stores repair location categories used for organizational purposes. Default delivered entries are Circuit Related and Non-Circuit Related. The client can set up their own types via table maintenance screen.';
comment on column REPAIR_LOCATION_TYPE.repair_location_type_id is 'System-assigned identifier of repair location types.';
comment on column REPAIR_LOCATION_TYPE.description is 'Description of repair location types.';
comment on column REPAIR_LOCATION_TYPE.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column REPAIR_LOCATION_TYPE.USER_ID is 'Standard System-assigned user id used for audit purposes.';

alter table REPAIR_LOCATION add REPAIR_LOCATION_TYPE_ID number(22);

alter table REPAIR_LOCATION
   add constraint FK_RL_REPAIR_LOCATION_TYPE
       foreign key (REPAIR_LOCATION_TYPE_ID)
       references REPAIR_LOCATION_TYPE;

comment on column REPAIR_LOCATION_TYPE.REPAIR_LOCATION_TYPE_ID is 'System-assigned identifier of repair location types.';

update REPAIR_LOCATION set REPAIR_LOCATION_TYPE_ID = NVL(IS_CIRCUIT, 0);

alter table REPAIR_LOCATION modify REPAIR_LOCATION_TYPE_ID number(22) not null;
alter table REPAIR_LOCATION drop column IS_CIRCUIT;

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM_SCREEN)
values
   ('repair_location_type', 's', 'Repair Location Type',
    'Repair location categories used for organizational purposes. Default delivered entries are Circuit Related and Non-Circuit Related.',
    'always');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'repair_location_type', TO_DATE('09-01-2014 17:06:25', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'description', '<CLOB>', 0, null, null, null, null, null, null,
    null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('repair_location_type_id', 'repair_location_type',
    TO_DATE('09-01-2014 17:11:32', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', null, 's', null,
    'repair location type id', '<CLOB>', 1, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'repair_location_type', TO_DATE('09-01-2014 17:06:25', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'time stamp', '<CLOB>', 100, null, null, null, null, null, null,
    null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'repair_location_type', TO_DATE('09-01-2014 17:06:25', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'user id', '<CLOB>', 101, null, null, null, null, null, null, null,
    null);

insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_location',
          'repair_location_type_id',
          'uo_rpr_config_wksp_rpr_location.dw_details',
          'Repair Location Type'
     from PP_REQUIRED_TABLE_COLUMN;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (852, 0, 10, 4, 2, 0, 35322, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035322_taxrpr_rpr_loc_type.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;