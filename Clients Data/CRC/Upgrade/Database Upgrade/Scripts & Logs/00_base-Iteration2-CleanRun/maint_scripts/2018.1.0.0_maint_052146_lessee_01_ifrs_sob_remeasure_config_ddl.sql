/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052146_lessee_01_ifrs_sob_remeasure_config_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.1.0.0  09/05/2018 Shane "C" Ward    New table for setting up VP Escalations by Set of Books
||============================================================================
*/
--New Table for holding VP to SOB mapping for IFRS escalations
CREATE table ls_vp_escalation_sob_map
(variable_payment_id NUMBER(22,0) NOT NULL,
set_of_books_id NUMBER(22,0) NOT NULL,
include_in_esc NUMBER(1,0) DEFAULT 0,
user_id VARCHAR2(18),
time_stamp DATE);


ALTER TABLE ls_vp_escalation_sob_map
  ADD CONSTRAINT ls_vp_escalation_sob_map_pk PRIMARY KEY (
    variable_payment_id,
    set_of_books_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
;

ALTER TABLE ls_vp_escalation_sob_map
  ADD CONSTRAINT ls_vp_escalation_sob_map_fk1 FOREIGN KEY (
    variable_payment_id
  ) REFERENCES ls_variable_payment (
    variable_payment_id
  )
;


ALTER TABLE ls_vp_escalation_sob_map
  ADD CONSTRAINT ls_vp_escalation_sob_map_fk2 FOREIGN KEY (
    set_of_books_id
  ) REFERENCES set_of_books (
    set_of_books_id
  )
;

ALTER TABLE ls_vp_escalation_sob_map
  ADD CONSTRAINT ls_vp_escalation_sob_map_fk3 FOREIGN KEY (
    include_in_esc
  ) REFERENCES yes_no (
   yes_no_id
  )
;


COMMENT ON TABLE ls_vp_escalation_sob_map IS '(S) [06] The Lessee Variable Payment Escalation SOB Map table holds mapping of Set of Books to Variable Payments that when the VP is associated with an ILR the escalating payment will create a new remeasurement.';

COMMENT ON COLUMN ls_vp_escalation_sob_map.variable_payment_id IS 'System assigned ID uniquely identifying this specific variable payment.';
COMMENT ON COLUMN ls_vp_escalation_sob_map.set_of_books_id IS 'System assigned identifier of Set of books';
COMMENT ON COLUMN ls_vp_escalation_sob_map.include_in_esc IS 'Switch for whether Set of Books is an IFRS Set of Books for Remeasurement to be used with VP';
COMMENT ON COLUMN ls_vp_escalation_sob_map.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_vp_escalation_sob_map.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--New Column on Rent bucket Admin
ALTER TABLE ls_rent_bucket_admin ADD incl_in_ifrs_remeasure NUMBER(1) default 2;

COMMENT ON COLUMN ls_rent_bucket_admin.incl_in_ifrs_remeasure IS 'Flag for identifying the Contingnent Bucket to hold the IFRS Remeasurement Amounts. Only one Contingent bucket may be selected as active (1). Rest must be inactive (2)';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9522, 0, 2018, 1, 0, 0, 52146, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052146_lessee_01_ifrs_sob_remeasure_config_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	