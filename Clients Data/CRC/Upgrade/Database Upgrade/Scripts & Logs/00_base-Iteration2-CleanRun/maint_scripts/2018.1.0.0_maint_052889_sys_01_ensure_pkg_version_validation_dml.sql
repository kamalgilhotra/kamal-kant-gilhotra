 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_052889_sys_01_ensure_pkg_version_validation_dml.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2018.1.0.0  12/18/2018 Alex Healey   	running version validation for all packages that exist in 2018.1
 ||============================================================================
 */

insert into pp_package_versions
(package_name, version)
select distinct name, '2018.1.0.0' From sys.all_source source
where type = 'PACKAGE' 
and owner = 'PWRPLANT'
and not exists (select 1 from pp_package_versions pp_pack where pp_pack.package_name = source.name);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13182, 0, 2018, 1, 0, 0, 52889, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052889_sys_01_ensure_pkg_version_validation_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;