/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052543_lessee_01_add_payment_calc_month_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.3  10/22/2018 Sarah Byers      Add date field for payment calculations
||============================================================================
*/

alter table ls_payment_hdr add (payment_calc_month date);

comment on column ls_payment_hdr.payment_calc_month is 'The month in which the payments are calculated.';

CREATE OR REPLACE VIEW V_LS_PAYMENT_HDR_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code,
                1                                    historic_rate
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code,
                0
         FROM   currency company_cur)
SELECT lph.payment_id,
       lph.lease_id,
       lph.vendor_id,
       lph.company_id,
	   lph.amount                           original_amount,
       lph.amount *  Decode(ls_cur_type, 2, nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg_rate.rate, cr.rate), cur.historic_rate), 1) amount,	   
       lph.description,
       lph.gl_posting_mo_yr,
       lph.payment_status_id,
       lph.ap_status_id,
       lph.ls_asset_id,
       lph.ilr_id,
       lph.payment_month,
       lease.contract_currency_id,
       cs.currency_id                       company_currency_id,
       cur.ls_cur_type                      AS ls_cur_type,
       nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg_rate.accounting_month, cr.accounting_month), '01-Jan-1900') rate_acc_month,
       nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg_rate.exchange_date, cr.exchange_date), '01-Jan-1900') exchange_date,
       Decode(ls_cur_type, 2, Nvl(cr.rate, cur.historic_rate),
                           1)               rate,
       Decode(ls_cur_type, 2, nvl(decode(lower(sc.control_value), 'yes', cr_avg_rate.rate, cr.rate), cur.historic_rate),
                           1)               average_rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_payment_hdr lph
       inner join ls_lease lease
               ON lph.lease_id = lease.lease_id
       inner join currency_schema cs
               ON lph.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = lease.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       left outer join ls_lease_calculated_date_rates cr
                    ON ( cr.company_id = lph.company_id
                         AND cr.contract_currency_id =
                             lease.contract_currency_id
                         AND cr.company_currency_id = cs.currency_id
                         AND (cr.accounting_month = lph.payment_month or cr.accounting_month = lph.gl_posting_mo_yr)
                         AND Nvl(cr.exchange_rate_type_id, 1) = 1)
       left outer join ls_lease_calculated_date_rates cr_avg_rate
                 ON ( cr_avg_rate.company_id = lph.company_id
                      AND cr_avg_rate.contract_currency_id = 
                          lease.contract_currency_id
                      AND cr_avg_rate.company_currency_id = cs.currency_id
                      AND (cr_avg_rate.accounting_month = lph.payment_month or cr_avg_rate.accounting_month = lph.gl_posting_mo_yr)
                      AND Nvl(cr_avg_rate.exchange_rate_type_id, 4) = 4
                      AND cr.accounting_month = cr_avg_rate.accounting_month)
       inner join pp_system_control_companies sc
                 ON lph.company_id = sc.company_id AND 
                    lower(trim(sc.control_name)) = 'lease mc: use average rates'                          
WHERE cs.currency_type_id = 1;

CREATE OR REPLACE VIEW V_LS_PAYMENT_LINE_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code,
                1                                    historic_rate
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code,
                0
         FROM   currency company_cur)
SELECT lpl.payment_id,
       lpl.payment_line_number,
       lpl.payment_type_id,
       lpl.ls_asset_id,
       lpl.amount * Decode(ls_cur_type, 2, nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.rate, cr.rate), cur.historic_rate),
                                        1)                      amount,
       lpl.gl_posting_mo_yr,
       lpl.description,
       lpl.set_of_books_id,
       Round(lpl.adjustment_amount * Decode(ls_cur_type, 2,
                                     nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.rate, cr.rate), cur.historic_rate),
                                                         1), 2)
       adjustment_amount,
       lease.contract_currency_id,
       cs.currency_id
       company_currency_id,
       cur.ls_cur_type                                          AS ls_cur_type
       ,
       nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.accounting_month, cr.accounting_month), '01-Jan-1900') rate_acc_month,
       nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.exchange_date, cr.exchange_date), '01-Jan-1900')
       exchange_date,
       Decode(ls_cur_type, 2, nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.rate, cr.rate), cur.historic_rate),
                           1)                                   rate,
       cur.iso_code,
       cur.currency_display_symbol,
       lps.ap_paid_amount * Decode(ls_cur_type, 2, nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.rate, cr.rate), cur.historic_rate),
                                        1)                      ap_paid_amount 
FROM   ls_payment_line lpl
       inner join ls_payment_hdr lph
               ON lph.payment_id = lpl.payment_id
       left outer join LS_PAYMENTS_SENT_TO_AP lps
              ON (lpl.ls_asset_id = lps.ls_asset_id
              AND lpl.set_of_books_id = lps.set_of_books_id 
              AND lpl.gl_posting_mo_yr = lps.gl_posting_mo_yr 
              AND lpl.payment_type_id = lps.payment_type_id)
       inner join ls_lease lease
               ON lph.lease_id = lease.lease_id
       inner join currency_schema cs
               ON lph.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = lease.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       left outer join ls_lease_calculated_date_rates cr
                    ON ( cr.company_id = lph.company_id
                         AND cr.contract_currency_id =
                             lease.contract_currency_id
                         AND cr.company_currency_id = cs.currency_id
                         AND (cr.accounting_month = lph.payment_month or cr.accounting_month = lph.gl_posting_mo_yr)
                         AND Nvl(cr.exchange_rate_type_id, 1) = 1 )
       left outer join ls_lease_calculated_date_rates cr_avg
                    ON ( cr_avg.company_id = lph.company_id
                         AND cr_avg.contract_currency_id =
                             lease.contract_currency_id
                         AND cr_avg.company_currency_id = cs.currency_id
                         AND (cr_avg.accounting_month = lph.payment_month or cr_avg.accounting_month = lph.gl_posting_mo_yr)
                         AND Nvl(cr_avg.exchange_rate_type_id, 4) = 4 
                         AND cr.accounting_month = cr_avg.accounting_month) 
       inner join pp_system_control_companies sc
                 ON lph.company_id = sc.company_id AND 
                    lower(trim(sc.control_name)) = 'lease mc: use average rates'                 
WHERE  cs.currency_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10822, 0, 2017, 4, 0, 3, 52543, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052543_lessee_01_add_payment_calc_month_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;