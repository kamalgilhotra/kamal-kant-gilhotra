 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045807_pwrtax_tax_control_table_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 07/01/2016 Rob Burns      Initial script to remove Tax Control from Table Maintenance (Tables-Select)
 ||============================================================================
 */ 

update powerplant_tables set pp_table_type_id = 'o' where table_name = 'tax_control' and pp_table_type_id = 'st*';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3234, 0, 2016, 1, 0, 0, 045807, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045807_pwrtax_tax_control_table_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;