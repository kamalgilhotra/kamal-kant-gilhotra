/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045450_mappr_change_input_options_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 03/16/2016 Charlie Shilling change options on mobile alerts display window
||============================================================================
*/
UPDATE mobile_inputs
SET description = 'Estimated Dates'
WHERE input_id = 2;

INSERT INTO mobile_inputs (input_id, description)
VALUES (3, 'Complete Date');

INSERT INTO mobile_disp_inpts_mapping (display_id, input_id)
VALUES (1, 3);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3110, 0, 2016, 1, 0, 0, 045450, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045450_mappr_change_input_options_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;