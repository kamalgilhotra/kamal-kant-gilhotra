/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_02_lease_group_rates_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

--execute p_drop_table('ls_lease_rate_group');

CREATE TABLE ls_LEASE_RATE_GROUP (
  lease_id        NUMBER(22,0) NOT NULL,
  rate_group_id   NUMBER(22,0) NOT NULL,
  effective_date  DATE         NOT NULL,
  rate            NUMBER(22,8) NULL,
  time_stamp      DATE         NULL,
  user_id         VARCHAR2(18) NULL
)
/

ALTER TABLE ls_lease_rate_group
  ADD CONSTRAINT pk_ls_lease_rate_group PRIMARY KEY (
    lease_id,
    rate_group_id,
    effective_date
  )
using index tablespace pwrplant_idx
/

comment on table pwrplant.ls_lease_rate_group is 'ILR adjustments by group which are associated to the MLA.  Each ILR under the MLA can have a group associated with it to which interest rate adjustments apply.';
comment on column ls_lease_rate_group.lease_id is 'The unique lease identifier associated with a MLA.';
comment on column ls_lease_rate_group.rate_group_id is 'A grouping that ILR''s under the MLA are associated with.  The group dicatates the interest rate adjustment';
comment on column ls_lease_rate_group.effective_date is 'The effective date of the interest rate adjustment.';
comment on column ls_lease_rate_group.rate is 'The rate adjustment associated with the rate group';
comment on column ls_lease_rate_group.user_id IS 'Standard system-assigned user id user for audit purposes.';
comment on column ls_lease_rate_group.time_stamp IS 'Standard system-assigned timestamp user for audit purposes.';

alter table ls_ilr
add rate_group_id number(22,0);

comment on column ls_ilr.rate_group_id is 'The rate group associated with the ILR.  The rate groups are defined on the ls_lease_rate_group table';




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2417, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_02_lease_group_rates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;