/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_037243_lease_standard_jes.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/17/2014 Brandon Beck
||============================================================================
*/

insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select max(A.JE_ID) + 1,
          'LEASERESIDUAL',
          'LEASERESIDUAL',
          'Lease Residual Journals',
          'Lease Residual Journals'
     from STANDARD_JOURNAL_ENTRIES A;

insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select max(A.JE_ID) + 1,
          'LAMACC ',
          'LAMACC ',
          'Lease Accrual Journals',
          'Lease Accrual Journals'
     from STANDARD_JOURNAL_ENTRIES A;

insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select max(A.JE_ID) + 1,
          'LAMPAY ',
          'LAMPAY ',
          'Lease Payment Journals',
          'Lease Payment  Journals'
     from STANDARD_JOURNAL_ENTRIES A;

insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select max(A.JE_ID) + 1,
          'LAMRECLASS ',
          'LAMRECLASS ',
          'Lease Reclass Journals',
          'Lease Reclass Journals'
     from STANDARD_JOURNAL_ENTRIES A;

insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select max(A.JE_ID) + 1, 'LAMDEPR ', 'LAMDEPR ', 'Lease Depr Journals', 'Lease Depr Journals'
     from STANDARD_JOURNAL_ENTRIES A;

insert into GL_JE_CONTROL
   (PROCESS_ID, JE_ID, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN)
   select S.GL_JE_CODE, S.JE_ID, 'NONE', 'NONE', 'NONE', 'NONE'
     from STANDARD_JOURNAL_ENTRIES S
    where trim(S.GL_JE_CODE) in ('LEASERESIDUAL', 'LAMACC', 'LAMPAY', 'LAMRECLASS', 'LAMDEPR');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1062, 0, 10, 4, 2, 0, 37243, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037243_lease_standard_jes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
