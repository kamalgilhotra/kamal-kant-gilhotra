/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048891_lessor_05_lessee_import_misc_2_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/29/2017 Shane "C" Ward  Set up Lessor Lessee Import Tables
||============================================================================
*/

DELETE FROM pp_import_template_fields
WHERE import_template_id IN
  (SELECT import_template_id FROM
  pp_import_Template
  WHERE description = 'Lessor Lessee Add'
   AND import_type_id = 500);

UPDATE pp_import_lookup SET long_description = 'Lessee/Lessor County ID' WHERE import_lookup_id = 1048;

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (500,
             'county_id',
             1048);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             1,
             500,
             'unique_lessee_identifier',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             2,
             500,
             'description',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             3,
             500,
             'long_description',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             4,
             500,
             'zip',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             5,
             500,
             'county_id',
             1048);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             6,
             500,
             'state_id',
             1);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             7,
             500,
             'country_id',
             1031);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             8,
             500,
             'external_lessee_number',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             9,
             500,
             'lease_group_id',
             1075);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             10,
             500,
             'address1',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             11,
             500,
             'address2',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             12,
             500,
             'address3',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             13,
             500,
             'address4',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             14,
             500,
             'postal',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             15,
             500,
             'city',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             16,
             500,
             'phone_number',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             17,
             500,
             'extension',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 500 AND
                      description = 'Lessor Lessee Add' ),
             18,
             500,
             'fax_number',
             NULL);

			 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3772, 0, 2017, 1, 0, 0, 48891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048891_lessor_05_lessee_import_misc_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;			 
