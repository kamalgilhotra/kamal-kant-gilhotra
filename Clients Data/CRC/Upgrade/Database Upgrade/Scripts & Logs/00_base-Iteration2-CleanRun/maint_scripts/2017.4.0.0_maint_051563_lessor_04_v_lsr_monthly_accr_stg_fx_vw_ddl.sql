/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051563_lessor_04_v_lsr_monthly_accr_stg_fx_vw_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/28/2018 Powers       Add an AVERAGE_RATE field
||============================================================================
*/

CREATE OR REPLACE VIEW V_LSR_MONTHLY_ACCR_STG_FX_VW AS
WITH
cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
    SELECT
    acr.accrual_id,
    acr.accrual_type_id,
    acr.ilr_id,
    gl_posting_mo_yr,
    acr.set_of_books_id,
    acr.amount * Decode(ls_cur_type, 2, Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg_rate.rate, cr.rate), 1) amount,
    cur.ls_cur_type ls_cur_type,
    cr.exchange_date,
    lease.contract_currency_id,
    cur.currency_id display_currency_id,
    cs.currency_id company_currency_id,
    cur.iso_code,
    cur.currency_display_symbol,
    Decode(ls_cur_type, 2, cr.rate,1) rate,
    Decode(ls_cur_type, 2, decode(lower(sc.control_value), 'yes', cr_avg_rate.rate, cr.rate),1) average_rate 
FROM lsr_monthly_accrual_stg acr
INNER JOIN lsr_ilr ilr on acr.ilr_id = ilr.ilr_id
INNER JOIN currency_schema cs  ON ilr.company_id = cs.company_id
INNER JOIN lsr_lease lease  ON ilr.lease_id = lease.lease_id
INNER JOIN cur  ON (cur.ls_cur_type = 1 
    AND cur.currency_id = lease.contract_currency_id) 
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
LEFT OUTER JOIN ls_lease_calculated_date_rates cr ON lease.contract_currency_id = cr.contract_currency_id
  AND cur.currency_id = cr.company_currency_id
  AND ilr.company_id = cr.company_id
  AND acr.gl_posting_mo_yr = cr.accounting_month
left outer join ls_lease_calculated_date_rates cr_avg_rate ON ( cr_avg_rate.company_id = ilr.company_id
  AND cr_avg_rate.contract_currency_id = lease.contract_currency_id
  AND cr_avg_rate.company_currency_id = cs.currency_id
  AND cr_avg_rate.accounting_month = acr.gl_posting_mo_yr )
inner join pp_system_control_companies sc ON ilr.company_id = sc.company_id 
  AND lower(trim(sc.control_name)) = 'lease mc: use average rates'    
WHERE Nvl(cr.exchange_rate_type_id, 1) = 1 and Nvl(cr_avg_rate.exchange_rate_type_id, 4) = 4
AND cs.currency_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7545, 0, 2017, 4, 0, 0, 51563, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051563_lessor_04_v_lsr_monthly_accr_stg_fx_vw_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;