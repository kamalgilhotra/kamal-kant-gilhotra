/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_06_deferred_rent_sys_ctrls_dml.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding system controls for deferred rent JEs
||========================================================================================
*/

insert into pp_system_control(control_id, control_name, control_value, description, long_description, company_id)
select pwrplant1.nextval, 'Generate Deferred Rent JEs', 'No', 'dw_yes_no;1',
   '"YES" means that short term and long term deferred rent entries will be generated for uneven payment schedules on operating off-balance sheet leases, typically used for escalating and de-escalating payments. "No" means system will not journalize deferred rent entries. Default is "No"',
-1 from dual
where not exists(
   select 1
   from pp_system_control
   where control_name = 'Generate Lease Deferred Rent Entries')
   ;

insert into pp_system_control(control_id, control_name, control_value, description, long_description, company_id)
select pwrplant1.nextval, 'Generate Catch-up Deferred Rent JEs', 'No', 'dw_yes_no;1',
   '"YES" means that catch-up short term and long term deferred rent entries will be generated on late-added operating off balance sheet leases. "No" means system will not journalize catch-up deferred rent entries. Default is "No"',
-1 from dual
where not exists(
   select 1
   from pp_system_control
   where control_name = 'Generate Catch-up Lease Deferred Rent Entries')
   ;

   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3554, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_06_deferred_rent_sys_ctrls_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;   