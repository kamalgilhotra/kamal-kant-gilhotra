/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048891_lessor_01_lessee_import_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/29/2017 Shane "C" Ward		Set up Lessor Lessee Import Tables
||============================================================================
*/

-- Import Tables
CREATE TABLE lsr_import_lessee (
  import_run_id             NUMBER(22,0)   NOT NULL,
  line_id                   NUMBER(22,0)   NOT NULL,
  time_stamp                DATE           NULL,
  user_id                   VARCHAR2(18)   NULL,
  lessee_id                 NUMBER(22,0)   NULL,
  description               VARCHAR2(35)   NULL,
  long_description          VARCHAR2(254)  NULL,
  zip                       NUMBER(5,0)    NULL,
  county_xlate              VARCHAR2(254)  NULL,
  county_id                 CHAR(18)       NULL,
  state_xlate               VARCHAR2(254)  NULL,
  state_id                  CHAR(18)       NULL,
  country_xlate             VARCHAR2(254)  NULL,
  country_id                CHAR(18)       NULL,
  external_lessee_number    VARCHAR2(35)   NULL,
  lease_group_xlate         VARCHAR2(254)  NULL,
  lease_group_id            NUMBER(22,0)   NULL,
  address1                  VARCHAR2(35)   NULL,
  address2                  VARCHAR2(35)   NULL,
  address3                  VARCHAR2(35)   NULL,
  address4                  VARCHAR2(35)   NULL,
  postal                    NUMBER(4,0)    NULL,
  city                      VARCHAR2(35)   NULL,
  phone_number              VARCHAR2(18)   NULL,
  extension                 VARCHAR2(8)    NULL,
  fax_number                VARCHAR2(18)   NULL,
  site_code                 VARCHAR2(50)   NULL,
  loaded                    NUMBER(22,0)   NULL,
  is_modified               NUMBER(22,0)   NULL,
  error_message             VARCHAR2(4000) NULL,
  unique_lessee_identifier  VARCHAR2(35)   NULL
)
;

ALTER TABLE lsr_import_lessee
  ADD CONSTRAINT lsr_import_lessee_pk PRIMARY KEY (
    import_run_id,
    line_id
  )
;

ALTER TABLE lsr_import_lessee
  ADD CONSTRAINT lsr_import_lessee_run_fk FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  )
;

COMMENT ON TABLE lsr_import_lessee IS '(S)  [06]
The Lessor Import Lessee table is an API table used to import Lessees.';

COMMENT ON COLUMN lsr_import_lessee.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN lsr_import_lessee.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN lsr_import_lessee.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_import_lessee.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_import_lessee.lessee_id IS 'The internal Lessees id within PowerPlant .';
COMMENT ON COLUMN lsr_import_lessee.description IS 'a description field for the table.';
COMMENT ON COLUMN lsr_import_lessee.long_description IS 'a long description field for the table.';
COMMENT ON COLUMN lsr_import_lessee.zip IS 'Represents the first five digits of the Lessee''s postal zip code.';
COMMENT ON COLUMN lsr_import_lessee.county_xlate IS 'Translation field for determining the county.';
COMMENT ON COLUMN lsr_import_lessee.county_id IS 'Records the name of the particular county/parish where the Lessee is located.';
COMMENT ON COLUMN lsr_import_lessee.state_xlate IS 'Translation field for determining the state.';
COMMENT ON COLUMN lsr_import_lessee.state_id IS 'Identifies the Lessee''s state.';
COMMENT ON COLUMN lsr_import_lessee.country_xlate IS 'Translation field for determining the country.';
COMMENT ON COLUMN lsr_import_lessee.country_id IS 'Identifies the Lessee''s country.';
COMMENT ON COLUMN lsr_import_lessee.external_lessee_number IS 'Number associated with a Lessee. This is usually a reference to an external numbering system.';
COMMENT ON COLUMN lsr_import_lessee.lease_group_xlate IS 'Translation field for determining the lease group.';
COMMENT ON COLUMN lsr_import_lessee.lease_group_id IS 'The lease group.';
COMMENT ON COLUMN lsr_import_lessee.address1 IS 'Contains the first distinct component of the Lessee''s address.';
COMMENT ON COLUMN lsr_import_lessee.address2 IS 'Contains the second distinct component of the Lessee''s address.';
COMMENT ON COLUMN lsr_import_lessee.address3 IS 'Contains the third distinct component of the Lessee''s address.';
COMMENT ON COLUMN lsr_import_lessee.address4 IS 'Contains the fourth distinct component of the Lessee''s address.';
COMMENT ON COLUMN lsr_import_lessee.postal IS 'Represents the last four digits of the Lessee''s postal zip code.';
COMMENT ON COLUMN lsr_import_lessee.city IS 'Identifies the city component of the Lessee''s mailing address.';
COMMENT ON COLUMN lsr_import_lessee.phone_number IS 'Records the main phone number for the Lessee.';
COMMENT ON COLUMN lsr_import_lessee.extension IS 'Records the phone extension for a primary contact person employed by the Lessee.';
COMMENT ON COLUMN lsr_import_lessee.fax_number IS 'Records the Lessee''s fax number.';
COMMENT ON COLUMN lsr_import_lessee.site_code IS 'Unused.';
COMMENT ON COLUMN lsr_import_lessee.loaded IS 'System-assigned number to specify if this row has been loaded into its table.';
COMMENT ON COLUMN lsr_import_lessee.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';
COMMENT ON COLUMN lsr_import_lessee.error_message IS 'Error messages resulting from data valdiation in the import process.';
COMMENT ON COLUMN lsr_import_lessee.unique_lessee_identifier IS 'Unique identifier for this Lessee, for this import run.  Used to link rows in this table that are under the same Lessee.';

CREATE TABLE lsr_import_lessee_archive (
  import_run_id             NUMBER(22,0)   NOT NULL,
  line_id                   NUMBER(22,0)   NOT NULL,
  time_stamp                DATE           NULL,
  user_id                   VARCHAR2(18)   NULL,
  lessee_id                 NUMBER(22,0)   NULL,
  description               VARCHAR2(35)   NULL,
  long_description          VARCHAR2(254)  NULL,
  zip                       NUMBER(5,0)    NULL,
  county_xlate              VARCHAR2(254)  NULL,
  county_id                 CHAR(18)       NULL,
  state_xlate               VARCHAR2(254)  NULL,
  state_id                  CHAR(18)       NULL,
  country_xlate             VARCHAR2(254)  NULL,
  country_id                CHAR(18)       NULL,
  external_lessee_number    VARCHAR2(35)   NULL,
  lease_group_xlate         VARCHAR2(254)  NULL,
  lease_group_id            NUMBER(22,0)   NULL,
  address1                  VARCHAR2(35)   NULL,
  address2                  VARCHAR2(35)   NULL,
  address3                  VARCHAR2(35)   NULL,
  address4                  VARCHAR2(35)   NULL,
  postal                    NUMBER(4,0)    NULL,
  city                      VARCHAR2(35)   NULL,
  phone_number              VARCHAR2(18)   NULL,
  extension                 VARCHAR2(8)    NULL,
  fax_number                VARCHAR2(18)   NULL,
  site_code                 VARCHAR2(50)   NULL,
  loaded                    NUMBER(22,0)   NULL,
  is_modified               NUMBER(22,0)   NULL,
  error_message             VARCHAR2(4000) NULL,
  unique_lessee_identifier  VARCHAR2(35)   NULL
)
;

COMMENT ON TABLE lsr_import_lessee_archive IS '(S)  [06]
The Lessor Import Lessee table is an API table used to import Lessees.';

COMMENT ON COLUMN lsr_import_lessee_archive.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN lsr_import_lessee_archive.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN lsr_import_lessee_archive.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_import_lessee_archive.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_import_lessee_archive.lessee_id IS 'The internal Lessees id within PowerPlant .';
COMMENT ON COLUMN lsr_import_lessee_archive.description IS 'a description field for the table.';
COMMENT ON COLUMN lsr_import_lessee_archive.long_description IS 'a long description field for the table.';
COMMENT ON COLUMN lsr_import_lessee_archive.zip IS 'Represents the first five digits of the Lessee''s postal zip code.';
COMMENT ON COLUMN lsr_import_lessee_archive.county_xlate IS 'Translation field for determining the county.';
COMMENT ON COLUMN lsr_import_lessee_archive.county_id IS 'Records the name of the particular county/parish where the Lessee is located.';
COMMENT ON COLUMN lsr_import_lessee_archive.state_xlate IS 'Translation field for determining the state.';
COMMENT ON COLUMN lsr_import_lessee_archive.state_id IS 'Identifies the Lessee''s state.';
COMMENT ON COLUMN lsr_import_lessee_archive.country_xlate IS 'Translation field for determining the country.';
COMMENT ON COLUMN lsr_import_lessee_archive.country_id IS 'Identifies the Lessee''s country.';
COMMENT ON COLUMN lsr_import_lessee_archive.external_lessee_number IS 'Number associated with a Lessee. This is usually a reference to an external numbering system.';
COMMENT ON COLUMN lsr_import_lessee_archive.lease_group_xlate IS 'Translation field for determining the lease group.';
COMMENT ON COLUMN lsr_import_lessee_archive.lease_group_id IS 'The lease group.';
COMMENT ON COLUMN lsr_import_lessee_archive.address1 IS 'Contains the first distinct component of the Lessee''s address.';
COMMENT ON COLUMN lsr_import_lessee_archive.address2 IS 'Contains the second distinct component of the Lessee''s address.';
COMMENT ON COLUMN lsr_import_lessee_archive.address3 IS 'Contains the third distinct component of the Lessee''s address.';
COMMENT ON COLUMN lsr_import_lessee_archive.address4 IS 'Contains the fourth distinct component of the Lessee''s address.';
COMMENT ON COLUMN lsr_import_lessee_archive.postal IS 'Represents the last four digits of the Lessee''s postal zip code.';
COMMENT ON COLUMN lsr_import_lessee_archive.city IS 'Identifies the city component of the Lessee''s mailing address.';
COMMENT ON COLUMN lsr_import_lessee_archive.phone_number IS 'Records the main phone number for the Lessee.';
COMMENT ON COLUMN lsr_import_lessee_archive.extension IS 'Records the phone extension for a primary contact person employed by the Lessee.';
COMMENT ON COLUMN lsr_import_lessee_archive.fax_number IS 'Records the Lessee''s fax number.';
COMMENT ON COLUMN lsr_import_lessee_archive.site_code IS 'Unused.';
COMMENT ON COLUMN lsr_import_lessee_archive.loaded IS 'System-assigned number to specify if this row has been loaded into its table.';
COMMENT ON COLUMN lsr_import_lessee_archive.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';
COMMENT ON COLUMN lsr_import_lessee_archive.error_message IS 'Error messages resulting from data valdiation in the import process.';
COMMENT ON COLUMN lsr_import_lessee_archive.unique_lessee_identifier IS 'Unique identifier for this Lessee, for this import run.  Used to link rows in this table that are under the same Lessee.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3746, 0, 2017, 1, 0, 0, 48891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048891_lessor_01_lessee_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;