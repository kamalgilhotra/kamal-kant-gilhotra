/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047986_lessor_lsr_lessee_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ------------------------------------
|| 2017.1.0.0 08/22/2017 Johnny Sisouphanh Create lsr_lessee table
||============================================================================
*/

CREATE TABLE lsr_lessee
  (
  lessee_id NUMBER(22,0) NOT NULL , 
  time_stamp DATE, 
  user_id VARCHAR2(18), 
  description VARCHAR2(35) NOT NULL , 
  long_description VARCHAR2(254) NOT NULL , 
  address1 VARCHAR2(35), 
  address2 VARCHAR2(35), 
  address3 VARCHAR2(35), 
  address4 VARCHAR2(35), 
  zip NUMBER(5,0), 
  postal NUMBER(4,0), 
  city VARCHAR2(35), 
  county_id CHAR(18), 
  state_id CHAR(18), 
  country_id CHAR(18), 
  phone_number VARCHAR2(18), 
  extension VARCHAR2(8), 
  fax_number VARCHAR2(18), 
  site_code VARCHAR2(50), 
  external_lessee_number VARCHAR2(35), 
  status_code_id NUMBER(22,0), 
  lease_group_id NUMBER(22,0)
  );
  
ALTER TABLE lsr_lessee ADD   
  CONSTRAINT pk_lsr_lessee PRIMARY KEY (lessee_id) USING INDEX tablespace pwrplant_idx;

ALTER TABLE lsr_lessee ADD
   CONSTRAINT fk_lsr_lessee_country FOREIGN KEY (country_id) REFERENCES country (country_id);

ALTER TABLE lsr_lessee ADD   
   CONSTRAINT  fk_lsr_lessee_county_state FOREIGN KEY (county_id, state_id)  REFERENCES county (county_id, state_id);

COMMENT ON TABLE lsr_lessee  IS '(S) [06] The Lessee data table maintains contact information for lessee companies.';
COMMENT ON COLUMN lsr_lessee.lessee_id IS 'System-assigned identifier of a particular lessee.';
COMMENT ON COLUMN lsr_lessee.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_lessee.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_lessee.description IS 'Records a brief description of the lessee company.';
COMMENT ON COLUMN lsr_lessee.long_description IS 'Records a more detailed description of the lessee company.';
COMMENT ON COLUMN lsr_lessee.address1 IS 'Contains the first distinct component of the lessee''s address.';
COMMENT ON COLUMN lsr_lessee.address2 IS 'Contains the second distinct component of the lessee''s address.';
COMMENT ON COLUMN lsr_lessee.address3 IS 'Contains the third distinct component of the lessee''s address.';
COMMENT ON COLUMN lsr_lessee.address4 IS 'Contains the fourth distinct component of the lessee''s address.';
COMMENT ON COLUMN lsr_lessee.zip IS 'Represents the first five digits of the lessee''s postal zip code.';
COMMENT ON COLUMN lsr_lessee.postal IS 'Represents the last four digits of the lessee''s postal zip code.';
COMMENT ON COLUMN lsr_lessee.city IS 'Identifies the city component of the lessee''s mailing address.';
COMMENT ON COLUMN lsr_lessee.county_id IS 'Records the name of the particular county/parish where the lessee is located.';
COMMENT ON COLUMN lsr_lessee.state_id IS 'Records the name of the county''s state.';
COMMENT ON COLUMN lsr_lessee.country_id IS 'Identifies the lessee''s country.';
COMMENT ON COLUMN lsr_lessee.phone_number IS 'Records the main phone NUMBER for the lessee.';
COMMENT ON COLUMN lsr_lessee.extension IS 'Records the phone extension for a primary contact person employed by the lessee.';
COMMENT ON COLUMN lsr_lessee.fax_number IS 'Records the lessee''s fax NUMBER.';
COMMENT ON COLUMN lsr_lessee.site_code IS 'Represents the site at which a particular lessee is paid.';
COMMENT ON COLUMN lsr_lessee.external_lessee_number IS 'A unique user generated identifier of a particular lessee.';
COMMENT ON COLUMN lsr_lessee.status_code_id IS 'A NUMBER indicating whether a lessee is active or inactive.';
COMMENT ON COLUMN lsr_lessee.lease_group_id IS 'A system generated identifier indicating under which lease group a lessee falls.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3670, 0, 2017, 1, 0, 0, 47986, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047986_lessor_lsr_lessee_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;