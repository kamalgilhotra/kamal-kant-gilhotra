/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052148_lessee_01_pending_remeasure_ws_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 09/21/2018 Shane "C" Ward 	New Lessee Pending Remeasurement Workspace
||============================================================================
*/

INSERT INTO ppbase_workspace
  (module, workspace_identifier, label, workspace_uo_name, object_type_id)
  SELECT 'LESSEE',
         'control_pending_rem',
         'Pending Remeasurements',
         'uo_ls_pending_remeasure_ws',
         1
    FROM dual
   WHERE NOT EXISTS (SELECT 1 FROM ppbase_workspace WHERE workspace_identifier = 'control_pending_rem');

INSERT INTO ppbase_menu_items
  (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
  SELECT 'LESSEE',
         'control_pending_rem',
         2,
         5,
         'Pending Remeasurements',
         'menu_wksp_control',
         'control_pending_rem',
         1
    FROM dual
   WHERE NOT EXISTS (SELECT 1 FROM ppbase_menu_items WHERE menu_identifier = 'control_pending_rem');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9842, 0, 2018, 1, 0, 0, 52148, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052148_lessee_01_pending_remeasure_ws_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;