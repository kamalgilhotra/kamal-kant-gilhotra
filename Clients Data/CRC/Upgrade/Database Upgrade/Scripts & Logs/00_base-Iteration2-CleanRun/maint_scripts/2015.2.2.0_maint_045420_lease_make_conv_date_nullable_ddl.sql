/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045420_lease_make_conv_date_nullable_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 02/17/2016 Sarah Byers  	 Change ls_forecast_version.conversion_date to nullable
|| 2015.2.2.0 04/14/2016 David Haupt	 Backport to 2015.2.2
||============================================================================
*/

alter table ls_forecast_version modify (conversion_date date null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3062, 0, 2015, 2, 2, 0, 045420, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045420_lease_make_conv_date_nullable_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;