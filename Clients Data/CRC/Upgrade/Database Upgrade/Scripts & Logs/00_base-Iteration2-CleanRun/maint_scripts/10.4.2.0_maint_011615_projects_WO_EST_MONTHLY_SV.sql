/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011615_projects_WO_EST_MONTHLY_SV.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/08/2013 Stephen Motter Point Release
||============================================================================
*/

create or replace view WO_EST_MONTHLY_SV as
select W.EST_MONTHLY_ID EST_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.REVISION REVISION,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) TOTAL,
       W.UTILITY_ACCOUNT_ID UTILITY_ACCOUNT_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WO_WORK_ORDER_ID WO_WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID,
       W.SUBSTITUTION_ID SUBSTITUTION_ID,
       NVL(W.HRS_JAN, 0) HRS_PERIOD1,
       NVL(W.HRS_FEB, 0) HRS_PERIOD2,
       NVL(W.HRS_MAR, 0) HRS_PERIOD3,
       NVL(W.HRS_APR, 0) HRS_PERIOD4,
       NVL(W.HRS_MAY, 0) HRS_PERIOD5,
       NVL(W.HRS_JUN, 0) HRS_PERIOD6,
       NVL(W.HRS_JUL, 0) HRS_PERIOD7,
       NVL(W.HRS_AUG, 0) HRS_PERIOD8,
       NVL(W.HRS_SEP, 0) HRS_PERIOD9,
       NVL(W.HRS_OCT, 0) HRS_PERIOD10,
       NVL(W.HRS_NOV, 0) HRS_PERIOD11,
       NVL(W.HRS_DEC, 0) HRS_PERIOD12,
       NVL(W.HRS_TOTAL, 0) HRS_TOTAL,
       NVL(W.QTY_JAN, 0) QTY_PERIOD1,
       NVL(W.QTY_FEB, 0) QTY_PERIOD2,
       NVL(W.QTY_MAR, 0) QTY_PERIOD3,
       NVL(W.QTY_APR, 0) QTY_PERIOD4,
       NVL(W.QTY_MAY, 0) QTY_PERIOD5,
       NVL(W.QTY_JUN, 0) QTY_PERIOD6,
       NVL(W.QTY_JUL, 0) QTY_PERIOD7,
       NVL(W.QTY_AUG, 0) QTY_PERIOD8,
       NVL(W.QTY_SEP, 0) QTY_PERIOD9,
       NVL(W.QTY_OCT, 0) QTY_PERIOD10,
       NVL(W.QTY_NOV, 0) QTY_PERIOD11,
       NVL(W.QTY_DEC, 0) QTY_PERIOD12,
       NVL(W.QTY_TOTAL, 0) QTY_TOTAL
  from WO_EST_MONTHLY W, ESTIMATE_CHARGE_TYPE ECT
 where W.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and W.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and ECT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(ECT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (676, 0, 10, 4, 2, 0, 11615, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_011615_projects_WO_EST_MONTHLY_SV.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;