/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045073_sys_add_ppsys_intfc_cust_2_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/27/2015 Sarah Byers    Add entries for ppsystem_interface_custom.pbd to pp_custom_pbd_versions
||============================================================================
*/

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'ppsystem_interface_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(executable_file) in ('pp_integration.exe');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2953, 0, 2015, 2, 0, 0, 45073, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045073_sys_add_ppsys_intfc_cust_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

