/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041551_pcm_forecast_attributes_dml.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	12/02/2014 Chris Mardis	       Dynamic project forecasting attributes
||========================================================================================
*/

delete from wo_est_forecast_attributes;
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Company', 'woc.company_id', 'co.description', 'woc.company_id, co.description', 'company', 'woc.company_id', 'co.description', 'woc.company_id,co.description', 'Company', 'woc.company_id', '', 1); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Funding Project', 'woc.work_order_id', 'woc.work_order_number || '' - '' || woc.description', 'woc.work_order_id, woc.work_order_number || '' - '' || woc.description', 'funding_project', 'woc.work_order_id', 'woc.work_order_number || '' - '' || woc.description', 'woc.work_order_id,woc.work_order_number || '' - '' || woc.description', 'Funding Project', 'woc.work_order_id', '', 1); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Estimated Charge Type', 'wem.est_chg_type_id', 'ect.description', 'wem.est_chg_type_id, ect.description', 'est_chg_type', 'ectwo.funding_chg_type', 'ect.description', 'ectwo.funding_chg_type,ect.description', 'Est Charge Type', '', 'est_chg_type_id', 1); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Job Task (List)', 'wem.job_task_id', 'nvl((select max(jtl.job_task_id || '' - '' || jtl.description) from job_task_list jtl where jtl.job_task_id = wem.job_task_id),wem.job_task_id)', 'wem.job_task_id', 'job_task', 'wem.job_task_id', 'nvl((select max(jtl.job_task_id || '' - '' || jtl.description) from job_task_list jtl where jtl.job_task_id = wem.job_task_id),wem.job_task_id)', 'wem.job_task_id', 'Job Task', '', 'job_task_id', (select nvl(max(1),0) from pp_system_control where lower(control_name) = lower('FP Ests-Job Task Source Table') and lower(control_value) = 'job_task_list')); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Expenditure Type', 'wem.expenditure_type_id', 'et.description', 'wem.expenditure_type_id, et.description', 'expenditure_type', 'wem.expenditure_type_id', 'et.description', 'wem.expenditure_type_id,et.description', 'Expenditure Type', '', 'expenditure_type_id', 1); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Department', 'wem.department_id', 'nvl2(dept.external_department_code,dept.external_department_code||'' - '','''') || dept.description', 'wem.department_id, nvl2(dept.external_department_code,dept.external_department_code||'' - '','''') || dept.description', 'department', 'wem.department_id', 'nvl2(dept.external_department_code,dept.external_department_code||'' - '','''') || dept.description', 'wem.department_id,nvl2(dept.external_department_code,dept.external_department_code||'' - '','''') || dept.description', 'Department', '', 'department_id', 1); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Budget Plant Class', 'wem.utility_account_id', 'bpc.description', 'wem.utility_account_id, bpc.description', 'utility_account', 'wem.utility_account_id', 'bpc.description', 'wem.utility_account_id,bpc.description', 'Budget Plant Class', '', 'utility_account_id', 1); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Budget Item', 'woc.budget_id', 'bud.budget_number || '' - '' || bud.description', 'woc.budget_id, bud.budget_number || '' - '' || bud.description', 'budget', 'woc.budget_id', 'bud.budget_number || '' - '' || bud.description', 'woc.budget_id,bud.budget_number || '' - '' || bud.description', 'Budget Item', 'woc.budget_id', '', 1); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Work Order', 'wem.wo_work_order_id', 'wowoc.work_order_number || nvl2(wowoc.work_order_number,'' - '','''') || wowoc.description', 'wem.wo_work_order_id, wowoc.work_order_number || nvl2(wowoc.work_order_number,'' - '','''') || wowoc.description', 'wo_work_order', 'wem.work_order_id', 'wo.work_order_number || '' - '' || wo.description', 'wem.work_order_id,wo.work_order_number || '' - '' || wo.description', 'Work Order', '', 'wo_work_order_id', 1); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Job Task (FP)', 'wem.job_task_id', 'nvl((select max(jt.job_task_id || '' - '' || jt.description) from job_task jt where jt.work_order_id = wem.work_order_id and jt.job_task_id = wem.job_task_id),wem.job_task_id)', 'wem.work_order_id, wem.job_task_id', 'job_task', 'wem.job_task_id', 'nvl((select max(jt.job_task_id || '' - '' || jt.description) from job_task jt, work_order_control w where jt.work_order_id = w.funding_wo_id and w.work_order_id = wem.work_order_id and jt.job_task_id = wem.job_task_id),wem.job_task_id)', 'wem.work_order_id, wem.job_task_id', 'Job Task', '', 'job_task_id', (select nvl(max(1),0) from pp_system_control where lower(control_name) = lower('FP Ests-Job Task Source Table') and lower(control_value) = 'job_task')); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Job Task (WO)', 'wem.job_task_id', 'nvl((select max(jt.job_task_id || '' - '' || jt.description) from job_task jt, work_order_control w where jt.work_order_id = w.work_order_id and jt.job_task_id = wem.job_task_id and w.funding_wo_id = wem.work_order_id and w.work_order_id = nvl(wem.wo_work_order_id,w.work_order_id)),wem.job_task_id)', 'wem.work_order_id, wem.wo_work_order_id, wem.job_task_id', 'job_task', 'wem.job_task_id', 'nvl((select max(jt.job_task_id || '' - '' || jt.description) from job_task jt where jt.work_order_id = wem.work_order_id and jt.job_task_id = wem.job_task_id),wem.job_task_id)', 'wem.work_order_id, wem.job_task_id', 'Job Task', '', 'job_task_id', (select nvl(max(1),0) from pp_system_control where lower(control_name) = lower('FP Ests-Job Task Source Table') and lower(control_value) = 'job_task_wo')); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active)
values ('Job Task Rollup (WO)',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a, work_order_control w where a.work_order_id = w.work_order_id and w.funding_wo_id = wem.work_order_id and w.work_order_id = nvl(wem.wo_work_order_id,w.work_order_id) and a.forecast = 1 and level = (select min(level) min_lvl from job_task b, work_order_control x where b.work_order_id = x.work_order_id and x.funding_wo_id = wem.work_order_id and x.work_order_id = nvl(wem.wo_work_order_id,x.work_order_id) and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id ),wem.job_task_id)',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a, work_order_control w where a.work_order_id = w.work_order_id and w.funding_wo_id = wem.work_order_id and w.work_order_id = nvl(wem.wo_work_order_id,w.work_order_id) and a.forecast = 1 and level = (select min(level) min_lvl from job_task b, work_order_control x where b.work_order_id = x.work_order_id and x.funding_wo_id = wem.work_order_id and x.work_order_id = nvl(wem.wo_work_order_id,x.work_order_id) and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id ),wem.job_task_id)',
   'wem.work_order_id, wem.wo_work_order_id, wem.job_task_id',
   'jt_rollup',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a where a.work_order_id = wem.work_order_id and a.forecast = 1 and level = (select min(level) min_lvl from job_task b where b.work_order_id = wem.work_order_id and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id ),wem.job_task_id)',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a where a.work_order_id = wem.work_order_id and a.forecast = 1 and level = (select min(level) min_lvl from job_task b where b.work_order_id = wem.work_order_id and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id ),wem.job_task_id)',
   'wem.work_order_id, wem.job_task_id',
   'Job Task Rollup',
   '',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a, work_order_control w where a.work_order_id = w.work_order_id and w.funding_wo_id = wem.work_order_id and w.work_order_id = nvl(wem.wo_work_order_id,w.work_order_id) and a.forecast = 1 and level = (select min(level) min_lvl from job_task b, work_order_control x where b.work_order_id = x.work_order_id and x.funding_wo_id = wem.work_order_id and x.work_order_id = nvl(wem.wo_work_order_id,x.work_order_id) and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id ),wem.job_task_id)',
   (select nvl(max(1),0) from pp_system_control where lower(control_name) = lower('FP Ests-Job Task Source Table') and lower(control_value) = 'job_task_wo')); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active)
values ('Job Task Rollup (FP)',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a where a.work_order_id = wem.work_order_id and a.forecast = 1 and level = (select min(level) min_lvl from job_task b where b.work_order_id = wem.work_order_id and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id ),wem.job_task_id)',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a where a.work_order_id = wem.work_order_id and a.forecast = 1 and level = (select min(level) min_lvl from job_task b where b.work_order_id = wem.work_order_id and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id ),wem.job_task_id)',
   'wem.work_order_id, wem.job_task_id',
   'jt_rollup',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a, work_order_control w where a.work_order_id = w.funding_wo_id and w.work_order_id = wem.work_order_id and a.forecast = 1 and level = (select min(level) min_lvl from job_task b, work_order_control x where b.work_order_id = x.funding_wo_id and x.work_order_id = wem.work_order_id and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id),wem.job_task_id)',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a, work_order_control w where a.work_order_id = w.funding_wo_id and w.work_order_id = wem.work_order_id and a.forecast = 1 and level = (select min(level) min_lvl from job_task b, work_order_control x where b.work_order_id = x.funding_wo_id and x.work_order_id = wem.work_order_id and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id),wem.job_task_id)',
   'wem.work_order_id, wem.job_task_id',
   'Job Task Rollup',
   '',
   'nvl((select max(a.job_task_id || '' - '' || a.description) from job_task a where a.work_order_id = wem.work_order_id and a.forecast = 1 and level = (select min(level) min_lvl from job_task b where b.work_order_id = wem.work_order_id and b.forecast = 1 start with b.job_task_id = wem.job_task_id connect by prior b.parent_job_task = b.job_task_id and prior b.work_order_id = b.work_order_id) start with a.job_task_id = wem.job_task_id connect by prior a.parent_job_task = a.job_task_id and prior a.work_order_id = a.work_order_id ),wem.job_task_id)',
   (select nvl(max(1),0) from pp_system_control where lower(control_name) = lower('FP Ests-Job Task Source Table') and lower(control_value) = 'job_task')); 
insert into wo_est_forecast_attributes (attribute_name, id_column, desc_column, group_by_column, column_name, id_column_act, desc_column_act, group_by_column_act, column_header, project_where_clause, estimate_where_clause, active) values ('Job Task Environmental', '(select max(jtcc.value) from job_task_class_code jtcc where jtcc.work_order_id = wem.work_order_id and jtcc.job_task_id = wem.job_task_id and jtcc.class_code_id = 13)', '(select max(jtcc.value) from job_task_class_code jtcc where jtcc.work_order_id = wem.work_order_id and jtcc.job_task_id = wem.job_task_id and jtcc.class_code_id = 13)', 'wem.work_order_id, wem.job_task_id', 'jt_environmental', '(select max(jtcc.value) from job_task_class_code jtcc, work_order_control w where jtcc.work_order_id = w.funding_wo_id and w.work_order_id = wem.work_order_id and jtcc.job_task_id = wem.job_task_id and jtcc.class_code_id = 13)', '(select max(jtcc.value) from job_task_class_code jtcc, work_order_control w where jtcc.work_order_id = w.funding_wo_id and w.work_order_id = wem.work_order_id and jtcc.job_task_id = wem.job_task_id and jtcc.class_code_id = 13)', 'wem.work_order_id, wem.job_task_id', 'Job Task Environmental', '', '(select max(jtcc.value) from job_task_class_code jtcc where jtcc.work_order_id = wem.work_order_id and jtcc.job_task_id = wem.job_task_id and jtcc.class_code_id = 13)', 0); 

update WO_EST_FORECAST_OPTIONS
set DETAIL_LEVEL1 = 'Funding Project',
   DETAIL_LEVEL2 = 'Job Task',
   DETAIL_LEVEL3 = nvl(trim(TASK_DETAIL_LEVEL3),'N/A'),
   DETAIL_LEVEL4 = nvl2(trim(TASK_DETAIL_LEVEL3),nvl(trim(TASK_DETAIL_LEVEL4),'N/A'),'N/A'),
   DETAIL_LEVEL5 = nvl2(trim(TASK_DETAIL_LEVEL3),nvl2(trim(TASK_DETAIL_LEVEL4),nvl(trim(TASK_DETAIL_LEVEL5),'N/A'),'N/A'),'N/A')
where template_level = 'jt';


update WO_EST_FORECAST_OPTIONS
set DETAIL_LEVEL1 = 'Funding Project',
   DETAIL_LEVEL2 = 'N/A',
   DETAIL_LEVEL3 = 'N/A',
   DETAIL_LEVEL4 = 'N/A',
   DETAIL_LEVEL5 = 'N/A'
where nvl(trim(DETAIL_LEVEL1),'N/A') = 'N/A'
and template_level <> 'jt';

update WO_EST_FORECAST_OPTIONS
set DETAIL_LEVEL1 = nvl(DETAIL_LEVEL1,'N/A'),
   DETAIL_LEVEL2 = nvl(DETAIL_LEVEL2,'N/A'),
   DETAIL_LEVEL3 = nvl(DETAIL_LEVEL3,'N/A'),
   DETAIL_LEVEL4 = nvl(DETAIL_LEVEL4,'N/A'),
   DETAIL_LEVEL5 = nvl(DETAIL_LEVEL5,'N/A')
;

update WO_EST_FORECAST_OPTIONS
set DETAIL_LEVEL1 = (
   select ATTRIBUTE_NAME
   from WO_EST_FORECAST_ATTRIBUTES
   where ATTRIBUTE_NAME in ('Job Task (List)','Job Task (FP)','Job Task (WO)')
   and ACTIVE = 1
   )
where DETAIL_LEVEL1 = 'Job Task';

update WO_EST_FORECAST_OPTIONS
set DETAIL_LEVEL2 = (
   select ATTRIBUTE_NAME
   from WO_EST_FORECAST_ATTRIBUTES
   where ATTRIBUTE_NAME in ('Job Task (List)','Job Task (FP)','Job Task (WO)')
   and ACTIVE = 1
   )
where DETAIL_LEVEL2 = 'Job Task';

update WO_EST_FORECAST_OPTIONS
set DETAIL_LEVEL3 = (
   select ATTRIBUTE_NAME
   from WO_EST_FORECAST_ATTRIBUTES
   where ATTRIBUTE_NAME in ('Job Task (List)','Job Task (FP)','Job Task (WO)')
   and ACTIVE = 1
   )
where DETAIL_LEVEL3 = 'Job Task';

update WO_EST_FORECAST_OPTIONS
set DETAIL_LEVEL4 = (
   select ATTRIBUTE_NAME
   from WO_EST_FORECAST_ATTRIBUTES
   where ATTRIBUTE_NAME in ('Job Task (List)','Job Task (FP)','Job Task (WO)')
   and ACTIVE = 1
   )
where DETAIL_LEVEL4 = 'Job Task';

update WO_EST_FORECAST_OPTIONS
set DETAIL_LEVEL5 = (
   select ATTRIBUTE_NAME
   from WO_EST_FORECAST_ATTRIBUTES
   where ATTRIBUTE_NAME in ('Job Task (List)','Job Task (FP)','Job Task (WO)')
   and ACTIVE = 1
   )
where DETAIL_LEVEL5 = 'Job Task';


update wo_est_forecast_options
set template_locked = 0;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2102, 0, 2015, 1, 0, 0, 41551, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041551_pcm_forecast_attributes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;