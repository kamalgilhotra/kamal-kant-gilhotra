/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045032_cwip_add_asset_id_to_rwip_close_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 10/19/2015 Charlie Shilling add asset_id column to rwip_closeout_staging and rwip_closeout_stag_al
||============================================================================
*/
ALTER TABLE rwip_closeout_stag_woe
ADD asset_id NUMBER(22,0);

COMMENT ON COLUMN rwip_closeout_stag_woe.asset_id IS 'Asset ID from the work order estimate, if applicable. When this is populated, the depreciation group for the RWIP allocation will be founding using the GL Account from the asset header rather than the Unitized GL Account from the work order.'

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2933, 0, 2015, 2, 0, 0, 45032, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045032_cwip_add_asset_id_to_rwip_close_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;