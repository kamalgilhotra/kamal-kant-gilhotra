/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038665_lease_PKG_LEASE_COMMON.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 03/28/2014 Kyle Peterson
|| 10.4.3.0	05/29/2014 Charlie Shilling	add trans_type column to gl_transaction insert
|| 10.4.3.0 07/07/2014 Charlie Shilling
||============================================================================
*/

create or replace package PKG_LEASE_COMMON as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_COMMON
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck    Original Version
   ||============================================================================
   */
   
   function F_GET_INIT_LIFE
   (
    A_LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
    A_REVISION number,
    A_ILR_ID LS_ILR.ILR_ID%type,
    A_ECONOMIC_LIFE number
   ) return number;

   -- function the return the process id for lessee calcs
   function F_GETPROCESS return number;

   -- Function to perform booking of LESSEE JEs
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_MSG           out varchar2) return number;

   --overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number;
end PKG_LEASE_COMMON;
/

create or replace package body PKG_LEASE_COMMON as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_COMMON
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck          Original Version
   ||============================================================================
   */

   --**************************************************************
   --         TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --         VARIABLES
   --**************************************************************
   L_PROCESS_ID number;

   --**************************************************************
   --         PROCEDURES
   --**************************************************************
    function F_GET_INIT_LIFE
   (
    A_LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
    A_REVISION number,
    A_ILR_ID LS_ILR.ILR_ID%type,
    A_ECONOMIC_LIFE number
   ) return number
   is
    L_INIT_LIFE    number;
    L_OWNER_TRF    number;
   begin
     -- get the initial life (all set of books have the same remaining life
    select the_count
    into L_INIT_LIFE
    from
    (
      select count(1) the_count, set_of_books_id
      from ls_asset_schedule
      where ls_asset_id = a_ls_asset_id
      and revision = A_REVISION
      group by set_of_books_id
    )
    where rownum = 1
    ;

    select case when purchase_option_type_id = 1 then 0 else 1 end
    into L_OWNER_TRF
    from ls_ilr_options
    where ilr_id = a_ilr_id
    and revision = a_revision
    ;

    -- if there is an ownership transfer, the init life (depreciable life)
    -- should be based on the economic life of the asset and not the lease term length
    if L_OWNER_TRF = 1 then
      L_INIT_LIFE := A_ECONOMIC_LIFE;
    end if;

    return L_INIT_LIFE;
   end F_GET_INIT_LIFE;
   
   
   procedure P_SETPROCESS is
   begin
      select PROCESS_ID
        into L_PROCESS_ID
        from PP_PROCESSES
       where UPPER(DESCRIPTION) = 'LESSEE CALCULATIONS';
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         L_PROCESS_ID := -1;
   end P_SETPROCESS;

   --**************************************************************
   --         FUNCTIONS
   --**************************************************************
   -- Returns the process id for lease calc
   function F_GETPROCESS return number is

   begin
      return L_PROCESS_ID;
   end F_GETPROCESS;

   --Wrapper for the overloaded version
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_PROCESSED  number;

   begin

      return f_bookje(A_LS_ASSET_ID, A_TRANS_TYPE, A_AMT, A_ASSET_ACT_ID, A_DG_ID,
                      A_WO_ID, A_GL_ACCOUNT_ID, A_GAIN_LOSS, A_PEND_TRANS_ID,
                      A_COMPANY_ID, A_MONTH, A_DR_CR, A_GL_JC, A_SOB_ID, '', A_MSG);
   exception
      when others then
         return -1;
   end F_BOOKJE;

   --**************************************************************************
   --                            F_BOOKJE
   --**************************************************************************
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_PROCESSED  number;
      L_MY_COUNT number;

   begin

      A_MSG       := 'Starting JE Creation for trans_type: ' || TO_CHAR(A_TRANS_TYPE);
      L_PROCESSED := 0;
      for L_METHOD_REC in (select M.JE_METHOD_ID        as JE_METHOD_ID,
                                  S.SET_OF_BOOKS_ID     as SET_OF_BOOKS_ID,
                                  M.AMOUNT_TYPE         as AMOUNT_TYPE,
                                  S.REVERSAL_CONVENTION as REVERSAL_CONVENTION
                             from JE_METHOD              M,
                                  JE_METHOD_SET_OF_BOOKS S,
                                  COMPANY_SET_OF_BOOKS   C,
                                  JE_METHOD_TRANS_TYPE   T
                            where M.JE_METHOD_ID = S.JE_METHOD_ID
                              and C.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                              and C.INCLUDE_INDICATOR = 1
                              and C.COMPANY_ID = A_COMPANY_ID
                              and M.JE_METHOD_ID = T.JE_METHOD_ID
                              and T.TRANS_TYPE = A_TRANS_TYPE
                              and C.SET_OF_BOOKS_ID = A_SOB_ID)
      loop
         L_PROCESSED  := 1;

       -- before getting the GL_ACCOUNT, check to see if we want to IGNORE the JE type
      select count(1)
      into l_my_count
      from ls_lease_type_jes_exclude a, ls_ilr_options lo, ls_asset la
      where lo.revision = la.approved_revision
      and lo.lease_cap_type_id = a.lease_cap_type_id
      and a.trans_type = A_TRANS_TYPE
      and a.company_id = A_COMPANY_ID
      ;

      L_GL_ACCOUNT := 'IGNORE';
      if l_my_count = 0 then
         L_GL_ACCOUNT := PP_GL_TRANSACTION2(A_TRANS_TYPE,
                                            A_LS_ASSET_ID,
                                            A_ASSET_ACT_ID,
                                            A_DG_ID,
                                            A_WO_ID,
                                            A_GL_ACCOUNT_ID,
                                            A_GAIN_LOSS,
                                            A_PEND_TRANS_ID,
                                            L_METHOD_REC.JE_METHOD_ID,
                                            L_METHOD_REC.SET_OF_BOOKS_ID,
                                            L_METHOD_REC.AMOUNT_TYPE,
                                            L_METHOD_REC.REVERSAL_CONVENTION);

         if LOWER(L_GL_ACCOUNT) like '%error%' then
            A_MSG := L_GL_ACCOUNT;
            return - 1;
         end if;
      end if;


         A_MSG := 'Inserting into gl_transaction: ' || TO_CHAR(A_TRANS_TYPE);
         insert into GL_TRANSACTION
            (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
             GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
             ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
            select PWRPLANT1.NEXTVAL,
                   A_MONTH,
                   C.GL_COMPANY_NO,
                   L_GL_ACCOUNT,
                   A_DR_CR,
                   A_AMT,
                   A_GL_JC,
                   1,
                   'TRANS TYPE: ' || TO_CHAR(A_TRANS_TYPE),
                   'LESSEE',
                   A_ORIG,
                   '',
                   A_PEND_TRANS_ID,
                   A_LS_ASSET_ID,
                   L_METHOD_REC.AMOUNT_TYPE,
                   L_METHOD_REC.JE_METHOD_ID,
                   null,
				   A_TRANS_TYPE
              from COMPANY_SETUP C
             where C.COMPANY_ID = A_COMPANY_ID;
      end loop;

      return 1;
   exception
      when others then
         return -1;
   end F_BOOKJE;

begin
   -- initialize the package
   P_SETPROCESS;
end PKG_LEASE_COMMON;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1235, 0, 10, 4, 3, 0, 38665, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038665_lease_PKG_LEASE_COMMON.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
