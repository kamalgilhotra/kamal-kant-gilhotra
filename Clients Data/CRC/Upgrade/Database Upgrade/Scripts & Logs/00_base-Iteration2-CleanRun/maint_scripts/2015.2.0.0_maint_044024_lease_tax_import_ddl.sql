/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044024_lease_tax_import_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/11/2015 William Davis   Tables creates for leased asset tax 
||                                      imports	
||============================================================================
*/

create table ls_import_asset_taxes (
import_run_id         NUMBER(22,0)   NOT NULL,
line_id               NUMBER(22,0)   NOT NULL,
time_stamp            DATE           NULL,
user_id               VARCHAR2(18)   NULL,
ls_asset_xlate        VARCHAR2(254)  NULL,
ls_asset_id           NUMBER(22,0)   NULL,
tax_local_xlate       VARCHAR2(254)  NULL,
tax_local_id          NUMBER(22,0)   NULL,
status_code_id        NUMBER(22,0)   NULL,
company_xlate         VARCHAR2(254)  NULL,
company_id            NUMBER(22,0)   NULL,
loaded                NUMBER(22,0)   NULL,
is_modified           NUMBER(22,0)   NULL,
error_message         VARCHAR2(4000) NULL
);

ALTER TABLE ls_import_asset_taxes
  ADD CONSTRAINT ls_import_asset_taxes_pk PRIMARY KEY (
    import_run_id,
	line_id
  )
  USING INDEX
     TABLESPACE pwrplant_idx
	 STORAGE (
	   NEXT       1024 K
	 );

ALTER TABLE ls_import_asset_taxes
  ADD CONSTRAINT ls_import_asset_taxes_fk FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  );

COMMENT ON TABLE ls_import_asset_taxes IS '(S) [06] The LS Import Asset Taxes table is an API table used to import leased asset tax mapping rows.';

COMMENT ON COLUMN ls_import_asset_taxes.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN ls_import_asset_taxes.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN ls_import_asset_taxes.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_import_asset_taxes.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_import_asset_taxes.ls_asset_xlate IS 'Translation field used to determine the internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_import_asset_taxes.ls_asset_id IS 'The internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_import_asset_taxes.tax_local_xlate IS 'Translation field for determining the internal local tax id within PowerPlant.';
COMMENT ON COLUMN ls_import_asset_taxes.tax_local_id IS 'The internal local tax id within PowerPlant.';
COMMENT ON COLUMN ls_import_asset_taxes.status_code_id IS 'Indicates whether tax is currently active or not. 1 indicates active, 0 is inactive.';
COMMENT ON COLUMN ls_import_asset_taxes.company_xlate IS 'Translation field for determining the company.';
COMMENT ON COLUMN ls_import_asset_taxes.company_id IS 'The company.';
COMMENT ON COLUMN ls_import_asset_taxes.loaded IS 'System-assigned number to specify if this row has been loaded into its table.';
COMMENT ON COLUMN ls_import_asset_taxes.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';
COMMENT ON COLUMN ls_import_asset_taxes.error_message IS 'Error messages resulting from data valdiation in the import process.';

create table ls_import_asset_taxes_archive (
import_run_id         NUMBER(22,0)   NOT NULL,
line_id               NUMBER(22,0)   NOT NULL,
time_stamp            DATE           NULL,
user_id               VARCHAR2(18)   NULL,
ls_asset_xlate        VARCHAR2(254)  NULL,
ls_asset_id           NUMBER(22,0)   NULL,
tax_local_xlate       VARCHAR2(254)  NULL,
tax_local_id          NUMBER(22,0)   NULL,
status_code_id        NUMBER(22,0)   NULL,
company_xlate         VARCHAR2(254)  NULL,
company_id            NUMBER(22,0)   NULL,
loaded                NUMBER(22,0)   NULL,
is_modified           NUMBER(22,0)   NULL,
error_message         VARCHAR2(4000) NULL
);

ALTER TABLE ls_import_asset_taxes_archive
  ADD CONSTRAINT ls_import_asset_taxes_archi_pk PRIMARY KEY (
    import_run_id,
	line_id
  )
  USING INDEX
     TABLESPACE pwrplant_idx
	 STORAGE (
	   NEXT       1024 K
	 );

ALTER TABLE ls_import_asset_taxes_archive
  ADD CONSTRAINT ls_import_asset_taxes_archi_fk FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  );

COMMENT ON TABLE ls_import_asset_taxes_archive IS '(S) [06] The LS Import Asset Taxes Archive table is an archive table used to store imported leased asset tax mapping rows.';

COMMENT ON COLUMN ls_import_asset_taxes_archive.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.ls_asset_xlate IS 'Translation field used to determine the internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.ls_asset_id IS 'The internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.tax_local_xlate IS 'Translation field for determining the internal local tax id within PowerPlant.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.tax_local_id IS 'The internal local tax id within PowerPlant.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.status_code_id IS 'Indicates whether tax is currently active or not. 1 indicates active, 0 is inactive.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.company_xlate IS 'Translation field for determining the company.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.company_id IS 'The company.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.loaded IS 'System-assigned number to specify if this row has been loaded into its table.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';
COMMENT ON COLUMN ls_import_asset_taxes_archive.error_message IS 'Error messages resulting from data valdiation in the import process.';




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2593, 0, 2015, 2, 0, 0, 044024, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044024_lease_tax_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;