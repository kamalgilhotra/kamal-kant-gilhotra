/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035940_reg_equity_return_method.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/27/2014 Sarah Byers
||============================================================================
*/

-- Add field for case level option for using return method or deficiency method
alter table REG_CASE add USE_EQUITY_RETURN_METHOD number(22,0);

-- Add return tag to reg_case_rev_req_results
-- Back up the table
create table REG_REV_REQ_RESULTS_012214 as (select * from REG_CASE_REV_REQ_RESULTS);

-- Drop the table
drop table REG_CASE_REV_REQ_RESULTS;

-- Add reg_return_tag_id
create table REG_CASE_REV_REQ_RESULTS
(
 REG_CASE_ID            number(22,0) not null,
 CASE_YEAR_END          number(22,0) not null,
 REG_RETURN_TAG_ID      number(22,0) not null,
 RATE_BASE              number(22,2),
 OVERALL_RETURN         number(22,8),
 TAXES                  number(22,2),
 OPERATING_EXPENSE      number(22,2),
 DEPRECIATION           number(22,2),
 REVENUE_ALLOWED        number(22,2),
 NOI_ALLOWED            number(22,2),
 REVENUE_CURRENT        number(22,2),
 NOI_CURRENT            number(22,2),
 NOI_CHANGE             number(22,2),
 REVENUE_GROSSUP_FACTOR number(22,2),
 REVENUE_IMPACT         number(22,2),
 USER_ID                varchar2(18),
 TIME_STAMP             date
);

alter table REG_CASE_REV_REQ_RESULTS
   add constraint PK_REG_CASE_REV_REQ_RESULTS
       primary key (REG_CASE_ID, CASE_YEAR_END, REG_RETURN_TAG_ID)
       using index tablespace PWRPLANT_IDX;

-- Foreign Keys
alter table REG_CASE_REV_REQ_RESULTS
   add constraint R_REG_CASE_REV_REQ_RESULTS1
       foreign key (REG_CASE_ID)
       references REG_CASE (REG_CASE_ID);

alter table REG_CASE_REV_REQ_RESULTS
   add constraint R_REG_CASE_REV_REQ_RESULTS2
       foreign key (REG_CASE_ID, REG_RETURN_TAG_ID)
       references REG_CASE_MONITOR_CONTROL (REG_CASE_ID, REG_RETURN_TAG_ID);

-- Comments
comment on column REG_CASE_REV_REQ_RESULTS.REG_CASE_ID is 'SYSTEM-ASSIGNED ID OF THE REGULATORY CASE.' ;
comment on column REG_CASE_REV_REQ_RESULTS.CASE_YEAR_END is 'MONTH END OF 12 MONTH ENDING ANALYSIS IN YYYYMM FORMAT.' ;
comment on column REG_CASE_REV_REQ_RESULTS.REG_RETURN_TAG_ID is 'System-assigned identifier of a case, allocation category, and allocation target used as a return tag in monitoring.' ;
comment on column REG_CASE_REV_REQ_RESULTS.OVERALL_RETURN is 'OVERALL (WEIGHTED OVERALL CAPITAL COMPONENTS DEBT, EQUITY, ETC.) EXPRESSED AS A DECIMAL.' ;
comment on column REG_CASE_REV_REQ_RESULTS.RATE_BASE is 'AMOUNT OF RATE BASE IN DOLLARS.' ;
comment on column REG_CASE_REV_REQ_RESULTS.TAXES is 'Amount of Taxes in dollars' ;
comment on column REG_CASE_REV_REQ_RESULTS.OPERATING_EXPENSE is 'Amount of Operating Expenses in dollars' ;
comment on column REG_CASE_REV_REQ_RESULTS.DEPRECIATION is 'Amount of Depreciation in dollars' ;
comment on column REG_CASE_REV_REQ_RESULTS.REVENUE_ALLOWED is 'NOI Allowed plus Operating Expenses in dollars' ;
comment on column REG_CASE_REV_REQ_RESULTS.NOI_ALLOWED is 'Rate Base multiplied by Overall Return in dollars' ;
comment on column REG_CASE_REV_REQ_RESULTS.REVENUE_CURRENT is 'Current Revenues in dollars' ;
comment on column REG_CASE_REV_REQ_RESULTS.NOI_CURRENT is '' ;
comment on column REG_CASE_REV_REQ_RESULTS.NOI_CHANGE is 'Revenue Allowed less Revenue Current in dollars' ;
comment on column REG_CASE_REV_REQ_RESULTS.REVENUE_GROSSUP_FACTOR is 'The Revenue Gross Up Factor entered by the user as a Case Parameter.' ;
comment on column REG_CASE_REV_REQ_RESULTS.REVENUE_IMPACT is 'NOI Change multiplied by the Revenue Gross Up Factor in dollars' ;
comment on column REG_CASE_REV_REQ_RESULTS.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.' ;
comment on column REG_CASE_REV_REQ_RESULTS.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.' ;

-- Put data back into the new table
insert into REG_CASE_REV_REQ_RESULTS
   (REG_CASE_ID, CASE_YEAR_END, REG_RETURN_TAG_ID, OVERALL_RETURN, RATE_BASE, TAXES,
    OPERATING_EXPENSE, DEPRECIATION, REVENUE_ALLOWED, NOI_ALLOWED, REVENUE_CURRENT, NOI_CURRENT,
    NOI_CHANGE, REVENUE_GROSSUP_FACTOR, REVENUE_IMPACT)
   select A.REG_CASE_ID,
          A.CASE_YEAR_END,
          C.REG_RETURN_TAG_ID,
          A.OVERALL_RETURN,
          A.RATE_BASE,
          A.TAXES,
          A.OPERATING_EXPENSE,
          A.DEPRECIATION,
          A.REVENUE_ALLOWED,
          A.NOI_ALLOWED,
          A.REVENUE_CURRENT,
          A.NOI_CURRENT,
          A.NOI_CHANGE,
          A.REVENUE_GROSSUP_FACTOR,
          A.REVENUE_IMPACT
     from REG_REV_REQ_RESULTS_012214 A,
          REG_CASE_MONITOR_CONTROL C,
          (select F.REG_CASE_ID REG_CASE_ID, min(T.REG_ALLOC_TARGET_ID) REG_ALLOC_TARGET_ID
             from REG_ALLOC_TARGET T, REG_CASE_TARGET_FORWARD F
            where T.REG_ALLOC_TARGET_ID = F.REG_ALLOC_TARGET_ID
              and T.RECOVERY_CLASS_ID = 0
            group by F.REG_CASE_ID
           union
           select M.REG_CASE_ID, M.REG_ALLOC_TARGET_ID
             from REG_CASE_MONITOR_CONTROL M, REG_CASE_ALLOC_STEPS S
            where M.REG_CASE_ID = S.REG_CASE_ID
              and M.REG_ALLOC_CATEGORY_ID = S.REG_ALLOC_CATEGORY_ID
              and S.RECOVERY_CLASS_FLAG = 1
              and M.SUBTOTAL_NAME is not null) T
    where A.REG_CASE_ID = C.REG_CASE_ID
      and C.REG_CASE_ID = T.REG_CASE_ID
      and C.REG_ALLOC_TARGET_ID = T.REG_ALLOC_TARGET_ID;
commit;

-- Drop the back up table
drop table REG_REV_REQ_RESULTS_012214;

-- Tax Table Changes
alter table REG_CASE_CALCULATION
   add (WACC                number(22,8),
        RETURN_ON_RATE_BASE number(22,2),
        EQUITY_RETURN       number(22,2),
        GROSSUP_RATE        number(22,8),
        RETURN_BEFORE_TAX   number(22,2));

-- Add new workspace
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_case_rev_req_ws', 'Case Revenue Req Details', 'uo_reg_case_rev_req_ws',
    'Case Revenue Requirement Details');

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 1
 where MODULE = 'REG'
   and PARENT_MENU_IDENTIFIER = 'RATE_CASES'
   and ITEM_ORDER > 9;

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'CASE_REV_REQ', 2, 10, 'Case Revenue Req Details', 'Case Revenue Requirement Details',
    'RATE_CASES', 'uo_reg_case_rev_req_ws', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (915, 0, 10, 4, 2, 0, 35940, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035940_reg_equity_return_method.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;