/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_20_v_mc_lis_inner_def_rent_ddl.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding columns for deferred rent
||========================================================================================
*/

CREATE OR REPLACE VIEW v_multicurrency_lis_inner (
  ilr_id,
  ilr_number,
  current_revision,
  revision,
  set_of_books_id,
  "MONTH",
  beg_capital_cost,
  end_capital_cost,
  beg_obligation,
  end_obligation,
  beg_lt_obligation,
  end_lt_obligation,
  interest_accrual,
  principal_accrual,
  interest_paid,
  principal_paid,
  executory_accrual1,
  executory_accrual2,
  executory_accrual3,
  executory_accrual4,
  executory_accrual5,
  executory_accrual6,
  executory_accrual7,
  executory_accrual8,
  executory_accrual9,
  executory_accrual10,
  executory_paid1,
  executory_paid2,
  executory_paid3,
  executory_paid4,
  executory_paid5,
  executory_paid6,
  executory_paid7,
  executory_paid8,
  executory_paid9,
  executory_paid10,
  contingent_accrual1,
  contingent_accrual2,
  contingent_accrual3,
  contingent_accrual4,
  contingent_accrual5,
  contingent_accrual6,
  contingent_accrual7,
  contingent_accrual8,
  contingent_accrual9,
  contingent_accrual10,
  contingent_paid1,
  contingent_paid2,
  contingent_paid3,
  contingent_paid4,
  contingent_paid5,
  contingent_paid6,
  contingent_paid7,
  contingent_paid8,
  contingent_paid9,
  contingent_paid10,
  current_lease_cost,
  beg_deferred_rent,
  deferred_rent,
  end_deferred_rent,
  beg_st_deferred_rent,
  end_st_deferred_rent,
  depr_expense,
  begin_reserve,
  end_reserve,
  depr_exp_alloc_adjust,
  lease_id,
  company_id,
  in_service_exchange_rate,
  purchase_option_amt,
  termination_amt,
  net_present_value,
  capital_cost,
  is_om
) AS
with depr AS (
  SELECT  /*+ materialize */ ilr_id, revision, set_of_books_id,
         month, sum(depr_expense) as depr_expense,
         sum(begin_reserve) as begin_reserve,
         sum(end_reserve) as end_reserve,
         sum(depr_exp_alloc_adjust) as depr_exp_alloc_adjust
  FROM ls_asset la, ls_depr_forecast ldf
  WHERE la.ls_asset_id = ldf.ls_asset_id
  GROUP BY ilr_id, revision, set_of_books_id, month
)
select /*+ NO_MERGE */
  lis.ilr_id,
  ilr.ilr_number,
  ilr.current_revision,
  lis.revision,
  lis.set_of_books_id,
  lis.MONTH,
  lis.beg_capital_cost,
  lis.end_capital_cost,
  lis.beg_obligation,
  lis.end_obligation,
  lis.beg_lt_obligation,
  lis.end_lt_obligation,
  lis.interest_accrual,
  lis.principal_accrual,
  lis.interest_paid,
  lis.principal_paid,
  lis.executory_accrual1,
  lis.executory_accrual2,
  lis.executory_accrual3,
  lis.executory_accrual4,
  lis.executory_accrual5,
  lis.executory_accrual6,
  lis.executory_accrual7,
  lis.executory_accrual8,
  lis.executory_accrual9,
  lis.executory_accrual10,
  lis.executory_paid1,
  lis.executory_paid2,
  lis.executory_paid3,
  lis.executory_paid4,
  lis.executory_paid5,
  lis.executory_paid6,
  lis.executory_paid7,
  lis.executory_paid8,
  lis.executory_paid9,
  lis.executory_paid10,
  lis.contingent_accrual1,
  lis.contingent_accrual2,
  lis.contingent_accrual3,
  lis.contingent_accrual4,
  lis.contingent_accrual5,
  lis.contingent_accrual6,
  lis.contingent_accrual7,
  lis.contingent_accrual8,
  lis.contingent_accrual9,
  lis.contingent_accrual10,
  lis.contingent_paid1,
  lis.contingent_paid2,
  lis.contingent_paid3,
  lis.contingent_paid4,
  lis.contingent_paid5,
  lis.contingent_paid6,
  lis.contingent_paid7,
  lis.contingent_paid8,
  lis.contingent_paid9,
  lis.contingent_paid10,
  lis.current_lease_cost,
  lis.beg_deferred_rent,
  lis.deferred_rent,
  lis.end_deferred_rent,
  lis.beg_st_deferred_rent,
  lis.end_st_deferred_rent,
  depr.depr_expense,
  depr.begin_reserve,
  depr.end_reserve,
  depr.depr_exp_alloc_adjust,
  ilr.lease_id,
  ilr.company_id,
  opt.in_service_exchange_rate,
  opt.purchase_option_amt,
  opt.termination_amt,
  liasob.net_present_value,
  liasob.capital_cost,
  lis.is_om
FROM ls_ilr_schedule lis
INNER JOIN ls_ilr_options opt
  ON lis.ilr_id = opt.ilr_id
  AND lis.revision = opt.revision
INNER JOIN ls_ilr_amounts_set_of_books liasob
  ON lis.ilr_id = liasob.ilr_id
  AND lis.revision = liasob.revision
  AND lis.set_of_books_id = liasob.set_of_books_id
INNER JOIN ls_ilr ilr
  ON lis.ilr_id = ilr.ilr_id
LEFT OUTER JOIN depr
  ON lis.ilr_id = depr.ilr_id
  AND lis.revision = depr.revision
  AND lis.set_of_books_id = depr.set_of_books_id
  AND lis.month = depr.month
INNER JOIN ls_lease lease
  ON ilr.lease_id = lease.lease_id
/

GRANT DELETE,INSERT,SELECT,UPDATE ON v_multicurrency_lis_inner TO pwrplant_role_dev;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4036, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_20_v_mc_lis_inner_def_rent_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;