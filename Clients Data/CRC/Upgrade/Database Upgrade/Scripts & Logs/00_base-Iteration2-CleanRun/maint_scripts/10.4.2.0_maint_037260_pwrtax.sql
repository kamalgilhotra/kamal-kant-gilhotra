/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037260_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date            Revised By          Reason for Change
|| -------- --------------- ------------------- ------------------------------
|| 10.4.2.0 03/19/2014      Andrew Scott        Remove the ability to call the
||                                              C versions of Run Fast.  They will
||                                              not work with MLP 10.4.2.0 table
||                                              structure.
||============================================================================
*/

delete from PP_SYSTEM_CONTROL_COMPANY where LOWER(CONTROL_NAME) like 'fast%tax%';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1065, 0, 10, 4, 2, 0, 37260, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037260_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
