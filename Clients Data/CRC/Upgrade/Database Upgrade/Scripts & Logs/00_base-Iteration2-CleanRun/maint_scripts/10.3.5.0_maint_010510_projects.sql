/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010510_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   06/13/2012 Chris Mardis   Point Release
||============================================================================
*/

create table WO_EST_RATE_FILTER
(
 ID                  number(22),
 RATE_TYPE_ID        number(22),
 EXPENDITURE_TYPE_ID number(22),
 EST_CHG_TYPE_ID     number(22),
 DEPARTMENT_ID       number(22),
 UTILITY_ACCOUNT_ID  number(22),
 WO_WORK_ORDER_ID    number(22),
 JOB_TASK_ID         varchar2(35)
);

alter table WO_EST_RATE_FILTER
   add constraint WO_EST_RATE_FILTER_PK
       primary key (id)
       using index tablespace PWRPLANT_IDX;

alter table WO_EST_RATE_FILTER
   add constraint WO_EST_RATE_FLT_DEPT_FK
       foreign key (DEPARTMENT_ID)
       references DEPARTMENT;

alter table WO_EST_RATE_FILTER
   add constraint WO_EST_RATE_FLT_EC_TYPE_FK
       foreign key (EST_CHG_TYPE_ID)
       references ESTIMATE_CHARGE_TYPE;

alter table WO_EST_RATE_FILTER
   add constraint WO_EST_RATE_FLT_EXP_TYPE_FK
       foreign key (EXPENDITURE_TYPE_ID)
       references EXPENDITURE_TYPE;

alter table WO_EST_RATE_FILTER
   add constraint WO_EST_RATE_FLT_RATE_FK
       foreign key (RATE_TYPE_ID)
       references RATE_TYPE;

alter table WO_EST_RATE_FILTER
   add (USER_ID    varchar2(18),
        TIME_STAMP date);

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CLIENT, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, POWERTAX, POWERTAX_PROVISION, PROPERTY_TAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, WHERE_CLAUSE)
values
   ('wo_est_rate_filter', TO_DATE('2009-04-13 15:22:48', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 's',
    'Wo Est Rate Filter', 'always', null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('department_id', 'wo_est_rate_filter', TO_DATE('2009-04-13 15:25:14', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'department', 'p', null, 'Department ', 5, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('time_stamp', 'wo_est_rate_filter', TO_DATE('2009-04-13 15:25:15', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'Time Stamp', 100, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('user_id', 'wo_est_rate_filter', TO_DATE('2009-04-13 15:25:15', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'User Id', 101, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('utility_account_id', 'wo_est_rate_filter',
    TO_DATE('2009-04-13 15:25:15', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'utility_account', 'p',
    null, 'Utility Account ', 6, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('est_chg_type_id', 'wo_est_rate_filter',
    TO_DATE('2009-04-13 15:25:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'estimate_charge_type', 'p',
    null, 'Est Chg Type ', 4, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('expenditure_type_id', 'wo_est_rate_filter',
    TO_DATE('2009-04-13 15:25:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'expenditure_type', 'p',
    null, 'Expenditure Type ', 3, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('id', 'wo_est_rate_filter', TO_DATE('2009-04-13 15:25:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    null, 's', null, 'Id', 1, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('job_task_id', 'wo_est_rate_filter', TO_DATE('2009-04-13 15:25:14', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'job_task_list', 'e', null, 'Job Task Id', 7, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE)
values
   ('rate_type_id', 'wo_est_rate_filter', TO_DATE('2009-04-13 15:25:14', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'rate_type', 'p', null, 'Rate Type ', 2, null, null, null, null, null, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (174, 0, 10, 3, 5, 0, 10510, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010510_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
