 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_041927_sys_wo_intfc_arc_miss_col_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 01/22/2014 D. Mendel     Add missing column to archive table that is on staging table
 ||============================================================================
 */ 

alter table WO_INTERFACE_STAGING_ARC add AUTO_APPROVED number(22,0);

comment on column WO_INTERFACE_STAGING_ARC.AUTO_APPROVED is 'Yes\No flag should this new revision be marked as approved.';

comment on column WO_INTERFACE_STAGING.AUTO_APPROVED is 'Yes\No flag should this new revision be marked as approved.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2211, 0, 2015, 1, 0, 0, 41927, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041927_sys_wo_intfc_arc_miss_col_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;