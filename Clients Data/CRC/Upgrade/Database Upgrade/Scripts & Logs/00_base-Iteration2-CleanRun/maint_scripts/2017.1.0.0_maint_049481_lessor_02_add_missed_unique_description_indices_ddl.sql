/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049481_lessor_02_add_missed_unique_description_indices_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/14/2017 Jared Watkins    add the new unique indices to all the Lessor tables we missed in original development
||============================================================================
*/

CREATE UNIQUE INDEX lsr_cap_type_desc_uidx
  ON lsr_cap_type (Upper( Trim( description ) ))
TABLESPACE pwrplant_idx;

CREATE UNIQUE INDEX lsr_fasb_type_desc_uidx
  ON lsr_fasb_cap_type (Upper( Trim( description ) ))
TABLESPACE pwrplant_idx;

CREATE UNIQUE INDEX lsr_ilr_number_uidx
  ON lsr_ilr (Upper( Trim( ilr_number ) ))
TABLESPACE pwrplant_idx;

CREATE UNIQUE INDEX lsr_ilr_group_desc_uidx
  ON lsr_ilr_group (Upper( Trim( description ) ))
TABLESPACE pwrplant_idx;

CREATE UNIQUE INDEX lsr_ilr_rate_type_desc_uidx
  ON lsr_ilr_rate_types (Upper( Trim( description ) ))
TABLESPACE pwrplant_idx;

DROP INDEX lsr_lease_lease_num_idx;

CREATE UNIQUE INDEX lsr_lease_number_uidx
  ON lsr_lease (Upper( Trim( lease_number ) ))
TABLESPACE pwrplant_idx;

CREATE UNIQUE INDEX lsr_lease_group_desc_uidx
  ON lsr_lease_group (Upper( Trim( description ) ))
TABLESPACE pwrplant_idx;

CREATE UNIQUE INDEX lsr_lessee_desc_uidx
  ON lsr_lessee (Upper( Trim( description ) ))
TABLESPACE pwrplant_idx;

CREATE UNIQUE INDEX lsr_rec_bucket_name_uidx
  ON lsr_receivable_bucket_admin (receivable_type, Upper( Trim( bucket_name ) ))
TABLESPACE pwrplant_idx; 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3947, 0, 2017, 1, 0, 0, 49481, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049481_lessor_02_add_missed_unique_description_indices_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;