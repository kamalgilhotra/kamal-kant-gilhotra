/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043667_projects_add_ests_forecasting3_ddl.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     04/25/2015 Chris Mardis   Additional estimate attributes requires flexible number of detail levels
||============================================================================
*/

alter table wo_est_forecast_options drop column detail_level2;
alter table wo_est_forecast_options drop column detail_level3;
alter table wo_est_forecast_options drop column detail_level4;
alter table wo_est_forecast_options drop column detail_level5;

alter table wo_est_forecast_options drop column subtotal_detail_1;
alter table wo_est_forecast_options drop column subtotal_detail_2;
alter table wo_est_forecast_options drop column subtotal_detail_3;
alter table wo_est_forecast_options drop column subtotal_detail_4;
alter table wo_est_forecast_options drop column subtotal_detail_5;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2529, 0, 2015, 1, 0, 0, 43667, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043667_projects_add_ests_forecasting3_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;