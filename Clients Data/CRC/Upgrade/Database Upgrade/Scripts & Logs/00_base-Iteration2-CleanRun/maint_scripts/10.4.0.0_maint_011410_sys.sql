/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011410_sys.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   10/29/2012 Joseph King    Point Release
||============================================================================
*/

alter table PP_PROCESSES add SYSTEM_LOCK_ENABLED number(22,0) default 0;
alter table PP_PROCESSES add SYSTEM_LOCK         number(22,0) default 0;
alter table PP_PROCESSES add PREREQUISITE        varchar2(254);
alter table PP_PROCESSES add POSTREQUISITE       varchar2(254);
alter table PP_PROCESSES add PRIMARY_TABLES      varchar2(254);
alter table PP_PROCESSES add FUNCTION_SUMMARY    varchar2(4000);
alter table PP_PROCESSES add TECHNICAL_SUMMARY   varchar2(4000);
alter table PP_PROCESSES add BASELINE_OCC_ID     number(22,0);

alter table PP_PROCESSES
   add constraint PP_PROC_BASELINE_FK
       foreign key (PROCESS_ID, BASELINE_OCC_ID)
       references PP_PROCESSES_OCCURRENCES;

alter table PP_PROCESSES_OCCURRENCES add RESOLVED number(22,0) default 1;
alter table PP_PROCESSES_OCCURRENCES add SENT     number(22,0) default 0;

alter table PP_PROCESSES_MESSAGES add PP_ERROR_CODE  varchar2(15);
alter table PP_PROCESSES_MESSAGES add SQL_ERROR_CODE varchar2(15);

create table PP_PROCESSES_OCC_ATTRIBUTE
(
 PROCESS_ID     number(22,0) not null,
 OCCURRENCE_ID  number(22,0) not null,
 CATEGORY       varchar2(100) not null,
 PERIOD         varchar2(100) not null,
 ATTRIBUTE      varchar2(100) not null,
 VALUE          number(22,8),
 TIME_STAMP     date,
 USER_ID        varchar2(18)
);

alter table PP_PROCESSES_OCC_ATTRIBUTE
   add constraint PP_PROCESSES_OCC_ATTRIB_PK
       primary key (PROCESS_ID, OCCURRENCE_ID, CATEGORY, PERIOD, ATTRIBUTE)
       using index tablespace PWRPLANT_IDX;

alter table PP_PROCESSES_OCC_ATTRIBUTE
   add constraint PP_PROC_OCC_ATTRIB_FK
       foreign key (PROCESS_ID, OCCURRENCE_ID)
       references PP_PROCESSES_OCCURRENCES;

create table PP_PROCESSES_OCC_METRIC
(
 PROCESS_ID    number(22,0) not null,
 OCCURRENCE_ID number(22,0) not null,
 DESCRIPTION   varchar2 (50) not null,
 START_TIME    date,
 END_TIME      date,
 VOLUME        number(22,2),
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

alter table PP_PROCESSES_OCC_METRIC
   add constraint PP_PROCESSES_OCC_METRIC_PK
       primary key (PROCESS_ID, OCCURRENCE_ID, DESCRIPTION)
       using index tablespace PWRPLANT_IDX;

alter table PP_PROCESSES_OCC_METRIC
   add constraint PP_PROC_OCC_METRIC_FK
       foreign key (PROCESS_ID, OCCURRENCE_ID)
       references PP_PROCESSES_OCCURRENCES;

create table PP_PROCESSES_ERROR_MSG
(
 PROCESS_ID    number(22,0) not null,
 PP_ERROR_CODE varchar2(15) not null,
 ERROR_MESSAGE varchar2 (254),
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 SENT          number(22,0) default 0
);

alter table PP_PROCESSES_ERROR_MSG
   add constraint PP_PROCESSES_ERROR_MSG_PK
       primary key (PROCESS_ID, PP_ERROR_CODE)
       using index tablespace PWRPLANT_IDX;

alter table PP_PROCESSES_MESSAGES
   add constraint PP_PROC_ERR_MSG_FK
       foreign key (PROCESS_ID, PP_ERROR_CODE)
       references PP_PROCESSES_ERROR_MSG;

create table PP_PROCESSES_INFO_TYPE
(
 INFO_TYPE_ID number(22,0) not null,
 DESCRIPTION  varchar2(35),
 TIME_STAMP   date,
 USER_ID      varchar2(18),
 SENT         number(22,0) default 0
);

alter table PP_PROCESSES_INFO_TYPE
   add constraint PP_PROCESSES_INFO_TYPE_PK
       primary key (INFO_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

create table PP_PROCESSES_ERROR_INFO
(
 ERROR_INFO_ID  number(22,0) not null,
 PROCESS_ID     number(22,0),
 PP_ERROR_CODE  varchar2(15),
 SQL_ERROR_CODE varchar2(15),
 ORDER_ID       number(22,0),
 SEVERITY       number(22,0),
 DESCRIPTION    varchar2(35),
 EXPLANATION    clob,
 RESOLUTION     clob,
 INFO_TYPE_ID   number(22,0),
 SENT           number(22,0) default 0,
 TIME_STAMP     date,
 USER_ID        varchar2(18)
);

alter table PP_PROCESSES_ERROR_INFO
   add constraint PP_PROCESSES_ERROR_INFO_PK
       primary key (ERROR_INFO_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_PROCESSES_ERROR_INFO
   add constraint PP_PROC_INFO_TYPE_FK1
       foreign key (INFO_TYPE_ID)
       references PP_PROCESSES_INFO_TYPE;

alter table PP_PROCESSES_ERROR_INFO
   add constraint PP_PROC_ERROR_MSG_FK
       foreign key (PROCESS_ID, PP_ERROR_CODE)
       references PP_PROCESSES_ERROR_MSG;

create table PP_PROCESS_ERROR_ACTION
(
 ERROR_ACTION_ID number(22,0) not null,
 ORDER_ID        number(22,0),
 ACTION          varchar2(35),
 ACTION_DETAIL   clob,
 SENT            number(22,0) default 0,
 TIME_STAMP      date,
 USER_ID         varchar2(18)
);

alter table PP_PROCESS_ERROR_ACTION
   add constraint PP_PROCESS_ERROR_ACTION_PK
       primary key (ERROR_ACTION_ID)
       using index tablespace PWRPLANT_IDX;

create table PP_PROCESSES_ERR_INFO_ACT
(
 ERROR_INFO_ID   number(22,0) not null,
 ERROR_ACTION_ID number(22,0) not null,
 SENT            number(22,0) default 0,
 TIME_STAMP      date,
 USER_ID         varchar2(18)
);

alter table PP_PROCESSES_ERR_INFO_ACT
   add constraint PP_PROCESSES_ERR_INFO_ACT_PK
       primary key (ERROR_INFO_ID, ERROR_ACTION_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_PROCESSES_ERR_INFO_ACT
   add constraint PP_PROC_ERROR_INFO_FK
       foreign key (ERROR_INFO_ID)
       references PP_PROCESSES_ERROR_INFO;

alter table PP_PROCESSES_ERR_INFO_ACT
   add constraint PP_PROC_ERROR_ACT_FK
       foreign key (ERROR_ACTION_ID)
       references PP_PROCESS_ERROR_ACTION;

alter table PP_PROCESSES_RETURN_VALUES add IS_SUCCESS number(22,0) default 0;

create table PP_PROCESSES_OCC_ERROR_INFO
(
 PROCESS_ID      number(22,0) not null,
 OCCURRENCE_ID   number(22,0) not null,
 MSG_ORDER       number(22,0) not null,
 ERROR_INFO_ID   number(22,0) not null,
 RESOLUTION_TIME number(22,2),
 DATE_RESOLVED   date,
 USER_ID         varchar2(18),
 TIME_STAMP      date
);

alter table PP_PROCESSES_OCC_ERROR_INFO
   add constraint PP_PROC_OCC_ERR_INFO_PK
       primary key (PROCESS_ID, OCCURRENCE_ID, MSG_ORDER, ERROR_INFO_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_PROCESSES_OCC_ERROR_INFO
   add constraint PP_PROC_OCC_ERR_INFO_FK
       foreign key (PROCESS_ID, OCCURRENCE_ID, MSG_ORDER)
       references PP_PROCESSES_MESSAGES;

alter table PP_PROCESSES_OCC_ERROR_INFO
   add constraint PP_PROC_OCC_ERR_INFO_FK2
       foreign key (ERROR_INFO_ID)
       references PP_PROCESSES_ERROR_INFO;

insert into PP_PROCESSES_INFO_TYPE (INFO_TYPE_ID, DESCRIPTION) values (1, 'Error');
insert into PP_PROCESSES_INFO_TYPE (INFO_TYPE_ID, DESCRIPTION) values (2, 'Validation');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (238, 0, 10, 4, 0, 0, 11410, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011410_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;