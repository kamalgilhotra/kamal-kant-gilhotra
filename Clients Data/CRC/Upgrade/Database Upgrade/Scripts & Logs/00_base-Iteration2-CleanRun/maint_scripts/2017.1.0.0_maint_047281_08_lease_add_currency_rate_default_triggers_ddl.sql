/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047281_08_lease_add_currency_rate_default_triggers_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/24/2017 Jared Watkins    add triggers to currency [rate] that push changes to currency_rate_default
||============================================================================
*/
--CREATE TRIGGERS TO PUSH DATA ADDED/CHANGED IN THE CURRENCY AND CURRENCY_RATE TABLES
--  INTO THE CURRENCY_RATE_DEFAULT TABLE
--ALSO DELETE ROWS FROM CURRENCY_RATE_DEFAULT WHEN THE CORRESPONDING ROW IN CURRENCY/CURRENCY_RATE IS REMOVED

CREATE OR REPLACE TRIGGER ls_new_default_currency_rates
AFTER INSERT ON currency
FOR EACH ROW
  BEGIN
    INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
    VALUES ('01-Jan-1900', :new.currency_id, :new.currency_id, 1, 1);
    
    INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
    SELECT '01-Jan-1900', :new.currency_id, currency_to, 1, 0
    FROM currency_rate_default
    WHERE currency_to <> :new.currency_id
    UNION
    SELECT '01-Jan-1900', currency_from, :new.currency_id, 1, 0
    FROM currency_rate_default
    WHERE currency_from <> :new.currency_id;
  END;
/

CREATE OR REPLACE TRIGGER ls_new_actual_currency_rates
AFTER INSERT ON currency_rate
FOR EACH ROW
  BEGIN
    IF :new.exchange_rate_type_id = 1 THEN
      INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
      VALUES (:new.exchange_date, :new.currency_from, :new.currency_to, :new.exchange_rate_type_id, :new.rate);
    END IF;
  END;
/

CREATE OR REPLACE TRIGGER ls_UPDATE_default_curr_rates
AFTER UPDATE ON currency
FOR EACH ROW
  BEGIN
    IF :new.currency_id <> :old.currency_id THEN
      UPDATE currency_rate_default 
      SET currency_from = :new.currency_id, 
          currency_to = :new.currency_id 
      WHERE currency_from = :old.currency_id 
      AND currency_to = :old.currency_id 
      AND rate = 1;
      
      UPDATE currency_rate_default
      SET currency_from = :new.currency_id
      WHERE currency_from = :old.currency_id
      AND currency_to <> :old.currency_id
      AND rate = 0;
      
      UPDATE currency_rate_default
      SET currency_to = :new.currency_id
      WHERE currency_to = :old.currency_id
      AND currency_from <> :old.currency_id
      AND rate = 0;
    END IF;
  END;
/

CREATE OR REPLACE TRIGGER ls_UPDATE_actual_curr_rates
AFTER UPDATE ON currency_rate
FOR EACH ROW
  BEGIN
    IF :new.exchange_rate_type_id = 1 AND :old.exchange_rate_type_id = 1 THEN
      UPDATE currency_rate_default
      SET currency_from = :new.currency_from,
          currency_to = :new.currency_to,
          exchange_date = :new.exchange_date,
          rate = :new.rate
      WHERE currency_from = :old.currency_from
      AND currency_to = :old.currency_to
      AND exchange_date = :old.exchange_date
      AND rate = :old.rate;
    END IF;
    
    IF :new.exchange_rate_type_id = 1 AND :old.exchange_rate_type_id <> 1 THEN
      INSERT INTO currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
      VALUES (:new.exchange_date, :new.currency_from, :new.currency_to, :new.exchange_rate_type_id, :new.rate);
    END IF;
    
    IF :new.exchange_rate_type_id <> 1 AND :old.exchange_rate_type_id = 1 THEN
      DELETE from currency_rate_default
      WHERE currency_from = :old.currency_from
      AND currency_to = :old.currency_to
      AND exchange_date = :old.exchange_date
      AND rate = :old.rate;
    END IF;
  END;
/

CREATE OR REPLACE TRIGGER ls_remove_default_curr_rates
BEFORE DELETE ON currency
FOR EACH ROW
  BEGIN
    DELETE from currency_rate_default 
    WHERE currency_from = :old.currency_id 
    AND currency_to = :old.currency_id
    AND rate = 1;
    
    DELETE from currency_rate_default 
    WHERE (currency_from = :old.currency_id 
        or currency_to = :old.currency_id) 
    AND rate = 0;
  END;
/

CREATE OR REPLACE TRIGGER ls_remove_actual_curr_rates
BEFORE DELETE ON currency_rate
FOR EACH ROW
  BEGIN
    DELETE from currency_rate_default 
    WHERE exchange_date = :old.exchange_date 
    AND currency_from = :old.currency_from 
    AND currency_to = :old.currency_to 
    AND exchange_rate_type_id = :old.exchange_rate_type_id 
    AND rate = :old.rate;
  END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3451, 0, 2017, 1, 0, 0, 47281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047281_08_lease_add_currency_rate_default_triggers_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
