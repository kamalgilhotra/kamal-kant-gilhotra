/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Regulatory
|| File Name:   maint_042187_reg_import_lookup_range_fix_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1	 01/15/2015 	Shane Ward			Put Reg Import Lookups back into fixed range
||==========================================================================================
*/

--Remove old column lookups
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup where Description = 'Reg Company.Description');
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup where Description = 'Reg-PP Company.Description');
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup where Description = 'Reg Acct.Description');
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup where Description = 'Reg Company.Description');
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup where Description = 'Reg-PP Company.Description');
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup  where Description = 'Reg Acct.Long Description');
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup  where Description = 'Reg Hist Version.Long Description');
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup  where Description = 'Reg Fcst Version.Long Description');
Delete from PP_IMPORT_COLUMN_LOOKUP where import_lookup_id in (select import_lookup_id from pp_import_lookup where Description = 'Reg Fcst Version.Description');

--Change ID's
update pp_import_lookup set import_lookup_id = 1518 where Description = 'Reg Company.Description';
update pp_import_lookup set import_lookup_id = 1519 where Description = 'Reg-PP Company.Description';
update pp_import_lookup set import_lookup_id = 1520 where Description = 'Reg Acct.Description';
update pp_import_lookup set import_lookup_id = 1523 where Description = 'Reg Acct.Long Description';
update pp_import_lookup set import_lookup_id = 1524 where Description = 'Reg Hist Version.Long Description';
update pp_import_lookup set import_lookup_id = 1525 where Description = 'Reg Fcst Version.Long Description';
update pp_import_lookup set import_lookup_id = 1526 where Description = 'Reg Fcst Version.Description';

--Re-insert Lookups
-- Historic
--Reg Company Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'reg_company_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG COMPANY.DESCRIPTION' ;
--PP Company DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'reg_company_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG-PP COMPANY.DESCRIPTION';
--Reg Account DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'reg_acct_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG ACCT.DESCRIPTION';
--Reg Account Long DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'reg_acct_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG ACCT.LONG DESCRIPTION';
--Historic Version Long DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'version_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG HIST VERSION.LONG DESCRIPTION';

-- Forecast
--Reg Company DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'reg_company_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG COMPANY.DESCRIPTION';
--PP Company DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'reg_company_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG-PP COMPANY.DESCRIPTION';
--Reg Account DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'reg_acct_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG ACCT.DESCRIPTION';
--Reg Account Long DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'reg_acct_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG ACCT.LONG DESCRIPTION';
--Forecast Version Long DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'version_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG FCST VERSION.LONG DESCRIPTION';
--Forecast Version DESCRIPTION
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'version_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG FCST VERSION.DESCRIPTION';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2176, 0, 2015, 1, 0, 0, 42187, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042187_reg_import_lookup_range_fix_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;