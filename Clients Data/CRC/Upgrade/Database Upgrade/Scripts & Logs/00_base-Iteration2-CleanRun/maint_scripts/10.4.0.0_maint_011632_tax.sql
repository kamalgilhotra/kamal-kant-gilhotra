/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011632_tax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   12/19/2012 Jennifer Butts Point Release
||============================================================================
*/

insert into RETIRE_UNIT_TAX_DISTINCTION
   (TAX_DISTINCTION_ID, DESCRIPTION, LONG_DESCRIPTION)
   select 1, 'Default', 'Default'
     from DUAL
    where 1 not in
          (select TAX_DISTINCTION_ID from RETIRE_UNIT_TAX_DISTINCTION where TAX_DISTINCTION_ID = 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (314, 0, 10, 4, 0, 0, 11632, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011632_tax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
