/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009578_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/02/2012 Julia Breuer   Point Release
|| 10.4.2.0   04/05/2014 Lee Quinn      Changed
||============================================================================
*/

--
-- Add prop tax company to the CPR tree.
--

-- Add the column and foreign key.
alter table pwrplant.prop_tax_type_assign_tree add prop_tax_company_id number(22,0);
alter table pwrplant.prop_tax_type_assign_tree add ( constraint ptt_asgn_tree_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );

-- Update any existing rows with the prop tax company ID.
update   pwrplant.prop_tax_type_assign_tree tree
set         tree.prop_tax_company_id =
         (  select      prop_tax_company_id
            from     ptv_co_view
            where state_id = tree.state_id
            and      company_id = tree.company_id
         );

-- Make the field not null.
alter table pwrplant.prop_tax_type_assign_tree modify prop_tax_company_id not null;

-- Recreate the primary key.
/* Altered 07122014
alter table pwrplant.prop_tax_type_assign_tree drop constraint prop_tax_type_assign_tree_pk;
drop index prop_tax_type_assign_tree_pk;
*/
/* NEW */
alter table pwrplant.prop_tax_type_assign_tree drop primary key drop index;
/* END */
alter table pwrplant.prop_tax_type_assign_tree add ( constraint prop_tax_type_assign_tree_pk primary key ( state_id, tax_year, prop_tax_company_id, method, company_id, gl_account_id, bus_segment_id, utility_account_id, asset_location_id, class_code_id, class_code_value, major_location_id, minor_location_id, retirement_unit_id, sub_account_id, situs_allocated_indicator, state_id2, county_id, vintage_start, vintage_end ) using index tablespace pwrplant_idx );

--
-- Add prop tax company to the Pseudo CWIP tree.
--

-- Add the column and foreign key.
alter table pwrplant.pt_type_assign_cwip_pseudo add prop_tax_company_id number(22,0);
alter table pwrplant.pt_type_assign_cwip_pseudo add ( constraint pt_type_cwip_pseudo_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );

-- Update any existing rows with the prop tax company ID.
update   pwrplant.pt_type_assign_cwip_pseudo tree
set         tree.prop_tax_company_id =
         (  select      prop_tax_company_id
            from     ptv_co_view
            where state_id = tree.state_id
            and      company_id = tree.company_id
         );

-- Make the field not null.
alter table pwrplant.pt_type_assign_cwip_pseudo modify prop_tax_company_id not null;

-- Recreate the primary key.
/* 04/05/2014 altered
alter table pwrplant.pt_type_assign_cwip_pseudo drop constraint pt_type_cwip_pseudo_pk;
drop index pt_type_cwip_pseudo_pk;
*/
/* NEW */
alter table pwrplant.pt_type_assign_cwip_pseudo drop primary key drop index;
/* END */
alter table pwrplant.pt_type_assign_cwip_pseudo add ( constraint pt_type_cwip_pseudo_pk primary key ( state_id, tax_year, prop_tax_company_id, method, company_id, gl_account_id, bus_segment_id, utility_account_id, asset_location_id, class_code_id, class_code_value, county_id, major_location_id, ps_class_code_id, ps_class_code_value, sub_account_id, situs_allocated_indicator, work_order_type_id ) using index tablespace pwrplant_idx );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (105, 0, 10, 3, 4, 0, 9578, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009578_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
