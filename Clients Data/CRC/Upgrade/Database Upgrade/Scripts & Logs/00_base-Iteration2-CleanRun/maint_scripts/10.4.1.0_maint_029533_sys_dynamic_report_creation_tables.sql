/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029533_sys_dynamic_report_creation_tables.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/26/2013 Stephen Motter Point Release
||============================================================================
*/

create table PP_COLOR_LOOKUP
(
 COLOR_NAME           varchar2(35),
 RED_VALUE            number(22,0),
 GREEN_VALUE          number(22,0),
 BLUE_VALUE           number(22,0),
 DISPLAY_COLORIN_DDDW number(1,0),
 TOOLBAR_ITEM_JPG     varchar2(254)
);

alter table PP_COLOR_LOOKUP
   add constraint PP_COLOR_LOOKUP_PK
       primary key (COLOR_NAME)
       using index tablespace PWRPLANT_IDX;

create table PP_AMT_FORMAT
(
 AMT_FORMAT      varchar2(35),
 DISPLAY_IN_DDDW number(1,0)
);

alter table PP_AMT_FORMAT
   add constraint PP_AMT_FORMAT_PK
       primary key (AMT_FORMAT)
       using index tablespace PWRPLANT_IDX;

insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('magenta dark', 128, 0, 128, 0, 'color_magenta_dark.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('green light', 0, 255, 0, 1, 'color_green_light.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('orange', 255, 165, 0, 0, 'color_orange.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('magenta', 255, 0, 255, 0, 'color_magenta.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('blue sky', 0, 191, 255, 0, 'color_blue_sky.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('olive', 128, 128, 0, 0, 'color_olive.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('green', 0, 128, 0, 0, 'color_green.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('yellow', 255, 215, 0, 1, 'color_yellow.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('blue', 0, 0, 255, 1, 'color_blue.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('blue dark', 0, 0, 128, 0, 'color_blue_dark.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('red dark', 128, 0, 0, 0, 'color_red_dark.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('gray light', 192, 192, 192, 1, 'color_gray_light.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('gray dark', 128, 128, 128, 1, 'color_gray_dark.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('cyan dark', 0, 128, 128, 0, 'color_cyan_dark.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('black', 0, 0, 0, 1, 'color_black.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('white', 255, 255, 255, 1, 'color_white.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('green mint', 192, 220, 192, 1, 'color_green_mint.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('red', 255, 0, 0, 1, 'color_red.bmp');
insert into PP_COLOR_LOOKUP
   (COLOR_NAME, RED_VALUE, GREEN_VALUE, BLUE_VALUE, DISPLAY_COLORIN_DDDW, TOOLBAR_ITEM_JPG)
values
   ('brown', 139, 69, 19, 0, 'color_brown.bmp');

insert into PP_AMT_FORMAT (AMT_FORMAT, DISPLAY_IN_DDDW) values ('$#,##0.00', 1);
insert into PP_AMT_FORMAT (AMT_FORMAT, DISPLAY_IN_DDDW) values ('$#,##0', 1);
insert into PP_AMT_FORMAT (AMT_FORMAT, DISPLAY_IN_DDDW) values ('$#,##0.00;[RED]$#,##0.00', 1);
insert into PP_AMT_FORMAT (AMT_FORMAT, DISPLAY_IN_DDDW) values ('$#,##0;[RED]$#,##0', 1);
insert into PP_AMT_FORMAT (AMT_FORMAT, DISPLAY_IN_DDDW) values ('$#,##0.00;[RED]($#,##0.00)', 1);
insert into PP_AMT_FORMAT (AMT_FORMAT, DISPLAY_IN_DDDW) values ('$#,##0.00;($#,##0.00)', 1);
insert into PP_AMT_FORMAT (AMT_FORMAT, DISPLAY_IN_DDDW) values ('$#,##0;[RED]($#,##0)', 1);
insert into PP_AMT_FORMAT (AMT_FORMAT, DISPLAY_IN_DDDW) values ('$#,##0;($#,##0)', 1);

create table PP_DYNAMIC_RPT_USER_SAVED
(
 DYN_RPT_USER      varchar2(10),
 REPORT_NAME       varchar2(10),
 DESCRIPTION       varchar2(35),
 REPORT_SYNTAX     long,
 IS_AUTO_SAVE      number(1,0),
 PP_REPORT_TYPE    varchar2(35),
 PP_SUBSYSTEM      varchar2(35),
 PP_FILTER_TYPE    varchar2(35),
 PP_REPORT_TIME    number(22,0),
 PP_RT_START_MONTH number(6, 0),
 PP_RT_END_MONTH   number(6, 0)
);

alter table PP_DYNAMIC_RPT_USER_SAVED
   add constraint PP_DYNAMIC_RPT_USER_SAVED_PK
       primary key (REPORT_NAME, DYN_RPT_USER, IS_AUTO_SAVE)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (332, 0, 10, 4, 1, 0, 29533, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029533_sys_dynamic_report_creation_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;