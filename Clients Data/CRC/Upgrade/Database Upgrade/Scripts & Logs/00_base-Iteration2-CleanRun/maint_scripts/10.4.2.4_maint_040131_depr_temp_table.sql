/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040131_depr_temp_table.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.4 09/30/2014 Brandon Beck
||============================================================================
*/

create global temporary table DEPR_FCST_GROUP_STG
(
 FCST_DEPR_GROUP_ID number(22,0)
) on commit preserve rows;

comment on table DEPR_FCST_GROUP_STG is 'A global temporary table used for calculating forecasted depreciation';
comment on column DEPR_FCST_GROUP_STG.FCST_DEPR_GROUP_ID is 'Identifier for the forecast depreciation group to go through a forecast depreciation calculation';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1444, 0, 10, 4, 2, 4, 40131, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.4_maint_040131_depr_temp_table.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;