/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043722_ls_tax_dist_id_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1   04/27/2015 Daniel Motter  Creation
||============================================================================
*/

alter table ls_monthly_tax drop constraint fk_ls_tax_dist_id;

alter table ls_monthly_tax modify (tax_district_id varchar2(18));



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2531, 0, 2015, 1, 0, 0, 43722, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043722_ls_tax_dist_id_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;