/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011111_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/19/2012 Julia Breuer   Point Release
||============================================================================
*/

-- Make the description field on responsible entity longer
alter table PWRPLANT.PT_PARCEL_RESPONSIBLE_ENTITY modify DESCRIPTION varchar2(100);

-- Update the import columns for responsible entity addresses
update PWRPLANT.PP_IMPORT_COLUMN set COLUMN_TYPE = 'varchar2(100)' where IMPORT_TYPE_ID = 14 and COLUMN_NAME in ('address_1', 'address_2', 'address_3', 'description');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (231, 0, 10, 4, 1, 0, 11111, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011111_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;


