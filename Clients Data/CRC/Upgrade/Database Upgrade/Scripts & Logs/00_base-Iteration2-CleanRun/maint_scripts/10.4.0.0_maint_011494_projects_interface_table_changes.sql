SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011494_projects_interface_table_changes.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   12/17/2012 Joseph King    Point Release
||============================================================================
*/

alter table PWRPLANT.WO_INTERFACE_STAGING     modify DESCRIPTION varchar2(35) null;
alter table PWRPLANT.WO_INTERFACE_STAGING_ARC modify DESCRIPTION varchar2(35) null;

alter table PWRPLANT.WO_INTERFACE_STAGING     add ERROR_MESSAGE clob;
alter table PWRPLANT.WO_INTERFACE_STAGING_ARC add ERROR_MESSAGE clob;

alter table PWRPLANT.WO_INTERFACE_UNIT     add ERROR_MESSAGE clob;
alter table PWRPLANT.WO_INTERFACE_UNIT_ARC add ERROR_MESSAGE clob;

alter table PWRPLANT.JOB_TASK_INTERFACE_STAGING     add ERROR_MESSAGE clob;
alter table PWRPLANT.JOB_TASK_INTERFACE_STAGING_ARC add ERROR_MESSAGE clob;

declare
   L_COUNT number;
   L_SQL   varchar2(255);

begin
   select count(*)
     into L_COUNT
     from ALL_TAB_COLUMNS
    where OWNER = 'PWRPLANT'
      and TABLE_NAME = 'JOB_TASK_INTERFACE_STAGING'
      and COLUMN_NAME = 'DESCRIPTION'
      and NULLABLE = 'N';

   L_SQL := 'alter table PWRPLANT.JOB_TASK_INTERFACE_STAGING MODIFY DESCRIPTION varchar2(35)';

   if L_COUNT > 0 then
      L_SQL := L_SQL || ' null';
      execute immediate L_SQL;
      DBMS_OUTPUT.PUT_LINE(L_SQL || ';');
      DBMS_OUTPUT.PUT_LINE('Successfull.');
   else
      execute immediate L_SQL;
      DBMS_OUTPUT.PUT_LINE(L_SQL || ';');
      DBMS_OUTPUT.PUT_LINE('Successfull.');
   end if;

   select count(*)
     into L_COUNT
     from ALL_TAB_COLUMNS
    where OWNER = 'PWRPLANT'
      and TABLE_NAME = 'JOB_TASK_INTERFACE_STAGING_ARC'
      and COLUMN_NAME = 'DESCRIPTION'
      and NULLABLE = 'N';

   L_SQL := 'alter table PWRPLANT.JOB_TASK_INTERFACE_STAGING_ARC MODIFY DESCRIPTION varchar2(35)';

   if L_COUNT > 0 then
      L_SQL := L_SQL || ' null';
      execute immediate L_SQL;
      DBMS_OUTPUT.PUT_LINE(L_SQL || ';');
      DBMS_OUTPUT.PUT_LINE('Successfull.');
   else
      execute immediate L_SQL;
      DBMS_OUTPUT.PUT_LINE(L_SQL || ';');
      DBMS_OUTPUT.PUT_LINE('Successfull.');
   end if;
end;
/

alter table PWRPLANT.WO_INTERFACE_STAGING     add OLD_WO_STATUS_ID number(22);
alter table PWRPLANT.WO_INTERFACE_STAGING_ARC add OLD_WO_STATUS_ID number(22);

alter table PWRPLANT.WO_INTERFACE_STAGING     add EXT_WORK_ORDER_STATUS number(22);
alter table PWRPLANT.WO_INTERFACE_STAGING_ARC add EXT_WORK_ORDER_STATUS number(22);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (266, 0, 10, 4, 0, 0, 11494, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011494_projects_interface_table_changes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;