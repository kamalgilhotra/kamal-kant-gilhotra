/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052083_lessee_01_backfill_in_service_and_weighted_exchange_rates_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  07/25/2018 Josh Sandler     Backfill rates on in service ILRs
||============================================================================
*/

DELETE FROM ls_ilr_weighted_avg_rates;

UPDATE ls_ilr_options
SET in_service_exchange_rate = 1
WHERE (ilr_id, revision) IN (
  SELECT lio.ilr_id, lio.revision
  FROM ls_ilr_approval lia
    JOIN ls_ilr_options lio ON lia.ilr_id = lio.ilr_id AND lia.revision = lio.revision
    JOIN ls_ilr ilr ON ilr.ilr_id = lio.ilr_id
  WHERE lia.approval_status_id IN (3,6) --Approved and Auto Approved
  AND lio.in_service_exchange_rate IS NULL
  AND ilr.ilr_status_id NOT IN (3,6) --Retired and Pending In Service
);

--Non-remeasurements and OM
INSERT INTO ls_ilr_weighted_avg_rates (ilr_id, revision, set_of_books_id, gross_weighted_avg_rate, net_weighted_avg_rate)
SELECT lio.ilr_id, lio.revision, csob.set_of_books_id,
lio.in_service_exchange_rate,
lio.in_service_exchange_rate
FROM ls_ilr ilr
  JOIN ls_ilr_options lio ON lio.ilr_id = ilr.ilr_id
  JOIN company_set_of_books csob ON csob.company_id = ilr.company_id
  JOIN ls_lease_cap_type lct ON lct.ls_lease_cap_type_id = lio.lease_cap_type_id
  JOIN ls_ilr_approval lia ON lia.ilr_id = lio.ilr_id AND lia.revision = lio.revision
  JOIN ls_fasb_cap_type_sob_map fasb ON fasb.lease_cap_type_id = lct.ls_lease_cap_type_id AND fasb.set_of_books_id = csob.set_of_books_id
WHERE lio.revision > 0
AND lia.approval_status_id IN (3,6)
AND (remeasurement_date IS NULL OR fasb.fasb_cap_type_id IN (4,5))
AND (lio.ilr_id, lio.revision, csob.set_of_books_id) NOT IN (SELECT ilr_id, revision, set_of_books_id FROM ls_ilr_weighted_avg_rates)
;

--Off Balance Sheet to On Balance Sheet Remeasurements
INSERT INTO ls_ilr_weighted_avg_rates (ilr_id, revision, set_of_books_id, gross_weighted_avg_rate, net_weighted_avg_rate)
WITH approved_revs AS (
  SELECT ilr_id,revision
  FROM ls_ilr_approval
  WHERE approval_status_id IN (3,6)
), sch AS (
  SELECT sch.ilr_id, sch.revision, sch.set_of_books_id, sch.is_om cur_is_om, Lag(sch.is_om, 1) OVER(PARTITION BY sch.ilr_id, sch.revision, set_of_books_id ORDER BY month) prev_is_om
  FROM ls_ilr_schedule sch
  WHERE sch.revision > 0
  AND (sch.ilr_id, sch.revision) IN (SELECT ilr_id, revision FROM approved_revs)
)
SELECT DISTINCT sch.ilr_id, sch.revision, sch.set_of_books_id, lio.in_service_exchange_rate, lio.in_service_exchange_rate
FROM sch
  JOIN ls_ilr_options lio ON lio.ilr_id = sch.ilr_id AND lio.revision = sch.revision
WHERE cur_is_om = 0
AND prev_is_om = 1
AND lio.remeasurement_date IS NOT NULL
AND (sch.ilr_id, sch.revision, set_of_books_id) NOT IN (SELECT ilr_id, revision, set_of_books_id FROM ls_ilr_weighted_avg_rates)
;


--On Balance Sheet Remeasurements
--Need to loop in order of effective date to build weighted rates on top of previous revision
BEGIN
  FOR L_REMEASURED_ILRS IN (
    SELECT
    DISTINCT ilr_rev.ilr_id, ilr_rev.revision, ilr_rev.previous_revision, ilr_rev.effective_month MONTH
    FROM (
      SELECT lio.ilr_id, lio.revision, Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month, remeasurement_date,
      Lag(lio.revision, 1) OVER(PARTITION BY lio.ilr_id, fasb.set_of_books_id ORDER BY Nvl(lio.remeasurement_date, ilr.est_in_svc_date)) previous_revision,
      fasb.fasb_cap_type_id,
      fasb.set_of_books_id
      FROM ls_ilr ilr
        JOIN ls_ilr_approval lia ON lia.ilr_id = ilr.ilr_id
        JOIN ls_ilr_options lio ON lio.ilr_id = lia.ilr_id AND lio.revision = lia.revision
        JOIN ls_lease_cap_type lct ON lio.lease_cap_type_id = lct.ls_lease_cap_type_id
        JOIN ls_fasb_cap_type_sob_map fasb ON fasb.lease_cap_type_id = lct.ls_lease_cap_type_id
      AND lia.approval_status_id IN (3,6)
      AND lio.revision > 0
    ) ilr_rev
      JOIN ls_ilr_options prev_lio ON prev_lio.ilr_id = ilr_rev.ilr_id AND prev_lio.revision = ilr_rev.previous_revision
      JOIN ls_lease_cap_type prev_lct ON prev_lio.lease_cap_type_id = prev_lct.ls_lease_cap_type_id
      JOIN ls_fasb_cap_type_sob_map prev_fasb ON prev_fasb.lease_cap_type_id = prev_lct.ls_lease_cap_type_id AND prev_fasb.set_of_books_id = ilr_rev.set_of_books_id
    WHERE ilr_rev.previous_revision IS NOT NULL
    AND ilr_rev.remeasurement_date IS NOT NULL
    AND ilr_rev.fasb_cap_type_id IN (1,2)
    AND prev_fasb.fasb_cap_type_id IN (1,2)
    ORDER BY ilr_rev.ilr_id, ilr_rev.effective_month ASC, revision ASC
  )
  LOOP
    merge INTO ls_ilr_weighted_avg_rates war USING (
      WITH
      amounts AS (
        SELECT DISTINCT
        sch.ilr_id, sch.revision, sch.set_of_books_id,
        lio.in_service_exchange_rate,
        war.gross_weighted_avg_rate,
        war.net_weighted_avg_rate,
        beg_net_rou_asset,
        end_capital_cost - beg_capital_cost AS capital_cost_change,
        beg_capital_cost,
        end_capital_cost
        FROM ls_ilr_schedule sch
          JOIN ls_ilr_options lio ON lio.ilr_id = sch.ilr_id AND lio.revision = sch.revision
          JOIN ls_ilr_weighted_avg_rates war ON war.ilr_id = sch.ilr_id AND war.set_of_books_id = sch.set_of_books_id
        WHERE sch.ilr_id = L_REMEASURED_ILRS.ILR_ID
        AND sch.revision = L_REMEASURED_ILRS.REVISION
        AND sch.MONTH =Trunc(lio.remeasurement_date,'month')
        AND war.revision = L_REMEASURED_ILRS.PREVIOUS_REVISION
        AND sch.is_om = 0
      )
      SELECT amounts.ilr_id, amounts.revision, amounts.set_of_books_id,
      (amounts.net_weighted_avg_rate*amounts.beg_net_rou_asset + amounts.in_service_exchange_rate*amounts.capital_cost_change)/(amounts.beg_net_rou_asset + amounts.capital_cost_change) new_net_weighted_avg_rate,
      (amounts.gross_weighted_avg_rate*amounts.beg_capital_cost + amounts.in_service_exchange_rate*amounts.capital_cost_change)/amounts.end_capital_cost new_gross_weighted_avg_rate
      FROM amounts
    ) rate_calc
    ON (war.ilr_id = rate_calc.ilr_id AND war.revision = rate_calc.revision AND war.set_of_books_id = rate_calc.set_of_books_id)
    WHEN matched THEN
    UPDATE SET war.gross_weighted_avg_rate = rate_calc.new_gross_weighted_avg_rate,
                war.net_weighted_avg_rate = rate_calc.new_net_weighted_avg_rate
    WHEN NOT matched THEN
    INSERT (
    war.ilr_id,
    war.revision,
    war.set_of_books_id,
    war.gross_weighted_avg_rate,
    war.net_weighted_avg_rate
    )
    VALUES (
    rate_calc.ilr_id,
    rate_calc.revision,
    rate_calc.set_of_books_id,
    rate_calc.new_gross_weighted_avg_rate,
    rate_calc.new_net_weighted_avg_rate
    );

    DBMS_OUTPUT.PUT_LINE('ILR_ID: ' || To_Char(L_REMEASURED_ILRS.ILR_ID) || ' Revision: ' || To_Char(L_REMEASURED_ILRS.REVISION) || ' (' || TO_Char(SQL%ROWCOUNT)||' rows merged)');
  END LOOP;
END;
/

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8704, 0, 2017, 4, 0, 0, 52083, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052083_lessee_01_backfill_in_service_and_weighted_exchange_rates_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;