/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047558_lease_payment_detail_by_asset_query_support_multicurrency_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/03/2017 Josh Sandler     Update Lease Payment Detail by Asset query to support filtering on both Company and Contract currency
||============================================================================
*/

UPDATE pp_any_query_criteria
SET SQL = 'WITH currency_selection AS (
SELECT lct.ls_currency_type_id, lct.description
FROM ls_lease_currency_type lct, pp_any_required_filter parf
where upper(trim(parf.column_name)) = ''CURRENCY TYPE''
AND  upper(trim(parf.filter_value)) =  upper(trim(lct.description))
)
select la.ls_asset_id,
       to_char(lpl.gl_posting_mo_yr, ''yyyymm'') as monthnum,
       co.company_id,
       co.description as company_description,
       la.leased_asset_number,
       ilr.ilr_id,
       ilr.ilr_number,
       ll.lease_id,
       ll.lease_number,
       lct.description as lease_cap_type,
       al.long_description as location,
       nvl(sum(decode(lpl.payment_type_id, 1, lpl.amount, 0)), 0) principal_payment,
       nvl(sum(decode(lpl.payment_type_id, 2, lpl.amount, 0)), 0) interest_payment,
       nvl(sum(decode(lpl.payment_type_id, 2, lpl.adjustment_amount, 0)), 0) interest_adjustment,
       nvl(sum(decode(lpl.payment_type_id, 3, lpl.amount, 0)), 0) executory_payment1,
       nvl(sum(decode(lpl.payment_type_id, 3, lpl.adjustment_amount, 0)), 0) executory_adjustment1,
       nvl(sum(decode(lpl.payment_type_id, 4, lpl.amount, 0)), 0) executory_payment2,
       nvl(sum(decode(lpl.payment_type_id, 4, lpl.adjustment_amount, 0)), 0) executory_adjustment2,
       nvl(sum(decode(lpl.payment_type_id, 5, lpl.amount, 0)), 0) executory_payment3,
       nvl(sum(decode(lpl.payment_type_id, 5, lpl.adjustment_amount, 0)), 0) executory_adjustment3,
       nvl(sum(decode(lpl.payment_type_id, 6, lpl.amount, 0)), 0) executory_payment4,
       nvl(sum(decode(lpl.payment_type_id, 6, lpl.adjustment_amount, 0)), 0) executory_adjustment4,
       nvl(sum(decode(lpl.payment_type_id, 7, lpl.amount, 0)), 0) executory_payment5,
       nvl(sum(decode(lpl.payment_type_id, 7, lpl.adjustment_amount, 0)), 0) executory_adjustment5,
       nvl(sum(decode(lpl.payment_type_id, 8, lpl.amount, 0)), 0) executory_payment6,
       nvl(sum(decode(lpl.payment_type_id, 8, lpl.adjustment_amount, 0)), 0) executory_adjustment6,
       nvl(sum(decode(lpl.payment_type_id, 9, lpl.amount, 0)), 0) executory_payment7,
       nvl(sum(decode(lpl.payment_type_id, 9, lpl.adjustment_amount, 0)), 0) executory_adjustment7,
       nvl(sum(decode(lpl.payment_type_id, 10, lpl.amount, 0)), 0) executory_payment8,
       nvl(sum(decode(lpl.payment_type_id, 10, lpl.adjustment_amount, 0)), 0) executory_adjustment8,
       nvl(sum(decode(lpl.payment_type_id, 11, lpl.amount, 0)), 0) executory_payment9,
       nvl(sum(decode(lpl.payment_type_id, 11, lpl.adjustment_amount, 0)), 0) executory_adjustment9,
       nvl(sum(decode(lpl.payment_type_id, 12, lpl.amount, 0)), 0) executory_payment10,
       nvl(sum(decode(lpl.payment_type_id, 12, lpl.adjustment_amount, 0)), 0) executory_adjustment10,
       nvl(sum(decode(lpl.payment_type_id, 13, lpl.amount, 0)), 0) contingent_payment1,
       nvl(sum(decode(lpl.payment_type_id, 13, lpl.adjustment_amount, 0)), 0) contingent_adjustment1,
       nvl(sum(decode(lpl.payment_type_id, 14, lpl.amount, 0)), 0) contingent_payment2,
       nvl(sum(decode(lpl.payment_type_id, 14, lpl.adjustment_amount, 0)), 0) contingent_adjustment2,
       nvl(sum(decode(lpl.payment_type_id, 15, lpl.amount, 0)), 0) contingent_payment3,
       nvl(sum(decode(lpl.payment_type_id, 15, lpl.adjustment_amount, 0)), 0) contingent_adjustment3,
       nvl(sum(decode(lpl.payment_type_id, 16, lpl.amount, 0)), 0) contingent_payment4,
       nvl(sum(decode(lpl.payment_type_id, 16, lpl.adjustment_amount, 0)), 0) contingent_adjustment4,
       nvl(sum(decode(lpl.payment_type_id, 17, lpl.amount, 0)), 0) contingent_payment5,
       nvl(sum(decode(lpl.payment_type_id, 17, lpl.adjustment_amount, 0)), 0) contingent_adjustment5,
       nvl(sum(decode(lpl.payment_type_id, 18, lpl.amount, 0)), 0) contingent_payment6,
       nvl(sum(decode(lpl.payment_type_id, 18, lpl.adjustment_amount, 0)), 0) contingent_adjustment6,',
sql2 = '
nvl(sum(decode(lpl.payment_type_id, 19, lpl.amount, 0)), 0) contingent_payment7,
       nvl(sum(decode(lpl.payment_type_id, 19, lpl.adjustment_amount, 0)), 0) contingent_adjustment7,
       nvl(sum(decode(lpl.payment_type_id, 20, lpl.amount, 0)), 0) contingent_payment8,
       nvl(sum(decode(lpl.payment_type_id, 20, lpl.adjustment_amount, 0)), 0) contingent_adjustment8,
       nvl(sum(decode(lpl.payment_type_id, 21, lpl.amount, 0)), 0) contingent_payment9,
       nvl(sum(decode(lpl.payment_type_id, 21, lpl.adjustment_amount, 0)), 0) contingent_adjustment9,
       nvl(sum(decode(lpl.payment_type_id, 22, lpl.amount, 0)), 0) contingent_payment10,
       nvl(sum(decode(lpl.payment_type_id, 22, lpl.adjustment_amount, 0)), 0) contingent_adjustment10,
       nvl(sum(decode(lpl.payment_type_id, 23, lpl.amount, 0)), 0) termination_penalty,
       nvl(sum(decode(lpl.payment_type_id, 24, lpl.amount, 0)), 0) sales_proceeds,
       nvl(tax_1, 0) as tax_1,
       nvl(tax_1_adjustment, 0) as tax_1_adjustment,
       nvl(tax_2, 0) as tax_2,
       nvl(tax_2_adjustment, 0) as tax_2_adjustment,
       nvl(tax_3, 0) as tax_3,
       nvl(tax_3_adjustment, 0) as tax_3_adjustment,
       nvl(tax_4, 0) as tax_4,
       nvl(tax_4_adjustment, 0) as tax_4_adjustment,
       nvl(tax_5, 0) as tax_5,
       nvl(tax_5_adjustment, 0) as tax_5_adjustment,
       nvl(tax_6, 0) as tax_6,
       nvl(tax_6_adjustment, 0) as tax_6_adjustment,
       nvl(tax_7, 0) as tax_7,
       nvl(tax_7_adjustment, 0) as tax_7_adjustment,
       InitCap(currency_selection.description) currency_type,
       lpl.iso_code currency,
       lpl.currency_display_symbol currency_symbol
  from ls_asset la
       JOIN company co ON (la.company_id = co.company_id)
       JOIN v_ls_payment_line_fx lpl ON (lpl.ls_asset_id = la.ls_asset_id)
       JOIN asset_location al ON (la.asset_location_id = al.asset_location_id)
       JOIN ls_ilr ilr ON (la.ilr_id = ilr.ilr_id)
       JOIN ls_lease ll ON (ilr.lease_id = ll.lease_id)
       JOIN ls_ilr_options ilro ON (la.ilr_id = ilro.ilr_id and la.approved_revision = ilro.revision)
       JOIN ls_lease_cap_type lct ON (ilro.lease_cap_type_id = lct.ls_lease_cap_type_id)
       JOIN currency_selection ON (lpl.ls_cur_type = currency_selection.ls_currency_type_id)
       left OUTER JOIN (select lmt.ls_asset_id,
               nvl(sum(decode(tax_local_id, 1, amount, 0)), 0) as tax_1,
               nvl(sum(decode(tax_local_id, 1, adjustment_amount, 0)), 0) as tax_1_adjustment,
               nvl(sum(decode(tax_local_id, 2, amount, 0)), 0) as tax_2,
               nvl(sum(decode(tax_local_id, 2, adjustment_amount, 0)), 0) as tax_2_adjustment,
               nvl(sum(decode(tax_local_id, 3, amount, 0)), 0) as tax_3,
               nvl(sum(decode(tax_local_id, 3, adjustment_amount, 0)), 0) as tax_3_adjustment,
               nvl(sum(decode(tax_local_id, 4, amount, 0)), 0) as tax_4,
               nvl(sum(decode(tax_local_id, 4, adjustment_amount, 0)), 0) as tax_4_adjustment,
               nvl(sum(decode(tax_local_id, 5, amount, 0)), 0) as tax_5,
               nvl(sum(decode(tax_local_id, 5, adjustment_amount, 0)), 0) as tax_5_adjustment,
               nvl(sum(decode(tax_local_id, 6, amount, 0)), 0) as tax_6,
               nvl(sum(decode(tax_local_id, 6, adjustment_amount, 0)), 0) as tax_6_adjustment,
               nvl(sum(decode(tax_local_id, 7, amount, 0)), 0) as tax_7,
               nvl(sum(decode(tax_local_id, 7, adjustment_amount, 0)), 0) as tax_7_adjustment,
               contract_currency_id,
               company_currency_id,
               ls_cur_type
          from v_ls_monthly_tax_fx_vw lmt, currency_selection
         where to_char(gl_posting_mo_yr, ''yyyymm'') in
               (select filter_value
                  from pp_any_required_filter
                 where upper(trim(column_name)) = ''MONTHNUM'')
           and lmt.accrual = 0
            AND lmt.ls_cur_type = currency_selection.ls_currency_type_id',
sql3 = '
  and lmt.company_id in
               (select filter_value
                  from pp_any_required_filter
                 where upper(trim(column_name)) = ''COMPANY ID'')
         group by lmt.ls_asset_id,
         contract_currency_id,
               company_currency_id,
               ls_cur_type) taxes ON (la.ls_asset_id = taxes.ls_asset_id AND currency_selection.ls_currency_type_id = taxes.ls_cur_type)
 where to_char(la.company_id) in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''COMPANY ID'')
   and to_char(lpl.gl_posting_mo_yr, ''yyyymm'') in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''MONTHNUM'')
 group by la.ls_asset_id,
          lpl.gl_posting_mo_yr,
          co.company_id,
          co.description,
          la.leased_asset_number,
          ilr.ilr_id,
          ilr.ilr_number,
          lct.description,
          al.long_description,
          ll.lease_id,
          ll.lease_number,
          tax_1,
          tax_1_adjustment,
          tax_2,
          tax_2_adjustment,
          tax_3,
          tax_3_adjustment,
          tax_4,
          tax_4_adjustment,
          tax_5,
          tax_5_adjustment,
          tax_6,
          tax_6_adjustment,
          tax_7,
          tax_7_adjustment,
          lpl.ls_cur_type,
          lpl.iso_code,
          lpl.currency_display_symbol,
          currency_selection.description'
WHERE description = 'Lease Payment Detail by Asset';

UPDATE pp_any_query_criteria_fields
SET hide_from_results = 0
WHERE id = (SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Payment Detail by Asset')
AND detail_field = 'currency_type';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3622, 0, 2017, 1, 0, 0, 47558, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047558_lease_payment_detail_by_asset_query_support_multicurrency_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;