/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_051563_lessor_01_create_rate_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2017.4.0.0 06/28/2018 Powers       	  Add new currency rate type and system control
||                                        Add new trans type 4068 Lease Currency Gain/Loss ST Debit
||============================================================================
*/

/* Add the new trans type 4068 - Lessor Currency Gain/Loss ST Debit */   
merge into je_trans_type a
using (select 4068 trans_type, '4068 - Lessor Currency Gain/Loss ST Debit' description from dual) x 
on (a.trans_type = x.trans_type)
when matched then update set a.description = x.description
when not matched then insert (a.trans_type, a.description) values (x.trans_type, x.description);   

/* Relate the new trans type to the delivered JE Method */
insert into je_method_trans_type(je_method_id, trans_type)
select * 
from (select 1 AS je_method_id,	trans_type 
	  from je_trans_type 
	  where trans_type = 4068
	 )
where not exists(
	select 1
	from je_method_trans_type
	where trans_type = 4068);
	
/* Update the description on 4051 to be '4051 - Lessor Currency Gain/Loss LT Debit'	*/
update je_trans_type 
set description = '4051 - Lessor Currency Gain/Loss LT Debit'
where trans_type = 4051;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7542, 0, 2017, 4, 0, 0, 51563, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051563_lessor_01_create_rate_type_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;