/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039542_reg_fix_inc_data_tables.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/13/2014 Shane Ward
||============================================================================
*/

delete from REG_INCREMENTAL_DEPR_ADJ_TRANS;

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS drop primary key drop index;

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS add INC_DATA_ID number(22,0) not null;

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint PK_REG_INCREMENTAL_DEPR_ADJ_TR
       primary key (INC_DATA_ID,
                    FORECAST_VERSION_ID,
                    INCREMENTAL_ADJ_ID)
       using index tablespace PWRPLANT_IDX;

delete from REG_INCREMENTAL_FP_ADJ_TRANS;

alter table REG_INCREMENTAL_FP_ADJ_TRANS drop primary key drop index;

alter table REG_INCREMENTAL_FP_ADJ_TRANS add INC_DATA_ID number(22,0) not null;

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint PK_REG_INCREMENTAL_FP_ADJ_TR
       primary key (INC_DATA_ID,
                    FORECAST_VERSION_ID,
                    INCREMENTAL_ADJ_ID)
       using index tablespace PWRPLANT_IDX;
	   
comment on column REG_INCREMENTAL_FP_ADJ_TRANS.INC_DATA_ID is 'System assigned identifier created by INC_DATA_SEQ sequence.';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.INC_DATA_ID is 'System assigned identifier created by INC_DATA_SEQ sequence.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1335, 0, 10, 4, 2, 7, 39542, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039542_reg_fix_inc_data_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
