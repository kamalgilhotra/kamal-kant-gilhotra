/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007443_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/15/2011 Blake Andrews  Point Release
||============================================================================
*/

--maint-007443

alter table PWRPLANT.TAX_ACCRUAL_M_ITEM_ROLLUPS
   drop constraint TA_M_ITEM_ROLL_DTL_M_ITEM_FK drop index;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (16, 0, 10, 3, 3, 0, 7443, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007443_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
