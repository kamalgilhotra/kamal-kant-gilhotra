/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030261_taxrpr_RPR_BASIS_PKG.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   07/25/2013 Alex P.         Point Release
||============================================================================
*/

create table D1_REPAIR_BOOK_SUMMARY as select * from REPAIR_BOOK_SUMMARY;
drop table REPAIR_BOOK_SUMMARY;

create or replace package RPR_BASIS_PKG is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: RPR_BASIS_PKG
   || Description: Functions and Procedures to maipulate Tax Repairs basis buckets
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 07/19/2013 Alex P.
   ||============================================================================
   */

   -- Public function and procedure declarations
   procedure P_POPULATE_BASIS_BUCKETS;

   procedure P_CREATE_REPAIR_BOOK_SUMMARY;

   procedure P_GET_BASIS_BUCKETS(A_REPAIR_BOOK_SUMMARY_ID     out number,
                                 A_RETIREMENT_BOOK_SUMMARY_ID out number,
                                 A_ADD_QUALIFIED_SQLS         out varchar2,
                                 A_RETIREMENT_SQLS            out varchar2,

                                 A_AFUDC_EQUITY_SQLS           out varchar2,
                                 A_AFUDC_DEBT_SQLS             out varchar2,
                                 A_OTHER_BOOK_BASIS_DIFFS_SQLS out varchar2,
                                 A_OTHER_TAX_BASIS_DIFFS_SQLS  out varchar2,
                                 A_TOTAL_BOOK_COST_SQLS        out varchar2,
                                 A_REVERSAL_BASIS_DIFFS_SQLS   out varchar2,

                                 A_ADD_QUALIFIED_STR        out varchar2,
                                 A_RETIREMENT_STR           out varchar2,
                                 A_REVERSAL_BASIS_DIFFS_STR out varchar2,

                                 A_REVERSALS_BOOKED_SQLS     out varchar2,
                                 A_ACTIVITY_COST_SQLS        out varchar2,
                                 A_CHARGE_TYPE_VIEW_SQLS     out varchar2,
                                 A_BOOK_ONLY_SQLS            out varchar2,
                                 A_TAX_ONLY_BASIS_DIFFS_SQLS out varchar2);

end RPR_BASIS_PKG;
/


create or replace package body RPR_BASIS_PKG is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: RPR_BASIS_PKG
   || Description: Functions and Procedures to maipulate Tax Repairs basis buckets
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 07/19/2013 Alex P.
   ||============================================================================
   */

   /******************************************************************************
   i_repair_book_summary_id:  Tax Repairs Taken basis bucket number
       Example: 19
   i_retirement_book_summary_id:  Retirement Reversal Taken basis bucket number
       Example: 21
   i_add_qualified_sqls: The SQL clause that captures all book costs that qualify to be expensed for tax.
       Example: cab.basis_1 + cab.basis_2 + cab.basis_3 + cab.basis_7 + cab.basis_8 + cab.basis_9 + cab.basis_10 + cab.basis_11 + cab.basis_15 + cab.basis_16 + cab.basis_17 + cab.basis_18
   i_retirement_sqls:  The SQL clause that captures the book costs that are not subject to tax retirements due to tax repairs.
       Example: i_retirement_sqls = cab.basis_1 + cab.basis_2 + cab.basis_3 + cab.basis_4 + cab.basis_5 + cab.basis_7 + cab.basis_8 + cab.basis_9 + cab.basis_10 + cab.basis_11 + cab.basis_15 + cab.basis_16 + cab.basis_17 + cab.basis_18
   i_afudc_equity_sqls:  Sum of ADUDC equity buckets
       Example:  cab.basis_5
   i_afudc_debt_sqls:  Sum of AFUDC debt buckets
       Example:  cab.basis_4
   i_other_book_basis_diffs_sqls: Book Only Costs other that AFUDC Debt and Equity
       Example: same format as AFUDC Equity and Debt, oftentimes empty.
   i_other_tax_basis_diffs_sqls: Tax Only Cost other than CPI
       Example: same format as AFUDC Equity and Debt, oftentimes empty.
   i_total_book_cost_sqls:  Sum of all the book cost basis nuckets
       Example: cab.basis_1 + cab.basis_2 + cab.basis_3 + cab.basis_4 + cab.basis_5 + cab.basis_7 + cab.basis_8 + cab.basis_9 + cab.basis_10 + cab.basis_11 + cab.basis_15 + cab.basis_16 + cab.basis_17 + cab.basis_18
   i_reversal_basis_diffs_sqls:  This holds an sql for the sum of tax expense reversal basis buckets.
       Example: cab.basis_20
   i_add_qualified_str: Comma seperated list of add qualified basis buckets.
   i_retirement_str: Comma seperated list of basis buckets eligible for retirement reversal.
   i_reversal_basis_diffs_str:  This holds the comma seperated list of tax repairs output basis buckets
       Example: 19, 21, 20
   i_reversals_booked_sqls: This holds the decode that is used to get the reversal booked in each Tax Repairs book summary in cpr_act_basis
       Example: decode(bs.book_summary_id, 19, decode (ca.ferc_activity_code, 2,0, cab.basis_19), 21, case when ca.ferc_activity_code = 2 and ca.tax_orig_month_number is not null then cab.basis_21 else 0 end , 20, decode  (ca.ferc_activity_code, 2, 0, cab.basis_20), 0)
   i_activity_cost_sqls:  This holds the decode that is used to get the activity that should be reversed for each Tax Repairs book summary in cpr_act_basis
       Example: decode(bs.book_summary_id, 19 decode (ca.ferc_activity_code, 2,0, cab.basis_1 + cab.basis_2 + cab.basis_3 + cab.basis_7 + cab.basis_8 + cab.basis_9 + cab.basis_10 + cab.basis_11 + cab.basis_15 + cab.basis_16 + cab.basis_17 + cab.basis_18) , 21, decode (ca.ferc_activity_code, 2, cab.basis_1 + cab.basis_2 + cab.basis_3 + cab.basis_4 + cab.basis_5 + cab.basis_7 + cab.basis_8 + cab.basis_9 + cab.basis_10 + cab.basis_11 + cab.basis_15 + cab.basis_16 + cab.basis_17 + cab.basis_18,0)20, decode (ca.ferc_activity_code, 2, 0, cab.basis_14) , 0)
   i_charge_type_view_sqls:  This holds the view that specifies which charge types to reverse in which tax repairs charge types
       Example: select * from ( select reversal_ct.charge_type_id reversal_charge_type, ce.cost_element_id reversal_cost_element, ct.charge_type_id charge_type_to_reverse, decode(ct.charge_type_id,reversal_ct.charge_type_id,1,0) is_reversal_ct from charge_type reversal_ct, charge_type ct, cost_element ce where reversal_ct.book_summary_id = 19 and reversal_ct.charge_type_id = ce.charge_type_id and ct.book_summary_id in (1, 2, 3, 7, 8, 9, 10, 11, 15, 16, 17, 18, 19) union all select reversal_ct.charge_type_id reversal_charge_type, ce.cost_element_id reversal_cost_element, ct.charge_type_id charge_type_to_reverse, decode(ct.charge_type_id,reversal_ct.charge_type_id,1,0) is_reversal_ct from charge_type reversal_ct, charge_type ct, cost_element ce where reversal_ct.book_summary_id = 20 and reversal_ct.charge_type_id = ce.charge_type_id and ct.book_summary_id in (14, 20) union all select 0 reversal_charge_type, 0 reversal_cost_element, ct.charge_type_id charge_type_to_reverse, 2 is_reversal_ct from charge_type ct where ct.book_summary_id in (5, 4) ) where -5=-5
   i_book_only_sqls:  This hold the sum of book only basis buckets. i_book_only_sqls = i_afudc_equity_sqls + i_afudc_debt_sqls + i_other_book_basis_diffs_sqls
       Example: cab.basis_5 + cab.basis_4
   i_tax_only_basis_diffs_sqls:  This holds an sql for the sum of tax only basis buckets, such as CPI found in tax_reversal_book_summary table.
       Example: cab.basis_14
   ******************************************************************************/

   I_REPAIR_BOOK_SUMMARY_ID     number;
   I_RETIREMENT_BOOK_SUMMARY_ID number;

   I_ADD_QUALIFIED_SQLS varchar2(2000);
   I_RETIREMENT_SQLS    varchar2(2000);

   I_AFUDC_EQUITY_SQLS           varchar2(2000);
   I_AFUDC_DEBT_SQLS             varchar2(2000);
   I_OTHER_BOOK_BASIS_DIFFS_SQLS varchar2(2000);
   I_OTHER_TAX_BASIS_DIFFS_SQLS  varchar2(2000);
   I_TOTAL_BOOK_COST_SQLS        varchar2(2000);
   I_REVERSAL_BASIS_DIFFS_SQLS   varchar2(2000);

   I_ADD_QUALIFIED_STR        varchar2(2000);
   I_RETIREMENT_STR           varchar2(2000);
   I_REVERSAL_BASIS_DIFFS_STR varchar2(2000);

   I_REVERSALS_BOOKED_SQLS     varchar2(2000);
   I_ACTIVITY_COST_SQLS        varchar2(2000);
   I_CHARGE_TYPE_VIEW_SQLS     varchar2(2000);
   I_BOOK_ONLY_SQLS            varchar2(2000);
   I_TAX_ONLY_BASIS_DIFFS_SQLS varchar2(2000);

   -- Function and procedure implementations

   --**************************************************************************
   --                            PRINTOUT
   --**************************************************************************
   procedure PRINTOUT(MESSAGE in varchar2) is
   begin
      DBMS_OUTPUT.PUT_LINE(MESSAGE);
   end PRINTOUT;

   --**************************************************************************
   --                            GETBASISINDICATOR
   --**************************************************************************
   procedure GETBASISINDICATOR(IN_COL_NAME in varchar2,
                               OUT_VAL     out number) is
   begin
      execute immediate 'SELECT ' || IN_COL_NAME || ' FROM set_of_books where set_of_books_id = 1'
         into OUT_VAL;
   end GETBASISINDICATOR;

   --**************************************************************************
   --                            P_POPULATE_BASIS_BUCKETS
   --**************************************************************************
   procedure P_POPULATE_BASIS_BUCKETS is

      --******************************************************************************
      -- Assigns values to the instance variables that contain the SQL statements used
      -- to insert basis amounts into the tax repairs processing tables.  This function
      -- only needs to be called once each time the repairs process runs.
      -- It populates private variables of this package.
      --******************************************************************************

      cursor CURREPAIRQUALIFIEDBUCKETS is
         select BS.BOOK_SUMMARY_ID BOOK_SUMMARY_ID,
                BS.SUMMARY_NAME SUMMARY_NAME,
                NVL(BS.RECONCILE_ITEM_ID, 0) RECONCILE_ITEM_ID,
                NVL(RTRIM(UPPER("TYPE")), 'TAX') TAX_RECONCILE_TYPE,
                DECODE(BS.BOOK_SUMMARY_ID,
                       AFC_EQUITY_BOOK_SUMMARY.AFC_EQUITY_BOOK_SUMMARY_ID,
                       'afudc_equity',
                       AFC_DEBT_BOOK_SUMMARY.AFC_DEBT_BOOK_SUMMARY_ID,
                       'afudc_debt',
                       CPI_BOOK_SUMMARY.CPI_BOOK_SUMMARY_ID,
                       'cpi',
                       'none') BOOK_SUMMARY_INTEREST_TYPE
           from BOOK_SUMMARY BS,
                TAX_RECONCILE_ITEM TRI,
                (select distinct C.BOOK_SUMMARY_ID AFC_EQUITY_BOOK_SUMMARY_ID
                   from AFUDC_CONTROL A, COST_ELEMENT B, CHARGE_TYPE C
                  where A.EQUITY_COST_ELEMENT = B.COST_ELEMENT_ID
                    and B.CHARGE_TYPE_ID = C.CHARGE_TYPE_ID) AFC_EQUITY_BOOK_SUMMARY,
                (select distinct C.BOOK_SUMMARY_ID AFC_DEBT_BOOK_SUMMARY_ID
                   from AFUDC_CONTROL A, COST_ELEMENT B, CHARGE_TYPE C
                  where A.DEBT_COST_ELEMENT = B.COST_ELEMENT_ID
                    and B.CHARGE_TYPE_ID = C.CHARGE_TYPE_ID) AFC_DEBT_BOOK_SUMMARY,
                (select distinct C.BOOK_SUMMARY_ID CPI_BOOK_SUMMARY_ID
                   from AFUDC_CONTROL A, COST_ELEMENT B, CHARGE_TYPE C
                  where A.CPI_COST_ELEMENT = B.COST_ELEMENT_ID
                    and B.CHARGE_TYPE_ID = C.CHARGE_TYPE_ID) CPI_BOOK_SUMMARY
          where BS.RECONCILE_ITEM_ID = TRI.RECONCILE_ITEM_ID(+)
            and BS.BOOK_SUMMARY_ID = AFC_EQUITY_BOOK_SUMMARY.AFC_EQUITY_BOOK_SUMMARY_ID(+)
            and BS.BOOK_SUMMARY_ID = AFC_DEBT_BOOK_SUMMARY.AFC_DEBT_BOOK_SUMMARY_ID(+)
            and BS.BOOK_SUMMARY_ID = CPI_BOOK_SUMMARY.CPI_BOOK_SUMMARY_ID(+)
          order by BS.BOOK_SUMMARY_ID;

      cursor CURTAXREVERSALBUCKETS is
         select BOOK_SUMMARY_ID, REVERSAL_BOOK_SUMMARY_ID
           from TAX_REVERSAL_BOOK_SUMMARY
          where TAX_REVERSAL_TYPE = 'Repairs'
          order by REVERSAL_BOOK_SUMMARY_ID;

      cursor CURSETOFBOOKS is
         select * from SET_OF_BOOKS where SET_OF_BOOKS_ID = 1;

      type REPAIRQUALIFIEDBUCKETSREC is table of CURREPAIRQUALIFIEDBUCKETS%rowtype index by binary_integer;
      type TAXREVERSALBUCKETSREC is table of CURTAXREVERSALBUCKETS%rowtype index by binary_integer;
      type SETOFBOOKSREC is table of CURSETOFBOOKS%rowtype index by binary_integer;

      LOCREPAIRQUALIFIEDBUCKETS      REPAIRQUALIFIEDBUCKETSREC;
      LOCTAXREVERSALBUCKETS          TAXREVERSALBUCKETSREC;
      LOCSETOFBOOKS                  SETOFBOOKSREC;
      I                              number := 0;
      K                              number := 0;
      BASIS_INDICATOR                number;
      ROWS_FOUND                     number;
      SQL_TMP                        varchar2(2000);
      STR_TMP                        varchar2(2000);
      BOOK_SUMMARIES_AFUDC_EQUITY    varchar2(2000);
      BOOK_SUMMARIES_AFUDC_DEBT      varchar2(2000);
      BOOK_SUMMARIES_OTHER_BOOKONLY  varchar2(2000);
      BOOK_SUMMARIES_ALL_BOOKONLY    varchar2(2000);
      CHARGE_TYPE_VIEW_SQLS_BASE     varchar2(2000);
      CHARGE_TYPE_VIEW_SQLS_BOOKONLY varchar2(2000);
      BOOK_SUMMARIES_TO_REVERSE      varchar2(2000);
      NEXT_REVERSAL_BUCKET           boolean;

   begin
      K := 0;
      for QUALIFIEDBUCKETROW in CURREPAIRQUALIFIEDBUCKETS
      loop
         K := K + 1;
         LOCREPAIRQUALIFIEDBUCKETS(K) := QUALIFIEDBUCKETROW; -- Load the buckets into a structure
      end loop;

      K := 0;
      for REVERSALBUCKETROW in CURTAXREVERSALBUCKETS
      loop
         K := K + 1;
         LOCTAXREVERSALBUCKETS(K) := REVERSALBUCKETROW; -- Load the book summary into a structure
      end loop;

      K := 0;
      for SETOFBOOKSROW in CURSETOFBOOKS
      loop
         K := K + 1;
         LOCSETOFBOOKS(K) := SETOFBOOKSROW; -- Load the book summary into a structure
      end loop;

      --
      --  Create a SQL that can be used to determine which charge types should be reversed and what charge types they should be reversed into
      --  This will be used to assign the instance variable i_charge_type_view_sqls
      --
      CHARGE_TYPE_VIEW_SQLS_BASE := 'select reversal_ct.charge_type_id reversal_charge_type, ' ||
                                    ' ce.cost_element_id reversal_cost_element, ' ||
                                    ' ct.charge_type_id charge_type_to_reverse, ' ||
                                    ' decode(ct.charge_type_id,reversal_ct.charge_type_id,1,0) is_reversal_ct ' ||
                                    '    from    charge_type reversal_ct, ' ||
                                    '            charge_type ct, ' ||
                                    '            cost_element ce ' ||
                                    '    where reversal_ct.book_summary_id = <REVERSAL_BOOKSUMMARY> ' ||
                                    '        and reversal_ct.charge_type_id = ce.charge_type_id ' ||
                                    '        and ct.book_summary_id in (<BOOKSUMMARY_TO_REVERSE>) ';

      CHARGE_TYPE_VIEW_SQLS_BOOKONLY := 'select 0 reversal_charge_type, ' ||
                                        '            0 reversal_cost_element, ' ||
                                        '            ct.charge_type_id charge_type_to_reverse, ' ||
                                        '            2 is_reversal_ct ' ||
                                        '    from    charge_type ct ' ||
                                        '    where    ct.book_summary_id in (<BOOKSUMMARY_BOOKONLY>) ';
      --
      -- Beging looping on the array
      --
      for K in 1 .. LOCREPAIRQUALIFIEDBUCKETS.COUNT
      loop

         case LOWER(LOCREPAIRQUALIFIEDBUCKETS(K).SUMMARY_NAME)
            when 'tax repairs' then
               I_REPAIR_BOOK_SUMMARY_ID := LOCREPAIRQUALIFIEDBUCKETS(K).BOOK_SUMMARY_ID;
               CONTINUE;
            when 'retirement reversal' then
               I_RETIREMENT_BOOK_SUMMARY_ID := LOCREPAIRQUALIFIEDBUCKETS(K).BOOK_SUMMARY_ID;
               CONTINUE;
            else
               null;
         end case;

         STR_TMP := TO_CHAR(LOCREPAIRQUALIFIEDBUCKETS(K).BOOK_SUMMARY_ID);
         SQL_TMP := 'cab.basis_' || STR_TMP;

         GETBASISINDICATOR('basis_' || LOCREPAIRQUALIFIEDBUCKETS(K).BOOK_SUMMARY_ID ||
                           '_indicator',
                           BASIS_INDICATOR);

         if BASIS_INDICATOR = 1 then
            -- For the Tax Repairs update method, use bases included in the financial set of books that are not assigned tax
            -- reconcile items (this will exclude items like AFUDC)
            if LOCREPAIRQUALIFIEDBUCKETS(K).RECONCILE_ITEM_ID = 0 then
               if I_ADD_QUALIFIED_SQLS is not null then
                  I_ADD_QUALIFIED_SQLS      := I_ADD_QUALIFIED_SQLS || ' + ';
                  I_ADD_QUALIFIED_STR       := I_ADD_QUALIFIED_STR || ', ';
                  BOOK_SUMMARIES_TO_REVERSE := BOOK_SUMMARIES_TO_REVERSE || ', ';
               else
                  I_ADD_QUALIFIED_SQLS      := null;
                  I_ADD_QUALIFIED_STR       := null;
                  BOOK_SUMMARIES_TO_REVERSE := null;
               end if;

               I_ADD_QUALIFIED_SQLS      := I_ADD_QUALIFIED_SQLS || SQL_TMP;
               I_ADD_QUALIFIED_STR       := I_ADD_QUALIFIED_STR || STR_TMP;
               BOOK_SUMMARIES_TO_REVERSE := BOOK_SUMMARIES_TO_REVERSE ||
                                            TO_CHAR(LOCREPAIRQUALIFIEDBUCKETS(K).BOOK_SUMMARY_ID);

            else
               -- Check to see if this should be included in the AFUDC SQLs

               case LOCREPAIRQUALIFIEDBUCKETS(K).BOOK_SUMMARY_INTEREST_TYPE
                  when 'afudc_equity' then
                     if I_AFUDC_EQUITY_SQLS is not null then
                        I_AFUDC_EQUITY_SQLS         := I_AFUDC_EQUITY_SQLS || ' + ';
                        BOOK_SUMMARIES_AFUDC_EQUITY := BOOK_SUMMARIES_AFUDC_EQUITY || ',  ';
                     else
                        I_AFUDC_EQUITY_SQLS         := null;
                        BOOK_SUMMARIES_AFUDC_EQUITY := null;
                     end if;

                     I_AFUDC_EQUITY_SQLS         := I_AFUDC_EQUITY_SQLS || SQL_TMP;
                     BOOK_SUMMARIES_AFUDC_EQUITY := BOOK_SUMMARIES_AFUDC_EQUITY ||
                                                    TO_CHAR(LOCREPAIRQUALIFIEDBUCKETS(K)
                                                            .BOOK_SUMMARY_ID);
                  when 'afudc_debt' then
                     if I_AFUDC_DEBT_SQLS is not null then
                        I_AFUDC_DEBT_SQLS         := I_AFUDC_DEBT_SQLS || ' + ';
                        BOOK_SUMMARIES_AFUDC_DEBT := BOOK_SUMMARIES_AFUDC_DEBT || ',  ';
                     else
                        I_AFUDC_DEBT_SQLS         := null;
                        BOOK_SUMMARIES_AFUDC_DEBT := null;
                     end if;

                     I_AFUDC_DEBT_SQLS         := I_AFUDC_DEBT_SQLS || SQL_TMP;
                     BOOK_SUMMARIES_AFUDC_DEBT := BOOK_SUMMARIES_AFUDC_DEBT ||
                                                  TO_CHAR(LOCREPAIRQUALIFIEDBUCKETS(K)
                                                          .BOOK_SUMMARY_ID);
                  else
                     --If it's not AFUDC, then include it in the "Other Book Basis Difference" buckets SQL
                     if I_OTHER_BOOK_BASIS_DIFFS_SQLS is not null then
                        I_OTHER_BOOK_BASIS_DIFFS_SQLS := I_OTHER_BOOK_BASIS_DIFFS_SQLS || ' + ';
                        BOOK_SUMMARIES_OTHER_BOOKONLY := BOOK_SUMMARIES_OTHER_BOOKONLY || ', ';
                     else
                        I_OTHER_BOOK_BASIS_DIFFS_SQLS := null;
                        BOOK_SUMMARIES_OTHER_BOOKONLY := null;
                     end if;

                     I_OTHER_BOOK_BASIS_DIFFS_SQLS := I_OTHER_BOOK_BASIS_DIFFS_SQLS || SQL_TMP;
                     BOOK_SUMMARIES_OTHER_BOOKONLY := BOOK_SUMMARIES_OTHER_BOOKONLY ||
                                                      TO_CHAR(LOCREPAIRQUALIFIEDBUCKETS(K)
                                                              .BOOK_SUMMARY_ID);
               end case;
            end if;

            -- For the Retirement Reversal update method, use bases included in the financial set of books that are not assigned
            -- tax reconcile items, or have tax reconcile items of type "BOOK"
            if LOCREPAIRQUALIFIEDBUCKETS(K)
             .RECONCILE_ITEM_ID = 0 or LOCREPAIRQUALIFIEDBUCKETS(K).TAX_RECONCILE_TYPE = 'BOOK' then

               if I_RETIREMENT_SQLS is not null then
                  I_RETIREMENT_SQLS := I_RETIREMENT_SQLS || ' + ';
                  I_RETIREMENT_STR  := I_RETIREMENT_STR || ', ';
               else
                  I_RETIREMENT_SQLS := null;
                  I_RETIREMENT_STR  := null;
               end if;

               if I_TOTAL_BOOK_COST_SQLS is not null then
                  I_TOTAL_BOOK_COST_SQLS := I_TOTAL_BOOK_COST_SQLS || ' + ';
               else
                  I_TOTAL_BOOK_COST_SQLS := null;
               end if;

               I_RETIREMENT_SQLS      := I_RETIREMENT_SQLS || SQL_TMP;
               I_RETIREMENT_STR       := I_RETIREMENT_STR || STR_TMP;
               I_TOTAL_BOOK_COST_SQLS := I_TOTAL_BOOK_COST_SQLS || SQL_TMP;
            end if;
         else
            -- basis_indicator = 0
            --
            -- For the Tax Repairs Reversal update method, use bases not included in the financial set of books with tax reconcile
            -- items of type "TAX" (This will pick up CPI and similar)
            --
            if LOCREPAIRQUALIFIEDBUCKETS(K).TAX_RECONCILE_TYPE = 'TAX' then
               --
               -- Determine if this guy is going to get reversed (if it's included in tax_reversal_book_summary).
               -- If it isn't, then let's capture it for reporting (reconciling back to tax basis)
               --
               ROWS_FOUND := 0;
               for I in 1 .. LOCTAXREVERSALBUCKETS.COUNT
               loop
                  if LOCTAXREVERSALBUCKETS(I)
                   .BOOK_SUMMARY_ID = LOCREPAIRQUALIFIEDBUCKETS(K).BOOK_SUMMARY_ID then
                     ROWS_FOUND := 1; -- There is atleast 1 record
                     exit;
                  end if;
               end loop;

               if ROWS_FOUND = 0 then
                  if I_OTHER_TAX_BASIS_DIFFS_SQLS is not null then
                     I_OTHER_TAX_BASIS_DIFFS_SQLS := I_OTHER_TAX_BASIS_DIFFS_SQLS || ' + ';
                  else
                     I_OTHER_TAX_BASIS_DIFFS_SQLS := null;
                  end if;
                  I_OTHER_TAX_BASIS_DIFFS_SQLS := I_OTHER_TAX_BASIS_DIFFS_SQLS || SQL_TMP;
                  -- Found a record in    tax_reversal_book_summary
               else
                  if I_TAX_ONLY_BASIS_DIFFS_SQLS is not null then
                     I_TAX_ONLY_BASIS_DIFFS_SQLS := I_TAX_ONLY_BASIS_DIFFS_SQLS || ' + ';
                  else
                     I_TAX_ONLY_BASIS_DIFFS_SQLS := null;
                  end if;
                  I_TAX_ONLY_BASIS_DIFFS_SQLS := I_TAX_ONLY_BASIS_DIFFS_SQLS || SQL_TMP;
               end if;
            end if;
         end if; -- basis_indicator = 1
      end loop;

      --
      --    Populate the instance variables
      --

      -- Create a comma-delimited string for the basis differences that store the reversal amounts
      I_REVERSAL_BASIS_DIFFS_STR := TO_CHAR(I_REPAIR_BOOK_SUMMARY_ID);

      -- The i_reversals_booked_sqls variable stores a decode that is used to pull the activity from cpr_act_basis for the tax repairs basis buckets
      I_REVERSALS_BOOKED_SQLS := ' decode(bs.book_summary_id, ' ||
                                 TO_CHAR(I_REPAIR_BOOK_SUMMARY_ID) ||
                                 ', decode (ca.ferc_activity_code, 2,0, cab.basis_' ||
                                 TO_CHAR(I_REPAIR_BOOK_SUMMARY_ID) || '), ';

      -- The i_activity_cost_sqls variable stores the decode that is used to get the activity that should be reversed for each Tax Repairs
      -- book summary in cpr_act_basis
      I_ACTIVITY_COST_SQLS := ' decode(bs.book_summary_id, ' || TO_CHAR(I_REPAIR_BOOK_SUMMARY_ID) ||
                              ' decode (ca.ferc_activity_code, 2,0, ' || I_ADD_QUALIFIED_SQLS || ') ';

      -- Create the view that will be used in CWIP processing to determine which charge types to reverse
      I_CHARGE_TYPE_VIEW_SQLS := replace(replace(CHARGE_TYPE_VIEW_SQLS_BASE,
                                                 '<REVERSAL_BOOKSUMMARY>',
                                                 TO_CHAR(I_REPAIR_BOOK_SUMMARY_ID)),
                                         '<BOOKSUMMARY_TO_REVERSE>',
                                         BOOK_SUMMARIES_TO_REVERSE || ', ' ||
                                         TO_CHAR(I_REPAIR_BOOK_SUMMARY_ID));

      -- Add the retirement book summary to our SQL if it's defined
      if I_RETIREMENT_BOOK_SUMMARY_ID > 0 then
         I_REVERSAL_BASIS_DIFFS_STR := I_REVERSAL_BASIS_DIFFS_STR || ', ' ||
                                       TO_CHAR(I_RETIREMENT_BOOK_SUMMARY_ID);
         I_REVERSALS_BOOKED_SQLS    := I_REVERSALS_BOOKED_SQLS ||
                                       TO_CHAR(I_RETIREMENT_BOOK_SUMMARY_ID) ||
                                       ', case when ca.ferc_activity_code = 2 and ca.tax_orig_month_number is not null then cab.basis_' ||
                                       TO_CHAR(I_RETIREMENT_BOOK_SUMMARY_ID) || ' else 0 end , ';

         I_ACTIVITY_COST_SQLS := I_ACTIVITY_COST_SQLS || ', ' ||
                                 TO_CHAR(I_RETIREMENT_BOOK_SUMMARY_ID) ||
                                 ', decode (ca.ferc_activity_code, 2, ' || I_RETIREMENT_SQLS ||
                                 ',0)';
      end if;

      NEXT_REVERSAL_BUCKET := true;
      for K in 1 .. LOCTAXREVERSALBUCKETS.COUNT
      loop
         if NEXT_REVERSAL_BUCKET then
            BOOK_SUMMARIES_TO_REVERSE  := null;
            I_REVERSAL_BASIS_DIFFS_STR := I_REVERSAL_BASIS_DIFFS_STR || ', ' ||
                                          TO_CHAR(LOCTAXREVERSALBUCKETS(K).REVERSAL_BOOK_SUMMARY_ID);

            if I_REVERSAL_BASIS_DIFFS_SQLS is null then
               I_REVERSAL_BASIS_DIFFS_SQLS := 'cab.basis_' ||
                                              TO_CHAR(LOCTAXREVERSALBUCKETS(K)
                                                      .REVERSAL_BOOK_SUMMARY_ID);
            else
               I_REVERSAL_BASIS_DIFFS_SQLS := I_REVERSAL_BASIS_DIFFS_SQLS || ' cab.basis_' ||
                                              TO_CHAR(LOCTAXREVERSALBUCKETS(K)
                                                      .REVERSAL_BOOK_SUMMARY_ID);
            end if;

            I_REVERSALS_BOOKED_SQLS := I_REVERSALS_BOOKED_SQLS ||
                                       TO_CHAR(LOCTAXREVERSALBUCKETS(K).REVERSAL_BOOK_SUMMARY_ID) ||
                                       ', decode  (ca.ferc_activity_code, 2, 0, cab.basis_' ||
                                       TO_CHAR(LOCTAXREVERSALBUCKETS(K).REVERSAL_BOOK_SUMMARY_ID) ||
                                       '), ';

            I_ACTIVITY_COST_SQLS := I_ACTIVITY_COST_SQLS ||
                                    TO_CHAR(LOCTAXREVERSALBUCKETS(K).REVERSAL_BOOK_SUMMARY_ID) ||
                                    ', decode (ca.ferc_activity_code, 2, 0, cab.basis_' ||
                                    TO_CHAR(LOCTAXREVERSALBUCKETS(K).BOOK_SUMMARY_ID) || ') ';

            BOOK_SUMMARIES_TO_REVERSE := TO_CHAR(LOCTAXREVERSALBUCKETS(K).BOOK_SUMMARY_ID);
         else
            I_ACTIVITY_COST_SQLS      := I_ACTIVITY_COST_SQLS || ' cab.basis_' ||
                                         TO_CHAR(LOCTAXREVERSALBUCKETS(K).BOOK_SUMMARY_ID);
            BOOK_SUMMARIES_TO_REVERSE := BOOK_SUMMARIES_TO_REVERSE || ', ' ||
                                         TO_CHAR(LOCTAXREVERSALBUCKETS(K).BOOK_SUMMARY_ID);
         end if;

         --
         -- Determine if the next row is a new reversal_book_summary_id.  If it's not, then add this reversal bucket the charge_type view
         --
         NEXT_REVERSAL_BUCKET := true;
         if K < LOCTAXREVERSALBUCKETS.COUNT then
            if LOCTAXREVERSALBUCKETS(K + 1)
             .REVERSAL_BOOK_SUMMARY_ID = LOCTAXREVERSALBUCKETS(K).REVERSAL_BOOK_SUMMARY_ID then
               NEXT_REVERSAL_BUCKET := false;
            end if;
         else
            --i_charge_type_view_sqls := REPLACE(REPLACE(charge_type_view_sqls_base,'<REVERSAL_BOOKSUMMARY>',TO_CHAR(i_repair_book_summary_id)),
            --                           '<BOOKSUMMARY_TO_REVERSE>', book_summaries_to_reverse||', '||TO_CHAR(i_repair_book_summary_id));

            I_CHARGE_TYPE_VIEW_SQLS := I_CHARGE_TYPE_VIEW_SQLS || ' union all ' ||
                                       replace(replace(CHARGE_TYPE_VIEW_SQLS_BASE,
                                                       '<REVERSAL_BOOKSUMMARY>',
                                                       TO_CHAR(LOCTAXREVERSALBUCKETS(K)
                                                               .REVERSAL_BOOK_SUMMARY_ID)),
                                               '<BOOKSUMMARY_TO_REVERSE>',
                                               BOOK_SUMMARIES_TO_REVERSE || ', ' ||
                                               TO_CHAR(LOCTAXREVERSALBUCKETS(K)
                                                       .REVERSAL_BOOK_SUMMARY_ID));
         end if;
      end loop;

      I_REVERSALS_BOOKED_SQLS := I_REVERSALS_BOOKED_SQLS || '0)';
      I_ACTIVITY_COST_SQLS    := I_ACTIVITY_COST_SQLS || ', 0)';

      --
      -- Get i_book_only_sqls
      --
      if I_AFUDC_EQUITY_SQLS is not null then
         I_BOOK_ONLY_SQLS            := I_AFUDC_EQUITY_SQLS;
         BOOK_SUMMARIES_ALL_BOOKONLY := BOOK_SUMMARIES_AFUDC_EQUITY;
      end if;

      if I_AFUDC_DEBT_SQLS is not null then
         if I_BOOK_ONLY_SQLS is null then
            I_BOOK_ONLY_SQLS            := I_AFUDC_DEBT_SQLS;
            BOOK_SUMMARIES_ALL_BOOKONLY := BOOK_SUMMARIES_AFUDC_DEBT;
         else
            I_BOOK_ONLY_SQLS            := I_BOOK_ONLY_SQLS || ' + ' || I_AFUDC_DEBT_SQLS;
            BOOK_SUMMARIES_ALL_BOOKONLY := BOOK_SUMMARIES_ALL_BOOKONLY || ', ' || BOOK_SUMMARIES_AFUDC_DEBT;
         end if;
      end if;

      if I_OTHER_BOOK_BASIS_DIFFS_SQLS is not null then
         if I_BOOK_ONLY_SQLS is null then
            I_BOOK_ONLY_SQLS            := I_OTHER_BOOK_BASIS_DIFFS_SQLS;
            BOOK_SUMMARIES_ALL_BOOKONLY := BOOK_SUMMARIES_OTHER_BOOKONLY;
         else
            I_BOOK_ONLY_SQLS            := I_BOOK_ONLY_SQLS || ' + ' || I_OTHER_BOOK_BASIS_DIFFS_SQLS;
            BOOK_SUMMARIES_ALL_BOOKONLY := BOOK_SUMMARIES_ALL_BOOKONLY || ', ' || BOOK_SUMMARIES_OTHER_BOOKONLY;
         end if;
      end if;

      if BOOK_SUMMARIES_ALL_BOOKONLY is not null then
         I_CHARGE_TYPE_VIEW_SQLS := I_CHARGE_TYPE_VIEW_SQLS || ' union all ' ||
                                    replace(CHARGE_TYPE_VIEW_SQLS_BOOKONLY,
                                            '<BOOKSUMMARY_BOOKONLY>',
                                            BOOK_SUMMARIES_ALL_BOOKONLY);
      end if;

      --
      -- Attach -5=-5, so we can attach a where clause if needed
      --
      I_CHARGE_TYPE_VIEW_SQLS := ' select * from ( ' || I_CHARGE_TYPE_VIEW_SQLS ||
                                 ' ) where -5=-5 ';

      /*PrintOut( 'i_add_qualified_sqls = ' || i_add_qualified_sqls );
      PrintOut( 'i_retirement_sqls = ' || i_retirement_sqls );
      PrintOut( 'i_add_qualified_str = ' || i_add_qualified_str );
      PrintOut( 'i_retirement_str = ' || i_retirement_str );
      PrintOut( 'i_afudc_debt_sqls = ' || i_afudc_debt_sqls );
      PrintOut( 'i_afudc_equity_sqls = ' || i_afudc_equity_sqls );
      PrintOut( 'i_other_book_basis_diffs_sqls = ' || i_other_book_basis_diffs_sqls );
      PrintOut( 'i_other_tax_basis_diffs_sqls = ' || i_other_tax_basis_diffs_sqls );
      PrintOut( 'i_total_book_cost_sqls = ' || i_total_book_cost_sqls );
      PrintOut( 'i_reversal_basis_diffs_sqls = ' || i_reversal_basis_diffs_sqls );
      PrintOut( 'i_reversal_basis_diffs_str = ' || i_reversal_basis_diffs_str );
      PrintOut( 'i_reversals_booked_sqls = ' || i_reversals_booked_sqls );
      PrintOut( 'i_activity_cost_sqls = ' || i_activity_cost_sqls );
      PrintOut( 'i_charge_type_view_sqls = ' || i_charge_type_view_sqls );
      PrintOut( 'i_book_only_sqls = ' || i_book_only_sqls );
      PrintOut( 'i_tax_only_basis_diffs_sqls = ' || i_tax_only_basis_diffs_sqls );
      PrintOut( 'i_retirement_book_summary_id = ' || to_char(i_retirement_book_summary_id) );
      PrintOut( 'i_repair_book_summary_id = ' || to_char(i_repair_book_summary_id) );*/

   end P_POPULATE_BASIS_BUCKETS;

   procedure P_CREATE_REPAIR_BOOK_SUMMARY is
      RBS_SQLS varchar2(4000);

   begin
      RBS_SQLS :=             ' CREATE OR REPLACE VIEW repair_book_summary AS ';
      RBS_SQLS := RBS_SQLS || ' SELECT  rs.repair_schema_id        repair_schema_id, ';
      RBS_SQLS := RBS_SQLS || '         rbs.book_summary_id        book_summary_id, ';
      RBS_SQLS := RBS_SQLS || '         rbs.update_method          update_method, ';
      RBS_SQLS := RBS_SQLS || '         NULL                       time_stamp, ';
      RBS_SQLS := RBS_SQLS || '         NULL                       user_ud, ';
      RBS_SQLS := RBS_SQLS || '         rbs.book_summary_id_output book_summary_id_output, ';
      RBS_SQLS := RBS_SQLS || '         ce_output.cost_element_id  cost_element_id_output ';
      RBS_SQLS := RBS_SQLS || ' FROM ';
      RBS_SQLS := RBS_SQLS || ' ( ';
      RBS_SQLS := RBS_SQLS || '   SELECT  bs.book_summary_id                 book_summary_id, ';
      RBS_SQLS := RBS_SQLS || '   ''add qualified''                          update_method, ';
      RBS_SQLS := RBS_SQLS || '   bs_output.book_summary_id                  book_summary_id_output ';
      RBS_SQLS := RBS_SQLS || ' FROM book_summary       bs, ';
      RBS_SQLS := RBS_SQLS || '      book_summary       bs_output ';
      RBS_SQLS := RBS_SQLS || ' WHERE bs.book_summary_id in (' || I_ADD_QUALIFIED_STR || ') ';
      RBS_SQLS := RBS_SQLS || '   and lower(bs_output.summary_name) = ''tax repairs'' ';
      RBS_SQLS := RBS_SQLS || ' UNION ';
      RBS_SQLS := RBS_SQLS || ' SELECT  bs.book_summary_id                     book_summary_id, ';
      RBS_SQLS := RBS_SQLS || '         ''retirement reversal''                update_method, ';
      RBS_SQLS := RBS_SQLS || '         bs_output.book_summary_id              book_summary_id_output ';
      RBS_SQLS := RBS_SQLS || '   FROM book_summary       bs, ';
      RBS_SQLS := RBS_SQLS || '        book_summary       bs_output ';
      RBS_SQLS := RBS_SQLS || '  WHERE bs.book_summary_id in (' || I_RETIREMENT_STR || ') ';
      RBS_SQLS := RBS_SQLS || '    and lower(bs_output.summary_name) = ''retirement reversal'' ';
      RBS_SQLS := RBS_SQLS || '  UNION ';
      RBS_SQLS := RBS_SQLS || '  SELECT book_summary_id, ';
      RBS_SQLS := RBS_SQLS || '         ''reverse'', ';
      RBS_SQLS := RBS_SQLS || '         reversal_book_summary_id ';
      RBS_SQLS := RBS_SQLS || '  FROM   tax_reversal_book_summary ';
      RBS_SQLS := RBS_SQLS || '  WHERE  tax_reversal_type = ''Repairs'' ';
      RBS_SQLS := RBS_SQLS || ' ) rbs, ';
      RBS_SQLS := RBS_SQLS || ' repair_schema      rs, ';
      RBS_SQLS := RBS_SQLS || ' charge_type        ct_output, ';
      RBS_SQLS := RBS_SQLS || ' cost_element       ce_output ';
      RBS_SQLS := RBS_SQLS || ' WHERE  ct_output.book_summary_id = rbs.book_summary_id_output ';
      RBS_SQLS := RBS_SQLS || ' and ct_output.charge_type_id = ce_output.charge_type_id ';

      --PrintOut( rbs_sqls );
      execute immediate RBS_SQLS;

   end P_CREATE_REPAIR_BOOK_SUMMARY;

   procedure P_GET_BASIS_BUCKETS(A_REPAIR_BOOK_SUMMARY_ID     out number,
                                 A_RETIREMENT_BOOK_SUMMARY_ID out number,

                                 A_ADD_QUALIFIED_SQLS out varchar2,
                                 A_RETIREMENT_SQLS    out varchar2,

                                 A_AFUDC_EQUITY_SQLS           out varchar2,
                                 A_AFUDC_DEBT_SQLS             out varchar2,
                                 A_OTHER_BOOK_BASIS_DIFFS_SQLS out varchar2,
                                 A_OTHER_TAX_BASIS_DIFFS_SQLS  out varchar2,
                                 A_TOTAL_BOOK_COST_SQLS        out varchar2,
                                 A_REVERSAL_BASIS_DIFFS_SQLS   out varchar2,

                                 A_ADD_QUALIFIED_STR        out varchar2,
                                 A_RETIREMENT_STR           out varchar2,
                                 A_REVERSAL_BASIS_DIFFS_STR out varchar2,

                                 A_REVERSALS_BOOKED_SQLS     out varchar2,
                                 A_ACTIVITY_COST_SQLS        out varchar2,
                                 A_CHARGE_TYPE_VIEW_SQLS     out varchar2,
                                 A_BOOK_ONLY_SQLS            out varchar2,
                                 A_TAX_ONLY_BASIS_DIFFS_SQLS out varchar2) is
   begin
      A_REPAIR_BOOK_SUMMARY_ID     := I_REPAIR_BOOK_SUMMARY_ID;
      A_RETIREMENT_BOOK_SUMMARY_ID := I_RETIREMENT_BOOK_SUMMARY_ID;

      A_ADD_QUALIFIED_SQLS := I_ADD_QUALIFIED_SQLS;
      A_RETIREMENT_SQLS    := I_RETIREMENT_SQLS;

      A_AFUDC_EQUITY_SQLS           := I_AFUDC_EQUITY_SQLS;
      A_AFUDC_DEBT_SQLS             := I_AFUDC_DEBT_SQLS;
      A_OTHER_BOOK_BASIS_DIFFS_SQLS := I_OTHER_BOOK_BASIS_DIFFS_SQLS;
      A_OTHER_TAX_BASIS_DIFFS_SQLS  := I_OTHER_TAX_BASIS_DIFFS_SQLS;
      A_TOTAL_BOOK_COST_SQLS        := I_TOTAL_BOOK_COST_SQLS;
      A_REVERSAL_BASIS_DIFFS_SQLS   := I_REVERSAL_BASIS_DIFFS_SQLS;

      A_ADD_QUALIFIED_STR        := I_ADD_QUALIFIED_STR;
      A_RETIREMENT_STR           := I_RETIREMENT_STR;
      A_REVERSAL_BASIS_DIFFS_STR := I_REVERSAL_BASIS_DIFFS_STR;

      A_REVERSALS_BOOKED_SQLS     := I_REVERSALS_BOOKED_SQLS;
      A_ACTIVITY_COST_SQLS        := I_ACTIVITY_COST_SQLS;
      A_CHARGE_TYPE_VIEW_SQLS     := I_CHARGE_TYPE_VIEW_SQLS;
      A_BOOK_ONLY_SQLS            := I_BOOK_ONLY_SQLS;
      A_TAX_ONLY_BASIS_DIFFS_SQLS := I_TAX_ONLY_BASIS_DIFFS_SQLS;
   end P_GET_BASIS_BUCKETS;

end RPR_BASIS_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (485, 0, 10, 4, 1, 0, 30261, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030261_taxrpr_RPR_BASIS_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;