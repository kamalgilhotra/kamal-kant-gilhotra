/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033221_lease_tax_location.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/08/2013 Kyle Peterson
||============================================================================
*/

alter table LS_ASSET add TAX_ASSET_LOCATION_ID number(22,0);

comment on column LS_ASSET."TAX_ASSET_LOCATION_ID" is 'A seperate location that is used as the tax jurisdiction when calculating local taxes.';

update LS_ASSET set TAX_ASSET_LOCATION_ID = ASSET_LOCATION_ID;

alter table LS_IMPORT_ASSET
   add (TAX_ASSET_LOCATION_XLATE varchar2(254),
        TAX_ASSET_LOCATION_ID    number(22,0));

comment on column LS_IMPORT_ASSET."TAX_ASSET_LOCATION_XLATE" is 'Translation field for determining tax asset location.';
comment on column LS_IMPORT_ASSET."TAX_ASSET_LOCATION_ID" is 'The asset tax location.';

alter table LS_IMPORT_ASSET_ARCHIVE
  add (TAX_ASSET_LOCATION_XLATE varchar2(254),
       TAX_ASSET_LOCATION_ID    number(22,0));

comment on column LS_IMPORT_ASSET_ARCHIVE."TAX_ASSET_LOCATION_XLATE" is 'Translation field for determining tax asset location.';
comment on column LS_IMPORT_ASSET_ARCHIVE."TAX_ASSET_LOCATION_ID" is 'The asset tax location.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'tax_asset_location_id',
           'Tax Asset Location ID',
           'tax_asset_location_xlate',
           0,
           1,
           'number(22,0)',
           'asset_location',
           1,
           'asset_location_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: Leased Asset%');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'tax_asset_location_id', 88
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: Leased Asset%');

alter table LS_IMPORT_ILR_ARCHIVE
   add (C_BUCKET_1  number(22,2),
        C_BUCKET_2  number(22,2),
        C_BUCKET_3  number(22,2),
        C_BUCKET_4  number(22,2),
        C_BUCKET_5  number(22,2),
        C_BUCKET_6  number(22,2),
        C_BUCKET_7  number(22,2),
        C_BUCKET_8  number(22,2),
        C_BUCKET_9  number(22,2),
        C_BUCKET_10 number(22,2),
        E_BUCKET_1  number(22,2),
        E_BUCKET_2  number(22,2),
        E_BUCKET_3  number(22,2),
        E_BUCKET_4  number(22,2),
        E_BUCKET_5  number(22,2),
        E_BUCKET_6  number(22,2),
        E_BUCKET_7  number(22,2),
        E_BUCKET_8  number(22,2),
        E_BUCKET_9  number(22,2),
        E_BUCKET_10 number(22,2));

comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_1" is 'Contingent Bucket 1.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_2" is 'Contingent Bucket 2.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_3" is 'Contingent Bucket 3.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_4" is 'Contingent Bucket 4.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_5" is 'Contingent Bucket 5.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_6" is 'Contingent Bucket 6.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_7" is 'Contingent Bucket 7.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_8" is 'Contingent Bucket 8.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_9" is 'Contingent Bucket 9.';
comment on column LS_IMPORT_ILR_ARCHIVE."C_BUCKET_10" is 'Contingent Bucket 10.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_1" is 'Executory Bucket 1.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_2" is 'Executory Bucket 2.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_3" is 'Executory Bucket 3.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_4" is 'Executory Bucket 4.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_5" is 'Executory Bucket 5.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_6" is 'Executory Bucket 6.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_7" is 'Executory Bucket 7.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_8" is 'Executory Bucket 8.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_9" is 'Executory Bucket 9.';
comment on column LS_IMPORT_ILR_ARCHIVE."E_BUCKET_10" is 'Executory Bucket 10.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (687, 0, 10, 4, 1, 1, 33221, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033221_lease_tax_location.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
