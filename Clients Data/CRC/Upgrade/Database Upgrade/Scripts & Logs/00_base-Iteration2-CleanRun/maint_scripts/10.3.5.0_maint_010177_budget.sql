/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010177_budget.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/06/2012 Chris Mardis   Point Release
|| 10.4.2.0   04/05/2014 Lee Quinn      Changed to not insert if report already exists
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'PWRPLANT',
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          'FP Estimates - 13 Mo by Dept',
          'FP Estimates - 13 Mo by Estimate Charge Type - Selected Department and Version',
          null,
          'dw_fpest_cur_tmp_13_ect_rep2_dept',
          'already has report time',
          ' ',
          null,
          'FP - 008 Dept',
          'dw_budget_version_report_list,dw_department_report_list',
          null,
          null,
          6,
          2,
          1,
          1,
          1,
          3,
          null,
          null,
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          null,
          null,
          null
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'PWRPLANT',
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          'FP Summary by Dept',
          'FP Summary by Estimate Charge Type - Selected Department and Version',
          'Funding Project',
          'dw_funding_summary_report2_t_dept',
          'no filter',
          'Work',
          null,
          'FP - 007 Dept',
          'dw_department_report_list',
          null,
          null,
          6,
          35,
          5,
          1,
          1,
          1,
          null,
          null,
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          null,
          null,
          null
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'PWRPLANT',
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          'FP Estimates - 13 Mo by Div',
          'FP Estimates - 13 Mo by Estimate Charge Type - Selected Division and Version',
          null,
          'dw_fpest_cur_tmp_13_ect_rep2_div',
          'already has report time',
          ' ',
          null,
          'FP - 008 Div',
          'dw_budget_version_report_list,dw_division_report_list_no_company',
          null,
          null,
          6,
          2,
          1,
          1,
          1,
          3,
          null,
          null,
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          null,
          null,
          null
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'PWRPLANT',
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          'FP Summary by Div',
          'FP Summary by Estimate Charge Type - Selected Division and Version',
          'Funding Project',
          'dw_funding_summary_report2_t_div',
          'no filter',
          'Work',
          null,
          'FP - 007 Div',
          'dw_division_report_list_no_company',
          null,
          null,
          6,
          35,
          5,
          1,
          1,
          1,
          null,
          null,
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          null,
          null,
          null
     from PP_REPORTS;

/* Altered 04052014 */
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select (select max(REPORT_ID) + 1 from PP_REPORTS),
          'PWRPLANT',
          TO_DATE('21-05-2010 19:43:03', 'DD-MM-YYYY HH24:MI:SS'),
          'Monthly Budget Listing - BPC - ECT',
          'Monthly Budget Listing - Budget Plant Class - ECT',
          'Budget',
          'dw_budget_report3_bdg_plt_class',
          'PWRPLANT',
          'Main - Budget',
          'monthly',
          'Budget - 002c',
          'dw_fp_est_chg_type_select',
          null,
          null,
          5,
          4,
          3,
          1,
          1,
          1,
          null,
          null,
          TO_DATE('24-08-2009 09:54:01', 'DD-MM-YYYY HH24:MI:SS'),
          null,
          null,
          0
     from DUAL
    where not exists (select 'X'
             from PP_REPORTS
            where DATAWINDOW = 'dw_budget_report3_bdg_plt_class'
              and PP_REPORT_SUBSYSTEM_ID = 5
              and REPORT_TYPE_ID = 4);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (176, 0, 10, 3, 5, 0, 10177, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010177_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
