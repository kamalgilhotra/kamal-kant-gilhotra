/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_041649_lease_prop_tax_fields_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 01/14/2015 	B.Beck				Adding property tax fields to leased assets
||==========================================================================================
*/

alter table ls_asset
add
(
	property_tax_date date,
	property_tax_amount number(22,2)
);

comment on column ls_asset.property_tax_date is 'The property tax start date';
comment on column ls_asset.property_tax_amount is 'The basis amount for property tax.  This field is only used for client specific property tax interfaces if the property tax basis is different than the capital book basis';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2169, 0, 10, 4, 3, 2, 041649, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041649_lease_prop_tax_fields_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;