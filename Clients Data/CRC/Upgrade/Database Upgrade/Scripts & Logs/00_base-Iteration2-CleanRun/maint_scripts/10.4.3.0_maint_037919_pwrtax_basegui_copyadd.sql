/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_037919_pwrtax_basegui_copyadd.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 10.4.3.0 06/30/2014 Andrew Scott        Script needed for the copy add window
||                                         in the new powertax base gui.
||============================================================================
*/

-- Create table
create global temporary table TAX_TEMP_COPY_ADD_INIT
(
 TAX_RECORD_ID         number(22,0) not null,
 TAX_YEAR              number(22,2) not null,
 TAX_BOOK_ID           number(22,0) not null,
 COMPANY_ID            number(22,0),
 TAX_CLASS_ID          number(22,0),
 VERSION_ID            number(22,0) not null,
 VINTAGE_ID            number(22,0) not null,
 IN_SERVICE_MONTH      date,
 TAX_LOCATION_ID       number(22,0),
 TAX_LAYER_ID          number(22,0),
 ASSET_ID              number(22,0) default 0 not null,
 NEW_TAX_RECORD_ID     number(22,0),
 NEW_COMPANY_ID        number(22,0),
 NEW_TAX_CLASS_ID      number(22,0),
 NEW_VERSION_ID        number(22,0),
 NEW_VINTAGE_ID        number(22,0),
 NEW_IN_SERVICE_MONTH  date,
 NEW_TAX_LOCATION_ID   number(22,0),
 NEW_TAX_LAYER_ID      number(22,0),
 NEW_ASSET_ID          number(22) default 0
)
on commit preserve rows;

comment on table TAX_TEMP_COPY_ADD_INIT
is '(T)(C) [09] The Tax Temp Copy Add Init temporary table holds the initial selected tax asset records and attributes used when doing a copy add through the asset selection workspace.';

comment on column TAX_TEMP_COPY_ADD_INIT.TAX_RECORD_ID is 'The tax record id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.TAX_YEAR is 'The tax year of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.TAX_BOOK_ID is 'The tax book id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.COMPANY_ID is 'The company id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.TAX_CLASS_ID is 'The tax class id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.VERSION_ID is 'The version id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.VINTAGE_ID is 'The vintage id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.IN_SERVICE_MONTH is 'The in service month of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.TAX_LOCATION_ID is 'The tax location id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.TAX_LAYER_ID is 'The tax layer id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.ASSET_ID is 'The asset id of the initially selected tax asset.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_TAX_RECORD_ID is 'The new tax record id populated at the end of the copy add process.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_COMPANY_ID is 'The new company id to use for new tax assets. Only populated if changed on the window.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_TAX_CLASS_ID is 'The new tax class id to use for new tax assets. Only populated if changed on the window.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_VERSION_ID is 'The new version id to use for new tax assets. Only populated if changed on the window.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_VINTAGE_ID is 'The new vintage id to use for new tax assets. Only populated if changed on the window.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_IN_SERVICE_MONTH is 'The new in service month to use for new tax assets. Only populated if changed on the window.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_TAX_LOCATION_ID is 'The new tax location id to use for new tax assets. Only populated if changed on the window.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_TAX_LAYER_ID is 'The new tax layer id to use for new tax assets. Only populated if changed on the window.';
comment on column TAX_TEMP_COPY_ADD_INIT.NEW_ASSET_ID is 'The new asset id to use for new tax assets. Only populated if changed on the window.';


-- Create table
create global temporary table TAX_TEMP_COPY_ADD_TRID
(
 TAX_RECORD_ID         number(22,0) not null,
 NEW_TAX_RECORD_ID     number(22,0) not null,
 COMPANY_ID            number(22,0),
 TAX_CLASS_ID          number(22,0),
 VERSION_ID            number(22,0) not null,
 VINTAGE_ID            number(22,0) not null,
 IN_SERVICE_MONTH      date,
 TAX_LOCATION_ID       number(22,0),
 TAX_LAYER_ID          number(22,0),
 ASSET_ID              number(22,0) default 0 not null
) on commit preserve rows;

comment on table TAX_TEMP_COPY_ADD_TRID
is '(T)(C) [09] The Tax Temp Copy Add Trid temporary table is used during the copy add processing to assign new tax record ids to the new records resulting from the copy add process.';

comment on column TAX_TEMP_COPY_ADD_TRID.TAX_RECORD_ID is 'The originally selected tax assets'' tax record ids';
comment on column TAX_TEMP_COPY_ADD_TRID.NEW_TAX_RECORD_ID is 'The new tax record id populated at the end of the copy add process.';
comment on column TAX_TEMP_COPY_ADD_TRID.COMPANY_ID is 'The company id to be used for the creation of the new tax assets.';
comment on column TAX_TEMP_COPY_ADD_TRID.TAX_CLASS_ID is 'The tax class id to be used for the creation of the new tax assets.';
comment on column TAX_TEMP_COPY_ADD_TRID.VERSION_ID is 'The version id to be used for the creation of the new tax assets.';
comment on column TAX_TEMP_COPY_ADD_TRID.VINTAGE_ID is 'The vintage id to be used for the creation of the new tax assets.';
comment on column TAX_TEMP_COPY_ADD_TRID.IN_SERVICE_MONTH is 'The in service month to be used for the creation of the new tax assets.';
comment on column TAX_TEMP_COPY_ADD_TRID.TAX_LOCATION_ID is 'The tax location id to be used for the creation of the new tax assets.';
comment on column TAX_TEMP_COPY_ADD_TRID.TAX_LAYER_ID is 'The tax layer id to be used for the creation of the new tax assets.';
comment on column TAX_TEMP_COPY_ADD_TRID.ASSET_ID is 'The asset id to be used for the creation of the new tax assets.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1230, 0, 10, 4, 3, 0, 37919, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037919_pwrtax_basegui_copyadd.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
