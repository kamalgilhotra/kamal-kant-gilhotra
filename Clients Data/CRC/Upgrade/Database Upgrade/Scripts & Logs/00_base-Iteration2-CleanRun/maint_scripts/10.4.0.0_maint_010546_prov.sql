/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010546_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   07/03/2012 Blake Andrews  Point Release
||============================================================================
*/

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'end_dit_balance'
 where REP_CONS_TYPE_ID = 15
   and ROW_TYPE = 'D';

update TAX_ACCRUAL_REP_CONS_ROWS
   set VALUE_TYPE = 'COL_VALUE_NUM', ROW_VALUE = 'compute_45'
 where REP_CONS_TYPE_ID = 15
   and ROW_TYPE = 'F'
   and GROUP_ID = 99;

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'm_balance_end'
 where REP_CONS_TYPE_ID = 16
   and ROW_TYPE = 'D';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'm_balance_end_cons'
 where REP_CONS_TYPE_ID = 16
   and ROW_TYPE = 'F'
   and GROUP_ID = 99;

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'dit_balance_all', VALUE_TYPE = 'COL_VALUE_NUM'
 where REP_CONS_TYPE_ID = 13
   and ROW_TYPE = 'F'
   and GROUP_ID = 99;

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'dit_balance_all', VALUE_TYPE = 'COL_VALUE_NUM'
 where REP_CONS_TYPE_ID = 11
   and ROW_TYPE = 'F'
   and GROUP_ID = 99;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (253, 0, 10, 4, 0, 0, 10546, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_010546_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;