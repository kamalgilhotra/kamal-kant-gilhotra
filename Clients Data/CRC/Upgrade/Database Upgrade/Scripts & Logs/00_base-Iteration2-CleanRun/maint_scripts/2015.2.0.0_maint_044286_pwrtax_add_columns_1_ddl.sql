/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044286_pwrtax_add_columns_1_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/02/2015 Daniel Motter  Creation
||============================================================================
*/

alter table tax_reconcile_item add (
    k1_export_gen_include   number(22,0)
);

comment on column tax_reconcile_item.k1_export_gen_include is 'Whether or not to include in the K1 Export Generation. 1 = Yes, 0 = No.';

alter table tax_k1_export_run_results add (
    book_balance            number(22,2)    null, 
    tax_balance             number(22,2)    null, 
    remaining_life          number(22,8)    null, 
    accum_reserve           number(22,2)    null, 
    sl_reserve              number(22,2)    null, 
    depreciable_base        number(22,2)    null, 
    fixed_depreciable_base  number(22,2)    null, 
    actual_salvage          number(22,2)    null, 
    estimated_salvage       number(22,2)    null, 
    accum_salvage           number(22,2)    null, 
    additions               number(22,2)    null, 
    transfers               number(22,2)    null, 
    adjustments             number(22,2)    null, 
    retirements             number(22,2)    null, 
    extraordinary_retires   number(22,2)    null, 
    accum_ordinary_retires  number(22,2)    null, 
    depreciation            number(22,2)    null, 
    cost_of_removal         number(22,2)    null, 
    cor_expense             number(22,2)    null, 
    gain_loss               number(22,2)    null, 
    capital_gain_loss       number(22,2)    null, 
    est_salvage_pct         number(22,8)    null, 
    book_balance_end        number(22,2)    null, 
    accum_salvage_end       number(22,2)    null, 
    accum_ordin_retires_end number(22,2)    null, 
    sl_reserve_end          number(22,2)    null, 
    retire_invol_conv       number(22,2)    null, 
    salvage_invol_conv      number(22,2)    null, 
    salvage_extraord        number(22,2)    null, 
    calc_depreciation       number(22,2)    null, 
    over_adj_depreciation   number(22,2)    null, 
    retire_res_impact       number(22,2)    null, 
    transfer_res_impact     number(22,2)    null, 
    salvage_res_impact      number(22,2)    null, 
    cor_res_impact          number(22,2)    null, 
    adjusted_retire_basis   number(22,2)    null, 
    reserve_at_switch       number(22,2)    null, 
    capitalized_depr        number(22,8)    null, 
    reserve_at_switch_end   number(22,2)    null, 
    number_months_beg       number(22,2)    null, 
    number_months_end       number(22,2)    null, 
    ex_retire_res_impact    number(22,2)    null, 
    ex_gain_loss            number(22,2)    null, 
    quantity_end            number(22,2)    null, 
    estimated_salvage_end   number(22,2)    null, 
    job_creation_amount     number(22,2)    null, 
    book_balance_adjust     number(22,2)    null, 
    accum_reserve_adjust    number(22,2)    null, 
    depreciable_base_adjust number(22,2)    null, 
    depreciation_adjust     number(22,2)    null, 
    gain_loss_adjust        number(22,2)    null, 
    cap_gain_loss_adjust    number(22,2)    null,
    gross_retirements       number(22,2)    null,
    gross_accum_res_retire  number(22,2)    null,
    basis_change_diff       number(22,2)    null,
    gross_additions         number(22,2)    null
);

comment on column tax_k1_export_run_results.book_balance is 'The beginning book basis balance of this tax asset record in dollars.';
comment on column tax_k1_export_run_results.tax_balance is 'The beginning tax basis balance of this tax asset record in dollars, not reduced for ordinary (ADR) retirements.';
comment on column tax_k1_export_run_results.remaining_life is 'If this tax asset record is to utilize a remaining life technique, this data item will hold the calculated remaining life.';
comment on column tax_k1_export_run_results.accum_reserve is 'The beginning accumulated tax depreciation reserve balance of this tax asset record.';
comment on column tax_k1_export_run_results.sl_reserve is 'For "remaining life plan" rates this is the beginning tax basis minus the beginning straight line reserve, i.e., the un-recovered straight line reserve in dollars.';
comment on column tax_k1_export_run_results.depreciable_base is 'The calculated depreciable base to which the tax depreciation rates are applied in order to calculate tax depreciation.';
comment on column tax_k1_export_run_results.fixed_depreciable_base is 'The locked-in depreciable base to which the tax depreciation rates are applied in order to calculate tax depreciation, which ignores retirements and other adjustments.';
comment on column tax_k1_export_run_results.actual_salvage is 'Current year actual salvage proceeds associated with ordinary (normal/retirements) in dollars.';
comment on column tax_k1_export_run_results.estimated_salvage is '(Currently not used)';
comment on column tax_k1_export_run_results.accum_salvage is 'Accumulated salvage proceeds included in the beginning depreciation reserve in dollars.';
comment on column tax_k1_export_run_results.additions is 'Current year tax basis additions in dollars.';
comment on column tax_k1_export_run_results.transfers is 'Current year net tax basis transfers in dollars.';
comment on column tax_k1_export_run_results.adjustments is 'Current year net tax basis adjustments in dollars.';
comment on column tax_k1_export_run_results.retirements is 'Current year tax basis, normal or ordinary, retirement or dispositions in dollars.';
comment on column tax_k1_export_run_results.extraordinary_retires is 'Current year tax basis, extraordinary or abnormal, retirements or dispositions in dollars.';
comment on column tax_k1_export_run_results.accum_ordinary_retires is 'Accumulated retirements not reducing the depreciable base at the beginning of the period in dollars.';
comment on column tax_k1_export_run_results.depreciation is 'This is the sum of calc_depreciation, over_adj_depreciation, and any input depreciation adjustment (depreciation_adjust).';
comment on column tax_k1_export_run_results.cost_of_removal is 'Current year cost of removal dollars, whether directly expensed, in gain/loss, or applied to the reserve.';
comment on column tax_k1_export_run_results.cor_expense is 'Current year cost of removal dollars that are directly expensed.';
comment on column tax_k1_export_run_results.gain_loss is 'Current year (ordinary and capital) gain (+) or loss (-) in dollars including the (extraordinary) gain/loss.';
comment on column tax_k1_export_run_results.capital_gain_loss is 'Current year capital gain (+) or loss (-) in dollars (ordinary and extraordinary).';
comment on column tax_k1_export_run_results.est_salvage_pct is 'Estimated salvage percent entered as a decimal. (It is after reduction for the ADR exclusion).';
comment on column tax_k1_export_run_results.book_balance_end is 'The ending book basis balance of this tax asset record in dollars.';
comment on column tax_k1_export_run_results.accum_salvage_end is 'Accumulated salvage proceeds included in the ending depreciation reserve in dollars. Current period allowed tax depreciation after adjustment but before reduction for the amount capitalized if any.';
comment on column tax_k1_export_run_results.accum_ordin_retires_end is 'Accumulated retirements not reducing the depreciable base at the end of the period in dollars.';
comment on column tax_k1_export_run_results.sl_reserve_end is 'For "remaining life plan" rates, this is the ending tax basis minus the ending straight line reserve, i.e., the ending un-recovered straight line reserve in dollars.';
comment on column tax_k1_export_run_results.retire_invol_conv is 'Currently not used.';
comment on column tax_k1_export_run_results.salvage_invol_conv is 'Currently not used.';
comment on column tax_k1_export_run_results.salvage_extraord is 'Current salvage associated with extraordinary retirement in dollars.';
comment on column tax_k1_export_run_results.calc_depreciation is 'Calculated current period depreciation in dollars before over depreciation and other adjustments.';
comment on column tax_k1_export_run_results.over_adj_depreciation is 'Current depreciation adjustments in dollars calculated because of reserve limits.';
comment on column tax_k1_export_run_results.retire_res_impact is 'Current impact of ordinary retirements on the reserve for depreciation in dollars.';
comment on column tax_k1_export_run_results.transfer_res_impact is 'Current impact of all transfers on the reserve for depreciation in dollars.';
comment on column tax_k1_export_run_results.salvage_res_impact is 'Current impact of all salvage on the reserve for depreciation in dollars.';
comment on column tax_k1_export_run_results.cor_res_impact is 'Current impact of all cost of removal on the reserve for depreciation in dollars.';
comment on column tax_k1_export_run_results.adjusted_retire_basis is '(Not currently used)';
comment on column tax_k1_export_run_results.reserve_at_switch is 'Reserve at switch is the reserve in dollars at the point of switch from a net method to a gross method. It is subsequently adjusted for retirements impacting the depreciable base (e.g., extraordinary retirements). This variable is the beginning of the year value.';
comment on column tax_k1_export_run_results.capitalized_depr is 'Input decimal rate for capitalizing a percentage of the depreciation calculated (for example, transportation equipment).';
comment on column tax_k1_export_run_results.reserve_at_switch_end is 'End of the year reserve at switch.';
comment on column tax_k1_export_run_results.number_months_beg is 'This is the cumulative number of months in the tax years up to the current tax year. (Note that no ''half year'' convention is used in the first year, e.g. it is ''12'' where the vintage equals the tax year). This is needed to calculate the impacts of short tax years.';
comment on column tax_k1_export_run_results.number_months_end is 'This is the cumulative number of months in the tax years including the current tax year. )Note that no ''half year'' convention is used in the first year). This is needed to calculate the impacts of short tax years.';
comment on column tax_k1_export_run_results.ex_retire_res_impact is 'Current impact of extraordinary retirements on the reserve for depreciation in dollars.';
comment on column tax_k1_export_run_results.ex_gain_loss is 'Current year extraordinary gain (+) loss (-) in dollars, i.e. gain/loss related to an extraordinary retirement.';
comment on column tax_k1_export_run_results.quantity_end is 'Ending quantity of , for example, luxury autos. This is used only when applying limit processing.';
comment on column tax_k1_export_run_results.estimated_salvage_end is 'Dollar amount of estimated salvage at the end of the period.';
comment on column tax_k1_export_run_results.job_creation_amount is 'Amount of the bonus depreciation (under the Job Creation Act of 2002, the Job Growth Act of 2003, etc) taken during the year.';
comment on column tax_k1_export_run_results.book_balance_adjust is 'Adjustment to the book balance in dollars.';
comment on column tax_k1_export_run_results.accum_reserve_adjust is 'User-input dollar amount adjustment to accumulated tax depreciation reserve balance of the tax asset.';
comment on column tax_k1_export_run_results.depreciable_base_adjust is 'User-input dollar amount adjustment to depreciable base of the tax asset.';
comment on column tax_k1_export_run_results.depreciation_adjust is 'User-input dollar amount adjustment to current year depreciation expense for the tax asset.';
comment on column tax_k1_export_run_results.gain_loss_adjust is 'User-input dollar amount adjustment to ordinary gain/loss.';
comment on column tax_k1_export_run_results.cap_gain_loss_adjust is 'User-input dollar amount adjustment to capital gain/loss.';
comment on column tax_k1_export_run_results.gross_retirements is 'The tax depreciation retirements plus the change in basis difference.';
comment on column tax_k1_export_run_results.gross_accum_res_retire is 'The tax depreciation retirement reserve impact plus the change in basis difference.';
comment on column tax_k1_export_run_results.basis_change_diff is 'The change in basis difference. For the MLP reconciling items, the summed value from tax book reconcile of : basis amount beginning - basis amount end - basis amount activity - basis amounts transfers';
comment on column tax_k1_export_run_results.gross_additions is 'The tax depreciation additions plus the MLP reconciling items summed value from tax book reconcile of basis amount activity.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2669, 0, 2015, 2, 0, 0, 044286, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044286_pwrtax_add_columns_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;