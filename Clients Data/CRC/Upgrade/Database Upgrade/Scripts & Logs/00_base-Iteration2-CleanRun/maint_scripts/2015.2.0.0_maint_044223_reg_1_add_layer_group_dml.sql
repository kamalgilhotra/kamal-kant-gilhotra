/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_044223_reg_1_add_layer_group_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/22/2015 Anand R        Reg - Add restrictions for layer group for reports
||============================================================================
*/

insert into pp_dynamic_filter_restrictions (restriction_id, filter_id, restrict_by_filter_id, sqls_column_expression)
values ( 76, 194, 193, 'reg_sub_acct_type.reg_acct_type_id' );

insert into pp_dynamic_filter_restrictions (restriction_id, filter_id, restrict_by_filter_id, sqls_column_expression)
values ( 77, 280, 191, 'reg_incremental_adjust_type.incremental_adj_type_id' );

insert into pp_dynamic_filter_restrictions (restriction_id, filter_id, restrict_by_filter_id, sqls_column_expression)
values ( 78, 192, 191, 'incremental_adj_type_id' );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2707, 0, 2015, 2, 0, 0, 044223, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044223_reg_1_add_layer_group_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;