/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044184_taxrpr_repair_thresholds_base_year_3_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/06/2015 Daniel Motter  Creation
||============================================================================
*/

-- Do the actual modify on base_year to number(4,0)
alter table repair_thresholds modify base_year number(4,0);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2674, 0, 2015, 2, 0, 0, 044184, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044184_taxrpr_repair_thresholds_base_year_3_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;