/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051029_lease_06_add_add_tools_window_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.1 04/27/2018 Crystal Yura     Add new window to Admin>Tools list
||============================================================================
*/
insert into PP_CONVERSION_WINDOWS( WINDOW_ID ,
          DESCRIPTION ,
          LONG_DESCRIPTION ,
          WINDOW_NAME     )
select max(window_id) + 1, 'Initialize Lessee/Lessor ME Tables', 'Initialize Lessee/Lessor ME Control Tables For New Company', 'w_ls_company_setup'
from PP_CONVERSION_WINDOWS;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6847, 0, 2017, 3, 0, 1, 51029, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051029_lease_06_add_add_tools_window_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;