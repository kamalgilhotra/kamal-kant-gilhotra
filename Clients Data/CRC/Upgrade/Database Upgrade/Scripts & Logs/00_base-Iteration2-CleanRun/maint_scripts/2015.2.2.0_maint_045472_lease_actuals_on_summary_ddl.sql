/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045472_lease_actuals_on_summary_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 02/26/2016 Will Davis 	 Add columns to summary forecast table
|| 2015.2.2.0 04/14/2016 David Haupt	  Backporting to 2015.2.2
||============================================================================
*/


ALTER TABLE LS_SUMMARY_FORECAST
ADD BEGIN_RESERVE NUMBER(22,2) NULL;

ALTER TABLE LS_SUMMARY_FORECAST
ADD END_RESERVE NUMBER(22,2) NULL;

ALTER TABLE LS_SUMMARY_FORECAST
ADD rental_expense NUMBER(22,2) NULL;

alter table ls_summary_forecast
ADD IS_ACTUALS NUMBER(1,0) DEFAULT 0 NOT NULL;

comment on column ls_summary_forecast.is_actuals is '0 indicates that the row is from a forecast. 1 indicates that it reflects current values';
comment on column ls_summary_forecast.end_reserve IS 'The ending reserve for this asset.';
comment on column ls_summary_forecast.begin_reserve IS 'The beginning reserve for this asset.';
comment on column ls_summary_forecast.rental_expense IS 'Rental expense associated with operating leases';

ALTER TABLE LS_SUMMARY_FORECAST DROP CONSTRAINT PK_LS_SUMMARY_FORECAST;

ALTER TABLE ls_summary_forecast
  ADD CONSTRAINT pk_ls_summary_forecast PRIMARY KEY (
    company_id,
    revision,
    set_of_books_id,
    "MONTH",
    is_actuals
  )
  USING INDEX
    TABLESPACE pwrplant_idx
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3076, 0, 2015, 2, 2, 0, 045472, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045472_lease_actuals_on_summary_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;