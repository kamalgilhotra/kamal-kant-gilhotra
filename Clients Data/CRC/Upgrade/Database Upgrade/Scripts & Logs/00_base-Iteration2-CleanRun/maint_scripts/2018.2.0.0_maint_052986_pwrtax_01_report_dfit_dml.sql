/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052986_pwrtax_01_report_dfit_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 01/31/2019 K Powers	  Add PowerTax Reports 260,261,262
||============================================================================
*/

INSERT
INTO pp_Reports
  (
    report_id,
    description,
    long_Description,
    subsystem,
    datawindow,
    special_note,
    report_Type,
    time_option,
    report_number,
    input_Window,
    filter_option,
    status,
    pp_Report_subsystem_id,
    report_Type_id,
    pp_Report_Time_option_id,
    pp_Report_filter_id,
    pp_Report_status_id,
    pp_report_envir_id,
    documentation,
    user_Comment,
    last_Approved_date,
    pp_Report_number,
    old_Report_number,
    dynamic_dw,
    turn_off_multi_thread
  )
SELECT
  (SELECT MAX(report_id)
  FROM pp_Reports
  WHERE report_id>=400000
  AND report_id  <=499999
  )+1,
  'DFIT Excess Activity',
  'Reports timing difference, ADIT, FAS109, Flowthrough, and Excess activity by normalization schema',
  subsystem,
  'dw_tax_rpt_dfit_summ_activity',
  special_note,
  report_type,
  time_option,
  'PwrTax - 260',
  input_Window,
  filter_option,
  status,
  pp_Report_subsystem_id,
  report_Type_id,
  pp_Report_Time_option_id,
  pp_Report_filter_id,
  pp_Report_status_id,
  pp_report_envir_id,
  documentation,
  user_Comment,
  last_Approved_date,
  pp_Report_number,
  old_Report_number,
  0 dynamic_dw,
  turn_off_multi_thread
FROM pp_Reports
WHERE report_number ='PwrTax - 257';

INSERT
INTO pp_Reports
  (
    report_id,
    description,
    long_Description,
    subsystem,
    datawindow,
    special_note,
    report_Type,
    time_option,
    report_number,
    input_Window,
    filter_option,
    status,
    pp_Report_subsystem_id,
    report_Type_id,
    pp_Report_Time_option_id,
    pp_Report_filter_id,
    pp_Report_status_id,
    pp_report_envir_id,
    documentation,
    user_Comment,
    last_Approved_date,
    pp_Report_number,
    old_Report_number,
    dynamic_dw,
    turn_off_multi_thread
  )
SELECT
  (SELECT MAX(report_id)
  FROM pp_Reports
  WHERE report_id>=400000
  AND report_id  <=499999
  )+1,
  'DFIT Excess Balances',
  'Reports timing difference, ADIT, FAS109, Flowthrough, and Excess balances by normalization schema',
  subsystem,
  'dw_tax_rpt_dfit_summ_balances',
  special_note,
  report_type,
  time_option,
  'PwrTax - 261',
  input_Window,
  filter_option,
  status,
  pp_Report_subsystem_id,
  report_Type_id,
  pp_Report_Time_option_id,
  pp_Report_filter_id,
  pp_Report_status_id,
  pp_report_envir_id,
  documentation,
  user_Comment,
  last_Approved_date,
  pp_Report_number,
  old_Report_number,
  0 dynamic_dw,
  turn_off_multi_thread
FROM pp_Reports
WHERE report_number ='PwrTax - 257';

INSERT
INTO pp_Reports
  (
    report_id,
    description,
    long_Description,
    subsystem,
    datawindow,
    special_note,
    report_Type,
    time_option,
    report_number,
    input_Window,
    filter_option,
    status,
    pp_Report_subsystem_id,
    report_Type_id,
    pp_Report_Time_option_id,
    pp_Report_filter_id,
    pp_Report_status_id,
    pp_report_envir_id,
    documentation,
    user_Comment,
    last_Approved_date,
    pp_Report_number,
    old_Report_number,
    dynamic_dw,
    turn_off_multi_thread
  )
SELECT
  (SELECT MAX(report_id)
  FROM pp_Reports
  WHERE report_id>=400000
  AND report_id  <=499999
  )+1,
  'DFIT Excess Detail',
  'Reports timing difference, ADIT, FAS109, Flowthrough, and Excess detail by normalization schema and tax record',
  subsystem,
  'dw_tax_rpt_dfit_excess_detail',
  special_note,
  report_type,
  time_option,
  'PwrTax - 262',
  input_Window,
  filter_option,
  status,
  pp_Report_subsystem_id,
  report_Type_id,
  pp_Report_Time_option_id,
  pp_Report_filter_id,
  pp_Report_status_id,
  pp_report_envir_id,
  documentation,
  user_Comment,
  last_Approved_date,
  pp_Report_number,
  old_Report_number,
  0 dynamic_dw,
  turn_off_multi_thread
FROM pp_Reports
WHERE report_number ='PwrTax - 257';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14542, 0, 2018, 2, 0, 0, 52986, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052986_pwrtax_01_report_dfit_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
