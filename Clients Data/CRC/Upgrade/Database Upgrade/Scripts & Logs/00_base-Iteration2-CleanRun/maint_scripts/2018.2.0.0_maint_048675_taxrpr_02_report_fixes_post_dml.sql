/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048675_taxrpr_02_report_fixes_post_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 08/01/2017 Eric Berger    Updating the datawindow values in
||                                      reporting tables to reflect the new
||                                      tax repairs standard.
||============================================================================
*/

--for report RPR - 0010
UPDATE pp_reports
SET datawindow = 'dw_rpr_0010'
WHERE report_number = 'RPR - 0010'
AND datawindow = 'dw_repairs_posted_orig_cpr_report';

--for report RPR - 0030
UPDATE pp_reports
SET datawindow = 'dw_rpr_0030'
WHERE report_number = 'RPR - 0030'
AND datawindow = 'dw_repairs_posted_details_cpr_report';

--report RPR - 0040

UPDATE pp_reports
SET datawindow = 'dw_rpr_0040'
WHERE datawindow = 'dw_repairs_106_101_batch_report';

--report RPR - 0050

UPDATE pp_reports
SET datawindow = 'dw_rpr_0050'
WHERE report_number = 'RPR - 0050'
AND datawindow = 'dw_repairs_106_101_taxtest_report';

--report RPR - 0060

UPDATE pp_reports
SET datawindow = 'dw_rpr_0060'
WHERE report_number = 'RPR - 0060';

--update report rpr - 0070, two reports used that report_number so the old data window needed to be referenced

UPDATE pp_reports
SET datawindow = 'dw_rpr_0070'
WHERE report_number = 'RPR - 0070'
AND datawindow = 'dw_rpr_rpt_asst_taxrpr_analysis';

--update report rpr 0071

UPDATE pp_reports
SET datawindow = 'dw_rpr_0071'
WHERE report_number = 'RPR - 0071';

--update report rpr - 0072

UPDATE pp_reports
SET datawindow = 'dw_rpr_0072'
WHERE report_number = 'RPR - 0072';

UPDATE pp_reports
SET pp_report_time_option_id = 44
WHERE report_number = 'RPR - 0072';

--for report RPR - 0080, there were two reports with that report number so the old datwindow was needed:
--also needed to update the pp_report_time_option_id to take a tax year instead of a date range

UPDATE pp_reports
SET datawindow = 'dw_rpr_0080'
WHERE report_number = 'RPR - 0080'
AND datawindow = 'dw_rpr_rpt_cwip_rollfwd';

UPDATE pp_reports
SET pp_report_time_option_id = 44
WHERE report_number = 'RPR - 0080'
AND datawindow = 'dw_rpr_0080';

--for rpr - 0090 had to also update pp_report_time_option_id so that it took a tax year as a parameter instead of a date range
    --*also had to create the table tax_repairs_company in order to run this report*

UPDATE pp_reports
SET datawindow = 'dw_rpr_0090'
WHERE report_number = 'RPR - 0090'
AND datawindow = 'dw_rpr_rpt_proj_taxrpr_analysis';

UPDATE pp_reports
SET pp_report_time_option_id = 44
WHERE report_number = 'RPR - 0090'
AND datawindow = 'dw_rpr_0090';

--for report RPR - 0091  had to update the pp_report_time_option_id

UPDATE pp_reports
SET datawindow = 'dw_rpr_0091'
WHERE report_number = 'RPR - 0091';

UPDATE pp_reports
SET pp_report_time_option_id = 44
WHERE report_number = 'RPR - 0091';

--for rpr - 0190 -- the report is hidden

UPDATE pp_reports
SET datawindow = 'dw_rpr_0190'
WHERE report_number = 'RPR - 0190';

UPDATE pp_reports
SET pp_report_envir_id = 1
WHERE datawindow = 'dw_rpr_0190';

--for rpr - 0191

UPDATE pp_reports
SET datawindow = 'dw_rpr_0191'
WHERE report_number = 'RPR - 0191';

--for rpr - 0200 -- the report is hidden*

UPDATE pp_reports
SET datawindow = 'dw_rpr_0200'
WHERE report_number = 'RPR - 0200';

--for RPR - 0201 -- the report is hidden*

UPDATE pp_reports
SET datawindow = 'dw_rpr_0201'
WHERE report_number = 'RPR - 0201';

--for report rpr - 300 the pp_report_time option_id needed to be changed to pass the correct inputs:

UPDATE pp_reports
SET pp_report_time_option_id = 44
WHERE report_number = 'RPR - 0300';

UPDATE pp_reports
SET datawindow = 'dw_rpr_0300'
WHERE report_number = 'RPR - 0300';

--for RPR - 0301, needed to also update the pp_report_time_option id from 2 to 44:

UPDATE pp_reports
SET datawindow = 'dw_rpr_0301'
WHERE report_number = 'RPR - 0301';

UPDATE pp_reports
SET pp_report_time_option_id = 44
WHERE report_number = 'RPR - 0301';

--set time option to date range for report 50
UPDATE pp_reports
SET pp_report_time_option_id = 2
WHERE report_number = 'RPR - 0050'
AND datawindow = 'dw_rpr_0050'
;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13727, 0, 2018, 2, 0, 0, 48675, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_048675_taxrpr_02_report_fixes_post_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;