/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_044527_sys_remove_change_book_schema_table_type_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/27/2015 Andrew Hill    Changed table type of book_schema table
||============================================================================
*/
UPDATE powerplant_tables
set pp_table_type_id = 'f'
WHERE table_name = 'book_schema';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2717, 0, 2015, 2, 0, 0, 044527, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044527_sys_remove_change_book_schema_table_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;