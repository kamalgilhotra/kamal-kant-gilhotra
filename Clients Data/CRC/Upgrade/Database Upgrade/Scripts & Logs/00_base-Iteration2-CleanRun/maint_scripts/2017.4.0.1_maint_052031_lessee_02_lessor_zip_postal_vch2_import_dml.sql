/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_052031_lessee_02_lessor_zip_postal_vch2_import_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.4.0.1  7/19/2018 build script   allow non numeric and extend zip/postal
||============================================================================
*/


update pp_import_column set column_type = 'varchar2(10)' 
where import_type_id in (
            select import_type_id From pp_import_Type 
            where lower(import_table_name) = 'ls_import_lessor')
            and lower(column_name) in ('zip','postal');
			
update pp_import_column set column_type = 'varchar2(10)' 
where import_type_id in (
            select import_type_id From pp_import_Type 
            where lower(import_table_name) = 'lsr_import_lessee')
            and lower(column_name) in ('zip','postal');
			

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8945, 0, 2017, 4, 0, 1, 52031, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052031_lessee_02_lessor_zip_postal_vch2_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		   