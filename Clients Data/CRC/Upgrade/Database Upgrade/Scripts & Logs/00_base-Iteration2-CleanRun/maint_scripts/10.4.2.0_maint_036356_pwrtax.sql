/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036356_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/10/2014 Andrew Scott        MLP Transfers, Drop Down QA issues
||============================================================================
*/

----add an index to this table
create index TAX_DEPRECIATION_TRC_IDX
   on TAX_DEPRECIATION (TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;

----regather stats
declare
   CODE number(22, 0);
begin
   CODE := ANALYZE_TABLE('TAX_TEMP_TRANSFER_CONTROL');
end;
/

----regather stats
declare CODE number(22, 0);
begin
   CODE := ANALYZE_TABLE('TAX_DEPRECIATION');
end;
/


----change this field from varchar2(35) to (254)
alter table TAX_PACKAGE_CONTROL modify DESCRIPTION varchar2(254);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1017, 0, 10, 4, 2, 0, 36356, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036356_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;