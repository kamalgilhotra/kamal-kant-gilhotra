/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040777_cwip_ssp_wo_gl_reconciliation_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 12/16/2014 Luke Warren      creation of ssp process
||============================================================================
*/
-- Use merge on the off chance that they already have a record called Depreciation Approval 
MERGE INTO pp_processes p1
USING (
	SELECT Nvl(Max(process_id),0) + 1 AS process_id, 'WO GL Reconciliation' AS description,
		'WO GL Reconciliation Server Side Process' AS long_description, 'ssp_wo_gl_reconciliation.exe' AS executable_file,
		'2015.1.0.0' AS version, 0 AS allow_concurrent
	FROM pp_processes) p2
ON (Lower(Trim(p1.description)) = Lower(Trim(p2.description)))
WHEN MATCHED THEN
	UPDATE SET p1.long_description = p2.long_description,
		p1.executable_file = p2.executable_file,
		p1.version = p2.version,
		p1.allow_concurrent = p2.allow_concurrent
WHEN NOT MATCHED THEN
	INSERT (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (p2.process_id, p2.description, p2.long_description, p2.executable_file, p2.version, p2.allow_concurrent)
;

-- Create new SQL Dynamic Validation type for the Work Order Control G/L Reconciliation 
insert into wo_validation_type (
	wo_validation_type_id, description, long_description, "FUNCTION", find_company, col1, col2, hard_edit)
values (
	1058, 'WO Control - GL Recon', 'WO Control - GL Reconciliation', 'f_reconcile_gl_wo', 'select company_id from company_setup where company_id = <arg2>', 'month', 'company_id', 1);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2120, 0, 2015, 1, 0, 0, 040777, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040777_cwip_ssp_wo_gl_reconciliation_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;