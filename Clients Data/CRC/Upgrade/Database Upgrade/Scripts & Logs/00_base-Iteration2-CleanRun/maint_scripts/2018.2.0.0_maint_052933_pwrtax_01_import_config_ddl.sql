/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052933_pwrtax_01_import_config_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/05/2019 Shane "C" Ward   Updates to include Asset ID on PowerTax Imports
||============================================================================
*/

alter table tax_import_adds add asset_id number(22,0);
alter table tax_import_adds_arc add asset_id number(22,0);

alter table tax_import_rets add asset_id number(22,0);
alter table tax_import_rets_arc add asset_id number(22,0);

alter table tax_import_trans add asset_id number(22,0);
alter table tax_import_trans_arc add asset_id number(22,0);

alter table tax_import_ua_depr add asset_id number(22,0);
alter table tax_import_ua_depr_arc add asset_id number(22,0);

alter table tax_import_adds add asset_xlate varchar2(254);
alter table tax_import_adds_arc add asset_xlate varchar2(254);

alter table tax_import_rets add asset_xlate varchar2(254);
alter table tax_import_rets_arc add asset_xlate varchar2(254);

alter table tax_import_trans add asset_xlate varchar2(254);
alter table tax_import_trans_arc add asset_xlate varchar2(254);

alter table tax_import_ua_depr add asset_xlate varchar2(254);
alter table tax_import_ua_depr_arc add asset_xlate varchar2(254);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15683, 0, 2018, 2, 0, 0, 52933, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052933_pwrtax_01_import_config_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;