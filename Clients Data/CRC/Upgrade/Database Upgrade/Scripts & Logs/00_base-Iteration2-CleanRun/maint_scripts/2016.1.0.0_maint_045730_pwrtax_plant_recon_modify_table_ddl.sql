/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045730_pwrtax_plant_recon_modify_table_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2016.1.0  06/16/2016 Anand R       drop and recreate table tax_plant_recon_form_adj
||============================================================================
*/

drop table tax_plant_recon_form_adj;

CREATE TABLE tax_plant_recon_form_adj (
  record_id      NUMBER(22,0) NOT NULL,
  tax_pr_form_id NUMBER(22,0) NOT NULL,
  jurisdiction_id NUMBER(22,0) NULL,
  tax_pr_adj_id  NUMBER(22,0) NOT NULL,
  beg_balance    NUMBER(22,2) NOT NULL,
  additions      NUMBER(22,2) NOT NULL,
  adjustments    NUMBER(22,2) NOT NULL,
  transfers      NUMBER(22,2) NOT NULL,
  retirements    NUMBER(22,2) NOT NULL,
  book_provision NUMBER(22,2) NOT NULL,
  actual_cor     NUMBER(22,2) NOT NULL,
  actual_salvage NUMBER(22,2) NOT NULL,
  other          NUMBER(22,2) NOT NULL,
  end_balance    NUMBER(22,2) NOT NULL,
  time_stamp     DATE         NULL,
  user_id        VARCHAR2(18) NULL
);

ALTER TABLE tax_plant_recon_form_adj
  ADD CONSTRAINT tax_plant_recon_form_adj_pk PRIMARY KEY ( record_id )
  USING INDEX TABLESPACE pwrplant_idx;
  
CREATE UNIQUE INDEX tax_plant_recon_form_adj_ui ON
tax_plant_recon_form_adj(tax_pr_form_id, jurisdiction_id, tax_pr_adj_id);

ALTER TABLE tax_plant_recon_form_adj
  ADD CONSTRAINT tax_plant_recon_form_adj_fk2 
  FOREIGN KEY ( tax_pr_form_id ) 
  REFERENCES tax_plant_recon_form ( tax_pr_form_id ); 
    
ALTER TABLE tax_plant_recon_form_adj
  ADD CONSTRAINT tax_plant_recon_form_adj_fk1 
  FOREIGN KEY ( tax_pr_adj_id ) 
  REFERENCES tax_plant_recon_adj (tax_pr_adj_id );

alter table tax_plant_recon_form_adj 
 add constraint tax_plant_recon_form_adj_fk3
 foreign key ( tax_pr_form_id, jurisdiction_id)
 references tax_plant_recon_form_jur ( tax_pr_form_id, jurisdiction_id);
  
COMMENT ON TABLE tax_plant_recon_form_adj IS '(S) [09] The Tax Plant Recon Form Adj Table stores the beginning and ending balances as well as current year activity by adjustment for a Plant Reconciliation form; the values are manually entered by the user.';

COMMENT ON COLUMN tax_plant_recon_form_adj.record_id IS 'System assigned identifier for a tax plant reconciliation form adjustment record.';
COMMENT ON COLUMN tax_plant_recon_form_adj.tax_pr_form_id IS 'System assigned identifier for a tax plant reconciliation form.';
COMMENT ON COLUMN tax_plant_recon_form_adj.tax_pr_form_id IS 'System assigned identifier for jurisdiction.';
COMMENT ON COLUMN tax_plant_recon_form_adj.tax_pr_adj_id IS 'System assigned identifier for a tax plant reconciliation adjustment.';
COMMENT ON COLUMN tax_plant_recon_form_adj.beg_balance IS 'Beginning balance';
COMMENT ON COLUMN tax_plant_recon_form_adj.additions IS 'Current year additions for the tax plant reconciliation adjustment.';
COMMENT ON COLUMN tax_plant_recon_form_adj.adjustments IS 'Current year adjustments for the tax plant reconciliation adjustment.';
COMMENT ON COLUMN tax_plant_recon_form_adj.transfers IS 'Current year transfers for the tax plant reconciliation adjustment.';
COMMENT ON COLUMN tax_plant_recon_form_adj.retirements IS 'Current year retirements for the tax plant reconciliation adjustment.';
COMMENT ON COLUMN tax_plant_recon_form_adj.book_provision IS 'Current year book provision';
COMMENT ON COLUMN tax_plant_recon_form_adj.actual_cor IS 'Current year cost of removal';
COMMENT ON COLUMN tax_plant_recon_form_adj.actual_salvage IS 'Current year salvage';
COMMENT ON COLUMN tax_plant_recon_form_adj.other IS 'Current year transfers and gain/loss';
COMMENT ON COLUMN tax_plant_recon_form_adj.end_balance IS 'Ending balance';
COMMENT ON COLUMN tax_plant_recon_form_adj.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN tax_plant_recon_form_adj.user_id IS 'Standard system-assigned user id used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3221, 0, 2016, 1, 0, 0, 045730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045730_pwrtax_plant_recon_modify_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;