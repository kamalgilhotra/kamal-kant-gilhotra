/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033667_proptax_standard_data.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     04/15/2015 Shane Ward     CR_Company is not used by all clients, allow clients to map cr control of company to reg companies
||============================================================================
*/

--REG_CR_COMPANY_MAP
CREATE TABLE reg_cr_company_map (
reg_company_id NUMBER(22,0) NOT NULL ,
cr_company_value  VARCHAR2(70) NOT NULL,
user_id VARCHAR2(30),
time_stamp timestamp );

ALTER TABLE reg_cr_company_map
  ADD CONSTRAINT pk_reg_cr_company_map PRIMARY KEY (
    reg_company_id,
    cr_company_value
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

ALTER TABLE reg_cr_combos_elements ADD map_to_reg_co NUMBER(22,0);
ALTER TABLE reg_cr_combos_elements_Bdg ADD map_to_reg_co NUMBER(22,0);
alter table reg_cr_combos add (reg_company_id number(22,0));
alter table reg_cr_where add (reg_company_id number(22,0));

COMMENT ON TABLE reg_cr_company_map IS 'Table used to map Reg Company to CR Company (driven by cr_system_control for company_value)';
COMMENT ON COLUMN reg_cr_company_map.reg_company_id IS 'Identifier of Regulatory Company';
COMMENT ON COLUMN reg_cr_company_map.cr_company_value IS 'Identifier of CR Company value for cr company field driven by cr system control';
COMMENT ON COLUMN reg_cr_combos_elements.map_to_reg_co IS 'Identifier of whether to map by CR Company(0) or Reg Company(1)';
COMMENT ON COLUMN reg_cr_combos_elements_bdg.map_to_reg_co IS 'Identifier of whether to map by CR Company(0) or Reg Company(1)';
COMMENT ON COLUMN reg_cr_combos.reg_company_id IS 'Identifier of Regulatory Company';
COMMENT ON COLUMN reg_cr_where.reg_company_id IS 'Identifier of Regulatory Company';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2500, 0, 2015, 1, 0, 0, 43519, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043519_reg_cr_company_map_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;