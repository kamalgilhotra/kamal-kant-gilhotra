/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044760_pwrtax_cc_fkey_drop_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 10/13/2015 Andrew Scott     Drop an fkey from tax book transactions.
||============================================================================
*/

SET SERVEROUTPUT ON

declare
   FK_TO_DROP varchar2(100);
   FK_FOUND boolean;
begin

   DBMS_OUTPUT.PUT_LINE('finding class code fkey to drop from TAX_BOOK_TRANSACTIONS.');

   FK_FOUND := true;

   begin 
      SELECT CONSTRAINT_NAME
      INTO FK_TO_DROP
      FROM USER_CONSTRAINTS C
      WHERE CONSTRAINT_TYPE = 'R'
      AND R_CONSTRAINT_NAME IN (
         select CONSTRAINT_NAME
         from USER_CONSTRAINTS C
         where CONSTRAINT_TYPE = 'P'
          and TABLE_NAME = 'CLASS_CODE'
      )
      AND TABLE_NAME = 'TAX_BOOK_TRANSACTIONS';
   exception
      when no_data_found then 
         FK_FOUND := false;
   end;
      
   if FK_FOUND then
      DBMS_OUTPUT.PUT_LINE('fkey found.');
      DBMS_OUTPUT.PUT_LINE('fkey name : ' || FK_TO_DROP);

      begin
         execute immediate 'alter table TAX_BOOK_TRANSACTIONS drop constraint '||FK_TO_DROP;
         DBMS_OUTPUT.PUT_LINE(FK_TO_DROP||' dropped from TAX_BOOK_TRANSACTIONS successfully.');
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE('Unable to drop constraint '||FK_TO_DROP||' from TAX_BOOK_TRANSACTIONS');
      end;
      
   else
      DBMS_OUTPUT.PUT_LINE('no fkey found to drop.');
   end if;
end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2914, 0, 2015, 2, 0, 0, 44760, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044760_pwrtax_cc_fkey_drop_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
