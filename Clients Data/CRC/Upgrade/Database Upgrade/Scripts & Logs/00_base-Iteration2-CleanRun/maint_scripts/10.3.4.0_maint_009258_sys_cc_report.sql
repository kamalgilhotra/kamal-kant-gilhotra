/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009258_sys_cc_report.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   02/27/2012 Sunjin Cone    Point Release
||============================================================================
*/

update PP_REPORTS
   set DATAWINDOW = 'dw_wo_monthly_charges_from_cc'
 where DATAWINDOW = 'dw_wo_monthly_charges';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (98, 0, 10, 3, 4, 0, 9258, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009258_sys_cc_report.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
