/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011166_cr_tables.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   03/14/2013 Marc Zawko      Point Release
||============================================================================
*/

create table CR_BUDGET_SUM_TEMPLATE
(
 TEMPLATE_ID number(22,0) not null,
 DESCRIPTION varchar2(35),
 TIME_STAMP  date,
 USER_ID     varchar2(18)
);

alter table CR_BUDGET_SUM_TEMPLATE
   add constraint CR_BUDGET_SUM_TEMPLATE_PK
       primary key (TEMPLATE_ID)
       using index tablespace PWRPLANT_IDX;

create table CR_BUDGET_SUM_ELEMENTS
(
 TEMPLATE_ID  number(22,0),
 ELEMENT_ID   number(22,0),
 STRUCTURE_ID number(22,0),
 TIME_STAMP   date,
 USER_ID      varchar2(18)
);

alter table CR_BUDGET_SUM_ELEMENTS
   add constraint CR_BUDGET_SUM_ELEMENTS_PK
       primary key (TEMPLATE_ID, ELEMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table CR_BUDGET_SUM_ELEMENTS
   add constraint CR_BUDGET_SUM_ELEMENTS_FK
       foreign key (TEMPLATE_ID)
       references CR_BUDGET_SUM_TEMPLATE (TEMPLATE_ID);

create table CR_BUDGET_SUM_VALUES
(
 TEMPLATE_ID   number(22,0),
 STRUCTURE_ID  number(22,0),
 ELEMENT_VALUE varchar2(75),
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

alter table CR_BUDGET_SUM_VALUES
   add constraint CR_BUDGET_SUM_VALUES_PK
       primary key (TEMPLATE_ID, STRUCTURE_ID, ELEMENT_VALUE)
       using index tablespace PWRPLANT_IDX;

alter table CR_BUDGET_SUM_VALUES
   add constraint CR_BUDGET_SUM_VALUES_FK
       foreign key (TEMPLATE_ID)
       references CR_BUDGET_SUM_TEMPLATE (TEMPLATE_ID);

create table CR_BUDGET_SUM_BV_MAP
(
 ID                        number(22,0),
 TABLE_NAME                varchar2(35),
 FROM_CR_BUDGET_VERSION_ID number(22,0),
 FROM_ID                   number(22,0),
 TO_CR_BUDGET_VERSION_ID   number(22,0),
 TO_ID                     number(22,0),
 TIME_STAMP                date,
 USER_ID                   varchar2(18)
);

alter table CR_BUDGET_SUM_BV_MAP
   add constraint CR_BUDGET_SUM_BV_MAP_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

create global temporary table CR_BUDGET_SUM_ELEMENTS_STG
(
 TEMPLATE_ID  number(22,0),
 ELEMENT_ID   number(22,0),
 STRUCTURE_ID number(22,0),
 TIME_STAMP   date,
 USER_ID      varchar2(18)
) on commit preserve rows;

alter table CR_BUDGET_SUM_ELEMENTS_STG
   add constraint CR_BUDGET_SUM_ELEMENTS_STG_PK
       primary key (TEMPLATE_ID, ELEMENT_ID);

create global temporary table CR_BUDGET_SUM_VALUES_STG
(
 TEMPLATE_ID   number(22,0),
 STRUCTURE_ID  number(22,0),
 ELEMENT_VALUE varchar2(75),
 TIME_STAMP    date,
 USER_ID       varchar2(18)
) on commit preserve rows;

alter table CR_BUDGET_SUM_VALUES_STG
   add constraint CR_BUDGET_SUM_VALUES_STG_PK
       primary key (TEMPLATE_ID, STRUCTURE_ID, ELEMENT_VALUE);

create global temporary table CR_BUDGET_SUM_VALUES_MAP
(
 ORIGINAL_VALUE number(22,0),
 NEW_VALUE      varchar2(75),
 TIME_STAMP     date,
 USER_ID        varchar2(18)
) on commit preserve rows;

alter table CR_BUDGET_SUM_VALUES_MAP
   add constraint CR_BUDGET_SUM_VALUES_MAP_PK
       primary key (ORIGINAL_VALUE);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (323, 0, 10, 4, 1, 0, 11166, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011166_cr_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;