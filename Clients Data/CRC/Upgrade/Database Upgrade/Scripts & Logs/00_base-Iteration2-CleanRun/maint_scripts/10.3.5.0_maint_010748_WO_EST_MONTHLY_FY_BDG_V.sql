/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010748_WO_EST_MONTHLY_FY_BDG_V.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/23/2012 Marc Zawko     Point Release
||============================================================================
*/

create or replace view WO_EST_MONTHLY_FY_BDG_V
(EST_MONTHLY_ID, TIME_STAMP, USER_ID, WORK_ORDER_ID, REVISION, YEAR, EXPENDITURE_TYPE_ID,
 EST_CHG_TYPE_ID, DEPARTMENT_ID, JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST,
 SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER, TOTAL, UTILITY_ACCOUNT_ID, LONG_DESCRIPTION,
 FUTURE_DOLLARS, HIST_ACTUALS, WO_WORK_ORDER_ID, JOB_TASK_ID, SUBSTITUTION_ID, HRS_JAN,
 HRS_FEB, HRS_MAR, HRS_APR, HRS_MAY, HRS_JUN, HRS_JUL, HRS_AUG, HRS_SEP, HRS_OCT, HRS_NOV,
 HRS_DEC, HRS_TOTAL, QTY_JAN, QTY_FEB, QTY_MAR, QTY_APR, QTY_MAY, QTY_JUN, QTY_JUL, QTY_AUG,
 QTY_SEP, QTY_OCT, QTY_NOV, QTY_DEC, QTY_TOTAL
) as
 select EST_MONTHLY_ID,
        TIME_STAMP,
        USER_ID,
        WORK_ORDER_ID,
        REVISION,
        year,
        EXPENDITURE_TYPE_ID,
        EST_CHG_TYPE_ID,
        DEPARTMENT_ID,
        JANUARY,
        FEBRUARY,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULY,
        AUGUST,
        SEPTEMBER,
        OCTOBER,
        NOVEMBER,
        DECEMBER,
        JANUARY + FEBRUARY + MARCH + APRIL + MAY + JUNE + JULY + AUGUST + SEPTEMBER + OCTOBER +
        NOVEMBER + DECEMBER TOTAL,
        UTILITY_ACCOUNT_ID,
        LONG_DESCRIPTION,
        FUTURE_DOLLARS,
        HIST_ACTUALS,
        WO_WORK_ORDER_ID,
        JOB_TASK_ID,
        SUBSTITUTION_ID,
        HRS_JAN,
        HRS_FEB,
        HRS_MAR,
        HRS_APR,
        HRS_MAY,
        HRS_JUN,
        HRS_JUL,
        HRS_AUG,
        HRS_SEP,
        HRS_OCT,
        HRS_NOV,
        HRS_DEC,
        HRS_JAN + HRS_FEB + HRS_MAR + HRS_APR + HRS_MAY + HRS_JUN + HRS_JUL + HRS_AUG + HRS_SEP +
        HRS_OCT + HRS_NOV + HRS_DEC HRS_TOTAL,
        QTY_JAN,
        QTY_FEB,
        QTY_MAR,
        QTY_APR,
        QTY_MAY,
        QTY_JUN,
        QTY_JUL,
        QTY_AUG,
        QTY_SEP,
        QTY_OCT,
        QTY_NOV,
        QTY_DEC,
        QTY_JAN + QTY_FEB + QTY_MAR + QTY_APR + QTY_MAY + QTY_JUN + QTY_JUL + QTY_AUG + QTY_SEP +
        QTY_OCT + QTY_NOV + QTY_DEC QTY_TOTAL
   from (select W.EST_MONTHLY_ID EST_MONTHLY_ID,
                max(W.TIME_STAMP) TIME_STAMP,
                max(W.USER_ID) USER_ID,
                W.WORK_ORDER_ID WORK_ORDER_ID,
                W.REVISION REVISION,
                P.FISCAL_YEAR year,
                max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
                max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
                max(W.DEPARTMENT_ID) DEPARTMENT_ID,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '01', W.JANUARY, 0), 0)) JANUARY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '02', W.FEBRUARY, 0), 0)) FEBRUARY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '03', W.MARCH, 0), 0)) MARCH,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '04', W.APRIL, 0), 0)) APRIL,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '05', W.MAY, 0), 0)) MAY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '06', W.JUNE, 0), 0)) JUNE,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '07', W.JULY, 0), 0)) JULY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '08', W.AUGUST, 0), 0)) AUGUST,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '09', W.SEPTEMBER, 0), 0)) SEPTEMBER,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '10', W.OCTOBER, 0), 0)) OCTOBER,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '11', W.NOVEMBER, 0), 0)) NOVEMBER,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '12', W.DECEMBER, 0), 0)) DECEMBER,
                max(W.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
                max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
                0 FUTURE_DOLLARS,
                0 HIST_ACTUALS,
                max(W.WO_WORK_ORDER_ID) WO_WORK_ORDER_ID,
                max(W.JOB_TASK_ID) JOB_TASK_ID,
                max(W.SUBSTITUTION_ID) SUBSTITUTION_ID,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '01', W.HRS_JAN, 0), 0)) HRS_JAN,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '02', W.HRS_FEB, 0), 0)) HRS_FEB,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '03', W.HRS_MAR, 0), 0)) HRS_MAR,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '04', W.HRS_APR, 0), 0)) HRS_APR,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '05', W.HRS_MAY, 0), 0)) HRS_MAY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '06', W.HRS_JUN, 0), 0)) HRS_JUN,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '07', W.HRS_JUL, 0), 0)) HRS_JUL,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '08', W.HRS_AUG, 0), 0)) HRS_AUG,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '09', W.HRS_SEP, 0), 0)) HRS_SEP,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '10', W.HRS_OCT, 0), 0)) HRS_OCT,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '11', W.HRS_NOV, 0), 0)) HRS_NOV,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '12', W.HRS_DEC, 0), 0)) HRS_DEC,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '01', W.QTY_JAN, 0), 0)) QTY_JAN,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '02', W.QTY_FEB, 0), 0)) QTY_FEB,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '03', W.QTY_MAR, 0), 0)) QTY_MAR,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '04', W.QTY_APR, 0), 0)) QTY_APR,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '05', W.QTY_MAY, 0), 0)) QTY_MAY,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '06', W.QTY_JUN, 0), 0)) QTY_JUN,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '07', W.QTY_JUL, 0), 0)) QTY_JUL,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '08', W.QTY_AUG, 0), 0)) QTY_AUG,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '09', W.QTY_SEP, 0), 0)) QTY_SEP,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '10', W.QTY_OCT, 0), 0)) QTY_OCT,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '11', W.QTY_NOV, 0), 0)) QTY_NOV,
                sum(NVL(DECODE(SUBSTR(P.MONTH_NUMBER, 5, 2), '12', W.QTY_DEC, 0), 0)) QTY_DEC
           from PP_CALENDAR P, WO_EST_MONTHLY W
          where P.YEAR = W.YEAR
          group by W.EST_MONTHLY_ID, P.FISCAL_YEAR, W.WORK_ORDER_ID, W.REVISION);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (204, 0, 10, 3, 5, 0, 10748, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010748_WO_EST_MONTHLY_FY_BDG_V.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
