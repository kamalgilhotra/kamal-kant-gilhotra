/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041290_anlyt_12_data.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
||  1.0     09/06/2013 Scott Moody      Initial
|| 10.4.3.1 11/17/2014 Chad Theilman    Asset Analytics - Data
||============================================================================
*/

declare 
  doesTableExist number := 0;
  
  begin
	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CONTROL_GROUPS','PWRPLANT');
	
	if doesTableExist = 1 then
		begin
			delete from PWRPLANT.PA_CONTROL_GROUPS;  
    
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('Business Segment', 1, 1);
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('Company', 1, 1);
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('Functional Class', 1, 1);
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('FERC Plant Account', 1, 1);
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('Depreciation Group', 1, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('Utility Account', 1, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('Retirement Unit', 0, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('Property Group', 0, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_GROUPS VALUES ('Property Unit', 0, 0);
		end;
	end if;
		
	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CONTROL_LOCATIONS','PWRPLANT');
	
	if doesTableExist = 1 then
		begin
			delete from PWRPLANT.PA_CONTROL_LOCATIONS;
      
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('State', 1, 1);
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('County', 1, 1);
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('Major Location', 1, 1);
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('Division', 0, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('Minor Location', 0, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('Town', 0, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('Zip Code', 0, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('HW Region', 0, 0);
			INSERT INTO PWRPLANT.PA_CONTROL_LOCATIONS VALUES ('Charge Location', 0, 0);
		end;
	end if;
	
	end;
 /


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2030, 0, 10, 4, 3, 1, 041290, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_041290_anlyt_12_data.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;