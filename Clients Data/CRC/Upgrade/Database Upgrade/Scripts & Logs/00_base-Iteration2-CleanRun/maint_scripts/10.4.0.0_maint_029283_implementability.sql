/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029283_implementability.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   02/13/2013 Joseph King    Point Release
||============================================================================
*/

--removes approval_type_id field from wo_interface staging tables
alter table WO_INTERFACE_STAGING     drop column APPROVAL_TYPE_ID;
alter table WO_INTERFACE_STAGING_ARC drop column APPROVAL_TYPE_ID;

--adds external reason_code fields
alter table REASON_CODE add EXTERNAL_REASON_CD varchar2(254);

alter table WO_INTERFACE_STAGING     add EXT_REASON_CD varchar2(254);
alter table WO_INTERFACE_STAGING_ARC add EXT_REASON_CD varchar2(254);

--adds external wo_approval_group fields
alter table WO_APPROVAL_GROUP add EXTERNAL_WO_APPROVAL_GROUP varchar2(254);

alter table WO_INTERFACE_STAGING     add EXT_WO_APPROVAL_GROUP varchar2(254);
alter table WO_INTERFACE_STAGING_ARC add EXT_WO_APPROVAL_GROUP varchar2(254);

--adds external agreement fields
alter table CO_TENANCY_AGREEMENT add EXTERNAL_AGREEMENT varchar2(254);

alter table WO_INTERFACE_STAGING     add EXT_AGREEMENT varchar2(254);
alter table WO_INTERFACE_STAGING_ARC add EXT_AGREEMENT varchar2(254);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_reason_cd', 'reason_code', TO_DATE('2013-01-15 00:19:27', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', 222, 'External Reason Code', 2, null, null, null, null, null, null, null,
    null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_wo_approval_group', 'wo_approval_group',
    TO_DATE('2013-01-15 00:16:54', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'External WO Approval Group', 2, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_agreement', 'co_tenancy_agreement',
    TO_DATE('2013-01-15 00:18:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', 317,
    'External Agreement', 51, null, null, null, null, null, null, null, null);

alter table WO_INTERFACE_STAGING     modify EXT_WORK_ORDER_STATUS varchar2(254);
alter table WO_INTERFACE_STAGING_ARC modify EXT_WORK_ORDER_STATUS varchar2(254);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (302, 0, 10, 4, 0, 0, 29283, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029283_implementability.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
