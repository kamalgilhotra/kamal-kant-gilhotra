 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_041918_sys_est_api_new_rev_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/06/2015 D. Mendel     Create system control to have estimate APIs always create a new revision
 ||============================================================================
 */ 

INSERT INTO pp_system_control_company 
(control_id, control_name, control_value, time_stamp, user_id, description, long_description, user_disabled, company_id, control_type)
SELECT 
(SELECT Max(Nvl(control_id,0)) + 1 FROM pp_system_control_company),
'WOEST - API Always New Revision',
'No',
SYSDATE,
USER,
'dw_yes_no;1',
'"Yes" will always create a new unit est revision in the API. "No" will only create a new revision if the current revision is locked (approval_status_id: 2 pending, 3 approved, 5 inactive). Default is "No"',
NULL,
-1,
NULL
FROM dual
WHERE NOT EXISTS (
SELECT 1 FROM pp_system_control_company
WHERE Lower(Trim(control_name)) = Lower('WOEST - API Always New Revision'));

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2474, 0, 2015, 1, 0, 0, 41918, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041918_sys_est_api_new_rev_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;