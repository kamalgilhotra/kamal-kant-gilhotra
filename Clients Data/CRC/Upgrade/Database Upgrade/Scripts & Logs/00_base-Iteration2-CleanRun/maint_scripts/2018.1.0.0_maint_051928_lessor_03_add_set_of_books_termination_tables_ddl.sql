/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051928_lessor_03_add_set_of_books_termination_tables_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/26/2018  Alex Healey    setting set of books id on the termination tables to be non-nullable
||============================================================================
*/
ALTER TABLE lsr_ilr_termination_op
modify set_of_books_id   NOT NULL;

ALTER TABLE lsr_ilr_termination_st_df
modify set_of_books_id  NOT NULL;

ALTER TABLE lsr_ilr_termination_op
DROP CONSTRAINT lsr_ilr_termination_op_pk;

-- $$$ KKG, 20190711. Commenting out the drop of the Index, as it's getting dropped as part of constraint in the above statement.
-- DROP INDEX lsr_ilr_termination_op_pk;

ALTER TABLE lsr_ilr_termination_op
add CONSTRAINT lsr_ilr_termination_op_pk PRIMARY KEY (
    ilr_id,
    set_of_books_id
) using index tablespace PWRPLANT_IDX;

ALTER TABLE lsr_ilr_termination_st_df
DROP CONSTRAINT lsr_ilr_termination_st_df_pk;

-- $$$ KKG, 20190711. Commenting out the drop of the Index, as it's getting dropped as part of constraint in the above statement.
-- DROP INDEX lsr_ilr_termination_st_df_pk;

ALTER TABLE lsr_ilr_termination_st_df
ADD  CONSTRAINT lsr_ilr_termination_st_df_pk PRIMARY KEY (
	ilr_id,
	set_of_books_id
) using index tablespace PWRPLANT_IDX;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11024, 0, 2018, 1, 0, 0, 51928, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051928_lessor_03_add_set_of_books_termination_tables_ddl', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		   