/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040781_cwip_ssp_wo_appr_oh_afudc_wip_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 2/2/2015   Luke Warren      creation of ssp process - Approve OH and AFUDC WIP
||============================================================================
*/
-- Use merge on the off chance that they already have a record called Depreciation Approval 
MERGE INTO pp_processes p1
USING (
	SELECT Nvl(Max(process_id),0) + 1 AS process_id, 'WO Appr Overheads and AFUDC WIP' AS description,
		'WO Appr Overheads and AFUDC WIP' AS long_description, 'ssp_wo_appr_oh_afudc_wip.exe' AS executable_file,
		'2015.1.0.0' AS version, 0 AS allow_concurrent, 1 AS wo_month_end_flag
	FROM pp_processes) p2
ON (Lower(Trim(p1.description)) = Lower(Trim(p2.description)))
WHEN MATCHED THEN
	UPDATE SET p1.long_description = p2.long_description,
		p1.executable_file = p2.executable_file,
		p1.version = p2.version,
		p1.allow_concurrent = p2.allow_concurrent,
		p1.wo_month_end_flag = p2.wo_month_end_flag
WHEN NOT MATCHED THEN
	INSERT (process_id, description, long_description, executable_file, version, allow_concurrent, wo_month_end_flag)
	VALUES (p2.process_id, p2.description, p2.long_description, p2.executable_file, p2.version, p2.allow_concurrent, p2.wo_month_end_flag)
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2242, 0, 2015, 1, 0, 0, 040781, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040781_cwip_ssp_wo_appr_oh_afudc_wip_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;