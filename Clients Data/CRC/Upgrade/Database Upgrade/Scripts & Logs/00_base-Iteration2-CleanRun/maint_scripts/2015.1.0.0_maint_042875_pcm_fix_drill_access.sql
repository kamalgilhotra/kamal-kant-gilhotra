/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042879_pcm_program_link_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/12/2015 Sarah Byers      Allow for opening non-response/child windows
||============================================================================
*/

UPDATE ppbase_workspace_links
SET LINKED_WORKSPACE_IDENTIFIER='wo_' || SUBSTR(LINKED_WORKSPACE_IDENTIFIER, 4)
WHERE MODULE='pcm'
AND SUBSTR(WORKSPACE_IDENTIFIER,1,3) = 'fp_' AND SUBSTR(LINKED_WORKSPACE_IDENTIFIER,1,3) = 'fp_';

UPDATE ppbase_workspace_links
SET LINKED_WORKSPACE_IDENTIFIER='fp_' || SUBSTR(LINKED_WORKSPACE_IDENTIFIER, 4)
WHERE MODULE='pcm'
AND SUBSTR(WORKSPACE_IDENTIFIER,1,3) = 'wo_' AND SUBSTR(LINKED_WORKSPACE_IDENTIFIER,1,3) = 'wo_';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2300, 0, 2015, 1, 0, 0, 042875, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042875_pcm_fix_drill_access.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;