/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009956_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/05/2012 Chris Mardis   Point Release
||============================================================================
*/

update PP_REPORTS
   set DYNAMIC_DW = 0
 where DATAWINDOW in ('dw_wo_conversion_report42',
                      'dw_fpest_cur_13_rep_bv',
                      'dw_fpestact_cur_13_rep_bv',
                      'dw_fpestact_cur_rep_bv',
                      'dw_budg_vs_act_beg_cwip_afudc_cpi',
                      'dw_budget_vs_actual_afudc_fp03',
                      'dw_budget_vs_actual_afudc_fp04',
                      'dw_budget_vs_actual_afudc_fp04a',
                      'dw_budget_vs_actual_afudc_fp05',
                      'dw_budget_vs_actual_afudc_fp05a',
                      'dw_alloc_insure_report2',
                      'dw_budg_vs_act_afudc_var_fp06',
                      'dw_budg_vs_act_afudc_var_fp06a');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (170, 0, 10, 3, 5, 0, 9956, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009956_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;