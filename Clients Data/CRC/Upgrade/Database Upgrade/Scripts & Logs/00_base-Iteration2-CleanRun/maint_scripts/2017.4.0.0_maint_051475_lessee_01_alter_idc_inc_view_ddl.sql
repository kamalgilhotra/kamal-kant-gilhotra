/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051475_lessee_01_alter_idc_inc_view_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/18/2018 Josh Sandler     Alter v_ls_ilr_idc_inc_accrued_fx_vw
||============================================================================
*/

CREATE OR replace VIEW v_ls_ilr_idc_inc_accrued_fx_vw
AS
  SELECT stg.ilr_id                                                                   ilr_id,
         stg.revision                                                                 revision,
         stg.set_of_books_id                                                          set_of_books_id,
         SUM( initial_direct_cost )                                                   accrued_idc,
         SUM( incentive_amount - Nvl( Decode( s.month, i.est_in_svc_date, 0,
                                                       incentive_math_amount ), 0 ) ) accrued_incentive,
         curr.remeasurement_date,
         curr_cap.is_om                                                               curr_is_om,
         appr_cap.is_om                                                               approved_is_om,
         Decode( appr_cap.is_om, 1, Decode( curr_cap.is_om, 0, 1 ),
                                 0 )                                                  switch_to_cap --whether or not we are converting cap types on the schedule being calculated
  FROM   LS_ILR_SCHEDULE s,
         LS_ILR_STG stg,
         LS_ILR i,
         LS_ILR_OPTIONS curr,
         LS_ILR_OPTIONS appr,
         LS_LEASE_CAP_TYPE curr_cap,
         LS_LEASE_CAP_TYPE appr_cap
  WHERE  stg.ilr_id = i.ilr_id AND
         s.ilr_id = i.ilr_id AND
         s.revision = i.current_revision AND
         i.ilr_id = appr.ilr_id AND
         i.current_revision = appr.revision AND
         stg.ilr_id = curr.ilr_id AND
         stg.revision = curr.revision AND
         s.set_of_books_id = stg.set_of_books_id AND
         curr_cap.ls_lease_cap_type_id = curr.lease_cap_type_id AND
         appr_cap.ls_lease_cap_type_id = appr.lease_cap_type_id AND
         s.month < stg.npv_start_date
  GROUP  BY stg.ilr_id,stg.revision,stg.set_of_books_id,curr.remeasurement_date,curr_cap.is_om,appr_cap.is_om
  UNION
  SELECT DISTINCT ilr_id,
                  revision,
                  set_of_books_id,
                  0    accrued_idc,
                  0    accrued_incentive,
                  NULL remeasurement_date,
                  0 curr_is_om,
                  0 approved_is_om,
                  0 switch_to_cap
  FROM   LS_ILR_STG
  WHERE npv_start_date IS NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6782, 0, 2017, 4, 0, 0, 51475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051475_lessee_01_alter_idc_inc_view_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;