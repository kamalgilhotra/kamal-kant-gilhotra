/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036244_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/06/2014 Julia Breuer
||============================================================================
*/

--
-- Create system options to replace system controls used in the Additions, Retirements, and Transfers interfaces.
--

insert into ppbase_system_options ( system_option_id, time_stamp, user_id, long_description, system_only, pp_default_value, option_value, is_base_option ) values ( 'Reverse Tax Expensing Months', sysdate, user, 'Indicates how many months in the current year of tax expensing data were picked up during processing of the prior tax year.', 0, '0', null, 1 );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '0', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '01', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '02', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '03', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '04', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '05', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '06', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '07', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '08', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '09', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '10', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '11', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', '12', sysdate, user );
insert into ppbase_system_options_module ( system_option_id, module, time_stamp, user_id ) values ( 'Reverse Tax Expensing Months', 'powertax', sysdate, user );

insert into ppbase_system_options ( system_option_id, time_stamp, user_id, long_description, system_only, pp_default_value, option_value, is_base_option ) values ( 'Forward Tax Expensing Months', sysdate, user, 'Indicates how many months in the next year of tax expensing data to process during the current tax year.', 0, '0', null, 1 );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '0', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '01', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '02', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '03', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '04', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '05', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '06', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '07', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '08', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '09', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '10', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '11', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', '12', sysdate, user );
insert into ppbase_system_options_module ( system_option_id, module, time_stamp, user_id ) values ( 'Forward Tax Expensing Months', 'powertax', sysdate, user );

insert into ppbase_system_options ( system_option_id, time_stamp, user_id, long_description, system_only, pp_default_value, option_value, is_base_option ) values ( 'Reverse Early In Service Months', sysdate, user, 'Indicates how many months in the current year of addition data with early in service dates were picked up during processing of the prior tax year.', 0, '0', null, 1 );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '0', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '01', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '02', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '03', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '04', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '05', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '06', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '07', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '08', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '09', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '10', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '11', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Reverse Early In Service Months', '12', sysdate, user );
insert into ppbase_system_options_module ( system_option_id, module, time_stamp, user_id ) values ( 'Reverse Early In Service Months', 'powertax', sysdate, user );

insert into ppbase_system_options ( system_option_id, time_stamp, user_id, long_description, system_only, pp_default_value, option_value, is_base_option ) values ( 'Forward Early In Service Months', sysdate, user, 'Indicates how many months in the next year of addition data with early in service dates to process during the current tax year.', 0, '0', null, 1 );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '0', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '01', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '02', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '03', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '04', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '05', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '06', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '07', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '08', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '09', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '10', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '11', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Forward Early In Service Months', '12', sysdate, user );
insert into ppbase_system_options_module ( system_option_id, module, time_stamp, user_id ) values ( 'Forward Early In Service Months', 'powertax', sysdate, user );

insert into ppbase_system_options ( system_option_id, time_stamp, user_id, long_description, system_only, pp_default_value, option_value, is_base_option ) values ( 'Keep Work Order', sysdate, user, 'Whether to keep the work order number when loading the additions interface.  If ''Yes'', The work order number will be visible through the audit button.', 0, 'No', null, 1 );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Keep Work Order', 'No', sysdate, user );
insert into ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Keep Work Order', 'Yes', sysdate, user );
insert into ppbase_system_options_module ( system_option_id, module, time_stamp, user_id ) values ( 'Keep Work Order', 'powertax', sysdate, user );

--
-- Create a menu item for the System Options workspace.
--
insert into pwrplant.ppbase_workspace ( module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp ) values ( 'powertax', 'admin_systemoptions', sysdate, user, 'System Options', 'uo_tax_admin_wksp_system_options', 'System Options' );

insert into pwrplant.ppbase_menu_items ( module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn ) values ( 'powertax', 'admin', sysdate, user, 1, 6, 'Admin', '', '', '', 1 );
insert into pwrplant.ppbase_menu_items ( module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn ) values ( 'powertax', 'admin_systemoptions', sysdate, user, 2, 1, 'System Options', '', 'admin', 'admin_systemoptions', 1 );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (947, 0, 10, 4, 2, 0, 36244, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036244_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;