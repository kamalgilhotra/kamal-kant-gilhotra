/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_053367_lessee_02_add_remeasure_incentive_backfill_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 4/10/19     C Yura	Add remeasure incentive column
||============================================================================
*/

declare
  old_sum number;
  new_sum number;
  sum_mismatch exception;
begin
  DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
  DBMS_OUTPUT.PUT_LINE('53367 BEGIN');

  old_sum := 0;
  
  select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into old_sum from ls_ilr_schedule a;

  DBMS_OUTPUT.PUT_LINE('ILR OLD INCENTIVE_MATH_AMOUNT_SUM:' || to_char(old_sum));
  
  DBMS_OUTPUT.PUT_LINE('updating ilr incentive_cap_amount for remeasurement dates');
  
  merge into ls_ilr_schedule a
  using (
    select s.ilr_id, s.revision, s.set_of_books_id, s.month, c.remeasurement_date, nvl(s.incentive_math_amount,0) incentive_math_amount
    from ls_ilr_schedule s, 
    ls_ilr b, 
    ls_ilr_options c
    where 
    s.ilr_id = b.ilr_id
    and s.ilr_id = c.ilr_id
    and s.revision = c.revision
    and nvl(s.incentive_math_amount,0) <> 0
    and c.remeasurement_date is not null
    and s.month = c.remeasurement_date) b
    on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.set_of_books_id = b.set_of_books_id and a.month = b.month)
  when matched then
  update 
  set incentive_cap_amount = nvl(b.incentive_math_amount,0),
  incentive_math_amount = 0;
  
  new_sum := 0;
  
  select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into new_sum from ls_ilr_schedule a;

  DBMS_OUTPUT.PUT_LINE('1:ILR NEW INCENTIVE_MATH_AMOUNT + incentive_cap_amount SUM:' || to_char(new_sum));
  
  if old_sum = new_sum then
    dbms_output.put_line('1:Done.');
  else
    rollback;
    dbms_output.put_line('SUMS NOT EQUAL');
    raise sum_mismatch;
  end if;
  
  old_sum := 0;
  
  select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into old_sum from ls_ilr_schedule a;

  DBMS_OUTPUT.PUT_LINE('ILR OLD INCENTIVE_MATH_AMOUNT_SUM:' || to_char(old_sum));
  
  DBMS_OUTPUT.PUT_LINE('updating ilr incentive_cap_amount for conversion dates');
  
  merge into ls_ilr_schedule a
  using (
      select s.ilr_id, s.revision, s.set_of_books_id, s.month, c.remeasurement_date, nvl(s.incentive_math_amount,0) incentive_math_amount
    from ls_ilr_schedule s, 
    ls_ilr b, 
    ls_ilr_options c,
    ls_forecast_ilr_master d,
    ls_forecast_version e
    where 
    s.ilr_id = b.ilr_id
    and s.ilr_id = c.ilr_id
    and s.revision = c.revision
    and s.ilr_id = d.ilr_id
    and s.revision = d.in_service_revision
    and nvl(s.incentive_math_amount,0) <> 0
    and e.revision = d.revision
    and s.month = e.conversion_date
    and e.forecast_type_id <> 4) b
    on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.set_of_books_id = b.set_of_books_id and a.month = b.month)
    when matched then
    update 
    set incentive_cap_amount = nvl(b.incentive_math_amount,0),
    incentive_math_amount = 0;

    new_sum := 0;
  
    select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into new_sum from ls_ilr_schedule a;

    DBMS_OUTPUT.PUT_LINE('2:ILR NEW INCENTIVE_MATH_AMOUNT + incentive_cap_amount SUM:' || to_char(new_sum));
   
    if old_sum = new_sum then
        dbms_output.put_line('2:Done.');
      else
        rollback;
        dbms_output.put_line('SUMS NOT EQUAL');
        raise sum_mismatch;
    end if;
  
  old_sum := 0;

  select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into old_sum from ls_asset_schedule a;

  DBMS_OUTPUT.PUT_LINE('Asset OLD INCENTIVE_MATH_AMOUNT_SUM:' || to_char(old_sum));
  
  DBMS_OUTPUT.PUT_LINE('updating asset incentive_cap_amount for remeasurement dates');
  
  merge into ls_asset_schedule a
  using (
    select s.ls_asset_id, s.revision, s.set_of_books_id, s.month, c.remeasurement_date, nvl(s.incentive_math_amount,0) incentive_math_amount
    from ls_asset_schedule s, 
    ls_asset la,
    ls_ilr b, 
    ls_ilr_options c
    where 
    s.ls_asset_id = la.ls_asset_id
    and la.ilr_id = b.ilr_id
    and b.ilr_id = c.ilr_id
    and s.revision = c.revision
    and nvl(s.incentive_math_amount,0) <> 0
    and c.remeasurement_date is not null
    and s.month = c.remeasurement_date) b
    on (a.ls_asset_id = b.ls_asset_id and a.revision = b.revision and a.set_of_books_id = b.set_of_books_id and a.month = b.month)
    when matched then
    update 
    set incentive_cap_amount = nvl(b.incentive_math_amount,0),
    incentive_math_amount = 0;
  
  new_sum := 0;
  
  select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into new_sum from ls_asset_schedule a;

  DBMS_OUTPUT.PUT_LINE('3:Asset NEW INCENTIVE_MATH_AMOUNT + incentive_cap_amount SUM:' || to_char(new_sum));
  
  if old_sum = new_sum then
    dbms_output.put_line('3:Done.');
  else
    rollback;
    dbms_output.put_line('SUMS NOT EQUAL');
    raise sum_mismatch;
  end if;
  
  old_sum := 0;
  
  select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into old_sum from ls_asset_schedule a;

  DBMS_OUTPUT.PUT_LINE('Asset OLD INCENTIVE_MATH_AMOUNT_SUM:' || to_char(old_sum));
  
  DBMS_OUTPUT.PUT_LINE('updating asset incentive_cap_amount for remeasurement dates');
  
  merge into ls_asset_schedule a
  using (
        select s.ls_asset_id, s.revision, s.set_of_books_id, s.month, c.remeasurement_date, nvl(s.incentive_math_amount,0) incentive_math_amount
    from ls_asset_schedule s, 
    ls_asset la,
    ls_ilr b, 
    ls_ilr_options c,
    ls_forecast_ilr_master d,
    ls_forecast_version e
    where 
    s.ls_asset_id = la.ls_asset_id
    and la.ilr_id = b.ilr_id
    and b.ilr_id = c.ilr_id
    and s.revision = c.revision
    and b.ilr_id = d.ilr_id
    and s.revision = d.in_service_revision
    and nvl(s.incentive_math_amount,0) <> 0
    and e.revision = d.revision
    and s.month = e.conversion_date
    and e.forecast_type_id <> 4) b
    on (a.ls_asset_id = b.ls_asset_id and a.revision = b.revision and a.set_of_books_id = b.set_of_books_id and a.month = b.month)
    when matched then
    update 
    set incentive_cap_amount = nvl(b.incentive_math_amount,0),
    incentive_math_amount = 0;

    new_sum := 0;
  
    select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into new_sum from ls_asset_schedule a;

    DBMS_OUTPUT.PUT_LINE('4:Asset NEW INCENTIVE_MATH_AMOUNT + incentive_cap_amount SUM:' || to_char(new_sum));
   
    if old_sum = new_sum then
        dbms_output.put_line('4:Done.');
      else
        rollback;
        dbms_output.put_line('SUMS NOT EQUAL');
        raise sum_mismatch;
    end if;
  
  old_sum := 0;
  
  select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into old_sum from ls_ilr_schedule a;

  DBMS_OUTPUT.PUT_LINE('ILR OLD INCENTIVE_MATH_AMOUNT_SUM:' || to_char(old_sum));
  
  DBMS_OUTPUT.PUT_LINE('updating ilr incentive_cap_amount for min month dates');
  
  
  merge into ls_ilr_schedule a
  using (
         select s.ilr_id, s.revision, s.set_of_books_id, s.month, nvl(s.incentive_math_amount,0) incentive_math_amount
    from ls_ilr_schedule s, 
    ls_ilr b, 
    ls_ilr_options c,
    (select ilr_id, revision, min(month) min_month from ls_ilr_schedule group by ilr_id, revision) d
    where 
    s.ilr_id = b.ilr_id
    and s.ilr_id = c.ilr_id
    and s.revision = c.revision
    and s.ilr_id = d.ilr_id
    and s.revision = d.revision
    and nvl(s.incentive_math_amount,0) <> 0
    and s.month = d.min_month) b
    on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.set_of_books_id = b.set_of_books_id and a.month = b.month)
    when matched then
    update 
    set incentive_cap_amount = nvl(b.incentive_math_amount,0),
    incentive_math_amount = 0;

    new_sum := 0;
  
    select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into new_sum from ls_ilr_schedule a;

    DBMS_OUTPUT.PUT_LINE('5:ILR NEW INCENTIVE_MATH_AMOUNT + incentive_cap_amount SUM:' || to_char(new_sum));
   
    if old_sum = new_sum then
        dbms_output.put_line('5:Done.');
      else
        rollback;
        dbms_output.put_line('SUMS NOT EQUAL');
        raise sum_mismatch;
    end if;
  
   old_sum := 0;
  
  select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into old_sum from ls_asset_schedule a;

  DBMS_OUTPUT.PUT_LINE('Asset OLD INCENTIVE_MATH_AMOUNT_SUM:' || to_char(old_sum));
  
  DBMS_OUTPUT.PUT_LINE('updating asset incentive_cap_amount for min month dates');
  
  
  merge into ls_asset_schedule a
  using (
        select s.ls_asset_id, s.revision, s.set_of_books_id, s.month, nvl(s.incentive_math_amount,0) incentive_math_amount
    from ls_asset_schedule s, 
    ls_asset la,
    ls_ilr b, 
    ls_ilr_options c,
    (select ls_asset_id, revision, min(month) min_month from ls_asset_schedule group by ls_asset_id, revision) d
    where 
    s.ls_asset_id = la.ls_asset_id
    and la.ilr_id = b.ilr_id
    and b.ilr_id = c.ilr_id
    and s.revision = c.revision
    and s.ls_asset_id = d.ls_asset_id
    and s.revision = d.revision
    and nvl(s.incentive_math_amount,0) <> 0
    and s.month = d.min_month) b
    on (a.ls_asset_id = b.ls_asset_id and a.revision = b.revision and a.set_of_books_id = b.set_of_books_id and a.month = b.month)
    when matched then
    update 
    set incentive_cap_amount = nvl(b.incentive_math_amount,0),
    incentive_math_amount = 0;

    new_sum := 0;
  
    select nvl(sum(nvl(a.incentive_math_amount,0) + nvl(incentive_cap_amount,0)),0) into new_sum from ls_asset_schedule a;

    DBMS_OUTPUT.PUT_LINE('6:Asset NEW INCENTIVE_MATH_AMOUNT + incentive_cap_amount SUM:' || to_char(new_sum));
   
    if old_sum = new_sum then
        dbms_output.put_line('6:Done.');
      else
        rollback;
        dbms_output.put_line('SUMS NOT EQUAL');
        raise sum_mismatch;
    end if;
  
  
exception
  WHEN sum_mismatch THEN
    dbms_output.put_line('');
    dbms_output.put_line('*******************************************************');
    dbms_output.put_line('*** ERROR ENCOUNTERED');
    dbms_output.put_line('*** OLD AND NEW SUMS DO NOT MATCH, ABORTING' );
    dbms_output.put_line('*******************************************************');
    raise_application_error(-20101, 'Sums do not match');
  WHEN OTHERS THEN
    dbms_output.put_line('');
    dbms_output.put_line('*******************************************');
    dbms_output.put_line('*** ERROR ENCOUNTERED');
    dbms_output.put_line('*** SQLCODE: ' || sqlcode );
    dbms_output.put_line('*******************************************');
    raise_application_error(-20102, sqlerrm);

end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16903, 0, 2018, 2, 1, 0, 53367, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.1.0_maint_053367_lessee_02_add_remeasure_incentive_backfill_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
