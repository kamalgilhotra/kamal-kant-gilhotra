 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046196_01_prov_create_tbbs_tables_views_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 09/02/2016 Jared Watkins  Harvest into base; drop all the old TBBS tables
 ||											and create the new versions of them
 ||============================================================================
 */

---------------------
--drop the old tables
---------------------
DECLARE
  CURSOR tbls IS (SELECT TABLE_NAME
  	      	 FROM all_tables
  	      	 WHERE LOWER(owner) = 'pwrplant'
		 AND (LOWER(TABLE_NAME) LIKE 'tbbs%'
		       OR LOWER(TABLE_NAME) = 'pp_tree_improved')
		 AND TABLE_NAME NOT IN (SELECT mview_name FROM all_mviews));
  CURSOR seqs IS (SELECT sequence_name
  	      	 FROM all_sequences
		 WHERE LOWER(sequence_owner) = 'pwrplant'
		 AND (LOWER(sequence_name) LIKE 'tbbs%'
		      OR LOWER(sequence_name) = 'pp_tree_improved_seq'));
  CURSOR mviews IS (SELECT mview_name
  	 	    FROM all_mviews
		    WHERE LOWER(owner) = 'pwrplant'
		    AND LOWER(mview_name) LIKE '%cr%');
  CURSOR idxs IS (SELECT index_name FROM all_indexes
  	      	  WHERE lower(owner) = 'pwrplant'
		  AND lower(index_name) LIKE '%tbbs%'
		  OR lower(index_name) LIKE 'pp%tree%improv%');
BEGIN
  FOR x IN tbls LOOP
    EXECUTE IMMEDIATE 'drop table ' || x.TABLE_NAME || ' cascade constraints';
  END LOOP;

  FOR x IN seqs LOOP
    EXECUTE IMMEDIATE 'drop sequence ' || x.sequence_name;
  END LOOP;

  FOR x IN mviews LOOP
    EXECUTE IMMEDIATE 'drop materialized view ' || x.mview_name;
  END LOOP;

  FOR x IN idxs LOOP
    EXECUTE IMMEDIATE 'drop index ' || x.index_name;
  END LOOP;

END;
/
DECLARE
  cnt NUMBER;
BEGIN
  SELECT COUNT(1) INTO cnt
  FROM all_mview_logs
  WHERE LOWER(MASTER) = 'cr_balances';

  IF cnt > 0 THEN
     EXECUTE IMMEDIATE 'drop materialized view log on cr_balances';
  END IF;
END;
/

--------------------------------
--create the necessary sequences
--------------------------------
CREATE SEQUENCE  TBBS_ACCOUNT_FIELD_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_BOOK_ACCOUNT_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_BOOK_ADJUST_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_BOOK_RECLASS_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_DOCUMENT_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_LINE_ITEM_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_SCHEMA_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_TAX_ADJUST_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_TAX_RECLASS_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
CREATE SEQUENCE  TBBS_TREATMENT_TYPE_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 101 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE  PP_TREE_IMPROVED_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

----------------------------
--add all the tables/columns
----------------------------
DECLARE
	cnt NUMBER;
BEGIN
	SELECT Count(1) INTO cnt
	FROM all_tab_cols
	WHERE LOWER(column_name) = 'tbbs_month'
	AND LOWER(table_name) = 'tax_accrual_gl_month_def';

	IF cnt = 0 THEN
		EXECUTE IMMEDIATE 'ALTER TABLE tax_accrual_gl_month_def ADD tbbs_month NUMBER(22,0) NULL';
	END IF;
END;
/
comment on column tax_accrual_gl_month_def.tbbs_month is 'The GL Month Number associated with this Provision month used when running the Tax Basis Balance Sheet report';

CREATE TABLE TBBS_ACCOUNT_FIELD
(	ACCOUNT_FIELD_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
ACCOUNT_FIELD VARCHAR2(35),
ABBREVIATION VARCHAR2(10),
SORT_ORDER NUMBER(22,0)
) ;

COMMENT ON COLUMN TBBS_ACCOUNT_FIELD.ACCOUNT_FIELD_ID IS 'An identifier for the additional account field.';
COMMENT ON COLUMN TBBS_ACCOUNT_FIELD.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_ACCOUNT_FIELD.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_ACCOUNT_FIELD.ACCOUNT_FIELD IS 'A string of the column name of the field. Should match a column name from the balances table or dynamic query.';
COMMENT ON COLUMN TBBS_ACCOUNT_FIELD.SORT_ORDER IS 'A number indicating the order to concatenate the additional fields.';
COMMENT ON TABLE TBBS_ACCOUNT_FIELD  IS '(O)  [08] The TBBS Account Fields table allows users to define additional accounting key fields that make up a book account for TBBS mapping purposes.';

CREATE TABLE TBBS_ACCOUNT_FIELD_ASSIGN
(	PRIMARY_ACCOUNT VARCHAR2(50) NOT NULL,
ACCOUNT_FIELD_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18)
) ;

COMMENT ON COLUMN TBBS_ACCOUNT_FIELD_ASSIGN.PRIMARY_ACCOUNT IS 'The primary account value based on definition in tbbs_system_control';
COMMENT ON COLUMN TBBS_ACCOUNT_FIELD_ASSIGN.ACCOUNT_FIELD_ID IS 'The identifier for the account field from tbbs_account_field. The values of this field will be concatenated to the primary account to generate a book account.';
COMMENT ON COLUMN TBBS_ACCOUNT_FIELD_ASSIGN.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_ACCOUNT_FIELD_ASSIGN.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON TABLE TBBS_ACCOUNT_FIELD_ASSIGN  IS '(O)  [08] The TBBS Account Field Assign table allows users to assign the additional account fields to primary account elements so the system can build out the list of final book accounts';

CREATE TABLE TBBS_ACCOUNT_STG_DEBUG
(	PRIMARY_ACCOUNT VARCHAR2(254),
ACCOUNT_FIELD VARCHAR2(30),
FIELD_VALUE VARCHAR2(254),
GRP NUMBER(22,0),
PRIMARY_DESCRIPTION VARCHAR2(254)
) ;

COMMENT ON COLUMN TBBS_ACCOUNT_STG_DEBUG.PRIMARY_ACCOUNT IS 'The base account that is broken out by additional fields';
COMMENT ON COLUMN TBBS_ACCOUNT_STG_DEBUG.ACCOUNT_FIELD IS 'The Account field associated with the primary account from tbbs_account_field';
COMMENT ON COLUMN TBBS_ACCOUNT_STG_DEBUG.FIELD_VALUE IS 'A value of the additional field from the balance table.';
COMMENT ON COLUMN TBBS_ACCOUNT_STG_DEBUG.GRP IS 'Group identifier for a set of values.';
COMMENT ON COLUMN TBBS_ACCOUNT_STG_DEBUG.PRIMARY_DESCRIPTION IS 'Description of the primary account.';
COMMENT ON TABLE TBBS_ACCOUNT_STG_DEBUG  IS '(O)  [08] Table that stores stores the data from the tbbs_account_stg_tmp table so that the data may be copied from one database session to another.  The f_tax_accrual_report_debug database function is used to insert rows into this table and to insert rows from this table back into the tax_accrual_rep_criteria_tmp in another session.  This function and table are useful when attempting to debug a report in a PowerBuilder datawindow using data that was loaded into the reporting temporary tables through the application';

CREATE GLOBAL TEMPORARY TABLE TBBS_ACCOUNT_STG_TMP
(	PRIMARY_ACCOUNT VARCHAR2(254),
ACCOUNT_FIELD VARCHAR2(30),
FIELD_VALUE VARCHAR2(254),
GRP NUMBER(22,0),
PRIMARY_DESCRIPTION VARCHAR2(254)
) ON COMMIT DELETE ROWS ;

COMMENT ON COLUMN TBBS_ACCOUNT_STG_TMP.PRIMARY_ACCOUNT IS 'The base account that is broken out by additional fields';
COMMENT ON COLUMN TBBS_ACCOUNT_STG_TMP.ACCOUNT_FIELD IS 'The Account field associated with the primary account from tbbs_account_field';
COMMENT ON COLUMN TBBS_ACCOUNT_STG_TMP.FIELD_VALUE IS 'A value of the additional field from the balance table.';
COMMENT ON COLUMN TBBS_ACCOUNT_STG_TMP.GRP IS 'Group identifier for a set of values.';
COMMENT ON COLUMN TBBS_ACCOUNT_STG_TMP.PRIMARY_DESCRIPTION IS 'Description of the primary account.';
COMMENT ON TABLE TBBS_ACCOUNT_STG_TMP  IS '(T)  [08] The TBBS Account STG TMP table is a global temporary table used to build out the concatenated account values used by the TBBS.';

CREATE TABLE TBBS_BOOK_ACCOUNT
(	BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
DESCRIPTION VARCHAR2(254),
PRIMARY_ACCOUNT VARCHAR2(254),
PRIMARY_DESCRIPTION VARCHAR2(254),
ADDITIONAL_FIELD_DESCRIPTION VARCHAR2(254)
) ;

COMMENT ON COLUMN TBBS_BOOK_ACCOUNT.BOOK_ACCOUNT_ID IS 'A system-generated identifier of the combination of account fields in tbbs_book_account_value. Defined by the fields set in tbbs_account_field and tbbs_account_field_assign.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT.TIME_STAMP IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT.USER_ID IS 'System-generated user_id used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT.DESCRIPTION IS 'The concatenated book account available for mapping. This is the value displayed to users that defines a single account. Defined by the fields set in tbbs_account_field and tbbs_account_field_secondary.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT.PRIMARY_ACCOUNT IS 'The primary account value from tbbs_field_assign.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT.PRIMARY_DESCRIPTION IS 'The description for the primary account. This will be the account description shown on the report.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT.ADDITIONAL_FIELD_DESCRIPTION IS 'Description details for any additional fields that are part of this book_account_id';
COMMENT ON TABLE TBBS_BOOK_ACCOUNT  IS '(C)  [08] The TBBS Book Account table stores each book account available for mapping in the TBBS. These accounts are defined by the fields set in tbbs_account_field and tbbs_account_field_assign.';

CREATE TABLE TBBS_BOOK_ACCOUNT_VALUE
(	BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
ACCOUNT_FIELD_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
FIELD_VALUE VARCHAR2(254) NOT NULL
) ;

COMMENT ON COLUMN TBBS_BOOK_ACCOUNT_VALUE.BOOK_ACCOUNT_ID IS 'A system-generated identifier of the combination of account fields in tbbs_book_account_value. Defined by the fields set in tbbs_account_field and tbbs_account_field_assign.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT_VALUE.ACCOUNT_FIELD_ID IS 'The account field ID to be broken out from tbbs_account_fields';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT_VALUE.TIME_STAMP IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT_VALUE.USER_ID IS 'System-generated user_id used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ACCOUNT_VALUE.FIELD_VALUE IS 'The field value to be concatenated to the primary account';
COMMENT ON TABLE TBBS_BOOK_ACCOUNT_VALUE  IS '(C)  [08] The TBBS Book Account Value table stores each book account available for mapping in the TBBS. These accounts are defined by the fields set in tbbs_account_field and tbbs_account_field_assign.';

CREATE TABLE TBBS_BOOK_ADJUST
(	BOOK_ADJUST_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
GL_COMPANY VARCHAR2(50) NOT NULL,
EFFECTIVE_MONTH NUMBER(22,0) NOT NULL,
DR_BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
CR_BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
AMOUNT NUMBER(22,2),
NOTE VARCHAR2(70)
) ;

COMMENT ON COLUMN TBBS_BOOK_ADJUST.BOOK_ADJUST_ID IS 'The ID of the manual book adjustment. Each adjustment for a company, month, dr and cr account has a separate ID';
COMMENT ON COLUMN TBBS_BOOK_ADJUST.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ADJUST.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ADJUST.GL_COMPANY IS 'The identifier of the company on the GL.';
COMMENT ON COLUMN TBBS_BOOK_ADJUST.EFFECTIVE_MONTH IS 'A number in YYYYMM format that indicates the first month the adjustment is active.';
COMMENT ON COLUMN TBBS_BOOK_ADJUST.DR_BOOK_ACCOUNT_ID IS 'The book account that will be debited for the adjustment amount.';
COMMENT ON COLUMN TBBS_BOOK_ADJUST.CR_BOOK_ACCOUNT_ID IS 'The book account that will be credited for the adjustment amount.';
COMMENT ON COLUMN TBBS_BOOK_ADJUST.AMOUNT IS 'Amount of the manual adjustment. This number should always be positive.';
COMMENT ON COLUMN TBBS_BOOK_ADJUST.NOTE IS 'A description where a user can write a note regarding the adjustment.';
COMMENT ON TABLE TBBS_BOOK_ADJUST  IS '(O)  [08] The TBBS Book Adjust table allows users to make manual adjustments to book acccount balances in the TBBS by company as of an effective month.';

CREATE TABLE TBBS_BOOK_ADJUST_DOC_RELATE
(	BOOK_ADJUST_ID NUMBER(22,0) NOT NULL,
DOCUMENT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18)
) ;

COMMENT ON COLUMN TBBS_BOOK_ADJUST_DOC_RELATE.BOOK_ADJUST_ID IS 'The ID of the book adjustment';
COMMENT ON COLUMN TBBS_BOOK_ADJUST_DOC_RELATE.DOCUMENT_ID IS 'The ID of the attached document from tbbs_document';
COMMENT ON COLUMN TBBS_BOOK_ADJUST_DOC_RELATE.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ADJUST_DOC_RELATE.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON TABLE TBBS_BOOK_ADJUST_DOC_RELATE  IS '(O)  [08] The TBBS Book Adjust Doc Relate table associates uploaded documents with manual book adjustments.';

CREATE TABLE TBBS_BOOK_ASSIGN
(	SCHEMA_ID NUMBER(22,0) NOT NULL,
BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
LINE_ITEM_ID NUMBER(22,0)
) ;

COMMENT ON COLUMN TBBS_BOOK_ASSIGN.SCHEMA_ID IS 'The Tax Basis Balance Sheet Schema associated with the book account to line item mapping.';
COMMENT ON COLUMN TBBS_BOOK_ASSIGN.BOOK_ACCOUNT_ID IS 'A system-generated identifier of the combination of account fields in tbbs_book_account_value. Defined by the fields set in tbbs_account_field and tbbs_account_field_assign.';
COMMENT ON COLUMN TBBS_BOOK_ASSIGN.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ASSIGN.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_ASSIGN.LINE_ITEM_ID IS 'A system assigned identifier to represent a tax basis balance sheet line item.';
COMMENT ON TABLE TBBS_BOOK_ASSIGN  IS '(S)  [08] The TBBS Book Assign table associates an account with a tax basis balance sheet line item by schema.';

CREATE TABLE TBBS_BOOK_BALANCE
(	GL_COMPANY VARCHAR2(50) NOT NULL,
MONTH_NUMBER NUMBER(22,0) NOT NULL,
BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
AMOUNT NUMBER(22,2) DEFAULT 0
) ;

COMMENT ON COLUMN TBBS_BOOK_BALANCE.GL_COMPANY IS 'The company identifier for balance records. Should match the GL_COMPANY_NO field in company setup.';
COMMENT ON COLUMN TBBS_BOOK_BALANCE.MONTH_NUMBER IS 'The month of the associated balance. Formatted YYYYMM.';
COMMENT ON COLUMN TBBS_BOOK_BALANCE.BOOK_ACCOUNT_ID IS 'The ID of the book account from tbbs_book_account.';
COMMENT ON COLUMN TBBS_BOOK_BALANCE.TIME_STAMP IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_BALANCE.USER_ID IS 'System-generated user_id used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_BALANCE.AMOUNT IS 'The balance for the associated gl_company, month and book_account_id.';
COMMENT ON TABLE TBBS_BOOK_BALANCE  IS '(C)  [08] The TBBS Book balances table summarizes the balance data from the CR Balances tables based on the tbbs account definition for use in creating the TBBS.';

CREATE TABLE TBBS_BOOK_RECLASS
(	BOOK_RECLASS_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
GL_COMPANY VARCHAR2(50) NOT NULL,
EFFECTIVE_MONTH NUMBER(22,0) NOT NULL,
SRC_BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
DST_BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
RECLASS_ACTIVE_IND NUMBER(22,0) DEFAULT 0,
NOTE VARCHAR2(70)
) ;

COMMENT ON COLUMN TBBS_BOOK_RECLASS.BOOK_RECLASS_ID IS 'The ID of the book reclass adjustment. Each reclass for a company, month and source account has a separate ID';
COMMENT ON COLUMN TBBS_BOOK_RECLASS.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_RECLASS.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_RECLASS.GL_COMPANY IS 'The identifier of the company on the GL.';
COMMENT ON COLUMN TBBS_BOOK_RECLASS.EFFECTIVE_MONTH IS 'A number in YYYYMM format that indicates the first month the reclass is active.';
COMMENT ON COLUMN TBBS_BOOK_RECLASS.SRC_BOOK_ACCOUNT_ID IS 'The source book account that will be zeroed out as part of the reclass.';
COMMENT ON COLUMN TBBS_BOOK_RECLASS.DST_BOOK_ACCOUNT_ID IS 'The destination book account that the source account balance will be transferred to';
COMMENT ON COLUMN TBBS_BOOK_RECLASS.RECLASS_ACTIVE_IND IS '1/0 toggle that idicates if the account reclass is active or inactive starting in the effective month.';
COMMENT ON COLUMN TBBS_BOOK_RECLASS.NOTE IS 'A description where a user can write a note regarding the reclass.';
COMMENT ON TABLE TBBS_BOOK_RECLASS  IS '(O)  [08] The TBBS Book Reclass table allows users to reclass entire book acccount balances in the TBBS by company as of an effective month.';

CREATE TABLE TBBS_BOOK_RECLASS_DOC_RELATE
(	BOOK_RECLASS_ID NUMBER(22,0) NOT NULL,
DOCUMENT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18)
) ;

COMMENT ON COLUMN TBBS_BOOK_RECLASS_DOC_RELATE.BOOK_RECLASS_ID IS 'The ID of the book reclass adjustment';
COMMENT ON COLUMN TBBS_BOOK_RECLASS_DOC_RELATE.DOCUMENT_ID IS 'The ID of the attached document from tbbs_document';
COMMENT ON COLUMN TBBS_BOOK_RECLASS_DOC_RELATE.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_BOOK_RECLASS_DOC_RELATE.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON TABLE TBBS_BOOK_RECLASS_DOC_RELATE  IS '(O)  [08] The TBBS Book Reclass Doc Relate table associates uploaded documents with book reclass adjustments.';

CREATE TABLE TBBS_COLUMN_CLOSE
(	COLUMN_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
COLUMN_NAME VARCHAR2(30) NOT NULL,
DESCRIPTION VARCHAR2(50),
LINE_ITEM_ID NUMBER(22,0)
) ;

COMMENT ON COLUMN TBBS_COLUMN_CLOSE.COLUMN_ID IS 'The ID of the column. Also used to order the columns in the same way they appear on the TBBS report.';
COMMENT ON COLUMN TBBS_COLUMN_CLOSE.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_COLUMN_CLOSE.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_COLUMN_CLOSE.COLUMN_NAME IS 'The name of the column in tbbs_report_stg_tmp and tbbs_report_data_tmp';
COMMENT ON COLUMN TBBS_COLUMN_CLOSE.DESCRIPTION IS 'A Header name of the column that appears on the TBBS report.';
COMMENT ON COLUMN TBBS_COLUMN_CLOSE.LINE_ITEM_ID IS 'The TBBS Line Item mapped for this closing adjustment';
COMMENT ON TABLE TBBS_COLUMN_CLOSE  IS '(F)  [08] The TBBS Column Close table table allows users to specify line items that can be used to reverse the remaining balance of a column.';

/* JAW */
CREATE TABLE TBBS_COMPANY_ASSIGN
(	COMPANY_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
GL_COMPANY VARCHAR2(50)
) ;

COMMENT ON COLUMN TBBS_COMPANY_ASSIGN.COMPANY_ID IS 'The company identifier. A company can have a single GL company mapped to it.';
COMMENT ON COLUMN TBBS_COMPANY_ASSIGN.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_COMPANY_ASSIGN.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_COMPANY_ASSIGN.GL_COMPANY IS 'The identifier of a company on the book balances table mapped to this company ID';
COMMENT ON TABLE TBBS_COMPANY_ASSIGN  IS '(S)  [08] The TBBS Company Assign table contains the company ID to GL Company assignments.';

CREATE TABLE TBBS_DOCUMENT
(	DOCUMENT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
DOCUMENT_DATA BLOB,
NOTES VARCHAR2(2000),
DESCRIPTION VARCHAR2(35),
FILE_NAME VARCHAR2(100) NOT NULL,
FILESIZE NUMBER(22,0),
FILE_LINK VARCHAR2(2000),
READ_ONLY NUMBER(1,0)
) ;

COMMENT ON COLUMN TBBS_DOCUMENT.DOCUMENT_ID IS 'The ID of the attached document';
COMMENT ON COLUMN TBBS_DOCUMENT.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_DOCUMENT.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_DOCUMENT.DOCUMENT_DATA IS 'The stored document';
COMMENT ON COLUMN TBBS_DOCUMENT.NOTES IS 'A freeform field for entering notes';
COMMENT ON COLUMN TBBS_DOCUMENT.DESCRIPTION IS 'Description of the attached document';
COMMENT ON COLUMN TBBS_DOCUMENT.FILE_NAME IS 'File name of the attached document';
COMMENT ON COLUMN TBBS_DOCUMENT.FILESIZE IS 'Size of the attached document';
COMMENT ON COLUMN TBBS_DOCUMENT.FILE_LINK IS 'Link to an external file';
COMMENT ON COLUMN TBBS_DOCUMENT.READ_ONLY IS 'Indicator showing if the attached file is read only. 1= read only, 0 = editable';
COMMENT ON TABLE TBBS_DOCUMENT  IS '(O)  [08] The TBBS Document table allows users to upload documents to associate with manual adjustments.';

CREATE TABLE TBBS_GL_COMPANY
(	GL_COMPANY VARCHAR2(50) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
DESCRIPTION VARCHAR2(50)
) ;

COMMENT ON COLUMN TBBS_GL_COMPANY.GL_COMPANY IS 'The identifier of a company on the book balances table';
COMMENT ON COLUMN TBBS_GL_COMPANY.TIME_STAMP IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN TBBS_GL_COMPANY.USER_ID IS 'System-generated user_id used for audit purposes.';
COMMENT ON COLUMN TBBS_GL_COMPANY.DESCRIPTION IS 'A description of the company on the book balances table.';
COMMENT ON TABLE TBBS_GL_COMPANY  IS '(S)  [08] The TBBS GL Company table contains the list of ledger companies that contain book balances for the TBBS';

CREATE TABLE TBBS_LINE_ITEM
(	LINE_ITEM_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
DESCRIPTION VARCHAR2(50),
TREATMENT_TYPE_ID NUMBER(22,0) NOT NULL,
COLLAPSE_DETAIL_IND NUMBER(1,0) DEFAULT 0
) ;

COMMENT ON COLUMN TBBS_LINE_ITEM.LINE_ITEM_ID IS 'A system-assigned identifier to represent an item on the Tax Basis Balance Sheet.';
COMMENT ON COLUMN TBBS_LINE_ITEM.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_LINE_ITEM.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_LINE_ITEM.DESCRIPTION IS 'The description of this line item.';
COMMENT ON COLUMN TBBS_LINE_ITEM.TREATMENT_TYPE_ID IS 'Identifier that defines how the TBBS treats this line item.';
COMMENT ON COLUMN TBBS_LINE_ITEM.COLLAPSE_DETAIL_IND IS 'Indicator that sets the TBBS report to collapse all Book, Tax and adjustment detail records for this line item into one detail row. 1: Collapse Detail, 0: Show detail';
COMMENT ON TABLE TBBS_LINE_ITEM  IS '(S)  [08] The TBBS Line Item table defines the lines of the Tax Basis Balance Sheet to which book accounts and Schedule M Item''s are assigned. Each line item is assigned to a treatment type.';

CREATE TABLE TBBS_LINE_ITEM_TREE_RELATE
(	LINE_ITEM_ID NUMBER(22,0) NOT NULL,
NODE_ID NUMBER(22,0) NOT NULL,
PP_TREE_TYPE_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18)
) ;

COMMENT ON COLUMN TBBS_LINE_ITEM_TREE_RELATE.LINE_ITEM_ID is 'Identifier designating the Line Item represetned by a a tree node';
COMMENT ON COLUMN TBBS_LINE_ITEM_TREE_RELATE.NODE_ID is 'Identifier designating the tree node to which to relate the line item';
COMMENT ON COLUMN TBBS_LINE_ITEM_TREE_RELATE.PP_TREE_TYPE_ID is 'Identifier designating the PP Tree Type for which this relation is valid';
COMMENT ON COLUMN TBBS_LINE_ITEM_TREE_RELATE.TIME_STAMP is 'System-generated time stamp used for audit purposes';
COMMENT ON COLUMN TBBS_LINE_ITEM_TREE_RELATE.USER_ID is 'System-generated user_id used for audit purposes';
COMMENT ON TABLE TBBS_LINE_ITEM_TREE_RELATE IS '(S) [08] The TBBS Line Item Tree Relate table is used to relate Line Items (from TBBS_LINE_ITEM) to Tree Nodes (from PP_TREE_IMPROVED)';

CREATE TABLE TBBS_LINE_TYPE
(	LINE_TYPE_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
LINE_TYPE_IND VARCHAR2(30),
LINE_TYPE_ORDER NUMBER(22,0),
DESCRIPTION VARCHAR2(80)
) ;

COMMENT ON COLUMN TBBS_LINE_TYPE.LINE_TYPE_ID IS 'A system-assigned identifier to represent a Tax Basis Balance Sheet Line Type.';
COMMENT ON COLUMN TBBS_LINE_TYPE.TIME_STAMP IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN TBBS_LINE_TYPE.USER_ID IS 'System-generated user_id used for audit purposes.';
COMMENT ON COLUMN TBBS_LINE_TYPE.LINE_TYPE_IND IS 'The name of this line type, appearing on the report.';
COMMENT ON COLUMN TBBS_LINE_TYPE.LINE_TYPE_ORDER IS 'The order that this line type should be sorted in under its parent.';
COMMENT ON COLUMN TBBS_LINE_TYPE.DESCRIPTION IS 'A description of this line type.';
COMMENT ON TABLE TBBS_LINE_TYPE  IS '(F)  [08] The TBBS Line Type table contains the list of different line types that will appear on the tax basis balance sheet report. These Line types are defined by PowerPlan.';

CREATE TABLE TBBS_REPORT_DATA_DEBUG
(	LINE_ID NUMBER(22,0) NOT NULL,
PARENT_LINE_ID NUMBER(22,0),
TREE_ORDER NUMBER(22,0),
LINE_TYPE_ORDER NUMBER(22,0),
GL_MONTH NUMBER(22,2),
TBBS_MONTH NUMBER(22,0),
LINE_TYPE_ID NUMBER(22,0),
LINE_TYPE_IND VARCHAR2(30),
TREATMENT_TYPE_ID NUMBER(22,0),
LINE_DESCRIPTION VARCHAR2(100),
BOOK_ACCOUNT_ID NUMBER(22,0),
BOOK_AMOUNT NUMBER(22,2) DEFAULT 0,
BOOK_RC NUMBER(22,2) DEFAULT 0,
BOOK_ADJ NUMBER(22,2) DEFAULT 0,
BOOK_ELIM NUMBER(22,2) DEFAULT 0,
ADJUSTED_BOOK_BASIS NUMBER(22,2) DEFAULT 0,
M_ITEM_ID NUMBER(22,0),
ENTITY_ID NUMBER(22,0),
M_BEG_BAL_PY NUMBER(22,2) DEFAULT 0,
M_ACTIVITY_PY NUMBER(22,2) DEFAULT 0,
M_ADJUST_PY NUMBER(22,2) DEFAULT 0,
M_VARIANCE_PY NUMBER(22,2) DEFAULT 0,
M_BEG_BAL_CY NUMBER(22,2) DEFAULT 0,
M_ACTIVITY_CY NUMBER(22,2) DEFAULT 0,
M_ADJUST_CY NUMBER(22,2) DEFAULT 0,
M_RTA_CY NUMBER(22,2) DEFAULT 0,
M_VARIANCE_CY NUMBER(22,2) DEFAULT 0,
M_LTD_BAL NUMBER(22,2) DEFAULT 0,
M_M13_PY NUMBER(22,2) DEFAULT 0,
M_UNBOOKED_RTA NUMBER(22,2) DEFAULT 0,
TAX_M_RC NUMBER(22,2) DEFAULT 0,
TAX_M_ADJ NUMBER(22,2) DEFAULT 0,
TAX_M_ELIM NUMBER(22,2) DEFAULT 0,
ADJUSTED_M_LTD_BAL NUMBER(22,2) DEFAULT 0,
CALC_TAX_BASIS NUMBER(22,2) DEFAULT 0,
M_DEF_TAX_BAL NUMBER(22,2) DEFAULT 0,
M_DEF_TAX_BAL_REG NUMBER(22,2) DEFAULT 0,
TAX_DEF_RC NUMBER(22,2) DEFAULT 0,
ADJUSTED_DEF_TAX_TOT NUMBER(22,2) DEFAULT 0,
CALC_DEF_RATE NUMBER(22,8) DEFAULT 0,
LINE_ITEM_DESCRIPTION VARCHAR2(50),
TREE_LEVEL NUMBER(22,0),
EMPTY_NODE NUMBER(22,0),
DETAIL_IND NUMBER(1,0),
TREE_PARENT_ID NUMBER(22,0),
TREE_CHILD_ID NUMBER(22,0),
TREE_PATH VARCHAR2(200)
) ;

COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.LINE_ID IS 'Line ID for the TBBS report';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.PARENT_LINE_ID IS 'ID for the parent line record in the TBBS report';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TREE_ORDER IS 'The order of the tree record (sort_order column from PP_TREE)';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.LINE_TYPE_ORDER IS 'Sort order for the Line Types';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.GL_MONTH IS 'The GL Month for the Provision tax data';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TBBS_MONTH IS 'The month_number (YYYYMM) associated with the Provision GL Month';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.LINE_TYPE_ID IS 'ID of the tbbs line type, indicating whether the line is a Book detail, Tax detail, etc. record';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.LINE_TYPE_IND IS 'Indicator showing whether the line is a Book, Tax or Header record';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TREATMENT_TYPE_ID IS 'The Treatment Type ID associated with the Line Item';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.LINE_DESCRIPTION IS 'The description for the the line ID of the TBBS. May be a Tree node, Line item header or individual Book account description / M Item description';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.BOOK_ACCOUNT_ID IS 'The Book_account_id associated with an account combination from tbbs_book_account';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.BOOK_AMOUNT IS 'The balance of the book_account_id';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.BOOK_RC IS 'The book account reclass amount total for the line';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.BOOK_ADJ IS 'The manual book adjustment amount total ';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.BOOK_ELIM IS 'Elimination column, used by treatment type logic to eliminate amounts from adjusted book basis.';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.ADJUSTED_BOOK_BASIS IS 'The total adjusted book balance: (book_amount + book_rc + book_adj + book_elim). Will appear on detail book records, line items and summary nodes.';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_ITEM_ID IS 'The M Item ID from Provision';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.ENTITY_ID IS 'The Entity ID from Provision';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_BEG_BAL_PY IS 'The M Item beginning balance in the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_ACTIVITY_PY IS 'The M Item current activity in the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_ADJUST_PY IS 'The M Item adjustment activity in the Prior year case. Also includes the RTA months in the Prior year case.';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_VARIANCE_PY IS 'Variance column, used to hold any difference between the m_beg_bal_cy column and (m_beg_bal_py + m_activity_py + m_adjust_py)';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_BEG_BAL_CY IS 'The M Item beginning balance in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_ACTIVITY_CY IS 'The M Item current activity in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_ADJUST_CY IS 'The M Item adjustment activity in the Current year case.';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_RTA_CY IS 'The RTA month M Item activity in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_VARIANCE_CY IS 'A Variance column used to hold any difference between the m_ltd_bal column and (m_beg_bal_cy + m_activity_cy + m_adjust_cy + m_rta_cy)';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_LTD_BAL IS 'The M Item ending balance in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_M13_PY IS 'The Period 13 activity in the Prior year case, used to compare with the RTA months in the current year case.';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_UNBOOKED_RTA IS 'RTA Activity that exists in Month 13 of the Prior Year case that has not yet been pulled into the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TAX_M_RC IS 'The M item Gross reclass total for the line';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TAX_M_ADJ IS 'The Manual M Item adjustment total on a line item';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TAX_M_ELIM IS 'Elimination column, used by treatment type logic to eliminate amounts from the adjusted timing difference balance.';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.ADJUSTED_M_LTD_BAL IS 'The total adjusted M Item balance: (m_ltd_bal + m_unbooked_rta (based on a system control toggle) + tax_m_rc + tax_m_adj + tax_m_elim). Will appear on detail tax records, line items and summary nodes.';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.CALC_TAX_BASIS IS 'The calculated tax basis for the line item: adjusted_book_basis + adjusted_m_ltd_bal. Will only appear on line items and summary nodes. Will be null on detail book and tax records.';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_DEF_TAX_BAL IS 'The ending total deferred taxes on the M item = accum_reg_dit_end_cy + fas109_increment_end_cy + entity_grossup_end_cy';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.M_DEF_TAX_BAL_REG IS 'The ending reg asset amount on the M item: if reg_ind = 1, then fas109_increment_end_cy + entity_grossup_end_cy. Else 0';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TAX_DEF_RC IS 'The M item deferred tax reclass total for the line';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.ADJUSTED_DEF_TAX_TOT IS 'The adjusted Deferred Tax Balance for the line. = (m_def_tax_bal + m_def_tax_bal_reg + tax_def_rc)';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.CALC_DEF_RATE IS 'The calculated deferred tax rate for the line. = adjusted_def_tax_tot / adjusted_m_ltd_bal';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.LINE_ITEM_DESCRIPTION IS 'Description of the Line Item for each book and tax detail record';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TREE_LEVEL IS 'The level of the line within the tree structure';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.EMPTY_NODE IS 'Indicator whether the line is associated with an empty tree node';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.DETAIL_IND IS 'Indicator showing if the TBBS Line is a detail or summary record. 1 = detail, 0 = summary';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TREE_PARENT_ID IS 'The ID of the parent record within the PP_TREE structure. Should always be a PP Tree empty node (to contain other nodes / line items) or a TBBS Line item (to contain line item detail records).';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TREE_CHILD_ID IS 'The ID of the child record within the PP_TREE structure. Can be a PP Tree empty node, line item, or Line item detail record (negative value dumbkey)';
COMMENT ON COLUMN TBBS_REPORT_DATA_DEBUG.TREE_PATH IS 'The root tree path for the TBBS Line ID';
COMMENT ON TABLE TBBS_REPORT_DATA_DEBUG  IS '(O)  [08] Table that stores stores the data from the tbbs_report_data_tmp table so that the data may be copied from one database session to another.  The f_tax_accrual_report_debug database function is used to insert rows into this table and to insert rows from this table back into the tax_accrual_rep_criteria_tmp in another session.  This function and table are useful when attempting to debug a report in a PowerBuilder datawindow using data that was loaded into the reporting temporary tables through the application';

CREATE GLOBAL TEMPORARY TABLE TBBS_REPORT_DATA_TMP
(	LINE_ID NUMBER(22,0) NOT NULL,
PARENT_LINE_ID NUMBER(22,0),
TREE_ORDER NUMBER(22,0),
LINE_TYPE_ORDER NUMBER(22,0),
GL_MONTH NUMBER(22,2),
TBBS_MONTH NUMBER(22,0),
LINE_TYPE_ID NUMBER(22,0),
LINE_TYPE_IND VARCHAR2(30),
TREATMENT_TYPE_ID NUMBER(22,0),
LINE_DESCRIPTION VARCHAR2(100),
BOOK_ACCOUNT_ID NUMBER(22,0),
BOOK_AMOUNT NUMBER(22,2) DEFAULT 0,
BOOK_RC NUMBER(22,2) DEFAULT 0,
BOOK_ADJ NUMBER(22,2) DEFAULT 0,
BOOK_ELIM NUMBER(22,2) DEFAULT 0,
ADJUSTED_BOOK_BASIS NUMBER(22,2) DEFAULT 0,
M_ITEM_ID NUMBER(22,0),
ENTITY_ID NUMBER(22,0),
M_BEG_BAL_PY NUMBER(22,2) DEFAULT 0,
M_ACTIVITY_PY NUMBER(22,2) DEFAULT 0,
M_ADJUST_PY NUMBER(22,2) DEFAULT 0,
M_VARIANCE_PY NUMBER(22,2) DEFAULT 0,
M_BEG_BAL_CY NUMBER(22,2) DEFAULT 0,
M_ACTIVITY_CY NUMBER(22,2) DEFAULT 0,
M_ADJUST_CY NUMBER(22,2) DEFAULT 0,
M_RTA_CY NUMBER(22,2) DEFAULT 0,
M_VARIANCE_CY NUMBER(22,2) DEFAULT 0,
M_LTD_BAL NUMBER(22,2) DEFAULT 0,
M_M13_PY NUMBER(22,2) DEFAULT 0,
M_UNBOOKED_RTA NUMBER(22,2) DEFAULT 0,
TAX_M_RC NUMBER(22,2) DEFAULT 0,
TAX_M_ADJ NUMBER(22,2) DEFAULT 0,
TAX_M_ELIM NUMBER(22,2) DEFAULT 0,
ADJUSTED_M_LTD_BAL NUMBER(22,2) DEFAULT 0,
CALC_TAX_BASIS NUMBER(22,2) DEFAULT 0,
M_DEF_TAX_BAL NUMBER(22,2) DEFAULT 0,
M_DEF_TAX_BAL_REG NUMBER(22,2) DEFAULT 0,
TAX_DEF_RC NUMBER(22,2) DEFAULT 0,
ADJUSTED_DEF_TAX_TOT NUMBER(22,2) DEFAULT 0,
CALC_DEF_RATE NUMBER(22,8) DEFAULT 0,
LINE_ITEM_DESCRIPTION VARCHAR2(50),
TREE_LEVEL NUMBER(22,0),
EMPTY_NODE NUMBER(22,0),
DETAIL_IND NUMBER(1,0),
TREE_PARENT_ID NUMBER(22,0),
TREE_CHILD_ID NUMBER(22,0),
TREE_PATH VARCHAR2(200)
) ON COMMIT PRESERVE ROWS ;

COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.LINE_ID IS 'Line ID for the TBBS report';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.PARENT_LINE_ID IS 'ID for the parent line record in the TBBS report';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TREE_ORDER IS 'The order of the tree record (sort_order column from PP_TREE)';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.LINE_TYPE_ORDER IS 'Sort order for the Line Types';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.GL_MONTH IS 'The GL Month for the Provision tax data';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TBBS_MONTH IS 'The month_number (YYYYMM) associated with the Provision GL Month';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.LINE_TYPE_ID IS 'ID of the tbbs line type, indicating whether the line is a Book detail, Tax detail, etc. record';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.LINE_TYPE_IND IS 'Indicator showing whether the line is a Book, Tax or Header record';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TREATMENT_TYPE_ID IS 'The Treatment Type ID associated with the Line Item';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.LINE_DESCRIPTION IS 'The description for the the line ID of the TBBS. May be a Tree node, Line item header or individual Book account description / M Item description';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.BOOK_ACCOUNT_ID IS 'The Book_account_id associated with an account combination from tbbs_book_account';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.BOOK_AMOUNT IS 'The balance of the book_account_id';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.BOOK_RC IS 'The book account reclass amount total for the line';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.BOOK_ADJ IS 'The manual book adjustment amount total ';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.BOOK_ELIM IS 'Elimination column, used by treatment type logic to eliminate amounts from adjusted book basis.';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.ADJUSTED_BOOK_BASIS IS 'The total adjusted book balance: (book_amount + book_rc + book_adj + book_elim). Will appear on detail book records, line items and summary nodes.';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_ITEM_ID IS 'The M Item ID from Provision';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.ENTITY_ID IS 'The Entity ID from Provision';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_BEG_BAL_PY IS 'The M Item beginning balance in the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_ACTIVITY_PY IS 'The M Item current activity in the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_ADJUST_PY IS 'The M Item adjustment activity in the Prior year case. Also includes the RTA months in the Prior year case.';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_VARIANCE_PY IS 'Variance column, used to hold any difference between the m_beg_bal_cy column and (m_beg_bal_py + m_activity_py + m_adjust_py)';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_BEG_BAL_CY IS 'The M Item beginning balance in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_ACTIVITY_CY IS 'The M Item current activity in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_ADJUST_CY IS 'The M Item adjustment activity in the Current year case.';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_RTA_CY IS 'The RTA month M Item activity in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_VARIANCE_CY IS 'A Variance column used to hold any difference between the m_ltd_bal column and (m_beg_bal_cy + m_activity_cy + m_adjust_cy + m_rta_cy)';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_LTD_BAL IS 'The M Item ending balance in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_M13_PY IS 'The Period 13 activity in the Prior year case, used to compare with the RTA months in the current year case.';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_UNBOOKED_RTA IS 'RTA Activity that exists in Month 13 of the Prior Year case that has not yet been pulled into the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TAX_M_RC IS 'The M item Gross reclass total for the line';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TAX_M_ADJ IS 'The Manual M Item adjustment total on a line item';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TAX_M_ELIM IS 'Elimination column, used by treatment type logic to eliminate amounts from the adjusted timing difference balance.';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.ADJUSTED_M_LTD_BAL IS 'The total adjusted M Item balance: (m_ltd_bal + m_unbooked_rta (based on a system control toggle) + tax_m_rc + tax_m_adj + tax_m_elim). Will appear on detail tax records, line items and summary nodes.';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.CALC_TAX_BASIS IS 'The calculated tax basis for the line item: adjusted_book_basis + adjusted_m_ltd_bal. Will only appear on line items and summary nodes. Will be null on detail book and tax records.';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_DEF_TAX_BAL IS 'The ending total deferred taxes on the M item = accum_reg_dit_end_cy + fas109_increment_end_cy + entity_grossup_end_cy';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.M_DEF_TAX_BAL_REG IS 'The ending reg asset amount on the M item: if reg_ind = 1, then fas109_increment_end_cy + entity_grossup_end_cy. Else 0';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TAX_DEF_RC IS 'The M item deferred tax reclass total for the line';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.ADJUSTED_DEF_TAX_TOT IS 'The adjusted Deferred Tax Balance for the line. = (m_def_tax_bal + m_def_tax_bal_reg + tax_def_rc)';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.CALC_DEF_RATE IS 'The calculated deferred tax rate for the line. = adjusted_def_tax_tot / adjusted_m_ltd_bal';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.LINE_ITEM_DESCRIPTION IS 'Description of the Line Item for each book and tax detail record';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TREE_LEVEL IS 'The level of the line within the tree structure';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.EMPTY_NODE IS 'Indicator whether the line is associated with an empty tree node';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.DETAIL_IND IS 'Indicator showing if the TBBS Line is a detail or summary record. 1 = detail, 0 = summary';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TREE_PARENT_ID IS 'The ID of the parent record within the PP_TREE structure. Should always be a PP Tree empty node (to contain other nodes / line items) or a TBBS Line item (to contain line item detail records).';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TREE_CHILD_ID IS 'The ID of the child record within the PP_TREE structure. Can be a PP Tree empty node, line item, or Line item detail record (negative value dumbkey)';
COMMENT ON COLUMN TBBS_REPORT_DATA_TMP.TREE_PATH IS 'The root tree path for the TBBS Line ID';
COMMENT ON TABLE TBBS_REPORT_DATA_TMP  IS '(T)  [08] The TBBS Report Data TMP table holds the final structured TBBS report data queried by the system.';

CREATE TABLE TBBS_REPORT_STG_DEBUG
(	ID NUMBER(22,0) NOT NULL,
TA_VERSION_ID_CY NUMBER(22,0),
TA_VERSION_ID_PY NUMBER(22,0),
GL_COMPANY VARCHAR2(50),
GL_MONTH NUMBER(22,2),
TBBS_MONTH NUMBER(22,0),
LINE_ITEM_ID NUMBER(22,0),
LINE_ITEM_DESCRIPTION VARCHAR2(50),
TREATMENT_TYPE_ID NUMBER(22,0),
LINE_TYPE_ID NUMBER(22,0),
LINE_TYPE_ORDER NUMBER(22,0),
LINE_DESCRIPTION VARCHAR2(100),
BOOK_ACCOUNT_ID NUMBER(22,0),
BOOK_ACCOUNT_DESCRIPTION VARCHAR2(50),
PRIMARY_ACCOUNT_DESCRIPTION VARCHAR2(50),
BOOK_AMOUNT NUMBER(22,2),
BOOK_RC NUMBER(22,2),
BOOK_ADJ NUMBER(22,2),
M_ITEM_ID NUMBER(22,0),
M_RESET_BAL_IND NUMBER(22,0),
M_PY_EXISTS_IND NUMBER(22,0),
ENTITY_ID NUMBER(22,0),
M_DESCRIPTION VARCHAR2(80),
M_BEG_BAL_PY NUMBER(22,2),
M_ACTIVITY_PY NUMBER(22,2),
M_ADJUST_PY NUMBER(22,2),
M_VARIANCE_PY NUMBER(22,2),
M_M13_PY NUMBER(22,2),
M_BEG_BAL_CY NUMBER(22,2),
M_ACTIVITY_CY NUMBER(22,2),
M_ADJUST_CY NUMBER(22,2),
M_RTA_CY NUMBER(22,2),
M_VARIANCE_CY NUMBER(22,2),
M_LTD_BAL NUMBER(22,2),
M_UNBOOKED_RTA NUMBER(22,2),
M_DEF_TAX_BAL NUMBER(22,2),
M_DEF_TAX_BAL_REG NUMBER(22,2),
TAX_M_RC NUMBER(22,2),
TAX_DEF_RC NUMBER(22,2),
TAX_M_ADJ NUMBER(22,2)
) ;

COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.ID IS 'Dummy ID for the staging table';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.TA_VERSION_ID_CY IS 'The ta_version_id of the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.TA_VERSION_ID_PY IS 'The ta_version_id of the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.GL_COMPANY IS 'The GL Company No that the Book and Tax data is summarized by';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.GL_MONTH IS 'The GL Month for the Provision tax data';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.TBBS_MONTH IS 'The month_number (YYYYMM) associated with the Provision GL Month';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.LINE_ITEM_ID IS 'The Line Item ID that the book account and M Items are mapped to';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.LINE_ITEM_DESCRIPTION IS 'The Description of the line item ID';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.TREATMENT_TYPE_ID IS 'The Treatment Type ID associated with the Line Item';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.LINE_TYPE_ID IS 'ID of the tbbs line type, indicating whether the line is a Book detail, Tax detail, etc. record';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.LINE_TYPE_ORDER IS 'Sort order for the Line Types';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.LINE_DESCRIPTION IS 'Description of an individual line on the TBBS';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.BOOK_ACCOUNT_ID IS 'The Book_account_id associated with an account combination from tbbs_book_account';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.BOOK_ACCOUNT_DESCRIPTION IS 'The Concatenated Book Account values for a book_account_id';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.PRIMARY_ACCOUNT_DESCRIPTION IS 'The description of the Primary account associated with a book_account_id';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.BOOK_AMOUNT IS 'The balance of the book_account_id';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.BOOK_RC IS 'The book account reclass amount total for the line';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.BOOK_ADJ IS 'The manual book adjustment amount total ';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_ITEM_ID IS 'The M Item ID from Provision';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_RESET_BAL_IND IS '1/0 indicator that displays whether the ending balance of the M item does not roll forward, meaning the PY beginning + activity will not tie to current year beginning';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_PY_EXISTS_IND IS '1/0 indicating whether prior year data exists for this M or not';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.ENTITY_ID IS 'The Entity ID from Provision';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_DESCRIPTION IS 'The description of the M Item from Provision';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_BEG_BAL_PY IS 'The M Item beginning balance in the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_ACTIVITY_PY IS 'The M Item current activity in the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_ADJUST_PY IS 'The M Item adjustment activity in the Prior year case. Also includes the RTA months in the Prior year case.';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_VARIANCE_PY IS 'Variance column, used to hold any difference between the m_beg_bal_cy column and (m_beg_bal_py + m_activity_py + m_adjust_py)';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_M13_PY IS 'The Period 13 activity in the Prior year case, used to compare with the RTA months in the current year case.';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_BEG_BAL_CY IS 'The M Item beginning balance in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_ACTIVITY_CY IS 'The M Item current activity in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_ADJUST_CY IS 'The M Item adjustment activity in the Current case.';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_RTA_CY IS 'The RTA month M Item activity in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_VARIANCE_CY IS 'A Variance column used to hold any difference between the m_ltd_bal column and (m_beg_bal_cy + m_activity_cy + m_adjust_cy + m_rta_cy)';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_LTD_BAL IS 'The M Item ending balance in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_UNBOOKED_RTA IS 'RTA Activity that exists in Month 13 of the Prior Year case that has not yet been pulled into the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_DEF_TAX_BAL IS 'The total non-regulatory deferred tax balance from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.M_DEF_TAX_BAL_REG IS 'The total regulatory deferred tax balance from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.TAX_M_RC IS 'The M item Gross reclass total for the line';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.TAX_DEF_RC IS 'The M item deferred tax reclass total for the line';
COMMENT ON COLUMN TBBS_REPORT_STG_DEBUG.TAX_M_ADJ IS 'The Manual M Item adjustment total on a line item';
COMMENT ON TABLE TBBS_REPORT_STG_DEBUG  IS '(O)  [08] Table that stores stores the data from the tbbs_report_stg_tmp table so that the data may be copied from one database session to another.  The f_tax_accrual_report_debug database function is used to insert rows into this table and to insert rows from this table back into the tax_accrual_rep_criteria_tmp in another session.  This function and table are useful when attempting to debug a report in a PowerBuilder datawindow using data that was loaded into the reporting temporary tables through the application';

CREATE GLOBAL TEMPORARY TABLE TBBS_REPORT_STG_TMP
(	ID NUMBER(22,0) NOT NULL,
TA_VERSION_ID_CY NUMBER(22,0),
TA_VERSION_ID_PY NUMBER(22,0),
GL_COMPANY VARCHAR2(50),
GL_MONTH NUMBER(22,2),
TBBS_MONTH NUMBER(22,0),
LINE_ITEM_ID NUMBER(22,0),
LINE_ITEM_DESCRIPTION VARCHAR2(50),
TREATMENT_TYPE_ID NUMBER(22,0),
LINE_TYPE_ID NUMBER(22,0),
LINE_TYPE_ORDER NUMBER(22,0),
LINE_DESCRIPTION VARCHAR2(100),
BOOK_ACCOUNT_ID NUMBER(22,0),
BOOK_ACCOUNT_DESCRIPTION VARCHAR2(50),
PRIMARY_ACCOUNT_DESCRIPTION VARCHAR2(50),
BOOK_AMOUNT NUMBER(22,2),
BOOK_RC NUMBER(22,2),
BOOK_ADJ NUMBER(22,2),
M_ITEM_ID NUMBER(22,0),
M_RESET_BAL_IND NUMBER(22,0),
M_PY_EXISTS_IND NUMBER(22,0),
ENTITY_ID NUMBER(22,0),
M_DESCRIPTION VARCHAR2(80),
M_BEG_BAL_PY NUMBER(22,2),
M_ACTIVITY_PY NUMBER(22,2),
M_ADJUST_PY NUMBER(22,2),
M_VARIANCE_PY NUMBER(22,2),
M_M13_PY NUMBER(22,2),
M_BEG_BAL_CY NUMBER(22,2),
M_ACTIVITY_CY NUMBER(22,2),
M_ADJUST_CY NUMBER(22,2),
M_RTA_CY NUMBER(22,2),
M_VARIANCE_CY NUMBER(22,2),
M_LTD_BAL NUMBER(22,2),
M_UNBOOKED_RTA NUMBER(22,2),
M_DEF_TAX_BAL NUMBER(22,2),
M_DEF_TAX_BAL_REG NUMBER(22,2),
TAX_M_RC NUMBER(22,2),
TAX_DEF_RC NUMBER(22,2),
TAX_M_ADJ NUMBER(22,2)
) ON COMMIT PRESERVE ROWS ;

COMMENT ON COLUMN TBBS_REPORT_STG_TMP.ID IS 'Dummy ID for the staging table';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.TA_VERSION_ID_CY IS 'The ta_version_id of the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.TA_VERSION_ID_PY IS 'The ta_version_id of the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.GL_COMPANY IS 'The GL Company No that the Book and Tax data is summarized by';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.GL_MONTH IS 'The GL Month for the Provision tax data';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.TBBS_MONTH IS 'The month_number (YYYYMM) associated with the Provision GL Month';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.LINE_ITEM_ID IS 'The Line Item ID that the book account and M Items are mapped to';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.LINE_ITEM_DESCRIPTION IS 'The Description of the line item ID';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.TREATMENT_TYPE_ID IS 'The Treatment Type ID associated with the Line Item';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.LINE_TYPE_ID IS 'ID of the tbbs line type, indicating whether the line is a Book detail, Tax detail, etc. record';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.LINE_TYPE_ORDER IS 'Sort order for the Line Types';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.LINE_DESCRIPTION IS 'Description of an individual line on the TBBS';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.BOOK_ACCOUNT_ID IS 'The Book_account_id associated with an account combination from tbbs_book_account';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.BOOK_ACCOUNT_DESCRIPTION IS 'The Concatenated Book Account values for a book_account_id';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.PRIMARY_ACCOUNT_DESCRIPTION IS 'The description of the Primary account associated with a book_account_id';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.BOOK_AMOUNT IS 'The balance of the book_account_id';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.BOOK_RC IS 'The book account reclass amount total for the line';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.BOOK_ADJ IS 'The manual book adjustment amount total ';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_ITEM_ID IS 'The M Item ID from Provision';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_RESET_BAL_IND IS '1/0 indicator that displays whether the ending balance of the M item does not roll forward, meaning the PY beginning + activity will not tie to current year beginning';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_PY_EXISTS_IND IS '1/0 indicating whether prior year data exists for this M or not';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.ENTITY_ID IS 'The Entity ID from Provision';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_DESCRIPTION IS 'The description of the M Item from Provision';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_BEG_BAL_PY IS 'The M Item beginning balance in the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_ACTIVITY_PY IS 'The M Item current activity in the Prior Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_ADJUST_PY IS 'The M Item adjustment activity in the Prior year case. Also includes the RTA months in the Prior year case.';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_VARIANCE_PY IS 'Variance column, used to hold any difference between the m_beg_bal_cy column and (m_beg_bal_py + m_activity_py + m_adjust_py)';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_M13_PY IS 'The Period 13 activity in the Prior year case, used to compare with the RTA months in the current year case.';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_BEG_BAL_CY IS 'The M Item beginning balance in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_ACTIVITY_CY IS 'The M Item current activity in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_ADJUST_CY IS 'The M Item adjustment activity in the Current case.';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_RTA_CY IS 'The RTA month M Item activity in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_VARIANCE_CY IS 'A Variance column used to hold any difference between the m_ltd_bal column and (m_beg_bal_cy + m_activity_cy + m_adjust_cy + m_rta_cy)';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_LTD_BAL IS 'The M Item ending balance in the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_UNBOOKED_RTA IS 'RTA Activity that exists in Month 13 of the Prior Year case that has not yet been pulled into the Current Year case';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_DEF_TAX_BAL IS 'The total non-regulatory deferred tax balance from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.M_DEF_TAX_BAL_REG IS 'The total regulatory deferred tax balance from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.TAX_M_RC IS 'The M item Gross reclass total for the line';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.TAX_DEF_RC IS 'The M item deferred tax reclass total for the line';
COMMENT ON COLUMN TBBS_REPORT_STG_TMP.TAX_M_ADJ IS 'The Manual M Item adjustment total on a line item';
COMMENT ON TABLE TBBS_REPORT_STG_TMP  IS '(T)  [08] The TBBS Report STG TMP table is a temporary table that stages the tbbs data from the book and tax sources.';

CREATE TABLE TBBS_REP_CRITERIA_DEBUG
(	CRITERIA_TYPE VARCHAR2(35) NOT NULL,
ID NUMBER(22,2) NOT NULL,
DESCRIPTION VARCHAR2(2000)
) ;

COMMENT ON COLUMN TBBS_REP_CRITERIA_DEBUG.CRITERIA_TYPE IS 'The type of selection criteria.  For example, the selected companies and month_number';
COMMENT ON COLUMN TBBS_REP_CRITERIA_DEBUG.ID IS 'The ID of the selection criteria.  For example, the company criteria would be the company ID.';
COMMENT ON COLUMN TBBS_REP_CRITERIA_DEBUG.DESCRIPTION IS 'The description of the selection criteria.  For example, in the case of company ID this column stores the company description.';
COMMENT ON TABLE TBBS_REP_CRITERIA_DEBUG  IS '(O)  [08] Table that stores stores the data from the tbbs_rep_criteria_tmp table so that the data may be copied from one database session to another.  The f_tax_accrual_report_debug database function is used to insert rows into this table and to insert rows from this table back into the tax_accrual_rep_criteria_tmp in another session.  This function and table are useful when attempting to debug a report in a PowerBuilder datawindow using data that was loaded into the reporting temporary tables through the application';

CREATE GLOBAL TEMPORARY TABLE TBBS_REP_CRITERIA_TMP
(	CRITERIA_TYPE VARCHAR2(35) NOT NULL,
ID NUMBER(22,2) NOT NULL,
DESCRIPTION VARCHAR2(2000)
) ON COMMIT PRESERVE ROWS ;

COMMENT ON COLUMN TBBS_REP_CRITERIA_TMP.CRITERIA_TYPE IS 'The type of selection criteria.  For example, the selected companies and month_number';
COMMENT ON COLUMN TBBS_REP_CRITERIA_TMP.ID IS 'The ID of the selection criteria.  For example, the company criteria would be the company ID.';
COMMENT ON COLUMN TBBS_REP_CRITERIA_TMP.DESCRIPTION IS 'The description of the selection criteria.  For example, in the case of company ID this column stores the company description.';
COMMENT ON TABLE TBBS_REP_CRITERIA_TMP  IS '(T)  [08] The TBBS Rep Criteria Tmp table is a global temporary table used to store the report filter criteria for the TBBS.';

CREATE TABLE TBBS_SCHEMA
(	SCHEMA_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
DESCRIPTION VARCHAR2(50)
) ;

COMMENT ON COLUMN TBBS_SCHEMA.SCHEMA_ID IS 'A system-assigned identifier that is unique to a schema.';
COMMENT ON COLUMN TBBS_SCHEMA.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_SCHEMA.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_SCHEMA.DESCRIPTION IS 'The description of the schema, usually the name of a general ledger system.';
COMMENT ON TABLE TBBS_SCHEMA  IS '(S)  [08] The TBBS Schema table contains a list of schemas for mapping book accounts and M Items to TBBS line items. A schema represents a mapping set of book accounts and M items. A company with multiple general ledger systems would have multiple schemas.';

/* JAW */
CREATE TABLE TBBS_SCHEMA_ASSIGN
(	GL_COMPANY VARCHAR2(50) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
SCHEMA_ID NUMBER(22,0)
) ;

COMMENT ON COLUMN TBBS_SCHEMA_ASSIGN.GL_COMPANY IS 'The identifier of a company on the book balances table mapped to this company ID';
COMMENT ON COLUMN TBBS_SCHEMA_ASSIGN.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_SCHEMA_ASSIGN.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_SCHEMA_ASSIGN.SCHEMA_ID IS 'A system-assigned identifier that is unique to a TBBS schema.';
COMMENT ON TABLE TBBS_SCHEMA_ASSIGN  IS '(S)  [08] The TBBS Schema Assign table contains the GL Company to schema assignments.';

CREATE TABLE TBBS_SYSTEM_CONTROL
(	CONTROL_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
CONTROL_NAME VARCHAR2(35),
CONTROL_VALUE VARCHAR2(254),
DESCRIPTION VARCHAR2(50),
LONG_DESCRIPTION VARCHAR2(2000)
) ;

COMMENT ON COLUMN TBBS_SYSTEM_CONTROL.CONTROL_ID IS 'The identifier of the TBBS control';
COMMENT ON COLUMN TBBS_SYSTEM_CONTROL.TIME_STAMP IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN TBBS_SYSTEM_CONTROL.USER_ID IS 'System-generated user_id used for audit purposes.';
COMMENT ON COLUMN TBBS_SYSTEM_CONTROL.CONTROL_NAME IS 'The name of the TBBS control';
COMMENT ON COLUMN TBBS_SYSTEM_CONTROL.CONTROL_VALUE IS 'The value of the TBBS control.  This is the field that is modified to change system settings.';
COMMENT ON COLUMN TBBS_SYSTEM_CONTROL.DESCRIPTION IS 'The description of the system control.';
COMMENT ON COLUMN TBBS_SYSTEM_CONTROL.LONG_DESCRIPTION IS 'The long description of the system control';
COMMENT ON TABLE TBBS_SYSTEM_CONTROL  IS '(S)  [08] The TBBS System control table stores system options that can be configured on a client-by-client basis.';

CREATE TABLE TBBS_TAX_ADJUST
(	TAX_ADJUST_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
GL_COMPANY VARCHAR2(50) NOT NULL,
EFFECTIVE_MONTH NUMBER(22,0) NOT NULL,
DR_M_ITEM_ID NUMBER(22,0) NOT NULL,
CR_M_ITEM_ID NUMBER(22,0) NOT NULL,
AMOUNT NUMBER(22,2),
NOTE VARCHAR2(70)
) ;

COMMENT ON COLUMN TBBS_TAX_ADJUST.TAX_ADJUST_ID IS 'The ID of the manual tax adjustment. Each adjustment for a company, month, dr and cr M item has a separate ID';
COMMENT ON COLUMN TBBS_TAX_ADJUST.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_TAX_ADJUST.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_TAX_ADJUST.GL_COMPANY IS 'The identifier of the company on the GL.';
COMMENT ON COLUMN TBBS_TAX_ADJUST.EFFECTIVE_MONTH IS 'A number in YYYYMM format that indicates the first month the adjustment is active.';
COMMENT ON COLUMN TBBS_TAX_ADJUST.DR_M_ITEM_ID IS 'The M item that will be debited for the adjustment amount.';
COMMENT ON COLUMN TBBS_TAX_ADJUST.CR_M_ITEM_ID IS 'The M item that will be credited for the adjustment amount.';
COMMENT ON COLUMN TBBS_TAX_ADJUST.AMOUNT IS 'Amount of the manual adjustment. This number should always be positive.';
COMMENT ON COLUMN TBBS_TAX_ADJUST.NOTE IS 'A description where a user can write a note regarding the adjustment.';
COMMENT ON TABLE TBBS_TAX_ADJUST  IS '(O)  [08] The TBBS Tax Adjust table allows users to make manual adjustments to tax amounts on M items in the TBBS by company as of an effective month.';

CREATE TABLE TBBS_TAX_ADJUST_DOC_RELATE
(	TAX_ADJUST_ID NUMBER(22,0) NOT NULL,
DOCUMENT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18)
) ;

COMMENT ON COLUMN TBBS_TAX_ADJUST_DOC_RELATE.TAX_ADJUST_ID IS 'The ID of the book adjustment';
COMMENT ON COLUMN TBBS_TAX_ADJUST_DOC_RELATE.DOCUMENT_ID IS 'The ID of the attached document from tbbs_document';
COMMENT ON COLUMN TBBS_TAX_ADJUST_DOC_RELATE.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_TAX_ADJUST_DOC_RELATE.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON TABLE TBBS_TAX_ADJUST_DOC_RELATE  IS '(O)  [08] The TBBS Tax Adjust Doc Relate table associates uploaded documents with manual tax adjustments.';

CREATE TABLE TBBS_TAX_ASSIGN
(	SCHEMA_ID NUMBER(22,0) NOT NULL,
M_ITEM_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
LINE_ITEM_ID NUMBER(22,0)
) ;

COMMENT ON COLUMN TBBS_TAX_ASSIGN.SCHEMA_ID IS 'The Tax Basis Balance Sheet Schema associated with the M Item to line item mapping.';
COMMENT ON COLUMN TBBS_TAX_ASSIGN.M_ITEM_ID IS 'The M Item from Provision, mapped to a line item in the selected schema.';
COMMENT ON COLUMN TBBS_TAX_ASSIGN.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_TAX_ASSIGN.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_TAX_ASSIGN.LINE_ITEM_ID IS 'A system assigned identifier to represent a tax basis balance sheet line item.';
COMMENT ON TABLE TBBS_TAX_ASSIGN  IS '(S)  [08] The TBBS Tax Assign table associates an M Item with a tax basis balance sheet line item by schema.';

CREATE TABLE TBBS_TAX_BALANCE_DEBUG
(	COMPANY_ID NUMBER(22,0) NOT NULL,
GL_MONTH NUMBER(22,2) NOT NULL,
M_ITEM_ID NUMBER(22,0) NOT NULL,
OPER_IND NUMBER(22,0) NOT NULL,
ENTITY_ID NUMBER(22,0) NOT NULL,
GL_COMPANY VARCHAR2(50),
TBBS_MONTH NUMBER(22,0),
M_TYPE_ID NUMBER(22,0),
RESET_BALANCE_IND NUMBER(22,0),
TA_VERSION_ID_CY NUMBER(22,0),
M_ID_CY NUMBER(22,0),
INCLUDED_IND_CY NUMBER(22,0),
M_BEG_YEAR_BALANCE_CY NUMBER(22,2),
M_ACTIVITY_YTD_CUR_CY NUMBER(22,2),
M_ACTIVITY_YTD_ADJ_CY NUMBER(22,2),
M_ACTIVITY_YTD_RTA_CY NUMBER(22,2),
M_ACTIVITY_YTD_M13_CY NUMBER(22,2),
M_END_LTD_BALANCE_CY NUMBER(22,2),
M_DEF_TAX_BAL NUMBER(22,2),
M_DEF_TAX_BAL_REG NUMBER(22,2),
TA_VERSION_ID_PY NUMBER(22,0),
M_ID_PY NUMBER(22,0),
M_BEG_YEAR_BALANCE_PY NUMBER(22,2),
M_ACTIVITY_YTD_CUR_PY NUMBER(22,2),
M_ACTIVITY_YTD_ADJ_PY NUMBER(22,2),
M_ACTIVITY_YTD_RTA_PY NUMBER(22,2),
M_ACTIVITY_YTD_M13_PY NUMBER(22,2)
) ;

COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.COMPANY_ID IS 'The system identifier of the company in Provision.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.GL_MONTH IS 'The month identifier in Provision. Associated with a specific tax case. This month relates to the Current Year case';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ITEM_ID IS 'The M Item identifier in Provision from tax_accrual_control';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.OPER_IND IS 'The Operating Indicator in in Provision from tax_accrual_control';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.ENTITY_ID IS 'The Entity ID in Provision from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.GL_COMPANY IS 'The identifier of a company on the book balances table';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.TBBS_MONTH IS 'The Ledger month number associated with this month. Formatted YYYYMM.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_TYPE_ID IS 'The Type identifier for an M Item in Provision.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.RESET_BALANCE_IND IS 'Indicator that exists by M Type and shows if that M Type balance will reset at the end of the year. 1 = reset, 0 = roll forward.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.TA_VERSION_ID_CY IS 'The system identifier for the current year Provision case from tax_accrual_version';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ID_CY IS 'The system identifier for a distinct Provision tax record in the current year case. A single M ID consists of a unique combination of ta_version_id, company_id, m_item_id and oper_ind';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.INCLUDED_IND_CY IS 'An indicator that states if the associated entity is included for deferred tax purposes during the GL month in the current year';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_BEG_YEAR_BALANCE_CY IS 'The beginning balance for the M Item in the first month of the current year case';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ACTIVITY_YTD_CUR_CY IS 'The YTD Current Year activity for the M Item in the current year case as of the gl_month. This can include Month 13 (Activity_type_id = 2)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ACTIVITY_YTD_ADJ_CY IS 'The YTD Adjustment activity for the M Item in the current year case as of the gl_month (Activity_type_id = 1)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ACTIVITY_YTD_RTA_CY IS 'The YTD RTP related activity for the M Item in the current year case as of the gl_month. This includes the Return To Provision processing option, not Month 13 (Activity_type_id = 3)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ACTIVITY_YTD_M13_CY IS 'The Total Month 13 related activity for the M Item in the prior year case. This will only include Current Year activity for Period 13 months in the prior year.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_END_LTD_BALANCE_CY IS 'The ending balance of the M Item in the current year case as of the gl_month';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_DEF_TAX_BAL IS 'The total non-regulatory deferred tax balance from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_DEF_TAX_BAL_REG IS 'The total regulatory deferred tax balance from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.TA_VERSION_ID_PY IS 'The system identifier for the prior year Provision case from tax_accrual_version.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ID_PY IS 'The system identifier for a distinct Provision tax record in the prior year case. A single M ID consists of a unique combination of ta_version_id, company_id, m_item_id and oper_ind';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_BEG_YEAR_BALANCE_PY IS 'The beginning balance for the M Item in the first month of the prior year case';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ACTIVITY_YTD_CUR_PY IS 'The Total Current Year activity for the M Item in the prior year case. This can include Month 13 (Activity_type_id = 2)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ACTIVITY_YTD_ADJ_PY IS 'The Total Adjustment activity for the M Item in the prior year case (Activity_type_id = 1)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_DEBUG.M_ACTIVITY_YTD_RTA_PY IS 'The Total RTP related activity for the M Item in the prior year case. This includes the Return To Provision processing option, not Month 13 (Activity_type_id = 3)';
COMMENT ON TABLE TBBS_TAX_BALANCE_DEBUG  IS '(O)  [08] Table that stores stores the data from the tbbs_tax_balance_tmp table so that the data may be copied from one database session to another.  The f_tax_accrual_report_debug database function is used to insert rows into this table and to insert rows from this table back into the tax_accrual_rep_criteria_tmp in another session.  This function and table are useful when attempting to debug a report in a PowerBuilder datawindow using data that was loaded into the reporting temporary tables through the application';

CREATE GLOBAL TEMPORARY TABLE TBBS_TAX_BALANCE_TMP
(	COMPANY_ID NUMBER(22,0) NOT NULL,
GL_MONTH NUMBER(22,2) NOT NULL,
M_ITEM_ID NUMBER(22,0) NOT NULL,
OPER_IND NUMBER(22,0) NOT NULL,
ENTITY_ID NUMBER(22,0) NOT NULL,
GL_COMPANY VARCHAR2(50),
TBBS_MONTH NUMBER(22,0),
M_TYPE_ID NUMBER(22,0),
RESET_BALANCE_IND NUMBER(22,0),
TA_VERSION_ID_CY NUMBER(22,0),
M_ID_CY NUMBER(22,0),
INCLUDED_IND_CY NUMBER(22,0),
M_BEG_YEAR_BALANCE_CY NUMBER(22,2),
M_ACTIVITY_YTD_CUR_CY NUMBER(22,2),
M_ACTIVITY_YTD_ADJ_CY NUMBER(22,2),
M_ACTIVITY_YTD_RTA_CY NUMBER(22,2),
M_ACTIVITY_YTD_M13_CY NUMBER(22,2),
M_END_LTD_BALANCE_CY NUMBER(22,2),
M_DEF_TAX_BAL NUMBER(22,2),
M_DEF_TAX_BAL_REG NUMBER(22,2),
TA_VERSION_ID_PY NUMBER(22,0),
M_ID_PY NUMBER(22,0),
M_BEG_YEAR_BALANCE_PY NUMBER(22,2),
M_ACTIVITY_YTD_CUR_PY NUMBER(22,2),
M_ACTIVITY_YTD_ADJ_PY NUMBER(22,2),
M_ACTIVITY_YTD_RTA_PY NUMBER(22,2),
M_ACTIVITY_YTD_M13_PY NUMBER(22,2)
) ON COMMIT PRESERVE ROWS ;

COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.COMPANY_ID IS 'The system identifier of the company in Provision.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.GL_MONTH IS 'The month identifier in Provision. Associated with a specific tax case. This month relates to the Current Year case';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ITEM_ID IS 'The M Item identifier in Provision from tax_accrual_control';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.OPER_IND IS 'The Operating Indicator in in Provision from tax_accrual_control';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.ENTITY_ID IS 'The Entity ID in Provision from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.GL_COMPANY IS 'The identifier of a company on the book balances table';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.TBBS_MONTH IS 'The Ledger month number associated with this month. Formatted YYYYMM.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_TYPE_ID IS 'The Type identifier for an M Item in Provision.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.RESET_BALANCE_IND IS 'Indicator that exists by M Type and shows if that M Type balance will reset at the end of the year. 1 = reset, 0 = roll forward.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.TA_VERSION_ID_CY IS 'The system identifier for the current year Provision case from tax_accrual_version';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ID_CY IS 'The system identifier for a distinct Provision tax record in the current year case. A single M ID consists of a unique combination of ta_version_id, company_id, m_item_id and oper_ind';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.INCLUDED_IND_CY IS 'An indicator that states if the associated entity is included for deferred tax purposes during the GL month in the current year';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_BEG_YEAR_BALANCE_CY IS 'The beginning balance for the M Item in the first month of the current year case';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ACTIVITY_YTD_CUR_CY IS 'The YTD Current Year activity for the M Item in the current year case as of the gl_month. This can include Month 13 (Activity_type_id = 2)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ACTIVITY_YTD_ADJ_CY IS 'The YTD Adjustment activity for the M Item in the current year case as of the gl_month (Activity_type_id = 1)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ACTIVITY_YTD_RTA_CY IS 'The YTD RTP related activity for the M Item in the current year case as of the gl_month. This includes the Return To Provision processing option, not Month 13 (Activity_type_id = 3)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ACTIVITY_YTD_M13_CY IS 'The Total Month 13 related activity for the M Item in the prior year case. This will only include Current Year activity for Period 13 months in the prior year.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_END_LTD_BALANCE_CY IS 'The ending balance of the M Item in the current year case as of the gl_month';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_DEF_TAX_BAL IS 'The total non-regulatory deferred tax balance from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_DEF_TAX_BAL_REG IS 'The total regulatory deferred tax balance from tax_accrual_fas109';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.TA_VERSION_ID_PY IS 'The system identifier for the prior year Provision case from tax_accrual_version.';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ID_PY IS 'The system identifier for a distinct Provision tax record in the prior year case. A single M ID consists of a unique combination of ta_version_id, company_id, m_item_id and oper_ind';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_BEG_YEAR_BALANCE_PY IS 'The beginning balance for the M Item in the first month of the prior year case';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ACTIVITY_YTD_CUR_PY IS 'The Total Current Year activity for the M Item in the prior year case. This can include Month 13 (Activity_type_id = 2)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ACTIVITY_YTD_ADJ_PY IS 'The Total Adjustment activity for the M Item in the prior year case (Activity_type_id = 1)';
COMMENT ON COLUMN TBBS_TAX_BALANCE_TMP.M_ACTIVITY_YTD_RTA_PY IS 'The Total RTP related activity for the M Item in the prior year case. This includes the Return To Provision processing option, not Month 13 (Activity_type_id = 3)';
COMMENT ON TABLE TBBS_TAX_BALANCE_TMP  IS '(T)  [08] The TBBS Tax balances table summarizes the tax data from the Provision tables for use in creating the TBBS.';

CREATE TABLE TBBS_TAX_RECLASS
(	TAX_RECLASS_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
GL_COMPANY VARCHAR2(50) NOT NULL,
EFFECTIVE_MONTH NUMBER(22,0) NOT NULL,
SRC_M_ITEM_ID NUMBER(22,0) NOT NULL,
DST_M_ITEM_ID NUMBER(22,0) NOT NULL,
RECLASS_ACTIVE_IND NUMBER(22,0) DEFAULT 0,
NOTE VARCHAR2(70)
) ;

COMMENT ON COLUMN TBBS_TAX_RECLASS.TAX_RECLASS_ID IS 'The ID of the tax reclass adjustment. Each reclass for a company, month and source M Item has a separate ID';
COMMENT ON COLUMN TBBS_TAX_RECLASS.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_TAX_RECLASS.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN TBBS_TAX_RECLASS.GL_COMPANY IS 'The identifier of the company on the GL.';
COMMENT ON COLUMN TBBS_TAX_RECLASS.EFFECTIVE_MONTH IS 'A number in YYYYMM format that indicates the first month the reclass is active.';
COMMENT ON COLUMN TBBS_TAX_RECLASS.SRC_M_ITEM_ID IS 'The source M Item that will be zeroed out as part of the reclass.';
COMMENT ON COLUMN TBBS_TAX_RECLASS.DST_M_ITEM_ID IS 'The destination M Item that the source M Item balance will be transferred to';
COMMENT ON COLUMN TBBS_TAX_RECLASS.RECLASS_ACTIVE_IND IS '1/0 toggle that idicates if the M Item reclass is active or inactive starting in the effective month.';
COMMENT ON COLUMN TBBS_TAX_RECLASS.NOTE IS 'A description where a user can write a note regarding the reclass.';
COMMENT ON TABLE TBBS_TAX_RECLASS  IS '(O)  [08] The TBBS Tax Reclass table allows users to reclass entire M Item ending balances in the TBBS by company as of an effective month.';

CREATE TABLE TBBS_TAX_RECLASS_DOC_RELATE
(	TAX_RECLASS_ID NUMBER(22,0) NOT NULL,
DOCUMENT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18)
) ;

COMMENT ON COLUMN TBBS_TAX_RECLASS_DOC_RELATE.TAX_RECLASS_ID IS 'The ID of the tax reclass adjustment';
COMMENT ON COLUMN TBBS_TAX_RECLASS_DOC_RELATE.DOCUMENT_ID IS 'The ID of the attached document from tbbs_document';
COMMENT ON COLUMN TBBS_TAX_RECLASS_DOC_RELATE.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_TAX_RECLASS_DOC_RELATE.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON TABLE TBBS_TAX_RECLASS_DOC_RELATE  IS '(O)  [08] The TBBS tax Reclass Doc Relate table associates uploaded documents with tax reclass adjustments.';

CREATE TABLE TBBS_TREATMENT_TYPE
(	TREATMENT_TYPE_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
DESCRIPTION VARCHAR2(50),
LONG_DESCRIPTION VARCHAR2(254),
TREATMENT_SQL VARCHAR2(4000)
) ;

COMMENT ON COLUMN TBBS_TREATMENT_TYPE.TREATMENT_TYPE_ID IS 'A system-assigned identifier to represent a certain Tax Basis Balance Sheet Treatment.';
COMMENT ON COLUMN TBBS_TREATMENT_TYPE.TIME_STAMP IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN TBBS_TREATMENT_TYPE.USER_ID IS 'System-generated user_id used for audit purposes.';
COMMENT ON COLUMN TBBS_TREATMENT_TYPE.DESCRIPTION IS 'A description of this treatment.';
COMMENT ON COLUMN TBBS_TREATMENT_TYPE.LONG_DESCRIPTION IS 'A long description of this treatment that explains the use cases.';
COMMENT ON COLUMN TBBS_TREATMENT_TYPE.TREATMENT_SQL IS 'The SQL Logic that will be used to calculate tax basis for this treatment';
COMMENT ON TABLE TBBS_TREATMENT_TYPE  IS '(F)  [08] The TBBS Treatment Type table contains the list of different treatments of a tax basis balance sheet line item. These treatment types are defined by PowerPlan.';

CREATE TABLE TBBS_UNUSED_BOOK_ACCOUNT
(	SCHEMA_ID NUMBER(22,0) NOT NULL,
BOOK_ACCOUNT_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18)
) ;

COMMENT ON COLUMN TBBS_UNUSED_BOOK_ACCOUNT.SCHEMA_ID IS 'The Tax Basis Balance Sheet Schema associated with the unused book account mapping.';
COMMENT ON COLUMN TBBS_UNUSED_BOOK_ACCOUNT.BOOK_ACCOUNT_ID IS 'A system-generated identifier of the combination of account fields in tbbs_book_account_value. Defined by the fields set in tbbs_account_field and tbbs_account_field_assign.';
COMMENT ON COLUMN TBBS_UNUSED_BOOK_ACCOUNT.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_UNUSED_BOOK_ACCOUNT.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON TABLE TBBS_UNUSED_BOOK_ACCOUNT  IS '(O)  [08] The TBBS Unused Book Account table allows users to mark book accounts as being unused for the TBBS in the selected schema. These accounts will not show in the available box.';

CREATE TABLE TBBS_UNUSED_M_ITEM
(	SCHEMA_ID NUMBER(22,0) NOT NULL,
M_ITEM_ID NUMBER(22,0) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18)
) ;

COMMENT ON COLUMN TBBS_UNUSED_M_ITEM.SCHEMA_ID IS 'The Tax Basis Balance Sheet Schema associated with the unused M item mapping.';
COMMENT ON COLUMN TBBS_UNUSED_M_ITEM.M_ITEM_ID IS 'The M Item from Provision, marked as unused in the selected schema.';
COMMENT ON COLUMN TBBS_UNUSED_M_ITEM.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TBBS_UNUSED_M_ITEM.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON TABLE TBBS_UNUSED_M_ITEM  IS '(O)  [08] The TBBS Unused M Item table allows users to mark M items as being unused for the TBBS in the selected schema. These items will not show in the available box.';

CREATE TABLE PP_TREE_IMPROVED
(	NODE_ID NUMBER(22,0),
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
PP_TREE_TYPE_ID NUMBER(22,0),
PARENT_ID NUMBER(22,0),
SORT_ORDER NUMBER(22,0),
DEFAULT_DESCRIPTION VARCHAR2(254),
MANDATORY_LEAF NUMBER(1,0)
) ;

COMMENT ON COLUMN PP_TREE_IMPROVED.NODE_ID IS 'Identifier for nodes of tree. Generated by pp_tree_improved_seq';
COMMENT ON COLUMN PP_TREE_IMPROVED.TIME_STAMP IS 'System generated time stamp for audit purposes';
COMMENT ON COLUMN PP_TREE_IMPROVED.USER_ID IS 'System generated user ID for audit purposes';
COMMENT ON COLUMN PP_TREE_IMPROVED.PP_TREE_TYPE_ID IS 'PP Tree Type of the tree of which this node is a part';
COMMENT ON COLUMN PP_TREE_IMPROVED.PARENT_ID IS 'Node ID of direct parent of this node, or NULL if at top level';
COMMENT ON COLUMN PP_TREE_IMPROVED.SORT_ORDER IS 'Order in which this node should appear among its siblings';
COMMENT ON COLUMN PP_TREE_IMPROVED.DEFAULT_DESCRIPTION IS 'Description of node that should appear by default. This will be used for all organizational nodes, and can be used to override description of leafs (which typically come from the item table via a node/item relation table';
COMMENT ON COLUMN PP_TREE_IMPROVED.MANDATORY_LEAF IS '0 if this node is allowed to have children, 1 if not';
COMMENT ON TABLE PP_TREE_IMPROVED IS '(O) [08] Table describing nodes of a tree, including ancestral relationships';

------------------------
--create necessary views
------------------------
CREATE OR REPLACE FORCE VIEW PP_TREE_TBBS_TMP (PP_TREE_CATEGORY_ID, PP_TREE_TYPE_ID, PARENT_ID, CHILD_ID, EMPTY_NODE, TIME_STAMP, USER_ID, OVERRIDE_PARENT_DESCRIPTION, SORT_ORDER) AS
SELECT  ptt.pp_tree_category_id, ptt.pp_tree_type_id, nvl(pti.parent_id + tli_max_id.id_offset, pti.node_id + tli_max_id.id_offset) AS parent_id,
	decode(pti.mandatory_leaf, 1, tli.line_item_id, pti.node_id + tli_max_id.id_offset) AS child_id, decode(pti.mandatory_leaf, 0, 1, 0) AS empty_node,
	pti.time_stamp, pti.user_id, decode(pti_parents.node_id, null, pti.DEFAULT_DESCRIPTION, pti_parents.default_description) as override_parent_description, pti.sort_order
FROM pp_tree_improved pti
JOIN pp_tree_type ptt ON pti.pp_tree_type_id = ptt.pp_tree_type_id
LEFT JOIN pp_tree_improved pti_parents ON pti.parent_id = pti_parents.node_id
LEFT JOIN tbbs_line_item_tree_relate tlitr ON pti.node_id = tlitr.node_id
LEFT JOIN tbbs_line_item tli ON tlitr.line_item_id = tli.line_item_id
CROSS JOIN (SELECT MAX(line_item_id) AS id_offset FROM tbbs_line_item) tli_max_id;

CREATE OR REPLACE FORCE VIEW TBBS_LINE_ITEM_TREE_VIEW (PP_TREE_TYPE_ID, PARENT_ID, NODE_ID, DESCRIPTION, SORT_ORDER, MANDATORY_LEAF, TREE_LEVEL) AS
SELECT pp_tree_type_id, to_number(parent_id) AS parent_id, child_id AS node_id, node_desc AS description, tree_order AS sort_order, DECODE(empty_node, 0, 1, 1, 0) AS
	mandatory_leaf, LEVEL AS tree_level
FROM
(SELECT A.pp_tree_type_id, A.empty_node, DECODE(A.parent_id, A.child_id, NULL, A.parent_id) AS parent_id, A.child_id, DECODE(A.empty_node, 0, tli.description, DECODE(
  A.parent_id, A.child_id, A.override_parent_description, b.override_parent_description ) ) node_desc, A.sort_order tree_order
FROM pp_tree a
LEFT OUTER JOIN
  ( SELECT DISTINCT pp_tree_type_id, parent_id, override_parent_description
  FROM pp_tree
  WHERE parent_id <> child_id
  ) b
ON b.pp_tree_type_id = a.pp_tree_type_id
AND b.parent_id      = A.child_id
AND b.parent_id     <> A.parent_id
LEFT JOIN tbbs_line_item tli
ON A.empty_node           = 0
AND A.child_id            = tli.line_item_id
WHERE pp_tree_category_id = 4
)
CONNECT BY PRIOR child_id = parent_id
AND PRIOR pp_tree_type_id   = pp_tree_type_id
START WITH parent_id     IS NULL
ORDER SIBLINGS BY tree_order;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3282, 0, 2016, 1, 0, 0, 046196, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046196_01_prov_create_tbbs_tables_views_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;