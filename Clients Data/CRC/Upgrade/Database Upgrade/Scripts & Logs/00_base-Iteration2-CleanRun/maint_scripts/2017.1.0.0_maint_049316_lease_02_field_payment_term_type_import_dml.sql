/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049316_lease_02_field_payment_term_type_import_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

insert into pp_import_column(import_type_id, column_name, description, import_column_name, 
   is_required, processing_order, column_type, is_on_table)
select 
   (select import_type_id from pp_import_type where lower(import_table_name) = 'ls_import_ilr') AS import_type_id,
   'payment_term_type_id' AS column_name,
   'Payment Term Type ID' AS description, 
   'payment_term_type_xlate' AS import_column_name,
   1 AS is_required, 
   1 AS processing_order,
   'number(22,0)' AS column_type,
   1 AS is_on_table
from dual
;

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
   is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql,
   derived_autocreate_yn)
select 
   (select max(import_lookup_id)+1 from pp_import_lookup) import_lookup_id,
   'Payment Term Type.Description' AS description,
   'Payment Term Type.Description' AS long_Description,
   'payment_term_type_id' AS column_name,
   '(select pt.payment_term_type_id from ls_payment_term_type pt where upper( trim( <importfield> ) ) = upper( trim( pt.description ) ) )'AS lookup_sql,
   0 AS is_derived,
   'ls_payment_term_type' AS lookup_table_name, 
   'description' AS lookup_column_name,
   null AS lookup_constraining_columns,
   null AS lookup_values_alternate_sql,
   null AS derived_autocreate_yn
from dual;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4011, 0, 2017, 1, 0, 0, 49316, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049316_lease_02_field_payment_term_type_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;