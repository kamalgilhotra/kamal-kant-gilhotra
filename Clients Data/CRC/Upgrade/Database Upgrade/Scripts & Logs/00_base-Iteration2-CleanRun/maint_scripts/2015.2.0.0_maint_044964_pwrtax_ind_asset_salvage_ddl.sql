/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044964_pwrtax_ind_asset_salvage_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/21/2015 Sarah Byers    Add subledger_type_id and asset_id
||============================================================================
*/

alter table book_alloc_group add (subledger_type_id number(22,0));
comment on column book_alloc_group.subledger_type_id is 'System assigned identifier of the subledger type.';

alter table utility_account_depreciation add (asset_id number(22,0));
comment on column utility_account_depreciation.asset_id is 'System assigned identifier of an asset; will be populated when loading from CPR DEPR for individually depreciated assets.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2934, 0, 2015, 2, 0, 0, 44964, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044964_pwrtax_ind_asset_salvage_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
