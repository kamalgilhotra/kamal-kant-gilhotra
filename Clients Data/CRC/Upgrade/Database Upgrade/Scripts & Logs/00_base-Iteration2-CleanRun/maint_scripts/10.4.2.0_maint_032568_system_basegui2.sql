/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032568_system_basegui2.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/03/2013 Alex P.        Point Release
||============================================================================
*/

alter table PP_DYNAMIC_FILTER_VALUES add SEARCH_TYPE number(1, 0) not null;

comment on column PP_DYNAMIC_FILTER_VALUES.SEARCH_TYPE is 'Numeric identifier defining the type of object that contains values. 1 = datawindow, 2 = single line edit, 3 = daterange, 4 = operation.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (673, 0, 10, 4, 2, 0, 32568, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032568_system_basegui2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
