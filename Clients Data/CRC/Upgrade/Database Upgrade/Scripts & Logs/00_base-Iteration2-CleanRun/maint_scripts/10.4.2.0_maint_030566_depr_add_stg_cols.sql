SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030566_depr_add_stg_cols.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 02/18/2014 Charlie Shilling maint-30566
||============================================================================
*/

declare
   procedure DROP_COLUMN(V_TABLE_NAME varchar2,
                         V_COL_NAME   varchar2) is
   begin
      execute immediate 'alter table ' || V_TABLE_NAME || ' drop column ' || V_COL_NAME;
      DBMS_OUTPUT.PUT_LINE('Sucessfully dropped column ' || V_COL_NAME || ' from table ' ||
                           V_TABLE_NAME || '.');
   exception
      when others then
         if sqlcode = -904 then
            --904 is invalid identifier, which means that the table already does not have this column, so do nothing
            DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' on table ' || V_TABLE_NAME ||
                                 ' was already removed. No action necessary.');
         else
            RAISE_APPLICATION_ERROR(-20000,
                                    'Could not drop column ' || V_COL_NAME || ' from ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
         end if;
   end DROP_COLUMN;

   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                           V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                                    V_TABLE_NAME || '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' ||
                                       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                           V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('  Sucessfully added the comment to column ' || V_COL_NAME ||
                              ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
begin
   --add combined_depr_group_id
   ADD_COLUMN('depr_calc_stg',
              'combined_depr_group_id',
              'number(22,0)',
              'The combined_depr_group_id assigned to a particular depreciation group.');
   ADD_COLUMN('depr_calc_stg_arc',
              'combined_depr_group_id',
              'number(22,0)',
              'The combined_depr_group_id assigned to a particular depreciation group.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'combined_depr_group_id',
              'number(22,0)',
              'The combined_depr_group_id assigned to a particular depreciation group.');

   --add combined_depr_adj
   ADD_COLUMN('depr_calc_stg',
              'combined_depr_adj',
              'number(22,2)',
              'The adjustment to depreciation expense as a result of combined deprecition group allocation. This amount will be summarized to depr_exp_alloc_adjust with the other adjustments.');
   ADD_COLUMN('depr_calc_stg_arc',
              'combined_depr_adj',
              'number(22,2)',
              'The adjustment to depreciation expense as a result of combined deprecition group allocation. This amount will be summarized to depr_exp_alloc_adjust with the other adjustments.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'combined_depr_adj',
              'number(22,2)',
              'The adjustment to depreciation expense as a result of combined deprecition group allocation. This amount will be summarized to depr_exp_alloc_adjust with the other adjustments.');

   --add combined_salv_adj
   ADD_COLUMN('depr_calc_stg',
              'combined_salv_adj',
              'number(22,2)',
              'The adjustment to salvage expense as a result of combined deprecition group allocation. This amount will be summarized to salv_exp_alloc_adjust with the other adjustments.');
   ADD_COLUMN('depr_calc_stg_arc',
              'combined_salv_adj',
              'number(22,2)',
              'The adjustment to salvage expense as a result of combined deprecition group allocation. This amount will be summarized to salv_exp_alloc_adjust with the other adjustments.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'combined_salv_adj',
              'number(22,2)',
              'The adjustment to salvage expense as a result of combined deprecition group allocation. This amount will be summarized to salv_exp_alloc_adjust with the other adjustments.');

   --add combined_cor_adj
   ADD_COLUMN('depr_calc_stg',
              'combined_cor_adj',
              'number(22,2)',
              'The adjustment to cost of removal expense as a result of combined deprecition group allocation. This amount will be summarized to cor_exp_alloc_adjust with the other adjustments.');
   ADD_COLUMN('depr_calc_stg_arc',
              'combined_cor_adj',
              'number(22,2)',
              'The adjustment to cost of removal expense as a result of combined deprecition group allocation. This amount will be summarized to cor_exp_alloc_adjust with the other adjustments.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'combined_cor_adj',
              'number(22,2)',
              'The adjustment to cost of removal expense as a result of combined deprecition group allocation. This amount will be summarized to cor_exp_alloc_adjust with the other adjustments.');

   --add rwip_salvage
   ADD_COLUMN('depr_calc_stg',
              'rwip_salvage',
              'number(22,2)',
              'Internal calc column for salvage.');
   ADD_COLUMN('depr_calc_stg_arc',
              'rwip_salvage',
              'number(22,2)',
              'Internal calc column for salvage.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'rwip_salvage',
              'number(22,2)',
              'Internal calc column for salvage.');

   --add rwip_cor
   ADD_COLUMN('depr_calc_stg',
              'rwip_cor',
              'number(22,2)',
              'Internal calc column for cost of removal.');
   ADD_COLUMN('depr_calc_stg_arc',
              'rwip_cor',
              'number(22,2)',
              'Internal calc column for cost of removal.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'rwip_cor',
              'number(22,2)',
              'Internal calc column for cost of removal.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (970, 0, 10, 4, 2, 0, 30566, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_030566_depr_add_stg_cols.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;