/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_040459_sys_pp_htmlhelp_pt2.sql
|| Description: Insert statements for PP_HTMLHELP.
||              Reload the pp_htmlhelp table with the latest map_id's for
||              accessing the context sensitive help system.
||              PT2 : Reg user guide had a lot of last minute
||                    changes.  Reimporting forced mappings to change
||                    script below just replaces Reg mappings
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 10/29/2014 Andrew Scott
||============================================================================
*/

SET DEFINE OFF

/*
select DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID
  from PP_HTMLHELP
 order by COUNTER_ID;
*/

begin
   execute immediate 'alter table PP_HTMLHELP modify W_NAME varchar2(255)';
exception
   when others then
      null;
end;
/

delete from pp_htmlhelp
where lower(trim(subsystem)) = 'regulatory';
commit;

insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','1_1_Introduction4',21000,null,3000);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','1_2_Overview_of_PowerPlan_Regulatory_Management_System_Features',21001,null,3001);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','1_2_1_Secure_Regulatory_Database_and_Delivered_Integration',21002,null,3002);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','1_2_2_The_Case_Engine_for_Flexibility_Power_and_Speed',21003,null,3003);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','1_2_3_Understandable_and_Supportable_Presentation',21004,null,3004);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','1_3_Better_Credibility_Sharpened_Policy_Improved_Agility',21005,null,3005);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','2_1_Introduction3',21006,null,3006);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','2_1_1_Monitoring',21007,null,3007);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','2_1_2_Internal_Strategies',21008,null,3008);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','2_1_3_Rate_Case_Timing_Analysis',21009,null,3009);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','2_1_4_Jurisdictional_Cases',21010,null,3010);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','2_2_RMS_and_the_Cap_Ex_Asset_Management_Cycle',21011,null,3011);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','3_1_Introduction5',21012,null,3012);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','3_2_Icons_and_Buttons',21013,null,3013);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','3_2_1_Universal_Icons',21014,null,3014);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','3_2_2_Report_Workspace_Icons',21015,null,3015);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','3_2_3_One_click_Buttons',21016,null,3016);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','3_2_4_Import_Buttons',21017,null,3017);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_1_Introduction5',21018,null,3018);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_10_Income_Tax_Entity_Setup',21020,null,3019);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_10_1_Introduction',21021,'w_reg_main/uo_reg_tax_entity',3020);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_10_2_Add_a_Tax_Entity',21022,null,3021);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_10_3_Add_a_Tax_Rate',21023,null,3022);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_10_4_Relate_Regulatory_Accounts_to_a_Tax_Entity',21024,null,3023);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_Regulatory_Company_Jurisdiction_Setup',21025,'w_reg_main/uo_reg_company_setup',3024);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_1_Introduction',21026,null,3025);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_10_Copy_a_Jurisdiction_Template',21027,null,3026);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_11_Delete_a_Jurisdiction_Template',21028,null,3027);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_2_Add_a_Regulatory_Company',21029,null,3028);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_3_Inactivate_a_Regulatory_Company',21030,null,3029);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_4_Relating_a_Regulatory_Company_and_PowerPlan_Company',21031,null,3030);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_5_Unrelating_a_Regulatory_Company_and_PowerPlan_Company',21032,null,3031);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_6_Add_a_Jurisdiction',21033,null,3032);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_7_Add_a_Jurisdiction_Template',21034,null,3033);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_8_Copy_a_Jurisdiction_Same_Company_',21035,null,3034);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_2_9_Copy_a_Jurisdiction_Different_Company_',21036,null,3035);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_3_Company_Security',21037,null,3036);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_3_1_Introduction',21038,'w_reg_main/uo_reg_company_security',3037);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_3_2_Assign_a_User_to_a_Regulatory_Company',21039,null,3038);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_3_3_Remove_a_User_from_Regulatory_Company',21040,null,3039);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_3_4_Assign_User_Options',21041,null,3040);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_3_5_Assign_Company_Options',21042,null,3041);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_3_6_Other_Assign_Options',21043,null,3042);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_4_Capital_Structure',21044,null,3043);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_4_1_Introduction',21045,'w_reg_main/uo_reg_capital_struct_control_ws',3044);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_4_2_Add_a_Capital_Structure_Type',21046,null,3045);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_4_3_Copy_a_Capital_Structure_Type_Rarely_Used_',21047,null,3046);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_4_4_Delete_a_Capital_Structure_Type',21048,null,3047);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_4_5_Relate_a_Capital_Structure_Type_to_Regulatory_Sub_Account_Type',21049,null,3048);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_5_Account_Master_Setup',21050,null,3049);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_5_1_Introduction',21051,'w_reg_main/uo_reg_acct_master_ws',3050);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_5_2_Searching_for_Regulatory_Accounts',21052,null,3051);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_5_3_Add_Integration_Related_Regulatory_Accounts',21053,null,3052);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_5_4_Add_an_Input_Regulatory_Account',21054,null,3053);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_5_5_Copy_an_Account',21055,null,3054);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_5_6_Delete_an_Account',21056,null,3055);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_6_Sub_Account_Type_Setup',21057,null,3056);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_6_1_Introduction',21058,'w_reg_main/uo_reg_sub_acct_type_ws',3057);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_6_2_Add_a_Sub_Account_Type',21059,null,3058);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_6_3_Copy_a_Sub_Account_Type_Rarely_Used_',21060,null,3059);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_6_4_Delete_a_Sub_Account_Type',21061,null,3060);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_7_Assign_Regulatory_Accounts_to_Regulatory_Companies',21062,null,3061);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_7_1_Introduction',21063,'w_reg_main/uo_reg_acct_company_ws',3062);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_7_2_Update_the_Regulatory_Account_Company_Matrix',21064,null,3063);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_8_Review_Account_Master_to_Jurisdiction',21065,null,3064);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_8_1_Introduction',21066,'w_reg_main/uo_reg_manage_master_jur_ws',3065);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_8_2_Comparing_Account_Master_to_Jurisdiction_Template_Accounts',21067,null,3066);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_8_3_Opening_the_Account_Master_record',21068,null,3067);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_8_4_Opening_the_Manage_Jurisdiction_Accounts_workspace',21069,'w_reg_main/uo_reg_manage_jur_case_ws',3068);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_9_Analysis_Key_Summary_Setup',21070,null,3069);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_9_1_Introduction',21071,'w_reg_main/uo_reg_fin_monitor_summary_setup_ws',3070);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_9_2_Manually_Add_an_Analysis_Summary',21072,null,3071);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_9_3_Map_Sub_Account_Type_to_Create_Analysis_Summary',21073,null,3072);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_9_4_Map_Sub_Account_Type_to_Existing_Analysis_Summary',21074,null,3073);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','4_9_5_Remove_Sub_Account_Type_from_Analysis_Summary',21075,null,3074);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_1_Introduction4',21076,null,3075);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_10_Load_History_Ledger',21077,null,3076);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_2_Regulatory_Historic_Ledgers',21078,null,3077);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_2_1_Introduction',21079,'w_reg_main/uo_reg_ledger_adjustment_ws',3078);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_2_2_Add_New_Historic_Ledger',21080,null,3079);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_2_3_Copy_a_Historic_Ledger',21081,null,3080);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_2_4_Delete_a_Historic_Ledger',21083,null,3081);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_Reconciliation_Maintenance',21084,null,3082);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_1_Introduction',21085,'w_reg_main/uo_reg_hist_recon_setup',3083);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_2_Recommended_Reconciliation_Guidelines',21086,null,3084);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_3_Map_Unmapped_CR_Accounts_to_Reconciliation_Items',21087,null,3085);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_4_Review_CR_Mappings_to_Recon_Items',21088,null,3086);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_5_Map_Unmapped_Regulatory_Accounts_to_Recon_Items',21089,null,3087);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_6_Review_Reg_Acct_Mappings_to_Recon_Items',21090,null,3088);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_7_Adding_Reconciliation_Items',21091,null,3089);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_8_Editing_Reconciliation_Items',21092,null,3090);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_3_9_Adding_Reconciliation_Groups',21093,null,3091);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_4_Assets_Depreciation_Integration',21094,null,3092);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_4_1_Introduction',21095,null,3093);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_4_2_Set_up_Depreciation_Components',21096,'w_reg_main/uo_reg_hist_depr_int',3094);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_4_3_Set_Up_Balance_Sheet_Roll_Forward_Activity',21097,null,3095);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_4_4_Set_Up_Depreciation_Family_Members',21098,null,3096);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_4_5_Map_Depreciation_Ledger_to_Components',21099,null,3097);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_4_6_Map_Depreciation_Ledger_to_Balance_Sheet_Roll_Forward_Activity',21101,null,3098);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_4_7_Creating_Regulatory_Accounts_from_Asset_Depr_Integration',21103,null,3099);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_CR_Integration',21104,null,3100);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_1_Introduction',21105,null,3101);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_10_Adding_an_Overall_Where_Clause',21106,null,3102);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_2_CR_Mapping_Considerations',21107,'w_reg_main/uo_reg_hist_cr_int_grid_map',3103);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_3_Identifying_CR_Elements',21108,null,3104);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_4_Mapping_CR_Combinations_to_Regulatory_Accounts',21109,null,3105);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_5_Creating_new_Regulatory_Accounts_for_CR',21110,null,3106);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_6_Copying_a_CR_Combination',21111,null,3107);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_7_Using_a_Range_of_Values_for_mapping_to_Regulatory_Accounts',21112,null,3108);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_8_Using_a_List_of_Values_for_mapping_to_Regulatory_Account',21113,null,3109);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_5_9_Changing_a_Regulatory_Account_for_an_Existing_Mapping',21114,null,3110);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_Project_CWIP_Integration',21115,null,3111);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_1_Introduction',21116,null,3112);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_2_Project_CWIP_Mapping_Considerations',21117,'w_reg_main/uo_reg_hist_cwip_int',3113);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_3_Identifying_CWIP_Elements',21118,null,3114);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_4_Setup_Components',21119,null,3115);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_5_Setup_Family_Members',21120,null,3116);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_6_Copy_Family_Member_Records',21121,null,3117);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_7_Map_Components_to_Family_Members',21122,null,3118);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_6_8_Creating_Regulatory_Accounts_from_Project_CWIP_Integration',21123,null,3119);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_7_Deferred_Tax_PowerTax_Integration',21124,null,3120);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_7_1_Introduction',21125,null,3121);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_7_2_PowerTax_Components',21126,'w_reg_main/uo_reg_hist_tax_int',3122);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_7_3_PowerTax_Family_Members',21127,null,3123);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_7_4_Map_Components_to_Family_Members',21128,null,3124);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_7_5_Creating_Regulatory_Accounts_from_PowerTax',21129,null,3125);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_7_6_PowerTax_Split_for_Provision',21130,null,3126);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_Tax_Provision_Integration',21131,null,3127);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_1_Introduction',21132,null,3128);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_10_Creating_New_Reg_Accounts',21133,null,3129);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_2_Tax_Provision_�_Setup',21134,'w_reg_main/uo_reg_hist_ta_setup',3130);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_3_Tax_Provision_�_Timing_Items',21135,null,3131);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_4_Tax_Provision_�_Timing_Items_Components',21136,'w_reg_main/uo_reg_hist_ta_def_int',3132);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_5_Tax_Provision_�_Family_Members',21137,null,3133);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_6_Tax_Provision_�_Timing_Items_�_Map_Components_to_Family_Members',21138,null,3134);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_7_Creating_Regulatory_Accounts_from_Tax_Provision_�_Timing_Difference_Deferred_Integration',21139,null,3135);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_8_Tax_Provision_�_Other_Tax_Items',21140,null,3136);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_8_9_Mapping_Tax_Provision_M_Items_to_Regulatory_Accounts',21141,'w_reg_main/uo_reg_hist_prov_int',3137);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_External_Source_Integration',21142,null,3138);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_1_Introduction',21143,'w_reg_main/uo_reg_ext_src_design_ws',3139);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_10_External_Import',21144,null,3140);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_11_Load_External_Transactions',21145,null,3141);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_2_Create_New_External_Source_Definition',21146,null,3142);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_3_Copy_an_External_Source_Definition',21147,null,3143);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_4_External_Translate_Workspace',21148,'w_reg_main/uo_reg_ext_trans_ws',3144);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_5_Mapping_External_Source_Data_to_Regulatory_Company',21149,null,3145);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_6_Mapping_External_Source_Data_to_Regulatory_Account',21150,null,3146);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_7_Creating_new_Regulatory_Accounts_for_External_Source',21151,null,3147);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_8_Copying_an_External_Source_Translate_Combination',21152,null,3148);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','5_9_9_Using_a_Range_of_External_Source_Values_for',21153,null,3149);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_1_Introduction5',21154,null,3150);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_2_Forecast_Ledgers',21155,null,3151);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_2_1_Introduction',21156,'w_reg_main/uo_reg_fcst_version',3152);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_2_2_Add_New_Forecast_Ledger',21157,null,3153);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_2_3_Copy_a_Forecast_Ledger',21158,null,3154);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_2_4_Delete_a_Forecast_Ledger',21159,null,3155);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_3_Forecast_Assets_Depreciation_Integration',21160,null,3156);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_3_1_Introduction',21161,null,3157);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_3_2_Set_up_Depreciation_Components',21162,'w_reg_main/uo_reg_fcst_depr_int',3158);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_3_3_Set_Up_Balance_Sheet_Roll_Forward_Activity',21163,null,3159);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_3_4_Set_Up_Depreciation_Family_Members',21164,null,3160);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_3_5_Map_Forecast_Depreciation_Ledger_to_Components',21165,null,3161);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_3_6_Map_Forecast_Depreciation_Ledger_to_Balance_Sheet_Roll_Forward_Activity',21166,null,3162);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_3_7_Creating_Regulatory_Account_from_Fcst_Asset_Depr_Integration',21167,null,3163);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_4_Capital_Budget_Integration',21168,null,3164);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_4_1_Introduction',21169,'w_reg_main/uo_reg_fcst_cap_bdg_int',3165);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_4_2_Capital_Budget_Mapping_Considerations',21170,null,3166);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_4_3_Identifying_Capital_Budget_Elements',21171,null,3167);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_4_4_Setup_Capital_Budget_Components',21172,null,3168);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_4_5_Setup_Capital_Budget_Family_Members',21173,null,3169);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_4_6_Map_Capital_Budget_Components_to_Family_Members',21174,null,3170);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_4_7_Creating_Regulatory_Accounts_from_Capital_Budget_Integration',21175,null,3171);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_5_O_M_Budget_CR_Integration',21176,'w_reg_main/uo_reg_fcst_cr_int_grid_map',3172);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_5_1_Introduction_SS5_',21177,null,3173);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_5_2_Setup_Budget_Elements',21178,null,3174);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_5_3_Mapping_Budget_Combinations_to_Regulatory_Accounts',21179,null,3175);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_5_4_Setup_new_Regulatory_Accounts',21180,null,3176);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_6_Forecast_Deferred_Tax_PowerTax_and_Provision_Integration',21181,null,3177);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','6_6_1_Introduction',21182,null,3178);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_1_Introduction5',21183,null,3179);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_2_View_Regulatory_Ledger',21184,null,3180);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_2_1_Overview',21185,null,3181);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_2_2_Query_the_Historic_Ledger',21186,null,3182);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_2_3_Drillback_to_Source_System',21187,null,3183);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_3_Load_Regulatory_Ledgers',21189,null,3184);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_3_1_Introduction',21190,'w_reg_main/uo_reg_hist_dashboard',3185);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_3_2_Run_Interfaces_Monitor_View',21191,null,3186);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_3_3_Execute_Mass_Run',21192,null,3187);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_3_4_Execute_Single_PowerPlan_Integration',21193,null,3188);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_3_5_Execute_External_Source_Integration',21194,null,3189);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_4_Reconcile_History_Ledger',21195,null,3190);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_4_1_Overview',21196,'w_reg_main/uo_reg_hist_recon_review',3191);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_4_2_Perform_Reconciliation',21197,null,3192);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_4_3_Reviewing_Reconciliation_Results',21198,null,3193);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_4_4_Add_a_Reconciliation_Adjustment',21199,null,3194);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_4_5_Copy_Prior_Month_Reconciliation_Adjustment',21200,null,3195);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_4_6_Ignore_the_Reconciliation_Difference',21201,null,3196);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_4_7_Mark_History_Ledger_as_Reconciled',21202,null,3197);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_5_Regulatory_Ledger_Adjustments',21203,null,3198);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_5_1_Introduction',21204,'w_reg_main/uo_reg_ledger_adjustment_ws',3199);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_5_2_Adding_Ledger_Adjustment_from_Regulatory_Ledger_Query',21205,null,3200);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_5_3_Viewing_and_Editing_a_Ledger_Adjustment',21206,'w_reg_main/uo_reg_ledger_adjust_manage_ws_hist',3201);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_5_4_Adding_Ledger_Adjustments_Manually',21207,null,3202);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_5_5_Copying_Ledger_Adjustments',21209,null,3203);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_5_6_Delete_a_Ledger_Adjustment',21210,null,3204);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_Incremental_Forecast_Adjustments',21211,null,3205);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_1_Introduction',21212,null,3206);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_2_Adding_Funding_Project_Incremental_Forecast_Adjustment',21213,'w_reg_main/uo_reg_inc_adj_ws',3207);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_3_Adding_Forecast_Depreciation_Version_Incremental_Forecast_Adjustment',21215,'w_reg_main/uo_reg_fcst_depr_inc_adj_ws',3208);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_4_Overriding_Regulatory_Account_Translation',21216,null,3209);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_5_Reviewing_Incremental_Forecast_Adjustments',21217,'w_reg_main/uo_reg_inc_ifa_fp_review',3210);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_6_Relating_Incremental_Forecast_Adjustments',21218,null,3211);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_7_Adding_Incremental_Forecast_Adjustment_Groups',21219,'w_reg_main/uo_reg_inc_group_ws',3212);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','7_6_8_Copying_Incremental_Forecast_Adjustment_Groups',21220,null,3213);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_1_Introduction3',21221,null,3214);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_Query_Workspace',21222,'w_reg_main/uo_reg_query_ws',3215);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_1_Running_an_Regulatory_Query',21223,null,3216);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_2_Running_a_Select_Query',21224,null,3217);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_3_Running_a_Summarize_Query',21225,null,3218);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_4_Running_a_Report_Query',21226,null,3219);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_5_Running_a_Saved_Query',21227,null,3220);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_6_Saving_a_Query',21228,null,3221);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_7_Query_Criteria',21229,null,3222);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_2_8_Query_Results�Right_Click_Menu',21230,null,3223);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_3_Report_Workspace',21231,'w_reg_main/uo_reg_workspace_reports',3224);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_3_1_Navigation_to_Reports_Workspace',21232,null,3225);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_3_2_Running_Reports',21233,null,3226);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','8_3_3_Saving_Printing_Emailing_Exporting_Reports',21234,null,3227);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','Chapter_1_Regulatory_Management_Suite_Overview',21235,null,3228);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','Chapter_2_RMS_and_the_Regulatory_and_Asset_Management_Cycles',21236,null,3229);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','Chapter_3_Regulatory_Management_Suite_Workspace_Layout',21237,null,3230);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','Chapter_4_General_Setup',21238,null,3231);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','Chapter_5_Historic_Integration',21239,null,3232);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','Chapter_6_Forecast_Integration',21240,null,3233);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','Chapter_7_Transaction_Processing',21241,null,3234);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID) values ('#define','regulatory','Chapter_8_Reports_and_Charts',21242,null,3235);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1585, 0, 10, 4, 3, 0, 40459, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040459_sys_pp_htmlhelp_pt2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON
