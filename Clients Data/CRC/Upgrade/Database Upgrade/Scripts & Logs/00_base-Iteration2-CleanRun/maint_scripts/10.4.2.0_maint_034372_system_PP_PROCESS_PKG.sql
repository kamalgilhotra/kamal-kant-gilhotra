/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034372_system_PP_PROCESS_PKG.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/29/2014 Stephen Motter
||============================================================================
*/

create or replace package PP_PROCESS_PKG is
  --||============================================================================
  --|| Application: PowerPlan
  --|| Object Name: pp_process_pkg
  --|| Description:
  --||============================================================================
  --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
  --||============================================================================
  --|| Version  Date       Revised By          Reason for Change
  --|| -------- ---------- ------------------- -----------------------------------
  --|| 1.0      03/20/2013 Charlie Shilling    Original Version
  --|| 1.3      12/05/2013 Stephen Motter      maint 34370 - Concurrent Processing
  --||============================================================================
  type STRING_ARRAY is table of varchar2(32767);

  function PP_SET_PROCESS(A_ARG varchar2, A_ARG2 varchar2) return varchar2;

  function PP_CHECK_PROCESS(A_ARG varchar2) return varchar2;

  function PP_CHECK_PROCESS_PREFIX(A_ARG varchar2) return varchar2;

end PP_PROCESS_PKG;
/

create or replace package body PP_PROCESS_PKG is
  -- =============================================================================
  --  Function PP_SET_PROCESS
  -- =============================================================================
  function PP_SET_PROCESS(A_ARG varchar2, A_ARG2 varchar2) return varchar2 as
    /*
    ||============================================================================
    || Application: PowerPlant
    || Object Name: PP_SET_PROCESS
    || Description:
    ||============================================================================
    || Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
    ||============================================================================
    || Version Date       Revised By       Reason for Change
    || ------- ---------- ---------------- -----------------------------------------
    || 1.1                                 Added a check for null argument
    || 1.2     08/30/2013 Charlie Shilling maint 31807
    || 1.3     12/05/2013 Stephen Motter   maint 34370 - Concurrent Processing
    ||============================================================================
    */

    PRAGMA AUTONOMOUS_TRANSACTION;

    SID                varchar(60);
    IDX                number(22, 0);
    CHECK_STRING       varchar2(32767);
    PROCESS_STRING     varchar2(32767);
    CHECK_STRING_ARR   string_array;
    PROCESS_STRING_ARR string_array;
    STATUS             varchar2(35);
    I                  number;
    J                  number;

  begin
    --Clean up previous runs
    delete from pp_processes_running
     where sid not in
           (select sid from GV$SESSION where STATUS <> 'KILLED');

    STATUS := 'OK';

    --Special case to remove the process (note: null/empty string different from space)
    begin
      if A_ARG is null then
        delete from pp_processes_running
         where sid = (select sid from v$mystat where rownum = 1);
        commit;
        return 'OK';
      end if;
    exception
      when others then
        RAISE_APPLICATION_ERROR(-20041,
                                'Error removing running process ! Msg = ' ||
                                sqlerrm(sqlcode) || ' code=' ||
                                TO_CHAR(sqlcode));
        STATUS := 'Error';
    end;

    --Check for unexpected behavior
    if instr(A_ARG, ';') > 0 then
      STATUS := 'Error';
    end if;

    --Lock the process temporarily while we check the table
    if STATUS = 'OK' then
      select count(*)
        into IDX
        from pp_processes_running
       where sid = (select sid from v$mystat where rownum = 1);
      if IDX = 0 and trim(A_ARG) is not null then
        insert into pp_processes_running
          (sid, process_name_string)
          select sid, trim(A_ARG)
            from v$mystat
           where rownum = 1;
      elsif trim(A_ARG) is not null then
        select trim(process_name_string)
          into PROCESS_STRING
          from pp_processes_running
         where sid = (select sid from v$mystat where rownum = 1);
        if length(PROCESS_STRING) > 0 then
          update pp_processes_running
             set process_name_string = trim(A_ARG) || ';' ||
                                       PROCESS_STRING
           where sid = (select sid from v$mystat where rownum = 1);
        else
          update pp_processes_running
             set process_name_string = trim(A_ARG)
           where sid = (select sid from v$mystat where rownum = 1);
        end if;
      end if;
      commit;
    end if;

    --Parse check string array
    if STATUS = 'OK' then
      IDX              := 1;
      CHECK_STRING     := A_ARG2;
      CHECK_STRING_ARR := string_array();
      while length(CHECK_STRING) > 0 loop
        if instr(CHECK_STRING, ';') > 0 then
          CHECK_STRING_ARR.extend(1);
          CHECK_STRING_ARR(IDX) := trim(substr(CHECK_STRING,
                                               1,
                                               instr(CHECK_STRING, ';') - 1));
          CHECK_STRING := substr(CHECK_STRING, instr(CHECK_STRING, ';') + 1);
          IDX := IDX + 1;
        else
          CHECK_STRING_ARR.extend(1);
          CHECK_STRING_ARR(IDX) := trim(CHECK_STRING);
          CHECK_STRING := '';
        end if;
      end loop;
    end if;

    --Parse process string array
    if STATUS = 'OK' then
      IDX                := 1;
      PROCESS_STRING_ARR := string_array();
      for process in (select process_name_string
                        from pp_processes_running
                       where sid <>
                             (select sid from v$mystat where rownum = 1)) loop
        PROCESS_STRING := process.process_name_string;
        while length(PROCESS_STRING) > 0 loop
          if instr(PROCESS_STRING, ';') > 0 then
            PROCESS_STRING_ARR.extend(1);
            PROCESS_STRING_ARR(IDX) := trim(substr(PROCESS_STRING,
                                                   1,
                                                   instr(PROCESS_STRING, ';') - 1));
            PROCESS_STRING := substr(PROCESS_STRING,
                                     instr(PROCESS_STRING, ';') + 1);
            IDX := IDX + 1;
          else
            PROCESS_STRING_ARR.extend(1);
            PROCESS_STRING_ARR(IDX) := trim(PROCESS_STRING);
            PROCESS_STRING := '';
          end if;
        end loop;
      end loop;
    end if;

    --Check to make sure the process isn't locked
    if STATUS = 'OK' then
     i := CHECK_STRING_ARR.FIRST;
      while i is not null loop
        --don't run the inner loop if we don't have to
        if instr(CHECK_STRING_ARR(i), '%') > 0 then
        j := PROCESS_STRING_ARR.FIRST;
        while j is not null loop
          if CHECK_STRING_ARR(i) like PROCESS_STRING_ARR(j) then
              STATUS := 'Error';
              EXIT;
            end if;
         j := PROCESS_STRING_ARR.NEXT(j);
        end loop;
        else
          if CHECK_STRING_ARR(i) member of
           PROCESS_STRING_ARR then
            STATUS := 'Error';
          end if;
        end if;
      i := CHECK_STRING_ARR.NEXT(i);
      end loop;
    end if;

    --Remove the temporary process lock if there were errors
    if STATUS <> 'OK' then
      select trim(process_name_string)
        into PROCESS_STRING
        from pp_processes_running
       where sid = (select sid from v$mystat where rownum = 1);

      if PROCESS_STRING = trim(A_ARG) then
        delete from pp_processes_running
         where sid = (select sid from v$mystat where rownum = 1);
      else
        update pp_processes_running
           set process_name_string = substr(PROCESS_STRING,
                                            instr(PROCESS_STRING, ';') + 1)
         where sid = (select sid from v$mystat where rownum = 1);
      end if;
    end if;

    commit;

    return STATUS;

  exception
    when others then
      RAISE_APPLICATION_ERROR(-20041,
                              'Error setting process lock ! Msg = ' ||
                              sqlerrm(sqlcode) || ' code=' ||
                              TO_CHAR(sqlcode));
      return 'Error';
  end;

  -- =============================================================================
  --  Function PP_CHECK_PROCESS
  -- =============================================================================
  function PP_CHECK_PROCESS(A_ARG varchar2) return varchar2 as

    /*
    ||============================================================================
    || Application: PowerPlant
    || Object Name: PP_CHECK_PROCESS
    || Description:
    ||============================================================================
    || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
    ||============================================================================
    || Version Date       Revised By      Reason for Change
    || ------- ---------- --------------- ----------------------------------------
    || 1.0     03/13/2013 Alex Pivoshenko Create
    || 1.1     08/29/2013 C. Shilling     maint 31807
    || 1.3     12/05/2013 Stephen Motter  **FUNCTION DEPRECIATED**
    ||============================================================================
    */

  begin
    return PP_SET_PROCESS(' ', A_ARG);
  end PP_CHECK_PROCESS;

  -- =============================================================================
  --  Function PP_CHECK_PROCESS_PREFIX
  -- =============================================================================
  function PP_CHECK_PROCESS_PREFIX(A_ARG varchar2) return varchar2 as

    /*
    ||============================================================================
    || Application: PowerPlant
    || Object Name: PP_CHECK_PROCESS_PREFIX
    || Description:
    ||============================================================================
    || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
    ||============================================================================
    || Version  Date       Revised By      Reason for Change
    || -------- ---------- --------------- ---------------------------------------
    || 10.4.1.0 08/01/2013 Charlie Shilling Create
    || 1.3      12/05/2013 Stephen Motter  **FUNCTION DEPRECIATED**
    ||============================================================================
    */

  begin
    return PP_SET_PROCESS(' ', A_ARG || '%');
  end PP_CHECK_PROCESS_PREFIX;
end PP_PROCESS_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (927, 0, 10, 4, 2, 0, 34372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034372_system_PP_PROCESS_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;