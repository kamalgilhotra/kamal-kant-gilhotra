/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039911_taxrpr_rpt_descs.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/11/2014 Alex P.
||============================================================================
*/

update PP_REPORTS
   set DESCRIPTION = 'UOP: Replacement Quantity/Cost',
       LONG_DESCRIPTION = 'UOP: Displays the replacement quantities and cost for each circuit and repair unit of property.'
 where REPORT_NUMBER = 'UOP - 2340';

update PP_REPORTS
   set DESCRIPTION = 'UOP: Replacement Qty/Cost Change',
       LONG_DESCRIPTION = 'UOP: Displays the change in replacement quantities and cost for each circuit and repair unit of property.'
 where REPORT_NUMBER = 'UOP - 2360';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1411, 0, 10, 4, 3, 0, 39911, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039911_taxrpr_rpt_descs.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
