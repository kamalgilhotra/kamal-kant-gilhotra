 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045701_pwrtax_import_add_type_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 05/11/2016 Rob Burns      Initial script to update tax additions column types
 ||============================================================================
 */ 

update pp_import_column
set column_type = 'monthnum'
where import_type_id = 212
and column_name in ('end_month','start_month','month_number')
and column_type = 'number(22,0)';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3187, 0, 2016, 1, 0, 0, 045701, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045701_pwrtax_import_add_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;