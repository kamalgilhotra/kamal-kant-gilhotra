/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042635_pcm_auth_options_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/02/2015 Ryan Oliveria    Porting FP Auth System Controls to System Options
||============================================================================
*/


/**************** System Options Workspace ************************/
insert into PPBASE_WORKSPACE (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID)
values ('pcm', 'config_sysoptions', 'System Options', 'uo_pcm_config_wksp_sysoptions', 'System Options', 1);

insert into PPBASE_MENU_ITEMS (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values ('pcm', 'config_sysoptions', 2, 11, 'System Options', 'System Options', 'configuration', 'config_sysoptions', 1);



/**************** Use Approval Defaults ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Funding Proj Use Approval Defaults',
		 long_description,
		 0, 'No', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'funding proj use approval defaults';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Funding Proj Use Approval Defaults',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'funding proj use approval defaults'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('Funding Proj Use Approval Defaults', 'Yes');
insert into ppbase_system_options_values (system_option_id, option_value) values ('Funding Proj Use Approval Defaults', 'No');

insert into ppbase_system_options_module (system_option_id, module) values ('Funding Proj Use Approval Defaults', 'pcm');



/**************** FP: Multiple Approvers Required ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WOAPPR FP : MULT : ONE REQUIRED',
		 long_description,
		 0, 'All', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'woappr fp : mult : one required';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WOAPPR FP : MULT : ONE REQUIRED',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'woappr fp : mult : one required'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ( 'WOAPPR FP : MULT : ONE REQUIRED', 'All');
insert into ppbase_system_options_values (system_option_id, option_value) values ( 'WOAPPR FP : MULT : ONE REQUIRED', 'One');

insert into ppbase_system_options_module (system_option_id, module) values ('WOAPPR FP : MULT : ONE REQUIRED', 'pcm');



/**************** WO: Multiple Approvers Required ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WOAPPR WO : MULT : ONE REQUIRED',
		 long_description,
		 0, 'All', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'woappr wo : mult : one required';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WOAPPR WO : MULT : ONE REQUIRED',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'woappr wo : mult : one required'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('WOAPPR WO : MULT : ONE REQUIRED', 'All');
insert into ppbase_system_options_values (system_option_id, option_value) values ('WOAPPR WO : MULT : ONE REQUIRED', 'One');

insert into ppbase_system_options_module (system_option_id, module) values ('WOAPPR WO : MULT : ONE REQUIRED', 'pcm');



/**************** Workflow User Object ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Workflow User Object',
		 long_description,
		 0, 'Yes', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'workflow user object';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Workflow User Object',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'workflow user object'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('Workflow User Object', 'Yes');
insert into ppbase_system_options_values (system_option_id, option_value) values ('Workflow User Object', 'No');

insert into ppbase_system_options_module (system_option_id, module) values ('Workflow User Object', 'pcm');




/**************** Subs Review - Use Workflow ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Subs Review - Use Workflow',
		 long_description,
		 0, 'Yes', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'subs review - use workflow';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Subs Review - Use Workflow',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'subs review - use workflow'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('Subs Review - Use Workflow', 'Yes');
insert into ppbase_system_options_values (system_option_id, option_value) values ('Subs Review - Use Workflow', 'No');

insert into ppbase_system_options_module (system_option_id, module) values ('Subs Review - Use Workflow', 'pcm');



/**************** Appr Delegation - Max Day Span ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Appr Delegation-Max Day Span',
		 long_description,
		 0, 'Yes', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'appr delegation-max day span';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Appr Delegation-Max Day Span',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'appr delegation-max day span'
   and company_id <> -1;

insert into ppbase_system_options_module (system_option_id, module) values ('Appr Delegation-Max Day Span', 'pcm');



/**************** Workflow User Object-Budg Review ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Workflow User Object-Budg Review',
		 long_description,
		 0, 'Yes', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'workflow user object-budg review';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Workflow User Object-Budg Review',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'workflow user object-budg review'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('Workflow User Object-Budg Review', 'Yes');
insert into ppbase_system_options_values (system_option_id, option_value) values ('Workflow User Object-Budg Review', 'No');

insert into ppbase_system_options_module (system_option_id, module) values ('Workflow User Object-Budg Review', 'pcm');



/**************** Budget Review : One Required ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'BUDGET REVIEW: ONE REQUIRED',
		 long_description,
		 0, 'All', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'budget review: one required';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'BUDGET REVIEW: ONE REQUIRED',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'budget review: one required'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('BUDGET REVIEW: ONE REQUIRED', 'All');
insert into ppbase_system_options_values (system_option_id, option_value) values ('BUDGET REVIEW: ONE REQUIRED', 'One');

insert into ppbase_system_options_module (system_option_id, module) values ('BUDGET REVIEW: ONE REQUIRED', 'pcm');




/**************** Subs Review : One Required ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'SUBS REVIEW: ONE REQUIRED',
		 long_description,
		 0, 'All', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'subs review: one required';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'SUBS REVIEW: ONE REQUIRED',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'subs review: one required'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('SUBS REVIEW: ONE REQUIRED', 'All');
insert into ppbase_system_options_values (system_option_id, option_value) values ('SUBS REVIEW: ONE REQUIRED', 'One');

insert into ppbase_system_options_module (system_option_id, module) values ('SUBS REVIEW: ONE REQUIRED', 'pcm');




/**************** Max Num of Relations per WO ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Max Num of Relations per WO',
		 long_description,
		 0, '100', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'max num of relations per wo';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Max Num of Relations per WO',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'max num of relations per wo'
   and company_id <> -1;

insert into ppbase_system_options_module (system_option_id, module) values ('Max Num of Relations per WO', 'pcm');



/**************** Tax Expense User Object ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Tax Expense User Object',
		 long_description,
		 0, 'Yes', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'tax expense user object';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Tax Expense User Object',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'tax expense user object'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('Tax Expense User Object', 'Yes');
insert into ppbase_system_options_values (system_option_id, option_value) values ('Tax Expense User Object', 'No');

insert into ppbase_system_options_module (system_option_id, module) values ('Tax Expense User Object', 'pcm');



/**************** WOEST - Header from wo_estimate ************************/
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WOEST - Header from wo_estimate',
		 long_description,
		 0, 'No', control_value, 1, 1
  from pp_system_control
 where lower(trim(control_name)) = 'woest - header from wo_estimate';

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WOEST - Header from wo_estimate',
		 company_id,
		 control_value
  from pp_system_control_company
 where lower(trim(control_name)) = 'woest - header from wo_estimate'
   and company_id <> -1;

insert into ppbase_system_options_values (system_option_id, option_value) values ('WOEST - Header from wo_estimate', 'Yes');
insert into ppbase_system_options_values (system_option_id, option_value) values ('WOEST - Header from wo_estimate', 'No');

insert into ppbase_system_options_module (system_option_id, module) values ('WOEST - Header from wo_estimate', 'pcm');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2248, 0, 2015, 1, 0, 0, 042635, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042635_pcm_auth_options_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;