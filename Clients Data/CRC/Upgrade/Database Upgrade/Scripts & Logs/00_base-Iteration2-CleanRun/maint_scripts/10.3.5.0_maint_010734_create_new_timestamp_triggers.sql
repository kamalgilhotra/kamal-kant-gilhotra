/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010734_create_new_timestamp_triggers.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/26/2012 Lee Quinn      Point Release
||============================================================================
*/

SET ECHO ON
SET SERVEROUTPUT ON
SET LINESIZE 300
SET TIME ON
SET SQLPROMPT '_CONNECT_IDENTIFIER> '

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: create_new_timestamp_triggers.sql
|| Description: Replace all user/timestamp triggers with new syntax
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version   Date       Revised By     Reason for Change
|| --------  ---------- -------------- -----------------------------------------
|| 10.3.5.0  07/26/2012 Lee Quinn      Created
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT      number;
   V_NEW_RCOUNT  number;
   V_DROP_RCOUNT number;
   V_RERRORS     number;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
   -- Log the start of the run
   insert into PWRPLANT.PP_UPDATE_FLEX
      (COL_ID, TYPE_NAME, FLEXVCHAR1, FLEXVCHAR2)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'create_new_timestamp_triggers.sql',
             'Status = Running'
        from PWRPLANT.PP_UPDATE_FLEX;
   commit;

   V_RERRORS     := 0;
   V_DROP_RCOUNT := 0;
   V_NEW_RCOUNT  := 0;
   V_RCOUNT      := 0;

   DBMS_OUTPUT.PUT_LINE('/* Drop User/Timestamp Triggers on TEMP Tables */');

   for CSR_DROP_DDL in (select 'DROP TRIGGER ' || TRIGGER_NAME DROP_DDL
                          from ALL_TRIGGERS T1, ALL_TABLES T2
                         where T1.TABLE_OWNER = 'PWRPLANT'
                           and T2.OWNER = 'PWRPLANT'
                           and T2.TABLE_NAME = T1.TRIGGER_NAME
                           and T2.TEMPORARY = 'Y'
                           and T1.TRIGGER_TYPE = 'BEFORE EACH ROW'
                           and T1.TRIGGERING_EVENT = 'INSERT OR UPDATE'
                           and T1.TABLE_NAME not in ('TEMP_WORK_ORDER', 'TEMP_BUDGET')
                         order by TRIGGER_NAME)
   loop
      begin
         -- If you don't want to automatically DROP the triggers then comment
         -- out the following line "execute immediate" and the script will just
         -- return the DDL that would have been run.
         execute immediate CSR_DROP_DDL.DROP_DDL;
         V_DROP_RCOUNT := V_DROP_RCOUNT + 1;
         DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_DROP_DDL.DROP_DDL || ';');
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL > ' || CSR_DROP_DDL.DROP_DDL);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;

   if V_DROP_RCOUNT < 1 then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'No triggers needed to be dropped.');
   end if;

   DBMS_OUTPUT.PUT_LINE('/* Create User/Timestamp Triggers */');

   for CSR_CREATE_DDL in (select 'create or replace trigger PWRPLANT."' || ATST.TABLE_NAME || '"' ||
                                 CHR(13) || '   before update or insert on PWRPLANT."' ||
                                 ATST.TABLE_NAME || '"' || CHR(13) || '   for each row' || CHR(13) ||
                                 'begin ' ||
                                 '   :new.user_id := SYS_CONTEXT (''USERENV'', ''SESSION_USER''); ' ||
                                 '   :new.time_stamp := SYSDATE; ' || 'end;' CREATE_DDL,
                                 'create or replace trigger PWRPLANT."' || ATST.TABLE_NAME || '"' ||
                                 CHR(13) LINE1,
                                 '   before update or insert on PWRPLANT."' || ATST.TABLE_NAME || '"' ||
                                 CHR(13) LINE2,
                                 '   for each row' || CHR(13) LINE3,
                                 'begin ' || CHR(13) LINE4,
                                 '   :new.user_id := SYS_CONTEXT (''USERENV'', ''SESSION_USER''); ' ||
                                 CHR(13) LINE5,
                                 '   :new.time_stamp := SYSDATE; ' || CHR(13) LINE6,
                                 'end;' LINE7,
                                 NVL(TRIGGER_EXISTS, 'FALSE') TRIGGER_EXISTS
                            from ((select T.TABLE_NAME
                                     from ALL_TAB_COLUMNS C, ALL_TABLES T
                                    where T.OWNER = 'PWRPLANT'
                                      and T.OWNER = C.OWNER
                                      and COLUMN_NAME = 'TIME_STAMP'
                                      and T.TABLE_NAME = C.TABLE_NAME
                                      and T.TEMPORARY <> 'Y'
                                   intersect
                                   select T.TABLE_NAME
                                     from ALL_TAB_COLUMNS C, ALL_TABLES T
                                    where T.OWNER = 'PWRPLANT'
                                      and T.OWNER = C.OWNER
                                      and COLUMN_NAME = 'USER_ID'
                                      and T.TABLE_NAME = C.TABLE_NAME
                                      and T.TEMPORARY <> 'Y') union all
                                  (select T.TABLE_NAME
                                     from ALL_TAB_COLUMNS C, ALL_TABLES T
                                    where T.OWNER = 'PWRPLANT'
                                      and T.OWNER = C.OWNER
                                      and COLUMN_NAME = 'TIME_STAMP'
                                      and T.TABLE_NAME = C.TABLE_NAME
                                      and T.TEMPORARY = 'Y'
                                      and T.TABLE_NAME in ('TEMP_WORK_ORDER', 'TEMP_BUDGET')
                                   intersect
                                   select T.TABLE_NAME
                                     from ALL_TAB_COLUMNS C, ALL_TABLES T
                                    where T.OWNER = 'PWRPLANT'
                                      and T.OWNER = C.OWNER
                                      and COLUMN_NAME = 'USER_ID'
                                      and T.TABLE_NAME = C.TABLE_NAME
                                      and T.TEMPORARY = 'Y'
                                      and T.TABLE_NAME in ('TEMP_WORK_ORDER', 'TEMP_BUDGET'))) ATST,
                                 (select AT.TABLE_NAME, 'TRUE' TRIGGER_EXISTS
                                    from ALL_TRIGGERS AT
                                   where AT.OWNER = 'PWRPLANT'
                                     and AT.TABLE_NAME = AT.TRIGGER_NAME) AT
                           where ATST.TABLE_NAME = AT.TABLE_NAME(+)
                           order by TRIGGER_EXISTS DESC, AT.TABLE_NAME)
   loop
      begin
         V_RCOUNT := V_RCOUNT + 1;
         -- If you don't want to automatically create the triggers then comment
         -- out the following line "execute immediate" and the script will just
         -- return the DDL that would have been run.
         execute immediate CSR_CREATE_DDL.CREATE_DDL;
         if CSR_CREATE_DDL.TRIGGER_EXISTS = 'FALSE' then
            DBMS_OUTPUT.PUT_LINE('--***** THIS IS A NEW TRIGGER.');
            V_NEW_RCOUNT := V_NEW_RCOUNT + 1;
         end if;
         DBMS_OUTPUT.PUT_LINE('--' || PPCSQL);
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE1);
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE2);
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE3);
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE4);
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE5);
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE6);
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE7);
         DBMS_OUTPUT.PUT_LINE('/');
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL > ');
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE1);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE2);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE3);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE4);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE5);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE6);
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_DDL.LINE7);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_DROP_RCOUNT || ' TABLE TRIGGER(s) dropped.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' TABLE Trigger(s) created.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_NEW_RCOUNT || ' TABLE Trigger(s) are new.');
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' Errors in Create Triggers.');

   update PWRPLANT.PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete', 
          FLEXVCHAR3 = V_DROP_RCOUNT || ' Table TRIGGER(s) dropped.',
          FLEXVCHAR4 = V_RCOUNT || ' Table TRIGGER(s) created.', 
	  FLEXVCHAR5 = V_RERRORS || ' ERROR(s)',
          FLEXVCHAR6 = V_NEW_RCOUNT || ' New TRIGGER(s) created.'
    where TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PWRPLANT.PP_UPDATE_FLEX
                      where FLEXVCHAR1 = 'create_new_timestamp_triggers.sql'
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;
end;
/

/*
|| END Create User/Timespamp Triggers
*/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (193, 0, 10, 3, 5, 0, 10734, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010734_create_new_timestamp_triggers.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
