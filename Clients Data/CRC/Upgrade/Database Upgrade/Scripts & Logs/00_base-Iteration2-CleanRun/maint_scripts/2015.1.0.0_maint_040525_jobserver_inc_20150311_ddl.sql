/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_040525_jobserver_web_keywords_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/03/2014 Chad Theilman    	 Creating new table 'PP_WEB_KEYWORD'
||==========================================================================================
*/

SET SERVEROUTPUT ON

begin
   execute immediate 'CREATE TABLE PP_WEB_KEYWORD
              (
                ID NUMBER(16,0) CONSTRAINT PP_WEB_KEYWORD01 NOT NULL ENABLE, 
                KEYWORD VARCHAR2(50) CONSTRAINT PP_WEB_KEYWORD002 NOT NULL ENABLE,
                DATA VARCHAR2(1000),
                SQL VARCHAR2(2000),
                CODE VARCHAR2(250),
                CONSTRAINT PP_WEB_KEYWORD_PK PRIMARY KEY (ID)
              )';
   DBMS_OUTPUT.PUT_LINE('Table PP_WEB_KEYWORD created.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table PP_WEB_KEYWORD already exists table create priv not granted.');
end;
/


begin
   EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_WEB_KEYWORD_SEQ START WITH 1 NOCACHE';
   DBMS_OUTPUT.PUT_LINE('sequence PP_WEB_KEYWORD_SEQ created.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Sequence PP_WEB_KEYWORD_SEQ already exists sequence create priv not granted.');
end;
/
COMMENT ON TABLE PP_WEB_KEYWORD IS 'PP Web Keyword holds the keywords that can be used as arguments, part of SQL, and user inputs as part of the PowerPlan Job Scheduler.';
COMMENT ON COLUMN PP_WEB_KEYWORD.ID IS 'Primary record identifier';
COMMENT ON COLUMN PP_WEB_KEYWORD.KEYWORD IS 'The keyword that will be replaced';
COMMENT ON COLUMN PP_WEB_KEYWORD.DATA IS 'The data to replace the keyword with';
COMMENT ON COLUMN PP_WEB_KEYWORD.SQL IS 'The sql to replace the keyword with';
COMMENT ON COLUMN PP_WEB_KEYWORD.CODE IS 'The code to run whose return value will be replace the keyword';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2485, 0, 2015, 1, 0, 0, 40525, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040525_jobserver_inc_20150311_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;