/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042809_pcm_menu_items_03_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 02/13/2015 		B.Beck			Modify Left Hand Navigation
||==========================================================================================
*/

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'wo_pend_auth', 2, 3, 'Pending Authorizations', 
	'Pending Work Order Authorizations', 'work_orders', '', 1
);

delete
from ppbase_menu_items
where module = 'pcm'
and menu_identifier in ('wo_close', 'wo_forecast');

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier in ('wo_close', 'wo_forecast');

update ppbase_menu_items
set label = 'Estimation', item_order = 5
where module = 'pcm'
and menu_identifier = 'wo_estimate';

update ppbase_menu_items
set label = 'Justification', item_order = 2
where module = 'pcm'
and menu_identifier = 'wo_justify';

update ppbase_menu_items
set label = 'Authorization', item_order = 3
where module = 'pcm'
and menu_identifier = 'wo_authorize';

update ppbase_menu_items
set label = 'Monitoring', item_order = 4
where module = 'pcm'
and menu_identifier = 'wo_monitor';


delete from ppbase_menu_items
where module = 'pcm'
and menu_identifier in ('wo_maint_billings', 'wo_maint_other');

delete from ppbase_workspace
where module = 'pcm'
and workspace_identifier in ('wo_maint_billings', 'wo_maint_other');

update ppbase_menu_items
set item_order = item_order + 10
where module = 'pcm'
and parent_menu_identifier = 'wo_maintain'
and item_order > 1;

update ppbase_menu_items
set item_order = 2, label = 'Accounting'
where module = 'pcm'
and menu_identifier = 'wo_maint_accounts';

update ppbase_menu_items
set item_order = 3
where module = 'pcm'
and menu_identifier = 'wo_maint_departments';

update ppbase_menu_items
set item_order = 4
where module = 'pcm'
and menu_identifier = 'wo_maint_contacts';

update ppbase_menu_items
set item_order = 5, label = 'Tasks'
where module = 'pcm'
and menu_identifier = 'wo_maint_job_tasks';

update ppbase_menu_items
set item_order = 6
where module = 'pcm'
and menu_identifier = 'wo_maint_class_codes';

update ppbase_menu_items
set item_order = 7
where module = 'pcm'
and menu_identifier = 'wo_maint_tax_status';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2281, 0, 2015, 1, 0, 0, 042809, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042809_pcm_menu_items_03_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;