/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033547_lease_department.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/21/2013 Kyle Peterson
||============================================================================
*/

alter table LS_IMPORT_ASSET
   add (DEPARTMENT_XLATE varchar2(254),
        DEPARTMENT_ID    number(22,0));

comment on column LS_IMPORT_ASSET.DEPARTMENT_XLATE is 'Translation field for determining department.';
comment on column LS_IMPORT_ASSET.DEPARTMENT_ID is 'The department.';

alter table LS_IMPORT_ASSET_ARCHIVE
   add (DEPARTMENT_XLATE varchar2(254),
        DEPARTMENT_ID    number(22,0));

comment on column LS_IMPORT_ASSET_ARCHIVE.DEPARTMENT_XLATE is 'Translation field for determining department.';
comment on column LS_IMPORT_ASSET_ARCHIVE.DEPARTMENT_ID is 'The department.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'department_id',
           'Department ID',
           'department_xlate',
           1,
           1,
           'number(22,0)',
           'department',
           1,
           'department_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: Leased Asset%');

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL,
    DERIVED_AUTOCREATE_YN)
   (select max(IMPORT_LOOKUP_ID) + 1,
           'Department.Description',
           'The passed in value corresponds to the Department: Description field.  Translate to the Department ID using the Description column on the Department table.',
           'department_id',
           '( select dp.department_id from department dp where upper( trim( <importfield> ) ) = upper( trim( dp.description ) ) )',
           0,
           'department',
           'description',
           null,
           null,
           null
      from PP_IMPORT_LOOKUP);

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL,
    DERIVED_AUTOCREATE_YN)
   (select max(IMPORT_LOOKUP_ID) + 1,
           'Department.External_Department_Code',
           'The passed in value corresponds to the Department: External Department Code field.  Translate to the Department ID using the External Department Code column on the Department table.',
           'department_id',
           '( select dp.department_id from department dp where upper( trim( <importfield> ) ) = upper( trim( dp.external_department_code ) ) )',
           0,
           'department',
           'external_department_code',
           null,
           null,
           null
      from PP_IMPORT_LOOKUP);

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'department_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: Leased Asset%'
       and PPIL.DESCRIPTION like 'Department.Desc%');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'department_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: Leased Asset%'
       and PPIL.DESCRIPTION like 'Department.Ext%');

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select distinct PPIT.IMPORT_TEMPLATE_ID,
                    61,
                    PPITY.IMPORT_TYPE_ID,
                    'department_id',
                    PICL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TEMPLATE        PPIT,
           PP_IMPORT_TYPE            PPITY,
           PP_IMPORT_LOOKUP          PICL,
           PP_IMPORT_TEMPLATE_FIELDS
     where PPITY.DESCRIPTION like 'Add: Leased Asset%'
       and PPIT.DESCRIPTION like 'Leased Asset Add'
       and PICL.DESCRIPTION like 'Department.Description'
       and PPITY.IMPORT_TYPE_ID = PPIT.IMPORT_TYPE_ID)

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (721, 0, 10, 4, 1, 1, 33547, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033547_lease_department.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;