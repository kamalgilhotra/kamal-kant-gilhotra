/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034336_lease_table_maint.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   11/26/2013 Ryan Oliveria
||============================================================================
*/

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('floating_rate_bucket', 'ls_rent_bucket_admin', 'status_code', 'p', null,
    'Floating Rate Bucket', null, 8, 0);

update POWERPLANT_COLUMNS
   set COLUMN_NAME = 'include_in_tax_basis'
 where TABLE_NAME = 'ls_rent_bucket_admin'
   and COLUMN_NAME = 'abs(INCLUDE_IN_TAX_BASIS - 2)';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (776, 0, 10, 4, 1, 2, 34336, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_034336_lease_table_maint.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;