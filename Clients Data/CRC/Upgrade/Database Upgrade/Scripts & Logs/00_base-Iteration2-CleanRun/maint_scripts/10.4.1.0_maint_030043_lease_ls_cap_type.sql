/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030043_lease_ls_cap_type.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Brandon Beck   Patch Release
||============================================================================
*/

create table LS_LEASE_CAP_TYPE
(
 LS_LEASE_CAP_TYPE_ID number(22,0),
 DESCRIPTION          varchar2(35),
 LONG_DESCRIPTION     varchar2(254),
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 BOOK_SUMMARY_ID      number(22,0)
);

alter table LS_LEASE_CAP_TYPE
   add constraint PK_LS_LEASE_CAP_TYPE
       primary key (LS_LEASE_CAP_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_LEASE_CAP_TYPE
   add constraint FK1_LS_TYPE_BK_SUMMARY
       foreign key (LS_LEASE_CAP_TYPE_ID)
       references book_summary (BOOK_SUMMARY_ID);

alter table LS_LEASE add LS_LEASE_CAP_TYPE_ID number(22,0);

alter table LS_LEASE
   add constraint FK1_LS_LEASE_LS_TYPE_ID
       foreign key (LS_LEASE_CAP_TYPE_ID)
       references LS_LEASE_CAP_TYPE (LS_LEASE_CAP_TYPE_ID);

alter table LS_ILR add LS_LEASE_CAP_TYPE_ID number(22,0);

alter table LS_ILR
   add constraint FK1_LS_ILR_LS_TYPE_ID
       foreign key (LS_LEASE_CAP_TYPE_ID)
       references LS_LEASE_CAP_TYPE (LS_LEASE_CAP_TYPE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (433, 0, 10, 4, 1, 0, 30043, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030043_lease_ls_cap_type.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
