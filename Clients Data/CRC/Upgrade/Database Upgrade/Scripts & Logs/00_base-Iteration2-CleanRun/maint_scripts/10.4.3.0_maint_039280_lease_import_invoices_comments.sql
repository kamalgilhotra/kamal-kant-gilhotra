/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_039280_lease_import_invoices_comments.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 08/12/2014 Daniel Motter  Add table and column comments
||============================================================================
*/

comment on table LS_PAYMENT_TYPE is 'Import table used to match payment descriptions to their type IDs';
comment on column LS_PAYMENT_TYPE.PAYMENT_TYPE_ID is 'Identifier used to specify payment type';
comment on column LS_PAYMENT_TYPE.DESCRIPTION is 'Description of the payment type';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1343, 0, 10, 4, 3, 0, 39280, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039280_lease_import_invoices_comments.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;