/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048372_lessee_1018_update_report_time_option_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/25/2017 Josh Sandler     Set report time option to Currency + Span for Lessee - 1018 report
||============================================================================
*/

UPDATE pp_reports
SET pp_report_time_option_id = 204
WHERE report_number = 'Lessee - 1018';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3607, 0, 2017, 1, 0, 0, 48372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048372_lessee_1018_update_report_time_option_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  
  