/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052875_lessee_02_tax_calc_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 02/05/2019 Sarah Byers    Convert existing monthly tax data
||============================================================================
*/
set serveroutput on;

-- Convert tax data into new tables
declare
  L_OPEN_MONTH date;
  L_NUMBER number;
  L_BUCKET_COUNTER NUMBER;
  L_BUCKET  LS_RENT_BUCKET_ADMIN%rowtype;
  L_SQLS varchar2(30000);
begin
  DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
  
  begin
    select count(*)
      into L_BUCKET_COUNTER
      from ls_rent_bucket_admin
     where tax_expense_bucket = 1;
  exception
    when NO_DATA_FOUND then
      DBMS_OUTPUT.PUT_LINE('There must be exactly 1 bucket defined as the tax bucket in LS_RENT_BUCKET_ADMIN to convert taxes.'); 
      DBMS_OUTPUT.PUT_LINE('No Tax Buckets found, so no conversion will occur.'); 
      return;
  end;
        
  if L_BUCKET_COUNTER > 1 then
    RAISE_APPLICATION_ERROR(-20000, 'There must be exactly 1 bucket defined as the tax bucket in LS_RENT_BUCKET_ADMIN.'); 
  elsif L_BUCKET_COUNTER = 0 then
     DBMS_OUTPUT.PUT_LINE('No Tax Buckets found, so no conversion will occur.'); 
     return;
  end if;

  -- Get the tax bucket information
  select *
    into L_BUCKET
    from LS_RENT_BUCKET_ADMIN
   where TAX_EXPENSE_BUCKET = 1;   

  -- Loop over the companies in ls_process_control to convert taxes from ls_monthly_tax to ls_monthly_tax_conversion
  for ITEM in (select distinct a.company_id 
                 from ls_ilr i
                 join ls_asset a on a.ilr_id = i.ilr_id
                 join ls_ilr_options o on o.ilr_id = i.ilr_id 
                                      and o.revision = a.approved_revision
                 join ls_asset_tax_map m on m.ls_asset_id = a.ls_asset_id
                 join ls_tax_local l on l.tax_local_id = m.tax_local_id
                where m.status_code_id = 1)
  loop
    DBMS_OUTPUT.PUT_LINE('Starting conversion for Company ID '||to_char(ITEM.COMPANY_ID)||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS')); 
    -- Get the Open Month
    select min(gl_posting_mo_yr)
      into L_OPEN_MONTH
      from ls_process_control
     where company_id = ITEM.COMPANY_ID
        and lam_closed is null;
        
    DBMS_OUTPUT.PUT_LINE('Open Month is '||to_char(L_OPEN_MONTH)||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS')); 
    
    DBMS_OUTPUT.PUT_LINE('  Converting data prior to open month'); 
    -- Copy the data from ls_monthly_tax directly into ls_monthly_tax_conversion for 
    -- all months prior to L_OPEN_MONTH
    insert into ls_monthly_tax_conversion (
      ls_asset_id, set_of_books_id, schedule_month, gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
      tax_base, tax_rate, payment_amount, accrual_amount, adjustment_amount)
    select ls_asset_id, set_of_books_id, schedule_month, gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
           tax_base, rate, sum(payment_amount), sum(accrual_amount), sum(adjustment_amount)
      from (
              select ls_asset_id, set_of_books_id, schedule_month, gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
                     tax_base, rate, amount payment_amount, 0 accrual_amount, adjustment_amount
                from ls_monthly_tax
               where accrual = 0
                 and gl_posting_mo_yr < L_OPEN_MONTH
              union all
              select ls_asset_id, set_of_books_id, schedule_month, gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
                     tax_base, rate, 0 payment_amount, amount accrual_amount, adjustment_amount
                from ls_monthly_tax
               where accrual = 1
                 and gl_posting_mo_yr < L_OPEN_MONTH
           )
     group by ls_asset_id, set_of_books_id, schedule_month, gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
              tax_base, rate;
              
    DBMS_OUTPUT.PUT_LINE('  Calculating tax payment amount from open month forward'||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS')); 
    -- Do the correct calculations for all months starting from L_OPEN_MONTH forward
    -- Calculate the payment amount first
    for ASSET_RANGE in (
      with ilr_asset as (
          select /*+ MATERIALIZE */ distinct a.ls_asset_id
            from ls_ilr i
            join ls_asset a on a.ilr_id = i.ilr_id
            join ls_ilr_options o on o.ilr_id = i.ilr_id
                                  and o.revision = a.approved_revision
            join ls_asset_tax_map m on m.ls_asset_id = a.ls_asset_id
            join ls_tax_local l on l.tax_local_id = m.tax_local_id
            where a.company_id = ITEM.company_id
              and (a.ls_asset_status_id in (3,5)
                  or
                  (a.ls_asset_status_id = 4 and a.retirement_date >= L_OPEN_MONTH)
                  )
              and m.status_code_id = 1
        )
        select fv + (nvl(lag(ls_asset_id,1) over(order by rownum),fv - 1) - fv) + 1 start_id, ls_asset_id end_id 
          from (
            select first_value(ls_asset_id) over() fv, last_value(ls_asset_id) over () lv, ls_asset_id, 
                   row_number() over(order by ls_asset_id) rn 
              from ilr_asset)
        where mod(rn,500) = 0
        union
        select max(ls_asset_id) + 1, max(lv) from (
          select first_value(ls_asset_id) over() fv, last_value(ls_asset_id) over () lv, ls_asset_id, 
                 row_number() over(order by ls_asset_id) rn 
            from ilr_asset)
        where mod(rn,500) = 0
    )                
    loop
      DBMS_OUTPUT.PUT_LINE('  - Processing Asset Range '||to_char(ASSET_RANGE.start_id)||' to '||to_char(ASSET_RANGE.end_id)||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS')); 
      insert into ls_monthly_tax_conversion (
        ls_asset_id, set_of_books_id, schedule_month, gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
        tax_base, tax_rate, payment_amount, adjustment_amount)
      with ilr_asset as (
             select /*+ MATERIALIZE */ i.ilr_id, i.ilr_number, i.lease_id, 
                    -1 * nvl(o.payment_shift,0) payment_shift,
                    a.ls_asset_id, a.approved_revision, a.ls_asset_status_id, a.retirement_date,
                    a.tax_asset_location_id, m.tax_local_id, a.company_id,
                    l.pay_lessor,
                    a.actual_purchase_amount,
                    nvl(a.actual_termination_amount,0) actual_termination_amount,
                    nvl(a.guaranteed_residual_amount,0) -  nvl(a.actual_residual_amount, nvl(a.guaranteed_residual_amount,0)) net_residual
               from ls_ilr i
               join ls_asset a on a.ilr_id = i.ilr_id
               join ls_ilr_options o on o.ilr_id = i.ilr_id 
                                    and o.revision = a.approved_revision
               join ls_asset_tax_map m on m.ls_asset_id = a.ls_asset_id
               join ls_tax_local l on l.tax_local_id = m.tax_local_id
              where a.company_id = ITEM.COMPANY_ID
                and (a.ls_asset_status_id in (3,5) 
                     or
                     (a.ls_asset_status_id = 4 and a.retirement_date >= L_OPEN_MONTH)
                    )
                and m.status_code_id = 1
                and a.ls_asset_id between ASSET_RANGE.start_id and ASSET_RANGE.end_id
           )
      select t.ls_asset_id,
             t.set_of_books_id,
             t.month schedule_month,
             add_months(t.month, t.shift) gl_posting_month,
             t.tax_local_id,
             t.vendor_id,
             t.tax_district_id,
             sum(t.amount) tax_base,
             t.rate,
             sum(t.rate * t.amount) amount,
             0 adjustment_amount
        from (
                select /*+ NO_MERGE */ las.month,
                       r.rate,
                       r.ls_asset_id,
                       r.tax_local_id,
                       las.set_of_books_id,
                       r.tax_district_id,
                       v.payment_pct * (
                         las.interest_paid + las.principal_paid
                             + nvl(b.cont1 * las.contingent_paid1, 0) + nvl(b.cont2 * las.contingent_paid2, 0)
                             + nvl(b.cont3 * las.contingent_paid3, 0) + nvl(b.cont4 * las.contingent_paid4, 0)
                             + nvl(b.cont5 * las.contingent_paid5, 0) + nvl(b.cont6 * las.contingent_paid6, 0)
                             + nvl(b.cont7 * las.contingent_paid7, 0) + nvl(b.cont8 * las.contingent_paid8, 0)
                             + nvl(b.cont9 * las.contingent_paid9, 0) + nvl(b.cont10 * las.contingent_paid10, 0)
                             + nvl(b.exec1 * las.executory_paid1, 0) + nvl(b.exec2 * las.executory_paid2, 0)
                             + nvl(b.exec3 * las.executory_paid3, 0) + nvl(b.exec4 * las.executory_paid4, 0)
                             + nvl(b.exec5 * las.executory_paid5, 0) + nvl(b.exec6 * las.executory_paid6, 0)
                             + nvl(b.exec7 * las.executory_paid7, 0) + nvl(b.exec8 * las.executory_paid8, 0)
                             + nvl(b.exec9 * las.executory_paid9, 0) + nvl(b.exec10 * las.executory_paid10, 0)
                             + nvl(las.residual_amount, 0) + nvl(las.term_penalty, 0)
                             + nvl(las.bpo_price, 0)) amount,
                       a.payment_shift shift,
                       decode(a.pay_lessor, 0, -1, v.vendor_id) vendor_id
                  from ls_asset_schedule las
                  join ilr_asset a on a.ls_asset_id = las.ls_asset_id
                                  and a.approved_revision = las.revision
                  join (
                         select /*+ NO_MERGE */ ls_asset_id from ilr_asset
                         minus
                         select ls_asset_id from ls_payment_line
                          where gl_posting_mo_yr = L_OPEN_MONTH
                            and payment_id in (
                                select payment_id from ls_payment_hdr
                                 where gl_posting_mo_yr = L_OPEN_MONTH
                                   and ((PAYMENT_STATUS_ID = 2 or PAYMENT_STATUS_ID = 3)
                                         or payment_id in (select payment_id from ls_invoice_payment_map where in_tolerance = 1)))
                       ) ra on ra.ls_asset_id = a.ls_asset_id
                  join ls_lease l on l.lease_id = a.lease_id
                  join ls_lease_options llo on llo.lease_id = a.lease_id AND llo.revision = l.current_revision
                  join ls_lease_vendor v on v.lease_id = a.lease_id
                                        and v.company_id = a.company_id
                  join (     
                          select sum(rates.rate) rate,
                                 rates.ls_asset_id ls_asset_id,
                                 rates.tax_local_id tax_local_id,
                                 rates.approved_revision revision,
                                 rates.tax_loc_id as tax_district_id,
                                 rates.effective_date eff_date,
                                 rates.next_date next_eff_date
                            from ( 
                                    select /*+ NO_MERGE */ r.rate, r.effective_date, r.next_date, r.tax_local_id, r.state_id tax_loc_id,
                                           a.ls_asset_id, a.approved_revision
                                      from ilr_asset a
                                      join (
                                             select s.tax_local_id, s.rate, trim(l.state_id) state_id, s.effective_date,
                                                    l.asset_location_id,
                                                    nvl(lead(s.effective_date) 
                                                          over (partition by s.tax_local_id, trim(s.state_id), l.asset_location_id order by s.effective_date), 
                                                        to_date('999912','YYYYMM')) next_date
                                               from ls_tax_state_rates s
                                               join asset_location l on l.state_id = s.state_id
                                           ) r on r.tax_local_id = a.tax_local_id 
                                                            and r.asset_location_id = a.tax_asset_location_id
                                    union all
                                    select /*+ NO_MERGE */ r.rate, r.effective_date, r.next_date, r.tax_local_id, r.tax_district_id tax_loc_id,
                                           a.ls_asset_id, a.approved_revision
                                      from ilr_asset a
                                      join (
                                             select d.tax_local_id, d.rate, to_char(l.tax_district_id) tax_district_id, d.effective_date,
                                                    l.asset_location_id,
                                                    nvl(lead(d.effective_date) 
                                                          over (partition by d.tax_local_id, to_char(l.tax_district_id), l.asset_location_id order by d.effective_date), 
                                                        to_date('999912','YYYYMM')) next_date
                                               from ls_tax_district_rates d
                                               join ls_location_tax_district l on l.tax_district_id = d.ls_tax_district_id
                                           ) r on r.tax_local_id = a.tax_local_id 
                                                            and r.asset_location_id = a.tax_asset_location_id
                                 ) rates
                           group by rates.ls_asset_id, rates.tax_local_id, rates.approved_revision, rates.tax_loc_id,
                                    rates.effective_date, rates.next_date
                       ) r on r.ls_asset_id = las.ls_asset_id
                          and r.revision = las.revision
						  and r.tax_local_id = a.tax_local_id
                          and case 
                               when llo.tax_rate_option_id = 1 then
                                last_day(add_months(las.month, nvl(a.payment_shift,0)))
                               when llo.tax_rate_option_id = 2 then
                                l.master_agreement_date 
                              end between trunc(r.eff_date,'day') and trunc(r.next_eff_date,'day')
                  cross join ls_tax_basis_buckets b
                  left outer join ilr_asset early_term_fees on early_term_fees.ls_asset_id = a.ls_asset_id
                                                           and early_term_fees.approved_revision = a.approved_revision
                                                           and early_term_fees.retirement_date = L_OPEN_MONTH
                 where las.month >= L_OPEN_MONTH
            ) t
       group by t.ls_asset_id, t.tax_local_id, t.month, t.set_of_books_id, add_months(t.month, t.shift), 
                t.vendor_id, t.tax_district_id, t.rate;
                
       DBMS_OUTPUT.PUT_LINE('  - Records inserted: '||SQL%ROWCOUNT); 
       COMMIT;
    end loop;
    
    DBMS_OUTPUT.PUT_LINE('  Gathering Payment Terms at '||to_char(sysdate,'MM/DD/YY HH:MI:SS'));
    insert into ls_tax_conv_pay_terms
    with ilr_asset as (
          select i.ilr_id, i.ilr_number, i.lease_id, a.ls_asset_id, a.approved_revision, l.tax_local_id, ll.pre_payment_sw prepay_switch
            from ls_ilr i
            join ls_asset a on a.ilr_id = i.ilr_id
            join ls_asset_tax_map m on m.ls_asset_id = a.ls_asset_id
            join ls_tax_local l on l.tax_local_id = m.tax_local_id
            JOIN ls_lease ll ON ll.lease_id = i.lease_id
            WHERE a.company_id = ITEM.COMPANY_ID
              and (a.ls_asset_status_id in (3,5) 
                  or
                  (a.ls_asset_status_id = 4 and a.retirement_date >= L_OPEN_MONTH)
                  )
              and m.status_code_id = 1
        )
        select i.ls_asset_id, i.approved_revision, i.lease_id, 
                p.ilr_id, p.payment_term_id, p.payment_term_type_id, p.payment_freq_id, p.number_of_terms, p.payment_term_date,
                i.prepay_switch,
                ROW_NUMBER() over (partition by p.ilr_id order by p.payment_term_date desc) as the_max
          from (select distinct ls_asset_id, approved_revision, lease_id, ilr_id, prepay_switch
                      from ilr_asset) i
          join ls_ilr_payment_term p on p.ilr_id = i.ilr_id
                                    and p.revision = i.approved_revision;
                                    
    DBMS_OUTPUT.PUT_LINE('  - Records inserted: '||SQL%ROWCOUNT); 
    COMMIT;                              
    
    DBMS_OUTPUT.PUT_LINE('  Gathering Payment Months at '||to_char(sysdate,'MM/DD/YY HH:MI:SS'));
    insert into ls_tax_conv_pay_months
    WITH n AS (SELECT /*+ MATERIALIZE */ rownum as the_row from dual connect by level <= 10000)
    SELECT /*+ NO_MERGE */ p.ls_asset_id,
          p.ilr_id, 
          p.approved_revision,
          add_months(trunc(p.payment_term_date, 'MM'), n.the_row - 1) schedule_month,
          p.prepay_switch,
          decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1) months_to_accrue
      from ls_tax_conv_pay_terms p  
      join ls_tax_conv_pay_terms lp on lp.ls_asset_id = p.ls_asset_id
                          and lp.approved_revision = p.approved_revision
                          and lp.ilr_id = p.ilr_id 
      join n on n.the_row <= (p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1))
                            + (case when p.payment_term_id = lp.payment_term_id then 1 else 0 end * decode(p.payment_term_type_id,4,1,0))
    where lp.the_max = 1
      and add_months(trunc(p.payment_term_date, 'MM'), n.the_row - 1) >= L_OPEN_MONTH;
      
    DBMS_OUTPUT.PUT_LINE('  - Records inserted: '||SQL%ROWCOUNT); 
    COMMIT; 

    DBMS_OUTPUT.PUT_LINE('  Calculating tax accrual amounts from open month forward at '||to_char(sysdate,'MM/DD/YY HH:MI:SS'));
    INSERT INTO ls_tax_conv_accrue_months
    select t.ls_asset_id, t.set_of_books_id, t.schedule_month payment_month, 
            t.tax_local_id, t.vendor_id, t.tax_district_id,
            add_months(t.schedule_month, decode(s.prepay_switch, 0, -1, 1) * (s.months_to_accrue -1)) accrue_to_month,
            round(t.payment_amount / case when s.prepay_switch = 0 
                                          and d.first_tax_month between add_months(t.schedule_month, 
                                                                                    -1 * (s.months_to_accrue -1)) 
                                                                    and t.schedule_month then
                                      months_between(t.schedule_month, d.first_tax_month) + 1
                                    else
                                      s.months_to_accrue
                                    end, 2) as accrual_amount,
            t.payment_amount 
              - ( case when s.prepay_switch = 0 
                        and d.first_tax_month between add_months(t.schedule_month, 
                                                                -1 * (s.months_to_accrue -1)) 
                                                  and t.schedule_month then
                    months_between(t.schedule_month, d.first_tax_month) + 1
                  else
                    s.months_to_accrue
                  end
                  * round(t.payment_amount / case when s.prepay_switch = 0 
                                                  and d.first_tax_month between add_months(t.schedule_month, 
                                                                                            -1 * (s.months_to_accrue -1)) 
                                                                            and t.schedule_month then
                                              months_between(t.schedule_month, d.first_tax_month) + 1
                                            else
                                              s.months_to_accrue
                                            end, 2)) as ROUNDER
      from ls_monthly_tax_conversion t
      join ls_tax_conv_pay_months s on s.ls_asset_id = t.ls_asset_id
                                    and s.schedule_month = t.schedule_month 
      join (
              select t.ls_asset_id, t.set_of_books_id, min(t.schedule_month) first_tax_month, 
                    t.tax_local_id, t.vendor_id, t.tax_district_id
              from ls_monthly_tax_conversion t 
              group by t.ls_asset_id, t.set_of_books_id,
                      t.tax_local_id, t.vendor_id, t.tax_district_id
            ) d on d.ls_asset_id = t.ls_asset_id
                              and d.set_of_books_id = t.set_of_books_id
                              and d.tax_local_id = t.tax_local_id
                              and d.vendor_id = t.vendor_id
                              and d.tax_district_id = t.tax_district_id
      where t.payment_amount <> 0;
      
    DBMS_OUTPUT.PUT_LINE('  - Records inserted: '||SQL%ROWCOUNT); 
    COMMIT; 
    
    DBMS_OUTPUT.PUT_LINE('  Merging tax accrual amount from open month forward'||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS')); 
    -- Calculate the accrual amount by straight-lining the tax payment over the payment period
    merge into ls_monthly_tax_conversion tax
    using (
             select c.ls_asset_id, c.set_of_books_id, c.schedule_month, 
                   c.tax_local_id, c.vendor_id, c.tax_district_id,
                   c.payment_amount,
                   am.accrual_amount + 
                     case when c.schedule_month = greatest(am.payment_month, am.accrue_to_month) then
                       am.rounder
                     else
                       0
                     end accrual_amount
              from ls_monthly_tax_conversion c
              join ls_tax_conv_accrue_months am on am.ls_asset_id = c.ls_asset_id
                       and am.set_of_books_id = c.set_of_books_id
                       and am.tax_local_id = c.tax_local_id
                       and am.vendor_id = c.vendor_id
                       and am.tax_district_id = c.tax_district_id
                       and c.schedule_month between least(am.payment_month, am.accrue_to_month) 
                                                and greatest(am.payment_month, am.accrue_to_month)
             where c.schedule_month >= L_OPEN_MONTH
           ) x
     on (    x.ls_asset_id = tax.ls_asset_id
         and x.set_of_books_id = tax.set_of_books_id
         and x.schedule_month = tax.schedule_month
         and x.tax_local_id = tax.tax_local_id
         and x.vendor_id = tax.vendor_id
         and x.tax_district_id = tax.tax_district_id)
     when matched then 
        update set tax.accrual_amount = x.accrual_amount; 
        
    DBMS_OUTPUT.PUT_LINE('  - Records inserted: '||SQL%ROWCOUNT); 
    COMMIT;  
        
    DBMS_OUTPUT.PUT_LINE('  Populating LS_ASSET_SCHEDULE_TAX from open month forward'||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS')); 
    -- Put the records into ls_asset_schedule_tax also
    merge into ls_asset_schedule_tax tax
    using (
             with ilr_asset as (
                   select i.ilr_id, i.ilr_number, i.lease_id, a.ls_asset_id, a.approved_revision, l.tax_local_id
                     from ls_ilr i
                     join ls_asset a on a.ilr_id = i.ilr_id
                     join ls_asset_tax_map m on m.ls_asset_id = a.ls_asset_id
                     join ls_tax_local l on l.tax_local_id = m.tax_local_id
                    where a.company_id = ITEM.COMPANY_ID
                      and (a.ls_asset_status_id in (3,5) 
                           or
                           (a.ls_asset_status_id = 4 and a.retirement_date >= L_OPEN_MONTH)
                          )
                      and m.status_code_id = 1
                 )
             select t.ls_asset_id, s.approved_revision revision, t.set_of_books_id, t.schedule_month,
                    t.gl_posting_mo_yr, t.tax_local_id, t.vendor_id, t.tax_district_id,
                    t.tax_base, t.tax_rate, t.payment_amount, t.accrual_amount
               from ls_monthly_tax_conversion t
               join ilr_asset s on s.ls_asset_id = t.ls_asset_id
                               and s.tax_local_id = t.tax_local_id
          ) x
    on (     x.ls_asset_id = tax.ls_asset_id
         and x.revision = tax.revision
         and x.set_of_books_id = tax.set_of_books_id
         and x.schedule_month = tax.schedule_month
         and x.tax_local_id = tax.tax_local_id
         and x.vendor_id = tax.vendor_id
         and x.tax_district_id = tax.tax_district_id)
    when not matched then
      insert (ls_asset_id, revision, set_of_books_id, schedule_month, gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
              tax_base, tax_rate, payment_amount, accrual_amount)
      values (x.ls_asset_id, x.revision, x.set_of_books_id, x.schedule_month, x.gl_posting_mo_yr, x.tax_local_id, x.vendor_id, x.tax_district_id,
              x.tax_base, x.tax_rate, x.payment_amount, x.accrual_amount);
              
    DBMS_OUTPUT.PUT_LINE('  - Records inserted: '||SQL%ROWCOUNT); 
    COMMIT; 
        
    DBMS_OUTPUT.PUT_LINE('  Updating LS_ASSET_SCHEDULE tax buckets from open month forward'||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS')); 
    -- Update the appropriate buckets on the schedule with the total amounts for the schedule months
    -- Asset Schedule
    L_SQLS:= '
    merge into ls_asset_schedule a
    using (
             with ilr_asset as (
                   select i.ilr_id, i.ilr_number, i.lease_id, a.ls_asset_id, a.approved_revision, l.tax_local_id
                     from ls_ilr i
                     join ls_asset a on a.ilr_id = i.ilr_id
                     join ls_asset_tax_map m on m.ls_asset_id = a.ls_asset_id
                     join ls_tax_local l on l.tax_local_id = m.tax_local_id
                    where a.company_id = ' || to_char(ITEM.COMPANY_ID) || '
                      and (a.ls_asset_status_id in (3,5) 
                           or
                           (a.ls_asset_status_id = 4 and a.retirement_date >= to_date('|| to_char(L_OPEN_MONTH,'YYYYMM') ||', ''yyyymm''))
                          )
                      and m.status_code_id = 1
                 )
             select t.ls_asset_id, s.approved_revision revision, t.set_of_books_id, t.schedule_month,
                    sum(t.payment_amount) payment_amount, 
                    sum(t.accrual_amount) accrual_amount
               from ls_monthly_tax_conversion t
               join ilr_asset s on s.ls_asset_id = t.ls_asset_id
                               and s.tax_local_id = t.tax_local_id
              group by t.ls_asset_id, s.approved_revision, t.set_of_books_id, t.schedule_month
          ) s
    on (    a.ls_asset_id = s.ls_asset_id
        and a.revision = s.revision
        and a.set_of_books_id = s.set_of_books_id
        and a.month = s.schedule_month
       )
    when matched then
      update set a.' ||L_BUCKET.RENT_TYPE || '_paid' || to_char(L_BUCKET.BUCKET_NUMBER) || ' = s.payment_amount,
                 a.' ||L_BUCKET.RENT_TYPE || '_accrual' || to_char(L_BUCKET.BUCKET_NUMBER) || ' = s.accrual_amount';  
                 
    execute immediate L_SQLS;
    
    DBMS_OUTPUT.PUT_LINE('  - Records inserted: '||SQL%ROWCOUNT); 
    COMMIT; 
     
    DBMS_OUTPUT.PUT_LINE('  Updating LS_ILR_SCHEDULE tax buckets from open month forward'||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS')); 
    -- ILR Schedule
    L_SQLS:= '
    merge into ls_ilr_schedule a
    using (
             with ilr_asset as (
                   select i.ilr_id, i.ilr_number, i.lease_id, a.ls_asset_id, a.approved_revision, l.tax_local_id
                     from ls_ilr i
                     join ls_asset a on a.ilr_id = i.ilr_id
                     join ls_asset_tax_map m on m.ls_asset_id = a.ls_asset_id
                     join ls_tax_local l on l.tax_local_id = m.tax_local_id
                    where a.company_id = ' || to_char(ITEM.COMPANY_ID) || '
                      and (a.ls_asset_status_id in (3,5) 
                           or
                           (a.ls_asset_status_id = 4 and a.retirement_date >= to_date('|| to_char(L_OPEN_MONTH,'YYYYMM') ||', ''yyyymm''))
                          )
                      and m.status_code_id = 1
                 )
             select s.ilr_id, s.approved_revision revision, t.set_of_books_id, t.schedule_month,
                    sum(t.payment_amount) payment_amount, 
                    sum(t.accrual_amount) accrual_amount
               from ls_monthly_tax_conversion t
               join ilr_asset s on s.ls_asset_id = t.ls_asset_id
                               and s.tax_local_id = t.tax_local_id
              group by s.ilr_id, s.approved_revision, t.set_of_books_id, t.schedule_month
          ) s
    on (    a.ilr_id = s.ilr_id
        and a.revision = s.revision
        and a.set_of_books_id = s.set_of_books_id
        and a.month = s.schedule_month
       )
    when matched then
      update set a.' ||L_BUCKET.RENT_TYPE || '_paid' || to_char(L_BUCKET.BUCKET_NUMBER) || ' = s.payment_amount,
                 a.' ||L_BUCKET.RENT_TYPE || '_accrual' || to_char(L_BUCKET.BUCKET_NUMBER) || ' = s.accrual_amount';  
    
    execute immediate L_SQLS;  
    
    DBMS_OUTPUT.PUT_LINE('  - Records inserted: '||SQL%ROWCOUNT); 
    
    -- commit when a company has been fully processed
    DBMS_OUTPUT.PUT_LINE('Completed conversion for Company ID '||to_char(ITEM.COMPANY_ID)||' at '||to_char(sysdate,'MM/DD/YY HH:MI:SS'));
    COMMIT; 
    
    -- clear out the helper tables
    DELETE from ls_tax_conv_pay_terms;
    DELETE from ls_tax_conv_pay_months;
    DELETE from ls_tax_conv_accrue_months; 
    COMMIT; 
  
  end loop;
  
  exception
     when others then
        DBMS_OUTPUT.PUT_LINE('ERROR: '||sqlerrm);
        RAISE_APPLICATION_ERROR(-20000, 'ERROR: '||sqlerrm); 
        rollback; 
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13067, 0, 2018, 2, 0, 0, 52875, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052875_lessee_02_tax_calc_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
