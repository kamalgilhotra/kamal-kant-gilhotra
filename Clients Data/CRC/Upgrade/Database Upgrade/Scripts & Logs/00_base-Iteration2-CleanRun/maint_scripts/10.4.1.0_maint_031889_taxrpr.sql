/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031889_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   08/28/2013 Nathan Hollis
||============================================================================
*/

update PP_REPORTS set PP_REPORT_ENVIR_ID = 7 where DATAWINDOW = 'dw_rpr_rpt_cwip_gen_blend';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (552, 0, 10, 4, 1, 0, 31889, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031889_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
