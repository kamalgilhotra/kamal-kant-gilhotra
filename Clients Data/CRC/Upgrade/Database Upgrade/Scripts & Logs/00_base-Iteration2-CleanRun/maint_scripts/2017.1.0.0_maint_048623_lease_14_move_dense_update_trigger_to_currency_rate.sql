/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048623_lease_14_move_dense_update_trigger_to_currency_rate.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/16/2017 Andrew Hill      Currency_rate_default is intended to be system managed;
||                                        all changes should go through currency_rate. Since
||                                        currency_rate has existing row-by-row triggers, this
||                                        would result in poor performance for the refresh process
||                                        (it should run after all updates, not row-by-row) where
||                                        the trigger to remain on currency_rate_default. Moving
||                                        the trigger to currency & currency_rate 
||                                        should mitigate the performance hit.
||============================================================================
*/

drop trigger curr_rate_dflt_refresh_dense;

create or replace trigger curr_refresh_rate_dflt_dense after insert or update or delete on currency
begin
  p_refresh_curr_rate_dflt_dense;
end;
/

create or replace trigger curr_rate_refresh_dflt_dense after insert or update or delete on currency_rate
begin
    p_refresh_curr_rate_dflt_dense;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3661, 0, 2017, 1, 0, 0, 48623, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048623_lease_14_move_dense_update_trigger_to_currency_rate.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;