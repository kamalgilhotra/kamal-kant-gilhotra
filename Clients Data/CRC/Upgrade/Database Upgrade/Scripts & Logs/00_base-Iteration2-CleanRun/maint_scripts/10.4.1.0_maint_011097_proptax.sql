/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011097_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/19/2012 Julia Breuer   Point Release
||============================================================================
*/

update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : Assessments (Parcel)'   where import_type_id = 11;
update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : Esc Value Indices'      where import_type_id = 4;
update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : Levy Rates'             where import_type_id = 5;
update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : Levy Rates (Composite)' where import_type_id = 26;
update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : Parcel History'         where import_type_id = 16;
update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : Parcel-Loc Mappings'    where import_type_id = 38;
update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : Reserve Factor Pcts'    where import_type_id = 28;
update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : Auth-Dist Combos'       where import_type_id = 13;
update PWRPLANT.PP_IMPORT_TYPE SET DESCRIPTION = 'Add/Update : CWIP Type Assignments'  where import_type_id = 12;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (230, 0, 10, 4, 1, 0, 11097, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011097_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
