/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008322_prov_rev2.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/09/2012 Blake Andrews  Point Release
||============================================================================
*/

-- Maint 8322 Rev2
insert into TAX_ACCRUAL_REPORT_OPTION
   (REPORT_OPTION_ID, DESCRIPTION)
   select 32, 'ETR_FOOTNOTE' from DUAL
   union
   select 33, 'ETR_FOOTNOTE_CONS' from DUAL;

update TAX_ACCRUAL_REP_SPECIAL_NOTE set REPORT_OPTION_ID = 32 where DESCRIPTION = 'ETR_FOOTNOTE';

update TAX_ACCRUAL_REP_SPECIAL_NOTE
   set REPORT_OPTION_ID = 33
 where DESCRIPTION = 'ETR_FOOTNOTE_CONS';

insert into TAX_ACCRUAL_REPORT_OPTION_DTL
   (REPORT_OPTION_ID, REPORT_OBJECT_ID)
   select 32, REPORT_OBJECT_ID from TAX_ACCRUAL_REPORT_OPTION_DTL where REPORT_OPTION_ID = 27;

insert into TAX_ACCRUAL_REPORT_OPTION_DTL
   (REPORT_OPTION_ID, REPORT_OBJECT_ID)
   select 33, REPORT_OBJECT_ID from TAX_ACCRUAL_REPORT_OPTION_DTL where REPORT_OPTION_ID = 30;

delete from TAX_ACCRUAL_REPORT_OPTION_DTL
 where REPORT_OPTION_ID in (32, 33)
   and REPORT_OBJECT_ID in (52);

insert into TAX_ACCRUAL_REPORT_OPTION_DTL
   (REPORT_OPTION_ID, REPORT_OBJECT_ID)
   select REPORT_OPTION_ID, REPORT_OBJECT_ID
     from TAX_ACCRUAL_REPORT_OPTION, TAX_ACCRUAL_REPORT_OBJECT
    where REPORT_OPTION_ID in (32, 33)
      and REPORT_OBJECT_ID in (43, 44, 55, 20, 21, 56)
      and (REPORT_OPTION_ID, REPORT_OBJECT_ID) not in
          (select REPORT_OPTION_ID, REPORT_OBJECT_ID from TAX_ACCRUAL_REPORT_OPTION_DTL);

insert into TAX_ACCRUAL_REP_SPECIAL_CRIT
   (SPECIAL_CRIT_ID, SPECIAL_NOTE_ID, DESCRIPTION, LONG_DESCRIPTION)
   select 3, SPECIAL_NOTE_ID, 'NO_STATE_IN_DISCRETE', 'Roll State Discretes in Total State'
     from TAX_ACCRUAL_REP_SPECIAL_NOTE
    where DESCRIPTION = 'ETR_FOOTNOTE';

insert into TAX_ACCRUAL_REP_SPECIAL_CRIT
   (SPECIAL_CRIT_ID, SPECIAL_NOTE_ID, DESCRIPTION, LONG_DESCRIPTION)
   select 4, SPECIAL_NOTE_ID, 'NO_STATE_IN_DISCRETE', 'Roll State Discretes in Total State'
     from TAX_ACCRUAL_REP_SPECIAL_NOTE
    where DESCRIPTION = 'ETR_FOOTNOTE_CONS';

-- to fix grid reporting
update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'total_tax_expense'
 where LABEL_VALUE = 'Tax Expense Booked'
   and REP_CONS_TYPE_ID in (32, 33);

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'comp_dtl_tte_after_discretes_tt + comp_dtl_tte_after_discretes_tt_adj - total_tax_expense',
       VALUE_TYPE = 'COL_VALUE_TRANS'
 where LABEL_VALUE = 'Difference'
   and REP_CONS_TYPE_ID in (32, 33);

-- advanced tab should not be an option for the grid report, though we need the special option for report to run
insert into TAX_ACCRUAL_REPORT_OPTION_DTL
   (REPORT_OPTION_ID, REPORT_OBJECT_ID)
   select REPORT_OPTION_ID, REPORT_OBJECT_ID
     from TAX_ACCRUAL_REPORT_OPTION, TAX_ACCRUAL_REPORT_OBJECT
    where REPORT_OPTION_ID in (33)
      and REPORT_OBJECT_ID in (52)
      and (REPORT_OPTION_ID, REPORT_OBJECT_ID) not in
          (select REPORT_OPTION_ID, REPORT_OBJECT_ID from TAX_ACCRUAL_REPORT_OPTION_DTL);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (265, 0, 10, 4, 0, 0, 8322, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_008322_prov_rev2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;