 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_042131_reg_ext_pull_type_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/13/2015 Shane Ward     Clarify Pull Types
||============================================================================
*/

UPDATE reg_ext_pull_type SET description = 'Load Amounts' ,long_description = 'Load Amounts as is' WHERE pull_type_id = 1;
UPDATE reg_ext_pull_type SET description = 'Build Balances' ,long_description = 'Build Balances from loaded amounts (activities)' WHERE pull_type_id = 2;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2164, 0, 2015, 1, 0, 0, 42131, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042131_reg_ext_pull_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;