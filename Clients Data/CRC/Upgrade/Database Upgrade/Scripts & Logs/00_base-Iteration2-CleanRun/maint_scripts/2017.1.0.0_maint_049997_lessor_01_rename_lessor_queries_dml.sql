/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049997_lessor_01_rename_lessor_queries_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/22/2017 Josh Sandler   Rename Lessor queries
||============================================================================
*/

UPDATE PP_ANY_QUERY_CRITERIA
SET    description = Replace( description, 'Non Disclosure: ', '' )
WHERE  description LIKE 'Non Disclosure: %';

UPDATE PP_ANY_QUERY_CRITERIA
SET    description = 'Lessor ILR by Set of Books (Current Revision)'
WHERE  description = 'ILR by Set of Books (Current Revision) - Lessor'; 



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4004, 0, 2017, 1, 0, 0, 49997, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049997_lessor_01_rename_lessor_queries_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
			 
