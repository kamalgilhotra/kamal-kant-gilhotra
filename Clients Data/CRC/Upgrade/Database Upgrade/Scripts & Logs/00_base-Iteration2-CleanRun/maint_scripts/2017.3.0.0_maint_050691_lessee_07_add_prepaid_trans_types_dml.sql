/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050691_lessee_07_add_prepaid_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/04/2018 Josh Sandler 	  Add prepaid rent trans types
||============================================================================
*/

INSERT INTO je_trans_type (trans_type, description)
SELECT 3060, '3060 - Prepaid Rent Debit' FROM dual
UNION ALL
SELECT 3061, '3061 - Prepaid Rent Adjustment Credit' FROM dual
UNION ALL
SELECT 3062, '3062 - Backdated Prepaid Rent Credit' FROM dual
UNION ALL
SELECT 3063, '3063 - Prepaid Rent Clearing Credit' from dual
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(4303, 0, 2017, 3, 0, 0, 50691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050691_lessee_07_add_prepaid_trans_types_dml.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

