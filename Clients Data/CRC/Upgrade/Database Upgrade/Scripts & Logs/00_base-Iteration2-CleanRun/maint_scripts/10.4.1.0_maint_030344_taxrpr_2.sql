/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030344_taxrpr_2.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   07/25/2013 Alex P.         Point Release
||============================================================================
*/


update PP_REPORTS
   set PP_REPORT_FILTER_ID = 1
 where REPORT_TYPE_ID = 100
   and PP_REPORT_FILTER_ID is null;

delete PPBASE_MENU_ITEMS where module = 'REPAIRS';
delete PPBASE_WORKSPACE where module = 'REPAIRS';

insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_tax_status', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Tax Status', 'uo_rpr_config_wksp_tax_status', 'Tax Status Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_range_test', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Range Test', 'uo_rpr_config_wksp_range_test', 'Range Test Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_tax_loc_rollups', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Repair Location Rollups', 'uo_rpr_config_wksp_rpr_loc_rollup', 'Repair Location Rollup Configuration for Blanket Allocations');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_tax_units_work_order', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Tax Units of Property (Gen)', 'uo_rpr_config_wksp_unit_code', 'Tax Units of Property Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_repair_schema', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Repair Schema', 'uo_rpr_config_wksp_repair_schema', 'Repair Schema Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_reports', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Reports', 'uo_rpr_rptcntr_wksp_report', 'Reports');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_tax_locations', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Repair Locations', 'uo_rpr_config_wksp_rpr_location', 'Repair Location Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_threshold_estimate', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Threshold Estimate', 'uo_rpr_config_wksp_threshold_estimate', 'Thresholds Estimate Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_tax_units_asset', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Tax Units of Property (UOP)', 'uo_rpr_config_wksp_unit_code', 'Tax Units of Property Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_tax_exp_tests', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Repair Tests', 'uo_rpr_config_wksp_repair_test', 'Repair Test Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_import', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Import', 'uo_rpr_import_wksp_main', 'Import Center');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_queries', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Queries', 'uo_rpr_rptcntr_wksp_query', 'Queries (launched in external window)');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_book_summary', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Book Summary', 'uo_rpr_config_wksp_book_summary', 'Repair Book Summary Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_tax_thresholds_asset', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Tax Thresholds (UOP)', 'uo_rpr_config_wksp_threshold', 'Tax Threshold Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_set_tax_status', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Set Tax Status', 'uo_rpr_process_wksp_wo_status', 'Work Order Tax Status Update');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_tax_thresholds_work_order', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Tax Thresholds (Gen)', 'uo_rpr_config_wksp_threshold', 'Tax Threshold Configuration');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_home', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Home', 'uo_rpr_home_wksp_main', 'Home');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_processing', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Processing', 'uo_rpr_process_wksp_main', 'Processing Center');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_posting', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Posting', 'uo_rpr_post_wksp_main', 'Posting Center');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_assign_tax_test', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Testing Parameters', 'uo_rpr_process_wksp_wo_tax_repairs', 'Tax Repairs Testing Parameters');
insert into PPBASE_WORKSPACE (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
values ('REPAIRS', 'menu_wksp_wo_balance', null, null, 'Work Order Balance', 'uo_rpr_process_wksp_wo_balance', 'Work Order Balancing');

insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_home', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 1, 1, 'Home', 'Home', null, 'menu_wksp_home', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_config', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 1, 2, 'Config', 'Configuration Center', null, null, 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_processing', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 1, 5, 'Processing', 'Processing Center', null, 'menu_wksp_processing', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_reporting', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 1, 6, 'Reporting', 'Reporting Center', null, null, 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_posting', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 1, 7, 'Posting', 'Posting Center', null, 'menu_wksp_posting', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_wo_setup', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 1, 4, 'Work Order', 'Work Order Setup', null, null, 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_assign_tax_test', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 1, 'Testing Parameters', 'Tax Repairs Testing Parameters', 'menu_wksp_wo_setup', 'menu_wksp_assign_tax_test', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_wo_balance', null, null, 2, 3, 'Work Order Balance', 'Find and correct Work Order out-of-balances', 'menu_wksp_wo_setup', 'menu_wksp_wo_balance', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_work_order', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 5, 'General Method', 'Work Order Configuration Workspaces', 'menu_wksp_config', null, 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_asset', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 6, 'Unit of Property Method', 'Asset Configuration Workspaces', 'menu_wksp_config', null, 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_set_tax_status', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Set Tax Status', 'Work Order Tax Status Update', 'menu_wksp_wo_setup', 'menu_wksp_set_tax_status', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_tax_exp_tests', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 4, 'Repair Tests', 'Repair Test Configuration', 'menu_wksp_config', 'menu_wksp_tax_exp_tests', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_import', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 7, 'Import', 'Import Center', 'menu_wksp_config', 'menu_wksp_import', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_queries', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Queries', 'Queries (launched in external window)', 'menu_wksp_reporting', 'menu_wksp_queries', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_book_summary', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 1, 'Book Summary', 'Repair Book Summary Configuration', 'menu_wksp_config', 'menu_wksp_book_summary', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_repair_schema', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Repair Schema', 'Repair Schema Configuration', 'menu_wksp_config', 'menu_wksp_repair_schema', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_reports', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 1, 'Reports', 'Reports', 'menu_wksp_reporting', 'menu_wksp_reports', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_tax_locations', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 3, 'Repair Locations', 'Repair Location Configuration', 'menu_wksp_config', 'menu_wksp_tax_locations', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_threshold_estimate', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 2, 3, 'Threshold Estimate', 'Thresholds Estimate Configuration', 'menu_wksp_reporting', 'menu_wksp_threshold_estimate', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_tax_status', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 3, 1, 'Tax Status', 'Tax Status Configuration', 'menu_wksp_work_order', 'menu_wksp_tax_status', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_range_test', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 3, 2, 'Range Test', 'Range Test Configuration', 'menu_wksp_work_order', 'menu_wksp_range_test', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_tax_loc_rollups', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 3, 2, 'Repair Location Rollups', 'Repair Location Rollup Configuration for Blanket Allocations', 'menu_wksp_asset', 'menu_wksp_tax_loc_rollups', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_tax_units_work_order', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 3, 3, 'Tax Units of Property (Gen)', 'Tax Units of Property Configuration', 'menu_wksp_work_order', 'menu_wksp_tax_units_work_order', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_tax_units_asset', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 3, 1, 'Tax Units of Property (UOP)', 'Tax Units of Property Configuration', 'menu_wksp_asset', 'menu_wksp_tax_units_asset', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_tax_thresholds_asset', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 3, 3, 'Tax Thresholds (UOP)', 'Tax Threshold Configuration', 'menu_wksp_asset', 'menu_wksp_tax_thresholds_asset', 1);
insert into PPBASE_MENU_ITEMS (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('REPAIRS', 'menu_wksp_tax_thresholds_work_order', to_date('26-06-2013 13:58:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 3, 4, 'Tax Thresholds (Gen)', 'Tax Threshold Configuration', 'menu_wksp_work_order', 'menu_wksp_tax_thresholds_work_order', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (484, 0, 10, 4, 1, 0, 30344, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030344_taxrpr_2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;