/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052400_lessee_reporting_multi_thread_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2018.1.0.0 09/27/2018 Shane "C" Ward    Set Lessee reports to delivered multi-threading switches
||============================================================================
*/

UPDATE PP_REPORTS SET TURN_OFF_MULTI_THREAD = NULL
WHERE REPORT_NUMBER LIKE 'Lessee - 1%';

UPDATE PP_REPORTS SET TURN_OFF_MULTI_THREAD = 1
WHERE REPORT_NUMBER LIKE 'Lessee - 2%';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9942, 0, 2018, 1, 0, 0, 52400, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052400_lessee_reporting_multi_thread_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;