/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033257_depr_trueup_adj.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/08/2013 Kyle Peterson
||============================================================================
*/

alter table CPR_DEPR_CALC_STG add TRUEUP_ADJ number(22,2);

comment on column CPR_DEPR_CALC_STG.TRUEUP_ADJ is 'The true up adjustment';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (689, 0, 10, 4, 1, 1, 33257, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033257_depr_trueup_adj.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
