/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031745_proptax_tbl_maint.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- --------------------------------
|| 10.4.1.0   08/20/2013 Andrew Scott        QA Errors in Prop Tax Table Maint
||============================================================================
*/

----this was "description", which caused an error
update POWERPLANT_DDDW
   set DISPLAY_COL = 'long_description'
 where DROPDOWN_NAME = 'pp_state_filter';

----remove property_tax_authority and pt_report_field_labels from table maint
delete from PP_TABLE_GROUPS
 where LOWER(TABLE_NAME) in ('property_tax_authority', 'pt_report_field_labels');

delete from POWERPLANT_COLUMNS
 where LOWER(TABLE_NAME) in ('property_tax_authority', 'pt_report_field_labels');

delete from POWERPLANT_TABLES
 where LOWER(TABLE_NAME) in ('property_tax_authority', 'pt_report_field_labels');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (525, 0, 10, 4, 1, 0, 31745, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031745_proptax_tbl_maint.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
