/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053051_pwrtax_01_tax_bk_alloc_rpt_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 03/21/2019  K. Powers	   PwrTax Book Depr Allocation Rpt
||============================================================================
*/

-----------------------------
--Remove if existing table
-----------------------------
DECLARE
  table_exists NUMBER(22);
BEGIN
  SELECT Count(1)
  INTO table_exists
  FROM tab
  WHERE tname LIKE 'TAX_BOOK_DEPR_BASIS_DIFFS';
  IF table_exists>0 THEN
     EXECUTE IMMEDIATE 'DROP TABLE TAX_BOOK_DEPR_BASIS_DIFFS';
  END IF;
END;
/

CREATE TABLE tax_book_depr_basis_diffs
  (
    jurisdiction          VARCHAR2(50) NOT NULL,
    book_alloc_group_id   NUMBER(22,0) NOT NULL,
    tax_year              NUMBER(22,2) NOT NULL,
    tax_class_id          NUMBER(22,0),
    vintage_id            NUMBER(22,0),
    normalization_id      NUMBER(22,0),
    basis_amount_end      NUMBER(22,2),
    norm_diff_balance_end NUMBER(22,2),
    depr_pct              NUMBER(22,8),
    calc_depr             NUMBER(22,2),
    allocated_depr        NUMBER(22,2),
    input_amort           NUMBER(22,2),
    basis_diff_add_ret    NUMBER(22,2),
    basis_amount_activity NUMBER(22,2),
    basis_avg             NUMBER(22,2),
    transferred_reserve   NUMBER(22,2),
    jurisdiction_id       NUMBER(22,0),
    basis_amount_beg      NUMBER(22,2),
    alloc_method          VARCHAR2(50),
    time_stamp            DATE,
    user_id               VARCHAR2(18)
  );
  
CREATE INDEX IDX_TAX_BOOK_DEPR_BASIS_DIFFS1 on tax_book_depr_basis_diffs(jurisdiction_id, book_alloc_group_id,tax_year) 
TABLESPACE PWRPLANT_IDX NOLOGGING;

COMMENT ON TABLE tax_book_depr_basis_diffs IS '(S)  [09]
The tax_book_depr_basis_diffs table contains the detail behind the PowerTax book depreciation allocation to basis differences. This table is deleted from and re-populated each time book depreciation is allocated in PowerTax, so the table will only show the results of the most recent run.';

COMMENT ON COLUMN tax_book_depr_basis_diffs.jurisdiction IS 'Description of the jurisdiction which book depreciation was allocated to. Typically will be Federal.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.book_alloc_group_id IS 'System-assigned identifier of the book allocation group.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.tax_year IS 'Tax year for which book depreciation was allocated.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.tax_class_id IS 'System-assigned identifier for each individual  tax class defined by the user.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.vintage_id IS 'System-defined identifier for a unique tax vintage';
COMMENT ON COLUMN tax_book_depr_basis_diffs.normalization_id IS 'System-assigned identifier of a normalization schema.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.basis_amount_end IS 'Ending book-to-tax basis difference in dollars.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.norm_diff_balance_end IS 'Ending timing difference amount in dollars';
COMMENT ON COLUMN tax_book_depr_basis_diffs.depr_pct IS 'System-calculated depreciation rate used when allocating book depreciation.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.calc_depr IS 'Calculated book depreciation based on the depr percentage';
COMMENT ON COLUMN tax_book_depr_basis_diffs.allocated_depr IS 'Allocated book depreciation, generally the same as the calc_depr column';
COMMENT ON COLUMN tax_book_depr_basis_diffs.input_amort IS 'Allocated book depreciation, generally the same as the calc_depr column';
COMMENT ON COLUMN tax_book_depr_basis_diffs.basis_diff_add_ret IS 'Change in a basis difference (for deferral) due to additions or retirements in dollars calculated by the system.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.basis_amount_activity IS 'Changes in book-to-tax basis differences in dollars, generally for additions since this is used as an originating difference for deferred taxes. Note it does not reconcile basis_amount_beg with basis_amount_end, since pro-rated retirement differences are excluded as are input amounts for retirements in basis_amount_input_retire.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.basis_avg IS 'Average basis difference throughout the year for use in allocation, calculated as ((basis amount end + basis amount beg) / 2)';
COMMENT ON COLUMN tax_book_depr_basis_diffs.transferred_reserve IS 'Amount of transferred book reserve';
COMMENT ON COLUMN tax_book_depr_basis_diffs.jurisdiction_id IS 'System-assigned identifier of the jurisdiction';
COMMENT ON COLUMN tax_book_depr_basis_diffs.basis_amount_beg IS 'Beginning book-to-tax basis difference in dollars. (Note that AFUDC will be negative, CIAC positive, etc.).';
COMMENT ON COLUMN tax_book_depr_basis_diffs.alloc_method IS 'Either Net or Gross, stores whether book depreciation was allocated on a Net or a Gross cost basis. The default in PowerTax is Gross, and it can be changed to Net using a system control';
COMMENT ON COLUMN tax_book_depr_basis_diffs.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN tax_book_depr_basis_diffs.user_id IS 'Standard System-assigned user id used for audit purposes.';

-----------------------------
--Remove if existing table
-----------------------------
DECLARE
  table_exists NUMBER(22);
BEGIN
  SELECT Count(1)
  INTO table_exists
  FROM tab
  WHERE tname LIKE 'TAX_BOOK_DEPR_METHOD_LIFE';
  IF table_exists>0 THEN
     EXECUTE IMMEDIATE 'DROP TABLE TAX_BOOK_DEPR_METHOD_LIFE';
  END IF;
END;
/

CREATE TABLE TAX_BOOK_DEPR_METHOD_LIFE
  (
    jurisdiction                 VARCHAR2(50) NOT NULL,
    book_alloc_group_id          NUMBER(22,0) NOT NULL,
    tax_class_id                 NUMBER(22,0),
    vintage_id                   NUMBER(22,0),
    in_service_month             DATE,
    tax_balance                  NUMBER(22,2),
    accum_reserve                NUMBER(22,2),
    percent                      NUMBER(22,8),
    alloc_amount                 NUMBER(22,2),
    depr                         NUMBER(22,2),
    additions                    NUMBER(22,2),
    retirements                  NUMBER(22,2),
    depreciation_adjust          NUMBER(22,2),
    gain_loss                    NUMBER(22,2),
    depreciation                 NUMBER(22,2),
    cost_of_removal              NUMBER(22,2),
    est_salvage_pct              NUMBER(22,8),
    transferred_reserve          NUMBER(22,2),
    jurisdiction_jurisdiction_id NUMBER(22,0),
    tax_balance_end              NUMBER(22,2),
    tax_balance_check            NUMBER(22,2),
    cor_res_impact               NUMBER(22,2),
    alloc_method                 VARCHAR2(50),
    beg_alloc_cost               NUMBER(22,2),
    end_alloc_cost               NUMBER(22,2),
    time_stamp                   DATE,
    user_id                      VARCHAR2(18)
  );
  
CREATE INDEX IDX_TAX_BOOK_DEPR_METHOD_LIFE1 on TAX_BOOK_DEPR_METHOD_LIFE(jurisdiction_jurisdiction_id,book_alloc_group_id) 
TABLESPACE PWRPLANT_IDX NOLOGGING;

COMMENT ON TABLE TAX_BOOK_DEPR_METHOD_LIFE IS '(S)  [09]
The tax_book_depr_basis_diffs table contains the detail behind the PowerTax book depreciation allocation to depreciation differences. This table is deleted from and re-populated each time book depreciation is allocated in PowerTax, so the table will only show the results of the most recent run.';

COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.jurisdiction IS 'Description of the jurisdiction which book depreciation was allocated to. Typically will be Federal.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.book_alloc_group_id IS 'System-assigned identifier of the book allocation group.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.tax_class_id IS 'System-assigned identifier for each individual tax class defined by the user.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.vintage_id IS 'System-defined identifier for a unique tax vintage';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.in_service_month IS 'Month/year field that is optional in the tax record key. It would be used for certain depreciation methods (such as for buildings), where the in-service month determines the depreciation rate for each tax year.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.tax_balance IS 'The beginning tax basis balance of the tax book on which book depreciation is allocated. For the book allocation tax book, this generally represents the method/life book balance. Includes any tax balance transferred in or out during the year.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.accum_reserve IS 'The beginning accumulated depreciation on this tax book. Includes any transfers of accumulated depreciation as well as accumulated depreciation adjustments.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.percent IS 'System-calculated depreciation rate used when allocating book depreciation.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.alloc_amount IS 'Allocated book depreciation, based on percent and tax balance. For NET allocation methods, the accumulated reserve is also taken into account.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.depr IS 'Calculated book depreciation, based on percent and the average of tax balance and tax balance end. For NET allocation methods, the accumulated reserve is also taken into account.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.additions IS 'Current year tax basis additions in dollars.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.retirements IS 'Current year tax basis retirements in dollars.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.depreciation_adjust IS 'Current year depreciation adjustments in dollars.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.gain_loss IS 'Current year (ordinary and capital) gain (+) or loss (-) in dollars including the (extraordinary) gain/loss.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.depreciation IS 'Current year depreciation in dollars.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.cost_of_removal IS 'Current year cost of removal dollars, whether directly expensed, in gain/loss, or applied to the reserve.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.est_salvage_pct IS 'Estimated salvage percent entered as a decimal.  (It is after reduction for the ADR exclusion).';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.transferred_reserve IS 'Amount of transferred book reserve';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.jurisdiction_jurisdiction_id IS 'System-assigned identifier of the jurisdiction';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.tax_balance_end IS 'The ending tax basis balance of this tax asset record in dollars, not reduced for ordinary (ADR) retirements.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.tax_balance_check IS 'Comparison of ending tax balance to the ending accumulated reserve';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.cor_res_impact IS 'Current impact of all cost of removal on the reserve for depreciation in dollars.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.alloc_method IS 'Either Net or Gross, stores whether book depreciation was allocated on a Net or a Gross cost basis. The default in PowerTax is Gross, and it can be changed to Net using a system control';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.beg_alloc_cost IS 'The beginning cost on which book depreciation is allocated. Will vary based on user selection of Net or Gross allocation methodology.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.end_alloc_cost IS 'The ending cost on which book depreciation is allocated. Will vary based on user selection of Net or Gross allocation methodology';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN TAX_BOOK_DEPR_METHOD_LIFE.user_id IS 'Standard System-assigned user id used for audit purposes.';

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16243, 0, 2018, 2, 0, 0, 53051, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053051_pwrtax_01_tax_bk_alloc_rpt_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
