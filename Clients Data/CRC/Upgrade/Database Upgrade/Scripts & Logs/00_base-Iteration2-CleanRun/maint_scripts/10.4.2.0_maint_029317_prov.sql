/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029317_prov.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/03/2013 Nathan Hollis  Point Release
||============================================================================
*/

begin
	EXECUTE IMMEDIATE 'alter table TAX_ACCRUAL_VERSION add CR_BUDGET_VERSION_ID number(22,0)';
exception
  when others then
	DBMS_OUTPUT.PUT_LINE('PROVISION: Column already exists. Ok.');
end;
/

comment on column TAX_ACCRUAL_VERSION.CR_BUDGET_VERSION_ID is '(Maint-29317) Link from provision budget case to CR budget data.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (674, 0, 10, 4, 2, 0, 29317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029317_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
