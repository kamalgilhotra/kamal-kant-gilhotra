/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045664_reg_schedule_precan_sql_ddl_010.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 05/11/2016  Shane Ward		Improve Reg Schedule Builder
||============================================================================
*/

ALTER table reg_schedule_column ADD  sql_arg VARCHAR2(200);

COMMENT ON COLUMN reg_schedule_column.sql_arg IS 'Holds arguments for sql needed for execution of schedule that can be saved and not be defined at run time, such as Month Number. ex. <month_1>';

--Views referenced by precanned SQL
--New View Reg Case Composition
CREATE OR REPLACE VIEW reg_case_composition AS (
 select reg_case_id,
       reg_acct_id,
       account_type_id,
       sub_acct_type_id,
       gl_month,
       coverage_yr_ended,
       sum(unadj_amount) unadjusted,
       sum(adj_amount) adjustments,
       Sum(amount) per_books_adjusted
  from (SELECT c.reg_case_id,
               ca.reg_acct_id reg_acct_id,
               ca.reg_acct_type_id account_type_id,
               sa.sub_acct_type_id sub_acct_type_id,
               gl_month,
               coverage_yr_ended,
               nvl(act_amount,0) unadj_amount,
               nvl(adj_amount, 0) adj_amount,
               nvl(act_amount, 0) + nvl(adj_amount, 0) +
               nvl(recon_adj_amount, 0) amount
          FROM reg_history_ledger         hl,
               reg_historic_version       hv,
               reg_jurisdiction           j,
               reg_case                   c,
               reg_case_coverage_versions ccv,
               reg_case_acct              ca,
               reg_acct_master            ram,
               reg_sub_acct_type          sa,
               reg_acct_type              rat
         WHERE j.reg_company_id = hl.reg_company_id
           AND hl.historic_version_id = hv.historic_version_id
           AND j.reg_jurisdiction_id = c.reg_jurisdiction_id
           AND c.reg_case_id = ccv.reg_case_id
           AND hl.historic_version_id = ccv.hist_version_id
           AND ccv.fore_version_id = 0
           AND c.reg_case_id = ca.reg_case_id
           AND ca.reg_acct_id = hl.reg_acct_id
           AND ram.reg_acct_id = ca.reg_acct_id
           AND ca.sub_acct_type_id = sa.sub_acct_type_id
           AND ca.reg_acct_type_id = sa.reg_acct_type_id
           AND rat.reg_acct_type_id = sa.reg_acct_type_id
           AND gl_month BETWEEN CASE
                 WHEN ccv.start_date =
                      to_char(add_months(to_date(coverage_yr_ended, 'YYYYMM'),
                                         -11),
                              'YYYYMM') THEN
                  To_Number(To_Char(Add_Months(To_Date(ccv.start_date,
                                                       'YYYYMM'),
                                               -2),
                                    'YYYYMM'))
                 ELSE
                  ccv.start_date
               END AND ccv.end_date
        UNION
        SELECT c.reg_case_id,
               ca.reg_acct_id reg_acct_id,
               ca.reg_acct_type_id account_type_id,
               sa.sub_acct_type_id sub_acct_type_id,
               gl_month,
               coverage_yr_ended,
               nvl(fcst_amount,0) unadj_amount,
               nvl(adj_amount, 0) adj_amount,
               nvl(fcst_amount, 0) + nvl(adj_amount, 0) +
               nvl(recon_adj_amount, 0) amount
          FROM reg_forecast_ledger        fl,
               reg_forecast_version       fv,
               reg_jurisdiction           j,
               reg_case                   c,
               reg_case_coverage_versions ccv,
               reg_case_acct              ca,
               reg_acct_master            ram,
               reg_sub_acct_type          sa,
               reg_acct_type              rat
         WHERE j.reg_company_id = fl.reg_company_id
           AND fv.forecast_version_id = fl.forecast_version_id
           AND j.reg_jurisdiction_id = c.reg_jurisdiction_id
           AND c.reg_case_id = ccv.reg_case_id
           AND fl.forecast_version_id = ccv.fore_version_id
           AND c.reg_case_id = ca.reg_case_id
           AND ca.reg_acct_id = fl.reg_acct_id
           AND ram.reg_acct_id = ca.reg_acct_id
           AND ca.sub_acct_type_id = sa.sub_acct_type_id
           AND ca.reg_acct_type_id = sa.reg_acct_type_id
           AND rat.reg_acct_type_id = sa.reg_acct_type_id
           AND gl_month BETWEEN CASE
                 WHEN ccv.start_date =
                      to_char(add_months(to_date(coverage_yr_ended, 'YYYYMM'),
                                         -11),
                              'YYYYMM') THEN
                  To_Number(To_Char(Add_Months(To_Date(ccv.start_date,
                                                       'YYYYMM'),
                                               -2),
                                    'YYYYMM'))
                 ELSE
                  ccv.start_date
               END AND ccv.end_date)
 where substr(gl_month, 5, 2) <> 99
 GROUP BY reg_case_id,
          reg_acct_id,
          account_type_id,
          sub_acct_type_id,
          gl_month,
          coverage_yr_ended);

--New View Allocation Results with Tax Control
CREATE OR REPLACE VIEW reg_alloc_results_w_tax (
  reg_case_id,
  reg_acct_id,
  reg_acct_type,
  sub_acct_type,
  tax_indicator,
  reg_alloc_category_id,
  reg_alloc_target_id,
  case_year,
  annual_reg_factor,
  statistical_value,
  allocator,
  reg_allocator_id,
  effective_date,
  alloc_amount,
  parent_target_id,
  alloc_level,
  per_books,
  adj_amount
) AS
(
SELECT x."REG_CASE_ID",x."REG_ACCT_ID",x."REG_ACCT_TYPE",x."SUB_ACCT_TYPE",x."TAX_INDICATOR",x."REG_ALLOC_CATEGORY_ID",x."REG_ALLOC_TARGET_ID",x."CASE_YEAR",x."ANNUAL_REG_FACTOR",x."STATISTICAL_VALUE",x."ALLOCATOR",x."REG_ALLOCATOR_ID",x."EFFECTIVE_DATE",x."ALLOC_AMOUNT",x."PARENT_TARGET_ID",x."ALLOC_LEVEL",
       x.alloc_amount - Nvl(aar.adj_amount, 0) per_books,
       aar.adj_amount adj_amount
  FROM (SELECT y.reg_case_id,
               m.reg_acct_id reg_acct_id,
               t.reg_acct_type_id reg_acct_type,
               s.sub_acct_type_id sub_acct_type,
               c.tax_control_id tax_indicator,
               ac.reg_alloc_category_id reg_alloc_category_id,
               x.reg_alloc_target_id reg_alloc_target_id,
               r.case_year case_year,
               f.annual_reg_factor,
               f.statistical_value,
               NVL(al.description, ' < none > ') allocator,
               al.reg_allocator_id,
               f.effective_date,
               r.amount alloc_amount,
               p.reg_alloc_target_id parent_target_id,
               lev.alloc_level alloc_level
          FROM reg_alloc_result r,
               reg_acct_type t,
               reg_sub_acct_type s,
               reg_tax_control c,
               reg_case_acct a,
               reg_acct_master m,
               reg_case_alloc_account b,
               reg_alloc_target x,
               reg_case y,
               reg_allocator al,
               reg_case_alloc_account p,
               reg_alloc_category ac,
               (SELECT reg_allocator_id,
                       reg_case_id,
                       reg_alloc_category_id,
                       reg_alloc_target_id,
                       effective_date,
                       annual_reg_factor,
                       statistical_value
                  FROM reg_case_alloc_factor af
                UNION
                SELECT reg_allocator_id,
                       reg_case_id,
                       -99,
                       reg_alloc_target_id,
                       test_yr_end,
                       annual_reg_factor,
                       statistical_value
                  FROM reg_case_allocator_dyn_factor daf
                 WHERE last_updated =
                       (SELECT Max(last_updated)
                          FROM reg_case_allocator_dyn_factor q
                         WHERE daf.reg_allocator_id = q.reg_allocator_id
                           AND daf.reg_case_id = q.reg_case_id
                           AND daf.reg_alloc_target_id = q.reg_alloc_target_id)) f,
               (SELECT c.reg_case_id,
                       c.reg_alloc_category_id,
                       f.reg_alloc_target_id,
                       c.step_order alloc_level
                  FROM reg_case_alloc_steps    s,
                       reg_case_alloc_steps    c,
                       reg_case_target_forward f
                 WHERE s.reg_case_id = c.reg_case_id
                   AND c.reg_case_id = f.reg_case_id
                   AND s.reg_alloc_category_id = f.reg_alloc_category_id
                   AND s.step_order + 1 = c.step_order
                union
                SELECT reg_case_id,
                       0           reg_alloc_target_id,
                       0           reg_alloc_category_id,
                       1           step_order
                  FROM reg_case) lev
         WHERE r.reg_case_id = a.reg_case_id
           AND r.reg_acct_id = a.reg_acct_id
           AND r.reg_case_id = b.reg_case_id
           AND r.reg_alloc_acct_id = b.reg_alloc_acct_id
           AND r.reg_acct_id = m.reg_acct_id
           AND b.reg_alloc_target_id = x.reg_alloc_target_id
           AND a.reg_acct_type_id = t.reg_acct_type_id
           AND a.reg_acct_type_id = s.reg_acct_type_id
           AND a.sub_acct_type_id = s.sub_acct_type_id
           AND s.tax_control_id = c.tax_control_id(+)
           AND r.reg_case_id = y.reg_case_id
           AND p.reg_allocator_id = al.reg_allocator_id(+)
           AND b.parent_reg_alloc_acct_id = p.reg_alloc_acct_id
           AND b.reg_case_id = p.reg_case_id
           AND p.reg_allocator_id = f.reg_allocator_id
           AND p.reg_case_id = f.reg_case_id
           AND ac.reg_alloc_category_id = r.reg_alloc_category_id
           AND b.reg_alloc_target_id = f.reg_alloc_target_id
           AND m.reg_acct_id <> 0
           AND lev.reg_alloc_category_id = ac.reg_alloc_category_id
           AND lev.reg_alloc_target_id = b.reg_alloc_target_id
           AND lev.reg_case_id = r.reg_case_id
           AND f.effective_date =
               (SELECT MAX(xx.effective_date)
                  FROM (SELECT reg_allocator_id,
                               reg_case_id,
                               reg_alloc_category_id,
                               reg_alloc_target_id,
                               effective_date,
                               annual_reg_factor,
                               statistical_value
                          FROM reg_case_alloc_factor af
                        UNION
                        SELECT reg_allocator_id,
                               reg_case_id,
                               -99,
                               reg_alloc_target_id,
                               test_yr_end,
                               annual_reg_factor,
                               statistical_value
                          FROM reg_case_allocator_dyn_factor daf) xx
                 WHERE xx.reg_case_id = r.reg_case_id
                   AND xx.reg_allocator_id = f.reg_allocator_id
                   AND xx.reg_alloc_category_id =
                       DECODE(f.reg_alloc_category_id,
                              -99,
                              xx.reg_alloc_category_id,
                              f.reg_alloc_category_id)
                   AND xx.reg_alloc_target_id = f.reg_alloc_target_id
                   AND xx.effective_date <= r.case_year)
        UNION
        SELECT s.reg_case_id,
               s.reg_acct_id,
               reg_acct_type_id reg_acct_type,
               sub_acct_type_id sub_acct_type,
               NULL tax_indicator,
               0 reg_alloc_category_id,
               0 reg_alloc_target_id,
               case_year,
               1 annual_reg_factor,
               NULL statistical_value,
               'Total Company' allocator,
               NULL reg_allocator_id,
               s.case_year effective_date,
               total_co_final_amount alloc_amount,
               NULL parent_target_id,
               1 alloc_level
          FROM reg_case_summary_ledger s, reg_case_acct a
         WHERE a.reg_case_id = s.reg_case_id
           AND a.reg_acct_id = s.reg_acct_id) x,
       (SELECT reg_case_id,
               case_year,
               reg_acct_id,
               reg_alloc_category_id,
               reg_alloc_target_Id,
               Sum(Nvl(alloc_amount, 0)) adj_amount
          FROM reg_case_adjust_alloc_result
         WHERE adjustment_id <> 0
         GROUP BY reg_case_id,
                  case_year,
                  reg_acct_id,
                  reg_alloc_category_id,
                  reg_alloc_target_Id) aar
 WHERE x.reg_case_id = aar.reg_case_id(+)
   AND x.case_year = aar.case_year(+)
   AND x.reg_acct_id = aar.reg_acct_id(+)
   AND x.reg_alloc_category_id = aar.reg_alloc_category_id(+)
   AND x.reg_alloc_target_id = aar.reg_alloc_target_id(+)
);

--Create new version of sql_args table
DROP TABLE reg_schedule_sql_args;

CREATE GLOBAL TEMPORARY TABLE reg_schedule_sql_args  (
schedule_id  NUMBER(22,0),
col_number NUMBER(22,0),
sql_arg VARCHAR2(200),
sql_type NUMBER(22,0),
user_id VARCHAR2(18),
time_stamp date)
ON COMMIT DELETE rows;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3188, 0, 2016, 1, 0, 0, 045664, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045664_reg_schedule_precan_sql_ddl_010.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;