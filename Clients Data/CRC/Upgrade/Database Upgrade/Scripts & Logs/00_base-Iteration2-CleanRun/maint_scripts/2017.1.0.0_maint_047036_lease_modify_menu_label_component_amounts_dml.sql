/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047036_lease_modify_menu_label_component_amounts_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/04/2017 Anand R          Modify menu label for Component Formula Amounts workspace
||============================================================================
*/

update ppbase_menu_items
set label = 'Component Amounts'
where menu_identifier = 'manage_var_comp_amounts' 
   and menu_level = 2 
   and item_order = 3;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3420, 0, 2017, 1, 0, 0, 47036, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047036_lease_modify_menu_label_component_amounts_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	