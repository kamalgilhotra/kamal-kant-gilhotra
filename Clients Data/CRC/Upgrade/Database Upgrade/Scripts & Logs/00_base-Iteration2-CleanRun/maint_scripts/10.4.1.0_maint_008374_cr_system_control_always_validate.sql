/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008374_cr_system_control_always_validate.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.1.0 07/08/2013 Marc Zawko     Point Release
||============================================================================
*/

insert into CR_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION)
   select max(CONTROL_ID) + 1,
          'CR BDG - Enable Always Validate',
          'No',
          'In the Update in the Departmental Budget Entry (cr_Budget_data_entry), this check will run through and ensure required fields have been filled out and months have a 0 value. Can be set to No to increase update speed for full budget versions'
     from CR_SYSTEM_CONTROL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (431, 0, 10, 4, 1, 0, 8374, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_008374_cr_system_control_always_validate.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
