/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049208_lessor_02_add_set_of_books_to_lsr_ilr_amounts_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/13/2017 Andrew Hill    Add set of books id to lsr_ilr_amounts (drop and readd to order columns nicely)
||============================================================================
*/

DROP TABLE lsr_ilr_amounts;

CREATE TABLE lsr_ilr_amounts (ilr_id NUMBER(22,0) NOT NULL,
                              revision NUMBER(22,0) NOT NULL,
                              set_of_books_id NUMBER(22,0),
                              time_stamp DATE,
                              user_id VARCHAR2(18 BYTE),
                              npv_lease_payments NUMBER(22,2) NOT NULL,
                              npv_guaranteed_residual NUMBER(22,2) NOT NULL,
                              npv_unguaranteed_residual NUMBER(22,2) NOT NULL,
                              selling_profit_loss NUMBER(22,2) NOT NULL,
                              beginning_lease_receivable NUMBER(22,2) NOT NULL,
                              beginning_net_investment NUMBER(22,2) NOT NULL,
                              cost_of_goods_sold NUMBER(22,2) NOT NULL);
                              
ALTER TABLE lsr_ilr_amounts ADD CONSTRAINT lsr_ilr_amounts_pk primary key (ilr_id, revision, set_of_books_id) using index tablespace pwrplant_idx;

ALTER TABLE lsr_ilr_amounts ADD CONSTRAINT lsr_ilr_amounts_ilr_rev_fk FOREIGN KEY (ilr_id, revision) REFERENCES lsr_ilr_approval(ilr_id, revision);

ALTER TABLE lsr_ilr_amounts ADD CONSTRAINT lsr_ilr_amounts_sob_fk FOREIGN KEY (set_of_books_id) REFERENCES set_of_books (set_of_books_id);

COMMENT ON COLUMN lsr_ilr_amounts.ilr_id IS
  'System-assigned identifier of a particular ILR.';

COMMENT ON COLUMN lsr_ilr_amounts.revision IS
  'The revision.';

COMMENT ON COLUMN lsr_ilr_amounts.set_of_books_id IS
  'The set of books.';

COMMENT ON COLUMN lsr_ilr_amounts.time_stamp IS
  'Standard System-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN lsr_ilr_amounts.user_id IS
  'Standard System-assigned user id used for audit purposes.';

COMMENT ON COLUMN lsr_ilr_amounts.npv_lease_payments IS
  'The net present value of the lease payments in contract currency.';

COMMENT ON COLUMN lsr_ilr_amounts.npv_guaranteed_residual IS
  'The net present value of the guaranteed residual in contract currency.';

COMMENT ON COLUMN lsr_ilr_amounts.npv_unguaranteed_residual IS
  'The net present value of the unguaranteed residul in contract currency.';

COMMENT ON COLUMN lsr_ilr_amounts.selling_profit_loss IS
  'The selling profit or loss in contract currency.';

COMMENT ON COLUMN lsr_ilr_amounts.beginning_lease_receivable IS
  'The beginning lease receivable amount in contract currency.';

COMMENT ON COLUMN lsr_ilr_amounts.beginning_net_investment IS
  'The beginning net investement amount in contract currency.';

COMMENT ON COLUMN lsr_ilr_amounts.cost_of_goods_sold IS
  'The cost of goods sold in contract currency.';

COMMENT ON TABLE lsr_ilr_amounts IS
  '(C)  [06] The Lessor ILR Amounts table contains calculated characteristic amounts for the associated ILR.';
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3793, 0, 2017, 1, 0, 0, 49208, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049208_lessor_02_add_set_of_books_to_lsr_ilr_amounts_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;