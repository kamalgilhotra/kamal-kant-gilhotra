/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009183_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.1   01/12/2012 Elhadj Bah     Point Release
||============================================================================
*/

alter table REPAIR_LOCATION add IS_CIRCUIT number(22,0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('te_fund_proj', 'work_order_type', TO_DATE('20-12-2011 14:06:50', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 'yes_no', 'p', null, 'Tax Expensing FP', null, 98, '3 CWIP Processing', null, null,
    null, null, null, 0, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (77, 0, 10, 3, 3, 1, 9183, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.1_maint_009183_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
