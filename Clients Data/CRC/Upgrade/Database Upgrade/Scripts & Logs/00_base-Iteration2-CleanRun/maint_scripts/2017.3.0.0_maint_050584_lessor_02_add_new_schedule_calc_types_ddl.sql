/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050584_lessor_02_add_new_schedule_calc_types_ddl.sql
|| Description:	Add new types to support schedule type
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/12/2018 Andrew Hill    Add types
||============================================================================
*/
CREATE OR REPLACE TYPE t_lsr_ilr_schedule_all_rates AS OBJECT ( calculated_rates t_lsr_rates_implicit_in_lease,
                                                                override_rates t_lsr_rates_implicit_in_lease,
                                                                rates_used t_lsr_rates_implicit_in_lease);
/
                                                                
CREATE OR REPLACE TYPE t_lsr_ilr_sales_df_prelims AS OBJECT ( payment_info lsr_ilr_op_sch_pay_info_tab,
                                                              rates t_lsr_ilr_schedule_all_rates,
                                                              initial_direct_costs lsr_init_direct_cost_info_tab,
                                                              sales_type_info lsr_ilr_sales_sch_info);
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4184, 0, 2017, 3, 0, 0, 50584, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050584_lessor_02_add_new_schedule_calc_types_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;