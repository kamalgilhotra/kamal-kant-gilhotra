/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053050_pwrtax_01_dfit_ret_brk_rpt_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 03/08/2019  K. Powers	   New PowerTax DFIT Retire Brkdwn Reports (265,266)
||============================================================================
*/

-- Report Filters are specific and uniquely generated from MasterDB
-- Clear out any past runs 

DELETE FROM pp_reports WHERE datawindow='dw_tax_rpt_retire_dfit_compare';
DELETE FROM pp_reports WHERE datawindow='dw_tax_rpt_ret_tax_bal_compare';

DELETE FROM pp_dynamic_filter_mapping WHERE pp_report_filter_id =109;
DELETE FROM pp_reports_filter WHERE pp_report_filter_id = 109;
DELETE FROM pp_dynamic_filter WHERE filter_id = 292;

-- Insert Report Filters

insert into pp_reports_filter (pp_report_filter_id, description, filter_uo_name)
select
  109, 
  'PowerTax - Case Compare',
  'uo_tax_selecttabs_rpts_rollup_cons'
from dual;

-- Insert filter into mapping along with all fields from 82
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id)
select  109, 
        filter_id
from pp_dynamic_filter_mapping a
where pp_report_filter_id = 82;

insert into pp_dynamic_filter (filter_id, label, input_type, sqls_column_expression, dw, dw_id, dw_description, dw_id_datatype, required)
select 
    292, 
    'Tax Case Compare', 
    input_type, 
    NULL, 
    dw, 
    dw_id, 
    dw_description, 
    dw_id_datatype, 
    1
from pp_dynamic_filter where filter_id = 147;

insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id)
select 109,292
from dual;

-- Create the two new reports

-- Start with book/tax balance/reserve comparison (PwrTax - 265)
insert into pp_reports (REPORT_TYPE,
        SPECIAL_NOTE,
        DESCRIPTION,
        DYNAMIC_DW,
        OLD_REPORT_NUMBER,
        TIME_OPTION,
        DATAWINDOW,
        DOCUMENTATION,
        PP_REPORT_ENVIR_ID,
        SUBSYSTEM,
        PP_REPORT_FILTER_ID,
        FILTER_OPTION,
        PP_REPORT_TIME_OPTION_ID,
        STATUS,
        LONG_DESCRIPTION,
        PP_REPORT_NUMBER,
        PP_REPORT_STATUS_ID,
        PP_REPORT_SUBSYSTEM_ID,
        REPORT_NUMBER,
        TURN_OFF_MULTI_THREAD,
        LAST_APPROVED_DATE,
        USER_COMMENT,
        REPORT_TYPE_ID,
        INPUT_WINDOW,
        REPORT_ID)
  select REPORT_TYPE,
        SPECIAL_NOTE,
        'Book/Tax Basis Balance Comparison' DESCRIPTION,
        DYNAMIC_DW,
        OLD_REPORT_NUMBER,
        TIME_OPTION,
        'dw_tax_rpt_ret_tax_bal_compare' DATAWINDOW,
        DOCUMENTATION,
        PP_REPORT_ENVIR_ID,
        SUBSYSTEM,
        109 PP_REPORT_FILTER_ID,
        FILTER_OPTION,
        PP_REPORT_TIME_OPTION_ID,
        STATUS,
        'Compares ending book/tax basis and reserve across two cases. Organized by norm schema.' LONG_DESCRIPTION,
        PP_REPORT_NUMBER,
        PP_REPORT_STATUS_ID,
        PP_REPORT_SUBSYSTEM_ID,
        'PwrTax - 265' REPORT_NUMBER,
        TURN_OFF_MULTI_THREAD,
        LAST_APPROVED_DATE,
        USER_COMMENT,
        REPORT_TYPE_ID,
        INPUT_WINDOW,
        (SELECT NVL(Max(report_id),0) FROM pp_reports WHERE report_id >= 400000 and report_id <= 499999 ) + 1 REPORT_ID
    from pp_Reports
      WHERE Lower(datawindow) = ('dw_tax_rpt_fas109_end_total')
      AND NOT EXISTS
        (SELECT 1 FROM pp_reports WHERE trim(Lower(datawindow)) = 'dw_tax_rpt_ret_tax_bal_compare');

-- Now deferred tax balance comparison (PwrTax - 266)
insert into pp_reports (REPORT_TYPE,
        SPECIAL_NOTE,
        DESCRIPTION,
        DYNAMIC_DW,
        OLD_REPORT_NUMBER,
        TIME_OPTION,
        DATAWINDOW,
        DOCUMENTATION,
        PP_REPORT_ENVIR_ID,
        SUBSYSTEM,
        PP_REPORT_FILTER_ID,
        FILTER_OPTION,
        PP_REPORT_TIME_OPTION_ID,
        STATUS,
        LONG_DESCRIPTION,
        PP_REPORT_NUMBER,
        PP_REPORT_STATUS_ID,
        PP_REPORT_SUBSYSTEM_ID,
        REPORT_NUMBER,
        TURN_OFF_MULTI_THREAD,
        LAST_APPROVED_DATE,
        USER_COMMENT,
        REPORT_TYPE_ID,
        INPUT_WINDOW,
        REPORT_ID)
  select REPORT_TYPE,
        SPECIAL_NOTE,
        'Deferred Tax Balance Comparison' DESCRIPTION,
        DYNAMIC_DW,
        OLD_REPORT_NUMBER,
        TIME_OPTION,
        'dw_tax_rpt_retire_dfit_compare' DATAWINDOW,
        DOCUMENTATION,
        PP_REPORT_ENVIR_ID,
        SUBSYSTEM,
        109 PP_REPORT_FILTER_ID,
        FILTER_OPTION,
        PP_REPORT_TIME_OPTION_ID,
        STATUS,
        'Compares ending timing differences, APB11, and reg asset/liability across two cases. Organized by norm schema.' LONG_DESCRIPTION,
        PP_REPORT_NUMBER,
        PP_REPORT_STATUS_ID,
        PP_REPORT_SUBSYSTEM_ID,
        'PwrTax - 266' REPORT_NUMBER,
        TURN_OFF_MULTI_THREAD,
        LAST_APPROVED_DATE,
        USER_COMMENT,
        REPORT_TYPE_ID,
        INPUT_WINDOW,
        (SELECT NVL(Max(report_id),0) FROM pp_reports WHERE report_id >= 400000 and report_id <= 499999 ) + 1 REPORT_ID
    from pp_Reports
    where Lower(datawindow) = ('dw_tax_rpt_fas109_end_total')
      AND NOT EXISTS
        (SELECT 1 FROM pp_reports WHERE trim(Lower(datawindow)) = 'dw_tax_rpt_retire_dfit_compare');
        
--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15762, 0, 2018, 2, 0, 0, 53050, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053050_pwrtax_01_dfit_ret_brk_rpt_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
