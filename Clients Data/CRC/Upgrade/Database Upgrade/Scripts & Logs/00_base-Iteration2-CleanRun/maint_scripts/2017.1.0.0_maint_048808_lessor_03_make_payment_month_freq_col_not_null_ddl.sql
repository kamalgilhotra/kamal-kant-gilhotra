/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048808_lessor_03_make_payment_month_freq_col_not_null_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/21/2017 Charlie Shilling make our new column NOT NULL now that it's been populated by script 02
||============================================================================
*/

ALTER TABLE ls_payment_freq
MODIFY payment_month_freq NOT NULL;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3737, 0, 2017, 1, 0, 0, 48808, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048808_lessor_03_make_payment_month_freq_col_not_null_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
