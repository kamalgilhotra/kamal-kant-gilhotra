/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_042130_cwip_cpiretro_01_ddl.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   1/29/2015   Sunjin Cone     Add new option for Idle Check for CPI Retro Calc
||============================================================================
*/ 

alter table AFUDC_CONTROL add CPI_RETRO_IDLE number(22,0);
alter table CPI_RETRO_WO_LIST_TEMP add CPI_RETRO_IDLE number(22,0);

comment on column AFUDC_CONTROL.CPI_RETRO_IDLE is 'Yes/No option (1 = Yes; 0/null = No).  The default is 0 (No).  This is a special option used by the CPI Retro Calc for how the IDLE_CPI_MONTH value is used for identifying idle status.  When this option is set to 1(Yes), the CPI Retro Calc will review the charge activity prior to and ahead of the calc month with a limit for the look ahead to stop with the last closed month, because months that have not yet been closed cannot be considered for idle status.  When the option is set to 0(No), the comparison only reviews the prior months, which is how the regular Month End calculation performs the idle check.';

comment on column CPI_RETRO_WO_LIST_TEMP.CPI_RETRO_IDLE is 'Yes/No option (1 = Yes; 0/null = No).  The default is 0 (No).  This is a special option used by the CPI Retro Calc for how the IDLE_CPI_MONTH value is used for identifying idle status.  When this option is set to 1(Yes), the CPI Retro Calc will review the charge activity prior to and ahead of the calc month with a limit for the look ahead to stop with the last closed month, because months that have not yet been closed cannot be considered for idle status.  When the option is set to 0(No), the comparison only reviews the prior months, which is how the regular Month End calculation performs the idle check.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2233, 0, 2015, 1, 0, 0, 42130, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042130_cwip_cpiretro_01_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;