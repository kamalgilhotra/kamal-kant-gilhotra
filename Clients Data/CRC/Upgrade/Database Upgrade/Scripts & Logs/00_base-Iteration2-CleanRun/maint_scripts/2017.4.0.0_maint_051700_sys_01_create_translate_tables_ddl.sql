/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051700_sys_01_create_translate_tables_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/29/2018 TechProdMgmt	New Tables to support generic translates
||============================================================================
*/

-- Create table
create table PP_TRANSLATE_RULE
(
  rule_id     NUMBER(22) not null,
  time_stamp  date,
  user_id     varchar2(18),
  description VARCHAR2(254) not null,
  comments    VARCHAR2(2000)
)
;

-- Create/Recreate primary, unique and foreign key constraints 
alter table PP_TRANSLATE_RULE
  add constraint PP_TRANSLATE_PK primary key (RULE_ID)
  using index tablespace pwrplant_idx;

-- Create table
create table PP_TRANSLATE_ELEMENTS
(
  element_id  number(22,0) not null,
  time_stamp  date,
  user_id     varchar2(18),
  description varchar2(254) not null,
  label       varchar2(128) not null,
  table_name  varchar2(128),
  data_column  varchar2(128),
  display_column varchar2(128)
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table PP_TRANSLATE_ELEMENTS
  add constraint PP_TRANSLATE_ELEMENT_PK primary key (ELEMENT_ID)
  using index tablespace pwrplant_idx;
  
-- Create table
create table PP_TRANSLATE_RULE_ELEMENT
(
  rule_id    NUMBER(22) not null,
  input_order number(22,0) not null,
  time_stamp  date,
  user_id     varchar2(18),
  element_id NUMBER(22) not null
)
;

-- Create/Recreate primary, unique and foreign key constraints 
alter table PP_TRANSLATE_RULE_ELEMENT
  add constraint PP_TRANS_RULE_ELE_ELEMENT_FK foreign key (ELEMENT_ID)
  references PP_TRANSLATE_ELEMENTS (ELEMENT_ID);
alter table PP_TRANSLATE_RULE_ELEMENT
  add constraint PP_TRANS_RULE_ELE_RULE_FK foreign key (RULE_ID)
  references PP_TRANSLATE_RULE (RULE_ID);
alter table PP_TRANSLATE_RULE_ELEMENT
  add constraint PP_TRANSLATE_RULE_ELEMENT_PK primary key (RULE_ID, input_order)
  using index tablespace pwrplant_idx;
  
-- Create table
create table PP_TRANSLATE_MAPPING
(
  rule_id      number(22,0) not null,
  map_id       number(22,0) not null,
  time_stamp  date,
  user_id     varchar2(18),
  input_value  varchar2(4000),
  output_value varchar2(4000)
)
;

-- Create/Recreate primary, unique and foreign key constraints 
alter table PP_TRANSLATE_MAPPING
  add constraint PP_TRANSLATE_MAPPING_PK primary key (RULE_ID, MAP_ID)
  using index tablespace pwrplant_idx;
alter table PP_TRANSLATE_MAPPING
  add constraint PP_TRANSLATE_MAP_RULE_FK foreign key (RULE_ID)
  references pp_translate_rule (RULE_ID);

-- Add comments to the PP_TRANSLATE_RULE table
comment on table PP_TRANSLATE_RULE
  is 'Translate Rules are generic translates that can be configured by clients for use by integration hub processes, JE keywords, dynamic validations, etc.  Number of input elements is not limited but each translate record only has one output value. ';
-- Add comments to the columns 
comment on column PP_TRANSLATE_RULE.rule_id
  is 'System-assigned id for the Translate Rule.';
comment on column PP_TRANSLATE_RULE.time_stamp
  is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_TRANSLATE_RULE.user_id
  is 'Standard system-assigned user id used for audit purposes';
comment on column PP_TRANSLATE_RULE.description
  is 'The description of the Translate Rule.';
comment on column PP_TRANSLATE_RULE.comments
  is 'Additional comments about the Translate Rule.';  
  
-- Add comments to the PP_TRANSLATE_ELEMENTS table
comment on table PP_TRANSLATE_ELEMENTS
  is 'Translate Elements represent the inputs for Translate Rules.  Elements values used by the Rule can be based on tables in the PowerPlan application or be freeform entry.';
-- Add comments to the columns 
comment on column PP_TRANSLATE_ELEMENTS.element_id
  is 'System-assigned id for the Translate Element.';
comment on column PP_TRANSLATE_ELEMENTS.time_stamp
  is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_TRANSLATE_ELEMENTS.user_id
  is 'Standard system-assigned user id used for audit purposes';
comment on column PP_TRANSLATE_ELEMENTS.description
  is 'The description of the Translate Element.';
comment on column PP_TRANSLATE_ELEMENTS.label
  is 'Label to use when constructing the Input Value JSON string.  For Example, "Co" for company or "TransType" for transaction type.';
comment on column PP_TRANSLATE_ELEMENTS.table_name
  is 'Table (or view) to use as the valid list for the element in rule mappings.';
comment on column PP_TRANSLATE_ELEMENTS.data_column
  is 'Field to use as the "data" to save in the Input Value JSON string.';
comment on column PP_TRANSLATE_ELEMENTS.display_column
  is 'Field to use as the "display" to show on the Translate Rule Setup window.';

-- Add comments to the PP_TRANSLATE_RULE_ELEMENT table
comment on table PP_TRANSLATE_RULE_ELEMENT
  is 'Defines the Translate Elements to use for each Translate Rule including the order they appear in the input value JSON string.';
-- Add comments to the columns 
comment on column PP_TRANSLATE_RULE_ELEMENT.rule_id
  is 'System-assigned id for the Translate Rule.';
comment on column PP_TRANSLATE_RULE_ELEMENT.input_order
  is 'The input order of the Element when used for the Rule.';
comment on column PP_TRANSLATE_RULE_ELEMENT.time_stamp
  is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_TRANSLATE_RULE_ELEMENT.user_id
  is 'Standard system-assigned user id used for audit purposes';
comment on column PP_TRANSLATE_RULE_ELEMENT.element_id
  is 'System-assigned id for the Translate Element.';
  
-- Add comments to the PP_TRANSLATE_MAPPING table 
comment on table PP_TRANSLATE_MAPPING
  is 'Translate Mappings are rule specific to translate from input elements to an output value.  Important to note the input elements are stored in a JSON string format.';
-- Add comments to the columns 
comment on column PP_TRANSLATE_MAPPING.rule_id
  is 'System-assigned id for the Translate Rule.';
comment on column PP_TRANSLATE_MAPPING.map_id
  is 'System-assigned id for the Translate Mapping that is used for uniqueness.';
comment on column PP_TRANSLATE_MAPPING.time_stamp
  is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_TRANSLATE_MAPPING.user_id
  is 'Standard system-assigned user id used for audit purposes';
comment on column PP_TRANSLATE_MAPPING.input_value
  is 'The input elements and values stored in JSON string format.';
comment on column PP_TRANSLATE_MAPPING.output_value
  is 'The value returned when Input Value is matched for a rule.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7591, 0, 2017, 4, 0, 0, 51700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051700_sys_01_create_translate_tables_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
