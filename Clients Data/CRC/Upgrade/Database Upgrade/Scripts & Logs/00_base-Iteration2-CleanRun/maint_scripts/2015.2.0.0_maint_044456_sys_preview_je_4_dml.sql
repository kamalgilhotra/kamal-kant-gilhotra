 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044456_sys_preview_je_4_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2015.2.0.0  08/10/2015 David Haupt    Add ppdepr_interface_custom line
 ||============================================================================
 */

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppdepr_interface_custom.pbd',
  '2015.2.0.0'
FROM pp_processes
WHERE lower(trim(executable_file)) = 'ssp_preview_je.exe';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2762, 0, 2015, 2, 0, 0, 044456, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044456_sys_preview_je_4_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;