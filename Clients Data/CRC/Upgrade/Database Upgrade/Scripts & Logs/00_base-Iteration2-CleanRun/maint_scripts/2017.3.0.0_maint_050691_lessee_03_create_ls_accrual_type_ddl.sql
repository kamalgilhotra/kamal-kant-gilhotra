/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050691_lessee_03_create_ls_accrual_type_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/30/2018 Josh Sandler 	  Add Lessee Accrual types
||============================================================================
*/

CREATE TABLE LS_ACCRUAL_TYPE
  (
     accrual_type_id NUMBER(22, 0) NOT NULL,
     description     VARCHAR2(35) NOT NULL,
     user_id         VARCHAR2(18) NULL,
     time_stamp      DATE NULL
  );

ALTER TABLE LS_ACCRUAL_TYPE
  ADD CONSTRAINT pk_ls_accrual_type PRIMARY KEY ( accrual_type_id ) USING INDEX TABLESPACE pwrplant_idx;

COMMENT ON COLUMN LS_ACCRUAL_TYPE.accrual_type_id IS 'The internal leased accrual type id within PowerPlant.';

COMMENT ON COLUMN LS_ACCRUAL_TYPE.description IS 'A description of the accrual type.';

COMMENT ON COLUMN LS_ACCRUAL_TYPE.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LS_ACCRUAL_TYPE.user_id IS 'Standard System-assigned user id used for audit purposes.'; 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(4299, 0, 2017, 3, 0, 0, 50691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050691_lessee_03_create_ls_accrual_type_ddl.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
