/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051042_lessee_01_default_deferred_rent_sw_to_on_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/30/2018 Josh Sandler 	  Default new Deferred/Prepaid Rent option to On
||============================================================================
*/

UPDATE ls_ilr_options
SET deferred_rent_sw = 1
WHERE deferred_rent_sw IS NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4902, 0, 2017, 3, 0, 0, 51042, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051042_lessee_01_default_deferred_rent_sw_to_on_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;