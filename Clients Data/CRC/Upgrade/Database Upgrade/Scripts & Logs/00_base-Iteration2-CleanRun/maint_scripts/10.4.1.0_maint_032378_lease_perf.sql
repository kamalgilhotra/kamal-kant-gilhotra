/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032378_lease_perf.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/16/2013 Brandon Beck   Point Release
||============================================================================
*/

create global temporary table LS_ILR_SCHEDULE_BUCKET_STG
(
 ID              number(22,0),
 ILR_ID          number(22,0),
 SET_OF_BOOKS_ID number(22,0),
 MONTH           date,
 LS_ASSET_ID     number(22,0),
 C1              number(22,8),
 C2              number(22,8),
 C3              number(22,8),
 C4              number(22,8),
 C5              number(22,8),
 C6              number(22,8),
 C7              number(22,8),
 C8              number(22,8),
 C9              number(22,8),
 C10             number(22,8),
 E1              number(22,8),
 E2              number(22,8),
 E3              number(22,8),
 E4              number(22,8),
 E5              number(22,8),
 E6              number(22,8),
 E7              number(22,8),
 E8              number(22,8),
 E9              number(22,8),
 E10             number(22,8)
) on commit preserve rows;

alter table LS_ILR_SCHEDULE_BUCKET_STG
   add constraint LS_ILR_SCHEDULE_BUCKET_PK
       primary key (ILR_ID, LS_ASSET_ID, SET_OF_BOOKS_ID, MONTH);

comment on table LS_ILR_SCHEDULE_BUCKET_STG is '(T) [06] This table is a temporary table used during the schedule calculation to hold the execturoy and contingent bucket amounts by period';

comment on column LS_ILR_SCHEDULE_BUCKET_STG.ID is 'Internal system generated id';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.ILR_ID is 'ID field linking to the ILR';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.LS_ASSET_ID is 'ID Field linking to the LS Asset';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.SET_OF_BOOKS_ID is 'ID field linking to the set of books';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.MONTH is 'The accounting month';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E1 is 'The executory paid amount associated with the executory bucket number 1.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E2 is 'The executory paid amount associated with the executory bucket number 2.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E3 is 'The executory paid amount associated with the executory bucket number 3.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E4 is 'The executory paid amount associated with the executory bucket number 4.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E5 is 'The executory paid amount associated with the executory bucket number 5.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E6 is 'The executory paid amount associated with the executory bucket number 6.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E7 is 'The executory paid amount associated with the executory bucket number 7.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E8 is 'The executory paid amount associated with the executory bucket number 8.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E9 is 'The executory paid amount associated with the executory bucket number 9.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.E10 is 'The executory paid amount associated with the executory bucket number 10.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C1 is 'The contingent paid amount associated with the contingent bucket number 1.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C2 is 'The contingent paid amount associated with the contingent bucket number 2.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C3 is 'The contingent paid amount associated with the contingent bucket number 3.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C4 is 'The contingent paid amount associated with the contingent bucket number 4.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C5 is 'The contingent paid amount associated with the contingent bucket number 5.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C6 is 'The contingent paid amount associated with the contingent bucket number 6.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C7 is 'The contingent paid amount associated with the contingent bucket number 7.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C8 is 'The contingent paid amount associated with the contingent bucket number 8.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C9 is 'The contingent paid amount associated with the contingent bucket number 9.';
comment on column LS_ILR_SCHEDULE_BUCKET_STG.C10 is 'The contingent paid amount associated with the contingent bucket number 10.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (617, 0, 10, 4, 1, 0, 32378, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032378_lease_perf.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
