/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046773_aro_set_aro1000_rpt_filter_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 11/15/2016 Jared Watkins	     Set the ARO - 1000 report to use the default 
||					ARO filter again and exclude the default SOB filter
||============================================================================
*/

update pp_reports 
set pp_report_filter_id = 17 
where datawindow = 'dw_aro_liability_span_report';

update pp_reports 
set special_note = special_note||', exclude set of books' 
where datawindow = 'dw_aro_liability_span_report';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3341, 0, 2016, 1, 0, 0, 046773, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046773_aro_set_aro1000_rpt_filter_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;