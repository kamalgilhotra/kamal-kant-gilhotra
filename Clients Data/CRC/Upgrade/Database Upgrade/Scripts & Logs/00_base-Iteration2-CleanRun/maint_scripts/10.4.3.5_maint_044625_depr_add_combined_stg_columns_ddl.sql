SET serveroutput ON
/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044625_depr_add_combined_stg_columns_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 08/15/2015 Charlie Shilling PP-44625 - combined calc errors
||============================================================================
*/
declare
   column_name 		VARCHAR2(30);
   data_type 		VARCHAR2(100);
   column_comment	VARCHAR2(1000);

   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) IS
      sqls VARCHAR2(4000);
   begin
      begin
         sqls := 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                           V_DATATYPE;
		 execute immediate sqls;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                                    V_TABLE_NAME || '. No action necessasry.');
            else
		 	   Dbms_Output.put_line('SQL Used: ' || sqls);
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' ||
                                       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         sqls := 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                           V_COMMENT || '''';
		 execute immediate sqls;
         DBMS_OUTPUT.PUT_LINE('	Sucessfully added the comment to column ' || V_COL_NAME ||
                              ' to table ' || V_TABLE_NAME || '.');
      exception
         when others THEN
		 	Dbms_Output.put_line('SQL Used: ' || sqls);
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
BEGIN
   --add unmatched_net_gross
   column_name := 'unmatched_net_gross';
   data_type := 'number(1,0) default 0';
   column_comment := 'A false (0)/ true(1) indicator for whether or not all of the child groups have the same net_gross setting as each other and as the combined_depr_group. If this column is true, then the depreciation base must be recalculated using the combined groups depr method rates options. The depreciation base can only be recalculated if all child groups have the "Monthly" mid-period_method';
   ADD_COLUMN('depr_calc_combined_stg',
              column_name,
              data_type,
              column_comment);
   ADD_COLUMN('depr_calc_combined_arc',
              column_name,
              data_type,
              column_comment);
   ADD_COLUMN('fcst_depr_calc_combined_arc',
              column_name,
              data_type,
              column_comment);

   --add combined_calc_error
   column_name := 'combined_calc_error';
   data_type := 'number(22,0) default 0';
   column_comment := 'A column to hold error codes for the combined depreciation calculation.';
   ADD_COLUMN('depr_calc_combined_stg',
              column_name,
              data_type,
              column_comment);
   ADD_COLUMN('depr_calc_combined_arc',
              column_name,
              data_type,
              column_comment);
   ADD_COLUMN('fcst_depr_calc_combined_arc',
              column_name,
              data_type,
              column_comment);
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2785, 0, 10, 4, 3, 5, 044625, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.5_maint_044625_depr_add_combined_stg_columns_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;