/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037318_cwip_fast101.sql  (part of epic 9415)
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.2.0 03/24/2014 Sunjin Cone    More Auto101 changes for performance improvement
||=================================================================================
*/

drop table UNITIZE_PEND_TRANS_TEMP;

create global temporary table UNITIZE_PEND_TRANS_TEMP
(
 COMPANY_ID          number(22,0),
 GL_POSTING_MO_YR    date,
 IN_SERVICE_YEAR     date,
 WORK_ORDER_ID       number(22,0),
 WORK_ORDER_NUMBER   varchar2(35),
 UNIT_ITEM_ID        number(22,0),
 PEND_TRANS_ID       number(22,0),
 LDG_ASSET_ID        number(22,0),
 LDG_ACTIVITY_ID     number(22,0),
 LDG_DEPR_GROUP_ID   number(22,0),
 GL_ACCOUNT_ID       number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 FUNC_CLASS_ID       number(22,0),
 RETIREMENT_UNIT_ID  number(22,0),
 PROPERTY_GROUP_ID   number(22,0),
 ASSET_LOCATION_ID   number(22,0),
 SUBLEDGER_INDICATOR number(22,0),
 POSTING_QUANTITY    number(22,2),
 POSTING_AMOUNT      number(22,2),
 POSTING_STATUS      number(22,0),
 GL_JE_CODE          char(18),
 SERIAL_NUMBER       varchar2(35),
 FERC_ACTIVITY_CODE  number(22,2),
 ACTIVITY_CODE       char(5),
 DESCRIPTION         varchar2(35),
 LONG_DESCRIPTION    varchar2(254),
 RETIRE_METHOD_ID    number(22,2),
 BOOKS_SCHEMA_ID     number(22,0),
 RESERVE_CREDITS     number(22,2),
 COST_OF_REMOVAL     number(22,2),
 SALVAGE_CASH        number(22,2),
 SALVAGE_RETURNS     number(22,2),
 GAIN_LOSS           number(22,2),
 REPLACEMENT_AMOUNT  number(22,2),
 USER_ID1            varchar2(18),
 BASIS_1             number(22,2),
 BASIS_2             number(22,2),
 BASIS_3             number(22,2),
 BASIS_4             number(22,2),
 BASIS_5             number(22,2),
 BASIS_6             number(22,2),
 BASIS_7             number(22,2),
 BASIS_8             number(22,2),
 BASIS_9             number(22,2),
 BASIS_10            number(22,2),
 BASIS_11            number(22,2),
 BASIS_12            number(22,2),
 BASIS_13            number(22,2),
 BASIS_14            number(22,2),
 BASIS_15            number(22,2),
 BASIS_16            number(22,2),
 BASIS_17            number(22,2),
 BASIS_18            number(22,2),
 BASIS_19            number(22,2),
 BASIS_20            number(22,2),
 BASIS_21            number(22,2),
 BASIS_22            number(22,2),
 BASIS_23            number(22,2),
 BASIS_24            number(22,2),
 BASIS_25            number(22,2),
 BASIS_26            number(22,2),
 BASIS_27            number(22,2),
 BASIS_28            number(22,2),
 BASIS_29            number(22,2),
 BASIS_30            number(22,2),
 BASIS_31            number(22,2),
 BASIS_32            number(22,2),
 BASIS_33            number(22,2),
 BASIS_34            number(22,2),
 BASIS_35            number(22,2),
 BASIS_36            number(22,2),
 BASIS_37            number(22,2),
 BASIS_38            number(22,2),
 BASIS_39            number(22,2),
 BASIS_40            number(22,2),
 BASIS_41            number(22,2),
 BASIS_42            number(22,2),
 BASIS_43            number(22,2),
 BASIS_44            number(22,2),
 BASIS_45            number(22,2),
 BASIS_46            number(22,2),
 BASIS_47            number(22,2),
 BASIS_48            number(22,2),
 BASIS_49            number(22,2),
 BASIS_50            number(22,2),
 BASIS_51            number(22,2),
 BASIS_52            number(22,2),
 BASIS_53            number(22,2),
 BASIS_54            number(22,2),
 BASIS_55            number(22,2),
 BASIS_56            number(22,2),
 BASIS_57            number(22,2),
 BASIS_58            number(22,2),
 BASIS_59            number(22,2),
 BASIS_60            number(22,2),
 BASIS_61            number(22,2),
 BASIS_62            number(22,2),
 BASIS_63            number(22,2),
 BASIS_64            number(22,2),
 BASIS_65            number(22,2),
 BASIS_66            number(22,2),
 BASIS_67            number(22,2),
 BASIS_68            number(22,2),
 BASIS_69            number(22,2),
 BASIS_70            number(22,2),
 NOTES               varchar2(254),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
) on commit preserve rows;

comment on table  UNITIZE_PEND_TRANS_TEMP is '(C) [04] Global temp table to hold the unitization pending transactions to be inserted into the real pending tables.  No primary key.';
comment on column UNITIZE_PEND_TRANS_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_PEND_TRANS_TEMP.GL_POSTING_MO_YR is 'Accounting month of the transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.IN_SERVICE_YEAR is 'Records the month and year in which the asset represented by the pending transaction was placed in service.  (This is the engineering in service year of the work order.)  For a retirement transaction this is the out-of-service year.';
comment on column UNITIZE_PEND_TRANS_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_PEND_TRANS_TEMP.WORK_ORDER_NUMBER is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_PEND_TRANS_TEMP.UNIT_ITEM_ID is 'System-assigned identifier of a particular record in the table.';
comment on column UNITIZE_PEND_TRANS_TEMP.PEND_TRANS_ID is 'System-assigned identifier of a particular pending transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.LDG_ASSET_ID is 'Records the existing CPR ledger entry, if any, which is associated with the pending transaction. For example, retirements, transfers out, and adjustment transactions would all affect existing entries.';
comment on column UNITIZE_PEND_TRANS_TEMP.LDG_ACTIVITY_ID is 'Records the existing link to CHARGE_GROUP_CONTROL.CHARGE_GROUP_ID if it is a URGL/SAGL retirement transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.LDG_DEPR_GROUP_ID is 'This field is used to communicate between the unitization routines and the post routine.';
comment on column UNITIZE_PEND_TRANS_TEMP.GL_ACCOUNT_ID is 'System-assigned identifier of a unique General Ledger account.';
comment on column UNITIZE_PEND_TRANS_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment.';
comment on column UNITIZE_PEND_TRANS_TEMP.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.';
comment on column UNITIZE_PEND_TRANS_TEMP.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account.';
comment on column UNITIZE_PEND_TRANS_TEMP.FUNC_CLASS_ID is 'System-assigned identifier of the functional class.';
comment on column UNITIZE_PEND_TRANS_TEMP.RETIREMENT_UNIT_ID is  'System-assigned identifier of the retirement unit.';
comment on column UNITIZE_PEND_TRANS_TEMP.PROPERTY_GROUP_ID is  'System-assigned identifier of the property group.';
comment on column UNITIZE_PEND_TRANS_TEMP.ASSET_LOCATION_ID is  'System-assigned identifier of the asset location.';
comment on column UNITIZE_PEND_TRANS_TEMP.SUBLEDGER_INDICATOR is 'System-assigned identifier of the subledger.  Always 0 for unitization transactions';
comment on column UNITIZE_PEND_TRANS_TEMP.POSTING_QUANTITY is 'Records the quantity of the transaction being posted on the pending transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.POSTING_AMOUNT is 'Records the dollar amount (financial set of books) of the transaction being posted on the pending transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.GL_JE_CODE is 'Records the General Ledger journal entry number code used to generate a General ledger journal entry.';
comment on column UNITIZE_PEND_TRANS_TEMP.SERIAL_NUMBER is 'Records the serial number associated with the asset component by the manufacturer.';
comment on column UNITIZE_PEND_TRANS_TEMP.FERC_ACTIVITY_CODE is 'Identifier of FERC transaction activity type (1:Addition  2:Retirement)';
comment on column UNITIZE_PEND_TRANS_TEMP.ACTIVITY_CODE is 'Records the type of activity the entry represents.  Possible types for unitization transactions would include UADD, MADD, URET.';
comment on column UNITIZE_PEND_TRANS_TEMP.DESCRIPTION is 'Records a short description of the transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.LONG_DESCRIPTION is 'Records a long description of the transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.RETIRE_METHOD_ID is 'System-assigned id associated with a retirement method.  It is set to null for unitization transactions with no asset_id.';
comment on column UNITIZE_PEND_TRANS_TEMP.BOOKS_SCHEMA_ID is 'System-assigned identifier of the book schema.  The default book schema is 1(all).';
comment on column UNITIZE_PEND_TRANS_TEMP.RESERVE_CREDITS is 'Reserve_credits records the dollar amount of any other credits such as insurance proceeds associated with a retirement.';
comment on column UNITIZE_PEND_TRANS_TEMP.COST_OF_REMOVAL is 'Records the dollar amount of removal cost associated with a retirement transaction';
comment on column UNITIZE_PEND_TRANS_TEMP.SALVAGE_CASH is 'Records the dollar amount of cash salvage proceeds associated with a retirement transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.SALVAGE_RETURNS is 'Records the dollar amount of salvage returns to stock associated with a retirement transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.GAIN_LOSS is 'Records the dollar amount (financial set of books) of gain (positive) or loss (negative), if any, recognized with a retirement transaction.  For transactions where the full asset amount is removed from the accumulated reserve (i.e., the gain/loss is netted in the reserve) no amount appears in this field.';
comment on column UNITIZE_PEND_TRANS_TEMP.REPLACEMENT_AMOUNT is 'Dollar amount of a corresponding retirement of an addition.  This is in current dollars, and will be indexed back to fund the actual amount of the retirements.  The Post program creates the retirement transaction automatically when the addition posts.  The retire_method_id can be HW-FIFO or HW-CURVE. If neither is given on the pending transaction, HW-FIFO will be used.';
comment on column UNITIZE_PEND_TRANS_TEMP.USER_ID1 is 'System-assigned id associated with the pending transaction creation.  An example would be the user id associated with the analyst or field engineer who generated the pending transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_1 is 'Records an amount associated with book summary level 1 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_2 is 'Records an amount associated with book summary level 2 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_3 is 'Records an amount associated with book summary level 3 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_4 is 'Records an amount associated with book summary level 4 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_5 is 'Records an amount associated with book summary level 5 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_6 is 'Records an amount associated with book summary level 6 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_7 is 'Records an amount associated with book summary level 7 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_8 is 'Records an amount associated with book summary level 8 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_9 is 'Records an amount associated with book summary level 9 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_10 is 'Records an amount associated with book summary level 10 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_11 is 'Records an amount associated with book summary level 11 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_12 is 'Records an amount associated with book summary level 12 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_13 is 'Records an amount associated with book summary level 13 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_14 is 'Records an amount associated with book summary level 14 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_15 is 'Records an amount associated with book summary level 15 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_16 is 'Records an amount associated with book summary level 16 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_17 is 'Records an amount associated with book summary level 17 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_18 is 'Records an amount associated with book summary level 18 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_19 is 'Records an amount associated with book summary level 19 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_20 is 'Records an amount associated with book summary level 20 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_21 is 'Records an amount associated with book summary level 21 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_22 is 'Records an amount associated with book summary level 22 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_23 is 'Records an amount associated with book summary level 23 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_24 is 'Records an amount associated with book summary level 24 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_25 is 'Records an amount associated with book summary level 25 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_26 is 'Records an amount associated with book summary level 26 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_27 is 'Records an amount associated with book summary level 27 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_28 is 'Records an amount associated with book summary level 28 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_29 is 'Records an amount associated with book summary level 29 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_30 is 'Records an amount associated with book summary level 30 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_31 is 'Records an amount associated with book summary level 31 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_32 is 'Records an amount associated with book summary level 32 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_33 is 'Records an amount associated with book summary level 33 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_34 is 'Records an amount associated with book summary level 34 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_35 is 'Records an amount associated with book summary level 35 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_36 is 'Records an amount associated with book summary level 36 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_37 is 'Records an amount associated with book summary level 37 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_38 is 'Records an amount associated with book summary level 38 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_39 is 'Records an amount associated with book summary level 39 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_40 is 'Records an amount associated with book summary level 40 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_41 is 'Records an amount associated with book summary level 41 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_42 is 'Records an amount associated with book summary level 42 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_43 is 'Records an amount associated with book summary level 43 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_44 is 'Records an amount associated with book summary level 44 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_45 is 'Records an amount associated with book summary level 45 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_46 is 'Records an amount associated with book summary level 46 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_47 is 'Records an amount associated with book summary level 47 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_48 is 'Records an amount associated with book summary level 48 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_49 is 'Records an amount associated with book summary level 49 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_50 is 'Records an amount associated with book summary level 50 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_51 is 'Records an amount associated with book summary level 51 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_52 is 'Records an amount associated with book summary level 52 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_53 is 'Records an amount associated with book summary level 53 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_54 is 'Records an amount associated with book summary level 54 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_55 is 'Records an amount associated with book summary level 55 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_56 is 'Records an amount associated with book summary level 56 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_57 is 'Records an amount associated with book summary level 57 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_58 is 'Records an amount associated with book summary level 58 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_59 is 'Records an amount associated with book summary level 59 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_60 is 'Records an amount associated with book summary level 60 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_61 is 'Records an amount associated with book summary level 61 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_62 is 'Records an amount associated with book summary level 62 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_63 is 'Records an amount associated with book summary level 63 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_64 is 'Records an amount associated with book summary level 64 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_65 is 'Records an amount associated with book summary level 65 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_66 is 'Records an amount associated with book summary level 66 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_67 is 'Records an amount associated with book summary level 67 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_68 is 'Records an amount associated with book summary level 68 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_69 is 'Records an amount associated with book summary level 69 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_70 is 'Records an amount associated with book summary level 70 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.NOTES is 'Records additional information used for processing.';
comment on column UNITIZE_PEND_TRANS_TEMP.TIME_STAMP is 'System-assigned timestamp used for audit purposes.';
comment on column UNITIZE_PEND_TRANS_TEMP.USER_ID is 'System-assigned user id used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1072, 0, 10, 4, 2, 0, 37318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037318_cwip_fast101.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;