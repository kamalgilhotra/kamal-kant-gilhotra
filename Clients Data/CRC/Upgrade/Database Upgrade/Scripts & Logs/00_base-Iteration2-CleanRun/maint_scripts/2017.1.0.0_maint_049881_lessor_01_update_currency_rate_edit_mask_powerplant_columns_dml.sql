/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049881_lessor_01_update_currency_rate_edit_mask_powerplant_columns_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/14/2017 Andrew Hill    Update edit masks for date columns on currency_rate in table maintenance
||============================================================================
*/

UPDATE powerplant_columns
SET edit_mask = 'mm/dd/yyyy'
WHERE table_name = 'currency_rate'
AND column_name = 'exchange_date';

UPDATE powerplant_columns
SET edit_mask = 'mm/dd/yyyy hh:mm am/pm'
WHERE table_name = 'currency_rate'
and column_name = 'time_stamp';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3951, 0, 2017, 1, 0, 0, 49881, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049881_lessor_01_update_currency_rate_edit_mask_powerplant_columns_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;