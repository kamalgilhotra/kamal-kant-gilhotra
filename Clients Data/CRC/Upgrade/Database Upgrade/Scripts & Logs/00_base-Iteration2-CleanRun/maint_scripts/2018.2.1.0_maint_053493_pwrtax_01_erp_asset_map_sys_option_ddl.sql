/*
||============================================================================
|| Application: PowerPlan
|| File Name:   2018.2.1.0_maint_053493_pwrtax_01_erp_asset_map_sys_option_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.1.0 04/29/2019 Sarah Byers      New System Option for "ERP Asset" mapping (Ecolab)
||============================================================================
*/

INSERT INTO ppbase_system_options
  (system_option_id, long_description, system_only, pp_default_value,
   option_value, is_base_option, allow_company_override)
VALUES
  ('ERP Asset Mapping',
   'Use ERP Asset Mapping for Additions and Retirements',
   0, 'No', 'No', 1, 0);
   
INSERT INTO ppbase_system_options_module (system_option_id, MODULE)
VALUES ('ERP Asset Mapping', 'powertax');

INSERT INTO ppbase_system_options_values (system_option_id, option_value)
VALUES ('ERP Asset Mapping', 'Yes');

INSERT INTO ppbase_system_options_values (system_option_id, option_value)
VALUES ('ERP Asset Mapping', 'No');

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (17382, 0, 2018, 2, 1, 0, 53493 , 'C:\PlasticWks\powerplant\sql\maint_scripts','2018.2.1.0_maint_053493_pwrtax_01_erp_asset_map_sys_option_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
