/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033375_lease_PKG_LEASE_CALC.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/09/2013 Kyle Peterson
||============================================================================
*/

create or replace package PKG_LEASE_CALC as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 07/10/2013 Ryan Oliveria  MLA functions (workflows and revisions)
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   || 10.4.1.0 07/30/2013 Kyle Peterson   Payments and Invoices
   ||============================================================================
   */

   procedure P_SET_ILR_ID(A_ILR_ID number);

   procedure P_INVOICE_COMPARE(MYINVOICE in LS_INVOICE%rowtype,
                               MYPAYMENT in LS_PAYMENT_HDR%rowtype);

   procedure P_PAYMENT_ROLLUP;

   function F_APPROVE_MLA(A_LEASE_ID number,
                          A_REVISION number) return number;

   function F_REJECT_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_SEND_MLA(A_LEASE_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_MLA(A_LEASE_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID number,
                                  A_REVISION number) return number;

   function F_APPROVE_ILR(A_ILR_ID   number,
                          A_REVISION number) return number;

   function F_REJECT_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_SEND_ILR(A_ILR_ID   number,
                       A_REVISION number) return number;

   function F_UNREJECT_ILR(A_ILR_ID   number,
                           A_REVISION number) return number;

   function F_UNSEND_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   number,
                                  A_REVISION number) return number;

   function F_ACCRUALS_CALC(A_COMPANY_ID in number,
                            A_MONTH      in date) return varchar2;

   function F_ACCRUALS_APPROVE(A_COMPANY_ID in number,
                               A_MONTH      in date) return varchar2;

   function F_PAYMENT_CALC(A_COMPANY_ID in number,
                           A_MONTH      in date) return varchar2;

   function F_PAYMENT_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date) return varchar2;

   function F_SEND_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UPDATE_WORKFLOW_PAYMENT(A_PAYMENT_ID number) return number;

   function F_REJECT_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UNREJECT_PAYMENT(A_PAYMENT_ID number) return number;
   --TO APPROVE SINGLE PAYMENT THROUGH WORKFLOW
   function F_APPROVE_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UNSEND_PAYMENT(A_PAYMENT_ID number) return number;

   function F_MATCH_INVOICES(A_MONTH in date) return varchar2;

   function F_GET_ILR_ID return number;

   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   out varchar2) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean) return number;

   function F_TAX_CALC(A_COMPANY_ID in number,
                       A_MONTH in date) return varchar2;

end PKG_LEASE_CALC;
/


create or replace package body PKG_LEASE_CALC as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0   07/22/2013 Brandon Beck      Modified Approval for ILRs
   || 10.4.1.0   07/22/2013 Brandon Beck    Modified Approval for ILRs
   || 10.4.1.0   07/22/2013 Brandon Beck     Modified Approval for ILRs
   ||============================================================================
   */

   L_ILR_ID number;
   type PAYMENT_HDR_TYPE is table of LS_PAYMENT_HDR%rowtype index by pls_integer;
   type INVOICE_HDR_TYPE is table of LS_INVOICE%rowtype index by pls_integer;
   type PAYMENT_LINE_TYPE is table of LS_PAYMENT_LINE%rowtype index by pls_integer;
   type ASSET_SCHEDULE_LINE_TYPE is table of LS_ASSET_SCHEDULE%rowtype index by pls_integer;
   type ASSET_SCHEDULE_LINE_TYPE2 is record(
      LS_ASSET_ID     LS_ASSET_SCHEDULE.LS_ASSET_ID%type,
      INTEREST_PAID   LS_ASSET_SCHEDULE.INTEREST_PAID%type,
      PRINCIPAL_PAID  LS_ASSET_SCHEDULE.PRINCIPAL_PAID%type,
      LEASE_ID        LS_LEASE.LEASE_ID%type,
      VENDOR_ID       LS_LEASE_VENDOR.VENDOR_ID%type,
      PAYMENT_PCT     LS_LEASE_VENDOR.PAYMENT_PCT%type,
      EXEC_PAID       LS_ASSET_SCHEDULE.EXECUTORY_PAID1%type,
      CONT_PAID       LS_ASSET_SCHEDULE.CONTINGENT_PAID1%type,
      TAX_PAID    LS_ASSET_SCHEDULE.CONTINGENT_PAID1%type,
      ROWNUMBER       number,
      SET_OF_BOOKS_ID LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID%type);
   type ASSET_SCHEDULE_LINE_TABLE is table of ASSET_SCHEDULE_LINE_TYPE2 index by pls_integer;
   type ASSET_TAX_LINE_TYPE is record(
      RATE LS_TAX_STATE_RATES.RATE%type,
      LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
      TAX_LOCAL_ID LS_TAX_LOCAL.TAX_LOCAL_ID%type,
      SET_OF_BOOKS_ID SET_OF_BOOKS.SET_OF_BOOKS_ID%type,
      AMOUNT LS_ASSET_SCHEDULE.INTEREST_ACCRUAL%type);
   type ASSET_TAX_TABLE is table of ASSET_TAX_LINE_TYPE index by pls_integer;

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_LOGERRORMESSAGE(A_ILR_ID in number,
                               A_MSG    in varchar2) is
      pragma autonomous_transaction;
   begin
      update LS_PEND_TRANSACTION set ERROR_MESSAGE = A_MSG where ILR_ID = A_ILR_ID;

      commit;
   end P_LOGERRORMESSAGE;

   --**************************************************************************
   --                            P_SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************

   --**************************************************************************
   --                            F_APPROVE_MLA
   --**************************************************************************

   function F_APPROVE_MLA(A_LEASE_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 3, APPROVAL_DATE = sysdate, CURRENT_REVISION = A_REVISION
       where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving MLA');
         return -1;
   end F_APPROVE_MLA;

   --**************************************************************************
   --                            F_REJECT_MLA
   --**************************************************************************

   function F_REJECT_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 4 where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting MLA');
         return -1;
   end F_REJECT_MLA;

   --**************************************************************************
   --                            F_SEND_MLA
   --**************************************************************************

   function F_SEND_MLA(A_LEASE_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN     number;
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin

      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      --Reject other revisions
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 5, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      --Call F_APPROVE_MLA for auto-approved assets
      commit;

      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LS_LEASE L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.LEASE_ID = A_LEASE_ID;

      if IS_AUTO = 'AUTO' then
         return F_APPROVE_MLA(A_LEASE_ID, A_REVISION);
      end if;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending MLA');
         return -1;
   end F_SEND_MLA;

   --**************************************************************************
   --                            F_UNREJECT_MLA
   --**************************************************************************

   function F_UNREJECT_MLA(A_LEASE_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting MLA');
         return -1;
   end F_UNREJECT_MLA;

   --**************************************************************************
   --                            F_UNSEND_MLA
   --**************************************************************************

   function F_UNSEND_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 1, APPROVAL_DATE = null where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending MLA');
         return -1;
   end F_UNSEND_MLA;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_MLA
   --**************************************************************************

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_LEASE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_LEASE_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'mla_approval'),
                                     0)
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Updating Workflow MLA');
         return -1;
   end F_UPDATE_WORKFLOW_MLA;

   --**************************************************************************
   --                            F_deletePendTrans
   --**************************************************************************
   function F_DELETEPENDTRANS(A_ILR_ID number,
                              A_MSG    out varchar2) return number is
      MY_RTN number;
   begin
      A_MSG := 'Clearing out ls_pend_class_code';
      delete from LS_PEND_CLASS_CODE
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_basis';
      delete from LS_PEND_BASIS
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_set_of_books';
      delete from LS_PEND_SET_OF_BOOKS
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_transaction';
      delete from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID;

      return 1;
   exception
      when others then
         return -1;
   end F_DELETEPENDTRANS;

   --**************************************************************************
   --                            F_archivePendTrans
   --**************************************************************************
   function F_ARCHIVEPENDTRANS(A_ILR_ID   in number,
                               A_REVISION in number,
                               A_MSG      out varchar2) return number is
      MY_RTN number;
   begin
      --archive
      A_MSG := 'Archiving ls_pend_transaction';
      insert into LS_PEND_TRANSACTION_ARC
         (LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, LS_ASSET_ID, POSTING_AMOUNT, POSTING_QUANTITY,
          ACTIVITY_CODE, GL_JE_CODE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID,
          FUNC_CLASS_ID, ILR_ID, COMPANY_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID,
          LEASED_ASSET_NUMBER, SUB_ACCOUNT_ID, WORK_ORDER_ID, SERIAL_NUMBER, REVISION)
         select LS_PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                LS_ASSET_ID,
                POSTING_AMOUNT,
                POSTING_QUANTITY,
                ACTIVITY_CODE,
                GL_JE_CODE,
                RETIREMENT_UNIT_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                FUNC_CLASS_ID,
                ILR_ID,
                COMPANY_ID,
                ASSET_LOCATION_ID,
                PROPERTY_GROUP_ID,
                LEASED_ASSET_NUMBER,
                SUB_ACCOUNT_ID,
                WORK_ORDER_ID,
                SERIAL_NUMBER,
                REVISION
           from LS_PEND_TRANSACTION L
          where L.ILR_ID = A_ILR_ID
            and L.REVISION = A_REVISION;

      A_MSG := 'Archiving ls_pend_basis';
      insert into LS_PEND_BASIS_ARC
         (LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5,
          BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14,
          BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70)
         select B.LS_PEND_TRANS_ID,
                B.TIME_STAMP,
                B.USER_ID,
                BASIS_1,
                BASIS_2,
                BASIS_3,
                BASIS_4,
                BASIS_5,
                BASIS_6,
                BASIS_7,
                BASIS_8,
                BASIS_9,
                BASIS_10,
                BASIS_11,
                BASIS_12,
                BASIS_13,
                BASIS_14,
                BASIS_15,
                BASIS_16,
                BASIS_17,
                BASIS_18,
                BASIS_19,
                BASIS_20,
                BASIS_21,
                BASIS_22,
                BASIS_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70
           from LS_PEND_BASIS B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      A_MSG := 'Archiving ls_pend_set_of_books';
      insert into LS_PEND_SET_OF_BOOKS_ARC
         (LS_PEND_TRANS_ID, SET_OF_BOOKS_ID, POSTING_AMOUNT)
         select B.LS_PEND_TRANS_ID, B.SET_OF_BOOKS_ID, B.POSTING_AMOUNT
           from LS_PEND_SET_OF_BOOKS B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      A_MSG := 'Archiving ls_pend_class_code';
      insert into LS_PEND_CLASS_CODE_ARC
         (CLASS_CODE_ID, LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, value)
         select B.CLASS_CODE_ID, B.LS_PEND_TRANS_ID, B.TIME_STAMP, B.USER_ID, B.VALUE
           from LS_PEND_CLASS_CODE B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      return F_DELETEPENDTRANS(A_ILR_ID, A_MSG);
   exception
      when others then
         return -1;
   end F_ARCHIVEPENDTRANS;

   --**************************************************************************
   --                            F_APPROVE_ILR
   --**************************************************************************
   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      rtn := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, A_STATUS, TRUE);
      return rtn;
   exception
      when others then
         P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

    function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                     A_REVISION in number,
                                     A_STATUS   out varchar2,
                                     A_LOG in boolean) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_STATUS := 'APPROVE ILR:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR:' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      --Approve this revision
      -- blank out the error message
      --if A_LOG then
         --A_STATUS := 'Clearing out error message';
         --PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
         --P_LOGERRORMESSAGE(A_ILR_ID, '');
      --end if;

      A_STATUS := 'Updating LS_ILR_APPROVAL1';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Updating LS_ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR L set ILR_STATUS_ID = 2, CURRENT_REVISION = A_REVISION where ILR_ID = A_ILR_ID;
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      -- LOAD THE CPR TABLES and create JEs... this is a call to the lease asset package
      A_STATUS := 'Loop over assets';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      for L_ASSET_ID in (select LS_ASSET_ID
                           from LS_PEND_TRANSACTION
                          where ILR_ID = A_ILR_ID
                            and REVISION = A_REVISION)
      loop
         A_STATUS := '   Processing asset_id: ' || TO_CHAR(L_ASSET_ID.LS_ASSET_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
         MY_STR := PKG_LEASE_ASSET_POST.F_ADD_ASSET(L_ASSET_ID.LS_ASSET_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE('   Returned: ' || MY_STR);
         if MY_STR <> 'OK' then
            -- log the error message
            A_STATUS := MY_STR;
            RAISE_APPLICATION_ERROR(-20000, 'Error Adding Leased Asset: ' || A_STATUS);
            return -1;
         end if;
      end loop;

      A_STATUS := 'Archiving';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      RTN := F_ARCHIVEPENDTRANS(A_ILR_ID, A_REVISION, A_STATUS);
      if RTN <> 1 then
         RAISE_APPLICATION_ERROR(-20000, 'Error Archiving Transactions: ' || A_STATUS);
         return -1;
      end if;



      return 1;
   exception
      when others then
      --if A_LOG then
         --P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);
      -- end if;
       PKG_PP_LOG.P_WRITE_MESSAGE(sqlerrm);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

   function F_APPROVE_ILR(A_ILR_ID   in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      MY_STR   varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      -- start by clearing our pend transaction
      RTN := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;
      return 1;
   exception
      when others then
         rollback;
         P_LOGERRORMESSAGE(A_ILR_ID, L_STATUS);
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_STATUS);
         return -1;
   end F_APPROVE_ILR;

   --**************************************************************************
   --                            F_REJECT_ILR
   --**************************************************************************

   function F_REJECT_ILR(A_ILR_ID   in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      L_STATUS varchar2(2000);
   begin
      RTN := F_DELETEPENDTRANS(A_ILR_ID, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Removing Transactions: ' || L_STATUS);
         return -1;
      end if;

      update LS_ILR_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L set ILR_STATUS_ID = 1 where ILR_ID = A_ILR_ID;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 2;

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_REJECT_ILR;

   --**************************************************************************
   --                            F_SEND_ILR
   -- Called when a user clicks send for approval (on an ILR)
   -- Need to mark the approval status to be sent.  And update the ILR to
   -- pending approval.  IN addition need to update the capitalized cost to
   -- be the beginning obligation on the asset schedule for the revision being
   -- sent for approval
   --**************************************************************************
   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   out varchar2) return number is
      RTN     number;
      ILR     number;
      WFS     number;
      SQLS    varchar2(32000);
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
      APPROVED_DATE date;
      START_DATE date;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_STATUS := 'SEND for approval:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR_ID: ' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Remove Prior pending transactions for ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      -- start by clearing our pend transaction
      RTN := F_DELETEPENDTRANS(A_ILR_ID, A_STATUS);
      if RTN <> 1 then
         return -1;
      end if;

      A_STATUS := 'Getting start date';
      select min(PAYMENT_TERM_DATE)
        into START_DATE
      from LS_ILR_PAYMENT_TERM
        where ILR_ID = A_ILR_ID
          and REVISION = A_REVISION;

      A_STATUS := 'Getting approved date';
      select min(PAYMENT_TERM_DATE)
        into APPROVED_DATE
      from ls_ilr_payment_term pt, ls_ilr_approval appr
      where pt.ilr_id = appr.ilr_id
      and pt.revision = appr.revision
      and appr.ilr_id = A_ILR_ID
      and appr.revision = (
         select max(revision)
         from ls_ilr_approval
         where ilr_id = appr.ilr_id
           and approval_status_id in (3,6)
           and revision <> A_REVISION
      );

      PKG_PP_LOG.P_WRITE_MESSAGE('   Start Date: ' || to_char(START_DATE, 'YYYY-MM-DD'));
      PKG_PP_LOG.P_WRITE_MESSAGE('   Approved Date: ' || to_char(APPROVED_DATE, 'YYYY-MM-DD'));

      if START_DATE <> APPROVED_DATE and APPROVED_DATE is not null then
        A_STATUS := 'The first payment term date can not be changed once an ILR has been approved';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

        return -1;
      end if;


      A_STATUS := 'updating LS_ILR_APPROVAL';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      A_STATUS := 'updating LS_ASSET';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      -- update the asset status to be pending.  Also update the cap cost here.
      update LS_ASSET L
         set LS_ASSET_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 1;

      -- identify whether or not it is an adjustment or addition
      A_STATUS := 'insert into LS_PEND_TRANSACTION';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_TRANSACTION
         (LS_PEND_TRANS_ID, LS_ASSET_ID, POSTING_AMOUNT, POSTING_QUANTITY, ACTIVITY_CODE,
          GL_JE_CODE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, FUNC_CLASS_ID, ILR_ID,
          COMPANY_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID, LEASED_ASSET_NUMBER, SUB_ACCOUNT_ID,
          WORK_ORDER_ID, SERIAL_NUMBER, REVISION)
         select LS_PEND_TRANSACTION_SEQ.NEXTVAL,
                L.LS_ASSET_ID,
                NVL(AA.BEG_CAPITAL_COST, 0) - NVL((select min(AA.BEG_CAPITAL_COST)
                                                    from LS_ASSET_SCHEDULE AA
                                                   where AA.LS_ASSET_ID = L.LS_ASSET_ID
                                                     and AA.REVISION = L.APPROVED_REVISION
                                                     and AA.SET_OF_BOOKS_ID = 1),
                                                  0),
                NVL(L.QUANTITY, 1),
                NVL((select case
                              when L.APPROVED_REVISION > 0 then
                               11
                              else
                               2
                           end
                      from LS_CPR_ASSET_MAP M
                     where M.LS_ASSET_ID = L.LS_ASSET_ID),
                    2),
                (select S.GL_JE_CODE
                   from STANDARD_JOURNAL_ENTRIES S, GL_JE_CONTROL G
                  where G.PROCESS_ID = 'LAM ADDS'
                    and G.JE_ID = S.JE_ID),
                L.RETIREMENT_UNIT_ID,
                L.UTILITY_ACCOUNT_ID,
                L.BUS_SEGMENT_ID,
                UA.FUNC_CLASS_ID,
                L.ILR_ID,
                L.COMPANY_ID,
                L.ASSET_LOCATION_ID,
                L.PROPERTY_GROUP_ID,
                L.LEASED_ASSET_NUMBER,
                L.SUB_ACCOUNT_ID,
                L.WORK_ORDER_ID,
                L.SERIAL_NUMBER,
                A_REVISION
           from LS_ASSET L,
                UTILITY_ACCOUNT UA,
                (select A.*,
                        ROW_NUMBER() OVER(partition by A.LS_ASSET_ID, A.SET_OF_BOOKS_ID, A.REVISION order by month) as THE_ROW
                   from LS_ASSET_SCHEDULE A, LS_ASSET LA
                  where A.LS_ASSET_ID = LA.LS_ASSET_ID
                    and LA.ILR_ID = A_ILR_ID
                    and A.REVISION = A_REVISION
                    and A.SET_OF_BOOKS_ID = 1) AA
          where L.ILR_ID = A_ILR_ID
            and AA.THE_ROW = 1
            and AA.LS_ASSET_ID = L.LS_ASSET_ID
            and UA.UTILITY_ACCOUNT_ID = L.UTILITY_ACCOUNT_ID
            and UA.BUS_SEGMENT_ID = L.BUS_SEGMENT_ID
            and exists (select 1
                   from LS_ASSET_SCHEDULE S
                  where S.REVISION = A_REVISION
                    and S.LS_ASSET_ID = L.LS_ASSET_ID);

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Getting Books Summary';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      select A.BOOK_SUMMARY_ID
        into RTN
        from LS_LEASE_CAP_TYPE A, LS_ILR_OPTIONS B
       where A.LS_LEASE_CAP_TYPE_ID = B.LEASE_CAP_TYPE_ID
         and B.ILR_ID = A_ILR_ID
         and B.REVISION = A_REVISION;

      PKG_PP_LOG.P_WRITE_MESSAGE('   ' || TO_CHAR(RTN));

      A_STATUS := 'insert into LS_PEND_BASIS';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_BASIS
         (LS_PEND_TRANS_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8,
          BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17,
          BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26,
          BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35,
          BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41, BASIS_42, BASIS_43, BASIS_44,
          BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50, BASIS_51, BASIS_52, BASIS_53,
          BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60, BASIS_61, BASIS_62,
          BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70)
         select B.LS_PEND_TRANS_ID,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
           from LS_PEND_TRANSACTION B
          where B.ILR_ID = A_ILR_ID;

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      -- backfill prior cpr_ldg_basis values
      A_STATUS := 'backfill buckets from cpr';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_PEND_BASIS PB
         set (BASIS_1,
               BASIS_2,
               BASIS_3,
               BASIS_4,
               BASIS_5,
               BASIS_6,
               BASIS_7,
               BASIS_8,
               BASIS_9,
               BASIS_10,
               BASIS_11,
               BASIS_12,
               BASIS_13,
               BASIS_14,
               BASIS_15,
               BASIS_16,
               BASIS_17,
               BASIS_18,
               BASIS_19,
               BASIS_20,
               BASIS_21,
               BASIS_22,
               BASIS_23,
               BASIS_24,
               BASIS_25,
               BASIS_26,
               BASIS_27,
               BASIS_28,
               BASIS_29,
               BASIS_30,
               BASIS_31,
               BASIS_32,
               BASIS_33,
               BASIS_34,
               BASIS_35,
               BASIS_36,
               BASIS_37,
               BASIS_38,
               BASIS_39,
               BASIS_40,
               BASIS_41,
               BASIS_42,
               BASIS_43,
               BASIS_44,
               BASIS_45,
               BASIS_46,
               BASIS_47,
               BASIS_48,
               BASIS_49,
               BASIS_50,
               BASIS_51,
               BASIS_52,
               BASIS_53,
               BASIS_54,
               BASIS_55,
               BASIS_56,
               BASIS_57,
               BASIS_58,
               BASIS_59,
               BASIS_60,
               BASIS_61,
               BASIS_62,
               BASIS_63,
               BASIS_64,
               BASIS_65,
               BASIS_66,
               BASIS_67,
               BASIS_68,
               BASIS_69,
               BASIS_70) =
              (select -1 * (C.BASIS_1 + P.BASIS_1),
                      -1 * (C.BASIS_2 + P.BASIS_2),
                      -1 * (C.BASIS_3 + P.BASIS_3),
                      -1 * (C.BASIS_4 + P.BASIS_4),
                      -1 * (C.BASIS_5 + P.BASIS_5),
                      -1 * (C.BASIS_6 + P.BASIS_6),
                      -1 * (C.BASIS_7 + P.BASIS_7),
                      -1 * (C.BASIS_8 + P.BASIS_8),
                      -1 * (C.BASIS_9 + P.BASIS_9),
                      -1 * (C.BASIS_10 + P.BASIS_10),
                      -1 * (C.BASIS_11 + P.BASIS_11),
                      -1 * (C.BASIS_12 + P.BASIS_12),
                      -1 * (C.BASIS_13 + P.BASIS_13),
                      -1 * (C.BASIS_14 + P.BASIS_14),
                      -1 * (C.BASIS_15 + P.BASIS_15),
                      -1 * (C.BASIS_16 + P.BASIS_16),
                      -1 * (C.BASIS_17 + P.BASIS_17),
                      -1 * (C.BASIS_18 + P.BASIS_18),
                      -1 * (C.BASIS_19 + P.BASIS_19),
                      -1 * (C.BASIS_20 + P.BASIS_20),
                      -1 * (C.BASIS_21 + P.BASIS_21),
                      -1 * (C.BASIS_22 + P.BASIS_22),
                      -1 * (C.BASIS_23 + P.BASIS_23),
                      -1 * (C.BASIS_24 + P.BASIS_24),
                      -1 * (C.BASIS_25 + P.BASIS_25),
                      -1 * (C.BASIS_26 + P.BASIS_26),
                      -1 * (C.BASIS_27 + P.BASIS_27),
                      -1 * (C.BASIS_28 + P.BASIS_28),
                      -1 * (C.BASIS_29 + P.BASIS_29),
                      -1 * (C.BASIS_30 + P.BASIS_30),
                      -1 * (C.BASIS_31 + P.BASIS_31),
                      -1 * (C.BASIS_32 + P.BASIS_32),
                      -1 * (C.BASIS_33 + P.BASIS_33),
                      -1 * (C.BASIS_34 + P.BASIS_34),
                      -1 * (C.BASIS_35 + P.BASIS_35),
                      -1 * (C.BASIS_36 + P.BASIS_36),
                      -1 * (C.BASIS_37 + P.BASIS_37),
                      -1 * (C.BASIS_38 + P.BASIS_38),
                      -1 * (C.BASIS_39 + P.BASIS_39),
                      -1 * (C.BASIS_40 + P.BASIS_40),
                      -1 * (C.BASIS_41 + P.BASIS_41),
                      -1 * (C.BASIS_42 + P.BASIS_42),
                      -1 * (C.BASIS_43 + P.BASIS_43),
                      -1 * (C.BASIS_44 + P.BASIS_44),
                      -1 * (C.BASIS_45 + P.BASIS_45),
                      -1 * (C.BASIS_46 + P.BASIS_46),
                      -1 * (C.BASIS_47 + P.BASIS_47),
                      -1 * (C.BASIS_48 + P.BASIS_48),
                      -1 * (C.BASIS_49 + P.BASIS_49),
                      -1 * (C.BASIS_50 + P.BASIS_50),
                      -1 * (C.BASIS_51 + P.BASIS_51),
                      -1 * (C.BASIS_52 + P.BASIS_52),
                      -1 * (C.BASIS_53 + P.BASIS_53),
                      -1 * (C.BASIS_54 + P.BASIS_54),
                      -1 * (C.BASIS_55 + P.BASIS_55),
                      -1 * (C.BASIS_56 + P.BASIS_56),
                      -1 * (C.BASIS_57 + P.BASIS_57),
                      -1 * (C.BASIS_58 + P.BASIS_58),
                      -1 * (C.BASIS_59 + P.BASIS_59),
                      -1 * (C.BASIS_60 + P.BASIS_60),
                      -1 * (C.BASIS_61 + P.BASIS_61),
                      -1 * (C.BASIS_62 + P.BASIS_62),
                      -1 * (C.BASIS_63 + P.BASIS_63),
                      -1 * (C.BASIS_64 + P.BASIS_64),
                      -1 * (C.BASIS_65 + P.BASIS_65),
                      -1 * (C.BASIS_66 + P.BASIS_66),
                      -1 * (C.BASIS_67 + P.BASIS_67),
                      -1 * (C.BASIS_68 + P.BASIS_68),
                      -1 * (C.BASIS_69 + P.BASIS_69),
                      -1 * (C.BASIS_70 + P.BASIS_70)
                 from CPR_LDG_BASIS C,
                      (select M1.ASSET_ID as ASSET_ID,
                              PT.LS_PEND_TRANS_ID as LS_PEND_TRANS_ID,
                              sum(NVL(PB.BASIS_1, 0)) as BASIS_1,
                              sum(NVL(PB.BASIS_2, 0)) as BASIS_2,
                              sum(NVL(PB.BASIS_3, 0)) as BASIS_3,
                              sum(NVL(PB.BASIS_4, 0)) as BASIS_4,
                              sum(NVL(PB.BASIS_5, 0)) as BASIS_5,
                              sum(NVL(PB.BASIS_6, 0)) as BASIS_6,
                              sum(NVL(PB.BASIS_7, 0)) as BASIS_7,
                              sum(NVL(PB.BASIS_8, 0)) as BASIS_8,
                              sum(NVL(PB.BASIS_9, 0)) as BASIS_9,
                              sum(NVL(PB.BASIS_10, 0)) as BASIS_10,
                              sum(NVL(PB.BASIS_11, 0)) as BASIS_11,
                              sum(NVL(PB.BASIS_12, 0)) as BASIS_12,
                              sum(NVL(PB.BASIS_13, 0)) as BASIS_13,
                              sum(NVL(PB.BASIS_14, 0)) as BASIS_14,
                              sum(NVL(PB.BASIS_15, 0)) as BASIS_15,
                              sum(NVL(PB.BASIS_16, 0)) as BASIS_16,
                              sum(NVL(PB.BASIS_17, 0)) as BASIS_17,
                              sum(NVL(PB.BASIS_18, 0)) as BASIS_18,
                              sum(NVL(PB.BASIS_19, 0)) as BASIS_19,
                              sum(NVL(PB.BASIS_20, 0)) as BASIS_20,
                              sum(NVL(PB.BASIS_21, 0)) as BASIS_21,
                              sum(NVL(PB.BASIS_22, 0)) as BASIS_22,
                              sum(NVL(PB.BASIS_23, 0)) as BASIS_23,
                              sum(NVL(PB.BASIS_24, 0)) as BASIS_24,
                              sum(NVL(PB.BASIS_25, 0)) as BASIS_25,
                              sum(NVL(PB.BASIS_26, 0)) as BASIS_26,
                              sum(NVL(PB.BASIS_27, 0)) as BASIS_27,
                              sum(NVL(PB.BASIS_28, 0)) as BASIS_28,
                              sum(NVL(PB.BASIS_29, 0)) as BASIS_29,
                              sum(NVL(PB.BASIS_30, 0)) as BASIS_30,
                              sum(NVL(PB.BASIS_31, 0)) as BASIS_31,
                              sum(NVL(PB.BASIS_32, 0)) as BASIS_32,
                              sum(NVL(PB.BASIS_33, 0)) as BASIS_33,
                              sum(NVL(PB.BASIS_34, 0)) as BASIS_34,
                              sum(NVL(PB.BASIS_35, 0)) as BASIS_35,
                              sum(NVL(PB.BASIS_36, 0)) as BASIS_36,
                              sum(NVL(PB.BASIS_37, 0)) as BASIS_37,
                              sum(NVL(PB.BASIS_38, 0)) as BASIS_38,
                              sum(NVL(PB.BASIS_39, 0)) as BASIS_39,
                              sum(NVL(PB.BASIS_40, 0)) as BASIS_40,
                              sum(NVL(PB.BASIS_41, 0)) as BASIS_41,
                              sum(NVL(PB.BASIS_42, 0)) as BASIS_42,
                              sum(NVL(PB.BASIS_43, 0)) as BASIS_43,
                              sum(NVL(PB.BASIS_44, 0)) as BASIS_44,
                              sum(NVL(PB.BASIS_45, 0)) as BASIS_45,
                              sum(NVL(PB.BASIS_46, 0)) as BASIS_46,
                              sum(NVL(PB.BASIS_47, 0)) as BASIS_47,
                              sum(NVL(PB.BASIS_48, 0)) as BASIS_48,
                              sum(NVL(PB.BASIS_49, 0)) as BASIS_49,
                              sum(NVL(PB.BASIS_50, 0)) as BASIS_50,
                              sum(NVL(PB.BASIS_51, 0)) as BASIS_51,
                              sum(NVL(PB.BASIS_52, 0)) as BASIS_52,
                              sum(NVL(PB.BASIS_53, 0)) as BASIS_53,
                              sum(NVL(PB.BASIS_54, 0)) as BASIS_54,
                              sum(NVL(PB.BASIS_55, 0)) as BASIS_55,
                              sum(NVL(PB.BASIS_56, 0)) as BASIS_56,
                              sum(NVL(PB.BASIS_57, 0)) as BASIS_57,
                              sum(NVL(PB.BASIS_58, 0)) as BASIS_58,
                              sum(NVL(PB.BASIS_59, 0)) as BASIS_59,
                              sum(NVL(PB.BASIS_60, 0)) as BASIS_60,
                              sum(NVL(PB.BASIS_61, 0)) as BASIS_61,
                              sum(NVL(PB.BASIS_62, 0)) as BASIS_62,
                              sum(NVL(PB.BASIS_63, 0)) as BASIS_63,
                              sum(NVL(PB.BASIS_64, 0)) as BASIS_64,
                              sum(NVL(PB.BASIS_65, 0)) as BASIS_65,
                              sum(NVL(PB.BASIS_66, 0)) as BASIS_66,
                              sum(NVL(PB.BASIS_67, 0)) as BASIS_67,
                              sum(NVL(PB.BASIS_68, 0)) as BASIS_68,
                              sum(NVL(PB.BASIS_69, 0)) as BASIS_69,
                              sum(NVL(PB.BASIS_70, 0)) as BASIS_70
                         from PEND_BASIS          PB,
                              PEND_TRANSACTION    PP,
                              LS_CPR_ASSET_MAP    M1,
                              LS_PEND_TRANSACTION PT
                        where PP.LDG_ASSET_ID(+) = M1.ASSET_ID
                          and PP.PEND_TRANS_ID = PB.PEND_TRANS_ID(+)
                          and pp.ferc_activity_code (+) = 2
                          and PT.ILR_ID = A_ILR_ID
                          and M1.LS_ASSET_ID = PT.LS_ASSET_ID
                        group by M1.ASSET_ID, PT.LS_PEND_TRANS_ID) P
                where C.ASSET_ID = P.ASSET_ID
                  and P.LS_PEND_TRANS_ID = PB.LS_PEND_TRANS_ID)
       where exists (select 1
                from LS_CPR_ASSET_MAP M, LS_PEND_TRANSACTION PT
               where M.LS_ASSET_ID = PT.LS_ASSET_ID
                 and PT.ILR_ID = A_ILR_ID
                 and PT.LS_PEND_TRANS_ID = PB.LS_PEND_TRANS_ID);

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      ILR      := A_ILR_ID;
      A_STATUS := 'dynmic update of basis bucket';
      SQLS     := 'UPDATE LS_PEND_BASIS B' || ' SET BASIS_' || TO_CHAR(RTN) || ' = ' || ' (' ||
                  ' SELECT B.BASIS_' || TO_CHAR(RTN) ||
                  '   + case when max(beg_capital_cost) = 0 then max(beg_obligation) else max(beg_capital_cost) end' ||
                  ' from' || ' (' ||
                  '  select pt.ls_pend_trans_id, las.beg_obligation, las.beg_capital_cost,' ||
                  '     row_number() over(partition by las.ls_asset_id, las.revision, las.set_of_books_id order by las.month) as the_row' ||
                  '  from ls_asset_schedule las, ls_pend_transaction pt' ||
                  '  where pt.ls_asset_id = las.ls_asset_id' || '  and las.revision = ' ||
                  TO_CHAR(A_REVISION) || '  and pt.ilr_id = ' || TO_CHAR(A_ILR_ID) || ' ) bbb' ||
                  ' where bbb.the_row = 1' || ' and bbb.ls_pend_trans_id = b.ls_pend_trans_id' ||
                  ' group by bbb.ls_pend_trans_id' || ' )' ||
                  ' where exists (select 1 from ls_pend_transaction d where d.ilr_id = ' ||
                  TO_CHAR(A_ILR_ID) || ' and b.ls_pend_trans_id = d.ls_pend_trans_id)';
      execute immediate SQLS;
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(SQLS);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'insert into ls_pend_class_code';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_CLASS_CODE
         (CLASS_CODE_ID, LS_PEND_TRANS_ID, value)
         select A.CLASS_CODE_ID, B.LS_PEND_TRANS_ID, A.VALUE
           from LS_ASSET_CLASS_CODE A, LS_PEND_TRANSACTION B
          where A.LS_ASSET_ID = B.LS_ASSET_ID
            and B.ILR_ID = A_ILR_ID;

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      -- insert into ls_pend_set_of_books
      A_STATUS := 'insert into ls_pend_set_of_books';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_SET_OF_BOOKS
         (LS_PEND_TRANS_ID, SET_OF_BOOKS_ID, POSTING_AMOUNT)
         select L.LS_PEND_TRANS_ID,
                S.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                S.BASIS_1_INDICATOR * PB.BASIS_1 + S.BASIS_2_INDICATOR * PB.BASIS_2 +
                S.BASIS_3_INDICATOR * PB.BASIS_3 + S.BASIS_4_INDICATOR * PB.BASIS_4 +
                S.BASIS_5_INDICATOR * PB.BASIS_5 + S.BASIS_6_INDICATOR * PB.BASIS_6 +
                S.BASIS_7_INDICATOR * PB.BASIS_7 + S.BASIS_8_INDICATOR * PB.BASIS_8 +
                S.BASIS_9_INDICATOR * PB.BASIS_9 + S.BASIS_10_INDICATOR * PB.BASIS_10 +
                S.BASIS_11_INDICATOR * PB.BASIS_11 + S.BASIS_12_INDICATOR * PB.BASIS_12 +
                S.BASIS_13_INDICATOR * PB.BASIS_13 + S.BASIS_14_INDICATOR * PB.BASIS_14 +
                S.BASIS_15_INDICATOR * PB.BASIS_15 + S.BASIS_16_INDICATOR * PB.BASIS_16 +
                S.BASIS_17_INDICATOR * PB.BASIS_17 + S.BASIS_18_INDICATOR * PB.BASIS_18 +
                S.BASIS_19_INDICATOR * PB.BASIS_19 + S.BASIS_20_INDICATOR * PB.BASIS_20 +
                S.BASIS_21_INDICATOR * PB.BASIS_21 + S.BASIS_22_INDICATOR * PB.BASIS_22 +
                S.BASIS_23_INDICATOR * PB.BASIS_23 + S.BASIS_24_INDICATOR * PB.BASIS_24 +
                S.BASIS_25_INDICATOR * PB.BASIS_25 + S.BASIS_26_INDICATOR * PB.BASIS_26 +
                S.BASIS_27_INDICATOR * PB.BASIS_27 + S.BASIS_28_INDICATOR * PB.BASIS_28 +
                S.BASIS_29_INDICATOR * PB.BASIS_29 + S.BASIS_30_INDICATOR * PB.BASIS_30 +
                S.BASIS_31_INDICATOR * PB.BASIS_31 + S.BASIS_32_INDICATOR * PB.BASIS_32 +
                S.BASIS_33_INDICATOR * PB.BASIS_33 + S.BASIS_34_INDICATOR * PB.BASIS_34 +
                S.BASIS_35_INDICATOR * PB.BASIS_35 + S.BASIS_36_INDICATOR * PB.BASIS_36 +
                S.BASIS_37_INDICATOR * PB.BASIS_37 + S.BASIS_38_INDICATOR * PB.BASIS_38 +
                S.BASIS_39_INDICATOR * PB.BASIS_39 + S.BASIS_40_INDICATOR * PB.BASIS_40 +
                S.BASIS_41_INDICATOR * PB.BASIS_41 + S.BASIS_42_INDICATOR * PB.BASIS_42 +
                S.BASIS_43_INDICATOR * PB.BASIS_43 + S.BASIS_44_INDICATOR * PB.BASIS_44 +
                S.BASIS_45_INDICATOR * PB.BASIS_45 + S.BASIS_46_INDICATOR * PB.BASIS_46 +
                S.BASIS_47_INDICATOR * PB.BASIS_47 + S.BASIS_48_INDICATOR * PB.BASIS_48 +
                S.BASIS_49_INDICATOR * PB.BASIS_49 + S.BASIS_50_INDICATOR * PB.BASIS_50 +
                S.BASIS_51_INDICATOR * PB.BASIS_51 + S.BASIS_52_INDICATOR * PB.BASIS_52 +
                S.BASIS_53_INDICATOR * PB.BASIS_53 + S.BASIS_54_INDICATOR * PB.BASIS_54 +
                S.BASIS_55_INDICATOR * PB.BASIS_55 + S.BASIS_56_INDICATOR * PB.BASIS_56 +
                S.BASIS_57_INDICATOR * PB.BASIS_57 + S.BASIS_58_INDICATOR * PB.BASIS_58 +
                S.BASIS_59_INDICATOR * PB.BASIS_59 + S.BASIS_60_INDICATOR * PB.BASIS_60 +
                S.BASIS_61_INDICATOR * PB.BASIS_61 + S.BASIS_62_INDICATOR * PB.BASIS_62 +
                S.BASIS_63_INDICATOR * PB.BASIS_63 + S.BASIS_64_INDICATOR * PB.BASIS_64 +
                S.BASIS_65_INDICATOR * PB.BASIS_65 + S.BASIS_66_INDICATOR * PB.BASIS_66 +
                S.BASIS_67_INDICATOR * PB.BASIS_67 + S.BASIS_68_INDICATOR * PB.BASIS_68 +
                S.BASIS_69_INDICATOR * PB.BASIS_69 + S.BASIS_70_INDICATOR * PB.BASIS_70 as AMOUNT
           from SET_OF_BOOKS S, COMPANY_SET_OF_BOOKS C, LS_PEND_BASIS PB, LS_PEND_TRANSACTION L
          where PB.LS_PEND_TRANS_ID = L.LS_PEND_TRANS_ID
            and C.COMPANY_ID = L.COMPANY_ID
            and S.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
            and L.ILR_ID = A_ILR_ID;
      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Setting posting amount on ls_pend_transaction';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_PEND_TRANSACTION PT
         set PT.POSTING_AMOUNT =
              (select S.POSTING_AMOUNT
                 from LS_PEND_SET_OF_BOOKS S
                where S.SET_OF_BOOKS_ID = 1
                  and S.LS_PEND_TRANS_ID = PT.LS_PEND_TRANS_ID)
       where ILR_ID = A_ILR_ID;

      --Update status of non-current revisions
      A_STATUS := 'update workflow';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update WORKFLOW
         set APPROVAL_STATUS_ID = 5
       where ID_FIELD1 = A_ILR_ID
         and ID_FIELD2 <> A_REVISION
         and APPROVAL_STATUS_ID = 2
         and SUBSYSTEM = 'ilr_approval';

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'update ls_ilr_approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 5
       where ILR_ID = A_ILR_ID
         and REVISION <> A_REVISION
         and APPROVAL_STATUS_ID = 2;

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

      return 1;
   exception
      when others then
         PKG_PP_LOG.P_WRITE_MESSAGE('Error Sending For Approval: ' || sqlerrm);

         return -1;
   end F_SEND_ILR_NO_COMMIT;

   function F_SEND_ILR(A_ILR_ID   in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      ILR      number;
      WFS      number;
      SQLS     varchar2(32000);
      L_STATUS varchar2(2000);
      IS_AUTO  WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin
      -- start by clearing our pend transaction
      RTN := F_SEND_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      --Call F_APPROVE_ILR for auto-approved assets
      L_STATUS := 'selecting if auto';
      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LS_ILR L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.ILR_ID = A_ILR_ID;

      if IS_AUTO = 'AUTO' then
         L_STATUS := 'sending for approval';
         RTN      := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
         if RTN <> 1 then
            rollback;
            RAISE_APPLICATION_ERROR(-20000, 'Error Approving: ' || L_STATUS);
            return -1;
         end if;
      end if;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending ILR: ' || L_STATUS);
         return -1;
   end F_SEND_ILR;

   --**************************************************************************
   --                            F_UNREJECT_ILR
   --**************************************************************************

   function F_UNREJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNREJECT_ILR;

   --**************************************************************************
   --                            F_UNSEND_ILR
   --**************************************************************************

   function F_UNSEND_ILR(A_ILR_ID   in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      L_STATUS varchar2(2000);
   begin
      RTN := F_DELETEPENDTRANS(A_ILR_ID, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Removing Transactions: ' || L_STATUS);
         return -1;
      end if;

      update LS_ILR_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L set ILR_STATUS_ID = 1 where ILR_ID = A_ILR_ID;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 2;

      --update workflow and ilr status

-- RO: This had to be removed to prevent deadlocks
--     Workflow gets updated in uo_workflow_tools
--
--      update WORKFLOW
--         set APPROVAL_STATUS_ID = 1
--       where ID_FIELD1 = A_ILR_ID
--         and ID_FIELD2 = A_REVISION
--         and APPROVAL_STATUS_ID = 2
--         and SUBSYSTEM = 'ilr_approval';

      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and APPROVAL_STATUS_ID = 2;

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNSEND_ILR;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_ILR
   --**************************************************************************

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_ILR_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_ILR_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'ilr_approval'),
                                     0)
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_ILR;

   --**************************************************************************
   --                            F_ACCRUALS_CALC
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_ACCRUALS_CALC(A_COMPANY_ID in number,
                            A_MONTH      in date) return varchar2 is
      L_STATUS      varchar2(2000);
      L_ASSET_SCH   ASSET_SCHEDULE_LINE_TYPE;
      SCHEDULEINDEX number;
      EXECACCRUAL   number;
      CONTACCRUAL   number;
   begin
      L_STATUS := 'Bulk collecting';
      select S.* bulk collect
        into L_ASSET_SCH
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and S.MONTH = A_MONTH;

      L_STATUS := 'Inserting interest accrual';
      forall SCHEDULEINDEX in indices of L_ASSET_SCH
         insert into LS_MONTHLY_ACCRUAL_STG
            (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL,
                    1,
                    L_ASSET_SCH                       (SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCH                       (SCHEDULEINDEX).INTEREST_ACCRUAL,
                    A_MONTH,
                    L_ASSET_SCH                       (SCHEDULEINDEX).SET_OF_BOOKS_ID
               from DUAL
              where L_ASSET_SCH(SCHEDULEINDEX).INTEREST_ACCRUAL <> 0);

      L_STATUS := 'Inserting executory accrual';
      forall SCHEDULEINDEX in indices of L_ASSET_SCH
         insert into LS_MONTHLY_ACCRUAL_STG
            (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL,
                    2,
                    L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID,
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL1, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).EXECUTORY_ACCRUAL2,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL3, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL4, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).EXECUTORY_ACCRUAL5,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL6, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL7, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).EXECUTORY_ACCRUAL8,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL9, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL10, 0),
                    A_MONTH,
                    L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from DUAL
              where NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL1, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL2, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL3, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).EXECUTORY_ACCRUAL4,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL5, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL6, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).EXECUTORY_ACCRUAL7,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL8, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).EXECUTORY_ACCRUAL9, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).EXECUTORY_ACCRUAL10,
                        0) <> 0);

      L_STATUS := 'Inserting contingent accrual';
      forall SCHEDULEINDEX in indices of L_ASSET_SCH
         insert into LS_MONTHLY_ACCRUAL_STG
            (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL,
                    3,
                    L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID,
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL1, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).CONTINGENT_ACCRUAL2,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL3, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL4, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).CONTINGENT_ACCRUAL5,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL6, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL7, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).CONTINGENT_ACCRUAL8,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL9, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL10, 0),
                    A_MONTH,
                    L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from DUAL
              where NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL1, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL2, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL3, 0) +
                    NVL

                    (L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL4, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL5, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL6, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).CONTINGENT_ACCRUAL7,
                        0) + NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL8, 0) +
                    NVL(L_ASSET_SCH(SCHEDULEINDEX).CONTINGENT_ACCRUAL9, 0) +
                    NVL(L_ASSET_SCH

                        (SCHEDULEINDEX).CONTINGENT_ACCRUAL10,
                        0) <> 0);

      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_ACCRUALS_CALC;

   --**************************************************************************
   --                            F_ACCRUALS_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve and post the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_ACCRUALS_APPROVE(A_COMPANY_ID in number,
                               A_MONTH      in date) return varchar2 is
      L_STATUS varchar2(2000);
      L_RTN    number;
   begin
      for L_ACCRUALS in (select H.LS_ASSET_ID as LS_ASSET_ID,
                                H.AMOUNT as AMOUNT,
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                case
                                   when H.ACCRUAL_TYPE_ID = 1 then
                                    3010
                                   when H.ACCRUAL_TYPE_ID = 2 then
                                    3012
                                   when H.ACCRUAL_TYPE_ID = 3 then
                                    3014
                                end as TRANS_TYPE,
                                H.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
                           from LS_MONTHLY_ACCRUAL_STG H, LS_ASSET A
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.AMOUNT <> 0
                            and H.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.COMPANY_ID = A_COMPANY_ID)
      loop
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_ACCRUALS.TRANS_TYPE);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
                                               -1,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               NVL(L_ACCRUALS.IN_SERVICE_DATE, sysdate),
                                               1,
                                               'LAMACC',
                                               L_ACCRUALS.SET_OF_BOOKS_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_ACCRUALS.TRANS_TYPE + 1);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE + 1,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
                                               -1,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               NVL(L_ACCRUALS.IN_SERVICE_DATE, sysdate),
                                               0,
                                               'LAMACC',
                                               L_ACCRUALS.SET_OF_BOOKS_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
      end loop;

      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_ACCRUALS_APPROVE;

   --**************************************************************************
   --                            F_PAYMENT_CALC
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_PAYMENT_CALC(A_COMPANY_ID in number,
                           A_MONTH      in date) return varchar2 is

      L_STATUS                varchar2(2000);
      L_RTN                   number;
      L_ASSET_SCHEDULE_LINE   ASSET_SCHEDULE_LINE_TABLE;
      L_ASSET_SCHEDULE_HEADER ASSET_SCHEDULE_LINE_TABLE;
      SCHEDULEINDEX           number;

   begin
      L_STATUS := 'Deleting';
      delete from LS_PAYMENT_APPROVAL
       where PAYMENT_ID in (select PAYMENT_ID
                              from LS_PAYMENT_HDR
                             where GL_POSTING_MO_YR = A_MONTH
                               and COMPANY_ID = A_COMPANY_ID);
      --Cascades to LS_INVOICE_PAYMENT_MAP and LS_PAYMENT_LINE
      delete from LS_PAYMENT_HDR
       where GL_POSTING_MO_YR = A_MONTH
         and COMPANY_ID = A_COMPANY_ID;

      select distinct 0, 0, 0, L.LEASE_ID, LV.VENDOR_ID, 0, 0, 0, 0, 0, 0 bulk collect
        into L_ASSET_SCHEDULE_HEADER
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I, LS_LEASE L, LS_LEASE_VENDOR LV
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and I.LEASE_ID = L.LEASE_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and LV.LEASE_ID = L.LEASE_ID
         and LV.COMPANY_ID = I.COMPANY_ID
         and S.MONTH = A_MONTH;

      select S.LS_ASSET_ID,
             S.INTEREST_PAID,
             S.PRINCIPAL_PAID,
             L.LEASE_ID,
             LV.VENDOR_ID,
             LV.PAYMENT_PCT,
             abs(LTEB.EXEC1 - 1) * NVL(S.EXECUTORY_PAID1, 0) + abs(LTEB.EXEC2 - 1) * NVL(S.EXECUTORY_PAID2, 0) + abs(LTEB.EXEC3 - 1) * NVL(S.EXECUTORY_PAID3, 0) +
             abs(LTEB.EXEC4 - 1) * NVL(S.EXECUTORY_PAID4, 0) + abs(LTEB.EXEC5 - 1) * NVL(S.EXECUTORY_PAID5, 0) + abs(LTEB.EXEC6 - 1) * NVL(S.EXECUTORY_PAID6, 0) +
             abs(LTEB.EXEC7 - 1) * NVL(S.EXECUTORY_PAID7, 0) + abs(LTEB.EXEC8 - 1) * NVL(S.EXECUTORY_PAID8, 0) + abs(LTEB.EXEC9 - 1) * NVL(S.EXECUTORY_PAID9, 0) +
             abs(LTEB.EXEC10 - 1) * NVL(S.EXECUTORY_PAID10, 0),
             abs(LTEB.CONT1 - 1) * NVL(S.CONTINGENT_PAID1, 0) + abs(LTEB.CONT2 - 1) * NVL(S.CONTINGENT_PAID2, 0) + abs(LTEB.CONT3 - 1) * NVL(S.CONTINGENT_PAID3, 0) +
             abs(LTEB.CONT4 - 1) * NVL(S.CONTINGENT_PAID4, 0) + abs(LTEB.CONT5 - 1) * NVL(S.CONTINGENT_PAID5, 0) + abs(LTEB.CONT6 - 1) * NVL(S.CONTINGENT_PAID6, 0) +
             abs(LTEB.CONT7 - 1) * NVL(S.CONTINGENT_PAID7, 0) + abs(LTEB.CONT8 - 1) * NVL(S.CONTINGENT_PAID8, 0) + abs(LTEB.CONT9 - 1) * NVL(S.CONTINGENT_PAID9, 0) +
             abs(LTEB.CONT10 - 1) * NVL(S.CONTINGENT_PAID10, 0),
             LTEB.EXEC1 * NVL(S.EXECUTORY_PAID1, 0) + LTEB.EXEC2 * NVL(S.EXECUTORY_PAID2, 0) + LTEB.EXEC3 * NVL(S.EXECUTORY_PAID3, 0) +
             LTEB.EXEC4 * NVL(S.EXECUTORY_PAID4, 0) + LTEB.EXEC5 * NVL(S.EXECUTORY_PAID5, 0) + LTEB.EXEC6 * NVL(S.EXECUTORY_PAID6, 0) +
             LTEB.EXEC7 * NVL(S.EXECUTORY_PAID7, 0) + LTEB.EXEC8 * NVL(S.EXECUTORY_PAID8, 0) + LTEB.EXEC9 * NVL(S.EXECUTORY_PAID9, 0) +
             LTEB.EXEC10 * NVL(S.EXECUTORY_PAID10, 0) + LTEB.CONT1 * NVL(S.CONTINGENT_PAID1, 0) + LTEB.CONT2 * NVL(S.CONTINGENT_PAID2, 0) +
             LTEB.CONT3 * NVL(S.CONTINGENT_PAID3, 0) + LTEB.CONT4 * NVL(S.CONTINGENT_PAID4, 0) + LTEB.CONT5 * NVL(S.CONTINGENT_PAID5, 0) +
             LTEB.CONT6 * NVL(S.CONTINGENT_PAID6, 0) + LTEB.CONT7 * NVL(S.CONTINGENT_PAID7, 0) + LTEB.CONT8 * NVL(S.CONTINGENT_PAID8, 0) +
             LTEB.CONT9 * NVL(S.CONTINGENT_PAID9, 0) + LTEB.CONT10 * NVL(S.CONTINGENT_PAID10, 0),
             ROW_NUMBER() OVER(partition by L.LEASE_ID, LV.VENDOR_ID, S.SET_OF_BOOKS_ID order by S.LS_ASSET_ID),
             S.SET_OF_BOOKS_ID bulk collect
        into L_ASSET_SCHEDULE_LINE
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I, LS_LEASE L, LS_LEASE_VENDOR LV, LS_ILR_OPTIONS O, LS_TAX_EXPENSE_BUCKET LTEB
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and I.LEASE_ID = L.LEASE_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and LV.LEASE_ID = L.LEASE_ID
         and LV.COMPANY_ID = I.COMPANY_ID
         and O.ILR_ID = I.ILR_ID
         and O.REVISION = I.CURRENT_REVISION
         and S.MONTH = add_months(A_MONTH, nvl(O.PAYMENT_SHIFT,0));

      L_STATUS := 'Inserting dummy headers';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_HEADER
         insert into LS_PAYMENT_HDR
            (PAYMENT_ID, LEASE_ID, VENDOR_ID, COMPANY_ID, GL_POSTING_MO_YR, PAYMENT_STATUS_ID)
            select LS_PAYMENT_HDR_SEQ.NEXTVAL,
                   L_ASSET_SCHEDULE_HEADER   (SCHEDULEINDEX).LEASE_ID,
                   L_ASSET_SCHEDULE_HEADER   (SCHEDULEINDEX).VENDOR_ID,
                   A_COMPANY_ID,
                   A_MONTH,
                   1
              from DUAL;

      --Insert into approval tables
      L_STATUS := 'Inserting into LS_PAYMENT_APPROVAL';
      insert into LS_PAYMENT_APPROVAL
         (PAYMENT_ID, TIME_STAMP, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID, APPROVER, APPROVAL_DATE,
          REJECTED)
         select PAYMENT_ID,
                sysdate,
                (select distinct DECODE(LG.PYMT_APPROVAL_FLAG, 1, 11, 4)
                   from LS_LEASE_GROUP LG, LS_LEASE L
                  where LG.LEASE_GROUP_ID = L.LEASE_GROUP_ID
                    and L.LEASE_ID = LS_PAYMENT_HDR.LEASE_ID),
                1,
                null,
                null,
                null
           from LS_PAYMENT_HDR
          where GL_POSTING_MO_YR = A_MONTH
            and COMPANY_ID = A_COMPANY_ID;

      L_STATUS := 'Principal';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    1,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PRINCIPAL_PAID,
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
                and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PRINCIPAL_PAID <> 0);

      L_STATUS := 'Interest';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    2,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).INTEREST_PAID,
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
                and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).INTEREST_PAID <> 0);

      L_STATUS := 'Executory';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    3,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).EXEC_PAID,
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
                and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).EXEC_PAID <> 0);

      L_STATUS := 'Contingent';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    4,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).CONT_PAID,
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
                and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).CONT_PAID <> 0);

      L_STATUS := 'Tax';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                  L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                  8,
                  L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                  L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).TAX_PAID,
                  A_MONTH,
                  L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
               and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
               and COMPANY_ID = A_COMPANY_ID
               and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
               and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).TAX_PAID <> 0);

      L_STATUS := 'Looping complete';
      P_PAYMENT_ROLLUP;
      L_STATUS := 'Roll up Complete';

      L_STATUS := 'Delete the payments with $0';
      delete from LS_PAYMENT_APPROVAL
       where PAYMENT_ID in (select PAYMENT_ID
                              from LS_PAYMENT_HDR
                             where AMOUNT = 0
                               and COMPANY_ID = A_COMPANY_ID
                               and GL_POSTING_MO_YR = A_MONTH);
      delete from LS_PAYMENT_HDR
       where AMOUNT = 0
         and COMPANY_ID = A_COMPANY_ID
         and GL_POSTING_MO_YR = A_MONTH;

      return 'OK';

   exception
      when others then
         return L_STATUS || sqlerrm;
   end F_PAYMENT_CALC;

   --**************************************************************************
   --                            F_PAYMENT_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve and post the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_PAYMENT_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date) return varchar2 is
      L_STATUS varchar2(2000);
      L_RTN    number;
   begin
      for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
                                H.VENDOR_ID as VENDOR_ID,
                                L.AMOUNT as PAYMENT_AMOUNT,
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                M.INVOICE_ID as INVOICE_ID,
                                case
                                   when L.PAYMENT_TYPE_ID = 1 then
                                    3018
                                   when L.PAYMENT_TYPE_ID in (2, 5) then
                                    3019
                                   when L.PAYMENT_TYPE_ID in (3, 6) then
                                    3020
                                   when L.PAYMENT_TYPE_ID in (4, 7) then
                                    3021
                                   when L.PAYMENT_TYPE_ID = 8 then
                                    3040
                                end as TRANS_TYPE,
                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
                           from LS_PAYMENT_HDR H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.PAYMENT_ID = L.PAYMENT_ID
                            and H.PAYMENT_ID = M.PAYMENT_ID
                            and L.AMOUNT <> 0
                            and L.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.COMPANY_ID = A_COMPANY_ID)
      loop
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_PAYMENTS.TRANS_TYPE);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               -1,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               NVL(L_PAYMENTS.IN_SERVICE_DATE, sysdate),
                                               1,
                                               'LAMPAY',
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit.  Payment credits all hit 3022
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                     ' trans type: 3022';
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               -1,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               NVL(L_PAYMENTS.IN_SERVICE_DATE, sysdate),
                                               0,
                                               'LAMPAY',
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
      end loop;

      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_PAYMENT_APPROVE;

   --**************************************************************************
   --                            F_SEND_PAYMENT
   --**************************************************************************

   function F_SEND_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LS_PAYMENT_HDR set PAYMENT_STATUS_ID = 2 where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_APPROVAL set APPROVAL_STATUS_ID = 2 where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending Payment');
         return -1;
   end F_SEND_PAYMENT;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_PAYMENT
   --**************************************************************************

   function F_UPDATE_WORKFLOW_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_PAYMENT_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_PAYMENT_ID)
                                        and SUBSYSTEM = 'ls_payment_approval'),
                                     0)
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_PAYMENT;

   --**************************************************************************
   --                            F_REJECT_PAYMENT
   --**************************************************************************

   function F_REJECT_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L set PAYMENT_STATUS_ID = 4 where L.PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting MLA');
         return -1;
   end F_REJECT_PAYMENT;

   --**************************************************************************
   --                            F_UNREJECT_PAYMENT
   --**************************************************************************

   function F_UNREJECT_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L set PAYMENT_STATUS_ID = 2 where L.PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting MLA');
         return -1;
   end F_UNREJECT_PAYMENT;

   --**************************************************************************
   --                            F_APPROVE_PAYMENT
   --**************************************************************************

   function F_APPROVE_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this payment
      update LS_PAYMENT_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L
         set PAYMENT_STATUS_ID = 3, GL_POSTING_MO_YR = sysdate
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving MLA');
         return -1;
   end F_APPROVE_PAYMENT;

   --**************************************************************************
   --                            F_UNSEND_PAYMENT
   --**************************************************************************

   function F_UNSEND_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L
         set PAYMENT_STATUS_ID = 1, GL_POSTING_MO_YR = null
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending MLA');
         return -1;
   end F_UNSEND_PAYMENT;

   --**************************************************************************
   --                            F_MATCH_INVOICES
   --**************************************************************************
   function F_MATCH_INVOICES(A_MONTH in date) return varchar2 is
      L_STATUS      varchar2(2000);
      L_RTN         number;
      MYPAYMENTHDRS PAYMENT_HDR_TYPE;
      MYINVOICEHDRS INVOICE_HDR_TYPE;
      INVOICEINDEX  number;
      PAYMENTINDEX  number;
   begin
      select * bulk collect
        into MYPAYMENTHDRS
        from LS_PAYMENT_HDR
       where GL_POSTING_MO_YR = A_MONTH;

      select * bulk collect into MYINVOICEHDRS from LS_INVOICE where GL_POSTING_MO_YR = A_MONTH;

      for INVOICEINDEX in 1 .. MYINVOICEHDRS.COUNT
      loop
         for PAYMENTINDEX in 1 .. MYPAYMENTHDRS.COUNT
         loop
            P_INVOICE_COMPARE(MYINVOICEHDRS(INVOICEINDEX), MYPAYMENTHDRS(PAYMENTINDEX));
         end loop;
      end loop;
      return 'OK';
   end F_MATCH_INVOICES;

   --**************************************************************************
   --                            P_INVOICE_COMPARE
   --**************************************************************************

   procedure P_INVOICE_COMPARE(MYINVOICE in LS_INVOICE%rowtype,
                               MYPAYMENT in LS_PAYMENT_HDR%rowtype) is
      TOLERANCE_PCT    number;
      LG_TOLERANCE_PCT number;
      IS_IN_TOLERANCE  number;
   begin
      if MYINVOICE.VENDOR_ID = MYPAYMENT.VENDOR_ID and MYINVOICE.COMPANY_ID = MYPAYMENT.COMPANY_ID and
         MYINVOICE.LEASE_ID = MYPAYMENT.LEASE_ID then
         --Check the tolerance status
         if MYPAYMENT.AMOUNT <> 0 then
            TOLERANCE_PCT := (MYPAYMENT.AMOUNT - MYINVOICE.INVOICE_AMOUNT) / MYPAYMENT.AMOUNT;
         else
            TOLERANCE_PCT := 1;
         end if;

         --
         select NVL(TOLERANCE_PCT, 0)
           into LG_TOLERANCE_PCT
           from LS_LEASE_GROUP
          where LEASE_GROUP_ID =
                (select LEASE_GROUP_ID from LS_LEASE where LEASE_ID = MYPAYMENT.LEASE_ID);

         --Accept it as being in tolerance if the field is null or zero on LS_LEASE_GROUP
         if ABS(TOLERANCE_PCT) < LG_TOLERANCE_PCT or LG_TOLERANCE_PCT = 0 then
            IS_IN_TOLERANCE := 1;
         else
            IS_IN_TOLERANCE := 0;
         end if;

         --INSERT INTO LS_INVOICE_PAYMENT_MAP (invoice_id, payment_id, in_tolerance) values (myInvoice.Invoice_id, myPayment.Payment_id, is_in_tolerance);
         merge into LS_INVOICE_PAYMENT_MAP LIPM
         using (select MYINVOICE.INVOICE_ID as INVOICE_ID,
                       MYPAYMENT.PAYMENT_ID as PAYMENT_ID,
                       IS_IN_TOLERANCE      as IS_IN_TOLERANCE
                  from DUAL)
         on (LIPM.INVOICE_ID = MYINVOICE.INVOICE_ID and LIPM.PAYMENT_ID = MYPAYMENT.PAYMENT_ID)
         when matched then
            update set LIPM.IN_TOLERANCE = IS_IN_TOLERANCE
         when not matched then
            insert
               (LIPM.INVOICE_ID, LIPM.PAYMENT_ID, LIPM.IN_TOLERANCE)
            values
               (MYINVOICE.INVOICE_ID, MYPAYMENT.PAYMENT_ID, IS_IN_TOLERANCE);

      end if;
   end P_INVOICE_COMPARE;

   --**************************************************************************
   --                            P_PAYMENT_ROLLUP
   --**************************************************************************
   procedure P_PAYMENT_ROLLUP is
      MYPAYMENTLINES PAYMENT_LINE_TYPE;
      LINESINDEX     number;
   begin
      select * bulk collect
        into MYPAYMENTLINES
        from LS_PAYMENT_LINE LP1
       where LP1.SET_OF_BOOKS_ID =
             (select min(SET_OF_BOOKS_ID)
                from LS_PAYMENT_LINE LP2
               where LP1.PAYMENT_ID = LP2.PAYMENT_ID
                 and LP1.GL_POSTING_MO_YR = LP2.GL_POSTING_MO_YR);

      update LS_PAYMENT_HDR set AMOUNT = 0;

      for LINESINDEX in 1 .. MYPAYMENTLINES.COUNT
      loop
         update LS_PAYMENT_HDR
            set AMOUNT = AMOUNT + MYPAYMENTLINES(LINESINDEX).AMOUNT
          where PAYMENT_ID = MYPAYMENTLINES(LINESINDEX).PAYMENT_ID;
      end loop;
   end P_PAYMENT_ROLLUP;

   --**************************************************************************
   --                            F_GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

   --**************************************************************************
   --                            F_TAX_CALC
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly tax amounts by ls_asset
   -- @@PARAMS
   --    date: a_month
   --       The month to process taxes for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_TAX_CALC(A_COMPANY_ID in number,
                            A_MONTH      in date) return varchar2 is
      L_STATUS      varchar2(2000);
      TAX_TABLE ASSET_TAX_TABLE;
      L_INDEX number;
      counter number;
      SQLS varchar2(4000);
      BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
   begin
      L_STATUS := 'Deleting old records';
      delete from LS_MONTHLY_TAX_STG
      where GL_POSTING_MO_YR = A_MONTH
      and LS_ASSET_ID in (select LS_ASSET_ID from LS_ASSET where COMPANY_ID = A_COMPANY_ID);

      L_STATUS := 'Bulk collecting';
      select RATES.RATE,
         RATES.LS_ASSET_ID,
         RATES.TAX_LOCAL_ID,
         LAS.SET_OF_BOOKS_ID,
         LAS.INTEREST_ACCRUAL + LAS.PRINCIPAL_ACCRUAL + NVL(LTBB.CONT1 * LAS.CONTINGENT_ACCRUAL1, 0) +
         NVL(LTBB.CONT2 * LAS.CONTINGENT_ACCRUAL2, 0) + NVL(LTBB.CONT3 * LAS.CONTINGENT_ACCRUAL3, 0) +
         NVL(LTBB.CONT4 * LAS.CONTINGENT_ACCRUAL4, 0) + NVL(LTBB.CONT5 * LAS.CONTINGENT_ACCRUAL5, 0) +
         NVL(LTBB.CONT6 * LAS.CONTINGENT_ACCRUAL6, 0) + NVL(LTBB.CONT7 * LAS.CONTINGENT_ACCRUAL7, 0) +
         NVL(LTBB.CONT8 * LAS.CONTINGENT_ACCRUAL8, 0) + NVL(LTBB.CONT9 * LAS.CONTINGENT_ACCRUAL9, 0) +
         NVL(LTBB.CONT10 * LAS.CONTINGENT_ACCRUAL10, 0) + NVL(LTBB.EXEC1 * LAS.EXECUTORY_ACCRUAL1, 0) +
         NVL(LTBB.EXEC2 * LAS.EXECUTORY_ACCRUAL2, 0) + NVL(LTBB.EXEC3 * LAS.EXECUTORY_ACCRUAL3, 0) +
         NVL(LTBB.EXEC4 * LAS.EXECUTORY_ACCRUAL4, 0) + NVL(LTBB.EXEC5 * LAS.EXECUTORY_ACCRUAL5, 0) +
         NVL(LTBB.EXEC6 * LAS.EXECUTORY_ACCRUAL6, 0) + NVL(LTBB.EXEC7 * LAS.EXECUTORY_ACCRUAL7, 0) +
         NVL(LTBB.EXEC8 * LAS.EXECUTORY_ACCRUAL8, 0) + NVL(LTBB.EXEC9 * LAS.EXECUTORY_ACCRUAL9, 0) +
         NVL(LTBB.EXEC10 * LAS.EXECUTORY_ACCRUAL10, 0)
         bulk collect
         into TAX_TABLE
      from (select sum(TAB1.RATE) RATE,
               TAB1.LS_ASSET_ID LS_ASSET_ID,
               TAB1.TAX_LOCAL_ID TAX_LOCAL_ID,
               TAB1.REVISION REVISION
           from (select LTSR.RATE            RATE,
                     LATM.STATUS_CODE_ID  STATUS_CODE_ID,
                     LATM.LS_ASSET_ID     LS_ASSET_ID,
                     LATM.TAX_LOCAL_ID    TAX_LOCAL_ID,
                     LA.APPROVED_REVISION REVISION
                 from LS_TAX_STATE_RATES LTSR,
                     ASSET_LOCATION     AL,
                     LS_TAX_LOCAL       LTL,
                     LS_ASSET_TAX_MAP   LATM,
                     LS_ASSET           LA
                where LTL.TAX_LOCAL_ID = LTSR.TAX_LOCAL_ID
                  and AL.STATE_ID = LTSR.STATE_ID
                  and AL.ASSET_LOCATION_ID = LA.TAX_ASSET_LOCATION_ID
                  and LA.LS_ASSET_ID = LATM.LS_ASSET_ID
                  and LTL.TAX_LOCAL_ID = LATM.TAX_LOCAL_ID
                  and LA.COMPANY_ID = A_COMPANY_ID
                  and LA.APPROVED_REVISION is not null
                  and LTSR.EFFECTIVE_DATE =
                     (select max(T2.EFFECTIVE_DATE)
                       from LS_TAX_STATE_RATES T2, LS_LEASE LL, LS_ILR LI, LS_LEASE_OPTIONS LLO
                      where T2.TAX_LOCAL_ID = LTSR.TAX_LOCAL_ID
                        and T2.STATE_ID = LTSR.STATE_ID
                        and T2.COMPANY_ID = LTSR.COMPANY_ID
                        and LA.ILR_ID = LI.ILR_ID
                        and LI.LEASE_ID = LL.LEASE_ID
                        and LL.CURRENT_REVISION = LLO.REVISION
                        and LLO.LEASE_ID = LL.LEASE_ID
                        and T2.EFFECTIVE_DATE <= case
                             when LLO.TAX_RATE_OPTION_ID = 1 then
                              sysdate
                             when LLO.TAX_RATE_OPTION_ID = 2 then
                              LL.MASTER_AGREEMENT_DATE
                           end)
               union
               select LTDR.RATE            RATE,
                     LATM.STATUS_CODE_ID  STATUS_CODE_ID,
                     LATM.LS_ASSET_ID     LS_ASSET_ID,
                     LATM.TAX_LOCAL_ID    TAX_LOCAL_ID,
                     LA.APPROVED_REVISION REVISION
                 from LS_TAX_DISTRICT_RATES LTDR,
                     ASSET_LOCATION        AL,
                     LS_TAX_LOCAL          LTL,
                     LS_ASSET_TAX_MAP      LATM,
                     LS_ASSET              LA
                where LTL.TAX_LOCAL_ID = LTDR.TAX_LOCAL_ID
                  and AL.LS_TAX_DISTRICT_ID = LTDR.LS_TAX_DISTRICT_ID
                  and AL.ASSET_LOCATION_ID = LA.TAX_ASSET_LOCATION_ID
                  and LA.LS_ASSET_ID = LATM.LS_ASSET_ID
                  and LTL.TAX_LOCAL_ID = LATM.TAX_LOCAL_ID
                  and LA.COMPANY_ID = A_COMPANY_ID
                  and LA.APPROVED_REVISION is not null
                  and LTDR.EFFECTIVE_DATE =
                     (select max(T2.EFFECTIVE_DATE)
                       from LS_TAX_DISTRICT_RATES T2, LS_LEASE LL, LS_ILR LI, LS_LEASE_OPTIONS LLO
                      where T2.TAX_LOCAL_ID = LTDR.TAX_LOCAL_ID
                        and T2.LS_TAX_DISTRICT_ID = LTDR.LS_TAX_DISTRICT_ID
                        and T2.COMPANY_ID = LTDR.COMPANY_ID
                        and LA.ILR_ID = LI.ILR_ID
                        and LI.LEASE_ID = LL.LEASE_ID
                        and LL.CURRENT_REVISION = LLO.REVISION
                        and LLO.LEASE_ID = LL.LEASE_ID
                        and T2.EFFECTIVE_DATE <= case
                             when LLO.TAX_RATE_OPTION_ID = 1 then
                              sysdate
                             when LLO.TAX_RATE_OPTION_ID = 2 then
                              LL.MASTER_AGREEMENT_DATE
                           end)) TAB1
          where TAB1.STATUS_CODE_ID = 1
          group by TAB1.LS_ASSET_ID, TAB1.TAX_LOCAL_ID, TAB1.REVISION) RATES,
         LS_ASSET_SCHEDULE LAS,
         LS_TAX_BASIS_BUCKETS LTBB
      where LAS.LS_ASSET_ID = RATES.LS_ASSET_ID
      and LAS.REVISION = RATES.REVISION
      and month = A_MONTH;

      L_STATUS := 'Calculating Taxes';
      forall L_INDEX in indices of TAX_TABLE
         insert into LS_MONTHLY_TAX_STG (LS_ASSET_ID, TAX_LOCAL_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID, AMOUNT)
         values
            (TAX_TABLE(L_INDEX).LS_ASSET_ID, TAX_TABLE(L_INDEX).TAX_LOCAL_ID, A_MONTH, TAX_TABLE(L_INDEX).SET_OF_BOOKS_ID, TAX_TABLE(L_INDEX).RATE * TAX_TABLE(L_INDEX).AMOUNT);

      L_STATUS := 'Insert tax amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET = 1;

      SQLS := 'update LS_ASSET_SCHEDULE '||
               'set LS_ASSET_SCHEDULE.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = ( '||
               'SELECT SUM(LS_MONTHLY_TAX_STG.AMOUNT) '||
               'from LS_MONTHLY_TAX_STG '||
               'where LS_ASSET_SCHEDULE.LS_ASSET_ID = LS_MONTHLY_TAX_STG.LS_ASSET_ID '||
               'and LS_ASSET_SCHEDULE.MONTH = LS_MONTHLY_TAX_STG.GL_POSTING_MO_YR '||
               'and LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID = LS_MONTHLY_TAX_STG.SET_OF_BOOKS_ID '||
               ') '||
               'where ls_asset_id in ( '||
               'select ls_asset_id from ls_asset la, ls_ilr li '||
               'where li.ilr_id = la.ilr_id '||
               'and li.company_id = '||to_char(A_COMPANY_ID)||' '||
               ') '||
               'and to_char(month) = '''||to_char(A_MONTH)||''' '||
               'and exists ( '||
               'select 1 '||
               'from ls_monthly_tax_stg '||
               'where ls_asset_id = ls_asset_schedule.ls_asset_id '||
               'and gl_posting_mo_yr = ls_asset_schedule.month '||
               'and set_of_books_id = ls_asset_schedule.set_of_books_id '||
               ') '||
               'and revision = (select approved_revision from ls_asset where ls_asset_id = ls_asset_schedule.ls_asset_id) ';

      L_STATUS := SQLS;
      execute immediate SQLS;

      SQLS := 'update LS_ASSET_SCHEDULE '||
               'set LS_ASSET_SCHEDULE.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = ( '||
               'SELECT SUM(LS_MONTHLY_TAX_STG.AMOUNT) '||
               'from LS_MONTHLY_TAX_STG '||
               'where LS_ASSET_SCHEDULE.LS_ASSET_ID = LS_MONTHLY_TAX_STG.LS_ASSET_ID '||
               'and LS_ASSET_SCHEDULE.MONTH = LS_MONTHLY_TAX_STG.GL_POSTING_MO_YR '||
               'and LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID = LS_MONTHLY_TAX_STG.SET_OF_BOOKS_ID '||
               ') '||
               'where ls_asset_id in ( '||
               'select ls_asset_id from ls_asset la, ls_ilr li '||
               'where li.ilr_id = la.ilr_id '||
               'and li.company_id = '||to_char(A_COMPANY_ID)||' '||
               ') '||
               'and to_char(month) = '''||to_char(A_MONTH)||''' '||
               'and exists ( '||
               'select 1 '||
               'from ls_monthly_tax_stg '||
               'where ls_asset_id = ls_asset_schedule.ls_asset_id '||
               'and gl_posting_mo_yr = ls_asset_schedule.month '||
               'and set_of_books_id = ls_asset_schedule.set_of_books_id '||
               ') '||
               'and revision = (select approved_revision from ls_asset where ls_asset_id = ls_asset_schedule.ls_asset_id) ';

      L_STATUS := SQLS;
      execute immediate SQLS;

      SQLS := 'update LS_ILR_SCHEDULE '||
               'set LS_ILR_SCHEDULE.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = ('||
               'select sum(nvl(LS_ASSET_SCHEDULE.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||',0) )'||
               'from LS_ASSET_SCHEDULE, LS_ASSET '||
               'where LS_ASSET_SCHEDULE.LS_ASSET_ID = LS_ASSET.LS_ASSET_ID and LS_ASSET.ILR_ID = LS_ILR_SCHEDULE.ILR_ID '||
               'and LS_ASSET_SCHEDULE.REVISION = LS_ILR_SCHEDULE.REVISION '||
               'and LS_ASSET_SCHEDULE.MONTH = LS_ILR_SCHEDULE.MONTH '||
               'and LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID = LS_ILR_SCHEDULE.SET_OF_BOOKS_ID) '||
            'where to_char(month) = '''||to_char(A_MONTH)||''' and ilr_id in (select ILR_ID from LS_ILR where COMPANY_ID = '||to_char(A_COMPANY_ID)||')';

      L_STATUS := SQLS;
      execute immediate SQLS;

      SQLS := 'update LS_ILR_SCHEDULE '||
               'set LS_ILR_SCHEDULE.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = ('||
               'select sum(nvl(LS_ASSET_SCHEDULE.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||',0) )'||
               'from LS_ASSET_SCHEDULE, LS_ASSET '||
               'where LS_ASSET_SCHEDULE.LS_ASSET_ID = LS_ASSET.LS_ASSET_ID and LS_ASSET.ILR_ID = LS_ILR_SCHEDULE.ILR_ID '||
               'and LS_ASSET_SCHEDULE.REVISION = LS_ILR_SCHEDULE.REVISION '||
               'and LS_ASSET_SCHEDULE.MONTH = LS_ILR_SCHEDULE.MONTH '||
               'and LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID = LS_ILR_SCHEDULE.SET_OF_BOOKS_ID) '||
            'where to_char(month) = '''||to_char(A_MONTH)||''' and ilr_id in (select ILR_ID from LS_ILR where COMPANY_ID = '||to_char(A_COMPANY_ID)||')';

      L_STATUS := SQLS;
      execute immediate SQLS;

      return 'OK';
   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_TAX_CALC;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end PKG_LEASE_CALC;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (723, 0, 10, 4, 1, 1, 33375, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033375_lease_PKG_LEASE_CALC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;