 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_042107_reg_cap_bdg_act_ddl.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/06/2015 Shane Ward     New Integration Piece
||============================================================================
*/

-- Activity Temp Table
CREATE GLOBAL TEMPORARY TABLE reg_fcst_cwip_load_act_temp (
  reg_company_id      NUMBER(22,0) NOT NULL,
  reg_component_id    NUMBER(22,0) NOT NULL,
  reg_activity_id     NUMBER(22,0) NOT NULL,
  reg_member_id       NUMBER(22,0) NOT NULL,
  reg_member_rule_id  NUMBER(22,0) NOT NULL,
  forecast_version_id NUMBER(22,0) NOT NULL,
  gl_month            NUMBER(22,1) NOT NULL,
  amount              NUMBER(22,2) NULL
)
  ON COMMIT delete ROWS;

ALTER TABLE reg_fcst_cwip_load_act_temp
  ADD CONSTRAINT pk_reg_fcst_cwip_load_act_temp PRIMARY KEY (
    reg_company_id,
    reg_component_id,
    reg_activity_id,
    reg_member_id,
    reg_member_rule_id,
    forecast_version_id,
    gl_month
  );

COMMENT ON table reg_fcst_cwip_load_act_temp IS 'Loading Temp table for CWIP Activity to Reg Activity';
COMMENT ON COLUMN reg_fcst_cwip_load_act_temp.reg_company_id IS 'Standard system-assigned identifier for the regulatory company';
COMMENT ON COLUMN reg_fcst_cwip_load_act_temp.reg_component_id IS 'Standard system-assigned identifier for the regulatory component';
COMMENT ON COLUMN reg_fcst_cwip_load_act_temp.reg_activity_id IS 'Standard system-assigned identifier for the regulatory activity';
COMMENT ON COLUMN reg_fcst_cwip_load_act_temp.reg_member_id IS 'Standard system-assigned identifier for the regulatory family member';
COMMENT ON COLUMN reg_fcst_cwip_load_act_temp.reg_member_rule_id IS 'System assigned identifier of the regulatory member rule';
COMMENT ON COLUMN reg_fcst_cwip_load_act_temp.forecast_version_id IS 'Standard system-assigned identifier for the forecast ledger';
COMMENT ON COLUMN reg_fcst_cwip_load_act_temp.gl_month IS 'General Ledger Month';
COMMENT ON COLUMN reg_fcst_cwip_load_act_temp.amount IS 'Amount pulled from CWIP';

--Add activity flag to budget view
CREATE OR REPLACE VIEW reg_budget_family_component (
  reg_component_id,
  reg_family_id,
  description,
  reg_acct_type_default,
  sub_acct_type_id,
  reg_annualization_id,
  acct_good_for,
  long_description,
  user_id,
  time_stamp,
  used_by_client,
  reg_cwip_source_id,
  hist_or_fcst,
  load_activity
) AS
select REG_COMPONENT_ID,
       REG_FAMILY_ID,
       DESCRIPTION,
       REG_ACCT_TYPE_DEFAULT,
       SUB_ACCT_TYPE_ID,
       REG_ANNUALIZATION_ID,
       ACCT_GOOD_FOR,
       LONG_DESCRIPTION,
       USER_ID,
       TIME_STAMP,
       USED_BY_CLIENT,
       REG_CWIP_SOURCE_ID,
       HIST_OR_FCST,
       load_activity
  from REG_CWIP_FAMILY_COMPONENT
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2146, 0, 2015, 1, 0, 0, 42107, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042107_reg_cap_bdg_act_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;