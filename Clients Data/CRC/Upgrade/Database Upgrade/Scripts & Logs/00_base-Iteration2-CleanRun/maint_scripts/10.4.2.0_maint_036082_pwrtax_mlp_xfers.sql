/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036082_pwrtax_mlp_xfers.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/19/2014 Andrew Scott        Transfers Interface for MLP on the
||                                         base gui.
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_input_book_transfers', 'Transfers', 'uo_tax_depr_act_wksp_intfc_xfer',
    'Transfers');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'depr_input_book_transfers', ENABLE_YN = 1
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_book_transfers';

----create a system option for the load activity traceback debug logging

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE,
    OPTION_VALUE, IS_BASE_OPTION)
values
   ('Transfer Load Traceback Debug Log', sysdate, user,
    'Whether to turn on the debug logging for the load activity traceback portion of the transfer interface.  Default is "No".',
    0, 'No', null, 1);
insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID)
values
   ('Transfer Load Traceback Debug Log', 'No', sysdate, user);
insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID)
values
   ('Transfer Load Traceback Debug Log', 'Yes', sysdate, user);
insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID)
values
   ('Transfer Load Traceback Debug Log', 'powertax', sysdate, user);


-- tax_co_def_tax_schema.  change the transfers interface and window to use this (instead of trying to figure it out
--       or let users select it
--
create table TAX_CO_DEF_TAX_SCHEMA
(
 COMPANY_ID             number(22,0) not null,
 EFFECTIVE_YEAR         number(22,0) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 DEFERRED_TAX_SCHEMA_ID number(22,0)
);

alter table TAX_CO_DEF_TAX_SCHEMA
   add constraint TAX_CO_DEF_TAX_SCHEMA_PK
       primary key (COMPANY_ID, EFFECTIVE_YEAR)
       using index tablespace PWRPLANT_IDX;

alter table TAX_CO_DEF_TAX_SCHEMA
   add constraint TAX_CO_DEF_TAX_SCHEMA_CO_FK
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

alter table TAX_CO_DEF_TAX_SCHEMA
   add constraint TAX_CO_DEF_TAX_SCHEMA_DTS_FK
       foreign key (DEFERRED_TAX_SCHEMA_ID)
       references DEFERRED_TAX_SCHEMA;

comment on table  TAX_CO_DEF_TAX_SCHEMA is '(S) [09] The Tax Co Def Tax Schema is a mapping table showing what deferred tax schema id should be used by company and effective year..';
comment on column TAX_CO_DEF_TAX_SCHEMA.COMPANY_ID is 'system-assigned identifier of a particular company.';
comment on column TAX_CO_DEF_TAX_SCHEMA.EFFECTIVE_YEAR is 'the year in which the deferred tax schema should be used with a particular vintage.  When a new effective year is added, vintages of that year onward will use the new mapping.';
comment on column TAX_CO_DEF_TAX_SCHEMA.TIME_STAMP is 'standard system-assigned time stamp used for audit purposes.';
comment on column TAX_CO_DEF_TAX_SCHEMA.USER_ID is 'standard system-assigned user id used for audit purposes.';
comment on column TAX_CO_DEF_TAX_SCHEMA.DEFERRED_TAX_SCHEMA_ID is 'system-assigned identifier of a particular deferred tax schema.';

insert into TAX_CO_DEF_TAX_SCHEMA
   (COMPANY_ID, EFFECTIVE_YEAR, DEFERRED_TAX_SCHEMA_ID)
   select COMPANY_ID, 1900, max(DEFERRED_TAX_SCHEMA_ID) DEFERRED_TAX_SCHEMA_ID
     from TAX_RECORD_CONTROL TRC, TAX_DEPRECIATION TD
    where TRC.TAX_RECORD_ID = TD.TAX_RECORD_ID
      and TD.TAX_BOOK_ID = 10
    group by TRC.COMPANY_ID;

----set up for powerplant tables and columns, both this new table and deferred tax schema
----run a delete statement first, in the unlikely event that deferred tax schema has been
----set up for that client.
delete from PP_TABLE_GROUPS where TABLE_NAME in ('tax_co_def_tax_schema', 'deferred_tax_schema');

delete from POWERPLANT_COLUMNS
 where TABLE_NAME in ('tax_co_def_tax_schema', 'deferred_tax_schema');

delete from POWERPLANT_TABLES where TABLE_NAME in ('tax_co_def_tax_schema', 'deferred_tax_schema');

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN)
values
   ('tax_co_def_tax_schema', 's', 'Tax Co Def Tax Schema', 'tax');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY, DROPDOWN_NAME)
values
   ('company_id', 'tax_co_def_tax_schema', 'p', 'Company', 1, 0, 'pp_company_filter');
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('effective_year', 'tax_co_def_tax_schema', 'e', 'Effective Year', 2, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY, DROPDOWN_NAME)
values
   ('deferred_tax_schema_id', 'tax_co_def_tax_schema', 'p', 'Deferred Tax Schema', 3, 0,
    'tax_def_tax_schema_filter');
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('time_stamp', 'tax_co_def_tax_schema', 'e', 'Time Stamp', 100, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('user_id', 'tax_co_def_tax_schema', 'e', 'User ID', 101, 0);

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN)
values
   ('deferred_tax_schema', 's', 'Deferred Tax Schema', 'tax');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('description', 'deferred_tax_schema', 'e', 'Description', 1, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('deferred_tax_schema_id', 'deferred_tax_schema', 's', 'Deferred Tax Schema ID', 2, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('time_stamp', 'deferred_tax_schema', 'e', 'Time Stamp', 100, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('user_id', 'deferred_tax_schema', 'e', 'User ID', 101, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (995, 0, 10, 4, 2, 0, 36082, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036082_pwrtax_mlp_xfers.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;