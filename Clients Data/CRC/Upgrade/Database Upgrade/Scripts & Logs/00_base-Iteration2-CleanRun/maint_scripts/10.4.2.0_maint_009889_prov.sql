/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009889_prov.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By      Reason for Change
|| -------- ---------- --------------- ---------------------------------------
|| 10.4.2.0 01/072014  Nathan Hollis   Point Release
||============================================================================
*/

create index TA_RESULTS_CON_JE_IDX1
   on TAX_ACCRUAL_RESULTS_CON_JE (JE_SCHEMA_ID)
      tablespace PWRPLANT_IDX;

create index TA_RESULTS_CON_JE_IDX2
   on TAX_ACCRUAL_RESULTS_CON_JE (COMPANY_ID, TA_VERSION_ID, ENTITY_ID, OPER_IND)
      tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (831, 0, 10, 4, 2, 0, 9889, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_009889_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;