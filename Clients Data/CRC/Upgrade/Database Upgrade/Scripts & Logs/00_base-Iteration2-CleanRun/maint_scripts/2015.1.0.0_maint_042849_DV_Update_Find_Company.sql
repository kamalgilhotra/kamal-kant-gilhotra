/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042849_DV_Update_Find_Company.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1	  3/17/2015   LAlston    	 	 Find Company SQL in Wo_Validation_Type was wrong
||==========================================================================================
*/

UPDATE wo_validation_type
SET FIND_COMPANY = 'SELECT MIN(company_id) FROM Company'
where wo_validation_type_id = 67;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2388, 0, 2015, 1, 0, 0, 042849, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042849_DV_Update_Find_Company.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;