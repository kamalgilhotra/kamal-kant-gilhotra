/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053118_lessee_01_purch_term_updates_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.2 02/26/2019  C yura           backfill purch/term option fields
||============================================================================
*/

-- Add purchase option record for revision 1 for records imported with amt via ILR import
-- Only add records if there is not a revision greater than 1 that does have an option record
INSERT INTO ls_ilr_purchase_options(ilr_id, revision, ilr_purchase_option_id, ilr_purchase_probability_id, purchase_option_type, purchase_amt)
SELECT ilro.ilr_id, 1, ls_ilr_purchase_options_seq.NEXTVAL, 1, ilro.purchase_option_type_id, ilro.purchase_option_amt
FROM ls_ilr_options ilro
WHERE ilro.revision = 1
AND (purchase_option_amt <> 0
OR purchase_option_type_id <> 1)
AND NOT EXISTS(
SELECT 1
FROM ls_ilr_purchase_options lipo
WHERE lipo.ilr_id = ilro.ilr_id
and lipo.revision = ilro.revision  )
AND NOT EXISTS(
SELECT 1
FROM ls_ilr_options lipo
WHERE lipo.ilr_id = ilro.ilr_id
and lipo.revision > 1 );

-- Add purchase option record for revision 1 for records imported with amt via ILR import
-- Only add records if there is not a revision greater than 1 that does have an option record
INSERT INTO ls_ilr_termination_options(ilr_id, revision, ilr_termination_option_id, ilr_termination_probability_id, termination_amt)
SELECT ilro.ilr_id, 1, ls_ilr_termination_options_seq.NEXTVAL, 1, ilro.termination_amt
FROM ls_ilr_options ilro
WHERE ilro.revision = 1
AND termination_amt <> 0
AND NOT EXISTS(
SELECT 1
FROM ls_ilr_termination_options lipo
WHERE lipo.ilr_id = ilro.ilr_id
and lipo.revision = ilro.revision  )
AND NOT EXISTS(
SELECT 1
FROM ls_ilr_options lipo
WHERE lipo.ilr_id = ilro.ilr_id
and lipo.revision > 1 );

-- Make fields not required in ILR import, should be using the imports for purch or term options instead 
update pp_import_column
set is_required = 0
where import_type_id in (
select import_type_id from pp_import_Type where trim(lower(import_table_name)) = 'ls_import_ilr' )
and is_required = 1
and trim(lower(column_name)) in ('purchase_option_amt', 'purchase_option_type_id', 'termination_amt');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15482, 0, 2018, 1, 0, 2, 53118, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053118_lessee_01_purch_term_updates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
