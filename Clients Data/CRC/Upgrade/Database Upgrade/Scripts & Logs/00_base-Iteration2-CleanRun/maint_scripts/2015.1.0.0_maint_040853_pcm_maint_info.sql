/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040853_pcm_maint_info.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   11/14/2014 Alex P.             Maint Info workspace
||
||========================================================================================
*/

/*
 *	Set up required fields
 */
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1000, 'work_order_control', 'description', 'uo_pcm_maint_wksp_info.dw_detail', 'Title' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1001, 'work_order_control', 'company_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Company' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1002, 'work_order_control', 'bus_segment_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Business Segment' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description, required_column_expression ) values ( 1003, 'work_order_control', 'wo_status_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Funding Project Status', 'funding_wo_indicator=1' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description, required_column_expression ) values ( 1004, 'work_order_control', 'wo_status_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Work Order Status', 'funding_wo_indicator=0' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description, required_column_expression ) values ( 1005, 'work_order_control', 'work_order_type_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Funding Project Type', 'funding_wo_indicator=1' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description, required_column_expression ) values ( 1006, 'work_order_control', 'work_order_type_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Work Order Type', 'funding_wo_indicator=0' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1007, 'work_order_control', 'major_location_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Major Location' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1008, 'work_order_control', 'department_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Department' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1009, 'work_order_control', 'est_start_date', 'uo_pcm_maint_wksp_info.dw_detail', 'Estimate Start Date' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1010, 'work_order_control', 'est_complete_date', 'uo_pcm_maint_wksp_info.dw_detail', 'Estimate Complete Date' );
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1011, 'work_order_control', 'est_in_service_date', 'uo_pcm_maint_wksp_info.dw_detail', 'Estimate In-Service Date' );

/*
 *	Add workspace identifier as the caller, so that dynamic validation can be customized based on which Maint workspace it is called from.
 */
update wo_validation_type 
set col2 = 'workspace_identifier'
where wo_validation_type_id = 6;

/*
 *	Link maint workspaces to the menu items.
 */
update ppbase_workspace
set workspace_uo_name = 'uo_pcm_maint_wksp_info'
where module = 'pcm' 
  and workspace_identifier in ( 'fp_maint_info', 'wo_maint_info' );
  

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2031, 0, 2015, 1, 0, 0, 040853, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040853_pcm_maint_info.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;