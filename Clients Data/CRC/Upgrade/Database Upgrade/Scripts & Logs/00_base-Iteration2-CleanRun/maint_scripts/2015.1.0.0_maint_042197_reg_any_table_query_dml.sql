/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042197_reg_any_table_query_dml.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   01/16/2015 Shane Ward    		    Any Query Setup for Reg
||========================================================================================
*/

INSERT INTO ppbase_workspace
	(MODULE,
	 workspace_identifier,
	 label,
	 workspace_uo_name,
	 minihelp,
	 object_type_id)
VALUES
	('REG',
	 'w_reg_any_table_query_options',
	 'Any Query Setup',
	 'w_reg_any_table_query_options',
	 'Create and Edit Reg Any Queries',
	 2);

INSERT INTO ppbase_menu_items
	(MODULE,
	 menu_identifier,
	 menu_level,
	 item_order,
	 label,
	 minihelp,
	 parent_menu_identifier,
	 workspace_identifier,
	 enable_yn)
VALUES
	('REG',
	 'REG_ANY_QUERY',
	 2,
	 9,
	 'Any Query',
	 'Create/Edit Any Queries',
	 'MONTHLY',
	 'w_reg_any_table_query_options',
	 1);

COMMENT ON COLUMN cr_dd_sources_criteria.source_id IS 'A pointer to the CR Sources table identifying the detail transaction table that the drilldown is being defined for.  Enter 1000 for the general ledger query definition. 1002,1003,1004 are Reg Any Query sources';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2181, 0, 2015, 1, 0, 0, 42197, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042197_reg_any_table_query_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;