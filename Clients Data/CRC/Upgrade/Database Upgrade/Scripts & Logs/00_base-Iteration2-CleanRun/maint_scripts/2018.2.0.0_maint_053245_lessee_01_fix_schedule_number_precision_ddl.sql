/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053245_lessee_01_fix_schedule_number_precision_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 03/29/2019 David Conway   Fix precision and scale on schedule number columns.
|| 2018.2.0.0 04/03/2019 David Conway   Re-worked to more gracefully handle errors, and be re-runnable.
|| 2018.2.0.0 04/04/2019 David Conway   Use raise_application_error in exception block, cast all columns
                                          to specified type, added dbms_output.enable, set user_id to (18) instead of (18 byte).
||============================================================================
*/
declare
  old_count integer;
  new_count integer;
  rows_mismatch exception;
begin
  DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
  DBMS_OUTPUT.PUT_LINE('53245 BEGIN');

  -- LS_ASSET_SCHEDULE
  select count(*) into old_count from tab where tname = 'LS_ASSET_SCHEDULE_53245';

  IF old_count > 0 THEN

    dbms_output.put('Dropping pre-existing LS_ASSET_SCHEDULE_53245...');
    execute immediate 'drop table ls_asset_schedule_53245';
    dbms_output.put_line(' Done.');

  END IF;

  dbms_output.put('Creating new table LS_ASSET_SCHEDULE_53245...');
  execute immediate '
    create table ls_asset_schedule_53245 as (
    select
        cast(LS_ASSET_ID                  as NUMBER(22,0)) as LS_ASSET_ID,
        cast(REVISION                     as NUMBER(22,0)) as REVISION,
        cast(SET_OF_BOOKS_ID              as NUMBER(22,0)) as SET_OF_BOOKS_ID,
        cast(MONTH                        as DATE)         as MONTH,
        cast(RESIDUAL_AMOUNT              as NUMBER(22,2)) as RESIDUAL_AMOUNT,
        cast(TERM_PENALTY                 as NUMBER(22,2)) as TERM_PENALTY,
        cast(BPO_PRICE                    as NUMBER(22,2)) as BPO_PRICE,
        cast(BEG_CAPITAL_COST             as NUMBER(22,2)) as BEG_CAPITAL_COST,
        cast(END_CAPITAL_COST             as NUMBER(22,2)) as END_CAPITAL_COST,
        cast(BEG_OBLIGATION               as NUMBER(22,2)) as BEG_OBLIGATION,
        cast(END_OBLIGATION               as NUMBER(22,2)) as END_OBLIGATION,
        cast(BEG_LT_OBLIGATION            as NUMBER(22,2)) as BEG_LT_OBLIGATION,
        cast(END_LT_OBLIGATION            as NUMBER(22,2)) as END_LT_OBLIGATION,
        cast(INTEREST_ACCRUAL             as NUMBER(22,2)) as INTEREST_ACCRUAL,
        cast(PRINCIPAL_ACCRUAL            as NUMBER(22,2)) as PRINCIPAL_ACCRUAL,
        cast(INTEREST_PAID                as NUMBER(22,2)) as INTEREST_PAID,
        cast(PRINCIPAL_PAID               as NUMBER(22,2)) as PRINCIPAL_PAID,
        cast(EXECUTORY_ACCRUAL1           as NUMBER(22,2)) as EXECUTORY_ACCRUAL1,
        cast(EXECUTORY_ACCRUAL2           as NUMBER(22,2)) as EXECUTORY_ACCRUAL2,
        cast(EXECUTORY_ACCRUAL3           as NUMBER(22,2)) as EXECUTORY_ACCRUAL3,
        cast(EXECUTORY_ACCRUAL4           as NUMBER(22,2)) as EXECUTORY_ACCRUAL4,
        cast(EXECUTORY_ACCRUAL5           as NUMBER(22,2)) as EXECUTORY_ACCRUAL5,
        cast(EXECUTORY_ACCRUAL6           as NUMBER(22,2)) as EXECUTORY_ACCRUAL6,
        cast(EXECUTORY_ACCRUAL7           as NUMBER(22,2)) as EXECUTORY_ACCRUAL7,
        cast(EXECUTORY_ACCRUAL8           as NUMBER(22,2)) as EXECUTORY_ACCRUAL8,
        cast(EXECUTORY_ACCRUAL9           as NUMBER(22,2)) as EXECUTORY_ACCRUAL9,
        cast(EXECUTORY_ACCRUAL10          as NUMBER(22,2)) as EXECUTORY_ACCRUAL10,
        cast(EXECUTORY_PAID1              as NUMBER(22,2)) as EXECUTORY_PAID1,
        cast(EXECUTORY_PAID2              as NUMBER(22,2)) as EXECUTORY_PAID2,
        cast(EXECUTORY_PAID3              as NUMBER(22,2)) as EXECUTORY_PAID3,
        cast(EXECUTORY_PAID4              as NUMBER(22,2)) as EXECUTORY_PAID4,
        cast(EXECUTORY_PAID5              as NUMBER(22,2)) as EXECUTORY_PAID5,
        cast(EXECUTORY_PAID6              as NUMBER(22,2)) as EXECUTORY_PAID6,
        cast(EXECUTORY_PAID7              as NUMBER(22,2)) as EXECUTORY_PAID7,
        cast(EXECUTORY_PAID8              as NUMBER(22,2)) as EXECUTORY_PAID8,
        cast(EXECUTORY_PAID9              as NUMBER(22,2)) as EXECUTORY_PAID9,
        cast(EXECUTORY_PAID10             as NUMBER(22,2)) as EXECUTORY_PAID10,
        cast(CONTINGENT_ACCRUAL1          as NUMBER(22,2)) as CONTINGENT_ACCRUAL1,
        cast(CONTINGENT_ACCRUAL2          as NUMBER(22,2)) as CONTINGENT_ACCRUAL2,
        cast(CONTINGENT_ACCRUAL3          as NUMBER(22,2)) as CONTINGENT_ACCRUAL3,
        cast(CONTINGENT_ACCRUAL4          as NUMBER(22,2)) as CONTINGENT_ACCRUAL4,
        cast(CONTINGENT_ACCRUAL5          as NUMBER(22,2)) as CONTINGENT_ACCRUAL5,
        cast(CONTINGENT_ACCRUAL6          as NUMBER(22,2)) as CONTINGENT_ACCRUAL6,
        cast(CONTINGENT_ACCRUAL7          as NUMBER(22,2)) as CONTINGENT_ACCRUAL7,
        cast(CONTINGENT_ACCRUAL8          as NUMBER(22,2)) as CONTINGENT_ACCRUAL8,
        cast(CONTINGENT_ACCRUAL9          as NUMBER(22,2)) as CONTINGENT_ACCRUAL9,
        cast(CONTINGENT_ACCRUAL10         as NUMBER(22,2)) as CONTINGENT_ACCRUAL10,
        cast(CONTINGENT_PAID1             as NUMBER(22,2)) as CONTINGENT_PAID1,
        cast(CONTINGENT_PAID2             as NUMBER(22,2)) as CONTINGENT_PAID2,
        cast(CONTINGENT_PAID3             as NUMBER(22,2)) as CONTINGENT_PAID3,
        cast(CONTINGENT_PAID4             as NUMBER(22,2)) as CONTINGENT_PAID4,
        cast(CONTINGENT_PAID5             as NUMBER(22,2)) as CONTINGENT_PAID5,
        cast(CONTINGENT_PAID6             as NUMBER(22,2)) as CONTINGENT_PAID6,
        cast(CONTINGENT_PAID7             as NUMBER(22,2)) as CONTINGENT_PAID7,
        cast(CONTINGENT_PAID8             as NUMBER(22,2)) as CONTINGENT_PAID8,
        cast(CONTINGENT_PAID9             as NUMBER(22,2)) as CONTINGENT_PAID9,
        cast(CONTINGENT_PAID10            as NUMBER(22,2)) as CONTINGENT_PAID10,
        cast(USER_ID                      as VARCHAR2(18)) as USER_ID,
        cast(TIME_STAMP                   as DATE)         as TIME_STAMP,
        cast(IS_OM                        as NUMBER(1,0))  as IS_OM,
        cast(CURRENT_LEASE_COST           as NUMBER(22,2)) as CURRENT_LEASE_COST,
        cast(BEG_DEFERRED_RENT            as NUMBER(22,2)) as BEG_DEFERRED_RENT,
        cast(DEFERRED_RENT                as NUMBER(22,2)) as DEFERRED_RENT,
        cast(END_DEFERRED_RENT            as NUMBER(22,2)) as END_DEFERRED_RENT,
        cast(BEG_ST_DEFERRED_RENT         as NUMBER(22,2)) as BEG_ST_DEFERRED_RENT,
        cast(END_ST_DEFERRED_RENT         as NUMBER(22,2)) as END_ST_DEFERRED_RENT,
        cast(BEG_LIABILITY                as NUMBER(22,2)) as BEG_LIABILITY,
        cast(END_LIABILITY                as NUMBER(22,2)) as END_LIABILITY,
        cast(BEG_LT_LIABILITY             as NUMBER(22,2)) as BEG_LT_LIABILITY,
        cast(END_LT_LIABILITY             as NUMBER(22,2)) as END_LT_LIABILITY,
        cast(PRINCIPAL_REMEASUREMENT      as NUMBER(22,2)) as PRINCIPAL_REMEASUREMENT,
        cast(LIABILITY_REMEASUREMENT      as NUMBER(22,2)) as LIABILITY_REMEASUREMENT,
        cast(INITIAL_DIRECT_COST          as NUMBER(22,2)) as INITIAL_DIRECT_COST,
        cast(INCENTIVE_AMOUNT             as NUMBER(22,2)) as INCENTIVE_AMOUNT,
        cast(BEG_PREPAID_RENT             as NUMBER(22,2)) as BEG_PREPAID_RENT,
        cast(PREPAY_AMORTIZATION          as NUMBER(22,2)) as PREPAY_AMORTIZATION,
        cast(PREPAID_RENT                 as NUMBER(22,2)) as PREPAID_RENT,
        cast(END_PREPAID_RENT             as NUMBER(22,2)) as END_PREPAID_RENT,
        cast(IDC_MATH_AMOUNT              as NUMBER(22,2)) as IDC_MATH_AMOUNT,
        cast(INCENTIVE_MATH_AMOUNT        as NUMBER(22,2)) as INCENTIVE_MATH_AMOUNT,
        cast(BEG_NET_ROU_ASSET            as NUMBER(22,2)) as BEG_NET_ROU_ASSET,
        cast(END_NET_ROU_ASSET            as NUMBER(22,2)) as END_NET_ROU_ASSET,
        cast(ROU_ASSET_REMEASUREMENT      as NUMBER(22,2)) as ROU_ASSET_REMEASUREMENT,
        cast(PARTIAL_TERM_GAIN_LOSS       as NUMBER(22,2)) as PARTIAL_TERM_GAIN_LOSS,
        cast(ADDITIONAL_ROU_ASSET         as NUMBER(22,2)) as ADDITIONAL_ROU_ASSET,
        cast(EXECUTORY_ADJUST             as NUMBER(22,2)) as EXECUTORY_ADJUST,
        cast(CONTINGENT_ADJUST            as NUMBER(22,2)) as CONTINGENT_ADJUST,
        cast(BEG_ARREARS_ACCRUAL          as NUMBER(22,2)) as BEG_ARREARS_ACCRUAL,
        cast(ARREARS_ACCRUAL              as NUMBER(22,2)) as ARREARS_ACCRUAL,
        cast(END_ARREARS_ACCRUAL          as NUMBER(22,2)) as END_ARREARS_ACCRUAL,
        cast(PARTIAL_MONTH_PERCENT        as NUMBER(22,8)) as PARTIAL_MONTH_PERCENT,
        cast(REMAINING_PRINCIPAL          as NUMBER(22,2)) as REMAINING_PRINCIPAL,
        cast(ST_OBLIGATION_REMEASUREMENT  as NUMBER(22,2)) as ST_OBLIGATION_REMEASUREMENT,
        cast(LT_OBLIGATION_REMEASUREMENT  as NUMBER(22,2)) as LT_OBLIGATION_REMEASUREMENT,
        cast(OBLIGATION_RECLASS           as NUMBER(22,2)) as OBLIGATION_RECLASS,
        cast(UNACCRUED_INTEREST_REMEASURE as NUMBER(22,2)) as UNACCRUED_INTEREST_REMEASURE,
        cast(UNACCRUED_INTEREST_RECLASS   as NUMBER(22,2)) as UNACCRUED_INTEREST_RECLASS,
        cast(BEGIN_ACCUM_IMPAIR           as NUMBER(22,2)) as BEGIN_ACCUM_IMPAIR,
        cast(IMPAIRMENT_ACTIVITY          as NUMBER(22,2)) as IMPAIRMENT_ACTIVITY,
        cast(END_ACCUM_IMPAIR             as NUMBER(22,2)) as END_ACCUM_IMPAIR
    from ls_asset_schedule
    )';
  dbms_output.put_line(' Done.');

  dbms_output.put('Comparing row counts...');
  execute immediate 'select (select count(*) from ls_asset_schedule) as c_old, (select count(*) from ls_asset_schedule_53245) as c_new from dual'
          into old_count, new_count;
  if old_count = new_count then
    dbms_output.put_line('Done.');
  else
    raise rows_mismatch;
  end if;
  
  dbms_output.put('Setting not null columns on LS_ASSET_SCHEDULE_53245...');
  execute immediate '
    alter table ls_asset_schedule_53245 modify (
      ls_asset_id     number(22,0) not null,
      revision        number(22,0)  not null,
      set_of_books_id number(22,0) not null,
      month           date         not null)
      ';
  dbms_output.put_line(' Done.');

  dbms_output.put('Dropping old table LS_ASSET_SCHEDULE...');
  execute immediate 'drop table ls_asset_schedule';
  dbms_output.put_line('Done.');

  dbms_output.put('Renaming LS_ASSET_SCHEDULE_53245 to LS_ASSET_SCHEDULE...');
  execute immediate 'alter table ls_asset_schedule_53245 rename to ls_asset_schedule';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding Primary key to LS_ASSET_SCHEDULE...');
  execute immediate '
    alter table ls_asset_schedule add constraint
      pk_ls_asset_schedule primary key (ls_asset_id, revision, set_of_books_id, month) using index tablespace pwrplant_idx
    ';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding index LS_ASSET_SCHEDULE_IDX to LS_ASSET_SCHEDULE...');
  execute immediate 'CREATE INDEX LS_ASSET_SCHEDULE_IDX ON LS_ASSET_SCHEDULE (REVISION , LS_ASSET_ID , SET_OF_BOOKS_ID ) TABLESPACE PWRPLANT_IDX';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding index LS_ASSET_SCHEDULE_TMTH_IDX to LS_ASSET_SCHEDULE...');
  execute immediate 'CREATE INDEX LS_ASSET_SCHEDULE_TMTH_IDX ON LS_ASSET_SCHEDULE (TRUNC(MONTH,''fmmonth'')) TABLESPACE PWRPLANT_IDX';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding index LS_ASSET_SCHEDULE_MANY_IDX to LS_ASSET_SCHEDULE...');
  execute immediate 'CREATE INDEX LS_ASSET_SCHEDULE_MANY_IDX ON LS_ASSET_SCHEDULE (LS_ASSET_ID, REVISION, MONTH, SET_OF_BOOKS_ID, TRUNC(MONTH,''fmmonth'')) TABLESPACE PWRPLANT_IDX';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding index LS_ASSET_SCHEDULE_OM_IDX to LS_ASSET_SCHEDULE...');
  execute immediate 'CREATE INDEX LS_ASSET_SCHEDULE_OM_IDX ON LS_ASSET_SCHEDULE (IS_OM, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH ) TABLESPACE PWRPLANT_IDX';
  dbms_output.put_line('Done.');
  
  --LS_ILR_SCHEDULE
  select count(*) into old_count from tab where tname = 'LS_ILR_SCHEDULE_53245';

  IF old_count > 0 THEN
    dbms_output.put('Dropping pre-existing LS_ILR_SCHEDULE_53245...');
    execute immediate 'drop table ls_ilr_schedule_53245';
    dbms_output.put_line(' Done.');
  END IF;

  dbms_output.put('Creating new table LS_ILR_SCHEDULE_53245...');
  execute immediate '
    create table ls_ilr_schedule_53245 as (
      select
        cast(ILR_ID                       as NUMBER(22,0)) as ILR_ID,
        cast(REVISION                     as NUMBER(22,0)) as REVISION,
        cast(SET_OF_BOOKS_ID              as NUMBER(22,0)) as SET_OF_BOOKS_ID,
        cast(MONTH                        as DATE)         as MONTH,
        cast(RESIDUAL_AMOUNT              as NUMBER(22,2)) as RESIDUAL_AMOUNT,
        cast(TERM_PENALTY                 as NUMBER(22,2)) as TERM_PENALTY,
        cast(BPO_PRICE                    as NUMBER(22,2)) as BPO_PRICE,
        cast(BEG_CAPITAL_COST             as NUMBER(22,2)) as BEG_CAPITAL_COST,
        cast(END_CAPITAL_COST             as NUMBER(22,2)) as END_CAPITAL_COST,
        cast(BEG_OBLIGATION               as NUMBER(22,2)) as BEG_OBLIGATION,
        cast(END_OBLIGATION               as NUMBER(22,2)) as END_OBLIGATION,
        cast(BEG_LT_OBLIGATION            as NUMBER(22,2)) as BEG_LT_OBLIGATION,
        cast(END_LT_OBLIGATION            as NUMBER(22,2)) as END_LT_OBLIGATION,
        cast(INTEREST_ACCRUAL             as NUMBER(22,2)) as INTEREST_ACCRUAL,
        cast(PRINCIPAL_ACCRUAL            as NUMBER(22,2)) as PRINCIPAL_ACCRUAL,
        cast(INTEREST_PAID                as NUMBER(22,2)) as INTEREST_PAID,
        cast(PRINCIPAL_PAID               as NUMBER(22,2)) as PRINCIPAL_PAID,
        cast(EXECUTORY_ACCRUAL1           as NUMBER(22,2)) as EXECUTORY_ACCRUAL1,
        cast(EXECUTORY_ACCRUAL2           as NUMBER(22,2)) as EXECUTORY_ACCRUAL2,
        cast(EXECUTORY_ACCRUAL3           as NUMBER(22,2)) as EXECUTORY_ACCRUAL3,
        cast(EXECUTORY_ACCRUAL4           as NUMBER(22,2)) as EXECUTORY_ACCRUAL4,
        cast(EXECUTORY_ACCRUAL5           as NUMBER(22,2)) as EXECUTORY_ACCRUAL5,
        cast(EXECUTORY_ACCRUAL6           as NUMBER(22,2)) as EXECUTORY_ACCRUAL6,
        cast(EXECUTORY_ACCRUAL7           as NUMBER(22,2)) as EXECUTORY_ACCRUAL7,
        cast(EXECUTORY_ACCRUAL8           as NUMBER(22,2)) as EXECUTORY_ACCRUAL8,
        cast(EXECUTORY_ACCRUAL9           as NUMBER(22,2)) as EXECUTORY_ACCRUAL9,
        cast(EXECUTORY_ACCRUAL10          as NUMBER(22,2)) as EXECUTORY_ACCRUAL10,
        cast(EXECUTORY_PAID1              as NUMBER(22,2)) as EXECUTORY_PAID1,
        cast(EXECUTORY_PAID2              as NUMBER(22,2)) as EXECUTORY_PAID2,
        cast(EXECUTORY_PAID3              as NUMBER(22,2)) as EXECUTORY_PAID3,
        cast(EXECUTORY_PAID4              as NUMBER(22,2)) as EXECUTORY_PAID4,
        cast(EXECUTORY_PAID5              as NUMBER(22,2)) as EXECUTORY_PAID5,
        cast(EXECUTORY_PAID6              as NUMBER(22,2)) as EXECUTORY_PAID6,
        cast(EXECUTORY_PAID7              as NUMBER(22,2)) as EXECUTORY_PAID7,
        cast(EXECUTORY_PAID8              as NUMBER(22,2)) as EXECUTORY_PAID8,
        cast(EXECUTORY_PAID9              as NUMBER(22,2)) as EXECUTORY_PAID9,
        cast(EXECUTORY_PAID10             as NUMBER(22,2)) as EXECUTORY_PAID10,
        cast(CONTINGENT_ACCRUAL1          as NUMBER(22,2)) as CONTINGENT_ACCRUAL1,
        cast(CONTINGENT_ACCRUAL2          as NUMBER(22,2)) as CONTINGENT_ACCRUAL2,
        cast(CONTINGENT_ACCRUAL3          as NUMBER(22,2)) as CONTINGENT_ACCRUAL3,
        cast(CONTINGENT_ACCRUAL4          as NUMBER(22,2)) as CONTINGENT_ACCRUAL4,
        cast(CONTINGENT_ACCRUAL5          as NUMBER(22,2)) as CONTINGENT_ACCRUAL5,
        cast(CONTINGENT_ACCRUAL6          as NUMBER(22,2)) as CONTINGENT_ACCRUAL6,
        cast(CONTINGENT_ACCRUAL7          as NUMBER(22,2)) as CONTINGENT_ACCRUAL7,
        cast(CONTINGENT_ACCRUAL8          as NUMBER(22,2)) as CONTINGENT_ACCRUAL8,
        cast(CONTINGENT_ACCRUAL9          as NUMBER(22,2)) as CONTINGENT_ACCRUAL9,
        cast(CONTINGENT_ACCRUAL10         as NUMBER(22,2)) as CONTINGENT_ACCRUAL10,
        cast(CONTINGENT_PAID1             as NUMBER(22,2)) as CONTINGENT_PAID1,
        cast(CONTINGENT_PAID2             as NUMBER(22,2)) as CONTINGENT_PAID2,
        cast(CONTINGENT_PAID3             as NUMBER(22,2)) as CONTINGENT_PAID3,
        cast(CONTINGENT_PAID4             as NUMBER(22,2)) as CONTINGENT_PAID4,
        cast(CONTINGENT_PAID5             as NUMBER(22,2)) as CONTINGENT_PAID5,
        cast(CONTINGENT_PAID6             as NUMBER(22,2)) as CONTINGENT_PAID6,
        cast(CONTINGENT_PAID7             as NUMBER(22,2)) as CONTINGENT_PAID7,
        cast(CONTINGENT_PAID8             as NUMBER(22,2)) as CONTINGENT_PAID8,
        cast(CONTINGENT_PAID9             as NUMBER(22,2)) as CONTINGENT_PAID9,
        cast(CONTINGENT_PAID10            as NUMBER(22,2)) as CONTINGENT_PAID10,
        cast(USER_ID                      as VARCHAR2(18)) as USER_ID,
        cast(TIME_STAMP                   as DATE)         as TIME_STAMP,
        cast(IS_OM                        as NUMBER(1,0))  as IS_OM,
        cast(CURRENT_LEASE_COST           as NUMBER(22,2)) as CURRENT_LEASE_COST,
        cast(BEG_DEFERRED_RENT            as NUMBER(22,2)) as BEG_DEFERRED_RENT,
        cast(DEFERRED_RENT                as NUMBER(22,2)) as DEFERRED_RENT,
        cast(END_DEFERRED_RENT            as NUMBER(22,2)) as END_DEFERRED_RENT,
        cast(BEG_ST_DEFERRED_RENT         as NUMBER(22,2)) as BEG_ST_DEFERRED_RENT,
        cast(END_ST_DEFERRED_RENT         as NUMBER(22,2)) as END_ST_DEFERRED_RENT,
        cast(BEG_LIABILITY                as NUMBER(22,2)) as BEG_LIABILITY,
        cast(END_LIABILITY                as NUMBER(22,2)) as END_LIABILITY,
        cast(BEG_LT_LIABILITY             as NUMBER(22,2)) as BEG_LT_LIABILITY,
        cast(END_LT_LIABILITY             as NUMBER(22,2)) as END_LT_LIABILITY,
        cast(PRINCIPAL_REMEASUREMENT      as NUMBER(22,2)) as PRINCIPAL_REMEASUREMENT,
        cast(LIABILITY_REMEASUREMENT      as NUMBER(22,2)) as LIABILITY_REMEASUREMENT,
        cast(INITIAL_DIRECT_COST          as NUMBER(22,2)) as INITIAL_DIRECT_COST,
        cast(INCENTIVE_AMOUNT             as NUMBER(22,2)) as INCENTIVE_AMOUNT,
        cast(BEG_PREPAID_RENT             as NUMBER(22,2)) as BEG_PREPAID_RENT,
        cast(PREPAY_AMORTIZATION          as NUMBER(22,2)) as PREPAY_AMORTIZATION,
        cast(PREPAID_RENT                 as NUMBER(22,2)) as PREPAID_RENT,
        cast(END_PREPAID_RENT             as NUMBER(22,2)) as END_PREPAID_RENT,
        cast(IDC_MATH_AMOUNT              as NUMBER(22,2)) as IDC_MATH_AMOUNT,
        cast(INCENTIVE_MATH_AMOUNT        as NUMBER(22,2)) as INCENTIVE_MATH_AMOUNT,
        cast(BEG_NET_ROU_ASSET            as NUMBER(22,2)) as BEG_NET_ROU_ASSET,
        cast(END_NET_ROU_ASSET            as NUMBER(22,2)) as END_NET_ROU_ASSET,
        cast(ROU_ASSET_REMEASUREMENT      as NUMBER(22,2)) as ROU_ASSET_REMEASUREMENT,
        cast(PARTIAL_TERM_GAIN_LOSS       as NUMBER(22,2)) as PARTIAL_TERM_GAIN_LOSS,
        cast(ADDITIONAL_ROU_ASSET         as NUMBER(22,2)) as ADDITIONAL_ROU_ASSET,
        cast(EXECUTORY_ADJUST             as NUMBER(22,2)) as EXECUTORY_ADJUST,
        cast(CONTINGENT_ADJUST            as NUMBER(22,2)) as CONTINGENT_ADJUST,
        cast(BEG_ARREARS_ACCRUAL          as NUMBER(22,2)) as BEG_ARREARS_ACCRUAL,
        cast(ARREARS_ACCRUAL              as NUMBER(22,2)) as ARREARS_ACCRUAL,
        cast(END_ARREARS_ACCRUAL          as NUMBER(22,2)) as END_ARREARS_ACCRUAL,
        cast(REMAINING_PRINCIPAL          as NUMBER(22,2)) as REMAINING_PRINCIPAL,
        cast(ST_OBLIGATION_REMEASUREMENT  as NUMBER(22,2)) as ST_OBLIGATION_REMEASUREMENT,
        cast(LT_OBLIGATION_REMEASUREMENT  as NUMBER(22,2)) as LT_OBLIGATION_REMEASUREMENT,
        cast(OBLIGATION_RECLASS           as NUMBER(22,2)) as OBLIGATION_RECLASS,
        cast(UNACCRUED_INTEREST_REMEASURE as NUMBER(22,2)) as UNACCRUED_INTEREST_REMEASURE,
        cast(UNACCRUED_INTEREST_RECLASS   as NUMBER(22,2)) as UNACCRUED_INTEREST_RECLASS,
        cast(PAYMENT_MONTH                as NUMBER(1,0))  as PAYMENT_MONTH,
        cast(ESCALATION_MONTH             as NUMBER(1,0))  as ESCALATION_MONTH,
        cast(ESCALATION_PCT               as NUMBER(22,8)) as ESCALATION_PCT,
        cast(BEGIN_ACCUM_IMPAIR           as NUMBER(22,2)) as BEGIN_ACCUM_IMPAIR,
        cast(IMPAIRMENT_ACTIVITY          as NUMBER(22,2)) as IMPAIRMENT_ACTIVITY,
        cast(END_ACCUM_IMPAIR             as NUMBER(22,2)) as END_ACCUM_IMPAIR
      from ls_ilr_schedule
    )
  ';
  dbms_output.put_line(' Done.');

  dbms_output.put('Comparing row counts...');
  execute immediate 'select (select count(*) from ls_ilr_schedule) as c_old, (select count(*) from ls_ilr_schedule_53245) as c_new from dual'
          into old_count, new_count;
  if old_count = new_count then
    dbms_output.put_line('Done.');
  else
    raise rows_mismatch;
  end if;

  dbms_output.put('Setting NOT NULL columns on LS_ILR_SCHEDULE_53245...');
  execute immediate '
    alter table ls_ilr_schedule_53245 modify (
      ilr_id          number(22,0) not null,
      revision        number(22,0)  not null,
      set_of_books_id number(22,0) not null )
    ';
  dbms_output.put_line(' Done.');

  dbms_output.put('Dropping old table LS_ILR_SCHEDULE...');
  execute immediate 'drop table ls_ilr_schedule';
  dbms_output.put_line('Done.');

  dbms_output.put('Renaming LS_ILR_SCHEDULE_53245 to LS_ILR_SCHEDULE...');
  execute immediate 'alter table ls_ilr_schedule_53245 rename to ls_ilr_schedule';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding Primary key to LS_ILR_SCHEDULE...');
  execute immediate '
      alter table ls_ilr_schedule add constraint
        pk_ls_ilr_schedule primary key (ilr_id, revision, set_of_books_id, month) using index tablespace pwrplant_idx
    ';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding index LS_ILR_SCHEDULE_IDX to LS_ILR_SCHEDULE...');
  execute immediate 'CREATE INDEX LS_ILR_SCHEDULE_IDX ON LS_ILR_SCHEDULE (REVISION , ILR_ID , SET_OF_BOOKS_ID) TABLESPACE PWRPLANT_IDX';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding index LS_ILR_SCHEDULE_MTH_IDX to LS_ILR_SCHEDULE...');
  execute immediate 'CREATE INDEX LS_ILR_SCHEDULE_MTH_IDX ON LS_ILR_SCHEDULE (MONTH) TABLESPACE PWRPLANT_IDX';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding index LS_ILR_SCHEDULE_TMTH_IDX to LS_ILR_SCHEDULE...');
  execute immediate 'CREATE INDEX LS_ILR_SCHEDULE_TMTH_IDX ON LS_ILR_SCHEDULE (TRUNC(MONTH,''fmmonth'')) TABLESPACE PWRPLANT_IDX';
  dbms_output.put_line('Done.');

  dbms_output.put('Adding index LS_ILR_SCHEDULE_OM_IDX to LS_ILR_SCHEDULE...');
  execute immediate 'CREATE INDEX LS_ILR_SCHEDULE_OM_IDX ON LS_ILR_SCHEDULE (IS_OM, ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH) TABLESPACE PWRPLANT_IDX';
  dbms_output.put_line('Done.');

exception
  WHEN rows_mismatch THEN
    dbms_output.put_line('');
    dbms_output.put_line('*******************************************************');
    dbms_output.put_line('*** ERROR ENCOUNTERED');
    dbms_output.put_line('*** OLD AND NEW TABLE ROWCOUNTS DO NOT MATCH, ABORTING' );
    dbms_output.put_line('*******************************************************');
    raise_application_error(-20101, 'Row counts do not match');
  WHEN OTHERS THEN
    dbms_output.put_line('');
    dbms_output.put_line('*******************************************');
    dbms_output.put_line('*** ERROR ENCOUNTERED');
    dbms_output.put_line('*** SQLCODE: ' || sqlcode );
    dbms_output.put_line('*******************************************');
    raise_application_error(-20102, sqlerrm);
end;
/
begin
  dbms_output.put_line('Adding comments for LS_ASSET_SCHEDULE');
end;
/
--LS_ASSET_SCHEDULE comments
COMMENT ON COLUMN LS_ASSET_SCHEDULE.LS_ASSET_ID IS 'System-assigned identifier of a particular leased asset. The internal leased asset id within PowerPlant.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.REVISION IS 'The revision.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.MONTH IS 'The month being processed.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.RESIDUAL_AMOUNT IS 'The guaranteed residual amount.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.TERM_PENALTY IS 'The termination penalty amount.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BPO_PRICE IS 'The bargain purchase amount.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_CAPITAL_COST IS 'The capital amount.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_CAPITAL_COST IS 'The capital amount.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_OBLIGATION IS 'The beginning obligation for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_OBLIGATION IS 'The ending obligation for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_LT_OBLIGATION IS 'The beginning long term obligation for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_LT_OBLIGATION IS 'The ending long term obligation for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.INTEREST_ACCRUAL IS 'The amount of interest accrued in this period .';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.PRINCIPAL_ACCRUAL IS 'The amount of principal accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.INTEREST_PAID IS 'The amount of interest paid in this period .';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.PRINCIPAL_PAID IS 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL1 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL2 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL3 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL4 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL5 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL6 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL7 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL8 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL9 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL10 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID1 IS 'The executory paid amount associated with the executory bucket number 1.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID2 IS 'The executory paid amount associated with the executory bucket number 2.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID3 IS 'The executory paid amount associated with the executory bucket number 3.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID4 IS 'The executory paid amount associated with the executory bucket number 4.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID5 IS 'The executory paid amount associated with the executory bucket number 5.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID6 IS 'The executory paid amount associated with the executory bucket number 6.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID7 IS 'The executory paid amount associated with the executory bucket number 7.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID8 IS 'The executory paid amount associated with the executory bucket number 8.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID9 IS 'The executory paid amount associated with the executory bucket number 9.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_PAID10 IS 'The executory paid amount associated with the executory bucket number 10.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL1 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL2 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL3 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL4 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL5 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL6 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL7 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL8 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL9 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL10 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID1 IS 'The contingent paid amount associated with the contingent bucket number 1.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID2 IS 'The contingent paid amount associated with the contingent bucket number 2.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID3 IS 'The contingent paid amount associated with the contingent bucket number 3.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID4 IS 'The contingent paid amount associated with the contingent bucket number 4.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID5 IS 'The contingent paid amount associated with the contingent bucket number 5.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID6 IS 'The contingent paid amount associated with the contingent bucket number 6.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID7 IS 'The contingent paid amount associated with the contingent bucket number 7.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID8 IS 'The contingent paid amount associated with the contingent bucket number 8.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID9 IS 'The contingent paid amount associated with the contingent bucket number 9.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_PAID10 IS 'The contingent paid amount associated with the contingent bucket number 10.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.IS_OM IS '1 means O and M.  0 means Capital. 1 means O and M.  0 means Capital.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CURRENT_LEASE_COST IS 'The total amount funded on the leased asset in the given month';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_DEFERRED_RENT IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.DEFERRED_RENT IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_DEFERRED_RENT IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_ST_DEFERRED_RENT IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_ST_DEFERRED_RENT IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_LIABILITY IS 'The beginning liability for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_LIABILITY IS 'The ending liability for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_LT_LIABILITY IS 'The beginning long term liability for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_LT_LIABILITY IS 'The ending long term liability for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.PRINCIPAL_REMEASUREMENT IS 'The principal/obligation adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.LIABILITY_REMEASUREMENT IS 'The liability adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.INITIAL_DIRECT_COST IS 'Holds Initial Direct Cost Amount for Asset Schedule.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.INCENTIVE_AMOUNT IS 'Holds Incentive Amount for Asset Schedule.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_PREPAID_RENT IS 'The beginning prepaid rent for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.PREPAY_AMORTIZATION IS 'The prepayment amortization amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.PREPAID_RENT IS 'The prepaid rent amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_PREPAID_RENT IS 'The ending prepaid rent for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.IDC_MATH_AMOUNT IS 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.INCENTIVE_MATH_AMOUNT IS 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_NET_ROU_ASSET IS 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_NET_ROU_ASSET IS 'End Capital Cost less End Reserve for all Assets on ILR';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.ROU_ASSET_REMEASUREMENT IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.PARTIAL_TERM_GAIN_LOSS IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.ADDITIONAL_ROU_ASSET IS 'Calculation of ROU Asset based on Quantity Retirement Method';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.EXECUTORY_ADJUST IS 'Summated value of all Executory Adjusted values, by ILR';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.CONTINGENT_ADJUST IS 'Summated value of all Contingent Adjusted values, by ILR';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEG_ARREARS_ACCRUAL IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.ARREARS_ACCRUAL IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_ARREARS_ACCRUAL IS 'The ending arrears accrual balance for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.PARTIAL_MONTH_PERCENT IS 'The percentage of the first month in the payment term to pay when the payment type is Partial Month.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.REMAINING_PRINCIPAL IS 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc logic';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.ST_OBLIGATION_REMEASUREMENT IS 'The short term obligation remeasurement amount.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.LT_OBLIGATION_REMEASUREMENT IS 'The long term obligation remeasurement amount.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.OBLIGATION_RECLASS IS 'The reclassification of obligation from short to long term.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.UNACCRUED_INTEREST_REMEASURE IS 'The unaccrued interest amount as a result of a remeasurement.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.UNACCRUED_INTEREST_RECLASS IS 'The reclassification of unaccrued interest from short to long term.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEGIN_ACCUM_IMPAIR IS 'Beginning accumulated impairment amount for the Asset.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.IMPAIRMENT_ACTIVITY IS 'Impairment amount for the Asset.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_ACCUM_IMPAIR IS 'Ending accumulated impairment amount for the Asset.';
COMMENT ON TABLE LS_ASSET_SCHEDULE IS '(C)  [06]
The LS Asset Schedule table is holds the interest, principal, executory, and contingent amounts accrued and paid by period by set of books for a leased asset.';
/
begin
  dbms_output.put_line('Adding comments for LS_ILR_SCHEDULE');
end;
/
--LS_ASSET_SCHEDULE comments
COMMENT ON COLUMN LS_ILR_SCHEDULE.ILR_ID IS 'System-assigned identifier of a particular ILR.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.REVISION IS 'The revision.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.SET_OF_BOOKS_ID IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.MONTH IS 'The month being processed.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.RESIDUAL_AMOUNT IS 'The guaranteed residual amount.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.TERM_PENALTY IS 'The termination penalty amount.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BPO_PRICE IS 'The bargain purchase amount.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_CAPITAL_COST IS 'The capital amount.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_CAPITAL_COST IS 'The capital amount.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_OBLIGATION IS 'The beginning obligation for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_OBLIGATION IS 'The ending obligation for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_LT_OBLIGATION IS 'The beginning long term obligation for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_LT_OBLIGATION IS 'The ending long term obligation for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.INTEREST_ACCRUAL IS 'Records the amount of interest accrued.  Accruals occur every month regardless of payment schedule.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.PRINCIPAL_ACCRUAL IS 'Records the amount of principal accrued.  Accruals occur every month regardless of payment schedule.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.INTEREST_PAID IS 'The amount of interest paid in this period .';
COMMENT ON COLUMN LS_ILR_SCHEDULE.PRINCIPAL_PAID IS 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL1 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL2 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL3 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL4 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL5 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL6 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL7 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL8 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL9 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL10 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID1 IS 'The executory paid amount associated with the executory bucket number 1.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID2 IS 'The executory paid amount associated with the executory bucket number 2.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID3 IS 'The executory paid amount associated with the executory bucket number 3.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID4 IS 'The executory paid amount associated with the executory bucket number 4.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID5 IS 'The executory paid amount associated with the executory bucket number 5.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID6 IS 'The executory paid amount associated with the executory bucket number 6.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID7 IS 'The executory paid amount associated with the executory bucket number 7.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID8 IS 'The executory paid amount associated with the executory bucket number 8.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID9 IS 'The executory paid amount associated with the executory bucket number 9.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_PAID10 IS 'The executory paid amount associated with the executory bucket number 10.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL1 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL2 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL3 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL4 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL5 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL6 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL7 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL8 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL9 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL10 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID1 IS 'The contingent paid amount associated with the contingent bucket number 1.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID2 IS 'The contingent paid amount associated with the contingent bucket number 2.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID3 IS 'The contingent paid amount associated with the contingent bucket number 3.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID4 IS 'The contingent paid amount associated with the contingent bucket number 4.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID5 IS 'The contingent paid amount associated with the contingent bucket number 5.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID6 IS 'The contingent paid amount associated with the contingent bucket number 6.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID7 IS 'The contingent paid amount associated with the contingent bucket number 7.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID8 IS 'The contingent paid amount associated with the contingent bucket number 8.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID9 IS 'The contingent paid amount associated with the contingent bucket number 9.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_PAID10 IS 'The contingent paid amount associated with the contingent bucket number 10.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.IS_OM IS '1 means O and M.  0 means Capital.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CURRENT_LEASE_COST IS 'The total amount funded on the ILR in the given month';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_DEFERRED_RENT IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.DEFERRED_RENT IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_DEFERRED_RENT IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_ST_DEFERRED_RENT IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_ST_DEFERRED_RENT IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_LIABILITY IS 'The beginning liability for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_LIABILITY IS 'The ending liability for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_LT_LIABILITY IS 'The beginning long term liability for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_LT_LIABILITY IS 'The ending long term liability for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.PRINCIPAL_REMEASUREMENT IS 'The principal/obligation adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.LIABILITY_REMEASUREMENT IS 'The liability adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.INITIAL_DIRECT_COST IS 'Holds Initial Direct Cost Amount for ILR Schedule.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.INCENTIVE_AMOUNT IS 'Holds Incentive Amount for ILR Schedule.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_PREPAID_RENT IS 'The beginning prepaid rent for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.PREPAY_AMORTIZATION IS 'The prepayment amortization amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.PREPAID_RENT IS 'The prepaid rent amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_PREPAID_RENT IS 'The ending prepaid rent for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.IDC_MATH_AMOUNT IS 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN LS_ILR_SCHEDULE.INCENTIVE_MATH_AMOUNT IS 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_NET_ROU_ASSET IS 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_NET_ROU_ASSET IS 'End Capital Cost less End Reserve for all Assets on ILR';
COMMENT ON COLUMN LS_ILR_SCHEDULE.ROU_ASSET_REMEASUREMENT IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN LS_ILR_SCHEDULE.PARTIAL_TERM_GAIN_LOSS IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN LS_ILR_SCHEDULE.ADDITIONAL_ROU_ASSET IS 'Calculation of ROU Asset based on Quantity Retirement Method';
COMMENT ON COLUMN LS_ILR_SCHEDULE.EXECUTORY_ADJUST IS 'Summated value of all Executory Adjusted values, by ILR';
COMMENT ON COLUMN LS_ILR_SCHEDULE.CONTINGENT_ADJUST IS 'Summated value of all Contingent Adjusted values, by ILR';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEG_ARREARS_ACCRUAL IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.ARREARS_ACCRUAL IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_ARREARS_ACCRUAL IS 'The ending arrears accrual balance for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.REMAINING_PRINCIPAL IS 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc logic';
COMMENT ON COLUMN LS_ILR_SCHEDULE.ST_OBLIGATION_REMEASUREMENT IS 'The short term obligation remeasurement amount.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.LT_OBLIGATION_REMEASUREMENT IS 'The long term obligation remeasurement amount.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.OBLIGATION_RECLASS IS 'The reclassification of obligation from short to long term.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.UNACCRUED_INTEREST_REMEASURE IS 'The unaccrued interest amount as a result of a remeasurement.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.UNACCRUED_INTEREST_RECLASS IS 'The reclassification of unaccrued interest from short to long term.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.PAYMENT_MONTH IS 'System derived flag from Schedule Build that denotes the actual months of Payment on the schedule after shifting logic';
COMMENT ON COLUMN LS_ILR_SCHEDULE.ESCALATION_MONTH IS 'System derived flag from Schedule Build that denotes the actual months of Escalation on the schedule after shifting logic';
COMMENT ON COLUMN LS_ILR_SCHEDULE.ESCALATION_PCT IS 'System derived escalation rate from Schedule Build after shifting logic';
COMMENT ON COLUMN LS_ILR_SCHEDULE.BEGIN_ACCUM_IMPAIR IS 'Beginning accumulated impairment amount for the ILR.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.IMPAIRMENT_ACTIVITY IS 'Impairment amount for the ILR.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_ACCUM_IMPAIR IS 'Ending accumulated impairment amount for the ILR.';
COMMENT ON TABLE LS_ILR_SCHEDULE IS '(C)  [06] The ILR Schedule table records the ILR''s payment schedule.  The ILR''s schedule is allocated to its leased assets as part of the monthly lease expense calc process.';
/
BEGIN
  DBMS_OUTPUT.PUT_LINE('53245 COMPLETED.');
END;
/
--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16462, 0, 2018, 2, 0, 0, 53245, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053245_lessee_01_fix_schedule_number_precision_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
