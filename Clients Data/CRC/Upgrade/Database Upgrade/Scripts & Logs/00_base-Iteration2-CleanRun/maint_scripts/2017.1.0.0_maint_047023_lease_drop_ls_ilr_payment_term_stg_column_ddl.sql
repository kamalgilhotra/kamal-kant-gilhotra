/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047023_lease_drop_ls_ilr_payment_term_stg_column_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2017.1.0.0 03/09/2017 David Haupt		 Remove nonexistent column from new staging table
||============================================================================
*/ 

alter table ls_ilr_payment_term_stg drop column term_air;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3393, 0, 2017, 1, 0, 0, 47023, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047023_lease_drop_ls_ilr_payment_term_stg_column_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;