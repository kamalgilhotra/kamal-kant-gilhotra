SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032568_system_basegui.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/02/2013 Alex P.        Point Release
||============================================================================
*/

create table PP_DYNAMIC_FILTER_SAVED
(
 SAVED_FILTER_ID     number(22) not null,
 DESCRIPTION         varchar2(35) not null,
 LONG_DESCRIPTION    varchar2(254),
 PP_REPORT_FILTER_ID number(22) not null,
 IS_SHARED_ID        number(1) default 0 not null,
 IS_MYPP_ID          number(1) default 0 not null,
 USERS               varchar2(18) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table PP_DYNAMIC_FILTER_SAVED
   add constraint PP_DYNAMIC_FILTER_SAVED_PK
       primary key (SAVED_FILTER_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_DYNAMIC_FILTER_SAVED
   add constraint FK_DFS_YES_NO_ID
       foreign key (IS_SHARED_ID)
       references YES_NO;

alter table PP_DYNAMIC_FILTER_SAVED
   add constraint FK_MYPP_YES_NO_ID
       foreign key (IS_MYPP_ID)
       references YES_NO;

alter table PP_DYNAMIC_FILTER_SAVED
   add constraint FK_PP_REPORT_FILTER_ID
       foreign key (PP_REPORT_FILTER_ID)
       references PP_REPORTS_FILTER;

create unique index IDX_DFS_USER_DESCRIPTION
   ON PP_DYNAMIC_FILTER_SAVED (DESCRIPTION, PP_REPORT_FILTER_ID, IS_SHARED_ID, USERS)
   tablespace PWRPLANT_IDX;

comment on table PP_DYNAMIC_FILTER_SAVED is '(S) [10] The PP Dynamic Filter Saved table stores the header information for a dynamic filter used in new Powerplan UI that was saved either by user or when adding the report to MyPowerplan.';
comment on column PP_DYNAMIC_FILTER_SAVED.SAVED_FILTER_ID is 'Standard system-assigned identifier for a particular saved filter.';
comment on column PP_DYNAMIC_FILTER_SAVED.DESCRIPTION is 'Short description for a particular saved filter.';
comment on column PP_DYNAMIC_FILTER_SAVED.LONG_DESCRIPTION is 'Long description for a particular saved filter.';
comment on column PP_DYNAMIC_FILTER_SAVED.PP_REPORT_FILTER_ID is 'System-assigned report filter identifier, as defined in PP_REPORTS_FILTER table, for a filter object whose criteria was saved.';
comment on column PP_DYNAMIC_FILTER_SAVED.IS_SHARED_ID is 'A binary flag indicating whether the saved filter is shared with other users. 1 = Saved filter is shared, 0 = Saved filter is visible only to the user who saved the filter.';
comment on column PP_DYNAMIC_FILTER_SAVED.IS_MYPP_ID is 'A binary flag indicating whether the saved filter is used by MyPowerplan. 1 = Saved filter was created when the report was added to MyPowerpln and will not be visible to user in the saved filters screen. 0 = Saved filter was creates by a user.';
comment on column PP_DYNAMIC_FILTER_SAVED.USERS is 'A user identifier of the user who saved the filter.';
comment on column PP_DYNAMIC_FILTER_SAVED.TIME_STAMP is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_SAVED.USER_ID is 'Standarad system-assigned timestamp used for audit purposes.';

create table PP_DYNAMIC_FILTER_SAVED_VALUES
(
 SAVED_FILTER_ID number(22) not null,
 LABEL           varchar2(35) not null,
 OPERATOR        varchar2(35),
 START_VALUE     varchar2(2000) not null,
 END_VALUE       varchar2(35),
 SEARCH_TYPE     number(2, 0) not null,
 USER_ID         varchar2(18),
 TIME_STAMP      date
);

alter table PP_DYNAMIC_FILTER_SAVED_VALUES
   add constraint PP_DYNAMIC_FILTER_SAVED_VAL_PK
       primary key (SAVED_FILTER_ID, LABEL);

alter table PP_DYNAMIC_FILTER_SAVED_VALUES
   add constraint fk_dfs_saved_filter_id
       foreign key (SAVED_FILTER_ID)
       references PP_DYNAMIC_FILTER_SAVED;

comment on table PP_DYNAMIC_FILTER_SAVED_VALUES is '(S) [10] The PP Dynamic Filter Saved Values table stores the values for a dynamic filter used in new Powerplan UI that was saved either by user or when adding the report to MyPowerplan.';
comment on column PP_DYNAMIC_FILTER_SAVED_VALUES.SAVED_FILTER_ID is 'Standard system-assigned identifier for a particular saved filter, as defined in PP_DYNAMIC_FILTER_SAVED table.';
comment on column PP_DYNAMIC_FILTER_SAVED_VALUES.LABEL is 'User displayed label for a filter item within the filter object whose values are being saved.';
comment on column PP_DYNAMIC_FILTER_SAVED_VALUES.START_VALUE is 'Start value selected for a particular filter item. This could be the first of two values, if end_value is used or the only value selected.';
comment on column PP_DYNAMIC_FILTER_SAVED_VALUES.END_VALUE is 'End value selected for a particular filter item, if both start_value and end_value are used.';
comment on column PP_DYNAMIC_FILTER_SAVED_VALUES.SEARCH_TYPE is 'Numeric identifier defining the type of object that contains values. 1 = datawindow, 2 = single line edit, 3 = daterange, 4 = operation.';
comment on column PP_DYNAMIC_FILTER_SAVED_VALUES.TIME_STAMP is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_SAVED_VALUES.USER_ID is 'Standarad system-assigned timestamp used for audit purposes.';

--*
--|| Dynamic SQL to create PP_DYNAMIC_FILTER_SAVED_SEQ sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);
   LOOPING_CHAIN_SYN exception;
   pragma exception_init(LOOPING_CHAIN_SYN, -01775);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select nvl(max(saved_filter_id), 0) from PP_DYNAMIC_FILTER_SAVED')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS or LOOPING_CHAIN_SYN then
         DBMS_OUTPUT.PUT_LINE('Table PP_DYNAMIC_FILTER_SAVED doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 100;

   SQLS := 'create SEQUENCE PWRPLANT.PP_DYNAMIC_FILTER_SAVED_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

update PP_MYPP_TIME set DESCRIPTION = 'User Selected Months', LONG_DESCRIPTION = 'User Selected Months' where MYPP_TIME_ID = 9;

alter table PP_MYPP_USER_REPORT DROP constraint PP_MYPP_USER_REPORT_FK;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (672, 0, 10, 4, 2, 0, 32568, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032568_system_basegui.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
