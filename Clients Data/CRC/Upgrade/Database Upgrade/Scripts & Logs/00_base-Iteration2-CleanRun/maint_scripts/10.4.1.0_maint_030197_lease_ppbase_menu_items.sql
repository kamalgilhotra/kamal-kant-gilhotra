/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030197_lease_ppbase_menu_items.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/12/2013 Brandon Beck   Point Release
||============================================================================
*/

insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_taxes', 'menu_wksp_admin', 7, 'Local Taxes', 'Local Taxes', 1, 2, null,
    'uo_ls_admincntr_wksp_taxes', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_payments', 'lease_top', 3, 'Payments', 'Payments Module', 1, 1, null,
    'uo_ls_pymntcntr_wksp', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_lessors', 'lease module', 1, 'Lessors', 'Lessor Module', 1, 2, null,
    'uo_ls_lssrcntr_wksp_lessors', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'system controls', 'menu_wksp_admin', 5, 'System Controls', 'System Controls', 1, 2,
    null, 'uo_ls_admincntr_system_control_wksp', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'asset details', 'invisible', 0, 'Asset Details', 'Asset Details', 1, 0, null,
    'uo_ls_assetcntr_wksp_leased_asset_details', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'asset search', 'lease module', 3, 'Asset Search', 'Asset Search', 1, 2, null,
    'uo_ls_assetcntr_wksp_search', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'lease search', 'lease module', 2, 'Lease Search', 'Lease Search', 1, 2, null,
    'uo_ls_lscntr_wksp_search', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'lease module', 'lease_top', 2, 'Lease', 'Lease Module', 1, 1, 'dummy.gif', null, null,
    null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'lease details', 'invisible', 0, 'Lease Details', 'Lease Details', 1, 0, null,
    'uo_ls_lscntr_wksp_lease', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_home', 'lease_top', 1, 'Home', 'Home', 1, 1, null, 'uo_ls_wksp_home', null,
    null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_approve_invoices', 'menu_wksp_invoices', 1, 'Approve Invoices',
    'Approve Invoices', 1, 2, null, 'uo_ls_billcntr_wksp_invoices', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_journals', 'lease_top', 4, 'Journals', 'Journal Module', 1, 1, null,
    'uo_ls_jecntr_wksp_search', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_forecasting', 'lease_top', 5, 'Forecasting', 'Forecasting Module', 0, 1,
    null, null, null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_reporting', 'lease_top', 6, 'Reporting', 'Reporting Module', 1, 1, null,
    'uo_ls_rptcntr_wksp_report', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_imports', 'lease_top', 7, 'Imports', 'Imports', 1, 1, null,
    'uo_ls_admincntr_imports_wksp', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_admin', 'lease_top', 8, 'Admin', 'Admin Module', 1, 1, 'dummy.gif', null,
    null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_lease_groups', 'menu_wksp_admin', 1, 'Lease Groups', 'Lease Groups', 1, 2,
    null, 'uo_ls_admincntr_lease_group', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_month_end_close', 'menu_wksp_admin', 6, 'Month End Close',
    'Month End Close', 1, 2, null, 'uo_ls_admincntr_month_end_close', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_payment_je', 'menu_wksp_admin', 8, 'Payment/JE', 'Payment/JE', 1, 2, null,
    'uo_ls_admincntr_payment_je_wksp', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('LEASE', 'menu_wksp_pymnttypes', 'menu_wksp_admin', 10, 'Payment Types', 'Payment Types Module',
    1, 2, null, 'uo_ls_pymnt_types_wksp', null, null, null, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (401, 0, 10, 4, 1, 0, 30197, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030197_lease_ppbase_menu_items.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

commit;