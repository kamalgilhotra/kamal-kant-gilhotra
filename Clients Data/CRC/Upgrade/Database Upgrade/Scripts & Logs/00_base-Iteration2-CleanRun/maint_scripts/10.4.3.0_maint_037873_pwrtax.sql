/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037873_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 05/07/2014 Julia Breuer
||============================================================================
*/

--
-- Create a version type table and add version type to the version table.
--
create table VERSION_TYPE
(
 VERSION_TYPE_ID number(22,0) not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 DESCRIPTION     varchar2(100)
);

alter table pwrplant.version_type add constraint version_type_pk primary key ( version_type_id ) using index tablespace pwrplant_idx;

insert into pwrplant.version_type ( version_type_id, time_stamp, user_id, description ) values ( 1, sysdate, user, 'PowerTax Case' );

alter table pwrplant.version add version_type_id number(22,0);
alter table pwrplant.version add constraint version_vtype_fk foreign key ( version_type_id ) references pwrplant.version_type;
update pwrplant.version set version_type_id = 1;
alter table pwrplant.version modify version_type_id not null;

comment on table version_type is '(S) [09] The Version Type table contains the various version types that may be assigned to a version.  The version type is used to differentiate groups of versions from each other (e.g., forecast versions, tax return versions, etc.).';
comment on column version_type.description is 'Records a short description of the version type.';
comment on column version_type.version_type_id is 'System-assigned identifier of a particular version type.';
comment on column version_type.time_stamp is 'Standard System-assigned time stamp used for audit purposes.';
comment on column version_type.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column version.version_type_id is 'System-assigned identifier of a particular version type.';

--
-- Add the table to PowerPlant tables for editing.
--
insert into pwrplant.powerplant_tables ( table_name, time_stamp, user_id, pp_table_type_id, description, long_description, subsystem_screen, subsystem_display, subsystem, asset_management, budget, charge_repository, cwip_accounting, depr_studies, lease, property_tax, powertax_provision, powertax, system, unitization, work_order_management, client ) values ( 'version_type', sysdate, user, 's', 'Version Type', 'The Version Type table consists of categories used to group PowerTax versions.', 'tax', 'tax', 'tax', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );

insert into pwrplant.powerplant_columns ( column_name, table_name, time_stamp, user_id, pp_edit_type_id, description, long_description, column_rank, short_long_description, read_only ) values ( 'description', 'version_type', sysdate, user, 'e', 'Description', 'Description', 1, 'Description', 0 );
insert into pwrplant.powerplant_columns ( column_name, table_name, time_stamp, user_id, pp_edit_type_id, description, long_description, column_rank, short_long_description, read_only ) values ( 'version_type_id', 'version_type', sysdate, user, 's', 'Version Type ID', 'Version Type ID', 2, 'Version Type ID', 0 );
insert into pwrplant.powerplant_columns ( column_name, table_name, time_stamp, user_id, pp_edit_type_id, description, long_description, column_rank, short_long_description, read_only ) values ( 'user_id', 'version_type', sysdate, user, 'e', 'User ', 'User ', 100, 'User ', 0 );
insert into pwrplant.powerplant_columns ( column_name, table_name, time_stamp, user_id, pp_edit_type_id, description, long_description, column_rank, short_long_description, read_only ) values ( 'time_stamp', 'version_type', sysdate, user, 'e', 'Time Stamp', 'Time Stamp', 101, 'Time Stamp', 0 );

--
-- Add the Home workspace to the menu.
--
update pwrplant.ppbase_menu_items set item_order = item_order + 1 where module = 'powertax' and menu_level = 1;
insert into pwrplant.ppbase_workspace ( module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp ) values ( 'powertax', 'admin_home', sysdate, user, 'Home', 'uo_tax_admin_wksp_home', 'Home' );
insert into pwrplant.ppbase_menu_items ( module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn ) values ( 'powertax', 'admin_home', sysdate, user, 1, 1, 'Home', '', '', 'admin_home', 1 );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1152, 0, 10, 4, 3, 0, 37873, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037873_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;