/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050390_lessor_01_update_rate_related_types_ddl.sql
|| Description:	Update rate type to use float type
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 02/01/2018 Andrew Hill    Update rate-related types
||============================================================================
*/

CREATE OR REPLACE TYPE t_lsr_rates_implicit_in_lease AS OBJECT (rate_implicit FLOAT,
                                                                rate_implicit_ni FLOAT);
/

DROP TYPE lsr_ilr_df_schedule_result_tab;

CREATE OR REPLACE TYPE lsr_ilr_df_schedule_result IS OBJECT(MONTH DATE,
                                                            principal_received NUMBER(22,2),
                                                            interest_income_received 		 NUMBER(22,2),
                                                            interest_income_accrued 		 NUMBER(22,2),
                                                            principal_accrued            NUMBER(22,2),
                                                            begin_receivable 				     NUMBER(22,2),
                                                            end_receivable 					     NUMBER(22,2),
                                                            begin_lt_receivable          NUMBER(22,2),
                                                            end_lt_receivable            NUMBER(22,2),
                                                            initial_direct_cost          NUMBER(22,2),
                                                            executory_accrual1           NUMBER(22,2),
                                                            executory_accrual2           NUMBER(22,2),
                                                            executory_accrual3           NUMBER(22,2),
                                                            executory_accrual4           NUMBER(22,2),
                                                            executory_accrual5           NUMBER(22,2),
                                                            executory_accrual6           NUMBER(22,2),
                                                            executory_accrual7           NUMBER(22,2),
                                                            executory_accrual8           NUMBER(22,2),
                                                            executory_accrual9           NUMBER(22,2),
                                                            executory_accrual10          NUMBER(22,2),
                                                            executory_paid1              NUMBER(22,2),
                                                            executory_paid2              NUMBER(22,2),
                                                            executory_paid3              NUMBER(22,2),
                                                            executory_paid4              NUMBER(22,2),
                                                            executory_paid5              NUMBER(22,2),
                                                            executory_paid6              NUMBER(22,2),
                                                            executory_paid7              NUMBER(22,2),
                                                            executory_paid8              NUMBER(22,2),
                                                            executory_paid9              NUMBER(22,2),
                                                            executory_paid10             NUMBER(22,2),
                                                            contingent_accrual1          NUMBER(22,2),
                                                            contingent_accrual2          NUMBER(22,2),
                                                            contingent_accrual3          NUMBER(22,2),
                                                            contingent_accrual4          NUMBER(22,2),
                                                            contingent_accrual5          NUMBER(22,2),
                                                            contingent_accrual6          NUMBER(22,2),
                                                            contingent_accrual7          NUMBER(22,2),
                                                            contingent_accrual8          NUMBER(22,2),
                                                            contingent_accrual9          NUMBER(22,2),
                                                            contingent_accrual10         NUMBER(22,2),
                                                            contingent_paid1             NUMBER(22,2),
                                                            contingent_paid2             NUMBER(22,2),
                                                            contingent_paid3             NUMBER(22,2),
                                                            contingent_paid4             NUMBER(22,2),
                                                            contingent_paid5             NUMBER(22,2),
                                                            contingent_paid6             NUMBER(22,2),
                                                            contingent_paid7             NUMBER(22,2),
                                                            contingent_paid8             NUMBER(22,2),
                                                            contingent_paid9             NUMBER(22,2),
                                                            contingent_paid10            NUMBER(22,2),
                                                            begin_unguaranteed_residual NUMBER(22,2),
                                                            int_on_unguaranteed_residual NUMBER(22,2),
                                                            end_unguaranteed_residual NUMBER(22,2),
                                                            begin_net_investment NUMBER(22,2),
                                                            int_on_net_investment NUMBER(22,2),
                                                            end_net_investment NUMBER(22,2),
                                                            begin_deferred_profit number(22,2),
                                                            recognized_profit number(22,2),
                                                            end_deferred_profit number(22,2),
                                                            rate_implicit FLOAT,
                                                            compounded_rate FLOAT,
                                                            discount_rate FLOAT,
                                                            rate_implicit_ni FLOAT,
                                                            compounded_rate_ni FLOAT,
                                                            discount_rate_ni FLOAT,
                                                            begin_lease_receivable number(22,2),
                                                            npv_lease_payments NUMBER(22,2),
                                                            npv_guaranteed_residual NUMBER(22,2),
                                                            npv_unguaranteed_residual NUMBER(22,2),
                                                            selling_profit_loss NUMBER(22,2),
                                                            cost_of_goods_sold NUMBER(22,2));
/
  
CREATE TYPE lsr_ilr_df_schedule_result_tab AS TABLE OF lsr_ilr_df_schedule_result;
/

DROP TYPE lsr_ilr_sales_sch_result_tab;

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER(22,2),
                                                          interest_income_received 		 NUMBER(22,2),
                                                          interest_income_accrued 		 NUMBER(22,2),
                                                          principal_accrued            NUMBER(22,2),
                                                          begin_receivable 				     NUMBER(22,2),
                                                          end_receivable 					     NUMBER(22,2),
                                                          begin_lt_receivable          NUMBER(22,2),
                                                          end_lt_receivable            NUMBER(22,2),
                                                          initial_direct_cost          NUMBER(22,2),
                                                          executory_accrual1           NUMBER(22,2),
                                                          executory_accrual2           NUMBER(22,2),
                                                          executory_accrual3           NUMBER(22,2),
                                                          executory_accrual4           NUMBER(22,2),
                                                          executory_accrual5           NUMBER(22,2),
                                                          executory_accrual6           NUMBER(22,2),
                                                          executory_accrual7           NUMBER(22,2),
                                                          executory_accrual8           NUMBER(22,2),
                                                          executory_accrual9           NUMBER(22,2),
                                                          executory_accrual10          NUMBER(22,2),
                                                          executory_paid1              NUMBER(22,2),
                                                          executory_paid2              NUMBER(22,2),
                                                          executory_paid3              NUMBER(22,2),
                                                          executory_paid4              NUMBER(22,2),
                                                          executory_paid5              NUMBER(22,2),
                                                          executory_paid6              NUMBER(22,2),
                                                          executory_paid7              NUMBER(22,2),
                                                          executory_paid8              NUMBER(22,2),
                                                          executory_paid9              NUMBER(22,2),
                                                          executory_paid10             NUMBER(22,2),
                                                          contingent_accrual1          NUMBER(22,2),
                                                          contingent_accrual2          NUMBER(22,2),
                                                          contingent_accrual3          NUMBER(22,2),
                                                          contingent_accrual4          NUMBER(22,2),
                                                          contingent_accrual5          NUMBER(22,2),
                                                          contingent_accrual6          NUMBER(22,2),
                                                          contingent_accrual7          NUMBER(22,2),
                                                          contingent_accrual8          NUMBER(22,2),
                                                          contingent_accrual9          NUMBER(22,2),
                                                          contingent_accrual10         NUMBER(22,2),
                                                          contingent_paid1             NUMBER(22,2),
                                                          contingent_paid2             NUMBER(22,2),
                                                          contingent_paid3             NUMBER(22,2),
                                                          contingent_paid4             NUMBER(22,2),
                                                          contingent_paid5             NUMBER(22,2),
                                                          contingent_paid6             NUMBER(22,2),
                                                          contingent_paid7             NUMBER(22,2),
                                                          contingent_paid8             NUMBER(22,2),
                                                          contingent_paid9             NUMBER(22,2),
                                                          contingent_paid10            NUMBER(22,2),
                                                          begin_unguaranteed_residual NUMBER(22,2),
                                                          int_on_unguaranteed_residual NUMBER(22,2),
                                                          end_unguaranteed_residual NUMBER(22,2),
                                                          begin_net_investment NUMBER(22,2),
                                                          int_on_net_investment NUMBER(22,2),
                                                          end_net_investment NUMBER(22,2),
                                                          rate_implicit FLOAT,
                                                          compounded_rate FLOAT,
                                                          discount_rate FLOAT,
                                                          rate_implicit_ni FLOAT,
                                                          compounded_rate_ni FLOAT,
                                                          discount_rate_ni FLOAT,
                                                          begin_lease_receivable number(22,2),
                                                          npv_lease_payments NUMBER(22,2),
                                                          npv_guaranteed_residual NUMBER(22,2),
                                                          npv_unguaranteed_residual NUMBER(22,2),
                                                          selling_profit_loss NUMBER(22,2),
                                                          cost_of_goods_sold NUMBER(22,2));
/

CREATE TYPE lsr_ilr_sales_sch_result_tab AS TABLE OF lsr_ilr_sales_sch_result;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4102, 0, 2017, 3, 0, 0, 50390, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050390_lessor_01_update_rate_related_types_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;