/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042760_reg_cap_bdg_drill_views_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   02/05/2015 Shane Ward    		    Added Key fields for joins when drilling back
||========================================================================================
*/

--Cap Budget Activity
CREATE OR REPLACE VIEW reg_fcst_cap_bdg_activity_view AS
select BAC.MONTH_NUMBER      MONTH_NUMBER,
       BAC.AMOUNT            AMOUNT,
       BAC.AFUDC_DEBT        AFUDC_DEBT,
       BAC.AFUDC_EQUITY      AFUDC_EQUITY,
       EX.DESCRIPTION        EXPENDITURE_TYPE,
       FP.ACTIVE             ACTIVE,
       WOC.WORK_ORDER_NUMBER work_order_number,
       BS.DESCRIPTION        BUSINESS_SEGMENT,
       BS.BUS_SEGMENT_ID     BUS_SEGMENT_ID,
       CS.DESCRIPTION        COMPANY,
       WOC.COMPANY_ID        COMPANY_ID,
       WOC.WORK_ORDER_TYPE_ID WORK_ORDER_TYPE_ID,
       woc.major_location_id major_location_id,
       woc.asset_location_id asset_location_id,
       woc.work_order_id    work_order_id
  from COMPANY_SETUP               CS,
       REG_FORECAST_VERSION        FV,
       BUDGET_AFUDC_CALC           BAC,
       WORK_ORDER_CONTROL          WOC,
       BUDGET_VERSION_FUND_PROJ    FP,
       REG_BUDGET_SOURCE_ACTIVITY  BSA,
       REG_BUDGET_COMPONENT_VALUES BCV,
       CR_DD_REQUIRED_FILTER       MO,
       CR_DD_REQUIRED_FILTER       RFV,
       CR_DD_REQUIRED_FILTER       COMP,
       CR_DD_REQUIRED_FILTER       CO,
       EXPENDITURE_TYPE            EX,
       BUSINESS_SEGMENT            BS
 where BAC.MONTH_NUMBER = MO.FILTER_STRING
   and MO.COLUMN_NAME = 'MONTH_NUMBER'
   and EX.EXPENDITURE_TYPE_ID = BAC.EXPENDITURE_TYPE_ID
   and BS.BUS_SEGMENT_ID = WOC.BUS_SEGMENT_ID
   and BAC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.COMPANY_ID = CS.COMPANY_ID
   and BAC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and FV.FORECAST_VERSION_ID = RFV.FILTER_STRING
   and RFV.COLUMN_NAME = 'FORECAST_VERSION_ID'
   and BAC.EXPENDITURE_TYPE_ID = 1
   and CS.REG_COMPANY_ID = CO.FILTER_STRING
   and CO.COLUMN_NAME = 'REG_COMPANY_ID'
   and (FP.BUDGET_VERSION_ID = FV.BUDGET_VERSION_ID and FP.ACTIVE = 1 and FP.WORK_ORDER_ID = BAC.WORK_ORDER_ID and
       FP.REVISION = BAC.REVISION and BAC.EXPENDITURE_TYPE_ID = BSA.EXPENDITURE_TYPE_ID and
       BSA.DETAIL_ID = BCV.ID_VALUE and BCV.REG_COMPONENT_ID = COMP.FILTER_STRING and
       COMP.COLUMN_NAME = 'REG_COMPONENT_ID' and BCV.REG_FAMILY_ID = 3)
;

--Cap Budget Balance
CREATE OR REPLACE VIEW reg_fcst_cap_bdg_balance_view  AS
select BAC.MONTH_NUMBER      MONTH_NUMBER,
       BAC.END_CWIP          END_CWIP,
       EX.DESCRIPTION        EXPENDITURE_TYPE,
       FP.ACTIVE             ACTIVE,
       WOC.WORK_ORDER_NUMBER  work_order_number,
       BS.DESCRIPTION        BUSINESS_SEGMENT,
       BS.BUS_SEGMENT_ID     BUS_SEGMENT_ID,
       CS.DESCRIPTION        COMPANY,
       WOC.COMPANY_ID        COMPANY_ID,
       major_location_id    major_location_id,
       asset_location_id    asset_location_id,
       woc.work_order_id    work_order_id
  from COMPANY_SETUP               CS,
       REG_FORECAST_VERSION        FV,
       BUDGET_AFUDC_CALC           BAC,
       WORK_ORDER_CONTROL          WOC,
       BUDGET_VERSION_FUND_PROJ    FP,
       REG_BUDGET_SOURCE_BALANCE   BSB,
       REG_BUDGET_COMPONENT_VALUES BCV,
       CR_DD_REQUIRED_FILTER       MO,
       CR_DD_REQUIRED_FILTER       FV,
       CR_DD_REQUIRED_FILTER       COMP,
       CR_DD_REQUIRED_FILTER       CO,
       EXPENDITURE_TYPE            EX,
       BUSINESS_SEGMENT            BS
 where BAC.MONTH_NUMBER = MO.FILTER_STRING
   and MO.COLUMN_NAME = 'MONTH_NUMBER'
   and EX.EXPENDITURE_TYPE_ID = BAC.EXPENDITURE_TYPE_ID
   and BS.BUS_SEGMENT_ID = WOC.BUS_SEGMENT_ID
   and BAC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.COMPANY_ID = CS.COMPANY_ID
   and BAC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and FV.FORECAST_VERSION_ID = FV.FILTER_STRING
   and FV.COLUMN_NAME = 'FORECAST_VERSION_ID'
   and CS.REG_COMPANY_ID = CO.FILTER_STRING
   and CO.COLUMN_NAME = 'REG_COMPANY_ID'
   and BAC.EXPENDITURE_TYPE_ID = 1
   and (FP.BUDGET_VERSION_ID = FV.BUDGET_VERSION_ID and FP.ACTIVE = 1 and FP.WORK_ORDER_ID = BAC.WORK_ORDER_ID and
       FP.REVISION = BAC.REVISION and BAC.EXPENDITURE_TYPE_ID = BSB.EXPENDITURE_TYPE_ID and
       BSB.DETAIL_ID = BCV.ID_VALUE and BCV.REG_COMPONENT_ID = COMP.FILTER_STRING and
       COMP.COLUMN_NAME = 'REG_COMPONENT_ID' and BCV.REG_FAMILY_ID = 3)
;





--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2265, 0, 2015, 1, 0, 0, 42760, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042760_reg_cap_bdg_drill_views_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;