/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008773_sys.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/03/2011 Lee Quinn      Point Release
|| 10.4.2.0   04/05/2014 Lee Quinn      Added PL/SQL around table statements
||============================================================================
*/

-- Ignore errors if object already exists
/* commented out 04052014
create table PP_SCHEMA_CHANGE_LOG
(
  ID              number(22) not null,
  SKIPPED         number(22) default 0 not null,
  MAJOR_VERSION   number(22) not null,
  MINOR_VERSION   number(22) not null,
  POINT_VERSION   number(22) not null,
  PATCH_VERSION   number(22) not null,
  MAINT_NUM       number(22) not null,
  SCRIPT_PATH     varchar2(256) not null,
  SCRIPT_NAME     varchar2(256) not null,
  SCRIPT_REVISION number(22) default 1 not null,
  DATE_APPLIED    timestamp default systimestamp not null,
  OS_USER         varchar2(256),
  TERMINAL        varchar2(256),
  SERVICE_NAME    varchar2(18),
  USER_ID         varchar2(18),
  TIME_STAMP      date
);

alter table PP_SCHEMA_CHANGE_LOG
  add constraint PP_SCHEMA_CHANGE_LOG_PK primary key (ID, SCRIPT_REVISION)
  using index tablespace PWRPLANT_IDX;

create or replace trigger PP_SCHEMA_CHANGE_LOG
before update or insert on PWRPLANT.PP_SCHEMA_CHANGE_LOG
for each row
begin
   :NEW.USER_ID    := user;
   :NEW.TIME_STAMP := sysdate;
end;
/
*/

/* Added 04052014 */
begin
   begin
      execute immediate 'create table PP_SCHEMA_CHANGE_LOG
                        (
                         ID              number(22) not null,
                         SKIPPED         number(22) default 0 not null,
                         MAJOR_VERSION   number(22) not null,
                         MINOR_VERSION   number(22) not null,
                         POINT_VERSION   number(22) not null,
                         PATCH_VERSION   number(22) not null,
                         MAINT_NUM       number(22) not null,
                         SCRIPT_PATH     varchar2(256) not null,
                         SCRIPT_NAME     varchar2(256) not null,
                         SCRIPT_REVISION number(22) default 1 not null,
                         DATE_APPLIED    timestamp default systimestamp not null,
                         OS_USER         varchar2(256),
                         TERMINAL        varchar2(256),
                         SERVICE_NAME    varchar2(18),
                         USER_ID         varchar2(18),
                         TIME_STAMP      date
                        )';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_CHEMA_CHANGE_LOG table already exists.');
   end;

   begin
      execute immediate 'alter table PP_SCHEMA_CHANGE_LOG
                            add constraint PP_SCHEMA_CHANGE_LOG_PK
                                primary key (ID, SCRIPT_REVISION)
                                using index tablespace PWRPLANT_IDX';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_CHEMA_CHANGE_LOG Primary Key already exists.');
   end;

   begin
      execute immediate 'create or replace trigger PP_SCHEMA_CHANGE_LOG
                            before update or insert on PWRPLANT.PP_SCHEMA_CHANGE_LOG
                            for each row
                         begin
                            :NEW.USER_ID    := SYS_CONTEXT(''USERENV'', ''SESSION_USER'');
                            :NEW.TIME_STAMP := sysdate;
                         end;';
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PP_CHEMA_CHANGE_LOG table already exists.');
   end;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1, 0, 10, 3, 3, 0, 8773, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008773_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
