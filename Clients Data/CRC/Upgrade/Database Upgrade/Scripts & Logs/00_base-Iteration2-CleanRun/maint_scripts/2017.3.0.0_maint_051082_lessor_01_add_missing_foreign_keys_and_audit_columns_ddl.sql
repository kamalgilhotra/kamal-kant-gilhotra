/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051082_lessor_01_add_missing_foreign_keys_and_audit_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 05/04/2018 Andrew Hill      Add missing foreign keys and audit columns
||============================================================================
*/

ALTER TABLE lsr_ilr_group ADD CONSTRAINT lsr_ilr_group_fk20 FOREIGN KEY (def_costs_account_id) REFERENCES gl_account(gl_account_id);
alter table lsr_ilr_group add constraint lsr_ilr_group_fk21 foreign key (def_selling_profit_account_id) references gl_account(gl_account_id);
ALTER TABLE lsr_ilr_group ADD CONSTRAINT lsr_ilr_group_fk22 FOREIGN KEY (incurred_costs_account_id) REFERENCES gl_account(gl_account_id);
ALTER TABLE lsr_ilr_group ADD CONSTRAINT lsr_ilr_group_fk23 FOREIGN KEY (workflow_type_id) REFERENCES workflow_type(workflow_type_id);

ALTER TABLE lsr_ilr_account ADD CONSTRAINT lsr_ilr_account_fk20 FOREIGN KEY (def_costs_account_id) REFERENCES gl_account(gl_account_id);
ALTER TABLE lsr_ilr_account ADD CONSTRAINT lsr_ilr_account_fk21 FOREIGN KEY (def_selling_profit_account_id) REFERENCES gl_account(gl_account_id);
ALTER TABLE lsr_ilr_account ADD CONSTRAINT lsr_ilr_account_fk22 FOREIGN KEY (incurred_costs_account_id) REFERENCES gl_account(gl_account_id);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5103, 0, 2017, 3, 0, 0, 51082, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051082_lessor_01_add_missing_foreign_keys_and_audit_columns_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
