/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044128_reg_create_new_sequence_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/26/2015 Anand R        create new sequence for reg_incr_process_run
||                                      	
||============================================================================
*/

SET SERVEROUTPUT ON

DECLARE
CURRENT_MAX_VALUE NUMBER;
SQLSTRING VARCHAR2(1000);
ERROR_CD NUMBER;
ERROR_MSG VARCHAR(2000);

BEGIN 
 
  SELECT MAX(NVL(INCREMENTAL_RUN_ID,0)) + 1 INTO CURRENT_MAX_VALUE FROM INCREMENTAL_PROCESS_RUN_LOG;

  IF CURRENT_MAX_VALUE IS NULL THEN
    CURRENT_MAX_VALUE := 0;
  END IF;
  
  SQLSTRING := '';
  SQLSTRING := 'CREATE SEQUENCE REG_INCR_PROCESS_RUN_SEQ';
  SQLSTRING := SQLSTRING || ' MINVALUE ' || TO_CHAR(CURRENT_MAX_VALUE) ;
  SQLSTRING := SQLSTRING || ' MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 NOCYCLE NOORDER NOCACHE ' ;
  
  DBMS_OUTPUT.PUT_LINE(SQLSTRING);
  
  EXECUTE IMMEDIATE SQLSTRING;
  
  EXCEPTION
    WHEN OTHERS THEN
      ERROR_CD := SQLCODE;
      ERROR_MSG := SUBSTR(SQLERRM, 1, 2000);
      DBMS_OUTPUT.PUT_LINE ('THERE WAS A ERROR DURING EXECUTION. ' || ERROR_CD || ' : ' || ERROR_MSG);
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2651, 0, 2015, 2, 0, 0, 044128, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044128_reg_create_new_sequence_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;