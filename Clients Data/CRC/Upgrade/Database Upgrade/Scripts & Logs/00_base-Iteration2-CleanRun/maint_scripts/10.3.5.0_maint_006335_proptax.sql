SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_006335_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    07/03/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Create a new import for updating bills with amounts.
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 20, sysdate, user, 'Update : Bills', 'Import Updates to Bills', 'pt_import_statement', 'pt_import_statement_archive', null, 0, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'statement_year_id', sysdate, user, 'Statement Year', 'statement_year_xlate', 1, 1, 'number(22,0)', 'pt_statement_year', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county', null, 'state_id', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'statement_id', sysdate, user, 'Statement', 'statement_xlate', 1, 3, 'number(22,0)', 'pt_statement', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'tax_amount', sysdate, user, 'Tax Amount', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'credit_amount', sysdate, user, 'Credit Amount', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'penalty_amount', sysdate, user, 'Penalty Amount', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'interest_amount', sysdate, user, 'Interest Amount', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'received_date', sysdate, user, 'Received Date', '', 0, 1, 'date', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'assessment', sysdate, user, 'Assessment', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'taxable_value', sysdate, user, 'Taxable Value', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 170, sysdate, user, 'PT Statement.Account Number', 'The passed in value corresponds to the PT Statement: Account Number field.  Translate to the Statement ID using the Account Number column on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.account_number ) ) )', 0, 0, 'pt_statement', 'account_number', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 171, sysdate, user, 'PT Statement.Account Number (for given State)', 'The passed in value corresponds to the PT Statement: Account Number field (for the given state).  Translate to the Statement ID using the Account Number and State columns on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.account_number ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( stmt.state_id, ''sww'' ) )', 0, 0, 'pt_statement', 'account_number', 'state_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 172, sysdate, user, 'PT Statement.Account Number (for given State and County)', 'The passed in value corresponds to the PT Statement: Account Number field (for the given state and county).  Translate to the Statement ID using the Account Number, State, and County columns on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.account_number ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( stmt.state_id, ''sww'' ) and nvl( <importtable>.county_id, ''sww'' ) = nvl( stmt.county_id, ''sww'' ) )', 0, 0, 'pt_statement', 'account_number', 'state_id, county_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 173, sysdate, user, 'PT Statement.Description', 'The passed in value corresponds to the PT Statement: Description field.  Translate to the Statement ID using the Description column on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.description ) ) )', 0, 0, 'pt_statement', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 174, sysdate, user, 'PT Statement.Description (for given State)', 'The passed in value corresponds to the PT Statement: Description field (for the given state).  Translate to the Statement ID using the Description and State columns on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.description ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( stmt.state_id, ''sww'' ) )', 0, 0, 'pt_statement', 'description', 'state_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 175, sysdate, user, 'PT Statement.Description (for given State and County)', 'The passed in value corresponds to the PT Statement: Description field (for the given state and county).  Translate to the Statement ID using the Description, State, and County columns on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.description ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( stmt.state_id, ''sww'' ) and nvl( <importtable>.county_id, ''sww'' ) = nvl( stmt.county_id, ''sww'' ) )', 0, 0, 'pt_statement', 'description', 'state_id, county_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 176, sysdate, user, 'PT Statement.Long Description', 'The passed in value corresponds to the PT Statement: Long Description field.  Translate to the Statement ID using the Long Description column on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.long_description ) ) )', 0, 0, 'pt_statement', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 177, sysdate, user, 'PT Statement.Long Description (for given State)', 'The passed in value corresponds to the PT Statement: Long Description field (for the given state).  Translate to the Statement ID using the Long Description and State columns on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.long_description ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( stmt.state_id, ''sww'' ) )', 0, 0, 'pt_statement', 'long_description', 'state_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 178, sysdate, user, 'PT Statement.Long Description (for given State & County)', 'The passed in value corresponds to the PT Statement: Long Description field (for the given state and county).  Translate to the Statement ID using the Long Description, State, and County columns on the PT Statement table.', 'statement_id', '( select stmt.statement_id from pt_statement stmt where upper( trim( <importfield> ) ) = upper( trim( stmt.long_description ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( stmt.state_id, ''sww'' ) and nvl( <importtable>.county_id, ''sww'' ) = nvl( stmt.county_id, ''sww'' ) )', 0, 0, 'pt_statement', 'long_description', 'state_id, county_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 179, sysdate, user, 'PT Statement Year.Description', 'The passed in value corresponds to the PT Statement Year: Description field.  Translate to the Statement Year ID using the Description column on the PT Statement Year table.', 'statement_year_id', '( select sy.statement_year_id from pt_statement_year sy where upper( trim( <importfield> ) ) = upper( trim( sy.description ) ) )', 0, 0, 'pt_statement_year', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 180, sysdate, user, 'PT Statement Year.Long Description', 'The passed in value corresponds to the PT Statement Year: Long Description field.  Translate to the Statement Year ID using the Long Description column on the PT Statement Year table.', 'statement_year_id', '( select sy.statement_year_id from pt_statement_year sy where upper( trim( <importfield> ) ) = upper( trim( sy.long_description ) ) )', 0, 0, 'pt_statement_year', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 181, sysdate, user, 'PT Statement Year.Year', 'The passed in value corresponds to the PT Statement Year: Year field.  Translate to the Statement ID using the Year column on the PT Statement Year table.', 'statement_year_id', '( select sy.statement_year_id from pt_statement_year sy where upper( trim( <importfield> ) ) = upper( trim( sy.year ) ) )', 0, 0, 'pt_statement_year', 'year', '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_year_id', 179, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_year_id', 180, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_year_id', 181, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 170, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 171, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 172, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 173, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 174, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 175, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 176, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 177, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 20, 'statement_id', 178, sysdate, user );

--
-- Create an import for updating bill lines with amounts.
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 21, sysdate, user, 'Update : Bill Lines', 'Import Updates to Bills by Line', 'pt_import_statement', 'pt_import_statement_archive', null, 0, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'statement_year_id', sysdate, user, 'Statement Year', 'statement_year_xlate', 1, 1, 'number(22,0)', 'pt_statement_year', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county', null, 'state_id', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'statement_id', sysdate, user, 'Statement', 'statement_xlate', 1, 3, 'number(22,0)', 'pt_statement', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'tax_authority_id', sysdate, user, 'Tax Authority', 'tax_authority_xlate', 1, 3, 'number(22,2)', 'property_tax_authority', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'levy_class_id', sysdate, user, 'Levy Class', 'levy_class_xlate', 1, 1, 'number(22,2)', 'pt_levy_class', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'prop_tax_company_id', sysdate, user, 'Prop Tax Company', 'prop_tax_company_xlate', 0, 1, 'number(22,2)', 'pt_company', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'assessor_id', sysdate, user, 'Assessor', 'assessor_xlate', 0, 1, 'number(22,2)', 'pt_assessor', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'parcel_id', sysdate, user, 'Parcel', 'parcel_xlate', 1, 3, 'number(22,2)', 'pt_parcel', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'verified_status_id', sysdate, user, 'Verified Status', 'verified_status_xlate', 0, 1, 'number(22,2)', 'pt_statement_verified_status', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'first_install_yn', sysdate, user, 'Pay in First Installment', 'first_install_xlate', 0, 1, 'number(22,2)', 'yes_no', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'tax_amount', sysdate, user, 'Tax Amount', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'credit_amount', sysdate, user, 'Credit Amount', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'penalty_amount', sysdate, user, 'Penalty Amount', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'interest_amount', sysdate, user, 'Interest Amount', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'assessment', sysdate, user, 'Assessment', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'taxable_value', sysdate, user, 'Taxable Value', '', 0, 1, 'number(22,2)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'notes', sysdate, user, 'Notes', '', 0, 1, 'varchar2(2000)', '', null, '', 0, '' );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 182, sysdate, user, 'PT Statement Verified Status.Description', 'The passed in value corresponds to the PT Statement Verified Status: Description field.  Translate to the Verified Status ID using the Description column on the PT Statement Verified Status table.', 'verified_status_id', '( select svs.verified_status_id from pt_statement_verified_status svs where upper( trim( <importfield> ) ) = upper( trim( svs.description ) ) )', 0, 0, 'pt_statement_verified_status', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 183, sysdate, user, 'PT Statement Verified Status.Long Description', 'The passed in value corresponds to the PT Statement Verified Status: Long Description field.  Translate to the Verified Status ID using the Long Description column on the PT Statement Verified Status table.', 'verified_status_id', '( select svs.verified_status_id from pt_statement_verified_status svs where upper( trim( <importfield> ) ) = upper( trim( svs.long_description ) ) )', 0, 0, 'pt_statement_verified_status', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 184, sysdate, user, 'Derive Composite Authority from Parcel and Statement', 'The Tax Authority ID will be derived from the given parcel and statement.', 'tax_authority_id', '( select ta.tax_authority_id from property_tax_authority ta, pt_parcel prcl, pt_statement_statement_year ssy where <importtable>.parcel_id = prcl.parcel_id and prcl.tax_district_id = ta.tax_district_id and <importtable>.statement_year_id = ssy.statement_year_id and <importtable>.statement_id = ssy.statement_id and ssy.statement_group_id = ta.statement_group_id and ta.tax_authority_type_id = -100 )', 1, 0, '', '', '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_year_id', 179, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_year_id', 180, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_year_id', 181, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 170, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 172, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 171, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 173, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 175, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 174, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 176, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 178, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'statement_id', 177, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 52, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 58, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 55, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 53, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 59, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 56, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 54, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 60, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 57, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'tax_authority_id', 184, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'levy_class_id', 50, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'levy_class_id', 51, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'prop_tax_company_id', 3, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'prop_tax_company_id', 5, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'prop_tax_company_id', 4, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'assessor_id', 67, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'assessor_id', 69, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'assessor_id', 68, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 15, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 16, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 11, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 12, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 13, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 14, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 9, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 10, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 71, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 70, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 17, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'parcel_id', 18, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'verified_status_id', 182, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'verified_status_id', 183, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'first_install_yn', 78, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 21, 'first_install_yn', 77, sysdate, user );

--
-- Create an import type for adding bill lines (it will allow updates on adds).
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 22, sysdate, user, 'Add : Bill Lines', 'Import Bill Lines', 'pt_import_statement', 'pt_import_statement_archive', null, 1, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'statement_year_id', sysdate, user, 'Statement Year', 'statement_year_xlate', 1, 1, 'number(22,0)', 'pt_statement_year', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county', 1, 'state_id', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'tax_authority_id', sysdate, user, 'Tax Authority', 'tax_authority_xlate', 0, 3, 'number(22,2)', 'property_tax_authority', 1, '', 1, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'levy_class_id', sysdate, user, 'Levy Class', 'levy_class_xlate', 1, 1, 'number(22,2)', 'pt_levy_class', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'prop_tax_company_id', sysdate, user, 'Prop Tax Company', 'prop_tax_company_xlate', 0, 1, 'number(22,2)', 'pt_company', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'assessor_id', sysdate, user, 'Assessor', 'assessor_xlate', 0, 1, 'number(22,2)', 'pt_assessor', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'parcel_id', sysdate, user, 'Parcel', 'parcel_xlate', 1, 3, 'number(22,2)', 'pt_parcel', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'verified_status_id', sysdate, user, 'Verified Status', 'verified_status_xlate', 0, 1, 'number(22,2)', 'pt_statement_verified_status', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'first_install_yn', sysdate, user, 'Pay in First Installment', 'first_install_xlate', 0, 1, 'number(22,2)', 'yes_no', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'tax_amount', sysdate, user, 'Tax Amount', '', 0, 1, 'number(22,2)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'credit_amount', sysdate, user, 'Credit Amount', '', 0, 1, 'number(22,2)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'penalty_amount', sysdate, user, 'Penalty Amount', '', 0, 1, 'number(22,2)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'interest_amount', sysdate, user, 'Interest Amount', '', 0, 1, 'number(22,2)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'assessment', sysdate, user, 'Assessment', '', 0, 1, 'number(22,2)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'taxable_value', sysdate, user, 'Taxable Value', '', 0, 1, 'number(22,2)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'line_notes', sysdate, user, 'Notes (Line)', '', 0, 1, 'varchar2(2000)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'description', sysdate, user, 'Description', '', 0, 1, 'varchar2(100)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'account_number', sysdate, user, 'Account Number', '', 0, 1, 'varchar2(35)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'statement_notes', sysdate, user, 'Notes (Statement)', '', 0, 1, 'varchar2(254)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'assessment_year_id', sysdate, user, 'Assessment Year', 'assessment_year_xlate', 0, 1, 'number(22,0)', 'pt_assessment_year', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'statement_group_id', sysdate, user, 'Statement Group', 'statement_group_xlate', 0, 2, 'number(22,0)', 'pt_statement_group', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'statement_number', sysdate, user, 'Statement Number', '', 0, 1, 'varchar2(35)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 22, 'year_notes', sysdate, user, 'Notes (Statement Year)', '', 0, 1, 'varchar2(2000)', '', 1, '', 0, '' );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 185, sysdate, user, 'PT Assessment Year.Description', 'The passed in value corresponds to the PT Assessment Year: Description field.  Translate to the Assessment Year ID using the Description column on the PT Assessment Year table.', 'assessment_year_id', '( select ay.assessment_year_id from pt_assessment_year ay where upper( trim( <importfield> ) ) = upper( trim( ay.description ) ) )', 0, 0, 'pt_assessment_year', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 186, sysdate, user, 'PT Assessment Year.Long Description', 'The passed in value corresponds to the PT Assessment Year: Long Description field.  Translate to the Assessment Year ID using the Long Description column on the PT Assessment Year table.', 'assessment_year_id', '( select ay.assessment_year_id from pt_assessment_year ay where upper( trim( <importfield> ) ) = upper( trim( ay.long_description ) ) )', 0, 0, 'pt_assessment_year', 'long_description', '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_year_id', 179, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_year_id', 180, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_year_id', 181, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 52, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 58, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 55, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 53, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 59, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 56, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 54, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 60, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 57, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'tax_authority_id', 184, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'levy_class_id', 50, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'levy_class_id', 51, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'prop_tax_company_id', 3, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'prop_tax_company_id', 5, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'prop_tax_company_id', 4, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'assessor_id', 67, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'assessor_id', 69, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'assessor_id', 68, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 15, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 16, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 11, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 12, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 13, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 14, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 9, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 10, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 71, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 70, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 17, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'parcel_id', 18, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'verified_status_id', 182, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'verified_status_id', 183, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'first_install_yn', 78, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'first_install_yn', 77, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'assessment_year_id', 185, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'assessment_year_id', 186, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_group_id', 151, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_group_id', 154, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_group_id', 152, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_group_id', 153, sysdate, user );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 170, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 172, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 171, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 173, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 175, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 174, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 176, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 178, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 177, sysdate, user );

--
-- Create the tables needed for these imports.
--
create table PWRPLANT.PT_IMPORT_STATEMENT
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 STATEMENT_YEAR_XLATE   varchar2(254),
 STATEMENT_YEAR_ID      number(22,0),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 STATEMENT_XLATE        varchar2(254),
 STATEMENT_ID           number(22,0),
 DESCRIPTION            varchar2(254),
 LONG_DESCRIPTION       varchar2(254),
 ACCOUNT_NUMBER         varchar2(254),
 STATEMENT_NOTES        varchar2(254),
 ASSESSMENT_YEAR_XLATE  varchar2(254),
 ASSESSMENT_YEAR_ID     number(22,0),
 STATEMENT_GROUP_XLATE  varchar2(254),
 STATEMENT_GROUP_ID     number(22,0),
 STATEMENT_NUMBER       varchar2(254),
 YEAR_NOTES             varchar2(2000),
 TAX_AUTHORITY_XLATE    varchar2(254),
 TAX_AUTHORITY_ID       number(22,0),
 LEVY_CLASS_XLATE       varchar2(254),
 LEVY_CLASS_ID          number(22,0),
 PROP_TAX_COMPANY_XLATE varchar2(254),
 PROP_TAX_COMPANY_ID    number(22,0),
 ASSESSOR_XLATE         varchar2(254),
 ASSESSOR_ID            number(22,0),
 PARCEL_XLATE           varchar2(254),
 PARCEL_ID              number(22,0),
 VERIFIED_STATUS_XLATE  varchar2(254),
 VERIFIED_STATUS_ID     number(22,0),
 FIRST_INSTALL_XLATE    varchar2(254),
 FIRST_INSTALL_YN       number(22,0),
 TAX_AMOUNT             varchar2(35),
 CREDIT_AMOUNT          varchar2(35),
 PENALTY_AMOUNT         varchar2(35),
 INTEREST_AMOUNT        varchar2(35),
 ASSESSMENT             varchar2(35),
 TAXABLE_VALUE          varchar2(35),
 RECEIVED_DATE          varchar2(35),
 LINE_NOTES             varchar2(2000),
 IS_MODIFIED            number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_STATEMENT
   add constraint PT_IMPORT_STMT_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_STATEMENT
   add constraint PT_IMPT_STMT_IMPT_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_STATEMENT_ARCHIVE
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 STATEMENT_YEAR_XLATE   varchar2(254),
 STATEMENT_YEAR_ID      number(22,0),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE              varchar2(254),
 COUNTY_ID              char(18),
 STATEMENT_XLATE        varchar2(254),
 STATEMENT_ID           number(22,0),
 DESCRIPTION            varchar2(254),
 LONG_DESCRIPTION       varchar2(254),
 ACCOUNT_NUMBER         varchar2(254),
 STATEMENT_NOTES        varchar2(254),
 ASSESSMENT_YEAR_XLATE  varchar2(254),
 ASSESSMENT_YEAR_ID     number(22,0),
 STATEMENT_GROUP_XLATE  varchar2(254),
 STATEMENT_GROUP_ID     number(22,0),
 STATEMENT_NUMBER       varchar2(254),
 YEAR_NOTES             varchar2(2000),
 TAX_AUTHORITY_XLATE    varchar2(254),
 TAX_AUTHORITY_ID       number(22,0),
 LEVY_CLASS_XLATE       varchar2(254),
 LEVY_CLASS_ID          number(22,0),
 PROP_TAX_COMPANY_XLATE varchar2(254),
 PROP_TAX_COMPANY_ID    number(22,0),
 ASSESSOR_XLATE         varchar2(254),
 ASSESSOR_ID            number(22,0),
 PARCEL_XLATE           varchar2(254),
 PARCEL_ID              number(22,0),
 VERIFIED_STATUS_XLATE  varchar2(254),
 VERIFIED_STATUS_ID     number(22,0),
 FIRST_INSTALL_XLATE    varchar2(254),
 FIRST_INSTALL_YN       number(22,0),
 TAX_AMOUNT             varchar2(35),
 CREDIT_AMOUNT          varchar2(35),
 PENALTY_AMOUNT         varchar2(35),
 INTEREST_AMOUNT        varchar2(35),
 ASSESSMENT             varchar2(35),
 TAXABLE_VALUE          varchar2(35),
 RECEIVED_DATE          varchar2(35),
 LINE_NOTES             varchar2(2000)
);

alter table PWRPLANT.PT_IMPORT_STATEMENT_ARCHIVE
   add constraint PT_IMPORT_STMT_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_STATEMENT_ARCHIVE
   add constraint PT_IMPT_STMT_ARC_IMPT_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create global temporary table PWRPLANT.PT_TEMP_STATEMENT_LINES
(
 STATEMENT_ID            number(22,0) not null,
 STATEMENT_YEAR_ID       number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 ASSESSMENT              number(22,2),
 TAXABLE_VALUE           number(22,2),
 TAX_AMOUNT              number(22,2),
 CREDIT_AMOUNT           number(22,2),
 PENALTY_AMOUNT          number(22,2),
 INTEREST_AMOUNT         number(22,2),
 CALCULATED_AMOUNT       number(22,2) default 0 not null,
 BILLED_TAXABLE_VALUE    number(22,2) default 0 not null,
 PARCEL_TAXABLE_VALUE    number(22,2) default 0 not null,
 BILLED_ASSESSMENT       number(22,2) default 0 not null,
 PARCEL_ASSESSMENT       number(22,2) default 0 not null,
 TOTAL_CALCULATED_AMOUNT number(22,2) default 0 not null,
 TOTAL_ASSESSMENT        number(22,2) default 0 not null,
 TOTAL_TAXABLE_VALUE     number(22,2) default 0 not null
) on commit preserve rows;

create unique index PWRPLANT.PT_TEMP_STATEMENT_LINES_PK
   on PWRPLANT.PT_TEMP_STATEMENT_LINES ( STATEMENT_ID, STATEMENT_YEAR_ID, LINE_ID );

begin
   DBMS_STATS.SET_TABLE_STATS ('PWRPLANT','PT_TEMP_STATEMENT_LINES','',NULL,NULL,1,1,36,NULL);
end;
/

--
-- Add statement number to the 'Update : Bills' and 'Update : Bill Lines' types.
--
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 20, 'statement_number', sysdate, user, 'Statement Number', '', 0, 1, 'varchar2(35)', '', null, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 21, 'statement_number', sysdate, user, 'Statement Number', '', 0, 1, 'varchar2(35)', '', null, '', 0, '' );


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (163, 0, 10, 3, 5, 0, 6335, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_006335_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON