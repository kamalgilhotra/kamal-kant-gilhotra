/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041230_cr_incr_alloc_ddl_1.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   11/20/2015 M Allen, A Scott Incremental Allocations Setup DDL 1
||============================================================================
*/ 

--DROP TABLE CR_ALLOC_INCREMENTAL_CONTROL;
--DROP TABLE CR_ALLOC_INCREMENTAL_STATUS;
--DROP SEQUENCE CR_ALLOC_INCREMENTAL_SEQ;
--alter table CR_ALLOCATION_CONTROL drop column INCREMENTAL_ALLOCATION;

CREATE TABLE CR_ALLOC_INCREMENTAL_STATUS (
   INCREMENTAL_STATUS_ID NUMBER(22, 0) NOT NULL,
   DESCRIPTION VARCHAR2(35) NOT NULL,
   USER_ID VARCHAR2(18),
   TIME_STAMP DATE
   );

comment on table CR_ALLOC_INCREMENTAL_STATUS is '(F) [03] Tracks the status of a month''s incremental runs.';
comment on column CR_ALLOC_INCREMENTAL_STATUS.INCREMENTAL_STATUS_ID is '1=Processed, 2=Deleted/Undone, 3=Reversed, 4=Reprocessed';
comment on column CR_ALLOC_INCREMENTAL_STATUS.DESCRIPTION is 'Description for each individual status.';
comment on column CR_ALLOC_INCREMENTAL_STATUS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column CR_ALLOC_INCREMENTAL_STATUS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';


ALTER TABLE CR_ALLOC_INCREMENTAL_STATUS
ADD CONSTRAINT PK_CR_ALLOC_INCR_STATUS
PRIMARY KEY (INCREMENTAL_STATUS_ID) using INDEX tablespace pwrplant_idx;

CREATE TABLE CR_ALLOC_INCREMENTAL_CONTROL (
   INCREMENTAL_RUN_ID NUMBER(22, 0) NOT NULL,
   ALLOCATION_ID NUMBER(22, 0) NOT NULL,
   MONTH_RUN_ID NUMBER(22, 0),
   SOURCE_MONTH_NUMBER NUMBER(22, 0) NOT NULL,
   RUN_MONTH_NUMBER NUMBER(22, 0) NOT NULL,
   MIN_SOURCE_VALUE NUMBER(22, 0) NOT NULL,
   MAX_SOURCE_VALUE NUMBER(22, 0) NOT NULL,
   SOURCE_TABLE_NAME VARCHAR2(35),
   SOURCE_SQL CLOB,
   FINISHED DATE,
   INCREMENTAL_STATUS_ID NUMBER(22, 0),
   INCREMENTAL_RERUN_ID NUMBER(22, 0),
   RERUN_DATE DATE,
   USER_ID VARCHAR2(18),
   TIME_STAMP DATE
   );

comment on table CR_ALLOC_INCREMENTAL_CONTROL is '(C) [03] Tracks the run-by-run status of an incremental allocation';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.INCREMENTAL_RUN_ID is 'Sequenced value for tracking incremental allocations';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.ALLOCATION_ID is 'Unique identifier for a given allocation. Also, see cr_allocation_control.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.MONTH_RUN_ID is 'Increments by 1 per allocation per month. First monthly run = 1.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.SOURCE_MONTH_NUMBER is 'Month_number associated with the source charges.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.RUN_MONTH_NUMBER is 'Month_number associated with when the allocation is run.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.MIN_SOURCE_VALUE is 'Source value is the ID if the source_table_name is cr_cost_repository, drilldown_key otherwise.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.MAX_SOURCE_VALUE is 'Source value is the ID if the source_table_name is cr_cost_repository, drilldown_key otherwise.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.SOURCE_TABLE_NAME is 'Source charge table of origin. Also, see cr_alloc_process_control.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.SOURCE_SQL is 'Stores the where clause for a given allocation by incremental_run_id.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.FINISHED is 'Date the process finished';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.INCREMENTAL_STATUS_ID is '1=Processed, 2=Deleted/Undone, 3=Reversed, 4=Reprocessed';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.INCREMENTAL_RERUN_ID is 'Incremental_run_id that picked up deleted or reversed charges.';
comment on column CR_ALLOC_INCREMENTAL_CONTROL.RERUN_DATE is 'Date the rerun finished';


ALTER TABLE CR_ALLOC_INCREMENTAL_CONTROL
ADD CONSTRAINT PK_CR_ALLOC_INCR_CONTROL PRIMARY KEY (
   INCREMENTAL_RUN_ID,
   ALLOCATION_ID,
   SOURCE_MONTH_NUMBER,
   RUN_MONTH_NUMBER
   ) using INDEX tablespace pwrplant_idx;

CREATE INDEX CR_ALLOC_INCR_CTRL_IRID_IDX
ON CR_ALLOC_INCREMENTAL_CONTROL (INCREMENTAL_RUN_ID) tablespace pwrplant_idx;

CREATE INDEX CR_ALLOC_INCR_CTRL_ALLID_IDX
ON CR_ALLOC_INCREMENTAL_CONTROL (ALLOCATION_ID) tablespace pwrplant_idx;

ALTER TABLE CR_ALLOC_INCREMENTAL_CONTROL
ADD CONSTRAINT CR_ALLOC_INCR_CTRL_STAT_FKEY
FOREIGN KEY (INCREMENTAL_STATUS_ID)
REFERENCES CR_ALLOC_INCREMENTAL_STATUS;


ALTER TABLE CR_ALLOCATION_CONTROL
ADD INCREMENTAL_ALLOCATION NUMBER(22, 0);

comment on column CR_ALLOCATION_CONTROL.INCREMENTAL_ALLOCATION is '1=incremental allocation. 0 or null is whole month.';


CREATE SEQUENCE CR_ALLOC_INCREMENTAL_SEQ 
minvalue 1
maxvalue 2147483647 ---set to long limit
start with 1
increment BY 1
nocache;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2056, 0, 2015, 1, 0, 0, 41230, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041230_cr_incr_alloc_01_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;