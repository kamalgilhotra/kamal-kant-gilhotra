/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037646_aro_import_liab_adj.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 05/07/2014 Ryan Oliveria    Adding import tables and config for
||                                      importing ARO Liab Adjustments
||============================================================================
*/

-- * IMPORT TABLES

create table ARO_IMPORT_LIABILITY_ADJ
(
 IMPORT_RUN_ID             number(22,0) not null,
 LINE_ID                   number(22,0) not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 ARO_XLATE                 varchar2(35),
 ARO_ID                    number(22,0),
 LAYER_ID                  number(22,0),
 MONTH_YR                  date,
 LAYER_STREAM_DATE         varchar2(254),
 ACCRETION_ADJUST          number(22,2),
 LIABILITY_ADJUST          number(22,2),
 SETTLEMENT_ADJUST         number(22,2),
 NOTES                     varchar2(2000),
 DEPR_LEDGER_INCLUDE_XLATE varchar2(254),
 DEPR_LEDGER_INCLUDE       number(22,0),
 ADD_TO_EXISTING_XLATE     varchar2(254),
 ADD_TO_EXISTING           number(22,0),
 LOADED                    number(22,0),
 IS_MODIFIED               number(22,0),
 ERROR_MESSAGE             varchar2(4000)
);

alter table ARO_IMPORT_LIABILITY_ADJ
   add constraint ARO_IMPORT_LIABILITY_ADJ_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table ARO_IMPORT_LIABILITY_ADJ
   add constraint ARO_IMPORT_LIABILITY_ADJ_FK1
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);

alter table ARO_IMPORT_LIABILITY_ADJ
   add constraint ARO_IMPORT_LIABILITY_ADJ_FK2
       foreign key (DEPR_LEDGER_INCLUDE)
       references YES_NO (YES_NO_ID);

alter table ARO_IMPORT_LIABILITY_ADJ
   add constraint ARO_IMPORT_LIABILITY_ADJ_FK3
       foreign key (ADD_TO_EXISTING)
       references YES_NO (YES_NO_ID);


create table ARO_IMPORT_LIABILITY_ADJ_ARC
(
 IMPORT_RUN_ID             number(22,0) not null,
 LINE_ID                   number(22,0) not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 ARO_XLATE                 varchar2(35),
 ARO_ID                    number(22,0),
 LAYER_ID                  number(22,0),
 MONTH_YR                  date,
 LAYER_STREAM_DATE         varchar2(254),
 ACCRETION_ADJUST          number(22,2),
 LIABILITY_ADJUST          number(22,2),
 SETTLEMENT_ADJUST         number(22,2),
 NOTES                     varchar2(2000),
 DEPR_LEDGER_INCLUDE_XLATE varchar2(254),
 DEPR_LEDGER_INCLUDE       number(22,0),
 ADD_TO_EXISTING_XLATE     varchar2(254),
 ADD_TO_EXISTING           number(22,0),
 LOADED                    number(22,0),
 IS_MODIFIED               number(22,0),
 ERROR_MESSAGE             varchar2(4000)
);


-- * Table Comments
comment on table ARO_IMPORT_LIABILITY_ADJ is 'Staging table to import liability adjustments on ARO''s';
comment on column ARO_IMPORT_LIABILITY_ADJ.IMPORT_RUN_ID is 'System-assigned ID that specifies the import run that this record was imported in.';
comment on column ARO_IMPORT_LIABILITY_ADJ.LINE_ID is 'System-assigned line number for this import run.';
comment on column ARO_IMPORT_LIABILITY_ADJ.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ARO_IMPORT_LIABILITY_ADJ.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column ARO_IMPORT_LIABILITY_ADJ.ARO_XLATE is 'Translation field for determining the ARO';
comment on column ARO_IMPORT_LIABILITY_ADJ.ARO_ID is 'The internal ARO id within PowerPlant .';
comment on column ARO_IMPORT_LIABILITY_ADJ.LAYER_ID is 'The ARO Layer for this adjustment';
comment on column ARO_IMPORT_LIABILITY_ADJ.MONTH_YR is 'The month for this adjustment';
comment on column ARO_IMPORT_LIABILITY_ADJ.LAYER_STREAM_DATE is 'The stream date. Used for the "adjustments by stream date" feature.';
comment on column ARO_IMPORT_LIABILITY_ADJ.ACCRETION_ADJUST is 'The adjustment amount for Accretion';
comment on column ARO_IMPORT_LIABILITY_ADJ.LIABILITY_ADJUST is 'The adjustment amount for Liability';
comment on column ARO_IMPORT_LIABILITY_ADJ.SETTLEMENT_ADJUST is 'The adjustment amount for Settlement';
comment on column ARO_IMPORT_LIABILITY_ADJ.NOTES is 'Notes';
comment on column ARO_IMPORT_LIABILITY_ADJ.DEPR_LEDGER_INCLUDE_XLATE is 'Translation field for determining the Depr Ledger Include';
comment on column ARO_IMPORT_LIABILITY_ADJ.DEPR_LEDGER_INCLUDE is 'A flag to specify whether or not to include this value on the Depr Ledger (Only used for Regulated ARO''s)';
comment on column ARO_IMPORT_LIABILITY_ADJ.ADD_TO_EXISTING_XLATE is 'Translation field for determining the Add to Existing';
comment on column ARO_IMPORT_LIABILITY_ADJ.ADD_TO_EXISTING is 'If yes, then adjustments will be added to any existing adjustments. If no or blank, existing adjustments will be overwritten.';
comment on column ARO_IMPORT_LIABILITY_ADJ.LOADED is 'System-assigned number to specify if this row has been loaded into its table.';
comment on column ARO_IMPORT_LIABILITY_ADJ.IS_MODIFIED is 'System-assigned number to specify if this row has been modified from its original values.';
comment on column ARO_IMPORT_LIABILITY_ADJ.ERROR_MESSAGE is 'Error messages resulting from data valdiation in the import process.';

comment on table ARO_IMPORT_LIABILITY_ADJ_ARC is 'An archive table to keep track of previously imported liability adjustments on ARO''s';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.IMPORT_RUN_ID is 'System-assigned ID that specifies the import run that this record was imported in.';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.LINE_ID is 'System-assigned line number for this import run.';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.ARO_XLATE is 'Translation field for determining the ARO';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.ARO_ID is 'The internal ARO id within PowerPlant .';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.LAYER_ID is 'The ARO Layer for this adjustment';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.MONTH_YR is 'The month for this adjustment';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.LAYER_STREAM_DATE is 'The stream date. Used for the "adjustments by stream date" feature.';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.ACCRETION_ADJUST is 'The adjustment amount for Accretion';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.LIABILITY_ADJUST is 'The adjustment amount for Liability';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.SETTLEMENT_ADJUST is 'The adjustment amount for Settlement';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.NOTES is 'Notes';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.DEPR_LEDGER_INCLUDE_XLATE is 'Translation field for determining the Depr Ledger Include';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.DEPR_LEDGER_INCLUDE is 'A flag to specify whether or not to include this value on the Depr Ledger (Only used for Regulated ARO''s)';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.ADD_TO_EXISTING_XLATE is 'Translation field for determining the Add to Existing';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.ADD_TO_EXISTING is 'If yes, then adjustments will be added to any existing adjustments. If no or blank, existing adjustments will be overwritten.';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.LOADED is 'System-assigned number to specify if this row has been loaded into its table.';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.IS_MODIFIED is 'System-assigned number to specify if this row has been modified from its original values.';
comment on column ARO_IMPORT_LIABILITY_ADJ_ARC.ERROR_MESSAGE is 'Error messages resulting from data valdiation in the import process.';

-- * Admin >> Tools
insert into PP_CONVERSION_WINDOWS
   (WINDOW_ID, DESCRIPTION, LONG_DESCRIPTION, WINDOW_NAME)
   select max(WINDOW_ID) + 1, 'ARO Loader', 'ARO Loader for settlement adjustments', 'w_aro_loader'
     from PP_CONVERSION_WINDOWS;

-- * PPBASE IMPORT CONFIG

insert into PP_IMPORT_SUBSYSTEM
   (IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (10, sysdate, user, 'ARO', 'ARO');

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
   IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD,
   DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
values
   (300, sysdate, user, 'Add/Update: ARO Adjustments', 'Add/Update: ARO Adjustments',
   'aro_import_liability_adj', 'aro_import_liability_adj_arc', null, 1,
   'nvo_aro_logic_import', null, null, null);

insert into PP_IMPORT_TYPE_SUBSYSTEM
   (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID)
values
   (300, 10, sysdate, user);

insert into PP_IMPORT_TEMPLATE
   (IMPORT_TEMPLATE_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE,
    DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL,
          TYPE.IMPORT_TYPE_ID,
          'ARO Adjustments',
          'ARO Adjustments',
          user,
          sysdate,
          1,
          null,
          null,
          0
     from PP_IMPORT_TYPE type
    where TYPE.IMPORT_TABLE_NAME = 'aro_import_liability_adj';



--* Columns

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'aro_id', 'ARO', 'aro_xlate',
   1, 1, 'number(22,0)', 'aro', null,
   1, null, 'aro_id', null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'layer_id', 'Layer #', null,
   1, 1, 'number(22,0)', null, null,
   1, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'layer_stream_date', 'Layer Stream Date', null,
   0, 1, 'date', null, null,
   1, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'accretion_adjust', 'Accretion Adjustment', null,
   0, 1, 'number(22,2)', null, null,
   1, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'liability_adjust', 'Liability Adjustment', null,
   0, 1, 'number(22,2)', null, null,
   1, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'settlement_adjust', 'Settlement Adjustment', null,
   0, 1, 'number(22,2)', null, null,
   1, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'notes', 'Notes', null,
   0, 1, 'varchar2(2000)', null, null,
   1, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'depr_ledger_include', 'Include in Depr Ledger', 'depr_ledger_include_xlate',
   0, 1, 'number(1,0)', null, null,
   1, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME,
   IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2,
   IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE)
values
   (300, 'add_to_existing', 'Add to Existing Adjustments?', 'add_to_existing_xlate',
   0, 1, 'number(22,0)', 'yes_no', null,
   1, null, 'yes_no_id', null);


--* Lookups

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME,
   LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
values
   (2601, 'ARO Description', 'The passed in value corresponds to the ARO Description.  Translates to ARO ID.', 'aro_id',
   '( select aro_id from aro where upper(trim(description)) = upper(trim(<importfield>)))', 0, 'aro', 'description');

insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) values (300, 'aro_id', 2601);

insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) values (300, 'depr_ledger_include', 77);

insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) values (300, 'add_to_existing', 77);


--* Template Fields

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 1, 300, 'aro_id', 2601, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 2, 300, 'layer_id', null, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 3, 300, 'layer_stream_date', null, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 4, 300, 'accretion_adjust', null, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 5, 300, 'liability_adjust', null, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 6, 300, 'settlement_adjust', null, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 7, 300, 'notes', null, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 8, 300, 'depr_ledger_include', 77, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID,
    DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select IMPORT_TEMPLATE_ID, 9, 300, 'add_to_existing', 77, null, null, null
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'ARO Adjustments';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1153, 0, 10, 4, 3, 0, 37646, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037646_aro_import_liab_adj.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;