/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038414_taxrpr_loc_method.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 10.4.3.0 08/11/2014 Alex Pivoshenko
||========================================================================================
*/

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION,
    READ_ONLY)
values
   ('repair_location_method_id', 'work_order_type', 'rpr_repair_location_method_filter', 'p', 'Repair Location Method',
    98, '3 CWIP Processing', 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1320, 0, 10, 4, 3, 0, 38414, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038414_taxrpr_loc_method.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
