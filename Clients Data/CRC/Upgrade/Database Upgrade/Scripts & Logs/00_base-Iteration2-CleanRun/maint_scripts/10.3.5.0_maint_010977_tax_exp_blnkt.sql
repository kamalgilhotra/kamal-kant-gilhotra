/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010977_tax_exp_blnkt.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/29/2012 Sunjin Cone    Point Release
||============================================================================
*/

--repair_blanket_processing is a global temp so this delete should be harmless
delete from repair_blanket_processing;

alter table REPAIR_BLANKET_PROCESSING
   modify (PRORATA_TEST_QTY  number(22,8),
           PROP_QTY_PER_CIRCUIT     number(22,8));

-- PL/SQL block to raise error and stop processing if any of the statements fail.
begin
   execute immediate 'alter table REPAIR_BLANKET_PROCESS_REPORT add A_COLUMN_TEMP number(22, 8)';

   execute immediate 'update REPAIR_BLANKET_PROCESS_REPORT set A_COLUMN_TEMP = PRORATA_TEST_QTY';
   execute immediate 'commit';
   execute immediate 'update REPAIR_BLANKET_PROCESS_REPORT set PRORATA_TEST_QTY = null';
   execute immediate 'commit';
   execute immediate 'alter table REPAIR_BLANKET_PROCESS_REPORT modify PRORATA_TEST_QTY number(22, 8)';
   execute immediate 'update REPAIR_BLANKET_PROCESS_REPORT set PRORATA_TEST_QTY = A_COLUMN_TEMP';
   execute immediate 'commit';

   execute immediate 'update REPAIR_BLANKET_PROCESS_REPORT set A_COLUMN_TEMP = PROP_QTY_PER_CIRCUIT';
   execute immediate 'commit';
   execute immediate 'update REPAIR_BLANKET_PROCESS_REPORT set PROP_QTY_PER_CIRCUIT = null';
   execute immediate 'commit';
   execute immediate 'alter table REPAIR_BLANKET_PROCESS_REPORT modify PROP_QTY_PER_CIRCUIT number(22, 8)';
   execute immediate 'update REPAIR_BLANKET_PROCESS_REPORT set PROP_QTY_PER_CIRCUIT = A_COLUMN_TEMP';

   execute immediate 'alter table REPAIR_BLANKET_PROCESS_REPORT drop column A_COLUMN_TEMP';
end;
/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: Blanket Run Results I',
          'Blanket Network Repairs:  Part 1 of 2 Process Run Results for Blanket Work Orders using Pro-Rata or Proporional.  Displays quantity values tested for each work order number, repair unit, and location.',
          'dw_repair_blnkt_uop1_report',
          '',
          'RBLKT - 0001',
          'dw_repair_batch_control_blnkt',
          13,
          13,
          0,
          1,
          1,
          'RBLKT - 0001',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: Blanket Run Results II',
          'Blanket Network Repairs:  Part 2 of 2 Process Run Results for Blanket Work Orders using Pro-Rata or Proporional.  Display dollar values for each work order and repair unit.',
          'dw_repair_blnkt_uop2_report',
          '',
          'RBLKT - 0002',
          'dw_repair_batch_control_blnkt',
          13,
          13,
          0,
          1,
          1,
          'RBLKT - 0002',
          3,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (209, 0, 10, 3, 5, 0, 10977, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010977_tax_exp_blnkt.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;