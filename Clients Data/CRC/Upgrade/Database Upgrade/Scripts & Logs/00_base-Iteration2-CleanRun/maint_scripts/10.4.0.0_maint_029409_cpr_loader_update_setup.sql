/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029409_cpr_loader_update_setup.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   03/04/2013 Stephen Wicks    Point Release
||============================================================================
*/

-- Unassociate asset id to lookup. New asset load doesn't require lookup
update PP_IMPORT_COLUMN
   set IMPORT_COLUMN_NAME = null, PROCESSING_ORDER = 1
 where IMPORT_TYPE_ID = 53
   and COLUMN_NAME = 'asset_id';

delete from PP_IMPORT_COLUMN_LOOKUP
 where IMPORT_TYPE_ID = 53
   and COLUMN_NAME = 'asset_id';

-- Create new subledger lookup with no restriction. and rename the current subledger lookup to say that it's restricted to only subledger.
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL,
    IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS,
    LOOKUP_VALUES_ALTERNATE_SQL)
values
   (623, sysdate, user, 'Subledger Control.Description (No Restriction)',
    'The passed in value corresponds to the Subledger Control: Description. Translate to the Subledger Type ID  using Description on Subledger Control table',
    'subledger_type_id',
    '( select slc.subledger_type_id from subledger_control slc where upper( trim( <importfield> ) ) = upper( trim( slc.description ) ) )',
    0, 'subledger_control', 'description', null, null);

update PP_IMPORT_LOOKUP
   set DESCRIPTION = DESCRIPTION || ' (Restrict to Subledger)'
 where IMPORT_LOOKUP_ID = 620;

update PP_IMPORT_COLUMN_LOOKUP
   set IMPORT_LOOKUP_ID = 623
 where IMPORT_TYPE_ID in (53, 54)
   and COLUMN_NAME = 'subledger_indicator';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (311, 0, 10, 4, 0, 0, 29409, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029409_cpr_loader_update_setup.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;