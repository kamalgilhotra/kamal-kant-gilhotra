/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045261_sys_pwrplant_admin_update_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- ----------------------------------------
|| 2015.2.1.0  01/28/2016 David Haupt    New System Control for PP-45261
||============================================================================
*/

insert into pp_system_control_company (
	control_id, control_name, control_value, description, long_description, user_disabled, company_id)
select max(nvl(control_id,0)) + 1,
		 'PWRPLANT_ADMIN: Remove Char',
		 ' ',
		 'Defines the character to remove from the OS User Name in the getrole section of PWRPLANT_ADMIN.',
		 'Defines the character to remove from the OS User Name in the getrole section of PWRPLANT_ADMIN; this is used when the Oracle user id does not match the OS user id exactly for single sign on.',
		 1,
		 -1
  from pp_system_control_company;

 --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2994, 0, 2015, 2, 1, 0, 45261, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045261_sys_pwrplant_admin_update_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
   SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
