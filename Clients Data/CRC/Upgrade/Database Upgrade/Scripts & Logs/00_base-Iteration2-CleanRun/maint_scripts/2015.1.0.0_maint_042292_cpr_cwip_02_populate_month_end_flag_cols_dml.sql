/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042292_cpr_cwip_02_populate_month_end_flag_cols_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 01/29/2015 Charlie Shilling add flag columns so logs window can know which processes to display
||============================================================================
*/

UPDATE pp_processes
SET cpr_month_end_flag = 1
WHERE executable_file IN (
	'ssp_aro_approve.exe',
	'ssp_aro_calc.exe',
	'ssp_cpr_balance_pp.exe',
	'ssp_cpr_close_month.exe',
	'ssp_cpr_new_month.exe',
	'ssp_depr_approval.exe',
	'ssp_depr_calc.exe',
	'ssp_release_je.exe',
	'ssp_gl_recon.exe',
	'ssp_cpr_balance_pp.exe',
	'ssp_close_powerplant.exe'
)
OR description IN (
	'powerplant close',
	'cpr monthly close'
);

UPDATE pp_processes
SET wo_month_end_flag = 1
WHERE Lower(executable_file) IN (
	'ssp_wo_close_charge_collection.exe',
	'ssp_wo_calc_oh_afudc_wip.exe',
	'ssp_wo_new_month.exe',
	'ssp_wo_gl_reconciliation.exe',
	'ssp_release_je_wo.exe'
)
OR Lower(description) IN (
	'wo monthly close'
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2237, 0, 2015, 1, 0, 0, 042292, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042292_cpr_cwip_02_populate_month_end_flag_cols_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;