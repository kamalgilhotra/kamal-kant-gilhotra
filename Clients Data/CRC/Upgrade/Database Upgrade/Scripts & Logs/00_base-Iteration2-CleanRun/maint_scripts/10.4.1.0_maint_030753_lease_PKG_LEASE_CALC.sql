/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030753_lease_PKG_LEASE_CALC.sql
|| Description: Updates to allow revisions on MLA's
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/14/2013 Matthew Mikulka   Point Release
||============================================================================
*/

create or replace package PKG_LEASE_CALC as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 07/10/2013 Ryan Oliveria  MLA functions (workflows and revisions)
   ||============================================================================
   */

   procedure P_SET_ILR_ID(A_ILR_ID number);

   function F_APPROVE_MLA(A_LEASE_ID number,
                          A_REVISION number) return number;

   function F_REJECT_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_SEND_MLA(A_LEASE_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_MLA(A_LEASE_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID number,
                                  A_REVISION number) return number;


   function F_APPROVE_ILR(A_ILR_ID number,
                          A_REVISION number) return number;

   function F_REJECT_ILR(A_ILR_ID number,
                         A_REVISION number) return number;

   function F_SEND_ILR(A_ILR_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_ILR(A_ILR_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_ILR(A_ILR_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID number,
                                  A_REVISION number) return number;


   function F_GET_ILR_ID return number;

end PKG_LEASE_CALC;
/

create or replace package body PKG_LEASE_CALC as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   ||============================================================================
   */

   L_ILR_ID number;

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            P_SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************

   --**************************************************************************
   --                            F_APPROVE_MLA
   --**************************************************************************

   function F_APPROVE_MLA(A_LEASE_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      --Reject other revisions
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 4,
	     APPROVAL_DATE = sysdate,
	     APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      update LS_LEASE L
         set LEASE_STATUS_ID = 3,
	     APPROVAL_DATE = sysdate
       where LEASE_ID = A_LEASE_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_APPROVE_MLA;



   --**************************************************************************
   --                            F_REJECT_MLA
   --**************************************************************************

   function F_REJECT_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_REJECT_MLA;



   --**************************************************************************
   --                            F_SEND_MLA
   --**************************************************************************

   function F_SEND_MLA(A_LEASE_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 2
       where L.LEASE_ID = A_LEASE_ID
      --and nvl(l.revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_SEND_MLA;



   --**************************************************************************
   --                            F_UNREJECT_MLA
   --**************************************************************************

   function F_UNREJECT_MLA(A_LEASE_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2,
	     APPROVAL_STATUS_ID = 7
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 2
       where L.LEASE_ID = A_LEASE_ID
      --and nvl(l.revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_UNREJECT_MLA;



   --**************************************************************************
   --                            F_UNSEND_MLA
   --**************************************************************************

   function F_UNSEND_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2,
	     APPROVAL_STATUS_ID = 1
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 1,
	     APPROVAL_DATE = null
       where LEASE_ID = A_LEASE_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_UNSEND_MLA;



   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_MLA
   --**************************************************************************

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_LEASE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_LEASE_ID)
                                        and NVL(ID_FIELD2, '0') = TO_CHAR(NVL(A_REVISION, '0'))),
                                     0)
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_UPDATE_WORKFLOW_MLA;

   --**************************************************************************
   --                            F_APPROVE_ILR
   --**************************************************************************

   function F_APPROVE_ILR(A_ILR_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      --Reject other revisions
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 4,
	     APPROVAL_DATE = sysdate,
	     APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      update LS_ILR L
         set ILR_STATUS_ID = 3
	    where ILR_ID = A_ILR_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 3
       where ILR_ID = A_ILR_ID;

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_APPROVE_ILR;

   --**************************************************************************
   --                            F_REJECT_ILR
   --**************************************************************************

   function F_REJECT_ILR(A_ILR_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_ILR_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L
         set ILR_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID;

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_REJECT_ILR;


   --**************************************************************************
   --                            F_SEND_ILR
   --**************************************************************************

   function F_SEND_ILR(A_ILR_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;
      ILR number;
      SQLS varchar2(32000);
   begin
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L
         set ILR_STATUS_ID = 2
       where L.ILR_ID = A_ILR_ID
      --and nvl(l.revision,0) = nvl(A_REVISION,0)
      ;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 2
       where ILR_ID = A_ILR_ID;


      insert into LS_PEND_TRANSACTION (
         LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, LS_ASSET_ID, POSTING_AMOUNT, POSTING_QUANTITY, ACTIVITY_CODE, GL_JE_CODE)
       select ls_pend_transaction_seq.nextval, sysdate, l.user_id, l.ls_asset_id, l.capitalized_cost, l.quantity, 2,
           (SELECT S.GL_JE_CODE FROM STANDARD_JOURNAL_ENTRIES S, GL_JE_CONTROL G WHERE G.PROCESS_ID = 'LAM ADDS' AND G.JE_ID =   S.JE_ID)
         from ls_asset l
         where l.ilr_id = A_ILR_ID;


      select A.BOOK_SUMMARY_ID into RTN
        from LS_LEASE_CAP_TYPE A, LS_ILR_OPTIONS B
        where A.LS_LEASE_CAP_TYPE_ID = B.LEASE_CAP_TYPE_ID
          and B.ILR_ID = A_ILR_ID
          and B.REVISION = A_REVISION;


      insert into LS_PEND_BASIS (
           LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4,
           BASIS_5, BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10, BASIS_11, BASIS_12,
           BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20,
           BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26, BASIS_27,
           BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33, BASIS_34,
           BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
           BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48,
           BASIS_49, BASIS_50, BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55,
           BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60, BASIS_61, BASIS_62,
           BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70)
        SELECT B.LS_PEND_TRANS_ID, SYSDATE, A.USER_ID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
         FROM LS_PEND_TRANSACTION B, LS_ASSET A
          WHERE B.LS_ASSET_ID = A.LS_ASSET_ID
            AND A.ILR_ID = A_ILR_ID;

      ILR := A_ILR_ID;

      SQLS := 'UPDATE LS_PEND_BASIS B SET BASIS_' || to_char(RTN);
      SQLS := SQLS || ' = NVL((SELECT A.CAPITALIZED_COST FROM LS_ASSET A, LS_PEND_TRANSACTION C WHERE A.LS_ASSET_ID = C.LS_ASSET_ID  AND A.ILR_ID = ' || to_char(ILR);
      SQLS := SQLS || ' AND B.LS_PEND_TRANS_ID = C.LS_PEND_TRANS_ID),0) WHERE LS_PEND_TRANS_ID IN (SELECT D.LS_PEND_TRANS_ID FROM LS_PEND_TRANSACTION D, LS_ASSET E  WHERE E.ILR_ID = ';
      SQLS := SQLS || to_char(ILR) || ')';
      EXECUTE IMMEDIATE SQLS;

      insert into LS_PEND_CLASS_CODE (CLASS_CODE_ID, LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, VALUE)
        select A.CLASS_CODE_ID, B.LS_PEND_TRANS_ID, sysdate, B.USER_ID, A.VALUE
        from LS_ASSET_CLASS_CODE A, LS_PEND_TRANSACTION B, LS_ASSET C
        where A.LS_ASSET_ID = B.LS_ASSET_ID
          and A.LS_ASSET_ID = C.LS_ASSET_ID
          and C.ILR_ID = A_ILR_ID;

       commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_SEND_ILR;

   --**************************************************************************
   --                            F_UNREJECT_ILR
   --**************************************************************************

   function F_UNREJECT_ILR(A_ILR_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_ILR_APPROVAL
         set REJECTED = 2,
	     APPROVAL_STATUS_ID = 7
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L
         set ILR_STATUS_ID = 2
       where L.ILR_ID = A_ILR_ID
      ;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID;

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_UNREJECT_ILR;

   --**************************************************************************
   --                            F_UNSEND_ILR
   --**************************************************************************

   function F_UNSEND_ILR(A_ILR_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_ILR_APPROVAL
         set REJECTED = 2,
	     APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L
         set ILR_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID;


      delete from ls_pend_basis
      where ls_pend_trans_id in
       (select a.ls_pend_trans_id from ls_pend_transaction a, ls_asset b
         where a.ls_asset_id = b.ls_asset_id
           and b.ilr_id = A_ILR_ID);


      delete from ls_pend_class_code
      where ls_pend_trans_id in
       (select a.ls_pend_trans_id from ls_pend_transaction a, ls_asset b
           where a.ls_asset_id = b.ls_asset_id
           and b.ilr_id = A_ILR_ID);


      delete from ls_pend_transaction
      where ls_pend_trans_id in
       (select a.ls_pend_trans_id from ls_pend_transaction a, ls_asset b
           where a.ls_asset_id = b.ls_asset_id
           and b.ilr_id = A_ILR_ID);

      commit;

      return 1;
   exception
   when others then
      return -1;
      rollback;
   end F_UNSEND_ILR;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_ILR
   --**************************************************************************

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_ILR_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_ILR_ID)
                                        and NVL(ID_FIELD2, '0') = TO_CHAR(NVL(A_REVISION, '0'))),
                                     0)
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;

   exception
   when others then
      return -1;
      rollback;
   end F_UPDATE_WORKFLOW_ILR;

   --**************************************************************************
   --                            F_GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end PKG_LEASE_CALC;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (479, 0, 10, 4, 1, 0, 30753, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030753_lease_PKG_LEASE_CALC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;