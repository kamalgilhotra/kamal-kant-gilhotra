/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050376_lessor_03_rename_trans_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/09/2018 Anand R        Change description for trans type 4007
||============================================================================
*/

update je_trans_type set description = 'Lessor Initial Direct Costs Payable Credit'
where TRANS_TYPE = 4007;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4174, 0, 2017, 3, 0, 0, 50376, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050376_lessor_03_rename_trans_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;