/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035258_proptax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/29/2014 Andrew Scott        Prop Tax Tables needing class code
||                                         value vc2 widths of 254, not 35.
||============================================================================
*/

--not putting this in a plsql block.  If this is run at an install where
--the field is already width 254, no error will be raised.

alter table PT_TYPE_ASSIGN_CWIP_PSEUDO
   modify (CLASS_CODE_VALUE varchar2(254),
           PS_CLASS_CODE_VALUE varchar2(254));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (931, 0, 10, 4, 2, 0, 35258, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035258_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
