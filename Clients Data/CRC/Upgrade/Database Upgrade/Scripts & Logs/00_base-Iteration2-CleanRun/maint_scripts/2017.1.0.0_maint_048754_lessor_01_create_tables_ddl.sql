/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048754_lessor_01_create_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/11/2017 Anand R          Add Payor, company tables and payor lease tables for Lessor module
||============================================================================
*/

create table LSR_LEASE_APPROVAL
(
  lease_id           NUMBER(22) not null,
  revision           NUMBER(22) not null,
  time_stamp         DATE,
  user_id            VARCHAR2(18)
 );
 
comment on table LSR_LEASE_APPROVAL  is '(F)  [06] The Lessor Approval table records changes in the lease table.  For routing and approval purposes, changes to records in the lease table are tracked as pending until they receive proper approval.  The values for lessor approval are as follows:   Pending, Approved, and Denied.';

comment on column LSR_LEASE_APPROVAL.lease_id is 'The internal lessor lease id within PowerPlant .';
comment on column LSR_LEASE_APPROVAL.revision is 'The revision number.';
comment on column LSR_LEASE_APPROVAL.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column LSR_LEASE_APPROVAL.user_id is 'Standard System-assigned user id used for audit purposes.';

alter table LSR_LEASE_APPROVAL
  add constraint PK_LSR_LEASE_APPROVAL primary key (LEASE_ID, REVISION)
  USING INDEX
    TABLESPACE pwrplant_idx;

  alter table LSR_LEASE_APPROVAL
  add constraint FK_LSR_LEASE_APPROVAL foreign key (LEASE_ID)
  references LSR_LEASE (LEASE_ID);
  
  

create table LSR_LEASE_OPTIONS
(
  lease_id                NUMBER(22) not null,
  revision                NUMBER(22) not null,
  time_stamp              DATE,
  user_id                 VARCHAR2(18),
  renewal_option_type_id  NUMBER(22),
  purchase_option_type_id NUMBER(22),
  purchase_option_amt     NUMBER(22,2),
  cancelable_type_id      NUMBER(22),
  itc_sw                  NUMBER(1),
  intent_to_purchase      NUMBER(1),
  likely_to_collect       NUMBER(1),
  specialized_asset       NUMBER(1),
  sublease                NUMBER(1),
  sublease_lease_id       NUMBER(22,0)   
);

comment on table LSR_LEASE_OPTIONS  is '(S)  [06]The Lessor Lease Options table holds different lease options by revision.';

comment on column LSR_LEASE_OPTIONS.lease_id  is 'The internal lease id within PowerPlant .';
comment on column LSR_LEASE_OPTIONS.revision  is 'The revision number.';
comment on column LSR_LEASE_OPTIONS.time_stamp  is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LSR_LEASE_OPTIONS.user_id  is 'Standard system-assigned user id used for audit purposes.';
comment on column LSR_LEASE_OPTIONS.renewal_option_type_id  is 'Represents if this ILR has a renewal option.';
comment on column LSR_LEASE_OPTIONS.purchase_option_type_id  is 'Represents if this ILR has a purchase option.';
comment on column LSR_LEASE_OPTIONS.purchase_option_amt  is 'The amount of the purchase option.';
comment on column LSR_LEASE_OPTIONS.cancelable_type_id  is 'A PowerPlant internal id representing whether or not a lease can be cancelled before the lease terms are up.';
comment on column LSR_LEASE_OPTIONS.itc_sw  is 'A flag identifying whether this ILR has investment tax credits associate with it.';
comment on column LSR_LEASE_OPTIONS.intent_to_purchase  is 'A flag identifying whether there is intent to purchase.';
comment on column LSR_LEASE_OPTIONS.likely_to_collect  is 'A flag identifying if the lease will likely to be collected.';
comment on column LSR_LEASE_OPTIONS.specialized_asset  is 'A flag identifying if this MLA is for specialized assets.';
comment on column LSR_LEASE_OPTIONS.sublease  is 'A flag identifying if the asset can be sub leased.';
comment on column LSR_LEASE_OPTIONS.sublease_lease_id  is 'Lessor Lease ID if the assets are sub leased.';

alter table LSR_LEASE_OPTIONS
  add constraint PK_LSR_LEASE_OPTIONS primary key (LEASE_ID, REVISION)
  USING INDEX
    TABLESPACE pwrplant_idx;

alter table LSR_LEASE_OPTIONS
  add constraint FK_LSR_LEASE_OPTIONS foreign key (LEASE_ID, REVISION)
  references LSR_LEASE_APPROVAL (LEASE_ID, REVISION);
  


create table LSR_PAYOR
(
  payor_id               NUMBER(22) not null,
  time_stamp             DATE,
  user_id                VARCHAR2(18),
  lessee_id              NUMBER(22) not null,
  description            VARCHAR2(35) not null,
  long_description       VARCHAR2(255),
  external_payor_number  VARCHAR2(35),
  remit_addr             VARCHAR2(35),
  status_code_id         NUMBER(22)
);

comment on table LSR_PAYOR is '(S)  [06] The LSR Payor table holds payor information in the Lessor module.  All payors are related to one Lessee.';
 
comment on column LSR_PAYOR.payor_id  is 'The internal payor id within PowerPlant .';
comment on column LSR_PAYOR.time_stamp  is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LSR_PAYOR.user_id  is 'Standard system-assigned user id used for audit purposes.';
comment on column LSR_PAYOR.lessee_id  is 'The internal lessee id within PowerPlant .';
comment on column LSR_PAYOR.description  is 'a description field for the table.';
comment on column LSR_PAYOR.long_description  is 'a long description field for the table.';
comment on column LSR_PAYOR.external_payor_number  is 'A user generated unique identifier of a particular payor.';
comment on column LSR_PAYOR.remit_addr  is 'A remittance address for the payor.';
comment on column LSR_PAYOR.status_code_id  is 'A number indicating whether or not the payor is active.';

alter table LSR_PAYOR
  add constraint LSR_PAYOR_PK primary key (PAYOR_ID)
  using index 
  tablespace PWRPLANT_IDX;
  
alter table LSR_PAYOR
  add constraint FK_LSR_PAYOR_1 foreign key (LESSEE_ID)
  references LSR_LESSEE (LESSEE_ID);


create table LSR_LEASE_COMPANY
(
  lease_id       NUMBER(22) not null,
  company_id     NUMBER(22) not null,
  time_stamp     DATE,
  user_id        VARCHAR2(18)
);

comment on table LSR_LEASE_COMPANY  is '(S)  [06] The Lease Company table defines the valid payor companies for a particular lease agreement.';

comment on column LSR_LEASE_COMPANY.lease_id  is 'System-assigned identifier of a particular lease.';
comment on column LSR_LEASE_COMPANY.company_id  is 'System-assigned identifier of a particular lease company.';
comment on column LSR_LEASE_COMPANY.time_stamp  is 'Standard System-assigned timestamp used for audit purposes.';
comment on column LSR_LEASE_COMPANY.user_id  is 'Standard System-assigned user id used for audit purposes.';

alter table LSR_LEASE_COMPANY
  add constraint PK_LSR_LEASE_COMPANY primary key (LEASE_ID, COMPANY_ID)
  using index 
  tablespace PWRPLANT_IDX;
  
alter table LSR_LEASE_COMPANY
  add constraint FK_PAYEE_COMPANY foreign key (COMPANY_ID)
  references COMPANY_SETUP (COMPANY_ID);
  
alter table LSR_LEASE_COMPANY
  add constraint FK_payor_LEASE foreign key (LEASE_ID)
  references LSR_LEASE (LEASE_ID);
  

create table LSR_LEASE_PAYOR
(
  lease_id    NUMBER(22) not null,
  company_id  NUMBER(22) not null,
  payor_id   NUMBER(22) not null,
  time_stamp  DATE,
  user_id     VARCHAR2(18)
);

comment on table LSR_LEASE_PAYOR  is '(S)  [06]The LSR Lease Payor table connects an MLA to a Payor.';

comment on column LSR_LEASE_PAYOR.lease_id  is 'The internal lease id within PowerPlant .';
comment on column LSR_LEASE_PAYOR.company_id  is 'The company.';
comment on column LSR_LEASE_PAYOR.payor_id  is 'The internal vendor id within PowerPlant .';
comment on column LSR_LEASE_PAYOR.time_stamp  is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LSR_LEASE_PAYOR.user_id  is 'Standard system-assigned user id used for audit purposes.';

alter table LSR_LEASE_PAYOR
  add constraint PK_LSR_LEASE_PAYOR primary key (LEASE_ID, COMPANY_ID, PAYOR_ID)
  using index 
  tablespace PWRPLANT_IDX;
  
alter table LSR_LEASE_PAYOR
  add constraint FK_LSR_LEASE_PAYOR_1 foreign key (LEASE_ID, COMPANY_ID)
  references LSR_LEASE_COMPANY (LEASE_ID, COMPANY_ID);
  
alter table LSR_LEASE_PAYOR
  add constraint FK_LSR_LEASE_PAYOR_2 foreign key (PAYOR_ID)
  references LSR_PAYOR (PAYOR_ID);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3696, 0, 2017, 1, 0, 0, 48754, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048754_lessor_01_create_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;