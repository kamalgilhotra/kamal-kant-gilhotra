/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_lookup_fixes.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   02/05/2013 Stephen Wicks  Point Release
||============================================================================
*/

-- fix some import lookup formatting issues
update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = 'decode( upper( trim( <importfield> ) ), ''NET'', 1, ''GROSS'', 2, 0 )'
 where IMPORT_LOOKUP_ID = 508;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = 'decode( upper( trim( <importfield> ) ), ''NO'', 0, ''YES/SEPARATE'', 1, ''YES/LIFE ONLY'', 2, ''YES/TOTAL'', 3 )'
 where IMPORT_LOOKUP_ID = 509;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = 'decode( upper( trim( <importfield> ) ), ''NO RECALC'', 0, ''USED'', 1, ''RECALC'', 2, ''BLEND RATES'', 3 )'
 where IMPORT_LOOKUP_ID = 510;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = 'decode( upper( trim( <importfield> ) ), ''BROAD GROUP'', ''BROAD'', ''EQUAL-LIFE GROUP'', ''ELG'' )'
 where IMPORT_LOOKUP_ID = 512;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = 'decode( upper( trim( <importfield> ) ), ''NO'', 0, ''YES'', 1, ''DEPR EXP'', 2 )'
 where IMPORT_LOOKUP_ID = 513;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select set_of_books_id from set_of_books sob where upper( trim( <importfield> ) ) = upper( trim( sob.description ) ) )'
 where IMPORT_LOOKUP_ID = 501;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select set_of_books_id from depr_group dg where upper( trim( <importfield> ) ) = upper( trim( dg.description ) ) and <importtable>.company_id = dg.company_id )'
 where IMPORT_LOOKUP_ID = 502;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select set_of_books_id from depr_group dg where upper( trim( <importfield> ) ) = upper( trim( dg.external_depr_code ) )  and <importtable>.company_id = dg.company_id )'
 where IMPORT_LOOKUP_ID = 503;

update PWRPLANT.PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select pt.parcel_type_id from pt_parcel_type pt where upper( trim( <importfield> ) ) = upper( trim( pt.description ) ) and pt.parcel_type_id <> -100 union select 0 from dual where upper( trim( <importfield> ) ) = ''ALL'' )'
 where IMPORT_LOOKUP_ID = 168;

update PWRPLANT.PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select pt.parcel_type_id from pt_parcel_type pt where upper( trim( <importfield> ) ) = upper( trim( pt.long_description ) ) and pt.parcel_type_id <> -100 union select 0 from dual where upper( trim( <importfield> ) ) = ''ALL'' )'
 where IMPORT_LOOKUP_ID = 169;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (292, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_lookup_fixes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
