/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043285_cwip_index_auto_unitization_gtt_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 3/20/2015  Luke Warren      Create indexes on Auto Unitization GTT
||============================================================================
*/

-- you cannot specify a tablespace for an index on a global temporary table
create index UNITIZE_ALLOC_CGC_INS_TEMP_IX on UNITIZE_ALLOC_CGC_INSERTS_TEMP (WORK_ORDER_ID, ALLOCATION_PRIORITY, ID);
create index UNITIZE_WO_LIST_TEMP_IX on UNITIZE_WO_LIST_TEMP (WORK_ORDER_ID, MAX_CHARGE_GROUP_ID, ERROR_MSG);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2391, 0, 2015, 1, 0, 0, 043285, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043285_cwip_index_auto_unitization_gtt_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;