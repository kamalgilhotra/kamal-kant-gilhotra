/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052035_lessee_payment_approval_amount_fix_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.4.0.0 07/23/2018 Shane "C" Ward     Fix delivered workflow_amount_sql for Lessee Payments
||============================================================================
*/

UPDATE WORKFLOW_AMOUNT_SQL
   SET SQL = 'select nvl(sum(amount),0) from LS_PAYMENT_HDR where payment_id = <<id_field1>>'
 WHERE UPPER(SUBSYSTEM) = 'LS_PAYMENT_APPROVAL'
   AND LOWER(SQL) = 'select nvl(sum(amount),0) from all ls_payment_hdr where payment_id = <<id_field1>>'
;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8462, 0, 2017, 4, 0, 0, 52035, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052035_lessee_payment_approval_amount_fix_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;   
