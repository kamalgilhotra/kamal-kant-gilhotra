/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051075_lease_payment_header_and_invoice_query_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 05/03/2018 Anand R          PP-51075
||============================================================================
*/

UPDATE pp_any_query_criteria
SET SQL = 'WITH currency_selection AS (
SELECT lct.ls_currency_type_id, lct.description
FROM ls_lease_currency_type lct, pp_any_required_filter parf
where upper(trim(parf.column_name)) = ''CURRENCY TYPE''
AND  upper(trim(parf.filter_value)) =  upper(trim(lct.description))
)
select to_char(lph.gl_posting_mo_yr, ''yyyymm'') as monthnum,
       lph.company_id,
       lv.description as vendor,
       aps.description as payment_status,
       lpl.amount as calculated_amount, 
       lpl.adjustment_amount as adjustments, 
       lph.amount as total_payment, 
       inv.invoice_number,
       inv.invoice_amount, 
       decode(map.in_tolerance, 1, ''Yes'', null, null, ''No'') as in_tolerance,
       ll.lease_id,
       ll.lease_number,
       lct.description as lease_cap_type,
       nvl(ilr.ilr_id, -1) as ilr_id,
       nvl(ilr.ilr_number, ''N/A'') as ilr_number,
       nvl(lfs.description, ''N/A'') as funding_status,
       nvl(la.ls_asset_id, -1) as ls_asset_id,
       nvl(la.leased_asset_number, ''N/A'') as leased_asset_number,
       co.description as company_description,
       InitCap(currency_selection.description) currency_type,
       lph.iso_code currency,
       lph.currency_display_symbol currency_symbol
  from v_ls_payment_hdr_fx lph -- Multi Curr View
       left OUTER JOIN ls_asset la ON (lph.ls_asset_id = la.ls_asset_id)
       left JOIN ls_ilr ilr ON (lph.ilr_id = ilr.ilr_id)
       JOIN ls_lease ll ON (lph.lease_id = ll.lease_id)
       JOIN company co ON (co.company_id = lph.company_id)
       JOIN ls_lease_cap_type lct ON (ll.lease_cap_type_id = lct.ls_lease_cap_type_id)
       left OUTER JOIN ls_funding_status lfs ON (ilr.funding_status_id = lfs.funding_status_id)
       JOIN ls_vendor lv ON (lph.vendor_id = lv.vendor_id)
       JOIN approval_status aps ON (lph.payment_status_id = aps.approval_status_id)
       left JOIN ls_invoice_payment_map map ON (lph.payment_id = map.payment_id)
       left OUTER JOIN v_ls_invoice_fx_vw inv ON (map.invoice_id = inv.invoice_id)
       JOIN (select payment_id,
               sum(nvl(amount, 0)) as amount,
               sum(nvl(adjustment_amount, 0)) as adjustment_amount,
               ls_cur_type
          from v_ls_payment_line_fx, currency_selection
         where v_ls_payment_line_fx.ls_cur_type = currency_selection.ls_currency_type_id
         AND to_char(gl_posting_mo_yr, ''yyyymm'') in
               (select filter_value
                  from pp_any_required_filter
                 where upper(trim(column_name)) = ''MONTHNUM'')       group by payment_id, ls_cur_type) lpl ON (lpl.ls_cur_type = lph.ls_cur_type AND lpl.payment_id = lph.payment_id)
       JOIN currency_selection ON (lpl.ls_cur_type = currency_selection.ls_currency_type_id AND inv.ls_cur_type = currency_selection.ls_currency_type_id)
 where to_char(lph.gl_posting_mo_yr, ''yyyymm'') in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''MONTHNUM'')
   and to_char(lph.company_id) in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''COMPANY ID'')'
WHERE description = 'Lease Payment Header and Invoice Detail';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5062, 0, 2017, 3, 0, 0, 51075, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051075_lease_payment_header_and_invoice_query_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;



