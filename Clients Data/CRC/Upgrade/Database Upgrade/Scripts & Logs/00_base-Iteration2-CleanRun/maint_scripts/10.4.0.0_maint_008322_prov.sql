/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008322_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/09/2012 Blake Andrews  Point Release
||============================================================================
*/

create table PWRPLANT.TAX_ACCRUAL_EFF_RATE
(
 TA_VERSION_ID          number(22),
 COMPANY_ID             number(22),
 ENTITY_INCLUDE_ID      number(22),
 RATE_CALC_TYPE_ID      number(22), -- 0 = Does not impact current tax (ignore these)
                                    -- 1 = Apportioned increase/decrease to taxable income
                                    --          - Pre-Tax Book Income
                                    --          - Tax Items
                                    --          - Temporary Differences/Perms
                                    -- 2 = Non-Apportioned increase/decrease to taxable income
                                    -- 3 = Tax credits (increase/decrease to tax)
 ENTITY_ID              number(22),
 GL_MONTH               number(22,2),
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 INCLUDED               number(1),
 STATUTORY_RATE         number(22,8),
 ALLOCATION_FACTOR      number(22,8),
 EFF_RATE_BEFORE_DEDUCT number(22,15),
 EFF_RATE_DEDUCT        number(22,15),
 EFF_RATE_DEDUCT_PRIOR  number(22,15),
 constraint TA_EFF_RATE primary key (TA_VERSION_ID, COMPANY_ID, ENTITY_INCLUDE_ID, RATE_CALC_TYPE_ID, ENTITY_ID, GL_MONTH)
    using index tablespace PWRPLANT_IDX
);

create global temporary table PWRPLANT.TAX_ACCRUAL_PROCESS_TMP
(
 CRITERIA_TYPE varchar2(35),
 VALUE1_NUM    number(22,8) default 0,
 VALUE2_NUM    number(22,8) default 0,
 VALUE3_NUM    number(22,8) default 0,
 VALUE1_STR    varchar2(35) default ' ',
 VALUE2_STR    varchar2(35) default ' ',
 VALUE3_STR    varchar2(35) default ' ',
 DESCRIPTION   varchar2(254),
 constraint TA_PROC_TMP_PK primary key (CRITERIA_TYPE, VALUE1_NUM, VALUE2_NUM, VALUE3_NUM, VALUE1_STR, VALUE2_STR, VALUE3_STR)
) on commit preserve rows;

create table PWRPLANT.TAX_ACCRUAL_M_TYPE_TREATMENT
(
 M_TYPE_TREATMENT_ID number(22),
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 DESCRIPTION         varchar2(35),
 RATE_CALC_TYPE_ID   number(22), -- 0 = Does not impact current tax (ignore these)
                                 -- 1 = Apportioned increase/decrease to taxable income
                                 --          - Pre-Tax Book Income
                                 --          - Tax Items
                                 --          - Temporary Differences/Perms
                                 -- 2 = Non-Apportioned increase/decrease to taxable income
                                 -- 3 = Tax credits (increase/decrease to tax)
 constraint TA_M_TYPE_TREATMENT_PK primary key (M_TYPE_TREATMENT_ID)
    using index tablespace PWRPLANT_IDX
);

insert into TAX_ACCRUAL_M_TYPE_TREATMENT
   (M_TYPE_TREATMENT_ID, DESCRIPTION, RATE_CALC_TYPE_ID)
   select 0,'Other',0                        from dual union all
   select 1,'PreTax Income',1                from dual union all
   select 2,'Tax Items',1                    from dual union all
   select 3,'Taxable Income Adjustments',1   from dual union all
   select 4,'Post Apportioned Adjustments',2 from dual union all
   select 5,'Tax Credits and Adjustments',3  from dual;
commit;

create table PWRPLANT.TAX_ACCRUAL_PROC_DEDUCT
(
 TA_VERSION_ID        number(22),
 COMPANY_ID           number(22),
 ENTITY_ID            number(22),
 ENTITY_ID_DEDUCTIBLE number(22),
 GL_MONTH             number(22,2),
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 TAX_CREDIT           number(22),
 DEDUCT_PERCENT       number(22,8),
 constraint TA_PROC_DEDUCT primary key (TA_VERSION_ID, COMPANY_ID, ENTITY_ID, ENTITY_ID_DEDUCTIBLE, GL_MONTH)
    using index tablespace PWRPLANT_IDX
);

create table PWRPLANT.TAX_ACCRUAL_M_CURRENT
(
 m_id                   number(22),
 entity_id              number(22),
 gl_month               number(22,2),
 time_stamp             date,
 user_id                varchar2(18),
 included               number(1),
 statutory_rate         number(22,8),
 allocation_factor      number(22,8),
 eff_rate_before_deduct number(22,15),
 eff_rate_deduct        number(22,15),
 amount                 number(22,2),
 tax_amount             number(22,2),
 tax_amount_deduct      number(22,2),
 amount_ytd             number(22,2),
 tax_amount_ytd         number(22,2),
 tax_amount_deduct_ytd  number(22,2),
 constraint TA_M_CURR_PK primary key (M_ID, ENTITY_ID, GL_MONTH)
    using index tablespace PWRPLANT_IDX
);

update PP_REPORTS
   set DESCRIPTION = 'Income Tax Footnote ETR',
       LONG_DESCRIPTION = 'ETR Footnote view with one line for Total State Tax.  Each adjustment shows only the Federal effect on the adjustment.  Runs for a range of months.',
       DATAWINDOW = 'dw_tax_accrual_etr_footnote', SPECIAL_NOTE = 'ETR_FOOTNOTE'
 where REPORT_ID = 54511;
commit;

insert into TAX_ACCRUAL_REP_SPECIAL_NOTE
   (SPECIAL_NOTE_ID, DESCRIPTION, REPORT_OPTION_ID, MONTH_REQUIRED, COMPARE_CASE, M_ROLLUP_REQUIRED,
    CALC_RATES, REPORT_TYPE, DW_PARAMETER_LIST, JE_TEMP_TABLE_IND, NS2FAS109_TEMP_TABLE_IND,
    REP_ROLLUP_GROUP_ID)
values
   (72, 'ETR_FOOTNOTE', 27, 1, 0, 1, 0, 0, 5, 0, 0, 12);

alter table PWRPLANT.TAX_ACCRUAL_ROLLUP_DETAIL add IS_DISCRETE number(22) default 0;
alter table PWRPLANT.TAX_ACCRUAL_M_TYPE        add IS_DISCRETE number(22) default 0;

update TAX_ACCRUAL_ROLLUP_DETAIL
   set IS_DISCRETE = 1
 where INSTR(UPPER(DESCRIPTION), 'DISCRETE') > 0;
commit;

update TAX_ACCRUAL_M_TYPE set IS_DISCRETE = 1 where INSTR(UPPER(DESCRIPTION), 'DISCRETE') > 0;
commit;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('is_discrete', 'tax_accrual_m_type', 'e', 'Is Discrete Item (1 or 0)', 8, 0);

--Moved view TAX_ACCRUAL_M_ROLLUP_VIEW to its own file: maint_008322_prov_TAX_ACCRUAL_M_ROLLUP_VIEW.sql

update PP_REPORTS
   set DESCRIPTION = 'Income Tax Footnote ETR - Grid',
       LONG_DESCRIPTION = 'ETR Footnote view with one line for Total State Tax.  Each adjustment shows only the Federal effect on the adjustment.  Runs for a range of months.  (Grid Format)',
       SUBSYSTEM = 'Tax Accrual', DATAWINDOW = 'dw_tax_accrual_etr_footnote_grid',
       SPECIAL_NOTE = 'ETR_FOOTNOTE_CONS'
 where REPORT_ID = 54513;

insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, REP_CONS_CATEGORY_ID, BALANCES_IND)
values
   (30, 'ETR Footnote by Company', 'dw_tax_accrual_etr_footnote_grid', 5, 0);

insert into TAX_ACCRUAL_REP_SPECIAL_NOTE
   (SPECIAL_NOTE_ID, DESCRIPTION, REPORT_OPTION_ID, MONTH_REQUIRED, COMPARE_CASE, M_ROLLUP_REQUIRED,
    CALC_RATES, REPORT_TYPE, WINDOW_NAME, DW_PARAMETER_LIST, JE_TEMP_TABLE_IND,
    NS2FAS109_TEMP_TABLE_IND, REP_ROLLUP_GROUP_ID, DEFAULT_ACCT_ROLLUP, REP_CONS_CATEGORY_ID)
   select 75                       SPECIAL_NOTE_ID,
          'ETR_FOOTNOTE_CONS',
          REPORT_OPTION_ID,
          MONTH_REQUIRED,
          COMPARE_CASE,
          M_ROLLUP_REQUIRED,
          CALC_RATES,
          REPORT_TYPE,
          WINDOW_NAME,
          DW_PARAMETER_LIST,
          JE_TEMP_TABLE_IND,
          NS2FAS109_TEMP_TABLE_IND,
          REP_ROLLUP_GROUP_ID,
          DEFAULT_ACCT_ROLLUP,
          5                        REP_CONS_CATEGORY_ID
     from TAX_ACCRUAL_REP_SPECIAL_NOTE
    where DESCRIPTION = 'ETR_PERIOD_CONS';

insert into TAX_ACCRUAL_REP_CONS_COLS(REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 30, 'parent_id', 1, 'number', 'consol_desc', 0                 from dual union all
   select 30, 'detail_description', 1, 'string', 'detail_description', 1 from dual union all
   select 30, 'rollup_description', 1, 'string', 'rollup_description', 2 from dual;

update TAX_ACCRUAL_REP_CONS_TYPE
   set SORT_SPEC = 'report_section, ms_summarized, rollup_description, detail_description'
 where REP_CONS_TYPE_ID = 30;

alter table PWRPLANT.TAX_ACCRUAL_REP_CONS_ROWS
   drop column SHOW_WHEN_SUMMARIZED;

alter table PWRPLANT.TAX_ACCRUAL_REP_CONS_ROWS
   drop column SHOW_ONLY_WHEN_SUMMARIZED;

alter table PWRPLANT.TAX_ACCRUAL_REP_CONS_ROWS
   add DISPLAY_IND_FIELD varchar2(2000);

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 30, 'report_section', 1, 'number', 'report_section', 3 from DUAL;

--Create the by-oper version of this report
insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, SORT_SPEC, REP_CONS_CATEGORY_ID, BALANCES_IND)
values
   (31, 'ETR Footnote by Oper', 'dw_tax_accrual_etr_footnote_grid',
    'report_section, ms_summarized, rollup_description, detail_description', 5, 0);

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 31, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID
     from TAX_ACCRUAL_REP_CONS_COLS
    where REP_CONS_TYPE_ID = 30;

update TAX_ACCRUAL_REP_CONS_COLS
   set CONS_COLNAME = 'oper_ind', CONS_DESCR_COLNAME = 'oper_descr'
 where REP_CONS_TYPE_ID = 31
   and GROUP_ID = 0;

insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, REP_CONS_CATEGORY_ID, BALANCES_IND)
values
   (0, 'None', 0, 0);

--Create the by-month version of this report
insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, SORT_SPEC, REP_CONS_CATEGORY_ID, BALANCES_IND)
values
   (32, 'ETR Footnote by Month', 'dw_tax_accrual_etr_footnote_grid',
    'report_section, ms_summarized, rollup_description, detail_description', 5, 0);

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 32, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID
     from TAX_ACCRUAL_REP_CONS_COLS
    where REP_CONS_TYPE_ID = 31;

update TAX_ACCRUAL_REP_CONS_COLS
   set CONS_COLNAME = 'month_id', CONS_DESCR_COLNAME = 'month_descr_dtl'
 where REP_CONS_TYPE_ID = 32
   and GROUP_ID = 0;

--Create the by-month type version of this report
insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, SORT_SPEC, REP_CONS_CATEGORY_ID, BALANCES_IND)
values
   (33, 'ETR Footnote by Month Type', 'dw_tax_accrual_etr_footnote_grid',
    'report_section, ms_summarized, rollup_description, detail_description', 5, 0);

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 33, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID
     from TAX_ACCRUAL_REP_CONS_COLS
    where REP_CONS_TYPE_ID = 32;

alter table PWRPLANT.TAX_ACCRUAL_REP_CONS_ROWS
   add TOTAL_COL_FIELDNAME varchar2(2000);

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 1, TO_TIMESTAMP('19-09-2012 12:03:11', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 1, 1,
    'TEXT', 1, 'Tax Footnote ETR Report', null, null, 'T', null, null);

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 2, TO_TIMESTAMP('19-09-2012 12:03:11', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 2, 1,
    'USER_TITLE', 0, null, null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 3, TO_TIMESTAMP('19-09-2012 12:03:11', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 3, 1,
    'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 4, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 5, 1,
    'COL_VALUE_TEXT', 0, 'oper_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 5, TO_TIMESTAMP('19-09-2012 13:19:25', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 6, 1,
    'COL_VALUE_TEXT', 0, 'month_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 6, TO_TIMESTAMP('19-09-2012 13:19:25', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 7, 1,
    'FILLER', 0, null, null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 7, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 1, 0,
    'COL_VALUE_NUM', 0, 'ptbi', 'TEXT', 'Book Income', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 8, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 2, 0,
    'COL_VALUE_NUM', 0, 'tax_items', 'TEXT', 'Tax Items', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 9, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 3, 0,
    'COL_VALUE_NUM', 0, 'comp_h1_bibit', 'TEXT',
    'Book Income Before Income Taxes (Adjusted for Tax Items)', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 10, TO_TIMESTAMP('19-09-2012 12:03:11', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 4, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 11, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 1, 0,
    'COL_VALUE_NUM', 0, 'comp_dtl_tt', 'COL_VALUE_TEXT', 'detail_description', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 12, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 2, 0,
    'COL_VALUE_NUM', 0, 'state_expense', 'TEXT', 'State Tax Expense', 'T',
    'format_003_state_taxes_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 13, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 3, 0,
    'COL_VALUE_NUM', 0, 'deduct_fed', 'TEXT', 'Fed Benefit of State Tax Deduction', 'T',
    'format_003_state_taxes_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 15, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 2, 1, 1,
    'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_description', 'T', 'format_011_subtotal_visible ',
    null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 14, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 2, 1, 0,
    'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_description', 'T',
    'format_001_rollup_description_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 16, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 2, 2, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', 'format_011_subtotal_visible ', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 17, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 1, 1,
    'TEXT', 0, null, 'TEXT', 'Taxes Before Adjustments', 'T', 'info_report_section_is_10', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 18, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 2, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', 'info_report_section_is_10', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 19, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 3, 1,
    'COL_VALUE_NUM', 0, 'comp_dtl_tte_b4_discretes_tt', 'TEXT',
    'Total Tax Expense (Benefit) Before Discrete Items', 'T', 'info_report_section_is_40', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 20, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 1, 0,
    'COL_VALUE_NUM', 0, 'curtax_not_expense_fed', 'TEXT', 'Current Tax Not Booked to Expense', 'T',
    'show_curtax_not_expense_cons', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 21, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 2, 1,
    'COL_VALUE_NUM', 0, 'comp_dtl_tte_after_discretes_tt', 'TEXT',
    'Total Tax Expense (Benefit) With Discrete Items', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 22, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 3, 0,
    'COL_VALUE_NUM', 0, 'total_tax_expense', 'TEXT', 'Tax Expense Booked', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 23, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 4, 0,
    'COL_VALUE_NUM', 0, 'comp_tt_difference', 'TEXT', 'Difference', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 24, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 5, 0,
    'COL_VALUE_NUM', 0, 'total_co_etr', 'TEXT', 'Total ETR Percentage', 'P', null,
    'all_co_etr_total');
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (30, 25, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 6, 0,
    'COL_VALUE_NUM', 0, 'all_co_etr', 'TEXT', 'Consolidated ETR Percentage', 'P', null,
    'all_co_etr_total');
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 1, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 1, 1,
    'TEXT', 1, 'Tax Footnote ETR Report', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 2, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 2, 1,
    'USER_TITLE', 0, null, null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 3, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 3, 1,
    'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 4, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 4, 1,
    'COL_VALUE_TEXT', 0, 'consol_desc', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 5, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 5, 1,
    'COL_VALUE_TEXT', 0, 'month_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 6, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 6, 1,
    'FILLER', 0, null, null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 7, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 1, 0,
    'COL_VALUE_NUM', 0, 'ptbi', 'TEXT', 'Book Income', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 8, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 2, 0,
    'COL_VALUE_NUM', 0, 'tax_items', 'TEXT', 'Tax Items', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 9, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 3, 0,
    'COL_VALUE_NUM', 0, 'comp_h1_bibit', 'TEXT',
    'Book Income Before Income Taxes (Adjusted for Tax Items)', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 10, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 4, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 11, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 1, 0,
    'COL_VALUE_NUM', 0, 'comp_dtl_tt', 'COL_VALUE_TEXT', 'detail_description', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 12, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 2, 0,
    'COL_VALUE_NUM', 0, 'state_expense', 'TEXT', 'State Tax Expense', 'T',
    'format_003_state_taxes_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 13, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 3, 0,
    'COL_VALUE_NUM', 0, 'deduct_fed', 'TEXT', 'Fed Benefit of State Tax Deduction', 'T',
    'format_003_state_taxes_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 14, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 2, 1, 0,
    'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_description', 'T',
    'format_001_rollup_description_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 15, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 2, 1, 1,
    'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_description', 'T', 'format_011_subtotal_visible ',
    null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 16, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 2, 2, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', 'format_011_subtotal_visible ', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 17, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 1, 1,
    'TEXT', 0, null, 'TEXT', 'Taxes Before Adjustments', 'T', 'info_report_section_is_10', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 18, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 2, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', 'info_report_section_is_10', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 19, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 3, 1,
    'COL_VALUE_NUM', 0, 'comp_dtl_tte_b4_discretes_tt', 'TEXT',
    'Total Tax Expense (Benefit) Before Discrete Items', 'T', 'info_report_section_is_40', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 20, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 1, 0,
    'COL_VALUE_NUM', 0, 'curtax_not_expense_fed', 'TEXT', 'Current Tax Not Booked to Expense', 'T',
    'show_curtax_not_expense_cons', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 21, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 2, 1,
    'COL_VALUE_NUM', 0, 'comp_dtl_tte_after_discretes_tt', 'TEXT',
    'Total Tax Expense (Benefit) With Discrete Items', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 22, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 3, 0,
    'COL_VALUE_NUM', 0, 'total_tax_expense', 'TEXT', 'Tax Expense Booked', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 23, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 4, 0,
    'COL_VALUE_NUM', 0, 'comp_tt_difference', 'TEXT', 'Difference', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 24, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 5, 0,
    'COL_VALUE_NUM', 0, 'total_co_etr', 'TEXT', 'Total ETR Percentage', 'P', null,
    'all_co_etr_total');
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (31, 25, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 6, 0,
    'COL_VALUE_NUM', 0, 'all_co_etr', 'TEXT', 'Consolidated ETR Percentage', 'P', null,
    'all_co_etr_total');
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 1, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 1, 1,
    'TEXT', 1, 'Tax Footnote ETR Report', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 2, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 2, 1,
    'USER_TITLE', 0, null, null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 3, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 3, 1,
    'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 4, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 4, 1,
    'COL_VALUE_TEXT', 0, 'consol_desc', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 5, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 5, 1,
    'COL_VALUE_TEXT', 0, 'oper_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 6, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 7, 1,
    'COL_VALUE_TEXT', 0, 'month_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 7, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 8, 1,
    'FILLER', 0, null, null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 8, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 1, 0,
    'COL_VALUE_NUM', 0, 'ptbi', 'TEXT', 'Book Income', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 9, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 2, 0,
    'COL_VALUE_NUM', 0, 'comp_h1_tax_items_total', 'TEXT', 'Tax Items', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 10, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 3, 0,
    'COL_VALUE_NUM', 0, 'comp_h1_bibit_total', 'TEXT',
    'Book Income Before Income Taxes (Adjusted for Tax Items)', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 11, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 4, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 12, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 1, 0,
    'COL_VALUE_NUM', 0, 'comp_dtl_tt_total', 'COL_VALUE_TEXT', 'detail_description', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 13, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 2, 0,
    'COL_VALUE_NUM', 0, 'comp_dtl_state_taxes_tt_total', 'TEXT',
    'Fed Benefit of State Tax Deduction', 'T', 'format_003_state_taxes_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 14, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 3, 0,
    'COL_VALUE_NUM', 0, 'comp_dtl_fed_deduct_tt_total', 'TEXT', 'State Tax Expense', 'T',
    'format_003_state_taxes_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 15, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 2, 1, 1,
    'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_description', 'T', 'format_011_subtotal_visible ',
    null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 16, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 2, 1, 0,
    'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_description', 'T',
    'format_001_rollup_description_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 17, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 2, 2, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', 'format_011_subtotal_visible ', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 18, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 1, 1,
    'TEXT', 0, null, 'TEXT', 'Taxes Before Adjustments', 'T', 'info_report_section_is_10', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 19, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 2, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', 'info_report_section_is_10', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 20, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 3, 1,
    'COL_VALUE_NUM', 0, 'comp_dtl_tte_b4_discretes_tt_total', 'TEXT',
    'Total Tax Expense (Benefit) Before Discrete Items', 'T', 'info_report_section_is_40', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 21, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 1, 0,
    'COL_VALUE_NUM', 0, 'curtax_not_expense_fed_total', 'TEXT', 'Current Tax Not Booked to Expense',
    'T', 'show_curtax_not_expense_cons', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 22, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 2, 1,
    'COL_VALUE_TRANS', 0, 'comp_dtl_tte_after_discretes_tt + comp_dtl_tte_after_discretes_tt_adj',
    'TEXT', 'Total Tax Expense (Benefit) With Discrete Items', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 23, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 3, 0,
    'COL_VALUE_NUM', 0, 'total_tax_expense', 'TEXT', 'Tax Expense Booked', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 24, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 4, 0,
    'COL_VALUE_TRANS', 0,
    'comp_dtl_tte_after_discretes_tt + comp_dtl_tte_after_discretes_tt_adj - total_tax_expense',
    'TEXT', 'Difference', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 25, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 5, 0,
    'COL_VALUE_NUM', 0, 'total_co_etr', 'TEXT', 'Total ETR Percentage', 'P', null,
    'all_co_etr_total');
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (32, 26, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 6, 0,
    'COL_VALUE_NUM', 0, 'all_co_etr', 'TEXT', 'Consolidated ETR Percentage', 'P', null,
    'all_co_etr_total');
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 1, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 1, 1,
    'TEXT', 1, 'Tax Footnote ETR Report', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 2, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 2, 1,
    'USER_TITLE', 0, null, null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 3, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 3, 1,
    'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 4, TO_TIMESTAMP('19-09-2012 12:03:12', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 4, 1,
    'COL_VALUE_TEXT', 0, 'consol_desc', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 5, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 5, 1,
    'COL_VALUE_TEXT', 0, 'oper_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 6, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 7, 1,
    'COL_VALUE_TEXT', 0, 'month_descr', null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 7, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', -1, 8, 1,
    'FILLER', 0, null, null, null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 8, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 1, 0,
    'COL_VALUE_NUM', 0, 'ptbi', 'TEXT', 'Book Income', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 9, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 2, 0,
    'COL_VALUE_NUM', 0, 'comp_h1_tax_items_total', 'TEXT', 'Tax Items', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 10, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 3, 0,
    'COL_VALUE_NUM', 0, 'comp_h1_bibit_total', 'TEXT',
    'Book Income Before Income Taxes (Adjusted for Tax Items)', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 11, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 0, 4, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 12, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 1, 0,
    'COL_VALUE_NUM', 0, 'comp_dtl_tt_total', 'COL_VALUE_TEXT', 'detail_description', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 13, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 2, 0,
    'COL_VALUE_NUM', 0, 'comp_dtl_state_taxes_tt_total', 'TEXT',
    'Fed Benefit of State Tax Deduction', 'T', 'format_003_state_taxes_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 14, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'D', 1, 3, 0,
    'COL_VALUE_NUM', 0, 'comp_dtl_fed_deduct_tt_total', 'TEXT', 'State Tax Expense', 'T',
    'format_003_state_taxes_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 16, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'H', 2, 1, 0,
    'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_description', 'T',
    'format_001_rollup_description_visible', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 15, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 2, 1, 1,
    'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_description', 'T', 'format_011_subtotal_visible ',
    null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 17, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 2, 2, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', 'format_011_subtotal_visible ', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 18, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 1, 1,
    'TEXT', 0, null, 'TEXT', 'Taxes Before Adjustments', 'T', 'info_report_section_is_10', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 19, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 2, 0,
    'TEXT', 0, null, 'FILLER', null, 'T', 'info_report_section_is_10', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 20, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 3, 3, 1,
    'COL_VALUE_NUM', 0, 'comp_dtl_tte_b4_discretes_tt_total', 'TEXT',
    'Total Tax Expense (Benefit) Before Discrete Items', 'T', 'info_report_section_is_40', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 21, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 1, 0,
    'COL_VALUE_NUM', 0, 'curtax_not_expense_fed_total', 'TEXT', 'Current Tax Not Booked to Expense',
    'T', 'show_curtax_not_expense_cons', null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 22, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 2, 1,
    'COL_VALUE_TRANS', 0, 'comp_dtl_tte_after_discretes_tt + comp_dtl_tte_after_discretes_tt_adj',
    'TEXT', 'Total Tax Expense (Benefit) With Discrete Items', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 23, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 3, 0,
    'COL_VALUE_NUM', 0, 'total_tax_expense', 'TEXT', 'Tax Expense Booked', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 24, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 4, 0,
    'COL_VALUE_TRANS', 0, 'comp_dtl_tte_after_discretes_tt + comp_dtl_tte_after_discretes_tt_adj - total_tax_expense',
    'TEXT', 'Difference', 'T', null, null);
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 25, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 5, 0,
    'COL_VALUE_NUM', 0, 'total_co_etr', 'TEXT', 'Total ETR Percentage', 'P', null,
    'all_co_etr_total');
insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, TIME_STAMP, USER_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE,
    IS_REPORT_TITLE, ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD,
    TOTAL_COL_FIELDNAME)
values
   (33, 26, TO_TIMESTAMP('19-09-2012 13:58:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'F', 99, 6, 0,
    'COL_VALUE_NUM', 0, 'all_co_etr', 'TEXT', 'Consolidated ETR Percentage', 'P', null,
    'all_co_etr_total');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (249, 0, 10, 4, 0, 0, 8322, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_008322_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;