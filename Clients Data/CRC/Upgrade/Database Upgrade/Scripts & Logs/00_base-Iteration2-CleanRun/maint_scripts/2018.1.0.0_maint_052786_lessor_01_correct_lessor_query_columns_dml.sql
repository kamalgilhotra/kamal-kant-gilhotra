 /*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052786_lessor_01_correct_lessor_query_columns_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 12/3/2018  Alex Healey    Alter Lessor ILR by SOB to delete missed remeasurement fields
||============================================================================
*/
 
 delete from pp_any_query_criteria_fields
 where lower(detail_field) = ('unguaran_resid_remeasure')
  AND id = (SELECT id FROM pp_any_query_criteria
            WHERE subsystem = 'lessor'
            and description = 'Lessor ILR by Set of Books (Current Revision)');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12564, 0, 2018, 1, 0, 0, 52786, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052786_lessor_01_correct_lessor_query_columns_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;