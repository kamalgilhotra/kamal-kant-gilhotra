/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041990_ds_import_dml_6.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   01/19/2015 A Scott          Import tool setup for depr studies.
||                                      Changes made based on QA, including
||                                      depr group validations, acct backfills.
||============================================================================
*/

update pp_import_type
set archive_additional_columns = 'depr_group_id, depr_group_xlate, subledger_type_id, mid_period_conv, mid_period_method, default_gl_acct_id'
where import_type_id in (453);

update pp_import_column
set is_required = 1
where import_type_id = 453
and lower(column_name) = 'description';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2206, 0, 2015, 1, 0, 0, 41990, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041990_ds_import_dml_6.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;