/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042645_depr_add_tables_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.3.3   02/18/2015 Daniel Motter    Create budget closings tables
||============================================================================
*/

--Create and prepare budget_closings_temp
CREATE GLOBAL TEMPORARY TABLE budget_closings_temp (
	id				NUMBER(22,0),
	work_order_id			NUMBER(22,0),
	revision			NUMBER(22,0),
	month_number			NUMBER(22,0),
	expenditure_type_id		NUMBER(22,0),
	actuals_month_number		NUMBER(22,0),
	company_id			NUMBER(22,0),
	bus_segment_id			NUMBER(22,0),
	major_location_id		NUMBER(22,0),
	job_task_id			VARCHAR2(35),
	book_summary_id			NUMBER(22,0),
	vintage				NUMBER(22,0),
	budget_plant_class_id		NUMBER(22,0),
	beg_cwip			NUMBER(22,2),
	current_month_amount		NUMBER(22,2),
	current_month_closings		NUMBER(22,2),
	end_cwip			NUMBER(22,2),
	CLOSINGS_PCT			NUMBER(22,8),
	CLOSING_TYPE			VARCHAR2(1),
	USER_ID				VARCHAR2(18),
	TIME_STAMP			DATE,
	FCST_DEPR_VERSION_ID		NUMBER(22,0),
	IN_SERVICE_DATE			DATE,
	CLOSING_PATTERN_ID		NUMBER(22,0),
	CLOSING_OPTION_ID		NUMBER(22,0)
) ON COMMIT PRESERVE ROWS;

ALTER TABLE budget_closings_temp 
ADD (CONSTRAINT pk_budget_closings_temp PRIMARY KEY (id));
							 
CREATE UNIQUE INDEX "IX_BUDGET_CLOSINGS_TEMP" ON PWRPLANT."BUDGET_CLOSINGS_TEMP" 
("WORK_ORDER_ID", "REVISION", "MONTH_NUMBER",
"EXPENDITURE_TYPE_ID", "COMPANY_ID", "BUS_SEGMENT_ID", "MAJOR_LOCATION_ID",
"JOB_TASK_ID", "BOOK_SUMMARY_ID", "VINTAGE", "BUDGET_PLANT_CLASS_ID",
"CLOSING_TYPE", "FCST_DEPR_VERSION_ID", "IN_SERVICE_DATE", "CLOSING_PATTERN_ID", "CLOSING_OPTION_ID");
								 
CREATE OR REPLACE PUBLIC SYNONYM budget_closings_temp FOR pwrplant.budget_closings_temp;

GRANT ALL ON budget_closings_temp TO pwrplant_role_dev;

--Create and prepare budget_closings
CREATE TABLE budget_closings (
	id				NUMBER(22,0),
	work_order_id			NUMBER(22,0),
	revision			NUMBER(22,0),
	month_number			NUMBER(22,0),
	expenditure_type_id		NUMBER(22,0),
	actuals_month_number		NUMBER(22,0),
	company_id			NUMBER(22,0),
	bus_segment_id			NUMBER(22,0),
	major_location_id		NUMBER(22,0),
	job_task_id			VARCHAR2(35),
	book_summary_id			NUMBER(22,0),
	vintage				NUMBER(22,0),	
	budget_plant_class_id		NUMBER(22,0),
	beg_cwip			NUMBER(22,2),
	current_month_amount		NUMBER(22,2),
	current_month_closings		NUMBER(22,2),
	end_cwip			NUMBER(22,2),
	closings_pct			NUMBER(22,8),
	closing_type			VARCHAR2(1),
	user_id				VARCHAR2(18),
	time_stamp			DATE,
	fcst_depr_version_id		NUMBER(22,0),
	in_service_date			DATE,
	closing_pattern_id		NUMBER(22,0),
	closing_option_id		NUMBER(22,0)
);

ALTER TABLE pwrplant.budget_closings 
ADD (CONSTRAINT pk_budget_closings PRIMARY KEY (id) USING INDEX tablespace pwrplant_idx);

ALTER TABLE pwrplant.budget_closings 
ADD (CONSTRAINT R_budget_closings1 FOREIGN KEY (fcst_depr_version_id) REFERENCES PWRPLANT.fcst_depr_version);

CREATE UNIQUE INDEX ix_budget_closings ON pwrplant.budget_closings 
(work_order_id, revision, month_number, expenditure_type_id, company_id, bus_segment_id, major_location_id,
job_task_id,book_summary_id,vintage, budget_plant_class_id, closing_type, fcst_depr_version_id, in_service_date,
closing_pattern_id, closing_option_id) tablespace pwrplant_idx;
								 
CREATE OR REPLACE PUBLIC SYNONYM budget_closings FOR pwrplant.budget_closings;

GRANT ALL ON budget_closings TO pwrplant_role_dev;

--Create and prepare budget_closings_values
CREATE TABLE budget_closings_values(
	"VALUE"				VARCHAR2(35),
	sql				VARCHAR2(4000),
	time_stamp			DATE,
	user_id				VARCHAR2(18)
);

ALTER TABLE budget_closings_values ADD (CONSTRAINT pk_budget_closings_values PRIMARY KEY ("VALUE") USING INDEX tablespace pwrplant_idx);

GRANT ALL ON budget_closings_values TO pwrplant_role_dev;

CREATE OR REPLACE PUBLIC SYNONYM budget_closings_values FOR pwrplant.budget_closings_values;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2317, 0, 10, 4, 3, 3, 42645, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.3_maint_042645_depr_add_tables_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;