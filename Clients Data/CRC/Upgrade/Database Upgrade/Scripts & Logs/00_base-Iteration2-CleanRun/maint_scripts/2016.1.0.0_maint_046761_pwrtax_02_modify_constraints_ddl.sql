/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046761_pwrtax_02_modify_constraints_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 10/31/2016 Charlie Shilling add constraint so that tax_plant_recon_form_rates
||											records must be in tax_plant_recon_form_jur too
||============================================================================
*/
--Create FK to tax_plant_recon_form_jur
ALTER TABLE tax_plant_recon_form_rates
  ADD CONSTRAINT tax_plant_recon_form_rates_fk6 FOREIGN KEY (
    tax_pr_form_id,
	jurisdiction_id
  ) REFERENCES tax_plant_recon_form_jur (
  	tax_pr_form_id,
    jurisdiction_id
  )
;

--Remove unnecessary constraints:
--  tax_plant_recon_form_jur has FKs to tax_plant_recon_form and jurisdiction already,
--  so the new constraint above takes care of these.

--constraint from tax_plant_recon_form_rates to jurisdiction
ALTER TABLE tax_plant_recon_form_rates
DROP CONSTRAINT tax_plant_recon_form_rates_fk;


--constraint from tax_plant_recon_form_rates to tax_plant_recon_form
ALTER TABLE tax_plant_recon_form_rates
DROP CONSTRAINT tax_plant_recon_form_rates_fk1;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3335, 0, 2016, 1, 0, 0, 046761, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046761_pwrtax_02_modify_constraints_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;