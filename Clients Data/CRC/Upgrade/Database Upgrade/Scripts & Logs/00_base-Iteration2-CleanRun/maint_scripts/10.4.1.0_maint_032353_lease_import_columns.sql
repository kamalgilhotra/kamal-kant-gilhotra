/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032353_lease_import_columns.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/12/2013 Ryan Oliveria  Point Release
||============================================================================
*/


alter table LS_IMPORT_ASSET         modify LONG_DESCRIPTION varchar2(254);
alter table LS_IMPORT_ASSET_ARCHIVE modify LONG_DESCRIPTION varchar2(254);

update PP_IMPORT_COLUMN
   set COLUMN_TYPE = 'varchar2(254)'
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_asset')
   and COLUMN_NAME = 'long_description';

alter table LS_IMPORT_LEASE add LEASE_TYPE_ID_ARC varchar2(254);

update LS_IMPORT_LEASE set LEASE_TYPE_ID_ARC = LEASE_TYPE_ID;

update LS_IMPORT_LEASE set LEASE_TYPE_ID = null;

alter table LS_IMPORT_LEASE modify LEASE_TYPE_ID number(22,0);

update LS_IMPORT_LEASE set LEASE_TYPE_ID = LEASE_TYPE_ID_ARC;

alter table LS_IMPORT_LEASE drop column LEASE_TYPE_ID_ARC;

alter table LS_IMPORT_LEASE_ARCHIVE add LEASE_TYPE_ID_ARC varchar2(254);

update LS_IMPORT_LEASE_ARCHIVE set LEASE_TYPE_ID_ARC = LEASE_TYPE_ID;

update LS_IMPORT_LEASE_ARCHIVE set LEASE_TYPE_ID = null;

alter table LS_IMPORT_LEASE_ARCHIVE modify LEASE_TYPE_ID number(22,0);

update LS_IMPORT_LEASE_ARCHIVE set LEASE_TYPE_ID = LEASE_TYPE_ID_ARC;

alter table LS_IMPORT_LEASE_ARCHIVE drop column LEASE_TYPE_ID_ARC;

update PP_IMPORT_COLUMN
   set COLUMN_TYPE = 'number(22,0)'
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_lease')
   and COLUMN_NAME = 'lease_type_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (610, 0, 10, 4, 1, 0, 32353, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032353_lease_import_columns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;