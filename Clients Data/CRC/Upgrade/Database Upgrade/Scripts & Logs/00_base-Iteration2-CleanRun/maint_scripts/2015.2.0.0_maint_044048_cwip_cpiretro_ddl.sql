/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044048_cwip_cpiretro_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   06/26/2015 Sunjin COne     Creation
||============================================================================
*/

CREATE TABLE CPI_RETRO_HIST_ADJUST 
( 
COMPANY_ID NUMBER(22,0) NOT NULL, 
WORK_ORDER_ID NUMBER(22,0) NOT NULL, 
END_MONTH_NUMBER NUMBER(22,0) NOT NULL, 
AMOUNT NUMBER(22,2) NOT NULL,  
IN_SERVICE_DATE DATE, 
WORK_ORDER_NUMBER VARCHAR2(35), 
LAST_CALC_MONTH_NUMBER NUMBER(22,0), 
MONTH_NUMBER NUMBER(22,0), 
CWIP_CHARGE_FOUND NUMBER(22,0), 
GL_ACCOUNT_ID NUMBER(22,0),
CLOSED_MONTH_NUMBER NUMBER(22,0), 
UNIT_CLOSED_MONTH_NUMBER NUMBER(22,0), 
ERROR_MSG VARCHAR2(254), 
NEW_CHARGE_ID NUMBER(22,0),  
BATCH_ID NUMBER(22,0) 
) ;

ALTER TABLE CPI_RETRO_HIST_ADJUST 
add constraint CPI_RETRO_HIST_ADJUST_PK
primary key (COMPANY_ID,  WORK_ORDER_ID)
using index tablespace PWRPLANT_IDX; 

comment on table   CPI_RETRO_HIST_ADJUST is '(C) [04] A staging table of conversion CPI dollars by company and work order for a particular given month.';
comment on column CPI_RETRO_HIST_ADJUST.COMPANY_ID is 'Identifier of the company.';
comment on column CPI_RETRO_HIST_ADJUST.WORK_ORDER_ID is 'Identifier of the work order number.';
comment on column CPI_RETRO_HIST_ADJUST.END_MONTH_NUMBER is 'Given month for the CPI dollars to be inserted.';
comment on column CPI_RETRO_HIST_ADJUST.AMOUNT is 'Given amount for the CPI dollars to be inserted.';
comment on column CPI_RETRO_HIST_ADJUST.IN_SERVICE_DATE is 'This column is determined by the CPI Retro Calc functionality based on a search on WORK_ORDER_CONTROL.';
comment on column CPI_RETRO_HIST_ADJUST.WORK_ORDER_NUMBER is 'This column is determined by the CPI Retro Calc functionality based on a search on WORK_ORDER_CONTROL.';
comment on column CPI_RETRO_HIST_ADJUST.LAST_CALC_MONTH_NUMBER is 'This column is determined by the CPI Retro Calc functionality based on a search on AFUDC_CALC.';
comment on column CPI_RETRO_HIST_ADJUST.MONTH_NUMBER is  'This column is determined by the CPI Retro Calc functionality to use for the CWIP_CHARGE insert.';
comment on column CPI_RETRO_HIST_ADJUST.CWIP_CHARGE_FOUND is 'This column is determined by the CPI Retro Calc functionality based on a search on CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST.GL_ACCOUNT_ID is 'This column is determined by the CPI Retro Calc functionality based on a search on CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST.CLOSED_MONTH_NUMBER is 'This column is determined by the CPI Retro Calc functionality based on a search on CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST.UNIT_CLOSED_MONTH_NUMBER is 'This column is determined by the CPI Retro Calc functionality based on a search on CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST.ERROR_MSG is 'This column is determined by the CPI Retro Calc functionality for validation errors.';
comment on column CPI_RETRO_HIST_ADJUST.NEW_CHARGE_ID is 'This column is determined by the CPI Retro Calc functionality for the charge_id to use for the insert into CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST.BATCH_ID is 'This column is determined by the CPI Retro Calc functionality for the batch_id used to process the CPI conversion data.';

CREATE TABLE CPI_RETRO_HIST_ADJUST_ARC 
( 
COMPANY_ID NUMBER(22,0) NOT NULL, 
WORK_ORDER_ID NUMBER(22,0) NOT NULL, 
END_MONTH_NUMBER NUMBER(22,0) NOT NULL,   
AMOUNT NUMBER(22,2) NOT NULL,  
IN_SERVICE_DATE DATE, 
WORK_ORDER_NUMBER VARCHAR2(35), 
LAST_CALC_MONTH_NUMBER NUMBER(22,0), 
CWIP_CHARGE_FOUND NUMBER(22,0), 
GL_ACCOUNT_ID NUMBER(22,0),
CLOSED_MONTH_NUMBER NUMBER(22,0), 
UNIT_CLOSED_MONTH_NUMBER NUMBER(22,0), 
MONTH_NUMBER NUMBER(22,0),
ERROR_MSG VARCHAR2(254),  
NEW_CHARGE_ID NUMBER(22,0),  
RUN_TIME_STRING VARCHAR2(35),
BATCH_ID NUMBER(22,0) 
) ; 

comment on table   CPI_RETRO_HIST_ADJUST_ARC is '(C) [04] An archive table of posted conversion CPI dollars from CPI_RETRO_HIST_ADJUST table.  No primary key.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.COMPANY_ID is 'Identifier of the company.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.WORK_ORDER_ID is 'Identifier of the work order number.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.END_MONTH_NUMBER is 'Given month for the CPI dollars to be inserted.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.AMOUNT is 'Given amount for the CPI dollars to be inserted.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.IN_SERVICE_DATE is 'This column is determined by the CPI Retro Calc functionality based on a search on WORK_ORDER_CONTROL.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.WORK_ORDER_NUMBER is 'This column is determined by the CPI Retro Calc functionality based on a search on WORK_ORDER_CONTROL.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.LAST_CALC_MONTH_NUMBER is 'This column is determined by the CPI Retro Calc functionality based on a search on AFUDC_CALC.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.MONTH_NUMBER is  'This column is determined by the CPI Retro Calc functionality to use for the CWIP_CHARGE insert.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.CWIP_CHARGE_FOUND is 'This column is determined by the CPI Retro Calc functionality based on a search on CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.GL_ACCOUNT_ID is 'This column is determined by the CPI Retro Calc functionality based on a search on CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.CLOSED_MONTH_NUMBER is 'This column is determined by the CPI Retro Calc functionality based on a search on CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.UNIT_CLOSED_MONTH_NUMBER is 'This column is determined by the CPI Retro Calc functionality based on a search on CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.ERROR_MSG is 'This column is determined by the CPI Retro Calc functionality for validation errors.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.NEW_CHARGE_ID is 'This column is determined by the CPI Retro Calc functionality for the charge_id to use for the insert into CWIP_CHARGE.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.RUN_TIME_STRING is 'This column is determined by the CPI Retro Calc functionality for the date and time of process of the CPI conversion data.';
comment on column CPI_RETRO_HIST_ADJUST_ARC.BATCH_ID is 'This column is determined by the CPI Retro Calc functionality for the batch_id used to process the CPI conversion data.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2658, 0, 2015, 2, 0, 0, 044048, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044048_cwip_cpiretro_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;