SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029520_proj.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/19/2013 Joseph King    Point Release
||============================================================================
*/

insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE)
   select 'Load Monthly Estimates',
          'Company Id (-1 = all companies)',
          'Replace Estimates (true / false)',
          'Batch / Instance',
          'Replace Year (true / false)',
          'Funding WO Indicator'
     from DUAL
    where not exists (select 1
             from PP_INTEGRATION_COMPONENTS
            where UPPER(COMPONENT) = UPPER('Load Monthly Estimates'));


begin
   execute immediate ('rename WO_INTERFACE_MONTHLY to WO_INTERFACE_MONTHLY_TEMP');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('Table WO_INTERFACE_MONTHLY_TEMP already exists.');
      else
         raise;
      end if;
end;
/


begin
   begin
      execute immediate ('rename WO_INTERFACE_MONTHLY_ARC to D1_WO_INTERFACE_MONTHLY_ARC');
   exception
      when others then
         if (sqlcode = -955) then
            DBMS_OUTPUT.PUT_LINE('Table D1_WO_INTERFACE_MONTHLY_ARC already exists.');
         else
            raise;
         end if;
   end;

   begin
      execute immediate ('drop trigger WO_INTERFACE_MONTHLY_ARC');
   exception
      when others then
         if (sqlcode = -4080) then
            DBMS_OUTPUT.PUT_LINE('Trigger WO_INTERFACE_MONTHLY_ARC did not exist.');
         else
            raise;
         end if;
   end;
end;
/


begin
   execute immediate ('create table WO_INTERFACE_MONTHLY
                       (
                        ROW_ID                   number(22,0),
                        TIME_STAMP               date,
                        USER_ID                  varchar2(35),
                        WORK_ORDER_NUMBER        varchar2(35),
                        EXT_COMPANY              varchar2(35),
                        EXT_EXPENDITURE_TYPE     varchar2(254),
                        EXT_EST_CHG_TYPE         varchar2(254),
                        EXT_DEPARTMENT           varchar2(254),
                        EXT_JOB_TASK             varchar2(254),
                        EXT_UTILITY_ACCOUNT      varchar2(254),
                        LONG_DESCRIPTION         varchar2(254),
                        EXT_WO_WORK_ORDER_NUMBER varchar2(254),
                        YEAR                     number(22,0) not null,
                        JANUARY                  number(22,2),
                        FEBRUARY                 number(22,2),
                        MARCH                    number(22,2),
                        APRIL                    number(22,2),
                        MAY                      number(22,2),
                        JUNE                     number(22,2),
                        JULY                     number(22,2),
                        AUGUST                   number(22,2),
                        SEPTEMBER                number(22,2),
                        OCTOBER                  number(22,2),
                        NOVEMBER                 number(22,2),
                        DECEMBER                 number(22,2),
                        TOTAL                    number(22,2),
                        HRS_JAN                  number(22,4),
                        HRS_FEB                  number(22,4),
                        HRS_MAR                  number(22,4),
                        HRS_APR                  number(22,4),
                        HRS_MAY                  number(22,4),
                        HRS_JUN                  number(22,4),
                        HRS_JUL                  number(22,4),
                        HRS_AUG                  number(22,4),
                        HRS_SEP                  number(22,4),
                        HRS_OCT                  number(22,4),
                        HRS_NOV                  number(22,4),
                        HRS_DEC                  number(22,4),
                        HRS_TOTAL                number(22,4),
                        QTY_JAN                  number(22,4),
                        QTY_FEB                  number(22,4),
                        QTY_MAR                  number(22,4),
                        QTY_APR                  number(22,4),
                        QTY_MAY                  number(22,4),
                        QTY_JUN                  number(22,4),
                        QTY_JUL                  number(22,4),
                        QTY_AUG                  number(22,4),
                        QTY_SEP                  number(22,4),
                        QTY_OCT                  number(22,4),
                        QTY_NOV                  number(22,4),
                        QTY_DEC                  number(22,4),
                        QTY_TOTAL                number(22,4),
                        EXT_BUDGET_VERSION       varchar2(35),
                        WORK_ORDER_ID            number(22,0),
                        COMPANY_ID               number(22,0),
                        EXPENDITURE_TYPE_ID      number(22,0),
                        EST_CHG_TYPE_ID          number(22,0),
                        DEPARTMENT_ID            number(22,0),
                        JOB_TASK_ID              varchar2(35),
                        UTILITY_ACCOUNT_ID       number(22,0),
                        WO_WORK_ORDER_ID         number(22,0),
                        BUDGET_VERSION_ID        number(22,0),
                        EST_MONTHLY_ID           number(22,0),
                        BATCH_ID                 varchar2(35),
                        FUNDING_WO_INDICATOR     number(22,0),
                        SEQ_ID                   number(22,0),
                        PROCESS_LEVEL            varchar2(35),
                        ERROR_MESSAGE            clob,
                        STATUS                   number(22,0),
                        OLD_REVISION             number(22,0),
                        NEW_REVISION             number(22,0),
                        REVISION                 number(22,0)
                       )');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('Table WO_INTERFACE_MONTHLY already exists.');
      else
         raise;
      end if;
end;
/


begin
   execute immediate ('alter table WO_INTERFACE_MONTHLY
                          add constraint WO_INTERFACE_MONTHLY_PK
                              primary key (ROW_ID)
                              using index tablespace PWRPLANT_IDX');
exception
   when others then
      if (sqlcode = -955 or sqlcode = -2260) then
         DBMS_OUTPUT.PUT_LINE('PK on table WO_INTERFACE_MONTHLY already exists.');
      else
         raise;
      end if;
end;
/


begin
   execute immediate ('create index WO_INTERFACE_MONTHLY_BID_IX
                          on WO_INTERFACE_MONTHLY (BATCH_ID)
                             tablespace PWRPLANT_IDX');
   DBMS_OUTPUT.PUT_LINE('Index WO_INTERFACE_MONTHLY_BID_IX created.');

   execute immediate ('create index WO_INTERFACE_MONTHLY_STATUS_IX
                          on WO_INTERFACE_MONTHLY (STATUS)
                             tablespace PWRPLANT_IDX');
   DBMS_OUTPUT.PUT_LINE('Index WO_INTERFACE_MONTHLY_STATUS_IX created.');

   execute immediate ('create index WO_INTERFACE_MONTHLY_WOID_IX
                          on WO_INTERFACE_MONTHLY (WORK_ORDER_ID)
                             tablespace PWRPLANT_IDX');
   DBMS_OUTPUT.PUT_LINE('Index WO_INTERFACE_MONTHLY_WOID_IX created.');
exception
   when others then
      if (sqlcode = -955 or sqlcode = -2260) then
         DBMS_OUTPUT.PUT_LINE('Indexes on table WO_INTERFACE_MONTHLY already exists.');
      else
         raise;
      end if;
end;
/


begin
   execute immediate ('create sequence WO_INTERFACE_MONTHLY_SEQ start with 1 increment by 1 cache 20');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('Sequence WO_INTERFACE_MONTHLY_SEQ already exists.');
      else
         raise;
      end if;
end;
/


create or replace trigger WO_INTERFACE_MONTHLY_PK
   before insert on WO_INTERFACE_MONTHLY
   for each row
declare
   L_NEW_SEQUENCE number;
begin
   select WO_INTERFACE_MONTHLY_SEQ.NEXTVAL into L_NEW_SEQUENCE from DUAL;
   :NEW.ROW_ID := L_NEW_SEQUENCE;
end;
/


begin
   execute immediate ('create table WO_INTERFACE_MONTHLY_ARC
                       (
                        ROW_ID                   number(22,0),
                        TIME_STAMP               date,
                        USER_ID                  varchar2(35),
                        WORK_ORDER_NUMBER        varchar2(35),
                        EXT_COMPANY              varchar2(35),
                        EXT_EXPENDITURE_TYPE     varchar2(254),
                        EXT_EST_CHG_TYPE         varchar2(254),
                        EXT_DEPARTMENT           varchar2(254),
                        EXT_JOB_TASK             varchar2(254),
                        EXT_UTILITY_ACCOUNT      varchar2(254),
                        LONG_DESCRIPTION         varchar2(254),
                        EXT_WO_WORK_ORDER_NUMBER varchar2(254),
                        YEAR                     number(22,0),
                        JANUARY                  number(22,0),
                        FEBRUARY                 number(22,0),
                        MARCH                    number(22,0),
                        APRIL                    number(22,0),
                        MAY                      number(22,0),
                        JUNE                     number(22,0),
                        JULY                     number(22,0),
                        AUGUST                   number(22,0),
                        SEPTEMBER                number(22,0),
                        OCTOBER                  number(22,0),
                        NOVEMBER                 number(22,0),
                        DECEMBER                 number(22,0),
                        TOTAL                    number(22,0),
                        HRS_JAN                  number(22,4),
                        HRS_FEB                  number(22,4),
                        HRS_MAR                  number(22,4),
                        HRS_APR                  number(22,4),
                        HRS_MAY                  number(22,4),
                        HRS_JUN                  number(22,4),
                        HRS_JUL                  number(22,4),
                        HRS_AUG                  number(22,4),
                        HRS_SEP                  number(22,4),
                        HRS_OCT                  number(22,4),
                        HRS_NOV                  number(22,4),
                        HRS_DEC                  number(22,4),
                        HRS_TOTAL                number(22,4),
                        QTY_JAN                  number(22,4),
                        QTY_FEB                  number(22,4),
                        QTY_MAR                  number(22,4),
                        QTY_APR                  number(22,4),
                        QTY_MAY                  number(22,4),
                        QTY_JUN                  number(22,4),
                        QTY_JUL                  number(22,4),
                        QTY_AUG                  number(22,4),
                        QTY_SEP                  number(22,4),
                        QTY_OCT                  number(22,4),
                        QTY_NOV                  number(22,4),
                        QTY_DEC                  number(22,4),
                        QTY_TOTAL                number(22,4),
                        EXT_BUDGET_VERSION       varchar2(35),
                        WORK_ORDER_ID            number(22,0),
                        COMPANY_ID               number(22,0),
                        EXPENDITURE_TYPE_ID      number(22,0),
                        EST_CHG_TYPE_ID          number(22,0),
                        DEPARTMENT_ID            number(22,0),
                        JOB_TASK_ID              varchar2(35),
                        UTILITY_ACCOUNT_ID       number(22,0),
                        WO_WORK_ORDER_ID         number(22,0),
                        BUDGET_VERSION_ID        number(22,0),
                        EST_MONTHLY_ID           number(22,0),
                        BATCH_ID                 varchar2(35),
                        FUNDING_WO_INDICATOR     number(22,0),
                        SEQ_ID                   number(22,0),
                        PROCESS_LEVEL            varchar2(35),
                        ERROR_MESSAGE            clob,
                        STATUS                   number(22,0),
                        OLD_REVISION             number(22,0),
                        NEW_REVISION             number(22,0),
                        REVISION                 number(22,0)
                       )');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('Table WO_INTERFACE_MONTHLY_ARC already exists.');
      else
         raise;
      end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (328, 0, 10, 4, 1, 0, 29520, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029520_proj.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;