/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043351_cap_bud_dyn_valid_bi_appr_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     03/23/2015 Chris Mardis   Point Release
||============================================================================
*/
insert into wo_validation_type (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, FUNCTION, TIME_STAMP, USER_ID, FIND_COMPANY, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10, COL11, COL12, COL13, COL14, COL15, COL16, COL17, COL18, COL19, COL20, HARD_EDIT)
values (71, 'BI Approve (PRE)', 'BI Approve (PRE)', 'f_workflow_base', to_date('23-03-2015 15:26:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'select company_id from budget where budget_id = <arg1>', 'budget_id', 'approval_number', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1);

insert into wo_validation_type (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, FUNCTION, TIME_STAMP, USER_ID, FIND_COMPANY, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10, COL11, COL12, COL13, COL14, COL15, COL16, COL17, COL18, COL19, COL20, HARD_EDIT)
values (72, 'BI Approve (POST)', 'BI Approve (POST)', 'f_workflow_base', to_date('23-03-2015 15:26:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'select company_id from budget where budget_id = <arg1>', 'budget_id', 'approval_number', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1);

insert into wo_validation_type (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, FUNCTION, TIME_STAMP, USER_ID, FIND_COMPANY, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10, COL11, COL12, COL13, COL14, COL15, COL16, COL17, COL18, COL19, COL20, HARD_EDIT)
values (74, 'BI Reject (PRE)', 'BI Reject (PRE)', 'f_workflow_base', to_date('23-03-2015 15:26:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'select company_id from budget where budget_id = <arg1>', 'budget_id', 'approval_number', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1);

insert into wo_validation_type (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, FUNCTION, TIME_STAMP, USER_ID, FIND_COMPANY, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10, COL11, COL12, COL13, COL14, COL15, COL16, COL17, COL18, COL19, COL20, HARD_EDIT)
values (75, 'BI Reject (POST)', 'BI Reject (POST)', 'f_workflow_base', to_date('23-03-2015 15:26:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'select company_id from budget where budget_id = <arg1>', 'budget_id', 'approval_number', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2427, 0, 2015, 1, 0, 0, 43351, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043351_cap_bud_dyn_valid_bi_appr_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;