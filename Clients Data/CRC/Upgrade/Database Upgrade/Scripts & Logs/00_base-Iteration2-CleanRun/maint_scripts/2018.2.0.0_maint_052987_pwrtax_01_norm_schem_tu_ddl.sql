/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052987_pwrtax_01_norm_schem_tu_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- -----------------------------------
|| 2018.2.0.0 03/13/2019  K. Powers	   PowerTax Normalization Schema True Up
||============================================================================
*/

--------------------------
--Remove if existing table
--------------------------
DECLARE
  table_exists NUMBER(22);
BEGIN
  SELECT Count(1)
  INTO table_exists
  FROM tab
  WHERE tname LIKE 'NORMALIZATION_SCHEMA_TRUEUP';
  IF table_exists>0 THEN
     EXECUTE IMMEDIATE 'DROP TABLE NORMALIZATION_SCHEMA_TRUEUP';
  END IF;
END;
/

CREATE TABLE NORMALIZATION_SCHEMA_TRUEUP
  (
    normalization_id  NUMBER(22,0) NOT NULL,
    tax_year          NUMBER(22,2) NOT NULL,
    time_stamp        DATE,
    user_id           VARCHAR2(18),
    company_id        NUMBER(22,0) NOT NULL,
    tax_class_id      NUMBER(22,0) NOT NULL,
    trueup_Adjustment NUMBER(1,0)
  ) ;
  
ALTER TABLE NORMALIZATION_SCHEMA_TRUEUP ADD CONSTRAINT PK_NORMALIZATION_SCHEMA_TRUEUP PRIMARY KEY
(
  normalization_id, tax_year, company_id, tax_class_id
)
USING INDEX TABLESPACE PWRPLANT_IDX;

COMMENT ON TABLE NORMALIZATION_SCHEMA_TRUEUP IS '(S) [09] NORMALIZATION_SCHEMA_TRUEUP contains the normalization schemas by tax year that should true up to the statutory rate on that schema.';

COMMENT ON column NORMALIZATION_SCHEMA_TRUEUP.normalization_id IS  'The normalization schema that should be configurated at statutory.';
COMMENT ON column NORMALIZATION_SCHEMA_TRUEUP.tax_year IS 'The tax years that the true up should occur.';
COMMENT ON column NORMALIZATION_SCHEMA_TRUEUP.company_id IS 'The company ID where the true up should occur.';
COMMENT ON column NORMALIZATION_SCHEMA_TRUEUP.tax_class_id IS 'The tax class ID(s) that the true up should occur for.';
COMMENT ON column NORMALIZATION_SCHEMA_TRUEUP.trueup_adjustment IS 'A flag indicating whether the true up occurs as a deferred tax adjustment or as a provision/reversal, 0= provision/reversal, 1 = deferred tax adjustment';
COMMENT ON column NORMALIZATION_SCHEMA_TRUEUP.TIME_STAMP IS 'Standard system assigned timestamp used for audit purposes.';
COMMENT ON column NORMALIZATION_SCHEMA_TRUEUP.USER_ID IS 'Standard system-assigned user id used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14783, 0, 2018, 2, 0, 0, 52987, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052987_pwrtax_01_norm_schem_tu_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
