/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_046808_reg_add_stat_group_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2016.1.1.0 01/09/2017 Charlie Shilling  Add tables to support statistic group functionality
||============================================================================
*/

--Normal statistics stuff
CREATE TABLE reg_statistic_group (
	reg_stat_group_id		NUMBER(22,0) 	NOT NULL,
	description				VARCHAR2(254) 	NOT NULL,
	long_description		VARCHAR2(2000) 	NULL,
	user_id					VARCHAR2(18) 	NULL,
	time_stamp				DATE 			NULL
);

ALTER TABLE reg_statistic_group
  ADD CONSTRAINT pk_reg_stat_group PRIMARY KEY (
    reg_stat_group_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE reg_statistic_group
  ADD CONSTRAINT reg_stat_group_uix UNIQUE (
    description
  )
/

COMMENT ON TABLE reg_statistic_group IS '(C) [01] {A}
The REG_STATISTIC_GROUP table is the base table for statistics groups.';

COMMENT ON COLUMN reg_statistic_group.reg_stat_group_id IS 'System-assigned identifier of a particular statistics group.';
COMMENT ON COLUMN reg_statistic_group.description IS 'A description of the statistics group.';
COMMENT ON COLUMN reg_statistic_group.long_description IS 'A long description of the statistics group.';
COMMENT ON COLUMN reg_statistic_group.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN reg_statistic_group.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

CREATE TABLE reg_statistic_group_map (
	reg_stat_group_id		NUMBER(22,0) 	NOT NULL,
	reg_stat_id				NUMBER(22,0)	NOT NULL,
	sort_order				NUMBER(22,0)	NOT NULL,
	user_id					VARCHAR2(18)	NULL,
	time_stamp				DATE			NULL
);

ALTER TABLE reg_statistic_group_map
  ADD CONSTRAINT pk_reg_stat_group_map PRIMARY KEY (
    reg_stat_group_id,
	reg_stat_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE reg_statistic_group_map
  ADD CONSTRAINT fk_reg_stat_group_map_1 FOREIGN KEY (
    reg_stat_group_id
  ) REFERENCES reg_statistic_group (
    reg_stat_group_id
  )
/

ALTER TABLE reg_statistic_group_map
  ADD CONSTRAINT fk_reg_stat_group_map_2 FOREIGN KEY (
    reg_stat_id
  ) REFERENCES reg_statistic (
    reg_stat_id
  )
/

ALTER TABLE reg_statistic_group_map
  ADD CONSTRAINT reg_stat_group_map_uix_1 UNIQUE (
    reg_stat_group_id, sort_order
  )
/

ALTER TABLE reg_statistic_group_map
  ADD CONSTRAINT reg_stat_group_map_uix_2 UNIQUE (
    reg_stat_id
  )
/

COMMENT ON TABLE reg_statistic_group_map IS '(C) [01] {A}
The REG_STATISTIC_GROUP_MAP table associates statistics with statistic groups and stores the order that the statistics should be executed within a group.';

COMMENT ON COLUMN reg_statistic_group_map.reg_stat_group_id IS 'System-assigned identifier of a particular statistics group.';
COMMENT ON COLUMN reg_statistic_group_map.reg_stat_id IS 'System assigned identifier of a Regulatory Statistic.';
COMMENT ON COLUMN reg_statistic_group_map.sort_order IS 'The order that the statistic should be exeucted within its group.';
COMMENT ON COLUMN reg_statistic_group_map.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN reg_statistic_group_map.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';


--account statistics stuff
CREATE TABLE reg_acct_stat_group (
	reg_acct_stat_group_id		NUMBER(22,0) 	NOT NULL,
	description				VARCHAR2(254) 	NOT NULL,
	long_description		VARCHAR2(2000) 	NULL,
	user_id					VARCHAR2(18) 	NULL,
	time_stamp				DATE 			NULL
);


ALTER TABLE reg_acct_stat_group
  ADD CONSTRAINT pk_reg_acct_stat_group PRIMARY KEY (
    reg_acct_stat_group_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE reg_acct_stat_group
  ADD CONSTRAINT reg_acct_stat_group_uix UNIQUE (
    description
  )
/

COMMENT ON TABLE reg_acct_stat_group IS '(C) [01] {A}
The REG_ACCT_STAT_GROUP table is the base table for account statistics groups.';

COMMENT ON COLUMN reg_acct_stat_group.reg_acct_stat_group_id IS 'System-assigned identifier of a particular account statistics group.';
COMMENT ON COLUMN reg_acct_stat_group.description IS 'A description of the account statistics group.';
COMMENT ON COLUMN reg_acct_stat_group.long_description IS 'A long description of the account statistics group.';
COMMENT ON COLUMN reg_acct_stat_group.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN reg_acct_stat_group.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

CREATE TABLE reg_acct_stat_group_map (
	reg_acct_stat_group_id	NUMBER(22,0) 	NOT NULL,
	reg_acct_id				NUMBER(22,0)	NOT NULL,
	sort_order				NUMBER(22,0)	NOT NULL,
	user_id					VARCHAR2(18)	NULL,
	time_stamp				DATE			NULL
);

ALTER TABLE reg_acct_stat_group_map
  ADD CONSTRAINT pk_reg_acct_stat_group_map PRIMARY KEY (
    reg_acct_stat_group_id,
	reg_acct_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE reg_acct_stat_group_map
  ADD CONSTRAINT fk_reg_acct_stat_group_map_1 FOREIGN KEY (
    reg_acct_stat_group_id
  ) REFERENCES reg_acct_stat_group (
    reg_acct_stat_group_id
  )
/

ALTER TABLE reg_acct_stat_group_map
  ADD CONSTRAINT fk_reg_acct_stat_group_map_2 FOREIGN KEY (
    reg_acct_id
  ) REFERENCES reg_acct_master (
    reg_acct_id
  )
/

ALTER TABLE reg_acct_stat_group_map
  ADD CONSTRAINT reg_acct_stat_group_map_uix_1 UNIQUE (
    reg_acct_stat_group_id, sort_order
  )
/

ALTER TABLE reg_acct_stat_group_map
  ADD CONSTRAINT reg_acct_stat_group_map_uix_2 UNIQUE (
    reg_acct_id
  )
/

COMMENT ON TABLE reg_acct_stat_group_map IS '(C) [01] {A}
The REG_ACCT_STAT_GROUP_MAP table associates statistics with account statistic groups and stores the order that the statistics should be executed within a group.';

COMMENT ON COLUMN reg_acct_stat_group_map.reg_acct_stat_group_id IS 'System-assigned identifier of a particular account statistics group.';
COMMENT ON COLUMN reg_acct_stat_group_map.reg_acct_id IS 'System assigned identifier of a Regulatory Account.';
COMMENT ON COLUMN reg_acct_stat_group_map.sort_order IS 'The order that the account''s statistic should be exeucted within its group.';
COMMENT ON COLUMN reg_acct_stat_group_map.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN reg_acct_stat_group_map.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3355, 0, 2016, 1, 1, 0, 046808, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.1.0_maint_046808_reg_add_stat_group_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;