/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044186_taxrpr_rpr_qty_incl_import_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/10/2015 Anand R        create a new import tables for repair quantity include update
||============================================================================
*/

-- create new import tables

create table RPR_IMPORT_RPR_QTY_INCL
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18) ,
 RETIREMENT_UNIT_ID     NUMBER(22,0)  ,
 RETIREMENT_UNIT_XLATE  VARCHAR2(254)  ,
 REPAIR_QTY_INCLUDE_XLATE VARCHAR2(254) ,
 REPAIR_QTY_INCLUDE     NUMBER(22,0) ,
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_RPR_QTY_INCL
   add constraint RPR_IMPORT_REPAIR_QTY_INCL_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_RPR_QTY_INCL
   add constraint RPR_IMPORT_REPAIR_QTY_INCL_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

comment on table RPR_IMPORT_RPR_QTY_INCL IS 'The Repair Quantity Include table is a staging table into which data from a run of the Repair Quantity Include is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Repair Quantity Include Archive table.';
comment on column RPR_IMPORT_RPR_QTY_INCL.IMPORT_RUN_ID IS 'System-assigned identifier of an import run occurrence.';
comment on column RPR_IMPORT_RPR_QTY_INCL.LINE_ID IS 'Line number corresponding to the row of this data in the original import file.';
comment on column RPR_IMPORT_RPR_QTY_INCL.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
comment on column RPR_IMPORT_RPR_QTY_INCL.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
comment on column RPR_IMPORT_RPR_QTY_INCL.RETIREMENT_UNIT_ID IS 'System-assigned identifier of a particular retirement unit.';
comment on column RPR_IMPORT_RPR_QTY_INCL.RETIREMENT_UNIT_XLATE IS 'Value from the import file translated to obtain the retirement_unit_id.';
comment on column RPR_IMPORT_RPR_QTY_INCL.REPAIR_QTY_INCLUDE_XLATE IS 'Value from the import file translated to obtain the repair_qty_include.';
comment on column RPR_IMPORT_RPR_QTY_INCL.REPAIR_QTY_INCLUDE IS 'indicator to denote if repair quantity include is true(1) or false (0)';
comment on column RPR_IMPORT_RPR_QTY_INCL.ERROR_MESSAGE IS 'Error message encountered while importing / translating / loading this row of data during the import process.';

create table RPR_IMPORT_RPR_QTY_INCL_ARC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18) ,
 RETIREMENT_UNIT_ID     NUMBER(22,0)  ,
 RETIREMENT_UNIT_XLATE  VARCHAR2(254)  ,
 REPAIR_QTY_INCLUDE_XLATE VARCHAR2(254) ,
 REPAIR_QTY_INCLUDE     NUMBER(22,0) 
);

alter table RPR_IMPORT_RPR_QTY_INCL_ARC
   add constraint RPR_IMPORT_RPR_QTY_INCL_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_RPR_QTY_INCL_ARC
   add constraint RPR_IMPORT_RPR_QTY_INCL_ARC_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;
       
comment on table RPR_IMPORT_RPR_QTY_INCL_ARC IS 'The Repair Quantity Include Archive table maintains an an archive of all successful runs of the Repair Quantity Include Import process.';
comment on column RPR_IMPORT_RPR_QTY_INCL_ARC.IMPORT_RUN_ID IS 'System-assigned identifier of an import run occurrence.';
comment on column RPR_IMPORT_RPR_QTY_INCL_ARC.LINE_ID IS 'Line number corresponding to the row of this data in the original import file.';
comment on column RPR_IMPORT_RPR_QTY_INCL_ARC.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';
comment on column RPR_IMPORT_RPR_QTY_INCL_ARC.USER_ID IS 'Standard System-assigned user id used for audit purposes.';
comment on column RPR_IMPORT_RPR_QTY_INCL_ARC.RETIREMENT_UNIT_ID IS 'System-assigned identifier of a particular retirement unit.';
comment on column RPR_IMPORT_RPR_QTY_INCL_ARC.RETIREMENT_UNIT_XLATE IS 'Value from the import file translated to obtain the retirement_unit_id.';
comment on column RPR_IMPORT_RPR_QTY_INCL_ARC.REPAIR_QTY_INCLUDE_XLATE IS 'Value from the import file translated to obtain the repair_qty_include.';
comment on column RPR_IMPORT_RPR_QTY_INCL_ARC.REPAIR_QTY_INCLUDE IS 'indicator to denote if repair quantity include is true(1) or false (0)';      


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2690, 0, 2015, 2, 0, 0, 044186, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044186_taxrpr_rpr_qty_incl_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;