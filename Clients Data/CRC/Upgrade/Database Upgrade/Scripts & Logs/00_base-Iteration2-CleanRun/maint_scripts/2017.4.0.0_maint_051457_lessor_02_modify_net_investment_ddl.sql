/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051457_lessor_02_modify_net_investment_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/29/2018 Anand R        Modify net_investment column to be number(22,2)
||============================================================================
*/

alter table lsr_ilr_termination_st_df 
modify  net_investment number(22,2);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7282, 0, 2017, 4, 0, 0, 51457, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051457_lessor_02_modify_net_investment_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;