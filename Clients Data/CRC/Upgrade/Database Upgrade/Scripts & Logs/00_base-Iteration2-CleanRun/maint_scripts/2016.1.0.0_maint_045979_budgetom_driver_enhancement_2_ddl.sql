 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045979_budgetom_driver_enhancement_2_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- -------------------------------------
 || 2016.1.0.0  08/17/2016 Anand R       PP-45979. Budget O&M driver enhancements
 ||============================================================================
 */ 

alter table CR_BUDGET_DATA_LABOR2_DEFINE drop constraint CR_BUDGET_DATA_LABOR2_DEF_PK;

drop index CR_BUDGET_DATA_LABOR2_DEF_PK;

alter table CR_BUDGET_DATA_LABOR2_DEFINE
add constraint CR_BUDGET_DATA_LABOR2_DEF_PK primary key (ID,LABOR_TEMPLATE_ID,TAB_INDICATOR)
using index tablespace PWRPLANT_IDX;

alter table CR_BUDGET_DATA_LABOR2_DEFINE
add constraint CR_BUDGET_DATA_LABOR2_DEF_FK foreign key (LABOR_TEMPLATE_ID) references CR_BUDGET_LABOR_TEMPLATES (LABOR_TEMPLATE_ID);

alter table CR_BUDGET_DATA_LABOR2_DEFINE
add constraint CR_BUDGET_DATA_LABOR2_DRV_FK foreign key (TAB_INDICATOR) references CR_BUDGET_LABOR_DRIVERS (TAB_INDICATOR);

alter table CR_BUDGET_DATA_LABOR2
add TAB_INDICATOR number(22,0);

comment on column CR_BUDGET_DATA_LABOR2.TAB_INDICATOR is 'Tab indicator for the driver';

--create cr_budget_labor_assign
create table CR_BUDGET_LABOR_ASSIGN (
    LABOR_TEMPLATE_ID number(22,0),
    CR_BUDGET_VERSION_ID number(22,0),
    ELEMENT_VALUE varchar2(35),
    USER_ID varchar2(18),
    TIME_STAMP date);

alter table CR_BUDGET_LABOR_ASSIGN
add constraint CR_BUDGET_LABOR_ASSIGN_pk primary key (LABOR_TEMPLATE_ID,CR_BUDGET_VERSION_ID,ELEMENT_VALUE)
using index tablespace PWRPLANT_IDX;

alter table CR_BUDGET_LABOR_ASSIGN
add constraint CR_BUDGET_LABOR_ASSIGN_fk foreign key (LABOR_TEMPLATE_ID) references CR_BUDGET_LABOR_TEMPLATES (LABOR_TEMPLATE_ID);

comment on table  CR_BUDGET_LABOR_ASSIGN is '(S) [02] A table to store budget labor driver assignment';
comment on column CR_BUDGET_LABOR_ASSIGN.LABOR_TEMPLATE_ID is 'Standard system assigned labor template id';
comment on column CR_BUDGET_LABOR_ASSIGN.CR_BUDGET_VERSION_ID is 'System assigned budget version id';
comment on column CR_BUDGET_LABOR_ASSIGN.ELEMENT_VALUE is 'Element value';
comment on column CR_BUDGET_LABOR_ASSIGN.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
comment on column CR_BUDGET_LABOR_ASSIGN.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';

create table CR_BUDGET_LBR_DRIVER_TEMPLATE (
LABOR_TEMPLATE_ID number(22,0),
TAB_INDICATOR number(22,0),
COLUMN_ORDER number(22,0),
USER_ID varchar2(18),
TIME_STAMP date);

alter table CR_BUDGET_LBR_DRIVER_TEMPLATE
add constraint CR_BUDGET_LBR_DRVS_TEMPLATE_PK primary key (LABOR_TEMPLATE_ID,TAB_INDICATOR)
using index tablespace PWRPLANT_IDX;

alter table CR_BUDGET_LBR_DRIVER_TEMPLATE
add constraint CR_BUDGET_LABOR_DRVS_TMPL_FK1 foreign key (LABOR_TEMPLATE_ID) references CR_BUDGET_LABOR_TEMPLATES (LABOR_TEMPLATE_ID);

alter table CR_BUDGET_LBR_DRIVER_TEMPLATE
add constraint CR_BUDGET_LABOR_DRVS_TMPL_FK2 foreign key (TAB_INDICATOR) references CR_BUDGET_LABOR_DRIVERS (TAB_INDICATOR);

comment on table  CR_BUDGET_LBR_DRIVER_TEMPLATE is '(S) [02] A table to store budget labor driver assignment';
comment on column CR_BUDGET_LBR_DRIVER_TEMPLATE.LABOR_TEMPLATE_ID is 'Standard system assigned labor template id';
comment on column CR_BUDGET_LBR_DRIVER_TEMPLATE.TAB_INDICATOR is 'Tab indicator for the driver';
comment on column CR_BUDGET_LBR_DRIVER_TEMPLATE.COLUMN_ORDER is 'Column Order';
comment on column CR_BUDGET_LBR_DRIVER_TEMPLATE.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
comment on column CR_BUDGET_LBR_DRIVER_TEMPLATE.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';

alter table CR_BUDGET_DATA_LABOR_SPREAD
add TAB_INDICATOR number(22,0);

comment on column CR_BUDGET_DATA_LABOR_SPREAD.TAB_INDICATOR is 'Tab indicator for the driver';

create table CR_BUDGET_DRIVER_FORMAT (
  TABLE_NAME         varchar2(30) not null,
  COLUMN_NAME        varchar2(35) not null,
  LABOR_TEMPLATE_ID  number(22,0) not null,
  TAB_INDICATOR      number(22,0) not null,
  DDDW_NAME          varchar2(35) null,
  ID_COLUMN          varchar2(35) null,
  DISPLAY_COLUMN     varchar2(35) null,
  WIDTH              number(22,0) null,
  DDDW_LINES         number(22,0) null,
  DDDW_WIDTH_PERCENT number(22,0) null,
  ALIGNMENT          number(22,0) null,
  USER_ID            varchar2(18) null,
  TIME_STAMP         date         null);

alter table CR_BUDGET_DRIVER_FORMAT
  add constraint CR_BUDGET_DATA_FORMAT_PK primary key (
    TABLE_NAME,
    COLUMN_NAME,
    LABOR_TEMPLATE_ID,
    TAB_INDICATOR
  )
  using index
    TABLESPACE PWRPLANT_IDX;

alter table CR_BUDGET_DRIVER_FORMAT
  add constraint CR_BUDGET_DATA_FORMAT_FK1 foreign key (
    LABOR_TEMPLATE_ID
  ) references CR_BUDGET_LABOR_TEMPLATES (
    LABOR_TEMPLATE_ID
  );

alter table CR_BUDGET_DRIVER_FORMAT
  add constraint CR_BUDGET_DATA_FORMAT_FK2 foreign key (
    TAB_INDICATOR
  ) references CR_BUDGET_LABOR_DRIVERS (
    TAB_INDICATOR
  );
  
comment on table  CR_BUDGET_DRIVER_FORMAT is '(S) [02] A table to store budget driver format';
comment on column CR_BUDGET_DRIVER_FORMAT.TABLE_NAME is 'Table Name';
comment on column CR_BUDGET_DRIVER_FORMAT.COLUMN_NAME is 'Column Name';
comment on column CR_BUDGET_DRIVER_FORMAT.LABOR_TEMPLATE_ID is 'Standard system assigned labor template id';
comment on column CR_BUDGET_DRIVER_FORMAT.TAB_INDICATOR is 'Tab indicator for the driver';
comment on column CR_BUDGET_DRIVER_FORMAT.DDDW_NAME is 'Drop down data window name';
comment on column CR_BUDGET_DRIVER_FORMAT.ID_COLUMN is 'ID column';
comment on column CR_BUDGET_DRIVER_FORMAT.DISPLAY_COLUMN is 'Display column';
comment on column CR_BUDGET_DRIVER_FORMAT.WIDTH is 'Width of the column';
comment on column CR_BUDGET_DRIVER_FORMAT.DDDW_LINES is 'Number of drop down data window lines';
comment on column CR_BUDGET_DRIVER_FORMAT.DDDW_WIDTH_PERCENT is 'Drop down data window percent width';
comment on column CR_BUDGET_DRIVER_FORMAT.ALIGNMENT is 'Alignment of the column';
comment on column CR_BUDGET_DRIVER_FORMAT.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
comment on column CR_BUDGET_DRIVER_FORMAT.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';  


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3267, 0, 2016, 1, 0, 0, 045979, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045979_budgetom_driver_enhancement_2_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;