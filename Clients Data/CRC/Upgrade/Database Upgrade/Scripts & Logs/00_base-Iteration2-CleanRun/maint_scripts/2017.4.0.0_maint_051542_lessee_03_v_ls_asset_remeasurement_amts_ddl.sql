/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051542_lessee_03_v_ls_asset_remeasurement_amts_DDL.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/15/2018 David Conway     Off Balance Sheet Expense by Set of Books
||============================================================================
*/

CREATE OR REPLACE VIEW V_LS_ASSET_REMEASUREMENT_AMTS AS
SELECT stg.ilr_id, stg.revision, sch.ls_asset_id, sch.set_of_books_id, o.remeasurement_date, ilr.current_revision,
CASE
  WHEN sch.is_om = 1 AND stg.is_om = 0 THEN
    1
  ELSE
    0
END om_to_cap_indicator,
sch.end_obligation AS prior_month_end_obligation, sch.end_liability AS prior_month_end_liability,
sch.end_lt_obligation AS prior_month_end_lt_obligation, sch.end_lt_liability AS prior_month_end_lt_liability,
sch.end_deferred_rent AS prior_month_end_deferred_rent,
sch.end_prepaid_rent AS prior_month_end_prepaid_rent,
depr.end_reserve AS prior_month_end_depr_reserve,
sch.end_capital_cost - depr.end_reserve AS prior_month_end_nbv,
sch.end_capital_cost AS prior_month_end_capital_cost,
sch.end_net_rou_asset as prior_month_net_rou_asset,
sch.end_arrears_accrual AS prior_month_end_arrears_accr,
o.remeasurement_type as remeasurement_type,
o.partial_termination as partial_termination,
prior_opt.asset_quantity as original_asset_quantity,
o.asset_quantity AS current_asset_quantity,
1 is_remeasurement
FROM ls_ilr_stg stg
  JOIN ls_ilr ilr ON stg.ilr_id = ilr.ilr_id
  JOIN ls_ilr_options o ON stg.ilr_id = o.ilr_id AND stg.revision = o.revision
  JOIN ls_ilr_options prior_opt ON stg.ilr_id = prior_opt.ilr_id AND prior_opt.revision = ilr.current_revision
  JOIN ls_ilr_asset_map map ON map.ilr_id = stg.ilr_id AND map.revision = ilr.current_revision
  JOIN ls_asset_schedule sch ON map.ls_asset_id = sch.ls_asset_id AND sch.revision = ilr.current_revision AND sch.set_of_books_id = stg.set_of_books_id
  left OUTER JOIN ls_depr_forecast depr ON depr.ls_asset_id = map.ls_asset_id AND depr.revision = ilr.current_revision AND depr.set_of_books_id = stg.set_of_books_id
WHERE ilr.current_revision <> stg.revision
AND o.remeasurement_date IS NOT NULL
AND Trunc(o.remeasurement_date, 'month') = Add_Months(sch.MONTH,1)
AND (Trunc(o.remeasurement_date, 'month') = Add_Months(depr.MONTH,1) OR depr.MONTH IS NULL)
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7464, 0, 2017, 4, 0, 0, 51542, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051542_lessee_03_v_ls_asset_remeasurement_amts_DDL.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;