create or replace package PKG_PP_SEARCH as

	TYPE results_ids_t IS TABLE OF number(22,0) INDEX BY BINARY_INTEGER;
	SUBTYPE results_ids_st is results_ids_t;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_PP_SEARCH
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   ||============================================================================
   */
	function F_SEARCH(a_searchString string, a_searchType string, a_results in out results_ids_st) return number;
end PKG_PP_SEARCH;
/

create or replace package body PKG_PP_SEARCH as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_PP_SEARCH
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 07/07/2013 B.Beck         Original Version
   ||============================================================================
   */

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************

   --**************************************************************************
   --                            F_SEARCH
   --**************************************************************************

   function F_SEARCH(a_searchString string, a_searchType string, a_results in out results_ids_st) return number is
   l_max_rows number;
   begin
		l_max_rows := a_results.COUNT;

		-- for Lessee, the searchTypes can be 'MLA', 'ILR', 'ASSETS', 'PAYMENTS', 'LESSOR
		if a_searchType = 'MLA' then
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct i.lease_id as the_ids
				from ls_lease i
				where
				(
					to_char(i.lease_id) like '%' || lower(a_searchString) || '%'
					or lower(i.notes) like '%' || lower(a_searchString) || '%'
					or lower(i.description) like '%' || lower(a_searchString) || '%'
					or lower(i.long_description) like '%' || lower(a_searchString) || '%'
					or lower(i.lease_number) like '%' || lower(a_searchString) || '%'
					or i.lease_status_id in 
						(select aa.lease_status_id from ls_lease_status aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%')
					or i.lease_group_id in 
						(select aa.lease_group_id from ls_lease_group aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%')
					or i.lease_id in 
						(select b.lease_id from company aa, ls_lease_company b
						where aa.company_id = b.company_id
						and (lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa.gl_company_no) like '%' || lower(a_searchString) || '%'))
					or i.lease_id in 
						(select aa.lease_id from ls_ilr aa 
						where lower(aa.notes) like '%' || lower(a_searchString) || '%'
						or lower(aa.ilr_number) like '%' || lower(a_searchString) || '%'
						or to_char(aa.ilr_id) like '%' || lower(a_searchString) || '%')
					or i.lease_id in 
						(select aa.lease_id from ls_mla_class_code aa 
						where lower(aa."VALUE") like '%' || lower(a_searchString) || '%'
						)
					or i.lease_id in 
						(select aa.lease_id from ls_lease_document aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa."FILE_NAME") like '%' || lower(a_searchString) || '%'
						)
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'ILR' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct i.ilr_id as the_ids
				from ls_ilr i
				where
				(
					to_char(i.ilr_id) like '%' || lower(a_searchString) || '%'
					or lower(i.notes) like '%' || lower(a_searchString) || '%'
					or lower(i.ilr_number) like '%' || lower(a_searchString) || '%'
					or i.ilr_group_id in 
						(select aa.ilr_group_id from ls_ilr_group aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%')
					or i.ilr_status_id in 
						(select aa.ilr_status_id from ls_ilr_status aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%')
					or i.company_id in 
						(select aa.company_id from company aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa.gl_company_no) like '%' || lower(a_searchString) || '%')
					or i.lease_id in 
						(select aa.lease_id from ls_lease aa 
						where lower(aa.notes) like '%' || lower(a_searchString) || '%'
						or lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa.lease_number) like '%' || lower(a_searchString) || '%'
						or to_char(aa.lease_id) like '%' || lower(a_searchString) || '%')
					or i.ilr_id in 
						(select aa.ilr_id from ls_ilr_class_code aa 
						where lower(aa."VALUE") like '%' || lower(a_searchString) || '%'
						)
					or i.ilr_id in 
						(select aa.ilr_id from ls_ilr_document aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa."FILE_NAME") like '%' || lower(a_searchString) || '%'
						)
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'ASSET' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct i.ls_asset_id as the_ids
				from ls_asset i
				where
				(
					to_char(i.ls_asset_id) like '%' || lower(a_searchString) || '%'
					or lower(i.notes) like '%' || lower(a_searchString) || '%'
					or lower(i.description) like '%' || lower(a_searchString) || '%'
					or lower(i.leased_asset_number) like '%' || lower(a_searchString) || '%'
					or i.ls_asset_status_id in 
						(select aa.ls_asset_status_id from ls_asset_status aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						)
					or i.company_id in 
						(select aa.company_id from company aa
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa.gl_company_no) like '%' || lower(a_searchString) || '%'
						)
					or i.ilr_id in 
						(select aa.ilr_id from ls_ilr aa 
						where lower(aa.notes) like '%' || lower(a_searchString) || '%'
						or lower(aa.ilr_number) like '%' || lower(a_searchString) || '%'
						or to_char(aa.ilr_id) like '%' || lower(a_searchString) || '%'
						)
					or i.ls_asset_id in 
						(select aa.ls_asset_id from ls_asset_class_code aa 
						where lower(aa."VALUE") like '%' || lower(a_searchString) || '%'
						)
					or i.ls_asset_id in 
						(select aa.ls_asset_id from ls_asset_document aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa."FILE_NAME") like '%' || lower(a_searchString) || '%'
						)
					or i.utility_account_id in 
						(select aa.utility_account_id from utility_account aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or to_char(aa.utility_account_id) like '%' || lower(a_searchString) || '%'
						)
					or i.retirement_unit_id in 
						(select aa.retirement_unit_id from retirement_unit aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or to_char(aa.retirement_unit_id) like '%' || lower(a_searchString) || '%'
						or to_char(aa.external_retire_unit) like '%' || lower(a_searchString) || '%'
						)
					or i.asset_location_id in 
						(select aa.asset_location_id from asset_location aa 
						where lower(aa.long_description) like '%' || lower(a_searchString) || '%'
						or to_char(aa.asset_location_id) like '%' || lower(a_searchString) || '%'
						or to_char(aa.ext_asset_location) like '%' || lower(a_searchString) || '%'
						)
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'PAYMENT' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct i.ilr_id as the_ids
				from ls_ilr i, ls_payment_line p, ls_payment_hdr h, ls_asset a
				where i.ilr_id = a.ilr_id
				and p.payment_id = h.payment_id
				and a.ls_asset_id = p.ls_asset_id
				and 
				(
					to_char(h.gl_posting_mo_yr, 'yyyymm') like '%' || lower(a_searchString) || '%'
					or to_char(i.ilr_id) like '%' || lower(a_searchString) || '%'
					or lower(i.notes) like '%' || lower(a_searchString) || '%'
					or lower(i.ilr_number) like '%' || lower(a_searchString) || '%'
					or i.ilr_group_id in 
						(select aa.ilr_group_id from ls_ilr_group aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%')
					or i.ilr_status_id in 
						(select aa.ilr_status_id from ls_ilr_status aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%')
					or i.company_id in 
						(select aa.company_id from company aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa.gl_company_no) like '%' || lower(a_searchString) || '%')
					or i.lease_id in 
						(select aa.lease_id from ls_lease aa 
						where lower(aa.notes) like '%' || lower(a_searchString) || '%'
						or lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa.lease_number) like '%' || lower(a_searchString) || '%'
						or to_char(aa.lease_id) like '%' || lower(a_searchString) || '%')
					or i.ilr_id in 
						(select aa.ilr_id from ls_ilr_class_code aa 
						where lower(aa."VALUE") like '%' || lower(a_searchString) || '%'
						)
					or i.ilr_id in 
						(select aa.ilr_id from ls_ilr_document aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%'
						or lower(aa."FILE_NAME") like '%' || lower(a_searchString) || '%'
						)
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'LESSOR' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct l.lessor_id as the_ids
				from ls_lessor l
				where (to_char(l.lessor_id) like '%' || lower(a_searchString) || '%'
					or lower(l.description) like '%' || lower(a_searchString) || '%'
					or lower(l.address1) like '%' || lower(a_searchString) || '%'
					or lower(l.external_lessor_number) like '%' || lower(a_searchString) || '%'
					or lower(l.city) like '%' || lower(a_searchString) || '%'
					or lower(l.county_id) like '%' || lower(a_searchString) || '%'
					or lower(l.state_id) like '%' || lower(a_searchString) || '%'
					or l.lease_group_id in 
						(select aa.lease_group_id from ls_lease_group aa 
						where lower(aa.description) like '%' || lower(a_searchString) || '%')
				)
			)
			where rownum <= l_max_rows
			;
		end if;
		
		return a_results.COUNT;
   end F_SEARCH;

--**************************************************************************
--                            Initialize Package
--**************************************************************************
end PKG_PP_SEARCH;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (584, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_system_PKG_PP_SEARCH.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;