/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049562_lessor_01_mec_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/27/2017 Sarah Byers      Create/Alter Lessor MEC related tables
||============================================================================
*/
-- LSR_MEC_CONFIG
create table lsr_mec_config (
company_id number(22,0) not null,
accruals number(1,0) default 0 not null,
invoices number(1,0) default 0 not null,
currency_gain_loss number(1,0) default 0 not null,
auto_termination number(1,0) default 0 not null,
user_id varchar2(18),
time_stamp date);

alter table lsr_mec_config add (
constraint pk_lsr_mec_config primary key (company_id) using index tablespace pwrplant_idx);

-- FK to company_setup
alter table lsr_mec_config
add constraint fk_lsr_mec_config1
foreign key (company_id)
references company_setup (company_id);

-- Comments
comment on table lsr_mec_config is '(S) [] The Lessor MEC Config table stores the Lessor Month End Close processes to be used by Company.';
comment on column lsr_mec_config.company_id is 'System-assigned identifier of a company.';
comment on column lsr_mec_config.accruals is 'Indicates whether Accruals should be processed as part of Lessor month end; 1 = process, 0 = do not process.';
comment on column lsr_mec_config.invoices is 'Indicates whether Invoices should be processed as part of Lessor month end; 1 = process, 0 = do not process.';
comment on column lsr_mec_config.currency_gain_loss is 'Indicates whether Currency Gain/Loss should be processed as part of Lessor month end; 1 = process, 0 = do not process.';
comment on column lsr_mec_config.auto_termination is 'Indicates whether Auto-Termination should be processed as part of Lessor month end; 1 = process, 0 = do not process.';
comment on column lsr_mec_config.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column lsr_mec_config.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- Add date columns for MEC processes to LSR_PROCESS_CONTROL
alter table lsr_process_control add (
accrual_calc date,
accrual_approved date,
invoices_calc date,
invoices_approved date,
auto_termination_calc date,
auto_termination_approved date,
currency_gain_loss_approved date,
exchange_lock_date date,
lsr_closed date);

comment on column lsr_process_control.accrual_calc is 'Records the datetime when accruals were calculated.';
comment on column lsr_process_control.accrual_approved is 'Records the datetime when accruals were approved.';
comment on column lsr_process_control.invoices_calc is 'Records the datetime when Invoices were calculated.';
comment on column lsr_process_control.invoices_approved is 'Records the datetime when Invoices were approved.';
comment on column lsr_process_control.auto_termination_calc is 'Records the datetime when Auto-Termination was calculated.';
comment on column lsr_process_control.auto_termination_approved is 'Records the datetime when Auto-Termination was approved.';
comment on column lsr_process_control.currency_gain_loss_approved is 'Records the datetime when Currency Gain/Loss was approved.';
comment on column lsr_process_control.exchange_lock_date is 'The datetime selected by user to lock the exchange rate.';
comment on column lsr_process_control.lsr_closed is 'Records the datetime when the Lessor module was closed.';
 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3849, 0, 2017, 1, 0, 0, 49562, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049562_lessor_01_mec_tables_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

