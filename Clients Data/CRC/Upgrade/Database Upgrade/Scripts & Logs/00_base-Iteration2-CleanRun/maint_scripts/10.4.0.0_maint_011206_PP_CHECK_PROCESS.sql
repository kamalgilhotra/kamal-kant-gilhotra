/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_PP_CHECK_PROCESS.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   03/13/2013 Alex Pivoshenko
||============================================================================
*/

create or replace function PP_CHECK_PROCESS(A_ARG varchar2) return varchar2 as

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PP_CHECK_PROCESS
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By      Reason for Change
   || ------- ---------- --------------- ----------------------------------------
   || 1.0     03/13/2013 Alex Pivoshenko Create
   ||============================================================================
   */

   SID      varchar(60);
   USERNAME varchar(60);
   TERMINAL varchar2(60);
   CODE     number(22, 0);

begin
   select AUDSID, USERNAME, NVL(TERMINAL, ' ')
     into SID, USERNAME, TERMINAL
     from V$SESSION
    where CLIENT_INFO = A_ARG
      and ROWNUM = 1;

   return 'Sid:' || TO_CHAR(SID) || ' User:' || USERNAME || ' Term:' || TERMINAL;

exception
   when NO_DATA_FOUND then
      return 'OK';
   when others then
      CODE := sqlcode;
      RAISE_APPLICATION_ERROR(-20041,
                              'Error getting client info ! Msg = ' ||
                              DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
      return 'Error';
end PP_CHECK_PROCESS;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (319, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_PP_CHECK_PROCESS.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;