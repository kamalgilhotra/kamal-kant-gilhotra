/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038413_reg_revenue_grossup.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 10.4.2.7 06/16/2014 Ryan Oliveria         Reg Revenue Grossup Calculation
||========================================================================================
*/

--drop table REG_CASE_REV_GROSSUP_RESULTS;
--drop table REG_CASE_REV_GROSSUP_ITEMS;

create table REG_CASE_REV_GROSSUP_ITEMS
(
 REG_CASE_ID     number(22,0) not null,
 GROSSUP_ITEM_ID number(22,0) not null,
 USER_ID         varchar2(18),
 TIME_STAMP      date,
 DESCRIPTION     varchar2(35) not null,
 PERCENT         number(22,8) not null,
 DEDUCT_FED      number(22,0) not null,
 DEDUCT_STATE    number(22,0) not null,
 TAX_ENTITY_ID   number(22,0)
);

alter table REG_CASE_REV_GROSSUP_ITEMS
   add constraint PK_REG_GROSSUP_ITEMS
       primary key (REG_CASE_ID, GROSSUP_ITEM_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_CASE_REV_GROSSUP_ITEMS
   add constraint FK_GROSSUP_ITEMS_CASE
       foreign key (REG_CASE_ID)
       references REG_CASE (REG_CASE_ID);

alter table REG_CASE_REV_GROSSUP_ITEMS
   add constraint FK_GROSSUP_ITEMS_TAX_ENT
       foreign key (TAX_ENTITY_ID)
       references REG_TAX_ENTITY (TAX_ENTITY_ID);

alter table REG_CASE_REV_GROSSUP_ITEMS
   add constraint FK_GROSSUP_ITEMS_FED
       foreign key (DEDUCT_FED)
       references YES_NO (YES_NO_ID);

alter table REG_CASE_REV_GROSSUP_ITEMS
   add constraint FK_GROSSUP_ITEMS_STATE
       foreign key (DEDUCT_STATE)
       references YES_NO (YES_NO_ID);


create table REG_CASE_REV_GROSSUP_RESULTS
(
 REG_CASE_ID       number(22,0) not null,
 REG_RETURN_TAG_ID number(22,0) not null,
 GROSSUP_ITEM_ID   number(22,0) not null,
 CASE_YEAR_END     number(6,0) not null,
 USER_ID           varchar2(18),
 TIME_STAMP        date,
 AMOUNT            number(22,2) not null
);

alter table REG_CASE_REV_GROSSUP_RESULTS
   add constraint PK_REG_GROSSUP_RESULTS
       primary key (REG_CASE_ID, REG_RETURN_TAG_ID, GROSSUP_ITEM_ID, CASE_YEAR_END)
       using index tablespace PWRPLANT_IDX;

alter table REG_CASE_REV_GROSSUP_RESULTS
   add constraint FK_GROSSUP_RESULTS_MONITOR
       foreign key (REG_CASE_ID, REG_RETURN_TAG_ID)
       references REG_CASE_MONITOR_CONTROL (REG_CASE_ID, REG_RETURN_TAG_ID);

alter table REG_CASE_REV_GROSSUP_RESULTS
   add constraint FK_GROSSUP_RESULTS_ITEMS
       foreign key (REG_CASE_ID, GROSSUP_ITEM_ID)
       references REG_CASE_REV_GROSSUP_ITEMS (REG_CASE_ID, GROSSUP_ITEM_ID);

alter table REG_CASE add CALCULATE_GROSSUP_FACTOR number(22,0);

comment on column REG_CASE.CALCULATE_GROSSUP_FACTOR is 'Specifies whether the Gross Up Factor for a case is entered by the user or calculated by the system (0 = USER; 1 = SYSTEM)';

update REG_CASE set CALCULATE_GROSSUP_FACTOR = 0 where CALCULATE_GROSSUP_FACTOR is null;

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (10, 'Gross Up Factor', 6, 1);


-- Modify REVENUE_GROSSUP_FACTOR from number(22,2) to number(22,8)
-- Have to null out column before you can change the size/precision
alter table REG_CASE_REV_REQ_RESULTS add TEMP_REVENUE_GROSSUP_FACTOR number(22,8);
update REG_CASE_REV_REQ_RESULTS set TEMP_REVENUE_GROSSUP_FACTOR = REVENUE_GROSSUP_FACTOR, REVENUE_GROSSUP_FACTOR = null;
alter table REG_CASE_REV_REQ_RESULTS modify REVENUE_GROSSUP_FACTOR number(22,8);
update REG_CASE_REV_REQ_RESULTS set REVENUE_GROSSUP_FACTOR = TEMP_REVENUE_GROSSUP_FACTOR;
alter table REG_CASE_REV_REQ_RESULTS drop column TEMP_REVENUE_GROSSUP_FACTOR;


comment on table REG_CASE_REV_GROSSUP_ITEMS is 'The Reg Case Rev Grossup Items table identifies the Revenue Sensitive Expense/Tax Items used to calculate the Gross Up Factor used for calculating Revenue Requirements.';
comment on column REG_CASE_REV_GROSSUP_ITEMS.REG_CASE_ID is 'System assigned identifier for the reg case';
comment on column REG_CASE_REV_GROSSUP_ITEMS.GROSSUP_ITEM_ID is 'System assigned identifier for the gross up item';
comment on column REG_CASE_REV_GROSSUP_ITEMS.DESCRIPTION is 'Description of the Gross Up Item, e.g. Public Utility Fee';
comment on column REG_CASE_REV_GROSSUP_ITEMS.PERCENT is 'The rate assigned to the Gross Up Item';
comment on column REG_CASE_REV_GROSSUP_ITEMS.DEDUCT_FED is 'Specifies whether the Gross Up Item is deductible for Federal Income Taxes (1 = YES; 0 = NO)';
comment on column REG_CASE_REV_GROSSUP_ITEMS.DEDUCT_STATE is 'Specifies whether the Gross Up Item is deductible for State Income Taxes (1 = YES; 0 = NO)';
comment on column REG_CASE_REV_GROSSUP_ITEMS.TAX_ENTITY_ID is 'Indicates that the Gross Up Items is an Effective Tax Rate for the Tax Entity Id assigned';

comment on table REG_CASE_REV_GROSSUP_RESULTS is 'The Reg Case Rev Grossup Results table stores the Pro Forma Amounts calculated for each Revenue Sensitive Expense/Tax Items.';
comment on column REG_CASE_REV_GROSSUP_RESULTS.REG_CASE_ID is 'System assigned identifier for the reg case';
comment on column REG_CASE_REV_GROSSUP_RESULTS.REG_RETURN_TAG_ID is 'System assigned identifier for the return tag';
comment on column REG_CASE_REV_GROSSUP_RESULTS.GROSSUP_ITEM_ID is 'System assigned identifier for the gross up item';
comment on column REG_CASE_REV_GROSSUP_RESULTS.CASE_YEAR_END is 'The year end in YYYYMM format';
comment on column REG_CASE_REV_GROSSUP_RESULTS.AMOUNT is 'The Pro Forma Amount for the Gross Up Item';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1168, 0, 10, 4, 2, 7, 38413, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038413_reg_revenue_grossup.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
