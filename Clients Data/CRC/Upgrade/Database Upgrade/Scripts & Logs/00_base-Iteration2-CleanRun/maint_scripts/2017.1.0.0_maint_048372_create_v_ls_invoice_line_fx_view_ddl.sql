/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048372_create_v_ls_invoice_line_fx_view_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/25/2017 Josh Sandler     Create new v_ls_invoice_line_fx view
||============================================================================
*/

CREATE OR REPLACE VIEW v_ls_invoice_line_fx (
  invoice_id,
  invoice_line_number,
  payment_type_id,
  ls_asset_id,
  gl_posting_mo_yr,
  amount,
  description,
  notes,
  contract_currency_id,
  company_currency_id,
  ls_cur_type,
  exchange_date,
  rate,
  iso_code,
  currency_display_symbol
) AS
WITH
cur AS (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id, contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id, company_cur.currency_display_symbol, company_cur.iso_code, 0
    FROM currency company_cur
    )
SELECT
  lil.invoice_id,
  lil.invoice_line_number,
  lil.payment_type_id,
  lil.ls_asset_id,
  lil.gl_posting_mo_yr,
  Round(lil.amount * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1), 2) amount,
  lil.description,
  lil.notes,
  lease.contract_currency_id,
  cs.currency_id company_currency_id,
  cur.ls_cur_type AS ls_cur_type,
  nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
  decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) rate,
  cur.iso_code,
  cur.currency_display_symbol
FROM ls_invoice_line lil
INNER JOIN ls_invoice li
  ON (li.invoice_id = lil.invoice_id)
INNER JOIN ls_lease lease
  ON li.lease_id = lease.lease_id
INNER JOIN currency_schema cs
  ON li.company_id = cs.company_id
INNER JOIN cur
ON (
  (cur.ls_cur_type = 1 AND cur.currency_id = lease.contract_currency_id)
  OR
  (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
  )
LEFT OUTER JOIN ls_lease_calculated_date_rates cr
  ON (cr.company_id = li.company_id
  AND cr.contract_currency_id =  lease.contract_currency_id
  AND cr.company_currency_id =  cs.currency_id
  AND cr.accounting_month = lil.gl_posting_mo_yr )
where Nvl(cr.exchange_rate_type_id, 1) = 1
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3605, 0, 2017, 1, 0, 0, 48372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048372_create_v_ls_invoice_line_fx_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  
  