 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048373_reg_alloc_dyn_ext_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 07/06/2017 Sarah Byers    Add display_computation
 ||============================================================================
 */

-- Remove the generic table
drop table reg_allocator_dyn_ext;

-- Create the jur table
create table reg_jur_allocator_dyn_ext (
reg_jur_template_id number(22,0) not null,
reg_allocator_id number(22,0) not null,
computation varchar2(4000) not null,
display_computation varchar2(4000) not null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_jur_allocator_dyn_ext add (
constraint pk_reg_jur_allocator_dyn_ext primary key (reg_jur_template_id, reg_allocator_id) using index tablespace pwrplant_idx);

alter table reg_jur_allocator_dyn_ext
add constraint fk_reg_jur_allocator_dyn_ext_1
foreign key (reg_jur_template_id)
references reg_jur_template (reg_jur_template_id);

alter table reg_jur_allocator_dyn_ext
add constraint fk_reg_jur_allocator_dyn_ext_2
foreign key (reg_allocator_id)
references reg_allocator (reg_allocator_id);

comment on table reg_jur_allocator_dyn_ext is 'The Reg Jur Allocator Dyn Ext table stores user defined formulas for calculating factors for a dynamic allocator.';
comment on column reg_jur_allocator_dyn_ext.reg_jur_template_id is 'System assigned identifier of a regulatory jurisdictional template';
comment on column reg_jur_allocator_dyn_ext.reg_allocator_id is 'System assigned identifier of a regulatory allocator';
comment on column reg_jur_allocator_dyn_ext.computation is 'Algebraic expression of the dynamic allocator basis rows used to compute the allocation factors.';
comment on column reg_jur_allocator_dyn_ext.display_computation is 'Display version of the computation that shows descriptions rather than row numbers.';
comment on column reg_jur_allocator_dyn_ext.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_jur_allocator_dyn_ext.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- Create the case table
create table reg_case_allocator_dyn_ext (
reg_case_id number(22,0) not null,
reg_allocator_id number(22,0) not null,
computation varchar2(4000) not null,
display_computation varchar2(4000) not null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_case_allocator_dyn_ext add (
constraint pk_reg_case_allocator_dyn_ext primary key (reg_case_id, reg_allocator_id) using index tablespace pwrplant_idx);

alter table reg_case_allocator_dyn_ext
add constraint fk_reg_case_alloc_dyn_ext_1
foreign key (reg_case_id)
references reg_case (reg_case_id);

alter table reg_case_allocator_dyn_ext
add constraint fk_reg_case_alloc_dyn_ext_2
foreign key (reg_allocator_id)
references reg_allocator (reg_allocator_id);

comment on table reg_case_allocator_dyn_ext is 'The Reg Case Allocator Dyn Ext table stores user defined formulas for calculating factors for a dynamic allocator.';
comment on column reg_case_allocator_dyn_ext.reg_case_id is 'System assigned identifier of a regulatory case';
comment on column reg_case_allocator_dyn_ext.reg_allocator_id is 'System assigned identifier of a regulatory allocator';
comment on column reg_case_allocator_dyn_ext.computation is 'Algebraic expression of the dynamic allocator basis rows used to compute the allocation factors.';
comment on column reg_case_allocator_dyn_ext.display_computation is 'Display version of the computation that shows descriptions rather than row numbers.';
comment on column reg_case_allocator_dyn_ext.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_case_allocator_dyn_ext.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3569, 0, 2017, 1, 0, 0, 48373, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048373_reg_alloc_dyn_ext_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;