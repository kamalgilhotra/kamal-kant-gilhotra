--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042917_taxrpr_ltd_prfmc6_20151_ddl.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/13/2015 A Scott          Improve LTD performance. datatype change on table.
--||============================================================================
--*/

alter table TEMP_WOS_IN_SCHEMA
modify blanket_method VARCHAR2(35);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2376, 0, 2015, 1, 0, 0, 42917, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042917_taxrpr_ltd_prfmc6_20151_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;