/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029824_prov.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/03/2013 Nathan Hollis  Point Release
||============================================================================
*/

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select REP_CONS_TYPE_ID, 'm_roll_descr', 1, 'string', 'm_roll_descr', 3
     from TAX_ACCRUAL_REP_CONS_TYPE
    where DATAWINDOW = 'dw_tax_accrual_def_tax_rpt2_cons';

update TAX_ACCRUAL_REP_CONS_ROWS
   set LABEL_VALUE = 'dit_account_m_rollup'
 where REP_CONS_TYPE_ID in
       (select REP_CONS_TYPE_ID
          from TAX_ACCRUAL_REP_CONS_TYPE
         where DATAWINDOW = 'dw_tax_accrual_def_tax_rpt2_cons')
   and LABEL_VALUE = 'dit_account';

update TAX_ACCRUAL_REP_CONS_ROWS
   set DISPLAY_IND_FIELD = 'show_m_roll_visible'
 where REP_CONS_TYPE_ID in
       (select REP_CONS_TYPE_ID
          from TAX_ACCRUAL_REP_CONS_TYPE
         where DATAWINDOW = 'dw_tax_accrual_def_tax_rpt2_cons')
   and LABEL_VALUE = 'dit_account_m_rollup';

-- fixing a problem with the M End Bal by Company Report
update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'm_balance_end_tot', VALUE_TYPE = 'COL_VALUE_NUM'
 where REP_CONS_TYPE_ID = 16
   and ROW_VALUE = 'm_balance_end_cons';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (675, 0, 10, 4, 2, 0, 29824, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029824_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;