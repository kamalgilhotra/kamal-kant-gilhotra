/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051527_lessee_06_v_ls_payment_hdr_fx_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/15/2018 Sisouphanh       Add an AVERAGE_RATE field
||============================================================================
*/

CREATE OR REPLACE VIEW V_LS_PAYMENT_HDR_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code,
                1                                    historic_rate
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code,
                0
         FROM   currency company_cur)
SELECT lph.payment_id,
       lph.lease_id,
       lph.vendor_id,
       lph.company_id,
	   lph.amount                           original_amount,
       lph.amount *  Decode(ls_cur_type, 2, Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg_rate.rate, Nvl(cr.rate, cur.historic_rate)), 1) amount,	   
       lph.description,
       lph.gl_posting_mo_yr,
       lph.payment_status_id,
       lph.ap_status_id,
       lph.ls_asset_id,
       lph.ilr_id,
       lph.payment_month,
       lease.contract_currency_id,
       cs.currency_id                       company_currency_id,
       cur.ls_cur_type                      AS ls_cur_type,
       Nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
       Decode(ls_cur_type, 2, Nvl(cr.rate, cur.historic_rate),
                           1)               rate,
       Decode(ls_cur_type, 2, decode(lower(sc.control_value), 'yes', cr_avg_rate.rate, Nvl(cr.rate, cur.historic_rate)),
                           1)               average_rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_payment_hdr lph
       inner join ls_lease lease
               ON lph.lease_id = lease.lease_id
       inner join currency_schema cs
               ON lph.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = lease.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       left outer join ls_lease_calculated_date_rates cr
                    ON ( cr.company_id = lph.company_id
                         AND cr.contract_currency_id =
                             lease.contract_currency_id
                         AND cr.company_currency_id = cs.currency_id
                         AND cr.accounting_month = lph.payment_month )
       left outer join ls_lease_calculated_date_rates cr_avg_rate
                 ON ( cr_avg_rate.company_id = lph.company_id
                      AND cr_avg_rate.contract_currency_id = 
                          lease.contract_currency_id
                      AND cr_avg_rate.company_currency_id = cs.currency_id
                      AND cr_avg_rate.accounting_month = lph.payment_month )
       inner join pp_system_control_companies sc
                 ON lph.company_id = sc.company_id AND 
                    lower(trim(sc.control_name)) = 'lease mc: use average rates'                          
WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1 and Nvl(cr_avg_rate.exchange_rate_type_id, 4) = 4
AND cs.currency_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7227, 0, 2017, 4, 0, 0, 51527, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051527_lessee_06_v_ls_payment_hdr_fx_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;