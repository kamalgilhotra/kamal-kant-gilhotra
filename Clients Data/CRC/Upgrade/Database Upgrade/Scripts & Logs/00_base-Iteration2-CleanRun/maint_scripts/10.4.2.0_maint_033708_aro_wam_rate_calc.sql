/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033708_aro_wam_rate_calc.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/07/2014 Ryan Oliveria  Drop foreign key on aro_wam_rate_calc
||============================================================================
*/

alter table ARO_WAM_RATE_CALC drop constraint R_ARO_WAM_RATE_CALC1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (842, 0, 10, 4, 2, 0, 33708, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033708_aro_wam_rate_calc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;