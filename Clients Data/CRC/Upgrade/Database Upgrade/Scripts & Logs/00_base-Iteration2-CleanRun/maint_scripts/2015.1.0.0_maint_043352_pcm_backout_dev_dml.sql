/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043352_pcm_backout_dev_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/24/2015 Alex P.          Postpone these features to a later version
||============================================================================
*/
-- The exact same report exists under both report_type_ids. Remove the duplicate for now, so that we don't get an Oracle error after the update below.
      delete from pp_reports
      where report_id = 300005
        and report_number = 'PCM - FP - BDG - 1000';

-- Change report type ids for new converted reports to Unknown (0), so that they dont show up among existing old reports.
	update pp_reports 
	set report_type_id = 0
	where report_id between 300000 and 300004
		and report_type_id in (35, 36);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2439, 0, 2015, 1, 0, 0, 043352, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043352_pcm_backout_dev_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;