/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030538_lease_mla_business_logic.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Kyle Peterson  Point release
||============================================================================
*/

alter table LS_LEASE modify CUT_OFF_DAY null;
alter table LS_LEASE modify DAYS_IN_BASIS_YEAR null;
alter table LS_LEASE modify EXTENDED_RENTAL_TYPE_ID null;

alter table LS_LEASE_GROUP add VENDOR_BY_COMPANY number(1,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (441, 0, 10, 4, 1, 0, 30538, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030538_lease_mla_business_logic.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
