 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_047985_lessor_lsr_ilr_group_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 08/30/2017 Shane "C" Ward    New table & sequence for Lessor ILR Groups
 ||============================================================================
 */


CREATE TABLE lsr_ilr_group (
  ilr_group_id                  NUMBER(22,0)  NOT NULL,
  description                   VARCHAR2(35)  NOT NULL,
  long_description              VARCHAR2(254) NULL,
  workflow_type_id              NUMBER(22,0)  NULL,
  payment_shift                 NUMBER(22,0)  NULL,
  time_stamp                    DATE          NULL,
  user_id                       VARCHAR2(18)  NULL
);

COMMENT ON TABLE lsr_ilr_group IS '(S)  [06]
The LSR (Lessor) ILR Group table is a table that holds groups of ILRs. This allows reporting rollups for ILRs at different levels than MLA and company.';

COMMENT ON COLUMN lsr_ilr_group.ilr_group_id IS 'An internal PowerPlant id representing the ILR Group for the ILR. Provided by lsr_ilr_group_seq Sequence';
COMMENT ON COLUMN lsr_ilr_group.description IS 'a description field for the ILR.';
COMMENT ON COLUMN lsr_ilr_group.long_description IS 'a long description field for the ILR.';
COMMENT ON COLUMN lsr_ilr_group.workflow_type_id IS 'An internal id used for routing records to approval.';
COMMENT ON COLUMN lsr_ilr_group.payment_shift IS 'The number of months that payments are shifted relative to the schedule months.';
COMMENT ON COLUMN lsr_ilr_group.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_group.user_id IS 'Standard system-assigned user id used for audit purposes.';

ALTER TABLE lsr_ilr_group
  ADD CONSTRAINT pk_lsr_ilr_group PRIMARY KEY (
    ilr_group_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
;

CREATE SEQUENCE lsr_ilr_group_seq INCREMENT BY 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3683, 0, 2017, 1, 0, 0, 47985, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047985_lessor_lsr_ilr_group_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
