/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011149_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/09/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Create a temp table to store the ledger tax year records before inserting into pt_ledger_tax_year.
-- This will be used by the national allocation process to handle rounding corrections.
--
create global temporary table PWRPLANT.PT_TEMP_LEDGER_TAX_YEAR
(
 PROPERTY_TAX_LEDGER_ID     number(22,0) not null,
 TAX_YEAR                   number(22,0) not null,
 FROM_TAX_TYPE_ID           number(22,0) not null,
 BEGINNING_BOOK_BALANCE     number(22,2) default 0,
 BOOK_ADDITIONS             number(22,2) default 0,
 BOOK_RETIREMENTS           number(22,2) default 0,
 BOOK_TRANSFERS_ADJ         number(22,2) default 0,
 ENDING_BOOK_BALANCE        number(22,2) default 0,
 CWIP_BALANCE               number(22,2) default 0,
 BEG_BAL_ADJUSTMENT         number(22,2) default 0,
 ADDITIONS_ADJUSTMENT       number(22,2) default 0,
 RETIREMENTS_ADJUSTMENT     number(22,2) default 0,
 TRANSFERS_ADJUSTMENT       number(22,2) default 0,
 END_BAL_ADJUSTMENT         number(22,2) default 0,
 CWIP_ADJUSTMENT            number(22,2) default 0,
 ALLOCATED_TAX_BASIS        number(22,2) default 0,
 ALLOCATED_ACCUM_ORE        number(22,2) default 0,
 ALLOCATED_TAX_RESERVE      number(22,2) default 0,
 TAX_BASIS_ADJUSTMENT       number(22,2) default 0,
 PROP_TAX_BALANCE           number(22,2) default 0,
 ASSOCIATED_RESERVE         number(22,2) default 0,
 CALCULATED_RESERVE         number(22,2) default 0,
 RESERVE_ADJUSTMENT         number(22,2) default 0,
 FINAL_RESERVE              number(22,2) default 0,
 ESCALATED_PROP_TAX_BALANCE number(22,2) default 0,
 ESCALATED_PROP_TAX_RESERVE number(22,2) default 0,
 COST_APPROACH_VALUE        number(22,2) default 0,
 MARKET_VALUE               number(22,2) default 0,
 QUANTITY                   number(22,8),
 ALLOCATION_STATISTIC       number(22,8)
) on commit preserve rows;

create unique index PWRPLANT.PT_TEMP_LEDGER_TAX_YEAR
   on PWRPLANT.PT_TEMP_LEDGER_TAX_YEAR (PROPERTY_TAX_LEDGER_ID, TAX_YEAR, FROM_TAX_TYPE_ID);

begin
   DBMS_STATS.SET_TABLE_STATS('PWRPLANT', 'PT_TEMP_LEDGER_TAX_YEAR', '', null, null, 1, 1, 36, null);
   DBMS_STATS.LOCK_TABLE_STATS('PWRPLANT', 'PT_TEMP_LEDGER_TAX_YEAR');
end;
/

--
-- Create a temp table to store the ledger adjustment records before inserting into pt_ledger_adjustment.
-- This will be used by the national allocation process to handle rounding corrections.
--
create global temporary table PWRPLANT.PT_TEMP_LEDGER_ADJUSTMENT
(
 PROPERTY_TAX_LEDGER_ID number(22,0) not null,
 PROPERTY_TAX_ADJUST_ID number(22,0) not null,
 USER_INPUT             number(22,0) not null,
 TAX_YEAR               number(22,0) not null,
 FROM_TAX_TYPE_ID       number(22,0) not null,
 BEG_BAL_ADJUSTMENT     number(22,2) default 0,
 ADDITIONS_ADJUSTMENT   number(22,2) default 0,
 RETIREMENTS_ADJUSTMENT number(22,2) default 0,
 TRANSFERS_ADJUSTMENT   number(22,2) default 0,
 END_BAL_ADJUSTMENT     number(22,2) default 0,
 CWIP_ADJUSTMENT        number(22,2) default 0,
 TAX_BASIS_ADJUSTMENT   number(22,2) default 0,
 RESERVE_ADJUSTMENT     number(22,2) default 0
) on commit preserve rows;

create unique index PWRPLANT.PT_TEMP_LEDGER_ADJUSTMENT
   on PWRPLANT.PT_TEMP_LEDGER_ADJUSTMENT (PROPERTY_TAX_LEDGER_ID, PROPERTY_TAX_ADJUST_ID, USER_INPUT, TAX_YEAR, FROM_TAX_TYPE_ID);

begin
   DBMS_STATS.SET_TABLE_STATS('PWRPLANT','PT_TEMP_LEDGER_ADJUSTMENT','',NULL,NULL,1,1,36,NULL);
   DBMS_STATS.LOCK_TABLE_STATS('PWRPLANT','PT_TEMP_LEDGER_TAX_YEAR');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (248, 0, 10, 4, 0, 0, 11149, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011149_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
