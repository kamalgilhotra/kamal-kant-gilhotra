/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051078_lessee_01_add_ROU_asset_ilr_DDL.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0   05/09/2018   K. Powers   PP-51078 Add Right of Use Asset (ROU)
 ||                                     Unamortized Balance to Schedule
 ||============================================================================
 */ 

 alter table ls_asset_schedule add BEG_NET_ROU_ASSET number(22,2);
 alter table ls_asset_schedule add END_NET_ROU_ASSET number(22,2) ;
  
 alter table ls_ilr_schedule add BEG_NET_ROU_ASSET number(22,2) ;
 alter table ls_ilr_schedule add END_NET_ROU_ASSET number(22,2) ;
 
 COMMENT ON COLUMN "PWRPLANT"."LS_ASSET_SCHEDULE"."BEG_NET_ROU_ASSET" IS 'Begin Capital Cost less Begin Reserve for the Asset';
 COMMENT ON COLUMN "PWRPLANT"."LS_ASSET_SCHEDULE"."END_NET_ROU_ASSET" IS 'End Capital Cost less End Reserve for the Asset';

 COMMENT ON COLUMN "PWRPLANT"."LS_ILR_SCHEDULE"."BEG_NET_ROU_ASSET" IS 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
 COMMENT ON COLUMN "PWRPLANT"."LS_ILR_SCHEDULE"."END_NET_ROU_ASSET" IS 'End Capital Cost less End Reserve for all Assets on ILR';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5622, 0, 2017, 4, 0, 0, 51078, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051078_lessee_01_add_ROU_asset_ilr_DDL.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
 
