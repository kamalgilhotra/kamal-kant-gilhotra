/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052859_lessee_01_default_disc_rate_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 01/14/2019 Shane "C" Ward  New Tables for ILR Default Discount Rate Functionality
||============================================================================
*/

CREATE TABLE LS_ILR_DEFAULT_RATE_TYPE
(rate_type_id NUMBER(22,0) NOT NULL,
description VARCHAR2(35) NOT NULL,
long_Description VARCHAR2(254),
active NUMBER(1,0) DEFAULT 1 NOT NULL ,
user_id VARCHAR2(18),
time_stamp DATE);

ALTER TABLE ls_ilr_default_rate_type
ADD CONSTRAINT ls_ilr_default_Rate_Type_pk
PRIMARY KEY (rate_type_id) USING INDEX TABLESPACE pwrplant_idx;

COMMENT ON TABLE ls_ilr_default_Rate_type IS '[S] Lessee Default Rate Type table stores definition for Default Rate Types to be used with Default Discount Rate';
COMMENT ON COLUMN ls_ilr_default_Rate_type.rate_type_id IS 'System Assigned Identifier of a Default Rate Type';
COMMENT ON COLUMN ls_ilr_default_Rate_type.description IS 'Description of a Default Rate Type';
COMMENT ON COLUMN ls_ilr_default_Rate_type.long_Description IS 'Long Description of a Default Rate Type';
COMMENT ON COLUMN ls_ilr_default_Rate_type.active IS 'System Flag for whether the Default Discount Rate Type is active (1 = yes 0 = no)';
COMMENT ON COLUMN ls_ilr_default_Rate_type.user_id IS 'Standard system-assigned user id used for audit purposes';
COMMENT ON COLUMN ls_ilr_default_Rate_type.time_stamp IS 'Standard system-assigned timestamp used for audit purposes';

CREATE TABLE ls_ilr_default_Rate_type_map
(rate_type_id NUMBER(22,0) NOT NULL,
company_id NUMBER(22,0) NOT NULL,
lease_group_id NUMBER(22,0) DEFAULT 0,
ilr_group_id NUMBER(22,0) DEFAULT 0,
user_id VARCHAR2(18),
time_stamp DATE);

COMMENT ON TABLE ls_ilr_default_Rate_type_map IS '[S] Lessee Default Rate Type Map table stores mapping of Company and ILR Group or Lease Group to Default Discount Rate Types. Only Lease Group or ILR Group can be populated, not both (accross the system)';
COMMENT ON COLUMN ls_ilr_default_Rate_type_map.rate_type_id IS 'System Assigned Identifier of a Default Rate Type';
COMMENT ON COLUMN ls_ilr_default_Rate_type_map.company_id IS 'System Assigned Identifier of a Company';
COMMENT ON COLUMN ls_ilr_default_Rate_type_map.LEASE_GROUP_ID IS 'System Assigned Identifier of a Lease Group';
COMMENT ON COLUMN ls_ilr_default_Rate_type_map.ilr_group_id IS 'System Assigned Identifier of an ILR Group';
COMMENT ON COLUMN ls_ilr_default_Rate_type_map.user_id IS 'Standard system-assigned user id used for audit purposes';
COMMENT ON COLUMN ls_ilr_default_Rate_type_map.time_stamp IS 'Standard system-assigned timestamp used for audit purposes';

ALTER TABLE ls_ilr_default_Rate_type_map
ADD CONSTRAINT ls_ilr_default_Rate_type_m_pk
PRIMARY KEY (rate_type_id, company_id, lease_group_id, ilr_group_id) USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE ls_ilr_default_Rate_type_map
  ADD CONSTRAINT ls_default_Rate_type_map_fk1 FOREIGN KEY (
    rate_type_id
  ) REFERENCES ls_ilr_default_rate_type (
    rate_type_id
  )
;

ALTER TABLE ls_ilr_default_Rate_type_map
  ADD CONSTRAINT ls_default_Rate_type_map_fk2 FOREIGN KEY (
    company_id
  ) REFERENCES company_setup (
    company_id
  )
;

CREATE TABLE ls_ilr_Default_rates
(rate_type_id NUMBER(22,0) NOT NULL,
term_length NUMBER(22,0) NOT NULL,
effective_date DATE NOT null,
borrowing_curr_id NUMBER (22,0),
rate NUMBER(22,8),
user_id VARCHAR2(18),
time_stamp DATE);

ALTER TABLE ls_ilr_Default_rates
ADD CONSTRAINT ls_ilr_Default_rates_pk
PRIMARY KEY (rate_type_id, term_length, effective_date, borrowing_curr_id) USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE ls_ilr_Default_rates
  ADD CONSTRAINT ls_ilr_Default_rates_fk1 FOREIGN KEY (
    rate_type_id
  ) REFERENCES ls_ilr_default_rate_type (
    rate_type_id
  )
;

COMMENT ON TABLE ls_ilr_Default_rates IS '[S] Lessee Default Rates table stores Default Discount Rates by Rate Type, Term Length, Effective Date, and Borrowing Currency';
COMMENT ON COLUMN ls_ilr_Default_rates.rate_type_id IS 'System Assigned Identifier of a Default Rate Type';
COMMENT ON COLUMN ls_ilr_Default_rates.term_length IS 'Length of Payment Terms in Months for the Default Discount Rate to apply to';
COMMENT ON COLUMN ls_ilr_Default_rates.effective_date IS 'Effective Date of the Rate to be applied forward from';
COMMENT ON COLUMN ls_ilr_Default_rates.borrowing_curr_id IS 'System Assigned Identifier of a Currency, Null = <ALL> ';
COMMENT ON COLUMN ls_ilr_Default_rates.rate IS 'User inputted rate to be leveraged by Default Discount Rate Type';
COMMENT ON COLUMN ls_ilr_Default_rates.user_id IS 'Standard system-assigned user id used for audit purposes';
COMMENT ON COLUMN ls_ilr_Default_rates.time_stamp IS 'Standard system-assigned timestamp used for audit purposes';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(14102, 0, 2018, 2, 0, 0, 52859, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052859_lessee_01_default_disc_rate_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;