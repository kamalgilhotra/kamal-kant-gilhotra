/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036338_pwrtax_mlp_rets.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/19/2014 Andrew Scott        Retirements Interface for MLP on the
||                                         base gui.
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_input_book_retirements', 'Retirements', 'uo_tax_depr_act_wksp_intfc_rets',
    'Retirements');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'depr_input_book_retirements', ENABLE_YN = 1
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_book_retirements';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (985, 0, 10, 4, 2, 0, 36338, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036338_pwrtax_mlp_rets.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;