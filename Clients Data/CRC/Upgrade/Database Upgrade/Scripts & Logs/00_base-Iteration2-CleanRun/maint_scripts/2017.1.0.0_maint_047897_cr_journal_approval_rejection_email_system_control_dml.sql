/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_047897_cr_journal_approval_rejection_email_system_control_dml.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 05/11/2017 Josh Sandler   Create a new cr system control Journal Approved/Rejected Use Email
||========================================================================================
*/

--Default initial value from existing system control: 'Journal Approval Use Email'. Yes >> Both and No >> None to maintain existing behavior
INSERT INTO cr_system_control
  (control_id, control_name, control_value, long_description)
  SELECT 271,
         'Journal Approved/Rejected Use Email',
         decode(upper(TRIM(control_value)), 'YES', 'Both', 'NO', 'None'),
         'Determines whether to send en e-mail to the journal creator when journals are approved and/or rejected. Options are ''Both'', ''Approved Only'', ''Rejected Only'', and ''None''.'
    FROM cr_system_control
   WHERE control_name = 'Journal Approval Use Email';

   --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3501, 0, 2017, 1, 0, 0, 47897, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047897_cr_journal_approval_rejection_email_system_control_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
   