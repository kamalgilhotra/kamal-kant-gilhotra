/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029946_lease_03_PKG_LEASE_ASSET_POST.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/21/2013 Brandon Beck   Point Release
||============================================================================
*/

create or replace package PKG_LEASE_ASSET_POST as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_ASSET_POST
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck    Original Version
   ||============================================================================
   */

   procedure P_SET_ILR_ID(A_ILR_ID number);

   -- Function to perform specific Leased Asset addition
   function F_ADD_ASSET(A_LS_ASSET_ID number) return varchar2;

   -- Function to perform specific Leased Asset retirements
   function F_RETIRE_ASSET(A_LS_ASSET_ID number) return varchar2;

   -- Function to perform specific Leased Asset transfer from
   function F_TRANSFER_ASSET_FROM(A_LS_ASSET_ID number) return varchar2;

   -- Function to perform specific Leased Asset transfer to
   function F_TRANSFER_ASSET_TO(A_LS_ASSET_ID number) return varchar2;

   -- Function to perform specific Leased Asset Adjustment
   function F_ADJUST_ASSET(A_LS_ASSET_ID number) return varchar2;

   function F_GET_ILR_ID return number;
end PKG_LEASE_ASSET_POST;
/

create or replace package body PKG_LEASE_ASSET_POST as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_ASSET_POST
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck          Original Version
   ||============================================================================
   */

   L_ILR_ID number;

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is
   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   -- Function to perform specific Leased Asset addition
   function F_ADD_ASSET(A_LS_ASSET_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      L_STATUS := 'Starting to add leased asset';
      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ADD_ASSET;

   -- Function to perform specific Leased Asset retirement
   function F_RETIRE_ASSET(A_LS_ASSET_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      L_STATUS := 'Starting to retire leased asset';
      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_RETIRE_ASSET;

   -- Function to perform specific Leased Asset transfer from
   function F_TRANSFER_ASSET_FROM(A_LS_ASSET_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      L_STATUS := 'Starting to transfer leased asset from';
      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_TRANSFER_ASSET_FROM;

   -- Function to perform specific Leased Asset transfer to
   function F_TRANSFER_ASSET_TO(A_LS_ASSET_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      L_STATUS := 'Starting to transfer leased asset to';
      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_TRANSFER_ASSET_TO;

   -- Function to perform specific Leased Asset Adjustment
   function F_ADJUST_ASSET(A_LS_ASSET_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      L_STATUS := 'Starting to adjust leased asset';
      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ADJUST_ASSET;

   --**************************************************************************
   --                            GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is
   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end PKG_LEASE_ASSET_POST;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (428, 0, 10, 4, 1, 0, 29946, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029946_lease_03_PKG_LEASE_ASSET_POST.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
