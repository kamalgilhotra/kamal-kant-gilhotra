/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050372_lessor_04_make_new_accounts_not_null_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/12/2018 Jared Watkins  Update the new accounts to be non-nullable
||============================================================================
*/

alter table lsr_ilr_group 
modify (incurred_costs_account_id number(22,0) not null,
        def_costs_account_id number(22,0) not null,
        def_selling_profit_account_id number(22,0) not null
);


alter table lsr_ilr_account 
modify (incurred_costs_account_id number(22,0) not null,
        def_costs_account_id number(22,0) not null,
        def_selling_profit_account_id number(22,0) not null
);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4312, 0, 2017, 3, 0, 0, 50372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050372_lessor_04_make_new_accounts_not_null_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;