SET SERVEROUTPUT ON

/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037840_reg_summary_columns.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/01/2014 Kyle Peterson
||========================================================================================
*/

/* This is a PLSQL Script with functions to add and drop columns from tables in the DB */
declare
   procedure DROP_COLUMN(V_TABLE_NAME varchar2,
                         V_COL_NAME   varchar2) is
   begin
      execute immediate 'alter table ' || V_TABLE_NAME || ' drop column ' || V_COL_NAME;
      DBMS_OUTPUT.PUT_LINE('Sucessfully dropped column ' || V_COL_NAME || ' from table ' || V_TABLE_NAME || '.');
   exception
      when others then
         if sqlcode = -904 then
            --904 is invalid identifier, which means that the table already does not have this column, so do nothing
            DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' on table ' || V_TABLE_NAME ||
                                 ' was already removed. No action necessary.');
         else
            RAISE_APPLICATION_ERROR(-20000,
                                    'Could not drop column ' || V_COL_NAME || ' from ' || V_TABLE_NAME ||
                                    '. SQL Error: ' || sqlerrm);
         end if;
   end DROP_COLUMN;

   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' || V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' || V_TABLE_NAME ||
                                    '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' || V_TABLE_NAME ||
                                       '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' || V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('               Sucessfully added the comment to column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' || V_TABLE_NAME ||
                                    '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
begin

   DROP_COLUMN('reg_column', 'column_name');
   DROP_COLUMN('reg_column', 'report_dw');

   ADD_COLUMN('reg_column', 'long_description', 'varchar2(254)', 'A long description of the summary column.');

end;
/

   CREATE OR REPLACE TRIGGER 
	PP_AU_REG_COLUMN AFTER 
	 INSERT  OR  UPDATE  OR  DELETE  OF COLUMN_SUMMARY_ID, DESCRIPTION, LONG_DESCRIPTION
	 ON REG_COLUMN FOR EACH ROW 
	DECLARE  
	  prog varchar2(60);  
	  osuser varchar2(60);  
	  machine varchar2(60);  
	  terminal varchar2(60);  
	  old_lookup varchar2(250);  
	  new_lookup varchar2(250);  
	  pk_lookup varchar2(1500);  
	  pk_temp varchar2(250);  
	  window varchar2(60);  
	  windowtitle varchar2(250);  
	  comments varchar2(250);  
	  trans varchar2(35);  
	  ret number(22,0);  
	BEGIN  
	IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then  
	   return;  
	end if; 
	  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window'); 
	  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),''); 
	  trans        := nvl(sys_context('powerplant_ctx','process'     ),''); 
	  comments     := nvl(sys_context('powerplant_ctx','comments'    ),''); 
	  prog         := nvl(sys_context('powerplant_ctx','program'     ),''); 
	  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),''); 
	  machine      := nvl(sys_context('powerplant_ctx','machine'     ),''); 
	  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),''); 
	  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
	IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF; 
	IF UPDATING THEN 
	   IF :old.COLUMN_SUMMARY_ID <> :new.COLUMN_SUMMARY_ID OR 
		 (:old.COLUMN_SUMMARY_ID is null AND :new.COLUMN_SUMMARY_ID is not null) OR 
		 (:new.COLUMN_SUMMARY_ID is null AND :old.COLUMN_SUMMARY_ID is not null) THEN 
		  
		  
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'COLUMN_SUMMARY_ID', 
			''|| 'COLUMN_SUMMARY_ID=' || :new.COLUMN_SUMMARY_ID||'; ', pk_lookup, :old.COLUMN_SUMMARY_ID, :new.COLUMN_SUMMARY_ID, 
			 old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
	   END IF; 
	   IF :old.DESCRIPTION <> :new.DESCRIPTION OR 
		 (:old.DESCRIPTION is null AND :new.DESCRIPTION is not null) OR 
		 (:new.DESCRIPTION is null AND :old.DESCRIPTION is not null) THEN 
		  
		  
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'DESCRIPTION', 
			''|| 'COLUMN_SUMMARY_ID=' || :new.COLUMN_SUMMARY_ID||'; ', pk_lookup, :old.DESCRIPTION, :new.DESCRIPTION, 
			 old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
	   END IF; 
	   IF :old.LONG_DESCRIPTION <> :new.LONG_DESCRIPTION OR 
		 (:old.LONG_DESCRIPTION is null AND :new.LONG_DESCRIPTION is not null) OR 
		 (:new.LONG_DESCRIPTION is null AND :old.LONG_DESCRIPTION is not null) THEN 
		  
		  
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'LONG_DESCRIPTION', 
			''|| 'COLUMN_SUMMARY_ID=' || :new.COLUMN_SUMMARY_ID||'; ', pk_lookup, :old.LONG_DESCRIPTION, :new.LONG_DESCRIPTION, 
			 old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
	   END IF; 
	END IF; 
	IF INSERTING THEN 
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'COLUMN_SUMMARY_ID', 
			''|| 'COLUMN_SUMMARY_ID=' || :new.COLUMN_SUMMARY_ID||'; ', pk_lookup, :new.COLUMN_SUMMARY_ID, 
			 new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'DESCRIPTION', 
			''|| 'COLUMN_SUMMARY_ID=' || :new.COLUMN_SUMMARY_ID||'; ', pk_lookup, :new.DESCRIPTION, 
			 new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'LONG_DESCRIPTION', 
			''|| 'COLUMN_SUMMARY_ID=' || :new.COLUMN_SUMMARY_ID||'; ', pk_lookup, :new.LONG_DESCRIPTION, 
			 new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
	END IF; 
	IF DELETING THEN 
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'COLUMN_SUMMARY_ID', 
			''|| 'COLUMN_SUMMARY_ID=' || :old.COLUMN_SUMMARY_ID||'; ', pk_lookup, :old.COLUMN_SUMMARY_ID, 
			 old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'DESCRIPTION', 
			''|| 'COLUMN_SUMMARY_ID=' || :old.COLUMN_SUMMARY_ID||'; ', pk_lookup, :old.DESCRIPTION, 
			 old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
		  INSERT INTO PP_AUDITS_REG_TRAIL 
			(SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
			 PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
			VALUES 
			(PP_TABLE_AUDIT_SEQ.NEXTVAL, 'REG_COLUMN', 'LONG_DESCRIPTION', 
			''|| 'COLUMN_SUMMARY_ID=' || :old.COLUMN_SUMMARY_ID||'; ', pk_lookup, :old.LONG_DESCRIPTION, 
			 old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
	END IF; 
	END; 
/

drop table REG_ISSUE;

alter table REG_CASE_ADJUST_SUMMARY
drop constraint PK_REG_ANNUAL_ADJUSTED_LEDGER;

alter table REG_CASE_ADJUST_SUMMARY
add constraint PK_REG_CASE_ADJUST_SUMMARY primary key (REG_CASE_ID, REG_ALLOC_ACCT_ID, TEST_YR_ENDED, COLUMN_SUMMARY_ID)
using index tablespace pwrplant_idx;

alter table REG_CASE_ADJUST_SUMMARY modify(RECOVERY_CLASS_ID null);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1142, 0, 10, 4, 2, 6, 37840, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_037840_reg_summary_columns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;