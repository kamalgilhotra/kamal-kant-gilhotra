/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009090_pp_reports.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/25/2012 David Liss    Point Release
||============================================================================
*/

update PP_REPORTS
   set SPECIAL_NOTE = 'DEPR_BL_TOTAL_REPORT_V'
 where DATAWINDOW in ('dw_depr_reserve_blend_report', 'dw_resv_blend_act_fc_s2_sr1_rep');

update PP_REPORTS
   set SPECIAL_NOTE = 'DEPR_BL_LIFE_ONLY_REPORT_V'
 where DATAWINDOW in ('dw_acct_depr_blend_res_life_report');

update PP_REPORTS
   set SPECIAL_NOTE = 'DEPR_BL_COR_ONLY_REPORT_V'
 where DATAWINDOW in ('dw_acct_depr_blend_res_cor_report');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (120, 0, 10, 3, 4, 0, 9090, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009090_pp_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

