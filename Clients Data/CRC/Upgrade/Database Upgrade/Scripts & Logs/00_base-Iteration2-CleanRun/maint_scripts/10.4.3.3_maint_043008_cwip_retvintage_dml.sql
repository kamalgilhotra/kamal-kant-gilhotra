/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043008_cwip_retvintage_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.3.3 02/19/2015 Sunjin Cone        Table Doc clarification
||============================================================================
*/


comment on column WO_ESTIMATE.RETIRE_VINTAGE is 'Non-Editable field with YYYY information that might be saved from OCR interface code used to identify the ASSET_ID based on a specific vintage for the OCR creation. The YYYY information saved in RETIRE_VINTAGE can be useful for the OCR records created as a URET with a specific ASSET_ID to help audit why that particular ASSET_ID was selected in the OCR interface. This field should not be made visible on WO Estimate window.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2311, 0, 10, 4, 3, 3, 43008, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.3_maint_043008_cwip_retvintage_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;