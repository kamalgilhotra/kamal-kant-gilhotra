/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052844_lessor_02_lt_def_profit_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 02/01/2019 Sarah Byers    backfill new account and schedule fields
||============================================================================
*/

-- LSR_ILR_GROUP
update lsr_ilr_group
   set lt_def_sell_profit_account_id = def_selling_profit_account_id
 where lt_def_sell_profit_account_id is null;
 
-- LSR_ILR_ACCOUNT
update lsr_ilr_account
   set lt_def_sell_profit_account_id = def_selling_profit_account_id
 where lt_def_sell_profit_account_id is null;
 
-- Populate new schedule fields
merge into lsr_ilr_schedule_direct_fin a
using (
          select ilr_id, revision, set_of_books_id, month, begin_deferred_profit,
                 nvl(lead(begin_deferred_profit, 12) over (partition by ilr_id, revision, set_of_books_id order by month), 0) begin_lt_deferred_profit,
                 end_deferred_profit,
                 nvl(lead(end_deferred_profit, 12) over (partition by ilr_id, revision, set_of_books_id order by month), 0) end_lt_deferred_profit
            from lsr_ilr_schedule_direct_fin
           order by ilr_id, revision, set_of_books_id, month
      ) b
   on (    a.ilr_id = b.ilr_id
       and a.revision = b.revision
       and a.set_of_books_id = b.set_of_books_id
       and a.month = b.month)
 when matched then
   update set a.begin_lt_deferred_profit = b.begin_lt_deferred_profit, 
              a.end_lt_deferred_profit = b.end_lt_deferred_profit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14903, 0, 2018, 2, 0, 0, 52844, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052844_lessor_02_lt_def_profit_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;