/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010518_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/09/2012 Chris Mardis   Point Release
||============================================================================
*/

update PP_SYSTEM_CONTROL_COMPANY P
   set CONTROL_VALUE = DECODE(LOWER(trim(P.CONTROL_VALUE)),
                               'off',
                               'off',
                               'warn-approved',
                               'warn-approved',
                               'warn-budget',
                               'warn-budget',
                               'restrict-approved',
                               'restrict-approved',
                               'restrict-budget',
                               'restrict-budget',
                               'approved',
                               'restrict-approved',
                               CONTROL_VALUE || '-approved'),
       DESCRIPTION = 'off;warn-approved;warn-budget;restrict-approved;restrict-budget'
 where LOWER(trim(CONTROL_NAME)) like LOWER('FP ESTIMATE RESTRICT');

alter table BUDGET_VERSION_TYPE
   add BUDGET_VERSION_STATUS_ID number(22);

create table BUDGET_VERSION_STATUS
(
 BUDGET_VERSION_STATUS_ID number(22),
 DESCRIPTION              varchar2(35),
 LONG_DESCRIPTION         varchar2(254),
 USER_ID                  varchar2(18),
 TIME_STAMP               date
);

alter table BUDGET_VERSION_STATUS
   add constraint BV_STATUS_PK
       primary key (BUDGET_VERSION_STATUS_ID)
       using index tablespace PWRPLANT_IDX;

alter table BUDGET_VERSION_TYPE
   add constraint BV_TYPE_STATUS_FK
       foreign key (BUDGET_VERSION_STATUS_ID)
       references BUDGET_VERSION_STATUS;

insert into BUDGET_VERSION_STATUS
   (BUDGET_VERSION_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION)
   select 1,
          'Working Version',
          'This is the most up-to-date, in-progress forecast reflecting the latest in-progress forecast information'
     from DUAL
   union all
   select 2,
          'Forecast Version',
          'This is the current locked forecast version reflecting the most recent finalized forecast'
     from DUAL
   union all
   select 3,
          'Previous Forecast Version',
          'This is the previous locked forecast version reflecting the previous finalized forecast'
     from DUAL
   union all
   select 4,
          'Approved Budget Version',
          'This is the approved annual budget reflecting what the Board approved for the year'
     from DUAL
   union all
   select 5,
          'Revised Budget Version',
          'This is the approved annual budget plus any approved transfers, increases, etc.'
     from DUAL;

create or replace view BUDGET_VERSION_VIEW as
select COMPANY_ID,
       WORKING_VERSION,
       FORECAST_VERSION,
       PREVIOUS_FORECAST_VERSION,
       BUDGET_VERSION,
       NVL(REVISED_BUDGET, BUDGET_VERSION) REVISED_BUDGET,
       CURRENT_YEAR,
       ACTUALS_MONTH
  from (select CS.COMPANY_ID COMPANY_ID,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) WORKING_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 2
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 2
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) FORECAST_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 3
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 3
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) PREVIOUS_FORECAST_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 4
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 4
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) BUDGET_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 5
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 5
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) REVISED_BUDGET,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.CURRENT_YEAR)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.CURRENT_YEAR)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) CURRENT_YEAR,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.ACTUALS_MONTH)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.ACTUALS_MONTH)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) ACTUALS_MONTH
          from COMPANY_SETUP CS, PP_SYSTEM_CONTROL_COMPANIES SC
         where SC.COMPANY_ID = CS.COMPANY_ID
           and UPPER(SC.CONTROL_NAME) = 'BV CURRENT VERSION BY COMPANY');


create or replace view WO_EST_ALL_VIEWAPP
   (WORK_ORDER_ID, REVISION, CAPITAL, EXPENSE,REMOVAL, JOBBING, CREDITS)
as
select WO_EST_CAPITAL_VIEWAPP.WORK_ORDER_ID,
       WO_EST_CAPITAL_VIEWAPP.REVISION,
       WO_EST_CAPITAL_VIEWAPP.CAPITAL,
       WO_EST_EXPENSE_VIEWAPP.EXPENSE,
       WO_EST_REMOVAL_VIEWAPP.REMOVAL,
       WO_EST_JOBBING_VIEWAPP.JOBBING,
       WO_EST_CREDITS_VIEWAPP.CREDITS
  from WO_EST_CAPITAL_VIEWAPP,
       WO_EST_EXPENSE_VIEWAPP,
       WO_EST_REMOVAL_VIEWAPP,
       WO_EST_JOBBING_VIEWAPP,
       WO_EST_CREDITS_VIEWAPP
 where WO_EST_CAPITAL_VIEWAPP.WORK_ORDER_ID = WO_EST_CREDITS_VIEWAPP.WORK_ORDER_ID
   and WO_EST_CREDITS_VIEWAPP.WORK_ORDER_ID = WO_EST_EXPENSE_VIEWAPP.WORK_ORDER_ID
   and WO_EST_EXPENSE_VIEWAPP.WORK_ORDER_ID = WO_EST_REMOVAL_VIEWAPP.WORK_ORDER_ID
   and WO_EST_REMOVAL_VIEWAPP.WORK_ORDER_ID = WO_EST_JOBBING_VIEWAPP.WORK_ORDER_ID
   and WO_EST_CAPITAL_VIEWAPP.REVISION = WO_EST_CREDITS_VIEWAPP.REVISION
   and WO_EST_CREDITS_VIEWAPP.REVISION = WO_EST_EXPENSE_VIEWAPP.REVISION
   and WO_EST_EXPENSE_VIEWAPP.REVISION = WO_EST_REMOVAL_VIEWAPP.REVISION
   and WO_EST_REMOVAL_VIEWAPP.REVISION = WO_EST_JOBBING_VIEWAPP.REVISION;

create or replace view WO_EST_ALL_VIEW2APP
   (WORK_ORDER_ID, REVISION, CAPITAL, EXPENSE, REMOVAL, JOBBING, CREDITS)
as
select WO_EST_CAPITAL_VIEW2APP.WORK_ORDER_ID,
       WO_EST_CAPITAL_VIEW2APP.REVISION,
       WO_EST_CAPITAL_VIEW2APP.CAPITAL,
       WO_EST_EXPENSE_VIEW2APP.EXPENSE,
       WO_EST_REMOVAL_VIEW2APP.REMOVAL,
       WO_EST_JOBBING_VIEW2APP.JOBBING,
       WO_EST_CREDITS_VIEW2APP.CREDITS
  from WO_EST_CAPITAL_VIEW2APP,
       WO_EST_EXPENSE_VIEW2APP,
       WO_EST_REMOVAL_VIEW2APP,
       WO_EST_JOBBING_VIEW2APP,
       WO_EST_CREDITS_VIEW2APP
 where WO_EST_CAPITAL_VIEW2APP.WORK_ORDER_ID = WO_EST_CREDITS_VIEW2APP.WORK_ORDER_ID
   and WO_EST_CREDITS_VIEW2APP.WORK_ORDER_ID = WO_EST_EXPENSE_VIEW2APP.WORK_ORDER_ID
   and WO_EST_EXPENSE_VIEW2APP.WORK_ORDER_ID = WO_EST_REMOVAL_VIEW2APP.WORK_ORDER_ID
   and WO_EST_REMOVAL_VIEW2APP.WORK_ORDER_ID = WO_EST_JOBBING_VIEW2APP.WORK_ORDER_ID
   and WO_EST_CAPITAL_VIEW2APP.REVISION = WO_EST_CREDITS_VIEW2APP.REVISION
   and WO_EST_CREDITS_VIEW2APP.REVISION = WO_EST_EXPENSE_VIEW2APP.REVISION
   and WO_EST_EXPENSE_VIEW2APP.REVISION = WO_EST_REMOVAL_VIEW2APP.REVISION
   and WO_EST_REMOVAL_VIEW2APP.REVISION = WO_EST_JOBBING_VIEW2APP.REVISION;


create or replace view WO_EST_ALL_VIEW
   (WORK_ORDER_ID, REVISION, CAPITAL, EXPENSE, REMOVAL, JOBBING, RETIREMENT, CREDITS)
as
select WO_EST_CAPITAL_VIEW.WORK_ORDER_ID,
       WO_EST_CAPITAL_VIEW.REVISION,
       WO_EST_CAPITAL_VIEW.CAPITAL,
       WO_EST_EXPENSE_VIEW.EXPENSE,
       WO_EST_REMOVAL_VIEW.REMOVAL,
       WO_EST_JOBBING_VIEW.JOBBING,
       WO_EST_RETIREMENT_VIEW.RETIREMENT,
       WO_EST_CREDITS_VIEW.CREDITS
  from WO_EST_CAPITAL_VIEW,
       WO_EST_EXPENSE_VIEW,
       WO_EST_REMOVAL_VIEW,
       WO_EST_JOBBING_VIEW,
       WO_EST_RETIREMENT_VIEW,
       WO_EST_CREDITS_VIEW
 where WO_EST_CAPITAL_VIEW.WORK_ORDER_ID = WO_EST_CREDITS_VIEW.WORK_ORDER_ID
   and WO_EST_CREDITS_VIEW.WORK_ORDER_ID = WO_EST_EXPENSE_VIEW.WORK_ORDER_ID
   and WO_EST_EXPENSE_VIEW.WORK_ORDER_ID = WO_EST_REMOVAL_VIEW.WORK_ORDER_ID
   and WO_EST_REMOVAL_VIEW.WORK_ORDER_ID = WO_EST_JOBBING_VIEW.WORK_ORDER_ID
   and WO_EST_REMOVAL_VIEW.WORK_ORDER_ID = WO_EST_RETIREMENT_VIEW.WORK_ORDER_ID
   and WO_EST_CAPITAL_VIEW.REVISION = WO_EST_CREDITS_VIEW.REVISION
   and WO_EST_CREDITS_VIEW.REVISION = WO_EST_EXPENSE_VIEW.REVISION
   and WO_EST_EXPENSE_VIEW.REVISION = WO_EST_REMOVAL_VIEW.REVISION
   and WO_EST_REMOVAL_VIEW.REVISION = WO_EST_JOBBING_VIEW.REVISION
   and WO_EST_REMOVAL_VIEW.REVISION = WO_EST_RETIREMENT_VIEW.REVISION;


create or replace view WO_EST_ALL_VIEW2
   (WORK_ORDER_ID, REVISION, CAPITAL, EXPENSE, REMOVAL, JOBBING, RETIREMENT, CREDITS)
as
select WO_EST_CAPITAL_VIEW2.WORK_ORDER_ID,
       WO_EST_CAPITAL_VIEW2.REVISION,
       WO_EST_CAPITAL_VIEW2.CAPITAL,
       WO_EST_EXPENSE_VIEW2.EXPENSE,
       WO_EST_REMOVAL_VIEW2.REMOVAL,
       WO_EST_JOBBING_VIEW2.JOBBING,
       WO_EST_RETIREMENT_VIEW2.RETIREMENT,
       WO_EST_CREDITS_VIEW2.CREDITS
  from WO_EST_CAPITAL_VIEW2,
       WO_EST_EXPENSE_VIEW2,
       WO_EST_REMOVAL_VIEW2,
       WO_EST_JOBBING_VIEW2,
       WO_EST_RETIREMENT_VIEW2,
       WO_EST_CREDITS_VIEW2
 where WO_EST_CAPITAL_VIEW2.WORK_ORDER_ID = WO_EST_CREDITS_VIEW2.WORK_ORDER_ID
   and WO_EST_CREDITS_VIEW2.WORK_ORDER_ID = WO_EST_EXPENSE_VIEW2.WORK_ORDER_ID
   and WO_EST_EXPENSE_VIEW2.WORK_ORDER_ID = WO_EST_REMOVAL_VIEW2.WORK_ORDER_ID
   and WO_EST_REMOVAL_VIEW2.WORK_ORDER_ID = WO_EST_JOBBING_VIEW2.WORK_ORDER_ID
   and WO_EST_REMOVAL_VIEW2.WORK_ORDER_ID = WO_EST_RETIREMENT_VIEW2.WORK_ORDER_ID
   and WO_EST_CAPITAL_VIEW2.REVISION = WO_EST_CREDITS_VIEW2.REVISION
   and WO_EST_CREDITS_VIEW2.REVISION = WO_EST_EXPENSE_VIEW2.REVISION
   and WO_EST_EXPENSE_VIEW2.REVISION = WO_EST_REMOVAL_VIEW2.REVISION
   and WO_EST_REMOVAL_VIEW2.REVISION = WO_EST_JOBBING_VIEW2.REVISION
   and WO_EST_REMOVAL_VIEW2.REVISION = WO_EST_RETIREMENT_VIEW2.REVISION;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (177, 0, 10, 3, 5, 0, 10518, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010518_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
