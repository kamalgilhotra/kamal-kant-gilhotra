/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052859_lessee_02_default_disc_rate_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 01/14/2019 Shane "C" Ward  New Tenant Level system control for Default Disc Rate Setup
||============================================================================
*/

INSERT INTO pp_system_control_company (control_id, control_name, control_value, description, long_description, company_id, control_type)
SELECT Max(control_id) + 1, 'Lessee Default Disc Rate Map', 0, 'dw_ls_default_disc_level_setup;1', 'Mapping Level of Default Discount Rate Types (Lease Group[1] vs ILR Group[2])', -1, 'system only'
FROM pp_system_control_company;

INSERT INTO ppbase_workspace (MODULE, workspace_identifier, label, workspace_uo_name, object_Type_id)
VALUES ('LESSEE', 'admin_disc_rate', 'Default Disc Rate', 'uo_ls_default_disc_rate_wksp', 1);

UPDATE ppbase_menu_Items SET item_order = item_order + 1 WHERE item_order > 8 AND MODULE = 'LESSEE' and parent_menu_identifier = 'menu_wksp_admin' ;

INSERT INTO ppbase_menu_items (MODULE, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
VALUES ('LESSEE', 'admin_disc_rate', 2, 9, 'Default Discount Rate', 'menu_wksp_admin', 'admin_disc_rate', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(14103, 0, 2018, 2, 0, 0, 52859, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052859_lessee_02_default_disc_rate_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;