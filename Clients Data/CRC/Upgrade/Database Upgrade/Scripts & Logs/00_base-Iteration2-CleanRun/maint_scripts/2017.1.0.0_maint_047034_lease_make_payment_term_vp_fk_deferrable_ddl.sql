/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047034_lease_make_payment_term_vp_fk_deferrable_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/06/2017 Charlie Shilling make the constraint deferrable
||============================================================================
*/
ALTER TABLE ls_ilr_payment_term_var_paymnt
	DROP CONSTRAINT ls_ilr_pay_term_var_paymnt_fk1;

ALTER TABLE ls_ilr_payment_term_var_paymnt
  ADD CONSTRAINT ls_ilr_pay_term_var_paymnt_fk1 FOREIGN KEY (
    ilr_id,
    revision,
    payment_term_id
  ) REFERENCES ls_ilr_payment_term (
    ilr_id,
    revision,
    payment_term_id
  )
  DEFERRABLE INITIALLY IMMEDIATE
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3423, 0, 2017, 1, 0, 0, 47034, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047034_lease_make_payment_term_vp_fk_deferrable_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	