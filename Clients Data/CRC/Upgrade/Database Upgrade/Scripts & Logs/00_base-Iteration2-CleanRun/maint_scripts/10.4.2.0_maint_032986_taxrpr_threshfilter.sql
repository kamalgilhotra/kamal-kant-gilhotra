/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032986_taxrpr_threshfilter.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/10/2013 Alex P.        Point Release
||============================================================================
*/

update PP_DYNAMIC_FILTER
   set SQLS_WHERE_CLAUSE = 'repair_thresholds.company_id in ( select c.company_id from company c where [selected_values] union select -1 from dual )'
 where FILTER_ID = 51
   and LABEL = 'Company';

update PP_DYNAMIC_FILTER_MAPPING
   set FILTER_ID = 51
 where PP_REPORT_FILTER_ID = 31
   and FILTER_ID = 13;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (684, 0, 10, 4, 2, 0, 32986, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032986_taxrpr_threshfilter.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
