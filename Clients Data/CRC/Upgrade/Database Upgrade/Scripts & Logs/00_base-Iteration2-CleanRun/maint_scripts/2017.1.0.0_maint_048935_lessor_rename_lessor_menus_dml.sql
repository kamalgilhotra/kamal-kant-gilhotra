/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048935_lessor_rename_lessor_menus_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/08/2017 Josh Sandler     Rename all lessor menus with new lessor UO/window names
||============================================================================
*/

UPDATE ppbase_workspace
SET workspace_uo_name = REPLACE(workspace_uo_name,'uo_ls_','uo_lsr_')
WHERE MODULE = 'LESSOR'
AND workspace_uo_name LIKE 'uo_ls\_%' ESCAPE '\';

UPDATE ppbase_workspace
SET workspace_uo_name = REPLACE(workspace_uo_name,'w_ls_','w_lsr_')
WHERE MODULE = 'LESSOR'
AND workspace_uo_name LIKE 'w_ls\_%' ESCAPE '\';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3693, 0, 2017, 1, 0, 0, 48935, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048935_lessor_rename_lessor_menus_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;