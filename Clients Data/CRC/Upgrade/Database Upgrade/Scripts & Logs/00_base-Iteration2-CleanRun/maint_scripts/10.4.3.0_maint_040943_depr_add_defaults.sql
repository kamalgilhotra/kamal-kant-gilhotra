/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040943_depr_add_defaults.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.3.0   10/26/2014 Charlie Shilling maint-40943
||============================================================================
*/
alter table FCST_DEPR_LEDGER
   modify (COR_BLENDING_ADJUSTMENT        number(22,2) default 0,
           COR_BLENDING_TRANSFER          number(22,2) default 0,
           IMPAIRMENT_ASSET_AMOUNT        number(22,2) default 0,
           IMPAIRMENT_EXPENSE_AMOUNT      number(22,2) default 0,
           RESERVE_BAL_IMPAIRMENT         number(22,2) default 0,
           RESERVE_BAL_SALVAGE_EXP        number(22,2) default 0,
           RESERVE_BLENDING_ADJUSTMENT    number(22,2) default 0,
           RESERVE_BLENDING_TRANSFER      number(22,2) default 0,
           RWIP_COST_OF_REMOVAL           number(22,2) default 0,
           RWIP_RESERVE_CREDITS           number(22,2) default 0,
           RWIP_SALVAGE_CASH              number(22,2) default 0,
           RWIP_SALVAGE_RETURNS           number(22,2) default 0,
           SALVAGE_BASE                   number(22,2) default 0,
           SALVAGE_EXPENSE                number(22,2) default 0,
           SALVAGE_EXP_ADJUST             number(22,2) default 0,
           SALVAGE_EXP_ALLOC_ADJUST       number(22,2) default 0,
           SALVAGE_RATE                   number(22,8) default 0);

update fcst_depr_ledger
set begin_reserve = nvl(begin_reserve,0),
   end_reserve = nvl(end_reserve,0),
   reserve_bal_provision = nvl(reserve_bal_provision,0),
   reserve_bal_cor = nvl(reserve_bal_cor,0),
   salvage_balance = nvl(salvage_balance,0),
   reserve_bal_adjust = nvl(reserve_bal_adjust,0),
   reserve_bal_retirements = nvl(reserve_bal_retirements,0),
   reserve_bal_tran_in = nvl(reserve_bal_tran_in,0),
   reserve_bal_tran_out = nvl(reserve_bal_tran_out,0),
   reserve_bal_other_credits = nvl(reserve_bal_other_credits,0),
   reserve_bal_gain_loss = nvl(reserve_bal_gain_loss,0),
   reserve_alloc_factor = nvl(reserve_alloc_factor,0),
   begin_balance = nvl(begin_balance,0),
   additions = nvl(additions,0),
   retirements = nvl(retirements,0),
   transfers_in = nvl(transfers_in,0),
   transfers_out = nvl(transfers_out,0),
   adjustments = nvl(adjustments,0),
   depreciation_base = nvl(depreciation_base,0),
   end_balance = nvl(end_balance,0),
   depreciation_rate = nvl(depreciation_rate,0),
   depreciation_expense = nvl(depreciation_expense,0),
   depr_exp_adjust = nvl(depr_exp_adjust,0),
   depr_exp_alloc_adjust = nvl(depr_exp_alloc_adjust,0),
   cost_of_removal = nvl(cost_of_removal,0),
   reserve_retirements = nvl(reserve_retirements,0),
   salvage_returns = nvl(salvage_returns,0),
   salvage_cash = nvl(salvage_cash,0),
   reserve_credits = nvl(reserve_credits,0),
   reserve_adjustments = nvl(reserve_adjustments,0),
   reserve_tran_in = nvl(reserve_tran_in,0),
   reserve_tran_out = nvl(reserve_tran_out,0),
   gain_loss = nvl(gain_loss,0),
   vintage_net_salvage_amort = nvl(vintage_net_salvage_amort,0),
   vintage_net_salvage_reserve = nvl(vintage_net_salvage_reserve,0),
   current_net_salvage_amort = nvl(current_net_salvage_amort,0),
   current_net_salvage_reserve = nvl(current_net_salvage_reserve,0),
   impairment_reserve_beg = nvl(impairment_reserve_beg,0),
   impairment_reserve_act = nvl(impairment_reserve_act,0),
   impairment_reserve_end = nvl(impairment_reserve_end,0),
   est_ann_net_adds = nvl(est_ann_net_adds,0),
   rwip_allocation = nvl(rwip_allocation,0),
   new_depreciation = nvl(new_depreciation,0),
   new_additions = nvl(new_additions,0),
   tax_basis_adjustments = nvl(tax_basis_adjustments,0),
   tax_basis_adjust_norm = nvl(tax_basis_adjust_norm,0),
   cor_beg_reserve = nvl(cor_beg_reserve,0),
   cor_expense = nvl(cor_expense,0),
   cor_exp_adjust = nvl(cor_exp_adjust,0),
   cor_res_tran_in = nvl(cor_res_tran_in,0),
   cor_res_tran_out = nvl(cor_res_tran_out,0),
   cor_res_adjust = nvl(cor_res_adjust,0),
   cor_exp_alloc_adjust = nvl(cor_exp_alloc_adjust,0),
   cost_of_removal_base = nvl(cost_of_removal_base,0),
   cost_of_removal_rate = nvl(cost_of_removal_rate,0),
   cor_end_reserve = nvl(cor_end_reserve,0),
   impairment_asset_activity_salv = nvl(impairment_asset_activity_salv,0),
   impairment_asset_begin_balance = nvl(impairment_asset_begin_balance,0),
   cor_blending_adjustment = nvl(cor_blending_adjustment,0),
   cor_blending_transfer = nvl(cor_blending_transfer,0),
   impairment_asset_amount = nvl(impairment_asset_amount,0),
   impairment_expense_amount = nvl(impairment_expense_amount,0),
   reserve_bal_impairment = nvl(reserve_bal_impairment,0),
   reserve_bal_salvage_exp = nvl(reserve_bal_salvage_exp,0),
   reserve_blending_adjustment = nvl(reserve_blending_adjustment,0),
   reserve_blending_transfer = nvl(reserve_blending_transfer,0),
   rwip_cost_of_removal = nvl(rwip_cost_of_removal,0),
   rwip_reserve_credits = nvl(rwip_reserve_credits,0),
   rwip_salvage_cash = nvl(rwip_salvage_cash,0),
   rwip_salvage_returns = nvl(rwip_salvage_returns,0),
   salvage_base = nvl(salvage_base,0),
   salvage_expense = nvl(salvage_expense,0),
   salvage_exp_adjust = nvl(salvage_exp_adjust,0),
   salvage_exp_alloc_adjust = nvl(salvage_exp_alloc_adjust,0),
   salvage_rate = nvl(salvage_rate,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1584, 0, 10, 4, 3, 0, 40943, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040943_depr_add_defaults.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
