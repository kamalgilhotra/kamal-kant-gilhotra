/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036022_reg_z_adj_dollars.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 02/27/2014 Sarah Byers
||============================================================================
*/

alter table REG_CASE_MONITOR_CONTROL
   add (ADJUSTED_DOLLARS number(22,0),
        CONTROL_ORDER    number(22,0));

comment on column REG_CASE_MONITOR_CONTROL.ADJUSTED_DOLLARS is '0 indicates that unadjusted dollars will be used for the return tag; 1 indicates that adjusted dollars will be used for the return tag. 1 is the default.';
comment on column REG_CASE_MONITOR_CONTROL.CONTROL_ORDER is 'User identified display order.';

alter table REG_JUR_MONITOR_CONTROL
   add (ADJUSTED_DOLLARS number(22,0),
        CONTROL_ORDER    number(22,0));

comment on column REG_JUR_MONITOR_CONTROL.ADJUSTED_DOLLARS is '0 indicates that unadjusted dollars will be used for the return tag; 1 indicates that adjusted dollars will be used for the return tag. 1 is the default.';
comment on column REG_JUR_MONITOR_CONTROL.CONTROL_ORDER is 'User identified display order.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1001, 0, 10, 4, 2, 0, 36022, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036022_reg_z_adj_dollars.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;