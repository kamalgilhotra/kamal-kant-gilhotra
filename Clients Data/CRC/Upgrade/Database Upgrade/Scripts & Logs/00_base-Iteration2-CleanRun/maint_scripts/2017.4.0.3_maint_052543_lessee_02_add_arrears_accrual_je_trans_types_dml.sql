/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052543_lessee_02_add_arrears_accrual_je_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.3  10/22/2018 Sarah Byers      Add new trans types for Purchase option gain/loss
||============================================================================
*/

-- Add new trans type
insert into je_trans_type (trans_type, description)
select 3079, '3079 - Arrears Accrual'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3079);

-- Add to je_method_trans_type
insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3079
   and not exists (select 1 from je_method_trans_type where trans_type = 3079 and je_method_id = 1);
   
-- Populate new payment_calc_month field on ls_payment_hdr with payment_month
update ls_payment_hdr
   set payment_calc_month = coalesce(payment_month, gl_posting_mo_yr);
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10823, 0, 2017, 4, 0, 3, 52543, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052543_lessee_02_add_arrears_accrual_je_trans_types_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;