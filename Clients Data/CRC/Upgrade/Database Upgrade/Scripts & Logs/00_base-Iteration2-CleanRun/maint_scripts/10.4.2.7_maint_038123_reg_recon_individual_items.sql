/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038123_reg_recon_individual_items.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/21/2014 Ryan Oliveria
||============================================================================
*/

alter table REG_HISTORIC_RECON_LEDGER add RECONCILED date;
alter table REG_FORECAST_RECON_LEDGER add RECONCILED date;

comment on column REG_HISTORIC_RECON_LEDGER.RECONCILED is 'Date that this individual item was reconciled.';
comment on column REG_FORECAST_RECON_LEDGER.RECONCILED is 'Date that this individual item was reconciled.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1396, 0, 10, 4, 2, 7, 38123, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038123_reg_recon_individual_items.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;