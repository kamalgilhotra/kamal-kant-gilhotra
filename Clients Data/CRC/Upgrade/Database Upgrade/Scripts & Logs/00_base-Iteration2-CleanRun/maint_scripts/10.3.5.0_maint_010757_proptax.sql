/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010757_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/23/2012 Alan Sandusky  Point Release
||============================================================================
*/

alter table PT_TEMP_ACCRUAL_PRIOR_MONTH add ACCRUAL_TYPE_ID number(22,0);

drop index PT_TEMP_ACCRUAL_PRIOR_MONTH_PK;

create unique index PT_TEMP_ACCRUAL_PRIOR_MONTH_PK
   on PT_TEMP_ACCRUAL_PRIOR_MONTH (PROPERTY_TAX_LEDGER_ID, TAX_AUTHORITY_ID, ACCRUAL_TYPE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (205, 0, 10, 3, 5, 0, 10757, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010757_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
