/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_sysweb_03_triggers.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/24/2014 Chad Theilman    Web Framework - Triggers
||============================================================================
*/

declare 
  begin
    execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STRING" 
                       before update or insert on "PWRPLANT"."PP_WEB_LOCALIZATION_STRING"
                       for each row
                    BEGIN
                      :new.user_id := USER;
                      :new.time_stamp := SYSDATE;
                    END;';
          
     execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STRING_REV"
                      BEFORE DELETE OR INSERT OR UPDATE ON "PWRPLANT"."PP_WEB_LOCALIZATION_STRING"
                      FOR EACH ROW
                    DECLARE
                    BEGIN
                       UPDATE PP_WEB_LOCALIZATION
                          SET REVISION = REVISION + 1
                        WHERE TRIM(LOWER(LOCALE)) = TRIM(LOWER(NVL(:new.locale, :old.locale)));
                    END;';
                          
      execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STR" before
                        UPDATE OR
                        INSERT ON PWRPLANT."PP_WEB_LOCALIZATION_STRING2" FOR EACH row BEGIN :new.user_id := USER;
                        :new.time_stamp                                                                  := SYSDATE;
                      END;';
                      
                           
      execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STR_REV" BEFORE
                        DELETE OR
                        INSERT OR
                        UPDATE ON "PWRPLANT"."PP_WEB_LOCALIZATION_STRING2" FOR EACH ROW DECLARE BEGIN
                        UPDATE PP_WEB_LOCALIZATION_REVISION
                        SET REVISION_DATE         = SYSDATE
                        WHERE TRIM(LOWER(LOCALE)) = TRIM(LOWER(NVL(:new.locale, :old.locale)));
                        END;';
     
       execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_VIEW_CHANGE_ID"
                        BEFORE INSERT ON "PWRPLANT"."PP_WEB_VIEW_CHANGE"
                        FOR EACH ROW
                        BEGIN
                            SELECT pwrplant.PP_WEB_VIEW_CHANGE_SEQ.nextval into :new.CHANGE_ID 
                           FROM dual;
                        END;';
                        
     execute immediate 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_WEB_VIEW_SESSION_ID
                BEFORE INSERT ON PWRPLANT.pp_web_view_session
                FOR EACH ROW
                BEGIN
                  SELECT pwrplant.pp_web_view_session_seq.nextval into :new.session_id 
                 FROM dual;
                END;';
                
      execute immediate 'CREATE OR REPLACE TRIGGER pwrplant.pp_web_view_session_audit
                 before update or insert on pwrplant.pp_web_view_session
                 for each row
                BEGIN
                :new.user_id := USER;    
                :new.time_stamp := SYSDATE; 
                END;';
       
      execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_VIEW_CHANGE_AUDIT"
                 before update or insert on PWRPLANT.PP_WEB_VIEW_CHANGE
                 for each row
                BEGIN
                :new.user_id := USER;    
                :new.time_stamp := SYSDATE; 
                END;';
                
      execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STRING" 
                 before update or insert on "PWRPLANT"."PP_WEB_LOCALIZATION_STRING"
                 for each row
                BEGIN
                :new.user_id := USER;
                :new.time_stamp := SYSDATE;
                END;';
                
      
                
      execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STRING_REV"
                BEFORE DELETE OR INSERT OR UPDATE ON "PWRPLANT"."PP_WEB_LOCALIZATION_STRING"
                FOR EACH ROW
                DECLARE
                BEGIN
                 UPDATE pwrplant.PP_WEB_LOCALIZATION
                  SET REVISION = REVISION + 1
                  WHERE TRIM(LOWER(LOCALE)) = TRIM(LOWER(NVL(:new.locale, :old.locale)));
                END;';
    
     execute immediate 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_SECURITY_PERMISSION_A" 
                             before update or insert on PWRPLANT."PP_WEB_SECURITY_PERMISSION"
                             for each row
                          BEGIN
                            :new.user_id := USER;    
                            :new.time_stamp := SYSDATE; 
                          END;';

      
      execute immediate 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_WEB_SECURITY_PERMISSION_R
                          AFTER INSERT OR UPDATE OR DELETE ON PP_WEB_SECURITY_PERMISSION
                          FOR EACH ROW
                        BEGIN
                          IF NVL(:OLD.PRINCIPAL_TYPE, :NEW.PRINCIPAL_TYPE) = ''U'' THEN
                            DELETE FROM PP_WEB_SECURITY_REVISION
                             WHERE "USERS" = NVL(:OLD."PRINCIPAL", :NEW."PRINCIPAL");
                           
                            INSERT INTO PP_WEB_SECURITY_REVISION ("USERS", "REVISION_DATE")
                              VALUES(NVL(:OLD."PRINCIPAL", :NEW."PRINCIPAL"), SYSDATE);
                              
                         ELSIF NVL(:OLD.PRINCIPAL_TYPE, :NEW.PRINCIPAL_TYPE) = ''G'' THEN 
                           FOR rec IN (SELECT "USERS" FROM PP_SECURITY_USERS_GROUPS WHERE "GROUPS" = NVL(:OLD."PRINCIPAL", :NEW."PRINCIPAL"))
                            LOOP
                              DELETE FROM PP_WEB_SECURITY_REVISION
                               WHERE "USERS" =  rec."USERS";
                           
                              INSERT INTO PP_WEB_SECURITY_REVISION ("USERS", "REVISION_DATE")
                                VALUES(rec."USERS", SYSDATE);
                              END LOOP;
                          END IF;  
                        END;';
                
  end;
  /

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1568, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_sysweb_03_triggers.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
