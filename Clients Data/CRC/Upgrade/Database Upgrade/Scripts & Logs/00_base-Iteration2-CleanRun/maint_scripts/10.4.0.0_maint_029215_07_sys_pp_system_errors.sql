/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029215_07_sys_pp_system_errors.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- --------------   ------------------------------------
|| 10.4.0.0   02/05/2013 Charlie Shilling Point Release
||============================================================================
*/

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1019',
    'POST 1019: Post detected that this transaction is missing records in pend_transaction_set_of_books for one or more set_of_books, but could not create data for the missing sets_of_books.',
    'POST 1019: ERROR: This transaction is missing one or more set_of_books from pend_transaction_set_of_books.',
    'POST 1019: Review the Post logs for any indication of why Post could not create records for the missing sets_of_books. Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1020',
    'POST 1020: ERROR: Post detected missing records in pend_transaction_set_of_books and has created records for the missing sets_of_books.',
    'POST 1020: ERROR: Post created new set_of_books data for this transaction. Please review and re-approve to Post.',
    'POST 1020: Review the new set_of_books data and reapprove the transaction to Post.');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (289, 0, 10, 4, 0, 0, 29215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029215_07_sys_pp_system_errors.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;