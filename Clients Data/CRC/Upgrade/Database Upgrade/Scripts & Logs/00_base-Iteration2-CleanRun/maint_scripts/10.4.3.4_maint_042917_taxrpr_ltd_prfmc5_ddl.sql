--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042917_taxrpr_ltd_prfmc5_ddl.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 10.4.3.4   03/13/2015 A Scott          Improve LTD performance. datatype change on table.
--||============================================================================
--*/

alter table TEMP_WOS_IN_SCHEMA
modify blanket_method VARCHAR2(35);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2375, 0, 10, 4, 3, 4, 42917, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.4_maint_042917_taxrpr_ltd_prfmc5_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;