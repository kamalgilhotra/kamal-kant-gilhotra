/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051474_lessor_01_add_termination_status_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/07/2018 Anand R        Add Lessor Termination statuses 
||============================================================================
*/

-- Creates 2 new statuses for ILR 
insert into ls_ilr_status (ilr_status_id, description, long_description)
values (4, 'Pending Termination', 'Pending Termination');  

insert into ls_ilr_status (ilr_status_id, description, long_description)
values (5, 'Terminated', 'Terminated');

-- Creates 2 new statuses for ILR Revision*/    
insert into approval_status (approval_status_id, description)
values (8, 'Pending Termination') ;

insert into approval_status (approval_status_id, description)
values (9, 'Terminated');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(6642, 0, 2017, 4, 0, 0, 51474, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051474_lessor_01_add_termination_status_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;