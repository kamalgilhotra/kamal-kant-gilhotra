/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047037_lease_fix_vc_month_number_coltype_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/23/2017 Jared Watkins    update the incurred_month_number for the VC import 
||                                        stg table to accept arbitrary date strings during the insert to stg table
||============================================================================
*/

--fix the Variable Component import to be able to import arbitrary date strings 
--    and then error if the formatting is wrong during the validation
alter table ls_import_vp_var_comp_amt
modify incurred_month_number varchar2(254);

alter table ls_import_vp_var_comp_amt_arch
modify incurred_month_number varchar2(254);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3511, 0, 2017, 1, 0, 0, 47037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047037_lease_fix_vc_month_number_coltype_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;