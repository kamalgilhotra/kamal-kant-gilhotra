/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049462_lessee_02_ilr_import_update_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/12/2017 Shane "C" Ward    Updates to Lessee ILR Import
||============================================================================
*/
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 252,
             'sublease_flag',
             'Sublease',
             'sublease_flag_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 252,
             'sublease_id',
             'Sublease Lease',
             'sublease_id_xlate',
             0,
             1,
             'number(22,0)',
             'lsr_lease',
             1,
             'lease_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 252,
             'intent_to_purch',
             'Intent to Purchase',
             'intent_to_purch_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 252,
             'specialized_asset',
             'Specialized Asset',
             'specialized_asset_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 252,
             'intercompany_lease',
             'Intercompany Lease',
             'intercompany_lease_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 252,
             'intercompany_company',
             'Intercompany Lease Company',
             'intercompany_company_xlate',
             0,
             1,
             'number(22,0)',
             'company_setup',
             1,
             'company_id');

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'intent_to_purch',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'intent_to_purch',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'intercompany_company',
             19);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'intercompany_company',
             20);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'intercompany_lease',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'intercompany_lease',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'specialized_asset',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'specialized_asset',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'sublease_flag',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'sublease_flag',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'sublease_id',
             1083);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'sublease_id',
             1084);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 252,
             'sublease_id',
             1085);

--PP_IMPORT_TEMPLATE_FIELDS
UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = field_id + 6
WHERE  import_type_id = 252 AND
       import_template_id IN
       ( SELECT import_template_id
         FROM   PP_IMPORT_TEMPLATE
         WHERE  description = 'ILR Add' ) AND
       field_id > 9;

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       10,
       252,
       'intent_to_purch',
       77
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 252 AND
       description = 'ILR Add';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       11,
       252,
       'specialized_asset',
       77
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 252 AND
       description = 'ILR Add';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       12,
       252,
       'sublease_flag',
       77
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 252 AND
       description = 'ILR Add';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       13,
       252,
       'sublease_id',
       1083
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 252 AND
       description = 'ILR Add';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       14,
       252,
       'intercompany_lease',
       77
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 252 AND
       description = 'ILR Add';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       15,
       252,
       'intercompany_company',
       19
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 252 AND
       description = 'ILR Add';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(3939, 0, 2017, 1, 0, 0, 49462, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049462_lessee_02_ilr_import_update_dml.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
