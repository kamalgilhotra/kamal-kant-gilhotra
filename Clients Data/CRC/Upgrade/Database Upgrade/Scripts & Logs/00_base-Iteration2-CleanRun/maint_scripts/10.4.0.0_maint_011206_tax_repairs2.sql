/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs2.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   02/11/2013 Blake Andrews
||============================================================================
*/

insert into REPAIR_SCHEMA
   (REPAIR_SCHEMA_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS, USAGE_FLAG)
   select RPR_REPAIR_SCHEMA_SEQ.NEXTVAL,
          'Unused RS for ' || UF_VIEW.USAGE_FLAG,
          'Used to create ''Exclude from Testing'' repair test entries',
          2,
          UF_VIEW.USAGE_FLAG
     from (select 'BLANKET' USAGE_FLAG
              from DUAL
            union
            select 'CPR'
              from DUAL
            union
            select 'CWIP'
              from DUAL
            union
            select 'CWIP_CPR'
              from DUAL
            union
            select 'WMIS' from DUAL) UF_VIEW;

insert into WO_TAX_EXPENSE_TEST
   (TAX_EXPENSE_TEST_ID, REPAIR_SCHEMA_ID, ALWAYS_CAPITAL, DESCRIPTION)
   select RPR_TAX_EXPENSE_TEST_SEQ.NEXTVAL,
          REPAIR_SCHEMA_ID,
          1,
          'Exclude from Test: ' || USAGE_FLAG
     from REPAIR_SCHEMA
    where LONG_DESCRIPTION = 'Used to create ''Exclude from Testing'' repair test entries';




update WORK_ORDER_TAX_REPAIRS
   set TAX_EXPENSE_TEST_ID =
        (select TAX_EXPENSE_TEST_ID
           from WO_TAX_EXPENSE_TEST
          where ALWAYS_CAPITAL = 1
            and REPAIR_SCHEMA_ID in
                (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'BLANKET'))
 where WORK_ORDER_ID in
       (select WORK_ORDER_ID
          from WORK_ORDER_ACCOUNT
         where REPAIR_EXCLUDE = 1
           and TAX_EXPENSE_TEST_ID in
               (select TAX_EXPENSE_TEST_ID
                  from WO_TAX_EXPENSE_TEST
                 where REPAIR_SCHEMA_ID in
                       (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'BLANKET')));
					   
update WORK_ORDER_TAX_REPAIRS
   set TAX_EXPENSE_TEST_ID =
        (select TAX_EXPENSE_TEST_ID
           from WO_TAX_EXPENSE_TEST
          where ALWAYS_CAPITAL = 1
            and REPAIR_SCHEMA_ID in
                (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'CPR'))
 where WORK_ORDER_ID in
       (select WORK_ORDER_ID
          from WORK_ORDER_ACCOUNT
         where REPAIR_EXCLUDE = 1
           and TAX_EXPENSE_TEST_ID in
               (select TAX_EXPENSE_TEST_ID
                  from WO_TAX_EXPENSE_TEST
                 where REPAIR_SCHEMA_ID in
                       (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'CPR')));	

update WORK_ORDER_TAX_REPAIRS
   set TAX_EXPENSE_TEST_ID =
        (select TAX_EXPENSE_TEST_ID
           from WO_TAX_EXPENSE_TEST
          where ALWAYS_CAPITAL = 1
            and REPAIR_SCHEMA_ID in
                (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'CWIP'))
 where WORK_ORDER_ID in
       (select WORK_ORDER_ID
          from WORK_ORDER_ACCOUNT
         where REPAIR_EXCLUDE = 1
           and TAX_EXPENSE_TEST_ID in
               (select TAX_EXPENSE_TEST_ID
                  from WO_TAX_EXPENSE_TEST
                 where REPAIR_SCHEMA_ID in
                       (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'CWIP')));
				
update WORK_ORDER_TAX_REPAIRS
   set TAX_EXPENSE_TEST_ID =
        (select TAX_EXPENSE_TEST_ID
           from WO_TAX_EXPENSE_TEST
          where ALWAYS_CAPITAL = 1
            and REPAIR_SCHEMA_ID in
                (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'CWIP_CPR'))
 where WORK_ORDER_ID in
       (select WORK_ORDER_ID
          from WORK_ORDER_ACCOUNT
         where REPAIR_EXCLUDE = 1
           and TAX_EXPENSE_TEST_ID in
               (select TAX_EXPENSE_TEST_ID
                  from WO_TAX_EXPENSE_TEST
                 where REPAIR_SCHEMA_ID in
                       (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'CWIP_CPR')));				

update WORK_ORDER_TAX_REPAIRS
   set TAX_EXPENSE_TEST_ID =
        (select TAX_EXPENSE_TEST_ID
           from WO_TAX_EXPENSE_TEST
          where ALWAYS_CAPITAL = 1
            and REPAIR_SCHEMA_ID in
                (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'WMIS'))
 where WORK_ORDER_ID in
       (select WORK_ORDER_ID
          from WORK_ORDER_ACCOUNT
         where REPAIR_EXCLUDE = 1
           and TAX_EXPENSE_TEST_ID in
               (select TAX_EXPENSE_TEST_ID
                  from WO_TAX_EXPENSE_TEST
                 where REPAIR_SCHEMA_ID in
                       (select REPAIR_SCHEMA_ID from REPAIR_SCHEMA where UPPER(USAGE_FLAG) = 'WMIS')));				
					   					   

commit;

insert into WO_TAX_STATUS
   (TAX_STATUS_ID, DESCRIPTION, PRIORITY)
   select RPR_TAX_STATUS_SEQ.NEXTVAL MAX_ID, 'Capital - Exclude from Test', 100
     from DUAL
commit;

update WO_TAX_EXPENSE_TEST
   set TAX_STATUS_DEFAULT_CAPITAL =
        (select min(TAX_STATUS_ID)
           from WO_TAX_STATUS
          where DESCRIPTION = 'Capital - Exclude from Test');

alter table WORK_ORDER_TAX_STATUS add TEST_EXPLANATION varchar2(254);


--
-- Remove priority from repair_schema.
--
alter table REPAIR_SCHEMA drop column PRIORITY;


--
-- Add new workspace for updating work_order_tax_repairs.
--    First, create a new parent level for this and Set Tax Status.  Re-order the other workspaces at the 'repairs_top' level.
--    Then move Set Tax Status under this new parent, and add the new Assign Tax Test workspace.
--    Finally, update the processing menu item so that it calls the Calculate Dollars workspace.
--
update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 1
 where PARENT_IDENTIFIER = 'repairs_top'
   and ITEM_ORDER >= 3;
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_wo_setup', 'repairs_top', 3, 'Work Order', 'Work Order Setup', 1, 1,
    'menu_arrow_right.png', null, null, null, null, null);

update PPBASE_MENU_ITEMS
   set IDENTIFIER = 'menu_wksp_set_tax_status', PARENT_IDENTIFIER = 'menu_wksp_wo_setup',
       ITEM_ORDER = 2, LABEL = 'Set Tax Status', MENU_LEVEL = 2
 where MODULE = 'REPAIRS'
   and UO_WKSP_NAME = 'uo_rpr_process_wksp_wo_status';
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_assign_tax_test', 'menu_wksp_wo_setup', 1, 'Assign Tax Test',
    'Assign Work Order Tax Test', 1, 2, null, 'uo_rpr_process_wksp_wo_tax_repairs', null, null, null,
    null);

update PPBASE_MENU_ITEMS
   set PICTURE_NAME = null, UO_WKSP_NAME = 'uo_rpr_process_wksp_main'
 where MODULE = 'REPAIRS'
   and IDENTIFIER = 'menu_wksp_processing';
delete from PPBASE_MENU_ITEMS
 where MODULE = 'REPAIRS'
   and IDENTIFIER = 'menu_wksp_processing_main';

--
-- Add repair_loc_rollup_id to work_order_tax_repairs.
-- Also, add a foreign key for repair_location_id.
--
alter table WORK_ORDER_TAX_REPAIRS add REPAIR_LOC_ROLLUP_ID number(22,0);

alter table WORK_ORDER_TAX_REPAIRS
   add constraint WO_TAX_REP_LOCATION_FK
       foreign key (REPAIR_LOCATION_ID)
       references REPAIR_LOCATION;

alter table WORK_ORDER_TAX_REPAIRS
   add constraint WO_TAX_REP_LOC_ROLL_FK
       foreign key (REPAIR_LOC_ROLLUP_ID)
       references REPAIR_LOC_ROLLUP;

--
-- Make tax_status_id and high_range required on repair_range_test.
--

alter table repair_range_test modify TAX_STATUS_ID not null;
alter table repair_range_test modify HIGH_RANGE not null;

--
-- Change the way priorities are stored on tax expense tests.  They were previously stored as comma-separated strings,
-- which allow for user error.  Make a table of valid priorities and allow the user to relate them to the tax expense tests.
--

-- Create a table to store the valid list of priorities.  This will be a fixed table.
create table REPAIR_PRIORITY
(
 REPAIR_PRIORITY_ID   number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(35)   not null,
 REPAIR_PRIORITY_CODE varchar2(35)
);

alter table REPAIR_PRIORITY
   add constraint RPR_PRIORITY_PK
       primary key (REPAIR_PRIORITY_ID)
       using index tablespace PWRPLANT_IDX;

insert into REPAIR_PRIORITY (REPAIR_PRIORITY_ID, DESCRIPTION, REPAIR_PRIORITY_CODE) values ( 1, 'External', 'EXT' );
insert into REPAIR_PRIORITY (REPAIR_PRIORITY_ID, DESCRIPTION, REPAIR_PRIORITY_CODE) values ( 2, 'Funding Project', 'FP' );
insert into REPAIR_PRIORITY (REPAIR_PRIORITY_ID, DESCRIPTION, REPAIR_PRIORITY_CODE) values ( 3, 'Property Group', 'PG' );
insert into REPAIR_PRIORITY (REPAIR_PRIORITY_ID, DESCRIPTION, REPAIR_PRIORITY_CODE) values ( 4, 'Replacment Check', 'RC' );
insert into REPAIR_PRIORITY (REPAIR_PRIORITY_ID, DESCRIPTION, REPAIR_PRIORITY_CODE) values ( 5, 'Utility Account Property Unit', 'UAPU' );
insert into REPAIR_PRIORITY (REPAIR_PRIORITY_ID, DESCRIPTION, REPAIR_PRIORITY_CODE) values ( 6, 'User', 'USR' );
insert into REPAIR_PRIORITY (REPAIR_PRIORITY_ID, DESCRIPTION, REPAIR_PRIORITY_CODE) values ( 7, 'Work Order Type', 'WOT' );
insert into REPAIR_PRIORITY (REPAIR_PRIORITY_ID, DESCRIPTION, REPAIR_PRIORITY_CODE) values ( 8, 'Work Questions', 'WQ' );

-- Create a table to store the different kinds of priorities.  This will be a fixed table.
create table REPAIR_PRIORITY_TYPE
(
 REPAIR_PRIORITY_TYPE_ID number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DESCRIPTION             varchar2(35)   not null
);

alter table REPAIR_PRIORITY_TYPE
   add constraint RPR_PRIORITY_TYPE_PK
       primary key (REPAIR_PRIORITY_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

insert into REPAIR_PRIORITY_TYPE (REPAIR_PRIORITY_TYPE_ID, DESCRIPTION) values ( 1, 'Replacement Flag' );
insert into REPAIR_PRIORITY_TYPE (REPAIR_PRIORITY_TYPE_ID, DESCRIPTION) values ( 2, 'Repair Unit Code' );

-- Create a table to relate to the valid values different kinds of priorities.  This will be a fixed table.
create table REPAIR_PRIORITY_TYPE_PRIORITY
(
 REPAIR_PRIORITY_TYPE_ID number(22,0)   not null,
 REPAIR_PRIORITY_ID      number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18)
);

alter table REPAIR_PRIORITY_TYPE_PRIORITY
   add constraint RPR_PRI_TYPE_PRI_PK
       primary key (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_PRIORITY_TYPE_PRIORITY
   add constraint RPR_PRI_TYPE_PRI_PRI_TYPE_FK
       foreign key (REPAIR_PRIORITY_TYPE_ID)
       references REPAIR_PRIORITY_TYPE;

alter table REPAIR_PRIORITY_TYPE_PRIORITY
   add constraint RPR_PRI_TYPE_PRI_PRI_FK
       foreign key (REPAIR_PRIORITY_ID)
       references REPAIR_PRIORITY;

insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 1, 1 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 1, 4 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 1, 6 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 1, 7 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 1, 8 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 2, 1 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 2, 2 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 2, 3 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 2, 5 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 2, 6 );
insert into REPAIR_PRIORITY_TYPE_PRIORITY (REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID) values ( 2, 7 );

-- Create a table to store the priorities for each repair test.
create table REPAIR_TEST_PRIORITY
(
 TAX_EXPENSE_TEST_ID     number(22,0)   not null,
 REPAIR_PRIORITY_TYPE_ID number(22,0)   not null,
 REPAIR_PRIORITY_ID      number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 SEQUENCE_NUMBER         number(22,0)   not null
);

alter table REPAIR_TEST_PRIORITY
   add constraint RPR_TEST_PRI_PK
       primary key (TAX_EXPENSE_TEST_ID, REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_TEST_PRIORITY
   add constraint RPR_TEST_PRI_TEST_FK
       foreign key (TAX_EXPENSE_TEST_ID)
       references WO_TAX_EXPENSE_TEST;

alter table REPAIR_TEST_PRIORITY
   add constraint RPR_TEST_PRI_PRI_TYPE_FK
       foreign key (REPAIR_PRIORITY_TYPE_ID)
       references REPAIR_PRIORITY_TYPE;

alter table REPAIR_TEST_PRIORITY
   add constraint RPR_TEST_PRI_PRI_FK
       foreign key (REPAIR_PRIORITY_ID)
       references REPAIR_PRIORITY;

create unique index RPR_TEST_PRI_SEQNUM_NDX
   on REPAIR_TEST_PRIORITY (TAX_EXPENSE_TEST_ID, REPAIR_PRIORITY_TYPE_ID, SEQUENCE_NUMBER)
      tablespace PWRPLANT_IDX;

--
-- Convert existing priorities into the new table structure.
--

--
-- Replacement Flag Priority.
--
insert into REPAIR_TEST_PRIORITY
   (TAX_EXPENSE_TEST_ID, REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID, SEQUENCE_NUMBER)
   select WTET.TAX_EXPENSE_TEST_ID, 1, RP.REPAIR_PRIORITY_ID, 1
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPLACEMENT_FLAG_PRIORITY is not null
      and trim(SUBSTR(WTET.REPLACEMENT_FLAG_PRIORITY,
                      1,
                      DECODE(INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 1),
                             0,
                             100,
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 1) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 1, RP.REPAIR_PRIORITY_ID, 2
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPLACEMENT_FLAG_PRIORITY is not null
      and INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 1) > 0
      and trim(SUBSTR(WTET.REPLACEMENT_FLAG_PRIORITY,
                      INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 1) + 1,
                      DECODE(INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 2),
                             0,
                             100,
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 2) -
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 1) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 1, RP.REPAIR_PRIORITY_ID, 3
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPLACEMENT_FLAG_PRIORITY is not null
      and INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 2) > 0
      and trim(SUBSTR(WTET.REPLACEMENT_FLAG_PRIORITY,
                      INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 2) + 1,
                      DECODE(INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 3),
                             0,
                             100,
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 3) -
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 2) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 1, RP.REPAIR_PRIORITY_ID, 4
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPLACEMENT_FLAG_PRIORITY is not null
      and INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 3) > 0
      and trim(SUBSTR(WTET.REPLACEMENT_FLAG_PRIORITY,
                      INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 3) + 1,
                      DECODE(INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 4),
                             0,
                             100,
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 4) -
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 3) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 1, RP.REPAIR_PRIORITY_ID, 5
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPLACEMENT_FLAG_PRIORITY is not null
      and INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 4) > 0
      and trim(SUBSTR(WTET.REPLACEMENT_FLAG_PRIORITY,
                      INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 4) + 1,
                      DECODE(INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 5),
                             0,
                             100,
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 5) -
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 4) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 1, RP.REPAIR_PRIORITY_ID, 6
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPLACEMENT_FLAG_PRIORITY is not null
      and INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 5) > 0
      and trim(SUBSTR(WTET.REPLACEMENT_FLAG_PRIORITY,
                      INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 5) + 1,
                      DECODE(INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 6),
                             0,
                             100,
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 6) -
                             INSTR(WTET.REPLACEMENT_FLAG_PRIORITY, ',', 1, 5) - 1))) =
          RP.REPAIR_PRIORITY_CODE;

--
-- Repair Unit Code Priority.
--
insert into REPAIR_TEST_PRIORITY
   (TAX_EXPENSE_TEST_ID, REPAIR_PRIORITY_TYPE_ID, REPAIR_PRIORITY_ID, SEQUENCE_NUMBER)
   select WTET.TAX_EXPENSE_TEST_ID, 2, RP.REPAIR_PRIORITY_ID, 1
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPAIR_UNIT_CODE_PRIORITY is not null
      and trim(SUBSTR(WTET.REPAIR_UNIT_CODE_PRIORITY,
                      1,
                      DECODE(INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 1),
                             0,
                             100,
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 1) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 2, RP.REPAIR_PRIORITY_ID, 2
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPAIR_UNIT_CODE_PRIORITY is not null
      and INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 1) > 0
      and trim(SUBSTR(WTET.REPAIR_UNIT_CODE_PRIORITY,
                      INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 1) + 1,
                      DECODE(INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 2),
                             0,
                             100,
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 2) -
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 1) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 2, RP.REPAIR_PRIORITY_ID, 3
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPAIR_UNIT_CODE_PRIORITY is not null
      and INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 2) > 0
      and trim(SUBSTR(WTET.REPAIR_UNIT_CODE_PRIORITY,
                      INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 2) + 1,
                      DECODE(INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 3),
                             0,
                             100,
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 3) -
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 2) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 2, RP.REPAIR_PRIORITY_ID, 4
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPAIR_UNIT_CODE_PRIORITY is not null
      and INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 3) > 0
      and trim(SUBSTR(WTET.REPAIR_UNIT_CODE_PRIORITY,
                      INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 3) + 1,
                      DECODE(INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 4),
                             0,
                             100,
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 4) -
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 3) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 2, RP.REPAIR_PRIORITY_ID, 5
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPAIR_UNIT_CODE_PRIORITY is not null
      and INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 4) > 0
      and trim(SUBSTR(WTET.REPAIR_UNIT_CODE_PRIORITY,
                      INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 4) + 1,
                      DECODE(INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 5),
                             0,
                             100,
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 5) -
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 4) - 1))) =
          RP.REPAIR_PRIORITY_CODE
   union
   select WTET.TAX_EXPENSE_TEST_ID, 2, RP.REPAIR_PRIORITY_ID, 6
     from WO_TAX_EXPENSE_TEST WTET, REPAIR_PRIORITY RP
    where WTET.REPAIR_UNIT_CODE_PRIORITY is not null
      and INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 5) > 0
      and trim(SUBSTR(WTET.REPAIR_UNIT_CODE_PRIORITY,
                      INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 5) + 1,
                      DECODE(INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 6),
                             0,
                             100,
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 6) -
                             INSTR(WTET.REPAIR_UNIT_CODE_PRIORITY, ',', 1, 5) - 1))) =
          RP.REPAIR_PRIORITY_CODE;

delete from REPAIR_TEST_PRIORITY
 where REPAIR_PRIORITY_ID in
       (select REPAIR_PRIORITY_ID from REPAIR_PRIORITY where REPAIR_PRIORITY_CODE = 'RC')
   and TAX_EXPENSE_TEST_ID in
       (select TAX_EXPENSE_TEST_ID from WO_TAX_EXPENSE_TEST where REPLACEMENT_CHECK = 0);

alter table WO_TAX_EXPENSE_TEST drop column REPLACEMENT_CHECK;

--
-- Drop the priority columns from wo_tax_expense_test.  They are now stored in a separate table.
--
alter table WO_TAX_EXPENSE_TEST drop column REPAIR_UNIT_CODE_PRIORITY;
alter table WO_TAX_EXPENSE_TEST drop column REPLACEMENT_FLAG_PRIORITY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (301, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;