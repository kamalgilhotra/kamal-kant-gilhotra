/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030682_Lease_move_lease_options.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/10/2013 Kyle Peterson  Point Release
||============================================================================
*/

alter table LS_LEASE drop (EXTENDED_RENTAL_TYPE_ID, EXTENDED_RENTAL_AMT, EXTENDED_RENTAL_PCT, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW);

alter table LS_LEASE_OPTIONS
   add (ITC_SW            number(1,0),
        PARTIAL_RETIRE_SW number(1,0),
        SUBLET_SW         number(1,0),
        MUNI_BO_SW        number(1,0));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (465, 0, 10, 4, 1, 0, 30682, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030682_Lease_move_lease_options.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
