/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037719_pwrtax.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.3.0 05/01/2014 Julia Breuer
||========================================================================================
*/

--
-- Create an import type for updating Tax Location on Asset Location.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 215, sysdate, user, 'Assign : Tax Loc to Asset Loc', 'Update Tax Location on Asset Location', 'tax_import_loc_asset_loc', 'tax_import_loc_asset_loc_arc', null, 0, 'nvo_tax_logic_import', '', 'Tax Loc to Asset Loc', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 215, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 215, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 215, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', '', 0, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 215, 'asset_location_id', sysdate, user, 'Asset Location', 'asset_location_xlate', 1, 2, 'number(22,0)', 'asset_location', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 215, 'tax_location_id', sysdate, user, 'Tax Location', 'tax_location_xlate', 1, 1, 'number(22,0)', 'tax_location', '', 1, null, '', '' );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 88, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 89, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 90, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 91, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 92, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 93, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 94, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 95, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 96, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 97, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 98, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 99, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 609, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'asset_location_id', 610, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'tax_location_id', 145, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'state_id', 1, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 215, 'state_id', 2, sysdate, user );

create table TAX_IMPORT_LOC_ASSET_LOC
(
 IMPORT_RUN_ID        number(22,0) not null,
 LINE_ID              number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 ASSET_LOCATION_XLATE varchar2(254),
 ASSET_LOCATION_ID    number(22,0),
 TAX_LOCATION_XLATE   varchar2(254),
 TAX_LOCATION_ID      number(22,0),
 COMPANY_XLATE        varchar2(254),
 COMPANY_ID           number(22,0),
 STATE_XLATE          varchar2(254),
 STATE_ID             char(18),
 IS_MODIFIED          number(22,0),
 ERROR_MESSAGE        varchar2(4000)
);

alter table TAX_IMPORT_LOC_ASSET_LOC add constraint TAX_IMP_LOC_AL_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_LOC_ASSET_LOC add constraint TAX_IMP_LOC_AL_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;


create table TAX_IMPORT_LOC_ASSET_LOC_ARC
(
 IMPORT_RUN_ID        number(22,0) not null,
 LINE_ID              number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 ASSET_LOCATION_XLATE varchar2(254),
 ASSET_LOCATION_ID    number(22,0),
 TAX_LOCATION_XLATE   varchar2(254),
 TAX_LOCATION_ID      number(22,0),
 COMPANY_XLATE        varchar2(254),
 COMPANY_ID           number(22,0),
 STATE_XLATE          varchar2(254),
 STATE_ID             char(18)
);

alter table TAX_IMPORT_LOC_ASSET_LOC_ARC add constraint TAX_IMP_LOC_AL_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_LOC_ASSET_LOC_ARC add constraint TAX_IMP_LOC_AL_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Tax Location.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 216, sysdate, user, 'Tax Location', 'Import Tax Locations', 'tax_import_loc', 'tax_import_loc_arc', null, 0, 'nvo_tax_logic_import', 'tax_location_id', 'Tax Location', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 216, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 216, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '', '' );

create table TAX_IMPORT_LOC
(
 IMPORT_RUN_ID        number(22,0) not null,
 LINE_ID              number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(254),
 TAX_LOCATION_ID      number(22,0),
 IS_MODIFIED          number(22,0),
 ERROR_MESSAGE        varchar2(4000)
);

alter table TAX_IMPORT_LOC add constraint TAX_IMP_LOC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_LOC add constraint TAX_IMP_LOC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_LOC_ARC
(
 IMPORT_RUN_ID        number(22,0) not null,
 LINE_ID              number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(254),
 TAX_LOCATION_ID      number(22,0)
);

alter table TAX_IMPORT_LOC_ARC add constraint TAX_IMP_LOC_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_LOC_ARC add constraint TAX_IMP_LOC_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Book Alloc Group.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 217, sysdate, user, 'Book Alloc Group', 'Import Book Alloc Groups', 'tax_import_bk_alloc_group', 'tax_import_bk_alloc_group_arc', null, 1, 'nvo_tax_logic_import', 'book_alloc_group_id', 'Book Alloc Group', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 217, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 217, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 217, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 217, 'mortality_curve_id', sysdate, user, 'Mortality Curve', 'mortality_curve_xlate', 0, 1, 'number(22,0)', 'mortality_curve', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 217, 'life', sysdate, user, 'Life', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 217, 'hw_table_line_id', sysdate, user, 'Handy Whitman Table Line', 'hw_table_line_xlate', 0, 1, 'number(22,0)', 'handy_whitman_index', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 217, 'hw_region_id', sysdate, user, 'Handy Whitman Region', 'hw_region_xlate', 0, 1, 'number(22,0)', 'handy_whitman_region', '', 1, null, '', '' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2031, sysdate, user, 'Handy Whitman Region.Description', 'The passed in value corresponds to the Handy Whitman Region: Description field.  Translate to the HW Region ID using the Description column on the Handy Whitman Region table.', 'hw_region_id', '( select hw.hw_region_id from handy_whitman_region hw where upper( trim( <importfield> ) ) = upper( trim( hw.description ) ) )', 0, 'handy_whitman_region', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2032, sysdate, user, 'Book Alloc Group.Description', 'The passed in value corresponds to the Book Alloc Group: Description field.  Translate to the Book Alloc Group ID using the Description column on the Book Alloc Group table.', 'book_alloc_group_id', '( select bag.book_alloc_group_id from book_alloc_group bag where upper( trim( <importfield> ) ) = upper( trim( bag.description ) ) )', 0, 'book_alloc_group', 'description', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 217, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 217, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 217, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 217, 'mortality_curve_id', 2016, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 217, 'hw_table_line_id', 1012, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 217, 'hw_region_id', 2031, sysdate, user );

insert into pwrplant.pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 217, 2032, sysdate, user );

create table TAX_IMPORT_BK_ALLOC_GROUP
(
 IMPORT_RUN_ID         number(22,0) not null,
 LINE_ID               number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 DESCRIPTION           varchar2(254),
 COMPANY_XLATE         varchar2(254),
 COMPANY_ID            number(22,0),
 MORTALITY_CURVE_XLATE varchar2(254),
 MORTALITY_CURVE_ID    number(22,0),
 LIFE                  varchar2(35),
 HW_TABLE_LINE_XLATE   varchar2(254),
 HW_TABLE_LINE_ID      number(22,0),
 HW_REGION_XLATE       varchar2(254),
 HW_REGION_ID          number(22,0),
 BOOK_ALLOC_GROUP_ID   number(22,0),
 IS_MODIFIED           number(22,0),
 ERROR_MESSAGE         varchar2(4000)
);

alter table TAX_IMPORT_BK_ALLOC_GROUP add constraint TAX_IMP_BAG_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_BK_ALLOC_GROUP add constraint TAX_IMP_BAG_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_BK_ALLOC_GROUP_ARC
(
 IMPORT_RUN_ID         number(22,0) not null,
 LINE_ID               number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 DESCRIPTION           varchar2(254),
 COMPANY_XLATE         varchar2(254),
 COMPANY_ID            number(22,0),
 MORTALITY_CURVE_XLATE varchar2(254),
 MORTALITY_CURVE_ID    number(22,0),
 LIFE                  varchar2(35),
 HW_TABLE_LINE_XLATE   varchar2(254),
 HW_TABLE_LINE_ID      number(22,0),
 HW_REGION_XLATE       varchar2(254),
 HW_REGION_ID          number(22,0),
 BOOK_ALLOC_GROUP_ID   number(22,0)
);

alter table TAX_IMPORT_BK_ALLOC_GROUP_ARC add constraint TAX_IMP_BAG_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_BK_ALLOC_GROUP_ARC add constraint TAX_IMP_BAG_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Tax Utility Account.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 218, sysdate, user, 'Assign : Book Alloc Grp to Depr Grp', 'Import Depr Group/Book Alloc Group Assignments to Tax Utility Account', 'tax_import_tax_ua', 'tax_import_tax_ua_arc', null, 1, 'nvo_tax_logic_import', 'tax_utility_account_id, description, company_id, mortality_curve_id', 'Book Alloc Grp to Depr Grp', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 218, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 218, 'depr_group_id', sysdate, user, 'Depreciation Group', 'depr_group_xlate', 1, 1, 'number(22,0)', 'depr_group', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 218, 'book_alloc_group_id', sysdate, user, 'Book Alloc Group', 'book_alloc_group_xlate', 1, 1, 'number(22,0)', 'book_alloc_group', '', 1, 217, '', '' );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 218, 'book_alloc_group_id', 2032, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 218, 'depr_group_id', 41, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 218, 'depr_group_id', 42, sysdate, user );

create table TAX_IMPORT_TAX_UA
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DEPR_GROUP_XLATE        varchar2(254),
 DEPR_GROUP_ID           number(22,0),
 BOOK_ALLOC_GROUP_XLATE  varchar2(254),
 BOOK_ALLOC_GROUP_ID     number(22,0),
 DESCRIPTION             varchar2(254),
 COMPANY_ID              number(22,0),
 MORTALITY_CURVE_ID      number(22,0),
 TAX_UTILITY_ACCOUNT_ID  number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table TAX_IMPORT_TAX_UA add constraint TAX_IMP_TUA_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_TAX_UA add constraint TAX_IMP_TUA_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_TAX_UA_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DEPR_GROUP_XLATE        varchar2(254),
 DEPR_GROUP_ID           number(22,0),
 BOOK_ALLOC_GROUP_XLATE  varchar2(254),
 BOOK_ALLOC_GROUP_ID     number(22,0),
 DESCRIPTION             varchar2(254),
 COMPANY_ID              number(22,0),
 MORTALITY_CURVE_ID      number(22,0),
 TAX_UTILITY_ACCOUNT_ID  number(22,0)
);

alter table TAX_IMPORT_TAX_UA_ARC add constraint TAX_IMP_TUA_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_TAX_UA_ARC add constraint TAX_IMP_TUA_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Book Alloc Assign.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 219, sysdate, user, 'Assign : Bk Alloc Grp to Tax Class', 'Import Tax Class/Book Alloc Group Assignments', 'tax_import_bk_alloc_assign', 'tax_import_bk_alloc_assign_arc', null, 1, 'nvo_tax_logic_import', 'book_alloc_assign_id', 'Book Alloc Grp to Tax Class', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 219, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 219, 'book_alloc_group_id', sysdate, user, 'Book Alloc Group', 'book_alloc_group_xlate', 1, 2, 'number(22,0)', 'book_alloc_group', '', 1, 217, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 219, 'tax_class_id', sysdate, user, 'Tax Class', 'tax_class_xlate', 1, 1, 'number(22,0)', 'tax_class', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 219, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '', '' );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 219, 'book_alloc_group_id', 2032, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 219, 'tax_class_id', 2007, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 219, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 219, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 219, 'company_id', 21, sysdate, user );

create table TAX_IMPORT_BK_ALLOC_ASSIGN
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 BOOK_ALLOC_GROUP_XLATE  varchar2(254),
 BOOK_ALLOC_GROUP_ID     number(22,0),
 TAX_CLASS_XLATE         varchar2(254),
 TAX_CLASS_ID            number(22,0),
 BOOK_ALLOC_ASSIGN_ID    number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table TAX_IMPORT_BK_ALLOC_ASSIGN add constraint TAX_IMP_BAA_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_BK_ALLOC_ASSIGN add constraint TAX_IMP_BAA_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_BK_ALLOC_ASSIGN_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 BOOK_ALLOC_GROUP_XLATE  varchar2(254),
 BOOK_ALLOC_GROUP_ID     number(22,0),
 TAX_CLASS_XLATE         varchar2(254),
 TAX_CLASS_ID            number(22,0),
 BOOK_ALLOC_ASSIGN_ID    number(22,0)
);

alter table TAX_IMPORT_BK_ALLOC_ASSIGN_ARC add constraint TAX_IMP_BAA_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_BK_ALLOC_ASSIGN_ARC add constraint TAX_IMP_BAA_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Tax Rollup.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 220, sysdate, user, 'Tax Rollup', 'Import Tax Rollups', 'tax_import_rollup', 'tax_import_rollup_arc', null, 0, 'nvo_tax_logic_import', 'tax_rollup_id', 'Tax Rollup', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 220, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 220, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '', '' );

create table TAX_IMPORT_ROLLUP
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DESCRIPTION             varchar2(254),
 TAX_ROLLUP_ID           number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table TAX_IMPORT_ROLLUP add constraint TAX_IMP_ROLLUP_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_ROLLUP add constraint TAX_IMP_ROLLUP_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_ROLLUP_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DESCRIPTION             varchar2(254),
 TAX_ROLLUP_ID           number(22,0)
);

alter table TAX_IMPORT_ROLLUP_ARC add constraint TAX_IMP_ROLLUP_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_ROLLUP_ARC add constraint TAX_IMP_ROLLUP_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Tax Rollup Detail.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 221, sysdate, user, 'Tax Rollup Detail', 'Import Tax Rollup Detail Records', 'tax_import_rollup_detail', 'tax_import_rollup_detail_arc', null, 0, 'nvo_tax_logic_import', 'tax_rollup_detail_id', 'Tax Rollup Detail', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 221, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 221, 'tax_rollup_id', sysdate, user, 'Tax Rollup', 'tax_rollup_xlate', 1, 1, 'number(22,0)', 'tax_rollup', '', 1, 220, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 221, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '', '' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2033, sysdate, user, 'Tax Rollup.Description', 'The passed in value corresponds to the Tax Rollup: Description field.  Translate to the Tax Rollup ID using the Description column on the Tax Rollup table.', 'tax_rollup_id', '( select tr.tax_rollup_id from tax_rollup tr where upper( trim( <importfield> ) ) = upper( trim( tr.description ) ) )', 0, 'tax_rollup', 'description', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 221, 'tax_rollup_id', 2033, sysdate, user );

create table TAX_IMPORT_ROLLUP_DETAIL
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_ROLLUP_XLATE        varchar2(254),
 TAX_ROLLUP_ID           number(22,0),
 DESCRIPTION             varchar2(254),
 TAX_ROLLUP_DETAIL_ID    number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table TAX_IMPORT_ROLLUP_DETAIL add constraint TAX_IMP_ROLL_DET_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_ROLLUP_DETAIL add constraint TAX_IMP_ROLL_DET_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_ROLLUP_DETAIL_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_ROLLUP_XLATE        varchar2(254),
 TAX_ROLLUP_ID           number(22,0),
 DESCRIPTION             varchar2(254),
 TAX_ROLLUP_DETAIL_ID    number(22,0)
);

alter table TAX_IMPORT_ROLLUP_DETAIL_ARC add constraint TAX_IMP_ROLL_DET_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_ROLLUP_DETAIL_ARC add constraint TAX_IMP_ROLL_DET_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Tax Class Rollups.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 222, sysdate, user, 'Tax Class Rollups', 'Import Tax Class Rollup Assignments', 'tax_import_class_rollups', 'tax_import_class_rollups_arc', null, 1, 'nvo_tax_logic_import', 'tax_class_rollups_id', 'Tax Class Rollups', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 222, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 222, 'tax_rollup_id', sysdate, user, 'Tax Rollup', 'tax_rollup_xlate', 1, 1, 'number(22,0)', 'tax_rollup', '', 0, 220, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 222, 'tax_rollup_detail_id', sysdate, user, 'Tax Rollup Detail', 'tax_rollup_detail_xlate', 1, 2, 'number(22,0)', 'tax_rollup_detail', '', 1, 221, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 222, 'tax_class_id', sysdate, user, 'Tax Class', 'tax_class_xlate', 1, 1, 'number(22,0)', 'tax_class', '', 1, null, '', '' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2034, sysdate, user, 'Tax Rollup Detail.Description (for given Tax Rollup)', 'The passed in value corresponds to the Tax Rollup Detail: Description field (for the given Tax Rollup).  Translate to the Tax Rollup Detail ID using the Description and Tax Rollup columns on the Tax Rollup Detail table.', 'tax_rollup_id', '( select trd.tax_rollup_detail_id from tax_rollup_detail trd where upper( trim( <importfield> ) ) = upper( trim( trd.description ) ) and <importtable>.tax_rollup_id = trd.tax_rollup_id )', 0, 'tax_rollup_detail', 'description', 'tax_rollup_id', 'select tax_rollup.<tax_rollup_id>, tax_rollup_detail.description from tax_rollup_detail, tax_rollup where tax_rollup_detail.tax_rollup_id = tax_rollup.tax_rollup_id and 5=5', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 222, 'tax_rollup_id', 2033, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 222, 'tax_rollup_detail_id', 2034, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 222, 'tax_class_id', 2007, sysdate, user );

create table TAX_IMPORT_CLASS_ROLLUPS
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_ROLLUP_XLATE        varchar2(254),
 TAX_ROLLUP_ID           number(22,0),
 TAX_ROLLUP_DETAIL_XLATE varchar2(254),
 TAX_ROLLUP_DETAIL_ID    number(22,0),
 TAX_CLASS_XLATE         varchar2(254),
 TAX_CLASS_ID            number(22,0),
 TAX_CLASS_ROLLUPS_ID    number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table TAX_IMPORT_CLASS_ROLLUPS add constraint TAX_IMP_CLASS_ROLL_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_CLASS_ROLLUPS add constraint TAX_IMP_CLASS_ROLL_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_CLASS_ROLLUPS_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_ROLLUP_XLATE        varchar2(254),
 TAX_ROLLUP_ID           number(22,0),
 TAX_ROLLUP_DETAIL_XLATE varchar2(254),
 TAX_ROLLUP_DETAIL_ID    number(22,0),
 TAX_CLASS_XLATE         varchar2(254),
 TAX_CLASS_ID            number(22,0),
 TAX_CLASS_ROLLUPS_ID    number(22,0)
);

alter table TAX_IMPORT_CLASS_ROLLUPS_ARC add constraint TAX_IMP_CLASS_ROLL_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_CLASS_ROLLUPS_ARC add constraint TAX_IMP_CLASS_ROLL_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Tax Transfer Control.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 223, sysdate, user, 'Tax Transfer Control', 'Import Tax Transfer Control Records', 'tax_import_trans_control', 'tax_import_trans_control_arc', null, 0, 'nvo_tax_logic_import', 'tax_transfer_id, version_id, package_id', 'Tax Transfer Control', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 223, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'tax_year', sysdate, user, 'Tax Year', '', 1, 1, 'number(24,2)', '', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'from_trid', sysdate, user, 'From Tax Record', 'from_trid_xlate', 1, 2, 'number(22,0)', 'tax_record_control', '', 1, null, 'tax_record_id', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'from_company_id', sysdate, user, 'From Company', 'from_company_xlate', 0, 1, 'number(22,0)', 'company', '', 0, null, 'company_id', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'from_tax_class_id', sysdate, user, 'From Tax Class', 'from_tax_class_xlate', 0, 1, 'number(22,0)', 'tax_class', '', 0, null, 'tax_class_id', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'from_vintage_id', sysdate, user, 'From Vintage', 'from_vintage_xlate', 0, 1, 'number(22,0)', 'vintage', '', 0, null, 'vintage_id', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'from_in_service_month', sysdate, user, 'From In Service Month', '', 0, 1, 'date', '', '', 0, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'to_trid', sysdate, user, 'To Tax Record', 'to_trid_xlate', 1, 2, 'number(22,0)', 'tax_record_control', '', 1, null, 'tax_record_id', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'to_company_id', sysdate, user, 'To Company', 'to_company_xlate', 0, 1, 'number(22,0)', 'company', '', 0, null, 'company_id', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'to_tax_class_id', sysdate, user, 'To Tax Class', 'to_tax_class_xlate', 0, 1, 'number(22,0)', 'tax_class', '', 0, null, 'tax_class_id', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'to_vintage_id', sysdate, user, 'To Vintage', 'to_vintage_xlate', 0, 1, 'number(22,0)', 'vintage', '', 0, null, 'vintage_id', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'to_in_service_month', sysdate, user, 'To In Service Month', '', 0, 1, 'date', '', '', 0, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 223, 'book_amount', sysdate, user, 'Book Amount', '', 1, 1, 'number(22,2)', '', '', 1, null, '', '' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2035, sysdate, user, 'Derive from Case, Company, Tax Class, Vintage, and Month (From Record)', 'The Tax Record ID will be derived from the given Case, Company, Tax Class, Vintage, and Month.', 'from_trid', '( select trc.tax_record_id from tax_record_control trc where <importtable>.from_company_id = trc.company_id and <importtable>.from_tax_class_id = trc.tax_class_id and <importtable>.from_vintage_id = trc.vintage_id and <importtable>.version_id = trc.version_id and nvl( <importtable>.from_in_service_month, ''01/01/1500'' ) = nvl( to_char( trc.in_service_month, ''mm/dd/yyyy'' ), ''01/01/1500'' ) )', 1, '', '', 'from_company_id, from_tax_class_id, from_vintage_id, from_in_service_month', '', 0 );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2036, sysdate, user, 'Derive from Case, Company, Tax Class, Vintage, and Month (To Record)', 'The Tax Record ID will be derived from the given Case, Company, Tax Class, Vintage, and Month.', 'to_trid', '( select trc.tax_record_id from tax_record_control trc where <importtable>.to_company_id = trc.company_id and <importtable>.to_tax_class_id = trc.tax_class_id and <importtable>.to_vintage_id = trc.vintage_id and <importtable>.version_id = trc.version_id and nvl( <importtable>.to_in_service_month, ''01/01/1500'' ) = nvl( to_char( trc.in_service_month, ''mm/dd/yyyy'' ), ''01/01/1500'' ) )', 1, '', '', 'to_company_id, to_tax_class_id, to_vintage_id, to_in_service_month', '', 0 );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'from_trid', 2035, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'from_company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'from_company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'from_company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'from_tax_class_id', 2007, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'from_vintage_id', 2008, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'from_vintage_id', 2009, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'to_trid', 2036, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'to_company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'to_company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'to_company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'to_tax_class_id', 2007, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'to_vintage_id', 2008, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 223, 'to_vintage_id', 2009, sysdate, user );

create table TAX_IMPORT_TRANS_CONTROL
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_YEAR                varchar2(35),
 FROM_TRID_XLATE         varchar2(254),
 FROM_TRID               number(22,0),
 FROM_COMPANY_XLATE      varchar2(254),
 FROM_COMPANY_ID         number(22,0),
 FROM_TAX_CLASS_XLATE    varchar2(254),
 FROM_TAX_CLASS_ID       number(22,0),
 FROM_VINTAGE_XLATE      varchar2(254),
 FROM_VINTAGE_ID         number(22,0),
 FROM_IN_SERVICE_MONTH   varchar2(254),
 TO_TRID_XLATE           varchar2(254),
 TO_TRID                 number(22,0),
 TO_COMPANY_XLATE        varchar2(254),
 TO_COMPANY_ID           number(22,0),
 TO_TAX_CLASS_XLATE      varchar2(254),
 TO_TAX_CLASS_ID         number(22,0),
 TO_VINTAGE_XLATE        varchar2(254),
 TO_VINTAGE_ID           number(22,0),
 TO_IN_SERVICE_MONTH     varchar2(254),
 BOOK_AMOUNT             varchar2(35),
 TAX_TRANSFER_ID         number(22,0),
 PACKAGE_ID              number(22,0),
 VERSION_ID              number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table TAX_IMPORT_TRANS_CONTROL add constraint TAX_IMP_TRANS_CTL_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_TRANS_CONTROL add constraint TAX_IMP_TRANS_CTL_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_TRANS_CONTROL_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_YEAR                varchar2(35),
 FROM_TRID_XLATE         varchar2(254),
 FROM_TRID               number(22,0),
 FROM_COMPANY_XLATE      varchar2(254),
 FROM_COMPANY_ID         number(22,0),
 FROM_TAX_CLASS_XLATE    varchar2(254),
 FROM_TAX_CLASS_ID       number(22,0),
 FROM_VINTAGE_XLATE      varchar2(254),
 FROM_VINTAGE_ID         number(22,0),
 FROM_IN_SERVICE_MONTH   varchar2(254),
 TO_TRID_XLATE           varchar2(254),
 TO_TRID                 number(22,0),
 TO_COMPANY_XLATE        varchar2(254),
 TO_COMPANY_ID           number(22,0),
 TO_TAX_CLASS_XLATE      varchar2(254),
 TO_TAX_CLASS_ID         number(22,0),
 TO_VINTAGE_XLATE        varchar2(254),
 TO_VINTAGE_ID           number(22,0),
 TO_IN_SERVICE_MONTH     varchar2(254),
 BOOK_AMOUNT             varchar2(35),
 TAX_TRANSFER_ID         number(22,0),
 PACKAGE_ID              number(22,0),
 VERSION_ID              number(22,0)
);

alter table TAX_IMPORT_TRANS_CONTROL_ARC add constraint TAX_IMP_TRANS_CTL_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_TRANS_CONTROL_ARC add constraint TAX_IMP_TRANS_CTL_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Create an import for Utility Account Depreciation.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 224, sysdate, user, 'Book Depreciation', 'Import Utility Account Depreciation Records', 'tax_import_ua_depr', 'tax_import_ua_depr_arc', null, 1, 'nvo_tax_logic_import', 'utility_account_depr_id', 'Book Depreciation', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 224, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'tax_utility_account_id', sysdate, user, 'Tax Utility Account', 'tax_utility_account_xlate', 1, 1, 'number(22,0)', 'tax_utility_account', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'tax_year', sysdate, user, 'Tax Year', '', 1, 1, 'number(24,2)', '', '', 1, null, '', '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'book_depr', sysdate, user, 'Book Depreciation', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'life_rate', sysdate, user, 'Life Rate', '', 0, 1, 'number(22,8)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'salvage_rate', sysdate, user, 'Salvage Rate', '', 0, 1, 'number(22,8)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'cor_rate', sysdate, user, 'COR Rate', '', 0, 1, 'number(22,8)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'actual_cost_of_removal', sysdate, user, 'Actual Cost of Removal', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'actual_cash_salvage', sysdate, user, 'Actual Cash Salvage', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'actual_salvage_returns', sysdate, user, 'Actual Salvage Returns', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'actual_reserve_credits', sysdate, user, 'Actual Reserve Credits', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'cor_reserve', sysdate, user, 'Net Salvage in Book Depr', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'rwip_cor_change', sysdate, user, 'RWIP COR Change', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'rwip_reserve_credits_change', sysdate, user, 'RWIP Credits Change', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 224, 'rwip_salvage_change', sysdate, user, 'RWIP Salvage Change', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2037, sysdate, user, 'Tax Utility Account.Description', 'The passed in value corresponds to the Tax Utility Account: Description field.  Translate to the Tax Utility Account ID using the Description column on the Tax Utility Account table.', 'tax_utility_account_id', '( select tua.tax_utility_account_id from tax_utility_account tua where upper( trim( <importfield> ) ) = upper( trim( tua.description ) ) )', 0, 'tax_utility_account', 'description', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 224, 'tax_utility_account_id', 2037, sysdate, user );

create table TAX_IMPORT_UA_DEPR
(
 IMPORT_RUN_ID                 number(22,0) not null,
 LINE_ID                       number(22,0) not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 TAX_YEAR                      varchar2(35),
 TAX_UTILITY_ACCOUNT_XLATE     varchar2(254),
 TAX_UTILITY_ACCOUNT_ID        number(22,0),
 BOOK_DEPR                     varchar2(35),
 LIFE_RATE                     varchar2(35),
 SALVAGE_RATE                  varchar2(35),
 COR_RATE                      varchar2(35),
 ACTUAL_COST_OF_REMOVAL        varchar2(35),
 ACTUAL_CASH_SALVAGE           varchar2(35),
 ACTUAL_SALVAGE_RETURNS        varchar2(35),
 ACTUAL_RESERVE_CREDITS        varchar2(35),
 COR_RESERVE                   varchar2(35),
 RWIP_COR_CHANGE               varchar2(35),
 RWIP_RESERVE_CREDITS_CHANGE   varchar2(35),
 RWIP_SALVAGE_CHANGE           varchar2(35),
 UTILITY_ACCOUNT_DEPR_ID       number(22,0),
 IS_MODIFIED                   number(22,0),
 ERROR_MESSAGE                 varchar2(4000)
);

alter table TAX_IMPORT_UA_DEPR add constraint TAX_IMP_UA_DEPR_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_UA_DEPR add constraint TAX_IMP_UA_DEPR_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

create table TAX_IMPORT_UA_DEPR_ARC
(
 IMPORT_RUN_ID                 number(22,0) not null,
 LINE_ID                       number(22,0) not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 TAX_YEAR                      varchar2(35),
 TAX_UTILITY_ACCOUNT_XLATE     varchar2(254),
 TAX_UTILITY_ACCOUNT_ID        number(22,0),
 BOOK_DEPR                     varchar2(35),
 LIFE_RATE                     varchar2(35),
 SALVAGE_RATE                  varchar2(35),
 COR_RATE                      varchar2(35),
 ACTUAL_COST_OF_REMOVAL        varchar2(35),
 ACTUAL_CASH_SALVAGE           varchar2(35),
 ACTUAL_SALVAGE_RETURNS        varchar2(35),
 ACTUAL_RESERVE_CREDITS        varchar2(35),
 COR_RESERVE                   varchar2(35),
 RWIP_COR_CHANGE               varchar2(35),
 RWIP_RESERVE_CREDITS_CHANGE   varchar2(35),
 RWIP_SALVAGE_CHANGE           varchar2(35),
 UTILITY_ACCOUNT_DEPR_ID       number(22,0)
);

alter table TAX_IMPORT_UA_DEPR_ARC add constraint TAX_IMP_UA_DEPR_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table TAX_IMPORT_UA_DEPR_ARC add constraint TAX_IMP_UA_DEPR_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PP_IMPORT_RUN;

--
-- Remove Tax Include from the Basis Amounts import.  It will always be 1.
--
delete from pwrplant.pp_import_column_lookup where import_type_id = 202 and column_name = 'tax_include_id';
delete from pwrplant.pp_import_column where import_type_id = 202 and column_name = 'tax_include_id';
alter table pwrplant.tax_import_basis_amounts drop column tax_include_xlate;
alter table pwrplant.tax_import_basis_amounts drop column tax_include_id;
alter table pwrplant.tax_import_basis_amounts_arc drop column tax_include_xlate;
alter table pwrplant.tax_import_basis_amounts_arc drop column tax_include_id;

--
-- Remove Book Balance Adjustment from the Depreciation Adjustments import.
--
delete from pwrplant.pp_import_column where import_type_id = 201 and column_name = 'book_balance_adjust';
alter table pwrplant.tax_import_depr_adjust drop column book_balance_adjust;
alter table pwrplant.tax_import_depr_adjust_arc drop column book_balance_adjust;

--
-- Add a column to store a third primary key column (for tables like sub_account that have a three-field primary key).
--
alter table pwrplant.pp_import_column add parent_table_pk_column3 varchar2(35);
update pwrplant.pp_import_column set parent_table_pk_column2 = 'bus_segment_id', parent_table_pk_column3 = 'utility_account_id' where column_name = 'sub_account_id' and import_type_id in (207,212,213,214);

--
-- Add table and column comments.
--
comment on table tax_import_loc_asset_loc is '(O and C) [09] The Tax Import Loc Asset Loc table is a staging table into which data from a run of the Assign : Tax Loc to Asset Loc Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Loc Asset Loc Archive table.';
comment on table tax_import_loc is '(O and C) [09] The Tax Import Loc table is a staging table into which data from a run of the Tax Location Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Loc Archive table.';
comment on table tax_import_bk_alloc_group is '(O and C) [09] The Tax Import Bk Alloc Group table is a staging table into which data from a run of the Book Alloc Group Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Bk Alloc Group Archive table.';
comment on table tax_import_tax_ua is '(O and C) [09] The Tax Import Tax Ua table is a staging table into which data from a run of the Assign : Book Alloc Grp to Depr Grp Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Tax Ua Archive table.';
comment on table tax_import_bk_alloc_assign is '(O and C) [09] The Tax Import Bk Alloc Assign table is a staging table into which data from a run of the Assign : Bk Alloc Grp to Tax Class Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Bk Alloc Assign Archive table.';
comment on table tax_import_rollup is '(O and C) [09] The Tax Import Rollup table is a staging table into which data from a run of the Tax Rollup Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Rollup Archive table.';
comment on table tax_import_rollup_detail is '(O and C) [09] The Tax Import Rollup Detail table is a staging table into which data from a run of the Tax Rollup Detail Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Rollup Detail Archive table.';
comment on table tax_import_class_rollups is '(O and C) [09] The Tax Import Class Rollups table is a staging table into which data from a run of the Tax Class Rollups Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Class Rollups Archive table.';
comment on table tax_import_trans_control is '(O and C) [09] The Tax Import Trans Control table is a staging table into which data from a run of the Tax Transfer Control Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Trans Control Archive table.';
comment on table tax_import_ua_depr is '(O and C) [09] The Tax Import Ua Depr table is a staging table into which data from a run of the Book Depreciation Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Ua Depr Archive table.';
comment on table tax_import_loc_asset_loc_arc is '(O and C) [09] The Tax Import Loc Asset Loc Archive table maintains an archive of all successful runs of the Assign : Tax Loc to Asset Loc Import process.';
comment on table tax_import_loc_arc is '(O and C) [09] The Tax Import Loc Archive table maintains an archive of all successful runs of the Tax Location Import process.';
comment on table tax_import_bk_alloc_group_arc is '(O and C) [09] The Tax Import Bk Alloc Group Archive table maintains an archive of all successful runs of the Book Alloc Group Import process.';
comment on table tax_import_tax_ua_arc is '(O and C) [09] The Tax Import Tax Ua Archive table maintains an archive of all successful runs of the Assign : Book Alloc Grp to Depr Grp Import process.';
comment on table tax_import_bk_alloc_assign_arc is '(O and C) [09] The Tax Import Bk Alloc Assign Archive table maintains an archive of all successful runs of the Assign : Bk Alloc Grp to Tax Class Import process.';
comment on table tax_import_rollup_arc is '(O and C) [09] The Tax Import Rollup Archive table maintains an archive of all successful runs of the Tax Rollup Import process.';
comment on table tax_import_rollup_detail_arc is '(O and C) [09] The Tax Import Rollup Detail Archive table maintains an archive of all successful runs of the Tax Rollup Detail Import process.';
comment on table tax_import_class_rollups_arc is '(O and C) [09] The Tax Import Class Rollups Archive table maintains an archive of all successful runs of the Tax Class Rollups Import process.';
comment on table tax_import_trans_control_arc is '(O and C) [09] The Tax Import Trans Control Archive table maintains an archive of all successful runs of the Tax Transfer Control Import process.';
comment on table tax_import_ua_depr_arc is '(O and C) [09] The Tax Import Ua Depr Archive table maintains an archive of all successful runs of the Book Depreciation Import process.';

comment on column tax_import_bk_alloc_assign.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_bk_alloc_assign.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_bk_alloc_group.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_bk_alloc_group.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_class_rollups.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_class_rollups.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_loc.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_loc.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_loc_asset_loc.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_loc_asset_loc.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_rollup.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_rollup.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_rollup_detail.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_rollup_detail.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_tax_ua.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_tax_ua.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_trans_control.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_trans_control.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_ua_depr.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_ua_depr.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_bk_alloc_assign.book_alloc_assign_id is 'System-assigned identifier of a particular Book Alloc Assign.';
comment on column tax_import_bk_alloc_assign.book_alloc_group_id is 'System-assigned identifier of a particular Book Alloc Group.';
comment on column tax_import_bk_alloc_assign.book_alloc_group_xlate is 'Value from the import file translated to obtain the book_alloc_group_id.';
comment on column tax_import_bk_alloc_assign.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_bk_alloc_assign.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_bk_alloc_assign.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_bk_alloc_assign.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_bk_alloc_assign.tax_class_id is 'System-assigned identifier of a particular Tax Class.';
comment on column tax_import_bk_alloc_assign.tax_class_xlate is 'Value from the import file translated to obtain the tax_class_id.';
comment on column tax_import_bk_alloc_assign.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_bk_alloc_assign.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_bk_alloc_group.book_alloc_group_id is 'System-assigned identifier of a particular Book Alloc Group.';
comment on column tax_import_bk_alloc_group.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_bk_alloc_group.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_bk_alloc_group.description is 'Records a short description of the book allocation group.';
comment on column tax_import_bk_alloc_group.hw_region_id is 'System-assigned identifier of a particular Handy Whitman Region.';
comment on column tax_import_bk_alloc_group.hw_region_xlate is 'Value from the import file translated to obtain the hw_region_id.';
comment on column tax_import_bk_alloc_group.hw_table_line_id is 'System-assigned identifier of a particular Handy Whitman Table Line.';
comment on column tax_import_bk_alloc_group.hw_table_line_xlate is 'Value from the import file translated to obtain the hw_table_line_id.';
comment on column tax_import_bk_alloc_group.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_bk_alloc_group.life is 'Life to be used with the particular mortality curve.';
comment on column tax_import_bk_alloc_group.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_bk_alloc_group.mortality_curve_id is 'System-assigned identifier of a particular Mortality Curve.';
comment on column tax_import_bk_alloc_group.mortality_curve_xlate is 'Value from the import file translated to obtain the mortality_curve_id.';
comment on column tax_import_bk_alloc_group.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_bk_alloc_group.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_class_rollups.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_class_rollups.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_class_rollups.tax_class_id is 'System-assigned identifier of a particular Tax Class.';
comment on column tax_import_class_rollups.tax_class_rollups_id is 'System-assigned identifier of a particular Tax Class Rollups.';
comment on column tax_import_class_rollups.tax_class_xlate is 'Value from the import file translated to obtain the tax_class_id.';
comment on column tax_import_class_rollups.tax_rollup_detail_id is 'System-assigned identifier of a particular Tax Rollup Detail.';
comment on column tax_import_class_rollups.tax_rollup_detail_xlate is 'Value from the import file translated to obtain the tax_rollup_detail_id.';
comment on column tax_import_class_rollups.tax_rollup_id is 'System-assigned identifier of a particular Tax Rollup.';
comment on column tax_import_class_rollups.tax_rollup_xlate is 'Value from the import file translated to obtain the tax_rollup_id.';
comment on column tax_import_class_rollups.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_class_rollups.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_loc.description is 'User-supplied name for the locations that represent depreciation distinctions for tax purposes.';
comment on column tax_import_loc.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_loc.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_loc.tax_location_id is 'System-assigned identifier of a particular Tax Location.';
comment on column tax_import_loc.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_loc.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_loc_asset_loc.asset_location_id is 'System-assigned identifier of a particular Asset Location.';
comment on column tax_import_loc_asset_loc.asset_location_xlate is 'Value from the import file translated to obtain the asset_location_id.';
comment on column tax_import_loc_asset_loc.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_loc_asset_loc.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_loc_asset_loc.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_loc_asset_loc.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_loc_asset_loc.state_id is 'System-assigned identifier of a particular State.';
comment on column tax_import_loc_asset_loc.state_xlate is 'Value from the import file translated to obtain the state_id.';
comment on column tax_import_loc_asset_loc.tax_location_id is 'System-assigned identifier of a particular Tax Location.';
comment on column tax_import_loc_asset_loc.tax_location_xlate is 'Value from the import file translated to obtain the tax_location_id.';
comment on column tax_import_loc_asset_loc.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_loc_asset_loc.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_rollup.description is 'Records a short description of a particular tax rollup, e.g., Services, Function.';
comment on column tax_import_rollup.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_rollup.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_rollup.tax_rollup_id is 'System-assigned identifier of a particular Tax Rollup.';
comment on column tax_import_rollup.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_rollup.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_rollup_detail.description is 'Records a short description of a particular tax rollup detail or value.';
comment on column tax_import_rollup_detail.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_rollup_detail.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_rollup_detail.tax_rollup_detail_id is 'System-assigned identifier of a particular Tax Rollup Detail.';
comment on column tax_import_rollup_detail.tax_rollup_id is 'System-assigned identifier of a particular Tax Rollup.';
comment on column tax_import_rollup_detail.tax_rollup_xlate is 'Value from the import file translated to obtain the tax_rollup_id.';
comment on column tax_import_rollup_detail.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_rollup_detail.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_tax_ua.book_alloc_group_id is 'System-assigned identifier of a particular Book Alloc Group.';
comment on column tax_import_tax_ua.book_alloc_group_xlate is 'Value from the import file translated to obtain the book_alloc_group_id.';
comment on column tax_import_tax_ua.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_tax_ua.depr_group_id is 'System-assigned identifier of a particular Depreciation Group.';
comment on column tax_import_tax_ua.depr_group_xlate is 'Value from the import file translated to obtain the depr_group_id.';
comment on column tax_import_tax_ua.description is 'Description of the tax utility account.';
comment on column tax_import_tax_ua.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_tax_ua.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_tax_ua.mortality_curve_id is 'System-assigned identifier of a particular Mortality Curve.';
comment on column tax_import_tax_ua.tax_utility_account_id is 'System-assigned identifier of a particular Tax Utility Account.';
comment on column tax_import_tax_ua.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_tax_ua.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_trans_control.book_amount is 'Book amount of the transfer.';
comment on column tax_import_trans_control.from_company_id is 'System-assigned identifier of a particular From Company.';
comment on column tax_import_trans_control.from_company_xlate is 'Value from the import file translated to obtain the from_company_id.';
comment on column tax_import_trans_control.from_in_service_month is 'Month/year field that is optional in the tax record key.  It would be used for certain depreciation methods (such as for buildings), where the in-service month determines the depreciation rate for each tax year.';
comment on column tax_import_trans_control.from_tax_class_id is 'System-assigned identifier of a particular From Tax Class.';
comment on column tax_import_trans_control.from_tax_class_xlate is 'Value from the import file translated to obtain the from_tax_class_id.';
comment on column tax_import_trans_control.from_trid is 'System-assigned identifier of a particular From Tax Record.';
comment on column tax_import_trans_control.from_trid_xlate is 'Value from the import file translated to obtain the from_trid.';
comment on column tax_import_trans_control.from_vintage_id is 'System-assigned identifier of a particular From Vintage.';
comment on column tax_import_trans_control.from_vintage_xlate is 'Value from the import file translated to obtain the from_vintage_id.';
comment on column tax_import_trans_control.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_trans_control.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_trans_control.package_id is 'System-assigned identifier of a particular Package.';
comment on column tax_import_trans_control.tax_transfer_id is 'System-assigned identifier of a particular Tax Transfer.';
comment on column tax_import_trans_control.tax_year is 'Calendar tax year of the transfer.  (The fraction is a sequential number for dealing with short tax years.)';
comment on column tax_import_trans_control.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_trans_control.to_company_id is 'System-assigned identifier of a particular To Company.';
comment on column tax_import_trans_control.to_company_xlate is 'Value from the import file translated to obtain the to_company_id.';
comment on column tax_import_trans_control.to_in_service_month is 'Month/year field that is optional in the tax record key.  It would be used for certain depreciation methods (such as for buildings), where the in-service month determines the depreciation rate for each tax year.';
comment on column tax_import_trans_control.to_tax_class_id is 'System-assigned identifier of a particular To Tax Class.';
comment on column tax_import_trans_control.to_tax_class_xlate is 'Value from the import file translated to obtain the to_tax_class_id.';
comment on column tax_import_trans_control.to_trid is 'System-assigned identifier of a particular To Tax Record.';
comment on column tax_import_trans_control.to_trid_xlate is 'Value from the import file translated to obtain the to_trid.';
comment on column tax_import_trans_control.to_vintage_id is 'System-assigned identifier of a particular To Vintage.';
comment on column tax_import_trans_control.to_vintage_xlate is 'Value from the import file translated to obtain the to_vintage_id.';
comment on column tax_import_trans_control.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_trans_control.version_id is 'System-assigned identifier of a particular Version.';
comment on column tax_import_ua_depr.actual_cash_salvage is 'Salvage cash proceeds in dollars (positive).';
comment on column tax_import_ua_depr.actual_cost_of_removal is 'Cost of removal incurred in dollars (negative).';
comment on column tax_import_ua_depr.actual_reserve_credits is 'As part of maint 33359, we are splitting salvage into three columns from two columns';
comment on column tax_import_ua_depr.actual_salvage_returns is 'Salvage returns to store in dollars (positive).';
comment on column tax_import_ua_depr.book_depr is 'Dollar amount of book depreciation, depreciation adjustments, reserve adjustments, and transfers.';
comment on column tax_import_ua_depr.cor_rate is 'Cost of removal component of the book depreciation rate.';
comment on column tax_import_ua_depr.cor_reserve is 'Dollar amount of Cost of Removal expense provision, depreciation adjustments, reserve adjustments, and transfers.  Amount not included in book_depr column.';
comment on column tax_import_ua_depr.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_ua_depr.life_rate is 'Life component of the book depreciation rate.';
comment on column tax_import_ua_depr.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_ua_depr.rwip_cor_change is 'Change (end less beginning) in cost of removal dollars in RWIP.';
comment on column tax_import_ua_depr.rwip_reserve_credits_change is 'Change (end less beginning) in other credits in RWIP.';
comment on column tax_import_ua_depr.rwip_salvage_change is 'Change (end less beginning) in salvage dollars in RWIP.';
comment on column tax_import_ua_depr.salvage_rate is 'Salvage component of the book depreciation rate.';
comment on column tax_import_ua_depr.tax_utility_account_id is 'System-assigned identifier of a particular Tax Utility Account.';
comment on column tax_import_ua_depr.tax_utility_account_xlate is 'Value from the import file translated to obtain the tax_utility_account_id.';
comment on column tax_import_ua_depr.tax_year is 'Tax year being processed.';
comment on column tax_import_ua_depr.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_ua_depr.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_ua_depr.utility_account_depr_id is 'System-assigned identifier of a particular Utility Account Depr.';

comment on column tax_import_bk_alloc_assign_arc.book_alloc_assign_id is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.book_alloc_group_id is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.book_alloc_group_xlate is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.company_id is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.company_xlate is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.import_run_id is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.line_id is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.tax_class_id is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.tax_class_xlate is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.time_stamp is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_assign_arc.user_id is 'Archive of same column from tax_import_bk_alloc_assign.';
comment on column tax_import_bk_alloc_group_arc.book_alloc_group_id is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.company_id is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.company_xlate is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.description is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.hw_region_id is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.hw_region_xlate is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.hw_table_line_id is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.hw_table_line_xlate is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.import_run_id is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.life is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.line_id is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.mortality_curve_id is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.mortality_curve_xlate is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.time_stamp is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_bk_alloc_group_arc.user_id is 'Archive of same column from tax_import_bk_alloc_group.';
comment on column tax_import_class_rollups_arc.import_run_id is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.line_id is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.tax_class_id is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.tax_class_rollups_id is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.tax_class_xlate is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.tax_rollup_detail_id is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.tax_rollup_detail_xlate is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.tax_rollup_id is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.tax_rollup_xlate is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.time_stamp is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_class_rollups_arc.user_id is 'Archive of same column from tax_import_class_rollups.';
comment on column tax_import_loc_arc.description is 'Archive of same column from tax_import_loc.';
comment on column tax_import_loc_arc.import_run_id is 'Archive of same column from tax_import_loc.';
comment on column tax_import_loc_arc.line_id is 'Archive of same column from tax_import_loc.';
comment on column tax_import_loc_arc.tax_location_id is 'Archive of same column from tax_import_loc.';
comment on column tax_import_loc_arc.time_stamp is 'Archive of same column from tax_import_loc.';
comment on column tax_import_loc_arc.user_id is 'Archive of same column from tax_import_loc.';
comment on column tax_import_loc_asset_loc_arc.asset_location_id is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.asset_location_xlate is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.company_id is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.company_xlate is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.import_run_id is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.line_id is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.state_id is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.state_xlate is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.tax_location_id is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.tax_location_xlate is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.time_stamp is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_loc_asset_loc_arc.user_id is 'Archive of same column from tax_import_loc_asset_loc.';
comment on column tax_import_rollup_arc.description is 'Archive of same column from tax_import_rollup.';
comment on column tax_import_rollup_arc.import_run_id is 'Archive of same column from tax_import_rollup.';
comment on column tax_import_rollup_arc.line_id is 'Archive of same column from tax_import_rollup.';
comment on column tax_import_rollup_arc.tax_rollup_id is 'Archive of same column from tax_import_rollup.';
comment on column tax_import_rollup_arc.time_stamp is 'Archive of same column from tax_import_rollup.';
comment on column tax_import_rollup_arc.user_id is 'Archive of same column from tax_import_rollup.';
comment on column tax_import_rollup_detail_arc.description is 'Archive of same column from tax_import_rollup_detail.';
comment on column tax_import_rollup_detail_arc.import_run_id is 'Archive of same column from tax_import_rollup_detail.';
comment on column tax_import_rollup_detail_arc.line_id is 'Archive of same column from tax_import_rollup_detail.';
comment on column tax_import_rollup_detail_arc.tax_rollup_detail_id is 'Archive of same column from tax_import_rollup_detail.';
comment on column tax_import_rollup_detail_arc.tax_rollup_id is 'Archive of same column from tax_import_rollup_detail.';
comment on column tax_import_rollup_detail_arc.tax_rollup_xlate is 'Archive of same column from tax_import_rollup_detail.';
comment on column tax_import_rollup_detail_arc.time_stamp is 'Archive of same column from tax_import_rollup_detail.';
comment on column tax_import_rollup_detail_arc.user_id is 'Archive of same column from tax_import_rollup_detail.';
comment on column tax_import_tax_ua_arc.book_alloc_group_id is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.book_alloc_group_xlate is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.company_id is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.depr_group_id is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.depr_group_xlate is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.description is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.import_run_id is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.line_id is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.mortality_curve_id is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.tax_utility_account_id is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.time_stamp is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_tax_ua_arc.user_id is 'Archive of same column from tax_import_tax_ua.';
comment on column tax_import_trans_control_arc.book_amount is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_company_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_company_xlate is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_in_service_month is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_tax_class_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_tax_class_xlate is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_trid is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_trid_xlate is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_vintage_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.from_vintage_xlate is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.import_run_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.line_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.package_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.tax_transfer_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.tax_year is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.time_stamp is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_company_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_company_xlate is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_in_service_month is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_tax_class_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_tax_class_xlate is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_trid is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_trid_xlate is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_vintage_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.to_vintage_xlate is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.user_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_trans_control_arc.version_id is 'Archive of same column from tax_import_trans_control.';
comment on column tax_import_ua_depr_arc.actual_cash_salvage is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.actual_cost_of_removal is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.actual_reserve_credits is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.actual_salvage_returns is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.book_depr is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.cor_rate is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.cor_reserve is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.import_run_id is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.life_rate is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.line_id is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.rwip_cor_change is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.rwip_reserve_credits_change is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.rwip_salvage_change is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.salvage_rate is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.tax_utility_account_id is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.tax_utility_account_xlate is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.tax_year is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.time_stamp is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.user_id is 'Archive of same column from tax_import_ua_depr.';
comment on column tax_import_ua_depr_arc.utility_account_depr_id is 'Archive of same column from tax_import_ua_depr.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1129, 0, 10, 4, 3, 0, 37719, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037719_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
