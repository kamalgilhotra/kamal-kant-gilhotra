/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_multi5.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/03/2013 Brandon Beck   Point release
||============================================================================
*/

alter table LS_LEASE_CAP_TYPE add IS_OM number(1,0);

update LS_LEASE_CAP_TYPE set IS_OM = 0;

/****** IMPORT TABLES *****/

create table LS_IMPORT_LESSOR
(
 IMPORT_RUN_ID             number(22,0) not null,
 LINE_ID                   number(22,0) not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 LESSOR_ID                 number(22,0),
 DESCRIPTION               varchar2(35),
 LONG_DESCRIPTION          varchar2(254) ,
 ZIP                       number(5,0),
 COUNTY_XLATE              varchar2(254),
 COUNTY_ID                 char(18),
 STATE_XLATE               varchar2(254),
 STATE_ID                  char(18),
 COUNTRY_XLATE             varchar2(254),
 COUNTRY_ID                char(18),
 EXTERNAL_LESSOR_NUMBER    varchar2(35),
 MULTI_VENDOR_YN_SW        number(22,0),
 LEASE_GROUP_XLATE         varchar2(254),
 LEASE_GROUP_ID            number(22,0),
 ADDRESS1                  varchar2(35),
 ADDRESS2                  varchar2(35),
 ADDRESS3                  varchar2(35),
 ADDRESS4                  varchar2(35),
 POSTAL                    number(4,0),
 CITY                      varchar2(35),
 PHONE_NUMBER              varchar2(18),
 EXTENSION                 varchar2(8),
 FAX_NUMBER                varchar2(18),
 SITE_CODE                 varchar2(50),
 VENDOR_ID                 number(22,0),
 VENDOR_DESCRIPTION        varchar2(35) ,
 VENDOR_LONG_DESCRIPTION   varchar2(255),
 EXTERNAL_VENDOR_NUMBER    varchar2(35),
 ACTIVE_START              varchar2(254),
 ACTIVE_END                varchar2(254),
 REMIT_ADDR                varchar2(35),
 PRIMARY                   number(1,0),
 LOADED                    number(22,0),
 IS_MODIFIED               number(22,0),
 ERROR_MESSAGE             varchar2(4000),
 UNIQUE_LESSOR_IDENTIFIER  varchar2(35),
 UNIQUE_VENDOR_INDENTIFIER varchar2(35)
);

alter table LS_IMPORT_LESSOR
   add constraint LS_IMPORT_LESSOR_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_IMPORT_LESSOR
   add constraint LS_IMPORT_LESSOR_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);

create table LS_IMPORT_LEASE
(
 IMPORT_RUN_ID              number(22,0) not null,
 LINE_ID                    number(22,0) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 LEASE_ID                   number(22,0),
 LEASE_NUMBER               varchar2(35),
 DESCRIPTION                varchar2(35),
 LONG_DESCRIPTION           varchar2(254),
 LESSOR_XLATE               varchar2(254),
 LESSOR_ID                  number(22,0),
 LEASE_TYPE_XLATE           varchar2(254),
 LEASE_TYPE_ID              varchar2(254),
 PAYMENT_DUE_DAY            number(22,0),
 PRE_PAYMENT_SW             varchar2(35),
 LEASE_GROUP_XLATE          varchar(254),
 LEASE_GROUP_ID             number(22,0),
 LEASE_CAP_TYPE_XLATE       varchar2(254),
 LEASE_CAP_TYPE_ID          number(22,0),
 WORKFLOW_TYPE_XLATE        varchar2(254),
 WORKFLOW_TYPE_ID           number(22,0),
 MASTER_AGREEMENT_DATE      varchar2(254),
 NOTES                      varchar2(4000),
 PURCHASE_OPTION_TYPE_XLATE varchar2(254),
 PURCHASE_OPTION_TYPE_ID    number(22,0),
 PURCHASE_OPTION_AMT        number(22,2),
 RENEWAL_OPTION_TYPE_XLATE  varchar2(254),
 RENEWAL_OPTION_TYPE_ID     number(22,0),
 CANCELABLE_TYPE_XLATE      varchar2(254),
 CANCELABLE_TYPE_ID         number(22,0),
 ITC_SW                     varchar2(35),
 PARTIAL_RETIRE_SW          varchar2(35),
 SUBLET_SW                  varchar2(35),
 MUNI_BO_SW                 varchar2(35),
 COMPANY_XLATE              varchar2(254),
 COMPANY_ID                 number(22,0),
 MAX_LEASE_LINE             number(22,2),
 VENDOR_XLATE               varchar2(254),
 VENDOR_ID                  number(22,0),
 PAYMENT_PCT                number(22,12),
 CLASS_CODE_XLATE1          varchar2(254),
 CLASS_CODE_ID1             number(22,0),
 CLASS_CODE_VALUE1          varchar2(254),
 CLASS_CODE_XLATE2          varchar2(254),
 CLASS_CODE_ID2             number(22,0),
 CLASS_CODE_VALUE2          varchar2(254),
 CLASS_CODE_XLATE3          varchar2(254),
 CLASS_CODE_ID3             number(22,0),
 CLASS_CODE_VALUE3          varchar2(254),
 CLASS_CODE_XLATE4          varchar2(254),
 CLASS_CODE_ID4             number(22,0),
 CLASS_CODE_VALUE4          varchar2(254),
 CLASS_CODE_XLATE5          varchar2(254),
 CLASS_CODE_ID5             number(22,0),
 CLASS_CODE_VALUE5          varchar2(254),
 CLASS_CODE_XLATE6          varchar2(254),
 CLASS_CODE_ID6             number(22,0),
 CLASS_CODE_VALUE6          varchar2(254),
 CLASS_CODE_XLATE7          varchar2(254),
 CLASS_CODE_ID7             number(22,0),
 CLASS_CODE_VALUE7          varchar2(254),
 CLASS_CODE_XLATE8          varchar2(254),
 CLASS_CODE_ID8             number(22,0),
 CLASS_CODE_VALUE8          varchar2(254),
 CLASS_CODE_XLATE9          varchar2(254),
 CLASS_CODE_ID9             number(22,0),
 CLASS_CODE_VALUE9          varchar2(254),
 CLASS_CODE_XLATE10         varchar2(254),
 CLASS_CODE_ID10            number(22,0),
 CLASS_CODE_VALUE10         varchar2(254),
 CLASS_CODE_XLATE11         varchar2(254),
 CLASS_CODE_ID11            number(22,0),
 CLASS_CODE_VALUE11         varchar2(254),
 CLASS_CODE_XLATE12         varchar2(254),
 CLASS_CODE_ID12            number(22,0),
 CLASS_CODE_VALUE12         varchar2(254),
 CLASS_CODE_XLATE13         varchar2(254),
 CLASS_CODE_ID13            number(22,0),
 CLASS_CODE_VALUE13         varchar2(254),
 CLASS_CODE_XLATE14         varchar2(254),
 CLASS_CODE_ID14            number(22,0),
 CLASS_CODE_VALUE14         varchar2(254),
 CLASS_CODE_XLATE15         varchar2(254),
 CLASS_CODE_ID15            number(22,0),
 CLASS_CODE_VALUE15         varchar2(254),
 CLASS_CODE_XLATE16         varchar2(254),
 CLASS_CODE_ID16            number(22,0),
 CLASS_CODE_VALUE16         varchar2(254),
 CLASS_CODE_XLATE17         varchar2(254),
 CLASS_CODE_ID17            number(22,0),
 CLASS_CODE_VALUE17         varchar2(254),
 CLASS_CODE_XLATE18         varchar2(254),
 CLASS_CODE_ID18            number(22,0),
 CLASS_CODE_VALUE18         varchar2(254),
 CLASS_CODE_XLATE19         varchar2(254),
 CLASS_CODE_ID19            number(22,0),
 CLASS_CODE_VALUE19         varchar2(254),
 CLASS_CODE_XLATE20         varchar2(254),
 CLASS_CODE_ID20            number(22,0),
 CLASS_CODE_VALUE20         varchar2(254),
 LOADED                     number(22,0),
 IS_MODIFIED                number(22,0),
 ERROR_MESSAGE              varchar2(4000),
 UNIQUE_LEASE_IDENTIFIER    varchar2(35)
);

alter table LS_IMPORT_LEASE
   add constraint LS_IMPORT_LEASE_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_IMPORT_LEASE
   add constraint LS_IMPORT_LEASE_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);

drop table LS_IMPORT_ILR;

create table LS_IMPORT_ILR
(
 IMPORT_RUN_ID              number(22,0) not null,
 LINE_ID                    number(22,0) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 ILR_ID                     number(22,0),
 ILR_NUMBER                 varchar2(35),
 LEASE_XLATE                varchar2(254),
 LEASE_ID                   number(22,0),
 COMPANY_XLATE              varchar2(254),
 COMPANY_ID                 number(22,0),
 EST_IN_SVC_DATE            varchar2(254),
 ILR_GROUP_XLATE            varchar2(254),
 ILR_GROUP_ID               number(22,0),
 WORKFLOW_TYPE_XLATE        varchar2(254),
 WORKFLOW_TYPE_ID           number(22,0),
 EXTERNAL_ILR               varchar2(35),
 NOTES                      varchar2(4000),
 INCEPTION_AIR              number(22,8),
 PURCHASE_OPTION_TYPE_XLATE varchar2(254),
 PURCHASE_OPTION_TYPE_ID    number(22,0),
 PURCHASE_OPTION_AMT        number(22,2),
 RENEWAL_OPTION_TYPE_XLATE  varchar2(254),
 RENEWAL_OPTION_TYPE_ID     number(22,0),
 CANCELABLE_TYPE_XLATE      varchar2(254),
 CANCELABLE_TYPE_ID         number(22,0),
 ITC_SW                     varchar2(35),
 PARTIAL_RETIRE_SW          varchar2(35),
 SUBLET_SW                  varchar2(35),
 MUNI_BO_SW                 varchar2(35),
 LEASE_CAP_TYPE_XLATE       varchar2(254),
 LEASE_CAP_TYPE_ID          number(22,0),
 TERMINATION_AMT            number(22,2),
 PAYMENT_TERM_ID            number(22,0),
 PAYMENT_TERM_DATE          varchar2(254),
 PAYMENT_FREQ_XLATE         varchar2(254),
 PAYMENT_FREQ_ID            number(22,0),
 NUMBER_OF_TERMS            number(22,0),
 PAID_AMOUNT                number(22,2),
 EST_EXECUTORY_COST         number(22,2),
 CONTINGENT_AMOUNT          number(22,2),
 CLASS_CODE_XLATE1          varchar2(254),
 CLASS_CODE_ID1             number(22,0),
 CLASS_CODE_VALUE1          varchar2(254),
 CLASS_CODE_XLATE2          varchar2(254),
 CLASS_CODE_ID2             number(22,0),
 CLASS_CODE_VALUE2          varchar2(254),
 CLASS_CODE_XLATE3          varchar2(254),
 CLASS_CODE_ID3             number(22,0),
 CLASS_CODE_VALUE3          varchar2(254),
 CLASS_CODE_XLATE4          varchar2(254),
 CLASS_CODE_ID4             number(22,0),
 CLASS_CODE_VALUE4          varchar2(254),
 CLASS_CODE_XLATE5          varchar2(254),
 CLASS_CODE_ID5             number(22,0),
 CLASS_CODE_VALUE5          varchar2(254),
 CLASS_CODE_XLATE6          varchar2(254),
 CLASS_CODE_ID6             number(22,0),
 CLASS_CODE_VALUE6          varchar2(254),
 CLASS_CODE_XLATE7          varchar2(254),
 CLASS_CODE_ID7             number(22,0),
 CLASS_CODE_VALUE7          varchar2(254),
 CLASS_CODE_XLATE8          varchar2(254),
 CLASS_CODE_ID8             number(22,0),
 CLASS_CODE_VALUE8          varchar2(254),
 CLASS_CODE_XLATE9          varchar2(254),
 CLASS_CODE_ID9             number(22,0),
 CLASS_CODE_VALUE9          varchar2(254),
 CLASS_CODE_XLATE10         varchar2(254),
 CLASS_CODE_ID10            number(22,0),
 CLASS_CODE_VALUE10         varchar2(254),
 CLASS_CODE_XLATE11         varchar2(254),
 CLASS_CODE_ID11            number(22,0),
 CLASS_CODE_VALUE11         varchar2(254),
 CLASS_CODE_XLATE12         varchar2(254),
 CLASS_CODE_ID12            number(22,0),
 CLASS_CODE_VALUE12         varchar2(254),
 CLASS_CODE_XLATE13         varchar2(254),
 CLASS_CODE_ID13            number(22,0),
 CLASS_CODE_VALUE13         varchar2(254),
 CLASS_CODE_XLATE14         varchar2(254),
 CLASS_CODE_ID14            number(22,0),
 CLASS_CODE_VALUE14         varchar2(254),
 CLASS_CODE_XLATE15         varchar2(254),
 CLASS_CODE_ID15            number(22,0),
 CLASS_CODE_VALUE15         varchar2(254),
 CLASS_CODE_XLATE16         varchar2(254),
 CLASS_CODE_ID16            number(22,0),
 CLASS_CODE_VALUE16         varchar2(254),
 CLASS_CODE_XLATE17         varchar2(254),
 CLASS_CODE_ID17            number(22,0),
 CLASS_CODE_VALUE17         varchar2(254),
 CLASS_CODE_XLATE18         varchar2(254),
 CLASS_CODE_ID18            number(22,0),
 CLASS_CODE_VALUE18         varchar2(254),
 CLASS_CODE_XLATE19         varchar2(254),
 CLASS_CODE_ID19            number(22,0),
 CLASS_CODE_VALUE19         varchar2(254),
 CLASS_CODE_XLATE20         varchar2(254),
 CLASS_CODE_ID20            number(22,0),
 CLASS_CODE_VALUE20         varchar2(254),
 LOADED                     number(22,0),
 IS_MODIFIED                number(22,0),
 ERROR_MESSAGE              varchar2(4000),
 UNIQUE_ILR_IDENTIFIER      varchar2(35)
);

alter table LS_IMPORT_ILR
   add constraint LS_IMPORT_ILR_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_IMPORT_ILR
   add constraint LS_IMPORT_ILR_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);

drop table LS_IMPORT_ASSET;

create table LS_IMPORT_ASSET
(
 IMPORT_RUN_ID              number(22,0) not null,
 LINE_ID                    number(22,0) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 LS_ASSET_ID                number(22,0),
 LEASED_ASSET_NUMBER        varchar2(35),
 ILR_XLATE                  varchar2(254),
 ILR_ID                     number(22,0),
 DESCRIPTION                varchar2(35),
 LONG_DESCRIPTION           varchar2(35),
 QUANTITY                   number(22,2),
 FMV                        number(22,2),
 COMPANY_XLATE              varchar2(254),
 COMPANY_ID                 number(22,0),
 BUS_SEGMENT_XLATE          varchar2(254),
 BUS_SEGMENT_ID             number(22,0),
 UTILITY_ACCOUNT_XLATE      varchar2(254),
 UTILITY_ACCOUNT_ID         number(22,0),
 SUB_ACCOUNT_XLATE          varchar2(254),
 SUB_ACCOUNT_ID             number(22,0),
 RETIREMENT_UNIT_XLATE      varchar2(254),
 RETIREMENT_UNIT_ID         number(22,0),
 PROPERTY_GROUP_XLATE       varchar2(254),
 PROPERTY_GROUP_ID          number(22,0),
 WORK_ORDER_XLATE           varchar2(254),
 WORK_ORDER_ID              number(22,0),
 ASSET_LOCATION_XLATE       varchar2(254),
 ASSET_LOCATION_ID          number(22,0),
 GUARANTEED_RESIDUAL_AMOUNT number(22,2),
 EXPECTED_LIFE              number(22,0),
 ECONOMIC_LIFE              number(22,0),
 NOTES                      varchar2(4000),
 CLASS_CODE_XLATE1          varchar2(254),
 CLASS_CODE_ID1             number(22,0),
 CLASS_CODE_VALUE1          varchar2(254),
 CLASS_CODE_XLATE2          varchar2(254),
 CLASS_CODE_ID2             number(22,0),
 CLASS_CODE_VALUE2          varchar2(254),
 CLASS_CODE_XLATE3          varchar2(254),
 CLASS_CODE_ID3             number(22,0),
 CLASS_CODE_VALUE3          varchar2(254),
 CLASS_CODE_XLATE4          varchar2(254),
 CLASS_CODE_ID4             number(22,0),
 CLASS_CODE_VALUE4          varchar2(254),
 CLASS_CODE_XLATE5          varchar2(254),
 CLASS_CODE_ID5             number(22,0),
 CLASS_CODE_VALUE5          varchar2(254),
 CLASS_CODE_XLATE6          varchar2(254),
 CLASS_CODE_ID6             number(22,0),
 CLASS_CODE_VALUE6          varchar2(254),
 CLASS_CODE_XLATE7          varchar2(254),
 CLASS_CODE_ID7             number(22,0),
 CLASS_CODE_VALUE7          varchar2(254),
 CLASS_CODE_XLATE8          varchar2(254),
 CLASS_CODE_ID8             number(22,0),
 CLASS_CODE_VALUE8          varchar2(254),
 CLASS_CODE_XLATE9          varchar2(254),
 CLASS_CODE_ID9             number(22,0),
 CLASS_CODE_VALUE9          varchar2(254),
 CLASS_CODE_XLATE10         varchar2(254),
 CLASS_CODE_ID10            number(22,0),
 CLASS_CODE_VALUE10         varchar2(254),
 CLASS_CODE_XLATE11         varchar2(254),
 CLASS_CODE_ID11            number(22,0),
 CLASS_CODE_VALUE11         varchar2(254),
 CLASS_CODE_XLATE12         varchar2(254),
 CLASS_CODE_ID12            number(22,0),
 CLASS_CODE_VALUE12         varchar2(254),
 CLASS_CODE_XLATE13         varchar2(254),
 CLASS_CODE_ID13            number(22,0),
 CLASS_CODE_VALUE13         varchar2(254),
 CLASS_CODE_XLATE14         varchar2(254),
 CLASS_CODE_ID14            number(22,0),
 CLASS_CODE_VALUE14         varchar2(254),
 CLASS_CODE_XLATE15         varchar2(254),
 CLASS_CODE_ID15            number(22,0),
 CLASS_CODE_VALUE15         varchar2(254),
 CLASS_CODE_XLATE16         varchar2(254),
 CLASS_CODE_ID16            number(22,0),
 CLASS_CODE_VALUE16         varchar2(254),
 CLASS_CODE_XLATE17         varchar2(254),
 CLASS_CODE_ID17            number(22,0),
 CLASS_CODE_VALUE17         varchar2(254),
 CLASS_CODE_XLATE18         varchar2(254),
 CLASS_CODE_ID18            number(22,0),
 CLASS_CODE_VALUE18         varchar2(254),
 CLASS_CODE_XLATE19         varchar2(254),
 CLASS_CODE_ID19            number(22,0),
 CLASS_CODE_VALUE19         varchar2(254),
 CLASS_CODE_XLATE20         varchar2(254),
 CLASS_CODE_ID20            number(22,0),
 CLASS_CODE_VALUE20         varchar2(254),
 LOADED                     number(22,0),
 IS_MODIFIED                number(22,0),
 ERROR_MESSAGE              varchar2(4000),
 UNIQUE_ASSET_IDENTIFIER    varchar2(35)
);

alter table LS_IMPORT_ASSET
   add constraint LS_IMPORT_ASSET_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_IMPORT_ASSET
   add constraint LS_IMPORT_ASSET_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);

drop table LS_IMPORT_INVOICE;

create table LS_IMPORT_INVOICE
(
 IMPORT_RUN_ID      number(22,0) not null,
 LINE_ID            number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 COMPANY_XLATE      varchar2(254),
 COMPANY_ID         number(22,0),
 LEASE_XLATE        varchar2(254),
 LEASE_ID           number(22,0),
 GL_POSTING_MO_YR   varchar2(254),
 VENDOR_XLATE       varchar2(254),
 VENDOR_ID          number(22,0),
 PAYMENT_TYPE_XLATE varchar2(254),
 PAYMENT_TYPE_ID    number(22,0),
 AMOUNT             number(22,2),
 LS_ASSET_XLATE     varchar2(254),
 LS_ASSET_ID        number(22,0),
 LOADED             number(22,0),
 IS_MODIFIED        number(22,0),
 ERROR_MESSAGE      varchar2(4000)
);

alter table LS_IMPORT_INVOICE
   add constraint LS_IMPORT_INVOICE_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_IMPORT_INVOICE
   add constraint LS_IMPORT_INVOICE_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);


/***** ARCHIVE TABLES *****/

create table LS_IMPORT_LESSOR_ARCHIVE as (select * from LS_IMPORT_LESSOR where 1=0);

create table LS_IMPORT_LEASE_ARCHIVE as (select * from LS_IMPORT_LEASE where 1=0);

create table LS_IMPORT_ASSET_ARCHIVE as (select * from LS_IMPORT_ASSET where 1=0);

create table LS_IMPORT_INVOICE_ARCHIVE as (select * from LS_IMPORT_INVOICE where 1=0);

create table LS_IMPORT_ILR_ARCHIVE as (select * from LS_IMPORT_ILR where 1=0);



/***** PP_IMPORT CONFIG *****/

insert into PP_IMPORT_SUBSYSTEM
   (IMPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (8, 'Lessee', 'Lessee');

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   select max(IMPORT_TYPE_ID) + 1,
          'Add: Lessors',
          'Lessee Lessors',
          'ls_import_lessor',
          'ls_import_lessor_archive',
          null,
          1,
          'nvo_ls_logic_import',
          null,
          null,
          null
     from PP_IMPORT_TYPE;

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   select max(IMPORT_TYPE_ID) + 1,
          'Add: MLA''s',
          'Lessee MLA''s',
          'ls_import_lease',
          'ls_import_lease_archive',
          null,
          1,
          'nvo_ls_logic_import',
          null,
          null,
          null
     from PP_IMPORT_TYPE;

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   select max(IMPORT_TYPE_ID) + 1,
          'Add: ILR''s',
          'Lessee ILR''s',
          'ls_import_ilr',
          'ls_import_ilr_archive',
          null,
          1,
          'nvo_ls_logic_import',
          null,
          null,
          null
     from PP_IMPORT_TYPE;

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   select max(IMPORT_TYPE_ID) + 1,
          'Add: Leased Assets',
          'Lessee Assets',
          'ls_import_asset',
          'ls_import_asset_archive',
          null,
          1,
          'nvo_ls_logic_import',
          null,
          null,
          null
     from PP_IMPORT_TYPE;

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   select max(IMPORT_TYPE_ID) + 1,
          'Add: Lease Invoices',
          'Lessee Invoices',
          'ls_import_invoice',
          'ls_import_invoice_archive',
          null,
          1,
          'nvo_ls_logic_import',
          null,
          null,
          null
     from PP_IMPORT_TYPE;

insert into PP_IMPORT_TYPE_SUBSYSTEM
   (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID)
   select IMPORT_TYPE_ID, 8 from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%';

--Insert Columns
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   select TYPE.IMPORT_TYPE_ID,
          LOWER(COL.COLUMN_NAME),
          INITCAP(replace(COL.COLUMN_NAME, '_', ' ')),
          null,
          0,
          1,
          LOWER(COL.DATA_TYPE) || DECODE(COL.DATA_TYPE,
                                         'DATE',
                                         '',
                                         'NUMBER',
                                         '(' || COL.DATA_PRECISION || ',' || COL.DATA_SCALE || ')',
                                         '(' || COL.DATA_LENGTH || ')'),
          null,
          null,
          1,
          null,
          null
     from PP_IMPORT_TYPE type, ALL_TAB_COLUMNS COL
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor'
      and COL.TABLE_NAME = 'LS_IMPORT_LESSOR'
      and COL.COLUMN_NAME not like '%_XLATE%'
      and LOWER(COL.COLUMN_NAME) not in ('import_run_id', 'line_id', 'time_stamp', 'user_id');

--Set import_column_name
update PP_IMPORT_COLUMN A
   set A.IMPORT_COLUMN_NAME = LOWER(replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'))
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor')
   and UPPER(A.COLUMN_NAME) like '%_ID'
   and exists (select 1
          from ALL_TAB_COLUMNS
         where TABLE_NAME = 'LS_IMPORT_LESSOR'
           and COLUMN_NAME = replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'));

--Set parent_table
update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_lease_group', A.PARENT_TABLE_PK_COLUMN = 'lease_group_id'
 where A.COLUMN_NAME = 'lease_group_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'county', A.PARENT_TABLE_PK_COLUMN = 'county_id'
 where A.COLUMN_NAME = 'county_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'state', A.PARENT_TABLE_PK_COLUMN = 'state_id'
 where A.COLUMN_NAME = 'state_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'country', A.PARENT_TABLE_PK_COLUMN = 'country_id'
 where A.COLUMN_NAME = 'country_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor');

--Set is_required
update PP_IMPORT_COLUMN A
   set A.IS_REQUIRED = 1
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor')
   and UPPER(A.COLUMN_NAME) in ('DESCRIPTION',
                                'LONG_DESCRIPTION',
                                'ZIP',
                                'COUNTY_ID',
                                'STATE_ID',
                                'COUNTRY_ID',
                                'EXTERNAL_LESSOR_NUMBER',
                                'MULTI_VENDOR_YN_SW',
                                'LEASE_GROUP_ID');

--Insert Columns
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   select TYPE.IMPORT_TYPE_ID,
          LOWER(COL.COLUMN_NAME),
          INITCAP(replace(COL.COLUMN_NAME, '_', ' ')),
          null,
          0,
          1,
          LOWER(COL.DATA_TYPE) || DECODE(COL.DATA_TYPE,
                                         'DATE',
                                         '',
                                         'NUMBER',
                                         '(' || COL.DATA_PRECISION || ',' || COL.DATA_SCALE || ')',
                                         '(' || COL.DATA_LENGTH || ')'),
          null,
          null,
          1,
          null,
          null
     from PP_IMPORT_TYPE type, ALL_TAB_COLUMNS COL
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease'
      and COL.TABLE_NAME = 'LS_IMPORT_LEASE'
      and COL.COLUMN_NAME not like '%_XLATE%'
      and LOWER(COL.COLUMN_NAME) not in ('import_run_id', 'line_id', 'time_stamp', 'user_id');

--Set import_column_name
update PP_IMPORT_COLUMN A
   set A.IMPORT_COLUMN_NAME = LOWER(replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'))
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease')
   and UPPER(A.COLUMN_NAME) like '%_ID%'
   and exists (select 1
          from ALL_TAB_COLUMNS
         where TABLE_NAME = 'LS_IMPORT_LEASE'
           and COLUMN_NAME = replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'));

--Set parent_table
update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_lease_group', A.PARENT_TABLE_PK_COLUMN = 'lease_group_id'
 where A.COLUMN_NAME = 'lease_group_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_lessor', A.PARENT_TABLE_PK_COLUMN = 'lessor_id'
 where A.COLUMN_NAME = 'lessor_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_lease_cap_type', A.PARENT_TABLE_PK_COLUMN = 'ls_lease_cap_type_id'
 where A.COLUMN_NAME = 'lease_cap_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_lease_type', A.PARENT_TABLE_PK_COLUMN = 'lease_type_id'
 where A.COLUMN_NAME = 'lease_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'workflow_type', A.PARENT_TABLE_PK_COLUMN = 'workflow_type_id'
 where A.COLUMN_NAME = 'workflow_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_purchase_option_type',
       A.PARENT_TABLE_PK_COLUMN = 'purchase_option_type_id'
 where A.COLUMN_NAME = 'purchase_option_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_renewal_option_type',
       A.PARENT_TABLE_PK_COLUMN = 'renewal_option_type_id'
 where A.COLUMN_NAME = 'renewal_option_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_cancelable_type', A.PARENT_TABLE_PK_COLUMN = 'cancelable_type_id'
 where A.COLUMN_NAME = 'cancelable_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'company_setup', A.PARENT_TABLE_PK_COLUMN = 'company_id'
 where A.COLUMN_NAME = 'company_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_vendor', A.PARENT_TABLE_PK_COLUMN = 'vendor_id'
 where A.COLUMN_NAME = 'vendor_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'class_code', A.PARENT_TABLE_PK_COLUMN = 'class_code_id'
 where A.COLUMN_NAME like '%class_code_id%'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease');

--Set is_required
update PP_IMPORT_COLUMN A
   set A.IS_REQUIRED = 1
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease')
   and UPPER(A.COLUMN_NAME) in ('LEASE_NUMBER',
                                'DESCRIPTION',
                                'LONG_DESCRIPTION',
                                'LESSOR_ID',
                                'LEASE_TYPE_ID',
                                'PAYMENT_DUE_DAY',
                                'PRE_PAYMENT_SW',
                                'LEASE_GROUP_ID',
                                'LEASE_CAP_TYPE_ID',
                                'WORKFLOW_TYPE_ID',
                                'PURCHASE_OPTION_TYPE_ID',
                                'PURCHASE_OPTION_AMT',
                                'RENEWAL_OPTION_TYPE_ID',
                                'CANCELABLE_TYPE_ID',
                                'ITC_SW',
                                'PARTIAL_RETIRE_SW',
                                'SUBLET_SW',
                                'MUNI_BO_SW',
                                'COMPANY_ID',
                                'MAX_LEASE_LINE',
                                'VENDOR_ID',
                                'PAYMENT_PCT');

--Insert Columns
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   select TYPE.IMPORT_TYPE_ID,
          LOWER(COL.COLUMN_NAME),
          INITCAP(replace(COL.COLUMN_NAME, '_', ' ')),
          null,
          0,
          1,
          LOWER(COL.DATA_TYPE) || DECODE(COL.DATA_TYPE,
                                         'DATE',
                                         '',
                                         'NUMBER',
                                         '(' || COL.DATA_PRECISION || ',' || COL.DATA_SCALE || ')',
                                         '(' || COL.DATA_LENGTH || ')'),
          null,
          null,
          1,
          null,
          null
     from PP_IMPORT_TYPE type, ALL_TAB_COLUMNS COL
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr'
      and COL.TABLE_NAME = 'LS_IMPORT_ILR'
      and COL.COLUMN_NAME not like '%_XLATE%'
      and LOWER(COL.COLUMN_NAME) not in ('import_run_id', 'line_id', 'time_stamp', 'user_id');

--Set import_column_name
update PP_IMPORT_COLUMN A
   set A.IMPORT_COLUMN_NAME = LOWER(replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'))
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr')
   and UPPER(A.COLUMN_NAME) like '%_ID'
   and exists (select 1
          from ALL_TAB_COLUMNS
         where TABLE_NAME = 'LS_IMPORT_ILR'
           and COLUMN_NAME = replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'));

--Set parent table
update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_lease', A.PARENT_TABLE_PK_COLUMN = 'lease_id'
 where A.COLUMN_NAME = 'lease_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'company_setup', A.PARENT_TABLE_PK_COLUMN = 'company_id'
 where A.COLUMN_NAME = 'company_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_ilr_group', A.PARENT_TABLE_PK_COLUMN = 'ilr_group_id'
 where A.COLUMN_NAME = 'ilr_group_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'workflow_type', A.PARENT_TABLE_PK_COLUMN = 'workflow_type_id'
 where A.COLUMN_NAME = 'workflow_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_purchase_option_type',
       A.PARENT_TABLE_PK_COLUMN = 'purchase_option_type_id'
 where A.COLUMN_NAME = 'purchase_option_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_renewal_option_type',
       A.PARENT_TABLE_PK_COLUMN = 'renewal_option_type_id'
 where A.COLUMN_NAME = 'renewal_option_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_cancelable_type', A.PARENT_TABLE_PK_COLUMN = 'cancelable_type_id'
 where A.COLUMN_NAME = 'cancelable_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_lease_cap_type', A.PARENT_TABLE_PK_COLUMN = 'ls_lease_cap_type_id'
 where A.COLUMN_NAME = 'lease_cap_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_payment_freq', A.PARENT_TABLE_PK_COLUMN = 'payment_freq_id'
 where A.COLUMN_NAME = 'payment_freq_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'class_code', A.PARENT_TABLE_PK_COLUMN = 'class_code_id'
 where A.COLUMN_NAME like '%class_code_id%'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr');

--Set is_required
update PP_IMPORT_COLUMN A
   set A.IS_REQUIRED = 1
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr')
   and UPPER(A.COLUMN_NAME) in ('ILR_NUMBER',
                                'LEASE_ID',
                                'COMPANY_ID',
                                'EST_IN_SVC_DATE',
                                'ILR_GROUP_ID',
                                'WORK_FLOR_TYPE_ID',
                                'INCEPTION_AIR',
                                'PURCHASE_OPTION_TYPE_ID',
                                'PURCHASE_OPTION_AMT',
                                'RENEWAL_OPTION_TYPE_ID',
                                'CANCELABLE_TYPE_ID',
                                'ITC_SW',
                                'PARTIAL_RETIRE_SW',
                                'SUBLET_SW',
                                'MUNI_BO_SW',
                                'LEASE_CAP_TYPE_ID',
                                'PAYMENT_TERM_ID',
                                'PAYMENT_TERM_DATE',
                                'PAYMENT_FREQ_ID',
                                'NUMBER_OF_TERMS',
                                'PAID_AMOUNT',
                                'EST_EXECUTORY_COST',
                                'CONTINGENT_AMOUNT');

--Insert Columns
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   select TYPE.IMPORT_TYPE_ID,
          LOWER(COL.COLUMN_NAME),
          INITCAP(replace(COL.COLUMN_NAME, '_', ' ')),
          null,
          0,
          1,
          LOWER(COL.DATA_TYPE) || DECODE(COL.DATA_TYPE,
                                         'DATE',
                                         '',
                                         'NUMBER',
                                         '(' || COL.DATA_PRECISION || ',' || COL.DATA_SCALE || ')',
                                         '(' || COL.DATA_LENGTH || ')'),
          null,
          null,
          1,
          null,
          null
     from PP_IMPORT_TYPE type, ALL_TAB_COLUMNS COL
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset'
      and COL.TABLE_NAME = 'LS_IMPORT_ASSET'
      and COL.COLUMN_NAME not like '%_XLATE%'
      and LOWER(COL.COLUMN_NAME) not in ('import_run_id', 'line_id', 'time_stamp', 'user_id');

--Set import_column_name
update PP_IMPORT_COLUMN A
   set A.IMPORT_COLUMN_NAME = LOWER(replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'))
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset')
   and UPPER(A.COLUMN_NAME) like '%_ID'
   and exists (select 1
          from ALL_TAB_COLUMNS
         where TABLE_NAME = 'LS_IMPORT_ASSET'
           and COLUMN_NAME = replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'));

--Set parent table
update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_ilr', A.PARENT_TABLE_PK_COLUMN = 'ilr_id'
 where A.COLUMN_NAME = 'ilr_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'company_setup', A.PARENT_TABLE_PK_COLUMN = 'company_id'
 where A.COLUMN_NAME = 'company_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'business_segment', A.PARENT_TABLE_PK_COLUMN = 'bus_segment_id'
 where A.COLUMN_NAME = 'bus_segment_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'utility_account', A.PARENT_TABLE_PK_COLUMN = 'utility_account_id'
 where A.COLUMN_NAME = 'utility_account_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

--RO: 3 Column PK could cause issues here
update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'sub_account', A.PARENT_TABLE_PK_COLUMN = 'sub_account_id'
 where A.COLUMN_NAME = 'sub_account_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'retirement_unit', A.PARENT_TABLE_PK_COLUMN = 'retirement_unit_id'
 where A.COLUMN_NAME = 'retirement_unit_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'property_group', A.PARENT_TABLE_PK_COLUMN = 'property_group_id'
 where A.COLUMN_NAME = 'property_group_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'work_order_control', A.PARENT_TABLE_PK_COLUMN = 'work_order_id'
 where A.COLUMN_NAME = 'work_order_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'asset_location', A.PARENT_TABLE_PK_COLUMN = 'asset_location_id'
 where A.COLUMN_NAME = 'asset_location_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'class_code', A.PARENT_TABLE_PK_COLUMN = 'class_code_id'
 where A.COLUMN_NAME like '%class_code_id%'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset');

--Set is_required
update PP_IMPORT_COLUMN A
   set A.IS_REQUIRED = 1
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset')
   and UPPER(A.COLUMN_NAME) in ('LEASED_ASSET_NUMBER',
                                'DESCRIPTION',
                                'LONG_DESCRIPTION',
                                'QUANTITY',
                                'FMV',
                                'COMPANY_ID',
                                'BUS_SEGMENT_ID',
                                'UTILITY_ACCOUNT_ID',
                                'SUB_ACCOUNT_ID',
                                'RETIREMENT_UNIT_ID',
                                'PROPERTY_GROUP_ID',
                                'WORK_ORDER_ID',
                                'ASSET_LOCATION_ID',
                                'GUARANTEED_RESIDUAL_AMOUNT');

--Insert Columns
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   select TYPE.IMPORT_TYPE_ID,
          LOWER(COL.COLUMN_NAME),
          INITCAP(replace(COL.COLUMN_NAME, '_', ' ')),
          null,
          0,
          1,
          LOWER(COL.DATA_TYPE) || DECODE(COL.DATA_TYPE,
                                         'DATE',
                                         '',
                                         'NUMBER',
                                         '(' || COL.DATA_PRECISION || ',' || COL.DATA_SCALE || ')',
                                         '(' || COL.DATA_LENGTH || ')'),
          null,
          null,
          1,
          null,
          null
     from PP_IMPORT_TYPE type, ALL_TAB_COLUMNS COL
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice'
      and COL.TABLE_NAME = 'LS_IMPORT_INVOICE'
      and COL.COLUMN_NAME not like '%_XLATE%'
      and LOWER(COL.COLUMN_NAME) not in ('import_run_id', 'line_id', 'time_stamp', 'user_id');

--Set import_column_name
update PP_IMPORT_COLUMN A
   set A.IMPORT_COLUMN_NAME = LOWER(replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'))
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice')
   and UPPER(A.COLUMN_NAME) like '%_ID'
   and exists (select 1
          from ALL_TAB_COLUMNS
         where TABLE_NAME = 'LS_IMPORT_INVOICE'
           and COLUMN_NAME = replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'));

--Set parent table
update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'company_setup', A.PARENT_TABLE_PK_COLUMN = 'company_id'
 where A.COLUMN_NAME = 'company_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_lease', A.PARENT_TABLE_PK_COLUMN = 'lease_id'
 where A.COLUMN_NAME = 'lease_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_vendor', A.PARENT_TABLE_PK_COLUMN = 'vendor_id'
 where A.COLUMN_NAME = 'vendor_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_payment_type', A.PARENT_TABLE_PK_COLUMN = 'payment_type_id'
 where A.COLUMN_NAME = 'payment_type_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice');

update PP_IMPORT_COLUMN A
   set A.PARENT_TABLE = 'ls_asset', A.PARENT_TABLE_PK_COLUMN = 'ls_asset_id'
 where A.COLUMN_NAME = 'ls_asset_id'
   and A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice');

--Set is_required
update PP_IMPORT_COLUMN A
   set A.IS_REQUIRED = 1
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice')
   and UPPER(A.COLUMN_NAME) in
       ('COMPANY_ID', 'LEASE_ID', 'GL_POSTING_MO_YR', 'VENDOR_ID', 'PAYMENT_TYPE_ID', 'AMOUNT');

insert into PP_IMPORT_TEMPLATE
   (IMPORT_TEMPLATE_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE,
    DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL,
          TYPE.IMPORT_TYPE_ID,
          'Lessor Add',
          'Lessor Add',
          user,
          sysdate,
          1,
          null,
          null,
          0
     from PP_IMPORT_TYPE type
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor';

insert into PP_IMPORT_TEMPLATE
   (IMPORT_TEMPLATE_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE,
    DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL,
          TYPE.IMPORT_TYPE_ID,
          'MLA Add',
          'MLA Add',
          user,
          sysdate,
          1,
          null,
          null,
          0
     from PP_IMPORT_TYPE type
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_lease';

insert into PP_IMPORT_TEMPLATE
   (IMPORT_TEMPLATE_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE,
    DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL,
          TYPE.IMPORT_TYPE_ID,
          'ILR Add',
          'ILR Add',
          user,
          sysdate,
          1,
          null,
          null,
          0
     from PP_IMPORT_TYPE type
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr';

insert into PP_IMPORT_TEMPLATE
   (IMPORT_TEMPLATE_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE,
    DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL,
          TYPE.IMPORT_TYPE_ID,
          'Leased Asset Add',
          'Leased Asset Add',
          user,
          sysdate,
          1,
          null,
          null,
          0
     from PP_IMPORT_TYPE type
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_asset';

insert into PP_IMPORT_TEMPLATE
   (IMPORT_TEMPLATE_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE,
    DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL,
          TYPE.IMPORT_TYPE_ID,
          'Lease Invoice Add',
          'Lease Invoice Add',
          user,
          sysdate,
          1,
          null,
          null,
          0
     from PP_IMPORT_TYPE type
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice';

--Non Class Code Lookups (covers all lease tables)
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL,
    DERIVED_AUTOCREATE_YN)
   select (select max(IMPORT_LOOKUP_ID) from PP_IMPORT_LOOKUP) + ROWNUM,
          'Lessee ' || PPCOL.DESCRIPTION,
          'Lessee ' || PPCOL.DESCRIPTION,
          PPCOL.COLUMN_NAME,
          '( select b.' || PPCOL.COLUMN_NAME || ' from ' || PPCOL.PARENT_TABLE ||
          ' b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )',
          0,
          PPCOL.PARENT_TABLE,
          'description',
          null,
          null,
          null
     from PP_IMPORT_COLUMN PPCOL
    where PPCOL.IMPORT_TYPE_ID in
          (select TYPE.IMPORT_TYPE_ID
             from PP_IMPORT_TYPE type
            where TYPE.IMPORT_TABLE_NAME like 'ls_import%')
      and PPCOL.IMPORT_COLUMN_NAME is not null
      and PPCOL.COLUMN_NAME not like '%class_code%'
      and not exists
    (select 1 from PP_IMPORT_LOOKUP B where B.COLUMN_NAME = PPCOL.COLUMN_NAME);

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL,
    DERIVED_AUTOCREATE_YN)
   select max(IMPORT_LOOKUP_ID) + 1,
          'Lessee County Id',
          'Lessee County Id',
          'county_id',
          '( select b.county_id from county b where upper( trim( <importfield> ) ) = upper( trim( b.county_id ) ) )',
          0,
          'county',
          'county_id',
          null,
          null,
          null
     from PP_IMPORT_LOOKUP;

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL,
    DERIVED_AUTOCREATE_YN)
   select max(IMPORT_LOOKUP_ID) + 1,
          'Lessee Work Order Id',
          'Lessee Work Order Id',
          'work_order_id',
          '( select b.work_order_id from work_order_control b where upper( trim( <importfield> ) ) = upper( trim( b.work_order_number ) ) )',
          0,
          'work_order_control',
          'work_order_number',
          null,
          null,
          null
     from PP_IMPORT_LOOKUP;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select b.ls_lease_cap_type_id from ls_lease_cap_type b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )'
 where COLUMN_NAME = 'lease_cap_type_id';

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select b.country_id from country b where upper( trim( <importfield> ) ) = upper( trim( b.country_id ) ) )',
       LOOKUP_COLUMN_NAME = 'country_id'
 where COLUMN_NAME = 'country_id'
   and DESCRIPTION like 'Lessee%';

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select b.ilr_id from ls_ilr b where upper( trim( <importfield> ) ) = upper( trim( b.ilr_number ) ) )',
       LOOKUP_COLUMN_NAME = 'ilr_number'
 where COLUMN_NAME = 'ilr_id'
   and DESCRIPTION like 'Lessee%';

--Connect columns with lookups (covers all lease tables)
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select TYPE.IMPORT_TYPE_ID, PPCOL.COLUMN_NAME, LOOK.IMPORT_LOOKUP_ID
     from PP_IMPORT_TYPE type, PP_IMPORT_COLUMN PPCOL, PP_IMPORT_LOOKUP LOOK
    where PPCOL.IMPORT_TYPE_ID = TYPE.IMPORT_TYPE_ID
      and (LOOK.COLUMN_NAME = PPCOL.COLUMN_NAME or
          (LOOK.COLUMN_NAME = 'class_code_id' and PPCOL.COLUMN_NAME like 'class_code_id%'))
      and TYPE.IMPORT_TABLE_NAME like 'ls_import%'
      and PPCOL.COLUMN_NAME like '%_id%'
      and LOOK.LOOKUP_VALUES_ALTERNATE_SQL is null
      and LOOK.LOOKUP_COLUMN_NAME like '%description%'
      and not exists (select 1
             from PP_IMPORT_COLUMN_LOOKUP
            where IMPORT_TYPE_ID = TYPE.IMPORT_TYPE_ID
              and COLUMN_NAME = PPCOL.COLUMN_NAME
              and IMPORT_LOOKUP_ID = LOOK.IMPORT_LOOKUP_ID)
    order by TYPE.IMPORT_TABLE_NAME;

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID,
    AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select TEMP.IMPORT_TEMPLATE_ID,
          COL.COLUMN_ID,
          TYPE.IMPORT_TYPE_ID,
          PPCOL.COLUMN_NAME,
          max(CL.IMPORT_LOOKUP_ID),
          null,
          null,
          null
     from PP_IMPORT_TEMPLATE      TEMP,
          PP_IMPORT_TYPE          type,
          PP_IMPORT_COLUMN        PPCOL,
          PP_IMPORT_COLUMN_LOOKUP CL,
          ALL_TAB_COLUMNS         COL
    where ((TEMP.DESCRIPTION = 'Lessor Add' and TYPE.IMPORT_TABLE_NAME = 'ls_import_lessor') or
          (TEMP.DESCRIPTION = 'MLA Add' and TYPE.IMPORT_TABLE_NAME = 'ls_import_lease') or
          (TEMP.DESCRIPTION = 'ILR Add' and TYPE.IMPORT_TABLE_NAME = 'ls_import_ilr') or
          (TEMP.DESCRIPTION = 'Leased Asset Add' and TYPE.IMPORT_TABLE_NAME = 'ls_import_asset') or
          (TEMP.DESCRIPTION = 'Lease Invoice Add' and TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice'))
      and PPCOL.IMPORT_TYPE_ID = TYPE.IMPORT_TYPE_ID
      and CL.COLUMN_NAME(+) = PPCOL.COLUMN_NAME
         --and (cl.IMPORT_TYPE_ID in (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%') or cl.IMPORT_TYPE_ID is null)
      and UPPER(COL.TABLE_NAME) = UPPER(TYPE.IMPORT_TABLE_NAME)
      and UPPER(COL.COLUMN_NAME) = UPPER(PPCOL.COLUMN_NAME)
    group by TEMP.IMPORT_TEMPLATE_ID, COL.COLUMN_ID, TYPE.IMPORT_TYPE_ID, PPCOL.COLUMN_NAME
    order by COL.COLUMN_ID;

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID = null
 where (IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_lessor') and
       COLUMN_NAME = 'lessor_id')
    or (IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_lessor') and
       COLUMN_NAME = 'vendor_id')
    or (IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_lease') and
       COLUMN_NAME = 'lease_id')
    or (IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_ilr') and
       COLUMN_NAME = 'ilr_id')
    or (IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_asset') and
       COLUMN_NAME = 'ls_asset_id');

--Make Field ID's Sequential
update PP_IMPORT_TEMPLATE_FIELDS TF
   set TF.FIELD_ID =
        (select count(1)
           from PP_IMPORT_TEMPLATE_FIELDS B
          where B.IMPORT_TEMPLATE_ID = TF.IMPORT_TEMPLATE_ID
            and B.FIELD_ID <= TF.FIELD_ID)
 where TF.IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID
          from PP_IMPORT_TEMPLATE
         where DESCRIPTION in
               ('MLA Add', 'Lessor Add', 'ILR Add', 'Leased Asset Add', 'Lease Invoice Add'));

--Manually update the company template field to use description
update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'company_id'
            and LOOKUP_COLUMN_NAME = 'description')
 where COLUMN_NAME = 'company_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'county_id'
            and LOOKUP_COLUMN_NAME = 'county_id'
            and LOOKUP_VALUES_ALTERNATE_SQL is null)
 where COLUMN_NAME = 'county_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'state_id'
            and LOOKUP_COLUMN_NAME = 'long_description'
            and LOOKUP_VALUES_ALTERNATE_SQL is null)
 where COLUMN_NAME = 'state_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'work_order_id'
            and LOOKUP_COLUMN_NAME = 'work_order_number'
            and LOOKUP_CONSTRAINING_COLUMNS is null
            and DESCRIPTION like 'Lessee%')
 where COLUMN_NAME = 'work_order_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'sub_account_id'
            and LOOKUP_COLUMN_NAME = 'description'
            and LOOKUP_CONSTRAINING_COLUMNS is null)
 where COLUMN_NAME = 'sub_account_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'bus_segment_id'
            and LOOKUP_COLUMN_NAME = 'description'
            and LOOKUP_VALUES_ALTERNATE_SQL is null)
 where COLUMN_NAME = 'bus_segment_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'utility_account_id'
            and LOOKUP_COLUMN_NAME = 'description'
            and LOOKUP_VALUES_ALTERNATE_SQL is null)
 where COLUMN_NAME = 'utility_account_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'asset_location_id'
            and LOOKUP_COLUMN_NAME = 'long_description'
            and LOOKUP_VALUES_ALTERNATE_SQL is null)
 where COLUMN_NAME = 'asset_location_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'retirement_unit_id'
            and LOOKUP_COLUMN_NAME = 'description'
            and LOOKUP_VALUES_ALTERNATE_SQL is null)
 where COLUMN_NAME = 'retirement_unit_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (565, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_multi5.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
