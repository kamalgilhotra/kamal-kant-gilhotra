/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030548_system_PKG_PP_SYSTEM_CONTROL.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 B. Beck        Point release
||============================================================================
*/

create or replace package PKG_PP_SYSTEM_CONTROL is
   /*
   ||============================================================================
   || Subsystem: PowerPlant
   || Object Name: PKG_PP_SYSTEM_CONTROL
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/09/2013 B.Beck         Original Version
   ||============================================================================
   */

   function F_PP_SYSTEM_CONTROL_COMPANY(A_CONTROL_NAME varchar2,
                                        A_COMPANY_ID   number default -1) return varchar2;
   function F_PP_SYSTEM_CONTROL(A_CONTROL_NAME varchar2) return varchar2;

end PKG_PP_SYSTEM_CONTROL;
/

create or replace package body PKG_PP_SYSTEM_CONTROL is
   /*
   ||============================================================================
   || Subsystem: PowerPlant
   || Object Name: PKG_PP_SYSTEM_CONTROL
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/09/2013 B.Beck         Original Version
   ||============================================================================
   */

   --**************************************************************************
   --                            F_PP_SYSTEM_CONTROL_COMPANY
   --**************************************************************************
   function F_PP_SYSTEM_CONTROL_COMPANY(A_CONTROL_NAME varchar2,
                                        A_COMPANY_ID   number default -1) return varchar2 is

      /*
      ||============================================================================
      || Function :   f_pp_system_control_company
      ||
      || Purpose  :   returns the  control value in LOWER CASE  for a given control name and
      ||              company in pp_system_control.
      ||              If the control name is not found in pp_system_control for the given company,
      ||              it returns the value for company_id = -1. If there is no control even for
      ||              company_id = -1 the it returns an empty string ""
      ||
      || Arguments:   a_control_name          : string
      ||              a_company_id            : long
      ||
      || Returns :    control_value           : string    :  control value in LOWER CASE for control, or empty string ""
      ||
      ||============================================================================
      */

      -- returns the  control value in lower case for a given control name in pp_system_control.
      -- If the control name is not found in pp_system_control, it returns an empty string ""

      CONTROL_VALUE  PP_SYSTEM_CONTROL_COMPANY.CONTROL_VALUE%type;
      ARG_COMPANY_ID number := A_COMPANY_ID;

   begin
      if ARG_COMPANY_ID is null or ARG_COMPANY_ID = 0 then
         ARG_COMPANY_ID := -1;
      end if;

      select max(trim(LOWER(CONTROL_VALUE)))
        into CONTROL_VALUE
        from PP_SYSTEM_CONTROL_COMPANY
       where trim(LOWER(CONTROL_NAME)) = trim(LOWER(A_CONTROL_NAME))
         and COMPANY_ID = ARG_COMPANY_ID;

      if trim(CONTROL_VALUE) is null and ARG_COMPANY_ID <> -1 then
         select max(trim(LOWER(CONTROL_VALUE)))
           into CONTROL_VALUE
           from PP_SYSTEM_CONTROL_COMPANY
          where trim(LOWER(CONTROL_NAME)) = trim(LOWER(A_CONTROL_NAME))
            and COMPANY_ID = -1;
      end if;

      return CONTROL_VALUE;

   exception
      when others then
         return null;

   end F_PP_SYSTEM_CONTROL_COMPANY;

   --**************************************************************************
   --                            F_PP_SYSTEM_CONTROL
   --**************************************************************************
   function F_PP_SYSTEM_CONTROL(A_CONTROL_NAME varchar2) return varchar2 is
      CONTROL_VALUE PP_SYSTEM_CONTROL_COMPANY.CONTROL_VALUE%type;
   begin
      CONTROL_VALUE := F_PP_SYSTEM_CONTROL_COMPANY(A_CONTROL_NAME, -1);

      return CONTROL_VALUE;

   exception
      when others then
         return null;

   end F_PP_SYSTEM_CONTROL;

end PKG_PP_SYSTEM_CONTROL;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (446, 0, 10, 4, 1, 0, 30548, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030548_system_PKG_PP_SYSTEM_CONTROL.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
