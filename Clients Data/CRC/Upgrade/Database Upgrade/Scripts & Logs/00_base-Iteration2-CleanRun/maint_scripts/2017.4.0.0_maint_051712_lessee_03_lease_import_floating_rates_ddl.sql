 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051712_lessee_03_lease_import_floating_rates_ddl.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.4.0.0 06/26/2018 Shane "C" Ward  Updates to Import table for new Floating Rates table
 ||============================================================================
 */


 --ILR Group is no longer part of Floating Rates Import
ALTER TABLE LS_IMPORT_FLOAT_RATES
DROP COLUMN ilr_group_xlate;

ALTER TABLE LS_IMPORT_FLOAT_RATES
DROP COLUMN ilr_group_id;

--Not removing from the archive table just in case Implementers need it. No harm in keeping it there

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7624, 0, 2017, 4, 0, 0, 51712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051712_lessee_03_lease_import_floating_rates_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
