/*
||============================================================================================
|| Application: PowerPlant
|| File Name:   maint_047500_lease_mc_ls_payment_hdr_view_ddl.sql
||============================================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- -----------------------------------------------------
|| 2017.1.0.0  04/21/2017 Anand R        PP-47500 Create a view to display currency conversion
||============================================================================================
*/

create view v_ls_payment_hdr_fx as 
WITH
cur AS (
    SELECT /*materialize*/ 1 ls_cur_type, contract_cur.currency_id AS currency_id, contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code
    FROM currency contract_cur
    UNION
    SELECT /*materialize*/ 2, company_cur.currency_id, company_cur.currency_display_symbol, company_cur.iso_code
    FROM currency company_cur
    ),
  cr AS (
    SELECT /*materialize*/ exchange_date, currency_from, currency_to, rate from currency_rate
    union
    select /*materialize*/ to_date('19000101' , 'yyyymmdd') exchange_date, currency_from.currency_id currency_from, currency_to.currency_id currency_to, 
           decode( currency_from.currency_id - currency_to.currency_id, 0, 1, 0) rate 
    from currency currency_from, currency currency_to
  )

SELECT lph.payment_id, 
       lph.lease_id, 
       lph.vendor_id, 
       lph.company_id, 
       lph.amount original_amount,
       Round(lph.amount * cr.rate,2) amount, 
       lph.description, lph.gl_posting_mo_yr,
       lph.payment_status_id, 
       lph.ap_status_id, 
       lph.ls_asset_id, 
       lph.ilr_id,
       lease.contract_currency_id, 
       cs.currency_id company_currency_id,
       cur.ls_cur_type AS ls_cur_type,
       cr.exchange_date,
       --cur.currency_id,
       cr.rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_payment_hdr lph     
INNER JOIN ls_lease lease
      ON lph.lease_id = lease.lease_id 
INNER JOIN currency_schema cs
      ON lph.company_id = cs.company_id      
INNER JOIN cur      
      ON (
          (cur.ls_cur_type = 1 AND cur.currency_id = lease.contract_currency_id)
          OR
          (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id) 
         )
INNER JOIN cr
      ON cr.currency_from =  lease.contract_currency_id
      AND cr.currency_to =  cur.currency_id
WHERE cr.exchange_date =  ( SELECT /*materialize*/ max(c.exchange_date) from cr c 
                            WHERE c.currency_from = cr.currency_from
                            AND c.currency_to = cr.currency_to
                            AND c.exchange_date <= add_months(lph.gl_posting_mo_yr, 1)
                          )

						  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3439, 0, 2017, 1, 0, 0, 47500, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047500_lease_mc_ls_payment_hdr_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;						  
						  
