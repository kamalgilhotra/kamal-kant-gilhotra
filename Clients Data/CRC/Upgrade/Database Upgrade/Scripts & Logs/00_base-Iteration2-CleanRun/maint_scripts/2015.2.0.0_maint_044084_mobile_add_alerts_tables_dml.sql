/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044084_mobile_add_alerts_tables_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   06/12/2015 Daniel Motter  Creation
||============================================================================
*/

/* Populate mobile_display table */
insert into mobile_display (display_id, description) values (1, 'Work Order Details');

/* Populate mobile_inputs table */
insert into mobile_inputs (input_id, description) values (1, 'In-Service Date');
insert into mobile_inputs (input_id, description) values (2, 'Estimate In-Service Date');
insert into mobile_inputs (input_id, description) values (3, 'Completion Date');
insert into mobile_inputs (input_id, description) values (4, 'Estimated Completion Date');

/* Populate mobile_disp_inpts_mapping table */
insert into mobile_disp_inpts_mapping (display_id, input_id) values (1, 1);
insert into mobile_disp_inpts_mapping (display_id, input_id) values (1, 2);
insert into mobile_disp_inpts_mapping (display_id, input_id) values (1, 3);
insert into mobile_disp_inpts_mapping (display_id, input_id) values (1, 4);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2618, 0, 2015, 2, 0, 0, 044084, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044084_mobile_add_alerts_tables_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;