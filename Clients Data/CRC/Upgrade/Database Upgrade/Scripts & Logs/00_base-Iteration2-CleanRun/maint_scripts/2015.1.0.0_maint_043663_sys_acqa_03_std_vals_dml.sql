
/*
 ||============================================================================
 || Application: acqaider_app
 || File Name: maint_043663_sys_acqa_03_std_vals_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2015.1.0.0 03/20/2015 Sherri Ramson  <Values for New tables for stand alone acqaider app>
 ||============================================================================
 */
--acqa_wizard
INSERT INTO acqa_wizard VALUES (
	1,
	TO_DATE('2-02-2015 13:49:26','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Company Setup',
	'This wizard is used to create company header information.',
	1,
	'uo_acqaider_wizard_wkspc_company',
	'uo_acqaider_wizard_wkspc_company',
	1);
INSERT INTO acqa_wizard VALUES (
	2,
	TO_DATE('2-02-2015 13:49:26','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Base Tables - Maintenance',
	'Need to update object name to correct uo once created, i.e. - uo_acqaider_bt_tab',
	2,
	'uo_acqaider_wizard_wkspc_bt',
	'uo_acqaider_wizard_wkspc_bt',
	1);
INSERT INTO acqa_wizard VALUES (
	3,
	TO_DATE('2-02-2015 13:49:26','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Base Tables - Unit Catalog',
	'Workspace for unit catalog maint',
	3,
	'uo_acqaider_wizard_wkspc_unit_cat',
	'uo_acqaider_wizard_wkspc_unit_cat',
	1);
INSERT INTO acqa_wizard VALUES (
	4,
	TO_DATE('2-02-2015 13:49:26','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Base Tables - Locations',
	'Workspace for location maint',
	4,
	'uo_acqaider_wizard_wkspc_locations',
	'uo_acqaider_wizard_wkspc_locations',
	1);
INSERT INTO acqa_wizard VALUES (
	5,
	TO_DATE('2-02-2015 13:49:26','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Depreciation Config',
	'Workspace for depr maint',
	5,
	'uo_acqaider_wizard_wkspc_depr',
	'uo_acqaider_wizard_wkspc_depr',
	1);
INSERT INTO acqa_wizard VALUES (
	6,
	TO_DATE('2-02-2015 13:49:26','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Manage Project Setup',
	'Workspace for project maint',
	6,
	'uo_acqaider_wizard_wkspc_project',
	'uo_acqaider_wizard_wkspc_project',
	1);
INSERT INTO acqa_wizard VALUES (
	7,
	TO_DATE('2-02-2015 13:49:34','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Charge Repository',
	'This wizard is used to configure the charge repository.',
	7,
	'uo_acqaider_under_construction_wksp',
	'uo_acqaider_under_construction_wksp',
	0);
INSERT INTO acqa_wizard VALUES (
	8,
	TO_DATE('2-02-2015 13:49:26','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'System / Admin',
	'This wizard is used to configure system and administrative items.',
	8,
	'uo_acqaider_under_construction_wksp',
	'uo_acqaider_under_construction_wksp',
	0);
INSERT INTO acqa_wizard VALUES (
	9,
	TO_DATE('2-02-2015 13:49:34','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Income Tax',
	'This wizard is used to configure income tax (PowerTax).',
	9,
	'uo_acqaider_under_construction_wksp',
	'uo_acqaider_under_construction_wksp',
	0);
INSERT INTO acqa_wizard VALUES (
	10,
	TO_DATE('2-02-2015 13:49:34','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Tax Provision',
	'This wizard is used to configure tax provision.',
	10,
	'uo_acqaider_under_construction_wksp',
	'uo_acqaider_under_construction_wksp',
	0);
INSERT INTO acqa_wizard VALUES (
	11,
	TO_DATE('2-02-2015 13:49:34','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Property Tax',
	'This wizard is used to configure property tax.',
	11,
	'uo_acqaider_under_construction_wksp',
	'uo_acqaider_under_construction_wksp',
	0);
INSERT INTO acqa_wizard VALUES (
	12,
	TO_DATE('2-02-2015 13:49:34','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Lease',
	'This wizard is used to configure lease.',
	12,
	'uo_acqaider_under_construction_wksp',
	'uo_acqaider_under_construction_wksp',
	0);
--acqa_wizard_status
INSERT INTO acqa_wizard_status VALUES (
	1,
	NULL,
	NULL,
	'Not Started',
	'This wizard has not been run.');
INSERT INTO acqa_wizard_status VALUES (
	2,
	NULL,
	NULL,
	'In Progress',
	'This wizard has been launched but is not complete.');
INSERT INTO acqa_wizard_status VALUES (
	3,
	NULL,
	NULL,
	'Succeeded',
	'This wizard has been successfully run.');
INSERT INTO acqa_wizard_status VALUES (
	4,
	NULL,
	NULL,
	'Failed',
	'This wizard has been run, but it failed.');
--ppbase_workspace
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'welcome',
	TO_DATE('13-01-2015 10:33:44','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Company Wizard',
	'uo_acqaider_welcome_wksp',
	'Welcome',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'asset_loader',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Asset Loader',
	'w_cpr_loader',
	'Asset Loader',
	2);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'depr_loader',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Depr Loader',
	'w_depr_loader',
	'Depr Loader',
	2);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'depr_rates_loader',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Depr Rates Loader',
	'w_depr_rates_loader',
	'Depr Rates Loader',
	2);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'aro_loader',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'ARO Loader',
	'uo_acqaider_aro_wksp_welcome',
	'ARO Loader',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'aro_input',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'ARO Input',
	'uo_acqaider_aro_wksp_input',
	'ARO Input',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'aro_input_header',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'ARO Input Header',
	'uo_acqaider_aro_wksp_input_header',
	'ARO Input Header',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'aro_input_layer',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'ARO Input Layer',
	'uo_acqaider_aro_wksp_input_layer',
	'ARO Input Layer',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'aro_input_liability',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'ARO Input Liability',
	'uo_acqaider_aro_wksp_input_liability',
	'ARO Input Liability',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'aro_template_import',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'ARO Template Import',
	'uo_acqaider_aro_wksp_import',
	'ARO Template Import',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'loaders',
	TO_DATE('7-01-2015 15:07:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Loaders',
	'uo_acqaider_loader_wksp',
	'Loaders',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'loaders2',
	TO_DATE('9-01-2015 13:19:02','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Loaders2',
	'w_wo_loader',
	'Loaders2',
	2);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'aro_conversion',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'ARO Conversion',
	'uo_acqaider_aro_wksp_conversion',
	'ARO Conversion',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'validation',
	TO_DATE('13-01-2015 15:20:19','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Validation',
	'uo_acqaider_under_construction_wksp',
	'Final Validation',
	1);
INSERT INTO ppbase_workspace VALUES (
	'acqaider',
	'reports',
	TO_DATE('13-01-2015 15:08:49','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	'Reports',
	'uo_acqaider_under_construction_wksp',
	'Reports',
	1);
--ppbase_menu_items
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'asset_loader',
	TO_DATE('13-01-2015 13:07:34','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	10,
	'Asset Loader',
	NULL,
	NULL,
	'asset_loader',
	0);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'depr_loader',
	TO_DATE('13-01-2015 13:05:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	3,
	'Depr Loader',
	NULL,
	NULL,
	'depr_loader',
	0);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'depr_rates_loader',
	TO_DATE('13-01-2015 13:05:29','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	4,
	'Depr Rates Loader',
	NULL,
	NULL,
	'depr_rates_loader',
	0);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'aro_loader',
	TO_DATE('13-01-2015 13:05:48','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	5,
	'ARO Loader',
	NULL,
	NULL,
	'aro_loader',
	0);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'aro_input',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	2,
	1,
	'ARO Input',
	NULL,
	'aro_loader',
	'aro_input',
	1);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'aro_input_header',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	3,
	1,
	'ARO Input Header',
	NULL,
	'aro_input',
	'aro_input_header',
	1);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'aro_input_layer',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	3,
	2,
	'ARO Input Layer',
	NULL,
	'aro_input',
	'aro_input_layer',
	1);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'aro_input_liability',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	3,
	3,
	'ARO Input Liability',
	NULL,
	'aro_input',
	'aro_input_liability',
	1);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'aro_template_import',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	2,
	2,
	'ARO Import',
	NULL,
	'aro_loader',
	'aro_template_import',
	1);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'aro_conversion',
	TO_DATE('26-09-2014 22:08:18','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	2,
	3,
	'ARO Conversion',
	NULL,
	'aro_loader',
	'aro_conversion',
	1);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'validation',
	TO_DATE('13-01-2015 13:11:20','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	6,
	'Validation',
	NULL,
	NULL,
	'validation',
	0);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'reports',
	TO_DATE('26-09-2014 22:51:43','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	7,
	'Reports',
	NULL,
	NULL,
	'reports',
	0);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'welcome',
	TO_DATE('13-01-2015 10:34:50','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	1,
	'Company Wizard',
	NULL,
	NULL,
	'welcome',
	1);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'loaders2',
	TO_DATE('13-01-2015 13:04:01','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	9,
	'Loaders2',
	NULL,
	NULL,
	'loaders2',
	0);
INSERT INTO ppbase_menu_items VALUES (
	'acqaider',
	'loaders',
	TO_DATE('13-01-2015 13:07:55','DD-MM-YYYY HH24:MI:SS'),
	'PWRPLANT',
	1,
	2,
	'Loaders',
	NULL,
	NULL,
	'loaders',
	1);
--acqa_overview_questions
INSERT INTO acqa_overview_questions VALUES (
	2,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Departments?',
	2,
	'department',
	'tabpage_department',
	20);
INSERT INTO acqa_overview_questions VALUES (
	3,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Functional Classes?',
	2,
	'func_class',
	'tabpage_func_class',
	30);
INSERT INTO acqa_overview_questions VALUES (
	6,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:43:11','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Company/Business Segment relationships',
	2,
	'company_bus_segment_control',
	'tabpage_bus_seg_relate',
	15);
INSERT INTO acqa_overview_questions VALUES (
	32,
	'PWRPLANT',
	TO_DATE('23-01-2015 14:12:13','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to import a New Funding Project?',
	12,
	NULL,
	'tabpage_loc_type',
	320);
INSERT INTO acqa_overview_questions VALUES (
	1,
	'PWRPLANT',
	TO_DATE('2-02-2015 11:45:12','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Business Segments?',
	2,
	'business_segment',
	'tabpage_bus_seg',
	10);
INSERT INTO acqa_overview_questions VALUES (
	5,
	'PWRPLANT',
	TO_DATE('15-01-2015 14:18:35','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Plant Accounts?',
	2,
	'utility_account',
	'tabpage_accounts',
	50);
INSERT INTO acqa_overview_questions VALUES (
	4,
	'PWRPLANT',
	TO_DATE('2-02-2015 12:24:35','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new GL Accounts?',
	2,
	'gl_account',
	'tabpage_gl',
	40);
INSERT INTO acqa_overview_questions VALUES (
	18,
	'PWRPLANT',
	TO_DATE('9-01-2015 10:01:31','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Divisions?',
	2,
	'division',
	'tabpage_department',
	18);
INSERT INTO acqa_overview_questions VALUES (
	19,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:47:52','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Retirement Units?',
	3,
	'retirement_unit',
	'tabpage_unit_cat',
	105);
INSERT INTO acqa_overview_questions VALUES (
	20,
	'PWRPLANT',
	TO_DATE('2-02-2015 13:09:02','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Property Groups?',
	3,
	'property_group',
	'tabpage_prop_group',
	200);
INSERT INTO acqa_overview_questions VALUES (
	7,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Major Locations?',
	4,
	'major_location',
	'tabpage_location',
	70);
INSERT INTO acqa_overview_questions VALUES (
	23,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:46:12','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Sub Accounts?',
	2,
	'sub_account',
	'tabpage_accounts',
	55);
INSERT INTO acqa_overview_questions VALUES (
	24,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Functional Class/Property Group relationships?',
	3,
	'func_class_prop_grp',
	'tabpage_prop_unit_relate',
	240);
INSERT INTO acqa_overview_questions VALUES (
	25,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Property Unit/Utility Account relationships?',
	3,
	'util_acct_prop_unit',
	'tabpage_prop_unit_relate',
	250);
INSERT INTO acqa_overview_questions VALUES (
	26,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Propery Unit/Property Group relationships?',
	3,
	'prop_group_prop_unit',
	'tabpage_prop_unit_relate',
	260);
INSERT INTO acqa_overview_questions VALUES (
	27,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Functional Class/Location Type relationships?',
	4,
	'func_class_loc_type',
	'tabpage_loc_type_relate',
	270);
INSERT INTO acqa_overview_questions VALUES (
	28,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:45:06','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Company/GL Account relationships?',
	2,
	'company_gl_account',
	'tabpage_gl_relate',
	44);
INSERT INTO acqa_overview_questions VALUES (
	29,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:45:16','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Business Segment/GL Account relationships?',
	2,
	'gl_acct_bus_segment',
	'tabpage_gl_relate',
	47);
INSERT INTO acqa_overview_questions VALUES (
	30,
	'PWRPLANT',
	TO_DATE('9-01-2015 11:07:53','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new AFUDC Data?',
	6,
	'afudc_data',
	'tabpage_afudc',
	125);
INSERT INTO acqa_overview_questions VALUES (
	31,
	'PWRPLANT',
	TO_DATE('9-01-2015 11:08:23','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Funding Project Types?',
	6,
	'work_order_type_fp',
	'tabpage_wo_type',
	128);
INSERT INTO acqa_overview_questions VALUES (
	8,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Class Codes?',
	2,
	'class_code',
	'tabpage_class_codes',
	80);
INSERT INTO acqa_overview_questions VALUES (
	9,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:49:13','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Company/Major Location relationships?',
	4,
	'company_major_location',
	'tabpage_loc_type_relate',
	265);
INSERT INTO acqa_overview_questions VALUES (
	10,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Property Units?',
	3,
	'property_unit',
	'tabpage_unit_cat',
	100);
INSERT INTO acqa_overview_questions VALUES (
	11,
	'PWRPLANT',
	TO_DATE('9-01-2015 10:55:59','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Company/Property Unit relationships?',
	3,
	'company_property_unit',
	'tabpage_prop_unit_relate',
	205);
INSERT INTO acqa_overview_questions VALUES (
	12,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new AFUDC Types?',
	6,
	'afudc_control',
	'tabpage_afudc',
	120);
INSERT INTO acqa_overview_questions VALUES (
	13,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Work Order Types?',
	6,
	'work_order_type',
	'tabpage_wo_type',
	130);
INSERT INTO acqa_overview_questions VALUES (
	14,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Workflow Approval Types?',
	6,
	'workflow_type',
	'tabpage_apr_types',
	140);
INSERT INTO acqa_overview_questions VALUES (
	15,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Workflow Approval Levels?',
	6,
	'workflow_rule',
	'tabpage_apr_lvl',
	150);
INSERT INTO acqa_overview_questions VALUES (
	16,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to maintain Workflow Type/Level relationships?',
	6,
	'workflow_type_rule',
	'tabpage_apr_lvl_relate',
	160);
INSERT INTO acqa_overview_questions VALUES (
	17,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Workflow Available Users?',
	6,
	'approval_auth_level',
	'tabpage_users',
	170);
INSERT INTO acqa_overview_questions VALUES (
	21,
	'PWRPLANT',
	TO_DATE('8-01-2015 16:42:38','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Asset Locations?',
	4,
	'asset_location',
	'tabpage_location',
	210);
INSERT INTO acqa_overview_questions VALUES (
	22,
	'PWRPLANT',
	TO_DATE('23-01-2015 14:12:58','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Location Types?',
	4,
	'location_type',
	'tabpage_loc_type',
	220);
INSERT INTO acqa_overview_questions VALUES (
	33,
	'PWRPLANT',
	TO_DATE('6-03-2015 10:30:26','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to create new Depreciation Summaries?',
	5,
	'depr_summary',
	'tabpage_depr_summary',
	330);
INSERT INTO acqa_overview_questions VALUES (
	34,
	'PWRPLANT',
	TO_DATE('6-03-2015 10:32:15','DD-MM-YYYY HH24:MI:SS'),
	'Would you like to load new Depreciation information?',
	5,
	'depr_group',
	'tabpage_depr_loader',
	340);
--acqa_overview_answers
INSERT INTO acqa_overview_answers VALUES (
	6,
	15,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	6,
	16,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	32,
	67,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	1,
	1,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	1,
	2,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	4,
	9,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	4,
	10,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	2,
	4,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	2,
	5,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	3,
	7,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	3,
	8,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:32','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	5,
	12,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	5,
	13,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	18,
	39,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	18,
	40,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	19,
	41,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	19,
	42,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	20,
	43,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	20,
	44,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	32,
	68,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	7,
	17,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	7,
	18,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	23,
	49,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	23,
	50,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	24,
	51,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	24,
	52,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	25,
	53,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	25,
	54,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	26,
	55,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	26,
	56,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	27,
	57,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	27,
	58,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	28,
	59,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	28,
	60,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	29,
	61,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	29,
	62,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	30,
	63,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	30,
	64,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	31,
	65,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	31,
	66,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	8,
	19,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	8,
	20,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	9,
	21,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	9,
	22,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	10,
	23,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	10,
	24,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	11,
	26,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	11,
	25,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	12,
	27,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	12,
	28,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	13,
	30,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	13,
	29,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	14,
	31,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	14,
	32,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	15,
	33,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	15,
	34,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	16,
	35,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	16,
	36,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	17,
	37,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	17,
	38,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	21,
	45,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	21,
	46,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	22,
	47,
	'Yes',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:47:50','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	22,
	48,
	'No',
	'PWRPLANT',
	TO_DATE('9-01-2015 12:48:16','DD-MM-YYYY HH24:MI:SS'),
	0);
INSERT INTO acqa_overview_answers VALUES (
	33,
	69,
	'Yes',
	'PWRPLANT',
	TO_DATE('6-03-2015 10:46:34','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	33,
	70,
	'No',
	'PWRPLANT',
	TO_DATE('6-03-2015 10:46:36','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	34,
	71,
	'Yes',
	'PWRPLANT',
	TO_DATE('6-03-2015 10:47:01','DD-MM-YYYY HH24:MI:SS'),
	1);
INSERT INTO acqa_overview_answers VALUES (
	34,
	72,
	'No',
	'PWRPLANT',
	TO_DATE('6-03-2015 10:47:04','DD-MM-YYYY HH24:MI:SS'),
	1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2510, 0, 2015, 1, 0, 0, 43663, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043663_sys_acqa_03_std_vals_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
