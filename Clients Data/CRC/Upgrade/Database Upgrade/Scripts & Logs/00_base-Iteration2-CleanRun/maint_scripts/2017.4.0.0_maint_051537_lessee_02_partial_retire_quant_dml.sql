/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051537_lessee_02_partial_retire_quant_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/11/2018 Shane "C" Ward    populate asset_quantity on ls_ilr_options
||============================================================================
*/

--Asset Quantity = Sum(quantity) of all assets within each ILR
MERGE INTO ls_ilr_options o
USING (SELECT SUM(Nvl(la.quantity, 0)) asset_quantity, la.ilr_id, m.revision
         FROM ls_asset la, ls_ilr_asset_map m
        WHERE la.ls_asset_id = m.ls_asset_id
          AND la.ilr_id = m.ilr_id
        GROUP BY la.ilr_id, m.revision) a
ON (a.ilr_id = o.ilr_id AND a.revision = o.revision)
WHEN MATCHED THEN
  UPDATE SET o.asset_quantity = a.asset_quantity

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6703, 0, 2017, 4, 0, 0, 51537, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051537_lessee_02_partial_retire_quant_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  