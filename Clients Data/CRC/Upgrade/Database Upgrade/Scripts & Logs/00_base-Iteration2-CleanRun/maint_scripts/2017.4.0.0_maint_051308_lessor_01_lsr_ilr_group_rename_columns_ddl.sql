/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051308_lessor_01_lsr_ilr_group_rename_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By        Reason for Change
|| ----------  ---------- ----------------  ------------------------------------
|| 2017.4.0.0  05/25/2018 Johnny Sisouphanh Rename columns for Deferred Rent Accounts
||============================================================================
*/

alter table lsr_ilr_group rename column ST_DEFERRED_ACCOUNT_ID to DEFERRED_RENT_ACCT_ID;

alter table lsr_ilr_group rename column LT_DEFERRED_ACCOUNT_ID to ACCRUED_RENT_ACCT_ID;

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5902, 0, 2017, 4, 0, 0, 51308, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051308_lessor_01_lsr_ilr_group_rename_columns_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;