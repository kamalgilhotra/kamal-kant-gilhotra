/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011215_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   11/05/2012 Julia Breuer   Point Release
||============================================================================
*/

insert into PWRPLANT.PP_REPORTS (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW) values (509960, 'Assessor Labels', 'Mailing labels for assessors.  Formatted for 1 inch x 2.63 inch labels (codes 5160 and 5260).', 'dw_ptr_labels_assessor', 'PropTax - 9960', 10, 9, 6, 9, 1, 3, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (243, 0, 10, 4, 1, 0, 11215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011215_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
