/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053056_pwrtax_01_depr_alloc_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/25/2019 K Powers	  	  Book depr alloc net gross improvement
||============================================================================
*/

INSERT
INTO ppbase_system_options
  (
    system_option_id,
    long_Description,
    system_only,
    pp_default_Value,
    option_Value,
    is_base_option,
    allow_Company_override
  )
  VALUES
  (
    'Deferred - Book Depr Allocate Method',
    'Defines the method to which the book depreciation allocation uses to allocate book depreciation. Gross method uses the gross book balance, Net method uses the net book balance, NetGross uses the largest of the net or gross balance when the gross balance is positive and the lesser when the gross balance is negative. Under the NetGross method, net is used if the gross book balance is zero.',
    0,
    'Gross',
    NULL,
    1,
    1
  );

insert into ppbase_system_options_module (system_option_id, module) values ('Deferred - Book Depr Allocate Method', 'powertax');
insert into ppbase_system_options_values (system_option_id, option_Value) values ('Deferred - Book Depr Allocate Method', 'Gross');
insert into ppbase_system_options_values (system_option_id, option_Value) values ('Deferred - Book Depr Allocate Method', 'Net');
insert into ppbase_system_options_values (system_option_id, option_Value) values ('Deferred - Book Depr Allocate Method', 'NetGross');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15522, 0, 2018, 2, 0, 0, 53056, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053056_pwrtax_01_depr_alloc_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
