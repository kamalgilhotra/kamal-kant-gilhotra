/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039594_pwrtax_dfit_norm_schema_1.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/11/2014 Anand Rajashekar    Normalization Schema Workspace update
||========================================================================================
*/

--Add a new dynamic filter for normalization schema workspace

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (212, 'Normalization Schema', 'dw', 'normalization_schema.normalization_id', null,
    'dw_tax_normalization_schema_filter', 'normalization_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT',
    TO_DATE('11-SEP-14', 'DD-MON-RR'), 0, 0, 0);

delete from PP_DYNAMIC_FILTER_MAPPING
 where PP_REPORT_FILTER_ID = 84
   and FILTER_ID = 180;

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (84, 212, 'PWRPLANT', TO_DATE('11-SEP-14', 'DD-MON-RR'));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1415, 0, 10, 4, 3, 0, 39594, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039594_pwrtax_dfit_norm_schema_1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
