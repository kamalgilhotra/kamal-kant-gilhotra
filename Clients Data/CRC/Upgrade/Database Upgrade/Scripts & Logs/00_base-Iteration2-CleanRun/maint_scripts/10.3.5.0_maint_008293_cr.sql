/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008293_cr.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   06/08/2012 Joseph King    Point Release
||============================================================================
*/

delete from CR_SYSTEM_CONTROL where UPPER(CONTROL_NAME) = UPPER('Enable Validations - JE - Batch');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (149, 0, 10, 3, 5, 0, 8293, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_008293_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
