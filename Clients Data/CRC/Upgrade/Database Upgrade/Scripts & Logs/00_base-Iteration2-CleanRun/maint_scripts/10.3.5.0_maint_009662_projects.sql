/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009662_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/16/2012 Chris Mardis   Point Release
||============================================================================
*/

create global temporary table TEMP_WORK_ORDER_CONTROL
(
 WORK_ORDER_ID        number(22,0),
 WORK_ORDER_NUMBER    varchar2(35),
 BUS_SEGMENT_ID       number(22,0),
 COMPANY_ID           number(22,0),
 WO_STATUS_ID         number(22,0),
 WORK_ORDER_TYPE_ID   number(22,0),
 MAJOR_LOCATION_ID    number(22,0),
 DEPARTMENT_ID        number(22,0),
 FUNDING_WO_INDICATOR number(22,0)
) on commit preserve rows;

alter table TEMP_WORK_ORDER_CONTROL
   add constraint PK_TEMP_WORK_ORDER_CONTROL
       primary key (WORK_ORDER_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (182, 0, 10, 3, 5, 0, 9662, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009662_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

