 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046105_pwrtax_disable_verify_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 02/17/2017 Rob Burns      Initial script to disable Verifies from the PowerTax menu
 ||============================================================================
 */ 

update ppbase_menu_items 
set enable_yn = 0
where module = 'powertax'
and   menu_identifier = 'reporting_verify'
and   label = 'Verify';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3386, 0, 2017, 1, 0, 0, 46105, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_046105_pwrtax_disable_verify_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
