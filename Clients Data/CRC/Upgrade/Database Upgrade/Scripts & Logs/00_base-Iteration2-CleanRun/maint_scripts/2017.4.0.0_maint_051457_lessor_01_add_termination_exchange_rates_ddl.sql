/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051457_lessor_01_add_termination_exchange_rates_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/15/2018 Jared Watkins  Add the termination exchange rates to the Lessor termination tables
||============================================================================
*/

alter table lsr_ilr_termination_op add termination_exchange_rate number(22,8);
comment on column lsr_ilr_termination_op.termination_exchange_rate is 'The multicurrency exchange rate set at point of termination to convert contract amounts into company amounts';

alter table lsr_ilr_termination_st_df add termination_exchange_rate number(22,8);
comment on column lsr_ilr_termination_st_df.termination_exchange_rate is 'The multicurrency exchange rate set at point of termination to convert contract amounts into company amounts';

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6749, 0, 2017, 4, 0, 0, 51457, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051457_lessor_01_add_termination_exchange_rates_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;