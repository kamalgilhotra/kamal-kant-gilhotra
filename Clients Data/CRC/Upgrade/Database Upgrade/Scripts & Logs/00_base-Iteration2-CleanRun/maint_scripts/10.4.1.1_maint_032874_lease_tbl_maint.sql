/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032874_lease_tbl_maint.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Ryan Oliveria  Patch Release
||============================================================================
*/

update POWERPLANT_TABLES
   set SUBSYSTEM_SCREEN = 'lessee', SUBSYSTEM_DISPLAY = 'lessee', SUBSYSTEM = 'lessee'
 where TABLE_NAME = 'ls_lease_cap_type'
   and SUBSYSTEM = 'lease';

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM)
values
   ('ls_rent_bucket_admin', 's', 'Lease Rent Bucket Admin', null, 'lessee', null, 'lessee',
    'lessee');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('rent_type', 'ls_rent_bucket_admin', null, 'e', null, 'Rent Type', null, 0, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('bucket_number', 'ls_rent_bucket_admin', null, 'e', null, 'Bucket Number', null, 1, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('bucket_name', 'ls_rent_bucket_admin', null, 'e', null, 'Bucket Name', null, 2, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('status_code_id', 'ls_rent_bucket_admin', 'status_code', 'p', null, 'Status Code', null, 3,
    null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('time_stamp', 'ls_rent_bucket_admin', null, 'e', null, 'Time Stamp', null, 4, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('user_id', 'ls_rent_bucket_admin', null, 'e', null, 'User', null, 5, null, 0);

delete from POWERPLANT_COLUMNS
 where TABLE_NAME in (select TABLE_NAME from POWERPLANT_TABLES where SUBSYSTEM = 'lease');

delete from POWERPLANT_TABLES where SUBSYSTEM = 'lease';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (643, 0, 10, 4, 1, 1, 32874, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032874_lease_tbl_maint.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
