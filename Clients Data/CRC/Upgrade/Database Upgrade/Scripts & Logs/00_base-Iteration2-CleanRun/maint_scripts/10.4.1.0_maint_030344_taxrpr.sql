/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030344_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   07/03/2013 Alex P.         Point Release
||============================================================================
*/

update PP_REPORTS_TIME_OPTION
   set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_mnum_single'
 where DESCRIPTION = 'Single Month'
   and PP_REPORT_TIME_OPTION_ID = 1;

update PP_REPORTS_TIME_OPTION
   set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_mnum_span'
 where DESCRIPTION = 'Span'
   and PP_REPORT_TIME_OPTION_ID = 2;

update PP_REPORTS
   set PP_REPORT_TIME_OPTION_ID = 1
 where REPORT_TYPE_ID = 100
   and PP_REPORT_TIME_OPTION_ID = 42;

update PP_REPORTS
   set PP_REPORT_TIME_OPTION_ID = 2
 where REPORT_TYPE_ID = 100
   and PP_REPORT_TIME_OPTION_ID = 43;

delete PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID in (40, 41, 42, 43, 44);

insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1, KEYCOLUMN1)
values
   (40, 'RPR - Single Batch', 'uo_ppbase_report_parms_singlegrid', 'dw_rpr_batch_control',
    'Repairs Batches', 'batch_id');

insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1, KEYCOLUMN1)
values
   (41, 'RPR - Single Batch CPR', 'uo_ppbase_report_parms_singlegrid', 'dw_rpr_batch_control_cpr',
    'CPR Batches', 'batch_id');

insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1, KEYCOLUMN1)
values
   (42, 'RPR - Single Batch Blnkt', 'uo_ppbase_report_parms_singlegrid',
    'dw_rpr_batch_control_blnkt', 'Blanket Batches', 'batch_id');

insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1, KEYCOLUMN1)
values
   (43, 'RPR - Single Batch WMIS', 'uo_ppbase_report_parms_singlegrid', 'dw_rpr_batch_control_wmis',
    'WMIS Batches', 'batch_id');

update PP_REPORTS
   set PP_REPORT_TIME_OPTION_ID = 40, INPUT_WINDOW = null
 where INPUT_WINDOW = 'dw_rpr_batch_control'
   and PP_REPORT_TIME_OPTION_ID = 45;

update PP_REPORTS
   set PP_REPORT_TIME_OPTION_ID = 41, INPUT_WINDOW = null
 where INPUT_WINDOW = 'dw_rpr_batch_control_cpr'
   and PP_REPORT_TIME_OPTION_ID = 45;

update PP_REPORTS
   set PP_REPORT_TIME_OPTION_ID = 42, INPUT_WINDOW = null
 where INPUT_WINDOW = 'dw_rpr_batch_control_blnkt'
   and PP_REPORT_TIME_OPTION_ID = 45;

update PP_REPORTS
   set PP_REPORT_TIME_OPTION_ID = 43, INPUT_WINDOW = null
 where INPUT_WINDOW = 'dw_rpr_batch_control_wmis'
   and PP_REPORT_TIME_OPTION_ID = 45;

delete PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 45;

update PP_REPORT_TYPE set SORT_ORDER = 1 where REPORT_TYPE_ID = 100;

insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER)
values
   (101, 'Tax Repairs - General', 2);

insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER)
values
   (102, 'Tax Repairs - Unit of Property', 3);

insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER)
values
   (103, 'Tax Repairs - Unit of Property WT', 4);

insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER)
values
   (104, 'Tax Repairs - Allocation', 5);

update PP_REPORTS
   set REPORT_TYPE_ID = 101
 where REPORT_TYPE_ID = 100
   and REPORT_NUMBER like 'GEN - %';

update PP_REPORTS
   set REPORT_TYPE_ID = 102
 where REPORT_TYPE_ID = 100
   and REPORT_NUMBER like 'UOP - %';

update PP_REPORTS
   set REPORT_TYPE_ID = 103
 where REPORT_TYPE_ID = 100
   and REPORT_NUMBER like 'UOPWT - %';

update PP_REPORTS
   set REPORT_TYPE_ID = 104
 where REPORT_TYPE_ID = 100
   and REPORT_NUMBER like 'ALLOC - %';

update PP_REPORTS_TIME_OPTION
   set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_mnum_single'
 where DESCRIPTION = 'Single Month'
   and PP_REPORT_TIME_OPTION_ID = 1;

update PP_REPORTS_TIME_OPTION
   set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_mnum_span'
 where DESCRIPTION = 'Span'
   and PP_REPORT_TIME_OPTION_ID = 2;

update PP_REPORTS
   set PP_REPORT_TIME_OPTION_ID = 1
 where REPORT_TYPE_ID = 100
   and PP_REPORT_TIME_OPTION_ID = 42;

update PP_REPORTS
   set PP_REPORT_TIME_OPTION_ID = 2
 where REPORT_TYPE_ID = 100
   and PP_REPORT_TIME_OPTION_ID = 43;

update PP_REPORTS_FILTER
   set FILTER_UO_NAME = 'uo_rpr_selecttabs_filter_dynamic'
 where DESCRIPTION like 'Repairs - %';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (429, 0, 10, 4, 1, 0, 30344, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030344_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;