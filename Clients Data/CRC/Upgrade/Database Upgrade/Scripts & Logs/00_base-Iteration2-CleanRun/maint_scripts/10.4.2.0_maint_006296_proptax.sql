/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_006296_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/05/2014 Julia Breuer
||============================================================================
*/

--
-- Remove PT Type Rollup Values from Table Maintenance.
--
delete from PP_TABLE_GROUPS    where TABLE_NAME = 'pt_type_rollup_values';
delete from POWERPLANT_COLUMNS where TABLE_NAME = 'pt_type_rollup_values';
delete from POWERPLANT_TABLES  where TABLE_NAME = 'pt_type_rollup_values';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (826, 0, 10, 4, 2, 0, 6296, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_006296_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;