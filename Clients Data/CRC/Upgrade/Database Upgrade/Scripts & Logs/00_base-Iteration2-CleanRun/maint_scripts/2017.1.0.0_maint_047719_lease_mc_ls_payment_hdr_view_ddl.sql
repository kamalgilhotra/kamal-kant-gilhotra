/*
||============================================================================================
|| Application: PowerPlant
|| File Name:   maint_047719_lease_mc_ls_payment_hdr_view_ddl.sql
||============================================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- -----------------------------------------------------
|| 2017.1.0.0  05/02/2017 Anand R        PP-47719 update view to display currency conversion
||============================================================================================
*/

CREATE OR REPLACE VIEW V_LS_PAYMENT_HDR_FX AS
WITH
cur AS (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id, contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id, company_cur.currency_display_symbol, company_cur.iso_code
    FROM currency company_cur
    )
SELECT lph.payment_id,
       lph.lease_id,
       lph.vendor_id,
       lph.company_id,
       lph.amount original_amount,
       Round(lph.amount * decode(ls_cur_type, 2, rate, 1) ,2) amount,
       lph.description, lph.gl_posting_mo_yr,
       lph.payment_status_id,
       lph.ap_status_id,
       lph.ls_asset_id,
       lph.ilr_id,
       lease.contract_currency_id,
       cs.currency_id company_currency_id,
       cur.ls_cur_type AS ls_cur_type,
       cr.exchange_date,
       decode(ls_cur_type, 2, rate, 1) rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_payment_hdr lph
INNER JOIN ls_lease lease
      ON lph.lease_id = lease.lease_id
INNER JOIN currency_schema cs
      ON lph.company_id = cs.company_id
INNER JOIN cur
      ON (
          (cur.ls_cur_type = 1 AND cur.currency_id = lease.contract_currency_id)
          OR
          (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
         )
INNER JOIN ls_lease_calculated_date_rates cr
      ON (cr.company_id = lph.company_id
        AND cr.contract_currency_id =  lease.contract_currency_id
        AND cr.company_currency_id =  cs.currency_id
        AND cr.accounting_month = lph.gl_posting_mo_yr ) 
where cr.exchange_rate_type_id = 1
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3471, 0, 2017, 1, 0, 0, 47719, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047719_lease_mc_ls_payment_hdr_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;