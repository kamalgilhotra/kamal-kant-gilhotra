/*
||=============================================================================================
|| Application: PowerPlan
|| Module: 	Security
|| File Name:   maint_044914_websys_security_permissions_mobile_approval_dml.sql
||=============================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------  -------------------------------------------------
|| 2015.2     09/15/2015 Khamchanh Sisouphanh Updating data for web security - Mobile Approvals
||=============================================================================================
*/

--Mobile Approvals Permissions
DELETE FROM PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL
 WHERE MODULE_ID = 2;

INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Projects.WorkOrdersApprove',2,1,null,'Work Orders','Approve');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Projects.WorkOrdersReject',2,1,null,'Work Orders','Reject');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Projects.WorkOrdersDetails',2,1,null,'Work Orders','Details');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Projects.FundingProjectsApprove',2,1,null,'Funding Project','Approve');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Projects.FundingProjectsReject',2,1,null,'Funding Project','Reject');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Projects.FundingProjectsDetails',2,1,null,'Funding Project','Details');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.PropertyTax.PaymentsApprove',2,2,null,'Payments','Approve');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.PropertyTax.PaymentsReject',2,2,null,'Payments','Reject');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.PropertyTax.PaymentsDetails',2,2,null,'Payments','Details');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.PropertyTax.JournalEntriesApprove',2,2,null,'Journal Entries','Approve');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.PropertyTax.JournalEntriesReject',2,2,null,'Journal Entries','Reject');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.PropertyTax.JournalEntriesDetails',2,2,null,'Journal Entries','Details');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.ILRApprove',2,3,null,'Individual Lease Records','Approve');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.ILRReject',2,3,null,'Individual Lease Records','Reject');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.ILRDetails',2,3,null,'Individual Lease Records','Details');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.MLAApprove',2,3,null,'Master Lease Agreements','Approve');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.MLAReject',2,3,null,'Master Lease Agreements','Reject');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.MLADetails',2,3,null,'Master Lease Agreements','Details');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.PaymentsApprove',2,3,null,'Payments','Approve');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.PaymentsReject',2,3,null,'Payments','Reject');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.Lease.PaymentsDetails',2,3,null,'Payments','Details');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.UpdatesAlerts.Attachments',2, null,'Updates/Alerts','Attachments','All');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.UpdatesAlerts.ActionsInServiceDate',2, null,'Updates/Alerts','Actions','In-Service Date');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.UpdatesAlerts.ActionsEstInServiceDate',2, null,'Updates/Alerts','Actions','Estimate In-Service Date');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.UpdatesAlerts.ActionsCompletionDate',2, null,'Updates/Alerts','Actions','Completion Date');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.UpdatesAlerts.ActionsEstCompletion',2, null,'Updates/Alerts','Actions','Estimated Completion Date');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('MobileApprovals.UpdatesAlerts.ActionsChangeInServiceDate',2, null,'Updates/Alerts','Actions','Change In-Service Date');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2875, 0, 2015, 2, 0, 0, 044914, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044914_websys_security_permissions_mobile_approval_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;