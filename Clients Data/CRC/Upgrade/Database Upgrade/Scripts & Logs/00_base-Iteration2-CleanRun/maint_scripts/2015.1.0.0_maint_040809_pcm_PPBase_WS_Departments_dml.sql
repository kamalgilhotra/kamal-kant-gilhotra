 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_040809_pcm_PPBase_WS_Departments_dml.sql
|| Description: Create Relationship record for Departments
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/15/2015 Louis Alston   Adding Departments page to PCM Module
||============================================================================
*/
SET ECHO ON
DECLARE 
  alreadyThere NUMBER;
BEGIN

  SELECT COUNT(*) 
  INTO alreadyThere
    FROM PPBASE_WORKSPACE
  WHERE MODULE = 'pcm' 
		AND WORKSPACE_IDENTIFIER = 'wo_maint_departments';


  IF( alreadyThere = 0 ) THEN
	INSERT INTO PPBASE_WORKSPACE 
	   ( MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID )
	   VALUES ( 'pcm'
				,'wo_maint_departments'
				,'Departments (WO)'
				,'uo_pcm_maint_wksp_departments'
				,'Departments (WO)'
				,1
				);
				
  ELSE --alreadyThere should = 1 since Module and WORKSPACE_IDENTIFIER are the primary key.
    UPDATE PPBASE_WORKSPACE
		SET WORKSPACE_UO_NAME = 'uo_pcm_maint_wksp_departments'
			, LABEL = 'Departments (WO)'
			, MINIHELP = 'Departments (WO)'
			, OBJECT_TYPE_ID = 1
	WHERE MODULE = 'pcm' AND WORKSPACE_IDENTIFIER = 'wo_maint_departments';
  END IF;
  
  
  SELECT COUNT(*) 
  INTO alreadyThere
    FROM PPBASE_WORKSPACE
  WHERE MODULE = 'pcm' 
		AND WORKSPACE_IDENTIFIER = 'fp_maint_departments';


  IF( alreadyThere = 0 ) THEN
	INSERT INTO PPBASE_WORKSPACE 
	   ( MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID )
	   VALUES ( 'pcm'
				,'fp_maint_departments'
				,'Departments (FP)'
				,'uo_pcm_maint_wksp_departments'
				,'Departments (FP)'
				,1
				);
				
  ELSE --alreadyThere should = 1 since Module and WORKSPACE_IDENTIFIER are the primary key.
    UPDATE PPBASE_WORKSPACE
		SET WORKSPACE_UO_NAME = 'uo_pcm_maint_wksp_departments'
			, LABEL = 'Departments (FP)'
			, MINIHELP = 'Departments (FP)'
			, OBJECT_TYPE_ID = 1
	WHERE MODULE = 'pcm' AND WORKSPACE_IDENTIFIER = 'fp_maint_departments';
  END IF;
  
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2204, 0, 2015, 1, 0, 0, 040809, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040809_pcm_PPBase_WS_Departments_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;