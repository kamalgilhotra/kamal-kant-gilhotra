/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029892_sys_job_request.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/29/2013 Joseph King    Point Release
||============================================================================
*/

create table PP_JOB_REQUEST
(
 PROCESS_IDENTIFIER  number(22,0) not null,
 REQUESTING_USER     varchar2(18) not null,
 PROCESS             varchar2(50) not null,
 PROCESS_DESCR       varchar2(255) not null,
 SYNCHRONOUS         number(22,0) not null,
 ARGUMENTS           clob not null,
 STATUS              varchar2(1) not null,
 PROCESS_ID          number(22,0),
 OCCURRENCE_ID       number(22,0),
 REQUEST_TIME        date,
 START_TIME          date,
 END_TIME            date,
 RETURN_CODE         varchar2(4000),
 USER_ID             varchar2(18),
 TIME_STAMP          date,
 NOTIFY_ON_COMPLETE  number(22,0)
);

alter table PP_JOB_REQUEST
   add constraint PP_JR_PK
       primary key (PROCESS_IDENTIFIER)
       using index tablespace PWRPLANT_IDX;

create sequence PP_JOB_REQUEST_SEQ start with 1 increment by 1 nocache;

comment on table PP_JOB_REQUEST is '(O) [10] The PP Job Request table is the queuing and messaging table used by the system to make remote requests.  The table is monitored by a Windows based service to launch other processes.';
comment on column PP_JOB_REQUEST.PROCESS_IDENTIFIER is 'Dummy key assigned to each request.';
comment on column PP_JOB_REQUEST.REQUESTING_USER is 'The User ID of the user that requested the process.';
comment on column PP_JOB_REQUEST.PROCESS is 'The process to be launched containing the file name and any necessary command line arguments.  The value will not contain a file path.  Note that the process typically includes an argument to pass in both the Requesting User and Process Identifier.';
comment on column PP_JOB_REQUEST.PROCESS_DESCR is 'A freeform description of the process being launched that typically contains the process name and any key attributes such as ''Depreciation Calculation for PowerPlan for October 2013''.  The description is included in any notifications sent by the process ';
comment on column PP_JOB_REQUEST.SYNCHRONOUS is '1 or 0 indicating if the request is synchronous or asynchronous respectively.  For a synchronous request, the requesting user''s session will wait until a return is available.';
comment on column PP_JOB_REQUEST.ARGUMENTS is 'JSON that contains the argument to the process.';
comment on column PP_JOB_REQUEST.STATUS is 'Status of the request.  Valid values are ''N'' (new), ''I'' (in process), ''D'' (done), ''E'' (error), and ''L'' (new local request).';
comment on column PP_JOB_REQUEST.PROCESS_ID is 'Link to a PP Process once the process has started.';
comment on column PP_JOB_REQUEST.OCCURRENCE_ID is 'Link to a Process Occurrence once the process has started.';
comment on column PP_JOB_REQUEST.REQUEST_TIME is 'The datetime the job was requested.';
comment on column PP_JOB_REQUEST.START_TIME is 'The datetime the job was started.';
comment on column PP_JOB_REQUEST.END_TIME is 'The datetime the job was completed.';
comment on column PP_JOB_REQUEST.RETURN_CODE is 'The return code or error message from the process.';
comment on column PP_JOB_REQUEST.USER_ID    is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_JOB_REQUEST.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_JOB_REQUEST.NOTIFY_ON_COMPLETE is '1 or 0 indicating if the requesting user is sent an email notification when the job completes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (521, 0, 10, 4, 1, 0, 29892, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029892_sys_job_request.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
