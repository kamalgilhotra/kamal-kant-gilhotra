/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052870_lessee_01_idc_inc_accrued_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.1 12/14/2018 Shane "C" Ward  Update to IDC Incentive view to handle split cap type conversions
||============================================================================
*/

CREATE OR replace VIEW v_ls_ilr_idc_inc_accrued_fx_vw
AS
WITH ilr_info AS
	 (SELECT stg.ilr_id,
			 stg.revision,
			 appr.revision appr_revision,
			 decode(sign(stg.revision), -1, npv_start_date, curr.remeasurement_date) remeasurement_date,
			 i.est_in_svc_date,
			 stg.is_om curr_is_om,
			 stg.set_of_books_id,
			 npv_start_date
		FROM ls_ilr         i,
			 ls_ilr_options curr,
			 ls_ilr_options appr,
			 ls_ilr_stg     stg
	   WHERE i.ilr_id = curr.ilr_id
		 AND curr.ilr_id = stg.ilr_id
		 AND curr.revision = stg.revision
		 AND i.current_revision = appr.revision
		 AND i.ilr_id = appr.ilr_id),
	num_months AS
	 (SELECT ilr.ilr_id,
			 ilr.set_of_books_id,
			 COUNT(MONTH) num_months
		FROM ls_ilr_schedule sched,
			 ilr_info        ilr
	   WHERE sched.ilr_id = ilr.ilr_id
		 AND sched.revision = ilr.appr_revision
		 AND sched.set_of_books_id = ilr.set_of_books_id
	   GROUP BY ilr.ilr_id,
				ilr.set_of_books_id)
	SELECT i.ilr_id ilr_id,
		   i.revision revision,
		   i.set_of_books_id set_of_books_id,
		   i.npv_start_date,
		   n.num_months,
       Sum(Nvl(initial_direct_cost,0)) accrued_idc_from_cap,
		   round((SUM(initial_direct_cost) / n.num_months) * months_between(remeasurement_date, MIN(s.month)), 2) accrued_idc_from_om, -- Rounding plug at end of calc will take care of rounding issues
		   SUM(incentive_amount - nvl(decode(s.month, i.est_in_svc_date, 0, incentive_math_amount), 0)) accrued_incentive,
		   months_between(remeasurement_date, MIN(s.month)) months_already_accrued,
		   i.remeasurement_date,
		   i.curr_is_om curr_is_om,
		   s.is_om approved_is_om,
		   nvl(decode(s.is_om, 1, decode(i.curr_is_om, 0, 1), 0),0) switch_to_cap --whether or not we are converting cap types on the schedule being calculated
	  FROM ls_ilr_schedule s,
		   ilr_info        i,
		   num_months      n
	 WHERE s.ilr_id = i.ilr_id
	   AND s.revision = i.appr_revision
	   AND s.set_of_books_id = i.set_of_books_id
	   AND n.ilr_id = i.ilr_id
	   AND n.set_of_books_id = i.set_of_books_id
	   AND s.month < i.npv_start_date
	   AND i.npv_start_date is not null -- Make sure to filter out Sets of Books in ILR_STG that have no NPV Start Date Which means no IDC Incentive Accrual
	 GROUP BY i.ilr_id,
			  i.revision,
			  i.set_of_books_id,
			  i.remeasurement_date,
			  i.curr_is_om,
			  s.is_om,
			  npv_start_date,
			  n.num_months
	UNION
	SELECT DISTINCT ilr_id,
					revision,
					set_of_books_id,
					npv_start_date,
					0               num_months,
					0               accrued_idc_from_cap,
					0               accrued_idc_from_om,
					0               accrued_incentive,
					NULL            months_already_accrued,
					NULL            remeasurement_date,
					0               curr_is_om,
					0               approved_is_om,
					0               switch_to_cap
	  FROM ls_ilr_stg
	 WHERE npv_start_date IS NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13065, 0, 2018, 1, 0, 1, 52870, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_052870_lessee_01_idc_inc_accrued_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;