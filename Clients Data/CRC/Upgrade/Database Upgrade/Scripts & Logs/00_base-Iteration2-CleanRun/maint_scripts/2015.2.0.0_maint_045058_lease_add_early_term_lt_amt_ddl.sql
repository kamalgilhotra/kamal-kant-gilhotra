/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045058_lease_add_early_term_lt_amt_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 10/07/2015 Will Davis 	  Add a long term ending balance field for
||										  proper balance sheet retirement reporting
||============================================================================
*/

alter table ls_asset add early_term_end_lt_obligation number(22,2) default 0 null;

comment on column ls_asset.early_term_end_lt_obligation is 'A value populated upon early retirement of the asset that shows the ending long term obligation for the retirement month.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2896, 0, 2015, 2, 0, 0, 45058, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045058_lease_add_early_term_lt_amt_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
