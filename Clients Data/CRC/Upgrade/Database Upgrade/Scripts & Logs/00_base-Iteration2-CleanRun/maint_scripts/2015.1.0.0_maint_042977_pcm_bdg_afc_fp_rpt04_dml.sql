--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042977_pcm_bdg_afc_fp_rpt04_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     02/24/2015 Andrew Scott     Budget - AFUDC FP 04 Report Conversion.
--||============================================================================
--*/

delete from pp_reports where report_id = 300002;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID)
values
   (300002, 'Budg vs. Act AFUDC Comp/BS/ML',
    'For a selected budget version, the report will display budget vs. actual debt and equity AFUDC by company, business segment, major location, and funding project.',
    'dw_pcm_rpt_bdgt_afudc_fp_04', 'ALREADY HAS REPORT TIME', 'PCM - Budget - AFUDC FP 04', 5, 35, 2,
    88, 1, 3);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2322, 0, 2015, 1, 0, 0, 42977, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042977_pcm_bdg_afc_fp_rpt04_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;