/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038107_reg_case_drillback.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/20/2014 Sarah Byers
||========================================================================================
*/

-- Add recon_adj_amount and recon_adj_comment to the view
create or replace view REG_HISTORY_LEDGER_ID_SV
   (REG_COMPANY,HISTORIC_LEDGER,REG_ACCOUNT,REG_ACCOUNT_TYPE,SUB_ACCOUNT_TYPE,REG_SOURCE,GL_MONTH,ACT_AMOUNT,ANNUALIZED_AMT,
    ADJ_AMOUNT,ADJ_MONTH,RECON_ADJ_AMOUNT,RECON_ADJ_COMMENT,REG_COMPANY_ID,REG_ACCT_ID,HISTORIC_LEDGER_ID,REG_SOURCE_ID)
as
select REG_COMPANY,
       HISTORIC_LEDGER,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       RECON_ADJ_AMOUNT,
       RECON_ADJ_COMMENT,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       HISTORIC_LEDGER_ID,
       REG_SOURCE_ID
  from (select C.DESCRIPTION         REG_COMPANY,
               M.DESCRIPTION         REG_ACCOUNT,
               T.DESCRIPTION         REG_ACCOUNT_TYPE,
               S.DESCRIPTION         SUB_ACCOUNT_TYPE,
               R.DESCRIPTION         REG_SOURCE,
               V.LONG_DESCRIPTION    HISTORIC_LEDGER,
               L.GL_MONTH            GL_MONTH,
               L.ACT_AMOUNT          ACT_AMOUNT,
               L.ANNUALIZED_AMT      ANNUALIZED_AMT,
               L.ADJ_AMOUNT          ADJ_AMOUNT,
               L.ADJ_MONTH           ADJ_MONTH,
               L.RECON_ADJ_AMOUNT    RECON_ADJ_AMOUNT,
               L.RECON_ADJ_COMMENT   RECON_ADJ_COMMENT,
               L.REG_COMPANY_ID      REG_COMPANY_ID,
               L.REG_ACCT_ID         REG_ACCT_ID,
               L.HISTORIC_VERSION_ID HISTORIC_LEDGER_ID,
               L.REG_SOURCE_ID       REG_SOURCE_ID
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER L,
               REG_ACCT_TYPE T,
               REG_SUB_ACCT_TYPE S,
               REG_SOURCE R
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);

-- Replace the columns
delete from cr_dd_sources_criteria_fields
 where id = (
   select id from cr_dd_sources_criteria
    where source_id > 1001
      and lower(table_name) = 'reg_history_ledger_id_sv');

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'ACT_AMOUNT', 8, 1, 1, null, 'Act Amount', 409, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'ADJ_AMOUNT', 10,  1, 1, null, 'Adj Amount', 313, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'ADJ_MONTH', 11, 0, 1, null, 'Adj Month', 281, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'ANNUALIZED_AMT', 9, 1, 1, null, 'Annualized Amt', 418, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'GL_MONTH', 7, 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'HISTORIC_LEDGER', 2, 0, 1, null, 'Historic Ledger', 422, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'HISTORIC_LEDGER_ID', 16, 0, 1, null, 'Historic Ledger ID', 482, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'RECON_ADJ_AMOUNT', 12, 1, 1, null, 'Recon Adj Amount', 500, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'RECON_ADJ_COMMENT', 13, 0, 1, null, 'Recon Adj Comment', 605, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'REG_ACCOUNT', 3, 0, 1, null, 'Reg Account', 1830, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'REG_ACCOUNT_TYPE', 4, 0, 1, null, 'Reg Account Type', 505, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'REG_ACCT_ID', 15, 0, 1, null, 'Reg Acct ID', 331, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'REG_COMPANY', 1, 0, 1, null, 'Reg Company', 687, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'REG_COMPANY_ID', 14, 0, 1, null, 'Reg Company ID', 445, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'REG_SOURCE', 6, 0, 1, null, 'Reg Source', 340, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'REG_SOURCE_ID', 17, 0, 1, null, 'Reg Source ID', 395, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_history_ledger_id_sv'),
'SUB_ACCOUNT_TYPE', 5, 0, 1, null, 'Sub Account Type', 587, 0, 'Any', null, null, 0, null, null, 0) ;

-- Create forecast ledger view
create or replace view REG_FORECAST_LEDGER_ID_SV
   (REG_COMPANY,FORECAST_LEDGER,REG_ACCOUNT,REG_ACCOUNT_TYPE,SUB_ACCOUNT_TYPE,REG_SOURCE,GL_MONTH,FCST_AMOUNT,ANNUALIZED_AMT,
    ADJ_AMOUNT,ADJ_MONTH,REG_COMPANY_ID,REG_ACCT_ID,FORECAST_LEDGER_ID,REG_SOURCE_ID)
as
select REG_COMPANY,
       FORECAST_LEDGER,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       GL_MONTH,
       FCST_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       FORECAST_LEDGER_ID,
       REG_SOURCE_ID
  from (select C.DESCRIPTION         REG_COMPANY,
               M.DESCRIPTION         REG_ACCOUNT,
               T.DESCRIPTION         REG_ACCOUNT_TYPE,
               S.DESCRIPTION         SUB_ACCOUNT_TYPE,
               R.DESCRIPTION         REG_SOURCE,
               V.DESCRIPTION         FORECAST_LEDGER,
               L.GL_MONTH            GL_MONTH,
               L.FCST_AMOUNT         FCST_AMOUNT,
               L.ANNUALIZED_AMT      ANNUALIZED_AMT,
               L.ADJ_AMOUNT          ADJ_AMOUNT,
               L.ADJ_MONTH           ADJ_MONTH,
               L.REG_COMPANY_ID      REG_COMPANY_ID,
               L.REG_ACCT_ID         REG_ACCT_ID,
               L.FORECAST_VERSION_ID FORECAST_LEDGER_ID,
               L.REG_SOURCE_ID       REG_SOURCE_ID
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_FORECAST_VERSION V,
               REG_FORECAST_LEDGER L,
               REG_ACCT_TYPE T,
               REG_SUB_ACCT_TYPE S,
               REG_SOURCE R
         where L.FORECAST_VERSION_ID = V.FORECAST_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);

-- Add the new any query
INSERT INTO "CR_DD_SOURCES_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "FEEDER_FIELD")
VALUES ((select nvl(max(id),0) from cr_dd_sources_criteria) + 1, 1002, 'none', 'reg_forecast_ledger_id_sv', 'Forecast Ledger with ID Values', 'none') ;

-- Add the fields
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'ADJ_AMOUNT', 10, 1, 1, null, 'Adj Amount', 313, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'ADJ_MONTH', 11, 0, 1, null, 'Adj Month', 281, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'ANNUALIZED_AMT', 9, 1, 1, null, 'Annualized Amt', 418, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'FCST_AMOUNT', 8, 1, 1, null, 'Fcst Amount', 345, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'FORECAST_LEDGER', 2, 0, 1, null, 'Forecast Ledger', 441, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'FORECAST_LEDGER_ID', 14, 0, 1, null, 'Forecast Ledger ID', 509, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'GL_MONTH', 7, 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'REG_ACCOUNT', 3, 0, 1, null, 'Reg Account', 1154, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'REG_ACCOUNT_TYPE', 4, 0, 1, null, 'Reg Account Type', 509, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'REG_ACCT_ID', 13, 0, 1, null, 'Reg Acct ID', 331, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'REG_COMPANY', 1, 0, 1, null, 'Reg Company', 687, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'REG_COMPANY_ID', 12, 0, 1, null, 'Reg Company ID', 445, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'REG_SOURCE', 6, 0, 1, null, 'Reg Source', 340, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'REG_SOURCE_ID', 15, 0, 1, null, 'Reg Source ID', 395, 0, 'Any', null, null, 0, null, null, 0) ;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
'SUB_ACCOUNT_TYPE', 5, 0, 1, null, 'Sub Account Type', 733, 0, 'Any', null, null, 0, null, null, 0) ;

insert into reg_query_drillback (
   reg_source_id, hist_fcst_flag, reg_query_id, description, default_flag)
values (
   15, 2,(select id from cr_dd_sources_criteria where lower(table_name) = 'reg_forecast_ledger_id_sv'),
   'Forecast Ledger Query from Recon', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1146, 0, 10, 4, 2, 6, 38107, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038107_reg_case_drillback.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;