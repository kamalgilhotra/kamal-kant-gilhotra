/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049499_taxrpr_02_mixed_rl_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 10/19/2017 Eric Berger    DML for mixed repair location method.
||============================================================================
*/

INSERT INTO WO_REPAIR_LOCATION_METHOD
            (repair_location_method_id,
             description)
SELECT 4,
       'Mixed Repair Location' description
FROM   DUAL
WHERE  NOT EXISTS
           ( SELECT 1
             FROM   WO_REPAIR_LOCATION_METHOD
             WHERE  description = 'Mixed Repair Location' );

			 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13734, 0, 2018, 2, 0, 0, 49499, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049499_taxrpr_02_mixed_rl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;