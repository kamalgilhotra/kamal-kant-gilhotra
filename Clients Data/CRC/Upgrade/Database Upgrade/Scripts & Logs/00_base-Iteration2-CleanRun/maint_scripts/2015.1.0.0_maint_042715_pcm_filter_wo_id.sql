/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042715_pcm_filter_wo_id.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/05/2015 Alex P.          Remove ID from Excel fields and use regular ID filters instead
||============================================================================
*/

--Remove dynamic filter for WO ID from Excel and FP ID from Excel
delete from pp_dynamic_filter_mapping where pp_report_filter_id in (86,87) and filter_id in (252, 253);

-- Create a Funding Project ID filter for Funding Projects - same as Work Order ID filter for Work Orders.
insert into pp_dynamic_filter( filter_id, label, input_type, sqls_column_expression)
values( 261, 'Funding Project ID', 'operation', 'work_order_control.work_order_id' );

-- Add WO ID, FP ID filters
insert into pp_dynamic_filter_mapping( pp_report_filter_id, filter_id) values(86, 261);
insert into pp_dynamic_filter_mapping( pp_report_filter_id, filter_id) values(87, 25);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2255, 0, 2015, 1, 0, 0, 042715, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042715_pcm_filter_wo_id.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;