/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008745_tax_exp.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/28/2011 Elhadj Bah     Point Release
||============================================================================
*/

create table REPAIR_LOC_ROLLUP
(
 REPAIR_LOC_ROLLUP_ID number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(35),
 LONG_DESCRIPTION     varchar2(254),
 WORK_ORDER_ID        number(22,0)) ;

alter table REPAIR_LOC_ROLLUP
   add constraint PK_REPAIR_LOC_ROLLUP
       primary key (REPAIR_LOC_ROLLUP_ID)
       using index tablespace PWRPLANT_IDX;

create table REPAIR_LOC_ROLLUP_MAPPING
(
 REPAIR_LOCATION_ID   number(22,0) not null,
 REPAIR_LOC_ROLLUP_ID number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

alter table REPAIR_LOC_ROLLUP_MAPPING
   add constraint PK_REPAIR_LOC_ROLLUP_MAPPING
       primary key (REPAIR_LOCATION_ID, REPAIR_LOC_ROLLUP_ID)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_LOC_ROLLUP_MAPPING
   add constraint FK_RLRM_REPAIR_LOCATION_ID
       foreign key (REPAIR_LOCATION_ID)
       references REPAIR_LOCATION;

alter table REPAIR_LOC_ROLLUP_MAPPING
   add constraint FK_RLRM_REPAIR_LOC_ROLLUP_ID
       foreign key (REPAIR_LOC_ROLLUP_ID)
       references REPAIR_LOC_ROLLUP;

create table REPAIR_LOC_ROLLUP_FUNC_CLASS
(
 REPAIR_LOC_ROLLUP_ID number(22,0) not null,
 FUNC_CLASS_ID        number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

alter table REPAIR_LOC_ROLLUP_FUNC_CLASS
   add constraint PK_REPAIR_ROLLUP_FUNC_CLASS
       primary key (REPAIR_LOC_ROLLUP_ID, FUNC_CLASS_ID)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_LOC_ROLLUP_FUNC_CLASS
   add constraint FK_RLRFC_REPAIR_LOC_ROLLUP_ID
       foreign key (REPAIR_LOC_ROLLUP_ID)
       references REPAIR_LOC_ROLLUP;

alter table REPAIR_LOC_ROLLUP_FUNC_CLASS
   add constraint FK_RLRFC_FUNC_CLASS
       foreign key (FUNC_CLASS_ID)
       references FUNC_CLASS;

-- ** EKB: Adding columns needed for the window to work properly
alter table WO_TAX_EXPENSE_TEST
   add (ADD_RETIRE_TOLERANCE_PCT  number(22,2),
        TAX_STATUS_OVER_TOLERANCE number(22,0),
        BLANKET_PROCESSING_OPTION number(22,0),
        BLANKET_METHOD            varchar2(35),
        EXPENSE_PERCENT           number(22,2));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (59, 0, 10, 3, 3, 0, 8745, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008745_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
