/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031674_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/27/2013 Andrew Scott   Point release
||============================================================================
*/

delete from PP_TABLE_GROUPS where LOWER(TABLE_NAME) = 'prop_tax_district';

delete from POWERPLANT_COLUMNS where LOWER(TABLE_NAME) = 'prop_tax_district';

delete from POWERPLANT_TABLES where LOWER(TABLE_NAME) = 'prop_tax_district';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (513, 0, 10, 4, 1, 0, 31674, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031674_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
