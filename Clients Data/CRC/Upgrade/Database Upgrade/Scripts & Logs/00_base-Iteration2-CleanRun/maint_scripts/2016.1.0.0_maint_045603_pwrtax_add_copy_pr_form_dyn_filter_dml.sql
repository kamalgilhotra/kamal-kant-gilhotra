/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045603_pwrtax_add_copy_pr_form_dyn_filter_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 06/15/2016 Charlie Shilling Add option to copy PR Forms with the case in case management.
||============================================================================
*/
INSERT INTO pp_dynamic_filter (filter_id, label, input_type, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only)
VALUES (285, 'Plant Reconciliation Forms', 'dw', 'dddw_yes_no', 'yes_no_id', 'description', 'N', 0, 1);

INSERT INTO pp_dynamic_filter_mapping (pp_report_filter_id, filter_id)
VALUES(65, 285);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3220, 0, 2016, 1, 0, 0, 045603, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045603_pwrtax_add_copy_pr_form_dyn_filter_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;