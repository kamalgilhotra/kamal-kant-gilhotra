/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040757_reg_not_mapped_acct.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/27/2014 Shane Ward
||============================================================================
*/

insert into REG_ACCT_MASTER
   (REG_ACCT_ID, DESCRIPTION, LONG_DESCRIPTION, REG_SOURCE_ID, REG_ACCT_TYPE_DEFAULT,
    SUB_ACCT_TYPE_ID, REG_ANNUALIZATION_ID, ACCT_GOOD_FOR, REG_FAMILY_ID, REG_MEMBER_ID,
    REG_COMPONENT_ID, USER_ID, TIME_STAMP, FERC_ACCT)
values
   (0, 'Not Mapped', 'Not Mapped', 1, 11, 1, 5, 4, 2, 0, 0, null, null, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1581, 0, 10, 4, 3, 0, 40757, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040757_reg_not_mapped_acct.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
