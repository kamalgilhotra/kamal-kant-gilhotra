/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039298_reg_ifa_reports.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/21/2014 Shane Ward       IFA Reports
||============================================================================
*/

--Updated view so I can join
CREATE OR REPLACE VIEW reg_forecast_ledger_ifa_view (
reg_company,
forecast_ledger,
reg_account,
reg_account_type,
REG_ACCT_TYPE_ID,
SUB_ACCT_TYPE_ID,
sub_account_type,
reg_source,
ifa_description,
ifa_type,
INCREMENTAL_ADJ_ID,
INCREMENTAL_ADJ_TYPE_ID,
LABEL,
ITEM,
gl_month,
fcst_amount,
annualized_amt,
adj_amount,
adj_month,
ifa_amount,
annualzied_ifa_amount,
reg_company_id,
reg_acct_id,
forecast_ledger_id,
reg_source_id,
recon_adj_amount,
recon_adj_comment,
total,
annualized_total
) AS
(
select REG_COMPANY,
       FORECAST_LEDGER,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       0 REG_ACCOUNT_TYPE_ID,
       0 SUB_ACCOUNT_TYPE_ID,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       'Ledger' as IFA_DESCRIPTION,
       ' ' as IFA_TYPE,
       -1 as INCREMENTAL_ADJ_ID,
       -1 as INCREMENTAL_ADJ_TYPE_ID,
       '' as LABEL,
       '' as ITEM,
       GL_MONTH,
       NVL(FCST_AMOUNT, 0),
       NVL(ANNUALIZED_AMT, 0),
       NVL(ADJ_AMOUNT, 0),
       ADJ_MONTH,
       0.00 as IFA_AMOUNT,
       0.00 as ANNUALIZED_IFA_AMOUNT,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       FORECAST_LEDGER_ID,
       REG_SOURCE_ID,
       NVL(RECON_ADJ_AMOUNT, 0),
       RECON_ADJ_COMMENT,
       NVL(FCST_AMOUNT, 0) + NVL(ADJ_AMOUNT, 0) + NVL(RECON_ADJ_AMOUNT, 0) as TOTAL,
       NVL(ANNUALIZED_AMT, 0) as ANNUALZIED_TOTAL
  from REG_FORECAST_LEDGER_ID_SV
union
select RC.DESCRIPTION REG_COMPANY,
       RFV.DESCRIPTION as FORECAST_LEDGER,
       RAM.DESCRIPTION as REG_ACCOUNT,
       RAT.DESCRIPTION as REG_ACCOUNT_TYPE,
       RAT.REG_ACCT_TYPE_ID AS REG_ACCOUNT_TYPE_ID,
       RSAT.SUB_ACCT_TYPE_ID AS SUB_ACCOUNT_TYPE_ID,
       RSAT.DESCRIPTION as SUB_ACCOUNT_TYPE,
       RS.DESCRIPTION as REG_SOURCE,
       RIA.DESCRIPTION as IFA_DESCRIPTION,
       RIT.DESCRIPTION as IFA_TYPE,
       RIA.INCREMENTAL_ADJ_ID INCREMENTAL_ADJ_ID,
       RIT.INCREMENTAL_ADJ_TYPE_ID INCREMENTAL_ADJ_TYPE_ID,
       IFAI.DESCRIPTION,
       IFAL.DESCRIPTION,
       RFL.GL_MONTH,
       0.00 as FCST_AMOUNT,
       0.00 as ANNUALZIED_AMT,
       0.00 as ADJ_AMOUNT,
       null as ADJ_MONTH,
       RFL.ADJ_AMOUNT as IFA_AMOUNT,
       RFL.ANNUALIZED_ADJ_AMT as ANNUALIZED_IFA_AMOUNT,
       RFL.REG_COMPANY_ID,
       RFL.REG_ACCT_ID,
       RFL.FORECAST_VERSION_ID as FORECAST_LEDGER_ID,
       RS.REG_SOURCE_ID,
       0 as RECON_ADJ_AMOUNT,
       '' as RECON_ADJ_COMMENT,
       RFL.ADJ_AMOUNT as TOTAL,
       RFL.ANNUALIZED_ADJ_AMT as ANNUALIZED_TOTAL
  from REG_INCREMENTAL_ADJUSTMENT RIA
  join REG_INCREMENTAL_ADJ_VERSION RIAV on RIAV.INCREMENTAL_ADJ_ID = RIA.INCREMENTAL_ADJ_ID
  join REG_INCREMENTAL_ADJUST_TYPE RIT on RIA.INCREMENTAL_ADJ_TYPE_ID = RIT.INCREMENTAL_ADJ_TYPE_ID
  join REG_FORECAST_VERSION RFV on RIAV.FORECAST_VERSION_ID = RFV.FORECAST_VERSION_ID
  join REG_FCST_INC_ADJ_LEDGER RFL on RIAV.FORECAST_VERSION_ID = RFL.FORECAST_VERSION_ID
                                  and RFL.INCREMENTAL_ADJ_ID = RIA.INCREMENTAL_ADJ_ID
  join REG_ACCT_MASTER RAM on RAM.REG_ACCT_ID = RFL.REG_ACCT_ID
  join REG_ACCT_TYPE RAT on RAM.REG_ACCT_TYPE_DEFAULT = RAT.REG_ACCT_TYPE_ID
  join REG_SUB_ACCT_TYPE RSAT on RSAT.SUB_ACCT_TYPE_ID = RAM.SUB_ACCT_TYPE_ID
                              AND rsat.reg_acct_type_id = ram.reg_acct_type_default
  join REG_SOURCE RS on RFL.REG_SOURCE_ID = RS.REG_SOURCE_ID
  join REG_COMPANY RC on RFL.REG_COMPANY_ID = RC.REG_COMPANY_ID
  join COMPANY_SETUP CS on RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
  join COMPANY C on C.COMPANY_ID = CS.COMPANY_ID
  join REG_INCREMENTAL_FP_ADJ_TRANS RIFAT on RFL.REG_ACCT_ID =
                                             DECODE(RIFAT.OVERRIDE_TRANSLATE,
                                                    0,
                                                    RIFAT.REG_ACCT_ID,
                                                    RIFAT.OVERRIDE_REG_ACCT_ID)
                                         and RFL.INCREMENTAL_ADJ_ID = RIFAT.INCREMENTAL_ADJ_ID
                                         and RIFAT.FORECAST_VERSION_ID = RFL.FORECAST_VERSION_ID
  join INCREMENTAL_FP_ADJ_ITEM IFAI on RIFAT.ITEM_ID = IFAI.ITEM_ID
                                   and RIFAT.LABEL_ID = IFAI.LABEL_ID
  join INCREMENTAL_FP_ADJ_LABEL IFAL on RIFAT.LABEL_ID = IFAL.LABEL_ID
 where RIAV.STATUS_CODE_ID = 1
)
;

--Time Option
insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1, KEYCOLUMN1)
   values( 108,
          'Fcst Version + Start + End Months',
          'uo_ppbase_report_parms_dddw_mnum_span',
          'dw_reg_forecast_version_pick',
          'Forecast Version',
          'forecast_version_id');

--Dynamic Filters
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
   values (191,
          'Reg IFA Type',
          'dw',
          'incremental_adj_type_id',
          'dw_reg_ifa_type_filter',
          'incremental_adj_type_id',
          'description',
          'N');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
   values (192,
          'Reg IFA',
          'dw',
          'incremental_adj_id',
          'dw_reg_ifa_filter',
          'incremental_adj_id',
          'description',
          'N');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
   values( 193,
          'Reg Acct Type',
          'dw',
          'reg_acct_type_id',
          'dw_reg_acct_type_filter',
          'reg_acct_type_id',
          'description',
          'N');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
   values (194,
          'Reg Sub Acct Type',
          'dw',
          'sub_acct_type_id',
          'dw_reg_sub_acct_type_filter',
          'sub_acct_type_id',
          'description',
          'N');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (83, 'IFAType-IFA-AcctType-SA Type', null, 'uo_ppbase_tab_filter_dynamic');
   
   
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   VALUES(83, 191);
   insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   VALUES(83, 192);
   insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   VALUES(83, 193);
   insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   VALUES(83, 194);

--Report time
insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID)
   select max(REPORT_ID) + 1,
          'IFA By Reg Account',
          'Display Incremental Forecast Adjustments by Regulatory Account',
          'dw_reg_ifa_report_reg_acct',
          'RL - 102',
          101,
          405,
          108,
          83,
          1,
          3
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID)
   select max(REPORT_ID) + 1,
          'Reg Account by IFA',
          'Display Reg Account information by Incremental Forecast Adjustments',
          'dw_reg_ifa_report_reg_acct_by_ifa',
          'RL - 103',
          101,
          405,
          108,
          83,
          1,
          3
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID)
   select max(REPORT_ID) + 1,
          'IFA Data',
          'Display Incremental Forecast Adjustment Data',
          'dw_reg_ifa_report_no_reg_details',
          'RL - 104',
          101,
          405,
          108,
          1,
          1,
          3
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1340, 0, 10, 4, 2, 7, 39298, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039298_reg_ifa_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;