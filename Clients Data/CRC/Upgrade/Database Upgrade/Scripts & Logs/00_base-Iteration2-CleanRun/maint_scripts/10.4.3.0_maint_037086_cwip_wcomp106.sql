/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037086_cwip_wcomp106.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.3.0 04/29/2014 Sunjin Cone    Wip Comp Related Assets 106
||=================================================================================
*/

delete from RELATED_ASSET A
 where exists (select 1
          from CPR_LEDGER B
         where A.ASSET_ID = B.ASSET_ID
           and B.RETIREMENT_UNIT_ID = 1);

delete from RELATED_ASSET A
 where exists (select 1
          from CPR_LEDGER B
         where A.RELATED_ASSET_ID = B.ASSET_ID
           and B.RETIREMENT_UNIT_ID = 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1122, 0, 10, 4, 3, 0, 37086, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037086_cwip_wcomp106.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;