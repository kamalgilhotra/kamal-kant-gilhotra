/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044574_budgetcap_add_versionid1_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2     08/03/2015 Kevin Dettbarn   No current delivered budget with 
||					  version_status_id = 1
||============================================================================
*/

insert into budget_version_type (budget_version_type_id, description, long_description, budget_version_status_id)
select 
(select max(budget_version_type_id)+1 from budget_version_type) AS budget_version_type_id,
'Working' AS description,
'Working Budget - This budget is the current budget being used for forecasting purposes' AS long_description,
1 AS budget_version_status_id
from dual;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2742, 0, 2015, 2, 0, 0, 044574, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044574_budgetcap_add_versionid1_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;