/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047036_add_component_amount_wksp_menu_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 03/22/2017 Anand R          Add new menu for component amounts workspace
||============================================================================
*/

INSERT INTO ppbase_workspace (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID) 
VALUES ('LESSEE', 'manage_var_comp_amounts', 'Formula Component Amts', 'uo_ls_var_pmnt_component_amounts_wksp', 'Formula Component Amts',  1);


INSERT INTO ppbase_menu_items (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN) VALUES (
	'LESSEE','manage_var_comp_amounts', 2, 3, 'Formula Component Amts', 'Formula Component Amounts', 'menu_wksp_var_payments', 'manage_var_comp_amounts', 1);
	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3408, 0, 2017, 1, 0, 0, 47036, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047036_add_component_amount_wksp_menu_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	