/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030784_cwip_106cpi.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/16/2013 Sunjin Cone
||============================================================================
*/


create table CPI_WO_COMPARE
(
 COMPANY_ID        number(22,0),
 WORK_ORDER_ID     number(22,0),
 WORK_ORDER_NUMBER varchar2(35),
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 COST_ELEMENT_ID   number(22,0),
 CHARGE_TYPE_ID    number(22,0),
 BOOK_SUMMARY_ID   number(22,0),
 CWIP_106          number(22,2),
 CPR_106           number(22,2),
 DIFF_106          number(22,2),
 CWIP_101          number(22,2),
 CPR_101           number(22,2),
 DIFF_101          number(22,2),
 CWIP_FAKE101      number(22,2),
 FIX_METHOD        varchar2(35)
);

alter table CPI_WO_COMPARE
   add constraint PK_CPI_WO_COMPARE
       primary key (WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  CPI_WO_COMPARE is '(C) [04] The CPI WO Compare table is used in the Non-Unitization balancing of CPI dollars between CWIP_CHARGE and CPR_ACT_BASIS tables.';

comment on column CPI_WO_COMPARE.COMPANY_ID        is 'System-assigned identifier of a particular company.';
comment on column CPI_WO_COMPARE.WORK_ORDER_ID     is 'System-assigned identifier of the work order number.';
comment on column CPI_WO_COMPARE.WORK_ORDER_NUMBER is 'The work order number associated with the CPI activity';
comment on column CPI_WO_COMPARE.TIME_STAMP        is 'Standard system-assigned timestamp used for audit purposes.';
comment on column CPI_WO_COMPARE.USER_ID           is 'Standard system-assigned user id used for audit purposes.';
comment on column CPI_WO_COMPARE.COST_ELEMENT_ID   is 'System-assigned identifier of the CPI cost element';
comment on column CPI_WO_COMPARE.CHARGE_TYPE_ID    is 'System-assigned identifier of the CPI charge type';
comment on column CPI_WO_COMPARE.BOOK_SUMMARY_ID   is 'System-assigned identifier of the CPI book summary';
comment on column CPI_WO_COMPARE.CWIP_106          is 'The amount of Non-Unitized CPI in CWIP_CHARGE table';
comment on column CPI_WO_COMPARE.CPR_106           is 'The amount of Non-Unitized CPI in CPR_ACT_BASIS table';
comment on column CPI_WO_COMPARE.DIFF_106          is 'The comparison amount of CWIP_106 and CPR_106';
comment on column CPI_WO_COMPARE.CWIP_101          is 'The amount of Unitized CPI in CWIP_CHARGE table';
comment on column CPI_WO_COMPARE.CPR_101           is 'The amount of Unitized CPI in CPR_ACT_BASIS table';
comment on column CPI_WO_COMPARE.DIFF_101          is 'The comparison amount of CWIP_101 and CPR_101';
comment on column CPI_WO_COMPARE.CWIP_FAKE101      is 'The amount of Unitized CPI in CWIP_CHARGE table associated with a fake unitization record';
comment on column CPI_WO_COMPARE.FIX_METHOD        is 'The data change approach used to fix the Non-Unitized DIFF_106 amount:  update gl_account_id = 2 on cwip/insert diff_106 into cwip';

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION)
   select max(PROCESS_ID) + 1, 'WO Bal CPI', 'Work Order Balancing for CPI' from PP_PROCESSES;

insert into PP_CONVERSION_WINDOWS
   (WINDOW_ID, DESCRIPTION, LONG_DESCRIPTION, WINDOW_NAME)
   select max(WINDOW_ID) + 1, 'Fix 106 Bal CPI', 'Fix 106 Out of Balance CPI', 'w_cpi_wo_balance'
     from PP_CONVERSION_WINDOWS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (489, 0, 10, 4, 1, 0, 30784, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030784_cwip_106cpi.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
