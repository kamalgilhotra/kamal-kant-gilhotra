/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045048_lease_new_forecast_wksp_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/13/2015 Sarah Byers    New Fcst Version workspace and tables
||============================================================================
*/

-- LS_FORECAST_TYPE
insert into ls_forecast_type (
	forecast_type_id, description)
values (
	1, 'Full Forecast');

insert into ls_forecast_type (
	forecast_type_id, description)
values (
	2, 'Summary Forecast');

insert into ls_forecast_type (
	forecast_type_id, description)
values (
	3, 'Full Retrospective Forecast');

-- LS_FORECAST_RATE_OPTIONS
insert into ls_forecast_rate_options (
	forecast_rate_option_id, description)
values (
	1, 'Use Current ILR Rates');

insert into ls_forecast_rate_options (
	forecast_rate_option_id, description)
values (
	2, 'Use one Rate for All ILRs');

insert into ls_forecast_rate_options (
	forecast_rate_option_id, description)
values (
	3, 'Apply Effective Dated Rates only to ILRs with 0%');

insert into ls_forecast_rate_options (
	forecast_rate_option_id, description)
values (
	4, 'Apply Effective Dated Rates to All ILRs');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2918, 0, 2015, 2, 0, 0, 45048, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045048_lease_new_forecast_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;