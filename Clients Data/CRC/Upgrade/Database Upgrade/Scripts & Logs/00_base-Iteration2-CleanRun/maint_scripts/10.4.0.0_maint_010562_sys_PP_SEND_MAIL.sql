/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010562_sys_PP_SEND_MAIL.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   09/17/2012 Joseph King    Point Release
||============================================================================
*/

create or replace function PWRPLANT.PP_SEND_MAIL(A_FROMUSER    varchar,
                                                 A_SMTP_USERID varchar,
                                                 A_SMTP_PASSWD varchar,
                                                 A_SUBJECT     varchar,
                                                 A_TEXT        varchar,
                                                 A_USER        varchar,
                                                 A_SERVER      varchar,
                                                 A_FILENAME    varchar2) return number as
   /*
   ||============================================================================
   || Application:   PowerPlant
   || Function Name: PP_SEND_MAIL
   || Description:
   ||============================================================================
   || Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------  ---------- -------------- ----------------------------------------
   || 1.1      12/08/2004 Roger Roach    Added support for long raw data type for file data
   || 1.2      12/09/2004 Roger Roach    Added delete for pp_mail_data
   || 1.3      04/18/2006 Roger Roach    Added to the content-type another CRLF
   || 1.4      05/21/2007 Roger Roach    Added support for HTML Messages
   || 1.5      07/02/2007 Roger Roach    Added check for zero amt
   || 1.6      08/27/2007 Roger Roach    Added check for null attachment
   || 1.6.1    02/15/2008 Roger Roach    Added backets for to and from
   || 1.6.2    03/13/2008 Roger Roach    Added check no user or subject
   || 1.7      09/18/2008 Roger Roach    Added support for multiple attachments
   || 1.8      07/21/2009 Roger Roach    On an exception the connection is closed
   || 1.9      12/01/2009 Roger Roach    Added user id and password
   || 2.0      12/03/2009 Roger Roach    Removed backets for to and from
   || 2.1      04/26/2010 Roger Roach    Added a new line to each attachment line
   || 2.2      05/18/2010 Roger Roach    Padded the attachment to 2400
   || 2.3      05/27/2010 Roger Roach    Added delete for pp_mail_data
   || 2.4      06/03/2010 Roger Roach    Added additional byte to padding
   || 2.5      06/07/2010 Roger Roach    Cchanged base_encode64 output from blob to raw and remove the delete
   || 2.6      06/11/2010 Roger Roach    Remove padding
   || 2.7      06/22/2010 Roger Roach    Added data length to the attachment table
   || 2.8      08/16/2010 Roger Roach    Added a truncate for the attachment length
   || 2.8.1    08/16/2010 Roger Roach    Added delete for pp_mail_data
   || 2.8.2    09/19/2010 Roger Roach    Increased the buffer
   || 2.8.3    06/12/2012 Roger Roach    Changed to code to support multiple recipients
   || 2.8.4    06/19/2012 Joseph King    Changed length() to dbms_lob.getlength()
   ||============================================================================
   */

   cursor ATTACHMENT_CUR is
      select FILEDATA, FILENAME, DATALEN from PP_MAIL_DATA where SESSIONID = USERENV('SESSIONID');

   NUMATTACHMENTS number(22, 0);
   J              int;
   C              UTL_SMTP.CONNECTION;
   DATA           blob;
   DATAIN         blob;
   DATALEN        number(22, 0);
   FILENAME       varchar(200);
   TYPE USER_LIST_TYPE is table of varchar2(40)  index by binary_integer;
   TO_USERS        USER_LIST_TYPE;
   TO_USER        varchar2(4000);
   VAR            varchar(2400);
   DATALONG       long raw;
   REPLY          UTL_SMTP.REPLY;
   RDATA          raw(2400);
   RDATA1         raw(2400);
   RDATA2         raw(2400);
   RDATA_OUT      raw(20000);
   LEN            number(22, 0);
   POS            number(22, 0);
   NUM            number(22, 0);
   AMT            number(22, 0);
   RM             number(22, 0);
   DATATYPE       varchar2(40);
   SMTP_PASSWD    varchar2(100);
   SMTP_USERID    varchar2(100);
   SMTP_AUTH      boolean := false;

begin
   DBMS_OUTPUT.ENABLE(100000000);

   if A_FROMUSER is null or LENGTH(A_FROMUSER) = 0 then
      RAISE_APPLICATION_ERROR(-20000, 'Failed to send mail due to send mail due no from user. ');
      return - 1;
   end if;

   if A_SUBJECT is null or LENGTH(A_SUBJECT) = 0 then
      RAISE_APPLICATION_ERROR(-20000, 'Failed to send mail due to send mail due no to subject. ');
      return - 1;
   end if;

   if A_USER is null or LENGTH(A_USER) = 0 then
      RAISE_APPLICATION_ERROR(-20000, 'Failed to send mail due no to user. ');
      return - 1;
   end if;

   C     := UTL_SMTP.OPEN_CONNECTION(A_SERVER);
   REPLY := UTL_SMTP.HELO(C, A_SERVER);

   if SMTP_AUTH then
      SMTP_USERID := 'userid';
      SMTP_PASSWD := 'passwd';
      UTL_SMTP.COMMAND(C, 'AUTH LOGIN');
      UTL_SMTP.COMMAND(C, UTL_RAW.CAST_TO_VARCHAR2(UTL_ENCODE.BASE64_ENCODE(UTL_RAW.CAST_TO_RAW(SMTP_USERID))));
      UTL_SMTP.COMMAND(C, UTL_RAW.CAST_TO_VARCHAR2(UTL_ENCODE.BASE64_ENCODE(UTL_RAW.CAST_TO_RAW(SMTP_PASSWD))));
   end if;
   TO_USER := A_USER;

   REPLY := UTL_SMTP.MAIL(C, A_FROMUSER);
   if INSTR(TO_USER, ';') > 0 then
      while INSTR(TO_USER, ';') > 0
       loop
         POS          := INSTR(TO_USER, ';');
         TO_USERS(1)  := SUBSTR(TO_USER, 1, POS - 1);
         TO_USER  := SUBSTR(TO_USER,  POS + 1);
         REPLY := UTL_SMTP.RCPT(C, TO_USERS(1));
      end loop;
      REPLY := UTL_SMTP.RCPT(C, TO_USER);
   else
      REPLY := UTL_SMTP.RCPT(C, TO_USER);
   end if;
   UTL_SMTP.OPEN_DATA(C);
   UTL_SMTP.WRITE_DATA(C, 'From: ' || A_FROMUSER || UTL_TCP.CRLF);
   UTL_SMTP.WRITE_DATA(C, 'To: ' || A_USER || UTL_TCP.CRLF);
   UTL_SMTP.WRITE_DATA(C, 'Subject: ' || A_SUBJECT || UTL_TCP.CRLF);
   UTL_SMTP.WRITE_DATA(C, 'Content-Type: multipart/mixed;' || UTL_TCP.CRLF || '   boundary="26734hesdry73gh7se78111"' || UTL_TCP.CRLF || UTL_TCP.CRLF);
   UTL_SMTP.WRITE_DATA(C, 'This is a multi-part message in MIME format.' || UTL_TCP.CRLF);
   UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF || '--26734hesdry73gh7se78111' || UTL_TCP.CRLF);

   UTL_SMTP.WRITE_DATA(C, 'Content-Type: text/html;' || UTL_TCP.CRLF || ' charset=us-ascii' || UTL_TCP.CRLF);
   UTL_SMTP.WRITE_DATA(C, 'Content-Transfer-Encoding: 8bit' || UTL_TCP.CRLF);
   UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF || A_TEXT || UTL_TCP.CRLF);

   if A_FILENAME != ' ' then
      UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF || '--26734hesdry73gh7se78111' || UTL_TCP.CRLF);

      select DATA_TYPE
        into DATATYPE
        from ALL_TAB_COLUMNS
       where TABLE_NAME = 'PP_MAIL_DATA'
         and COLUMN_NAME = 'FILEDATA';

      select count(*) into NUMATTACHMENTS from PP_MAIL_DATA where SESSIONID = USERENV('SESSIONID');

      open ATTACHMENT_CUR;
      for J in 1 .. NUMATTACHMENTS
      loop
         if DATATYPE <> 'BLOB' then
            fetch ATTACHMENT_CUR
               into DATALONG, FILENAME, DATALEN;
            --select filedata into datalong from pp_mail_data where sessionid = userenv('SESSIONID');
            DATAIN := TO_BLOB(DATALONG);
         else
            fetch ATTACHMENT_CUR
               into DATAIN, FILENAME, DATALEN;
            --select filedata into datain from pp_mail_data where sessionid = userenv('SESSIONID');
         end if;

         -- JAK: 2012-06-19:  use dbms_lob.getlength instead of length.  Can use this generally
         -- rather than relying on the datalen field on pp_mail_data to be populated.
         DATALEN := DBMS_LOB.GETLENGTH(DATAIN);

         if DATALEN > 0 then
            --utl_smtp.write_data(c,utl_tcp.CRLF ||'--26734hesdry73gh7se78111' ||  utl_tcp.CRLF );
            UTL_SMTP.WRITE_DATA(C, 'Content-Type: application/octet-stream;' || UTL_TCP.CRLF || ' name="' || FILENAME || '"' || UTL_TCP.CRLF);
            UTL_SMTP.WRITE_DATA(C, 'Content-Description: ' || A_FILENAME || UTL_TCP.CRLF);
            UTL_SMTP.WRITE_DATA(C, 'Content-Transfer-Encoding: base64' || UTL_TCP.CRLF);
            UTL_SMTP.WRITE_DATA(C, 'Content-Disposition: attachment;' || UTL_TCP.CRLF || ' filename="' || FILENAME || '"' || UTL_TCP.CRLF || UTL_TCP.CRLF);
            LEN := DATALEN;
            POS := 1;
            DBMS_OUTPUT.PUT_LINE('Len=' || TO_CHAR(DATALEN));
            NUM := TRUNC(LEN / 2400) + 1;

            for I in 1 .. NUM
            loop
               AMT := 2400;

               if LEN < (POS + AMT) then
                  AMT := LEN - POS + 1;
               end if;

               begin
                  DBMS_LOB.READ(DATAIN, AMT, POS, RDATA);
               exception
                  when others then
                     null;
               end;

               if LEN < 2400 then
                  -- The attachment must be 2400 characters or greater
                  RM     := 2400 - AMT;
                  RDATA1 := RDATA;
                  VAR    := LPAD(' ', RM);
                  RDATA2 := UTL_RAW.CAST_TO_RAW(LPAD(' ', RM));
                  RDATA  := UTL_RAW.CONCAT(RDATA1, RDATA2);
                  AMT    := 2400;
               end if;

               DBMS_OUTPUT.PUT_LINE('amt=' || TO_CHAR(AMT));

               if AMT <> 0 then
                  POS       := POS + AMT;
                  RDATA_OUT := UTL_ENCODE.BASE64_ENCODE(RDATA);
                  UTL_SMTP.WRITE_RAW_DATA(C, RDATA_OUT);
                  UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF);
               end if;

            end loop;

            if J = NUMATTACHMENTS then
               UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF || '--26734hesdry73gh7se78111' || '--');
            else
               UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF || '--26734hesdry73gh7se78111');
            end if;

            UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF);
         else
            UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF || ' Error Sending Attachment! ' || FILENAME || UTL_TCP.CRLF);
         end if;

      end loop;

      close ATTACHMENT_CUR;
   end if;

   UTL_SMTP.WRITE_DATA(C, UTL_TCP.CRLF);
   UTL_SMTP.CLOSE_DATA(C);
   UTL_SMTP.QUIT(C);
   delete PP_MAIL_DATA where SESSIONID = USERENV('SESSIONID');
   commit;
   return 0;

exception
   when others then
      UTL_SMTP.CLOSE_DATA(C);
      UTL_SMTP.QUIT(C);
      RAISE_APPLICATION_ERROR(-20000, 'Failed to send mail due to the following error: ' || sqlerrm);
      return - 1;
end;
/


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (216, 0, 10, 4, 0, 0, 10562, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_010562_sys_PP_SEND_MAIL.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
