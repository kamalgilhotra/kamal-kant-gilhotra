/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045367_lease_make_fcst_version_descr_unique_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 02/25/2016 Charlie Shilling make description of fcst versions unique.
||============================================================================
*/
DECLARE
	duplicate_keys EXCEPTION;
	PRAGMA EXCEPTION_INIT(duplicate_keys, -1452); --O R A - 0 1 4 5 2 cannot CREATE UNIQUE INDEX; duplicate keys found
	l_sql VARCHAR2(2000);
BEGIN
	EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX ls_fcst_ver_descr_uix ON ls_forecast_version(Lower(Trim(description))) TABLESPACE pwrplant_idx';
EXCEPTION
	WHEN duplicate_keys THEN
		Raise_Application_Error(-20000, 'ACTION REQUIRED: Could not add unique constraint on LS_FORECAST_VERSION.DESCRIPTION because multiple forecast versions have the same description. Update the duplicate descriptions and re-run this script. See script for details.');
END;
/

--ERROR DETAILS:
--Run the following SQL to identify descriptions that are assigned to multiple forecast versions.

--	SELECT description, Count(1)
--	FROM ls_forecast_version
--	GROUP BY description
--	HAVING Count(1) > 1;

--Here is a sample UPDATE statement that will remove the dupilicate descriptions by concatenating them with the ID:

--	UPDATE ls_forecast_version lfv
--	SET description = description || ': ' || revision
--	WHERE EXISTS (
--		SELECT 1
--		FROM (
--			SELECT description
--			FROM ls_forecast_version
--			GROUP BY description
--			HAVING Count(1) > 1
--		)descr
--		WHERE descr.description  = lfv.description
--	);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3072, 0, 2016, 1, 0, 0, 045367, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045367_lease_make_fcst_version_descr_unique_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;