/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037654_depr_update_cpr_depr_mpms.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/09/2014 Charlie Shilling
||============================================================================
*/

update DEPR_GROUP
   set MID_PERIOD_METHOD = 'SL'
 where SUBLEDGER_TYPE_ID in
      (select A.SUBLEDGER_TYPE_ID from SUBLEDGER_CONTROL A where A.DEPRECIATION_INDICATOR = 3)
   and MID_PERIOD_METHOD not in
      (select B.MID_PERIOD_METHOD from DEPR_MID_PERIOD_METHOD B where B.CALC_OPTION = 'I');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1105, 0, 10, 4, 2, 0, 37654, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037654_depr_update_cpr_depr_mpms.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
