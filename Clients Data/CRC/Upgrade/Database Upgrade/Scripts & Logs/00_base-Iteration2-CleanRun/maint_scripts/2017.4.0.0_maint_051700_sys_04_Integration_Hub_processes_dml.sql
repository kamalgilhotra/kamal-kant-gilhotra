/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051700_sys_04_Integration_Hub_processes_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/29/2018 TechProdMgmt	Support of Base Integration Hub APIs
||============================================================================
*/

-- this script is rerunnable
-- pp_processes
insert into pp_processes (PROCESS_ID, DESCRIPTION, TIME_STAMP, USER_ID, LONG_DESCRIPTION, EXECUTABLE_FILE, RUNNING_SESSION_ID, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED, SYSTEM_LOCK, PREREQUISITE, POSTREQUISITE, PRIMARY_TABLES, FUNCTION_SUMMARY, TECHNICAL_SUMMARY, BASELINE_OCC_ID, KICKOUT_WINDOW, KICKOUT_ARG, ASYNC_EMAIL_ON_COMPLETE, CPR_MONTH_END_FLAG, WO_MONTH_END_FLAG)
select p_id, 'Integration Hub', sysdate, 'PWRPLANT', 'Integration Hub', 'Integration Hub', null, null, 0, 0, 1, null, null, null, null, null, null, null, null, 0, null, null
from (
  select max(process_id) + 1 p_id
  from pp_processes )
where not exists
	(select 1 from pp_processes x where x.description = 'Integration Hub')
;

insert into pp_processes (PROCESS_ID, DESCRIPTION, TIME_STAMP, USER_ID, LONG_DESCRIPTION, EXECUTABLE_FILE, RUNNING_SESSION_ID, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED, SYSTEM_LOCK, PREREQUISITE, POSTREQUISITE, PRIMARY_TABLES, FUNCTION_SUMMARY, TECHNICAL_SUMMARY, BASELINE_OCC_ID, KICKOUT_WINDOW, KICKOUT_ARG, ASYNC_EMAIL_ON_COMPLETE, CPR_MONTH_END_FLAG, WO_MONTH_END_FLAG)
select p_id, 'GL Master Element Data (Inbound)', sysdate, 'PWRPLANT', 'Standard GL Master Element Data API (Outbound via Integration Hub)', 'Master_Data_API', null, null, 0, 0, 1, null, null, null, null, null, null, null, null, 0, null, null
from (
  select max(process_id) + 1 p_id
  from pp_processes )
where not exists
	(select 1 from pp_processes x where x.description = 'GL Master Element Data (Inbound)')
;

insert into pp_processes (PROCESS_ID, DESCRIPTION, TIME_STAMP, USER_ID, LONG_DESCRIPTION, EXECUTABLE_FILE, RUNNING_SESSION_ID, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED, SYSTEM_LOCK, PREREQUISITE, POSTREQUISITE, PRIMARY_TABLES, FUNCTION_SUMMARY, TECHNICAL_SUMMARY, BASELINE_OCC_ID, KICKOUT_WINDOW, KICKOUT_ARG, ASYNC_EMAIL_ON_COMPLETE, CPR_MONTH_END_FLAG, WO_MONTH_END_FLAG)
select p_id, 'GL Journals API (Outbound)', sysdate, 'PWRPLANT', 'Standard GL Journals API (Outbound via Integration Hub)', 'GL_Journals_API', null, null, 0, 0, 1, null, null, null, null, null, null, null, null, 0, null, null
from (
  select max(process_id) + 1 p_id
  from pp_processes )
where not exists
	(select 1 from pp_processes x where x.description = 'GL Journals API (Outbound)')
;

insert into pp_processes (PROCESS_ID, DESCRIPTION, TIME_STAMP, USER_ID, LONG_DESCRIPTION, EXECUTABLE_FILE, RUNNING_SESSION_ID, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED, SYSTEM_LOCK, PREREQUISITE, POSTREQUISITE, PRIMARY_TABLES, FUNCTION_SUMMARY, TECHNICAL_SUMMARY, BASELINE_OCC_ID, KICKOUT_WINDOW, KICKOUT_ARG, ASYNC_EMAIL_ON_COMPLETE, CPR_MONTH_END_FLAG, WO_MONTH_END_FLAG)
select p_id, 'Currency Rate API (Inbound)', sysdate, 'PWRPLANT', 'Standard Currency Rate API (Inbound via Integration Hub)', 'Currency_Rate_API', null, null, 0, 0, 1, null, null, null, null, null, null, null, null, 0, null, null
from (
  select max(process_id) + 1 p_id
  from pp_processes )
where not exists
	(select 1 from pp_processes x where x.description = 'Currency Rate API (Inbound)')
;

insert into pp_processes (PROCESS_ID, DESCRIPTION, TIME_STAMP, USER_ID, LONG_DESCRIPTION, EXECUTABLE_FILE, RUNNING_SESSION_ID, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED, SYSTEM_LOCK, PREREQUISITE, POSTREQUISITE, PRIMARY_TABLES, FUNCTION_SUMMARY, TECHNICAL_SUMMARY, BASELINE_OCC_ID, KICKOUT_WINDOW, KICKOUT_ARG, ASYNC_EMAIL_ON_COMPLETE, CPR_MONTH_END_FLAG, WO_MONTH_END_FLAG)
select p_id, 'Currency API (Inbound)', sysdate, 'PWRPLANT', 'Standard Currency API (Inbound via Integration Hub)', 'Currency_API', null, null, 0, 0, 1, null, null, null, null, null, null, null, null, 0, null, null
from (
  select max(process_id) + 1 p_id
  from pp_processes )
where not exists
	(select 1 from pp_processes x where x.description = 'Currency API (Inbound)')
;

insert into pp_processes (PROCESS_ID, DESCRIPTION, TIME_STAMP, USER_ID, LONG_DESCRIPTION, EXECUTABLE_FILE, RUNNING_SESSION_ID, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED, SYSTEM_LOCK, PREREQUISITE, POSTREQUISITE, PRIMARY_TABLES, FUNCTION_SUMMARY, TECHNICAL_SUMMARY, BASELINE_OCC_ID, KICKOUT_WINDOW, KICKOUT_ARG, ASYNC_EMAIL_ON_COMPLETE, CPR_MONTH_END_FLAG, WO_MONTH_END_FLAG)
select p_id, 'GL Combo Validation Data (Inbound)', sysdate, 'PWRPLANT', 'Standard GL Combo Validation API (Inbound via Integration Hub)', 'Combo_Validation_API', null, null, 0, 0, 1, null, null, null, null, null, null, null, null, 0, null, null
from (
  select max(process_id) + 1 p_id
  from pp_processes )
where not exists
	(select 1 from pp_processes x where x.description = 'GL Combo Validation Data (Inbound)')
;

insert into pp_processes (PROCESS_ID, DESCRIPTION, TIME_STAMP, USER_ID, LONG_DESCRIPTION, EXECUTABLE_FILE, RUNNING_SESSION_ID, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED, SYSTEM_LOCK, PREREQUISITE, POSTREQUISITE, PRIMARY_TABLES, FUNCTION_SUMMARY, TECHNICAL_SUMMARY, BASELINE_OCC_ID, KICKOUT_WINDOW, KICKOUT_ARG, ASYNC_EMAIL_ON_COMPLETE, CPR_MONTH_END_FLAG, WO_MONTH_END_FLAG)
select p_id, 'Lessee Data (Inbound)', sysdate, 'PWRPLANT', 'Standard Lessee Data API (Inbound via Integration Hub)', 'Lessee_Data_API', null, null, 0, 0, 1, null, null, null, null, null, null, null, null, 0, null, null
from (
  select max(process_id) + 1 p_id
  from pp_processes )
where not exists
	(select 1 from pp_processes x where x.description = 'Lessee Data (Inbound)')
;

-- pp_processes_kickout_control
insert into pp_processes_kickout_control(PROCESS,COMPONENT,PRIORITY)
select 'GL Master Element Data (Inbound)','Integration Hub',1 from dual
minus
select process, component, priority from pp_processes_kickout_control
;
insert into pp_processes_kickout_control(PROCESS,COMPONENT,PRIORITY)
select 'Currency Rate API (Inbound)','Integration Hub',1 from dual
minus
select process, component, priority from pp_processes_kickout_control
;
insert into pp_processes_kickout_control(PROCESS,COMPONENT,PRIORITY)
select 'GL Combo Validation Data (Inbound)','Integration Hub',1 from dual
minus
select process, component, priority from pp_processes_kickout_control
;

insert into pp_processes_kickout_control(PROCESS,COMPONENT,PRIORITY)
select 'Integration Hub','Integration Hub',1 from dual
minus
select process, component, priority from pp_processes_kickout_control
;

--pp_processes_kickout_table...
insert into pp_processes_kickout_table (KICKOUT_TABLE_ID, COMPONENT, TABLE_NAME, ARC_TABLE_NAME, BASE_INDICATOR, DESCRIPTION, REVIEW_WHERE_CLAUSE, TIME_STAMP, USER_ID, PROCESS, PRIORITY)
select k_id, 'Integration Hub', 'pp_processes_kickout_data', 'pp_processes_kickout_data_arc', 1, 'Standard API Kickout table', null, sysdate, 'PWRPLANT', 'Currency Rate API (Inbound)', 1
from (
  select nvl(max(KICKOUT_TABLE_ID) + 1, 1) k_id
  from pp_processes_kickout_table )
where not exists
	(select 1 from pp_processes_kickout_table x where x.PROCESS = 'Currency Rate API (Inbound)')
;

insert into pp_processes_kickout_table (KICKOUT_TABLE_ID, COMPONENT, TABLE_NAME, ARC_TABLE_NAME, BASE_INDICATOR, DESCRIPTION, REVIEW_WHERE_CLAUSE, TIME_STAMP, USER_ID, PROCESS, PRIORITY)
select k_id, 'Integration Hub', 'pp_processes_kickout_data', 'pp_processes_kickout_data_arc', 1, 'Standard API Kickout table', null, sysdate, 'PWRPLANT', 'GL Master Element Data (Inbound)', 1
from (
  select nvl(max(KICKOUT_TABLE_ID) + 1, 1) k_id
  from pp_processes_kickout_table )
where not exists
	(select 1 from pp_processes_kickout_table x where x.PROCESS = 'GL Master Element Data (Inbound)')
;

insert into pp_processes_kickout_table (KICKOUT_TABLE_ID, COMPONENT, TABLE_NAME, ARC_TABLE_NAME, BASE_INDICATOR, DESCRIPTION, REVIEW_WHERE_CLAUSE, TIME_STAMP, USER_ID, PROCESS, PRIORITY)
select k_id, 'Integration Hub', 'pp_processes_kickout_data', 'pp_processes_kickout_data_arc', 1, 'Standard API Kickout table', null, sysdate, 'PWRPLANT', 'GL Combo Validation Data (Inbound)', 1
from (
  select nvl(max(KICKOUT_TABLE_ID) + 1, 1) k_id
  from pp_processes_kickout_table )
where not exists
	(select 1 from pp_processes_kickout_table x where x.PROCESS = 'GL Combo Validation Data (Inbound)')
;

insert into pp_processes_kickout_table (KICKOUT_TABLE_ID, COMPONENT, TABLE_NAME, ARC_TABLE_NAME, BASE_INDICATOR, DESCRIPTION, REVIEW_WHERE_CLAUSE, TIME_STAMP, USER_ID, PROCESS, PRIORITY)
select k_id, 'Integration Hub', 'pp_processes_kickout_data', 'pp_processes_kickout_data_arc', 1, 'Standard API Kickout table', null, sysdate, 'PWRPLANT', 'Integration Hub', 1
from (
  select nvl(max(KICKOUT_TABLE_ID) + 1, 1) k_id
  from pp_processes_kickout_table )
where not exists
	(select 1 from pp_processes_kickout_table x where x.PROCESS = 'Integration Hub')
;

--pp_processes_return_values...
insert into pp_processes_return_values
select process_id, 1, 'SUCCESS','SUCCESSS',1,SYSDATE,'PWRPLANT',NULL,NULL,NULL,1
from (
select process_id
FROM PP_PROCESSES
WHERE DESCRIPTION in ('GL Master Element Data (Inbound)', 'Currency API (Inbound)','Currency Rate API (Inbound)','GL Journals API (Outbound)','GL Combo Validation Data (Inbound)','Lessee Data (Inbound)', 'Integration Hub')
minus
select process_id from pp_processes_return_values where description = 'SUCCESS'
);

insert into pp_processes_return_values
select process_id, -1, 'FAILURE','FAILURE',-1,SYSDATE,'PWRPLANT',NULL,NULL,NULL,0
from (
select process_id
FROM PP_PROCESSES
WHERE DESCRIPTION in ('GL Master Element Data (Inbound)', 'Currency API (Inbound)','Currency Rate API (Inbound)','GL Journals API (Outbound)','GL Combo Validation Data (Inbound)','Lessee Data (Inbound)', 'Integration Hub')
minus
select process_id from pp_processes_return_values where description = 'FAILURE'
);

insert into pp_processes_return_values
select process_id, -2, 'VALIDATION ERROR','VALIDATION ERROR',-2,SYSDATE,'PWRPLANT',NULL,NULL,NULL,0
from (
select process_id
FROM PP_PROCESSES
WHERE DESCRIPTION in ('GL Master Element Data (Inbound)', 'Currency API (Inbound)','Currency Rate API (Inbound)','GL Journals API (Outbound)','GL Combo Validation Data (Inbound)','Lessee Data (Inbound)', 'Integration Hub')
minus
select process_id from pp_processes_return_values where description = 'VALIDATION ERROR'
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7594, 0, 2017, 4, 0, 0, 51700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051700_sys_04_Integration_Hub_processes_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

