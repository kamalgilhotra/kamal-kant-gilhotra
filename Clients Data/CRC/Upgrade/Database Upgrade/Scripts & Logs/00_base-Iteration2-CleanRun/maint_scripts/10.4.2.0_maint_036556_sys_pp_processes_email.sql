/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036556_sys_pp_processes_email.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/25/2014 Brandon Beck
||============================================================================
*/

alter table PP_PROCESSES add ASYNC_EMAIL_ON_COMPLETE number(1,0) default 0;

comment on column PP_PROCESSES.ASYNC_EMAIL_ON_COMPLETE is 'A column specifying whether or not notifications are turned on for this interface when run in ASYNC mode.';

update PP_PROCESSES set ASYNC_EMAIL_ON_COMPLETE = 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (988, 0, 10, 4, 2, 0, 36556, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036556_sys_pp_processes_email.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;