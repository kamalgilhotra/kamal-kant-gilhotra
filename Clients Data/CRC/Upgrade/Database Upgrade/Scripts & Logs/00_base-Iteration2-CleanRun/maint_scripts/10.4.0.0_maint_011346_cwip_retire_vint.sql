/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011343_cwip_retire_vint.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/05/2012 Sunjin Cone    Point Release
||============================================================================
*/

delete from PP_WO_EST_CUSTOMIZE where LOWER(COLUMN_NAME) = 'retire_vintage';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (245, 0, 10, 4, 0, 0, 11346, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011346_cwip_retire_vint.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
