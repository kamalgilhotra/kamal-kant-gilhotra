/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_050125_lease_02_asset_schedule_by_mla_query_dml.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 11/12/2017 Josh Sandler		     	Restore overwritten multicurrency columns
||========================================================================================
*/

UPDATE pp_any_query_criteria
SET SQL = 'select la.ls_asset_id,          la.leased_asset_number,          co.company_id,          co.description               as company_description,          ilr.ilr_id,          ilr.ilr_number,          ll.lease_id,          ll.lease_number,          lct.description              as lease_cap_type,          al.long_description          as location,          las.REVISION,          las.SET_OF_BOOKS_ID,          to_char(las.MONTH, ''yyyymm'') as monthnum,          lcurt.DESCRIPTION AS currency_type,         las.iso_code currency,         las.currency_display_symbol,         las.BEG_CAPITAL_COST,          las.END_CAPITAL_COST,          las.BEG_OBLIGATION,          las.END_OBLIGATION,          las.BEG_LT_OBLIGATION,          las.END_LT_OBLIGATION,          las.INTEREST_ACCRUAL,          las.PRINCIPAL_ACCRUAL,          las.INTEREST_PAID,          las.PRINCIPAL_PAID,          las.EXECUTORY_ACCRUAL1,          las.EXECUTORY_ACCRUAL2,          las.EXECUTORY_ACCRUAL3,          las.EXECUTORY_ACCRUAL4,          las.EXECUTORY_ACCRUAL5,          las.EXECUTORY_ACCRUAL6,          las.EXECUTORY_ACCRUAL7,          las.EXECUTORY_ACCRUAL8,          las.EXECUTORY_ACCRUAL9,          las.EXECUTORY_ACCRUAL10,          las.EXECUTORY_PAID1,          las.EXECUTORY_PAID2,          las.EXECUTORY_PAID3,          las.EXECUTORY_PAID4,          las.EXECUTORY_PAID5,          las.EXECUTORY_PAID6,          las.EXECUTORY_PAID7,          las.EXECUTORY_PAID8,          las.EXECUTORY_PAID9,          las.EXECUTORY_PAID10,          las.CONTINGENT_ACCRUAL1,          las.CONTINGENT_ACCRUAL2,          las.CONTINGENT_ACCRUAL3,          las.CONTINGENT_ACCRUAL4,          las.CONTINGENT_ACCRUAL5,          las.CONTINGENT_ACCRUAL6,          las.CONTINGENT_ACCRUAL7,          las.CONTINGENT_ACCRUAL8,          las.CONTINGENT_ACCRUAL9,          las.CONTINGENT_ACCRUAL10,          las.CONTINGENT_PAID1,          las.CONTINGENT_PAID2' ||
',          las.CONTINGENT_PAID3,          las.CONTINGENT_PAID4,          las.CONTINGENT_PAID5,          las.CONTINGENT_PAID6,          las.CONTINGENT_PAID7,          las.CONTINGENT_PAID8,          las.CONTINGENT_PAID9,          las.CONTINGENT_PAID10,          las.IS_OM,          las.CURRENT_LEASE_COST,          las.RESIDUAL_AMOUNT,          las.TERM_PENALTY,          las.BPO_PRICE,          las.BEG_DEFERRED_RENT,          las.DEFERRED_RENT,          las.END_DEFERRED_RENT,          las.BEG_ST_DEFERRED_RENT,          las.END_ST_DEFERRED_RENT   from   ls_asset la,          ls_ilr ilr,          ls_lease ll,          company co,          asset_location al,          ls_lease_cap_type lct,          ls_ilr_options ilro,          v_ls_asset_schedule_fx_vw las ,      ls_lease_currency_type lcurt  where  la.ilr_id = ilr.ilr_id          and ilr.lease_id = ll.lease_id          and ilr.ilr_id = ilro.ilr_id          and ilr.current_revision = ilro.revision          and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id          and la.asset_location_id = al.asset_location_id          and la.company_id = co.company_id          and las.ls_asset_id = la.ls_asset_id       and las.ls_cur_type = lcurt.ls_currency_type_id         and las.revision = la.approved_revision          and upper(ll.lease_number) in (select upper(filter_value)                                         from   pp_any_required_filter                                         where              upper(trim(column_name)) = ''LEASE NUMBER'')       AND UPPER(TRIM(lcurt.DESCRIPTION)) IN (SELECT UPPER(filter_value) FROM pp_any_required_filter WHERE Upper(Trim(column_name)) = ''CURRENCY TYPE'')'
WHERE description = 'Lease Asset Schedule By MLA';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4064, 0, 2017, 1, 0, 0, 50125, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_050125_lease_02_asset_schedule_by_mla_query_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
