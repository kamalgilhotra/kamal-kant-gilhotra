/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_045616_6_reg_stat_ledger_query_dml.sql
||========================================================================================
|| Copyright (C) 2016 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 2016.1.0.0 04/05/2016 Sarah Byers	  Create Stat Ledger Query
||========================================================================================
*/

insert into cr_dd_sources_criteria (
	id, source_id, criteria_field, table_name, description, feeder_field, sql)
select nvl(max(id),0) + 1, 
		 1002,
		 'none',
		 'reg_statistic_ledger_sv',
		 'Regulatory Statistic Ledger',
		 'none',
		 null
  from cr_dd_sources_criteria;

insert into cr_dd_sources_criteria_fields (
	id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, quantity_field, column_case, element_id, format_override,
	required_filter, required_one_multi, dddw_sql, table_lookup)
select id, 'HISTORIC_LEDGER', 1, 0, 1, null,
		 'Historic Ledger', 413, 0, 'Any', null, null,
		 0, null, null, 0
  from cr_dd_sources_criteria
 where lower(trim(table_name)) = 'reg_statistic_ledger_sv';

insert into cr_dd_sources_criteria_fields (
	id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, quantity_field, column_case, element_id, format_override,
	required_filter, required_one_multi, dddw_sql, table_lookup)
select id, 'FORECAST_LEDGER', 2, 0, 1, null,
		 'Forecast Ledger', 441, 0, 'Any', null, null,
		 0, null, null, 0
  from cr_dd_sources_criteria
 where lower(trim(table_name)) = 'reg_statistic_ledger_sv';

insert into cr_dd_sources_criteria_fields (
	id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, quantity_field, column_case, element_id, format_override,
	required_filter, required_one_multi, dddw_sql, table_lookup)
select id, 'REG_COMPANY', 3, 0, 1, null,
		 'Reg Company', 441, 0, 'Any', null, null,
		 0, null, null, 0
  from cr_dd_sources_criteria
 where lower(trim(table_name)) = 'reg_statistic_ledger_sv';

insert into cr_dd_sources_criteria_fields (
	id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, quantity_field, column_case, element_id, format_override,
	required_filter, required_one_multi, dddw_sql, table_lookup)
select id, 'STATISTIC', 4, 0, 1, null,
		 'Statistic', 230, 0, 'Any', null, null,
		 0, null, null, 0
  from cr_dd_sources_criteria
 where lower(trim(table_name)) = 'reg_statistic_ledger_sv';

insert into cr_dd_sources_criteria_fields (
	id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, quantity_field, column_case, element_id, format_override,
	required_filter, required_one_multi, dddw_sql, table_lookup)
select id, 'MONTH_NUMBER', 5, 0, 1, null,
		 'Month Number', 395, 0, 'Any', null, null,
		 0, null, null, 0
  from cr_dd_sources_criteria
 where lower(trim(table_name)) = 'reg_statistic_ledger_sv';

insert into cr_dd_sources_criteria_fields (
	id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, quantity_field, column_case, element_id, format_override,
	required_filter, required_one_multi, dddw_sql, table_lookup)
select id, 'STAT_VALUE', 6, 0, 0, null,
		 'Stat Value', 290, 1, 'Any', null, null,
		 0, null, null, 0
  from cr_dd_sources_criteria
 where lower(trim(table_name)) = 'reg_statistic_ledger_sv';

insert into cr_dd_sources_criteria_fields (
	id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, quantity_field, column_case, element_id, format_override,
	required_filter, required_one_multi, dddw_sql, table_lookup)
select id, 'UNIT_OF_MEASURE', 7, 0, 1, null,
		 'Unit Of Measure', 441, 0, 'Any', null, null,
		 0, null, null, 0
  from cr_dd_sources_criteria
 where lower(trim(table_name)) = 'reg_statistic_ledger_sv';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3160, 0, 2016, 1, 0, 0, 045616, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045616_6_reg_stat_ledger_query_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;