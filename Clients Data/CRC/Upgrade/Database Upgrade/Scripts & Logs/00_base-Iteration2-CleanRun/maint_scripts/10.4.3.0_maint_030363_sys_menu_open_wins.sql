/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_030363_sys_menu_open_wins.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/11/2014 Alex P.
||============================================================================
*/

create table PPBASE_WORKSPACE_OBJECT_TYPE
(
 OBJECT_TYPE_ID number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(254) not null
);

alter table PPBASE_WORKSPACE_OBJECT_TYPE
   add constraint PWOT_OBJECT_TYPE_ID_PK
       primary key (OBJECT_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

comment on table PPBASE_WORKSPACE_OBJECT_TYPE is '(F)  [10] The Ppbase Workspace Object Type table defines types of objects that can be opened from the ppbase menu. These may include workspace user objects and different types of windows.';
comment on column PPBASE_WORKSPACE_OBJECT_TYPE.OBJECT_TYPE_ID is 'System-assigned identifier of an object type.';
comment on column PPBASE_WORKSPACE_OBJECT_TYPE.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column PPBASE_WORKSPACE_OBJECT_TYPE.USER_ID is 'Standard System-assigned user id used for audit purposes.';
comment on column PPBASE_WORKSPACE_OBJECT_TYPE.DESCRIPTION is 'Description of the object type.';

insert into PPBASE_WORKSPACE_OBJECT_TYPE
   (OBJECT_TYPE_ID, DESCRIPTION)
values
   (1, 'User Object inherited from uo_ppbase_workspace.');
insert into PPBASE_WORKSPACE_OBJECT_TYPE
   (OBJECT_TYPE_ID, DESCRIPTION)
values
   (2, 'A Powerbuilder window object, opened with an OpenSheet() command.');

alter table PPBASE_WORKSPACE add OBJECT_TYPE_ID number(22,0);

comment on column PPBASE_WORKSPACE.OBJECT_TYPE_ID is 'System-assigned identifier of an object type defined in PPBASE_WORKSPACE_OBJECT_TYPE.';

alter table PPBASE_WORKSPACE
   add constraint PW_OBJECT_TYPE_ID_FK
       foreign key (OBJECT_TYPE_ID)
       references PPBASE_WORKSPACE_OBJECT_TYPE;

update PPBASE_WORKSPACE set OBJECT_TYPE_ID = 1;

alter table PPBASE_WORKSPACE modify OBJECT_TYPE_ID not null;

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'w_tax_archive',
       OBJECT_TYPE_ID = 2
 where WORKSPACE_UO_NAME = 'uo_tax_case_wksp_archive'
   and MODULE = 'powertax';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1413, 0, 10, 4, 3, 0, 30363, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_030363_sys_menu_open_wins.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
