/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_043986_jobserver_filewatcher_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2     06/22/2015 Ryan Oliveria    	 Creating new table 'PP_JOB_FILE_WATCHER'
||==========================================================================================
*/

SET serveroutput ON size 30000;

declare
	doesTableExist number := 0;
begin
	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_JOB_FILE_WATCHER','PWRPLANT');

	if doesTableExist = 0 then
        begin
			dbms_output.put_line('Creating table PP_JOB_FILE_WATCHER');

			EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_JOB_FILE_WATCHER"
								(
									"JOB_FILE_WATCHER_ID" number(16,0) not null,
									"TIME_STAMP" date,
									"USER_ID" varchar2(18),
									"TITLE" varchar2(50) not null,
									"DESCRIPTION" varchar2(250),
									"FILE_PATH" varchar2(250) not null,
									"FILE_NAME" varchar2(250) not null,
									"FILE_TIMESTAMP" date,
									"JOB_WORKFLOW_ID" number(16,0),
									"IS_INACTIVE" char(1) not null,
									"IS_DELETED" char(1) not null,
									"CREATED" date not null,
									"CREATED_BY" varchar2(18) not null,
									"DATA" CLOB,
									CONSTRAINT PP_JOB_FILE_WATCHER PRIMARY KEY ("JOB_FILE_WATCHER_ID"),
									CONSTRAINT FK_PP_JOB_FILE_WATCHER_WF FOREIGN KEY ("JOB_WORKFLOW_ID") REFERENCES "PP_JOB_WORKFLOW" ("JOB_WORKFLOW_ID")
								) TABLESPACE PWRPLANT';

			EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_FILE_WATCHER" IS ''Keeps track of file paths and names to look out for. If a new file appears, it will kick off the specified Job Flow.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."JOB_FILE_WATCHER_ID" is ''Primary record identifier''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."TIME_STAMP" is ''Timestamp of record''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."USER_ID" is ''Standard system-assigned user id used for audit purposes''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."TITLE" is ''The title of this file watcher''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."DESCRIPTION" is ''A description of this file watcher''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."FILE_PATH" is ''The directory to monitor for the specified file name''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."FILE_NAME" is ''The file name or pattern (using %) to look for''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."FILE_TIMESTAMP" is ''The timestamp of the last file that was processed''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."JOB_WORKFLOW_ID" is ''The job flow that will be started when a new file is found''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."IS_INACTIVE" is ''Whether or not this file watcher is inactive''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."IS_DELETED" is ''Whether or not this file watcher has been deleted. Deleted file watchers will be hidden from the UI and used only for audit purposes.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."CREATED" is ''The user that created this File Watcher''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."CREATED_BY" is ''The time this File Watcher was created''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER"."DATA" is ''Key-Value pairs to store extra information, like when to send email results''';

			EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_FILE_WATCHER FOR PWRPLANT.PP_JOB_FILE_WATCHER';

			EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_FILE_WATCHER TO pwrplant_role_dev';

			EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_FILE_WATCHER_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';

			EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_FILE_WATCHER
								BEFORE INSERT ON PP_JOB_FILE_WATCHER
								FOR EACH ROW
								BEGIN
									:new.JOB_FILE_WATCHER_ID := PP_JOB_FILE_WATCHER_SEQ.nextval;
								END;';

		end;
	end if;

	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_JOB_FILE_WATCHER_HISTORY','PWRPLANT');

	if doesTableExist = 0 then
        begin
			dbms_output.put_line('Creating table PP_JOB_FILE_WATCHER_HISTORY');

			EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_JOB_FILE_WATCHER_HISTORY"
								(
									"JOB_FILE_WATCHER_ID" number(16,0) not null,
									"JOB_SCHEDULE_ID" number(16,0) not null,
									"FILE_TIMESTAMP" date not null,
									"FILE_NAME" varchar2(250) not null,
									"PROCESSED_DATE" date not null,
									CONSTRAINT PP_JOB_FILE_WATCHER_HISTORY PRIMARY KEY ("JOB_FILE_WATCHER_ID", "JOB_SCHEDULE_ID"),
									CONSTRAINT FK_PP_JOB_FILE_WATCHER_HIST_FW FOREIGN KEY ("JOB_FILE_WATCHER_ID") REFERENCES "PP_JOB_FILE_WATCHER" ("JOB_FILE_WATCHER_ID"),
									CONSTRAINT FK_PP_JOB_FILE_WATCHER_HIST_SC FOREIGN KEY ("JOB_SCHEDULE_ID") REFERENCES "PP_JOB_SCHEDULE" ("JOB_SCHEDULE_ID")
								) TABLESPACE PWRPLANT';

			EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_FILE_WATCHER_HISTORY" IS ''A record of all scheduled job flows that were created as a result of a File Watcher.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER_HISTORY"."JOB_FILE_WATCHER_ID" is ''The file watcher that scheduled this job flow''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER_HISTORY"."JOB_SCHEDULE_ID" is ''The scheduled job flow that was created''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER_HISTORY"."FILE_TIMESTAMP" is ''The timestamp of the file processed''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER_HISTORY"."FILE_NAME" is ''The name of the file''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_FILE_WATCHER_HISTORY"."PROCESSED_DATE" is ''The time this schedule was created''';

			EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_FILE_WATCHER_HISTORY FOR PWRPLANT.PP_JOB_FILE_WATCHER_HISTORY';
			EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_FILE_WATCHER_HISTORY TO pwrplant_role_dev';

		end;
	end if;
end;

/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2648, 0, 2015, 2, 0, 0, 043986, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043986_jobserver_filewatcher_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;