/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048626_lessor_02_create_payment_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/18/2017 Anand R         create lessor payment dates table
||============================================================================
*/

create table LSR_ILR_PAYMENT_TERM
(
  ilr_id                      NUMBER(22,0) not null,
  revision                    NUMBER(22,0) not null,
  payment_term_id             NUMBER(22,0) not null,
  time_stamp                  DATE,
  user_id                     VARCHAR2(18),
  payment_term_type_id        NUMBER(22,0) not null,
  payment_term_date           DATE not null,
  payment_freq_id             NUMBER(22) not null,
  number_of_terms             NUMBER(22,0),
  payment_amount              NUMBER(22,2),
  est_executory_cost          NUMBER(22,2),
  contingent_amount           NUMBER(22,2) default 0,
  c_bucket_1                  NUMBER(22,2),
  c_bucket_2                  NUMBER(22,2),
  c_bucket_3                  NUMBER(22,2),
  c_bucket_4                  NUMBER(22,2),
  c_bucket_5                  NUMBER(22,2),
  c_bucket_6                  NUMBER(22,2),
  c_bucket_7                  NUMBER(22,2),
  c_bucket_8                  NUMBER(22,2),
  c_bucket_9                  NUMBER(22,2),
  c_bucket_10                 NUMBER(22,2),
  e_bucket_1                  NUMBER(22,2),
  e_bucket_2                  NUMBER(22,2),
  e_bucket_3                  NUMBER(22,2),
  e_bucket_4                  NUMBER(22,2),
  e_bucket_5                  NUMBER(22,2),
  e_bucket_6                  NUMBER(22,2),
  e_bucket_7                  NUMBER(22,2),
  e_bucket_8                  NUMBER(22,2),
  e_bucket_9                  NUMBER(22,2),
  e_bucket_10                 NUMBER(22,2)
);

comment on table LSR_ILR_PAYMENT_TERM  is '(O)  [06] The Lessor ILR payment term table contains the payment parameters for the associated ILR.  An ILR will typically contain several records in the payment term table spanning all interim, normal, and extended payment periods.';

comment on column LSR_ILR_PAYMENT_TERM.ilr_id  is 'System-assigned identifier of a particular Individual Lease Record.';
comment on column LSR_ILR_PAYMENT_TERM.revision  is 'The revision.';
comment on column LSR_ILR_PAYMENT_TERM.payment_term_id  is 'Sequence number used to maintain uniqueness among the ILR payment term records.';
comment on column LSR_ILR_PAYMENT_TERM.time_stamp  is 'Standard System-assigned timestamp used for audit purposes.';
comment on column LSR_ILR_PAYMENT_TERM.user_id  is 'Standard System-assigned user id used for audit purposes.';
comment on column LSR_ILR_PAYMENT_TERM.payment_term_type_id  is 'References a payment term type from the fixed table, ls_payment_term_type.  Payment term types include the following values:  Annual, Semi-annual, Monthly, Quarterly, or Actual.';
comment on column LSR_ILR_PAYMENT_TERM.payment_term_date  is 'Records date related to the payment term.';
comment on column LSR_ILR_PAYMENT_TERM.payment_freq_id  is 'System-assigned identifier for one of the following values:  Annual, Semi-annual, Monthly, Quarterly, or Actual.';
comment on column LSR_ILR_PAYMENT_TERM.number_of_terms  is 'Indicates a number of successive months that possess this record''s [type - frequency - date] combination.';
comment on column LSR_ILR_PAYMENT_TERM.payment_amount  is 'Represents the payment amount for the particular term.  For variable leases, the Paid Amount is not used due to dynamic interest rates.';
comment on column LSR_ILR_PAYMENT_TERM.est_executory_cost  is 'Records estimated payments to be made for maintenance, taxes, and insurance that is included in the lease payment.';
comment on column LSR_ILR_PAYMENT_TERM.contingent_amount  is 'Records estimated payments to be made for contingent rent that is included in the lease payment.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_1  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_2  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_3  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_4  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_5  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_6  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_7  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_8  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_9  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.c_bucket_10  is 'Payment Amount for Contingent Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_1  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_2  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_3  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_4  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_5  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_6  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_7  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_8  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_9  is 'Payment Amount for Executory Bucket.';
comment on column LSR_ILR_PAYMENT_TERM.e_bucket_10  is 'Payment Amount for Executory Bucket.';

alter table LSR_ILR_PAYMENT_TERM
  add constraint PK_LSR_ILR_PAYMENT_TERM primary key (ILR_ID, REVISION, PAYMENT_TERM_ID)
  using index 
  tablespace PWRPLANT_IDX;
  
alter table LSR_ILR_PAYMENT_TERM
  add constraint FK_PYMTTERMS_LSR_ILR foreign key (ILR_ID, REVISION)
  references LSR_ILR_APPROVAL (ILR_ID, REVISION);
  
alter table LSR_ILR_PAYMENT_TERM
  add constraint R_LSR_ILR_PAYMENT_TERM1 foreign key (PAYMENT_TERM_TYPE_ID)
  references LS_PAYMENT_TERM_TYPE (PAYMENT_TERM_TYPE_ID);
  
alter table LSR_ILR_PAYMENT_TERM
  add constraint R_LSR_ILR_PAYMENT_TERM2 foreign key (PAYMENT_FREQ_ID)
  references LS_PAYMENT_FREQ (PAYMENT_FREQ_ID);

  
create table LSR_ILR_PAYMENT_TERM_VAR_PAY
(
  ilr_id                    NUMBER(22) not null,
  revision                  NUMBER(22) not null,
  payment_term_id           NUMBER(22) not null,
  receivable_type           VARCHAR2(35) default 'Contingent',
  bucket_number             NUMBER(2) not null,
  variable_payment_id       NUMBER(22),
  incl_in_initial_measure   NUMBER(22) not null,
  initial_var_payment_value NUMBER(22,2),
  run_order                 NUMBER(3),
  user_id                   VARCHAR2(18),
  time_stamp                DATE
);

comment on table LSR_ILR_PAYMENT_TERM_VAR_PAY  is 'Lease table that stores the association between payment terms and variable payments';

comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.ilr_id  is 'System assigned identifier for an Individual Lease Record or ILR';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.revision  is 'System assigned identifier for a revision of an ILR';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.payment_term_id  is 'System assigned identifier for a payment term on an ILR';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.receivable_type  is 'System assigned value for rent type used in Lease';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.bucket_number  is 'System assigned identifier for a bucket used in Lease';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.variable_payment_id  is 'System assigned identifier for a variable payment';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.incl_in_initial_measure  is 'Include variable payments in initial schedule measurement, 1=Yes, 0=No';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.initial_var_payment_value  is 'Location to store initially calculated value for a variable payment included in initial measure';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.run_order  is 'A value denoting the order in which variable payments associated to the given ILR should be evaluated.';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.user_id  is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LSR_ILR_PAYMENT_TERM_VAR_PAY.time_stamp  is 'Standard system-assigned user id used for audit purposes.';

alter table LSR_ILR_PAYMENT_TERM_VAR_PAY
  add constraint LSR_ILR_PAY_TERM_VAR_PAYMNT_PK primary key (ILR_ID, REVISION, PAYMENT_TERM_ID, BUCKET_NUMBER)
  using index 
  tablespace PWRPLANT_IDX;
  
alter table LSR_ILR_PAYMENT_TERM_VAR_PAY
  add constraint LSR_ILR_PAY_TERM_VAR_PAY_FK1 foreign key (ILR_ID, REVISION, PAYMENT_TERM_ID)
  references LSR_ILR_PAYMENT_TERM (ILR_ID, REVISION, PAYMENT_TERM_ID)
  deferrable;
  
alter table LSR_ILR_PAYMENT_TERM_VAR_PAY
  add constraint LSR_ILR_PAY_TERM_VAR_PAY_FK2 foreign key (RECEIVABLE_TYPE, BUCKET_NUMBER)
  references LSR_RECEIVABLE_BUCKET_ADMIN (RECEIVABLE_TYPE, BUCKET_NUMBER);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3718, 0, 2017, 1, 0, 0, 48626, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048626_lessor_02_create_payment_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  
  
