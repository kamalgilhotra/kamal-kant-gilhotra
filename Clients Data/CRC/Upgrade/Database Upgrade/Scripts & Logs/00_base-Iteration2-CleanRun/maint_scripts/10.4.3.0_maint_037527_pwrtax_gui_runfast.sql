/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037527_pwrtax_gui_runfast.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 04/08/2014 Andrew Scott        Base GUI workspaces for Run Fast
||============================================================================
*/

----
----  set up the three new workspaces to be accessible in powertax gui
----
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_process', 'Processing', 'uo_tax_depr_processing_wskp', 'Processing');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'depr_process'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_process';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'deferred_process', 'Processing', 'uo_tax_def_processing_wskp', 'Processing');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'deferred_process'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'deferred_process';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'forecast_process', 'Processing', 'uo_tax_fcst_processing_wskp', 'Processing');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'forecast_process'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'forecast_process';

----
---- setup the dynamic filters needed for these workspaces.
---- values *SHOULD* be hard-coded here--do *NOT* use max+1.
---- ids used below were free in master db as of 4/7/2014
----

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (60, 'Powertax - Depr Processing', 'uo_ppbase_tab_filter_dynamic');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (61, 'Powertax - Dfit Processing', 'uo_ppbase_tab_filter_dynamic');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (62, 'Powertax - Frcst Processing', 'uo_ppbase_tab_filter_dynamic');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (134, 'Depr-Company', 'dw', '', 'dw_tax_company_by_version_filter', 'company_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (135, 'Depr-Tax Book', 'dw', '', 'dw_tax_book_filter', 'tax_book_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (136, 'Depr-Vintage', 'dw', '', 'dw_tax_vintage_by_version_filter', 'vintage_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (137, 'Depr-Tax Class', 'dw', '', 'dw_tax_class_by_version_filter', 'tax_class_id', 'description', 'N', 0, 0, 1, 0,
    0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (138, 'DFIT-Company', 'dw', '', 'dw_tax_company_by_version_filter', 'company_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (139, 'DFIT-Deferred Type', 'dw', '', 'dw_tax_normalization_schema_filter', 'normalization_id', 'description', 'N',
    0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (140, 'DFIT-Vintage', 'dw', '', 'dw_tax_vintage_by_version_filter', 'vintage_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (141, 'DFIT-Tax Class', 'dw', '', 'dw_tax_class_by_version_filter', 'tax_class_id', 'description', 'N', 0, 0, 1, 0,
    0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (142, 'FCST-Company', 'dw', '', 'dw_tax_company_by_version_filter', 'company_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (143, 'FCST-Tax Book', 'dw', '', 'dw_tax_book_filter', 'tax_book_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (144, 'FCST-Vintage', 'dw', '', 'dw_tax_vintage_by_version_filter', 'vintage_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (145, 'FCST-Tax Class', 'dw', '', 'dw_tax_class_by_version_filter', 'tax_class_id', 'description', 'N', 0, 0, 1, 0,
    0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (146, 'FCST-Forecast Version', 'dw', '', 'dw_tax_fcst_version_by_version_filter', 'tax_forecast_version_id',
    'description', 'N', 1, 1, 1, 0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   select 60, FILTER_ID from PP_DYNAMIC_FILTER where FILTER_ID between 134 and 137;

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   select 61, FILTER_ID from PP_DYNAMIC_FILTER where FILTER_ID between 138 and 141;

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   select 62, FILTER_ID from PP_DYNAMIC_FILTER where FILTER_ID between 142 and 146;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1114, 0, 10, 4, 3, 0, 37527, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037527_pwrtax_gui_runfast.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;