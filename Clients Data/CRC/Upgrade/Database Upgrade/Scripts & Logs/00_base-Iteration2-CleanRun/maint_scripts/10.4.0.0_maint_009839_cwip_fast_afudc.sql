/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009839_cwip_fast_afudc.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version   Date     Revised By    Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0  10/30/2012  Sunjin Cone    Point Release
||============================================================================
*/

create global temporary table AFUDC_CALC_WO_LIST_TEMP
(
 WORK_ORDER_ID            number(22,0),
 COMPANY_ID               number(22,0),
 AFUDC_TYPE_ID            number(22,0),
 AFUDC_ELIG_AMOUNT        number(22,2),
 CPI_ELIG_AMOUNT          number(22,2),
 AFUDC_START_DATE         date,
 AFUDC_STOP_DATE          date,
 IN_SERVICE_DATE          date,
 WO_STATUS_ID             number(22,0),
 DEPARTMENT_ID            number(22,0),
 BUS_SEGMENT_ID           number(22,0),
 CWIP_GL_ACCOUNT          number(22,0),
 ELIGIBLE_FOR_AFUDC       number(22,0),
 ELIGIBLE_FOR_CPI         number(22,0),
 ORIGINAL_IN_SERVICE_DATE date,
 ORIG_ISD_MONTH_NUMBER    number(22,0),
 EST_START_DATE           date,
 EST_COMPLETE_DATE        date,
 EST_IN_SERVICE_DATE      date,
 FUNDING_WO_ID            number(22,0),
 CURRENT_REVISION         number(22,0),
 EQUITY_COST_ELEMENT      number(22,0),
 DEBT_COST_ELEMENT        number(22,0),
 CPI_COST_ELEMENT         number(22,0),
 MIN_EST_AMT_AFUDC        number(22,2),
 MIN_EST_AMT_CPI          number(22,2),
 MIN_EST_MOS_AFUDC        number(22,0),
 MIN_EST_MOS_CPI          number(22,0),
 ALLOW_NEG_AFUDC          number(22,0),
 ALLOW_NEG_CPI            number(22,0),
 PERCENT_EST_FOR_CPI      number(22,8),
 IGNORE_CURRENT_MONTH     number(22,2),
 IGNORE_CURRENT_MONTH_CPI number(22,2),
 IN_SERVICE_OPTION        varchar2(35),
 EST_REQUIRED             varchar2(35),
 SKIP_TRIGGER             number(22,0),
 CPI_SKIP_TRIGGER         number(22,0),
 IDLE_AFUDC_MONTHS        number(22,0),
 IDLE_CPI_MONTHS          number(22,0),
 IDLE_EXP_TYPE_ADDITION   number(22,0),
 CURRENT_MONTH_ONLY       number(22,0),
 CPI_DE_MINIMIS_YN        number(1,0) default 0,
 CPI_DE_MINIMIS_WO_OR_FP  varchar2(35),
 CPI_DE_MINIMIS_AMOUNT    number(22,2),
 CPI_DE_MINIMIS_MAX_DAYS  number(22,0),
 EQUITY_CHARGE_TYPE       number(22,0),
 DEBT_CHARGE_TYPE         number(22,0),
 CPI_CHARGE_TYPE          number(22,0),
 MAX_REVISION             number(22,0),
 HAS_PRIOR_AFC            number(22,0),
 HAS_PRIOR_CPI            number(22,0),
 MIN_CHARGE_MONUM         number(22,0),
 AFUDC_STATUS_ID          number(22,0),
 CPI_STATUS_ID            number(22,0),
 WO_EST_AMOUNT            number(22,2),
 AFUDC_ACT_AMOUNT         number(22,2),
 CPI_ACT_AMOUNT           number(22,2),
 CPI_DEMIN_EST_DAYS       number(22,0),
 CPI_DEMIN_BUD_AMOUNT     number(22,2),
 STOP_MONTH_RATE_ADJ      number(22,8),
 DEBT_RATE                number(22,8),
 EQUITY_RATE              number(22,8),
 CPI_RATE                 number(22,8),
 DEBT_PAST_ISD_AMOUNT     number(22,2),
 EQUITY_PAST_ISD_AMOUNT   number(22,2),
 CPI_PAST_ISD_AMOUNT      number(22,2),
 DEBT_ISD_AMOUNT          number(22,2),
 EQUITY_ISD_AMOUNT        number(22,2),
 CPI_ISD_AMOUNT           number(22,2),
 AFUDC_BASE_ADJ           number(22,2),
 AFUDC_CALC_MISSING       number(22,0),
 LAST_COMP_AFC_MN         number(22,0),
 LAST_COMP_CPI_MN         number(22,0)
) on commit preserve rows;


create global temporary table CAP_INT_ADJUST_TEMP
(
 PARENT_WO_ID         number(22,0),
 CHILD_TOTAL_DEBT     number(22,2),
 CAP_INT_ADJUST_RATIO number(22,8)
) on commit preserve rows;

update AFUDC_STATUS
   set DESCRIPTION = 'Failed Minimum Estimate and Time Test, No AFUDC Calculated'
 where AFUDC_STATUS_ID = 10;

update AFUDC_STATUS
   set DESCRIPTION = 'Failed Minimum Estimate and Time Test, No CPI Calculated'
 where AFUDC_STATUS_ID = 11;

insert into AFUDC_STATUS
   (AFUDC_STATUS_ID, DESCRIPTION)
values
   (24, 'Current Month Afudc Stop Date Work Order');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (241, 0, 10, 4, 0, 0, 9839, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_009839_cwip_fast_afudc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
