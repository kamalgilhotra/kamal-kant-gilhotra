/*
 ||============================================================================
 || Application: acqaider_app
 || File Name: maint_043663_sys_acqa_08_depr_imp2_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2015.1.0.0 04/17/2015 A. McKinlay    <More DDL for depr import tool in acqaider app>
 ||============================================================================
 */
 
--
-- table comments for the depr_config_import_stg tables
--

comment on table depr_config_import_stg is 'Depr Config Import Stg acts as a staging table for all the data loaded into the database from the depr config loader. From here it is then moved to depreciation_method, depr_group, etc.';
comment on table depr_config_import_stg_arc is 'Depr Config Import Stg Arc acts as the archive table for the staging table for all the data loaded into the database from the depr config loader. All loaded batches are archived here';

alter table depr_config_import_stg
drop column depr_group_import_Type_id;

alter table depr_config_import_stg_arc
drop column depr_group_import_Type_id;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2515, 0, 2015, 1, 0, 0, 43663, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043663_sys_acqa_08_depr_imp2_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;