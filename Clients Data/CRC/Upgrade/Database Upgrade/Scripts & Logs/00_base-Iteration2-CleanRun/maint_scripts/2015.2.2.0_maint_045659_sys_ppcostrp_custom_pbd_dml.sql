/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045659_sys_ppcostrp_custom_pbd_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2015.2.2.0 05/03/2016 David Haupt		Adding pp_costrp_interface_custom PBL version
||============================================================================
*/

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'budget_allocations.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_allocations.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_balances.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_balances_bdg.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_batch_derivation.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_batch_derivation_bdg.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_budget_entry_calc_all.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_build_combos.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_build_deriver.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_delete_budget_allocations.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_financial_reports_build.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_flatten_structures.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_man_jrnl_reversals.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_posting.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_posting_bdg.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_sap_trueup.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_sum.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_sum_ab.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cr_to_commitments.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'cwip_charge.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'pp_integration.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'ssp_preview_je.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppcostrp_interface_custom.pbd',
  '2015.2.2.0'
FROM pp_processes
WHERE Lower(Trim(executable_file)) = 'pptocr.exe';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3179, 0, 2015, 2, 2, 0, 045659, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045659_sys_ppcostrp_custom_pbd_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;