/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052545_lessor_01_lsr_remeasurement_jes_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/12/2018 Anand R        PP-52545 Add Lessor Re measurement JEs
||============================================================================
*/

/* Add 4 trans types for lessor ILR re measurement and assign JE method 1 as default */

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4070, '4070 - Receivable remeasurement Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4071, '4071 - Receivable remeasurement Credit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4072, '4072 - LT Receivable Remeasurement Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4073, '4073 - Unguar_Residual Remeasurement Debit');

insert into je_method_trans_type(je_method_id, trans_type)    
select 1 je_method_id, trans_type 
from (    
  select trans_type 
  from je_trans_type    
  where trans_type in (4070, 4071, 4072, 4073)
);

/* Add new GL_JE_CODE   */

insert into standard_journal_entries (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
values ( (select nvl((max(je_id) + 1),1) from standard_journal_entries), 'LSR Remeasurement', 'Lessor Remeasurement', 'Lessor Remeasurement', 'Lessor Remeasurement');

insert into gl_je_control (PROCESS_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN, CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
values ('LSR Remeasurement', (select max(je_id) from standard_journal_entries ), null, null, 'NONE', 'NONE', 'NONE', 'NONE', null, null, null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(11842, 0, 2018, 1, 0, 0, 52545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052545_lessor_01_lsr_remeasurement_jes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 