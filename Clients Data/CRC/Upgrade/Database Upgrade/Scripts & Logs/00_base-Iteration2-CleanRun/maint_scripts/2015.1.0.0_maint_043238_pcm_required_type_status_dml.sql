/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043238_pcm_required_type_status_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/11/2015 Ryan Oliveria    Fix Required Fields on WO Info
||============================================================================
*/

--Delete the WO only version of these fields
delete from PP_REQUIRED_TABLE_COLUMN where ID in (1004, 1006);

--Change the FP only version to work for all cases
update PP_REQUIRED_TABLE_COLUMN set DESCRIPTION = 'Status', REQUIRED_COLUMN_EXPRESSION = null
where ID = 1003;

update PP_REQUIRED_TABLE_COLUMN set DESCRIPTION = 'WO/FP Type', REQUIRED_COLUMN_EXPRESSION = null
where ID = 1005;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2370, 0, 2015, 1, 0, 0, 043238, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043238_pcm_required_type_status_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;