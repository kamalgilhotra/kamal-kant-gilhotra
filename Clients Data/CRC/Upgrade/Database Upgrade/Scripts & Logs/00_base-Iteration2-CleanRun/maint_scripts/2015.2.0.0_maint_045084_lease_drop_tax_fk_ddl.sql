/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045084_lease_drop_tax_fk_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 10/14/2015 Will Davis     Drop an fkey from ls_asset_tax_map
||============================================================================
*/

alter table ls_asset_tax_map
drop constraint fk_ls_asset_tax_map1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2921, 0, 2015, 2, 0, 0, 45084, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045084_lease_drop_tax_fk_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
