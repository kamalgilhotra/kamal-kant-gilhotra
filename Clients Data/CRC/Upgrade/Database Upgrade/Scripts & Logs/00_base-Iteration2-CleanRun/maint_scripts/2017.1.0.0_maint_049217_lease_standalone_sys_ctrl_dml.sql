/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049217_lease_standalone_sys_ctrl_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

INSERT INTO PP_SYSTEM_CONTROL
            (control_id,
             control_name,
             control_value,
             description,
             long_description,
             company_id)
SELECT pwrplant1.NEXTVAL,
       'Lease Standalone',
       'No',
       'dw_yes_no;1',
       'Determine if the system is a Lease Standalone Product. Default is "No", and closing activities will run through normal CPR close.',
       -1
FROM   DUAL
WHERE  NOT EXISTS
           ( SELECT 1
             FROM   PP_SYSTEM_CONTROL
             WHERE  control_name = 'Lease Standalone' ); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4013, 0, 2017, 1, 0, 0, 49217, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049217_lease_standalone_sys_ctrl_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;