/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040386_pwrtax_remove_report_625.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/22/2014 Anand Rajashekar    Delete report 625
||============================================================================
*/

--Report 625, 401004. This is an obsolete report

delete from PP_REPORTS where REPORT_ID = 401004;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1542, 0, 10, 4, 3, 0, 40386, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040386_pwrtax_remove_report_625.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
