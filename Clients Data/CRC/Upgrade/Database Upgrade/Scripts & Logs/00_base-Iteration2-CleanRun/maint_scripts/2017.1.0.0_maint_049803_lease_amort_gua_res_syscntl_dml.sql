/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_049803_lease_amort_gua_res_syscntl_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2017.1.0 11/17/2017 Aaron Smith    System control missed for PP-44017
 ||============================================================================
 */

insert into pp_system_control_company (control_id, control_name, control_value, company_id, long_description, description)
select (SELECT Max(control_id) FROM pp_system_control_company) + 1, 'Lease Amort to Guaranteed Residual', 'Yes', -1,
'Determines if guaranteed residual amount should be included in capital cost', 'dw_yes_no;1'
from dual 
where not exists 
(select 1 from pp_system_control_company where lower(trim(control_name)) = lower('Lease Amort to Guaranteed Residual') and company_id=-1 );
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4025, 0, 2017, 1, 0, 0, 49803, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049803_lease_amort_gua_res_syscntl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;