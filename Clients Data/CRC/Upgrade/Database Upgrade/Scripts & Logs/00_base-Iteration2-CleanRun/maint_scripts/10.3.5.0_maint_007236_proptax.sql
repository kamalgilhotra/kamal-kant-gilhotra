/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_007236_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    04/27/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Maint 7236
--
-- Create a reserve factor import.
--

insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID ) values ( 28, sysdate, user, 'Add : Reserve Factor Percents', 'Import New Reserve Factor Percents', 'pt_import_rsvfctr_pcts', 'pt_import_rsvfctr_pcts_archive', null );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 28, 'reserve_factor_id', sysdate, user, 'Reserve Factor', 'reserve_factor_xlate', 1, 1, 'number(22,0)', 'pt_reserve_factors' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 28, 'age', sysdate, user, 'Age', '', 1, 1, 'number(22,0)', '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 28, 'tax_year', sysdate, user, 'Tax Year', 'tax_year_xlate', 1, 1, 'number(22,0)', 'property_tax_year' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 28, 'reserve_factor_percent', sysdate, user, 'Reserve Factor Percent', '', 1, 1, 'number(22,8)', '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 28, 'tax_district_id', sysdate, user, 'Tax District', 'tax_district_xlate', 0, 3, 'number(22,0)', 'prop_tax_district' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 28, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 28, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county' );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED ) values ( 160, sysdate, user, 'PT Reserve Factors.Description', 'The passed in value corresponds to the PT Reserve Factors: Description field.  Translate to the Reserve Factor ID using the Description column on the PT Reserve Factors table.', 'reserve_factor_id', '( select rf.reserve_factor_id from pt_reserve_factors rf where upper( trim( <importfield> ) ) = upper( trim( rf.description ) ) )', 0, 0 );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED ) values ( 161, sysdate, user, 'PT Reserve Factors.Long Description', 'The passed in value corresponds to the PT Reserve Factors: Long Description field.  Translate to the Reserve Factor ID using the Long Description column on the PT Reserve Factors table.', 'reserve_factor_id', '( select rf.reserve_factor_id from pt_reserve_factors rf where upper( trim( <importfield> ) ) = upper( trim( rf.long_description ) ) )', 0, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_year', 35, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_year', 36, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_year', 37, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_year', 38, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 79, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 80, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 81, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 82, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 83, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 84, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 85, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 86, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 87, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 105, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 106, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'tax_district_id', 107, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'reserve_factor_id', 160, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 28, 'reserve_factor_id', 161, sysdate, user );

create table PT_IMPORT_RSVFCTR_PCTS
(
 IMPORT_RUN_ID          number(22,0) not null,
 LINE_ID                number(22,0) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 TAX_YEAR_XLATE         varchar2(254),
 TAX_YEAR               number(22,0),
 RESERVE_FACTOR_XLATE   varchar2(254),
 RESERVE_FACTOR_ID      number(22,0),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 TAX_DISTRICT_XLATE     varchar2(254),
 TAX_DISTRICT_ID        number(22,0),
 AGE                    varchar2(35),
 RESERVE_FACTOR_PERCENT varchar2(35),
 ERROR_MESSAGE          varchar2(4000)
);

alter table PT_IMPORT_RSVFCTR_PCTS
   add constraint PT_IMPORT_RF_PCTS_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PT_IMPORT_RSVFCTR_PCTS
   add constraint PT_IMPORT_RF_PCTS_RUN_FK
       foreign key (IMPORT_RUN_ID) references PT_IMPORT_RUN;

create table PT_IMPORT_RSVFCTR_PCTS_ARCHIVE
(
 IMPORT_RUN_ID          number(22,0) not null,
 LINE_ID                number(22,0) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 TAX_YEAR_XLATE         varchar2(254),
 TAX_YEAR               number(22,0),
 RESERVE_FACTOR_XLATE   varchar2(254),
 RESERVE_FACTOR_ID      number(22,0),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 TAX_DISTRICT_XLATE     varchar2(254),
 TAX_DISTRICT_ID        number(22,0),
 AGE                    varchar2(35),
 RESERVE_FACTOR_PERCENT varchar2(35)
);

alter table pt_import_rsvfctr_pcts_archive
   add constraint PT_IMPORT_RF_PCTS_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PT_IMPORT_RSVFCTR_PCTS_ARCHIVE
   add constraint PT_IMPORT_RF_PCTS_ARC_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PT_IMPORT_RUN;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (136, 0, 10, 3, 5, 0, 7236, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_007236_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
