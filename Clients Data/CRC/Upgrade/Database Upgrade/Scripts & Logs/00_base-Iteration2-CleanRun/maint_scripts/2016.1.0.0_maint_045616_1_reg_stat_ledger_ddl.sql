/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045616_1_reg_stat_ledger_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2016.1.0.0 04/04/2016 Sarah Byers	  Create RMS Statistic Ledger Tables
||============================================================================
*/

-- REG_STATISTIC_FORM_ID
create table reg_statistic_form_id (
stat_formula_id number(22,0) not null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_statistic_form_id add (
constraint pk_reg_statistic_form_id primary key (stat_formula_id) using index tablespace pwrplant_idx);

comment on table reg_statistic_form_id is 'The Reg Statistic Form Id table is used to relate Statistic Formulas to Regulatory Statistics.';
comment on column reg_statistic_form_id.stat_formula_id is 'System assigned identifier of a Regulatory Statistic Formula.';
comment on column reg_statistic_form_id.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_statistic_form_id.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_STATISTIC
create table reg_statistic (
reg_stat_id number(22,0) not null,
description varchar2(100) not null,
long_description varchar2(2000) null,
unit_of_measure varchar2(70) null,
stat_used_for number(1,0) not null,
stat_formula_id number(22,0) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_statistic add (
constraint pk_reg_statistic primary key (reg_stat_id) using index tablespace pwrplant_idx);

alter table reg_statistic
add constraint fk_reg_statistic1
foreign key (stat_formula_id)
references reg_statistic_form_id (stat_formula_id);

comment on table reg_statistic is 'The Reg Statistic Table stores the definition of Regulatory Statistics.';
comment on column reg_statistic.reg_stat_id is 'System assigned identifier of a Regulatory Statistic.';
comment on column reg_statistic.description is 'Description of the Regulatory Statistic.';
comment on column reg_statistic.long_description is 'Long Description of the Regulatory Statistic.';
comment on column reg_statistic.unit_of_measure is 'Unit of Measure for the Regulatory Statistic, e.g. kW, USD';
comment on column reg_statistic.stat_used_for is 'Determines whether the Regulatory Statistic is used for Historic and/or Forecast purposes.';
comment on column reg_statistic.stat_formula_id is 'System assigned identifier of a Regulatory Statistic Formula.';
comment on column reg_statistic.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_statistic.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_STATISTIC_FORMULA
create table reg_statistic_formula (
stat_formula_id number(22,0) not null,
row_order number(22,0) not null,
reg_stat_id number(22,0) null,
constant number(22,8) null,
left_paren varchar2(1) null,
operation varchar2(1) null,
right_paren varchar2(1) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_statistic_formula add (
constraint pk_reg_statistic_formula primary key (stat_formula_id, row_order) using index tablespace pwrplant_idx);

alter table reg_statistic_formula
add constraint fk_reg_statistic_formula1
foreign key (stat_formula_id)
references reg_statistic_form_id (stat_formula_id);

alter table reg_statistic_formula
add constraint fk_reg_statistic_formula2
foreign key (reg_stat_id)
references reg_statistic (reg_stat_id);

comment on table reg_statistic_formula is 'The Reg Statistic Formula table stores formulas of Regulatory Statistics used to create Regulatory Statistics.';
comment on column reg_statistic_formula.stat_formula_id is 'System assigned identifier of a Regulatory Statistic Formula.';
comment on column reg_statistic_formula.row_order is 'Order of rows in the statistic formula.';
comment on column reg_statistic_formula.reg_stat_id is 'System assigned identifier of a Regulatory Statistic.';
comment on column reg_statistic_formula.constant is 'Constant value used in a Statistic Formula.';
comment on column reg_statistic_formula.left_paren is '(';
comment on column reg_statistic_formula.operation is '+,-,*,/';
comment on column reg_statistic_formula.right_paren is ')';
comment on column reg_statistic_formula.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_statistic_formula.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_STATISTIC_LEDGER
create table reg_statistic_ledger (
historic_version_id number(22,0) not null,
forecast_version_id number(22,0) not null,
reg_company_id number(22,0) not null,
reg_stat_id number(22,0) not null,
month_number number(6,0) not null,
stat_value number(22,16) not null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_statistic_ledger add (
constraint pk_reg_statistic_ledger primary key (historic_version_id, forecast_version_id, reg_company_id, reg_stat_id, month_number)
using index tablespace pwrplant_idx);

alter table reg_statistic_ledger
add constraint fk_reg_statistic_ledger1
foreign key (historic_version_id)
references reg_historic_version (historic_version_id);

alter table reg_statistic_ledger
add constraint fk_reg_statistic_ledger2
foreign key (forecast_version_id)
references reg_forecast_version (forecast_version_id);

alter table reg_statistic_ledger
add constraint fk_reg_statistic_ledger3
foreign key (reg_company_id)
references reg_company (reg_company_id);

alter table reg_statistic_ledger
add constraint fk_reg_statistic_ledger4
foreign key (reg_stat_id)
references reg_statistic (reg_stat_id);

comment on table reg_statistic_ledger is 'The Reg Statistic Ledger Table contains monthly statistical values related to Regulatory Historic and/or Forecast Ledgers.';
comment on column reg_statistic_ledger.historic_version_id is 'System assigned identifier of a Regulatory Historic Ledger.';
comment on column reg_statistic_ledger.forecast_version_id is 'System assigned identifier of a Regulatory Forecast Ledger.';
comment on column reg_statistic_ledger.reg_company_id is 'System assigned identifier of a Regulatory Company.';
comment on column reg_statistic_ledger.reg_stat_id is 'System assigned identifier of a Regulatory Statistic.';
comment on column reg_statistic_ledger.month_number is 'The month associated with the Regulatory Statistic Value; format YYYYMM.';
comment on column reg_statistic_ledger.stat_value is 'The Regulatory Statistic value.';
comment on column reg_statistic_ledger.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_statistic_ledger.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_ACCT_STATISTIC
create table reg_acct_statistic (
reg_acct_stat_id number(22,0) not null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_acct_statistic add (
constraint pk_reg_acct_statistic primary key (reg_acct_stat_id) using index tablespace pwrplant_idx);

comment on table reg_acct_statistic is 'The Reg Acct Statistic table is used to relate Statistic Formulas to Regulatory Accounts.';
comment on column reg_acct_statistic.reg_acct_stat_id is 'System assigned identifier of a Regulatory Account Statistic Formula.';
comment on column reg_acct_statistic.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_acct_statistic.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_ACCT_STAT_FORMULA
create table reg_acct_stat_formula (
reg_acct_stat_id number(22,0) not null,
row_order number(22,0) not null,
reg_stat_id number(22,0) null,
reg_acct_id number(22,0) null,
left_paren varchar2(1) null,
operation varchar2(1) null,
right_paren varchar2(1) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_acct_stat_formula add (
constraint pk_reg_acct_stat_formula primary key (reg_acct_stat_id, row_order) using index tablespace pwrplant_idx);

alter table reg_acct_stat_formula
add constraint fk_reg_acct_stat_formula1
foreign key (reg_acct_stat_id)
references reg_acct_statistic (reg_acct_stat_id);

alter table reg_acct_stat_formula
add constraint fk_reg_acct_stat_formula2
foreign key (reg_stat_id)
references reg_statistic (reg_stat_id);

alter table reg_acct_stat_formula
add constraint fk_reg_acct_stat_formula3
foreign key (reg_acct_id)
references reg_acct_master (reg_acct_id);

comment on table reg_acct_stat_formula is 'The Reg Acct Stat Formula table stores formulas of Regulatory Statistics used to create Regulatory Accounts.';
comment on column reg_acct_stat_formula.reg_acct_stat_id is 'System assigned identifier of a Regulatory Account Statistic Formula.';
comment on column reg_acct_stat_formula.row_order is 'Order of rows in the statistic formula.';
comment on column reg_acct_stat_formula.reg_stat_id is 'System assigned identifier of a Regulatory Statistic.';
comment on column reg_acct_stat_formula.reg_acct_id is 'System assigned identifier of a Regulatory Account.';
comment on column reg_acct_stat_formula.left_paren is '(';
comment on column reg_acct_stat_formula.operation is '+,-,*,/';
comment on column reg_acct_stat_formula.right_paren is ')';
comment on column reg_acct_stat_formula.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_acct_stat_formula.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_ACCT_MASTER
alter table reg_acct_master add (reg_acct_stat_id number(22,0) null);

alter table reg_acct_master
add constraint fk_reg_acct_master_stat
foreign key (reg_acct_stat_id)
references reg_acct_statistic (reg_acct_stat_id);

comment on column reg_acct_master.reg_acct_stat_id is 'System assigned identifier of a Regulatory Account Statistic Formula.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3154, 0, 2016, 1, 0, 0, 045616, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045616_1_reg_stat_ledger_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;