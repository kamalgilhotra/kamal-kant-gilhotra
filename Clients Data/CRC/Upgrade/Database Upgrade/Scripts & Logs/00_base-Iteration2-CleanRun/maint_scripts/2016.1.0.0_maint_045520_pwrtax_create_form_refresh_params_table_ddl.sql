/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045520_pwrtax_create_form_refresh_params_table_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 03/15/2016 Charlie Shilling create new table for job parameters and modify PK on tax_plant_recon_form_rates
||============================================================================
*/
CREATE TABLE tax_job_form_refresh_params (
  job_no                NUMBER(22,0) NOT NULL,
  tax_pr_form_id		NUMBER(22,0) NOT NULL,
  refresh_plant			NUMBER(1,0) NOT NULL,
  refresh_depr			NUMBER(1,0) NOT NULL,
  refresh_tax			NUMBER(1,0) NOT NULL,
  time_stamp            DATE         NULL,
  user_id               VARCHAR2(18) NULL
)
/

ALTER TABLE tax_job_form_refresh_params
  ADD CONSTRAINT pk_tax_job_form_ref_parms PRIMARY KEY (
    job_no
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

COMMENT ON TABLE tax_job_form_refresh_params IS '(O)  [09] [10]
The tax_job_form_refresh_params is an internal table used in the plant reconciliation.';

COMMENT ON COLUMN tax_job_form_refresh_params.job_no IS 'The Oracle Job number created by dbms_submit..';
COMMENT ON COLUMN tax_job_form_refresh_params.tax_pr_form_id IS 'The tax_pr_for_id to refresh data for.';
COMMENT ON COLUMN tax_job_form_refresh_params.refresh_plant IS '1 = True - refresh plant data. 0 = False - do not fresh plant data';
COMMENT ON COLUMN tax_job_form_refresh_params.refresh_depr IS '1 = True - refresh depreciation data. 0 = False - do not fresh depreciation data';
COMMENT ON COLUMN tax_job_form_refresh_params.refresh_tax IS '1 = True - refresh tax data. 0 = False - do not fresh tax data';
COMMENT ON COLUMN tax_job_form_refresh_params.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN tax_job_form_refresh_params.user_id IS 'Standard system-assigned user id used for audit purposes.';


ALTER TABLE tax_plant_recon_form_rates
DROP CONSTRAINT tax_plant_recon_form_rates_pk;

ALTER TABLE tax_plant_recon_form_rates
  ADD CONSTRAINT tax_plant_recon_form_rates_pk PRIMARY KEY (
    tax_pr_form_id,
	def_income_tax_rate_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3104, 0, 2016, 1, 0, 0, 45520, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045520_pwrtax_create_form_refresh_params_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;