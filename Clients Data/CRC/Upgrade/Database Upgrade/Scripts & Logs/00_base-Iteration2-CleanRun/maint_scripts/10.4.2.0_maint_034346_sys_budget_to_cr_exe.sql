/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034346_sys_budget_to_cr_exe.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   12/06/2013 Stephen Motter
||============================================================================
*/

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE)
   select max(PROCESS_ID) + 1,
          'Budget To CR',
          'Budget Send To CR / Budget Pull From CR',
          'budget_to_cr.exe'
     from PP_PROCESSES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (789, 0, 10, 4, 2, 0, 34346, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034346_sys_budget_to_cr_exe.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;