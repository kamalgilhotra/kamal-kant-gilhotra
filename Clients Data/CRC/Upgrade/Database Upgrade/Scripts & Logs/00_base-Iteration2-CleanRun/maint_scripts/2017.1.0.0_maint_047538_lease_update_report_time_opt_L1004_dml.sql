/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047538_lease_update_report_time_opt_L1004_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/16/2017 Shane Ward		 Set Lessee - 1004 Time Option to Multi Currency
||============================================================================
*/

update pp_reports set pp_report_time_option_id = 205 where report_number = 'Lessee - 1004';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3502, 0, 2017, 1, 0, 0, 47538, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047538_lease_update_report_time_opt_L1004_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
