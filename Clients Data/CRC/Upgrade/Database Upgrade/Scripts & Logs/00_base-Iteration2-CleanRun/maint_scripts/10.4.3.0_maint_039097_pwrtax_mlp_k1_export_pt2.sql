/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039097_pwrtax_mlp_k1_export_pt2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 08/14/2014 Andrew Scott        MLP K1 Export Workspace : gui menus,
||                                         filters, tables, data
||============================================================================
*/

----
----  set up the new workspace to be accessible in powertax base gui
----

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'mlp_k1_export_gen', 'K1 Export Generation', 'uo_tax_mlp_wksp_k1_export_gen', 'K1 Export Generation');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('powertax', 'mlp_k1_export_gen', 2, 2, 'K1 Export Generation', 'mlp', 'mlp_k1_export_gen', 1);

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'mlp_k1_export_search', 'K1 Export Search', 'uo_tax_mlp_wksp_k1_export_search', 'K1 Export Search');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('powertax', 'mlp_k1_export_search', 2, 3, 'K1 Export Search', 'mlp', 'mlp_k1_export_search', 1);

----
----  Set up the dynamic filters
----
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (78, TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'PowerTax - MLP K1 Exp Generate', null,
    'uo_ppbase_tab_filter_dynamic');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (79, TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'PowerTax - MLP K1 Exp Search', null,
    'uo_ppbase_tab_filter_dynamic');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (188, 'Company', 'dw', 'tax_k1_export_run_results.company_id', null, 'dw_tax_company_by_version_filter',
    'company_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT',
    TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (187, 'Tax Year', 'dw', 'tax_k1_export_run_results.tax_year', null, 'dw_tax_year_by_version_filter', 'tax_year',
    'tax_year', 'D', 0, 1, null, null, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (186, 'K1 Export Run', 'dw', 'tax_k1_export_run_results.k1_export_run_id', null, 'dw_tax_k1_export_run_filter',
    'k1_export_run_id', 'description', 'N', 1, 1, null, null, 'PWRPLANT',
    TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (184, 'K1 Export Run Description', 'sle', null, null, null, null, null, null, 1, 1, null, null, 'PWRPLANT',
    TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'), 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (183, 'K1 Export Definition', 'dw', null, null, 'dw_tax_k1_export_definition_filter', 'k1_export_definition_id',
    'description', 'N', 1, 1, null, null, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'), 1, 0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (78, 157, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (78, 163, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (78, 183, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (78, 184, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (79, 186, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (79, 187, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (79, 188, 'PWRPLANT', TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'));

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (58, 186, 187, 'tax_k1_export_run.tax_year', null, 'PWRPLANT',
    TO_DATE('08-08-2014 13:10:17', 'dd-mm-yyyy hh24:mi:ss'));

----
----  New tables and data
----
alter table TAX_EVENT add DISPLAY_FOR_K1_EXPORT number(22,0);

comment on column TAX_EVENT.DISPLAY_FOR_K1_EXPORT is 'A yes/no flag used to identify tax events available for MLP K1 export definition to use.';

update TAX_EVENT set DISPLAY_FOR_K1_EXPORT = DECODE(TAX_EVENT_ID, 1, 0, 2, 0, 1);

alter table TAX_EVENT modify DISPLAY_FOR_K1_EXPORT not null;
alter table TAX_EVENT modify DISPLAY_FOR_K1_EXPORT default 1;

alter table TAX_CREDIT add EXT_TAX_CREDIT varchar2(50);

comment on column TAX_CREDIT.EXT_TAX_CREDIT is 'External tax credit descriptive column that may be used by interfaces and exports.';

alter table TAX_RATE_CONTROL add EXT_TAX_RATE varchar2(50);

comment on column TAX_RATE_CONTROL.EXT_TAX_RATE is 'External tax rate descriptive column that may be used by interfaces and exports.';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('ext_tax_rate', 'tax_rate_control', 'e', 'External Tax Rate', 14, 0);

--
create table TAX_K1_EXPORT_DEFINITION
(
 K1_EXPORT_DEFINITION_ID number(22,0) not null,
 USER_ID                 varchar2(18),
 TIME_STAMP              date,
 DESCRIPTION             varchar2(35) not null,
 LONG_DESCRIPTION        varchar2(254),
 CREATED_BY              varchar2(18),
 CREATED_DATE            date,
 USE_EVENT_OVERRIDE      number(22,0) default 0 not null,
 PREFIX                  varchar2(254)
);

alter table TAX_K1_EXPORT_DEFINITION
   add constraint TAX_K1_EXPORT_DEFINITION_PK
       primary key (K1_EXPORT_DEFINITION_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_K1_EXPORT_DEFINITION is '(C)[09]The Tax K1 Export Definition table holds the header information of how data should be exported from PowerTax for other systems that generate MLP K1 filings.';

comment on column TAX_K1_EXPORT_DEFINITION.K1_EXPORT_DEFINITION_ID is 'The system generated key of the K1 export defintion.';
comment on column TAX_K1_EXPORT_DEFINITION.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_K1_EXPORT_DEFINITION.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_K1_EXPORT_DEFINITION.DESCRIPTION is 'Description of the K1 export defintion.';
comment on column TAX_K1_EXPORT_DEFINITION.LONG_DESCRIPTION is 'Long Description of the K1 export defintion.';
comment on column TAX_K1_EXPORT_DEFINITION.CREATED_BY is 'The date when the K1 export definition was created.';
comment on column TAX_K1_EXPORT_DEFINITION.CREATED_DATE is 'The user who created the K1 export definition.';
comment on column TAX_K1_EXPORT_DEFINITION.USE_EVENT_OVERRIDE is 'A Yes/No flag indicating whether fields used in creating the K1 export value will differ for one or more tax events.';
comment on column TAX_K1_EXPORT_DEFINITION.PREFIX is 'An optional entered static text field for k1 export values to use as the beginning character set.';

--
create table TAX_K1_EXPORT_FIELDS
(
 K1_FIELD_ID number(22,0) not null,
 USER_ID     varchar2(18),
 TIME_STAMP  date,
 DESCRIPTION varchar2(254) not null,
 FIELD_SQL   varchar2(254) not null,
 WHERE_SQL   varchar2(254),
 FROM_SQL    varchar2(40) not null
);

alter table TAX_K1_EXPORT_FIELDS
   add constraint TAX_K1_EXPORT_FIELDS_PK
       primary key (K1_FIELD_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_K1_EXPORT_FIELDS is '(F)[09]The Tax K1 Export Fields table holds the list of available fields from PowerTax to use in the creation of the K1 export values.';

comment on column TAX_K1_EXPORT_FIELDS.K1_FIELD_ID is 'The system generated key of the field available for the MLP K1 export process.';
comment on column TAX_K1_EXPORT_FIELDS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_K1_EXPORT_FIELDS.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_K1_EXPORT_FIELDS.DESCRIPTION is 'Description of the field available for the creating of the K1 export value.';
comment on column TAX_K1_EXPORT_FIELDS.FIELD_SQL is 'The SQL clause used by the process to build the selection of the field''s data.';
comment on column TAX_K1_EXPORT_FIELDS.WHERE_SQL is 'The SQL clause that may be used by the process to filter records in the selection of the field''s data.';
comment on column TAX_K1_EXPORT_FIELDS.FROM_SQL is 'The SQL clause used by the process to use the appropriate table of the field''s data.';


--
create table TAX_K1_EXPORT_DEF_FIELDS
(
 K1_EXPORT_DEFINITION_ID number(22,0) not null,
 TAX_EVENT_ID            number(22,0),
 FIELD_ORDER             number(22,0) not null,
 USER_ID                 varchar2(18),
 TIME_STAMP              date,
 K1_FIELD_ID             number(22,0),
 SEPARATOR               varchar2(254)
);

create unique index TAX_K1_EXPORT_DEF_FIELDS_UIDX
    on TAX_K1_EXPORT_DEF_FIELDS (K1_EXPORT_DEFINITION_ID, TAX_EVENT_ID, FIELD_ORDER)
       tablespace PWRPLANT_IDX;

alter table TAX_K1_EXPORT_DEF_FIELDS
   add constraint TAX_K1_EXPORT_DEF_FLD_DEF_FK
       foreign key (K1_EXPORT_DEFINITION_ID)
       references TAX_K1_EXPORT_DEFINITION;

alter table TAX_K1_EXPORT_DEF_FIELDS
   add constraint TAX_K1_EXPORT_DEF_FLD_FLD_FK
       foreign key (K1_FIELD_ID)
       references TAX_K1_EXPORT_FIELDS;

alter table TAX_K1_EXPORT_DEF_FIELDS
   add constraint TAX_K1_EXPORT_DEF_FLD_EVT_FK
       foreign key (TAX_EVENT_ID)
       references TAX_EVENT;

comment on table TAX_K1_EXPORT_DEF_FIELDS is '(C)[09]The Tax K1 Export Def Fields holds the fields assigned to export definitions for the MLP K1 export process.';

comment on column TAX_K1_EXPORT_DEF_FIELDS.K1_EXPORT_DEFINITION_ID is 'The system generated key of the K1 export defintion.';
comment on column TAX_K1_EXPORT_DEF_FIELDS.TAX_EVENT_ID is 'The system generated key of the tax event. Fields used in the K1 export value generation may differ by tax event.  If tax event is blank, the fields associated with the definition are the default fields to be used by the process.';
comment on column TAX_K1_EXPORT_DEF_FIELDS.FIELD_ORDER is 'The order in which the fields should be used in creating the K1 export value (by definition and tax event).';
comment on column TAX_K1_EXPORT_DEF_FIELDS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_K1_EXPORT_DEF_FIELDS.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_K1_EXPORT_DEF_FIELDS.K1_FIELD_ID is 'The system generated key of the field used for the MLP K1 export process.';
comment on column TAX_K1_EXPORT_DEF_FIELDS.SEPARATOR is 'An optional field for static text to be entered and used immediately after each field value is pulled into the k1 export value.';


--
create table TAX_K1_EXPORT_RUN
(
 K1_EXPORT_RUN_ID        number(22,0) not null,
 USER_ID                 varchar2(18),
 TIME_STAMP              date,
 DESCRIPTION             varchar2(100),
 K1_EXPORT_DEFINITION_ID number(22,0) not null,
 TAX_YEAR                number(22,2) not null
);

alter table TAX_K1_EXPORT_RUN
   add constraint TAX_K1_EXPORT_RUN_PK
       primary key (K1_EXPORT_RUN_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_K1_EXPORT_RUN
   add constraint TAX_K1_EXPORT_RUN_DEF_FK
       foreign key (K1_EXPORT_DEFINITION_ID)
       references TAX_K1_EXPORT_DEFINITION;

comment on table TAX_K1_EXPORT_RUN is '(C)[09]The Tax K1 Export Run table holds the header information related to a run of the MLP K1 export process.';
comment on column TAX_K1_EXPORT_RUN.K1_EXPORT_RUN_ID is 'The system generated key of the K1 export run.';
comment on column TAX_K1_EXPORT_RUN.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_K1_EXPORT_RUN.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_K1_EXPORT_RUN.DESCRIPTION is 'Description of the K1 export run.';
comment on column TAX_K1_EXPORT_RUN.K1_EXPORT_DEFINITION_ID is 'The system generated key of the K1 export defintion used in the run.';
comment on column TAX_K1_EXPORT_RUN.TAX_YEAR is 'The PowerTax tax year used in the run.';

--
create table TAX_K1_EXPORT_RUN_RESULTS
(
 K1_EXPORT_RUN_ID      number(22,0) not null,
 TAX_RECORD_ID         number(22,0) not null,
 USER_ID               varchar2(18),
 TIME_STAMP            date,
 TAX_YEAR              number(22,2),
 EXISTING_K1_EXPORT_ID number(22,0),
 AUTOGEN_K1_EXPORT_ID  number(22,0),
 FINAL_K1_EXPORT_ID    number(22,0),
 TAX_BALANCE_END       number(22,2),
 ACCUM_RESERVE_END     number(22,2),
 COMPANY_ID            number(22,0),
 VINTAGE_ID            number(22,0),
 TAX_CLASS_ID          number(22,0),
 IN_SERVICE_MONTH      date,
 TAX_LAYER_ID          number(22,0),
 TAX_EVENT_ID          number(22,0),
 FIELD_VALUE_1         varchar2(50),
 FIELD_VALUE_2         varchar2(50),
 FIELD_VALUE_3         varchar2(50),
 FIELD_VALUE_4         varchar2(50),
 FIELD_VALUE_5         varchar2(50),
 FIELD_VALUE_6         varchar2(50),
 FIELD_VALUE_7         varchar2(50),
 FIELD_VALUE_8         varchar2(50),
 FIELD_VALUE_9         varchar2(50),
 FIELD_VALUE_10        varchar2(50),
 FIELD_VALUE_11        varchar2(50),
 FIELD_VALUE_12        varchar2(50),
 FIELD_VALUE_13        varchar2(50),
 FIELD_VALUE_14        varchar2(50),
 FIELD_VALUE_15        varchar2(50),
 FIELD_VALUE_16        varchar2(50),
 FIELD_VALUE_17        varchar2(50),
 FIELD_VALUE_18        varchar2(50),
 FIELD_VALUE_19        varchar2(50),
 FIELD_VALUE_20        varchar2(50),
 FINAL_VALUE           varchar2(2000)
);

alter table TAX_K1_EXPORT_RUN_RESULTS
   add constraint TAX_K1_EXPORT_RUN_RESULTS_PK
       primary key (K1_EXPORT_RUN_ID, TAX_RECORD_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_K1_EXPORT_RUN_RESULTS
   add constraint TAX_K1_EXPORT_RUN_RSLT_RN_FK
       foreign key (K1_EXPORT_RUN_ID)
       references TAX_K1_EXPORT_RUN;

alter table TAX_K1_EXPORT_RUN_RESULTS
   add constraint TAX_K1_EXPORT_RUN_RSLT_EX_FK
       foreign key (EXISTING_K1_EXPORT_ID)
       references TAX_K1_EXPORT (K1_EXPORT_ID);

alter table TAX_K1_EXPORT_RUN_RESULTS
   add constraint TAX_K1_EXPORT_RUN_RSLT_AUT_FK
       foreign key (AUTOGEN_K1_EXPORT_ID)
       references TAX_K1_EXPORT (K1_EXPORT_ID);

alter table TAX_K1_EXPORT_RUN_RESULTS
   add constraint TAX_K1_EXPORT_RUN_RSLT_FIN_FK
       foreign key (FINAL_K1_EXPORT_ID)
       references TAX_K1_EXPORT (K1_EXPORT_ID);

comment on table TAX_K1_EXPORT_RUN_RESULTS is '(C)[09]The Tax K1 Export Run Results table holds the generated results of the MLP K1 export processing.';

comment on column TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID is 'The system generated key of the K1 export run.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID is 'System-assigned key that identifies an individual tax asset.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.TAX_YEAR is 'Calendar tax year associated with the tax record''s  depreciation calculation and stored balances.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.EXISTING_K1_EXPORT_ID is 'The k1 export value on the tax record before any results have been saved.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.AUTOGEN_K1_EXPORT_ID is 'The automatically generated k1 export value linked to the tax record in the run.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID is 'The final k1 export value linked to the tax record. By default, the existing k1 export value id is used, and if that does not exist, the automatical k1 export value is used.  When the user saves on the workspace, this id is used to update the k1 export back to tax record control.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.TAX_BALANCE_END is 'The ending tax basis balance of this tax asset record in dollars, not reduced for ordinary (ADR) retirements.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.ACCUM_RESERVE_END is 'The ending accumulated tax depreciation reserve balance in dollars.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.COMPANY_ID is 'System-assigned identifier of a particular company. Saved to the results table instead of selecting from tax record control for performance gains.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.VINTAGE_ID is 'System-assigned identifier for a unique tax vintage. Saved to the results table instead of selecting from tax record control for performance gains.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.TAX_CLASS_ID is 'System-assigned identifier for each individual tax class. Saved to the results table instead of selecting from tax record control for performance gains.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.IN_SERVICE_MONTH is 'Month/year field that is optional in the tax record key. Saved to the results table instead of selecting from tax record control for performance gains.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.TAX_LAYER_ID is 'System-assigned identifier of a tax layer. Saved to the results table instead of selecting from tax depreciation for performance gains.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.TAX_EVENT_ID is 'System-assigned identifier of a tax event. Saved to the results table instead of selecting from tax depreciation for performance gains.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_1 is 'The tax record''s calculated value for the first field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_2 is 'The tax record''s calculated value for the second field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_3 is 'The tax record''s calculated value for the third field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_4 is 'The tax record''s calculated value for the fourth field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_5 is 'The tax record''s calculated value for the fifth field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_6 is 'The tax record''s calculated value for the 6th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_7 is 'The tax record''s calculated value for the 7th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_8 is 'The tax record''s calculated value for the 8th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_9 is 'The tax record''s calculated value for the 9th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_10 is 'The tax record''s calculated value for the 10th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_11 is 'The tax record''s calculated value for the 11th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_12 is 'The tax record''s calculated value for the 12th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_13 is 'The tax record''s calculated value for the 13th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_14 is 'The tax record''s calculated value for the 14th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_15 is 'The tax record''s calculated value for the 15th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_16 is 'The tax record''s calculated value for the 16th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_17 is 'The tax record''s calculated value for the 17th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_18 is 'The tax record''s calculated value for the 18th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_19 is 'The tax record''s calculated value for the 19th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_20 is 'The tax record''s calculated value for the 20th field and selector of the export definition''s fields.';
comment on column TAX_K1_EXPORT_RUN_RESULTS.FINAL_VALUE is 'The tax record''s final calculated value made from concatenating the other numbered calculated values.  The distinct list is used to create new record into tax k1 export.';

--
create table TAX_K1_EXPORT_CONFIG_AVAIL
(
 K1_EXP_FIELD_ID number(22,0) not null,
 USER_ID         varchar2(18),
 TIME_STAMP      date,
 DESCRIPTION     varchar2(254) not null,
 EXP_FIELD_SQL   varchar2(254) not null,
 SUM_FIELD       number(22,0) not null
);

alter table TAX_K1_EXPORT_CONFIG_AVAIL
   add constraint TAX_K1_EXPORT_CONFIG_AVAIL_PK
       primary key (K1_EXP_FIELD_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_K1_EXPORT_CONFIG_AVAIL is '(F)[09]The Tax K1 Export Config Avail table holds the available values of the MLP K1 export process results that can be summarized in the final export of the actual results.';
comment on column TAX_K1_EXPORT_CONFIG_AVAIL.K1_EXP_FIELD_ID is 'The system generated key of the MLP K1 configuration export field.';
comment on column TAX_K1_EXPORT_CONFIG_AVAIL.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_K1_EXPORT_CONFIG_AVAIL.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_K1_EXPORT_CONFIG_AVAIL.DESCRIPTION is 'Description of the MLP K1 configuration export field.';
comment on column TAX_K1_EXPORT_CONFIG_AVAIL.SUM_FIELD is 'Indicates whether the configuation export field is a numeric field that can be summed.';

--
create table TAX_K1_EXPORT_CONFIG_SAVE
(
 K1_EXP_FIELD_ID number(22,0) not null,
 USER_ID         varchar2(18),
 TIME_STAMP      date,
 FIELD_ORDER     number(22,0) not null
);

alter table TAX_K1_EXPORT_CONFIG_SAVE
   add constraint TAX_K1_EXPORT_CONFIG_SAVE_PK
       primary key (K1_EXP_FIELD_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_K1_EXPORT_CONFIG_SAVE is '(C)[09]The Tax K1 Export Config Save table holds the system settings for the final fields used in the summarization and saving of the MLP K1 export processing results.';
comment on column TAX_K1_EXPORT_CONFIG_SAVE.K1_EXP_FIELD_ID is 'The system generated key of the MLP K1 configuration export field.';
comment on column TAX_K1_EXPORT_CONFIG_SAVE.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_K1_EXPORT_CONFIG_SAVE.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_K1_EXPORT_CONFIG_SAVE.FIELD_ORDER is 'The order in which the configuration export fields are saved to the file.';


--
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (1, 'Company.Company ID', 'company_setup.company_id', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (2, 'Company.Description', 'company_setup.description', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (3, 'Company.Short Description', 'company_setup.short_description', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (4, 'Company.GL Company No', 'company_setup.gl_company_no', '', 'company_setup');

insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (5, 'Vintage.Vintage ID', 'vintage.vintage_id', '', 'vintage');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (6, 'Vintage.Description', 'vintage.description', '', 'vintage');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (7, 'Vintage.Year', 'vintage.year', '', 'vintage');

insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (8, 'Tax Record Control.In Service Month (MM)', 'to_char(tax_record_control.in_service_month,''MM'')', '',
    'tax_record_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (9, 'Tax Record Control.In Service Month (Mon)', 'to_char(tax_record_control.in_service_month,''Mon'')', '',
    'tax_record_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (10, 'Tax Record Control.In Service Month (Month)', 'to_char(tax_record_control.in_service_month,''Month'')', '',
    'tax_record_control');

insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (11, 'Tax Layer.Tax Layer ID', 'tax_layer.tax_layer_id', '', 'tax_layer');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (12, 'Tax Layer.Description', 'tax_layer.description', '', 'tax_layer');

insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (13, 'Tax Credit.Tax Credit ID (Federal)', 'tax_credit.tax_credit_id', 'tax_book.tax_book_id = 10', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (14, 'Tax Credit.Description (Federal)', 'tax_credit.description', 'tax_book.tax_book_id = 10', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (15, 'Tax Credit.Tax Credit ID (AMT)', 'tax_credit.tax_credit_id', 'tax_book.tax_book_id = 20', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (16, 'Tax Credit.Description (AMT)', 'tax_credit.description', 'tax_book.tax_book_id = 20', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (17, 'Tax Credit.Tax Credit ID (ACE)', 'tax_credit.tax_credit_id', 'tax_book.tax_book_id = 30', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (18, 'Tax Credit.Description (ACE)', 'tax_credit.description', 'tax_book.tax_book_id = 30', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (19, 'Tax Credit.Tax Credit ID (EP)', 'tax_credit.tax_credit_id', 'tax_book.tax_book_id = 40', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (20, 'Tax Credit.Description (EP)', 'tax_credit.description', 'tax_book.tax_book_id = 40', 'tax_credit');

insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (23, 'Tax Rate Control.Tax Rate ID (Federal)', 'tax_rate_control.tax_rate_id', 'tax_book.tax_book_id = 10',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (24, 'Tax Rate Control.Description (Federal)', 'tax_rate_control.description', 'tax_book.tax_book_id = 10',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (25, 'Tax Rate Control.Tax Rate ID (AMT)', 'tax_rate_control.tax_rate_id', 'tax_book.tax_book_id = 20',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (26, 'Tax Rate Control.Description (AMT)', 'tax_rate_control.description', 'tax_book.tax_book_id = 20',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (27, 'Tax Rate Control.Tax Rate ID (ACE)', 'tax_rate_control.tax_rate_id', 'tax_book.tax_book_id = 30',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (28, 'Tax Rate Control.Description (ACE)', 'tax_rate_control.description', 'tax_book.tax_book_id = 30',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (29, 'Tax Rate Control.Tax Rate ID (EP)', 'tax_rate_control.tax_rate_id', 'tax_book.tax_book_id = 40',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (30, 'Tax Rate Control.Description (EP)', 'tax_rate_control.description', 'tax_book.tax_book_id = 40',
    'tax_rate_control');

----Phil requested all fields from company setup be available.
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (31, 'Company.Owned', 'company_setup.owned', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (32, 'Company.Status Code ID', 'company_setup.status_code_id', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (33, 'Company.Company Summary ID', 'company_setup.company_summary_id', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (34, 'Company.Auto Life Mo', 'company_setup.auto_life_month', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (35, 'Company.Auto Curve Mo', 'company_setup.auto_curve_month', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (36, 'Company.Auto Close Wo No', 'company_setup.auto_close_wo_num', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (37, 'Company.Tax Return ID', 'company_setup.tax_return_id', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (38, 'Company.Parent Co ID', 'company_setup.parent_company_id', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (39, 'Company.Company Type', 'company_setup.company_type', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (40, 'Company.Company EIN', 'company_setup.company_ein', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (41, 'Company.Closing Rollup ID', 'company_setup.closing_rollup_id', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (42, 'Company.Auto Life Mo Month', 'company_setup.auto_life_month_monthly', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (43, 'Company.CWIP Allo Mo', 'company_setup.cwip_allocation_months', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (44, 'Company.PT Co ID', 'company_setup.prop_tax_company_id', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (45, 'Company.PowerTax Co ID', 'company_setup.powertax_company_id', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (46, 'Company.Is Lease Co', 'company_setup.is_lease_company', '', 'company_setup');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (47, 'Company.Reg Co ID', 'company_setup.reg_company_id', '', 'company_setup');

--new ext fields...
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (48, 'Tax Credit.Ext Tax Credit (Federal)', 'tax_credit.ext_tax_credit', 'tax_book.tax_book_id = 10', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (49, 'Tax Credit.Ext Tax Credit (AMT)', 'tax_credit.ext_tax_credit', 'tax_book.tax_book_id = 20', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (50, 'Tax Credit.Ext Tax Credit (ACE)', 'tax_credit.ext_tax_credit', 'tax_book.tax_book_id = 30', 'tax_credit');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (51, 'Tax Credit.Ext Tax Credit (EP)', 'tax_credit.ext_tax_credit', 'tax_book.tax_book_id = 40', 'tax_credit');

insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (52, 'Tax Rate Control.Ext Tax Rate (Federal)', 'tax_rate_control.ext_tax_rate', 'tax_book.tax_book_id = 10',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (53, 'Tax Rate Control.Ext Tax Rate (AMT)', 'tax_rate_control.ext_tax_rate', 'tax_book.tax_book_id = 20',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (54, 'Tax Rate Control.Ext Tax Rate (ACE)', 'tax_rate_control.ext_tax_rate', 'tax_book.tax_book_id = 30',
    'tax_rate_control');
insert into TAX_K1_EXPORT_FIELDS
   (K1_FIELD_ID, DESCRIPTION, FIELD_SQL, WHERE_SQL, FROM_SQL)
values
   (55, 'Tax Rate Control.Ext Tax Rate (EP)', 'tax_rate_control.ext_tax_rate', 'tax_book.tax_book_id = 40',
    'tax_rate_control');

--
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (1, 'Company', 'company', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (2, 'Tax Class', 'tax_class', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (3, 'Vintage', 'vintage', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (4, 'In Service Month', 'in_service_mo', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (5, 'Tax Layer', 'tax_layer', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (6, 'Tax Event', 'tax_event', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (7, 'Tax Year', 'tax_year', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (8, 'K1 Export Value', 'final_k1_export', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (9, 'K1 Export ID', 'final_k1_export_id', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (10, 'Tax Balance End', 'tax_balance_end', 1);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (11, 'Accum Reserve End', 'accum_reserve_end', 1);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (12, 'Tax Record ID', 'tax_record_id', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (13, 'K1 Export Run ID', 'k1_export_run_id', 0);
insert into TAX_K1_EXPORT_CONFIG_AVAIL
   (K1_EXP_FIELD_ID, DESCRIPTION, EXP_FIELD_SQL, SUM_FIELD)
values
   (14, 'K1 Export Run', 'k1_export_run', 0);

----default the set config
insert into TAX_K1_EXPORT_CONFIG_SAVE (K1_EXP_FIELD_ID, FIELD_ORDER) values (8, 1);
insert into TAX_K1_EXPORT_CONFIG_SAVE (K1_EXP_FIELD_ID, FIELD_ORDER) values (10, 2);
insert into TAX_K1_EXPORT_CONFIG_SAVE (K1_EXP_FIELD_ID, FIELD_ORDER) values (11, 3);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1357, 0, 10, 4, 3, 0, 39097, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039097_pwrtax_mlp_k1_export_pt2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;