/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050998_lessor_01_create_termination_op_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/24/2018 Anand R     	  Create new table to store termination info for Operating ILR
||============================================================================
*/

create table lsr_ilr_termination_op (
  ilr_id number(22,0) not null,
  termination_date date not null,
  penalty_other_payment number(22,2) not null,
  penalty_other_payment_acct_id number(22,0),
  income_gainloss number(22,2) not null,
  income_gainloss_acct_id number(22,0),
  comments varchar2(2000) not null,
  user_id varchar2(18),
  time_stamp date ) ;
  
comment on table lsr_ilr_termination_op is 'Table to store termination information for Operating type ILR';

comment on column lsr_ilr_termination_op.ilr_id is 'System generated id of the ILR';
comment on column lsr_ilr_termination_op.termination_date is 'Date when the ILR will be terminated';
comment on column lsr_ilr_termination_op.penalty_other_payment is 'Penalty or Other payment amount paid towards ILR termination';
comment on column lsr_ilr_termination_op.penalty_other_payment_acct_id is 'System generated account ID for penalty or other payment';
comment on column lsr_ilr_termination_op.income_gainloss is 'Calculated gain/loss for the ILR termination';
comment on column lsr_ilr_termination_op.income_gainloss_acct_id is 'System generated account ID for gain/loss payment';
comment on column lsr_ilr_termination_op.comments is 'Comments for the ILR termination';
comment on column lsr_ilr_termination_op.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column lsr_ilr_termination_op.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

alter table lsr_ilr_termination_op
add constraint lsr_ilr_termination_op_pk
primary key (ilr_id)
using index tablespace PWRPLANT_IDX;

alter table lsr_ilr_termination_op
add constraint lsr_ilr_termination_op_fk
foreign key (ilr_id)
references lsr_ilr (ilr_id);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(5882, 0, 2017, 4, 0, 0, 50998, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050998_lessor_01_create_termination_op_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;