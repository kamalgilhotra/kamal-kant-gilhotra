/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052340_lessor_01_st_df_remeasurement_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2018.1.0.0 10/17/2018 Anand R           PP-52340 - Add remeasurement columns to sales schedule type object and table LSR_ILR_SCHEDULE_SALES_DIRECT
||============================================================================
*/

drop type LSR_ILR_SALES_SCH_INFO_TAB
/
drop type T_LSR_ILR_SALES_DF_PRELIMS
/

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO AS OBJECT (carrying_cost NUMBER,
                                                         carrying_cost_company_curr number,
                                                         fair_market_value NUMBER,
                                                         fair_market_value_company_curr number,
                                                         guaranteed_residual NUMBER,
                                                         estimated_residual NUMBER,
                                                         days_in_year NUMBER,
                                                         purchase_option_amount NUMBER,
                                                         termination_amount NUMBER,
                                                         remeasurement_date date,
                                                         investment_amount number,
                                                         original_profit_loss number,
                                                         new_beg_receivable number,
                                                         prior_end_unguaran_residual number)
/														 
                                                         
CREATE OR REPLACE TYPE T_LSR_ILR_SALES_DF_PRELIMS AS OBJECT ( payment_info lsr_ilr_op_sch_pay_info_tab,
                                                              rates t_lsr_ilr_schedule_all_rates,
                                                              initial_direct_costs lsr_init_direct_cost_info_tab,
                                                              sales_type_info lsr_ilr_sales_sch_info)
/															  
                                                         
create type LSR_ILR_SALES_SCH_INFO_TAB as table of LSR_ILR_SALES_SCH_INFO
/

--ST/DF Schedule Result Types (and dependents)
drop type lsr_ilr_df_schedule_result_tab
/

CREATE OR REPLACE TYPE LSR_ILR_DF_SCHEDULE_RESULT IS OBJECT(MONTH DATE,
                                                            principal_received NUMBER,
                                                            interest_income_received   NUMBER,
                                                            interest_income_accrued    NUMBER,
                                                            principal_accrued            NUMBER,
                                                            begin_receivable       NUMBER,
                                                            end_receivable           NUMBER,
                                                            begin_lt_receivable          NUMBER,
                                                            end_lt_receivable            NUMBER,
                                                            initial_direct_cost          NUMBER,
                                                            executory_accrual1           NUMBER,
                                                            executory_accrual2           NUMBER,
                                                            executory_accrual3           NUMBER,
                                                            executory_accrual4           NUMBER,
                                                            executory_accrual5           NUMBER,
                                                            executory_accrual6           NUMBER,
                                                            executory_accrual7           NUMBER,
                                                            executory_accrual8           NUMBER,
                                                            executory_accrual9           NUMBER,
                                                            executory_accrual10          NUMBER,
                                                            executory_paid1              NUMBER,
                                                            executory_paid2              NUMBER,
                                                            executory_paid3              NUMBER,
                                                            executory_paid4              NUMBER,
                                                            executory_paid5              NUMBER,
                                                            executory_paid6              NUMBER,
                                                            executory_paid7              NUMBER,
                                                            executory_paid8              NUMBER,
                                                            executory_paid9              NUMBER,
                                                            executory_paid10             NUMBER,
                                                            contingent_accrual1          NUMBER,
                                                            contingent_accrual2          NUMBER,
                                                            contingent_accrual3          NUMBER,
                                                            contingent_accrual4          NUMBER,
                                                            contingent_accrual5          NUMBER,
                                                            contingent_accrual6          NUMBER,
                                                            contingent_accrual7          NUMBER,
                                                            contingent_accrual8          NUMBER,
                                                            contingent_accrual9          NUMBER,
                                                            contingent_accrual10         NUMBER,
                                                            contingent_paid1             NUMBER,
                                                            contingent_paid2             NUMBER,
                                                            contingent_paid3             NUMBER,
                                                            contingent_paid4             NUMBER,
                                                            contingent_paid5             NUMBER,
                                                            contingent_paid6             NUMBER,
                                                            contingent_paid7             NUMBER,
                                                            contingent_paid8             NUMBER,
                                                            contingent_paid9             NUMBER,
                                                            contingent_paid10            NUMBER,
                                                            begin_unguaranteed_residual NUMBER,
                                                            int_on_unguaranteed_residual NUMBER,
                                                            end_unguaranteed_residual NUMBER,
                                                            begin_net_investment NUMBER,
                                                            int_on_net_investment NUMBER,
                                                            end_net_investment NUMBER,
                                                            begin_deferred_profit number,
                                                            recognized_profit number,
                                                            end_deferred_profit number,
                                                            receivable_remeasurement number,
                                                            unguaran_residual_remeasure number,
                                                            rate_implicit FLOAT,
                                                            discount_rate FLOAT,
                                                            rate_implicit_ni FLOAT,
                                                            discount_rate_ni FLOAT,
                                                            begin_lease_receivable number,
                                                            original_net_investment number,
                                                            npv_lease_payments NUMBER,
                                                            npv_guaranteed_residual NUMBER,
                                                            npv_unguaranteed_residual NUMBER,
                                                            selling_profit_loss NUMBER,
                                                            cost_of_goods_sold NUMBER,
                                                            schedule_rates t_lsr_ilr_schedule_all_rates)
/															

create or replace type LSR_ILR_DF_SCHEDULE_RESULT_tab as table of LSR_ILR_DF_SCHEDULE_RESULT
/

drop type lsr_ilr_sales_sch_result_tab
/

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER,
                                                          interest_income_received     NUMBER,
                                                          interest_income_accrued      NUMBER,
                                                          principal_accrued            NUMBER,
                                                          begin_receivable         NUMBER,
                                                          end_receivable         NUMBER,
                                                          begin_lt_receivable          NUMBER,
                                                          end_lt_receivable            NUMBER,
                                                          initial_direct_cost          NUMBER,
                                                          executory_accrual1           NUMBER,
                                                          executory_accrual2           NUMBER,
                                                          executory_accrual3           NUMBER,
                                                          executory_accrual4           NUMBER,
                                                          executory_accrual5           NUMBER,
                                                          executory_accrual6           NUMBER,
                                                          executory_accrual7           NUMBER,
                                                          executory_accrual8           NUMBER,
                                                          executory_accrual9           NUMBER,
                                                          executory_accrual10          NUMBER,
                                                          executory_paid1              NUMBER,
                                                          executory_paid2              NUMBER,
                                                          executory_paid3              NUMBER,
                                                          executory_paid4              NUMBER,
                                                          executory_paid5              NUMBER,
                                                          executory_paid6              NUMBER,
                                                          executory_paid7              NUMBER,
                                                          executory_paid8              NUMBER,
                                                          executory_paid9              NUMBER,
                                                          executory_paid10             NUMBER,
                                                          contingent_accrual1          NUMBER,
                                                          contingent_accrual2          NUMBER,
                                                          contingent_accrual3          NUMBER,
                                                          contingent_accrual4          NUMBER,
                                                          contingent_accrual5          NUMBER,
                                                          contingent_accrual6          NUMBER,
                                                          contingent_accrual7          NUMBER,
                                                          contingent_accrual8          NUMBER,
                                                          contingent_accrual9          NUMBER,
                                                          contingent_accrual10         NUMBER,
                                                          contingent_paid1             NUMBER,
                                                          contingent_paid2             NUMBER,
                                                          contingent_paid3             NUMBER,
                                                          contingent_paid4             NUMBER,
                                                          contingent_paid5             NUMBER,
                                                          contingent_paid6             NUMBER,
                                                          contingent_paid7             NUMBER,
                                                          contingent_paid8             NUMBER,
                                                          contingent_paid9             NUMBER,
                                                          contingent_paid10            NUMBER,
                                                          begin_unguaranteed_residual NUMBER,
                                                          int_on_unguaranteed_residual NUMBER,
                                                          end_unguaranteed_residual NUMBER,
                                                          begin_net_investment NUMBER,
                                                          int_on_net_investment NUMBER,
                                                          end_net_investment NUMBER,
                                                          receivable_remeasurement number,
                                                          unguaran_residual_remeasure number,
                                                          rate_implicit FLOAT,
                                                          discount_rate FLOAT,
                                                          rate_implicit_ni FLOAT,
                                                          discount_rate_ni FLOAT,
                                                          begin_lease_receivable number,
                                                          original_net_investment number,
                                                          npv_lease_payments NUMBER,
                                                          npv_guaranteed_residual NUMBER,
                                                          npv_unguaranteed_residual NUMBER,
                                                          selling_profit_loss NUMBER,
                                                          cost_of_goods_sold NUMBER,
                                                          schedule_rates t_lsr_ilr_schedule_all_rates)
/														  

CREATE OR REPLACE TYPE lsr_ilr_sales_sch_result_tab AS TABLE OF lsr_ilr_sales_sch_result
/

-- Add a column to store un guaranteed residual for re measurement ILRs
alter table LSR_ILR_SCHEDULE_SALES_DIRECT 
  add unguaran_residual_remeasure number(22,2);

-- Add global temp table to hold the calculated amounts from the prior approved revision
create global temporary table LSR_ILR_PRIOR_AMOUNTS_TEMP
(
  ilr_id                     NUMBER(22) not null,
  revision                   NUMBER(22) not null,
  set_of_books_id            NUMBER(22) not null,
  npv_lease_payments         NUMBER(22,2),
  npv_guaranteed_residual    NUMBER(22,2),
  npv_unguaranteed_residual  NUMBER(22,2),
  selling_profit_loss        NUMBER(22,2),
  beginning_lease_receivable NUMBER(22,2),
  beginning_net_investment   NUMBER(22,2),
  cost_of_goods_sold         NUMBER(22,2),
  schedule_rates             T_LSR_ILR_SCHEDULE_ALL_RATES not null
) on commit delete rows;

-- Add comments to the table 
comment on table LSR_ILR_PRIOR_AMOUNTS_TEMP is '(C)  [06] The ILR Prior Amounts table holds the prior approved calculated amounts from the revision being remeasured.';
  
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.ilr_id is 'System-assigned identifier of a particular ILR.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.revision is 'The revision.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.set_of_books_id is 'The set of books.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.npv_lease_payments is 'The net present value of the lease payments in contract currency.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.npv_guaranteed_residual is 'The net present value of the guaranteed residual in contract currency.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.npv_unguaranteed_residual is 'The net present value of the unguaranteed residul in contract currency.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.selling_profit_loss is 'The selling profit or loss in contract currency.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.beginning_lease_receivable is 'The beginning lease receivable amount in contract currency.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.beginning_net_investment is 'The beginning net investement amount in contract currency.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.cost_of_goods_sold is 'The cost of goods sold in contract currency.';
comment on column LSR_ILR_PRIOR_AMOUNTS_TEMP.schedule_rates is 'Rates used during schedule building process for this ILR/Revision/Set of Books';

alter table LSR_ILR_PRIOR_AMOUNTS_TEMP
  add constraint LSR_ILR_PRIOR_AMOUNTS_TEMP_PK primary key (ILR_ID, REVISION, SET_OF_BOOKS_ID);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10622, 0, 2018, 1, 0, 0, 52340, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052340_lessor_01_st_df_remeasurement_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;