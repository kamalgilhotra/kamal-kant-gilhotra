/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009501_prov.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/26/2014 Nathan Hollis
||============================================================================
*/

insert into TAX_ACCRUAL_SQL_HINTS
   (SQL_NAME, SELECT_NUMBER, REASON)
   select SQL_LABELS.SQL_NAME, 0, 'Placeholder'
     from (select 'ETR_EFF_RATE_INSERT1' SQL_NAME       from DUAL union all
           select 'ETR_EFF_RATE_INSERT'                 from DUAL union all
           select 'JE_TRANSLATE RETRIEVE'               from DUAL union all
           select 'POWERTAX RETRIEVE'                   from DUAL union all
           select 'TAX_ACCRUAL_PT_TRC_TMP INSERT'       from DUAL union all
           select 'TAX_ACCRUAL_PT_TC_ROLLUP_TMP INSERT' from DUAL union all
           select 'M_TRUEUP_TYPES_WINDOW'               from DUAL union all
           select 'DEFTAX_WINDOW_RETRIEVE'              from DUAL union all
           select 'DEFTAX_WINDOW_RTP_INSERT'            from DUAL union all
           select 'DEFTAX_WINDOW_RTP_UPDATE'            from DUAL union all
           select 'DEF_TAX_RETRIEVE'                    from DUAL union all
           select 'DEFTAX_CALC_RTP_INSERT'              from DUAL union all
           select 'DEFTAX_CALC_RTP_UPDATE'              from DUAL union all
           select 'FAS109_WINDOW_RETRIEVE'              from DUAL union all
           select 'FAS109_WINDOW_RTP_INSERT'            from DUAL union all
           select 'FAS109_WINDOW_RTP_UPDATE'            from DUAL union all
           select 'FAS109_RETRIEVE'                     from DUAL union all
           select 'FAS109_CALC_RTP_INSERT'              from DUAL union all
           select 'FAS109_CALC_RTP_UPDATE'              from DUAL union all
           select 'M_EST_SPREAD_TYPES_WINDOW'           from DUAL union all
           select 'M_EST_SPREAD_MS_WINDOW'              from DUAL union all
           select 'M_EST_SPREAD_TYPES_PROCESS'          from DUAL union all
           select 'M_EST_SPREAD_MS_PROCESS'             from DUAL union all
           select 'M_ITEM_CALC_RTP_INSERT'              from DUAL union all
           select 'M_ITEM_CALC_RTP_UPDATE'              from DUAL union all
           select 'M_TRUEUP_TYPES_PROCESS'              from DUAL union all
           select 'M_TRUEUP_MS_PROCESS'                 from DUAL union all
           select 'M_TRUEUP_TYPES_WINDOW'               from DUAL union all
           select 'M_TRUEUP_MS_WINDOW'                  from DUAL union all
           select 'M_ITEM_WINDOW_RTP_UPDATE'            from DUAL union all
           select 'DT_EST_SPREAD_PROCESS'               from DUAL union all
           select 'M_EST_SPREAD_TYPES_WINDOW'           from DUAL union all
           select 'DT_EST_SPREAD_WINDOW'                from DUAL) SQL_LABELS
   minus
   select SQL_NAME, 0, 'Placeholder'
     from TAX_ACCRUAL_SQL_HINTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (993, 0, 10, 4, 2, 0, 9501, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_009501_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;