/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045699_jobserver_03add_requesting_user_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 06/30/2016 Jared Watkins  Add requesting_user column to the pp_job_schedule
||										table to track who schedules a job
||============================================================================
*/

alter table pp_job_schedule add requesting_user varchar2(18);

comment on column pp_job_schedule.requesting_user is 'The User ID of the user that scheduled this job to run.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3231, 0, 2016, 1, 0, 0, 045699, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045699_jobserver_03add_requesting_user_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;