/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035317_taxrpr_always_capital.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/27/2014 Alex Pivoshenko
||============================================================================
*/

update WO_TAX_EXPENSE_TEST set ALWAYS_CAPITAL = 0 where ALWAYS_CAPITAL is null;

alter table WO_TAX_EXPENSE_TEST modify ALWAYS_CAPITAL NUMBER(22,0) DEFAULT 0 not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (910, 0, 10, 4, 2, 0, 35317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035317_taxrpr_always_capital.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;