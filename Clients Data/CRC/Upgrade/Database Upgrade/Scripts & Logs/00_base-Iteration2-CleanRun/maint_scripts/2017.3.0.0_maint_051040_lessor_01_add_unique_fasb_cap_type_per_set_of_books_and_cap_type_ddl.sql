/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051040_lessor_01_add_unique_fasb_cap_type_per_set_of_books_and_cap_type_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.3.0.0  05/01/2018 Andrew Hill      Add unique constraint to lsr_fasb_type_sob(cap_type_id, set_of_books_id)
||============================================================================
*/

ALTER TABLE lsr_fasb_type_sob add constraint lsr_fasb_type_sob_cap_sob_unq unique (cap_type_id, set_of_books_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4922, 0, 2017, 3, 0, 0, 51040, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051040_lessor_01_add_unique_fasb_cap_type_per_set_of_books_and_cap_type_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;