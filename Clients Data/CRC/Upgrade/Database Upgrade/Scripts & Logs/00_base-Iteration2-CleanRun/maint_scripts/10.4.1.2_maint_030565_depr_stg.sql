/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030565_depr_stg.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.2   09/05/2013 Charlie Shilling add defaults
||============================================================================
*/

alter table DEPR_CALC_OVER_STG
   modify (END_BALANCE             number(22,2) default 0,
           END_RESERVE             number(22,2) default 0,
           EXPENSE                 number(22,2) default 0,
           RETRO_EXPENSE           number(22,2) default 0,
           NET_SALVAGE_PCT         number(22,8) default 0,
           OVER_DEPR_AMOUNT        number(22,2) default 0,
           OVER_DEPR_CHECK_AMOUNT  number(22,2) default 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (759, 0, 10, 4, 1, 2, 30565, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_030565_depr_stg.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;