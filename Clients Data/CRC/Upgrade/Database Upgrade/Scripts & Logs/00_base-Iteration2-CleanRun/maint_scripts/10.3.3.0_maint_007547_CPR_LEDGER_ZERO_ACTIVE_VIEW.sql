/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007547_CPR_LEDGER_ZERO_ACTIVE_VIEW.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/08/2011 Luis Kramarz   Point Release
||============================================================================
*/

create or replace view CPR_LEDGER_ZERO_ACTIVE_VIEW  as
select CPR_LEDGER.ASSET_ID,
       COMPANY_ID,
       RETIREMENT_UNIT_ID,
       UTILITY_ACCOUNT_ID,
       BUS_SEGMENT_ID,
       ASSET_LOCATION_ID
  from CPR_LEDGER, CPR_LDG_BASIS
 where CPR_LEDGER.ASSET_ID = CPR_LDG_BASIS.ASSET_ID
   and ABS(BASIS_1) + ABS(BASIS_2) + ABS(BASIS_3) + ABS(BASIS_4) + ABS(BASIS_5) + ABS(BASIS_6) +
       ABS(BASIS_7) + ABS(BASIS_8) + ABS(BASIS_9) + ABS(BASIS_10) + ABS(BASIS_11) +
       ABS(BASIS_12) + ABS(BASIS_13) + ABS(BASIS_14) + ABS(BASIS_15) + ABS(BASIS_16) +
       ABS(BASIS_17) + ABS(BASIS_18) + ABS(BASIS_19) + ABS(BASIS_20) + ABS(BASIS_21) +
       ABS(BASIS_22) + ABS(BASIS_23) + ABS(BASIS_24) + ABS(BASIS_25) + ABS(BASIS_26) +
       ABS(BASIS_27) + ABS(BASIS_28) + ABS(BASIS_29) + ABS(BASIS_30) + ABS(BASIS_31) +
       ABS(BASIS_32) + ABS(BASIS_33) + ABS(BASIS_34) + ABS(BASIS_35) + ABS(BASIS_36) +
       ABS(BASIS_37) + ABS(BASIS_38) + ABS(BASIS_39) + ABS(BASIS_40) + ABS(BASIS_41) +
       ABS(BASIS_42) + ABS(BASIS_43) + ABS(BASIS_44) + ABS(BASIS_45) + ABS(BASIS_46) +
       ABS(BASIS_47) + ABS(BASIS_48) + ABS(BASIS_49) + ABS(BASIS_50) + ABS(BASIS_51) +
       ABS(BASIS_52) + ABS(BASIS_53) + ABS(BASIS_54) + ABS(BASIS_55) + ABS(BASIS_56) +
       ABS(BASIS_57) + ABS(BASIS_58) + ABS(BASIS_59) + ABS(BASIS_60) + ABS(BASIS_61) +
       ABS(BASIS_62) + ABS(BASIS_63) + ABS(BASIS_64) + ABS(BASIS_65) + ABS(BASIS_66) +
       ABS(BASIS_67) + ABS(BASIS_68) + ABS(BASIS_69) + ABS(BASIS_70) > 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (47, 0, 10, 3, 3, 0, 7547, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007547_CPR_LEDGER_ZERO_ACTIVE_VIEW.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;