/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041290_anlyt_11_sequences.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
||  1.0     09/06/2013 Scott Moody      Initial
|| 10.4.3.1 11/17/2014 Chad Theilman    Asset Analytics - Sequences
||============================================================================
*/

declare 
  doesSequenceExist number := 0;
  seqCount number := 0;
  
  begin
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRPLANT' and Object_Name = 'PA_ANALYTICS_SEQ';
	
	if seqCount = 0 then
		begin
		    execute immediate 'CREATE SEQUENCE PWRPLANT.PA_ANALYTICS_SEQ';  --NEEDED FOR APPLICATION
		end;
	end if;
		
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRPLANT' and Object_Name = 'PA_ACCOUNTS_SEQ';
	
	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRPLANT.PA_ACCOUNTS_SEQ';
		end;
	end if;
		
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRPLANT' and Object_Name = 'PA_LOCATION_AGG_SEQ';
	
	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRPLANT.PA_LOCATION_AGG_SEQ';
		end;
	end if;
		
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRPLANT' and Object_Name = 'PA_PROPERTY_SEQ';
	
	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRPLANT.PA_PROPERTY_SEQ';
		end;
	end if;
		
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRPLANT' and Object_Name = 'PA_VINTAGE_SPREADS_SEQ';

	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRPLANT.PA_VINTAGE_SPREADS_SEQ';
		end;
	end if;
	
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRPLANT' and Object_Name = 'PA_AVERAGE_AGES_SEQ';
	
	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRPLANT.PA_AVERAGE_AGES_SEQ';
		end;
	end if;
		
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRPLANT' and Object_Name = 'PA_CPR_BALANCES_SEQ';
	
	if seqCount = 0 then 
		begin
			execute immediate 'CREATE SEQUENCE PWRPLANT.PA_CPR_BALANCES_SEQ';
		end;
	end if;
  end;
/



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2029, 0, 10, 4, 3, 1, 041290, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_041290_anlyt_11_sequences.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;