/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053105_lessee_03_fy_reports_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.2 3/5/2019  C.Yura          Fix Consol report options
||============================================================================
*/

update pp_reports
set report_type_id = 312,
pp_report_time_option_id = 206,
pp_report_filter_id = 104
where lower(trim(datawindow)) = 'dw_ls_rpt_disc_maturity_det_consol_fy';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15642, 0, 2018, 1, 0, 2, 53105, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053105_lessee_03_fy_reports_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;