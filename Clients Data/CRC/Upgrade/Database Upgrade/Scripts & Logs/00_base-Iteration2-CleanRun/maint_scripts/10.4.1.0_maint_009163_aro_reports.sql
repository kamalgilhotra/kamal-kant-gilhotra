/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009163_aro_reports.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/28/2013 Brandon Beck   Point Release
||============================================================================
*/

update PP_REPORTS
   set INPUT_WINDOW = 'dw_company_select,dw_aro_report_select',
       SPECIAL_NOTE = 'already has report time, INPUT_FILTER=company.company_id'
 where REPORT_NUMBER in ('ARO - 4003', 'ARO - 4004', 'ARO - 4005');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (339, 0, 10, 4, 1, 0, 09163, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009163_aro_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
