/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032297_taxrpr_rpt_ldescr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.1.0 09/10/2013 Andrew Scott        Tax Repair Report Long Description Change
||============================================================================
*/

update PP_REPORTS
   set LONG_DESCRIPTION = 'Shows CPR tax expense and retirement reversal with a tax origination month ' ||
                           'within the selected time span. Displays the Tax Orig Month and the accounting month. Tax Repairs ' ||
                           'entries must be posted to display in this report.'
 where REPORT_NUMBER like 'RPR%0030'
   and PP_REPORT_ENVIR_ID <> 7;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (606, 0, 10, 4, 1, 0, 32297, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032297_taxrpr_rpt_ldescr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;