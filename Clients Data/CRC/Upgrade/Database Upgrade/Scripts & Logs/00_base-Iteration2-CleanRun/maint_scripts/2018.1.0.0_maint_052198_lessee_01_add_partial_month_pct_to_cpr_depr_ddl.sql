/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052198_lessee_01_add_partial_month_pct_to_cpr_depr_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/02/2018 Sarah Byers    Add partial month pct to cpr_depr_calc_stg and arc
||============================================================================
*/

alter table cpr_depr_calc_stg add (partial_month_percent number(22,8) default 1);

comment on column cpr_depr_calc_stg.partial_month_percent is 'The percentage of the first and last month in the payment term on a leased asset when the payment type is Partial Month.  All other months are 1.  Column defaults to 1.';

alter table cpr_depr_calc_stg_arc add (partial_month_percent number(22,8));

comment on column cpr_depr_calc_stg_arc.partial_month_percent is 'The percentage of the first and last month in the payment term on a leased asset when the payment type is Partial Month.  All other months are 1.  Column defaults to 1.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(11182, 0, 2018, 1, 0, 0, 52198, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052198_lessee_01_add_partial_month_pct_to_cpr_depr_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 