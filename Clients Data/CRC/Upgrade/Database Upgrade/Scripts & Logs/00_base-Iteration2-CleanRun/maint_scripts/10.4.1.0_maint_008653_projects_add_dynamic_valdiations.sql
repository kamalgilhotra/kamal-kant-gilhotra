/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008653_projects_add_dynamic_valdiations.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/22/2013 David Nichols
||============================================================================
*/

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    HARD_EDIT)
values
   (63, 'FP Reject Budget Review (Pre)', 'FP Reject Budget Review (Pre)', 'f_workflow_base',
    'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id',
    'revision', 1);

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    HARD_EDIT)
values
   (64, 'FP Reject Budget Review (Post)', 'FP Reject Budget Review (Post)', 'f_workflow_base',
    'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id',
    'revision', 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (530, 0, 10, 4, 1, 0, 8653, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_008653_projects_add_dynamic_valdiations.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
