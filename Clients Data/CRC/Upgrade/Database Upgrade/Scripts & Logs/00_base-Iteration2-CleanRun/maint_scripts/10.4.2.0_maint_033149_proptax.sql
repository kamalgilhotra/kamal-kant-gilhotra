/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033149_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   12/06/2013 Julia Breuer
||============================================================================
*/

insert into PPBASE_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Payments Center - Home - Default State', sysdate, user, 'The default state, if any, that is populated when first entering the Payments Center Home workspace.', 0, 'None', null, 1 );
insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default State', 'DDDW:dw_pp_state_filter;state_id;long_description', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default State', 'None', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default State', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default State', 'payment_home', sysdate, user );

insert into PPBASE_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Payments Center - Home - Default PT Company', sysdate, user, 'The default prop tax company, if any, that is populated when first entering the Payments Center Home workspace.', 0, 'None', null, 1 );
insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default PT Company', 'DDDW:dw_pt_prop_tax_company_filter;prop_tax_company_id;description', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default PT Company', 'None', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default PT Company', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default PT Company', 'payment_home', sysdate, user );

delete from PPBASE_SYSTEM_OPTIONS_WKSP where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Vouchered - Default State';
delete from PPBASE_SYSTEM_OPTIONS_MODULE where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Vouchered - Default State';
delete from PPBASE_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Vouchered - Default State';
delete from PPBASE_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Vouchered - Default State';

delete from PPBASE_SYSTEM_OPTIONS_WKSP where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Vouchered - Default PT Company';
delete from PPBASE_SYSTEM_OPTIONS_MODULE where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Vouchered - Default PT Company';
delete from PPBASE_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Vouchered - Default PT Company';
delete from PPBASE_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Vouchered - Default PT Company';

delete from PPBASE_SYSTEM_OPTIONS_WKSP where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Released - Default State';
delete from PPBASE_SYSTEM_OPTIONS_MODULE where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Released - Default State';
delete from PPBASE_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Released - Default State';
delete from PPBASE_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Released - Default State';

delete from PPBASE_SYSTEM_OPTIONS_WKSP where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Released - Default PT Company';
delete from PPBASE_SYSTEM_OPTIONS_MODULE where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Released - Default PT Company';
delete from PPBASE_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Released - Default PT Company';
delete from PPBASE_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Payments Center - Home - Un-Released - Default PT Company';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (788, 0, 10, 4, 2, 0, 33149, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033149_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;