/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038136_reg_case_adjustment_report.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/30/2014 Shane Ward     Add report Case Adjustment
||========================================================================================
*/

insert into PP_REPORTS_TIME_OPTION 
	(PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1, KEYCOLUMN1, 
	 DWNAME2, LABEL2, KEYCOLUMN2, DWNAME3, LABEL3, KEYCOLUMN3)
values 
	(109, 'Case + Year End + Adjustment', 'uo_reg_report_parms_case_adj', 'dw_reg_case_select_dddw', 'Reg Case', 'reg_case_id',
	 'dw_reg_case_year_select_dddw', 'Case Year End', 'coverage_yr_ended', 'dw_reg_case_adj_select_dddw', 'Adjustment', 'adjustment_id');

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_FILTER_ID)
   select max(REPORT_ID) + 1,
          'Case Adjustment Report',
          'This report displays the summary and details of a specific Case, Year, and Adjustment.',
          'Regulatory',
          'dw_reg_report_case_adjust',
          'CA - 102',
          101,
          408,
          109,
          1,
          3,
          1
     from PP_REPORTS;

update PP_REPORTS set DESCRIPTION = 'Revenue Requirement - Equity Method', long_description = 'Revenue Requirement - Equity Method' where REPORT_NUMBER = 'RR - 101';

update PP_REPORTS set DESCRIPTION = 'Revenue Requirement - Def. Method', long_description = 'Revenue Requirement - Def. Method' where REPORT_NUMBER = 'RR - 102';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1205, 0, 10, 4, 2, 6, 38136, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038136_reg_case_adjustment_report.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
