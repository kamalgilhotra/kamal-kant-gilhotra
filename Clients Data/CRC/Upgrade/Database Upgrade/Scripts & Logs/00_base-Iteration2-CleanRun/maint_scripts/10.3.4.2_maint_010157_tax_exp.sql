SET DEFINE OFF
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010157_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.2   07/02/2012 Sunjin Cone    Point Release
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: Replacement Qty Change',
          'T&D Network Repairs: Displays the change in replacement quantities for each circuit and repair unit of property.',
          'dw_repair_netwk_comp_repl_qty',
          'ALREADY HAS REPORT TIME',
          'NETWK - 0036',
          'dw_company_select',
          13,
          13,
          1,
          1,
          1,
          'NETWK - 0036',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: Replacement Cost Change',
          'Situs WO Repairs: Displays the change in replacement costs for each repair location and unit of property.',
          'dw_repair_cwip_comp_repl_cost',
          'ALREADY HAS REPORT TIME',
          'SITUS - 0037',
          'dw_company_select',
          13,
          13,
          1,
          1,
          1,
          'SITUS - 0037',
          3,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (156, 0, 10, 3, 4, 2, 10157, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.2_maint_010157_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON