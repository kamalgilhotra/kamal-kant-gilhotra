/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		 PCM
|| File Name:   42849_Final_Create_Screen.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     2/24/2015  LAlston    	      Final Create Screen Named and Placed in Workspace
||==========================================================================================
*/

Insert into PWRPLANT.PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID) 
values ('pcm','fp_create_information','Create (FP) - Information','uo_pcm_maint_wksp_info','Create Information (FP)',1);

Insert into PWRPLANT.PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID) 
values ('pcm','wo_create_information','Create (WO) - Information','uo_pcm_maint_wksp_info','Create Information (WO)',1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2324, 0, 2015, 1, 0, 0, 042849, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042849_Final_Create_Screen.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;