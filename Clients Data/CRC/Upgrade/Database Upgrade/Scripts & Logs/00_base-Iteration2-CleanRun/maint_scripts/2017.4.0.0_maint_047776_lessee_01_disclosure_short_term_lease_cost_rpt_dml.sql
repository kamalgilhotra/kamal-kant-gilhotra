/*
||============================================================================
|| Application: PowerPlan  Disclosure Reports - Short Term Lease Cost
|| File Name:   maint_047776_lessee_01_disclosure_short_term_lease_cost_rpt_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  04/25/2018 David Conway     Add new lease disclosure Short Term Lease Cost report
||============================================================================
*/

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             special_note,
             report_type,
             time_option,
             report_number,
             input_window,
             filter_option,
             status,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
             documentation,
             user_comment,
             last_approved_date,
             pp_report_number,
             old_report_number,
             dynamic_dw,
             turn_off_multi_thread)
VALUES      ( ( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Short Term Lease Cost',
             'Disclosure: Short Term Lease Cost',
             'Lessee',
             'dw_ls_rpt_disc_short_term_lease_cost',
             NULL,
             NULL,
             NULL,
             'Lessee - 2008',
             NULL,
             NULL,
             NULL,
             11,
             311,
             201,
             103,
             1,
             3,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4802, 0, 2017, 4, 0, 0, 47776, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_047776_lessee_01_disclosure_short_term_lease_cost_rpt_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;