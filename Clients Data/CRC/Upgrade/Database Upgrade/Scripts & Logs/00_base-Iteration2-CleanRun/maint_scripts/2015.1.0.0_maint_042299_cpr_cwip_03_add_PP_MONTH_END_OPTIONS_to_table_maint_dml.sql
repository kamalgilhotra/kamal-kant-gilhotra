/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042299_cpr_cwip_03_add_PP_MONTH_END_OPTIONS_to_table_maint_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/03/2015 Charlie Shilling maint-42299
||============================================================================
*/

--------------------------------
-- INSERT INTO POWERPLANT_TABLES
--------------------------------
INSERT INTO powerplant_tables (table_name, pp_table_type_id, description, long_description)
VALUES ('pp_month_end_options','c','PowerPlan Month End Options','Table to hold default selections for month end user options.');

---------------------------------
-- INSERT INTO POWERPLANT_COLUMNS
---------------------------------
INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank)
VALUES ('company_id','pp_month_end_options', 'e', 'Company ID', 30);

INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank)
VALUES ('process_id','pp_month_end_options', 's', 'Process ID', 10);

INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank)
VALUES ('option_id','pp_month_end_options', 's', 'Option ID', 20);

INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank)
VALUES ('description','pp_month_end_options', 'e', 'Description', 50);

INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank)
VALUES ('long_description','pp_month_end_options', 'e', 'Long Description', 60);

INSERT INTO powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank)
VALUES ('option_value','pp_month_end_options', 'e', 'Option Value', 40);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2268, 0, 2015, 1, 0, 0, 042299, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042299_cpr_cwip_03_add_PP_MONTH_END_OPTIONS_to_table_maint_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;