/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045426_jobserver_set_server_col_null_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2015.2.1  02/12/2016  Anand R			 Make pp_job_history.server column nullable
||											 
||============================================================================
*/ 

ALTER TABLE "PWRPLANT"."PP_JOB_HISTORY" MODIFY ( "SERVER" NULL );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3058, 0, 2015, 2, 1, 0, 045426, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045426_jobserver_set_server_col_null_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;