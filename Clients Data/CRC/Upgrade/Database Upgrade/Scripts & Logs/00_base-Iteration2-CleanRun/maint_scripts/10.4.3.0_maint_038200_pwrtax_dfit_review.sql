/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_038200_pwrtax_dfit_review.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.3.0 07/29/2014 Alex P.
||========================================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'deferred_review_tax_grid', 'Deferred Tax Grid', 'uo_tax_dfit_wksp_review', 'Deferred Tax Grid');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'deferred_review_tax_grid'
 where LABEL = 'Deferred Tax Grid'
   and MODULE = 'powertax';

--
-- Create a new filters to use for Individual Asset Depreciation.
--
insert into PP_REPORTS_FILTER ( PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME ) values ( 75, sysdate, user, 'PowerTax - DFIT Grid - Indiv Depr', 'tax_record_control', 'uo_ppbase_tab_filter_dynamic' );
insert into PP_REPORTS_FILTER ( PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME ) values ( 76, sysdate, user, 'PowerTax - DFIT Grid', 'tax_record_control', 'uo_ppbase_tab_filter_dynamic' );

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (179, 'Tax Year', 'dw', 'deferred_income_tax.tax_year', '', 'dw_tax_year_by_version_filter', 'tax_year', 'tax_year',
    'N', 0, 0, '', '', user, sysdate, 0, 0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 94, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 95, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 96, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 97, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 98, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 99, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 100, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 101, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 102, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 103, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 106, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 108, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 109, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 110, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 75, 179, user, sysdate );

insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 76, 101, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 76, 102, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 76, 103, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 76, 106, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 76, 108, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 76, 109, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 76, 110, user, sysdate );
insert into PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 76, 179, user, sysdate );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1278, 0, 10, 4, 3, 0, 38200, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038200_pwrtax_dfit_review.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;