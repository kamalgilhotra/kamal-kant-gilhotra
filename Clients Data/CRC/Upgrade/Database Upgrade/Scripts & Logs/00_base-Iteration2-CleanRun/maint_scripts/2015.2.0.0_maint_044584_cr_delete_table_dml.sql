/*
||=============================================================================
|| Application: PowerPlant
|| File Name:   maint_044584_cr_delete_table_dml.sql
||=============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||=============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- -------------------------------------
|| 2015.2   08/06/2015    Jennifer Yim      Deleted cr_crosstab_source_dw table
||                                          from powerplant_tables
||=============================================================================
*/

DELETE FROM pp_table_groups
WHERE table_name = 'cr_crosstab_source_dw';

DELETE FROM powerplant_columns
WHERE table_name = 'cr_crosstab_source_dw';

DELETE FROM powerplant_tables
WHERE table_name = 'cr_crosstab_source_dw';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2760, 0, 2015, 2, 0, 0, 044584, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044584_cr_delete_table_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;