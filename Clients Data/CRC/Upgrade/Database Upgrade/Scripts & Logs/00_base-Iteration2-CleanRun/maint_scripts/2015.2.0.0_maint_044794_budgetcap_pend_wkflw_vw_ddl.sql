/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044794_budgetcap_pend_wkflw_vw_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2.0   08/21/2015 D. Mendel        Helpful view for pending workflows
||============================================================================
*/

CREATE OR REPLACE VIEW pending_workflow_detail_view
(workflow_id, id, workflow_rule_id, workflow_rule_desc, rule_order, users, date_approved, user_approved, approval_status_id, required, notes, authority_limit,
num_approvers, num_required, SQL, allow_edit, orig_authority_limit, is_custom_limit, mobile_approval, group_approval, sql_users, user_id, time_stamp, date_sent)
AS SELECT pending.workflow_id, pending.id, pending.workflow_rule_id, pending.workflow_rule_desc, pending.rule_order, pending.users, pending.date_approved, 
pending.user_approved, pending.approval_status_id, pending.required, pending.notes, pending.authority_limit, pending.num_approvers, pending.num_required, 
pending.SQL, pending.allow_edit, pending.orig_authority_limit, pending.is_custom_limit, pending.mobile_approval, pending.group_approval, pending.sql_users, 
pending.user_id, pending.time_stamp, prev_approved.date_approved date_sent
FROM workflow_detail pending, workflow_detail prev_approved
WHERE pending.workflow_id = prev_approved.workflow_id
AND pending.approval_status_id = 2
AND prev_approved.rule_order = (
  SELECT Max(rule_order)
  FROM workflow_detail pending2
  WHERE pending.workflow_id = pending2.workflow_id
  AND pending.rule_order > pending2.rule_order)
AND pending.rule_order <> (
  SELECT Min(rule_order)
  FROM workflow_detail pending2
  WHERE pending.workflow_id = pending2.workflow_id)
UNION ALL
SELECT pending.workflow_id, pending.id, pending.workflow_rule_id, pending.workflow_rule_desc, pending.rule_order, pending.users, pending.date_approved, 
pending.user_approved, pending.approval_status_id, pending.required, pending.notes, pending.authority_limit, pending.num_approvers, pending.num_required, 
pending.SQL, pending.allow_edit, pending.orig_authority_limit, pending.is_custom_limit, pending.mobile_approval, pending.group_approval, pending.sql_users, 
pending.user_id, pending.time_stamp, workflow.date_sent date_sent
FROM workflow_detail pending, workflow
WHERE pending.workflow_id = workflow.workflow_id
AND pending.approval_status_id = 2
AND pending.rule_order = (
  SELECT Min(rule_order)
  FROM workflow_detail pending2
  WHERE pending.workflow_id = pending2.workflow_id);
  
  

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2819, 0, 2015, 2, 0, 0, 044794, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044794_budgetcap_pend_wkflw_vw_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;