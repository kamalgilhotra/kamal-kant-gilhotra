/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032873_lease_class_code.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Ryan Oliveria  Patch Release
||============================================================================
*/

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
   select 'mla_indicator',
          'class_code',
          'yes_no',
          'p',
          null,
          'MLA Indicator',
          null,
          max(COLUMN_RANK) + 1,
          null,
          0
     from POWERPLANT_COLUMNS
    where TABLE_NAME = 'class_code';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
   select 'ilr_indicator',
          'class_code',
          'yes_no',
          'p',
          null,
          'ILR Indicator',
          null,
          max(COLUMN_RANK) + 1,
          null,
          0
     from POWERPLANT_COLUMNS
    where TABLE_NAME = 'class_code';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
   select 'asset_indicator',
          'class_code',
          'yes_no',
          'p',
          null,
          'Asset Indicator',
          null,
          max(COLUMN_RANK) + 1,
          null,
          0
     from POWERPLANT_COLUMNS
    where TABLE_NAME = 'class_code';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (642, 0, 10, 4, 1, 1, 32873, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032873_lease_class_code.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
