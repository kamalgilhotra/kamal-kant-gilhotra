/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052859_lessee_03_update_default_disc_control_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 01/22/2019 Shane "C" Ward Update Default discount rate mapping level system control to include company in description
||============================================================================
*/

Update pp_system_control_company 
	SET long_description = 'Mapping Level of Default Discount Rate Types (ILR Group, Lease Group, or Company)' 
WHERE control_name = 'Lessee Default Disc Rate Map';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(14162, 0, 2018, 2, 0, 0, 52859, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052859_lessee_03_update_default_disc_control_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;