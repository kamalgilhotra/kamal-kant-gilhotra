/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047753_reg_integration_col_width_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/03/2017 Shane Ward		 	Allow users to specify Col Widths in DW's on Reg Mapping Screens
||============================================================================
*/

create table REG_INTERFACE_COL_WIDTH (
REG_SOURCE_ID NUMBER(22,0) NOT NULL,
HIST_OR_FCST VARCHAR2(1) NOT NULL,
USERNAME VARCHAR2(18) NOT NULL,
FIELD_NAME VARCHAR2(254) NOT NULL,
DESCRIPTION VARCHAR2(254),
COL_WIDTH NUMBER(22,0),
USER_ID VARCHAR2(18),
TIME_STAMP DATE);

ALTER TABLE REG_INTERFACE_COL_WIDTH
  ADD CONSTRAINT pk_REG_INTERFACE_COL_WIDTH PRIMARY KEY (
    REG_SOURCE_ID, HIST_OR_FCST, USERNAME, FIELD_NAME
  )using index tablespace PWRPLANT_IDX;

ALTER TABLE REG_INTERFACE_COL_WIDTH
  ADD CONSTRAINT R_REG_INTERFACE_COL_WIDTH_1 FOREIGN KEY (
    REG_SOURCE_ID
  ) REFERENCES REG_SOURCE (
    REG_SOURCE_ID
  )
;

comment on table pwrplant.reg_Interface_col_width is '(S) [19] Houses column specific widths by Reg Source for each User for mapping workspaces';

comment on column reg_Interface_col_width.reg_source_id is 'System Assigned ID of Integration Source';
comment on column reg_Interface_col_width.hist_or_fcst is 'Identifier of whether the configuration saved is for the Historic or Forecast version of integration. Values can be "H" or "F"';
comment on column reg_Interface_col_width.USERNAME is 'User the column width is specified for';
comment on column reg_Interface_col_width.FIELD_NAME is 'Identifier of the column for the Source';
comment on column reg_Interface_col_width.DESCRIPTION is 'Description of the column displayed to the user (ex. gl_account vs GL Account)';
comment on column reg_Interface_col_width.COL_WIDTH is 'Width of column specified in PB Units';
comment on column reg_Interface_col_width.USER_ID is  'Standard system-assigned user id used for audit purposes';
comment on column reg_Interface_col_width.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3476, 0, 2017, 1, 0, 0, 47753, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047753_reg_integration_col_width_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;