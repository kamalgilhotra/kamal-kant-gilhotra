/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_006330_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/26/2011 Blake Andrews  Point Release
||============================================================================
*/

/* ###MAINT(6330) */

--add CWIP M Item ID to PowerTax interface tables

alter table PWRPLANT.TAX_ACCRUAL_POWERTAX_MAP
   add MID_CWIP number(22,0);

alter table PWRPLANT.TAX_ACCRUAL_POWERTAX_ERRORS
   add MID_CWIP_ERROR number(22,0);

alter table PWRPLANT.TAX_ACCRUAL_POWERTAX_RETRIEVE
   add (MID_CWIP         number(22,0),
        MID_CONTROL_CWIP number(22,0));

alter table PWRPLANT.TAX_ACCRUAL_POWERTAX_ARC
   add (MID_CWIP         number(22,0),
        MID_CONTROL_CWIP number(22,0));

drop table PWRPLANT.TAX_ACCRUAL_POWERTAX_ERRORS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (19, 0, 10, 3, 3, 0, 6330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_006330_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
