/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_depr_cpr_import_update.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   02/05/2013 Stephen Wicks  Point Release
||============================================================================
*/

update PWRPLANT.PP_IMPORT_TYPE
   set ARCHIVE_ADDITIONAL_COLUMNS = 'accum_cost, accum_quantity, activity_cost, activity_status, asset_activity_id, depr_group_id, func_class_id, func_class_id_xlate, orig_asset_activity_id, tax_disposition_code, tax_orig_month_number, time_stamp, user_id, wip_computation_id'
 where IMPORT_TYPE_ID = 53;

update PWRPLANT.PP_IMPORT_TYPE
   set ARCHIVE_ADDITIONAL_COLUMNS = 'accum_cost, accum_cost_2, accum_quantity, activity_cost, activity_status, asset_activity_id, asset_location_id, asset_location_id_xlate, books_schema_id, books_schema_id_xlate,   bus_segment_id, bus_segment_id_xlate, depr_group_id, description, func_class_id, func_class_id_xlate, gl_account_id, gl_account_id_xlate, in_service_year, ledger_status, long_description, orig_asset_activity_id, property_group_id, property_group_id_xlate, retirement_unit_id, retirement_unit_id_xlate, second_financial_cost, serial_number, sub_account_id, sub_account_id_xlate, subledger_indicator, subledger_indicator_xlate, tax_disposition_code, tax_orig_month_number, time_stamp, user_id, utility_account_id, utility_account_id_xlate, wip_computation_id, work_order_number'
 where IMPORT_TYPE_ID = 54;

update PWRPLANT.PP_IMPORT_TYPE
   set ARCHIVE_ADDITIONAL_COLUMNS = 'dr_comment_id, mort_mem_mort_curve_id_xlate, time_stamp, user_id'
 where IMPORT_TYPE_ID = 52;

update PWRPLANT.PP_IMPORT_TYPE
   set ARCHIVE_ADDITIONAL_COLUMNS = 'additions, adjustments, begin_balance, calc_end_reserve, end_balance, retirements, transfers_in, transfers_out'
 where IMPORT_TYPE_ID = 50;

update PWRPLANT.PP_IMPORT_TYPE
   set ARCHIVE_ADDITIONAL_COLUMNS = 'asset_dollars, beg_asset_dollars, calc_end_reserve, net_adds_and_adjust, retirements, transfers_in, transfers_out'
 where IMPORT_TYPE_ID = 51;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (291, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_depr_cpr_import_update.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
