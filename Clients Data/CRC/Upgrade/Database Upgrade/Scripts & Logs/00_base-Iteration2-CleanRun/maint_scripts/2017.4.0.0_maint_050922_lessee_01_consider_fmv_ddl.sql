/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050922_lessee_01_consider_fmv_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 04/20/2018 Josh Sandler     Add Consider FMV option
||============================================================================
*/

ALTER TABLE LS_FASB_CAP_TYPE_SOB_MAP
  ADD CONSIDER_FMV NUMBER(1) NULL;

COMMENT ON COLUMN LS_FASB_CAP_TYPE_SOB_MAP.consider_fmv IS 'Whether FMV should be used to determine ROU/Liability (1=yes, 0=no)';

ALTER TABLE LS_FASB_CAP_TYPE_SOB_MAP
  ADD CONSTRAINT fk_ls_fasb_ct_fmv FOREIGN KEY (
    CONSIDER_FMV
  ) REFERENCES yes_no (
    yes_no_id
  );

ALTER TABLE LS_ILR_STG
  ADD CONSIDER_FMV NUMBER(1) NULL;

COMMENT ON COLUMN LS_ILR_STG.consider_fmv IS 'Whether FMV should be used to determine ROU/Liability (1=yes, 0=no)';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4862, 0, 2017, 4, 0, 0, 50922, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050922_lessee_01_consider_fmv_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;