set serveroutput on;

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_anlyt_05_user.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/24/2014 Chad Theilman    Asset Analytics - User
||============================================================================
*/

declare 
	doesUserExist number := 0;
	
	begin
		doesUserExist := PWRPLANT.SYS_DOES_USER_EXIST('pwranlyt');
		
		if doesUserExist = 0 then
			begin
				-- User creation moved to v10_sys_grants.sql and dbsetup.sql
				
				INSERT INTO PWRPLANT.PP_SECURITY_USERS 
					(USERS, TIME_STAMP, USER_ID, DESCRIPTION, STATUS, DEL_COUNT, LANGUAGE_ID, MY_POWERPLANT_OPEN, MY_POWERPLANT_CLOSE_ALL, 
					MY_POWERPLANT_FONT, MY_POWERPLANT_TEXT_COLOR, MY_POWERPLANT_BACK_COLOR, MY_POWERPLANT_FONT_SIZE) 
					VALUES ('pwranlyt', TO_DATE('31-JUL-13', 'DD-MON-RR'), 'PWRPLANT', 'ANALYTICS ADMIN', '1', '0', '1', '1', '0', 
					'MS San Serif', '0', '14595452', '11');

				/*INSERT INTO PWRPLANT.PP_WEB_AUTHENTICATION 
				(AUTHENTICATION_NAME, AUTHENTICATION_TYPE, IS_ACTIVE, MAX_FAILED_ATTEMPTS, EXPIRE_LOCKOUT_MINUTES, MUST_CHANGE_PASSWORD_DAYS, 
				PASSWORD_TYPE, PASSWORD_ALLOWED_CHARACTERS, PASSWORD_CONTAINS_CHARACTERS, 
				PASSWORD_MINIMUM_LENGTH, PASSWORD_MAXIMUM_LENGTH, PASSWORD_REUSE_COUNT, MODULE_ID) 
				VALUES ('DIRECT-DB', 'DATABASE', '1', '5', '60', '0', 'TEXT', ' ', ' ', '0', '0', '0', '3');*/

				INSERT INTO PWRPLANT.PP_WEB_AUTHENTICATION_USERS 
				(AUTHENTICATION_NAME, USERS, ACCOUNT, IS_ACTIVE, IS_LOCKED_OUT, FAILED_LOGIN_ATTEMPTS, TIME_STAMP) 
				VALUES ('DIRECT-DB', 'pwranlyt', 'pwranlyt', '1', '0', '0', TO_DATE('31-JUL-13', 'DD-MON-RR'));
			end;
		end If;
	end;
/

GRANT SELECT ON PWRPLANT.COMPANY_SETUP TO pwranlyt WITH GRANT OPTION
/
GRANT SELECT ON PWRPLANT.PP_COMPANY_SECURITY TO pwranlyt WITH GRANT OPTION
/
GRANT SELECT ON PWRPLANT.PP_COMPANY_SECURITY_TEMP TO pwranlyt WITH GRANT OPTION
/
GRANT SELECT ON PWRPLANT.MORTALITY_CURVE_POINTS TO pwranlyt
/
GRANT PWRPLANT_ROLE_SELECT TO pwranlyt
/
GRANT PWRPLANT_ROLE_DEV TO pwranlyt
/
  
--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1570, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_anlyt_05_user.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;