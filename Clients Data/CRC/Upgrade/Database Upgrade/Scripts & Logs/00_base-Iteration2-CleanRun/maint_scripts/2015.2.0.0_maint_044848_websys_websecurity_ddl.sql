/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		System (Web Framework)
|| File Name:   maint_044848_websys_websecurity_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------  -----------------------------------------------
|| 2015.2     08/31/2015 Khamchanh Sisouphanh Creating new tables for Web Security
||==========================================================================================
*/

ALTER TABLE PP_WEB_SECURITY_USERS_GROUPS DROP CONSTRAINT FK_PP_WEBSECURITYUSERS_GROUPSU;

ALTER TABLE PP_WEB_SECURITY_USERS DROP CONSTRAINT PK_PP_WEB_SECURITY_USERS;

DROP INDEX PK_PP_WEB_SECURITY_USERS; 

DROP TABLE PP_WEB_SECURITY_USERS;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2840, 0, 2015, 2, 0, 0, 044848, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044848_websys_websecurity_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;