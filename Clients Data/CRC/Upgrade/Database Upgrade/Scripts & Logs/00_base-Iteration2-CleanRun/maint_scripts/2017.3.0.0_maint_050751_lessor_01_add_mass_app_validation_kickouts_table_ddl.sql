/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050751_lessor_01_add_mass_app_validation_kickouts_table_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/12/2018 Andrew Hill       Add table for business validation kickouts and update view
||============================================================================
*/

CREATE SEQUENCE lsr_ilr_massappval_kickoutseq;
CREATE TABLE lsr_ilr_mass_app_val_kickouts (kickout_id number(22,0),
                                            ilr_id NUMBER(22,0),
                                            revision NUMBER(22,0),
                                            user_id VARCHAR2(18),
                                            time_stamp date,
                                            message VARCHAR2(4000));
ALTER TABLE lsr_ilr_mass_app_val_kickouts add constraint lsr_ilr_massappvalkickout_pk primary key (kickout_id) using index tablespace pwrplant_idx;

COMMENT ON TABLE lsr_ilr_mass_app_val_kickouts IS 'Table to hold business validation kickouts during Lessor Mass Approval Process';
COMMENT ON COLUMN lsr_ilr_mass_app_val_kickouts.kickout_id IS 'System-generated unique ID (from lsr_ilr_massappval_kickoutseq)';
COMMENT ON COLUMN lsr_ilr_mass_app_val_kickouts.ilr_id IS 'The ILR ID with the kickout';
COMMENT ON COLUMN lsr_ilr_mass_app_val_kickouts.revision IS 'The ILR Revision with the kickout';
COMMENT ON COLUMN lsr_ilr_mass_app_val_kickouts.user_id IS 'System-assigned user ID';
COMMENT ON COLUMN lsr_ilr_mass_app_val_kickouts.time_stamp IS 'System-assigned timestamp';
COMMENT ON COLUMN lsr_ilr_mass_app_val_kickouts.message IS 'Message describing the reason for the kickout';

                                            
CREATE OR REPLACE VIEW V_LSR_ILR_MASS_APP_KICKOUTS AS
SELECT DISTINCT ilr_id, revision, occurrence_id, message
FROM lsr_ilr_schedule_kickouts
UNION ALL
SELECT DISTINCT ilr_id, revision, occurrence_id, message
FROM lsr_ilr_approval_kickouts
UNION ALL
SELECT DISTINCT ilr_id, revision, NULL, message
FROM lsr_ilr_mass_app_val_kickouts;

INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4512, 0, 2017, 3, 0, 0, 50751, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050751_lessor_01_add_mass_app_validation_kickouts_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;