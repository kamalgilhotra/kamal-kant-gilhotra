/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050531_taxrpr_01_wo_status_pkg_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 08/01/2017 Eric Berger    Create the wo_status processing tables
||										and pl/sql package.
||============================================================================
*/

BEGIN
    EXECUTE IMMEDIATE 'create table WORK_ORDER_TAX_STATUS_PROCESS
(
  batch_id                       NUMBER(22) not null,
  work_order_id                  NUMBER(22) not null,
  final_tax_status_id            NUMBER(22),
  tax_status_id                  NUMBER(22),
  new_tax_status_id              NUMBER(22),
  tax_status_debug               VARCHAR2(10),
  tax_status_priority            NUMBER(22),
  new_tax_status_priority        NUMBER(22),
  tax_status_desc                VARCHAR2(35),
  new_tax_status_desc            VARCHAR2(35),
  test_tax_status_id             NUMBER(22),
  tax_expense_test_id            NUMBER(22),
  repair_schema_id               NUMBER(22),
  usage_flag                     VARCHAR2(35),
  work_order_number              VARCHAR2(35),
  te_fund_proj                   NUMBER(22),
  processing_period_id           NUMBER(22),
  relation_id                    NUMBER(22),
  company_id                     NUMBER(22),
  company_description            VARCHAR2(100),
  current_open_month_number      NUMBER(22),
  repair_exclude                 NUMBER(22),
  tax_status_default_capital     NUMBER(22),
  effective_accounting_month     DATE,
  month_number                   NUMBER(22),
  work_order_type_id             NUMBER(22),
  funding_wo_indicator           NUMBER(22),
  funding_wo_id                  NUMBER(22),
  asset_location_id              NUMBER(22),
  major_location_id              NUMBER(22),
  repair_location_id             NUMBER(22),
  mixed_repair_location_id       NUMBER(22),
  repair_location_method_id      NUMBER(22),
  temp_tax_test_id               NUMBER(22),
  est_percent                    NUMBER(22,8),
  tax_status_from_fp             NUMBER(22),
  estimate_dollars_vs_units      VARCHAR2(35),
  est_revision                   VARCHAR2(20),
  revision                       NUMBER(22),
  wo_current_revision            NUMBER(22),
  tax_status_no_replacement      NUMBER(22),
  tax_status_fp_fully_qualifying NUMBER(22),
  tax_status_fp_non_qualifying   NUMBER(22),
  tax_status_ovh_to_udg          NUMBER(22),
  tax_status_minor_uop           NUMBER(22),
  add_retire_tolerance_pct       NUMBER(22,8),
  tax_status_over_tolerance      NUMBER(22),
  tax_status_over_max_amount     NUMBER(22),
  te_aggregation_id              NUMBER(22),
  total_cost_debug               VARCHAR2(10),
  total_cost                     NUMBER(22,2),
  total_cost_retire              NUMBER(22,2),
  total_quantity_debug           VARCHAR2(10),
  total_quantity                 NUMBER(22,2),
  total_quantity_retire          NUMBER(22,2),
  replacement_cost               NUMBER(22,2),
  unit_of_measure_id             NUMBER(22),
  quantity_vs_cost               NUMBER(2),
  add_retire_ratio               NUMBER(22,8),
  count_minor_uop                NUMBER(22,8),
  count_major_uop                NUMBER(22,8),
  estimate_total_by_uop          VARCHAR2(10),
  estimated_costs                NUMBER(22,2),
  test_replace_cost              NUMBER(22,2),
  test_date                      DATE,
  max_proj_amt                   NUMBER(22,2),
  replacement_quantity           NUMBER(22,8),
  base_year                      NUMBER(22),
  reviewed_date                  DATE,
  review_user                    VARCHAR2(18),
  external_reviewed_date         DATE,
  external_review_user           VARCHAR2(18),
  override_note                  VARCHAR2(254),
  processing_month               NUMBER(22),
  fp_percent_expense             NUMBER(22,8),
  user_id                        VARCHAR2(18),
  time_stamp                     DATE,
  estimated_qty                  NUMBER(22),
  test_replace_qty               NUMBER(22),
  tested_actuals                 NUMBER(22),
  use_actuals                    NUMBER(22),
  replacement_flag               NUMBER(22),
  replacement_source             VARCHAR2(35),
  replacement_source_debug       VARCHAR2(35),
  replacement_flag_1             VARCHAR2(10),
  replacement_flag_2             VARCHAR2(10),
  replacement_flag_3             VARCHAR2(10),
  replacement_flag_4             VARCHAR2(10),
  replacement_flag_5             VARCHAR2(10),
  repair_unit_code_id            NUMBER(22),
  repair_unit_source             VARCHAR2(35),
  repair_unit_source_debug       VARCHAR2(35),
  repair_unit_code_1             VARCHAR2(10),
  repair_unit_code_2             VARCHAR2(10),
  repair_unit_code_3             VARCHAR2(10),
  repair_unit_code_4             VARCHAR2(10),
  repair_unit_code_5             VARCHAR2(10),
  repair_unit_code_6             VARCHAR2(10),
  oh_ug                          VARCHAR2(10),
  oh_ug_count                    NUMBER(22),
  ruc_func_class_count           NUMBER(22),
  sum_estimate_rows              VARCHAR2(10),
  range_test_id                  NUMBER(22),
  hw_table_line_id               NUMBER(22),
  hw_region_id                   NUMBER(22),
  base_rate                      NUMBER(22,8),
  new_rate                       NUMBER(22,8),
  hw_region_descr                VARCHAR2(35),
  hw_line_descr                  VARCHAR2(35),
  hw_allow_non_index_repairs     VARCHAR2(10),
  start_year_month               NUMBER(22),
  end_year_month                 NUMBER(22),
  reviewed_flag                  NUMBER(22),
  external_reviewed_flag         NUMBER(22),
  test_explanation               VARCHAR2(2000),
  logfile                        VARCHAR2(2000)
)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Table already exists.');
END;
/

-- Create/Recreate indexes
BEGIN
    EXECUTE IMMEDIATE 'create index IDX0_WO_TAX_STATUS_PROCESS on WORK_ORDER_TAX_STATUS_PROCESS (WORK_ORDER_ID)
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE ' create index IDX1_WO_TAX_STATUS_PROCESS on WORK_ORDER_TAX_STATUS_PROCESS (NEW_TAX_STATUS_ID)
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'create index IDX2_WO_TAX_STATUS_PROCESS on WORK_ORDER_TAX_STATUS_PROCESS (REPAIR_EXCLUDE)
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'create index IDX3_WO_TAX_STATUS_PROCESS on WORK_ORDER_TAX_STATUS_PROCESS (USE_ACTUALS)
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'create index IDX4_WO_TAX_STATUS_PROCESS on WORK_ORDER_TAX_STATUS_PROCESS (REPAIR_UNIT_CODE_ID)
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'create index IDX5_WO_TAX_STATUS_PROCESS on WORK_ORDER_TAX_STATUS_PROCESS (QUANTITY_VS_COST)
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'create index IDX6_WO_TAX_STATUS_PROCESS on WORK_ORDER_TAX_STATUS_PROCESS (ESTIMATE_DOLLARS_VS_UNITS)
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'create index IDX7_WO_TAX_STATUS_PROCESS on WORK_ORDER_TAX_STATUS_PROCESS (EST_REVISION)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/

-- Create/Recreate primary, unique and foreign key constraints
BEGIN
    EXECUTE IMMEDIATE 'alter table WORK_ORDER_TAX_STATUS_PROCESS
  add constraint WO_TAX_STATUS_PROCESS_PK primary key (BATCH_ID, WORK_ORDER_ID)
  using index
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint already exists.');
END;
/

-- Create table
BEGIN
    EXECUTE IMMEDIATE 'create table WORK_ORDER_TAX_STATUS_PROC_ARC
(
  batch_id                       NUMBER(22) not null,
  work_order_id                  NUMBER(22) not null,
  final_tax_status_id            NUMBER(22),
  tax_status_id                  NUMBER(22),
  new_tax_status_id              NUMBER(22),
  tax_status_debug               VARCHAR2(10),
  tax_status_priority            NUMBER(22),
  new_tax_status_priority        NUMBER(22),
  tax_status_desc                VARCHAR2(35),
  new_tax_status_desc            VARCHAR2(35),
  test_tax_status_id             NUMBER(22),
  tax_expense_test_id            NUMBER(22),
  repair_schema_id               NUMBER(22),
  usage_flag                     VARCHAR2(35),
  work_order_number              VARCHAR2(35),
  te_fund_proj                   NUMBER(22),
  processing_period_id           NUMBER(22),
  relation_id                    NUMBER(22),
  company_id                     NUMBER(22),
  company_description            VARCHAR2(100),
  current_open_month_number      NUMBER(22),
  repair_exclude                 NUMBER(22),
  tax_status_default_capital     NUMBER(22),
  effective_accounting_month     DATE,
  month_number                   NUMBER(22),
  work_order_type_id             NUMBER(22),
  funding_wo_indicator           NUMBER(22),
  funding_wo_id                  NUMBER(22),
  asset_location_id              NUMBER(22),
  major_location_id              NUMBER(22),
  repair_location_id             NUMBER(22),
  mixed_repair_location_id       NUMBER(22),
  repair_location_method_id      NUMBER(22),
  temp_tax_test_id               NUMBER(22),
  est_percent                    NUMBER(22,8),
  tax_status_from_fp             NUMBER(22),
  estimate_dollars_vs_units      VARCHAR2(35),
  est_revision                   VARCHAR2(20),
  revision                       NUMBER(22),
  wo_current_revision            NUMBER(22),
  tax_status_no_replacement      NUMBER(22),
  tax_status_fp_fully_qualifying NUMBER(22),
  tax_status_fp_non_qualifying   NUMBER(22),
  tax_status_ovh_to_udg          NUMBER(22),
  tax_status_minor_uop           NUMBER(22),
  add_retire_tolerance_pct       NUMBER(22,8),
  tax_status_over_tolerance      NUMBER(22),
  tax_status_over_max_amount     NUMBER(22),
  te_aggregation_id              NUMBER(22),
  total_cost_debug               VARCHAR2(10),
  total_cost                     NUMBER(22,2),
  total_cost_retire              NUMBER(22,2),
  total_quantity_debug           VARCHAR2(10),
  total_quantity                 NUMBER(22,2),
  total_quantity_retire          NUMBER(22,2),
  replacement_cost               NUMBER(22,2),
  unit_of_measure_id             NUMBER(22),
  quantity_vs_cost               NUMBER(2),
  add_retire_ratio               NUMBER(22,8),
  count_minor_uop                NUMBER(22,8),
  count_major_uop                NUMBER(22,8),
  estimate_total_by_uop          VARCHAR2(10),
  estimated_costs                NUMBER(22,2),
  test_replace_cost              NUMBER(22,2),
  test_date                      DATE,
  max_proj_amt                   NUMBER(22,2),
  replacement_quantity           NUMBER(22,8),
  base_year                      NUMBER(22),
  reviewed_date                  DATE,
  review_user                    VARCHAR2(18),
  external_reviewed_date         DATE,
  external_review_user           VARCHAR2(18),
  override_note                  VARCHAR2(254),
  processing_month               NUMBER(22),
  fp_percent_expense             NUMBER(22,8),
  user_id                        VARCHAR2(18),
  time_stamp                     DATE,
  estimated_qty                  NUMBER(22),
  test_replace_qty               NUMBER(22),
  tested_actuals                 NUMBER(22),
  use_actuals                    NUMBER(22),
  replacement_flag               NUMBER(22),
  replacement_source             VARCHAR2(35),
  replacement_source_debug       VARCHAR2(35),
  replacement_flag_1             VARCHAR2(10),
  replacement_flag_2             VARCHAR2(10),
  replacement_flag_3             VARCHAR2(10),
  replacement_flag_4             VARCHAR2(10),
  replacement_flag_5             VARCHAR2(10),
  repair_unit_code_id            NUMBER(22),
  repair_unit_source             VARCHAR2(35),
  repair_unit_source_debug       VARCHAR2(35),
  repair_unit_code_1             VARCHAR2(10),
  repair_unit_code_2             VARCHAR2(10),
  repair_unit_code_3             VARCHAR2(10),
  repair_unit_code_4             VARCHAR2(10),
  repair_unit_code_5             VARCHAR2(10),
  repair_unit_code_6             VARCHAR2(10),
  oh_ug                          VARCHAR2(10),
  oh_ug_count                    NUMBER(22),
  ruc_func_class_count           NUMBER(22),
  sum_estimate_rows              VARCHAR2(10),
  range_test_id                  NUMBER(22),
  hw_table_line_id               NUMBER(22),
  hw_region_id                   NUMBER(22),
  base_rate                      NUMBER(22,8),
  new_rate                       NUMBER(22,8),
  hw_region_descr                VARCHAR2(35),
  hw_line_descr                  VARCHAR2(35),
  hw_allow_non_index_repairs     VARCHAR2(10),
  start_year_month               NUMBER(22),
  end_year_month                 NUMBER(22),
  reviewed_flag                  NUMBER(22),
  external_reviewed_flag         NUMBER(22),
  test_explanation               VARCHAR2(2000),
  logfile                        VARCHAR2(2000)
)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Table already exists.');
END;
/

-- Create/Recreate primary, unique and foreign key constraints
BEGIN
    EXECUTE IMMEDIATE 'alter table WORK_ORDER_TAX_STATUS_PROC_ARC
  add constraint WO_TAX_STATUS_PROC_ARC_PK primary key (BATCH_ID, WORK_ORDER_ID)
  using index
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'create index IDX0_WO_TAX_STATUS_PROC_ARC on WORK_ORDER_TAX_STATUS_PROC_ARC (WORK_ORDER_ID)
  tablespace PWRPLANT_IDX';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already exists.');
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13739, 0, 2018, 2, 0, 0, 50531, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_050531_taxrpr_01_wo_status_pkg_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;