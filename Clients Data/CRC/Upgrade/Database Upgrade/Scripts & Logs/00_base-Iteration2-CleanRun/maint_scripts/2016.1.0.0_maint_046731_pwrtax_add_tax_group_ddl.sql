/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046731_pwrtax_add_tax_group_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 10/24/2016 Jared Watkins  Add the tax_group table for any clients missing it
||============================================================================
*/
begin
  execute immediate 'CREATE TABLE tax_group ( ' ||
    'tax_group_id NUMBER(22,0) NOT NULL, ' ||
    'time_stamp   DATE         NULL, ' ||
    'user_id      VARCHAR2(18) NULL, ' ||
    'description  VARCHAR2(35) NULL ' ||
  ')';

  DBMS_OUTPUT.PUT_LINE('Sucessfully created tax_group table.');
exception
  when others then
      if sqlcode = -955 then
        --955 is 'name already used by existing object',
          -- which means we have already created the table, so do nothing
        DBMS_OUTPUT.PUT_LINE('Table already exists. No action necessary.');
      else
        RAISE_APPLICATION_ERROR(-20000,
                                'Could not create table tax_group. SQL Error: ' || sqlerrm);
      end if;
end;
/

begin
  execute immediate 'ALTER TABLE tax_group
    ADD CONSTRAINT pk_tax_group PRIMARY KEY (
      tax_group_id
    )
    USING INDEX
      TABLESPACE pwrplant_idx';

  DBMS_OUTPUT.PUT_LINE('Sucessfully created tax_group PK.');
exception
  when others then
      if sqlcode = -2260 then
        --2260 is 'table can only have one primary key',
          -- which means we have already added that PK, so do nothing
        DBMS_OUTPUT.PUT_LINE('Primary key already exists. No action necessary.');
      else
        RAISE_APPLICATION_ERROR(-20000,
                                'Could not create primary key on tax_group. SQL Error: ' || sqlerrm);
      end if;
end;
/

COMMENT ON TABLE tax_group IS '(S)[09] Table used at some clients to divide/report tax values by groups.';

COMMENT ON COLUMN tax_group.tax_group_id IS 'Unique value for this specific Tax Group.';
COMMENT ON COLUMN tax_group.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN tax_group.user_id IS 'Standard System-assigned user_id used for audit purposes.';
COMMENT ON COLUMN tax_group.description IS 'Description of the Tax Group';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3327, 0, 2016, 1, 0, 0, 046731, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046731_pwrtax_add_tax_group_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;