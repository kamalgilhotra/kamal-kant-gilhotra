/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047721_lessee_01_add_new_disc_rpt_ls_options_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/14/2018  Alex Healey    Add new disclosure report for displaying the liability, ROU Asset and Estimated in service date for ilrs
||============================================================================
*/

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Lease Options',
             'Disclosure: Displays the lease options for renewal and termination',
             'Lessee',
             'dw_ls_rpt_disclosure_ls_options',
             'Lessee - 2014',
             11,
             311,
             206,
             103,
             1,
             3);
			 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5462, 0, 2017, 4, 0, 0, 47721, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_047721_lessee_01_add_new_disc_rpt_ls_options_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;