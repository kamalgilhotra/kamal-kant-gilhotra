/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050748_lessor_02_add_new_col_to_kickouts_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------  ------------------------------------
|| 2017.3.0.0 04/12/2018 Anand R        Add the new column occurrence_id to mass approval kickout tables
||============================================================================
*/

alter table LSR_ILR_SCHEDULE_KICKOUTS 
add OCCURRENCE_ID number(22,0);

comment on column LSR_ILR_SCHEDULE_KICKOUTS.OCCURRENCE_ID is 'system assigned identifier of a particular occurrence of a process.';

alter table LSR_ILR_APPROVAL_KICKOUTS 
add OCCURRENCE_ID number(22,0);

comment on column LSR_ILR_APPROVAL_KICKOUTS.OCCURRENCE_ID is 'system assigned identifier of a particular occurrence of a process.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4313, 0, 2017, 3, 0, 0, 50748, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050748_lessor_02_add_new_col_to_kickouts_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
