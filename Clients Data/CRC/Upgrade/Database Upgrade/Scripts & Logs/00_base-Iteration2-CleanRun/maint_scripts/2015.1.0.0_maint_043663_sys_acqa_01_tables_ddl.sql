
/*
 ||============================================================================
 || Application: acqaider_app
 || File Name: maint_043663_sys_acqa_01_tables_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2015.1.0.0 03/20/2015 Sherri Ramson  <New tables for stand alone acqaider app>
 ||============================================================================
 */

 --
 --Main Tables
 --
CREATE TABLE acqa_wizard (
  acqa_wizard_id   NUMBER(22,0)  NOT NULL,
  time_stamp       DATE          NULL,
  user_id          VARCHAR2(18)  NULL,
  description      VARCHAR2(35)  NOT NULL,
  long_description VARCHAR2(254) NOT NULL,
  step_number      NUMBER(22,0)  NOT NULL,
  object_name      VARCHAR2(254) NULL,
  identifier       VARCHAR2(35)  NULL,
  visible          NUMBER(22,0)  NULL
)
/

ALTER TABLE acqa_wizard
  ADD CONSTRAINT acqa_wizard_pk PRIMARY KEY (
    acqa_wizard_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/


comment on table acqa_wizard        is '(S) [11] The Acqa Wizard Id Table is used to assign a wizard wkspc to a roadmap step.';
comment on column acqa_wizard.ACQA_WIZARD_ID  is 'Step number in the wizard.';
comment on column acqa_wizard.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column acqa_wizard.USER_ID  is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_wizard.DESCRIPTION  is 'Records a short description of the wizard step .';
comment on column acqa_wizard.LONG_DESCRIPTION  is 'Records a long description of the wizard step .';
comment on column acqa_wizard.OBJECT_NAME  is 'Workspace User Object for the wizard step.';
comment on column acqa_wizard.IDENTIFIER  is 'Workspace User Object for the wizard step.';
comment on column acqa_wizard.VISIBLE is 'Insert 1 for workspace to be visible, otherwise 0.';

CREATE TABLE acqa_wizard_batch (
  company_id       NUMBER(22,0)  NOT NULL,
  batch_id         NUMBER(22,0)  NOT NULL,
  description      VARCHAR2(35)  NOT NULL,
  long_description VARCHAR2(254) NULL,
  user_id          VARCHAR2(18)  NULL,
  time_stamp       DATE          NULL
)
/

ALTER TABLE acqa_wizard_batch
  ADD CONSTRAINT acqa_wizard_batch_pk PRIMARY KEY (
    batch_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE acqa_wizard_batch
  ADD CONSTRAINT acqa_wizard_bat_co_fk FOREIGN KEY (
    company_id
  ) REFERENCES company_setup (
    company_id
  )
/


comment on table acqa_wizard_batch       is '(S) [11] The Acqa Wizard Batch Table relates company id to the batch id.';
comment on column acqa_wizard_batch.COMPANY_ID  is 'System-assigned identifier of a particular company.';
comment on column acqa_wizard_batch.BATCH_ID   is 'Batch id is a system generated id for the batch.';
comment on column acqa_wizard_batch.DESCRIPTION  is 'Records a short description of the batch .';
comment on column acqa_wizard_batch.LONG_DESCRIPTION  is 'Records a long description of the batch .';
comment on column acqa_wizard_batch.USER_ID       is 'Standard system-assigned id used for audit purposes.';
comment on column acqa_wizard_batch.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';



CREATE TABLE acqa_current_user_batch (
  user_id    VARCHAR2(18) NOT NULL,
  session_id NUMBER(22,0) NOT NULL,
  batch_id   NUMBER(22,0) NOT NULL,
  time_stamp DATE         NULL
)
/

ALTER TABLE acqa_current_user_batch
  ADD CONSTRAINT pk_acqa_current_user_batch PRIMARY KEY (
    user_id,
    session_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

comment on table acqa_current_user_batch        is '(S) [11] The Acqa Current User Batch Table relates the session to the batch id.';
comment on column acqa_current_user_batch.USER_ID  is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_current_user_batch.SESSION_ID is 'Standard system-assigned session id used for audit purposes.';
comment on column acqa_current_user_batch.BATCH_ID   is 'Batch id is a system generated id for the batch.';
comment on column acqa_current_user_batch.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';


CREATE TABLE acqa_wizard_status (
  acqa_wizard_status_id NUMBER(22,0)  NOT NULL,
  time_stamp            DATE          NULL,
  user_id               VARCHAR2(18)  NULL,
  description           VARCHAR2(35)  NULL,
  long_description      VARCHAR2(254) NULL
)
/

ALTER TABLE acqa_wizard_status
  ADD CONSTRAINT acqa_wizard_status_pk PRIMARY KEY (
    acqa_wizard_status_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

comment on table acqa_wizard_status       is '(S) [11] The Acqa Wizard Status Table records the status for the Wizard Steps.';
comment on column acqa_wizard_status.ACQA_WIZARD_STATUS_ID  is 'Records the status for the wizard step.';
comment on column acqa_wizard_status.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column acqa_wizard_status.USER_ID  is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_wizard_batch.DESCRIPTION  is 'Records a short description of the status .';
comment on column acqa_wizard_batch.LONG_DESCRIPTION  is 'Records a long description of the status.';

CREATE TABLE acqa_wizard_company (
  company_id            NUMBER(22,0) NOT NULL,
  acqa_wizard_id        NUMBER(22,0) NOT NULL,
  time_stamp            DATE         NULL,
  user_id               VARCHAR2(18) NULL,
  acqa_wizard_status_id NUMBER(22,0) NOT NULL,
  last_run_time         DATE         NULL,
  last_run_by           VARCHAR2(18) NULL,
  from_company_id       NUMBER(22,0) NULL,
  batch_id              NUMBER(22,0) NOT NULL
)
/

ALTER TABLE acqa_wizard_company
  ADD CONSTRAINT acqa_wizard_co_pk PRIMARY KEY (
    company_id,
    acqa_wizard_id,
    batch_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE acqa_wizard_company
  ADD CONSTRAINT acqa_wizard_co_wzst_fk FOREIGN KEY (
    acqa_wizard_status_id
  ) REFERENCES acqa_wizard_status (
    acqa_wizard_status_id
  )
/

ALTER TABLE acqa_wizard_company
  ADD CONSTRAINT acqa_wizard_co_wz_fk FOREIGN KEY (
    acqa_wizard_id
  ) REFERENCES acqa_wizard (
    acqa_wizard_id
  )
/

comment on table acqa_wizard_company       is '(S) [11] The Acqa Wizard Company Table relates the wizard step status for each company by wizard step.';
comment on column acqa_wizard_company.COMPANY_ID  is 'System-assigned identifier of a particular company.';
comment on column acqa_wizard_company.ACQA_WIZARD_ID  is 'Records the Wizard Id for the wizard step.';
comment on column acqa_wizard_company .TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column acqa_wizard_company.USER_ID  is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_wizard_company.ACQA_WIZARD_STATUS_ID  is 'Records the status for the wizard step.';
comment on column acqa_wizard_company.LAST_RUN_TIME  is 'Records the last run time used for audit purposes.';
comment on column acqa_wizard_company.LAST_RUN_BY  is 'Records the who last run for audit purposes.';
comment on column acqa_wizard_company.FROM_COMPANY_ID  is 'The company the attributes where copied from when created the new batch.';
comment on column acqa_wizard_company.BATCH_ID  is 'Batch id is a system generated id for the batch.';

CREATE TABLE acqa_batch_audit_trail_stg (
  id            NUMBER(22,0)   NOT NULL,
  batch_id      NUMBER(22,0)   NOT NULL,
  table_name    VARCHAR2(35)   NOT NULL,
  column_name   VARCHAR2(35)   NOT NULL,
  action        VARCHAR2(35)   NOT NULL,
  old_value     VARCHAR2(4000) NULL,
  new_value     VARCHAR2(4000) NULL,
  user_id       VARCHAR2(18)   NOT NULL,
  time_stamp    DATE           NOT NULL,
  window_title  VARCHAR2(250)  NULL,
  active_window VARCHAR2(60)   NULL,
  comments      VARCHAR2(254)  NULL
)
  STORAGE (
    NEXT          8 K
  )
/

comment on table acqa_batch_audit_trail_stg       is '(S) [11] The Acqa Batch Audit STG Table is the staging tabel for acqa_batch_audit_trail.';
comment on column acqa_batch_audit_trail_stg.ID       is 'Standard system-assigned id used for audit purposes.';
comment on column acqa_batch_audit_trail_stg.BATCH_ID    is 'Batch id is the identifier for the session the insert or delete are relavant for.';
comment on column acqa_batch_audit_trail_stg.TABLE_NAME   is 'Name of the table impacted by insert or delete';
comment on column acqa_batch_audit_trail_stg.COLUMN_NAME is 'Name of the column impacted by the insert or delete.';
comment on column acqa_batch_audit_trail_stg.ACTION is 'Indicates if an insert or delete';
comment on column acqa_batch_audit_trail_stg.OLD_VALUE is 'Old value';
comment on column acqa_batch_audit_trail_stg.NEW_VALUE is 'New Value.';
comment on column acqa_batch_audit_trail_stg.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_batch_audit_trail_stg.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column acqa_batch_audit_trail_stg.WINDOW_TITLE is 'Window the insert or delete was executed in.';
comment on column acqa_batch_audit_trail_stg.ACTIVE_WINDOW is 'Active window the insert or delete executed in.';
comment on column acqa_batch_audit_trail_stg.COMMENTS is 'Additional information such as OSUSER, MACHINE, and TERMINAL.';



CREATE TABLE acqa_batch_audit_trail (
  id            NUMBER(22,0)   NOT NULL,
  batch_id      NUMBER(22,0)   NOT NULL,
  table_name    VARCHAR2(35)   NOT NULL,
  column_name   VARCHAR2(35)   NOT NULL,
  action        VARCHAR2(35)   NOT NULL,
  old_value     VARCHAR2(4000) NULL,
  new_value     VARCHAR2(4000) NULL,
  user_id       VARCHAR2(18)   NOT NULL,
  time_stamp    DATE           NOT NULL,
  window_title  VARCHAR2(250)  NULL,
  active_window VARCHAR2(60)   NULL,
  comments      VARCHAR2(254)  NULL
)
/

ALTER TABLE acqa_batch_audit_trail
  ADD CONSTRAINT pk_acqa_batch_audit_trail PRIMARY KEY (
    id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

comment on table acqa_batch_audit_trail           is '(S) [11] The Acqa Batch Audit Table is the audit table used to track inserts and deltes made to any tables through the acqusition tool.';
comment on column acqa_batch_audit_trail.ID       is 'Standard system-assigned id used for audit purposes.';
comment on column acqa_batch_audit_trail.BATCH_ID    is 'Batch id is the identifier for the session the insert or delete are relavant for.';
comment on column acqa_batch_audit_trail.TABLE_NAME   is 'Name of the table impacted by insert or delete';
comment on column acqa_batch_audit_trail.COLUMN_NAME is 'Name of the column impacted by the insert or delete.';
comment on column acqa_batch_audit_trail.ACTION is 'Indicates if an insert or delete';
comment on column acqa_batch_audit_trail.OLD_VALUE is 'Old value';
comment on column acqa_batch_audit_trail.NEW_VALUE is 'New Value.';
comment on column acqa_batch_audit_trail.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_batch_audit_trail.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column acqa_batch_audit_trail.WINDOW_TITLE is 'Window the insert or delete was executed in.';
comment on column acqa_batch_audit_trail.ACTIVE_WINDOW is 'Active window the insert or delete executed in.';
comment on column acqa_batch_audit_trail.COMMENTS is 'Additional information such as OSUSER, MACHINE, and TERMINAL.';




CREATE TABLE acqa_company_copy_attributes (
  batch_id          NUMBER(22,0) NOT NULL,
  co_bus_seg        NUMBER(22,0) NOT NULL,
  co_gl_acct        NUMBER(22,0) NOT NULL,
  curr_schema       NUMBER(22,0) NOT NULL,
  company_set_books NUMBER(22,0) NOT NULL,
  co_security       NUMBER(22,0) NOT NULL,
  cpr_mo_end        NUMBER(22,0) NOT NULL,
  wo_mo_end         NUMBER(22,0) NOT NULL
)
/

ALTER TABLE acqa_company_copy_attributes
  ADD CONSTRAINT pk_acqa_comp_copy_attr PRIMARY KEY (
    batch_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

comment on table acqa_company_copy_attributes     is '(S) [11] The Company Copy Attributes Table is used to store which attributes for each batch to copy';
comment on column acqa_company_copy_attributes.BATCH_ID    is 'Batch id is the identifier for the session the attributes to copy are relavant for.';
comment on column acqa_company_copy_attributes.CO_BUS_SEG  is 'Insert 1 to copy the company business segment, otherwise 0';
comment on column acqa_company_copy_attributes.CO_GL_ACCT  is 'Insert 1 to copy the company gl account, otherwise 0';
comment on column acqa_company_copy_attributes.CURR_SCHEMA  is 'Insert 1 to copy the currency schema, otherwise 0';
comment on column acqa_company_copy_attributes.COMPANY_SET_BOOKS  is 'Insert 1 to copy the company set of books, otherwise 0';
comment on column acqa_company_copy_attributes.CO_SECURITY  is 'Insert 1 to copy the security set up, otherwise 0';
comment on column acqa_company_copy_attributes.CPR_MO_END  is 'Insert 1 to copy the cpr month end, otherwise 0';
comment on column acqa_company_copy_attributes.WO_MO_END  is 'Insert 1 to copy the wo month end, otherwise 0';




CREATE TABLE acqa_overview_questions (
  question_id       NUMBER(22,0)   NOT NULL,
  user_id           VARCHAR2(18)   NULL,
  time_stamp        DATE           NULL,
  question          VARCHAR2(2000) NOT NULL,
  acqa_wizard_id    NUMBER(22,0)   NULL,
  table_name        VARCHAR2(30)   NULL,
  tabpage           VARCHAR2(254)  NULL,
  question_order_id NUMBER(22,0)   NULL
)
/

ALTER TABLE acqa_overview_questions
  ADD CONSTRAINT acqa_overview_questions PRIMARY KEY (
    question_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE acqa_overview_questions
  ADD CONSTRAINT acqa_overview_q_wiz FOREIGN KEY (
    acqa_wizard_id
  ) REFERENCES acqa_wizard (
    acqa_wizard_id
  )
/


comment on table acqa_overview_questions   is '(S) [11] The Acqa Overview Questions Table records all the questions for the overview tabpages.';
comment on column acqa_overview_questions.QUESTION_ID       is 'Records the ID for the question.';
comment on column acqa_overview_questions.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_overview_questions.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column acqa_overview_questions.QUESTION    is 'Records the text for the Question.';
comment on column acqa_overview_questions.ACQA_WIZARD_ID  is 'Records the Wizard Id for the wizard step.';
comment on column acqa_overview_questions.TABLE_NAME   is 'Name of the table related to the question.';
comment on column acqa_overview_questions.TABPAGE      is 'Name of the tabpage realted to the question.';
comment on column acqa_overview_questions.QUESTION_ORDER_ID is 'The order the question should appear on the overview tabpage.';




CREATE TABLE acqa_overview_answers (
  question_id     NUMBER(22,0)  NOT NULL,
  answer_id       NUMBER(22,0)  NOT NULL,
  description     VARCHAR2(254) NOT NULL,
  user_id         VARCHAR2(18)  NULL,
  time_stamp      DATE          NULL,
  tabpage_visible NUMBER(1,0)   NULL
)
/

ALTER TABLE acqa_overview_answers
  ADD CONSTRAINT acqa_overview_answers PRIMARY KEY (
    answer_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE acqa_overview_answers
  ADD CONSTRAINT acqa_overview_answers_question FOREIGN KEY (
    question_id
  ) REFERENCES acqa_overview_questions (
    question_id
  )
/

comment on table acqa_overview_answers   is '(S) [11] The Acqa Overview Questions Table records all the questions for the overview tabpages.';
comment on column acqa_overview_answers.QUESTION_ID       is 'Records the ID for the question.';
comment on column acqa_overview_answers.ANSWER_ID is 'Records the ID for the answer to the question .';
comment on column acqa_overview_answers.DESCRIPTION  is 'Records a short description of the answer .';
comment on column acqa_overview_answers.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_overview_answers.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column acqa_overview_answers.TABPAGE_VISIBLE  is 'Insert 1 for the tabpage to be visible, otherwise 0.';





CREATE TABLE acqa_overview_questions_batch (
  question_id       NUMBER(22,0) NOT NULL,
  batch_id          NUMBER(22,0) NOT NULL,
  answer_id         NUMBER(22,0) NULL,
  user_id           VARCHAR2(18) NULL,
  time_stamp        DATE         NULL,
  question_order_id NUMBER(22,0) NULL
)
/

ALTER TABLE acqa_overview_questions_batch
  ADD CONSTRAINT acqa_overview_questions_batch PRIMARY KEY (
    question_id,
    batch_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE acqa_overview_questions_batch
  ADD CONSTRAINT acqa_overview_q_batch_batch FOREIGN KEY (
    batch_id
  ) REFERENCES acqa_wizard_batch (
    batch_id
  )
/

ALTER TABLE acqa_overview_questions_batch
  ADD CONSTRAINT acqa_overview_q_batch_q FOREIGN KEY (
    question_id
  ) REFERENCES acqa_overview_questions (
    question_id
  )
/


comment on table acqa_overview_questions_batch   is '(S) [11] The Acqa Overview Questions Batch Table records all the answers to the questions by Batch.';
comment on column acqa_overview_questions_batch.QUESTION_ID   is 'Records the ID for the question.';
comment on column acqa_overview_questions_batch.BATCH_ID   is 'Batch id is a system generated id for the batch.';
comment on column acqa_overview_questions_batch.ANSWER_ID is 'Records the ID for the answer to the question .';
comment on column acqa_overview_questions_batch.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column acqa_overview_questions_batch.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column acqa_overview_questions_batch.QUESTION_ORDER_ID is 'The order the question should appear on the overview tabpage.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2508, 0, 2015, 1, 0, 0, 43663, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043663_sys_acqa_01_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
