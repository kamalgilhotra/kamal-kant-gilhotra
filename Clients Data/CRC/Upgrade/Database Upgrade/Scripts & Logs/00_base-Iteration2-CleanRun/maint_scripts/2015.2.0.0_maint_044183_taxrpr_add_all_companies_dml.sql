/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044183_taxrpr_add_all_companies_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/07/2015 Daniel Motter  Creation
||============================================================================
*/

update pp_import_lookup
set lookup_sql = '( select co.company_id from (select company.company_id, company.description from company union select -1, ''ALL COMPANIES'' FROM DUAL) co where upper( trim( <importfield> ) ) = upper( trim( co.description ) ) )'
where description = 'Company.Description';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2683, 0, 2015, 2, 0, 0, 044183, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044183_taxrpr_add_all_companies_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;