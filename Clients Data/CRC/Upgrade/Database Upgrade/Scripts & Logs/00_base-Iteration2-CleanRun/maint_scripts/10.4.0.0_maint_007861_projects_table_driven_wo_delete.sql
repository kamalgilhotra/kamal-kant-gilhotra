/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007861_projects_table_driven_wo_delete.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   09/24/2012 Chris Mardis   Point Release
||============================================================================
*/

--drop table WO_DELETE;

create table WO_DELETE
(
 ID                   number(22,0) not null,
 SORT_ORDER           number(22,8),
 EXCEPTION_FLAG       number(22,0),
 DESCRIPTION          varchar2(35),
 FUNDING_WO_INDICATOR number(22,0),
 TABLE_NAME           varchar2(35),
 WHERE_CLAUSE         varchar2(4000),
 EXCEPTION_MESSAGE    varchar2(254),
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table WO_DELETE
   add constraint PK_WO_DELETE
       primary key (ID)
       using index tablespace PWRPLANT_IDX;


--drop table WO_DELETE_CUSTOM;

create table WO_DELETE_CUSTOM
(
 ID                   number(22,0) not null,
 SORT_ORDER           number(22,8),
 EXCEPTION_FLAG       number(22,0),
 DESCRIPTION          varchar2(35),
 FUNDING_WO_INDICATOR number(22,0),
 TABLE_NAME           varchar2(35),
 WHERE_CLAUSE         varchar2(4000),
 EXCEPTION_MESSAGE    varchar2(254),
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table WO_DELETE_CUSTOM
   add constraint PK_WO_DELETE_CUSTOM
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

--drop table BUDGET_DELETE;

create table BUDGET_DELETE
(
 id                number(22,0) not null,
 sort_order        number(22,8),
 exception_flag    number(22,0),
 description       varchar2(35),
 table_name        varchar2(35),
 where_clause      varchar2(4000),
 exception_message varchar2(254),
 user_id           varchar2(18),
 time_stamp        date
);

alter table BUDGET_DELETE
   add constraint PK_BUDGET_DELETE
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

--drop table BUDGET_DELETE_CUSTOM;

create table BUDGET_DELETE_CUSTOM
(
 ID                number(22,0) not null,
 SORT_ORDER        number(22,8),
 EXCEPTION_FLAG    number(22,0),
 DESCRIPTION       varchar2(35),
 TABLE_NAME        varchar2(35),
 WHERE_CLAUSE      varchar2(4000),
 EXCEPTION_MESSAGE varchar2(254),
 USER_ID           varchar2(18),
 TIME_STAMP        date
);

alter table BUDGET_DELETE_CUSTOM
   add constraint PK_BUDGET_DELETE_CUSTOM
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

--FP/WO Delete
--delete from WO_DELETE;
--delete from BUDGET_DELETE;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1 ID,
          1 EXCEPTION_FLAG,
          0 FUNDING_WO_INDICATOR,
          'CWIP_CHARGE' TABLE_NAME,
          'where work_order_id = <<wo_id>>' WHERE_CLAUSE,
          'Charges exist for this Work Order in cwip charge'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1 ID,
          1 EXCEPTION_FLAG,
          1 FUNDING_WO_INDICATOR,
          'CWIP_CHARGE' TABLE_NAME,
          'where work_order_id in (select work_order_id from work_order_control where funding_wo_id = <<wo_id>>)' WHERE_CLAUSE,
          'Work Orders under this Funding Project have charges in cwip charge'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1 ID,
          1 EXCEPTION_FLAG,
          -1 FUNDING_WO_INDICATOR,
          'WORK_ORDER_APPROVAL' TABLE_NAME,
          'where work_order_id = <<wo_id>> and approval_status_id = 3' WHERE_CLAUSE,
          'Cannot delete records with approved revisions.'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1 ID,
          1 EXCEPTION_FLAG,
          1 FUNDING_WO_INDICATOR,
          'WORK_ORDER_CONTROL' TABLE_NAME,
          'where funding_wo_id = <<wo_id>>' WHERE_CLAUSE,
          'Work Orders are associated to this Funding Project.'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1 ID,
          1 EXCEPTION_FLAG,
          0 FUNDING_WO_INDICATOR,
          'WO_EST_MONTHLY' TABLE_NAME,
          'where wo_work_order_id = <<wo_id>>' WHERE_CLAUSE,
          'Work Order has been estimated on Funding Project estimates.'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1 ID,
          1 EXCEPTION_FLAG,
          1 FUNDING_WO_INDICATOR,
          'BUDGET_MONTHLY_DATA' TABLE_NAME,
          'where work_order_id = <<wo_id>>' WHERE_CLAUSE,
          'Funding Project has been estimated on Budget Item estimates.'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1 ID,
          0 EXCEPTION_FLAG,
          -1 FUNDING_WO_INDICATOR,
          'CO_TENANCY_WO' TABLE_NAME,
          'where co_tenant_wo = <<wo_id>>' WHERE_CLAUSE
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1 ID,
          0 EXCEPTION_FLAG,
          -1 FUNDING_WO_INDICATOR,
          'CO_TENANCY_WO' TABLE_NAME,
          'where work_order_id = <<wo_id>>' WHERE_CLAUSE
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'CWIP_IN_RATE_BASE',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'FUNDING_JUSTIFICATION',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_DOC_COMMENTS',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_DOC_JUSTIFICATION',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_DOCUMENTATION',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORK_ORDER_DOCUMENT',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'BUDGET_VERSION_FUND_PROJ',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'SUBSTITUTION_REVIEW',
          'where budget_sub_id in ( select budget_sub_id from budget_substitutions where to_work_order_id = <<wo_id>> or from_work_order_id = <<wo_id>>)'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'SUBSTITUTION_REVIEW_DETAILS',
          'where  budget_sub_id in ( select budget_sub_id from budget_substitutions where to_work_order_id = <<wo_id>> or from_work_order_id = <<wo_id>>)'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'workflow_detail',
          'where workflow_id in (select workflow_id from workflow where id_field1 in (select budget_sub_id from budget_substitutions where to_work_order_id = <<wo_id>> or from_work_order_id = <<wo_id>>) and subsystem in (''subs_review''))'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'workflow',
          'where id_field1 in (select budget_sub_id from budget_substitutions where to_work_order_id = <<wo_id>> or from_work_order_id = <<wo_id>>) and subsystem in (''subs_review'')'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'BUDGET_SUBSTITUTIONS',
          'where to_work_order_id = <<wo_id>> or from_work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'BUDGET_REVIEW',
          'where work_order_id  = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'BUDGET_REVIEW_DETAIL',
          'where work_order_id  = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'JOB_TASK_CLASS_CODE',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'JOB_TASK_DOCUMENT',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'JOB_TASK',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'UNITIZED_WORK_ORDER',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_CLEAR_OVER',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_ESTIMATE',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_EST_MONTHLY_SPREAD',
          'where est_monthly_id in (select est_monthly_id from wo_est_monthly where work_order_id = <<wo_id>>)'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'wo_est_monthly_escalation',
          'where est_monthly_id in (select est_monthly_id from wo_est_monthly where work_order_id = <<wo_id>>)'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_EST_MONTHLY',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_EST_DATE_CHANGES',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_REIMBURSABLE',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORK_ORDER_ACCOUNT',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_APPROVAL_MULTIPLE',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORK_ORDER_APPROVAL',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORKFLOW_DETAIL',
          'where workflow_id in (select workflow_id from workflow where id_field1 = to_char(<<wo_id>>) and subsystem in (''fp_closure'',''fp_approval'',''wo_approval''))'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORKFLOW',
          'where id_field1 = to_char(<<wo_id>>) and subsystem in (''fp_closure'',''fp_approval'',''wo_approval'')'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORK_ORDER_CLASS_CODE',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORK_ORDER_DEPARTMENT',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORK_ORDER_INITIATOR',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_ACCRUALS_HISTORY',
          'where funding_wo_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_ACCRUALS_HISTORY',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_ACCRUALS',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORK_ORDER_TAX_STATUS',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'budget_afudc_calc',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'budget_closings_stage',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'budget_closings_pct',
          'where work_order_id = <<wo_id>>'
     from DUAL;

--insert into WO_DELETE
--   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
--   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
--          0,
--          1,
--          'CR_DERIVER_CONTROL',
--          'where work_order_id = <<wo_id>>'
--     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WO_EST_SUMMARY_TBL',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'wo_arc_results',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'wo_arc_alert_history',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into WO_DELETE
   (ID, EXCEPTION_FLAG, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from WO_DELETE) + 1,
          0,
          -1,
          'WORK_ORDER_CONTROL',
          'where work_order_id = <<wo_id>>'
     from DUAL;

--BI Delete
insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          1,
          'WORK_ORDER_CONTROL',
          'where budget_id = <<wo_id>>',
          'Budget Item has underlying Work Orders or Funding Projects'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'budget_monthly_data_escalation',
          'where budget_monthly_id in (select budget_monthly_id from budget_monthly_data where budget_id = <<wo_id>>)'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_MONTHLY_SPREAD',
          'where budget_monthly_id in (select budget_monthly_id from budget_monthly_data where budget_id = <<wo_id>>)'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_MONTHLY_DATA',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_SUMMARY_DATA',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_CLASS_CODE',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_AMOUNTS',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_AMOUNTS_ARCH',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_MONTHLY_DATA_ARCH',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'CLEARING_WO_CONTROL_BDG',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'WO_CLEAR_OVER_BDG',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_APPROVAL_DETAIL',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_APPROVAL_LEVELS',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_APPROVAL_DATA',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET_APPROVAL',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'budget_afudc_calc',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'budget_closings_stage',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'budget_closings_pct',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'fcst_budget_load',
          'where budget_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'WORKFLOW_DETAIL',
          'where workflow_id in (select workflow_id from workflow where id_field1 = to_char(<<wo_id>>) and subsystem =''bi_approval'')'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'WORKFLOW',
          'where id_field1 = to_char(<<wo_id>>) and subsystem =''bi_approval'''
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'WO_DOC_COMMENTS',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'WO_DOC_JUSTIFICATION',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'WO_DOCUMENTATION',
          'where work_order_id = <<wo_id>>'
     from DUAL;

insert into BUDGET_DELETE
   (ID, EXCEPTION_FLAG, TABLE_NAME, WHERE_CLAUSE)
   select (select NVL(max(ID), 0) from BUDGET_DELETE) + 1,
          0,
          'BUDGET',
          'where budget_id = <<wo_id>>'
     from DUAL;

update WO_DELETE set DESCRIPTION = replace(TABLE_NAME, '_', ' ');

update WO_DELETE set SORT_ORDER = ID * 100;

update BUDGET_DELETE set DESCRIPTION = replace(TABLE_NAME, '_', ' ');

update BUDGET_DELETE set SORT_ORDER = ID * 100;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (233, 0, 10, 4, 0, 0, 7861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_007861_projects_table_driven_wo_delete.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
