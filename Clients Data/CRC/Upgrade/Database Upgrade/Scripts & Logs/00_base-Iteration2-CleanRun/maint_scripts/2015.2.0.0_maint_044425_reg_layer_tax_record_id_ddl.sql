/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044425_reg_layer_tax_record_id_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 08/19/2015 Sarah Byers 		 	Add tax_record_id to incremental tables
||========================================================================================
*/
--INCREMENTAL_FP_ADJ_DATA
alter table INCREMENTAL_FP_ADJ_DATA add (tax_record_id number(22,0) null);
comment on column INCREMENTAL_FP_ADJ_DATA.TAX_RECORD_ID is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables.   Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).   Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';

--INCREMENTAL_HIST_LAYER_DATA
alter table INCREMENTAL_HIST_LAYER_DATA add (tax_record_id number(22,0) null);
comment on column INCREMENTAL_HIST_LAYER_DATA.TAX_RECORD_ID is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables.   Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).   Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';

--INCREMENTAL_DEPR_ADJ_DATA
alter table INCREMENTAL_DEPR_ADJ_DATA add (tax_record_id number(22,0) null);
comment on column INCREMENTAL_DEPR_ADJ_DATA.TAX_RECORD_ID is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables.   Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).   Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';

--REG_INCREMENTAL_FP_ADJ_TRANS
alter table REG_INCREMENTAL_FP_ADJ_TRANS add (tax_record_id number(22,0) null);
comment on column REG_INCREMENTAL_FP_ADJ_TRANS.TAX_RECORD_ID is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables.   Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).   Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';

--REG_INC_HIST_LAYER_TRANS
alter table REG_INC_HIST_LAYER_TRANS add (tax_record_id number(22,0) null);
comment on column REG_INC_HIST_LAYER_TRANS.TAX_RECORD_ID is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables.   Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).   Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';

--REG_INCREMENTAL_DEPR_ADJ_TRANS
alter table REG_INCREMENTAL_DEPR_ADJ_TRANS add (tax_record_id number(22,0) null);
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.TAX_RECORD_ID is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables.   Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).   Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2811, 0, 2015, 2, 0, 0, 044425, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044425_reg_layer_tax_record_id_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;