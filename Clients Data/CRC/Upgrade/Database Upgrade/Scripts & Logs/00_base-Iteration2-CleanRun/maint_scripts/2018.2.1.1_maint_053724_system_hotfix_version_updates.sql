/*
||============================================================================
|| Application: PowerPlan
|| File Name:  2018.2.1.1_maint_053724_system_hotfix_version_updates.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.1.1 6/19/2019  CYura            2018.2.1.1 Hot Fix for 2018.2.1.0
||============================================================================
*/
update PP_VERSION
   set PP_PATCH   = 'Patch: 2018.2.1.1',
       POWERTAX_VERSION = '2018.2.1.1',
       PROVISION_VERSION = '2018.2.1.1',
       PROP_TAX_VERSION = '2018.2.1.1',
       LEASE_VERSION = '2018.2.1.1',
       BUDGET_VERSION = '2018.2.1.1',
       DATE_INSTALLED = sysdate
where PP_VERSION_ID = 1;


insert into PP_VERSION_EXES 
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
          select '2018.2.1.1', executable_file, '2018.2.1.0' from PP_VERSION_EXES A where pp_version = '2018.2.1.0'
and not exists (select 1 from PP_VERSION_EXES B where A.executable_file = B.executable_file and b.pp_version = '2018.2.1.1');


update pp_package_versions set version = '2018.2.1.1' where upper(package_name) = 'PKG_LEASE_SCHEDULE';

update pp_package_versions set version = '2018.2.1.1' where upper(package_name) = 'PKG_LEASE_ASSET_POST';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (18662, 0, 2018, 2, 1, 1, 53724, 'C:\BitBucketRepos\classic_pb\scripts\00_base\maint_scripts', '2018.2.1.1_maint_053724_system_hotfix_version_updates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;