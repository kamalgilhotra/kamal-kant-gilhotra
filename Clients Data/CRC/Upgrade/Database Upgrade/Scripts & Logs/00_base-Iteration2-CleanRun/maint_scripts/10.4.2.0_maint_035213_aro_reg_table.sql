/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035213_aro_reg_table.sql
|| Description: Create a table to hold pending activity for regulated ARO's
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change 
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/10/2014 Ryan Oliveria
||============================================================================
*/

create table PEND_ARO_REG_ACTIVITY
(
 ARO_ID          number(22,0) not null,
 SET_OF_BOOKS_ID number(22,0) not null,
 DEPR_GROUP_ID   number(22,0) not null,
 GL_POST_MO_YR   date not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 AMOUNT          number(22,2)
);

comment on column PEND_ARO_REG_ACTIVITY.ARO_ID is 'The ARO that this activity is for.';
comment on column PEND_ARO_REG_ACTIVITY.SET_OF_BOOKS_ID is 'System-assigned identifier of a unique set of books maintained by the utility in PowerPlan';
comment on column PEND_ARO_REG_ACTIVITY.DEPR_GROUP_ID is 'System-assigned identifier of a particular depreciation group.';
comment on column PEND_ARO_REG_ACTIVITY.GL_POST_MO_YR is 'The accounting month and year of activity the entry represents.';
comment on column PEND_ARO_REG_ACTIVITY.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PEND_ARO_REG_ACTIVITY.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column PEND_ARO_REG_ACTIVITY.AMOUNT is 'The amount to be recorded in the Depreciation Ledger.';

alter table PEND_ARO_REG_ACTIVITY
   add constraint PK_PEND_ARO_REG_ACTIVITY
       primary key (ARO_ID, SET_OF_BOOKS_ID, DEPR_GROUP_ID, GL_POST_MO_YR)
       using index tablespace PWRPLANT_IDX;

alter table PEND_ARO_REG_ACTIVITY
   add constraint FK1_PEND_ARO_REG_ACTIVITY
       foreign key (ARO_ID)
       references ARO (ARO_ID);

alter table PEND_ARO_REG_ACTIVITY
   add constraint FK2_PEND_ARO_REG_ACTIVITY
       foreign key (SET_OF_BOOKS_ID, DEPR_GROUP_ID, GL_POST_MO_YR)
       references DEPR_LEDGER (SET_OF_BOOKS_ID, DEPR_GROUP_ID, GL_POST_MO_YR);


alter table DEPR_TRANS_SET
	add REGULATED_ARO number(1,0);

comment on column DEPR_TRANS_SET.REGULATED_ARO is 'A number to indicate whether or not this trans set can be used for Regulated ARO''s.  0 = No.  1 = Yes.';




create table PEND_ARO_REG_ACTIVITY_ARC
(
 ARO_ID          number(22,0) not null,
 SET_OF_BOOKS_ID number(22,0) not null,
 DEPR_GROUP_ID   number(22,0) not null,
 GL_POST_MO_YR   date not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 AMOUNT          number(22,2)
);
comment on column PEND_ARO_REG_ACTIVITY_ARC.ARO_ID is 'The ARO that this activity is for.';
comment on column PEND_ARO_REG_ACTIVITY_ARC.SET_OF_BOOKS_ID is 'System-assigned identifier of a unique set of books maintained by the utility in PowerPlan';
comment on column PEND_ARO_REG_ACTIVITY_ARC.DEPR_GROUP_ID is 'System-assigned identifier of a particular depreciation group.';
comment on column PEND_ARO_REG_ACTIVITY_ARC.GL_POST_MO_YR is 'The accounting month and year of activity the entry represents.';
comment on column PEND_ARO_REG_ACTIVITY_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PEND_ARO_REG_ACTIVITY_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column PEND_ARO_REG_ACTIVITY_ARC.AMOUNT is 'The amount to be recorded in the Depreciation Ledger.';


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (847, 0, 10, 4, 2, 0, 35213, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035213_aro_reg_table.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
