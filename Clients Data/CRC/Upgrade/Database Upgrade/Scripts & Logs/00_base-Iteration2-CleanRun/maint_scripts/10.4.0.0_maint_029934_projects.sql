/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029934_projects.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   05/06/2013 Chris Mardis   Point Release
||============================================================================
*/

alter table WORKFLOW_DETAIL add MOBILE_APPROVAL number(22);

create or replace trigger WORKFLOW_DETAIL_APPROVALS
   after update of APPROVAL_STATUS_ID on WORKFLOW_DETAIL
   for each row

declare
   PROG        varchar2(60);
   OSUSER      varchar2(60);
   MACHINE     varchar2(60);
   TERMINAL    varchar2(60);
   OLD_LOOKUP  varchar2(250);
   NEW_LOOKUP  varchar2(250);
   PK_LOOKUP   varchar2(1500);
   WINDOW      varchar2(60);
   WINDOWTITLE varchar2(250);
   COMMENTS    varchar2(250);
   TRANS       varchar2(35);

begin
   if NVL(SYS_CONTEXT('powerplant_ctx', 'audit'), 'yes') = 'no' then
      return;
   end if;
   WINDOW      := NVL(SYS_CONTEXT('powerplant_ctx', 'window'), 'unknown window');
   WINDOWTITLE := NVL(SYS_CONTEXT('powerplant_ctx', 'windowtitle'), '');
   TRANS       := NVL(SYS_CONTEXT('powerplant_ctx', 'process'), '');
   COMMENTS    := NVL(SYS_CONTEXT('powerplant_ctx', 'comments'), '');
   PROG        := NVL(SYS_CONTEXT('powerplant_ctx', 'program'), '');
   OSUSER      := NVL(SYS_CONTEXT('powerplant_ctx', 'osuser'), '');
   MACHINE     := NVL(SYS_CONTEXT('powerplant_ctx', 'machine'), '');
   TERMINAL    := NVL(SYS_CONTEXT('powerplant_ctx', 'terminal'), '');
   COMMENTS    := COMMENTS || '; OSUSER=' || trim(OSUSER) || '; MACHINE=' || trim(MACHINE) ||
                  '; TERMINAL=' || trim(TERMINAL);
   if trim(PROG) is null then
      select PROGRAM into PROG from V$SESSION where AUDSID = USERENV('sessionid');
   end if;

   if UPDATING then
      if :OLD.APPROVAL_STATUS_ID <> :NEW.APPROVAL_STATUS_ID or
         (:OLD.APPROVAL_STATUS_ID is null and :NEW.APPROVAL_STATUS_ID is not null) or
         (:NEW.APPROVAL_STATUS_ID is null and :OLD.APPROVAL_STATUS_ID is not null) then

         insert into PP_AUDITS_PROJECT_TRAIL
            (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE,
             OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, ACTION,
             MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
         values
            (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'WORKFLOW_DETAIL', 'APPROVAL_STATUS_ID',
             '' || 'WORKFLOW_ID=' || :NEW.WORKFLOW_ID || '; ' || 'ID=' || :NEW.ID || '; ', PK_LOOKUP,
             :OLD.APPROVAL_STATUS_ID, :NEW.APPROVAL_STATUS_ID, OLD_LOOKUP, NEW_LOOKUP, user, sysdate,
             PROG, 'U', TO_CHAR(sysdate, 'yyyymm'), COMMENTS, WINDOW, WINDOWTITLE, TRANS);
      end if;
   end if;
end;
/


create or replace trigger WORKFLOW_DETAIL_MA
   before update of APPROVAL_STATUS_ID on WORKFLOW_DETAIL
   for each row

declare
   PROCESS  varchar2(60);
   COMMENTS varchar2(250);

begin
   PROCESS  := NVL(SYS_CONTEXT('powerplant_ctx', 'process'), '');
   COMMENTS := NVL(SYS_CONTEXT('powerplant_ctx', 'comments'), '');
   if :NEW.APPROVAL_STATUS_ID in (3, 4) and LOWER(trim(PROCESS)) = 'web' and
      (INSTR(UPPER(COMMENTS), UPPER('Mobile')) > 0 or INSTR(UPPER(COMMENTS), UPPER('iPhone')) > 0 or
      INSTR(UPPER(COMMENTS), UPPER('iPod')) > 0 or INSTR(UPPER(COMMENTS), UPPER('iPad')) > 0 or
      INSTR(UPPER(COMMENTS), UPPER('Android')) > 0 or
      INSTR(UPPER(COMMENTS), UPPER('BlackBerry')) > 0 or
      INSTR(UPPER(COMMENTS), UPPER('IEMobile')) > 0) then
      :NEW.MOBILE_APPROVAL := 1;
   elsif :NEW.APPROVAL_STATUS_ID in (3, 4) and LOWER(trim(PROCESS)) = 'web' then
      :NEW.MOBILE_APPROVAL := 2;
   elsif :NEW.APPROVAL_STATUS_ID in (3, 4) then
      :NEW.MOBILE_APPROVAL := 0;
   else
      :NEW.MOBILE_APPROVAL := null;
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (379, 0, 10, 4, 0, 0, 29934, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029934_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
