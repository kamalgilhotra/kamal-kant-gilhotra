/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034360_projects_client_fxns.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/06/2014 Stephen Motter
||============================================================================
*/

create or replace function F_CLIENT_BUDGET_AFUDC(A_CALL                 number,
                                                 A_TABLE_NAME           varchar2,
                                                 A_ACTUALS_MONTH_NUMBER number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************

begin
   -- maps to PB function f_budget_afudc_custom
   return 1;

exception
   when others then
      rollback;

end F_CLIENT_BUDGET_AFUDC;
/


create or replace function F_CLIENT_BUDGET_OVERHEADS(A_CALL           number,
                                                     A_CALLING_WINDOW varchar2,
                                                     A_TABLE_NAME     varchar2) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************

begin
   -- maps to PB function f_budget_overheads_custom
   return 1;

exception
   when others then
      rollback;

end F_CLIENT_BUDGET_OVERHEADS;
/


create or replace function F_CLIENT_BUDGET_UWA(A_BV_ID  number,
                                               A_CALLER varchar2) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************

begin
   -- maps to PB function f_budget_update_with_act_custom
   return 1;

exception
   when others then
      rollback;

end F_CLIENT_BUDGET_UWA;
/


create or replace function F_CLIENT_BUDGET_TO_CR(A_ID                  number,
                                                 A_INSERT_WHERE_CLAUSE varchar2,
                                                 A_DELETE_WHERE_CLAUSE varchar2,
                                                 A_CAP_BUDGET_VERSION  varchar2,
                                                 A_CALLED_FROM         varchar2,
                                                 A_CR_BUDGET_VERSION   varchar2) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************

begin
   -- maps to PB function f_cr_to_budget_to_cr_custom
   return 1;

exception
   when others then
      rollback;

end F_CLIENT_BUDGET_TO_CR;
/


create or replace function F_CLIENT_BUDGET_REVISION(A_FUNCTION     varchar2,
                                                    A_CALL         number,
                                                    A_WO_FP        varchar2,
                                                    A_CALL_VAR_NUM DBMS_SQL.NUMBER_TABLE,
                                                    A_CALL_VAR_STR DBMS_SQL.VARCHAR2_TABLE,
                                                    A_CST_RTN      number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************

begin
   -- maps to PB function f_budget_revisions_custom
   return 1;

exception
   when others then
      rollback;

end F_CLIENT_BUDGET_REVISION;
/


delete from WO_VALIDATION_TYPE
 where WO_VALIDATION_TYPE_ID > 80
   and WO_VALIDATION_TYPE_ID < 90;

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID, DESCRIPTION, FIND_COMPANY, COL1, COL2, COL3)
values
  (81,
   'budget afudc custom',
   'select case
         when lower(''<arg2>'') = ''budget_monthly_data'' then
          (select nvl(min(company_id), -1)
             from budget, wo_est_processing_temp
            where budget.budget_id = wo_est_processing_temp.work_order_id)
         else
          (select nvl(min(company_id), -1)
             from work_order_control, wo_est_processing_temp
            where work_order_control.work_order_id =
                  wo_est_processing_temp.work_order_id)
       end case
  from dual',
   'call_location',
   'table_name',
   'actuals_month_number');

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID, DESCRIPTION, FIND_COMPANY, COL1, COL2, COL3)
values
  (82,
   'budget overheads custom',
   'select case
         when lower(''<arg2>'') = ''budget_monthly_data'' then
          (select nvl(min(company_id), -1)
             from budget, wo_est_processing_temp
            where budget.budget_id = wo_est_processing_temp.work_order_id)
         else
          (select nvl(min(company_id), -1)
             from work_order_control, wo_est_processing_temp
            where work_order_control.work_order_id =
                  wo_est_processing_temp.work_order_id)
       end case
  from dual',
   'call_location',
   'table_name',
   'actuals_month_number');

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID,
   DESCRIPTION,
   FIND_COMPANY,
   COL1,
   COL2,
   COL3,
   COL4,
   COL5)
values
  (83,
   'budget update with act custom',
   'select case
         when lower(''<arg2>'') = ''bi'' then
          (select nvl(company_id, -1)
             from budget_version
            where budget_version_id = <arg3>)
         else
          (select nvl(min(company_id), -1)
             from work_order_control, wo_est_processing_temp
            where work_order_control.work_order_id = wo_est_processing_temp.work_order_id)
       end case
  from dual',
   'call_location',
   'wo_fp',
   'cst_rtn',
   'actuals_month',
   'only_cur_year');

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID,
   DESCRIPTION,
   FIND_COMPANY,
   COL1,
   COL2,
   COL3,
   COL4,
   COL5,
   COL6)
values
  (84,
   'cr to budget to cr custom',
   'select nvl(company_id,0) from budget_version where budget_version_id = <arg4>',
   'id',
   'insert_where_clause',
   'delete_where_clause',
   'cap_budget_version',
   'called_from',
   'cr_budget_version');

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID, DESCRIPTION, FIND_COMPANY, COL1, COL2, COL3)
values
  (85,
   'budget new revision custom',
   'select nvl(min(company_id), -1)
  from work_order_control, wo_est_processing_temp
 where work_order_control.work_order_id =
       wo_est_processing_temp.work_order_id',
   'call_location',
   'wo_fp',
   'cst_rtn');

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID,
   DESCRIPTION,
   FIND_COMPANY,
   COL1,
   COL2,
   COL3,
   COL4,
   COL5,
   COL6)
values
  (86,
   'budget date change custom',
   'select nvl(min(company_id), -1)
  from work_order_control, wo_est_processing_temp
 where work_order_control.work_order_id =
       wo_est_processing_temp.work_order_id',
   'call_location',
   'wo_fp',
   'cst_rtn',
   'new_start_date',
   'new_end_date',
   'new_in_svc_date');

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID,
   DESCRIPTION,
   FIND_COMPANY,
   COL1,
   COL2,
   COL3,
   COL4,
   COL5)
values
  (87,
   'budget new justification custom',
   'select case
         when lower(''<arg2>'') = ''bi'' then
          (select nvl(min(company_id), -1)
             from budget, wo_est_processing_temp
            where budget.work_order_id = wo_est_processing_temp.work_order_id)
         else
          (select nvl(min(company_id), -1)
             from work_order_control, wo_est_processing_temp
            where work_order_control.work_order_id =
                  wo_est_processing_temp.work_order_id)
       end case
  from dual',
   'call_location',
   'wo_fp',
   'cst_rtn',
   'tab_indicator',
   'doc_stage');

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID, DESCRIPTION, FIND_COMPANY, COL1, COL2)
values
  (88,
   'budget new bi version custom',
   'select nvl(min(company_id), -1)
  from budget, wo_est_processing_temp
 where budget.work_order_id = wo_est_processing_temp.work_order_id',
   'call_location',
   'cst_rtn');

insert into WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID, DESCRIPTION, FIND_COMPANY, COL1, COL2)
values
  (89,
   'budget load fp to bi custom',
   'select nvl(min(company_id), -1)
  from budget, wo_est_processing_temp
 where budget.work_order_id = wo_est_processing_temp.work_order_id',
   'call_location',
   'cst_rtn');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (830, 0, 10, 4, 2, 0, 34360, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034360_projects_client_fxns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;