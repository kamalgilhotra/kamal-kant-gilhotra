/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038120_lease_component_ii_calc.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.3.0 06/19/2014  Kyle Peterson
||========================================================================================
*/

create table LS_COMPONENT_MONTHLY_II_STG
(
 COMPONENT_ID number(22,0) not null,
 MONTH        date not null,
 AMOUNT       number(22,2)
);

alter table LS_COMPONENT_MONTHLY_II_STG
   add constraint PK_LS_COMPONENT_MONTHLY_II_STG
       primary key (COMPONENT_ID, MONTH)
       using index tablespace PWRPLANT_IDX;

alter table LS_COMPONENT_MONTHLY_II_STG
   add constraint FK_LS_COMPONENT_MONTHLY_II_STG
       foreign key (COMPONENT_ID)
       references LS_COMPONENT(COMPONENT_ID);

comment on table LS_COMPONENT_MONTHLY_II_STG is 'An intermediary table that holds the results of the interim interest calculation on a month by month basis as the component level.';
comment on column LS_COMPONENT_MONTHLY_II_STG.COMPONENT_ID is 'The component id from LS_COMPONENT.';
comment on column LS_COMPONENT_MONTHLY_II_STG.MONTH is 'The month of the interim interest.';
comment on column LS_COMPONENT_MONTHLY_II_STG.AMOUNT is 'The interim interest amount.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1212, 0, 10, 4, 3, 0, 38120, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038120_lease_component_ii_calc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
