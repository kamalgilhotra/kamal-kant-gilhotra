/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_041658_lease_pay_lessor_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 01/15/2015 	B.Beck				Moving the pay_lessor flag from the summary tax to the local tax
||==========================================================================================
*/

alter table ls_tax_summary
drop column pay_lessor;

alter table ls_tax_local
add
(
	pay_lessor number(1, 0) default 0
);

delete
from powerplant_columns
where column_name = 'pay_lessor';

alter table ls_tax_local
add
(
	ap_acct_id number(22, 0)
);

comment on column ls_tax_local.pay_lessor is 'A flag to specify whether or not the local tax is paid to the lessor or the state.  If the flag is a 1 then the tax is paid to the lessor and SHOULD be included in payment reconciliation.';
comment on column ls_tax_local.ap_acct_id is 'A GL Account ID specifying the accunts payable account used for Tax Journals.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2170, 0, 10, 4, 3, 2, 041658, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041658_lease_pay_lessor_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;