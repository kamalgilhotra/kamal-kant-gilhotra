/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039733_prov_various_1043.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 09/23/2014 Jarrett Skov        various changes made to provision that
||                                         need to be in the base.
||============================================================================
*/


----
----  Originally from : prov_37200_Estimate_Import_setup.sql
----

-- Combining M items and def taxes into the same table. Use ta_norm_id = -1 for gross M amounts.

create table TAX_ACCRUAL_EST_IMPORT
(
 COMPANY_ID        number(22,0) not null,
 M_ITEM_ID         number(22,0) not null,
 OPER_IND          number(22,0) not null,
 TA_NORM_ID        number(22,0) not null,
 GL_COMPANY_NO     varchar2(30),
 TAX_RETURN_KEY    varchar2(30),
 COMPANY_DESC      varchar2(50),
 M_TYPE_DESC       varchar2(50),
 M_ITEM_DESC       varchar2(80),
 OPER_DESC         varchar2(50),
 JURISDICTION_DESC varchar2(50),
 ANNUAL_AMOUNT_EST number(22,2),
 TA_VERSION_ID     number(22,0) not null,
 M_ID              number(22,0),
 ERROR_MSG         varchar2(254),
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 RUN_NUMBER        number(22,0)
);

alter table TAX_ACCRUAL_EST_IMPORT
   add constraint TA_EST_IMPORT_PK
       primary key (TA_VERSION_ID, COMPANY_ID, M_ITEM_ID, OPER_IND, TA_NORM_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_EST_IMPORT is '(C) [08] The Tax Accrual Est Import table is used to import annual estimate amounts for timing differences and deferred taxes using an Excel template';
comment on column TAX_ACCRUAL_EST_IMPORT.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column TAX_ACCRUAL_EST_IMPORT.M_ITEM_ID is 'System-assigned identifier of a particular m-item, e.g. ''AFUDC-Equity'', ''Pension Accrual'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT.OPER_IND is 'System-assigned identifier of a segment below company for which taxes are separately calculated and booked, i.e. operating and non-operating.';
comment on column TAX_ACCRUAL_EST_IMPORT.TA_NORM_ID is 'System-assigned identifier of a particular tax accrual normalization schema. Uses -1 for timing difference records';
comment on column TAX_ACCRUAL_EST_IMPORT.GL_COMPANY_NO is 'Records a company code or description used by external systems such as the general ledger system.';
comment on column TAX_ACCRUAL_EST_IMPORT.TAX_RETURN_KEY is 'Records a user defined description of the external tax return compliance id.';
comment on column TAX_ACCRUAL_EST_IMPORT.COMPANY_DESC is 'Records a short description of the company.';
comment on column TAX_ACCRUAL_EST_IMPORT.M_TYPE_DESC is 'The description of the M Type.';
comment on column TAX_ACCRUAL_EST_IMPORT.M_ITEM_DESC is 'Description of the m-item, e.g. ''Interest for Tax'', ''Rehab Credit'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT.OPER_DESC is 'Description of the operating indicator, e.g. ''Electric'', ''Gas'', ''Non Operating''.';
comment on column TAX_ACCRUAL_EST_IMPORT.JURISDICTION_DESC is 'Description of the taxing authority, e.g. ''Federal'', ''DC'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT.ANNUAL_AMOUNT_EST is 'Estimated annual amount of the m-item in dollars.  Addition to taxable income is ''+''.';
comment on column TAX_ACCRUAL_EST_IMPORT.TA_VERSION_ID is 'System-assigned identifier of a particular version.';
comment on column TAX_ACCRUAL_EST_IMPORT.M_ID is 'System-assigned identifier for the unique key on the tax accrual control table.';
comment on column TAX_ACCRUAL_EST_IMPORT.ERROR_MSG is 'Error message if something goes wrong with import';
comment on column TAX_ACCRUAL_EST_IMPORT.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_ACCRUAL_EST_IMPORT.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_ACCRUAL_EST_IMPORT.RUN_NUMBER is 'Run number used to archive multiple imports for same items';


-- staging table
create table TAX_ACCRUAL_EST_IMPORT_STG
(
 COMPANY_ID        number(22,0) not null,
 M_ITEM_ID         number(22,0) not null,
 OPER_IND          number(22,0) not null,
 TA_NORM_ID        number(22,0) not null,
 GL_COMPANY_NO     varchar2(30),
 TAX_RETURN_KEY    varchar2(30),
 COMPANY_DESC      varchar2(50),
 M_TYPE_DESC       varchar2(50),
 M_ITEM_DESC       varchar2(80),
 OPER_DESC         varchar2(50),
 JURISDICTION_DESC varchar2(50),
 ANNUAL_AMOUNT_EST number(22,2),
 TA_VERSION_ID     number(22,0) not null,
 M_ID              number(22,0),
 ERROR_MSG         varchar2(254),
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 RUN_NUMBER        number(22,0)
);

alter table TAX_ACCRUAL_EST_IMPORT_STG
   add constraint TA_EST_IMPORT_STG_PK
       primary key (TA_VERSION_ID, COMPANY_ID, M_ITEM_ID, OPER_IND, TA_NORM_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_EST_IMPORT_STG is '(C) [08] The Tax Accrual Est Import Stg table is a staging table used to import annual estimates into Provision using an Excel template';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.M_ITEM_ID is 'System-assigned identifier of a particular m-item, e.g. ''AFUDC-Equity'', ''Pension Accrual'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.OPER_IND is 'System-assigned identifier of a segment below company for which taxes are separately calculated and booked, i.e. operating and non-operating.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.TA_NORM_ID is 'System-assigned identifier of a particular tax accrual normalization schema. Uses -1 for timing difference records';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.GL_COMPANY_NO is 'Records a company code or description used by external systems such as the general ledger system.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.TAX_RETURN_KEY is 'Records a user defined description of the external tax return compliance id.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.COMPANY_DESC is 'Records a short description of the company.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.M_TYPE_DESC is 'The description of the M Type.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.M_ITEM_DESC is 'Description of the m-item, e.g. ''Interest for Tax'', ''Rehab Credit'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.OPER_DESC is 'Description of the operating indicator, e.g. ''Electric'', ''Gas'', ''Non Operating''.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.JURISDICTION_DESC is 'Description of the taxing authority, e.g. ''Federal'', ''DC'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.ANNUAL_AMOUNT_EST is 'Estimated annual amount of the m-item in dollars.  Addition to taxable income is ''+''.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.TA_VERSION_ID is 'System-assigned identifier of a particular version.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.M_ID is 'System-assigned identifier for the unique key on the tax accrual control table.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.ERROR_MSG is 'Error message if something goes wrong with import';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_ACCRUAL_EST_IMPORT_STG.RUN_NUMBER is 'Run number used to archive multiple imports for same items';


-- archive table
create table TAX_ACCRUAL_EST_IMPORT_ARC
(
 COMPANY_ID        number(22,0) not null,
 M_ITEM_ID         number(22,0) not null,
 OPER_IND          number(22,0) not null,
 TA_NORM_ID        number(22,0) not null,
 GL_COMPANY_NO     varchar2(30),
 TAX_RETURN_KEY    varchar2(30),
 COMPANY_DESC      varchar2(50),
 M_TYPE_DESC       varchar2(50),
 M_ITEM_DESC       varchar2(80),
 OPER_DESC         varchar2(50),
 JURISDICTION_DESC varchar2(50),
 ANNUAL_AMOUNT_EST number(22,2),
 TA_VERSION_ID     number(22,0) not null,
 M_ID              number(22,0),
 ERROR_MSG         varchar2(254),
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 RUN_NUMBER        number(22,0) not null
);

alter table TAX_ACCRUAL_EST_IMPORT_ARC
   add constraint TA_EST_IMPORT_ARC_PK
       primary key (TA_VERSION_ID, COMPANY_ID, M_ITEM_ID, OPER_IND, TA_NORM_ID, RUN_NUMBER)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_EST_IMPORT_ARC is '(C) [08] The Tax Accrual Est Import Arc table is an archive table used to import annual estimates into Provision using an Excel template';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.M_ITEM_ID is 'System-assigned identifier of a particular m-item, e.g. ''AFUDC-Equity'', ''Pension Accrual'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.OPER_IND is 'System-assigned identifier of a segment below company for which taxes are separately calculated and booked, i.e. operating and non-operating.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.TA_NORM_ID is 'System-assigned identifier of a particular tax accrual normalization schema. Uses -1 for timing difference records';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.GL_COMPANY_NO is 'Records a company code or description used by external systems such as the general ledger system.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.TAX_RETURN_KEY is 'Records a user defined description of the external tax return compliance id.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.COMPANY_DESC is 'Records a short description of the company.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.M_TYPE_DESC is 'The description of the M Type.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.M_ITEM_DESC is 'Description of the m-item, e.g. ''Interest for Tax'', ''Rehab Credit'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.OPER_DESC is 'Description of the operating indicator, e.g. ''Electric'', ''Gas'', ''Non Operating''.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.JURISDICTION_DESC is 'Description of the taxing authority, e.g. ''Federal'', ''DC'', etc.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.ANNUAL_AMOUNT_EST is 'Estimated annual amount of the m-item in dollars.  Addition to taxable income is ''+''.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.TA_VERSION_ID is 'System-assigned identifier of a particular version.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.M_ID is 'System-assigned identifier for the unique key on the tax accrual control table.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.ERROR_MSG is 'Error message if something goes wrong with import';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_ACCRUAL_EST_IMPORT_ARC.RUN_NUMBER is 'Run number used to archive multiple imports for same items';

insert into PP_WINDOW_DISABLE_RESIZE
   (WINDOW_NAME, OBJECT_NAME)
   select 'w_tax_accrual_estimate_import', 'None' from DUAL;


----
----  Originally from : prov_37782_m_type.sql
----

update POWERPLANT_COLUMNS
   set PP_EDIT_TYPE_ID = 'e'
 where LOWER(TABLE_NAME) = 'tax_accrual_m_type'
   and LOWER(COLUMN_NAME) = 'm_type_id';


----
----  Originally from : prov_39730_CR_Balances_setup.sql
----

-- Provision CR Balances Interface setup scripts

-- Base table rows
insert into TAX_ACCRUAL_CUSTOM_TAB (TABLE_ID, TABLE_NAME) values (6, 'TAX_ACCRUAL_CR_BALANCES');

insert into TAX_ACCRUAL_CUSTOM_TAB_FIELDS
   (TABLE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, COLUMN_ORDER, COLUMN_TYPE, COLUMN_WIDTH,
    COLUMN_DECIMAL, AMOUNT_FIELD, KEY_FIELD, DESCRIPTION, TRANSLATE_FIELD)
values
   (6, 'SOURCE', TO_DATE('7-01-2014 15:53:06', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 1, 'VARCHAR',
    50, null, 0, 1, 'Source', 0);
insert into TAX_ACCRUAL_CUSTOM_TAB_FIELDS
   (TABLE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, COLUMN_ORDER, COLUMN_TYPE, COLUMN_WIDTH,
    COLUMN_DECIMAL, AMOUNT_FIELD, KEY_FIELD, DESCRIPTION, TRANSLATE_FIELD)
values
   (6, 'COMPANY', TO_DATE('7-01-2014 15:53:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 2, 'VARCHAR',
    50, null, 0, 1, 'GL Company Number', 1);
insert into TAX_ACCRUAL_CUSTOM_TAB_FIELDS
   (TABLE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, COLUMN_ORDER, COLUMN_TYPE, COLUMN_WIDTH,
    COLUMN_DECIMAL, AMOUNT_FIELD, KEY_FIELD, DESCRIPTION, TRANSLATE_FIELD)
values
   (6, 'ACCOUNT', TO_DATE('7-01-2014 15:53:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 3, 'VARCHAR',
    50, null, 0, 1, 'Account', 0);
insert into TAX_ACCRUAL_CUSTOM_TAB_FIELDS
   (TABLE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, COLUMN_ORDER, COLUMN_TYPE, COLUMN_WIDTH,
    COLUMN_DECIMAL, AMOUNT_FIELD, KEY_FIELD, DESCRIPTION, TRANSLATE_FIELD)
values
   (6, 'MONTH_NUMBER', TO_DATE('7-01-2014 15:53:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 22,
    'NUMBER', 22, 0, 0, 1, 'Month Number', 0);
insert into TAX_ACCRUAL_CUSTOM_TAB_FIELDS
   (TABLE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, COLUMN_ORDER, COLUMN_TYPE, COLUMN_WIDTH,
    COLUMN_DECIMAL, AMOUNT_FIELD, KEY_FIELD, DESCRIPTION, TRANSLATE_FIELD)
values
   (6, 'AMOUNT', TO_DATE('7-01-2014 15:53:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 23, 'NUMBER',
    22, 2, 1, 0, 'Amount', 0);
insert into TAX_ACCRUAL_CUSTOM_TAB_FIELDS
   (TABLE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, COLUMN_ORDER, COLUMN_TYPE, COLUMN_WIDTH,
    COLUMN_DECIMAL, AMOUNT_FIELD, KEY_FIELD, DESCRIPTION, TRANSLATE_FIELD)
values
   (6, 'DR_CR_ID', TO_DATE('7-01-2014 15:53:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 24, 'NUMBER',
    22, 0, 0, 0, 'Debit / Credit Indicator', 0);
insert into TAX_ACCRUAL_CUSTOM_TAB_FIELDS
   (TABLE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, COLUMN_ORDER, COLUMN_TYPE, COLUMN_WIDTH,
    COLUMN_DECIMAL, AMOUNT_FIELD, KEY_FIELD, DESCRIPTION, TRANSLATE_FIELD)
values
   (6, 'ID', TO_DATE('7-01-2014 15:53:07', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 25, 'NUMBER', 22,
    0, 0, 0, 'ID', 0);

-- Staging table

-- New Dynamic Staging Table

create table TAX_ACCRUAL_CR_BALANCES_STG1
(
 ID               number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 COL_A            varchar2(100),
 COL_B            varchar2(100),
 COL_C            varchar2(100),
 COL_D            varchar2(100),
 COL_E            varchar2(100),
 COL_F            varchar2(100),
 COL_G            varchar2(100),
 COL_H            varchar2(100),
 COL_I            varchar2(100),
 COL_J            varchar2(100),
 COL_K            varchar2(100),
 COL_L            varchar2(100),
 COL_M            varchar2(100),
 COL_N            varchar2(100),
 COL_O            varchar2(100),
 COL_P            varchar2(100),
 COL_Q            varchar2(100),
 COL_R            varchar2(100),
 COL_S            varchar2(100),
 COL_T            varchar2(100),
 COL_U            varchar2(100),
 COL_V            varchar2(100),
 COL_W            varchar2(100),
 COL_X            varchar2(100),
 COL_Y            varchar2(100),
 COL_Z            varchar2(100)
);

alter table TAX_ACCRUAL_CR_BALANCES_STG1
   add constraint TAX_ACCRUAL_CR_BAL_STG1_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_CR_BALANCES_STG1 is '(C) [08] The Tax Accrual CR Balances Stg 1 table is a staging table for the Provision GL Balances loader used to import data into the Tax Accrual CR Balances table. Columns A-Z correspond to the first 26 columns in an excel file that are loaded in';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.ID is 'ID is the system generated unique identifier for the Tax Accrual CR Balances Stg table.';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_A is 'Column A is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_B is 'Column B is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_C is 'Column C is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_D is 'Column D is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_E is 'Column E is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_F is 'Column F is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_G is 'Column G is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_H is 'Column H is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_I is 'Column I is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_J is 'Column J is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_K is 'Column K is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_L is 'Column L is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_M is 'Column M is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_N is 'Column N is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_O is 'Column O is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_P is 'Column P is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_Q is 'Column Q is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_R is 'Column R is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_S is 'Column S is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_T is 'Column T is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_U is 'Column U is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_V is 'Column V is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_W is 'Column W is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_X is 'Column X is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_Y is 'Column Y is a staging column';
comment on column TAX_ACCRUAL_CR_BALANCES_STG1.COL_Z is 'Column Z is a staging column';

-- STG 2 created in system


-- Translate table
create table TAX_ACCRUAL_CR_BALANCES_TRANS
(
 SOURCE     varchar2(50) not null,
 FIELD      varchar2(50) not null,
 OLD_ID     varchar2(50) not null,
 TIME_STAMP date,
 USER_ID    varchar2(18),
 OLD_DESC   varchar2(50),
 NEW_ID     varchar2(50),
 NEW_DESC   varchar2(50)
);

alter table TAX_ACCRUAL_CR_BALANCES_TRANS
   add constraint TAX_ACCRUAL_CR_BAL_TRANS_PK
       primary key (SOURCE, FIELD, OLD_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_CR_BALANCES_TRANS is '(C) [08] The Tax Accrual CR Balances Trans table is a translate table for the Provision GL Balances loader used to translate values from the import file to their corresponding values in the provision system';
comment on column TAX_ACCRUAL_CR_BALANCES_TRANS.SOURCE is 'The balances table source definition being used to import the data. This is a specific outline of an import template.';
comment on column TAX_ACCRUAL_CR_BALANCES_TRANS.FIELD is 'The column name that is being translated';
comment on column TAX_ACCRUAL_CR_BALANCES_TRANS.OLD_ID is 'The translated column value from the import file';
comment on column TAX_ACCRUAL_CR_BALANCES_TRANS.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_ACCRUAL_CR_BALANCES_TRANS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_ACCRUAL_CR_BALANCES_TRANS.OLD_DESC is 'The description field (if applicable) from the import file. Defaults to same value as old_id. Can be changed by user if desired.';
comment on column TAX_ACCRUAL_CR_BALANCES_TRANS.NEW_ID is 'The value that the old_id should be translated to during the load';
comment on column TAX_ACCRUAL_CR_BALANCES_TRANS.NEW_DESC is 'A translated description field that can be updated if desired. It is not used by the logic.';


-- Control Table
create table TAX_ACCRUAL_CR_BALANCES_CON
(
 SOURCE          varchar2(50) not null,
 CONTROL_NAME    varchar2(50) not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 CONTROL_VALUE   varchar2(200),
 DESCRIPTION     varchar2(100),
 DIRECT_MAP      varchar2(20),
 TRANSLATE_FIELD varchar2(20)
);

alter table TAX_ACCRUAL_CR_BALANCES_CON
   add constraint TAX_ACCRUAL_CR_BAL_CON_PK
       primary key (SOURCE, CONTROL_NAME)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_CR_BALANCES_CON is '(C) [08] The Tax Accrual CR Balances Con table is a control table for the Provision GL Balances loader, storing control information by source';
comment on column TAX_ACCRUAL_CR_BALANCES_CON.SOURCE is 'The balances table source definition being used to import the data. This is a specific outline of an import template.';
comment on column TAX_ACCRUAL_CR_BALANCES_CON.CONTROL_NAME is 'The name of the control parameter';
comment on column TAX_ACCRUAL_CR_BALANCES_CON.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_ACCRUAL_CR_BALANCES_CON.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_ACCRUAL_CR_BALANCES_CON.CONTROL_VALUE is 'The value of the control parameter';
comment on column TAX_ACCRUAL_CR_BALANCES_CON.DESCRIPTION is 'A description of the control parameter';
comment on column TAX_ACCRUAL_CR_BALANCES_CON.DIRECT_MAP is 'A field denoting if a column is being directly mapped from the staging table or is using logic. Only used by control rows involving mapping';
comment on column TAX_ACCRUAL_CR_BALANCES_CON.TRANSLATE_FIELD is 'a field denoting if a column should be translated after being loaded to the staging table. Only used by control rows involving mapping';


create table TAX_ACCRUAL_CR_BAL_UPDATES
(
 ID          number(22,0) not null,
 TIME_STAMP  date,
 USER_ID     varchar2(18),
 SOURCE      varchar2(50),
 DESCRIPTION varchar2(254),
 UPDATE_NAME varchar2(35),
 UPDATE_SQL  varchar2(2000),
 SEQUENCE    number(22,0)
);

alter table TAX_ACCRUAL_CR_BAL_UPDATES
   add constraint TA_CR_BAL_UPDATES_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_CR_BAL_UPDATES is '(C) [08] The Tax Accrual CR Balances Updates table is a table used to make SQL updates to Provision GL balances data after it has been loaded to the staging table';
comment on column TAX_ACCRUAL_CR_BAL_UPDATES.ID is 'ID is the system generated unique identifier for the Tax Accrual CR Bal Updates table.';
comment on column TAX_ACCRUAL_CR_BAL_UPDATES.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_ACCRUAL_CR_BAL_UPDATES.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_ACCRUAL_CR_BAL_UPDATES.SOURCE is 'The balances table source definition being used to import the data. This is a specific outline of an import template.';
comment on column TAX_ACCRUAL_CR_BAL_UPDATES.DESCRIPTION is 'Description of the SQL update';
comment on column TAX_ACCRUAL_CR_BAL_UPDATES.UPDATE_NAME is 'Name of the SQL update';
comment on column TAX_ACCRUAL_CR_BAL_UPDATES.UPDATE_SQL is 'Saved SQL script of the update';
comment on column TAX_ACCRUAL_CR_BAL_UPDATES.SEQUENCE is 'Order in which the update should run';


insert into PP_WINDOW_DISABLE_RESIZE
   (WINDOW_NAME, OBJECT_NAME)
   select 'w_tax_accrual_cr_balances', 'None' from DUAL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1459, 0, 10, 4, 3, 0, 39733, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039733_prov_various_1043.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;