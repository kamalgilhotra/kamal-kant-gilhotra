/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044184_taxrpr_repair_thresholds_base_year_2_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/06/2015 Daniel Motter  Creation
||============================================================================
*/

-- Fill the temporary column with first 4 digits of base_year
update repair_thresholds set base_year_temp = substr(base_year, 0, 4);

-- Clear out base_year
update repair_thresholds set base_year = null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2673, 0, 2015, 2, 0, 0, 044184, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044184_taxrpr_repair_thresholds_base_year_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;