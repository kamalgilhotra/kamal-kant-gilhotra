/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036261_projects_default_hist_future.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/05/2014 Chris Mardis   Point Release
||============================================================================
*/

--
-- Change defaults to zero
--
alter table WO_EST_MONTHLY modify HIST_ACTUALS default 0;
update WO_EST_MONTHLY set HIST_ACTUALS = 0 where HIST_ACTUALS is null;

alter table WO_EST_MONTHLY modify FUTURE_DOLLARS default 0;
update WO_EST_MONTHLY set FUTURE_DOLLARS = 0 where FUTURE_DOLLARS is null;

alter table WO_INTERFACE_MONTHLY_TEMP modify HIST_ACTUALS default 0;
alter table WO_INTERFACE_MONTHLY_TEMP modify FUTURE_DOLLARS default 0;

alter table WO_EST_MONTHLY_UPLOAD     modify HIST_ACTUALS default 0;
alter table WO_EST_MONTHLY_UPLOAD     modify FUTURE_DOLLARS default 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (943, 0, 10, 4, 2, 0, 36261, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036261_projects_default_hist_future.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;