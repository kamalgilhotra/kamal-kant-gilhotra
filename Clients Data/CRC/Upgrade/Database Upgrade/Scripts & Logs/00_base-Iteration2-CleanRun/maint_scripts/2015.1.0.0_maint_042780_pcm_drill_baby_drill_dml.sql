/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042780_pcm_drill_baby_drill_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 2/10/2015  LAlston           	 Insert Records for Drill Down options
||==========================================================================================
*/

--(SELECT MAX(FILTER_ID)+1
--			FROM PP_DYNAMIC_FILTER)
 
--* Dynamic Filters
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 26) /*WO -> FP ID*/;

insert into PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE) 
	values (263, 'Work Order Number(s)', 'sle', 'work_order_control.work_order_number', 'work_order_control.work_order_id in (select funding_wo_id from work_order_control where funding_wo_indicator = 0 and [selected_values])');
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 263) /*FP -> WO ID */;

--Funding Project Search Links
INSERT INTO PPBASE_WORKSPACE_LINKS (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WORKSPACE_IDENTIFIER, LINKED_WINDOW, LINKED_WINDOW_LABEL)
 VALUES ('pcm', 'fp_search', 1, 'wo_search', null, 'My Work Orders');
 
INSERT INTO PPBASE_WORKSPACE_LINKS (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WORKSPACE_IDENTIFIER, LINKED_WINDOW, LINKED_WINDOW_LABEL)
 VALUES ('pcm', 'fp_search', 2, null, 'w_budget_select_tabs', 'My Programs (FP)');
 
INSERT INTO PPBASE_WORKSPACE_LINKS (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WORKSPACE_IDENTIFIER, LINKED_WINDOW, LINKED_WINDOW_LABEL)
 VALUES ('pcm', 'fp_search', 3, 'fp_maint_job_tasks', null, null);
 
INSERT INTO PPBASE_WORKSPACE_LINKS (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WORKSPACE_IDENTIFIER, LINKED_WINDOW, LINKED_WINDOW_LABEL)
 VALUES ('pcm', 'fp_search', 4, 'rpt_reports', null, null);
 
 --Work Order Search Links
 INSERT INTO PPBASE_WORKSPACE_LINKS (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WORKSPACE_IDENTIFIER, LINKED_WINDOW, LINKED_WINDOW_LABEL)
 VALUES ('pcm', 'wo_search', 1, 'fp_search', null, 'My Funding Project');
 
INSERT INTO PPBASE_WORKSPACE_LINKS (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WORKSPACE_IDENTIFIER, LINKED_WINDOW, LINKED_WINDOW_LABEL)
 VALUES ('pcm', 'wo_search', 2, null, 'w_budget_select_tabs', 'My Programs (WO)');
 
INSERT INTO PPBASE_WORKSPACE_LINKS (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WORKSPACE_IDENTIFIER, LINKED_WINDOW, LINKED_WINDOW_LABEL)
 VALUES ('pcm', 'wo_search', 3, 'wo_maint_job_tasks', null, null);
 
INSERT INTO PPBASE_WORKSPACE_LINKS (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WORKSPACE_IDENTIFIER, LINKED_WINDOW, LINKED_WINDOW_LABEL)
 VALUES ('pcm', 'wo_search', 4, 'rpt_reports', null, null);
 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2285, 0, 2015, 1, 0, 0, 042780, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042780_pcm_drill_baby_drill_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;