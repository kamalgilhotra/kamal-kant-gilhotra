/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_049662_sys_01_syn_grant_t_num_array.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 11/29/2017 Shane "C" Ward     	Allow DW's to use t_num_array in select statements
||========================================================================================
*/
CREATE PUBLIC SYNONYM t_num_array for pwrplant.t_num_array;
GRANT execute ON t_num_array TO pwrplant_role_dev;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4056, 0, 2017, 1, 0, 0, 49662, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049662_sys_01_syn_grant_t_num_array.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;