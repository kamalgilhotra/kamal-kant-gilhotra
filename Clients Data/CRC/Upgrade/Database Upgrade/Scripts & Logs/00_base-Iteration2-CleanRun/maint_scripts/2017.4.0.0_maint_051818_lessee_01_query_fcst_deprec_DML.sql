/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051818_lessee_01_query_fcst_deprec_DML.sql
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2017.4.0.0 06/26/2018 K Powers  Fix end/begin reserve and rou fields
||============================================================================
*/

DECLARE
  V_ID NUMBER;
BEGIN

  update pp_any_query_criteria
     set SQL = 'sELECT fct.description as forecast_version, la.leased_asset_number, ilr.ilr_number, mla.lease_number, cs.description as company, sob.description as sob,
                to_number(to_char(ldf.MONTH,''yyyymm'')) as month, sch.depr_expense, sch.begin_reserve, sch.end_reserve, sch.end_capital_cost, 
                (sch.end_net_rou_asset) as ROU,llct.description as CURRENCY_TYPE,iso_code,currency_display_symbol 
                FROM ls_lease mla,
                     ls_ilr ilr,
                     ls_asset la,
                     ls_depr_forecast ldf,
                     company cs,
                     set_of_books sob,
                     v_ls_asset_schedule_fx_vw sch,
                     ls_forecast_version fct,
                     ls_lease_currency_type llct 
                WHERE la.ilr_id = ilr.ilr_id
                AND   ilr.lease_id = mla.lease_id
                AND   fct.revision = ldf.revision
                AND   abs(fct.set_of_books_id) = abs(sob.set_of_books_id)
                AND   la.ls_asset_id = ldf.ls_asset_id
                AND   la.company_id = cs.company_id 
                and   llct.ls_currency_type_id = sch.ls_cur_type 
                and   ldf.set_of_books_id = sob.set_of_books_id
                AND   la.ls_asset_status_id = 3
                and   sch.ls_asset_id = la.ls_asset_id
                and   sch.revision = fct.revision
                and   sch.month = ldf.month
                and   sch.set_of_books_id = sob.set_of_books_id
                ORDER BY la.leased_asset_number, ldf.MONTH, sob.description',is_multicurrency = 1 
   where description = 'Forecast Lease Depreciation';

END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7142, 0, 2017, 4, 0, 0, 51818, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051818_lessee_01_query_fcst_deprec_DML.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
