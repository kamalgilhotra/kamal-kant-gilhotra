/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035368_reg_PKG_REG_ANNUALIZATION.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014 Shane Ward
||============================================================================
*/

create or replace package PKG_REG_ANNUALIZATION
/*
||============================================================================
|| Application: PowerPlant (Rates and Regulatory)
|| Object Name: PKG_REG_ANNUALIZATION
|| Description: Reg Annualization Functions.
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date        Revised By         Reason for Change
|| ------- ----------  ---------------    ------------------------------------
|| 1.0     01/09/2014  Shane "C" Ward     Create
||============================================================================
*/
/*
******************************************************
**  NOTE: CALLER MUST HANDLE ROLLBACKS AND COMMITS  **
******************************************************
*/

 as

   procedure P_ANNUALIZEBYSOURCE(A_VERSIONID       number,
                                 A_SOURCEID        number,
                                 A_STARTDATE       number,
                                 A_ENDDATE         number,
                                 A_COMPANYID       number,
                                 A_LEDGERTABLENAME varchar2,
                                 A_ACCTTABLENAME   varchar2,
                                 A_VERSIONCOLNAME  varchar2);

   procedure P_ANNUALIZEDATA(A_VERSIONID       number,
                             A_STARTDATE       number,
                             A_ENDDATE         number,
                             A_COMPANYID       number,
                             A_LEDGERTABLENAME varchar2,
                             A_ACCTTABLENAME   varchar2,
                             A_VERSIONCOLNAME  varchar2,
                             A_WHEREACCT       varchar2,
                             A_ALLOWPARTIAL    number);

   procedure P_ANNUALIZEACCT(A_VERSIONID       number,
                             A_ACCTID          number,
                             A_STARTDATE       number,
                             A_ENDDATE         number,
                             A_COMPANYID       number,
                             A_LEDGERTABLENAME varchar2,
                             A_ACCTTABLENAME   varchar2,
                             A_VERSIONCOLNAME  varchar2);

   procedure P_ANNUALIZECASE(A_CASEID    number,
                             A_COMPANYID number,
                             A_YEAREND   number);

   procedure P_ANNUALIZECASE(A_CASEID    number,
                             A_COMPANYID number);

   procedure P_ANNUALIZEMONTHLYADJUSTMENTS(A_ADJUSTMENT number,
                                           A_CASEID     number,
                                           A_COMPANYID  number,
                                           A_ENDYEAR    number);

end PKG_REG_ANNUALIZATION;
/


Create or Replace package body PKG_REG_ANNUALIZATION as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************


   --**************************************************************
   --       VARIABLES
   --**************************************************************
   g_IncludeCaseId       BOOLEAN := FALSE;

   --Note: Each Annualization Type is stored in reg_annualization_type
   -- Types of Annualization
   --       13 Month Avg        (1)
   --       12 Month Avg        (2)
   --       2 Point Avg         (3)
   --       Ending Balance      (4)
   --       Sum of 12 Months    (5)
   --       Ending Month * 12   (6)
   --       Input               (7)
   --       13 Month Avg of Avg (8)




   --**************************************************************
   --       PROCEDURES
   --**************************************************************



   --**************************************************************************
   --                            p_AnnualizeMonthlyAdjustments
   --
   -- Procedure: p_AnnualizeMonthlyAdjustments
   --
   -- Description: Method used to Annualize a specified Case. To perform this
   -- annualization this method will annualize every account for the specified
   -- Case for each year speified.
   --
   -- Paramaters:
   --     a_adjustment: The ID of the adjustment to annualize.
   --     a_caseId: The ID of the regulatory case that the adjustment belongs to.
   --     a_companyId: The ID of the company that the case belongs to.
   --     a_endYear: The adjustment year end date.
   --
   --**************************************************************************

    PROCEDURE p_Annualizemonthlyadjustments(a_Adjustment NUMBER,
                                            a_Caseid     NUMBER,
                                            a_Companyid  NUMBER,
                                            a_Endyear    NUMBER) IS

    BEGIN

      PKG_REG_ANNUALIZATION.p_Annualizebysource(a_CaseId,
                                                0,
                                                a_Endyear,
                                                a_Endyear,
                                                a_Companyid,
                                                'reg_annualize_work',
                                                'reg_case_acct',
                                                'version_id');

    END p_Annualizemonthlyadjustments;

   --**************************************************************************
   --                            P_AnnualizeBySource
   --
   -- Procedure: P_AnnualizeBySource
   --
   -- Description: Used for REG_..._LEDGER. All supporting months must exist
   -- in the table to be able to generate an annualized amount.  For example, if
   -- the reg account uses the 12 Month Average annualization type the table must
   -- have the prior 11 months of data for the month that needs to be annualized.
   -- If the supporting months do not exist, the annualization type will be null.
   --
   -- Paramaters:
   --     a_versionId: The version ID (historical or forecast).
   --     a_sourceId: The source ID (0 = all sources).
   --     a_startDate
   --     a_endDatE
   --     a_companyId: The company ID that the various accounts that are to be
   --     annualized belong to.
   --     a_ledgerTableName: The name of the ledger table to pull data from for
   --     the annualization calculation.
   --     a_acctTableName: The account table to pull the annualization type from.
   --     a_versionColName: The column name that contains the version data
   --
   --**************************************************************************
  procedure p_AnnualizeBySource(a_versionId       number,
                                a_sourceId        number,
                                a_startDate       number,
                                a_endDate         number,
                                a_companyId       number,
                                a_ledgerTableName varchar2,
                                a_acctTableName   varchar2,
                                a_versionColName  varchar2) is

    reg_acct_source_id NUMBER;
    whereAcct          VARCHAR2(2000);

  begin
    --Make sure source ID is between 0 and 6 or greater than 100
    if a_sourceId not between 0 and 6 and a_sourceId < 100 then
      if a_sourceId is not null then
        --Error
        Raise_Application_Error(-20000,
                                to_char(a_sourceId) ||
                                ' is an unsupported source ID. Valid values are: 0 - all sources, 1 - ' ||
                                'Assets/Depr, 2 - CWIP/RWIP/WIP Computation, 3 - CR, 4 - Deferred Tax, ' ||
                                '5 - Property Tax, 6 - Tax Provision, or > 100 for external sources.');
      end IF;
    end IF;

    if lower(a_acctTableName) = 'reg_case_acct' then
      g_includeCaseId := true;
    else
      g_includeCaseId := false;
    end if;

    if a_sourceId > 100 then
      --Look up reg acct source
      BEGIN
        SELECT reg_acct_source_id
          INTO reg_acct_source_id
          FROM reg_ext_source
         WHERE reg_source_id = a_sourceId;
      EXCEPTION
        WHEN No_Data_Found THEN
          --set reg_acct_source_id to a_sourceId
          reg_acct_source_id := a_sourceId;
      END;
    end IF;

    if a_sourceId <> 0 then
      whereAcct := 'and nvl(lt.reg_source_id,' || to_char(a_sourceId) ||
                   ') = ' || to_char(a_sourceId) || '   ';
    else
      whereAcct := '';
    END IF;

    whereAcct := whereAcct ||
                 ' and lt.reg_acct_id in (select m.reg_acct_id from ' ||
                 a_acctTableName || ' m ';
    if a_sourceId <> 0 then
      if a_sourceId > 100 then
        whereAcct := whereAcct || ' where m.reg_source_id = ' ||
                     to_char(reg_acct_source_id) || '   ';
      else
        whereAcct := whereAcct || ' where m.reg_source_id = ' ||
                     to_char(a_sourceId) || '   ';
      end if;
    end if;

    if g_includeCaseId then
      if a_sourceId = 0 or a_sourceId < 100 then
        whereAcct := whereAcct || ' where ';
      else
        whereAcct := whereAcct || ' and ';
      end if;
      whereAcct := whereAcct || ' m.reg_case_id = ' || to_char(a_versionId);
    end if;

    PKG_REG_ANNUALIZATION.p_annualizeData(a_versionId,
                    a_startDate,
                    a_endDate,
                    a_companyId,
                    a_ledgerTableName,
                    a_acctTableName,
                    a_versionColName,
                    whereAcct,
                    0);

  end p_AnnualizeBySource;

  --**************************************************************************
  --                            P_AnnualizeAcct
  -- Procedure: p_AnnualizeAcct
  --
  -- Annualizes a single regulatory account based on annualization type from AcctTableName argument,
  --    i.e., REG_ACCT_MASTER.  Annualizaton is for LedgerTableName argument, i.e.,
  --    REG_HISTORY_LEDGER. All supporting months must exist in the table to be able to generate an annualized amount.
  --    For example, if the reg account uses the 12 Month Average annualization type the table must have the prior 11 months of
  --    data for the month that needs to be annualized. If the supporting months do not exist,
  --    the annualization type will be null.
  --
  --    CALLER MUST HANDLE ROLLBACKS AND COMMITS
  --
  -- Paramaters:
  --     a_versionId: The version ID (historical or forecast).
  --     a_startDate:
  --     a_endDate:
  --     a_companyId: The company ID that the various accounts that are to be annualized belong to.
  --     a_ledgerTableName: The name of the ledger table to pull data from for the annualization calculation.
  --     a_acctTableName: The account table to pull the annualization type from.
  --     a_versionColName: The column name that contains the version data
  --     a_whereacct: Where clause for annualization
  --
  --**************************************************************************
  procedure p_annualizeAcct(a_versionID       number,
                            a_acctId          number,
                            a_startDate       number,
                            a_endDate         number,
                            a_companyId       number,
                            a_ledgerTableName varchar2,
                            a_acctTableName   varchar2,
                            a_versionColName  varchar2) is
    where_acct          varchar2(2000);

  Begin
    if lower(a_acctTableName) = 'reg_case_acct' then
      g_includeCaseId := true;
    else
      g_includeCaseId := false;
    end if;

    where_acct := 'and lt.reg_acct_id in (select m.reg_acct_id from '||a_acctTableName
               ||' m where m.reg_acct_Id = '||to_char(a_acctId);

    if g_includeCaseId then
      where_acct := where_acct|| ' and m.reg_case_id = '||to_char(a_versionId);
    end if;

    PKG_REG_ANNUALIZATION.p_annualizedata(a_versionId,
                                           a_startDate,
                                          a_endDate,
                                          a_companyId,
                                          a_ledgerTableName,
                                          a_acctTableName,
                                          a_versionColName,
                                          where_Acct,
                                           0);

  end p_annualizeAcct;

  --**************************************************************************
  --                            P_12MoAvg
  --**************************************************************************
  --Annualization Logic for 12 Month Average
    procedure p_12MoAvg(a_versionId       number,
                      a_startDate       number,
                      a_endDate         number,
                      a_companyId       number,
                      a_ledgerTableName varchar2,
                      a_acctTableName   varchar2,
                      a_versionColName  varchar2,
                      a_whereAcct       varchar2,
                      a_amtColumn       VARCHAR2,
                      a_companyClause   varchar2) IS
    sqls          VARCHAR2(20000);

  BEGIN

    sqls := 'update ' || a_ledgerTableName || ' lt '
           || '   set annualized_amt = ('
           || '      select aa from ( '
           || '      select ROUND((SUM(' || a_amtColumn || ') '
           || '                OVER(PARTITION BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.reg_source_id '
           || '                ORDER BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.reg_source_id, x.gl_month '
           || '                ROWS BETWEEN 11 PRECEDING AND CURRENT ROW))/12,2) aa, '
           || '                x.* '
           || '                from ' || a_ledgerTableName || ' x) h '
           || '      where h.reg_company_id = lt.reg_company_id '
           || '      and h.reg_acct_id = lt.reg_acct_id '
           || '      and nvl(h.reg_source_id,-1) = nvl(lt.reg_source_id,-1) '
           || '      and h.' || a_versionColName || ' = lt.' || a_versionColName||''
           || '      and h.gl_month = lt.gl_month) '
           || '   where lt.' || a_versionColName || ' = ' || to_char(a_versionId) ||''
           || '   and lt.gl_month between ' || to_char(a_startDate) || ' and ' || to_char(a_endDate)||''
           || nvl(a_companyClause, ' ')
           || a_whereacct
           || '   and m.reg_annualization_id = 2)';

    dbms_output.put_line(sqls);

    execute immediate sqls;

  end p_12MoAvg;

  --**************************************************************************
  --                            p_2PointAvg
  --**************************************************************************
  --Annualization Logic for 2 Point Average
  procedure p_2PointAvg(a_versionId       number,
                        a_startDate       number,
                        a_endDate         number,
                        a_companyId       number,
                        a_ledgerTableName varchar2,
                        a_acctTableName   varchar2,
                        a_versionColName  varchar2,
                        a_whereAcct       varchar2,
                        a_amtColumn       VARCHAR2,
                        a_companyClause   varchar2) is

    sqls          VARCHAR2(20000);

  Begin

    sqls := 'update ' || a_ledgerTableName || ' lt  '
         || '   set annualized_amt = (  '
         || '      select aa from (   '
         || '      select ROUND((' || a_amtColumn || ' + LAG(' || a_amtColumn || ', 12, '
         || '                      '|| a_amtColumn || ')   '
         || '                             OVER(PARTITION BY x.' || a_versionColName || ', '
         || '                             x.reg_company_id, x.reg_acct_id, x.reg_source_id   '
         || '                             ORDER BY x.' || a_versionColName || ', '
         || '                             x.reg_company_id, x.reg_acct_id, x.reg_source_id, x.gl_month))/2,2) aa,   '
         || '                             x.gl_month, x.reg_acct_id, x.reg_source_id, x.reg_company_id, x.' || a_versionColName || '  '
         || '                             from ' || a_ledgerTableName || ' x) h  '
         || '                  where h.reg_company_id = lt.reg_company_id  '
         || '                  and h.reg_acct_id = lt.reg_acct_id'||'  '
         || '                  and nvl(h.reg_source_id,-1) = nvl(lt.reg_source_id,-1)  '
         || '                  and h.' || a_versionColName || ' = lt.' || a_versionColName||'  '
         || '                  and h.gl_month = lt.gl_month)  '
         || '   where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||' '
         || '   and lt.gl_month between ' || to_char(a_startDate) || ' and ' || to_char(a_endDate)||' '
         || nvl(a_companyClause, ' ')
         || a_whereacct
         || '      and m.reg_annualization_id = 3)';

         dbms_output.put_line(sqls);

     execute immediate sqls;

  end p_2PointAvg;


  --**************************************************************************
  --                            p_EndingBal
  --**************************************************************************
  --Annualization Logic for Ending Balance
  procedure p_EndingBal(a_versionId       number,
                        a_startDate       number,
                        a_endDate         number,
                        a_companyId       number,
                        a_ledgerTableName varchar2,
                        a_acctTableName   varchar2,
                        a_versionColName  varchar2,
                        a_whereAcct       varchar2,
                        a_amtColumn       VARCHAR2,
                        a_companyClause   varchar2) is

    sqls          VARCHAR2(20000);

  Begin

      sqls := 'update ' || a_ledgerTableName || ' lt'||'  '
           || '  set annualized_amt = ' || a_amtColumn||'  '
           || ' where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||'  '
           || '   and lt.gl_month between ' || to_char(a_startDate) || ' and ' || to_char(a_endDate)||'  '
           || nvl(a_companyClause, ' ')
           || a_whereAcct
           || '      and m.reg_annualization_id = 4)';

           dbms_output.put_line(sqls);

      execute immediate sqls;

  end p_endingBal;


  --**************************************************************************
  --                            p_SumOf12Months
  --**************************************************************************
  --Annualization Logic for Sum of 12 Months
  procedure p_sumOf12Months(a_versionId       number,
                            a_startDate       number,
                            a_endDate         number,
                            a_companyId       number,
                            a_ledgerTableName varchar2,
                            a_acctTableName   varchar2,
                            a_versionColName  varchar2,
                            a_whereAcct       varchar2,
                            a_amtColumn       VARCHAR2,
                            a_companyClause   varchar2) is

    sqls          VARCHAR2(20000);

  Begin

    sqls := 'update ' || a_ledgerTableName || ' lt'||'  '
         || '   set annualized_amt = ('||'  '
         || '    select aa from ( '||'  '
         || '      select ROUND((SUM(' || a_amtColumn || ') '||'  '
         || '                OVER(PARTITION BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.reg_source_id '||'  '
         || '                ORDER BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.reg_source_id, x.gl_month '||'  '
         || '                ROWS BETWEEN 11 PRECEDING AND CURRENT ROW)),2) aa, '||'  '
         || '            x.*'||'  '
         || '           from ' || a_ledgerTableName || ' x) h'||'  '
         || '     where h.reg_company_id = lt.reg_company_id'||'  '
         || '         and h.reg_acct_id = lt.reg_acct_id'||'  '
         || '      and nvl(h.reg_source_id,-1) = nvl(lt.reg_source_id,-1)  '
         || '         and h.' || a_versionColName || ' = lt.' || a_versionColName||'  '
         || '         and h.gl_month = lt.gl_month)'||'  '
         || ' where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||'  '
         || '  and lt.gl_month between ' || to_char(a_startDate) || ' and ' || to_char(a_endDate)||'  '
         || nvl(a_companyClause, ' ')
         || a_whereacct
         || '      and m.reg_annualization_id = 5)';

         dbms_output.put_line(sqls);

     execute immediate sqls;
  end p_sumOf12Months;

  --**************************************************************************
  --                            p_EndMonthX12
  --**************************************************************************
  --Annualization Logic for Ending Month * 12
  procedure p_endMonthX12(a_versionId       number,
                          a_startDate       number,
                          a_endDate         number,
                          a_companyId       number,
                          a_ledgerTableName varchar2,
                          a_acctTableName   varchar2,
                          a_versionColName  varchar2,
                          a_whereAcct       varchar2,
                          a_amtColumn       VARCHAR2,
                          a_companyClause   varchar2) is

    sqls          VARCHAR2(20000);

  Begin

      sqls := 'update ' || a_ledgerTableName || ' lt'||'  '
           || '   set annualized_amt = round((' || a_amtColumn || ') * 12,2)'||'  '
           || ' where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||'  '
           || '   and lt.gl_month between ' || to_char(a_startDate) || ' and ' || to_char(a_endDate)||'  '
           || nvl(a_companyClause, ' ')
           || a_whereacct
           || '      and m.reg_annualization_id = 6)';

           dbms_output.put_line(sqls);

      execute immediate sqls;
  end p_endMonthx12;

  --**************************************************************************
  --                            p_13MoAvg
  --**************************************************************************
  --Annualization Logic for 13 Month Avg
  procedure p_13moAvg(a_versionId       number,
                      a_startDate       number,
                      a_endDate         number,
                      a_companyId       number,
                      a_ledgerTableName varchar2,
                      a_acctTableName   varchar2,
                      a_versionColName  varchar2,
                      a_whereAcct       varchar2,
                      a_amtColumn       VARCHAR2,
                      a_companyClause   varchar2) is


    sqls          VARCHAR2(20000);

  Begin

  sqls := 'update ' || a_ledgerTableName || ' lt'||'  '
       || '  set annualized_amt = ('||'  '
       || '    select aa from ('||'  '
       || '      select ROUND((SUM(' || a_amtColumn || ') OVER(PARTITION BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.reg_source_id '
       || '                         ORDER BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.reg_source_id, x.gl_month ROWS BETWEEN 12 PRECEDING AND CURRENT ROW))/13,2) '
       || '                               aa, x.*'||'  '
       || '           from ' || a_ledgerTableName || ' x) h'||'  '
       || '     where h.reg_company_id = lt.reg_company_id'||'  '
       || '         and h.reg_acct_id = lt.reg_acct_id'||'  '
       || '      and nvl(h.reg_source_id,-1) = nvl(lt.reg_source_id,-1)  '
       || '         and h.' || a_versionColName || ' = lt.' || a_versionColName||'  '
       || '         and h.gl_month = lt.gl_month)'||'  '
       || ' where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||'  '
       || '   and lt.gl_month between ' || to_char(a_startDate) || ' AND ' || to_char(a_endDate)||'  '
       || nvl(a_companyClause, ' ')
       || a_whereacct
       || '      and m.reg_annualization_id = 1)';

       dbms_output.put_line(sqls);

  execute immediate sqls;


  end p_13moAvg;


    --**************************************************************************
  --                            p_13MoAvgOfAvg
  --**************************************************************************
  --Annualization Logic for 13 Month Avg of Avg
  procedure p_13moAvgOfAvg(a_versionId       number,
                           a_startDate       number,
                           a_endDate         number,
                           a_companyId       number,
                           a_ledgerTableName varchar2,
                           a_acctTableName   varchar2,
                           a_versionColName  varchar2,
                           a_whereAcct       varchar2,
                           a_amtColumn       VARCHAR2,
                           a_companyClause   varchar2) is
    sqls          VARCHAR2(20000);

  Begin

  sqls := 'update ' || a_ledgerTableName || ' lt'||'  '
       || '  set annualized_amt = ('||'  '
       || '    select aa from ('||'  '
       || '      select ROUND((' || a_amtColumn || ' * .5 + (LAG(' || a_amtColumn || ',13,'
       ||              a_amtColumn || ') OVER(PARTITION BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.reg_source_id ORDER BY x.'
       ||              a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.gl_month)) * .5 + sum(' || a_amtColumn || ') OVER(PARTITION BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id ORDER BY x.' || a_versionColName || ', x.reg_company_id, x.reg_acct_id, x.reg_source_id, x.gl_month ROWS BETWEEN 12 PRECEDING AND 1 PRECEDING) )/13,2) aa, x.gl_month, x.reg_acct_id, x.reg_source_id, x.reg_company_id, x.' || a_versionColName||'  '
       || '           from ' || a_ledgerTableName || ' x) h'||'  '
       || '       where h.reg_company_id = lt.reg_company_id'||'  '
       || '         and h.reg_acct_id = lt.reg_acct_id'||'  '
       || '      and nvl(h.reg_source_id,-1) = nvl(lt.reg_source_id,-1)  '
       || '         and h.' || a_versionColName || ' = lt.' || a_versionColName||'  '
       || '         and h.gl_month = lt.gl_month)'||'  '
       || ' where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||'  '
       || '   and lt.gl_month between ' || to_char(a_startDate) || ' and ' || to_char(a_endDate)||'  '
       || nvl(a_companyClause, ' ')
       || a_whereacct
       || '      and m.reg_annualization_id = 8)';

       dbms_output.put_line(sqls);

       execute immediate sqls;

  end p_13moAvgOfAvg;


    --**************************************************************************
  --                            p_CleanFor12Month
  --**************************************************************************
  -- Annualization Logic for nulling out annualized amounts for
  --               annualization methods that require 12 Months
  procedure p_CleanFor12Month(a_versionId       number,
                              a_startDate       number,
                              a_endDate         number,
                              a_companyId       number,
                              a_ledgerTableName varchar2,
                              a_acctTableName   varchar2,
                              a_versionColName  varchar2,
                              a_whereAcct       varchar2,
                              a_amtColumn       VARCHAR2,
                              a_companyClause   varchar2) is
    sqls          VARCHAR2(20000);
    caseClause    VARCHAR2(2000);
  Begin

    if g_includeCaseId then
      caseClause := '      and m.reg_case_id = ' || to_char(a_versionId)||'  ';
    else
      caseClause := ' ';
    end if;

    sqls := 'update ' || a_ledgerTableName || ' lt '||'  '
         || '  set annualized_amt = null'||'  '
         || ' where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||'  '
         || '   and lt.gl_month between ' || to_char(a_startDate) || ' AND + ' || to_char(a_endDate)||'  '
         || nvl(a_companyClause, ' ')
         || '  and exists ('||'  '
         || '    select 1 from ' || dbms_assert.simple_sql_name(a_acctTableName) || ' m'||'  '
         || '       where m.reg_acct_id = lt.reg_acct_id'||'  '
         || nvl(caseClause, ' ')
         || '      and m.reg_annualization_id in (2,5))'||'  '
         || '         and lt.gl_month IN ('||'  '
         || '           select r.gl_month - LAG(r.gl_month,11,0) OVER (PARTITION BY r.reg_acct_id order by r.reg_acct_id, r.gl_month)'||'  '
         || '             from ' || a_ledgerTableName || ' r'||'  '
         || '             where r.' || a_versionColName || ' = lt.' || a_versionColName||'  '
         || '              and r.reg_company_id = lt.reg_company_id'||'  '
         || '              and r.reg_acct_id = lt.reg_acct_id)'||'  '
         || a_whereAcct ||')';

         dbms_output.put_line(sqls);

    execute immediate sqls;

  end p_CleanFor12Month;


    --**************************************************************************
  --                            p_CleanFor13Month
  --**************************************************************************
  -- Annualization Logic for nulling out annualized amounts for
  --               annualization methods that require 13 Months
  procedure p_CleanFor13Month(a_versionId       number,
                              a_startDate       number,
                              a_endDate         number,
                              a_companyId       number,
                              a_ledgerTableName varchar2,
                              a_acctTableName   varchar2,
                              a_versionColName  varchar2,
                              a_whereAcct       varchar2,
                              a_amtColumn       VARCHAR2,
                              a_companyClause   varchar2) is
    sqls varchar2(20000);
    caseClause varchar2(2000);

  Begin

    if g_includeCaseId then
      caseClause := '      and m.reg_case_id = ' || to_char(a_versionId)||'  ';
    else
      caseClause := ' ';
    end if;

    sqls  := 'update ' || a_ledgerTableName || ' lt '||'  '
     || '  set annualized_amt = null'||'  '
     || ' where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||'  '
     || '  and lt.gl_month between ' || to_char(a_startDate) || ' AND + ' || to_char(a_endDate)||'  '
     || nvl(a_companyClause, ' ')
     || '  and exists ('||'  '
     || '    select 1 from ' || dbms_assert.simple_sql_name(a_acctTableName) || ' m'||'  '
     || '       where m.reg_acct_id = lt.reg_acct_id'||'  '
     || nvl(caseClause, ' ')
     || '       and m.reg_annualization_id in (1,3))'||'  '
     || '       and lt.gl_month in ('||'  '
     || '         select r.gl_month - lag(r.gl_month,12,0) OVER (PARTITION BY r.reg_acct_id order by r.reg_acct_id, r.gl_month)'||'  '
     || '           from ' || a_ledgerTableName || ' r'||'  '
     || '           where r.' || a_versionColName || ' = lt.' || a_versionColName||'  '
     || '            and r.reg_company_id = lt.reg_company_id'||'  '
     || '           and r.reg_acct_id = lt.reg_acct_id)'||'  '
     || a_whereAcct||')';

     dbms_output.put_line(sqls);

     execute immediate sqls;

  end p_CleanFor13Month;


    --**************************************************************************
  --                            p_CleanFor14Month
  --**************************************************************************
  -- Annualization Logic for nulling out annualized amounts for
  --               annualization methods that require 14 Months
  procedure p_CleanFor14Month(a_versionId       number,
                              a_startDate       number,
                              a_endDate         number,
                              a_companyId       number,
                              a_ledgerTableName varchar2,
                              a_acctTableName   varchar2,
                              a_versionColName  varchar2,
                              a_whereAcct       varchar2,
                              a_amtColumn       VARCHAR2,
                              a_companyClause   varchar2) is
    sqls varchar2(20000);
    caseClause varchar2(2000);

  Begin

    if g_includeCaseId then
      caseClause := '      and m.reg_case_id = ' || to_char(a_versionId)||'  ';
    else
      caseClause := ' ';
    end if;

    sqls := 'update ' || a_ledgerTableName || ' lt '||'  '
         || '  set annualized_amt = null'||'  '
         || ' where lt.' || a_versionColName || ' = ' || to_char(a_versionId)||'  '
         || '  and lt.gl_month between ' || to_char(a_startDate) || ' AND + ' || to_char(a_endDate)||'  '
         || nvl(a_companyClause, ' ')
         || '  and exists ('||'  '
         || '    select 1 from ' || dbms_assert.simple_sql_name(a_acctTableName) || ' m'||'  '
         || '       where m.reg_acct_id = lt.reg_acct_id'||'  '
         || nvl(caseClause, ' ')
         || '       and m.reg_annualization_id in (8))'||'  '
         || '       and lt.gl_month in ('||'  '
         || '         select r.gl_month - lag(r.gl_month,13,0) OVER (PARTITION BY r.reg_acct_id order by r.reg_acct_id, r.gl_month)'||'  '
         || '           from ' || a_ledgerTableName || ' r'||'  '
         || '           where r.' || a_versionColName || ' = lt.' || a_versionColName||'  '
         || '            and r.reg_company_id = lt.reg_company_id'||'  '
         || '           and r.reg_acct_id = lt.reg_acct_id)'||'  '
         || a_whereAcct||')';

         dbms_output.put_line(sqls);

    execute immediate sqls;

  end p_CleanFor14Month;

  --**************************************************************************
  --                            p_AnnualizeCase
  -- Procedure: p_AnnualizeCase
  --
  -- Description: Method used to Annualize a specified Case. To perform this
  -- annualization this method will annualize every account for the specified
  -- Case for each year speified.
  --
  -- Paramaters:
  --     a_caseId: The ID of the regulatory case to annualize.
  --     a_companyId: The ID of the company that the case belongs to.
  --     a_yearEnd: The year end to annualize
  --
  -- Note: Historic_flag: 1 = true 0 = false
  -- Returns: Nested Table that contains version information
  --***************************************************************************

  PROCEDURE p_Annualizecase(a_Caseid    NUMBER,
                            a_Companyid NUMBER,
                            a_Yearend   NUMBER) IS
    Rangestart        NUMBER;
    Numversions      NUMBER;
    Versionstart     NUMBER;
    Versionend       NUMBER;
    Historic_Flag    NUMBER;
    Tempversionstart NUMBER;
    Versionid        NUMBER;
    i                NUMBER;

  BEGIN
    RangeStart := to_number(to_char(Add_months((to_date(a_yearEnd, 'YYYYMM')),-12), 'yyyymm'));

    FOR c_Versions IN (SELECT Reg_Case_Id,
                              Hist_Version_Id,
                              Fore_Version_Id,
                              Start_Date,
                              End_Date
                       FROM Reg_Case_Coverage_Versions
                       WHERE Reg_Case_Id = a_Caseid
                       AND Start_Date BETWEEN RangeStart AND a_Yearend
                       AND End_Date BETWEEN RangeStart AND a_Yearend
                       ORDER BY Start_Date ASC)
    LOOP

      Versionstart := c_Versions.Start_Date;
      Versionend   := c_Versions.End_Date;

      IF Versionstart = rangestart
      THEN
        Tempversionstart := to_number(to_char(Add_months((to_date(VersionStart, 'YYYYMM')),-1), 'yyyymm'));
      ELSE
        Tempversionstart := Versionstart;
      END IF;

      IF c_Versions.Hist_Version_Id > 0
      THEN
        Historic_Flag := 1;

        Versionid := c_Versions.Hist_Version_Id;

        INSERT INTO Reg_Annualize_Work
          (Reg_Acct_Id, Gl_Month, Act_Amount, Version_Id, Reg_Company_Id)
          SELECT l.Reg_Acct_Id,
                 l.Gl_Month,
                 SUM(Nvl(l.Act_Amount, 0) + Nvl(l.Adj_Amount, 0) +
                     Nvl(l.Recon_Adj_Amount, 0)),
                 a_Caseid,
                 l.Reg_Company_Id
          FROM Reg_History_Ledger l, Reg_Case_Acct a
          WHERE l.Reg_Company_Id = a_Companyid
          AND l.Gl_Month BETWEEN Tempversionstart AND Versionend
          AND l.Historic_Version_Id = Versionid
          AND l.Reg_Acct_Id = a.Reg_Acct_Id
          AND a.Reg_Case_Id = a_Caseid
          AND a.Status_Code_Id = 1
          GROUP BY l.Reg_Acct_Id, l.Gl_Month, l.Reg_Company_Id;

        FOR i IN Tempversionstart .. Versionend
        LOOP
          IF To_Number(Substr(To_Char(i), 5)) > 12 OR
             To_Number(Substr(To_Char(i), 5)) < 1
          THEN
            Continue;
          END IF;

          INSERT INTO Reg_Annualize_Work
            (Reg_Acct_Id, Gl_Month, Act_Amount, Version_Id, Reg_Company_Id)
            SELECT a.Reg_Acct_Id, i, 0, a_Caseid, a_Companyid
            FROM Reg_Case_Acct a
            WHERE a.Reg_Case_Id = a_Caseid
            AND a.Status_Code_Id = 1
            AND NOT EXISTS (SELECT 1
                   FROM Reg_History_Ledger l
                   WHERE l.Historic_Version_Id = Versionid
                   AND l.Gl_Month = i
                   AND l.Reg_Company_Id = a_Companyid
                   AND l.Reg_Acct_Id = a.Reg_Acct_Id);

        END LOOP;

      ELSE
        Historic_Flag := 0;

        Versionid := c_Versions.Fore_Version_Id;

        INSERT INTO Reg_Annualize_Work
          (Reg_Acct_Id, Gl_Month, Act_Amount, Version_Id, Reg_Company_Id)
          SELECT l.Reg_Acct_Id,
                 l.Gl_Month,
                 SUM(l.Fcst_Amount),
                 a_Caseid,
                 l.Reg_Company_Id
          FROM Reg_Forecast_Ledger l, Reg_Case_Acct a
          WHERE l.Reg_Company_Id = a_Companyid
          AND l.Gl_Month BETWEEN Tempversionstart AND Versionend
          AND l.Forecast_Version_Id = Versionid
          AND l.Reg_Acct_Id = a.Reg_Acct_Id
          AND a.Reg_Case_Id = a_Caseid
          AND a.Status_Code_Id = 1
          GROUP BY l.Reg_Acct_Id, l.Gl_Month, l.Reg_Company_Id;

        FOR i IN Tempversionstart .. Versionend
        LOOP
          IF To_Number(Substr(To_Char(i), 5)) > 12 OR
             To_Number(Substr(To_Char(i), 5)) < 1
          THEN
            Continue;
          END IF;

          INSERT INTO Reg_Annualize_Work
            (Reg_Acct_Id, Gl_Month, Act_Amount, Version_Id, Reg_Company_Id)
            SELECT a.Reg_Acct_Id, i, 0, a_Caseid, a_Companyid
            FROM Reg_Case_Acct a
            WHERE a.Reg_Case_Id = a_Caseid
            AND a.Status_Code_Id = 1
            AND NOT EXISTS (SELECT 1
                   FROM Reg_Forecast_Ledger l
                   WHERE l.Forecast_Version_Id = Versionid
                   AND l.Gl_Month = i
                   AND l.Reg_Company_Id = a_Companyid
                   AND l.Reg_Acct_Id = a.Reg_Acct_Id);

        END LOOP;

      END IF;

    END LOOP;

    p_Annualizebysource(a_Caseid,
                        0,
                        to_number(to_char(Add_months((to_date(rangeStart, 'YYYYMM')),-1), 'yyyymm')),
                        a_Yearend,
                        a_Companyid,
                        'reg_annualize_work',
                        'reg_case_acct',
                        'version_id');

    MERGE INTO Reg_Case_Summary_Ledger d
    USING (SELECT Version_Id     Reg_Case_Id,
                  Reg_Acct_Id,
                  Gl_Month       Case_Year,
                  Annualized_Amt
           FROM Reg_Annualize_Work
           WHERE Annualized_Amt IS NOT NULL
           AND Gl_Month = a_Yearend) s
    ON (d.Reg_Case_Id = s.Reg_Case_Id AND d.Reg_Acct_Id = s.Reg_Acct_Id AND d.Case_Year = s.Case_Year)
    WHEN MATCHED THEN
      UPDATE
      SET d.Total_Co_Ledger_Amount   = s.Annualized_Amt,
          d.Total_Co_Included_Amount = s.Annualized_Amt,
          d.Total_Co_Final_Amount    = s.Annualized_Amt
    WHEN NOT MATCHED THEN
      INSERT
        (Reg_Case_Id,
         Reg_Acct_Id,
         Case_Year,
         Total_Co_Ledger_Amount,
         Total_Co_Included_Amount,
         Total_Co_Adjust_Amount,
         Total_Co_Final_Amount,
         Jur_Allo_Percent,
         Jur_Allo_Results_Amount,
         Jur_Adjust_Amount,
         Jur_Final_Amount,
         Jur_Recovery_Class_Amount,
         Jur_Base_Rev_Req_Amount)
      VALUES
        (s.Reg_Case_Id,
         s.Reg_Acct_Id,
         s.Case_Year,
         s.Annualized_Amt,
         s.Annualized_Amt,
         0,
         s.Annualized_Amt,
         0,
         0,
         0,
         0,
         0,
         0);

    INSERT INTO Reg_Case_Summary_Ledger
      (Reg_Case_Id,
       Reg_Acct_Id,
       Case_Year,
       Total_Co_Ledger_Amount,
       Total_Co_Included_Amount,
       Total_Co_Adjust_Amount,
       Total_Co_Final_Amount,
       Jur_Allo_Percent,
       Jur_Allo_Results_Amount,
       Jur_Adjust_Amount,
       Jur_Final_Amount,
       Jur_Recovery_Class_Amount,
       Jur_Base_Rev_Req_Amount)
      SELECT c.Reg_Case_Id,
             c.Reg_Acct_Id,
             a_yearEnd,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0
      FROM Reg_Case_Acct c
      WHERE c.Reg_Case_Id = a_Caseid
      AND NOT EXISTS (SELECT 1
             FROM Reg_Case_Summary_Ledger l
             WHERE l.Reg_Case_Id = c.Reg_Case_Id
             AND l.Case_Year = a_yearEnd
             AND l.Reg_Acct_Id = c.Reg_Acct_Id);

  END p_Annualizecase;

  --**************************************************************************
  --                            p_AnnualizeCase
  --
  -- Procedure: p_AnnualizeCase
  --
  -- Description: Method used to Annualize a specified Case. To perform this
  -- annualization this method will annualize every account for the specified
  -- Case for each year specified.
  --
  -- Paramaters:
  --     a_caseId: The ID of the regulatory case to annualize.
  --     a_companyId: The ID of the company that the case belongs to.
  --**************************************************************************

  PROCEDURE p_Annualizecase(a_Caseid    NUMBER,
                            a_Companyid NUMBER) IS

  BEGIN

    FOR c_Yearends IN (SELECT DISTINCT Coverage_Yr_Ended
                       FROM Reg_Case_Coverage_Versions
                       WHERE Reg_Case_Id = a_Caseid)
    LOOP
      Pkg_Reg_Annualization.p_Annualizecase(a_Caseid,
                                            a_Companyid,
                                            c_Yearends.Coverage_Yr_Ended);

    END LOOP;

  END p_Annualizecase;
  --**************************************************************************
  --                            p_AnnualizeData
  --
  --  Annualizes data supported a single regulatory account based on annualization type
  --  from a_AccTableName argument. Annualization is for a_LedgerTableName.
  --  All supporting months must exist in the table to be able to generate an annualized amount.
  --  For example, if the reg account uses the 12 Month Average annualization type the table must have the prior
  --  11 months of data for the month that needs to be annualized. If the supporting months do not exist,
  --  the annualized amount for the first months will be null
  --
  --  a_allowPartial: 0 = false 1 = true
  --**************************************************************************


  PROCEDURE p_Annualizedata(a_Versionid       NUMBER,
                            a_Startdate       NUMBER,
                            a_Enddate         NUMBER,
                            a_Companyid       NUMBER,
                            a_Ledgertablename VARCHAR2,
                            a_Accttablename   VARCHAR2,
                            a_Versioncolname  VARCHAR2,
                            a_Whereacct       VARCHAR2,
                            a_Allowpartial    NUMBER) IS

    Amtcolumn     VARCHAR2(2000);
    Companyclause VARCHAR2(2000);

  BEGIN
    IF Lower(a_Ledgertablename) = 'reg_forecast_ledger'
    THEN
      Amtcolumn := '(nvl(fcst_amount,0) + nvl(adj_amount,0))';
    ELSIF Lower(a_Ledgertablename) = 'reg_history_ledger'
    THEN
      Amtcolumn := '(nvl(act_amount,0) + nvl(adj_amount,0) + nvl(recon_adj_amount,0))';
    ELSE
      Amtcolumn := 'act_amount';
    END IF;

    IF a_Companyid IS NOT NULL AND a_Companyid <> 0
    THEN
      Companyclause := ' and lt.reg_company_id = ' || To_Char(a_Companyid) || ' ';
    ELSE
      Companyclause := ' ';
    END IF;

    --12 month avg
    p_12moavg(a_Versionid,
              a_Startdate,
              a_Enddate,
              a_Companyid,
              a_Ledgertablename,
              a_Accttablename,
              a_Versioncolname,
              a_Whereacct,
              Amtcolumn,
              Companyclause);
    --2 Point Annual Avg
    p_2pointavg(a_Versionid,
                a_Startdate,
                a_Enddate,
                a_Companyid,
                a_Ledgertablename,
                a_Accttablename,
                a_Versioncolname,
                a_Whereacct,
                Amtcolumn,
                Companyclause);
    --Ending Balance
    p_Endingbal(a_Versionid,
                a_Startdate,
                a_Enddate,
                a_Companyid,
                a_Ledgertablename,
                a_Accttablename,
                a_Versioncolname,
                a_Whereacct,
                Amtcolumn,
                Companyclause);
    --Sum of 12 Months
    p_Sumof12months(a_Versionid,
                    a_Startdate,
                    a_Enddate,
                    a_Companyid,
                    a_Ledgertablename,
                    a_Accttablename,
                    a_Versioncolname,
                    a_Whereacct,
                    Amtcolumn,
                    Companyclause);
    --Ending Month*12
    p_Endmonthx12(a_Versionid,
                  a_Startdate,
                  a_Enddate,
                  a_Companyid,
                  a_Ledgertablename,
                  a_Accttablename,
                  a_Versioncolname,
                  a_Whereacct,
                  Amtcolumn,
                  Companyclause);
    --13 Month Average
    p_13moavg(a_Versionid,
              a_Startdate,
              a_Enddate,
              a_Companyid,
              a_Ledgertablename,
              a_Accttablename,
              a_Versioncolname,
              a_Whereacct,
              Amtcolumn,
              Companyclause);
    --13 Month Average of Averages
    p_13moavgofavg(a_Versionid,
                   a_Startdate,
                   a_Enddate,
                   a_Companyid,
                   a_Ledgertablename,
                   a_Accttablename,
                   a_Versioncolname,
                   a_Whereacct,
                   Amtcolumn,
                   Companyclause);

    IF a_Allowpartial < 1
    THEN
      -- Update annualized amount to null for reg accounts that have 12 month annualization type for dates less than startDate + 11
      p_Cleanfor12month(a_Versionid,
                        a_Startdate,
                        a_Enddate,
                        a_Companyid,
                        a_Ledgertablename,
                        a_Accttablename,
                        a_Versioncolname,
                        a_Whereacct,
                        Amtcolumn,
                        Companyclause);
      -- Update annualized amount to null for reg accounts that have a 13 month annualization type for dates less than startDate + 12
      p_Cleanfor13month(a_Versionid,
                        a_Startdate,
                        a_Enddate,
                        a_Companyid,
                        a_Ledgertablename,
                        a_Accttablename,
                        a_Versioncolname,
                        a_Whereacct,
                        Amtcolumn,
                        Companyclause);
      -- Update annualized amount to null for reg accounts that have a 14 month annualization type for dates less than startDate + 13
      p_Cleanfor14month(a_Versionid,
                        a_Startdate,
                        a_Enddate,
                        a_Companyid,
                        a_Ledgertablename,
                        a_Accttablename,
                        a_Versioncolname,
                        a_Whereacct,
                        Amtcolumn,
                        Companyclause);

    END IF;

  END p_Annualizedata;


end PKG_REG_ANNUALIZATION;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (908, 0, 10, 4, 2, 0, 35368, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035368_reg_PKG_REG_ANNUALIZATION.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;