
 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_043497_prov_02_20151_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/08/2015 	Jarrett Skov   Deletion and re-insertion of data in tax_accrual_rep_cons_rows to configure proper functionality of the 51052 and 54513 reports.
 ||============================================================================
 */ 

-- **********************
-- 54513 fix
-- **********************

delete from tax_accrual_rep_cons_rows where rep_cons_type_id = 33 and row_id = 24 and rownum = 1
and (rep_cons_type_id, row_type, row_id) in 
( select rep_cons_type_id, row_type, row_id
  from tax_accrual_rep_cons_rows 
  having count(*) > 1
  group by  rep_cons_type_id, row_type, row_id
)
;

-- Duplicate rows that have different row IDs
delete from tax_accrual_rep_cons_rows
where (rep_cons_type_id, row_type, row_id) 
in (
  select rep_cons_type_id, row_type, row_id
  from 
  ( 
  select rep_cons_type_id, row_type, group_id, rank, max(row_id) row_id 
  from tax_accrual_rep_cons_rows where rep_cons_type_id in (30, 31, 32, 33)
  and (rep_cons_type_id, row_type, group_id, rank) in
    ( select rep_cons_type_id, row_type, group_id, rank
      from tax_accrual_rep_cons_rows
      where rep_cons_type_id in (30, 31, 32, 33)
      having count(*) > 1
      group by rep_cons_type_id, row_type, group_id, rank
    )
  group by rep_cons_type_id, row_type, group_id, rank
  order by rep_cons_type_id, group_id, row_type, rank
  )
)
;

commit;

-- **********************
-- 51052 fix
-- **********************

delete from tax_accrual_rep_cons_rows where rep_cons_type_id in (11,12,13,14,15,16);

INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 1, 'H', -1, 1, 1, 'TEXT', 1, 'Deferred Tax Rollforward by Month', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'parent_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'gl_month_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 6, 'H', -1, 6, 0, 'FILLER', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 7, 'D', 1, 1, 0, 'COL_VALUE_NUM', 0, 'dit_act_total', 'COL_VALUE_TEXT', 'detail_description', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 8, 'H', 2, 1, 0, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 9, 'F', 2, 1, 1, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 10, 'F', 2, 2, 0, 'TEXT', 0, NULL, 'FILLER', NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	11, 11, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'dit_balance_all', 'TEXT', 'Grand Total', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 1, 'H', -1, 1, 1, 'TEXT', 1, 'M Item Rollforward by Month', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'parent_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'gl_month_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 6, 'H', -1, 6, 0, 'FILLER', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 7, 'D', 1, 1, 0, 'COL_VALUE_NUM', 0, 'm_activity_total', 'COL_VALUE_TEXT', 'detail_description', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 8, 'H', 2, 1, 0, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 9, 'F', 2, 1, 1, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 10, 'F', 2, 2, 0, 'TEXT', 0, NULL, 'FILLER', NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	12, 11, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'm_activity_total_tot', 'TEXT', 'Grand Total', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 1, 'H', -1, 1, 1, 'TEXT', 1, 'Deferred Tax Rollforward by Month Type', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'parent_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'gl_month_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 6, 'H', -1, 6, 0, 'FILLER', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 7, 'D', 1, 1, 0, 'COL_VALUE_NUM', 0, 'dit_act_total', 'COL_VALUE_TEXT', 'detail_description', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 8, 'H', 2, 1, 0, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 9, 'F', 2, 1, 1, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 10, 'F', 2, 2, 0, 'TEXT', 0, NULL, 'FILLER', NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	13, 11, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'dit_balance_all', 'TEXT', 'Grand Total', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 1, 'H', -1, 1, 1, 'TEXT', 1, 'M Item Rollforward by Month Type', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'parent_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'gl_month_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 6, 'H', -1, 6, 0, 'FILLER', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 7, 'D', 1, 1, 0, 'COL_VALUE_NUM', 0, 'm_activity_total', 'COL_VALUE_TEXT', 'detail_description', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 8, 'H', 2, 1, 0, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 9, 'F', 2, 1, 1, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 10, 'F', 2, 2, 0, 'TEXT', 0, NULL, 'FILLER', NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	14, 11, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'm_activity_total_tot', 'TEXT', 'Grand Total', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 1, 'H', -1, 1, 1, 'TEXT', 1, 'Deferred Tax Rollforward by Company', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'gl_month_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 6, 'H', -1, 6, 0, 'FILLER', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 7, 'D', 1, 1, 0, 'COL_VALUE_NUM', 0, 'end_dit_balance', 'COL_VALUE_TEXT', 'detail_description', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 8, 'H', 2, 1, 0, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 9, 'F', 2, 1, 1, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 10, 'F', 2, 2, 0, 'TEXT', 0, NULL, 'FILLER', NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	15, 11, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'compute_45', 'TEXT', 'Grand Total', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 1, 'H', -1, 1, 1, 'TEXT', 1, 'M Item Rollforward by Company', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'gl_month_descr', NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 6, 'H', -1, 6, 0, 'FILLER', 0, NULL, NULL, NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 7, 'D', 1, 1, 0, 'COL_VALUE_NUM', 0, 'm_balance_end', 'COL_VALUE_TEXT', 'detail_description', 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 8, 'H', 2, 1, 0, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 9, 'F', 2, 1, 1, 'TEXT', 0, NULL, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 10, 'F', 2, 2, 0, 'TEXT', 0, NULL, 'FILLER', NULL, 'T', NULL, NULL);
INSERT INTO tax_accrual_rep_cons_rows (rep_cons_type_id, row_id, row_type, group_id, rank, color_id, 
	value_type, is_report_title, row_value, label_value_type, label_value, col_format, display_ind_field, total_col_fieldname) 
VALUES (	16, 11, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'm_balance_end_cons', 'TEXT', 'Grand Total', 'T', NULL, NULL);




-- ##JAS CHANGE 04/09/2015 Fix for duplicate rows on other report (rep_cons_type_id 8) found in other databases
delete from TAX_ACCRUAL_REP_CONS_ROWS
 where rowid in
       (select MAX_ROW
          from (select max(A.ROWID) MAX_ROW, A.REP_CONS_TYPE_ID, A.ROW_TYPE, A.GROUP_ID, A.RANK
                  from TAX_ACCRUAL_REP_CONS_ROWS A
                 where (REP_CONS_TYPE_ID, ROW_TYPE, ROW_ID) in
                       (select REP_CONS_TYPE_ID, ROW_TYPE, ROW_ID
                          from (select REP_CONS_TYPE_ID, ROW_TYPE, GROUP_ID, RANK, max(ROW_ID) ROW_ID
                                  from TAX_ACCRUAL_REP_CONS_ROWS
                                 where (REP_CONS_TYPE_ID, ROW_TYPE, GROUP_ID, RANK) in
                                       (select REP_CONS_TYPE_ID, ROW_TYPE, GROUP_ID, RANK
                                          from TAX_ACCRUAL_REP_CONS_ROWS
                                        having count(*) > 1
                                         group by REP_CONS_TYPE_ID, ROW_TYPE, GROUP_ID, RANK)
                                 group by REP_CONS_TYPE_ID, ROW_TYPE, GROUP_ID, RANK
                                 order by REP_CONS_TYPE_ID, GROUP_ID, ROW_TYPE, RANK))
                 group by A.REP_CONS_TYPE_ID, A.ROW_TYPE, A.GROUP_ID, A.RANK
                 order by REP_CONS_TYPE_ID, GROUP_ID, ROW_TYPE, RANK))
   and REP_CONS_TYPE_ID = 8;


----****************************************************
----Log the run of the script to local PP_SCHEMA_LOG
----****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2483, 0, 2015, 1, 0, 0, 43497, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043497_prov_01_20151_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
