/*
||==========================================================================================
|| Application: PowerPlan
|| Module: Property Tax
|| File Name:   maint_044166_proptax_gis_workspace_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2    05/04/2015  Graham Miller    Add returns workspace for GIS Intergration
||==========================================================================================
*/


--Add delivered import template


declare
   NEW_SEQ_ID number;
   L_MSG      varchar2(2000);
begin

   L_MSG := 'selecting next sequence value';
   DBMS_OUTPUT.PUT_LINE(L_MSG);

   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL into NEW_SEQ_ID from DUAL;

   L_MSG := 'New seq id : ' || NEW_SEQ_ID;
   DBMS_OUTPUT.PUT_LINE(L_MSG);




INSERT INTO "PP_IMPORT_TEMPLATE" ("IMPORT_TEMPLATE_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "DESCRIPTION", "LONG_DESCRIPTION", "CREATED_BY", "CREATED_DATE",
"DO_UPDATE_WITH_ADD", "UPDATES_IMPORT_LOOKUP_ID", "FILTER_CLAUSE", "IS_AUTOCREATE_TEMPLATE") VALUES (NEW_SEQ_ID, to_date('2015-05-04 14:17:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'GIS Interface Import', 'GIS Interface Import', 'PWRPLANT', to_date('2014-05-28 13:46:05', 'yyyy-mm-dd hh24:mi:ss'), null, null, null, 0) ;
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID",
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES (NEW_SEQ_ID, 1, to_date('2015-05-05 14:15:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'tax_year', 35, null, null, null) ;
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID",
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES (NEW_SEQ_ID, 2, to_date('2015-05-05 14:15:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'prop_tax_company_id', 3, null, null, null) ;
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID",
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES (NEW_SEQ_ID, 3, to_date('2015-05-05 14:15:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'allocation_id', 46, null, null, null) ;
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID",
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES (NEW_SEQ_ID, 4, to_date('2015-05-05 14:15:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'parcel_id', 70, null, null, null) ;
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID",
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES (NEW_SEQ_ID, 5, to_date('2015-05-05 14:15:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'county_id', 61, null, null, null) ;
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID",
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES (NEW_SEQ_ID, 6, to_date('2015-05-05 14:15:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'state_id', null, null, null, null) ;
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID",
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES (NEW_SEQ_ID, 7, to_date('2015-05-05 14:15:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'vintage', null, null, null, null) ;
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID",
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES (NEW_SEQ_ID, 8, to_date('2015-05-05 14:15:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2,
'statistic', null, null, null, null) ;



   L_MSG := 'Row inserted';
   DBMS_OUTPUT.PUT_LINE(L_MSG);

exception
   when others then
      L_MSG := SUBSTR('Last message : ' || L_MSG || '.  Error : ' || sqlerrm, 1, 2000);
      DBMS_OUTPUT.PUT_LINE(L_MSG);
end;
/




--Add system option

INSERT INTO PPBASE_SYSTEM_OPTIONS (SYSTEM_OPTION_ID,LONG_DESCRIPTION,SYSTEM_ONLY,PP_DEFAULT_VALUE,
                                   OPTION_VALUE,IS_BASE_OPTION,ALLOW_COMPANY_OVERRIDE)
VALUES ('Returns - GIS Interface - File Path','This option identifies the file path to save the import file created by the GIS Interface to',
        0,'C:\Temp',null,1,0);

INSERT INTO PPBASE_SYSTEM_OPTIONS_MODULE(SYSTEM_OPTION_ID,MODULE)
VALUES ('Returns - GIS Interface - File Path','proptax');


--Add to PT_PROCESS
Insert into pt_process (pt_process_id,description,long_description,short_description,
                        identifier,pt_process_type_id,is_base_process,default_step_number,
                        object_name)
Values (55,'GIS Interface','Create allocation statistic template from GIS data','GIS Interface',
        'gis_interface',1,1,99,'uo_ptc_rtncntr_wksp_gis');




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2667, 0, 2015, 2, 0, 0, 044166, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044166_proptax_gis_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;