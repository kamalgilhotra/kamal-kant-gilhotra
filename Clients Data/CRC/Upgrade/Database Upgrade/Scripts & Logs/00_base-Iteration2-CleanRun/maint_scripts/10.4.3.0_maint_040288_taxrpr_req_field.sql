/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040288_taxrpr_req_field.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/14/2014 Alex P.
||============================================================================
*/

insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION)
values
   (31, 'repair_schema', 'processing_period_id', 'uo_rpr_config_wksp_repair_schema.dw_details', 'Processing Period');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1539, 0, 10, 4, 3, 0, 40288, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040288_taxrpr_req_field.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;