/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048700_lease_2_add_currency_gain_loss_import_columns_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/04/2017 Josh Sandler     Add columns for currency gain/loss and currency gain/loss offset accounts for import tool
||============================================================================
*/

INSERT INTO pp_import_column
  (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table, is_on_table, parent_table_pk_column)
VALUES
  (252, 'currency_gain_loss_dr_acct_id','Currency Gain/Loss Account', 'curr_gain_loss_dr_acct_xlate', 0, 2, 'number(22,0)', 'gl_account', 1, 'gl_account_id')
;

INSERT INTO pp_import_column
  (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table, is_on_table, parent_table_pk_column)
VALUES
  (252, 'currency_gain_loss_cr_acct_id','Currency Gain/Loss Offset Account', 'curr_gain_loss_cr_acct_xlate', 0, 2, 'number(22,0)', 'gl_account', 1, 'gl_account_id')
;

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'currency_gain_loss_dr_acct_id', 601)
;

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'currency_gain_loss_dr_acct_id', 602)
;

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'currency_gain_loss_dr_acct_id', 603)
;

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'currency_gain_loss_cr_acct_id', 601)
;

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'currency_gain_loss_cr_acct_id', 602)
;

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'currency_gain_loss_cr_acct_id', 603)
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3626, 0, 2017, 1, 0, 0, 48700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048700_lease_2_add_currency_gain_loss_import_columns_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;