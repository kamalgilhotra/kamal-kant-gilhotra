/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038366_lease_tax_table_maint.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 06/11/2014 Ryan Oliveria       Lessee Taxes Front End Changes
||============================================================================
*/

insert into POWERPLANT_DDDW
   (DROPDOWN_NAME, TABLE_NAME, CODE_COL, DISPLAY_COL)
values
   ('ls_gl_account', 'gl_account', 'gl_account_id', 'description');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
   select 'accrual_acct_id' COLUMN_NAME,
          'ls_rent_bucket_admin' TABLE_NAME,
          'ls_gl_account' DROPDOWN_NAME,
          'p' PP_EDIT_TYPE_ID,
          'Accrual Account' DESCRIPTION,
          9 COLUMN_RANK,
          0 READ_ONLY
     from DUAL;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
   select 'expense_acct_id' COLUMN_NAME,
          'ls_rent_bucket_admin' TABLE_NAME,
          'ls_gl_account' DROPDOWN_NAME,
          'p' PP_EDIT_TYPE_ID,
          'Expense Account' DESCRIPTION,
          10 COLUMN_RANK,
          0 READ_ONLY
     from DUAL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1183, 0, 10, 4, 3, 0, 38366, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038366_lease_tax_table_maint.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
