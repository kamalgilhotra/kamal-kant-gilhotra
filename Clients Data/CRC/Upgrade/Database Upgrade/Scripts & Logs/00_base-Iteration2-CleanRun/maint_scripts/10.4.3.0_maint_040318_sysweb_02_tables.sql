/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_sysweb_02_tables.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/24/2014 Chad Theilman    Web Framework - Tables
||============================================================================
*/

declare
  doesTableExist       number := 0;
  doesTableColumnExist number := 0;
  doesSequenceExist    number := 0;
  doesIndexExist       number := 0;

begin

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_MODULES',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      -- Create the module type table (pp_web_modules).
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_MODULES (
                  module_id NUMBER(22,0) NOT NULL,
                  module_name VARCHAR2(18) NOT NULL,
                  classification VARCHAR2(18) NOT NULL,
                  licensable NUMBER(1, 0) NOT NULL,
                  description VARCHAR2(2000),
                  parent_id NUMBER(22, 0),
                  user_id VARCHAR2(18),
                  time_stamp DATE
                )';
    
      execute immediate 'ALTER TABLE PWRPLANT.PP_WEB_MODULES ADD (CONSTRAINT pk_pp_web_modules PRIMARY KEY (module_id) USING INDEX tablespace pwrplant_idx)';
      execute immediate 'CREATE OR REPLACE PUBLIC SYNONYM pp_web_modules FOR pwrplant.pp_web_modules';
      execute immediate 'GRANT ALL ON pp_web_modules TO pwrplant_role_dev';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_CONFIGURATION',
                                                  'PWRPLANT');

  if doesTableExist = 1 then
    begin
      dbms_output.put_line('PP_WEB_CONFIGURATION table exists');
    
      doesTableColumnExist := PWRPLANT.SYS_DOES_TABLE_COLUMN_EXIST('MODULE_ID',
                                                                   'PP_WEB_CONFIGURATION',
                                                                   'PWRPLANT');
    
      if doesTableColumnExist = 0 then
        begin
          dbms_output.put_line('Creating column MODULE_ID for PP_WEB_CONFIGURATION table');
        
          execute immediate 'ALTER TABLE "PWRPLANT"."PP_WEB_CONFIGURATION" 
              ADD 
              (
                "MODULE_ID" NUMBER(22,0) DEFAULT ''2'' NOT NULL ENABLE
              )';
			  
		  execute immediate 'delete from pwrplant.pp_web_configuration'; 
		  execute immediate 'ALTER TABLE pwrplant.pp_web_configuration ADD CONSTRAINT r_pp_web_configuration1
                     FOREIGN KEY (module_id)   REFERENCES pp_web_modules(module_id)';
        
        end;
      else
        begin
          dbms_output.put_line('Column MODULE_ID for PP_WEB_CONFIGURATION table exists');
        
        end;
      end if;
    
      --doesTableColumnExist := PWRPLANT.SYS_DOES_TABLE_COLUMN_EXIST('CONFIG_TYPE', 'PP_WEB_CONFIGURATION','PWRPLANT');
      --if doesTableColumnExist = 1 then
      --  begin
      --    dbms_output.put_line('Drop column CONFIG_TYPE for PP_WEB_CONFIGURATION table');
    
      --    execute immediate 'ALTER TABLE "PWRPLANT"."PP_WEB_CONFIGURATION" 
      --    DROP COLUMN CONFIG_TYPE';        
    
      --  end;
      --end if;     
    
    end;
  else
    begin
      dbms_output.put_line('PP_WEB_CONFIGURATION table does not exists, creating table.');
    
      execute immediate 'CREATE TABLE "PWRPLANT"."PP_WEB_CONFIGURATION"
        (
          "COMPANY_ID"   NUMBER(22,0) DEFAULT -1 CONSTRAINT "C_PP_WEB_CONFIGURATION01" NOT NULL,
          "CONFIG_KEY"   VARCHAR2(100 BYTE) CONSTRAINT "C_PP_WEB_CONFIGURATION02" NOT NULL,
          "CONFIG_VALUE" VARCHAR2(4000 BYTE) CONSTRAINT "C_PP_WEB_CONFIGURATION03" NOT NULL,
          "CONFIG_TITLE" VARCHAR2(100 BYTE) CONSTRAINT "C_PP_WEB_CONFIGURATION04" NOT NULL,
          "CONFIG_DESC"  VARCHAR2(254 BYTE),
          "CONFIG_TYPE"  VARCHAR2(35 BYTE) CONSTRAINT "C_PP_WEB_CONFIGURATION05" NOT NULL,
          "TIME_STAMP" DATE, 
          "MODULE_ID" NUMBER(22,0) NOT NULL ENABLE, 
          CONSTRAINT "PK_PP_WEB_CONFIGURATION" PRIMARY KEY ("COMPANY_ID", "CONFIG_KEY") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 STORAGE(INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "PWRPLANT_IDX" ENABLE
        )
        SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
        (
          INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
        )
        TABLESPACE "PWRPLANT"';
		
		execute immediate 'ALTER TABLE pwrplant.pp_web_configuration ADD CONSTRAINT r_pp_web_configuration1
                     FOREIGN KEY (module_id)   REFERENCES pp_web_modules(module_id)';
		execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_CONFIGURATION for pwrplant.PP_WEB_CONFIGURATION';
		execute immediate 'GRANT ALL on PP_WEB_CONFIGURATION to pwrplant_role_dev';
    
    end;
  
  end if;

  doesTableExist := SYS_DOES_TABLE_EXIST('PP_WEB_AUTHENTICATION',
                                         'PWRPLANT');

  if doesTableExist = 1 then
    begin
      dbms_output.put_line('PP_WEB_AUTHENTICATION table exists');
    
      doesTableColumnExist := PWRPLANT.SYS_DOES_TABLE_COLUMN_EXIST('MODULE_ID',
                                                                   'PP_WEB_AUTHENTICATION',
                                                                   'PWRPLANT');
    
      if doesTableColumnExist = 0 then
        begin
          dbms_output.put_line('Creating column MODULE_ID for PP_WEB_AUTHENTICATION table');
        
          execute immediate 'ALTER TABLE "PWRPLANT"."PP_WEB_AUTHENTICATION" 
              ADD 
              (
                "MODULE_ID" NUMBER(22,0) DEFAULT ''2'' NOT NULL ENABLE
              )';
			  
           execute immediate 'delete from pwrplant.pp_web_authentication';
           execute immediate 'ALTER TABLE pwrplant.pp_web_authentication ADD CONSTRAINT r_pp_web_authentication1 FOREIGN KEY (module_id) REFERENCES pp_web_modules(module_id)';        
        end;
      else
        begin
          dbms_output.put_line('Column MODULE_ID for PP_WEB_AUTHENTICATION table exists');
         end;
      end if;
    
    end;
  else
    begin
      dbms_output.put_line('PP_WEB_AUTHENTICATION table does not exists, creating table.');
    
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_AUTHENTICATION 
        (
          AUTHENTICATION_NAME varchar2(50) CONSTRAINT PP_WEB_AUTHENTICATION01 NOT NULL,
          AUTHENTICATION_TYPE varchar2(15) CONSTRAINT PP_WEB_AUTHENTICATION02 NOT NULL,
          IS_ACTIVE number(1,0) CONSTRAINT PP_WEB_AUTHENTICATION03 NOT NULL,
          MAX_FAILED_ATTEMPTS number(2,0) CONSTRAINT PP_WEB_AUTHENTICATION04 NOT NULL,
          EXPIRE_LOCKOUT_MINUTES number(6,0) CONSTRAINT PP_WEB_AUTHENTICATION05 NOT NULL,
          MUST_CHANGE_PASSWORD_DAYS number(3,0) CONSTRAINT PP_WEB_AUTHENTICATION06 NOT NULL,
          PASSWORD_TYPE varchar2(15) CONSTRAINT PP_WEB_AUTHENTICATION07 NOT NULL,
          PASSWORD_ALLOWED_CHARACTERS varchar2(256) CONSTRAINT PP_WEB_AUTHENTICATION08 NOT NULL,
          PASSWORD_CONTAINS_CHARACTERS varchar2(25) CONSTRAINT PP_WEB_AUTHENTICATION09 NOT NULL,
          PASSWORD_MINIMUM_LENGTH number(2,0) CONSTRAINT PP_WEB_AUTHENTICATION10 NOT NULL,
          PASSWORD_MAXIMUM_LENGTH number(2,0) CONSTRAINT PP_WEB_AUTHENTICATION11 NOT NULL,
          PASSWORD_REUSE_COUNT number(2,0) CONSTRAINT PP_WEB_AUTHENTICATION12 NOT NULL,
          INTERFACE_DOT_NET_TYPE varchar2(256) NULL,
          MODULE_ID NUMBER(22,0) NOT NULL ENABLE,
          CONSTRAINT "PP_WEB_AUTHENTICATION_PK" PRIMARY KEY (AUTHENTICATION_NAME)
        ) 
        TABLESPACE PWRPLANT';
		
	  execute immediate 'ALTER TABLE pwrplant.pp_web_authentication ADD CONSTRAINT r_pp_web_authentication1 FOREIGN KEY (module_id) REFERENCES pp_web_modules(module_id)';        

      execute immediate 'INSERT INTO PWRPLANT.PP_WEB_AUTHENTICATION ("AUTHENTICATION_NAME", "AUTHENTICATION_TYPE", "IS_ACTIVE", "MAX_FAILED_ATTEMPTS", "EXPIRE_LOCKOUT_MINUTES", "MUST_CHANGE_PASSWORD_DAYS", "PASSWORD_TYPE", "PASSWORD_ALLOWED_CHARACTERS", "PASSWORD_CONTAINS_CHARACTERS", "PASSWORD_MINIMUM_LENGTH", "PASSWORD_MAXIMUM_LENGTH", "PASSWORD_REUSE_COUNT", "INTERFACE_DOT_NET_TYPE", "MODULE_ID") VALUES (''EMAIL-PIN'', ''TABLE'', 1, 5, 60, 0, ''TEXT'', ''0123456789'', ''N:4'', 4, 4, 0, '''', 2)';
      execute immediate 'INSERT INTO PWRPLANT.PP_WEB_AUTHENTICATION ("AUTHENTICATION_NAME", "AUTHENTICATION_TYPE", "IS_ACTIVE", "MAX_FAILED_ATTEMPTS", "EXPIRE_LOCKOUT_MINUTES", "MUST_CHANGE_PASSWORD_DAYS", "PASSWORD_TYPE", "PASSWORD_ALLOWED_CHARACTERS", "PASSWORD_CONTAINS_CHARACTERS", "PASSWORD_MINIMUM_LENGTH", "PASSWORD_MAXIMUM_LENGTH", "PASSWORD_REUSE_COUNT", "INTERFACE_DOT_NET_TYPE", "MODULE_ID") VALUES (''DIRECT-DB'', ''DATABASE'', 1, 5, 60, 0, ''TEXT'', '' '', '' '', 0, 0, 0, '''', 2)';
    
	  execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_AUTHENTICATION for pwrplant.PP_WEB_AUTHENTICATION';
	  execute immediate 'GRANT ALL on PP_WEB_AUTHENTICATION to pwrplant_role_dev';
	
    end;
  end if;



  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_AUTHENTICATION_USERS',
                                                  'PWRPLANT');

  if doesTableExist = 1 then
    begin
      dbms_output.put_line('PP_WEB_AUTHENTICATION_USERS table exists');
    end;
  else
    begin
      dbms_output.put_line('PP_WEB_AUTHENTICATION table does not exists, creating table.');
    
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_AUTHENTICATION_USERS
        (
          AUTHENTICATION_NAME varchar2(50) CONSTRAINT PP_WEB_AUTHENTICATION_USERS01 NOT NULL,
          USERS varchar2(18) CONSTRAINT PP_WEB_AUTHENTICATION_USERS02 NOT NULL,
          "ACCOUNT" varchar2(100) CONSTRAINT PP_WEB_AUTHENTICATION_USERS03 NOT NULL,
          "PASSWORD" varchar2(4000) NULL,
          IS_ACTIVE number(1,0) CONSTRAINT PP_WEB_AUTHENTICATION_USERS04 NOT NULL,
          IS_LOCKED_OUT number(1,0) CONSTRAINT PP_WEB_AUTHENTICATION_USERS05 NOT NULL,
          "FAILED_LOGIN_ATTEMPTS" number(2,0) CONSTRAINT PP_WEB_AUTHENTICATION_USERS06 NOT NULL,
          LOCK_EXPIRATION_TIME_STAMP date NULL,
          RESET_TOKEN varchar2(256) NULL,
          TIME_STAMP date CONSTRAINT PP_WEB_AUTHENTICATION_USERS07 NOT NULL,
          
          CONSTRAINT "PP_WEB_AUTHENTICATION_USERS_PK" PRIMARY KEY (AUTHENTICATION_NAME, USERS)
        ) 
        TABLESPACE PWRPLANT';
		
		 execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_AUTHENTICATION_USERS for pwrplant.PP_WEB_AUTHENTICATION_USERS';
		 execute immediate 'GRANT ALL on PP_WEB_AUTHENTICATION_USERS to pwrplant_role_dev';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_AUTHENTICATION_HISTORY',
                                                  'PWRPLANT');

  if doesTableExist = 1 then
    begin
      dbms_output.put_line('PP_WEB_AUTHENTICATION_HISTORY table exists');
    end;
  else
    begin
      dbms_output.put_line('PP_WEB_AUTHENTICATION_HISTORY table does not exists, creating table.');
    
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_AUTHENTICATION_HISTORY
        (
          AUTHENTICATION_NAME varchar2(50) CONSTRAINT PP_WEB_AUTHENTICATION_HIST01 NOT NULL,
          USERS varchar2(18) CONSTRAINT PP_WEB_AUTHENTICATION_HIST02 NOT NULL,
          PASSWORD varchar2(4000),
          TIME_STAMP date CONSTRAINT PP_WEB_AUTHENTICATION_HIST03 NOT NULL,  
          
          CONSTRAINT "PP_WEB_AUTHENTICATION_HIST_PK" PRIMARY KEY (AUTHENTICATION_NAME, USERS, TIME_STAMP)
        ) 
        TABLESPACE PWRPLANT';
		
		execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_AUTHENTICATION_HISTORY for pwrplant.PP_WEB_AUTHENTICATION_HISTORY';
		execute immediate 'GRANT ALL on PP_WEB_AUTHENTICATION_HISTORY to pwrplant_role_dev';
		
		execute immediate 'ALTER TABLE pwrplant.pp_web_authentication_history ADD CONSTRAINT r_pp_web_authentication_hist1
							FOREIGN KEY (module_id) REFERENCES pp_web_modules(module_id)';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_AUTHENTICATION_DEVICE',
                                                  'PWRPLANT');

  if doesTableExist = 1 then
    begin
      dbms_output.put_line('PP_WEB_AUTHENTICATION_DEVICE table exists');
    end;
  else
    begin
      dbms_output.put_line('PP_WEB_AUTHENTICATION_DEVICE table does not exists, creating table.');
    
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_AUTHENTICATION_DEVICE
        (
          USERS varchar2(18) CONSTRAINT PP_WEB_AUTHENTICATION_DEV01 NOT NULL,
          DEVICE_NAME varchar2(25) CONSTRAINT PP_WEB_AUTHENTICATION_DEV02 NOT NULL,
          FINGERPRINT varchar2(50) CONSTRAINT PP_WEB_AUTHENTICATION_DEV03 NOT NULL,
          DETAILS varchar2(250) CONSTRAINT PP_WEB_AUTHENTICATION_DEV04 NOT NULL,
          STATUS number(1,0) CONSTRAINT PP_WEB_AUTHENTICATION_DEV05 NOT NULL,
          LOGIN number(1,0) CONSTRAINT PP_WEB_AUTHENTICATION_DEV06 NOT NULL,
          
          CONSTRAINT "PP_WEB_AUTHENTICATION_DEV_PK" PRIMARY KEY (USERS,DEVICE_NAME)
        ) 
        TABLESPACE PWRPLANT';
		
		execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_AUTHENTICATION_DEVICE for pwrplant.PP_WEB_AUTHENTICATION_DEVICE';
		execute immediate 'GRANT ALL on PP_WEB_AUTHENTICATION_DEVICE to pwrplant_role_dev';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_LOCALIZATION',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_LOCALIZATION 
                (
                LOCALE VARCHAR2(30 BYTE) CONSTRAINT PP_WEB_LOCALIZATION01 NOT NULL,
                REVISION NUMBER(22, 0) CONSTRAINT PP_WEB_LOCALIZATION02 NOT NULL
                ) 
                TABLESPACE PWRPLANT';
    
      -- The primary key for pp_web_localization.
      execute immediate 'ALTER TABLE PWRPLANT.PP_WEB_LOCALIZATION ADD CONSTRAINT "PP_WEB_LOCALIZATION_PK" PRIMARY KEY (LOCALE)';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_LOCALIZATION_STRING',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_LOCALIZATION_STRING
                (
                LOCALE VARCHAR2(30 BYTE) CONSTRAINT PP_WEB_LOCALIZATION_STRING01 NOT NULL,
                MODULE VARCHAR2(50 BYTE) CONSTRAINT PP_WEB_LOCALIZATION_STRING02 NOT NULL,
                OBJECT VARCHAR2(50 BYTE) CONSTRAINT PP_WEB_LOCALIZATION_STRING03 NOT NULL, 
                MEMBER VARCHAR2(50 BYTE) CONSTRAINT PP_WEB_LOCALIZATION_STRING04 NOT NULL,
                TYPE VARCHAR2(50 BYTE) CONSTRAINT PP_WEB_LOCALIZATION_STRING05 NOT NULL,
                ITEM VARCHAR2(50 BYTE) CONSTRAINT PP_WEB_LOCALIZATION_STRING06 NOT NULL,
                VALUE VARCHAR2(4000 BYTE) CONSTRAINT PP_WEB_LOCALIZATION_STRING07 NOT NULL,
                module_id NUMBER(22, 0) NOT NULL,
                TIME_STAMP DATE,
                USER_ID VARCHAR2(18 BYTE)
                ) 
                TABLESPACE PWRPLANT';
    
      -- The primary key for pp_web_localization_string.
      execute immediate 'ALTER TABLE PWRPLANT.PP_WEB_LOCALIZATION_STRING ADD CONSTRAINT "PP_WEB_LOCALIZATION_STRING_PK" PRIMARY KEY (LOCALE, MODULE, OBJECT, MEMBER, TYPE, ITEM)';
    
      -- The foreign key(s) for pp_web_localization_string.
      execute immediate 'ALTER TABLE PWRPLANT.PP_WEB_LOCALIZATION_STRING ADD CONSTRAINT r_pp_web_localization_string1 FOREIGN KEY (module_id) REFERENCES pp_web_modules(module_id)';
    end;
  end if;

  doesTableExist := SYS_DOES_TABLE_EXIST('PP_WEB_LOCALIZATION_REVISION',
                                         'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE "PWRPLANT"."PP_WEB_LOCALIZATION_REVISION"
                (
                "LOCALE" VARCHAR2(30 BYTE) CONSTRAINT "PP_WEB_LOCALIZATION_REVISION01" NOT NULL,
                "REVISION_DATE" DATE,
                CONSTRAINT "PP_WEB_LOCALIZATION_REV_PK" PRIMARY KEY ("LOCALE") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE(INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "PWRPLANT" 
                )
                SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
                (
                INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
                )
                TABLESPACE "PWRPLANT"';
    
      execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_LOCALIZATION_REVISION for PWRPLANT.PP_WEB_LOCALIZATION_REVISION';
    
      execute immediate 'GRANT ALL on PP_WEB_LOCALIZATION_REVISION to pwrplant_role_dev';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_LOCALIZATION_STRING2',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE "PWRPLANT"."PP_WEB_LOCALIZATION_STRING2"
                (
                "OBJECTS" VARCHAR2(100 BYTE) CONSTRAINT "PP_WEB_LOCALIZATION_STRING202" NOT NULL ,
                "EN"      VARCHAR2(1000 BYTE) CONSTRAINT "PP_WEB_LOCALIZATION_STRING203" NOT NULL ,
                "LOCALE"  VARCHAR2(30 BYTE) CONSTRAINT "PP_WEB_LOCALIZATION_STRING204" NOT NULL ,
                "VALUE"   VARCHAR2(1000 BYTE) CONSTRAINT "PP_WEB_LOCALIZATION_STRING205" NOT NULL ,
                "TIME_STAMP" DATE,
                "USER_ID" VARCHAR2(18 BYTE),
                CONSTRAINT "PP_WEB_LOCALIZATION_STR_PK" PRIMARY KEY ("OBJECTS", "EN", "LOCALE") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 STORAGE(INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "PWRPLANT" ENABLE
                )
                SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
                (
                INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
                )
                TABLESPACE "PWRPLANT"';
    
      execute immediate 'GRANT ALL on PP_WEB_LOCALIZATION_STRING2 to pwrplant_role_dev';
	  
	  execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_LOCALIZATION_STRING2 for PWRPLANT.PP_WEB_LOCALIZATION_STRING2';
    
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_LOG', 'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_LOG
                (
                  "WEB_LOG_ID" number(16, 0) CONSTRAINT PP_WEB_LOG01 NOT NULL,
                  "LOG_SESSION" varchar2(36) CONSTRAINT PP_WEB_LOG02 NOT NULL,
                  "LOG_ACCOUNT" varchar2(50) CONSTRAINT PP_WEB_LOG03 NOT NULL,
                  "LOG_USER" varchar2(18) CONSTRAINT PP_WEB_LOG04 NOT NULL,
                  "LOG_TYPE" varchar2(15) CONSTRAINT PP_WEB_LOG05 NOT NULL,
                  "LOG_MESSAGE" varchar2(4000),
                  "LOG_DATA" varchar2(4000),
                  "TIME_STAMP" DATE,
                  
                  CONSTRAINT "PP_WEB_LOG_PK" PRIMARY KEY (WEB_LOG_ID)
                )
                TABLESPACE PWRPLANT';
    
      doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PP_WEB_LOG_SEQ',
                                                            'PWRPLANT');
    
      if doesSequenceExist = 0 then
        begin
          execute immediate 'CREATE SEQUENCE PWRPLANT.PP_WEB_LOG_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
        end;
      end if;
    
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_SECURITY_PERMISSION',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_SECURITY_PERMISSION
      (
        "OBJECTS" varchar2(100) CONSTRAINT PP_WEB_SECURITY_PERMISSIONS01 NOT NULL,
        "PRINCIPAL_TYPE" varchar2(1) CONSTRAINT PP_WEB_SECURITY_PERMISSIONS02 NOT NULL,
        "PRINCIPAL" varchar2(30) CONSTRAINT PP_WEB_SECURITY_PERMISSIONS03 NOT NULL,
        "PERMISSION" varchar2(1) CONSTRAINT PP_WEB_SECURITY_PERMISSIONS04 NOT NULL,
        "TIME_STAMP" DATE,
        "USER_ID" VARCHAR2(18 BYTE),
        
        CONSTRAINT "PP_WEB_SECURITY_PERMISSIONS_PK" PRIMARY KEY ("OBJECTS", "PRINCIPAL_TYPE", "PRINCIPAL")
      )';
    
      execute immediate 'CREATE OR REPLACE PUBLIC SYNONYM PP_WEB_SECURITY_PERMISSION for pwrplant.PP_WEB_SECURITY_PERMISSION';
      execute immediate 'GRANT ALL on PP_WEB_SECURITY_PERMISSION to pwrplant_role_dev';
    
      execute immediate 'CREATE INDEX PP_WEB_SECURITY_PERMISSION_01 ON PP_WEB_SECURITY_PERMISSION ("OBJECTS")';
      execute immediate 'CREATE INDEX PP_WEB_SECURITY_PERMISSION_02 ON PP_WEB_SECURITY_PERMISSION ("PRINCIPAL_TYPE", "PRINCIPAL")';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_SECURITY_REVISION',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_SECURITY_REVISION
		(
		  "USERS" varchar2(30) CONSTRAINT PP_WEB_SECURITY_REVISION01 NOT NULL,
		  "REVISION_DATE" DATE CONSTRAINT PP_WEB_SECURITY_REVISION02 NOT NULL,
		  
		  CONSTRAINT "PP_WEB_SECURITY_REVISION_PK" PRIMARY KEY ("USERS")
		)';
    
      execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_SECURITY_REVISION for pwrplant.PP_WEB_SECURITY_REVISION';
      execute immediate 'GRANT ALL on PP_WEB_SECURITY_REVISION to pwrplant_role_dev';
    
      execute immediate 'CREATE INDEX PP_WEB_SECURITY_REVISION_01 ON PP_WEB_SECURITY_REVISION (TRIM(UPPER("USERS")))';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_VIEW_SESSION',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_VIEW_SESSION
                (
                  SESSION_ID number(16,0) NOT NULL,
                  VIEW_ID varchar2(50) NOT NULL,
                  CONTEXT varchar2(100),
                  STATE varchar2(25) NOT NULL,
                  SETTINGS varchar2(1000) NOT NULL,
                  PARENT_SESSION_ID varchar2(32),
                  TIME_STAMP DATE,
                  USER_ID VARCHAR2(18 BYTE)
                )';
				
		execute immediate 'ALTER TABLE pwrplant.pp_web_view_session ADD (CONSTRAINT pk_pp_web_view_session PRIMARY KEY (SESSION_ID) USING INDEX tablespace pwrplant_idx)';
		execute immediate 'CREATE OR REPLACE PUBLIC SYNONYM pp_web_view_session FOR pwrplant.pp_web_view_session';
		execute immediate 'GRANT ALL ON pwrplant.pp_web_view_session TO pwrplant_role_dev';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_VIEW_CHANGE',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_VIEW_CHANGE
                (
                  CHANGE_ID number(16,0) NOT NULL,
                  VIEW_ID varchar2(50),
                  CONTEXT varchar2(100),
                  PROPERTY_NAME varchar2(200),
                  VALUE varchar2(4000),
                  SESSION_ID number(16,0) NOT NULL,
                  TIME_STAMP DATE,
                  USER_ID VARCHAR2(18 BYTE)
                )';
				
		execute immediate 'ALTER TABLE pwrplant.pp_web_view_change ADD (CONSTRAINT pk_pp_web_view_change PRIMARY KEY (CHANGE_ID) USING INDEX tablespace pwrplant_idx)';
		execute immediate 'CREATE OR REPLACE PUBLIC SYNONYM pp_web_view_change FOR pwrplant.pp_web_view_change';
		execute immediate 'GRANT ALL ON pwrplant.pp_web_view_change TO pwrplant_role_dev';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_COMPONENT',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
		  execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_COMPONENT
		  (
			component_id NUMBER(22,0) NOT NULL,
			description VARCHAR2(18) NOT NULL,
			long_description VARCHAR2(250)
		  )';
	  
		execute immediate 'ALTER TABLE pwrplant.pp_web_component ADD (CONSTRAINT pk_pp_web_component PRIMARY KEY (component_id) USING INDEX tablespace pwrplant_idx)';
		execute immediate 'CREATE OR REPLACE PUBLIC SYNONYM pp_web_component FOR pwrplant.pp_web_component';
		execute immediate 'GRANT ALL ON pwrplant.pp_web_component TO pwrplant_role_dev';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_MODULE_COMPONENTS',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_MODULE_COMPONENTS
                (
                  component_id NUMBER(22,0) NOT NULL,
                  module_id NUMBER(22,0) NOT NULL
                )';
				
		execute immediate 'ALTER TABLE pwrplant.pp_web_module_components ADD (CONSTRAINT pk_pp_web_module_components PRIMARY KEY (component_id, module_id) USING INDEX tablespace pwrplant_idx)';
		execute immediate 'CREATE OR REPLACE PUBLIC SYNONYM pp_web_module_components FOR pwrplant.pp_web_module_components';
		execute immediate 'GRANT ALL ON pwrplant.pp_web_module_components TO pwrplant_role_dev';
		
		execute immediate 'ALTER TABLE pwrplant.pp_web_module_components ADD CONSTRAINT r_pp_web_module_components1
                     FOREIGN KEY (module_id)   REFERENCES pp_web_modules(module_id)';

		execute immediate 'ALTER TABLE pwrplant.pp_web_module_components ADD CONSTRAINT r_pp_web_module_components2
                     FOREIGN KEY (component_id) REFERENCES pp_web_component(component_id)';
    end;
  end if;

  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_USER_ACCESS',
                                                  'PWRPLANT');

  if doesTableExist = 0 then
    begin
       execute immediate 'CREATE TABLE PWRPLANT.PP_WEB_USER_ACCESS
                (
                  user_id VARCHAR2(18) NOT NULL,
                  module_id NUMBER(22,0) NOT NULL,
                  component_id NUMBER(22,0) NOT NULL
                )';
				
		execute immediate 'ALTER TABLE pwrplant.pp_web_user_access ADD (CONSTRAINT pk_pp_web_user_access PRIMARY KEY (user_id, module_id, component_id) USING INDEX tablespace pwrplant_idx)';
		execute immediate 'CREATE OR REPLACE PUBLIC SYNONYM pp_web_user_access FOR pwrplant.pp_web_user_access';
		execute immediate 'GRANT ALL ON pwrplant.pp_web_user_access TO pwrplant_role_dev';
		
		execute immediate 'ALTER TABLE pwrplant.pp_web_user_access ADD CONSTRAINT r_pp_web_user_access1
                     FOREIGN KEY (module_id) REFERENCES pp_web_modules(module_id)';

		execute immediate 'ALTER TABLE pwrplant.pp_web_user_access ADD CONSTRAINT r_pp_web_user_access2
                     FOREIGN KEY (component_id) REFERENCES pp_web_component(component_id)';
    end;
  end if;

  doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PP_WEB_VIEW_SESSION_seq',
                                                        'PWRPLANT');

  if doesSequenceExist = 0 then
    begin
      execute immediate 'CREATE SEQUENCE PWRPLANT.pp_web_view_session_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE';
    end;
  end if;

  doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PP_WEB_VIEW_CHANGE_SEQ',
                                                        'PWRPLANT');

  if doesSequenceExist = 0 then
    begin
      execute immediate 'CREATE SEQUENCE pp_web_view_change_seq START WITH 1 INCREMENT BY 1 NOMAXVALUE';
    end;
  end if;

  doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('PP_WEB_VIEW_SESSION_01',
                                                  'PWRPLANT');

  if doesIndexExist = 0 then
    begin
      execute immediate 'CREATE INDEX pp_web_view_session_01 ON pp_web_view_session (VIEW_ID, CONTEXT)';
    end;
  end if;

  doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('PP_WEB_VIEW_CHANGE_01',
                                                  'PWRPLANT');

  if doesIndexExist = 0 then
    begin
      execute immediate 'CREATE INDEX pwrplant.pp_web_view_change_01 ON pwrplant.PP_WEB_VIEW_CHANGE (SESSION_ID)';
    end;
  end if;

end;
/

 CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STRING" 
            before update or insert on "PWRPLANT"."PP_WEB_LOCALIZATION_STRING"
            for each row
            BEGIN
              :new.user_id := USER;
              :new.time_stamp := SYSDATE;
            END;
/

  CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_LOCALIZATION_STRING_REV"
            BEFORE DELETE OR INSERT OR UPDATE ON "PWRPLANT"."PP_WEB_LOCALIZATION_STRING"
            FOR EACH ROW
            DECLARE
            BEGIN
                 UPDATE PP_WEB_LOCALIZATION
                  SET REVISION = REVISION + 1
                WHERE TRIM(LOWER(LOCALE)) = TRIM(LOWER(NVL(:new.locale, :old.locale)));
            END;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1567, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_sysweb_02_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
