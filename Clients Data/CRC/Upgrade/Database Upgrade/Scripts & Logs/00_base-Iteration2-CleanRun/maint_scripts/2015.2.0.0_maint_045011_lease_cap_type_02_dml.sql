/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_045011_lease_cap_type_02_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 09/30/2015 Andrew Scott   new cap type window, new fasb cap type table
||============================================================================
*/

insert into LS_FASB_CAP_TYPE (FASB_CAP_TYPE_ID, DESCRIPTION) values (1, 'Type A');
insert into LS_FASB_CAP_TYPE (FASB_CAP_TYPE_ID, DESCRIPTION) values (2, 'Type B');

update LS_LEASE_CAP_TYPE set ACTIVE = 1;

insert into powerplant_columns (column_name, table_name, dropdown_name, pp_edit_type_id, description, column_rank, read_only)
values ('fasb_cap_type_id', 'ls_lease_cap_type', 'ls_fasb_cap_type', 'p', 'FASB Cap Type', 5, 0);
insert into powerplant_columns (column_name, table_name, dropdown_name, pp_edit_type_id, description, column_rank, read_only)
values ('active', 'ls_lease_cap_type', 'status_code', 'p', 'Active', 5, 0);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2892, 0, 2015, 2, 0, 0, 45011, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045011_lease_cap_type_02_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;



