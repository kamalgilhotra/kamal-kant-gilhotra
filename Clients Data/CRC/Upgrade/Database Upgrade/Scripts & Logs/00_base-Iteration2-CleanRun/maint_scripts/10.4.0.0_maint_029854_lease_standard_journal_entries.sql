/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029854_lease_standard_journal_entries.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/29/2013 Brandon Beck   Point Release
||============================================================================
*/

--STANDARD_JOURNAL_ENTRIES
insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, TIME_STAMP, USER_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select 127496,
          TO_DATE('9-12-2011 14:56:00', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          'LEASERETIRE',
          'LEASERETIRE',
          'LAM Retire',
          'Leased Asset Retirements'
     from DUAL
    where not exists
    (select GL_JE_CODE from STANDARD_JOURNAL_ENTRIES where trim(GL_JE_CODE) = 'LEASERETIRE');

insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, TIME_STAMP, USER_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select 127497,
          TO_DATE('9-12-2011 14:56:00', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          'LEASETRANSF' GL_JE_CODE,
          'LEASETRANSF',
          'LAM Transfer',
          'Leased Asset Transfers'
     from DUAL
    where not exists
    (select GL_JE_CODE from STANDARD_JOURNAL_ENTRIES where trim(GL_JE_CODE) = 'LEASETRANSF');

insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, TIME_STAMP, USER_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select 127498,
          TO_DATE('9-12-2011 14:56:00', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          'LEASEADD' GL_JE_CODE,
          'LEASEADD',
          'LAM Add',
          'Leased Asset Adds'
     from DUAL
    where not exists
    (select GL_JE_CODE from STANDARD_JOURNAL_ENTRIES where trim(GL_JE_CODE) = 'LEASEADD');


--GL_JE_CONTROL
insert into GL_JE_CONTROL
   (PROCESS_ID, TIME_STAMP, USER_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE,
    CR_COLUMN, CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
   select 'LAM RETIREMENTS                    ',
          TO_DATE('29-04-2013 13:07:35', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          127496,
          null,
          null,
          'NONE',
          'NONE',
          'NONE',
          'NONE',
          null,
          null,
          null
     from DUAL
    where not exists
    (select PROCESS_ID from GL_JE_CONTROL where UPPER(trim(PROCESS_ID)) like 'LAM RETIREMENTS');


insert into GL_JE_CONTROL
   (PROCESS_ID, TIME_STAMP, USER_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE,
    CR_COLUMN, CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
   select 'LAM TRANSFERS                      ',
          TO_DATE('29-04-2013 13:07:36', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          127497,
          null,
          null,
          'NONE',
          'NONE',
          'NONE',
          'NONE',
          null,
          null,
          null
     from DUAL
    where not exists
    (select PROCESS_ID from GL_JE_CONTROL where UPPER(trim(PROCESS_ID)) like 'LAM TRANSFERS');

insert into GL_JE_CONTROL
   (PROCESS_ID, TIME_STAMP, USER_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE,
    CR_COLUMN, CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
   select 'LAM ADDS                           ',
          TO_DATE('29-04-2013 13:07:35', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          127498,
          null,
          null,
          'NONE',
          'NONE',
          'NONE',
          'NONE',
          null,
          null,
          null
     from DUAL
        where not exists
        (select PROCESS_ID from GL_JE_CONTROL where UPPER(trim(PROCESS_ID)) like 'LAM ADDS');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (369, 0, 10, 4, 0, 0, 29854, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029854_lease_standard_journal_entries.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
