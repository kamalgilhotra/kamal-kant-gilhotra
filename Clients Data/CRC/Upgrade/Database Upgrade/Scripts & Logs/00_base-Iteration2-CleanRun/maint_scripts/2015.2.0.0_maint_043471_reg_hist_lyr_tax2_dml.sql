/*
||============================================================================
|| Application: PowerPlant
|| Object Name:maint_043471_reg_hist_lyr_tax2_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/18/2015 Andrew Scott   add missing incremental processes
||                                      backfill incremental process id to the tax fp tables
||============================================================================
*/

rollback;

insert into INCREMENTAL_PROCESS
   (INCREMENTAL_PROCESS_ID, FUNDING_WO_ID, REVISION)
   select ROWNUM + (select NVL(max(INCREMENTAL_PROCESS_ID), 0) from INCREMENTAL_PROCESS),
      FUNDING_WO_ID, REVISION
     from (select FUNDING_WO_ID, REVISION
             from (select distinct FUNDING_WO_ID, REVISION
                     from TAX_DEPRECIATION_FP
                   union
                   select distinct FUNDING_WO_ID, REVISION
                     from BASIS_AMOUNTS_FP
                   union
                   select distinct FUNDING_WO_ID, REVISION
                     from TAX_BOOK_RECONCILE_FP
                   union
                   select distinct FUNDING_WO_ID, REVISION
                     from DEFERRED_INCOME_TAX_FP
                   union
                   select distinct FUNDING_WO_ID, REVISION
                     from UTILITY_ACCT_DEPRECIATION_FP
                   union
                   select distinct FUNDING_WO_ID, REVISION
                     from TAX_FCST_BUDGET_ADDS_FP
                   union
                   select distinct FUNDING_WO_ID, REVISION
                     from TAX_JOB_PARAMS)
           minus
           select FUNDING_WO_ID, REVISION
             from INCREMENTAL_PROCESS
);

update TAX_DEPRECIATION_FP x
set x.INCREMENTAL_PROCESS_ID = (
   select min(y.INCREMENTAL_PROCESS_ID)
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update BASIS_AMOUNTS_FP x
set x.INCREMENTAL_PROCESS_ID = (
   select min(y.INCREMENTAL_PROCESS_ID)
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update TAX_BOOK_RECONCILE_FP x
set x.INCREMENTAL_PROCESS_ID = (
   select min(y.INCREMENTAL_PROCESS_ID)
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update DEFERRED_INCOME_TAX_FP x
set x.INCREMENTAL_PROCESS_ID = (
   select min(y.INCREMENTAL_PROCESS_ID)
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update UTILITY_ACCT_DEPRECIATION_FP x
set x.INCREMENTAL_PROCESS_ID = (
   select min(y.INCREMENTAL_PROCESS_ID)
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update TAX_FCST_BUDGET_ADDS_FP x
set x.INCREMENTAL_PROCESS_ID = (
   select min(y.INCREMENTAL_PROCESS_ID)
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update TAX_JOB_PARAMS x
set x.INCREMENTAL_PROCESS_ID = (
   select min(y.INCREMENTAL_PROCESS_ID)
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

commit;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2638, 0, 2015, 2, 0, 0, 043471, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043471_reg_hist_lyr_tax2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;