/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048886_lessor_02_create_fasb_cap_types_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/21/2017 Jared Watkins    Insert the new FASB Cap Type records for Lessor
||============================================================================
*/

insert into lsr_fasb_cap_type(fasb_cap_type_id, description)
select 1, 'Operating'
from dual
where not exists (select 1 from lsr_fasb_cap_type where fasb_cap_type_id = 1 and description = 'Operating')
union
select 2, 'Sales Type'
from dual
where not exists (select 1 from lsr_fasb_cap_type where fasb_cap_type_id = 2 and description = 'Sales Type')
union
select 3, 'Direct Finance'
from dual
where not exists (select 1 from lsr_fasb_cap_type where fasb_cap_type_id = 3 and description = 'Direct Finance');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3731, 0, 2017, 1, 0, 0, 48886, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048886_lessor_02_create_fasb_cap_types_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;