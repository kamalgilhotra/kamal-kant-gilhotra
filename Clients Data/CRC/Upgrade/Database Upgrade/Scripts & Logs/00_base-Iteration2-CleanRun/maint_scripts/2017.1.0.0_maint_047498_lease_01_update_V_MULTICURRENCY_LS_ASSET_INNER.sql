/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047498_lease_01_update_V_MULTICURRENCY_LS_ASSET_INNER.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 06/13/2017 Charlie Shilling add fields to view
||============================================================================
*/
CREATE OR REPLACE VIEW v_multicurrency_ls_asset_inner (
  ilr_id,
  ls_asset_id,
  revision,
  set_of_books_id,
  month,
  contract_currency_id,
  residual_amount,
  term_penalty,
  bpo_price,
  beg_capital_cost,
  end_capital_cost,
  beg_obligation,
  end_obligation,
  beg_lt_obligation,
  end_lt_obligation,
  interest_accrual,
  principal_accrual,
  interest_paid,
  principal_paid,
  executory_accrual1,
  executory_accrual2,
  executory_accrual3,
  executory_accrual4,
  executory_accrual5,
  executory_accrual6,
  executory_accrual7,
  executory_accrual8,
  executory_accrual9,
  executory_accrual10,
  executory_paid1,
  executory_paid2,
  executory_paid3,
  executory_paid4,
  executory_paid5,
  executory_paid6,
  executory_paid7,
  executory_paid8,
  executory_paid9,
  executory_paid10,
  contingent_accrual1,
  contingent_accrual2,
  contingent_accrual3,
  contingent_accrual4,
  contingent_accrual5,
  contingent_accrual6,
  contingent_accrual7,
  contingent_accrual8,
  contingent_accrual9,
  contingent_accrual10,
  contingent_paid1,
  contingent_paid2,
  contingent_paid3,
  contingent_paid4,
  contingent_paid5,
  contingent_paid6,
  contingent_paid7,
  contingent_paid8,
  contingent_paid9,
  contingent_paid10,
  current_lease_cost,
  depr_expense,
  begin_reserve,
  end_reserve,
  depr_exp_alloc_adjust,
  company_id,
  in_service_exchange_rate,
  asset_description,
  leased_asset_number,
  fmv
) AS
with depr AS (
  SELECT  /*+ materialize */ ls_asset_id, 
         revision, 
		 set_of_books_id,
         month, 
		 depr_expense,
         begin_reserve,
         end_reserve,
         depr_exp_alloc_adjust
  FROM ls_depr_forecast ldf
)
select /*+ NO_MERGE */
  la.ilr_id,
  las.ls_asset_id,
  las.revision,
  las.set_of_books_id,
  las.MONTH,
  la.contract_currency_id,
  las.residual_amount,
  las.term_penalty,
  las.bpo_price,
  las.beg_capital_cost,
  las.end_capital_cost,
  las.beg_obligation,
  las.end_obligation,
  las.beg_lt_obligation,
  las.end_lt_obligation,
  las.interest_accrual,
  las.principal_accrual,
  las.interest_paid,
  las.principal_paid,
  las.executory_accrual1,
  las.executory_accrual2,
  las.executory_accrual3,
  las.executory_accrual4,
  las.executory_accrual5,
  las.executory_accrual6,
  las.executory_accrual7,
  las.executory_accrual8,
  las.executory_accrual9,
  las.executory_accrual10,
  las.executory_paid1,
  las.executory_paid2,
  las.executory_paid3,
  las.executory_paid4,
  las.executory_paid5,
  las.executory_paid6,
  las.executory_paid7,
  las.executory_paid8,
  las.executory_paid9,
  las.executory_paid10,
  las.contingent_accrual1,
  las.contingent_accrual2,
  las.contingent_accrual3,
  las.contingent_accrual4,
  las.contingent_accrual5,
  las.contingent_accrual6,
  las.contingent_accrual7,
  las.contingent_accrual8,
  las.contingent_accrual9,
  las.contingent_accrual10,
  las.contingent_paid1,
  las.contingent_paid2,
  las.contingent_paid3,
  las.contingent_paid4,
  las.contingent_paid5,
  las.contingent_paid6,
  las.contingent_paid7,
  las.contingent_paid8,
  las.contingent_paid9,
  las.contingent_paid10,
  las.current_lease_cost,
  depr.depr_expense,
  depr.begin_reserve,
  depr.end_reserve,
  depr.depr_exp_alloc_adjust,
  la.company_id,
  opt.in_service_exchange_rate,
  la.description,
  la.leased_asset_number,
  la.fmv
FROM ls_asset_schedule las
INNER JOIN ls_asset la
  ON las.ls_asset_id = la.ls_asset_id
INNER JOIN ls_ilr_options opt
  ON la.ilr_id = opt.ilr_id
  AND las.revision = opt.revision
LEFT OUTER JOIN depr
  ON las.ls_asset_id = depr.ls_asset_id
  AND las.revision = depr.revision
  AND las.set_of_books_id = depr.set_of_books_id
  AND las.month = depr.month
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3524, 0, 2017, 1, 0, 0, 47498, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047498_lease_01_update_V_MULTICURRENCY_LS_ASSET_INNER.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;