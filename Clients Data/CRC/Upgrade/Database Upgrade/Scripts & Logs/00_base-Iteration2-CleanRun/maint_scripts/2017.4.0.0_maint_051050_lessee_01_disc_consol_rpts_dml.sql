/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051050_lessee_01_disc_consol_rpts_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/21/2018 Sarah Byers      Add configuration for Lessee Consolidated Disclosure Reports
||============================================================================
*/

-- New system controls for exchange rate type to use for activity and balances for consolidated disclosure reporting
insert into pp_system_control_company (
       control_id, control_name, control_value, description, long_description, company_id)
values (
       (select max(nvl(control_id,0)) + 1 from pp_system_control_company), 
       'Lease Consolidated IS Curr Type', 4, 'Lease Consolidated Disclosure Income Statement Currency Type', 
       'Determines the Currency Type used for the Income Statement amounts on the Consolidated Disclosure Reports.', -1);
       
insert into pp_system_control_company (
       control_id, control_name, control_value, description, long_description, company_id)
values (
       (select max(nvl(control_id,0)) + 1 from pp_system_control_company), 
       'Lease Consolidated BS Curr Type', 1, 'Lease Consolidated Disclosure Balance Sheet Currency Type', 
       'Determines the Currency Type used for the Balance Sheet amounts on the Consolidated Disclosure Reports.', -1);      

-- New Report Type
insert into pp_report_type (report_type_id, description, sort_order)
values (312, 'Lessee - Disclosure Consolidated', 5);

-- New Consolidated Report Filter
insert into pp_reports_filter (
       pp_report_filter_id, description, filter_uo_name)
values (
       104, 'Lessee - Disclosure - Consolidated', 'uo_ls_selecttabs_disclosure_consol');

-- Create New Consolidated Disclosure Reports
insert into pp_reports (
       report_id, description, long_description, subsystem, datawindow, report_number,
       pp_report_subsystem_id, report_type_id, pp_report_time_option_id,
       pp_report_filter_id, pp_report_status_id, pp_report_envir_id)
select max_report.rid + rownum, description, 'Company Consolidation - '||long_description, 'Lessee', datawindow, 
       report_number||' - Consolidated', pp_report_subsystem_id, 312, pp_report_time_option_id, 
       104, pp_report_status_id, pp_report_envir_id
  from pp_reports r, (select nvl(max(report_id), 0) rid from pp_reports) max_report
 where r.report_type_id = 311
   and report_number not in ('Lessee - 2009', 'Lessee - 2012');       

-- Update report time options
-- 208 -> 201
update pp_reports
   set pp_report_time_option_id = 201
 where report_type_id = 312
   and pp_report_time_option_id = 208;

-- 207 -> 206 
update pp_reports
   set pp_report_time_option_id = 206
 where report_type_id = 312
   and pp_report_time_option_id = 207;
   
-- Lease Cost
update pp_reports
   set datawindow = 'dw_ls_rpt_disclosure_lease_cost_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2001 - Consolidated';

-- Weighted Average Lease Term
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_wtd_avg_term_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2002 - Consolidated';
   
-- Weighted Average Discount Rate
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_wtd_discnt_rt_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2003 - Consolidated';
   
-- Maturity Analysis
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_maturity_det_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2004 - Consolidated';
     
-- Cash Paid
update pp_reports
   set datawindow = 'dw_ls_rpt_disclosure_cash_paid_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2005 - Consolidated';
   
-- New ROU Assets
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_new_rou_asset_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2006 - Consolidated';
   
-- Statement of Financial Position
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_stmnt_fin_pos_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2007 - Consolidated'; 
 
-- Short Term Lease Cost
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_st_lease_cost_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2008 - Consolidated';   
   
-- Lease Residuals
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_lease_residuals_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2010 - Consolidated';  
   
-- Non Lease Costs
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_non_lease_cost_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2011 - Consolidated';  
   
-- Leases Not Yet Commenced
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_not_commenced_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2013 - Consolidated'; 
   
-- Lease Options
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_ls_options_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2014 - Consolidated';
   
-- Lease Classification Determination
update pp_reports
   set datawindow = 'dw_ls_rpt_disc_lease_class_consol'
 where report_type_id = 312
   and report_number = 'Lessee - 2015 - Consolidated';   

-- Set up PP Tree Category and Type
insert into pp_tree_category (pp_tree_category_id, description, table_name, column_name, column_description)
select pp_tree_category_id, description, table_name, column_name, column_description from (
  select nvl(max(pp_tree_category_id),0) + 1 pp_tree_category_id, 'Company' description, 
         'company_setup' table_name, 'company_id' column_name, 'description' column_description
    from pp_tree_category) a
 where not exists (
       select 1 from pp_tree_category b
        where lower(b.table_name) = 'company_setup'
          and lower(a.table_name) = lower(b.table_name));

insert into pp_tree_type (pp_tree_category_id, pp_tree_type_id, description)
select pp_tree_category_id, pp_tree_type_id, description from (
  select (select pp_tree_category_id from pp_tree_category where lower(table_name) = 'company_setup') pp_tree_category_id, 
         nvl((select max(pp_tree_type_id) from pp_tree_type),0) + 1 pp_tree_type_id, 
         'Lessee Disclosure Consolidated'description
    from dual) a
 where not exists (
       select 1 from pp_tree_type b
        where b.description = 'Lessee Disclosure Consolidated'
          and lower(a.description) = lower(b.description));

		  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7883, 0, 2017, 4, 0, 0, 51050, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051050_lessee_01_disc_consol_rpts_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
		  