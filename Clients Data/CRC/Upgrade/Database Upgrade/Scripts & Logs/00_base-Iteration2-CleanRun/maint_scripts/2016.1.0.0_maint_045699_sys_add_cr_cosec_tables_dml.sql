/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045699_sys_add_cr_cosec_tables_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 06/27/2016 David Haupt    Populate new table with CR tables list
||============================================================================
*/

insert into cr_company_security_tables(table_name)
(select distinct(table_name)
  from all_tab_cols
  where table_name in (
    select upper(table_name) from cr_sources
      union
    select upper(view_name) from cr_sources
      union
    select upper(archive_table_name) from cr_sources)
  and lower(column_name) = (
    select control_value
    from cr_system_control
    where lower(control_name) = 'company field')
) order by table_name ;

INSERT INTO cr_company_security_tables (table_name) VALUES ('cr_summary');
INSERT INTO cr_company_security_tables (table_name) VALUES ('cr_balances');
INSERT INTO cr_company_security_tables (table_name) VALUES ('cr_sum');
INSERT INTO cr_company_security_tables (table_name) VALUES ('cr_sum_ab');
INSERT INTO cr_company_security_tables (table_name) VALUES ('cr_budget_data');
INSERT INTO cr_company_security_tables (table_name) VALUES ('cr_budget_balances');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3230, 0, 2016, 1, 0, 0, 045699, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045699_sys_add_cr_cosec_tables_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;