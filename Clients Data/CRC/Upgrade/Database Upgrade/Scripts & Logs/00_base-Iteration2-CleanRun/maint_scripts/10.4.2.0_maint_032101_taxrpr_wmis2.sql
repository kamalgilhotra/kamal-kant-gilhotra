/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032101_taxrpr_wmis2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/17/2014 Alex Pivoshenko     Point Release
||============================================================================
*/

insert into REPAIR_PRETEST_EXPLAIN
   (ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (12, '0 MNR',
    'Failed because REPAIRS-Fail Zero Qty Minors system control is set to YES');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (964, 0, 10, 4, 2, 0, 32101, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032101_taxrpr_wmis2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;