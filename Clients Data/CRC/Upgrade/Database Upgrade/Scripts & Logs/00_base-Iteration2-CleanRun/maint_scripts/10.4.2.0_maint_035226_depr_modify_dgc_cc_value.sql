/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035226_depr_modify_dgc_cc_value.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.2.0   01/03/2014 Charlie Shilling increase size of cc_value column
||============================================================================
*/

alter table DEPR_GROUP_CONTROL modify CC_VALUE varchar2(254);
alter table CLASS_CODE_VALUES modify EXT_CLASS_CODE_VALUE VARCHAR2(254);
alter table DS_DATA_ACCOUNT_CONTROL modify CC_VALUE varchar2(254);
alter table PP_QUERY_DW_CC_VALUES modify CLASS_CODE_VALUE varchar2(254);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (832, 0, 10, 4, 2, 0, 35226, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035226_depr_modify_dgc_cc_value.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;