/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030208_reimb_fix_reimbursable_reversals.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.1.0 06/10/2013 Charlie Shilling Patch Release
||============================================================================
*/

update REIMB_GL_TRANSACTION set IS_REVERSAL = 1 where LOWER(DESCRIPTION) like '%reversal%';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (400, 0, 10, 4, 1, 0, 30208, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030208_reimb_fix_reimbursable_reversals.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

commit;