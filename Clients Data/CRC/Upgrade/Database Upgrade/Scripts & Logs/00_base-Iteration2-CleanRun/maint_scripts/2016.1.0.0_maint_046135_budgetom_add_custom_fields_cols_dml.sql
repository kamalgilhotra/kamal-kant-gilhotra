 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046135_budgetom_add_custom_fields_cols_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 09/08/2016 Jared Watkins  Add missing columns from cr_budget_labor_resource
 ||                                     to the cr_budget_labor_resource_order table.
 ||============================================================================
 */

insert into cr_budget_labor_resource_order(column_name, column_order)
select 'field_1', 25 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_1')
union all
select 'field_2', 26 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_2')
union all
select 'field_3', 27 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_3')
union all
select 'field_4', 28 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_4')
union all
select 'field_5', 29 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_5')
union all
select 'field_6', 30 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_6')
union all
select 'field_7', 31 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_7')
union all
select 'field_8', 32 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_8')
union all
select 'field_9', 33 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_9')
union all
select 'field_10', 34 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_10')
union all
select 'field_11', 35 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_11')
union all
select 'field_12', 36 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_12')
union all
select 'field_13', 37 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_13')
union all
select 'field_14', 38 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_14')
union all
select 'field_15', 39 from dual
where not exists (select 1 from cr_budget_labor_resource_order where column_name = 'field_15');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3281, 0, 2016, 1, 0, 0, 046135, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046135_budgetom_add_custom_fields_cols_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;