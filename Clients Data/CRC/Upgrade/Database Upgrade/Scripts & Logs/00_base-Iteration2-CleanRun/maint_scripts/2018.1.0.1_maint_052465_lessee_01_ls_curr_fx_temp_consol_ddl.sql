/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052465_lessee_01_ls_curr_fx_temp_consol_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.1 01/07/2019 Sarah Byers    Adding fields for consolidated rates
||============================================================================
*/

alter table ls_curr_fx_temp add (
consol_is_rate number(22,8), 
consol_bs_rate number(22,8));

comment on column ls_curr_fx_temp.consol_is_rate is 'Stores the rate related Currency Type to use for Income Statement amounts determined by the Lease Consolidated Disclosure Income Statement Currency Type system control.';
comment on column ls_curr_fx_temp.consol_bs_rate is 'Stores the rate related Currency Type to use for Balance Sheet amounts determined by the Lease Consolidated Disclosure Balance Sheet Currency Type system control.';

CREATE OR REPLACE VIEW V_LS_COMPANY_FX (
  company_id, company_description, currency_id, iso_code, currency_display_symbol,
  month, currency_from, currency_to, now_rate, bal_rate, act_rate) AS
with lease_co as (
       select /*+ MATERIALIZE */ c.company_id, c.description, cs.currency_id
         from company c
         join currency_schema cs on c.company_id = cs.company_id
        where cs.currency_type_id = 1
     ),
     sc_bs as (
        select /*+ MATERIALIZE */ * 
          from pp_system_control_company
         where trim(lower(control_name)) = 'lease consolidated bs curr type'
     ),
     sc_is as (
        select /*+ MATERIALIZE */ * 
          from pp_system_control_company
         where trim(lower(control_name)) = 'lease consolidated is curr type'
     )
select c.company_id, c.description company_description, cur.currency_id, cur.iso_code, cur.currency_display_symbol, 
       fx.month, fx.currency_from, fx.currency_to, fx.act_now_rate now_rate,
       nvl(calc_rate_bs.rate, fx.consol_bs_rate) bal_rate,
       nvl(calc_rate_is.rate, fx.consol_is_rate) act_rate
  from lease_co c
  join currency cur on cur.currency_id = c.currency_id
  join ls_curr_fx_temp fx on fx.currency_to = cur.currency_id
  join (
          select sc_bs.company_id, to_number(lower(trim(sc_bs.control_value))) control_value
            from sc_bs
           where sc_bs.company_id <> -1
          union all
          select c.company_id, to_number(lower(trim(sc_bs.control_value))) control_value
            from sc_bs
           cross join (select company_id from lease_co
                       minus
                       select company_id from sc_bs b) c
           where sc_bs.company_id = -1
       ) bs_cv on bs_cv.company_id = c.company_id
  join (
          select sc_is.company_id, to_number(lower(trim(sc_is.control_value))) control_value
            from sc_is
           where sc_is.company_id <> -1
          union all
          select c.company_id, to_number(lower(trim(sc_is.control_value))) control_value
            from sc_is
           cross join (select company_id from lease_co
                       minus
                       select company_id from sc_is b) c
           where sc_is.company_id = -1
       ) is_cv on is_cv.company_id = c.company_id 
  left outer join ls_lease_calculated_date_rates calc_rate_bs on calc_rate_bs.company_id = c.company_id
                                                             and calc_rate_bs.exchange_rate_type_id = bs_cv.control_value
                                                             and calc_rate_bs.contract_currency_id = fx.currency_from
                                                             and calc_rate_bs.company_currency_id = cur.currency_id
                                                             and trunc(calc_rate_bs.accounting_month,'fmmonth') = fx.month
  left outer join ls_lease_calculated_date_rates calc_rate_is on calc_rate_is.company_id = c.company_id
                                                             and calc_rate_is.exchange_rate_type_id = is_cv.control_value
                                                             and calc_rate_is.contract_currency_id = fx.currency_from
                                                             and calc_rate_is.company_currency_id = cur.currency_id
                                                             and trunc(calc_rate_is.accounting_month,'fmmonth') = fx.month;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13542, 0, 2018, 1, 0, 1, 52465, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_052465_lessee_01_ls_curr_fx_temp_consol_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;