/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_multi6.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/03/2013 Brandon Beck   Point release
||============================================================================
*/

alter table LS_ILR_SCHEDULE rename to LS_ILR_SCHEDULE_BB;

create table LS_ILR_SCHEDULE
(
 ILR_ID               number(22,0) not null,
 REVISION             number(4,0) not null,
 SET_OF_BOOKS_ID      number(22,0) not null,
 MONTH                date not null,
 RESIDUAL_AMOUNT      number(22,2),
 TERM_PENALTY         number(22,2),
 BPO_PRICE            number(22,2),
 BEG_CAPITAL_COST     number(22,2),
 END_CAPITAL_COST     number(22,2),
 BEG_OBLIGATION       number(22,2),
 END_OBLIGATION       number(22,2),
 BEG_LT_OBLIGATION    number(22,2),
 END_LT_OBLIGATION    number(22,2),
 INTEREST_ACCRUAL     number(22,2),
 PRINCIPAL_ACCRUAL    number(22,2),
 INTEREST_PAID        number(22,2),
 PRINCIPAL_PAID       number(22,2),
 EXECUTORY_ACCRUAL1   number(22,2),
 EXECUTORY_ACCRUAL2   number(22,2),
 EXECUTORY_ACCRUAL3   number(22,2),
 EXECUTORY_ACCRUAL4   number(22,2),
 EXECUTORY_ACCRUAL5   number(22,2),
 EXECUTORY_ACCRUAL6   number(22,2),
 EXECUTORY_ACCRUAL7   number(22,2),
 EXECUTORY_ACCRUAL8   number(22,2),
 EXECUTORY_ACCRUAL9   number(22,2),
 EXECUTORY_ACCRUAL10  number(22,2),
 EXECUTORY_PAID1      number(22,2),
 EXECUTORY_PAID2      number(22,2),
 EXECUTORY_PAID3      number(22,2),
 EXECUTORY_PAID4      number(22,2),
 EXECUTORY_PAID5      number(22,2),
 EXECUTORY_PAID6      number(22,2),
 EXECUTORY_PAID7      number(22,2),
 EXECUTORY_PAID8      number(22,2),
 EXECUTORY_PAID9      number(22,2),
 EXECUTORY_PAID10     number(22,2),
 CONTINGENT_ACCRUAL1  number(22,2),
 CONTINGENT_ACCRUAL2  number(22,2),
 CONTINGENT_ACCRUAL3  number(22,2),
 CONTINGENT_ACCRUAL4  number(22,2),
 CONTINGENT_ACCRUAL5  number(22,2),
 CONTINGENT_ACCRUAL6  number(22,2),
 CONTINGENT_ACCRUAL7  number(22,2),
 CONTINGENT_ACCRUAL8  number(22,2),
 CONTINGENT_ACCRUAL9  number(22,2),
 CONTINGENT_ACCRUAL10 number(22,2),
 CONTINGENT_PAID1     number(22,2),
 CONTINGENT_PAID2     number(22,2),
 CONTINGENT_PAID3     number(22,2),
 CONTINGENT_PAID4     number(22,2),
 CONTINGENT_PAID5     number(22,2),
 CONTINGENT_PAID6     number(22,2),
 CONTINGENT_PAID7     number(22,2),
 CONTINGENT_PAID8     number(22,2),
 CONTINGENT_PAID9     number(22,2),
 CONTINGENT_PAID10    number(22,2),
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table LS_ILR_SCHEDULE
   add constraint PK_LS_ILR_SCHEDULE
       primary key (ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH)
       using index tablespace PWRPLANT_IDX;

insert into LS_ILR_SCHEDULE
   (SET_OF_BOOKS_ID, ILR_ID, REVISION, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
    BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
    END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
    EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
    EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
    EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
    EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
    EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3,
    CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7,
    CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, CONTINGENT_PAID1,
    CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
    CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10)
   select 1,
          ILR_ID,
          REVISION,
          month,
          RESIDUAL_AMOUNT,
          TERM_PENALTY,
          BPO_PRICE,
          BEG_CAPITAL_COST,
          END_CAPITAL_COST,
          BEG_OBLIGATION,
          END_OBLIGATION,
          BEG_LT_OBLIGATION,
          END_LT_OBLIGATION,
          INTEREST_ACCRUAL,
          PRINCIPAL_ACCRUAL,
          INTEREST_PAID,
          PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1,
          EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3,
          EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5,
          EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7,
          EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9,
          EXECUTORY_ACCRUAL10,
          EXECUTORY_PAID1,
          EXECUTORY_PAID2,
          EXECUTORY_PAID3,
          EXECUTORY_PAID4,
          EXECUTORY_PAID5,
          EXECUTORY_PAID6,
          EXECUTORY_PAID7,
          EXECUTORY_PAID8,
          EXECUTORY_PAID9,
          EXECUTORY_PAID10,
          CONTINGENT_ACCRUAL1,
          CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3,
          CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5,
          CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7,
          CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9,
          CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1,
          CONTINGENT_PAID2,
          CONTINGENT_PAID3,
          CONTINGENT_PAID4,
          CONTINGENT_PAID5,
          CONTINGENT_PAID6,
          CONTINGENT_PAID7,
          CONTINGENT_PAID8,
          CONTINGENT_PAID9,
          CONTINGENT_PAID10
     from LS_ILR_SCHEDULE_BB;

alter table LS_ASSET_SCHEDULE rename to LS_ASSET_SCHEDULE_BB;

create table LS_ASSET_SCHEDULE
(
 LS_ASSET_ID          number(22,0) not null,
 REVISION             number(4,0) not null,
 SET_OF_BOOKS_ID      number(22,0) not null,
 MONTH                date not null,
 RESIDUAL_AMOUNT      number(22,2),
 TERM_PENALTY         number(22,2),
 BPO_PRICE            number(22,2),
 BEG_CAPITAL_COST     number(22,2),
 END_CAPITAL_COST     number(22,2),
 BEG_OBLIGATION       number(22,2),
 END_OBLIGATION       number(22,2),
 BEG_LT_OBLIGATION    number(22,2),
 END_LT_OBLIGATION    number(22,2),
 INTEREST_ACCRUAL     number(22,2),
 PRINCIPAL_ACCRUAL    number(22,2),
 INTEREST_PAID        number(22,2),
 PRINCIPAL_PAID       number(22,2),
 EXECUTORY_ACCRUAL1   number(22,2),
 EXECUTORY_ACCRUAL2   number(22,2),
 EXECUTORY_ACCRUAL3   number(22,2),
 EXECUTORY_ACCRUAL4   number(22,2),
 EXECUTORY_ACCRUAL5   number(22,2),
 EXECUTORY_ACCRUAL6   number(22,2),
 EXECUTORY_ACCRUAL7   number(22,2),
 EXECUTORY_ACCRUAL8   number(22,2),
 EXECUTORY_ACCRUAL9   number(22,2),
 EXECUTORY_ACCRUAL10  number(22,2),
 EXECUTORY_PAID1      number(22,2),
 EXECUTORY_PAID2      number(22,2),
 EXECUTORY_PAID3      number(22,2),
 EXECUTORY_PAID4      number(22,2),
 EXECUTORY_PAID5      number(22,2),
 EXECUTORY_PAID6      number(22,2),
 EXECUTORY_PAID7      number(22,2),
 EXECUTORY_PAID8      number(22,2),
 EXECUTORY_PAID9      number(22,2),
 EXECUTORY_PAID10     number(22,2),
 CONTINGENT_ACCRUAL1  number(22,2),
 CONTINGENT_ACCRUAL2  number(22,2),
 CONTINGENT_ACCRUAL3  number(22,2),
 CONTINGENT_ACCRUAL4  number(22,2),
 CONTINGENT_ACCRUAL5  number(22,2),
 CONTINGENT_ACCRUAL6  number(22,2),
 CONTINGENT_ACCRUAL7  number(22,2),
 CONTINGENT_ACCRUAL8  number(22,2),
 CONTINGENT_ACCRUAL9  number(22,2),
 CONTINGENT_ACCRUAL10 number(22,2),
 CONTINGENT_PAID1     number(22,2),
 CONTINGENT_PAID2     number(22,2),
 CONTINGENT_PAID3     number(22,2),
 CONTINGENT_PAID4     number(22,2),
 CONTINGENT_PAID5     number(22,2),
 CONTINGENT_PAID6     number(22,2),
 CONTINGENT_PAID7     number(22,2),
 CONTINGENT_PAID8     number(22,2),
 CONTINGENT_PAID9     number(22,2),
 CONTINGENT_PAID10    number(22,2),
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table LS_ASSET_SCHEDULE
   add constraint PK_LS_ASSET_SCHEDULE
       primary key (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH)
       using index tablespace PWRPLANT_IDX;

insert into LS_ASSET_SCHEDULE
   (SET_OF_BOOKS_ID, LS_ASSET_ID, REVISION, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
    BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
    END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
    EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
    EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
    EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
    EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
    EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3,
    CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7,
    CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, CONTINGENT_PAID1,
    CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
    CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10)
   select 1,
          LS_ASSET_ID,
          REVISION,
          month,
          RESIDUAL_AMOUNT,
          TERM_PENALTY,
          BPO_PRICE,
          BEG_CAPITAL_COST,
          END_CAPITAL_COST,
          BEG_OBLIGATION,
          END_OBLIGATION,
          BEG_LT_OBLIGATION,
          END_LT_OBLIGATION,
          INTEREST_ACCRUAL,
          PRINCIPAL_ACCRUAL,
          INTEREST_PAID,
          PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1,
          EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3,
          EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5,
          EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7,
          EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9,
          EXECUTORY_ACCRUAL10,
          EXECUTORY_PAID1,
          EXECUTORY_PAID2,
          EXECUTORY_PAID3,
          EXECUTORY_PAID4,
          EXECUTORY_PAID5,
          EXECUTORY_PAID6,
          EXECUTORY_PAID7,
          EXECUTORY_PAID8,
          EXECUTORY_PAID9,
          EXECUTORY_PAID10,
          CONTINGENT_ACCRUAL1,
          CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3,
          CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5,
          CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7,
          CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9,
          CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1,
          CONTINGENT_PAID2,
          CONTINGENT_PAID3,
          CONTINGENT_PAID4,
          CONTINGENT_PAID5,
          CONTINGENT_PAID6,
          CONTINGENT_PAID7,
          CONTINGENT_PAID8,
          CONTINGENT_PAID9,
          CONTINGENT_PAID10
     from LS_ASSET_SCHEDULE;


drop table LS_ILR_STG;

create global temporary table LS_ILR_STG
(
 ILR_ID             number(22,0) not null,
 REVISION           number(4,0) not null,
 SET_OF_BOOKS_ID    number(22,0) not null,
 NPV                number(22,2),
 FMV                number(22,2),
 IRR                number(22,12),
 PREPAY_SWITCH      number(1,0),
 DISCOUNT_RATE      number(22,12),
 RESIDUAL_AMOUNT    number(22,2),
 TERM_PENALTY       number(22,2),
 BPO_PRICE          number(22,2),
 PROCESS_NPV        number(1,0),
 VALIDATION_MESSAGE varchar2(254)
) on commit preserve rows;

alter table LS_ILR_STG
   add constraint LS_ILR_STG_PK
       primary key (ILR_ID, REVISION, SET_OF_BOOKS_ID);

drop table LS_ILR_SCHEDULE_STG;

create global temporary table LS_ILR_SCHEDULE_STG
(
 ID                   number(22,0),
 ILR_ID               number(22,0) not null,
 REVISION             number(4,0),
 SET_OF_BOOKS_ID      number(22,0),
 MONTH                date not null,
 AMOUNT               number(22,2),
 RESIDUAL_AMOUNT      number(22,2),
 TERM_PENALTY         number(22,2),
 BPO_PRICE            number(22,2),
 PREPAY_SWITCH        number(1,0),
 RATE                 number(22,12),
 NPV                  number(22,12),
 PROCESS_NPV          number(1,0),
 PAYMENT_MONTH        number(1,0),
 MONTHS_TO_ACCRUE     number(2,0),
 EXECUTORY_ACCRUAL1   number(22,2),
 EXECUTORY_ACCRUAL2   number(22,2),
 EXECUTORY_ACCRUAL3   number(22,2),
 EXECUTORY_ACCRUAL4   number(22,2),
 EXECUTORY_ACCRUAL5   number(22,2),
 EXECUTORY_ACCRUAL6   number(22,2),
 EXECUTORY_ACCRUAL7   number(22,2),
 EXECUTORY_ACCRUAL8   number(22,2),
 EXECUTORY_ACCRUAL9   number(22,2),
 EXECUTORY_ACCRUAL10  number(22,2),
 EXECUTORY_PAID1      number(22,2),
 EXECUTORY_PAID2      number(22,2),
 EXECUTORY_PAID3      number(22,2),
 EXECUTORY_PAID4      number(22,2),
 EXECUTORY_PAID5      number(22,2),
 EXECUTORY_PAID6      number(22,2),
 EXECUTORY_PAID7      number(22,2),
 EXECUTORY_PAID8      number(22,2),
 EXECUTORY_PAID9      number(22,2),
 EXECUTORY_PAID10     number(22,2),
 CONTINGENT_ACCRUAL1  number(22,2),
 CONTINGENT_ACCRUAL2  number(22,2),
 CONTINGENT_ACCRUAL3  number(22,2),
 CONTINGENT_ACCRUAL4  number(22,2),
 CONTINGENT_ACCRUAL5  number(22,2),
 CONTINGENT_ACCRUAL6  number(22,2),
 CONTINGENT_ACCRUAL7  number(22,2),
 CONTINGENT_ACCRUAL8  number(22,2),
 CONTINGENT_ACCRUAL9  number(22,2),
 CONTINGENT_ACCRUAL10 number(22,2),
 CONTINGENT_PAID1     number(22,2),
 CONTINGENT_PAID2     number(22,2),
 CONTINGENT_PAID3     number(22,2),
 CONTINGENT_PAID4     number(22,2),
 CONTINGENT_PAID5     number(22,2),
 CONTINGENT_PAID6     number(22,2),
 CONTINGENT_PAID7     number(22,2),
 CONTINGENT_PAID8     number(22,2),
 CONTINGENT_PAID9     number(22,2),
 CONTINGENT_PAID10    number(22,2)
) on commit preserve rows;

alter table LS_ILR_SCHEDULE_STG
   add constraint LS_ILR_SCHEDULE_STG_PK
       primary key (ILR_ID, REVISION, MONTH, SET_OF_BOOKS_ID);

drop table LS_ILR_ASSET_STG;

create global temporary table LS_ILR_ASSET_STG
(
 ID                     number(22,0),
 ILR_ID                 number(22,0) not null,
 SET_OF_BOOKS_ID        number(22,0),
 REVISION               number(4,0),
 LS_ASSET_ID            number(22,0) not null,
 CURRENT_LEASE_COST     number(22,2),
 ALLOC_NPV              number(22,2),
 RESIDUAL_AMOUNT        number(22,2),
 TERM_PENALTY           number(22,2),
 BPO_PRICE              number(22,2),
 PREPAY_SWITCH          number(1,0),
 RESIDUAL_NPV           number(22,2),
 NPV_MINUS_RESIDUAL_NPV number(22,2)
) on commit preserve rows;

alter table LS_ILR_ASSET_STG
   add constraint LS_ILR_ASSET_STG_PK
       primary key (ILR_ID, LS_ASSET_ID, SET_OF_BOOKS_ID);

drop table LS_ILR_ASSET_SCHEDULE_STG;

create global temporary table LS_ILR_ASSET_SCHEDULE_STG
(
 ID                   number(22,0),
 ILR_ID               number(22,0) not null,
 REVISION             number(4,0),
 LS_ASSET_ID          number(22,0) not null,
 SET_OF_BOOKS_ID      number(22,0) not null,
 MONTH                date not null,
 AMOUNT               number(22,2),
 RESIDUAL_AMOUNT      number(22,2),
 TERM_PENALTY         number(22,2),
 BPO_PRICE            number(22,2),
 PREPAY_SWITCH        number(1,0),
 PAYMENT_MONTH        number(1,0),
 MONTHS_TO_ACCRUE     number(2,0),
 RATE                 number(22,12),
 NPV                  number(22,12),
 CURRENT_LEASE_COST   number(22,2),
 BEG_CAPITAL_COST     number(22,2),
 END_CAPITAL_COST     number(22,2),
 BEG_OBLIGATION       number(22,2),
 END_OBLIGATION       number(22,2),
 BEG_LT_OBLIGATION    number(22,2),
 END_LT_OBLIGATION    number(22,2),
 UNPAID_BALANCE       number(22,2),
 INTEREST_ACCRUAL     number(22,2),
 PRINCIPAL_ACCRUAL    number(22,2),
 INTEREST_PAID        number(22,2),
 PRINCIPAL_PAID       number(22,2),
 EXECUTORY_ACCRUAL1   number(22,2),
 EXECUTORY_ACCRUAL2   number(22,2),
 EXECUTORY_ACCRUAL3   number(22,2),
 EXECUTORY_ACCRUAL4   number(22,2),
 EXECUTORY_ACCRUAL5   number(22,2),
 EXECUTORY_ACCRUAL6   number(22,2),
 EXECUTORY_ACCRUAL7   number(22,2),
 EXECUTORY_ACCRUAL8   number(22,2),
 EXECUTORY_ACCRUAL9   number(22,2),
 EXECUTORY_ACCRUAL10  number(22,2),
 EXECUTORY_PAID1      number(22,2),
 EXECUTORY_PAID2      number(22,2),
 EXECUTORY_PAID3      number(22,2),
 EXECUTORY_PAID4      number(22,2),
 EXECUTORY_PAID5      number(22,2),
 EXECUTORY_PAID6      number(22,2),
 EXECUTORY_PAID7      number(22,2),
 EXECUTORY_PAID8      number(22,2),
 EXECUTORY_PAID9      number(22,2),
 EXECUTORY_PAID10     number(22,2),
 CONTINGENT_ACCRUAL1  number(22,2),
 CONTINGENT_ACCRUAL2  number(22,2),
 CONTINGENT_ACCRUAL3  number(22,2),
 CONTINGENT_ACCRUAL4  number(22,2),
 CONTINGENT_ACCRUAL5  number(22,2),
 CONTINGENT_ACCRUAL6  number(22,2),
 CONTINGENT_ACCRUAL7  number(22,2),
 CONTINGENT_ACCRUAL8  number(22,2),
 CONTINGENT_ACCRUAL9  number(22,2),
 CONTINGENT_ACCRUAL10 number(22,2),
 CONTINGENT_PAID1     number(22,2),
 CONTINGENT_PAID2     number(22,2),
 CONTINGENT_PAID3     number(22,2),
 CONTINGENT_PAID4     number(22,2),
 CONTINGENT_PAID5     number(22,2),
 CONTINGENT_PAID6     number(22,2),
 CONTINGENT_PAID7     number(22,2),
 CONTINGENT_PAID8     number(22,2),
 CONTINGENT_PAID9     number(22,2),
 CONTINGENT_PAID10    number(22,2)
) on commit preserve rows;

alter table LS_ILR_ASSET_SCHEDULE_STG
   add constraint LS_ILR_ASSET_SCHEDULE_STG_PK
       primary key (ILR_ID, LS_ASSET_ID, SET_OF_BOOKS_ID, MONTH);

drop table LS_ILR_ASSET_SCHEDULE_CALC_STG;

create table LS_ILR_ASSET_SCHEDULE_CALC_STG
(
 ID                   number(22,0),
 ILR_ID               number(22,0),
 REVISION             number(4,0),
 LS_ASSET_ID          number(22,0),
 SET_OF_BOOKS_ID      number(22,0),
 MONTH                date,
 AMOUNT               number(22,2),
 RESIDUAL_AMOUNT      number(22,2),
 TERM_PENALTY         number(22,2),
 BPO_PRICE            number(22,2),
 PREPAY_SWITCH        number(1,0),
 PAYMENT_MONTH        number(1,0),
 MONTHS_TO_ACCRUE     number(2,0),
 RATE                 number(22,12),
 NPV                  number(22,12),
 CURRENT_LEASE_COST   number(22,8),
 BEG_CAPITAL_COST     number(22,8),
 END_CAPITAL_COST     number(22,8),
 BEG_OBLIGATION       number(22,8),
 END_OBLIGATION       number(22,8),
 BEG_LT_OBLIGATION    number(22,8),
 END_LT_OBLIGATION    number(22,8),
 UNPAID_BALANCE       number(22,8),
 INTEREST_ACCRUAL     number(22,8),
 PRINCIPAL_ACCRUAL    number(22,8),
 INTEREST_PAID        number(22,8),
 PRINCIPAL_PAID       number(22,8),
 PENNY_ROUNDER        number(22,2),
 PENNY_PRIN_ROUNDER   number(22,2),
 PENNY_INT_ROUNDER    number(22,2),
 PENNY_END_ROUNDER    number(22,2),
 PRINCIPAL_ACCRUED    number(22,8),
 INTEREST_ACCRUED     number(22,8),
 MONTHS_ACCRUED       number(4,0),
 PRIN_ROUND_ACCRUED   number(22,2),
 INT_ROUND_ACCRUED    number(22,2),
 EXECUTORY_ACCRUAL1   number(22,2),
 EXECUTORY_ACCRUAL2   number(22,2),
 EXECUTORY_ACCRUAL3   number(22,2),
 EXECUTORY_ACCRUAL4   number(22,2),
 EXECUTORY_ACCRUAL5   number(22,2),
 EXECUTORY_ACCRUAL6   number(22,2),
 EXECUTORY_ACCRUAL7   number(22,2),
 EXECUTORY_ACCRUAL8   number(22,2),
 EXECUTORY_ACCRUAL9   number(22,2),
 EXECUTORY_ACCRUAL10  number(22,2),
 EXECUTORY_PAID1      number(22,2),
 EXECUTORY_PAID2      number(22,2),
 EXECUTORY_PAID3      number(22,2),
 EXECUTORY_PAID4      number(22,2),
 EXECUTORY_PAID5      number(22,2),
 EXECUTORY_PAID6      number(22,2),
 EXECUTORY_PAID7      number(22,2),
 EXECUTORY_PAID8      number(22,2),
 EXECUTORY_PAID9      number(22,2),
 EXECUTORY_PAID10     number(22,2),
 CONTINGENT_ACCRUAL1  number(22,2),
 CONTINGENT_ACCRUAL2  number(22,2),
 CONTINGENT_ACCRUAL3  number(22,2),
 CONTINGENT_ACCRUAL4  number(22,2),
 CONTINGENT_ACCRUAL5  number(22,2),
 CONTINGENT_ACCRUAL6  number(22,2),
 CONTINGENT_ACCRUAL7  number(22,2),
 CONTINGENT_ACCRUAL8  number(22,2),
 CONTINGENT_ACCRUAL9  number(22,2),
 CONTINGENT_ACCRUAL10 number(22,2),
 CONTINGENT_PAID1     number(22,2),
 CONTINGENT_PAID2     number(22,2),
 CONTINGENT_PAID3     number(22,2),
 CONTINGENT_PAID4     number(22,2),
 CONTINGENT_PAID5     number(22,2),
 CONTINGENT_PAID6     number(22,2),
 CONTINGENT_PAID7     number(22,2),
 CONTINGENT_PAID8     number(22,2),
 CONTINGENT_PAID9     number(22,2),
 CONTINGENT_PAID10    number(22,2)
);

alter table LS_ILR_STG                     add IS_OM number(1,0);
alter table LS_ILR_SCHEDULE_STG            add IS_OM number(1,0);
alter table LS_ILR_ASSET_STG               add IS_OM number(1,0);
alter table LS_ILR_ASSET_SCHEDULE_STG      add IS_OM number(1,0);
alter table LS_ILR_ASSET_SCHEDULE_CALC_STG add IS_OM number(1,0);
alter table LS_ASSET_SCHEDULE              add IS_OM number(1,0);
alter table LS_ILR_SCHEDULE                add IS_OM number(1,0);

create table LS_ILR_AMOUNTS_SET_OF_BOOKS
(
 ILR_ID               number(22,0) not null,
 REVISION             number(4,0) not null,
 SET_OF_BOOKS_ID      number(22,0) not null,
 NET_PRESENT_VALUE    number(22,2) not null,
 INTERNAL_RATE_RETURN number(22,8) not null,
 CAPITAL_COST         number(22,2) not null,
 CURRENT_LEASE_COST   number(22,2) not null,
 IS_OM                number(1,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

alter table LS_ILR_AMOUNTS_SET_OF_BOOKS
   add constraint LS_ILR_AMOUNTS_SET_OF_BOOKS_PK
       primary key (ILR_ID, REVISION, SET_OF_BOOKS_ID)
       using index tablespace PWRPLANT_IDX;

insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
   (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST, IS_OM,
    CURRENT_LEASE_COST)
   select ILR_ID,
          REVISION,
          1,
          NVL(NET_PRESENT_VALUE, 0),
          NVL(INTERNAL_RATE_RETURN, 0),
          NVL(NET_PRESENT_VALUE, 0),
          0,
          NVL(CURRENT_LEASE_COST, 0)
     from LS_ILR_OPTIONS;

alter table LS_ILR_OPTIONS drop column NET_PRESENT_VALUE;

alter table LS_ILR_OPTIONS drop column INTERNAL_RATE_RETURN;

alter table LS_ILR_OPTIONS drop column CURRENT_LEASE_COST;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (566, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_multi6.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
