/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049800_lease_iso_code_powerplant_columns_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/22/2017 A. Smith       ISO Code is not editable in standard table maintenance.
||                                      script is rerunnable without error.
||============================================================================
*/

insert into powerplant_columns (column_name, table_name, pp_edit_type_id, description, column_rank)
select 'iso_code', 'currency', 'e', 'ISO Code', 5 from dual
where not exists(select 1 from powerplant_columns where column_name = 'iso_code' and table_name = 'currency');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4000, 0, 2017, 1, 0, 0, 49800, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049800_lease_iso_code_powerplant_columns_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;