/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043957_reg_ext_stg_user_time_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/05/2015 Shane Ward     Add Timestamp and User Id to reg_ext_stage
||============================================================================
*/

ALTER TABLE reg_ext_stage ADD user_id VARCHAR2(18);
ALTER TABLE reg_ext_stage ADD time_stamp DATE;

COMMENT ON COLUMN reg_ext_stage.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN reg_ext_stage.user_id IS 'Standard system-assigned user id used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2579, 0, 2015, 2, 0, 0, 043957, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043957_reg_ext_stg_user_time_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;