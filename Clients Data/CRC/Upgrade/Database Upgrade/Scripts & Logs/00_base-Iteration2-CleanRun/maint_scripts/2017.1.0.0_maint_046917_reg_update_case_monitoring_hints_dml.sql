/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_046917_reg_update_case_monitoring_hints_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2017.1.0.0 01/20/2017 Sarah Byers		 Update hints for RMS case processing
||============================================================================
*/

-- remove existing entries
delete from pp_datawindow_hints
 where datawindow = 'UPDATE REG_CASE_RETURN_RESULT_SUMMARY (1)';

-- Add materialize hints for select statements 1 and 3
insert into pp_datawindow_hints (
	datawindow, select_number, hint, change_by, change_date)
values (
	'UPDATE REG_CASE_RETURN_RESULT_SUMMARY (1)', 1, '/*+ materialize */', user, sysdate);

insert into pp_datawindow_hints (
	datawindow, select_number, hint, change_by, change_date)
values (
	'UPDATE REG_CASE_RETURN_RESULT_SUMMARY (1)', 3, '/*+ materialize */', user, sysdate);
	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3360, 0, 2017, 1, 0, 0, 046917, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_046917_reg_update_case_monitoring_hints_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;


