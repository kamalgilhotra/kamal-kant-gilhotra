/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_031905_lease_import_fields.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/04/2013 Ryan Oliveria  Point Release
||============================================================================
*/

--Remove Active Start/End from Vendor import
delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME in ('active_start', 'active_end')
   and IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_lessor');

delete from PP_IMPORT_COLUMN
 where COLUMN_NAME in ('active_start', 'active_end')
   and IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_lessor');

alter table LS_IMPORT_LESSOR drop column ACTIVE_START;
alter table LS_IMPORT_LESSOR drop column ACTIVE_END;

alter table LS_IMPORT_LESSOR_ARCHIVE drop column ACTIVE_START;
alter table LS_IMPORT_LESSOR_ARCHIVE drop column ACTIVE_END;


--Remove MUNI BO SW from import template
delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME = 'muni_bo_sw'
   and IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_lease');

delete from PP_IMPORT_COLUMN
 where COLUMN_NAME = 'muni_bo_sw'
   and IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME = 'ls_import_lease');

--Add Serial Number (Required) to LS_IMPORT_ASSET
alter table LS_IMPORT_ASSET         add SERIAL_NUMBER varchar2(35);

alter table LS_IMPORT_ASSET_ARCHIVE add SERIAL_NUMBER varchar2(35);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, IS_ON_TABLE)
   select IMPORT_TYPE_ID, 'serial_number', 'Serial Number', null, 1, 1, 'varchar2(35)', 1
     from PP_IMPORT_TYPE
    where IMPORT_TABLE_NAME = 'ls_import_asset';

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   select IMPORT_TEMPLATE_ID,
          (select max(FIELD_ID)
             from PP_IMPORT_TEMPLATE_FIELDS
            where IMPORT_TEMPLATE_ID = PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID) + 1,
          IMPORT_TYPE_ID,
          'serial_number'
     from PP_IMPORT_TEMPLATE
    where DESCRIPTION = 'Leased Asset Add';

--Make field ID's sequential
update PP_IMPORT_TEMPLATE_FIELDS A
   set A.FIELD_ID =
        (select count(1)
           from PP_IMPORT_TEMPLATE_FIELDS B
          where B.IMPORT_TEMPLATE_ID = A.IMPORT_TEMPLATE_ID
            and B.FIELD_ID <= A.FIELD_ID)
 where IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Move serial number to be before Notes and Class Codes
update PP_IMPORT_TEMPLATE_FIELDS
   set FIELD_ID = FIELD_ID + 1
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where DESCRIPTION = 'Leased Asset Add')
   and FIELD_ID >= 19;

update PP_IMPORT_TEMPLATE_FIELDS
   set FIELD_ID = 19
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where DESCRIPTION = 'Leased Asset Add')
   and COLUMN_NAME = 'serial_number';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (580, 0, 10, 4, 1, 0, 31905, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031905_lease_import_fields.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;