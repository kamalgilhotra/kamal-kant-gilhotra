/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049928_taxrpr_expense_at_uop_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 11/10/2017 Eric Berger    DML multi-batch reporting.
||============================================================================
*/

INSERT INTO wo_tax_status (tax_status_id, description, priority, expense_yn)
SELECT
  -998 tax_status_id,
  'Tested at UOP Level (Expense)' description,
  -999 priprity,
  1 expense_yn
FROM dual
WHERE NOT EXISTS (
  SELECT 1
  FROM wo_tax_status
  WHERE tax_status_id = -998
)
;

UPDATE wo_tax_status
SET description = 'Tested at UOP Level (Capital)'
WHERE tax_status_id = -999
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13738, 0, 2018, 2, 0, 0, 49928, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049928_taxrpr_expense_at_uop_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;