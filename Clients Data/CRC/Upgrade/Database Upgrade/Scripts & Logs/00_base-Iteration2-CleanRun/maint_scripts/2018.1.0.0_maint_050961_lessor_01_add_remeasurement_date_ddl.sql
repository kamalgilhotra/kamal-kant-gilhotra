/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050961_lessor_01_add_remeasurement_date_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 09/12/2018 Anand R        Add remeasurement date to ILR Details 
||============================================================================
*/

alter table lsr_ilr_options
add remeasurement_date date null;

COMMENT ON COLUMN lsr_ilr_options.remeasurement_date IS 'The date that the ILR revision is remeasured from.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9723, 0, 2018, 1, 0, 0, 50961, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_050961_lessor_01_add_remeasurement_date_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;