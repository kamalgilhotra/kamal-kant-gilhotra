SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051353_lessor_01_drop_lessor_calc_sch_view_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.1 06/19/2018 Crystal Yura     Drop unneeded lessor views
||============================================================================
*/

begin

  begin
      execute immediate ('drop view V_LSR_ILR_DF_SCHEDULE_CALC');
      DBMS_OUTPUT.PUT_LINE('V_LSR_ILR_DF_SCHEDULE_CALC view dropped from database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('V_LSR_ILR_DF_SCHEDULE_CALC does not need to be dropped from database.');
   end;   

   begin 
      execute immediate ('drop view V_LSR_ILR_SALES_SCHEDULE_CALC');
      DBMS_OUTPUT.PUT_LINE('V_LSR_ILR_SALES_SCHEDULE_CALC view dropped from database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('V_LSR_ILR_SALES_SCHEDULE_CALC does not need to be dropped from database.');
   end;   

   begin
      execute immediate ('drop public synonym V_LSR_ILR_DF_SCHEDULE_CALC');
      DBMS_OUTPUT.PUT_LINE('V_LSR_ILR_DF_SCHEDULE_CALC synonym dropped from database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('V_LSR_ILR_DF_SCHEDULE_CALC synonym does not need to be dropped from database.');
   end;   
   
      begin
      execute immediate ('drop public synonym V_LSR_ILR_SALES_SCHEDULE_CALC');
      DBMS_OUTPUT.PUT_LINE('V_LSR_ILR_SALES_SCHEDULE_CALC synonym dropped from database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('V_LSR_ILR_SALES_SCHEDULE_CALC synonym does not need to be dropped from database.');
   end;   

   
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6866, 0, 2017, 3, 0, 1, 51353, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051353_lessor_01_drop_lessor_calc_sch_view_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;