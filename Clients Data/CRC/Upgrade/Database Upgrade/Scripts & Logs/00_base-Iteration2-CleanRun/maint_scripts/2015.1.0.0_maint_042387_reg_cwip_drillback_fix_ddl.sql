/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042387_reg_cwip_drillback_fix_ddl.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   01/22/2015 Shane Ward    		Remove work_order_class_code table from view
||========================================================================================
*/

--Update interface table name for work_order_class_code 
ALTER TABLE reg_cwip_member_element MODIFY interface_table VARCHAR2(70);
ALTER TABLE reg_budget_member_element MODIFY interface_table VARCHAR2(70);

--Remove Class Codes from Normal View
CREATE OR REPLACE VIEW reg_fcst_cap_bdg_balance_view (
  month_number,
  end_cwip,
  expenditure_type,
  active,
  work_order_number,
  business_segment,
  bus_segment_id,
  company,
  company_id,
  major_location_id,
  asset_location_id
) AS
select BAC.MONTH_NUMBER      MONTH_NUMBER,
       BAC.END_CWIP          END_CWIP,
       EX.DESCRIPTION        EXPENDITURE_TYPE,
       FP.ACTIVE             ACTIVE,
       WOC.WORK_ORDER_NUMBER,
       BS.DESCRIPTION        BUSINESS_SEGMENT,
       BS.BUS_SEGMENT_ID     BUS_SEGMENT_ID,
       CS.DESCRIPTION        COMPANY,
       WOC.COMPANY_ID        COMPANY_ID,
       major_location_id    major_location_id,
       asset_location_id    asset_location_id
  from COMPANY_SETUP               CS,
       REG_FORECAST_VERSION        FV,
       BUDGET_AFUDC_CALC           BAC,
       WORK_ORDER_CONTROL          WOC,
       BUDGET_VERSION_FUND_PROJ    FP,
       REG_BUDGET_SOURCE_BALANCE   BSB,
       REG_BUDGET_COMPONENT_VALUES BCV,
       CR_DD_REQUIRED_FILTER       MO,
       CR_DD_REQUIRED_FILTER       FV,
       CR_DD_REQUIRED_FILTER       COMP,
       CR_DD_REQUIRED_FILTER       CO,
       EXPENDITURE_TYPE            EX,
       BUSINESS_SEGMENT            BS
 where BAC.MONTH_NUMBER = MO.FILTER_STRING
   and MO.COLUMN_NAME = 'MONTH_NUMBER'
   and EX.EXPENDITURE_TYPE_ID = BAC.EXPENDITURE_TYPE_ID
   and BS.BUS_SEGMENT_ID = WOC.BUS_SEGMENT_ID
   and BAC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.COMPANY_ID = CS.COMPANY_ID
   and BAC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and FV.FORECAST_VERSION_ID = FV.FILTER_STRING
   and FV.COLUMN_NAME = 'FORECAST_VERSION_ID'
   and CS.REG_COMPANY_ID = CO.FILTER_STRING
   and CO.COLUMN_NAME = 'REG_COMPANY_ID'
   and BAC.EXPENDITURE_TYPE_ID = 1
   and (FP.BUDGET_VERSION_ID = FV.BUDGET_VERSION_ID and FP.ACTIVE = 1 and FP.WORK_ORDER_ID = BAC.WORK_ORDER_ID and
       FP.REVISION = BAC.REVISION and BAC.EXPENDITURE_TYPE_ID = BSB.EXPENDITURE_TYPE_ID and
       BSB.DETAIL_ID = BCV.ID_VALUE and BCV.REG_COMPONENT_ID = COMP.FILTER_STRING and
       COMP.COLUMN_NAME = 'REG_COMPONENT_ID' and BCV.REG_FAMILY_ID = 3)
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2207, 0, 2015, 1, 0, 0, 42387, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042387_reg_cwip_drillback_fix_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;