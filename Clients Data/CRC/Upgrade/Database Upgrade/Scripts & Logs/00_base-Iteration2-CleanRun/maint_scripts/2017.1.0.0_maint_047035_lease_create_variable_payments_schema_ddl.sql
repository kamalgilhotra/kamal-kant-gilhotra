/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047035_lease_create_variable_payments_schema_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 02/20/2017 Jared Watkins  Create the initial table structure for the lease variable payments feature
||============================================================================
*/ 

--create necessary sequences for the new schema
create sequence ls_formula_component_seq
  minvalue 1
  start with 1
  increment by 1
  cache 20;

create sequence ls_formula_component_value_seq
  minvalue 1
  start with 1
  increment by 1
  cache 20;

  

--create the number_format table
--this is a system table describing the different number formats supported in 'generic' fields
create table number_format (
  number_format_id    number(22) not null,
  description       varchar2(35) not null,
  user_id			varchar2(18),
  time_stamp		date
);

alter table number_format 
  add constraint number_format_pk
  primary key (number_format_id)
  using index tablespace pwrplant_idx;
  
create unique index number_format_desc_idx
  on number_format (upper(description))
  tablespace pwrplant_idx;
  
comment on table number_format is '(F) [06] The Number Format table lists the specific number formats that are supported within the Lease Variable Payments system.';
comment on column number_format.number_format_id is 'The unique ID for the number format type.';
comment on column number_format.description is 'The name of the number format used to specify how to parse whichever value the type is for.';
comment on column number_format.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column number_format.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

  

--create the ls_component_type table
--this is a lease reference table for the current formula component types supported
create table ls_component_type (
  ls_component_type_id  number(22) not null,
  description       varchar2(35) not null,
  user_id			varchar2(18),
  time_stamp		date
);

alter table ls_component_type
  add constraint ls_component_type_pk
  primary key (ls_component_type_id)
  using index tablespace pwrplant_idx;
  
create unique index ls_component_type_desc_idx
  on ls_component_type (upper(description))
  tablespace pwrplant_idx;
  
comment on table ls_component_type is '(F) [06] The LS Component Type table lists the types of Formula Component supported within the Lease Variable Payments functionality.';
comment on column ls_component_type.ls_component_type_id is 'The unique ID for the Lease Formula Component type.';
comment on column ls_component_type.description is 'The name of the formula component type, presented to the user to facilitate component creation.';
comment on column ls_component_type.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_component_type.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
  


--create the ls_formula_component table
--this table holds the header-level information for all of the lease variable payments formula components
create table ls_formula_component (
  formula_component_id  number(22) not null,
  description       varchar2(35) not null,
  long_description    varchar2(254),
  ls_component_type_id  number(22) not null,
  number_format_id    number(22) not null,
  user_id			varchar2(18),
  time_stamp		date
);

alter table ls_formula_component 
  add constraint ls_formula_component_pk 
  primary key (formula_component_id) 
  using index tablespace pwrplant_idx;
  
create unique index ls_formula_comp_desc_idx 
  on ls_formula_component (upper(description))
  tablespace pwrplant_idx;
  
alter table ls_formula_component 
  add constraint ls_formula_comp_type_fk
  foreign key (ls_component_type_id)
  references ls_component_type (ls_component_type_id);
  
alter table ls_formula_component 
  add constraint ls_formula_comp_num_fmt_fk
  foreign key (number_format_id)
  references number_format (number_format_id);

comment on table ls_formula_component is '(S) [06] The LS Formula Component table holds the header-level information for all of the Lease Variable Payment Formula Components created by the user';
comment on column ls_formula_component.formula_component_id is 'The system-assigned ID for user-created lease formula components.';
comment on column ls_formula_component.description is 'The name of the formula component.';
comment on column ls_formula_component.long_description is 'A full description of the formula component''s use/purpose.';
comment on column ls_formula_component.ls_component_type_id is 'A reference to which type of lease formula component this item is.';
comment on column ls_formula_component.number_format_id is 'A reference to which type of number format the values for this formula component should take.';
comment on column ls_formula_component.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_formula_component.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

  
  
--create the ls_variable_component_values table
--this table holds the values for the lease formula components that have the 'Variable Component' type
create table ls_variable_component_values (
  formula_component_id    number(22) not null,
  ilr_id            number(22) not null,
  ls_asset_id         number(22) not null,
  incurred_month_number   number(6) not null,
  payment_month_number    number(6) not null,
  amount            number(28,8),
  user_id			varchar2(18),
  time_stamp		date
);

alter table ls_variable_component_values 
  add constraint ls_var_comp_vals_pk
  primary key (formula_component_id, ilr_id, ls_asset_id, incurred_month_number)
  using index tablespace pwrplant_idx;
  
alter table ls_variable_component_values
  add constraint ls_var_comp_val_form_comp_fk
  foreign key (formula_component_id)
  references ls_formula_component (formula_component_id);
  
alter table ls_asset
  add constraint ls_asset_ilr_uk
  unique (ls_asset_id, ilr_id)
  using index tablespace pwrplant_idx;
  
alter table ls_variable_component_values
  add constraint ls_var_comp_val_ilr_fk
  foreign key (ilr_id, ls_asset_id)
  references ls_asset (ilr_id, ls_asset_id);
  
comment on table ls_variable_component_values is '(S) [06] The LS Variable Component Values table stores the by-month values of a Formula Component for a given ILR/Lease Asset combination.';
comment on column ls_variable_component_values.formula_component_id is 'A reference to the formula component for which the values will be stored.';
comment on column ls_variable_component_values.ilr_id is 'A reference to the ILR to which the Lease Asset is related.';
comment on column ls_variable_component_values.ls_asset_id is 'A reference to the Lease Asset for which the value(s) are entered.';
comment on column ls_variable_component_values.incurred_month_number is 'The month number (yyyymm format) in which the ''amount'' was incurred for the given Lease Asset.';
comment on column ls_variable_component_values.payment_month_number is 'The month number (yyyymm format) in which the ''amount'' will be applied in the month-end lease calculations.';
comment on column ls_variable_component_values.amount is 'The specific value applied to the given lease asset for the month and formula component specified.';
comment on column ls_variable_component_values.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_variable_component_values.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
  

  
--create the ls_index_component_values table
--this table holds the effective-dated values for lease formula components of the 'Index/Rate Component' type
create table ls_index_component_values (
  formula_component_id    number(22) not null,
  effective_date        date not null,
  amount            number (28,8),
  user_id			varchar2(18),
  time_stamp		date
);

alter table ls_index_component_values 
  add constraint ls_idx_comp_vals_pk
  primary key (formula_component_id, effective_date)
  using index tablespace pwrplant_idx;
  
alter table ls_index_component_values
  add constraint ls_idx_comp_vals_form_comp_fk
  foreign key (formula_component_id)
  references ls_formula_component (formula_component_id);
  
comment on table ls_index_component_values is '(S) [06] The LS Index Component Values table contains the effective-dated values entered for user-defined Index/Rate Formula Components.';
comment on column ls_index_component_values.formula_component_id is 'A reference to the formula component for which this index/rate is being entered';
comment on column ls_index_component_values.effective_date is 'The date from which the entered index/rate should be applied; is used for any dates from this value until another row is entered with a date that supercedes this.';
comment on column ls_index_component_values.amount is 'The numerical index/rate to be applied from the given date until another date supercedes it.';
comment on column ls_index_component_values.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_index_component_values.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

  
  
--create the ls_variable_payment table
--this table contains the header-level information for variable payments within the lease module
create table ls_variable_payment (
  variable_payment_id     number(22) not null,
  description         varchar2(35) not null,
  long_description      varchar2(254) not null,
  active_for_new_ilrs     number(22) not null,
  payment_formula       varchar2(4000),
  payment_formula_db       varchar2(4000),
  payment_preview_option    number(4) not null,
  accrual_preview_option    number(4) not null,
  preview_ilr         number(22),
  preview_revision      number(22),
  preview_set_of_books    number(22),
  user_id				varchar2(18),
  time_stamp			date
);

alter table ls_variable_payment
  add constraint ls_var_payment_pk
  primary key (variable_payment_id)
  using index tablespace pwrplant_idx;
  
create unique index ls_var_payment_desc_idx 
  on ls_variable_payment (upper(description))
  tablespace pwrplant_idx;
  
alter table ls_variable_payment
  add constraint ls_var_payment_yes_no_fk
  foreign key (active_for_new_ilrs)
  references yes_no (yes_no_id);
  
alter table ls_variable_payment
  add constraint ls_var_payment_ilr_revision_fk
  foreign key (preview_ilr, preview_revision)
  references ls_ilr_approval (ilr_id, revision);
  
alter table ls_variable_payment
  add constraint ls_var_payment_sob_fk
  foreign key (preview_set_of_books)
  references set_of_books (set_of_books_id);
  
comment on table ls_variable_payment is '(S) [06] The LS Variable Payment table holds the header-information relating to a specific Variable Payment created by the user, as well as the current set of run options most recently selected and saved.';
comment on column ls_variable_payment.variable_payment_id is 'System assigned ID uniquely identifying this specific variable payment.';
comment on column ls_variable_payment.description is 'The name of the user-defined variable payment';
comment on column ls_variable_payment.long_description is 'A full description of the variable payment''s use/purpose.';
comment on column ls_variable_payment.active_for_new_ilrs is 'Field denoting whether this variable payment should appear as an option when assign variable payments to new ILRs.';
comment on column ls_variable_payment.payment_formula is 'The formula used to calculate the monthly payment values for ILRs using this variable payment';
comment on column ls_variable_payment.payment_formula_db is 'The variable payment formula where column descriptions are replaced with column names. This will be used in calculation of bucket values';
comment on column ls_variable_payment.payment_preview_option is 'The number of months for which to calculate payment values during the formula preview; -1 to calculate until end of lease life.';
comment on column ls_variable_payment.accrual_preview_option is 'The number of months for which to calculate accrual values during the formula preview; -1 to calculate until end of lease life.';
comment on column ls_variable_payment.preview_ilr is 'The ID of the ILR to use in the formula preview calculation.';
comment on column ls_variable_payment.preview_revision is 'The ID of the ILR Revision to use in the formula preview calculation.';
comment on column ls_variable_payment.preview_set_of_books is 'The ID of the Set of Books to use in the formula preview calculation.';
comment on column ls_variable_payment.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_variable_payment.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

  
  
--create the ls_variable_payment_components table
--this table stores the relationships between lease variable payments and their formula components
create table ls_variable_payment_components (
  variable_payment_id     number(22) not null,
  formula_component_id    number(22) not null,
  user_id				  varchar2(18),
  time_stamp			  date
);

alter table ls_variable_payment_components 
  add constraint ls_var_payment_comp_pk
  primary key (variable_payment_id, formula_component_id)
  using index tablespace pwrplant_idx;
  
alter table ls_variable_payment_components
  add constraint ls_var_payment_comp_varpymt_fk
  foreign key (variable_payment_id)
  references ls_variable_payment (variable_payment_id);
  
alter table ls_variable_payment_components
  add constraint ls_var_payment_comp_frmcomp_fk
  foreign key (formula_component_id)
  references ls_formula_component (formula_component_id);
  
comment on table ls_variable_payment_components is '(S) [06] The LS Variable Payment Components table is a relational table showing which Formula Components have been related to a given Variable Payment, and vice versa.';
comment on column ls_variable_payment_components.variable_payment_id is 'A reference to the variable payment to which the formula component is related.';
comment on column ls_variable_payment_components.formula_component_id is 'A reference to the formula component being related to this variable payment';
comment on column ls_variable_payment_components.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_variable_payment_components.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3373, 0, 2017, 1, 0, 0, 47035, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047035_lease_create_variable_payments_schema_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;