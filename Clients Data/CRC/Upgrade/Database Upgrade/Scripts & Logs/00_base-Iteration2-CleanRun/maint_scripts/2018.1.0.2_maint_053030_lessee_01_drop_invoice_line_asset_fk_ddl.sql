/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053030_lessee_01_drop_invoice_line_asset_fk_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.2 02/04/2019 Sarah Byers    drop fk on ls_asset_id from ls_invoice_line
||============================================================================
*/

alter table ls_invoice_line drop constraint FK_LS_INVOICE_LINE3;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(14562, 0, 2018, 1, 0, 2, 53030, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053030_lessee_01_drop_invoice_line_asset_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;