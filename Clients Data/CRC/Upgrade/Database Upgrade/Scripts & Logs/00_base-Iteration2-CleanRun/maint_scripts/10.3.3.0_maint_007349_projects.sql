/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007349_projects.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/01/2011 Chris Mardis   Point Release
||============================================================================
*/

insert into PP_ANY_QUERY_CRITERIA
   (ID, SOURCE_ID, CRITERIA_FIELD, TABLE_NAME, DESCRIPTION, FEEDER_FIELD, SUBSYSTEM, QUERY_TYPE,
    sql)
   select max(ID) + 1 ID,
          1001 SOURCE_ID,
          'none' CRITERIA_FIELD,
          'Dynamic View' TABLE_NAME,
          'Pending Substitutions' DESCRIPTION,
          'none' FEEDER_FIELD,
          'funding project' SUBSYSTEM,
          'user' QUERY_TYPE,
          'select to_woc.work_order_number to_funding_project, to_woc.description to_fp_description, from_woc.work_order_number from_funding_project, from_woc.description from_fp_description, bs.to_dollars amount, psu.last_name||'', ''||psu.first_name initiated_by, psu2.last_name||'', ''||psu2.first_name pending_approval_by, bs.budget_sub_id budget_sub_id from budget_substitutions bs, work_order_control from_woc, work_order_control to_woc, pp_security_users psu, pp_security_users psu2, workflow w, workflow_detail wd, company c where bs.from_work_order_id = from_woc.work_order_id and bs.to_work_order_id = to_woc.work_order_id and lower(bs.initiated_user) = lower(psu.users) and bs.budget_sub_id = to_number(w.id_field1) and w.workflow_id = wd.workflow_id and lower(wd.users) = lower(psu2.users) and from_woc.company_id = c.company_id and to_woc.company_id = c.company_id and bs.status = 3 and upper(w.subsystem) = ''SUBS_REVIEW'' and wd.approval_status_id = 2 order by bs.budget_sub_id, to_woc.work_order_number' sql
     from PP_ANY_QUERY_CRITERIA;

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER,
    COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD)
   select ID, 'to_funding_project', 1, 0, 1, 'To Funding Project', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Pending Substitutions'
   union
   select ID, 'to_fp_description', 2, 0, 1, 'To FP Description', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Pending Substitutions'
   union
   select ID, 'from_funding_project', 3, 0, 1, 'From Funding Project', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Pending Substitutions'
   union
   select ID, 'from_fp_description', 4, 0, 1, 'From FP Description', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Pending Substitutions'
   union
   select ID, 'amount', 5, 1, 1, 'Amount', 300, 'NUMBER', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Pending Substitutions'
   union
   select ID, 'initiated_by', 6, 0, 1, 'Initiated By', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Pending Substitutions'
   union
   select ID, 'pending_approval_by', 7, 0, 1, 'Pending Approval By', 300, 'VARCHAR2', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Pending Substitutions'
   union
   select ID, 'budget_sub_id', 8, 0, 1, 'Budget Sub ID', 300, 'NUMBER', 0
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Pending Substitutions';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (37, 0, 10, 3, 3, 0, 7349, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007349_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
