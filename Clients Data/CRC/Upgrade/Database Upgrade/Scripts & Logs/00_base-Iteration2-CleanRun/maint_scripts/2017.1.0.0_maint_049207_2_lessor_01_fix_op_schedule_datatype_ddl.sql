/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049207_2_lessor_01_fix_op_schedule_datatype_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/07/2017 Andrew Hill    Correct precision of begin_deferred_rev
||============================================================================
*/

DROP TYPE lsr_ilr_op_sch_result_tab;


CREATE OR REPLACE TYPE lsr_ilr_op_sch_result IS OBJECT( month DATE,
                                                        interest_income_received 		NUMBER(22,2),
                                                        interest_income_accrued 		NUMBER(22,2),
                                                        interest_rental_recvd_spread 	NUMBER(22,2),
                                                        begin_deferred_rev 				NUMBER(22,2),
                                                        deferred_rev 					NUMBER(22,2),
                                                        end_deferred_rev 				NUMBER(22,2),
                                                        begin_receivable 				NUMBER(22,2),
                                                        end_receivable 					NUMBER(22,2),
                                                        executory_accrual1           NUMBER(22,2),
                                                        executory_accrual2           NUMBER(22,2),
                                                        executory_accrual3           NUMBER(22,2),
                                                        executory_accrual4           NUMBER(22,2),
                                                        executory_accrual5           NUMBER(22,2),
                                                        executory_accrual6           NUMBER(22,2),
                                                        executory_accrual7           NUMBER(22,2),
                                                        executory_accrual8           NUMBER(22,2),
                                                        executory_accrual9           NUMBER(22,2),
                                                        executory_accrual10          NUMBER(22,2),
                                                        executory_paid1              NUMBER(22,2),
                                                        executory_paid2              NUMBER(22,2),
                                                        executory_paid3              NUMBER(22,2),
                                                        executory_paid4              NUMBER(22,2),
                                                        executory_paid5              NUMBER(22,2),
                                                        executory_paid6              NUMBER(22,2),
                                                        executory_paid7              NUMBER(22,2),
                                                        executory_paid8              NUMBER(22,2),
                                                        executory_paid9              NUMBER(22,2),
                                                        executory_paid10             NUMBER(22,2),
                                                        contingent_accrual1          NUMBER(22,2),
                                                        contingent_accrual2          NUMBER(22,2),
                                                        contingent_accrual3          NUMBER(22,2),
                                                        contingent_accrual4          NUMBER(22,2),
                                                        contingent_accrual5          NUMBER(22,2),
                                                        contingent_accrual6          NUMBER(22,2),
                                                        contingent_accrual7          NUMBER(22,2),
                                                        contingent_accrual8          NUMBER(22,2),
                                                        contingent_accrual9          NUMBER(22,2),
                                                        contingent_accrual10         NUMBER(22,2),
                                                        contingent_paid1             NUMBER(22,2),
                                                        contingent_paid2             NUMBER(22,2),
                                                        contingent_paid3             NUMBER(22,2),
                                                        contingent_paid4             NUMBER(22,2),
                                                        contingent_paid5             NUMBER(22,2),
                                                        contingent_paid6             NUMBER(22,2),
                                                        contingent_paid7             NUMBER(22,2),
                                                        contingent_paid8             NUMBER(22,2),
                                                        contingent_paid9             NUMBER(22,2),
                                                        contingent_paid10            NUMBER(22,2));
/
  
CREATE TYPE lsr_ilr_op_sch_result_tab AS TABLE OF lsr_ilr_op_sch_result;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3891, 0, 2017, 1, 0, 0, 49207, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049207_2_lessor_01_fix_op_schedule_datatype_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
