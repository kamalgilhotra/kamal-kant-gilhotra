/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046116_prov_tbbs_line_item_constraints_02_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version	  Date	     Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 09/22/2016 Jared Watkins  Add missing unique index and make line
||                            item and treatment type descriptions non-nullable
||============================================================================
*/
alter table tbbs_line_item
  modify (description not null);

ALTER TABLE tbbs_treatment_type
  ADD CONSTRAINT tbbs_treatment_type_desc_unq UNIQUE (
    description
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

alter table tbbs_treatment_type
  modify (description not null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3293, 0, 2016, 1, 0, 0, 046116, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046116_prov_tbbs_line_item_constraints_02_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;