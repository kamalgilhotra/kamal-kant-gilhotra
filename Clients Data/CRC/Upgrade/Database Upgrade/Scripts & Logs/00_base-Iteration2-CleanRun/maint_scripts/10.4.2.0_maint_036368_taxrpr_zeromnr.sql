/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036368_taxrpr_zeromnr.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/21/2014 Alex P.
||============================================================================
*/

update PP_SYSTEM_CONTROL_COMPANY
   set LONG_DESCRIPTION = 'Default is "yes". Minor items with zero quantity that have no corresponding major tax unit of property will be automatically failed to avoid passing late charges. It works the same way as major items with zero quantity.'
 where CONTROL_NAME = 'REPAIRS-Fail Zero Qty Minors';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (982, 0, 10, 4, 2, 0, 36368, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036368_taxrpr_zeromnr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;