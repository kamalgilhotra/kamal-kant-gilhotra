/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036074_reg_rounding.sql
|| Description: Rounding Calculated Numbers PL SQL Tables
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/05/2014 Shane Ward
||============================================================================
*/

create table REG_INPUT_SCALE
(
 SCALE_ID        NUMBER(22,0) NOT NULL,
 DESCRIPTION     VARCHAR2(35) NOT NULL,
 SCALE_LIMIT     NUMBER(22,0) NOT NULL,
 PERCENTAGE_FLAG NUMBER(1,0) NOT NULL
);

comment on table REG_INPUT_SCALE is 'This is a generic staging table to be used for all regulatory ledger transaction data that cannot be loaded via the delivered PowerPlan integration';
comment on column REG_INPUT_SCALE.SCALE_ID is 'System assigned identifier for the input scale record.';
comment on column REG_INPUT_SCALE.DESCRIPTION is 'The value that identifies what data field or column the input scale is defined for.';
comment on column REG_INPUT_SCALE.SCALE_LIMIT is 'The maximum number of decimal places allowed.';
comment on column REG_INPUT_SCALE.PERCENTAGE_FLAG is 'Indicates if the number is a percentage and multiplied by 100 for presentation. The input scale is applied after the conversion to percentage so cale of 2 allows 4 decimals to be physically stored in database, i.e. 12.34% is 0.1234. The default is false. 1 = True (a percentage) 0 = False (not a percentage)';

alter table REG_INPUT_SCALE
   add constraint PK_REG_INPUT_SCALE
       primary key (SCALE_ID)
       using index tablespace PWRPLANT_IDX;

create table REG_JUR_INPUT_SCALE
(
 SCALE_ID            number(22,0) not null,
 REG_JUR_TEMPLATE_ID number(22,0) not null,
 INPUT_SCALE         number(22,0) not null
);

comment on table REG_JUR_INPUT_SCALE is 'This is generic staging table to be used for all regulatory ledger transaction data that cannot be loaded via the delivered PowerPlan integration.';
comment on column REG_JUR_INPUT_SCALE.SCALE_ID is 'System assigned identifier for ithe input scale record.';
comment on column REG_JUR_INPUT_SCALE.REG_JUR_TEMPLATE_ID is 'System assigned identifier for the regulatory jurisdicition template.';
comment on column REG_JUR_INPUT_SCALE.INPUT_SCALE is 'The number of decimal places the data is rounded to.';

alter table REG_JUR_INPUT_SCALE
   add constraint PK_REG_JUR_INPUT_SCALE
       primary key (SCALE_ID, REG_JUR_TEMPLATE_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_JUR_INPUT_SCALE
   add constraint FK_REG_JUR_INPUT_SCALE_1
       foreign key (SCALE_ID)
       references REG_INPUT_SCALE (SCALE_ID);

alter table REG_JUR_INPUT_SCALE
   add constraint FK_REG_JUR_INPUT_SCALE_2
       foreign key (REG_JUR_TEMPLATE_ID)
       references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);

create table REG_CO_INPUT_SCALE
(
 SCALE_ID       number(22,0) not null,
 REG_COMPANY_ID number(22,0) not null,
 INPUT_SCALE    number(22,0) not null
);

comment on table REG_CO_INPUT_SCALE is 'This is generic staging table to be used for all regulatory ledger transaction data that cannot be loaded via the delivered PowerPlan integration.';
comment on column REG_CO_INPUT_SCALE.SCALE_ID is 'System assigned identifier for ithe input scale record.';
comment on column REG_CO_INPUT_SCALE.REG_COMPANY_ID is 'System assigned identifier for the regulatory company.';
comment on column REG_CO_INPUT_SCALE.INPUT_SCALE is 'The number of decimal places the data is rounded to.';

alter table reg_co_input_scale
   add constraint PK_REG_CO_INPUT_SCALE
       primary key (SCALE_ID, REG_COMPANY_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_CO_INPUT_SCALE
   add constraint FK_REG_CO_INPUT_SCALE1
       foreign key (SCALE_ID)
       references REG_INPUT_SCALE (SCALE_ID);

alter table REG_CO_INPUT_SCALE
   add constraint FK_RREG_CO_INPUT_SCALE2
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

--Fixed Data in Reg_input_scale
insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (1, 'Weighted Cost Rate', 6, 1);

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (2, 'Weighted Average Cost of Capital', 6, 1);

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (3, 'Interest Synchronization Rate', 6, 1);

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (4, 'Allocation Factors', 16, 0);

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (5, 'Overall Return Percentage', 6, 1);

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (6, 'Equity Return Percentage', 6, 1);

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (7, 'Capital Structure Weighting', 6, 1);

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (8, 'Cost Rate', 6, 1);

insert into REG_INPUT_SCALE
   (SCALE_ID, DESCRIPTION, SCALE_LIMIT, PERCENTAGE_FLAG)
values
   (9, 'ROE Impact', 6, 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (948, 0, 10, 4, 2, 0, 36074, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036074_reg_rounding.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;