SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_030565_depr_recreate_arc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.2   11/18/2013 Charlie Shilling added and reordered columns
||============================================================================
*/

declare
   TABLE_DNE exception;
   pragma exception_init(TABLE_DNE, -942);

   V_COUNT number(22, 0);
   V_SQL   varchar2(2000) := 'select count(1) from DEPR_CALC_STG_ARC';

   C_CHECK_ROWS sys_refcursor;
begin
   open C_CHECK_ROWS for V_SQL;
   fetch C_CHECK_ROWS
      into V_COUNT;
   close C_CHECK_ROWS;

   if V_COUNT > 0 then
      RAISE_APPLICATION_ERROR(-20000,
                              'Trying to drop and recreate DEPR_CALC_STG_ARC. There are records currently in this table. Please make sure that the records are archived or are no longer needed, clear the table, and re-run the script.');
   end if;

   execute immediate 'DROP TABLE depr_calc_stg_arc';
exception
   when TABLE_DNE then
      DBMS_OUTPUT.PUT_LINE('Table DEPR_CALC_STG_ARC does not exist. Creating table now...');
end;
/

create global temporary table DEPR_CALC_STG_ARC
(
 depr_group_id                number(22,0)   null,
 set_of_books_id              number(22,0)   null,
 gl_post_mo_yr                date           null,
 calc_month                   date           null,
 depr_calc_status             number(2,0)    null,
 depr_calc_message            varchar2(2000) null,
 trf_in_est_adds              varchar2(35)   null,
 include_rwip_in_net          varchar2(35)   null,
 dnsa_cor_bal                 number(22,2)   default 0 null,
 dnsa_salv_bal                number(22,2)   default 0 null,
 cor_ytd                      number(22,2)   default 0 null,
 salv_ytd                     number(22,2)   default 0 null,
 fiscalyearoffset             number(2,0)    null,
 fiscalyearstart              date           null,
 company_id                   number(22,0)   null,
 subledger_type_id            number(22,0)   null,
 description                  varchar2(254)  null,
 depr_method_id               number(22,0)   null,
 mid_period_conv              number(22,2)   null,
 mid_period_method            varchar2(35)   null,
 dg_est_ann_net_adds          number(22,8)   null,
 true_up_cpr_depr             number(22,0)   null,
 rate                         number(22,8)   null,
 net_gross                    number(22,0)   null,
 over_depr_check              number(22,0)   null,
 net_salvage_pct              number(22,8)   null,
 rate_used_code               number(22,0)   null,
 end_of_life                  number(22,0)   null,
 expected_average_life        number(22,0)   null,
 reserve_ratio_id             number(22,0)   null,
 dmr_cost_of_removal_rate     number(22,8)   null,
 cost_of_removal_pct          number(22,8)   null,
 dmr_salvage_rate             number(22,8)   null,
 interest_rate                number(22,8)   null,
 amortizable_life             number(22,0)   null,
 cor_treatment                number(1,0)    null,
 salvage_treatment            number(1,0)    null,
 net_salvage_amort_life       number(22,0)   null,
 allocation_procedure         varchar2(5)    null,
 depr_ledger_status           number(22,0)   null,
 begin_reserve                number(22,2)   default 0 null,
 end_reserve                  number(22,2)   default 0 null,
 reserve_bal_provision        number(22,2)   default 0 null,
 reserve_bal_cor              number(22,2)   default 0 null,
 salvage_balance              number(22,2)   default 0 null,
 reserve_bal_adjust           number(22,2)   default 0 null,
 reserve_bal_retirements      number(22,2)   default 0 null,
 reserve_bal_tran_in          number(22,2)   default 0 null,
 reserve_bal_tran_out         number(22,2)   default 0 null,
 reserve_bal_other_credits    number(22,2)   default 0 null,
 reserve_bal_gain_loss        number(22,2)   default 0 null,
 reserve_alloc_factor         number(22,2)   default 0 null,
 begin_balance                number(22,2)   default 0 null,
 additions                    number(22,2)   default 0 null,
 retirements                  number(22,2)   default 0 null,
 transfers_in                 number(22,2)   default 0 null,
 transfers_out                number(22,2)   default 0 null,
 adjustments                  number(22,2)   default 0 null,
 depreciation_base            number(22,2)   default 0 null,
 end_balance                  number(22,2)   default 0 null,
 depreciation_rate            number(22,8)   null,
 depreciation_expense         number(22,2)   default 0 null,
 depr_exp_adjust              number(22,2)   default 0 null,
 depr_exp_alloc_adjust        number(22,2)   default 0 null,
 cost_of_removal              number(22,2)   default 0 null,
 reserve_retirements          number(22,2)   default 0 null,
 salvage_returns              number(22,2)   default 0 null,
 salvage_cash                 number(22,2)   default 0 null,
 reserve_credits              number(22,2)   default 0 null,
 reserve_adjustments          number(22,2)   default 0 null,
 reserve_tran_in              number(22,2)   default 0 null,
 reserve_tran_out             number(22,2)   default 0 null,
 gain_loss                    number(22,2)   default 0 null,
 vintage_net_salvage_amort    number(22,2)   default 0 null,
 vintage_net_salvage_reserve  number(22,2)   default 0 null,
 current_net_salvage_amort    number(22,2)   default 0 null,
 current_net_salvage_reserve  number(22,2)   default 0 null,
 impairment_reserve_beg       number(22,2)   default 0 null,
 impairment_reserve_act       number(22,2)   default 0 null,
 impairment_reserve_end       number(22,2)   default 0 null,
 est_ann_net_adds             number(22,2)   default 0 null,
 rwip_allocation              number(22,2)   default 0 null,
 cor_beg_reserve              number(22,2)   default 0 null,
 cor_expense                  number(22,2)   default 0 null,
 cor_exp_adjust               number(22,2)   default 0 null,
 cor_exp_alloc_adjust         number(22,2)   default 0 null,
 cor_res_tran_in              number(22,2)   default 0 null,
 cor_res_tran_out             number(22,2)   default 0 null,
 cor_res_adjust               number(22,2)   default 0 null,
 cor_end_reserve              number(22,2)   default 0 null,
 cost_of_removal_rate         number(22,8)   null,
 cost_of_removal_base         number(22,2)   default 0 null,
 rwip_cost_of_removal         number(22,2)   default 0 null,
 rwip_salvage_cash            number(22,2)   default 0 null,
 rwip_salvage_returns         number(22,2)   default 0 null,
 rwip_reserve_credits         number(22,2)   default 0 null,
 salvage_rate                 number(22,8)   null,
 salvage_base                 number(22,2)   default 0 null,
 salvage_expense              number(22,2)   default 0 null,
 salvage_exp_adjust           number(22,2)   default 0 null,
 salvage_exp_alloc_adjust     number(22,2)   default 0 null,
 reserve_bal_salvage_exp      number(22,2)   default 0 null,
 reserve_blending_adjustment  number(22,2)   default 0 null,
 reserve_blending_transfer    number(22,2)   default 0 null,
 cor_blending_adjustment      number(22,2)   default 0 null,
 cor_blending_transfer        number(22,2)   default 0 null,
 impairment_asset_amount      number(22,2)   default 0 null,
 impairment_expense_amount    number(22,2)   default 0 null,
 reserve_bal_impairment       number(22,2)   default 0 null,
 begin_balance_year           number(22,2)   default 0 null,
 begin_reserve_year           number(22,2)   default 0 null,
 begin_cor_year               number(22,2)   default 0 null,
 begin_balance_qtr            number(22,2)   default 0 null,
 begin_reserve_qtr            number(22,2)   default 0 null,
 begin_cor_qtr                number(22,2)   default 0 null,
 plant_activity               number(22,2)   default 0 null,
 reserve_activity             number(22,2)   default 0 null,
 cor_activity                 number(22,2)   default 0 null,
 plant_activity_2             number(22,2)   default 0 null,
 reserve_activity_2           number(22,2)   default 0 null,
 cor_activity_2               number(22,2)   default 0 null,
 est_net_adds                 number(22,2)   default 0 null,
 cumulative_transfers         number(22,2)   default 0 null,
 reserve_diff                 number(22,2)   default 0 null,
 fiscal_month                 number(2,0)    null,
 fiscalqtrstart               date           null,
 type_2_exist                 number(1,0)    null,
 cum_uop_depr                 number(22,2)   default 0 null,
 cum_uop_depr_2               number(22,2)   default 0 null,
 production                   number(22,2)   default 0 null,
 estimated_production         number(22,2)   default 0 null,
 production_2                 number(22,2)   default 0 null,
 estimated_production_2       number(22,2)   default 0 null,
 smooth_curve                 varchar2(5)    null,
 min_calc                     number(22,2)   default 0 null,
 min_uop_exp                  number(22,2)   default 0 null,
 ytd_uop_depr                 number(22,2)   default 0 null,
 ytd_uop_depr_2               number(22,2)   default 0 null,
 curr_uop_exp                 number(22,2)   default 0 null,
 curr_uop_exp_2               number(22,2)   default 0 null,
 less_year_uop_depr           number(22,2)   default 0 null,
 depreciation_uop_rate        number(22,8)   default 0 null,
 depreciation_uop_rate_2      number(22,8)   default 0 null,
 depreciation_base_uop        number(22,2)   default 0 null,
 depreciation_base_uop_2      number(22,2)   default 0 null,
 over_depr_adj                number(22,2)   default 0 null,
 over_depr_adj_cor            number(22,2)   default 0 null,
 over_depr_adj_salv           number(22,2)   default 0 null,
 orig_begin_reserve           number(22,2)   default 0 null,
 orig_cor_beg_reserve         number(22,2)   default 0 null,
 orig_depr_expense            number(22,2)   default 0 null,
 orig_depr_exp_alloc_adjust   number(22,2)   default 0 null,
 orig_salv_expense            number(22,2)   default 0 null,
 orig_salv_exp_alloc_adjust   number(22,2)   default 0 null,
 orig_cor_expense             number(22,2)   default 0 null,
 orig_cor_exp_alloc_adjust    number(22,2)   default 0 null,
 retro_depr_adj               number(22,2)   default 0 null,
 retro_cor_adj                number(22,2)   default 0 null,
 retro_salv_adj               number(22,2)   default 0 null
) on commit preserve rows;

comment on table DEPR_CALC_STG_ARC is '(T)  [01]
A temporary table to perform group depreciation calculation against.  It is the callers repsonsibility to stage the data and handle the results.';

comment on column DEPR_CALC_STG_ARC.DEPR_GROUP_ID is 'Internal ID linking to the depreciation group';
comment on column DEPR_CALC_STG_ARC.SET_OF_BOOKS_ID is 'The set of books';
comment on column DEPR_CALC_STG_ARC.GL_POST_MO_YR is 'The accounting month';
comment on column DEPR_CALC_STG_ARC.CALC_MONTH is 'The month being calculated';
comment on column DEPR_CALC_STG_ARC.DEPR_CALC_STATUS is 'Status column';
comment on column DEPR_CALC_STG_ARC.DEPR_CALC_MESSAGE is 'An error mesaging';
comment on column DEPR_CALC_STG_ARC.TRF_IN_EST_ADDS is 'Are transfers included int eh net adds';
comment on column DEPR_CALC_STG_ARC.INCLUDE_RWIP_IN_NET is 'Is rwip included in the net reserve';
comment on column DEPR_CALC_STG_ARC.DNSA_COR_BAL is 'Net Salvage Amortization Balance for Cost of Removal';
comment on column DEPR_CALC_STG_ARC.DNSA_SALV_BAL is 'Net Salvage Amortization Balance for Salvage';
comment on column DEPR_CALC_STG_ARC.COR_YTD is 'The year to date cost of removal';
comment on column DEPR_CALC_STG_ARC.SALV_YTD is 'The year to date salvage';
comment on column DEPR_CALC_STG_ARC.FISCALYEAROFFSET is 'Fiscal year offset';
comment on column DEPR_CALC_STG_ARC.FISCALYEARSTART is 'The start of the fiscal year';
comment on column DEPR_CALC_STG_ARC.COMPANY_ID is 'Internal ID linking to company';
comment on column DEPR_CALC_STG_ARC.SUBLEDGER_TYPE_ID is 'The subledger indicator';
comment on column DEPR_CALC_STG_ARC.DESCRIPTION is 'The description';
comment on column DEPR_CALC_STG_ARC.DEPR_METHOD_ID is 'Internal ID linking to the depreciation method';
comment on column DEPR_CALC_STG_ARC.MID_PERIOD_CONV is 'The percent of current activity to include in depreciation base';
comment on column DEPR_CALC_STG_ARC.MID_PERIOD_METHOD is 'The mid period method';
comment on column DEPR_CALC_STG_ARC.DG_EST_ANN_NET_ADDS is 'The estimated net additions and adjustment';
comment on column DEPR_CALC_STG_ARC.TRUE_UP_CPR_DEPR is 'Trueup individually depreciated assets';
comment on column DEPR_CALC_STG_ARC.RATE is 'The rate';
comment on column DEPR_CALC_STG_ARC.NET_GROSS is '1 means calculate depreciation based on the net balance.';
comment on column DEPR_CALC_STG_ARC.OVER_DEPR_CHECK is 'Perform overdepreciation checks';
comment on column DEPR_CALC_STG_ARC.NET_SALVAGE_PCT is 'Percent of beginning balance that should be salvage';
comment on column DEPR_CALC_STG_ARC.RATE_USED_CODE is 'Has the rate been used in a calculation before';
comment on column DEPR_CALC_STG_ARC.END_OF_LIFE is 'End of life for end of life methods';
comment on column DEPR_CALC_STG_ARC.EXPECTED_AVERAGE_LIFE is 'The expected average life';
comment on column DEPR_CALC_STG_ARC.RESERVE_RATIO_ID is 'The reserve ration';
comment on column DEPR_CALC_STG_ARC.DMR_COST_OF_REMOVAL_RATE is 'The cost of removal rate';
comment on column DEPR_CALC_STG_ARC.COST_OF_REMOVAL_PCT is 'The percentage of the plant balance that is estimated cost of removal';
comment on column DEPR_CALC_STG_ARC.DMR_SALVAGE_RATE is 'The salvage rate';
comment on column DEPR_CALC_STG_ARC.INTEREST_RATE is 'The interest rate';
comment on column DEPR_CALC_STG_ARC.AMORTIZABLE_LIFE is 'The amortizable life of the depreciation group';
comment on column DEPR_CALC_STG_ARC.COR_TREATMENT is 'How cost of removal should be treated';
comment on column DEPR_CALC_STG_ARC.SALVAGE_TREATMENT is 'How is salvage treated';
comment on column DEPR_CALC_STG_ARC.NET_SALVAGE_AMORT_LIFE is 'Net Salvage Amortization life';
comment on column DEPR_CALC_STG_ARC.ALLOCATION_PROCEDURE is 'The depreciation allocation procedure';
comment on column DEPR_CALC_STG_ARC.DEPR_LEDGER_STATUS is 'The depr ledger status';
comment on column DEPR_CALC_STG_ARC.BEGIN_RESERVE is 'The Beginning reserve for the period';
comment on column DEPR_CALC_STG_ARC.END_RESERVE is 'The end reserve for the period';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_PROVISION is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_COR is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.SALVAGE_BALANCE is 'The salvage balance';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_ADJUST is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_RETIREMENTS is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_TRAN_IN is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_TRAN_OUT is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_OTHER_CREDITS is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_GAIN_LOSS is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_ALLOC_FACTOR is 'The reserve allocation factor';
comment on column DEPR_CALC_STG_ARC.BEGIN_BALANCE is 'The beginning plant balance for the period';
comment on column DEPR_CALC_STG_ARC.ADDITIONS is 'The plant addidtions';
comment on column DEPR_CALC_STG_ARC.RETIREMENTS is 'The retirement amount';
comment on column DEPR_CALC_STG_ARC.TRANSFERS_IN is 'Transfer In Amount';
comment on column DEPR_CALC_STG_ARC.TRANSFERS_OUT is 'Transfer Out Amount';
comment on column DEPR_CALC_STG_ARC.ADJUSTMENTS is 'Plant Adjustments';
comment on column DEPR_CALC_STG_ARC.DEPRECIATION_BASE is 'The deprecation base';
comment on column DEPR_CALC_STG_ARC.END_BALANCE is 'The end plant balance';
comment on column DEPR_CALC_STG_ARC.DEPRECIATION_RATE is 'The depreciation rate';
comment on column DEPR_CALC_STG_ARC.DEPRECIATION_EXPENSE is 'The current period calculated depreciation expense';
comment on column DEPR_CALC_STG_ARC.DEPR_EXP_ADJUST is 'Depreciation Expense Adjustment';
comment on column DEPR_CALC_STG_ARC.DEPR_EXP_ALLOC_ADJUST is 'Depreciation Expense Adjustment';
comment on column DEPR_CALC_STG_ARC.COST_OF_REMOVAL is 'Cost of Removal for the period';
comment on column DEPR_CALC_STG_ARC.RESERVE_RETIREMENTS is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.SALVAGE_RETURNS is 'Salvage returns';
comment on column DEPR_CALC_STG_ARC.SALVAGE_CASH is 'Salvage Cash';
comment on column DEPR_CALC_STG_ARC.RESERVE_CREDITS is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_ADJUSTMENTS is 'Reserve adjustment amount';
comment on column DEPR_CALC_STG_ARC.RESERVE_TRAN_IN is 'Reserve Transfer Into this group';
comment on column DEPR_CALC_STG_ARC.RESERVE_TRAN_OUT is 'Reserve Transfer out of this group';
comment on column DEPR_CALC_STG_ARC.GAIN_LOSS is 'Gain loss';
comment on column DEPR_CALC_STG_ARC.VINTAGE_NET_SALVAGE_AMORT is 'The vintaged net salvage amortization';
comment on column DEPR_CALC_STG_ARC.VINTAGE_NET_SALVAGE_RESERVE is 'The vintaged net salvage reserve';
comment on column DEPR_CALC_STG_ARC.CURRENT_NET_SALVAGE_AMORT is 'Current Net Salvage Amortization';
comment on column DEPR_CALC_STG_ARC.CURRENT_NET_SALVAGE_RESERVE is 'Current Net Salvage Reserve';
comment on column DEPR_CALC_STG_ARC.IMPAIRMENT_RESERVE_BEG is 'The beginning balance for the impairment basis bucket';
comment on column DEPR_CALC_STG_ARC.IMPAIRMENT_RESERVE_ACT is 'The activity for the impairment basis bucket';
comment on column DEPR_CALC_STG_ARC.IMPAIRMENT_RESERVE_END is 'The ending reserve for the impairment';
comment on column DEPR_CALC_STG_ARC.EST_ANN_NET_ADDS is 'Estimate adds and adjustments';
comment on column DEPR_CALC_STG_ARC.RWIP_ALLOCATION is 'The amount of rwip allocation to this group';
comment on column DEPR_CALC_STG_ARC.COR_BEG_RESERVE is 'The COR beginning Reserve for the perios';
comment on column DEPR_CALC_STG_ARC.COR_EXPENSE is 'Current month calculated cost of removal expense';
comment on column DEPR_CALC_STG_ARC.COR_EXP_ADJUST is 'Current month cost of removal expense adjustment';
comment on column DEPR_CALC_STG_ARC.COR_EXP_ALLOC_ADJUST is 'Current month cost of removal expense adjustment';
comment on column DEPR_CALC_STG_ARC.COR_RES_TRAN_IN is 'Cost of removal transferred in';
comment on column DEPR_CALC_STG_ARC.COR_RES_TRAN_OUT is 'Cost of removal transferred out';
comment on column DEPR_CALC_STG_ARC.COR_RES_ADJUST is 'Cost of removal reserve adjustment';
comment on column DEPR_CALC_STG_ARC.COR_END_RESERVE is 'Cost of removal ending reserve';
comment on column DEPR_CALC_STG_ARC.COST_OF_REMOVAL_RATE is 'The cost of removal rate';
comment on column DEPR_CALC_STG_ARC.COST_OF_REMOVAL_BASE is 'The basis for calculating cost of removal expense';
comment on column DEPR_CALC_STG_ARC.RWIP_COST_OF_REMOVAL is 'The amount of RWIP Allocation that is cost of removal';
comment on column DEPR_CALC_STG_ARC.RWIP_SALVAGE_CASH is 'The amount of RWIP Allocation that is salvage cash';
comment on column DEPR_CALC_STG_ARC.RWIP_SALVAGE_RETURNS is 'The amount of RWIP Allocation that is salvage returns';
comment on column DEPR_CALC_STG_ARC.RWIP_RESERVE_CREDITS is 'The amount of RWIP Allocation that is salvage credits';
comment on column DEPR_CALC_STG_ARC.SALVAGE_RATE is 'The salvage rate';
comment on column DEPR_CALC_STG_ARC.SALVAGE_BASE is 'The salvage base';
comment on column DEPR_CALC_STG_ARC.SALVAGE_EXPENSE is 'The salvage expense';
comment on column DEPR_CALC_STG_ARC.SALVAGE_EXP_ADJUST is 'The salvage expense adjustment';
comment on column DEPR_CALC_STG_ARC.SALVAGE_EXP_ALLOC_ADJUST is 'The salvage expense adjustment';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_SALVAGE_EXP is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_BLENDING_ADJUSTMENT is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.RESERVE_BLENDING_TRANSFER is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.COR_BLENDING_ADJUSTMENT is 'The COR adjustment due to blended set of books';
comment on column DEPR_CALC_STG_ARC.COR_BLENDING_TRANSFER is 'The COR Transfer due to blended set of books';
comment on column DEPR_CALC_STG_ARC.IMPAIRMENT_ASSET_AMOUNT is 'The amount of the impairment';
comment on column DEPR_CALC_STG_ARC.IMPAIRMENT_EXPENSE_AMOUNT is 'The expense amount being impaired';
comment on column DEPR_CALC_STG_ARC.RESERVE_BAL_IMPAIRMENT is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.BEGIN_BALANCE_YEAR is 'The beginning plant balance for the year';
comment on column DEPR_CALC_STG_ARC.BEGIN_RESERVE_YEAR is 'The beginning reserve balance for the year';
comment on column DEPR_CALC_STG_ARC.BEGIN_COR_YEAR is 'The beginning cost of removal reserve balance for the year';
comment on column DEPR_CALC_STG_ARC.BEGIN_BALANCE_QTR is 'The beginning plant balance for the quarter';
comment on column DEPR_CALC_STG_ARC.BEGIN_RESERVE_QTR is 'The beginning reserve balance for the quarter';
comment on column DEPR_CALC_STG_ARC.BEGIN_COR_QTR is 'The beginning cost of removal reserve balance for the quarter.';
comment on column DEPR_CALC_STG_ARC.PLANT_ACTIVITY is 'The plant activity for current month.';
comment on column DEPR_CALC_STG_ARC.RESERVE_ACTIVITY is 'The reserve activity for the month.';
comment on column DEPR_CALC_STG_ARC.COR_ACTIVITY is 'The cost of removal reserve activity for the month';
comment on column DEPR_CALC_STG_ARC.PLANT_ACTIVITY_2 is 'The plant activity for the period. This can be different from plant_activity depending on the mid-period method.';
comment on column DEPR_CALC_STG_ARC.RESERVE_ACTIVITY_2 is 'The reserve activity for the period. This can be different from plant_activity depending on the mid-period method.';
comment on column DEPR_CALC_STG_ARC.COR_ACTIVITY_2 is 'The cost of removal reserve activity for the period. This can be different from plant_activity depending on the mid-period method.';
comment on column DEPR_CALC_STG_ARC.EST_NET_ADDS is 'Estimated Adds';
comment on column DEPR_CALC_STG_ARC.CUMULATIVE_TRANSFERS is 'Cumulative transfers';
comment on column DEPR_CALC_STG_ARC.RESERVE_DIFF is 'Internal field used for depreciation calculation';
comment on column DEPR_CALC_STG_ARC.FISCAL_MONTH is 'The firscal moth';
comment on column DEPR_CALC_STG_ARC.FISCALQTRSTART is 'The starting quarter for the month';
comment on column DEPR_CALC_STG_ARC.TYPE_2_EXIST is 'Used for unit of production calcualations';
comment on column DEPR_CALC_STG_ARC.CUM_UOP_DEPR is 'Unit of Production';
comment on column DEPR_CALC_STG_ARC.CUM_UOP_DEPR_2 is 'Unit of Production 2';
comment on column DEPR_CALC_STG_ARC.PRODUCTION is 'Used for unit of production calcualations';
comment on column DEPR_CALC_STG_ARC.ESTIMATED_PRODUCTION is 'Estimated Production for unit of production';
comment on column DEPR_CALC_STG_ARC.PRODUCTION_2 is 'Used for unit of production calcualations';
comment on column DEPR_CALC_STG_ARC.ESTIMATED_PRODUCTION_2 is 'Estimated Production for unit of production approach 2';
comment on column DEPR_CALC_STG_ARC.SMOOTH_CURVE is 'Should the trueup methodolgy use smoothing';
comment on column DEPR_CALC_STG_ARC.MIN_CALC is 'Used for unit of production calcualations';
comment on column DEPR_CALC_STG_ARC.MIN_UOP_EXP is 'Used for unit of production calcualations';
comment on column DEPR_CALC_STG_ARC.YTD_UOP_DEPR is 'Used for unit of production calcualations';
comment on column DEPR_CALC_STG_ARC.YTD_UOP_DEPR_2 is 'Used for unit of production calcualations';
comment on column DEPR_CALC_STG_ARC.CURR_UOP_EXP is 'Expense for unit of production 1';
comment on column DEPR_CALC_STG_ARC.CURR_UOP_EXP_2 is 'Expense for unit of production 2';
comment on column DEPR_CALC_STG_ARC.LESS_YEAR_UOP_DEPR is 'Used for unit of production calcualations';
comment on column DEPR_CALC_STG_ARC.DEPRECIATION_UOP_RATE is 'The depreciation rate for unit of production';
comment on column DEPR_CALC_STG_ARC.DEPRECIATION_UOP_RATE_2 is 'The depreciation rate for unit of production approach 2';
comment on column DEPR_CALC_STG_ARC.DEPRECIATION_BASE_UOP is 'The depreciation base for unit of production';
comment on column DEPR_CALC_STG_ARC.DEPRECIATION_BASE_UOP_2 is 'The depreciation base for unit of production approach 2';
comment on column DEPR_CALC_STG_ARC.OVER_DEPR_ADJ is 'The over depr adjustment';
comment on column DEPR_CALC_STG_ARC.OVER_DEPR_ADJ_COR is 'The over depr adjustment from COR';
comment on column DEPR_CALC_STG_ARC.OVER_DEPR_ADJ_SALV is 'The over depr adjustment from Salvage';
comment on column DEPR_CALC_STG_ARC.ORIG_BEGIN_RESERVE is 'The original begin_reserve from depr_ledger. Used because the retroactive rate change calculation can change the begin_reserve column on this table.';
comment on column DEPR_CALC_STG_ARC.ORIG_COR_BEG_RESERVE is 'The original cor_beg_reserve from depr_ledger. Used because the retroactive rate change calculation can change the cor_beg_reserve column on this table.';
comment on column DEPR_CALC_STG_ARC.ORIG_DEPR_EXPENSE is 'The original depreciation expense from depr_ledger. Used because the retroactive rate change calculation can change the depreciation_expense column on this table.';
comment on column DEPR_CALC_STG_ARC.ORIG_DEPR_EXP_ALLOC_ADJUST is 'The original depr_exp_alloc_adjust from depr_ledger. Used because the retroactive rate change calculation can change the depr_exp_alloc_adjust column on this table.';
comment on column DEPR_CALC_STG_ARC.ORIG_SALV_EXPENSE is 'The original salvage expense from depr_ledger. Used because the retroactive rate change calculation can change the salvage_expense column on this table.';
comment on column DEPR_CALC_STG_ARC.ORIG_SALV_EXP_ALLOC_ADJUST is 'The original salv_exp_alloc_adj from depr_ledger. Used because the retroactive rate change calculation can change the salv_exp_alloc_adjust column on this table.';
comment on column DEPR_CALC_STG_ARC.ORIG_COR_EXPENSE is 'The original cost of removal expense from depr_ledger. Used because the retroactive rate change calculation can change the cor_expense column on this table.';
comment on column DEPR_CALC_STG_ARC.ORIG_COR_EXP_ALLOC_ADJUST is 'The original cor_exp_alloc_adjust from depr_ledger. Used because the retroactive rate change calculation can change the cor_exp_alloc_adjust column on this table.';
comment on column DEPR_CALC_STG_ARC.RETRO_DEPR_ADJ is 'The adjustment to depreciation expense due to the retroactive rate change calculation. This value makes up part of the depr_exp_alloc_adjust column on depr_ledger.';
comment on column DEPR_CALC_STG_ARC.RETRO_COR_ADJ is 'The adjustment to cost of removal expense due to the retroactive rate change calculation. This value makes up part of the cor_exp_alloc_adjust column on depr_ledger.';
comment on column DEPR_CALC_STG_ARC.RETRO_SALV_ADJ is 'The adjustment to salvage expene due to the retroactive rate change calculation. This value makes up part of the salv_exp_alloc_adjust column on depr_ledger.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (757, 0, 10, 4, 1, 2, 30565, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_030565_depr_recreate_arc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
