/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043362_depr_02_reload_saved_data_2015_1_0_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/24/2015 Charlie Shilling columns have incorrect decimal size
||============================================================================
*/

--changing the precision of a column can throw an O R A - 1440,
--so we need to:
-- 1. save off the data in a temporary table, (in previous script)
-- 2. trucate the old table, (in previous script)
-- 3. modify the old table, (in previous script)
-- 4. reload the old table, and
-- 5. delete the temp table (in subsequent script)

--NOTE only the arc table has permanent data. the stg table is global temp,
-- so we don't need to worry about saving its data.

--
-- #4
--
INSERT INTO depr_calc_combined_arc
SELECT *
FROM depr_calc_combined_temp;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2443, 0, 2015, 1, 0, 0, 043362, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043362_depr_02_reload_saved_data_2015_1_0_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;