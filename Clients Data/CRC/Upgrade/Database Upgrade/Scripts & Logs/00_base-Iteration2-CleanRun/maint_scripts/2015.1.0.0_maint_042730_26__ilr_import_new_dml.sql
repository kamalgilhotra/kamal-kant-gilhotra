/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_26__ilr_import_new_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

insert into pp_import_column(import_type_id, column_name, description, import_column_name,
   is_required, processing_order, column_type, is_on_table)
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_ilr') AS import_type_id,
   'funding_status_id' AS column_name,
   'ILR Funding Status' AS description,
   'funding_status_xlate' AS import_column_name,
   1 AS is_required,
   1 AS processing_order,
   'number(22,0)' AS column_type,
   1 AS is_on_table
from dual
;

declare 
	max_field_id pp_import_template_fields.field_id%type;
begin 
	select nvl(max(field_id),0) 
	into max_field_id
	from pp_import_template_fields pitf
	where pitf.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_ilr');
 
	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select pit.import_template_id, max_field_id + 1, pit.import_type_id, 'funding_status_id', null
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_ilr');
end;
/

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
   is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql,
   derived_autocreate_yn)
select
   (select max(import_lookup_id)+1 from pp_import_lookup) import_lookup_id,
   'ILR Funding Status.Description' AS description,
   'ILR Funding Status.Description' AS long_Description,
   'funding_status_id' AS column_name,
   '(select fs.funding_status_id from ls_funding_status fs where upper( trim( <importfield> ) ) = upper( trim( fs.description ) ) )'AS lookup_sql,
   0 AS is_derived,
   'ls_funding_status' AS lookup_table_name,
   'description' AS lookup_column_name,
   null AS lookup_constraining_columns,
   null AS lookup_values_alternate_sql,
   null AS derived_autocreate_yn
from dual;

update pp_import_template_fields set import_lookup_id = (
   select import_lookup_id
   from pp_import_lookup
   where lookup_table_name = 'ls_funding_status')
where column_name = 'funding_status_id'
;

commit;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2424, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_26__ilr_import_new_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;