/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052241_lessee_01_modify_ls_forecast_version_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.1  08/28/2018 Sarah Byers      Modify curr type and currency to nullable on ls_forecast_version
||============================================================================
*/

alter table ls_forecast_version modify (
ls_currency_type_id number(22,0) null,
currency_id number(22,0) null);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
  (9302, 0, 2017, 4, 0, 1, 52241, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052241_lessee_01_modify_ls_forecast_version_ddl.sql', 1,
  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
  