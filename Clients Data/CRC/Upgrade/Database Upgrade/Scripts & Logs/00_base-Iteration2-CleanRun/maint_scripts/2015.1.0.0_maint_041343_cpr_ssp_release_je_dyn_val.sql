/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041343_cpr_ssp_release_je_dyn_val.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1     12/1/2014  Sarah Byers      Dynamic Validation Type for CPR - Release JE process
||============================================================================
*/

insert into wo_validation_type (
	wo_validation_type_id, description, long_description, "FUNCTION", find_company, col1, col2, col3, hard_edit)
values (
	1006, 'CPR - Release JE', 'CPR Control - Release Journal Entries', 'f_release_je', 'select company_id from company_setup where company_id = <arg2>', 'month', 'company_id', 'dw_sqls', 1);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2075, 0, 2015, 1, 0, 0, 041343, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041343_cpr_ssp_release_je_dyn_val.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;