/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040132_reg_PKG_REG_ANNUALIZATION.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.7 09/29/2014 Shane Ward
||============================================================================
*/

CREATE OR REPLACE package pkg_reg_annualization
/*
||============================================================================
|| Application: PowerPlant (Rates and Regulatory)
|| Object Name: PKG_REG_ANNUALIZATION
|| Description: Reg Annualization Functions.
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date        Revised By           Reason for Change
|| ------- ----------  ------------------   ------------------------------------
|| 1.0     01/09/2014  Shane "C" Ward       Create
|| 1.1     05/23/2014  Shane "B-day" Ward   Update Case Logic (MAINT-38301)
||============================================================================
*/
/*
******************************************************
**  NOTE: CALLER MUST HANDLE ROLLBACKS AND COMMITS  **
******************************************************
*/

 as

   procedure P_ANNUALIZEBYSOURCE(A_VERSIONID       number,
                                 A_SOURCEID        number,
                                 A_STARTDATE       number,
                                 A_ENDDATE         number,
                                 A_COMPANYID       number,
                                 A_LEDGERTABLENAME varchar2,
                                 A_ACCTTABLENAME   varchar2,
                                 A_VERSIONCOLNAME  varchar2);

   procedure P_ANNUALIZEDATA(A_VERSIONID       number,
                             A_STARTDATE       number,
                             A_ENDDATE         number,
                             A_COMPANYID       number,
                             A_LEDGERTABLENAME varchar2,
                             A_ACCTTABLENAME   varchar2,
                             A_VERSIONCOLNAME  varchar2,
                             A_WHEREACCT       varchar2,
                             A_ALLOWPARTIAL    number);

   procedure P_ANNUALIZEACCT(A_VERSIONID       number,
                             A_ACCTID          number,
                             A_STARTDATE       number,
                             A_ENDDATE         number,
                             A_COMPANYID       number,
                             A_LEDGERTABLENAME varchar2,
                             A_ACCTTABLENAME   varchar2,
                             A_VERSIONCOLNAME  varchar2);

   procedure P_ANNUALIZECASE(A_CASEID    number,
                             A_COMPANYID number,
                             A_YEAREND   number);

   procedure P_ANNUALIZECASE(A_CASEID    number,
                             A_COMPANYID number);

   procedure P_ANNUALIZEMONTHLYADJUSTMENTS(A_ADJUSTMENT number,
                                           A_CASEID     number,
                                           A_COMPANYID  number,
                                           A_ENDYEAR    number);

   PROCEDURE P_ANNUALIZEINCADJUST (A_VERSIONID NUMBER,
                                  A_STARTDATE NUMBER,
                                  A_ENDDATE   NUMBER);


end PKG_REG_ANNUALIZATION;
/

CREATE OR REPLACE package body pkg_reg_annualization as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --       VARIABLES
   --**************************************************************
   G_INCLUDECASEID boolean := false;

   --Note: Each Annualization Type is stored in reg_annualization_type
   -- Types of Annualization
   --       13 Month Avg        (1)
   --       12 Month Avg        (2)
   --       2 Point Avg         (3)
   --       Ending Balance      (4)
   --       Sum of 12 Months    (5)
   --       Ending Month * 12   (6)
   --       Input               (7)
   --       13 Month Avg of Avg (8)

   --**************************************************************
   --       PROCEDURES
   --**************************************************************

   --**************************************************************************
   --                            p_AnnualizeMonthlyAdjustments
   --
   -- Procedure: p_AnnualizeMonthlyAdjustments
   --
   -- Description: Method used to Annualize a specified Case. To perform this
   -- annualization this method will annualize every account for the specified
   -- Case for each year speified.
   --
   -- Paramaters:
   --     a_adjustment: The ID of the adjustment to annualize.
   --     a_caseId: The ID of the regulatory case that the adjustment belongs to.
   --     a_companyId: The ID of the company that the case belongs to.
   --     a_endYear: The adjustment year end date.
   --
   --**************************************************************************

   procedure P_ANNUALIZEMONTHLYADJUSTMENTS(A_ADJUSTMENT number,
                                           A_CASEID     number,
                                           A_COMPANYID  number,
                                           A_ENDYEAR    number) is

   begin

      PKG_REG_ANNUALIZATION.P_ANNUALIZEBYSOURCE(A_CASEID,
                                                0,
                                                A_ENDYEAR,
                                                A_ENDYEAR,
                                                A_COMPANYID,
                                                'reg_annualize_work',
                                                'reg_case_acct',
                                                'version_id');

   end P_ANNUALIZEMONTHLYADJUSTMENTS;

   --**************************************************************************
   --                            P_AnnualizeBySource
   --
   -- Procedure: P_AnnualizeBySource
   --
   -- Description: Used for REG_..._LEDGER. All supporting months must exist
   -- in the table to be able to generate an annualized amount.  For example, if
   -- the reg account uses the 12 Month Average annualization type the table must
   -- have the prior 11 months of data for the month that needs to be annualized.
   -- If the supporting months do not exist, the annualization type will be null.
   --
   -- Paramaters:
   --     a_versionId: The version ID (historical or forecast).
   --     a_sourceId: The source ID (0 = all sources).
   --     a_startDate
   --     a_endDatE
   --     a_companyId: The company ID that the various accounts that are to be
   --     annualized belong to.
   --     a_ledgerTableName: The name of the ledger table to pull data from for
   --     the annualization calculation.
   --     a_acctTableName: The account table to pull the annualization type from.
   --     a_versionColName: The column name that contains the version data
   --
   --**************************************************************************
   procedure P_ANNUALIZEBYSOURCE(A_VERSIONID       number,
                                 A_SOURCEID        number,
                                 A_STARTDATE       number,
                                 A_ENDDATE         number,
                                 A_COMPANYID       number,
                                 A_LEDGERTABLENAME varchar2,
                                 A_ACCTTABLENAME   varchar2,
                                 A_VERSIONCOLNAME  varchar2) is

      REG_ACCT_SOURCE_ID number;
      WHEREACCT          varchar2(2000);

   begin
      --Make sure source ID is between 0 and 6 or greater than 100
      if A_SOURCEID not between 0 and 6 and A_SOURCEID < 100 then
         if A_SOURCEID is not null then
            --Error
            RAISE_APPLICATION_ERROR(-20000,
                                    TO_CHAR(A_SOURCEID) ||
                                    ' is an unsupported source ID. Valid values are: 0 - all sources, 1 - ' ||
                                    'Assets/Depr, 2 - CWIP/RWIP/WIP Computation, 3 - CR, 4 - Deferred Tax, ' ||
                                    '5 - Property Tax, 6 - Tax Provision, or > 100 for external sources.');
         end if;
      end if;

      if LOWER(A_ACCTTABLENAME) = 'reg_case_acct' then
         G_INCLUDECASEID := true;
      else
         G_INCLUDECASEID := false;
      end if;

      if A_SOURCEID > 100 then
         --Look up reg acct source
         begin
            select REG_ACCT_SOURCE_ID
              into REG_ACCT_SOURCE_ID
              from REG_EXT_SOURCE
             where REG_SOURCE_ID = A_SOURCEID;
         exception
            when NO_DATA_FOUND then
               --set reg_acct_source_id to a_sourceId
               REG_ACCT_SOURCE_ID := A_SOURCEID;
         end;
      end if;

      if A_SOURCEID <> 0 then
         WHEREACCT := 'and nvl(lt.reg_source_id,' || TO_CHAR(A_SOURCEID) || ') = ' ||
                      TO_CHAR(A_SOURCEID) || '   ';
      else
         WHEREACCT := '';
      end if;

      WHEREACCT := WHEREACCT || ' and lt.reg_acct_id in (select m.reg_acct_id from ' ||
                   A_ACCTTABLENAME || ' m where 5 = 5   and ';
      if A_SOURCEID <> 0 then
         if A_SOURCEID > 100 then
            WHEREACCT := WHEREACCT || ' m.reg_source_id = ' || TO_CHAR(REG_ACCT_SOURCE_ID) ||
                         ' and ';
         else
            WHEREACCT := WHEREACCT || ' m.reg_source_id = ' || TO_CHAR(A_SOURCEID) || ' and ';
         end if;
      end if;

      if G_INCLUDECASEID then
         WHEREACCT := WHEREACCT || ' m.reg_case_id = ' || TO_CHAR(A_VERSIONID) || ' and ';
      end if;

      WHEREACCT := SUBSTR(WHEREACCT, 0, LENGTH(WHEREACCT) - 4);

      PKG_REG_ANNUALIZATION.P_ANNUALIZEDATA(A_VERSIONID,
                                            A_STARTDATE,
                                            A_ENDDATE,
                                            A_COMPANYID,
                                            A_LEDGERTABLENAME,
                                            A_ACCTTABLENAME,
                                            A_VERSIONCOLNAME,
                                            WHEREACCT,
                                            0);

   end P_ANNUALIZEBYSOURCE;

   --**************************************************************************
   --                            P_AnnualizeAcct
   -- Procedure: p_AnnualizeAcct
   --
   -- Annualizes a single regulatory account based on annualization type from AcctTableName argument,
   --    i.e., REG_ACCT_MASTER.  Annualizaton is for LedgerTableName argument, i.e.,
   --    REG_HISTORY_LEDGER. All supporting months must exist in the table to be able to generate an annualized amount.
   --    For example, if the reg account uses the 12 Month Average annualization type the table must have the prior 11 months of
   --    data for the month that needs to be annualized. If the supporting months do not exist,
   --    the annualization type will be null.
   --
   --    CALLER MUST HANDLE ROLLBACKS AND COMMITS
   --
   -- Paramaters:
   --     a_versionId: The version ID (historical or forecast).
   --     a_startDate:
   --     a_endDate:
   --     a_companyId: The company ID that the various accounts that are to be annualized belong to.
   --     a_ledgerTableName: The name of the ledger table to pull data from for the annualization calculation.
   --     a_acctTableName: The account table to pull the annualization type from.
   --     a_versionColName: The column name that contains the version data
   --     a_whereacct: Where clause for annualization
   --
   --**************************************************************************
   procedure P_ANNUALIZEACCT(A_VERSIONID       number,
                             A_ACCTID          number,
                             A_STARTDATE       number,
                             A_ENDDATE         number,
                             A_COMPANYID       number,
                             A_LEDGERTABLENAME varchar2,
                             A_ACCTTABLENAME   varchar2,
                             A_VERSIONCOLNAME  varchar2) is
      WHERE_ACCT varchar2(2000);

   begin
      if LOWER(A_ACCTTABLENAME) = 'reg_case_acct' then
         G_INCLUDECASEID := true;
      else
         G_INCLUDECASEID := false;
      end if;

      WHERE_ACCT := 'and lt.reg_acct_id in (select m.reg_acct_id from ' || A_ACCTTABLENAME ||
                    ' m where m.reg_acct_Id = ' || TO_CHAR(A_ACCTID);

      if G_INCLUDECASEID then
         WHERE_ACCT := WHERE_ACCT || ' and m.reg_case_id = ' || TO_CHAR(A_VERSIONID);
      end if;

      PKG_REG_ANNUALIZATION.P_ANNUALIZEDATA(A_VERSIONID,
                                            A_STARTDATE,
                                            A_ENDDATE,
                                            A_COMPANYID,
                                            A_LEDGERTABLENAME,
                                            A_ACCTTABLENAME,
                                            A_VERSIONCOLNAME,
                                            WHERE_ACCT,
                                            0);

   end P_ANNUALIZEACCT;


   --**************************************************************************
   --                            p_AnnualizeIncAdjust
   -- Procedure: p_AnnualizeIncAdjust
   --
   -- Annualizes data for a specific forecast version and date range in reg_inc_adj_ledger
   --
   --    CALLER MUST HANDLE ROLLBACKS AND COMMITS
   --
   -- Paramaters:
   --     a_versionId: The version ID (forecast only for now).
   --     a_startDate:
   --     a_endDate:
   --
   --**************************************************************************
   PROCEDURE P_ANNUALIZEINCADJUST (A_VERSIONID NUMBER,
                                  A_STARTDATE NUMBER,
                                  A_ENDDATE   NUMBER) is
      WHERE_ACCT varchar2(2000);

   BEGIN

      where_acct := ' and lt.reg_acct_id in '||
      '(select m.reg_acct_id from reg_acct_master m where 5=5 ';


      --ORIGINAL AMOUNT
      insert into REG_ANNUALIZE_WORK
               (REG_ACCT_ID, GL_MONTH, ACT_AMOUNT, VERSION_ID, REG_COMPANY_ID)
               select L.REG_ACCT_ID,
                      L.GL_MONTH,
                      sum(NVL(orig_amount, 0)),
                      a_versionid,
                      0
                 from reg_fcst_inc_adj_ledger l
                where L.incremental_adj_id = a_versionID
                group by L.REG_ACCT_ID, L.GL_MONTH;

      /*
      If we ever supress loading 0 amount rows for incremental
      we need to have a for loop here and after following insert statements
      to insert 0 records for annualization
      like in P_ANNUALIZECASE() - SCW 8/6/14
      */

      PKG_REG_ANNUALIZATION.P_ANNUALIZEDATA(A_VERSIONID,
                                            A_STARTDATE,
                                            A_ENDDATE,
                                            0,
                                            'reg_annualize_work',
                                            'reg_acct_master',
                                            'version_id',
                                            where_acct,
                                            1);

      update REG_FCST_INC_ADJ_LEDGER L
        set ANNUALIZED_ORIG_AMT =
              (select ANNUALIZED_AMT
                from REG_ANNUALIZE_WORK W
                where W.REG_ACCT_ID = L.REG_ACCT_ID
                  and W.GL_MONTH = L.GL_MONTH
                  and L.INCREMENTAL_ADJ_ID = A_VERSIONID)
                  WHERE l.incremental_adj_id = a_versionId;

      DELETE FROM REG_ANNUALIZE_WORK;


      --UPDATED AMOUNT
      insert into REG_ANNUALIZE_WORK
               (REG_ACCT_ID, GL_MONTH, ACT_AMOUNT, VERSION_ID, REG_COMPANY_ID)
               select L.REG_ACCT_ID,
                      L.GL_MONTH,
                      sum(NVL(updated_amount, 0)),
                      a_versionid,
                      0
                 from reg_fcst_inc_adj_ledger l
                where L.incremental_adj_id = a_versionID
                group by L.REG_ACCT_ID, L.GL_MONTH;


      PKG_REG_ANNUALIZATION.P_ANNUALIZEDATA(A_VERSIONID,
                                            A_STARTDATE,
                                            A_ENDDATE,
                                            0,
                                            'reg_annualize_work',
                                            'reg_acct_master',
                                            'version_id',
                                            where_acct,
                                            1);
      update REG_FCST_INC_ADJ_LEDGER L
        set ANNUALIZED_UPDATED_AMT =
              (select ANNUALIZED_AMT
                from REG_ANNUALIZE_WORK W
                where W.REG_ACCT_ID = L.REG_ACCT_ID
                  and W.GL_MONTH = L.GL_MONTH
                  and L.INCREMENTAL_ADJ_ID = A_VERSIONID)
                  WHERE l.incremental_adj_id = a_versionId;

      DELETE FROM REG_ANNUALIZE_WORK;


      --ADJUST AMOUNT
      insert into REG_ANNUALIZE_WORK
               (REG_ACCT_ID, GL_MONTH, ACT_AMOUNT, VERSION_ID, REG_COMPANY_ID)
               select L.REG_ACCT_ID,
                      L.GL_MONTH,
                      sum(NVL(adj_amount, 0)),
                      a_versionid,
                      0
                 from reg_fcst_inc_adj_ledger l
                where L.incremental_adj_id = a_versionID
                group by L.REG_ACCT_ID, L.GL_MONTH;

      PKG_REG_ANNUALIZATION.P_ANNUALIZEDATA(A_VERSIONID,
                                            A_STARTDATE,
                                            A_ENDDATE,
                                            0,
                                            'reg_annualize_work',
                                            'reg_acct_master',
                                            'version_id',
                                            where_acct,
                                            1);

      update REG_FCST_INC_ADJ_LEDGER L
        set ANNUALIZED_ADJ_AMT =
              (select ANNUALIZED_AMT
                from REG_ANNUALIZE_WORK W
                where W.REG_ACCT_ID = L.REG_ACCT_ID
                  and W.GL_MONTH = L.GL_MONTH
                  and L.INCREMENTAL_ADJ_ID = A_VERSIONID)
                  WHERE l.incremental_adj_id = a_versionId;

      DELETE FROM REG_ANNUALIZE_WORK;

   end P_ANNUALIZEINCADJUST;



   --**************************************************************************
   --                            P_12MoAvg
   --**************************************************************************
   --Annualization Logic for 12 Month Average
   procedure P_12MOAVG(A_VERSIONID       number,
                       A_STARTDATE       number,
                       A_ENDDATE         number,
                       A_COMPANYID       number,
                       A_LEDGERTABLENAME varchar2,
                       A_ACCTTABLENAME   varchar2,
                       A_VERSIONCOLNAME  varchar2,
                       A_WHEREACCT       varchar2,
                       A_AMTCOLUMN       varchar2,
                       A_COMPANYCLAUSE   VARCHAR2,
                       A_ANNUALIZECOL    VARCHAR2) is
      SQLS varchar2(20000);
      annualizedCol VARCHAR2(100);

   BEGIN

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' =
						(select AA
						   from (select ROUND((sum('||A_AMTCOLUMN||')
											   OVER(partition by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID order by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID,
													X.GL_MONTH ROWS between 11 PRECEDING and current row)) / 12,
											  2) AA,
										X.*
								   from '||A_LEDGERTABLENAME||' X) H
						  where H.REG_COMPANY_ID = LT.REG_COMPANY_ID
							and H.REG_ACCT_ID = LT.REG_ACCT_ID
							and NVL(H.REG_SOURCE_ID, -1) = NVL(LT.REG_SOURCE_ID, -1)
							and H.'||A_VERSIONCOLNAME||' = LT.'||A_VERSIONCOLNAME||'
							and H.GL_MONTH = LT.GL_MONTH)
				 where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||' '
				   || NVL(A_COMPANYCLAUSE,' ')|| A_WHEREACCT ||'
				    and M.REG_ANNUALIZATION_ID = 2)';

      execute immediate SQLS;

   end P_12MOAVG;

   --**************************************************************************
   --                            p_2PointAvg
   --**************************************************************************
   --Annualization Logic for 2 Point Average
   procedure P_2POINTAVG(A_VERSIONID       number,
                         A_STARTDATE       number,
                         A_ENDDATE         number,
                         A_COMPANYID       number,
                         A_LEDGERTABLENAME varchar2,
                         A_ACCTTABLENAME   varchar2,
                         A_VERSIONCOLNAME  varchar2,
                         A_WHEREACCT       varchar2,
                         A_AMTCOLUMN       varchar2,
                         A_COMPANYCLAUSE   VARCHAR2,
                         A_ANNUALIZECOL    VARCHAR2) is

      SQLS varchar2(20000);

   begin

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' =
						(select AA
						   from (select ROUND(('||A_AMTCOLUMN||' + LAG('||A_AMTCOLUMN||', 12, '||A_AMTCOLUMN||')
											   OVER(partition by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID order by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID,
													X.GL_MONTH)) / 2,
											  2) AA,
										X.GL_MONTH,
										X.REG_ACCT_ID,
										X.REG_SOURCE_ID,
										X.REG_COMPANY_ID,
										X.'||A_VERSIONCOLNAME||'
								   from '||A_LEDGERTABLENAME||' X) H
						  where H.REG_COMPANY_ID = LT.REG_COMPANY_ID
							and H.REG_ACCT_ID = LT.REG_ACCT_ID
							and NVL(H.REG_SOURCE_ID, -1) = NVL(LT.REG_SOURCE_ID, -1)
							and H.'||A_VERSIONCOLNAME||' = LT.'||A_VERSIONCOLNAME||'
							and H.GL_MONTH = LT.GL_MONTH)
				 where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||'
				   and '||TO_CHAR(A_ENDDATE)||' '||NVL(A_COMPANYCLAUSE,' ')||'
				   '|| A_WHEREACCT||'
				    and M.REG_ANNUALIZATION_ID = 3)';

      execute immediate SQLS;

   end P_2POINTAVG;

   --**************************************************************************
   --                            p_EndingBal
   --**************************************************************************
   --Annualization Logic for Ending Balance
   procedure P_ENDINGBAL(A_VERSIONID       number,
                         A_STARTDATE       number,
                         A_ENDDATE         number,
                         A_COMPANYID       number,
                         A_LEDGERTABLENAME varchar2,
                         A_ACCTTABLENAME   varchar2,
                         A_VERSIONCOLNAME  varchar2,
                         A_WHEREACCT       varchar2,
                         A_AMTCOLUMN       varchar2,
                         A_COMPANYCLAUSE   VARCHAR2,
                         A_ANNUALIZECOL    VARCHAR2) is

      SQLS varchar2(20000);

   begin

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				set '||A_ANNUALIZECOL||' = '||A_AMTCOLUMN||'
				where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||
				' ' ||NVL(A_COMPANYCLAUSE,' ')||' '
				|| A_WHEREACCT ||
				' and M.REG_ANNUALIZATION_ID = 4)';


      execute immediate SQLS;

   end P_ENDINGBAL;

   --**************************************************************************
   --                            p_SumOf12Months
   --**************************************************************************
   --Annualization Logic for Sum of 12 Months
   procedure P_SUMOF12MONTHS(A_VERSIONID       number,
                             A_STARTDATE       number,
                             A_ENDDATE         number,
                             A_COMPANYID       number,
                             A_LEDGERTABLENAME varchar2,
                             A_ACCTTABLENAME   varchar2,
                             A_VERSIONCOLNAME  varchar2,
                             A_WHEREACCT       varchar2,
                             A_AMTCOLUMN       varchar2,
                             A_COMPANYCLAUSE   VARCHAR2,
                             A_ANNUALIZECOL    VARCHAR2) is

      SQLS varchar2(20000);

   begin

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' =
						(select AA
						   from (select ROUND((sum('||A_AMTCOLUMN||')
											   OVER(partition by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID order by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID,
													X.GL_MONTH ROWS between 11 PRECEDING and current row)),
											  2) AA,
										X.*
								   from '||A_LEDGERTABLENAME||' X) H
						  where H.REG_COMPANY_ID = LT.REG_COMPANY_ID
							and H.REG_ACCT_ID = LT.REG_ACCT_ID
							and NVL(H.REG_SOURCE_ID, -1) = NVL(LT.REG_SOURCE_ID, -1)
							and H.'||A_VERSIONCOLNAME||' = LT.'||A_VERSIONCOLNAME||'
							and H.GL_MONTH = LT.GL_MONTH)
				 where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||' '
				   || NVL(A_COMPANYCLAUSE, ' ') || ' '
				   || A_WHEREACCT ||
				   ' and M.REG_ANNUALIZATION_ID = 5)';

      execute immediate SQLS;
   end P_SUMOF12MONTHS;

   --**************************************************************************
   --                            p_EndMonthX12
   --**************************************************************************
   --Annualization Logic for Ending Month * 12
   procedure P_ENDMONTHX12(A_VERSIONID       number,
                           A_STARTDATE       number,
                           A_ENDDATE         number,
                           A_COMPANYID       number,
                           A_LEDGERTABLENAME varchar2,
                           A_ACCTTABLENAME   varchar2,
                           A_VERSIONCOLNAME  varchar2,
                           A_WHEREACCT       varchar2,
                           A_AMTCOLUMN       varchar2,
                           A_COMPANYCLAUSE   VARCHAR2,
                           A_ANNUALIZECOL    VARCHAR2) is

      SQLS varchar2(20000);

   begin

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' = ROUND(('||A_AMTCOLUMN||') * 12, 2)
				 where LT.'||A_VERSIONCOLNAME||' = TO_CHAR('||A_VERSIONID||')
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||' '
				   ||NVL(A_COMPANYCLAUSE,' ')||' '
				   ||A_WHEREACCT||
				   'and M.REG_ANNUALIZATION_ID = 6)';

      execute immediate SQLS;
   end P_ENDMONTHX12;

   --**************************************************************************
   --                            p_13MoAvg
   --**************************************************************************
   --Annualization Logic for 13 Month Avg
   procedure P_13MOAVG(A_VERSIONID       number,
                       A_STARTDATE       number,
                       A_ENDDATE         number,
                       A_COMPANYID       number,
                       A_LEDGERTABLENAME varchar2,
                       A_ACCTTABLENAME   varchar2,
                       A_VERSIONCOLNAME  varchar2,
                       A_WHEREACCT       varchar2,
                       A_AMTCOLUMN       varchar2,
                       A_COMPANYCLAUSE   VARCHAR2,
                       A_ANNUALIZECOL    VARCHAR2) is

      SQLS varchar2(20000);

   begin

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' =
						(select AA
						   from (select ROUND((sum('||A_AMTCOLUMN||')
											   OVER(partition by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID order by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID,
													X.GL_MONTH ROWS between 12 PRECEDING and current row)) / 13,
											  2) AA,
										X.*
								   from '||A_LEDGERTABLENAME||' X) H
						  where H.REG_COMPANY_ID = LT.REG_COMPANY_ID
							and H.REG_ACCT_ID = LT.REG_ACCT_ID
							and NVL(H.REG_SOURCE_ID, -1) = NVL(LT.REG_SOURCE_ID, -1)
							and H.'||A_VERSIONCOLNAME||' = LT.'||A_VERSIONCOLNAME||'
							and H.GL_MONTH = LT.GL_MONTH)
				 where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||'
				    '|| NVL(A_COMPANYCLAUSE,' ')||'
					 '||A_WHEREACCT||'
				   and M.REG_ANNUALIZATION_ID = 1)';

      execute immediate SQLS;

   end P_13MOAVG;

   --**************************************************************************
   --                            p_13MoAvgOfAvg
   --**************************************************************************
   --Annualization Logic for 13 Month Avg of Avg
   procedure P_13MOAVGOFAVG(A_VERSIONID       number,
                            A_STARTDATE       number,
                            A_ENDDATE         number,
                            A_COMPANYID       number,
                            A_LEDGERTABLENAME varchar2,
                            A_ACCTTABLENAME   varchar2,
                            A_VERSIONCOLNAME  varchar2,
                            A_WHEREACCT       varchar2,
                            A_AMTCOLUMN       varchar2,
                            A_COMPANYCLAUSE   VARCHAR2,
                            A_ANNUALIZECOL    VARCHAR2) is
      SQLS varchar2(20000);

   begin

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' =
						(select AA
						   from (select ROUND(('||A_AMTCOLUMN||'*.5 + (LAG('||A_AMTCOLUMN||', 13, '||A_AMTCOLUMN||')
											   OVER(partition by X.'||A_VERSIONCOLNAME||',
																		X.REG_COMPANY_ID,
																		X.REG_ACCT_ID,
																		X.REG_SOURCE_ID order by X.'||A_VERSIONCOLNAME||',
																		X.REG_COMPANY_ID,
																		X.REG_ACCT_ID,
																		X.GL_MONTH)) * .5 + sum('||A_AMTCOLUMN||')
											   OVER(partition by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID order by X.'||A_VERSIONCOLNAME||',
													X.REG_COMPANY_ID,
													X.REG_ACCT_ID,
													X.REG_SOURCE_ID,
													X.GL_MONTH ROWS between 12 PRECEDING and 1 PRECEDING)) / 13,
											  2) AA,
										X.GL_MONTH,
										X.REG_ACCT_ID,
										X.REG_SOURCE_ID,
										X.REG_COMPANY_ID,
										X.'||A_VERSIONCOLNAME||'
								   from '||A_LEDGERTABLENAME||' X) H
						  where H.REG_COMPANY_ID = LT.REG_COMPANY_ID
							and H.REG_ACCT_ID = LT.REG_ACCT_ID
							and NVL(H.REG_SOURCE_ID, -1) = NVL(LT.REG_SOURCE_ID, -1)
							and H.'||A_VERSIONCOLNAME||' = LT.'||A_VERSIONCOLNAME||'
							and H.GL_MONTH = LT.GL_MONTH)
				 where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||'
				   '||NVL(A_COMPANYCLAUSE,' ')||
				   ' '||A_WHEREACCT||'
				   and M.REG_ANNUALIZATION_ID = 8)';


      execute immediate SQLS;

   end P_13MOAVGOFAVG;

   --**************************************************************************
   --                            p_CleanFor12Month
   --**************************************************************************
   -- Annualization Logic for nulling out annualized amounts for
   --               annualization methods that require 12 Months
   procedure P_CLEANFOR12MONTH(A_VERSIONID       number,
                               A_STARTDATE       number,
                               A_ENDDATE         number,
                               A_COMPANYID       number,
                               A_LEDGERTABLENAME varchar2,
                               A_ACCTTABLENAME   varchar2,
                               A_VERSIONCOLNAME  varchar2,
                               A_WHEREACCT       varchar2,
                               A_AMTCOLUMN       varchar2,
                               A_COMPANYCLAUSE   VARCHAR2,
                               A_ANNUALIZECOL    VARCHAR2) is
      SQLS       varchar2(20000);
      CASECLAUSE varchar2(2000);
   begin

      if G_INCLUDECASEID then
         CASECLAUSE := '      and m.reg_case_id = ' || TO_CHAR(A_VERSIONID) || '  ';
      else
         CASECLAUSE := ' ';
      end if;

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' = null
				 where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||' '
				   ||NVL(A_COMPANYCLAUSE,' ')||'
				   and exists
				 (select 1
						  from '||DBMS_ASSERT.SIMPLE_SQL_NAME(A_ACCTTABLENAME)||' M
						 where M.REG_ACCT_ID = LT.REG_ACCT_ID
						 '||NVL(CASECLAUSE,' ')||'
						   and M.REG_ANNUALIZATION_ID in (2, 5))
				   and LT.GL_MONTH in (select R.GL_MONTH - LAG(R.GL_MONTH, 11, 0) OVER(partition by R.REG_ACCT_ID order by R.REG_ACCT_ID, R.GL_MONTH)
										 from '||A_LEDGERTABLENAME||' R
										where R.'||A_VERSIONCOLNAME||' = LT.'||A_VERSIONCOLNAME||'
										  and R.REG_COMPANY_ID = LT.REG_COMPANY_ID
										  and R.REG_ACCT_ID = LT.REG_ACCT_ID) '||A_WHEREACCT||')';

      execute immediate SQLS;

   end P_CLEANFOR12MONTH;

   --**************************************************************************
   --                            p_CleanFor13Month
   --**************************************************************************
   -- Annualization Logic for nulling out annualized amounts for
   --               annualization methods that require 13 Months
   procedure P_CLEANFOR13MONTH(A_VERSIONID       number,
                               A_STARTDATE       number,
                               A_ENDDATE         number,
                               A_COMPANYID       number,
                               A_LEDGERTABLENAME varchar2,
                               A_ACCTTABLENAME   varchar2,
                               A_VERSIONCOLNAME  varchar2,
                               A_WHEREACCT       varchar2,
                               A_AMTCOLUMN       varchar2,
                               A_COMPANYCLAUSE   VARCHAR2,
                               A_ANNUALIZECOL    VARCHAR2) is
      SQLS       varchar2(20000);
      CASECLAUSE varchar2(2000);

   begin

      if G_INCLUDECASEID then
         CASECLAUSE := '      and m.reg_case_id = ' || TO_CHAR(A_VERSIONID) || '  ';
      else
         CASECLAUSE := ' ';
      end if;

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' = null
				 where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||' '
				   || NVL(A_COMPANYCLAUSE,' ')||'
				   and exists
				 (select 1
						  from '||DBMS_ASSERT.SIMPLE_SQL_NAME(A_ACCTTABLENAME)||' M
						 where M.REG_ACCT_ID = LT.REG_ACCT_ID '
						 ||NVL(CASECLAUSE,' ')||'
						   and M.REG_ANNUALIZATION_ID in (1, 3))
				   and LT.GL_MONTH in (select R.GL_MONTH - LAG(R.GL_MONTH, 12, 0) OVER(partition by R.REG_ACCT_ID order by R.REG_ACCT_ID, R.GL_MONTH)
										 from '||A_LEDGERTABLENAME||' R
										where R.'||A_VERSIONCOLNAME||' = LT.'||A_VERSIONCOLNAME||'
										  and R.REG_COMPANY_ID = LT.REG_COMPANY_ID
										  and R.REG_ACCT_ID = LT.REG_ACCT_ID) '||A_WHEREACCT||')';

      execute immediate SQLS;

   end P_CLEANFOR13MONTH;

   --**************************************************************************
   --                            p_CleanFor14Month
   --**************************************************************************
   -- Annualization Logic for nulling out annualized amounts for
   --               annualization methods that require 14 Months
   procedure P_CLEANFOR14MONTH(A_VERSIONID       number,
                               A_STARTDATE       number,
                               A_ENDDATE         number,
                               A_COMPANYID       number,
                               A_LEDGERTABLENAME varchar2,
                               A_ACCTTABLENAME   varchar2,
                               A_VERSIONCOLNAME  varchar2,
                               A_WHEREACCT       varchar2,
                               A_AMTCOLUMN       varchar2,
                               A_COMPANYCLAUSE   VARCHAR2,
                               A_ANNUALIZECOL    VARCHAR2) is
      SQLS       varchar2(20000);
      CASECLAUSE varchar2(2000);

   begin

      if G_INCLUDECASEID then
         CASECLAUSE := '      and m.reg_case_id = ' || TO_CHAR(A_VERSIONID) || '  ';
      else
         CASECLAUSE := ' ';
      end if;

      SQLS := 'update '||A_LEDGERTABLENAME||' LT
				   set '||A_ANNUALIZECOL||' = null
				 where LT.'||A_VERSIONCOLNAME||' = '||TO_CHAR(A_VERSIONID)||'
				   and LT.GL_MONTH between '||TO_CHAR(A_STARTDATE)||' and '||TO_CHAR(A_ENDDATE)||' '
				   || NVL(A_COMPANYCLAUSE,' ')||'
				   and exists
				 (select 1
						  from '||DBMS_ASSERT.SIMPLE_SQL_NAME(A_ACCTTABLENAME)||' M
						 where M.REG_ACCT_ID = LT.REG_ACCT_ID '
						 ||NVL(CASECLAUSE,' ')||'
						   and M.REG_ANNUALIZATION_ID in (8))
				   and LT.GL_MONTH in (select R.GL_MONTH - LAG(R.GL_MONTH, 13, 0) OVER(partition by R.REG_ACCT_ID order by R.REG_ACCT_ID, R.GL_MONTH)
										 from '||A_LEDGERTABLENAME||' R
										where R.'||A_VERSIONCOLNAME||' = LT.'||A_VERSIONCOLNAME||'
										  and R.REG_COMPANY_ID = LT.REG_COMPANY_ID
										  and R.REG_ACCT_ID = LT.REG_ACCT_ID) '||A_WHEREACCT||')';


      execute immediate SQLS;

   end P_CLEANFOR14MONTH;

   --**************************************************************************
   --                            p_AnnualizeCase
   -- Procedure: p_AnnualizeCase
   --
   -- Description: Method used to Annualize a specified Case. To perform this
   -- annualization this method will annualize every account for the specified
   -- Case for each year speified.
   --
   -- Paramaters:
   --     a_caseId: The ID of the regulatory case to annualize.
   --     a_companyId: The ID of the company that the case belongs to.
   --     a_yearEnd: The year end to annualize
   --
   -- Note: Historic_flag: 1 = true 0 = false
   -- Returns: Nested Table that contains version information
   --***************************************************************************

   procedure P_ANNUALIZECASE(A_CASEID    number,
                             A_COMPANYID number,
                             A_YEAREND   number) is
      RANGESTART       number;
      NUMVERSIONS      number;
      VERSIONSTART     number;
      VERSIONEND       number;
      HISTORIC_FLAG    number;
      TEMPVERSIONSTART number;
      VERSIONID        number;
      I                number;

   begin
      RANGESTART := TO_NUMBER(TO_CHAR(ADD_MONTHS((TO_DATE(A_YEAREND, 'YYYYMM')), -11), 'yyyymm'));

      for C_VERSIONS in (select REG_CASE_ID, HIST_VERSION_ID, FORE_VERSION_ID, START_DATE, END_DATE
                           from REG_CASE_COVERAGE_VERSIONS
                          where REG_CASE_ID = A_CASEID
                            and coverage_yr_ended = a_yearend
                          order by START_DATE asc)
      loop

         VERSIONSTART := C_VERSIONS.START_DATE;
         VERSIONEND   := C_VERSIONS.END_DATE;

         if VERSIONSTART = RANGESTART then
            TEMPVERSIONSTART := TO_NUMBER(TO_CHAR(ADD_MONTHS((TO_DATE(VERSIONSTART, 'YYYYMM')), -2),
                                                  'yyyymm'));
         else
            TEMPVERSIONSTART := VERSIONSTART;
         end if;

         if C_VERSIONS.HIST_VERSION_ID > 0 then
            HISTORIC_FLAG := 1;

            VERSIONID := C_VERSIONS.HIST_VERSION_ID;

            insert into REG_ANNUALIZE_WORK
               (REG_ACCT_ID, GL_MONTH, ACT_AMOUNT, VERSION_ID, REG_COMPANY_ID)
               select L.REG_ACCT_ID,
                      L.GL_MONTH,
                      sum(NVL(L.ACT_AMOUNT, 0) + NVL(L.ADJ_AMOUNT, 0) + NVL(L.RECON_ADJ_AMOUNT, 0)),
                      A_CASEID,
                      L.REG_COMPANY_ID
                 from REG_HISTORY_LEDGER L, REG_CASE_ACCT A
                where L.REG_COMPANY_ID = A_COMPANYID
                  and L.GL_MONTH between TEMPVERSIONSTART and VERSIONEND
                  and L.HISTORIC_VERSION_ID = VERSIONID
                  and L.REG_ACCT_ID = A.REG_ACCT_ID
                  and A.REG_CASE_ID = A_CASEID
                  and A.STATUS_CODE_ID = 1
                group by L.REG_ACCT_ID, L.GL_MONTH, L.REG_COMPANY_ID;

            for I in TEMPVERSIONSTART .. VERSIONEND
            loop
               if TO_NUMBER(SUBSTR(TO_CHAR(I), 5)) > 12 or TO_NUMBER(SUBSTR(TO_CHAR(I), 5)) < 1 then
                  CONTINUE;
               end if;

               insert into REG_ANNUALIZE_WORK
                  (REG_ACCT_ID, GL_MONTH, ACT_AMOUNT, VERSION_ID, REG_COMPANY_ID)
                  select A.REG_ACCT_ID, I, 0, A_CASEID, A_COMPANYID
                    from REG_CASE_ACCT A
                   where A.REG_CASE_ID = A_CASEID
                     and A.STATUS_CODE_ID = 1
                     and not exists (select 1
                            from REG_HISTORY_LEDGER L
                           where L.HISTORIC_VERSION_ID = VERSIONID
                             and L.GL_MONTH = I
                             and L.REG_COMPANY_ID = A_COMPANYID
                             and L.REG_ACCT_ID = A.REG_ACCT_ID);

            end loop;

         else
            HISTORIC_FLAG := 0;

            VERSIONID := C_VERSIONS.FORE_VERSION_ID;

            insert into REG_ANNUALIZE_WORK
               (REG_ACCT_ID, GL_MONTH, ACT_AMOUNT, VERSION_ID, REG_COMPANY_ID)
               select L.REG_ACCT_ID, L.GL_MONTH, sum(L.FCST_AMOUNT), A_CASEID, L.REG_COMPANY_ID
                 from REG_FORECAST_LEDGER L, REG_CASE_ACCT A
                where L.REG_COMPANY_ID = A_COMPANYID
                  and L.GL_MONTH between TEMPVERSIONSTART and VERSIONEND
                  and L.FORECAST_VERSION_ID = VERSIONID
                  and L.REG_ACCT_ID = A.REG_ACCT_ID
                  and A.REG_CASE_ID = A_CASEID
                  and A.STATUS_CODE_ID = 1
                group by L.REG_ACCT_ID, L.GL_MONTH, L.REG_COMPANY_ID;

            for I in TEMPVERSIONSTART .. VERSIONEND
            loop
               if TO_NUMBER(SUBSTR(TO_CHAR(I), 5)) > 12 or TO_NUMBER(SUBSTR(TO_CHAR(I), 5)) < 1 then
                  CONTINUE;
               end if;

               insert into REG_ANNUALIZE_WORK
                  (REG_ACCT_ID, GL_MONTH, ACT_AMOUNT, VERSION_ID, REG_COMPANY_ID)
                  select A.REG_ACCT_ID, I, 0, A_CASEID, A_COMPANYID
                    from REG_CASE_ACCT A
                   where A.REG_CASE_ID = A_CASEID
                     and A.STATUS_CODE_ID = 1
                     and not exists (select 1
                            from REG_FORECAST_LEDGER L
                           where L.FORECAST_VERSION_ID = VERSIONID
                             and L.GL_MONTH = I
                             and L.REG_COMPANY_ID = A_COMPANYID
                             and L.REG_ACCT_ID = A.REG_ACCT_ID);

            end loop;

         end if;

      end loop;

      P_ANNUALIZEBYSOURCE(A_CASEID,
                          0,
                          tempversionstart,
                          versionEnd,
                          A_COMPANYID,
                          'reg_annualize_work',
                          'reg_case_acct',
                          'version_id');

      merge into REG_CASE_SUMMARY_LEDGER D
      using (select VERSION_ID REG_CASE_ID, REG_ACCT_ID, GL_MONTH CASE_YEAR, ANNUALIZED_AMT
               from REG_ANNUALIZE_WORK
              where ANNUALIZED_AMT is not null
                and GL_MONTH = A_YEAREND) S
      on (D.REG_CASE_ID = S.REG_CASE_ID and D.REG_ACCT_ID = S.REG_ACCT_ID and D.CASE_YEAR = S.CASE_YEAR)
      when matched then
         update
            set D.TOTAL_CO_LEDGER_AMOUNT = S.ANNUALIZED_AMT,
                D.TOTAL_CO_INCLUDED_AMOUNT = S.ANNUALIZED_AMT,
                D.TOTAL_CO_FINAL_AMOUNT = S.ANNUALIZED_AMT
      when not matched then
         insert
            (REG_CASE_ID, REG_ACCT_ID, CASE_YEAR, TOTAL_CO_LEDGER_AMOUNT, TOTAL_CO_INCLUDED_AMOUNT,
             TOTAL_CO_ADJUST_AMOUNT, TOTAL_CO_FINAL_AMOUNT, JUR_ALLO_PERCENT,
             JUR_ALLO_RESULTS_AMOUNT, JUR_ADJUST_AMOUNT, JUR_FINAL_AMOUNT, JUR_RECOVERY_CLASS_AMOUNT,
             JUR_BASE_REV_REQ_AMOUNT)
         values
            (S.REG_CASE_ID, S.REG_ACCT_ID, S.CASE_YEAR, S.ANNUALIZED_AMT, S.ANNUALIZED_AMT, 0,
             S.ANNUALIZED_AMT, 0, 0, 0, 0, 0, 0);

      insert into REG_CASE_SUMMARY_LEDGER
         (REG_CASE_ID, REG_ACCT_ID, CASE_YEAR, TOTAL_CO_LEDGER_AMOUNT, TOTAL_CO_INCLUDED_AMOUNT,
          TOTAL_CO_ADJUST_AMOUNT, TOTAL_CO_FINAL_AMOUNT, JUR_ALLO_PERCENT, JUR_ALLO_RESULTS_AMOUNT,
          JUR_ADJUST_AMOUNT, JUR_FINAL_AMOUNT, JUR_RECOVERY_CLASS_AMOUNT, JUR_BASE_REV_REQ_AMOUNT)
         select C.REG_CASE_ID, C.REG_ACCT_ID, A_YEAREND, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
           from REG_CASE_ACCT C
          where C.REG_CASE_ID = A_CASEID
            and not exists (select 1
                   from REG_CASE_SUMMARY_LEDGER L
                  where L.REG_CASE_ID = C.REG_CASE_ID
                    and L.CASE_YEAR = A_YEAREND
                    and L.REG_ACCT_ID = C.REG_ACCT_ID);

      EXECUTE IMMEDIATE 'Truncate table reg_annualize_work';

   end P_ANNUALIZECASE;

   --**************************************************************************
   --                            p_AnnualizeCase
   --
   -- Procedure: p_AnnualizeCase
   --
   -- Description: Method used to Annualize a specified Case. To perform this
   -- annualization this method will annualize every account for the specified
   -- Case for each year specified.
   --
   -- Paramaters:
   --     a_caseId: The ID of the regulatory case to annualize.
   --     a_companyId: The ID of the company that the case belongs to.
   --**************************************************************************

   procedure P_ANNUALIZECASE(A_CASEID    number,
                             A_COMPANYID number) is

   begin

      for C_YEARENDS in (select distinct COVERAGE_YR_ENDED
                           from REG_CASE_COVERAGE_VERSIONS
                          where REG_CASE_ID = A_CASEID)
      loop
         PKG_REG_ANNUALIZATION.P_ANNUALIZECASE(A_CASEID, A_COMPANYID, C_YEARENDS.COVERAGE_YR_ENDED);
      end loop;

   end P_ANNUALIZECASE;
   --**************************************************************************
   --                            p_AnnualizeData
   --
   --  Annualizes data supported a single regulatory account based on annualization type
   --  from a_AccTableName argument. Annualization is for a_LedgerTableName.
   --  All supporting months must exist in the table to be able to generate an annualized amount.
   --  For example, if the reg account uses the 12 Month Average annualization type the table must have the prior
   --  11 months of data for the month that needs to be annualized. If the supporting months do not exist,
   --  the annualized amount for the first months will be null
   --
   --  a_allowPartial: 0 = false 1 = true
   --**************************************************************************

   procedure P_ANNUALIZEDATA(A_VERSIONID       number,
                             A_STARTDATE       number,
                             A_ENDDATE         number,
                             A_COMPANYID       number,
                             A_LEDGERTABLENAME VARCHAR2,
                             A_ACCTTABLENAME   varchar2,
                             A_VERSIONCOLNAME  varchar2,
                             A_WHEREACCT       varchar2,
                             A_ALLOWPARTIAL    number) is

      AMTCOLUMN     varchar2(2000);
      COMPANYCLAUSE varchar2(2000);
      LedgerTableName VARCHAR2(2000);
      AnnualizeColumn VARCHAR2(2000);

   BEGIN
      --Set Amount Columns
      if LOWER(A_LEDGERTABLENAME) = 'reg_forecast_ledger' then
         AMTCOLUMN := '(nvl(fcst_amount,0) + nvl(adj_amount,0))';
         AnnualizeColumn := 'annualized_amt';
      elsif LOWER(A_LEDGERTABLENAME) = 'reg_history_ledger' then
         AMTCOLUMN := '(nvl(act_amount,0) + nvl(adj_amount,0) + nvl(recon_adj_amount,0))';
         AnnualizeColumn := 'annualized_amt';
      else
         AMTCOLUMN := 'act_amount';
         AnnualizeColumn := 'annualized_amt';
      end if;

      ledgerTableName := a_ledgerTableName;

      if A_COMPANYID is not null and A_COMPANYID <> 0 then
         COMPANYCLAUSE := ' and lt.reg_company_id = ' || TO_CHAR(A_COMPANYID) || ' ';
      else
         COMPANYCLAUSE := ' ';
      end if;

      --12 month avg
      P_12MOAVG(A_VERSIONID,
                A_STARTDATE,
                A_ENDDATE,
                A_COMPANYID,
                a_ledgerTableName,
                A_ACCTTABLENAME,
                A_VERSIONCOLNAME,
                A_WHEREACCT,
                AMTCOLUMN,
                COMPANYCLAUSE,
                ANNUALIZECOLUMN);
      --2 Point Annual Avg
      P_2POINTAVG(A_VERSIONID,
                  A_STARTDATE,
                  A_ENDDATE,
                  A_COMPANYID,
                  a_ledgerTableName,
                  A_ACCTTABLENAME,
                  A_VERSIONCOLNAME,
                  A_WHEREACCT,
                  AMTCOLUMN,
                  COMPANYCLAUSE,
                  ANNUALIZECOLUMN);
      --Ending Balance
      P_ENDINGBAL(A_VERSIONID,
                  A_STARTDATE,
                  A_ENDDATE,
                  A_COMPANYID,
                  a_ledgerTableName,
                  A_ACCTTABLENAME,
                  A_VERSIONCOLNAME,
                  A_WHEREACCT,
                  AMTCOLUMN,
                  COMPANYCLAUSE,
                  ANNUALIZECOLUMN);
      --Sum of 12 Months
      P_SUMOF12MONTHS(A_VERSIONID,
                      A_STARTDATE,
                      A_ENDDATE,
                      A_COMPANYID,
                      a_ledgerTableName,
                      A_ACCTTABLENAME,
                      A_VERSIONCOLNAME,
                      A_WHEREACCT,
                      AMTCOLUMN,
                      COMPANYCLAUSE,
                      ANNUALIZECOLUMN);
      --Ending Month*12
      P_ENDMONTHX12(A_VERSIONID,
                    A_STARTDATE,
                    A_ENDDATE,
                    A_COMPANYID,
                    a_ledgerTableName,
                    A_ACCTTABLENAME,
                    A_VERSIONCOLNAME,
                    A_WHEREACCT,
                    AMTCOLUMN,
                    COMPANYCLAUSE,
                    ANNUALIZECOLUMN);
      --13 Month Average
      P_13MOAVG(A_VERSIONID,
                A_STARTDATE,
                A_ENDDATE,
                A_COMPANYID,
                a_ledgerTableName,
                A_ACCTTABLENAME,
                A_VERSIONCOLNAME,
                A_WHEREACCT,
                AMTCOLUMN,
                COMPANYCLAUSE,
                ANNUALIZECOLUMN);
      --13 Month Average of Averages
      P_13MOAVGOFAVG(A_VERSIONID,
                     A_STARTDATE,
                     A_ENDDATE,
                     A_COMPANYID,
                     a_ledgerTableName,
                     A_ACCTTABLENAME,
                     A_VERSIONCOLNAME,
                     A_WHEREACCT,
                     AMTCOLUMN,
                     COMPANYCLAUSE,
                     ANNUALIZECOLUMN);

      if A_ALLOWPARTIAL < 1 then
         -- Update annualized amount to null for reg accounts that have 12 month annualization type for dates less than startDate + 11
         P_CLEANFOR12MONTH(A_VERSIONID,
                           A_STARTDATE,
                           A_ENDDATE,
                           A_COMPANYID,
                           a_ledgerTableName,
                           A_ACCTTABLENAME,
                           A_VERSIONCOLNAME,
                           A_WHEREACCT,
                           AMTCOLUMN,
                           COMPANYCLAUSE,
                           ANNUALIZECOLUMN);
         -- Update annualized amount to null for reg accounts that have a 13 month annualization type for dates less than startDate + 12
         P_CLEANFOR13MONTH(A_VERSIONID,
                           A_STARTDATE,
                           A_ENDDATE,
                           A_COMPANYID,
                           a_ledgerTableName,
                           A_ACCTTABLENAME,
                           A_VERSIONCOLNAME,
                           A_WHEREACCT,
                           AMTCOLUMN,
                           COMPANYCLAUSE,
                           ANNUALIZECOLUMN);
            -- Update annualized amount to null for reg accounts that have a 14 month annualization type for dates less than startDate + 13
        P_CLEANFOR14MONTH(A_VERSIONID,
                          A_STARTDATE,
                          A_ENDDATE,
                          A_COMPANYID,
                          a_ledgerTableName,
                          A_ACCTTABLENAME,
                          A_VERSIONCOLNAME,
                          A_WHEREACCT,
                          AMTCOLUMN,
                          COMPANYCLAUSE,
                          ANNUALIZECOLUMN);


      end if;

   end P_ANNUALIZEDATA;

end PKG_REG_ANNUALIZATION;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1403, 0, 10, 4, 2, 7, 40132, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_040132_reg_PKG_REG_ANNUALIZATION.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;