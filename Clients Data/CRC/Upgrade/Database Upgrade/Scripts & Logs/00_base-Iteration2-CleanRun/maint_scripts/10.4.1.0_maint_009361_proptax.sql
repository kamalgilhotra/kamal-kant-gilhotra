/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009361_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   01/22/2012 Julia Breuer   Point Release
||============================================================================
*/

-- pt_temp_bills_copy
create global temporary table PT_TEMP_BILLS_COPY
(
 STATEMENT_ID      number(22,0) not null,
 STATEMENT_YEAR_ID number(22,0) not null
) on commit preserve rows;

create unique index PT_TEMP_BILLS_COPY
   on PT_TEMP_BILLS_COPY (STATEMENT_ID, STATEMENT_YEAR_ID);

begin
   DBMS_STATS.SET_TABLE_STATS('PWRPLANT', 'PT_TEMP_BILLS_COPY', '', null, null, 1, 1, 36, null);
   DBMS_STATS.LOCK_TABLE_STATS('PWRPLANT', 'PT_TEMP_BILLS_COPY');
end;
/

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (278, 0, 10, 4, 1, 0, 9361, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009361_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
