 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041729_pcm_dynamic_filters_ddl.sql
|| Description: Allow searching on really long values (like justifications)
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	12/30/2014 Ryan Oliveria  Justification values are too long
||============================================================================
*/

alter table PP_DYNAMIC_FILTER_VALUES_DW
modify (
	VALUE_ID varchar2(4000),
	VALUE_DESC varchar2(4000)
);

alter table PP_DYN_FILTER_SAVED_VALUES_DW
modify (
	VALUE_ID varchar2(4000),
	VALUE_DESC varchar2(4000)
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2131, 0, 2015, 1, 0, 0, 041729, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041729_pcm_dynamic_filters_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;