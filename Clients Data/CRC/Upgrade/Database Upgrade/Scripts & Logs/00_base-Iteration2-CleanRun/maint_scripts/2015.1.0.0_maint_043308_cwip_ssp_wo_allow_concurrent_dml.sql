/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043308_cwip_ssp_wo_allow_concurrent_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- ----------------     ------------------------------------
|| 2015.1.0.0 03/24/2015 Khamchanh Sisouphanh Set ALLOW_CONCURRENT for Server Side Processes
||============================================================================
*/

-- Update Server Side Processes
-- 13 records expected
update pp_processes set ALLOW_CONCURRENT = 1 where executable_file in 
('ssp_wo_approve_accruals.exe',
'ssp_wo_close_charge_collection.exe',
'ssp_wo_gl_reconciliation.exe',
'ssp_wo_new_month.exe',
'ssp_wo_calc_oh_afudc_wip.exe',
'ssp_wo_appr_oh_afudc_wip.exe',
'ssp_wo_auto_nonunitize.exe non',
'ssp_wo_auto_unitization.exe',
'ssp_wo_calculate_accruals.exe',
'ssp_wo_close_month.exe',
'ssp_wo_retirements.exe',
'ssp_wo_auto_nonunitize.exe failed',
'ssp_release_je_wo.exe');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2451, 0, 2015, 1, 0, 0, 043308, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043308_cwip_ssp_wo_allow_concurrent_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;