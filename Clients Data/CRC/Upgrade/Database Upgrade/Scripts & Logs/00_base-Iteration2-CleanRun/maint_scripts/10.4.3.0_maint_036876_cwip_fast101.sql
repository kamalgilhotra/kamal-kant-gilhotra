/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_036876_cwip_fast101.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.3.0 07/02/2014 Sunjin Cone
||=================================================================================
*/


create global temporary table UNITIZE_WO_LIST_TEMP
(
 WORK_ORDER_ID           number(22,0) not null,
 WORK_ORDER_NUMBER       varchar2(35) not null,
 COMPANY_ID              number(22,0) not null,
 BUS_SEGMENT_ID          number(22,0) not null,
 MAJOR_LOCATION_ID       number(22,0) not null,
 ASSET_LOCATION_ID       number(22,0),
 WO_STATUS_ID            number(22,0) not null,
 IN_SERVICE_DATE         date,
 COMPLETION_DATE         date,
 LATE_CHG_WAIT_PERIOD    number(22,0),
 FUNDING_WO_ID           number(22,0),
 DESCRIPTION             varchar2(35),
 LONG_DESCRIPTION        varchar2(254),
 CLOSING_OPTION_ID       number(22,0) not null,
 CWIP_GL_ACCOUNT         number(22,0) not null,
 NON_UNITIZED_GL_ACCOUNT number(22,0) not null,
 UNITIZED_GL_ACCOUNT     number(22,0) not null,
 ALLOC_METHOD_TYPE_ID    number(22,0) not null,
 ACCRUAL_TYPE_ID         number(22,0),
 RWIP_YES_NO             varchar2(3),
 EST_UNIT_ITEM_OPTION    number(22,0),
 UNIT_ITEM_FROM_ESTIMATE number(22,0),
 UNIT_ITEM_SOURCE        varchar2(35),
 UNITIZE_BY_ACCOUNT      number(22,0),
 TOLERANCE_ID            number(22,0),
 TOLERANCE_REVISION      number(22,0),
 TOLERANCE_THRESHOLD     number(22,2),
 TOLERANCE_PCT           number(22,4),
 TOLERANCE_ESTIMATE_AMT  number(22,2),
 TOLERANCE_ACTUAL_AMT    number(22,2),
 TOLERANCE_EST_ACT_DIFF  number(22,2),
 MAX_CHARGE_GROUP_ID     number(22,0),
 MAX_REVISION            number(22,0),
 MAX_UNIT_ITEM_ID        number(22,0),
 RTN_CODE                number(22,0),
 ERROR_MSG               varchar2(2000),
 UNITIZATION_TYPE        varchar2(35)
) on commit preserve rows;

comment on table  UNITIZE_WO_LIST_TEMP is '(C) [04] Global temp table to hold the work order(s) being unitized in a particular run.  No primary key.';
comment on column UNITIZE_WO_LIST_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_LIST_TEMP.WORK_ORDER_NUMBER is 'User-defined number associated with a Work Order.  This is usually a reference to an external numbering system.';
comment on column UNITIZE_WO_LIST_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_LIST_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of a particular business segment.';
comment on column UNITIZE_WO_LIST_TEMP.MAJOR_LOCATION_ID is 'System-assigned identifier of a particular major location.';
comment on column UNITIZE_WO_LIST_TEMP.ASSET_LOCATION_ID is 'System-assigned identifier of a particular asset location.';
comment on column UNITIZE_WO_LIST_TEMP.WO_STATUS_ID is 'System-assigned identifier of a particular work order status.';
comment on column UNITIZE_WO_LIST_TEMP.IN_SERVICE_DATE is 'Actual (day, month, year) date in which facility constructed is in service; AFUDC should cease and the accounting may now be in 106 or 101.';
comment on column UNITIZE_WO_LIST_TEMP.COMPLETION_DATE is 'Month/year/date that the job is complete and charges are in; at this point it is waiting unitization and closing.';
comment on column UNITIZE_WO_LIST_TEMP.LATE_CHG_WAIT_PERIOD is 'Number of months that a Work Order remains after the completion date before it is available for automatic unitization.';
comment on column UNITIZE_WO_LIST_TEMP.FUNDING_WO_ID is 'Refers to another work order identifier that is the funding work order to which this work order has been assigned.  Funding work  orders, if used, can be set up using a special Work Order Type on the Work Order Type table.';
comment on column UNITIZE_WO_LIST_TEMP.DESCRIPTION is 'Brief description of the Work Order.';
comment on column UNITIZE_WO_LIST_TEMP.LONG_DESCRIPTION is 'Detailed description of the Work Order.';
comment on column UNITIZE_WO_LIST_TEMP.CLOSING_OPTION_ID is 'System-assigned identifier of a particular closing option.';
comment on column UNITIZE_WO_LIST_TEMP.CWIP_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account for relieving the CWIP side of the work order (e.g., 107, 183 when posting to 106 or 101.';
comment on column UNITIZE_WO_LIST_TEMP.NON_UNITIZED_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account used when posting in a non-unitized manner (e.g., 106).';
comment on column UNITIZE_WO_LIST_TEMP.UNITIZED_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account to which unitized entries are.';
comment on column UNITIZE_WO_LIST_TEMP.ALLOC_METHOD_TYPE_ID is 'System-assigned identifier of a particular Allocation Method Type.';
comment on column UNITIZE_WO_LIST_TEMP.ACCRUAL_TYPE_ID is 'This field indicates which accrual type is assigned to the work order/funding project.';
comment on column UNITIZE_WO_LIST_TEMP.RWIP_YES_NO is 'Yes = Unitization includes RWIP charges.  No = Unitization excludes RWIP Charges.';
comment on column UNITIZE_WO_LIST_TEMP.EST_UNIT_ITEM_OPTION is 'Summarize (=0) ? Force unitization to create unit items from the summarized estimates (default). One for One (=1) = Allow unitization to create unit items from non-summarized estimate.';
comment on column UNITIZE_WO_LIST_TEMP.UNIT_ITEM_FROM_ESTIMATE is 'Yes (=1) ? Force unitization to create unit items from the estimates only and ignore actuals. No (=0) = Allow unitization to decide how to create unit items based on the presence of actuals first and then estimate (default).';
comment on column UNITIZE_WO_LIST_TEMP.UNIT_ITEM_SOURCE is 'This field indicates if the source of the unit item creation is Actuals, Estimates, or CPR.';
comment on column UNITIZE_WO_LIST_TEMP.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_WO_LIST_TEMP.TOLERANCE_ID is 'System-assigned identifier of a tolerance test used in unitization and set on the unitization tolerance table.';
comment on column UNITIZE_WO_LIST_TEMP.TOLERANCE_REVISION is 'Estimate revision associated with the unitization tolerance validation.';
comment on column UNITIZE_WO_LIST_TEMP.TOLERANCE_THRESHOLD is 'Estimate dollar VS Actual dollar threshold for the unitization tolerance.';
comment on column UNITIZE_WO_LIST_TEMP.TOLERANCE_PCT is 'Estimate vs Actual dollar difference vs Estimate dollar percent threshold for the unitization tolerance.';
comment on column UNITIZE_WO_LIST_TEMP.TOLERANCE_ESTIMATE_AMT is 'Estimate dollar amount for the work order.';
comment on column UNITIZE_WO_LIST_TEMP.TOLERANCE_ACTUAL_AMT is 'Actual dollar amount for the work order.';
comment on column UNITIZE_WO_LIST_TEMP.TOLERANCE_EST_ACT_DIFF is 'Absoulte value of the difference of Estimate vs Actual dollar amounts for the work order.';
comment on column UNITIZE_WO_LIST_TEMP.MAX_CHARGE_GROUP_ID is 'The maximum charge_group_id from CHARGE_GROUP_CONTROL table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST_TEMP.MAX_REVISION is 'The maximum revision from WO_ESTIMATE table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST_TEMP.MAX_UNIT_ITEM_ID is 'The maximum unit_item_id from UNITIZED_WORK_ORDER table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST_TEMP.RTN_CODE is 'System-assigned identifier of a particular return code saved to Auto101 errors log(WO_AUTO101_CONTROL).';
comment on column UNITIZE_WO_LIST_TEMP.ERROR_MSG is 'Error messages saved to Auto101 errors log(WO_AUTO101_CONTROL).';
comment on column UNITIZE_WO_LIST_TEMP.UNITIZATION_TYPE is 'Unitization can be run in muliptlie ways:  AUTO, AUTO LATE, AUTO WO, MANUAL';


drop table UNITIZE_ALLOC_LIST;

create global temporary table UNITIZE_ALLOC_LIST_TEMP
(
 COMPANY_ID     number(22,0) not null,
 ALLOC_ID       number(22,0) not null,
 ALLOC_PRIORITY number(22,0) not null,
 ALLOC_TYPE     varchar2(35) not null,
 ALLOC_BASIS    varchar2(35) not null
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_LIST_TEMP is '(C) [04] A global temp table that stores the list of distinct allocation methods for the work order(s) being unitized.';
comment on column UNITIZE_ALLOC_LIST_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LIST_TEMP.ALLOC_ID is 'System-assigned identifier of the unit allocation type.';
comment on column UNITIZE_ALLOC_LIST_TEMP.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_LIST_TEMP.ALLOC_TYPE is 'Indicates whether the unitization basis will be on Actual, Estimate, or Standards.';
comment on column UNITIZE_ALLOC_LIST_TEMP.ALLOC_BASIS is 'Defines the allocation methodology given by allocation type, ie dollars, quantity, material cost, etc.';


drop table UNITIZE_WO_SINGLE;

drop table UNITIZE_WO_LIST;

create table UNITIZE_WO_LIST
(
 COMPANY_ID              number(22,0) not null,
 WORK_ORDER_ID           number(22,0) not null,
 WORK_ORDER_NUMBER       varchar2(35) not null,
 UNITIZATION_TYPE        varchar2(35),
 DB_SESSION_ID           number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18)
);

alter table UNITIZE_WO_LIST
   add constraint PK_UNITIZE_WO_LIST
       primary key (WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_WO_LIST is '(C) [04] The table that stores the list of work order(s) being unitized.  Used to avoid concurrent unitization of the same work order.';
comment on column UNITIZE_WO_LIST.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_LIST.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_LIST.WORK_ORDER_NUMBER is 'User-defined number associated with a Work Order.  This is usually a reference to an external numbering system.';
comment on column UNITIZE_WO_LIST.UNITIZATION_TYPE is 'Unitization can be run in muliptlie ways:  AUTO, AUTO LATE, AUTO WO, MANUAL';
comment on column UNITIZE_WO_LIST.DB_SESSION_ID is 'System-assigned identifier of the database session.';
comment on column UNITIZE_WO_LIST.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_WO_LIST.USER_ID is 'Standard system-assigned user id used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1238, 0, 10, 4, 3, 0, 36876, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036876_cwip_fast101.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
