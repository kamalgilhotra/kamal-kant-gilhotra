/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_043577_jobserver_inc_20150415_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     04/15/2015 Paul Cordero    	 Changing varchar2s to CLOBs
||==========================================================================================
*/

set serveroutput on;

declare 
  doesTableColumnExistAs number := 0;
begin
	begin
	   
	   -- "PWRPLANT"."PP_JOB_EXECUTABLE"."ARGUMENTS" COLUMN
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 4000
		  and COLUMN_NAME = upper('ARGUMENTS')
		  and TABLE_NAME = upper('PP_JOB_EXECUTABLE') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column ARGUMENTS in PP_JOB_EXECUTABLE table to CLOB');
	
				-- CREATE NEW TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTABLE" 
				ADD ( "ARGUMENTS_TMP" CLOB )';

				-- COPY DATA FROM EXISTING VARCHAR2 COLUMN TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_EXECUTABLE" 
				SET ARGUMENTS_TMP = ARGUMENTS';
				execute immediate 'COMMIT';				

				-- DROP EXISTING VARCHAR2 COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTABLE" 
				DROP COLUMN ARGUMENTS';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTABLE" 
				RENAME COLUMN ARGUMENTS_TMP TO ARGUMENTS';			  
			end;
		  else
			begin
			  dbms_output.put_line('Column ARGUMENTS in PP_JOB_EXECUTABLE table is a CLOB');
			end;
		end if;
		
	   -- "PWRPLANT"."PP_JOB_EXECUTION_JOB"."ARGUMENTS" COLUMN
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 2000
		  and COLUMN_NAME = upper('ARGUMENTS')
		  and TABLE_NAME = upper('PP_JOB_EXECUTION_JOB') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column ARGUMENTS in PP_JOB_EXECUTION_JOB table to CLOB');
	
				-- CREATE NEW TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				ADD ( "ARGUMENTS_TMP" CLOB )';

				-- COPY DATA FROM EXISTING VARCHAR2 COLUMN TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				SET ARGUMENTS_TMP = ARGUMENTS';
				execute immediate 'COMMIT';				

				-- DROP EXISTING VARCHAR2 COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				DROP COLUMN ARGUMENTS';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				RENAME COLUMN ARGUMENTS_TMP TO ARGUMENTS';			  
			end;
		  else
			begin
			  dbms_output.put_line('Column ARGUMENTS in PP_JOB_EXECUTION_JOB table is a CLOB');
			end;
		end if;		
		
	   -- "PWRPLANT"."PP_JOB_EXECUTION_JOB"."DATA" COLUMN
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 4000
		  and COLUMN_NAME = upper('DATA')
		  and TABLE_NAME = upper('PP_JOB_EXECUTION_JOB') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column DATA in PP_JOB_EXECUTION_JOB table to CLOB');
	
				-- CREATE NEW TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				ADD ( "DATA_TMP" CLOB )';

				-- COPY DATA FROM EXISTING VARCHAR2 COLUMN TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				SET DATA_TMP = DATA';
				execute immediate 'COMMIT';				

				-- DROP EXISTING VARCHAR2 COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				DROP COLUMN DATA';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				RENAME COLUMN DATA_TMP TO DATA';			  
			end;
		  else
			begin
			  dbms_output.put_line('Column DATA in PP_JOB_EXECUTION_JOB table is a CLOB');
			end;
		end if;		
		
	   -- "PWRPLANT"."PP_JOB_EXECUTION"."VARIABLES" COLUMN
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 4000
		  and COLUMN_NAME = upper('VARIABLES')
		  and TABLE_NAME = upper('PP_JOB_EXECUTION') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column VARIABLES in PP_JOB_EXECUTION table to CLOB');
	
				-- CREATE NEW TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION" 
				ADD ( "VARIABLES_TMP" CLOB )';

				-- COPY VARIABLES FROM EXISTING VARCHAR2 COLUMN TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_EXECUTION" 
				SET VARIABLES_TMP = VARIABLES';
				execute immediate 'COMMIT';				

				-- DROP EXISTING VARCHAR2 COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION" 
				DROP COLUMN VARIABLES';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION" 
				RENAME COLUMN VARIABLES_TMP TO VARIABLES';			  
			end;
		  else
			begin
			  dbms_output.put_line('Column VARIABLES in PP_JOB_EXECUTION table is a CLOB');
			end;
		end if;		
		
	   -- "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."ARGUMENTS" COLUMN
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 2000
		  and COLUMN_NAME = upper('ARGUMENTS')
		  and TABLE_NAME = upper('PP_JOB_WORKFLOW_JOB') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column ARGUMENTS in PP_JOB_WORKFLOW_JOB table to CLOB');
	
				-- CREATE NEW TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				ADD ( "ARGUMENTS_TMP" CLOB )';

				-- COPY DATA FROM EXISTING VARCHAR2 COLUMN TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				SET ARGUMENTS_TMP = ARGUMENTS';
				execute immediate 'COMMIT';				

				-- DROP EXISTING VARCHAR2 COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				DROP COLUMN ARGUMENTS';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				RENAME COLUMN ARGUMENTS_TMP TO ARGUMENTS';			  
			end;
		  else
			begin
			  dbms_output.put_line('Column ARGUMENTS in PP_JOB_WORKFLOW_JOB table is a CLOB');
			end;
		end if;		
		
	   -- "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."ARGUMENTS_OBJECT" COLUMN
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 3000
		  and COLUMN_NAME = upper('ARGUMENTS_OBJECT')
		  and TABLE_NAME = upper('PP_JOB_WORKFLOW_JOB') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column ARGUMENTS_OBJECT in PP_JOB_WORKFLOW_JOB table to CLOB');
	
				-- CREATE NEW TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				ADD ( "ARGUMENTS_OBJECT_TMP" CLOB )';

				-- COPY DATA FROM EXISTING VARCHAR2 COLUMN TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				SET ARGUMENTS_OBJECT_TMP = ARGUMENTS_OBJECT';
				execute immediate 'COMMIT';				

				-- DROP EXISTING VARCHAR2 COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				DROP COLUMN ARGUMENTS_OBJECT';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				RENAME COLUMN ARGUMENTS_OBJECT_TMP TO ARGUMENTS_OBJECT';			  
			end;
		  else
			begin
			  dbms_output.put_line('Column ARGUMENTS_OBJECT in PP_JOB_WORKFLOW_JOB table is a CLOB');
			end;
		end if;			
		
	   -- "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."DATA" COLUMN
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 4000
		  and COLUMN_NAME = upper('DATA')
		  and TABLE_NAME = upper('PP_JOB_WORKFLOW_JOB') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column DATA in PP_JOB_WORKFLOW_JOB table to CLOB');
	
				-- CREATE NEW TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				ADD ( "DATA_TMP" CLOB )';

				-- COPY DATA FROM EXISTING VARCHAR2 COLUMN TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				SET DATA_TMP = DATA';
				execute immediate 'COMMIT';				

				-- DROP EXISTING VARCHAR2 COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				DROP COLUMN DATA';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
				RENAME COLUMN DATA_TMP TO DATA';			  
			end;
		  else
			begin
			  dbms_output.put_line('Column DATA in PP_JOB_WORKFLOW_JOB table is a CLOB');
			end;
		end if;		
		
	   -- "PWRPLANT"."PP_JOB_WORKFLOW"."VARIABLES" COLUMN
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 4000
		  and COLUMN_NAME = upper('VARIABLES')
		  and TABLE_NAME = upper('PP_JOB_WORKFLOW') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column VARIABLES in PP_JOB_WORKFLOW table to CLOB');
	
				-- CREATE NEW TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW" 
				ADD ( "VARIABLES_TMP" CLOB )';

				-- COPY VARIABLES FROM EXISTING VARCHAR2 COLUMN TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_WORKFLOW" 
				SET VARIABLES_TMP = VARIABLES';
				execute immediate 'COMMIT';				

				-- DROP EXISTING VARCHAR2 COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW" 
				DROP COLUMN VARIABLES';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW" 
				RENAME COLUMN VARIABLES_TMP TO VARIABLES';			  
			end;
		  else
			begin
			  dbms_output.put_line('Column VARIABLES in PP_JOB_WORKFLOW table is a CLOB');
			end;
		end if;		
		
	end;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2507, 0, 2015, 1, 0, 0, 043577, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043577_jobserver_inc_20150415_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;