/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_029595_pwrtax_og_own_pct.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 09/25/2014 Alex P.
||============================================================================
*/

create table COMPANY_PERCENT
(
 COMPANY_ID number(22, 0) not null,
 USER_ID    varchar2(18),
 TIME_STAMP date,
 PERCENT    number(22, 8)
);

alter table COMPANY_PERCENT
   add constraint COMPANY_PERCENT_PK
       primary key (COMPANY_ID)
       using index tablespace PWRPLANT_IDX;

comment on table COMPANY_PERCENT is '(S) [09] The company_percent table is used to represent what percentage of each company is owned by the operating company.  The default is 100%.';
comment on column COMPANY_PERCENT.COMPANY_ID is 'Standard system-assigned identifier.';
comment on column COMPANY_PERCENT.USER_ID is 'Standarad system-assigned timestamp used for audit purposes.';
comment on column COMPANY_PERCENT.TIME_STAMP is 'Standard system-assigned user id used for audit purposes.';
comment on column COMPANY_PERCENT.PERCENT is 'The percent of a given company owned by the operating company.';

create or replace view COMPANY_CONSOL_VIEW
(
 PP_TREE_CATEGORY_ID,
 CONSOLIDATION_ID,
 PARENT_ID,
 COMPANY_ID,
 DESCRIPTION,
 SORT_ORDER,
 PERCENT
)
as
select PP_TREE_CATEGORY_ID,
       CONSOLIDATION_ID,
       PARENT_ID,
       MAIN_VIEW.COMPANY_ID,
       DESCRIPTION,
       SORT_ORDER,
       NVL(COMPANY_PERCENT.PERCENT, 1) PERCENT
  from (select B.PP_TREE_CATEGORY_ID,
               B.PP_TREE_TYPE_ID CONSOLIDATION_ID,
               B.MAIN_CHILD PARENT_ID,
               B.CHILD_ID COMPANY_ID,
               NVL(A.OVERRIDE_PARENT_DESCRIPTION, NVL(COMPANY.DESCRIPTION, 'NO DESCRIPTION')) DESCRIPTION,
               B.SORT_ORDER
          from (select distinct PP_TREE_TYPE_ID, PARENT_ID, OVERRIDE_PARENT_DESCRIPTION
                  from (select PP_TREE_CATEGORY_ID,
                               PP_TREE_TYPE_ID,
                               PARENT_ID,
                               CHILD_ID,
                               EMPTY_NODE,
                               OVERRIDE_PARENT_DESCRIPTION,
                               NVL(SORT_ORDER, 0) SORT_ORDER
                          from PP_TREE
                         where PP_TREE_CATEGORY_ID = 1)
                 where OVERRIDE_PARENT_DESCRIPTION is not null) A,
               COMPANY COMPANY,
               (select A.PP_TREE_CATEGORY_ID,
                       A.PP_TREE_TYPE_ID,
                       A.PARENT_ID,
                       A.CHILD_ID,
                       level                 LEVEL_NUM,
                       CONNECT_BY_ROOT       A.CHILD_ID MAIN_CHILD,
                       A.EMPTY_NODE,
                       CONNECT_BY_ROOT       A.SORT_ORDER SORT_ORDER
                  from (select PP_TREE_CATEGORY_ID,
                               PP_TREE_TYPE_ID,
                               PARENT_ID,
                               CHILD_ID,
                               EMPTY_NODE,
                               OVERRIDE_PARENT_DESCRIPTION,
                               NVL(SORT_ORDER, 0) SORT_ORDER
                          from PP_TREE
                         where PP_TREE_CATEGORY_ID = 1) A
                 start with A.CHILD_ID is not null
                connect by NOCYCLE prior A.CHILD_ID = A.PARENT_ID
                       and prior A.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID) B,
               (select A.PP_TREE_CATEGORY_ID, A.PP_TREE_TYPE_ID, A.PARENT_ID, A.CHILD_ID, level LEVEL_NUM
                  from (select PP_TREE_CATEGORY_ID,
                               PP_TREE_TYPE_ID,
                               PARENT_ID,
                               CHILD_ID,
                               EMPTY_NODE,
                               OVERRIDE_PARENT_DESCRIPTION,
                               NVL(SORT_ORDER, 0) SORT_ORDER
                          from PP_TREE
                         where PP_TREE_CATEGORY_ID = 1) A
                 start with A.CHILD_ID = A.PARENT_ID
                connect by NOCYCLE prior A.CHILD_ID = A.PARENT_ID
                       and prior A.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID) C
         where B.MAIN_CHILD = A.PARENT_ID(+)
           and B.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID(+)
           and B.MAIN_CHILD = COMPANY.COMPANY_ID(+)
           and B.MAIN_CHILD = C.CHILD_ID
           and B.PP_TREE_TYPE_ID = C.PP_TREE_TYPE_ID
           and B.MAIN_CHILD in (select COMPANY_ID
                                  from COMPANY
                                union
                                select CHILD_ID
                                  from (select PP_TREE_CATEGORY_ID,
                                               PP_TREE_TYPE_ID,
                                               PARENT_ID,
                                               CHILD_ID,
                                               EMPTY_NODE,
                                               OVERRIDE_PARENT_DESCRIPTION,
                                               NVL(SORT_ORDER, 0) SORT_ORDER
                                          from PP_TREE
                                         where PP_TREE_CATEGORY_ID = 1) INSIDE
                                 where INSIDE.EMPTY_NODE = 1
                                   and B.PP_TREE_TYPE_ID = INSIDE.PP_TREE_TYPE_ID)
        union
        select 1 PP_TREE_CATEGORY_ID,
               -1 CONSOLIDATION_ID,
               COMPANY_ID PARENT_ID,
               COMPANY_ID COMPANY_ID,
               DESCRIPTION,
               0 SORT_ORDER
          from COMPANY COMPANY) MAIN_VIEW,
       COMPANY_PERCENT
 where MAIN_VIEW.COMPANY_ID = COMPANY_PERCENT.COMPANY_ID(+);


alter table TAX_COMPANY_CONSOL add PERCENT number(22, 8) not null;
comment on column TAX_COMPANY_CONSOL.PERCENT is 'The percent of a given company owned by the operating company.';


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1433, 0, 10, 4, 3, 0, 29595, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_029595_pwrtax_og_own_pct.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;