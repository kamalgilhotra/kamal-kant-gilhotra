/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052656_lessee_02_v_ls_asset_schedule_fx_gain_loss_change_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.3 11/08/2018 Crystal Yura     Do not Calc Gain/loss if contract=company currency
||============================================================================
*/
CREATE OR REPLACE FORCE VIEW V_LS_ASSET_SCHEDULE_FX_VW AS
WITH cur
       AS ( SELECT ls_currency_type_id AS ls_cur_type,
                   currency_id,
                   currency_display_symbol,
                   iso_code,
                   CASE ls_currency_type_id
                     WHEN 1 THEN 1
                     ELSE NULL
                   END                 AS contract_approval_rate
            FROM   CURRENCY
                   cross join LS_LEASE_CURRENCY_TYPE ),
       open_month
       AS ( SELECT  company_id,
                   Min( gl_posting_mo_yr ) open_month
            FROM   LS_PROCESS_CONTROL
            WHERE  open_next IS NULL
            GROUP  BY company_id ),
       calc_rate
       AS ( SELECT  Nvl(a.company_id, b.company_id) company_id,
                   Nvl(a.contract_currency_id, b.contract_currency_id) contract_currency_id,
                   Nvl(a.company_currency_id, b.company_currency_id) company_currency_id,
                   Nvl(a.accounting_month, Add_Months(b.accounting_month,1)) accounting_month,
                   nvl(a.exchange_rate_type_id, b.exchange_rate_type_id) exchange_rate_type_id,
                   Nvl(a.exchange_date, b.exchange_date) exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM ls_lease_calculated_date_rates a
              FULL OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
              AND a.contract_currency_id = b.contract_currency_id
              AND a.accounting_month = add_months(b.accounting_month,1)
              and b.exchange_rate_type_id = a.exchange_rate_type_id),
       cr_now
       AS ( SELECT  currency_from,
                   currency_to,
                   rate
            FROM   ( SELECT currency_from,
                            currency_to,
                            rate,
                            Row_number( )
                              over(
                                PARTITION BY currency_from, currency_to
                                ORDER BY exchange_date DESC ) AS rn
                     FROM   CURRENCY_RATE_DEFAULT_DENSE
                     WHERE  Trunc( exchange_date, 'MONTH' ) <= Trunc( SYSDATE, 'MONTH' ) AND
                            exchange_rate_type_id = 1 )
            WHERE  rn = 1 ),
       war
       AS ( SELECT  war.ilr_id,
                   war.revision,
                   war.set_of_books_id,
                   war.gross_weighted_avg_rate,
                   war.net_weighted_avg_rate,
                   lio.in_service_exchange_rate,
                   Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
            FROM ls_ilr_weighted_avg_rates war
                 JOIN ls_ilr ilr ON war.ilr_id = ilr.ilr_id
                 JOIN ls_ilr_options lio ON lio.ilr_id = war.ilr_id AND lio.revision = war.revision
           union
           select ilr.ilr_id,
                  lio.revision,
                  sob.set_of_books_id,
                  lio.in_service_exchange_rate,
                  lio.in_service_exchange_rate,
                  lio.in_service_exchange_rate,
                  Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
             from ls_ilr ilr
             join ls_ilr_options lio on lio.ilr_id = ilr.ilr_id
             cross join set_of_books sob
             where (ilr.ilr_id, lio.revision) not in (select ilr_id, revision from ls_ilr_weighted_avg_rates)
           ),
      ilr_dates AS (
        SELECT  lio.ilr_id, lio.revision,
        Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month,
        lio.in_service_exchange_rate
        FROM ls_ilr_options lio
          JOIN ls_ilr ilr ON ilr.ilr_id = lio.ilr_id
      ),
      orig_in_svc AS (
        select ilr_id, revision, effective_month, in_service_exchange_rate
          from ilr_dates
         where(ilr_id, revision) in ( 
          select ilr_id, min(revision) over (partition by ilr_id order by approval_date) revision 
            from ls_ilr_approval
           where approval_status_id <> 1)
       )
  SELECT las.ilr_id                                                                                                                                               ilr_id,
         las.ls_asset_id                                                                                                                                          ls_asset_id,
         las.revision                                                                                                                                             revision,
         las.set_of_books_id                                                                                                                                      set_of_books_id,
         las.month                                                                                                                                                month,
         OPEN_MONTH.company_id                                                                                                                                    company_id,
         OPEN_MONTH.open_month                                                                                                                                    open_month,
         CUR.ls_cur_type                                                                                                                                          ls_cur_type,
         cr.exchange_date                                                                                                                                         exchange_date,
         CALC_RATE.exchange_date                                                                                                                                  prev_exchange_date,
         las.contract_currency_id                                                                                                                                 contract_currency_id,
         CUR.currency_id                                                                                                                                          currency_id,
         cr.rate                                                                                                                                                  rate,
         CALC_RATE.rate                                                                                                                                           calculated_rate,
         CALC_RATE.prev_rate                                                                                                                                      previous_calculated_rate,
         CALC_AVG_RATE.rate                                                                                                                                       calculated_average_rate,
         CALC_AVG_RATE.prev_rate                                                                                                                                  previous_calculated_avg_rate,
         Decode(lower(sc.CONTROL_VALUE), 'yes', CALC_AVG_RATE.rate, CALC_RATE.rate)                                                                               average_rate,
         las.effective_in_svc_rate,
         original_in_svc_rate,
         CUR.iso_code                                                                                                                                             iso_code,
         CUR.currency_display_symbol                                                                                                                              currency_display_symbol,
         las.residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     residual_amount,
         las.term_penalty * Nvl( CALC_RATE.rate, cr.rate )                                                                                                        term_penalty,
         las.bpo_price * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      bpo_price,
         las.beg_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, war2.gross_weighted_avg_rate), CR_NOW.rate )                                      beg_capital_cost,
         las.end_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                          then war2.gross_weighted_avg_rate
                                                                          else war.gross_weighted_avg_rate
                                                                     end), CR_NOW.rate )                                            end_capital_cost,
         las.beg_obligation * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                            beg_obligation,
         las.end_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 end_obligation,
         las.beg_lt_obligation * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                         beg_lt_obligation,
         las.end_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                              end_lt_obligation,
         las.principal_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate )                                       principal_remeasurement,
         las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                                  beg_liability,
         las.end_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  end_liability,
         las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                               beg_lt_liability,
         las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                               end_lt_liability,
         las.liability_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate )                                       liability_remeasurement,
         las.interest_accrual      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  interest_accrual,
         las.principal_accrual     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  principal_accrual,
         las.interest_paid         * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  interest_paid,
         las.principal_paid        * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  principal_paid,
         las.executory_accrual1    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual1,
         las.executory_accrual2    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual2,
         las.executory_accrual3    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual3,
         las.executory_accrual4    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual4,
         las.executory_accrual5    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual5,
         las.executory_accrual6    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual6,
         las.executory_accrual7    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual7,
         las.executory_accrual8    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual8,
         las.executory_accrual9    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual9,
         las.executory_accrual10   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual10,
         las.executory_paid1       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid1,
         las.executory_paid2       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid2,
         las.executory_paid3       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid3,
         las.executory_paid4       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid4,
         las.executory_paid5       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid5,
         las.executory_paid6       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid6,
         las.executory_paid7       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid7,
         las.executory_paid8       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid8,
         las.executory_paid9       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid9,
         las.executory_paid10      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid10,
         las.contingent_accrual1   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual1,
         las.contingent_accrual2   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual2,
         las.contingent_accrual3   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual3,
         las.contingent_accrual4   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual4,
         las.contingent_accrual5   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual5,
         las.contingent_accrual6   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual6,
         las.contingent_accrual7   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual7,
         las.contingent_accrual8   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual8,
         las.contingent_accrual9   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual9,
         las.contingent_accrual10  * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual10,
         las.contingent_paid1      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid1,
         las.contingent_paid2      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid2,
         las.contingent_paid3      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid3,
         las.contingent_paid4      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid4,
         las.contingent_paid5      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid5,
         las.contingent_paid6      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid6,
         las.contingent_paid7      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid7,
         las.contingent_paid8      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid8,
         las.contingent_paid9      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid9,
         las.contingent_paid10     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid10,
         las.current_lease_cost    * Nvl( Nvl( CUR.contract_approval_rate, original_in_svc_rate), CR_NOW.rate ) current_lease_cost,
         las.beg_deferred_rent     * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_deferred_rent,
         las.deferred_rent         * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  deferred_rent,
         las.end_deferred_rent     * Nvl( CALC_RATE.rate, cr.rate )                                                                         end_deferred_rent,
         las.beg_st_deferred_rent  * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                              beg_st_deferred_rent,
         las.end_st_deferred_rent  * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_st_deferred_rent,
         las.initial_direct_cost   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  initial_direct_cost,
         las.incentive_amount      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  incentive_amount,
         las.depr_expense          * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         depr_expense,
         las.begin_reserve         * Nvl( Nvl( CUR.contract_approval_rate, war2.net_weighted_avg_rate), CR_NOW.rate )                         begin_reserve,
         las.end_reserve           * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         end_reserve,
         las.depr_exp_alloc_adjust * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         depr_exp_alloc_adjust,
         las.asset_description                                                                                                                   asset_description,
         las.leased_asset_number                                                                                                                 leased_asset_number,
         las.fmv * Nvl( Nvl( CUR.contract_approval_rate, original_in_svc_rate), CR_NOW.rate )                                                    fmv,
         las.is_om                                                                                                                               is_om,
         las.approved_revision                                                                                                                   approved_revision,
         las.lease_cap_type_id                                                                                                                   lease_cap_type_id,
         las.ls_asset_status_id                                                                                                                  ls_asset_status_id,
         las.retirement_date                                                                                                                     retirement_date,
         las.beg_prepaid_rent      * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_prepaid_rent,
         las.prepay_amortization   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  prepay_amortization,
         las.prepaid_rent          * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  prepaid_rent,
         las.end_prepaid_rent      * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_prepaid_rent,
         Nvl(las.beg_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, war2.net_weighted_avg_rate), CR_NOW.rate ), 0)                  beg_net_rou_asset,
         Nvl(las.end_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate ), 0)                        end_net_rou_asset,
         las.actual_residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                             actual_residual_amount,
         las.guaranteed_residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                         guaranteed_residual_amount,
         (las.fmv * las.estimated_residual) * Nvl( CALC_RATE.rate, cr.rate )                                                                     estimated_residual,
         nvl(las.rou_asset_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ),0)                    rou_asset_remeasurement,
         nvl(las.PARTIAL_TERM_GAIN_LOSS * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ),0)                     partial_term_gain_loss,
         las.beg_arrears_accrual * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                           beg_arrears_accrual,
         las.arrears_accrual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))    arrears_accrual,
         las.end_arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                end_arrears_accrual,
         nvl(las.EXECUTORY_ADJUST * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )),0)   executory_adjust,
         nvl(las.CONTINGENT_ADJUST * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )),0)  contingent_adjust,
        case when las.is_om = 1 then
          0
        when las.contract_currency_id = cs.currency_id then 0
        else
          (
            ((las.end_liability * Nvl( CALC_RATE.rate, cr.rate )) - (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )))
            - ((las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )) - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )))
            - (las.interest_accrual *  Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (las.interest_paid * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (las.principal_paid * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            - (las.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            - (las.st_obligation_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ))
            + (las.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            + (las.unaccrued_interest_remeasure * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ))
          )
        end st_currency_gain_loss,
        case when las.is_om = 1 then
          0
        when las.contract_currency_id = cs.currency_id then 0
        else
          (
              (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate ))
            - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate ))
            + ((las.beg_lt_liability - las.end_lt_liability) * NVL( CALC_RATE.rate, cr.rate))
          )
        end lt_currency_gain_loss,
        case when las.is_om = 1 then
          0
        when las.contract_currency_id = cs.currency_id then 0
        else
          (
            ((las.end_liability * Nvl( CALC_RATE.rate, cr.rate )) - (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )))
            - ((las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )) - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )))
            - (las.interest_accrual *  Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (las.interest_paid * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (las.principal_paid * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            - (las.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            - (las.st_obligation_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ))
            + (las.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            + (las.unaccrued_interest_remeasure * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ))
          ) +
          (
              (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate ))
            - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate ))
            + ((las.beg_lt_liability - las.end_lt_liability) * NVL( CALC_RATE.rate, cr.rate))
          )
        end gain_loss_fx,
        las.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ) obligation_reclass,
        las.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ) unaccrued_interest_reclass,
        las.BEGIN_ACCUM_IMPAIR * Nvl( Nvl( CUR.contract_approval_rate, war2.net_weighted_avg_rate), CR_NOW.rate ) BEGIN_ACCUM_IMPAIR,
        las.IMPAIRMENT_ACTIVITY * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )) IMPAIRMENT_ACTIVITY,
        las.END_ACCUM_IMPAIR * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate ) END_ACCUM_IMPAIR 
  FROM   ( SELECT la.ilr_id,
                  las.ls_asset_id,
                  las.revision,
                  las.set_of_books_id,
                  las.month,
                  la.contract_currency_id,
                  las.residual_amount,
                  las.term_penalty,
                  las.bpo_price,
                  las.beg_capital_cost,
                  las.end_capital_cost,
                  las.beg_obligation,
                  las.end_obligation,
                  las.beg_lt_obligation,
                  las.end_lt_obligation,
                  las.principal_remeasurement,
                  las.beg_liability,
                  las.end_liability,
                  las.beg_lt_liability,
                  las.end_lt_liability,
                  las.liability_remeasurement,
                  las.interest_accrual,
                  las.principal_accrual,
                  las.interest_paid,
                  las.principal_paid,
                  las.executory_accrual1,
                  las.executory_accrual2,
                  las.executory_accrual3,
                  las.executory_accrual4,
                  las.executory_accrual5,
                  las.executory_accrual6,
                  las.executory_accrual7,
                  las.executory_accrual8,
                  las.executory_accrual9,
                  las.executory_accrual10,
                  las.executory_paid1,
                  las.executory_paid2,
                  las.executory_paid3,
                  las.executory_paid4,
                  las.executory_paid5,
                  las.executory_paid6,
                  las.executory_paid7,
                  las.executory_paid8,
                  las.executory_paid9,
                  las.executory_paid10,
                  las.contingent_accrual1,
                  las.contingent_accrual2,
                  las.contingent_accrual3,
                  las.contingent_accrual4,
                  las.contingent_accrual5,
                  las.contingent_accrual6,
                  las.contingent_accrual7,
                  las.contingent_accrual8,
                  las.contingent_accrual9,
                  las.contingent_accrual10,
                  las.contingent_paid1,
                  las.contingent_paid2,
                  las.contingent_paid3,
                  las.contingent_paid4,
                  las.contingent_paid5,
                  las.contingent_paid6,
                  las.contingent_paid7,
                  las.contingent_paid8,
                  las.contingent_paid9,
                  las.contingent_paid10,
                  las.current_lease_cost,
                  las.beg_deferred_rent,
                  las.deferred_rent,
                  las.end_deferred_rent,
                  las.beg_st_deferred_rent,
                  las.end_st_deferred_rent,
                  las.initial_direct_cost,
                  las.incentive_amount,
                  ldf.depr_expense,
                  ldf.begin_reserve,
                  ldf.end_reserve,
                  ldf.depr_exp_alloc_adjust,
                  la.company_id,
                  opt.in_service_exchange_rate,
                  la.description AS asset_description,
                  la.leased_asset_number,
                  la.fmv,
                  las.is_om,
                  la.approved_revision,
                  opt.lease_cap_type_id,
                  la.ls_asset_status_id,
                  la.retirement_date,
                  las.beg_prepaid_rent,
                  las.prepay_amortization,
                  las.prepaid_rent,
                  las.end_prepaid_rent,
                  las.beg_net_rou_asset,
                  las.end_net_rou_asset,
                  la.actual_residual_amount,
                  la.guaranteed_residual_amount,
                  la.estimated_residual,
                  las.rou_asset_remeasurement,
                  las.partial_term_gain_loss,
                  las.beg_arrears_accrual,
                  las.arrears_accrual,
                  las.end_arrears_accrual,
                  las.additional_rou_asset,
                  las.executory_adjust,
                  las.contingent_adjust,
                  ilr_dates.in_service_exchange_rate AS effective_in_svc_rate,
                  orig_in_svc.in_service_exchange_rate AS original_in_svc_rate,
                  opt.remeasurement_date remeasurement_date,
                  nvl(las.obligation_reclass,0) obligation_reclass,
                  nvl(las.st_obligation_remeasurement,0) st_obligation_remeasurement,
                  nvl(las.unaccrued_interest_reclass,0) unaccrued_interest_reclass,
                  nvl(las.unaccrued_interest_remeasure,0) unaccrued_interest_remeasure,
                  nvl(las.begin_accum_impair,0) begin_accum_impair,
                  nvl(las.impairment_activity,0) impairment_activity,
                  nvl(las.end_accum_impair,0) end_accum_impair
           FROM   LS_ASSET_SCHEDULE las
                  JOIN LS_ASSET la ON las.ls_asset_id = la.ls_asset_id
                  JOIN LS_ILR_OPTIONS opt ON las.revision = opt.revision AND la.ilr_id = opt.ilr_id
                  left OUTER JOIN LS_DEPR_FORECAST ldf ON las.ls_asset_id = ldf.ls_asset_id
                                                        AND las.revision = ldf.revision
                                                        AND las.set_of_books_id = ldf.set_of_books_id
                                                        AND las.month = ldf.month
                  JOIN ilr_dates ON la.ilr_id = ilr_dates.ilr_id
                                                      AND las.revision = ilr_dates.revision
                  left outer join orig_in_svc on la.ilr_id = orig_in_svc.ilr_id and las.revision = orig_in_svc.revision
                   ) las
         inner join CURRENCY_SCHEMA cs
                 ON las.company_id = cs.company_id
         inner join cur
                 ON CUR.currency_id = CASE CUR.ls_cur_type
                                        WHEN 1 THEN las.contract_currency_id
                                        WHEN 2 THEN cs.currency_id
                                        ELSE NULL
                                      END
         inner join open_month
                 ON las.company_id = OPEN_MONTH.company_id
         inner join CURRENCY_RATE_DEFAULT_DENSE cr
                 ON CUR.currency_id = cr.currency_to AND
                    las.contract_currency_id = cr.currency_from AND
                    Trunc( cr.exchange_date, 'MONTH' ) = Trunc( las.month, 'MONTH' )
         inner join cr_now
                 ON CUR.currency_id = CR_NOW.currency_to AND
                    las.contract_currency_id = CR_NOW.currency_from
         left outer join calc_rate
                      ON las.contract_currency_id = CALC_RATE.contract_currency_id AND
                         CUR.currency_id = CALC_RATE.company_currency_id AND
                         las.company_id = CALC_RATE.company_id AND
                         las.month = CALC_RATE.accounting_month
                         and calc_rate.exchange_rate_type_id = 1
         left outer join calc_rate calc_avg_rate
                 ON las.contract_currency_id = calc_avg_rate.contract_currency_id AND
                    CUR.currency_id = calc_avg_rate.company_currency_id AND
                    las.company_id = calc_avg_rate.company_id AND
                    las.month = calc_avg_rate.accounting_month 
                    and calc_avg_rate.exchange_rate_type_id = 4
         inner join pp_system_control_companies sc
                 ON open_month.company_id = sc.company_id AND
                    lower(trim(sc.control_name)) = 'lease mc: use average rates'
         join war war2 on las.ilr_id = war2.ilr_id
                      and las.set_of_books_id = war2.set_of_books_id
                      and las.month >= war2.effective_month
                      and war2.revision = case when las.month <= nvl(las.remeasurement_date, las.month)
                                              then case when las.revision - 1 <= 0 then 1 else las.revision - 1 end
                                              else las.revision
                                              end	
         join war   on las.ilr_id = war.ilr_id
                   and las.set_of_books_id = war.set_of_books_id
                   and las.revision = war.revision
         inner join CURRENCY_RATE_DEFAULT_DENSE cr_avg
                 ON CUR.currency_id = cr_avg.currency_to AND
                    las.contract_currency_id = cr_avg.currency_from AND
                    Trunc( cr_avg.exchange_date, 'MONTH' ) = Trunc( las.month, 'MONTH' )
                    AND cr_avg.exchange_rate_type_id = Decode(lower(sc.CONTROL_VALUE), 'yes', 4, 1)
  WHERE  cs.currency_type_id = 1 AND
         cr.exchange_rate_type_id = 1 ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(11447, 0, 2017, 4, 0, 3, 52656, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052656_lessee_02_v_ls_asset_schedule_fx_gain_loss_change_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;