/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011368_reimb_je_layout.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   02/01/2013 Brandon Beck   Point Release
||============================================================================
*/

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('pp_reimb_journal_layouts', null, null, 'spl*', 'Reimb Journal Layouts',
    'Reimbursable Accounting', 'reimb', null, 'w_pp_reimb_journal_layouts', 'reimb', null, null,
    null, null, null, null, null, null, null, null, null, null, null, null, null, null);

insert into PP_TABLE_GROUPS (TABLE_NAME, GROUPS) values ('pp_reimb_journal_layouts', 'system');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (282, 0, 10, 4, 0, 0, 11368, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011368_reimb_je_layout.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
