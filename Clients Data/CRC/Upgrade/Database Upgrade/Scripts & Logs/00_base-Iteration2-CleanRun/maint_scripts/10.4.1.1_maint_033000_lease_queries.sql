/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033000_lease_queries.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/02/2013 Ryan Oliveria  Interim Interest
||============================================================================
*/

delete from PP_ANY_QUERY_CRITERIA_FIELDS
 where ID in (select ID
                from PP_ANY_QUERY_CRITERIA
               where DESCRIPTION in ('ILR by Set of Books (CURRENT Revision)'));

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, DEFAULT_VALUE,
    COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE, QUANTITY_FIELD,
    DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select (select ID
             from PP_ANY_QUERY_CRITERIA
            where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)'),
          DETAIL_FIELD,
          COLUMN_ORDER,
          AMOUNT_FIELD,
          INCLUDE_IN_SELECT_CRITERIA,
          DEFAULT_VALUE,
          COLUMN_HEADER,
          COLUMN_WIDTH,
          DISPLAY_FIELD,
          DISPLAY_TABLE,
          COLUMN_TYPE,
          QUANTITY_FIELD,
          DATA_FIELD,
          REQUIRED_FILTER,
          REQUIRED_ONE_MULT,
          SORT_COL
     from PP_ANY_QUERY_CRITERIA_FIELDS
    where ID = (select ID
                  from PP_ANY_QUERY_CRITERIA
                 where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)');

update PP_ANY_QUERY_CRITERIA
   set SOURCE_ID = 1001, CRITERIA_FIELD = 'none', TABLE_NAME = 'Dynamic View',
       DESCRIPTION = 'Future Minimum Lease Payments', FEEDER_FIELD = 'none', SUBSYSTEM = 'lessee',
       QUERY_TYPE = 'user',
       sql = 'select    (select s.description from set_of_books s where s.set_of_books_id = sch2.set_of_books_id) as set_of_books,    c.description company_description,
       (select lg.description from ls_lease_group lg where mla.lease_group_id = lg.lease_group_id) as lease_group,    (select ls.description from ls_lessor ls where ls.lessor_id = mla.lessor_id)
       as lessor,    mla.lease_number as lease_number, mla.description as lease_description,    case when mla.pre_payment_sw = 1 then ''Prepay'' else ''Arrears'' end as prepay,
       (select ig.description from ls_ilr_group ig where ilr.ilr_group_id = ig.ilr_group_id) as ilr_group,    ilr.ilr_number as ilr_number,
       (select lct.description from ls_lease_cap_type lct where lct.ls_lease_cap_type_id = ilro.lease_cap_type_id) as capitalization_type,
       la.description as asset_description, la.serial_number as serial_number,    al.ext_asset_location as ext_asset_location, al.long_description as asset_loc_description,
       (select ua.description from utility_account ua where ua.utility_account_id = la.utility_account_id and ua.bus_segment_id = la.bus_segment_id) as utility_account,
       (select ru.description from retirement_unit ru where ru.retirement_unit_id = la.retirement_unit_id) as retirement_unit,    nvl(td.county_id, al.county_id) as county,
       nvl(td.state_id, al.state_id) as state,    td.tax_district_code as tax_district_code,    case when sch2.the_row = 1 then la.fmv else 0 end as fair_market_value,
       sch2.month_number as month_number,    case when sch2.the_row = 1 then sch2.beg_capital_cost else 0 end as beg_capital_cost,    nvl(sch2.residual_amount, 0) as residual_amount,
       nvl(sch2.termination_penalty_amount, 0) as termination_penalty_amount,    nvl(sch2.bargain_purchase_amount, 0) as bargain_purchase_amount,    nvl(sch2.beg_obligation, 0) as beg_obligation,
       nvl(sch2.beg_lt_obligation, 0) as beg_lt_obligation,    nvl(sch2.interest_accrual, 0) as interest_accrual, nvl(sch2.principal_accrual, 0) as principal_accrual,
       nvl(sch2.interest_paid, 0) as interest_paid, nvl(sch2.principal_paid, 0) as principal_paid from    ls_lease mla, ls_lease_company lsc,    ls_ilr ilr, ls_ilr_options ilro,    ls_asset la,    (       select row_number() over( partition by sch.ls_asset_id, sch.revision, sch.set_of_books_id order by sch.month ) as the_row,          sch.set_of_books_id, sch.ls_asset_id, sch.revision,          to_number(to_char(sch.month, ''yyyymm'')) as month_number,          sch.residual_amount as residual_amount, sch.term_penalty as termination_penalty_amount,          sch.bpo_price as bargain_purchase_amount, sch.beg_capital_cost as beg_capital_cost,          sch.beg_obligation as beg_obligation, sch.beg_lt_obligation as beg_lt_obligation,          sch.interest_accrual as interest_accrual, sch.principal_accrual as principal_accrual,          sch.interest_paid as interest_paid, sch.principal_paid as principal_paid       from ls_asset_schedule sch    ) sch2,    company c,    asset_location al, prop_tax_district td where mla.lease_id = lsc.lease_id and lsc.company_id = c.company_id and lsc.lease_id = ilr.lease_id (+) and lsc.company_id = ilr.company_id (+) and ilr.ilr_id = la.ilr_id (+) and ilr.ilr_id = ilro.ilr_id (+) and ilr.current_revision = ilro.revision (+) and la.ls_asset_id = sch2.ls_asset_id and la.approved_revision = sch2.revision and la.asset_location_id = al.asset_location_id (+) and al.tax_district_id = td.tax_district_id (+) order by 1, 2, 3, 5, 8, 9, 11, 21',
       SQL2 = null, SQL3 = null, SQL4 = null, SQL5 = null, SQL6 = null
 where DESCRIPTION in ('Future Minimum Lease Payments');

delete from PP_ANY_QUERY_CRITERIA_FIELDS
 where ID in
       (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments'));

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'asset_description', 11, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0,
    1, null, 'Asset Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'asset_loc_description', 14, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    0, 1, null, 'Asset Location', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'bargain_purchase_amount', 25, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 1, 1, null, 'Bargain Purchase Amount', 300, null, null, 'NUMBER', 0, null, null,
    null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'beg_capital_cost', 22, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1,
    1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'beg_lt_obligation', 27, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1,
    1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'beg_obligation', 26, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1,
    null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'capitalization_type', 10, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    0, 1, null, 'Capitalization Type', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'company_description', 2, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0,
    1, null, 'Company', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'county', 17, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null,
    'County', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'ext_asset_location', 13, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0,
    1, null, 'Ext Asset Location', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'fair_market_value', 20, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1,
    1, null, 'Fair Market Value', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'ilr_group', 8, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null,
    'ILR Group', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'ilr_number', 9, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null,
    'ILR Number', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'interest_accrual', 28, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1,
    1, null, 'Interest Accrual', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'interest_paid', 30, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1,
    null, 'Interest Paid', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'lease_description', 6, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0,
    1, null, 'Lease Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'lease_group', 3, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1,
    null, 'Lease Group', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'lease_number', 5, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1,
    null, 'Lease Number', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'lessor', 4, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null,
    'Lessor', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'month_number', 21, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1,
    null, 'Month Number', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'prepay', 7, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null,
    'Prepay', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'principal_accrual', 29, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1,
    1, null, 'Principal Accrual', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'principal_paid', 31, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1,
    null, 'Principal Paid', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'residual_amount', 23, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1,
    null, 'Residual Amount', 300, null, null, 'NUMBER', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'retirement_unit', 16, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1,
    null, 'Retirement Unit', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'serial_number', 12, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1,
    null, 'Serial Number', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'set_of_books', 1, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1,
    null, 'Set Of Books', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'state', 18, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null,
    'State', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'tax_district_code', 19, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0,
    1, null, 'Tax District Code', 300, null, null, 'VARCHAR2', 0, null, null, null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'termination_penalty_amount', 24, TO_DATE('2013-10-03 14:13:09', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 1, 1, null, 'Termination Penalty Amount', 300, null, null, 'NUMBER', 0, null, null,
    null, null);
insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
values
   ((select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION in ('Future Minimum Lease Payments')),
    'utility_account', 15, TO_DATE('2013-10-03 14:08:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1,
    null, 'Utility Account', 300, null, null, 'VARCHAR2', 0, null, null, null, null);

update PP_ANY_QUERY_CRITERIA_FIELDS
   set AMOUNT_FIELD = 0, QUANTITY_FIELD = 1
 where ID in (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION like 'ILR by Set of Books%')
   and LOWER(DETAIL_FIELD) = 'number_of_assets';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (652, 0, 10, 4, 1, 1, 33000, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033000_lease_queries.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
