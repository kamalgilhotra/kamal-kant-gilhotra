/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_scripts\maint_009097_tax_exp.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   12/12/2011 Elhadj Bah     Point Release
||============================================================================
*/

--** Maint 9097 -- Create a new column to hold the tax_status_default_capital status.
--                 Will need to update w_tax_expense_control to be able to specify this value.
--                 Also think about what priority this should get since we probably don't
--                 want it overriding manually assigned statuses

alter table WO_TAX_EXPENSE_TEST add TAX_STATUS_DEFAULT_CAPITAL number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (68, 0, 10, 3, 3, 0, 9097, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_009097_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

