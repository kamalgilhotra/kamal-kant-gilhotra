/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052193_lessee_01_lease_group_security_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/12/2018 K Powers       Add lease group security to payment wksp
||============================================================================
*/

CREATE TABLE LS_LEASE_GROUP_SECURITY
(LEASE_GROUP_ID   NUMBER(22,0) NOT NULL,
LEASE_USER_ID     VARCHAR2(18) NOT NULL,
USER_ID           VARCHAR2(18),
TIME_STAMP        date) ;

ALTER TABLE LS_LEASE_GROUP_SECURITY 
ADD CONSTRAINT PK_LS_LEASE_GROUP_SECURITY 
PRIMARY KEY (LEASE_GROUP_ID,LEASE_USER_ID) using index tablespace pwrplant_idx;

ALTER TABLE LS_LEASE_GROUP_SECURITY 
ADD CONSTRAINT FK_LS_LEASE_GROUP_SECURITY_1 
  FOREIGN KEY (LEASE_GROUP_ID)
  REFERENCES LS_LEASE_GROUP(LEASE_GROUP_ID);
  
ALTER TABLE LS_LEASE_GROUP_SECURITY 
ADD CONSTRAINT FK_LS_LEASE_GROUP_SECURITY_2
  FOREIGN KEY (LEASE_USER_ID)
  REFERENCES PP_SECURITY_USERS(USERS);
  
  -- table comment
COMMENT ON TABLE LS_LEASE_GROUP_SECURITY IS 'Restricts access to Payments and Invoices by User and Lease Group.';
COMMENT ON COLUMN LS_LEASE_GROUP_SECURITY.LEASE_GROUP_ID IS 'A number representing a Lease Group.';
COMMENT ON COLUMN LS_LEASE_GROUP_SECURITY.LEASE_USER_ID IS 'A user ID allowed to access the Lease Group Payments and Invoices';
COMMENT ON COLUMN LS_LEASE_GROUP_SECURITY.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN LS_LEASE_GROUP_SECURITY.user_id IS 'Standard system-assigned user id used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(10462, 0, 2018, 1, 0, 0, 52193, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052193_lessee_01_lease_group_security_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;