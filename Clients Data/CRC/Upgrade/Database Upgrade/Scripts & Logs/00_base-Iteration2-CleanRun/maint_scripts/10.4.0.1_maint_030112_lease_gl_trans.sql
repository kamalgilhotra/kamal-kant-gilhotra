/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030112_lease_gl_trans.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.1   06/05/2013 Travis Nemes   Point Release
||============================================================================
*/

alter table LS_DISTRIBUTION_TYPE add ENABLED_US_OPER number(1,0);
alter table LS_DISTRIBUTION_TYPE add ENABLED_GAAP_CAP number(1,0);
alter table LS_DISTRIBUTION_TYPE add ENABLED_FERC_CAP number(1,0);
alter table LS_DISTRIBUTION_TYPE add ENABLED_IFRS_OPER number(1,0);
alter table LS_DISTRIBUTION_TYPE add ENABLED_IFRS_CAP number(1,0);
alter table LS_DISTRIBUTION_TYPE add ENABLED_CUSTOM number(1,0);

alter table LS_DISTRIBUTION_TYPE
   add constraint CHECK_EMBED_US_OPER
       check (ENABLED_US_OPER IN (0,1));

alter table LS_DISTRIBUTION_TYPE
   add constraint CHECK_EMBED_GAAP_CAP
       check (ENABLED_GAAP_CAP IN (0,1));

alter table LS_DISTRIBUTION_TYPE
   add constraint CHECK_EMBED_FERC_CAP
       check (ENABLED_FERC_CAP IN (0,1));

alter table LS_DISTRIBUTION_TYPE
   add constraint CHECK_EMBED_IFRS_CAP
       check (ENABLED_IFRS_CAP IN (0,1));

alter table LS_DISTRIBUTION_TYPE
   add constraint CHECK_EMBED_IFRS_OPER
       check (ENABLED_IFRS_OPER IN (0,1));

alter table LS_DISTRIBUTION_TYPE
   add constraint CHECK_EMBED_CUSTOM
       check (ENABLED_CUSTOM IN (0,1));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (398, 0, 10, 4, 0, 1, 30112, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_030112_lease_gl_trans.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

commit;