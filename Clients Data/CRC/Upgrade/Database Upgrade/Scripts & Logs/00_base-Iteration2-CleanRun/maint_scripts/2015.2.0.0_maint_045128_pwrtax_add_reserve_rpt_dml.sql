/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045128_pwrtax_add_reserve_rpt_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2   10/23/2015 Betsy Calender Added Reserve Report to Ptax Base
 ||============================================================================
 */ 

insert into pp_reports (REPORT_TYPE,
SPECIAL_NOTE,
DESCRIPTION,
USER_ID,
DYNAMIC_DW,
OLD_REPORT_NUMBER,
TIME_OPTION,
DATAWINDOW,
DOCUMENTATION,
PP_REPORT_ENVIR_ID,
SUBSYSTEM,
PP_REPORT_FILTER_ID,
FILTER_OPTION,
PP_REPORT_TIME_OPTION_ID,
STATUS,
LONG_DESCRIPTION,
PP_REPORT_NUMBER,
TIME_STAMP,
PP_REPORT_STATUS_ID,
PP_REPORT_SUBSYSTEM_ID,
REPORT_NUMBER,
TURN_OFF_MULTI_THREAD,
LAST_APPROVED_DATE,
USER_COMMENT,
REPORT_TYPE_ID,
INPUT_WINDOW,
REPORT_ID)
select REPORT_TYPE,
SPECIAL_NOTE,
'Depreciation Reserve Report' DESCRIPTION,
null USER_ID,
DYNAMIC_DW,
OLD_REPORT_NUMBER,
TIME_OPTION,
'dw_tax_rpt_depr_reserve_rollfwd' DATAWINDOW,
DOCUMENTATION,
PP_REPORT_ENVIR_ID,
SUBSYSTEM,
PP_REPORT_FILTER_ID,
FILTER_OPTION,
PP_REPORT_TIME_OPTION_ID,
STATUS,
'Displays Reserve Activity by Vintage / Tax Class' LONG_DESCRIPTION,
PP_REPORT_NUMBER,
null TIME_STAMP,
PP_REPORT_STATUS_ID,
PP_REPORT_SUBSYSTEM_ID,
'PwrTax - 020' REPORT_NUMBER,
TURN_OFF_MULTI_THREAD,
LAST_APPROVED_DATE,
USER_COMMENT,
REPORT_TYPE_ID,
INPUT_WINDOW,
403050 REPORT_ID
from pp_Reports where report_id=403006;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2937, 0, 2015, 2, 0, 0, 45128, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045128_pwrtax_add_reserve_rpt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;