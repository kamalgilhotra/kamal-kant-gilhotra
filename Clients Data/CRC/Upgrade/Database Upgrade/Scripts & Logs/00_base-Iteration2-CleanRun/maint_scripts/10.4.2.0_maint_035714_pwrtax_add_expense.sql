SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035714_pwrtax_add_expense.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/29/2014 Ron Ferentini
||          04/02/2014 Lee Quinn      Renamed from maint_035714_taxrpr_add_expense.sql
||                                    to maint_035714_pwrtax_add_expense.sql
||============================================================================
*/

declare
   LV_SQL varchar2(2000);

begin
   LV_SQL := 'create table TAX_REPAIRS_ADD_EXPENSE_TEMP as select * from TAX_REPAIRS_ADD_EXPENSE';
   execute immediate LV_SQL;
   DBMS_OUTPUT.PUT_LINE(LV_SQL || ';');

   LV_SQL := 'delete from TAX_REPAIRS_ADD_EXPENSE';
   execute immediate LV_SQL;
   DBMS_OUTPUT.PUT_LINE(LV_SQL || ';');

   LV_SQL := 'alter table TAX_REPAIRS_ADD_EXPENSE modify TAX_YEAR number(22, 2)';
   execute immediate LV_SQL;
   DBMS_OUTPUT.PUT_LINE(LV_SQL || ';');

   LV_SQL := 'insert into TAX_REPAIRS_ADD_EXPENSE select * from TAX_REPAIRS_ADD_EXPENSE_TEMP';
   execute immediate LV_SQL;
   DBMS_OUTPUT.PUT_LINE(LV_SQL || ';');

   LV_SQL := 'drop table TAX_REPAIRS_ADD_EXPENSE_TEMP';
   execute immediate LV_SQL;
   DBMS_OUTPUT.PUT_LINE(LV_SQL || ';');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (930, 0, 10, 4, 2, 0, 35714, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035714_pwrtax_add_expense.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;