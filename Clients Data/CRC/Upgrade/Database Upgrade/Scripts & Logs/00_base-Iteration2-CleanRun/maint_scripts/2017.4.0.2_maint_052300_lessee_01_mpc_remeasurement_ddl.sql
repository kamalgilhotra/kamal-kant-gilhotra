/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052300_lessee_01_mpc_remeasurement_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.2  09/14/2018 Shane "C" Ward    New table for holding on to on bs remeasurements remaining and init life for following Month End to pick up
||============================================================================
*/

CREATE TABLE ls_mpc_asset_remeasure_life (
asset_id NUMBER(22,0),
set_of_books_id NUMBER(22,0),
gl_posting_mo_yr DATE,
init_life NUMBER(22,2),
remaining_life NUMBER(22,2),
user_id VARCHAR2(18),
time_stamp DATE);

alter table ls_mpc_asset_remeasure_life add (
constraint pk_ls_mpc_asset_remeasure_life primary key (asset_id, set_of_books_id, gl_posting_mo_yr) using index tablespace pwrplant_idx);

COMMENT ON TABLE ls_mpc_asset_remeasure_life IS '[S] Table is populated with System Calculated life for a Lease asset at Remeasurement.
The table is populated with Remeasured Assets going from on to on Balance Sheet with MPC = 0, so that the life of the Asset can be updated during the following month end close.';

COMMENT ON COLUMN ls_mpc_asset_remeasure_life.asset_id IS 'System assigned Identifier of a CPR Asset (this is not leased asset id)';
COMMENT ON COLUMN ls_mpc_asset_remeasure_life.set_of_books_id IS 'System-assigned identifier of a unique set of books maintained by the company';
COMMENT ON COLUMN ls_mpc_asset_remeasure_life.gl_posting_mo_yr IS 'Gl Posting Month that the MPC Remeasurement Life should apply to';
COMMENT ON COLUMN ls_mpc_asset_remeasure_life.remaining_life IS 'The remaining life calculated to be used after remeasurement';
COMMENT ON COLUMN ls_mpc_asset_remeasure_life.init_life IS 'The initial life calculated to be used after remeasurement';
COMMENT ON COLUMN ls_mpc_asset_remeasure_life.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_mpc_asset_remeasure_life.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9762, 0, 2017, 4, 0, 2, 52300, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.2_maint_052300_lessee_01_mpc_remeasurement_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;