SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009076_cpr_detect_errors.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   04/02/2013 Charlie Shilling
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';
   PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

   LL_BAD_COUNT number;
   -- change value of LB_SKIP_CHECK to TRUE if you determine there is data that is OK but will always
   -- show up as an exception.
   LB_SKIP_CHECK boolean := false;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   if not LB_SKIP_CHECK then

      select count(*)
        into LL_BAD_COUNT
        from CPR_ACTIVITY A, PEND_TRANSACTION_ARCHIVE B, CPR_ACT_BASIS E
       where A.ACTIVITY_STATUS = B.PEND_TRANS_ID
         and A.ASSET_ID = E.ASSET_ID
         and A.ASSET_ACTIVITY_ID = E.ASSET_ACTIVITY_ID
         and A.ACTIVITY_QUANTITY <> 0
         and A.ACTIVITY_COST = 0
         and B.ACTIVITY_CODE = 'MRET'
         and NVL(B.RETIRE_METHOD_ID,
                 NVL((select C.RETIRE_METHOD_ID
                       from PROPERTY_UNIT_DEFAULT_LIFE C, RETIREMENT_UNIT D
                      where D.RETIREMENT_UNIT_ID = B.RETIREMENT_UNIT_ID
                        and C.PROPERTY_UNIT_ID = D.PROPERTY_UNIT_ID
                        and C.UTILITY_ACCOUNT_ID = B.UTILITY_ACCOUNT_ID
                        and C.BUS_SEGMENT_ID = B.BUS_SEGMENT_ID
                        and C.COMPANY_ID = B.COMPANY_ID),
                     NVL((select C.RETIRE_METHOD_ID
                           from PROPERTY_UNIT_DEFAULT_LIFE C, RETIREMENT_UNIT D
                          where D.RETIREMENT_UNIT_ID = B.RETIREMENT_UNIT_ID
                            and C.PROPERTY_UNIT_ID = D.PROPERTY_UNIT_ID
                            and C.UTILITY_ACCOUNT_ID = B.UTILITY_ACCOUNT_ID
                            and C.BUS_SEGMENT_ID = B.BUS_SEGMENT_ID
                            and C.COMPANY_ID = -1),
                         NVL((select C.RETIRE_METHOD_ID
                               from PROPERTY_UNIT C, RETIREMENT_UNIT D
                              where D.RETIREMENT_UNIT_ID = B.RETIREMENT_UNIT_ID
                                and D.PROPERTY_UNIT_ID = C.PROPERTY_UNIT_ID),
                             0)))) in (2, 4)
         and (E.BASIS_1 <> 0 or E.BASIS_2 <> 0 or E.BASIS_3 <> 0 or E.BASIS_4 <> 0 or
             E.BASIS_5 <> 0 or E.BASIS_6 <> 0 or E.BASIS_7 <> 0 or E.BASIS_8 <> 0 or
             E.BASIS_9 <> 0 or E.BASIS_10 <> 0 or E.BASIS_11 <> 0 or E.BASIS_12 <> 0 or
             E.BASIS_13 <> 0 or E.BASIS_14 <> 0 or E.BASIS_15 <> 0 or E.BASIS_16 <> 0 or
             E.BASIS_17 <> 0 or E.BASIS_18 <> 0 or E.BASIS_19 <> 0 or E.BASIS_20 <> 0 or
             E.BASIS_21 <> 0 or E.BASIS_22 <> 0 or E.BASIS_23 <> 0 or E.BASIS_24 <> 0 or
             E.BASIS_25 <> 0 or E.BASIS_26 <> 0 or E.BASIS_27 <> 0 or E.BASIS_28 <> 0 or
             E.BASIS_29 <> 0 or E.BASIS_30 <> 0 or E.BASIS_31 <> 0 or E.BASIS_32 <> 0 or
             E.BASIS_33 <> 0 or E.BASIS_34 <> 0 or E.BASIS_35 <> 0 or E.BASIS_36 <> 0 or
             E.BASIS_37 <> 0 or E.BASIS_38 <> 0 or E.BASIS_39 <> 0 or E.BASIS_40 <> 0 or
             E.BASIS_41 <> 0 or E.BASIS_42 <> 0 or E.BASIS_43 <> 0 or E.BASIS_44 <> 0 or
             E.BASIS_45 <> 0 or E.BASIS_46 <> 0 or E.BASIS_47 <> 0 or E.BASIS_48 <> 0 or
             E.BASIS_49 <> 0 or E.BASIS_50 <> 0 or E.BASIS_51 <> 0 or E.BASIS_52 <> 0 or
             E.BASIS_53 <> 0 or E.BASIS_54 <> 0 or E.BASIS_55 <> 0 or E.BASIS_56 <> 0 or
             E.BASIS_57 <> 0 or E.BASIS_58 <> 0 or E.BASIS_59 <> 0 or E.BASIS_60 <> 0 or
             E.BASIS_61 <> 0 or E.BASIS_62 <> 0 or E.BASIS_63 <> 0 or E.BASIS_64 <> 0 or
             E.BASIS_65 <> 0 or E.BASIS_66 <> 0 or E.BASIS_67 <> 0 or E.BASIS_68 <> 0 or
             E.BASIS_69 <> 0 or E.BASIS_70 <> 0);

      if LL_BAD_COUNT > 0 then
         DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA' ||
                              '-02000 - Contact PPC support for help.  Read message below.');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  Please edit the script and read the comments at ');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  the bottom to see how to evaluate the data issues.');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  To skip checking this in the future.');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  change this line: LB_SKIP_CHECK boolean := false;');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  to:               LB_SKIP_CHECK boolean := true;');

         RAISE_APPLICATION_ERROR(-20000, 'Data issue, contact PPC support for help.');
      else
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Data is OK.');
      end if;
   else
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Check not performed.');
   end if;
end;
/

/*
Maintenance 9860 fixed a problem in Post.exe where, when a net-zero asset
(asset with a non-zero accum_quantity but a zero accum_cost) was selected as
one of the retirements in a mass retirement transaction, the basis amounts
on cpr_act_basis end up with the incorrect sign, and, consequently, the
cpr_ldg_basis values are adjusted in the incorrect direction.

For example, if an asset has a Basis 1 amount of 100 and a Basis 2 amount of
-100, and has no other basis values but has a positive quantity, this is a
net-zero asset that is eligible for mass retirement selection. If a mass
retirement is retiring half of the quantity of the asset, then the
cpr_act_basis values for the activity should be -50 for Basis 1 and 50 for
Basis 2, bringing the cpr_ldg_basis values to 50 and -50.

However, Post was getting the activity amounts with the incorrect sign, so the
cpr_act_basis values would actually 50 for Basis 1 and -50 for Basis 2,
causing the cpr_ldg_basis values to go to 150 and -150.

The overall asset and activity costs correctly remain at zero and the asset
quantity is adjusted correctly, so this bug only affects reporting in
situations where one or more of the non-zero basis buckets is omitted.

The script provided with this maintenance was designed to detect mass
retirement activities on net-zero assets, as this bug is believed to have
affected all net-zero mass retirements. The query provided below can be used
to review the data errors in detail.

Once the activities have been identified, some analysis should be done to
confirm that these records have the incorrect sign by comparing the
cpr_act_basis values to the cpr_ldg_basis values. You should also check that
the incorrect activity has not already been corrected.

To correct the outstanding bad records, the activities should be reversed
using the CPR unretire feature. Then the quantity should be re-retired using
a specific retirement on that asset, taking care to set the
out_of_service_mo_yr on the retirement to that of the original retirement
activity so that depr_studies is not affected.

Please contact Charlie Shilling at cshilling@pwrplan.com with any questions.

Review data using the following query:

select A.ASSET_ID, A.ASSET_ACTIVITY_ID, B.PEND_TRANS_ID, A.ACTIVITY_QUANTITY, A.ACTIVITY_COST, E.*
  from CPR_ACTIVITY A, PEND_TRANSACTION_ARCHIVE B, CPR_ACT_BASIS E
 where A.ACTIVITY_STATUS = B.PEND_TRANS_ID
   and A.ASSET_ID = E.ASSET_ID
   and A.ASSET_ACTIVITY_ID = E.ASSET_ACTIVITY_ID
   and A.ACTIVITY_QUANTITY <> 0
   and A.ACTIVITY_COST = 0
   and B.ACTIVITY_CODE = 'MRET'
   and NVL(B.RETIRE_METHOD_ID,
           NVL((select C.RETIRE_METHOD_ID
                 from PROPERTY_UNIT_DEFAULT_LIFE C, RETIREMENT_UNIT D
                where D.RETIREMENT_UNIT_ID = B.RETIREMENT_UNIT_ID
                  and C.PROPERTY_UNIT_ID = D.PROPERTY_UNIT_ID
                  and C.UTILITY_ACCOUNT_ID = B.UTILITY_ACCOUNT_ID
                  and C.BUS_SEGMENT_ID = B.BUS_SEGMENT_ID
                  and C.COMPANY_ID = B.COMPANY_ID),
               NVL((select C.RETIRE_METHOD_ID
                     from PROPERTY_UNIT_DEFAULT_LIFE C, RETIREMENT_UNIT D
                    where D.RETIREMENT_UNIT_ID = B.RETIREMENT_UNIT_ID
                      and C.PROPERTY_UNIT_ID = D.PROPERTY_UNIT_ID
                      and C.UTILITY_ACCOUNT_ID = B.UTILITY_ACCOUNT_ID
                      and C.BUS_SEGMENT_ID = B.BUS_SEGMENT_ID
                      and C.COMPANY_ID = -1),
                   NVL((select C.RETIRE_METHOD_ID
                         from PROPERTY_UNIT C, RETIREMENT_UNIT D
                        where D.RETIREMENT_UNIT_ID = B.RETIREMENT_UNIT_ID
                          and D.PROPERTY_UNIT_ID = C.PROPERTY_UNIT_ID),
                       0)))) in (2, 4) --2 = FIFO; 4 = Curve
   and (E.BASIS_1 <> 0 or E.BASIS_2 <> 0 or E.BASIS_3 <> 0 or E.BASIS_4 <> 0 or E.BASIS_5 <> 0 or
       E.BASIS_6 <> 0 or E.BASIS_7 <> 0 or E.BASIS_8 <> 0 or E.BASIS_9 <> 0 or E.BASIS_10 <> 0 or
       E.BASIS_11 <> 0 or E.BASIS_12 <> 0 or E.BASIS_13 <> 0 or E.BASIS_14 <> 0 or E.BASIS_15 <> 0 or
       E.BASIS_16 <> 0 or E.BASIS_17 <> 0 or E.BASIS_18 <> 0 or E.BASIS_19 <> 0 or E.BASIS_20 <> 0 or
       E.BASIS_21 <> 0 or E.BASIS_22 <> 0 or E.BASIS_23 <> 0 or E.BASIS_24 <> 0 or E.BASIS_25 <> 0 or
       E.BASIS_26 <> 0 or E.BASIS_27 <> 0 or E.BASIS_28 <> 0 or E.BASIS_29 <> 0 or E.BASIS_30 <> 0 or
       E.BASIS_31 <> 0 or E.BASIS_32 <> 0 or E.BASIS_33 <> 0 or E.BASIS_34 <> 0 or E.BASIS_35 <> 0 or
       E.BASIS_36 <> 0 or E.BASIS_37 <> 0 or E.BASIS_38 <> 0 or E.BASIS_39 <> 0 or E.BASIS_40 <> 0 or
       E.BASIS_41 <> 0 or E.BASIS_42 <> 0 or E.BASIS_43 <> 0 or E.BASIS_44 <> 0 or E.BASIS_45 <> 0 or
       E.BASIS_46 <> 0 or E.BASIS_47 <> 0 or E.BASIS_48 <> 0 or E.BASIS_49 <> 0 or E.BASIS_50 <> 0 or
       E.BASIS_51 <> 0 or E.BASIS_52 <> 0 or E.BASIS_53 <> 0 or E.BASIS_54 <> 0 or E.BASIS_55 <> 0 or
       E.BASIS_56 <> 0 or E.BASIS_57 <> 0 or E.BASIS_58 <> 0 or E.BASIS_59 <> 0 or E.BASIS_60 <> 0 or
       E.BASIS_61 <> 0 or E.BASIS_62 <> 0 or E.BASIS_63 <> 0 or E.BASIS_64 <> 0 or E.BASIS_65 <> 0 or
       E.BASIS_66 <> 0 or E.BASIS_67 <> 0 or E.BASIS_68 <> 0 or E.BASIS_69 <> 0 or E.BASIS_70 <> 0);
*/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (344, 0, 10, 4, 0, 0, 9076, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_009076_cpr_detect_errors.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;