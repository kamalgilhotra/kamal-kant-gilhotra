/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051723_lessee_01_add_arrears_accrual_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/21/2018 Josh Sandler     Add arrears accrual columns
||============================================================================
*/

ALTER TABLE LS_ASSET_SCHEDULE
  ADD (
        beg_arrears_accrual NUMBER(22, 2) NULL,
        arrears_accrual NUMBER(22, 2) NULL,
        end_arrears_accrual NUMBER(22, 2) NULL
      )
;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD (
        beg_arrears_accrual NUMBER(22, 2) NULL,
        arrears_accrual NUMBER(22, 2) NULL,
        end_arrears_accrual NUMBER(22, 2) NULL
      )
;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD (
        beg_arrears_accrual NUMBER(22, 2) NULL,
        arrears_accrual NUMBER(22, 2) NULL,
        end_arrears_accrual NUMBER(22, 2) NULL
      )
;

ALTER TABLE LS_ILR_SCHEDULE
  ADD (
        beg_arrears_accrual NUMBER(22, 2) NULL,
        arrears_accrual NUMBER(22, 2) NULL,
        end_arrears_accrual NUMBER(22, 2) NULL
      )
;

ALTER TABLE LS_SUMMARY_FORECAST
  ADD (
        beg_arrears_accrual NUMBER(22, 2) NULL,
        arrears_accrual NUMBER(22, 2) NULL,
        end_arrears_accrual NUMBER(22, 2) NULL
      )
;

COMMENT ON COLUMN LS_ASSET_SCHEDULE.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN LS_SUMMARY_FORECAST.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN LS_SUMMARY_FORECAST.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';



  CREATE OR REPLACE FORCE VIEW "PWRPLANT"."V_LS_ILR_SCHEDULE_FX_VW" ("ILR_ID", "ILR_NUMBER", "LEASE_ID", "LEASE_NUMBER", "CURRENT_REVISION", "REVISION",
   "SET_OF_BOOKS_ID", "MONTH", "COMPANY_ID", "OPEN_MONTH", "LS_CUR_TYPE", "EXCHANGE_DATE", "PREV_EXCHANGE_DATE", "CONTRACT_CURRENCY_ID", "DISPLAY_CURRENCY_ID",
   "RATE", "CALCULATED_RATE", "PREVIOUS_CALCULATED_RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL", "IS_OM", "PURCHASE_OPTION_AMT", "TERMINATION_AMT",
   "NET_PRESENT_VALUE", "CAPITAL_COST", "BEG_CAPITAL_COST", "END_CAPITAL_COST", "BEG_OBLIGATION", "END_OBLIGATION", "BEG_LT_OBLIGATION", "END_LT_OBLIGATION",
   "PRINCIPAL_REMEASUREMENT", "BEG_LIABILITY", "END_LIABILITY", "BEG_LT_LIABILITY", "END_LT_LIABILITY", "LIABILITY_REMEASUREMENT", "INTEREST_ACCRUAL",
   "PRINCIPAL_ACCRUAL", "INTEREST_PAID", "PRINCIPAL_PAID", "EXECUTORY_ACCRUAL1", "EXECUTORY_ACCRUAL2", "EXECUTORY_ACCRUAL3", "EXECUTORY_ACCRUAL4",
   "EXECUTORY_ACCRUAL5", "EXECUTORY_ACCRUAL6", "EXECUTORY_ACCRUAL7", "EXECUTORY_ACCRUAL8", "EXECUTORY_ACCRUAL9", "EXECUTORY_ACCRUAL10", "EXECUTORY_PAID1",
   "EXECUTORY_PAID2", "EXECUTORY_PAID3", "EXECUTORY_PAID4", "EXECUTORY_PAID5", "EXECUTORY_PAID6", "EXECUTORY_PAID7", "EXECUTORY_PAID8", "EXECUTORY_PAID9",
   "EXECUTORY_PAID10", "CONTINGENT_ACCRUAL1", "CONTINGENT_ACCRUAL2", "CONTINGENT_ACCRUAL3", "CONTINGENT_ACCRUAL4", "CONTINGENT_ACCRUAL5", "CONTINGENT_ACCRUAL6",
   "CONTINGENT_ACCRUAL7", "CONTINGENT_ACCRUAL8", "CONTINGENT_ACCRUAL9", "CONTINGENT_ACCRUAL10", "CONTINGENT_PAID1", "CONTINGENT_PAID2", "CONTINGENT_PAID3",
   "CONTINGENT_PAID4", "CONTINGENT_PAID5", "CONTINGENT_PAID6", "CONTINGENT_PAID7", "CONTINGENT_PAID8", "CONTINGENT_PAID9", "CONTINGENT_PAID10",
   "CURRENT_LEASE_COST", "BEG_DEFERRED_RENT", "DEFERRED_RENT", "END_DEFERRED_RENT", "BEG_ST_DEFERRED_RENT", "END_ST_DEFERRED_RENT", "INITIAL_DIRECT_COST",
   "INCENTIVE_AMOUNT", "GAIN_LOSS_FX", "DEPR_EXPENSE", "BEGIN_RESERVE", "END_RESERVE", "DEPR_EXP_ALLOC_ADJUST", "BEG_PREPAID_RENT", "PREPAY_AMORTIZATION",
   "PREPAID_RENT", "END_PREPAID_RENT","BEG_NET_ROU_ASSET", "END_NET_ROU_ASSET", "ROU_ASSET_REMEASUREMENT", "PARTIAL_TERM_GAIN_LOSS",
   "BEG_ARREARS_ACCRUAL", "ARREARS_ACCRUAL", "END_ARREARS_ACCRUAL") AS
  WITH cur
       AS ( SELECT ls_currency_type_id AS ls_cur_type,
                   currency_id,
                   currency_display_symbol,
                   iso_code,
                   CASE ls_currency_type_id
                     WHEN 1 THEN 1
                     ELSE NULL
                   END                 AS contract_approval_rate
            FROM   CURRENCY
                   cross join LS_LEASE_CURRENCY_TYPE ),
       open_month
       AS ( SELECT company_id,
                   Min( gl_posting_mo_yr ) open_month
            FROM   LS_PROCESS_CONTROL
            WHERE  open_next IS NULL
            GROUP  BY company_id ),
       calc_rate
       AS ( SELECT a.company_id,
                   a.contract_currency_id,
                   a.company_currency_id,
                   a.accounting_month,
                   a.exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM   LS_LEASE_CALCULATED_DATE_RATES a
                   left outer join LS_LEASE_CALCULATED_DATE_RATES b
                                ON a.company_id = b.company_id AND
                                   a.contract_currency_id = b.contract_currency_id AND
                                   a.accounting_month = Add_months( b.accounting_month, 1 ) ),
       cr_now
       AS ( SELECT currency_from,
                   currency_to,
                   rate
            FROM   ( SELECT currency_from,
                            currency_to,
                            rate,
                            Row_number( )
                              over(
                                PARTITION BY currency_from, currency_to
                                ORDER BY exchange_date DESC ) AS rn
                     FROM   CURRENCY_RATE_DEFAULT_DENSE
                     WHERE  Trunc( exchange_date, 'MONTH' ) <= Trunc( SYSDATE, 'MONTH' ) AND
                            exchange_rate_type_id = 1 )
            WHERE  rn = 1 )
  SELECT lis.ilr_id                                                                                                      ilr_id,
         lis.ilr_number                                                                                                  ilr_number,
         lease.lease_id                                                                                                  lease_id,
         lease.lease_number                                                                                              lease_number,
         lis.current_revision                                                                                            current_revision,
         lis.revision                                                                                                    revision,
         lis.set_of_books_id                                                                                             set_of_books_id,
         lis.month                                                                                                       month,
         OPEN_MONTH.company_id                                                                                           company_id,
         OPEN_MONTH.open_month                                                                                           open_month,
         CUR.ls_cur_type                                                                                                 AS ls_cur_type,
         cr.exchange_date                                                                                                exchange_date,
         CALC_RATE.exchange_date                                                                                         prev_exchange_date,
         lease.contract_currency_id                                                                                      contract_currency_id,
         CUR.currency_id                                                                                                 display_currency_id,
         cr.rate                                                                                                         rate,
         CALC_RATE.rate                                                                                                  calculated_rate,
         CALC_RATE.prev_rate                                                                                             previous_calculated_rate,
         CUR.iso_code                                                                                                    iso_code,
         CUR.currency_display_symbol                                                                                     currency_display_symbol,
         lis.is_om                                                                                                       is_om,
         lis.purchase_option_amt * Nvl( CALC_RATE.rate, cr.rate )                                                        purchase_option_amt,
         lis.termination_amt * Nvl( CALC_RATE.rate, cr.rate )                                                            termination_amt,
         lis.net_present_value * Nvl( CALC_RATE.rate, cr.rate )                                                          net_present_value,
         lis.capital_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )          capital_cost,
         lis.beg_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )      beg_capital_cost,
         lis.end_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )      end_capital_cost,
         lis.beg_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                             beg_obligation,
         lis.end_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                             end_obligation,
         lis.beg_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                          beg_lt_obligation,
         lis.end_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                          end_lt_obligation,
         lis.principal_remeasurement * Nvl( CALC_RATE.rate, cr.rate )                                                    principal_remeasurement,
         lis.beg_liability * Nvl( CALC_RATE.rate, cr.rate )                                                              beg_liability,
         lis.end_liability * Nvl( CALC_RATE.rate, cr.rate )                                                              end_liability,
         lis.beg_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                           beg_lt_liability,
         lis.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                           end_lt_liability,
         lis.liability_remeasurement * Nvl( CALC_RATE.rate, cr.rate )                                                    liability_remeasurement,
         lis.interest_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                           interest_accrual,
         lis.principal_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                          principal_accrual,
         lis.interest_paid * Nvl( CALC_RATE.rate, cr.rate )                                                              interest_paid,
         lis.principal_paid * Nvl( CALC_RATE.rate, cr.rate )                                                             principal_paid,
         lis.executory_accrual1 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual1,
         lis.executory_accrual2 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual2,
         lis.executory_accrual3 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual3,
         lis.executory_accrual4 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual4,
         lis.executory_accrual5 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual5,
         lis.executory_accrual6 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual6,
         lis.executory_accrual7 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual7,
         lis.executory_accrual8 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual8,
         lis.executory_accrual9 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual9,
         lis.executory_accrual10 * Nvl( CALC_RATE.rate, cr.rate )                                                        executory_accrual10,
         lis.executory_paid1 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid1,
         lis.executory_paid2 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid2,
         lis.executory_paid3 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid3,
         lis.executory_paid4 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid4,
         lis.executory_paid5 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid5,
         lis.executory_paid6 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid6,
         lis.executory_paid7 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid7,
         lis.executory_paid8 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid8,
         lis.executory_paid9 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid9,
         lis.executory_paid10 * Nvl( CALC_RATE.rate, cr.rate )                                                           executory_paid10,
         lis.contingent_accrual1 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual1,
         lis.contingent_accrual2 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual2,
         lis.contingent_accrual3 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual3,
         lis.contingent_accrual4 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual4,
         lis.contingent_accrual5 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual5,
         lis.contingent_accrual6 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual6,
         lis.contingent_accrual7 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual7,
         lis.contingent_accrual8 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual8,
         lis.contingent_accrual9 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual9,
         lis.contingent_accrual10 * Nvl( CALC_RATE.rate, cr.rate )                                                       contingent_accrual10,
         lis.contingent_paid1 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid1,
         lis.contingent_paid2 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid2,
         lis.contingent_paid3 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid3,
         lis.contingent_paid4 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid4,
         lis.contingent_paid5 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid5,
         lis.contingent_paid6 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid6,
         lis.contingent_paid7 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid7,
         lis.contingent_paid8 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid8,
         lis.contingent_paid9 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid9,
         lis.contingent_paid10 * Nvl( CALC_RATE.rate, cr.rate )                                                          contingent_paid10,
         lis.current_lease_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )    current_lease_cost,
         lis.beg_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                          beg_deferred_rent,
         lis.deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                              deferred_rent,
         lis.end_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                          end_deferred_rent,
         lis.beg_st_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                       beg_st_deferred_rent,
         lis.end_st_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                       end_st_deferred_rent,
         lis.initial_direct_cost * Nvl( CALC_RATE.rate, cr.rate )                                                       initial_direct_cost,
         lis.incentive_amount * Nvl( CALC_RATE.rate, cr.rate )                                                       incentive_amount,
         lis.beg_obligation * ( Nvl( CALC_RATE.rate, 0 ) - Nvl( CALC_RATE.prev_rate, 0 ) )                               gain_loss_fx,
         lis.depr_expense * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )          depr_expense,
         lis.begin_reserve * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )         begin_reserve,
         lis.end_reserve * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )           end_reserve,
         lis.depr_exp_alloc_adjust * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate ) depr_exp_alloc_adjust,
         lis.beg_prepaid_rent * Nvl( CALC_RATE.rate, cr.rate )                                                           beg_prepaid_rent,
         lis.prepay_amortization * Nvl( CALC_RATE.rate, cr.rate )                                                        prepay_amortization,
         lis.prepaid_rent * Nvl( CALC_RATE.rate, cr.rate )                                                               prepaid_rent,
         lis.end_prepaid_rent * Nvl( CALC_RATE.rate, cr.rate )                                                           end_prepaid_rent,
         lis.beg_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )     beg_net_rou_asset,
         lis.end_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )     end_net_rou_asset,
		 lis.rou_asset_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )     rou_asset_remeasurement,
		 lis.partial_term_gain_loss * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )     partial_term_gain_loss,
     lis.beg_arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                        beg_arrears_accrual,
     lis.arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                        arrears_accrual,
     lis.end_arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                        end_arrears_accrual
  FROM   ( SELECT a.ilr_id,
                  a.ilr_number,
                  a.current_revision,
                  a.revision,
                  a.set_of_books_id,
                  a.month,
                  a.beg_capital_cost,
                  a.end_capital_cost,
                  a.beg_obligation,
                  a.end_obligation,
                  a.beg_lt_obligation,
                  a.end_lt_obligation,
                  a.principal_remeasurement,
                  a.beg_liability,
                  a.end_liability,
                  a.beg_lt_liability,
                  a.end_lt_liability,
                  a.liability_remeasurement,
                  a.interest_accrual,
                  a.principal_accrual,
                  a.interest_paid,
                  a.principal_paid,
                  a.executory_accrual1,
                  a.executory_accrual2,
                  a.executory_accrual3,
                  a.executory_accrual4,
                  a.executory_accrual5,
                  a.executory_accrual6,
                  a.executory_accrual7,
                  a.executory_accrual8,
                  a.executory_accrual9,
                  a.executory_accrual10,
                  a.executory_paid1,
                  a.executory_paid2,
                  a.executory_paid3,
                  a.executory_paid4,
                  a.executory_paid5,
                  a.executory_paid6,
                  a.executory_paid7,
                  a.executory_paid8,
                  a.executory_paid9,
                  a.executory_paid10,
                  a.contingent_accrual1,
                  a.contingent_accrual2,
                  a.contingent_accrual3,
                  a.contingent_accrual4,
                  a.contingent_accrual5,
                  a.contingent_accrual6,
                  a.contingent_accrual7,
                  a.contingent_accrual8,
                  a.contingent_accrual9,
                  a.contingent_accrual10,
                  a.contingent_paid1,
                  a.contingent_paid2,
                  a.contingent_paid3,
                  a.contingent_paid4,
                  a.contingent_paid5,
                  a.contingent_paid6,
                  a.contingent_paid7,
                  a.contingent_paid8,
                  a.contingent_paid9,
                  a.contingent_paid10,
                  a.current_lease_cost,
                  a.beg_deferred_rent,
                  a.deferred_rent,
                  a.end_deferred_rent,
                  a.beg_st_deferred_rent,
                  a.end_st_deferred_rent,
                  a.initial_direct_cost,
                  a.incentive_amount,
                  depr.depr_expense,
                  depr.begin_reserve,
                  depr.end_reserve,
                  depr.depr_exp_alloc_adjust,
                  a.lease_id,
                  a.company_id,
                  a.in_service_exchange_rate,
                  a.purchase_option_amt,
                  a.termination_amt,
                  a.net_present_value,
                  a.capital_cost,
                  a.is_om,
                  a.beg_prepaid_rent,
                  a.prepay_amortization,
                  a.prepaid_rent,
                  a.end_prepaid_rent,
                  a.beg_net_rou_asset,
                  a.end_net_rou_asset ,
                  a.rou_asset_remeasurement,
                  a.partial_term_gain_loss,
                  a.beg_arrears_accrual,
                  a.arrears_accrual,
                  a.end_arrears_accrual
           FROM   ( SELECT lis.ilr_id,
                           ilr.ilr_number,
                           ilr.current_revision,
                           lis.revision,
                           lis.set_of_books_id,
                           lis.month,
                           lis.beg_capital_cost,
                           lis.end_capital_cost,
                           lis.beg_obligation,
                           lis.end_obligation,
                           lis.beg_lt_obligation,
                           lis.end_lt_obligation,
                           lis.principal_remeasurement,
                           lis.beg_liability,
                           lis.end_liability,
                           lis.beg_lt_liability,
                           lis.end_lt_liability,
                           lis.liability_remeasurement,
                           lis.interest_accrual,
                           lis.principal_accrual,
                           lis.interest_paid,
                           lis.principal_paid,
                           lis.executory_accrual1,
                           lis.executory_accrual2,
                           lis.executory_accrual3,
                           lis.executory_accrual4,
                           lis.executory_accrual5,
                           lis.executory_accrual6,
                           lis.executory_accrual7,
                           lis.executory_accrual8,
                           lis.executory_accrual9,
                           lis.executory_accrual10,
                           lis.executory_paid1,
                           lis.executory_paid2,
                           lis.executory_paid3,
                           lis.executory_paid4,
                           lis.executory_paid5,
                           lis.executory_paid6,
                           lis.executory_paid7,
                           lis.executory_paid8,
                           lis.executory_paid9,
                           lis.executory_paid10,
                           lis.contingent_accrual1,
                           lis.contingent_accrual2,
                           lis.contingent_accrual3,
                           lis.contingent_accrual4,
                           lis.contingent_accrual5,
                           lis.contingent_accrual6,
                           lis.contingent_accrual7,
                           lis.contingent_accrual8,
                           lis.contingent_accrual9,
                           lis.contingent_accrual10,
                           lis.contingent_paid1,
                           lis.contingent_paid2,
                           lis.contingent_paid3,
                           lis.contingent_paid4,
                           lis.contingent_paid5,
                           lis.contingent_paid6,
                           lis.contingent_paid7,
                           lis.contingent_paid8,
                           lis.contingent_paid9,
                           lis.contingent_paid10,
                           lis.current_lease_cost,
                           lis.beg_deferred_rent,
                           lis.deferred_rent,
                           lis.end_deferred_rent,
                           lis.beg_st_deferred_rent,
                           lis.end_st_deferred_rent,
                           lis.initial_direct_cost,
                           lis.incentive_amount,
                           ilr.lease_id,
                           ilr.company_id,
                           opt.in_service_exchange_rate,
                           opt.purchase_option_amt,
                           opt.termination_amt,
                           liasob.net_present_value,
                           liasob.capital_cost,
                           lis.is_om,
                           lis.beg_prepaid_rent,
                           lis.prepay_amortization,
                           lis.prepaid_rent,
                           lis.end_prepaid_rent,
                           lis.beg_net_rou_asset,
                           lis.end_net_rou_asset ,
						   lis.rou_asset_remeasurement,
                           lis.partial_term_gain_loss,
                           lis.beg_arrears_accrual,
                           lis.arrears_accrual,
                           lis.end_arrears_accrual
                    FROM   LS_ILR_SCHEDULE lis,
                           LS_ILR_OPTIONS opt,
                           LS_ILR_AMOUNTS_SET_OF_BOOKS liasob,
                           LS_ILR ilr,
                           LS_LEASE lease
                    WHERE  lis.ilr_id = opt.ilr_id AND
                           lis.revision = opt.revision AND
                           lis.ilr_id = liasob.ilr_id AND
                           lis.revision = liasob.revision AND
                           lis.set_of_books_id = liasob.set_of_books_id AND
                           lis.ilr_id = ilr.ilr_id AND
                           ilr.lease_id = lease.lease_id ) a
                  left outer join ( SELECT la.ilr_id,
                                           ldf.revision,
                                           ldf.set_of_books_id,
                                           ldf.month,
                                           SUM( ldf.depr_expense )          AS depr_expense,
                                           SUM( ldf.begin_reserve )         AS begin_reserve,
                                           SUM( ldf.end_reserve )           AS end_reserve,
                                           SUM( ldf.depr_exp_alloc_adjust ) AS depr_exp_alloc_adjust
                                    FROM   LS_ASSET la,
                                           LS_DEPR_FORECAST ldf
                                    WHERE  la.ls_asset_id = ldf.ls_asset_id
                                    GROUP  BY la.ilr_id,ldf.revision,ldf.set_of_books_id,ldf.month ) depr
                               ON a.ilr_id = depr.ilr_id AND
                                  a.revision = depr.revision AND
                                  a.set_of_books_id = depr.set_of_books_id AND
                                  a.month = depr.month ) lis
         inner join LS_LEASE lease
                 ON lis.lease_id = lease.lease_id
         inner join CURRENCY_SCHEMA cs
                 ON lis.company_id = cs.company_id
         inner join cur
                 ON CUR.currency_id = CASE CUR.ls_cur_type
                                        WHEN 1 THEN lease.contract_currency_id
                                        WHEN 2 THEN cs.currency_id
                                        ELSE NULL
                                      END
         inner join open_month
                 ON lis.company_id = OPEN_MONTH.company_id
         inner join CURRENCY_RATE_DEFAULT_DENSE cr
                 ON CUR.currency_id = cr.currency_to AND
                    lease.contract_currency_id = cr.currency_from AND
                    Trunc( cr.exchange_date, 'MONTH' ) = Trunc( lis.month, 'MONTH' )
         inner join cr_now
                 ON CUR.currency_id = CR_NOW.currency_to AND
                    lease.contract_currency_id = CR_NOW.currency_from
         left outer join calc_rate
                      ON lease.contract_currency_id = CALC_RATE.contract_currency_id AND
                         CUR.currency_id = CALC_RATE.company_currency_id AND
                         lis.company_id = CALC_RATE.company_id AND
                         lis.month = CALC_RATE.accounting_month
  WHERE  cs.currency_type_id = 1 AND
         cr.exchange_rate_type_id = 1;




CREATE OR REPLACE FORCE VIEW "PWRPLANT"."V_LS_ASSET_SCHEDULE_FX_VW" ("ILR_ID", "LS_ASSET_ID", "REVISION", "SET_OF_BOOKS_ID", "MONTH", "COMPANY_ID",
  "OPEN_MONTH", "LS_CUR_TYPE", "EXCHANGE_DATE", "PREV_EXCHANGE_DATE", "CONTRACT_CURRENCY_ID", "CURRENCY_ID", "RATE", "CALCULATED_RATE",
  "PREVIOUS_CALCULATED_RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL", "RESIDUAL_AMOUNT", "TERM_PENALTY", "BPO_PRICE", "BEG_CAPITAL_COST",
  "END_CAPITAL_COST", "BEG_OBLIGATION", "END_OBLIGATION", "BEG_LT_OBLIGATION", "END_LT_OBLIGATION", "PRINCIPAL_REMEASUREMENT", "BEG_LIABILITY",
  "END_LIABILITY", "BEG_LT_LIABILITY", "END_LT_LIABILITY", "LIABILITY_REMEASUREMENT", "INTEREST_ACCRUAL", "PRINCIPAL_ACCRUAL", "INTEREST_PAID",
  "PRINCIPAL_PAID", "EXECUTORY_ACCRUAL1", "EXECUTORY_ACCRUAL2", "EXECUTORY_ACCRUAL3", "EXECUTORY_ACCRUAL4", "EXECUTORY_ACCRUAL5", "EXECUTORY_ACCRUAL6",
  "EXECUTORY_ACCRUAL7", "EXECUTORY_ACCRUAL8", "EXECUTORY_ACCRUAL9", "EXECUTORY_ACCRUAL10", "EXECUTORY_PAID1", "EXECUTORY_PAID2", "EXECUTORY_PAID3",
  "EXECUTORY_PAID4", "EXECUTORY_PAID5", "EXECUTORY_PAID6", "EXECUTORY_PAID7", "EXECUTORY_PAID8", "EXECUTORY_PAID9", "EXECUTORY_PAID10",
  "CONTINGENT_ACCRUAL1", "CONTINGENT_ACCRUAL2", "CONTINGENT_ACCRUAL3", "CONTINGENT_ACCRUAL4", "CONTINGENT_ACCRUAL5", "CONTINGENT_ACCRUAL6",
  "CONTINGENT_ACCRUAL7", "CONTINGENT_ACCRUAL8", "CONTINGENT_ACCRUAL9", "CONTINGENT_ACCRUAL10", "CONTINGENT_PAID1", "CONTINGENT_PAID2", "CONTINGENT_PAID3",
  "CONTINGENT_PAID4", "CONTINGENT_PAID5", "CONTINGENT_PAID6", "CONTINGENT_PAID7", "CONTINGENT_PAID8", "CONTINGENT_PAID9", "CONTINGENT_PAID10",
  "CURRENT_LEASE_COST", "BEG_DEFERRED_RENT", "DEFERRED_RENT", "END_DEFERRED_RENT", "BEG_ST_DEFERRED_RENT", "END_ST_DEFERRED_RENT", "INITIAL_DIRECT_COST",
  "INCENTIVE_AMOUNT", "GAIN_LOSS_FX", "DEPR_EXPENSE", "BEGIN_RESERVE", "END_RESERVE", "DEPR_EXP_ALLOC_ADJUST", "ASSET_DESCRIPTION", "LEASED_ASSET_NUMBER",
  "FMV", "IS_OM", "APPROVED_REVISION", "LEASE_CAP_TYPE_ID", "LS_ASSET_STATUS_ID", "RETIREMENT_DATE", "BEG_PREPAID_RENT", "PREPAY_AMORTIZATION",
  "PREPAID_RENT", "END_PREPAID_RENT","BEG_NET_ROU_ASSET","END_NET_ROU_ASSET", "ACTUAL_RESIDUAL_AMOUNT", "GUARANTEED_RESIDUAL_AMOUNT", "ESTIMATED_RESIDUAL", "ROU_ASSET_REMEASUREMENT", "PARTIAL_TERM_GAIN_LOSS",
  "BEG_ARREARS_ACCRUAL", "ARREARS_ACCRUAL", "END_ARREARS_ACCRUAL") AS
  WITH cur
       AS ( SELECT ls_currency_type_id AS ls_cur_type,
                   currency_id,
                   currency_display_symbol,
                   iso_code,
                   CASE ls_currency_type_id
                     WHEN 1 THEN 1
                     ELSE NULL
                   END                 AS contract_approval_rate
            FROM   CURRENCY
                   cross join LS_LEASE_CURRENCY_TYPE ),
       open_month
       AS ( SELECT company_id,
                   Min( gl_posting_mo_yr ) open_month
            FROM   LS_PROCESS_CONTROL
            WHERE  open_next IS NULL
            GROUP  BY company_id ),
       calc_rate
       AS ( SELECT a.company_id,
                   a.contract_currency_id,
                   a.company_currency_id,
                   a.accounting_month,
                   a.exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM   LS_LEASE_CALCULATED_DATE_RATES a
                   left outer join LS_LEASE_CALCULATED_DATE_RATES b
                                ON a.company_id = b.company_id AND
                                   a.contract_currency_id = b.contract_currency_id AND
                                   a.accounting_month = Add_months( b.accounting_month, 1 ) ),
       cr_now
       AS ( SELECT currency_from,
                   currency_to,
                   rate
            FROM   ( SELECT currency_from,
                            currency_to,
                            rate,
                            Row_number( )
                              over(
                                PARTITION BY currency_from, currency_to
                                ORDER BY exchange_date DESC ) AS rn
                     FROM   CURRENCY_RATE_DEFAULT_DENSE
                     WHERE  Trunc( exchange_date, 'MONTH' ) <= Trunc( SYSDATE, 'MONTH' ) AND
                            exchange_rate_type_id = 1 )
            WHERE  rn = 1 )
  SELECT las.ilr_id                                                                                                                                               ilr_id,
         las.ls_asset_id                                                                                                                                          ls_asset_id,
         las.revision                                                                                                                                             revision,
         las.set_of_books_id                                                                                                                                      set_of_books_id,
         las.month                                                                                                                                                month,
         OPEN_MONTH.company_id                                                                                                                                    company_id,
         OPEN_MONTH.open_month                                                                                                                                    open_month,
         CUR.ls_cur_type                                                                                                                                          ls_cur_type,
         cr.exchange_date                                                                                                                                         exchange_date,
         CALC_RATE.exchange_date                                                                                                                                  prev_exchange_date,
         las.contract_currency_id                                                                                                                                 contract_currency_id,
         CUR.currency_id                                                                                                                                          currency_id,
         cr.rate                                                                                                                                                  rate,
         CALC_RATE.rate                                                                                                                                           calculated_rate,
         CALC_RATE.prev_rate                                                                                                                                      previous_calculated_rate,
         CUR.iso_code                                                                                                                                             iso_code,
         CUR.currency_display_symbol                                                                                                                              currency_display_symbol,
         las.residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     residual_amount,
         las.term_penalty * Nvl( CALC_RATE.rate, cr.rate )                                                                                                        term_penalty,
         las.bpo_price * Nvl( CALC_RATE.rate, cr.rate )                                                                                                           bpo_price,
         las.beg_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                               beg_capital_cost,
         las.end_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                               end_capital_cost,
         las.beg_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      beg_obligation,
         las.end_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      end_obligation,
         las.beg_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   beg_lt_obligation,
         las.end_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   end_lt_obligation,
         las.principal_remeasurement * Nvl( CALC_RATE.rate, cr.rate )                                                                                             principal_remeasurement,
         las.beg_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                       beg_liability,
         las.end_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                       end_liability,
         las.beg_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    beg_lt_liability,
         las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    end_lt_liability,
         las.liability_remeasurement * Nvl( CALC_RATE.rate, cr.rate )                                                                                             liability_remeasurement,
         las.interest_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    interest_accrual,
         las.principal_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   principal_accrual,
         las.interest_paid * Nvl( CALC_RATE.rate, cr.rate )                                                                                                       interest_paid,
         las.principal_paid * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      principal_paid,
         las.executory_accrual1 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual1,
         las.executory_accrual2 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual2,
         las.executory_accrual3 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual3,
         las.executory_accrual4 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual4,
         las.executory_accrual5 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual5,
         las.executory_accrual6 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual6,
         las.executory_accrual7 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual7,
         las.executory_accrual8 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual8,
         las.executory_accrual9 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual9,
         las.executory_accrual10 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 executory_accrual10,
         las.executory_paid1 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid1,
         las.executory_paid2 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid2,
         las.executory_paid3 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid3,
         las.executory_paid4 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid4,
         las.executory_paid5 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid5,
         las.executory_paid6 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid6,
         las.executory_paid7 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid7,
         las.executory_paid8 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid8,
         las.executory_paid9 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid9,
         las.executory_paid10 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    executory_paid10,
         las.contingent_accrual1 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual1,
         las.contingent_accrual2 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual2,
         las.contingent_accrual3 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual3,
         las.contingent_accrual4 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual4,
         las.contingent_accrual5 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual5,
         las.contingent_accrual6 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual6,
         las.contingent_accrual7 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual7,
         las.contingent_accrual8 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual8,
         las.contingent_accrual9 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual9,
         las.contingent_accrual10 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                contingent_accrual10,
         las.contingent_paid1 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid1,
         las.contingent_paid2 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid2,
         las.contingent_paid3 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid3,
         las.contingent_paid4 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid4,
         las.contingent_paid5 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid5,
         las.contingent_paid6 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid6,
         las.contingent_paid7 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid7,
         las.contingent_paid8 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid8,
         las.contingent_paid9 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid9,
         las.contingent_paid10 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   contingent_paid10,
         las.current_lease_cost * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                             current_lease_cost,
         las.beg_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   beg_deferred_rent,
         las.deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                       deferred_rent,
         las.end_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   end_deferred_rent,
         las.beg_st_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                beg_st_deferred_rent,
         las.end_st_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                end_st_deferred_rent,
         las.initial_direct_cost * Nvl( CALC_RATE.rate, cr.rate )                                                                                                initial_direct_cost,
         las.incentive_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                                                incentive_amount,
         Decode( CALC_RATE.rate, NULL, 0,
                                 ( las.beg_obligation * ( CALC_RATE.rate - Coalesce( CALC_RATE.prev_rate, las.in_service_exchange_rate, CALC_RATE.rate, 0 ) ) ) ) gain_loss_fx,
         las.depr_expense * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                                   depr_expense,
         las.begin_reserve * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                                  begin_reserve,
         las.end_reserve * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                                    end_reserve,
         las.depr_exp_alloc_adjust * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                          depr_exp_alloc_adjust,
         las.asset_description                                                                                                                                    asset_description,
         las.leased_asset_number                                                                                                                                  leased_asset_number,
         las.fmv * Nvl( CALC_RATE.rate, cr.rate )                                                                                                                 fmv,
         las.is_om                                                                                                                                                is_om,
         las.approved_revision                                                                                                                                    approved_revision,
         las.lease_cap_type_id                                                                                                                                    lease_cap_type_id,
         las.ls_asset_status_id                                                                                                                                   ls_asset_status_id,
         las.retirement_date                                                                                                                                      retirement_date,
         las.beg_prepaid_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    beg_prepaid_rent,
         las.prepay_amortization * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 prepay_amortization,
         las.prepaid_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                        prepaid_rent,
         las.end_prepaid_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    end_prepaid_rent,
         nvl(las.beg_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate ),0)                                       beg_net_rou_asset,
         nvl(las.end_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate ),0)                                       end_net_rou_asset,
		 las.actual_residual_amount * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                         actual_residual_amount,
         las.guaranteed_residual_amount * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                     guaranteed_residual_amount,
         (las.fmv * las.estimated_residual) * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                 estimated_residual,
		 nvl(las.rou_asset_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate ),0)                                 rou_asset_remeasurement,
		 nvl(las.PARTIAL_TERM_GAIN_LOSS * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate ),0)                                 PARTIAL_TERM_GAIN_LOSS,
     las.beg_arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 beg_arrears_accrual,
     las.arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 arrears_accrual,
     las.end_arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 end_arrears_accrual
  FROM   ( SELECT la.ilr_id,
                  las.ls_asset_id,
                  las.revision,
                  las.set_of_books_id,
                  las.month,
                  la.contract_currency_id,
                  las.residual_amount,
                  las.term_penalty,
                  las.bpo_price,
                  las.beg_capital_cost,
                  las.end_capital_cost,
                  las.beg_obligation,
                  las.end_obligation,
                  las.beg_lt_obligation,
                  las.end_lt_obligation,
                  las.principal_remeasurement,
                  las.beg_liability,
                  las.end_liability,
                  las.beg_lt_liability,
                  las.end_lt_liability,
                  las.liability_remeasurement,
                  las.interest_accrual,
                  las.principal_accrual,
                  las.interest_paid,
                  las.principal_paid,
                  las.executory_accrual1,
                  las.executory_accrual2,
                  las.executory_accrual3,
                  las.executory_accrual4,
                  las.executory_accrual5,
                  las.executory_accrual6,
                  las.executory_accrual7,
                  las.executory_accrual8,
                  las.executory_accrual9,
                  las.executory_accrual10,
                  las.executory_paid1,
                  las.executory_paid2,
                  las.executory_paid3,
                  las.executory_paid4,
                  las.executory_paid5,
                  las.executory_paid6,
                  las.executory_paid7,
                  las.executory_paid8,
                  las.executory_paid9,
                  las.executory_paid10,
                  las.contingent_accrual1,
                  las.contingent_accrual2,
                  las.contingent_accrual3,
                  las.contingent_accrual4,
                  las.contingent_accrual5,
                  las.contingent_accrual6,
                  las.contingent_accrual7,
                  las.contingent_accrual8,
                  las.contingent_accrual9,
                  las.contingent_accrual10,
                  las.contingent_paid1,
                  las.contingent_paid2,
                  las.contingent_paid3,
                  las.contingent_paid4,
                  las.contingent_paid5,
                  las.contingent_paid6,
                  las.contingent_paid7,
                  las.contingent_paid8,
                  las.contingent_paid9,
                  las.contingent_paid10,
                  las.current_lease_cost,
                  las.beg_deferred_rent,
                  las.deferred_rent,
                  las.end_deferred_rent,
                  las.beg_st_deferred_rent,
                  las.end_st_deferred_rent,
                  las.initial_direct_cost,
                  las.incentive_amount,
                  ldf.depr_expense,
                  ldf.begin_reserve,
                  ldf.end_reserve,
                  ldf.depr_exp_alloc_adjust,
                  la.company_id,
                  opt.in_service_exchange_rate,
                  la.description AS asset_description,
                  la.leased_asset_number,
                  la.fmv,
                  las.is_om,
                  la.approved_revision,
                  opt.lease_cap_type_id,
                  la.ls_asset_status_id,
                  la.retirement_date,
                  las.beg_prepaid_rent,
                  las.prepay_amortization,
                  las.prepaid_rent,
                  las.end_prepaid_rent,
                  las.beg_net_rou_asset,
                  las.end_net_rou_asset,
                  la.actual_residual_amount,
                  la.guaranteed_residual_amount,
                  la.estimated_residual,
                  las.rou_asset_remeasurement,
                  las.partial_term_gain_loss,
                  las.beg_arrears_accrual,
                  las.arrears_accrual,
                  las.end_arrears_accrual
           FROM   LS_ASSET_SCHEDULE las,
                  LS_ASSET la,
                  LS_ILR_OPTIONS opt,
                  LS_DEPR_FORECAST ldf
           WHERE  las.ls_asset_id = la.ls_asset_id AND
                  la.ilr_id = opt.ilr_id AND
                  las.revision = opt.revision AND
                  las.ls_asset_id = ldf.ls_asset_id (+) AND
                  las.revision = ldf.revision (+) AND
                  las.set_of_books_id = ldf.set_of_books_id (+) AND
                  las.month = ldf.month (+) ) las
         inner join CURRENCY_SCHEMA cs
                 ON las.company_id = cs.company_id
         inner join cur
                 ON CUR.currency_id = CASE CUR.ls_cur_type
                                        WHEN 1 THEN las.contract_currency_id
                                        WHEN 2 THEN cs.currency_id
                                        ELSE NULL
                                      END
         inner join open_month
                 ON las.company_id = OPEN_MONTH.company_id
         inner join CURRENCY_RATE_DEFAULT_DENSE cr
                 ON CUR.currency_id = cr.currency_to AND
                    las.contract_currency_id = cr.currency_from AND
                    Trunc( cr.exchange_date, 'MONTH' ) = Trunc( las.month, 'MONTH' )
         inner join cr_now
                 ON CUR.currency_id = CR_NOW.currency_to AND
                    las.contract_currency_id = CR_NOW.currency_from
         left outer join calc_rate
                      ON las.contract_currency_id = CALC_RATE.contract_currency_id AND
                         CUR.currency_id = CALC_RATE.company_currency_id AND
                         las.company_id = CALC_RATE.company_id AND
                         las.month = CALC_RATE.accounting_month
  WHERE  cs.currency_type_id = 1 AND
         cr.exchange_rate_type_id = 1;




CREATE OR REPLACE VIEW V_LS_ASSET_REMEASUREMENT_AMTS AS
SELECT stg.ilr_id, stg.revision, sch.ls_asset_id, sch.set_of_books_id, o.remeasurement_date, ilr.current_revision,
CASE
  WHEN sch.is_om = 1 AND llct.is_om = 0 THEN
    1
  ELSE
    0
END om_to_cap_indicator,
sch.end_obligation AS prior_month_end_obligation, sch.end_liability AS prior_month_end_liability,
sch.end_lt_obligation AS prior_month_end_lt_obligation, sch.end_lt_liability AS prior_month_end_lt_liability,
sch.end_deferred_rent AS prior_month_end_deferred_rent,
sch.end_prepaid_rent AS prior_month_end_prepaid_rent,
depr.end_reserve AS prior_month_end_depr_reserve,
sch.end_capital_cost - depr.end_reserve AS prior_month_end_nbv,
sch.end_capital_cost AS prior_month_end_capital_cost,
sch.end_net_rou_asset as prior_month_net_rou_asset,
sch.end_arrears_accrual AS prior_month_end_arrears_accr,
1 is_remeasurement
FROM ls_ilr_stg stg
  JOIN ls_ilr ilr ON stg.ilr_id = ilr.ilr_id
  JOIN ls_ilr_options o ON stg.ilr_id = o.ilr_id AND stg.revision = o.revision
  JOIN ls_lease_cap_type llct ON llct.ls_lease_cap_type_id = o.lease_cap_type_id
  JOIN ls_ilr_asset_map map ON map.ilr_id = stg.ilr_id AND map.revision = ilr.current_revision
  JOIN ls_asset_schedule sch ON map.ls_asset_id = sch.ls_asset_id AND sch.revision = ilr.current_revision AND sch.set_of_books_id = stg.set_of_books_id
  left OUTER JOIN ls_depr_forecast depr ON depr.ls_asset_id = map.ls_asset_id AND depr.revision = ilr.current_revision AND depr.set_of_books_id = stg.set_of_books_id
WHERE ilr.current_revision <> stg.revision
AND o.remeasurement_date IS NOT NULL
AND Trunc(o.remeasurement_date, 'month') = Add_Months(sch.MONTH,1)
AND (Trunc(o.remeasurement_date, 'month') = Add_Months(depr.MONTH,1) OR depr.MONTH IS NULL)
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7022, 0, 2017, 4, 0, 0, 51723, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051723_lessee_01_add_arrears_accrual_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;