SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029908_sys_add_new_post_errors.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 10.4.1.0   08/09/2013 Charlie Shilling
||============================================================================
*/

declare
   V_COUNT number(22, 0);

begin

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1024';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1024',
          'POST 1024: ERROR: The FROM company has sets of books not found in the TO company. Post cannot process intercompany transfers with misaligned sets of books. FROM company_id: ### TO company_id: ###',
          'POST 1024: ERROR: The FROM company has sets of books not found in the TO company. Post cannot process intercompany transfers with misaligned sets of books. FROM company_id: ### TO company_id: ###',
          'POST 1024: Change the set of books configuration so that both the TO and FROM company have the same sets of books.');

      DBMS_OUTPUT.PUT_LINE('POST 1024 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1024 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1025';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1025',
          'POST 1025: ERROR: The TO company has sets of books not found in the FROM company. Post cannot process intercompany transfers with misaligned sets of books. FROM company_id: ### TO company_id: ###',
          'POST 1025: ERROR: The TO company has sets of books not found in the FROM company. Post cannot process intercompany transfers with misaligned sets of books. FROM company_id: ### TO company_id: ###',
          'POST 1025: Change the set of books configuration so that both the TO and FROM company have the same sets of books.');

      DBMS_OUTPUT.PUT_LINE('POST 1025 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1025 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1026';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1026',
          'POST 1026: ERROR: Retirement quantity ### is greater than the total quantity remaining ###.',
          'POST 1026: ERROR: Retirement quantity ### is greater than the total quantity remaining ###.',
          'POST 1026: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1026 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1026 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1027';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1027',
          'POST 1027: ERROR: Reserve amount ### given on pend_transaction or pend_transaction_set_of_books for set_of_books ###  but the depr_res_allo_factor.factor value is 0, so life reserve and COR reserve cannot be calculated.',
          'POST 1027: ERROR: Reserve amount ### given on pend_transaction or pend_transaction_set_of_books for set_of_books ###  but the depr_res_allo_factor.factor value is 0, so life reserve and COR reserve cannot be calculated.',
          'POST 1027: Correct the factors on depr_res_allo_factors.');

      DBMS_OUTPUT.PUT_LINE('POST 1027 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1027 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1028';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1028',
          'POST 1028: ERROR: Replacement amount cannot be null for Handy-Whitman curve retirements.',
          'POST 1028: ERROR: Replacement amount cannot be null for Handy-Whitman curve retirements.',
          'POST 1028: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1028 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1028 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1029';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1029',
          'POST 1029: ERROR: Replacement amount cannot be null for Handy-Whitman FIFO retirements.',
          'POST 1029: ERROR: Replacement amount cannot be null for Handy-Whitman FIFO retirements.',
          'POST 1029: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1029 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1029 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1030';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1030',
          'POST 1030: ERROR: A URET transaction must have a ldg_asset_id if the either the posting_amount or posting_quantity is non-zero. ',
          'POST 1030: ERROR: A URET transaction must have a ldg_asset_id if the either the posting_amount or posting_quantity is non-zero. ',
          'POST 1030: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1030 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1030 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1031';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1031',
          'POST 1031: ERROR: CWIP cost of removal ### not equal to pend transaction cost of removal ### for work order number XXXXX.',
          'POST 1031: ERROR: CWIP cost of removal ### not equal to pend transaction cost of removal ### for work order number XXXXX.',
          'POST 1031: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1031 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1031 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1032';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1032',
          'POST 1032: ERROR: CWIP salvage cash ### not equal to pend transaction salvage cash ### for work order number XXXXX.',
          'POST 1032: ERROR: CWIP salvage cash ### not equal to pend transaction salvage cash ### for work order number XXXXX.',
          'POST 1032: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1032 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1032 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1033';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1033',
          'POST 1033: ERROR: CWIP salvage returns ### not equal to pend transaction salvage returns ### for work order number XXXXX.',
          'POST 1033: ERROR: CWIP salvage returns ### not equal to pend transaction salvage returns ### for work order number XXXXX.',
          'POST 1033: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1033 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1033 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1034';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1034',
          'POST 1034: ERROR: CWIP reserve credits ### not equal to pend transaction reserve credits ### for work order number XXXXX.',
          'POST 1034: ERROR: CWIP reserve credits ### not equal to pend transaction reserve credits ### for work order number XXXXX.',
          'POST 1034: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1034 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1034 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1035';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1035',
          'POST 1035: Error: Company ID ### could not be found in COMPANY_SETUP table.',
          'POST 1035: Error: Company ID ### could not be found in COMPANY_SETUP table.',
          'POST 1035: The company_id specified on the Post command line is not in the COMPANY_SETUP table.');

      DBMS_OUTPUT.PUT_LINE('POST 1035 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1035 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1036';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1036',
          'POST 1036: Error: Post is already running for company_id ###. Running session info: �',
          'POST 1036: Error: Post is already running for company_id ###. Running session info: �',
          'POST 1036: Do not run multiple copies of Post for the same company at the same time. If a previous session disconnected improperly, a DBA may need to kill that session.');

      DBMS_OUTPUT.PUT_LINE('POST 1036 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1036 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1037';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1037',
          'POST 1037: Error: Cannot start Post for all companies because Post is already running for one or more specific companies. Running session info: ...',
          'POST 1037: Error: Cannot start Post for all companies because Post is already running for one or more specific companies. Running session info: ...',
          'POST 1037: Do not run Post for all companies if another copy of Post is already running for any specific company. If a previous session disconnected improperly, a DBA may need to kill that session.');

      DBMS_OUTPUT.PUT_LINE('POST 1037 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1037 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1038';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1038',
          'POST 1038: Error: Cannot start Post for company_id ### because Post is already running for all companies. Running session info: �',
          'POST 1038: Error: Cannot start Post for company_id ### because Post is already running for all companies. Running session info: �',
          'POST 1038: Do not run Post for a specific company if another copy of Post is already running for all companies. If a previous session disconnected improperly, a DBA may need to kill that session.');

      DBMS_OUTPUT.PUT_LINE('POST 1038 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1038 error already exists in pp_system_errors table.');
   end if;

   select count(1)
     into V_COUNT
     from PP_SYSTEM_ERRORS
    where UPPER(trim(DESCRIPTION)) = 'POST 1039A';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1039a',
          'POST 1039a: BOOK_SUMMARY record has an Asset Impairment book_summary_type but a NULL book_summary_id.',
          'POST 1039a: BOOK_SUMMARY record has an Asset Impairment book_summary_type but a NULL book_summary_id.',
          'POST 1039a: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1039a error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1039a error already exists in pp_system_errors table.');
   end if;

   select count(1)
     into V_COUNT
     from PP_SYSTEM_ERRORS
    where UPPER(trim(DESCRIPTION)) = 'POST 1039B';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1039b',
          'POST 1039b: BOOK_SUMMARY record has an Asset Impairment book_summary_type but a NULL book_summary_id.',
          'POST 1039b: BOOK_SUMMARY record has an Asset Impairment book_summary_type but a NULL book_summary_id.',
          'POST 1039b: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1039b error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1039b error already exists in pp_system_errors table.');
   end if;

   select count(1)
     into V_COUNT
     from PP_SYSTEM_ERRORS
    where UPPER(trim(DESCRIPTION)) = 'POST 1040A';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1040a',
          'POST 1040a: Only one impairment basis bucket is allowed per set of books, but ### were found for set_of_books_id ###',
          'POST 1040a: Only one impairment basis bucket is allowed per set of books, but ### were found for set_of_books_id ###',
          'POST 1040a: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1040a error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1040a error already exists in pp_system_errors table.');
   end if;

   select count(1)
     into V_COUNT
     from PP_SYSTEM_ERRORS
    where UPPER(trim(DESCRIPTION)) = 'POST 1040B';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1040b',
          'POST 1040b: Only one impairment basis bucket is allowed per set of books, but ### were found for set_of_books_id ###',
          'POST 1040b: Only one impairment basis bucket is allowed per set of books, but ### were found for set_of_books_id ###',
          'POST 1040b: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1040b error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1040b error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1041';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1041',
          'POST 1041: Error: PKG_LEASE_ASSET_POST.F_RETIRE_ASSET returned an error. Message: XXXXX',
          'POST 1041: Error: PKG_LEASE_ASSET_POST.F_RETIRE_ASSET returned an error. Message: XXXXX',
          'POST 1041: Unknown error returned from a lease-specific database function. Call PPC. ');

      DBMS_OUTPUT.PUT_LINE('POST 1041 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1041 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1042';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1042',
          'POST 1042: Error: PKG_LEASE_ASSET_POST.F_TRANSFER_ASSET_FROM returned an error.  Message: XXXXX',
          'POST 1042: Error: PKG_LEASE_ASSET_POST.F_TRANSFER_ASSET_FROM returned an error.  Message: XXXXX',
          'POST 1042: Unknown error returned from a lease-specific database function. Call PPC. ');

      DBMS_OUTPUT.PUT_LINE('POST 1042 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1042 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1043';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1043',
          'POST 1043: Error: PKG_LEASE_ASSET_POST.F_TRANSFER_ASSET_TO returned an error. Message: XXXXX',
          'POST 1043: Error: PKG_LEASE_ASSET_POST.F_TRANSFER_ASSET_TO returned an error. Message: XXXXX',
          'POST 1043: Unknown error returned from a lease-specific database function. Call PPC. ');

      DBMS_OUTPUT.PUT_LINE('POST 1043 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1043 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1044';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1044',
          'POST 1044: Error: PKG_LEASE_ASSET_POST.F_ADJUST_ASSET returned an error. Message: XXXXX',
          'POST 1044: Error: PKG_LEASE_ASSET_POST.F_ADJUST_ASSET returned an error. Message: XXXXX',
          'POST 1044: Unknown error returned from a lease-specific database function. Call PPC. ');

      DBMS_OUTPUT.PUT_LINE('POST 1044 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1044 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1045';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1045',
          'POST 1045: Error: Lease Retirement Debit Account not Found.',
          'POST 1045: Error: Lease Retirement Debit Account not Found.',
          'POST 1045: PP_GL_TRANSACTION threw an error. Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1045 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1045 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1046';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1046',
          'POST 1046: Error: Lease Retirement Credit Account not Found',
          'POST 1046: Error: Lease Retirement Credit Account not Found',
          'POST 1046: PP_GL_TRANSACTION threw an error. Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1046 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1046 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1047';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1047',
          'POST 1047: Error: Reserve Debit Account not Found',
          'POST 1047: Error: Reserve Debit Account not Found', 'POST 1047: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1047 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1047 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1048';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1048',
          'POST 1048: Error: gain/loss Debit Account not Found',
          'POST 1048: Error: gain/loss Debit Account not Found', 'POST 1048: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1048 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1048 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1049';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1049',
          'POST 1049:  Error: Credit Account for gain/loss  not Found',
          'POST 1049:  Error: Credit Account for gain/loss  not Found', 'POST 1049: Call PPC. ');

      DBMS_OUTPUT.PUT_LINE('POST 1049 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1049 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1050';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1050',
          'POST 1050:  Error: Lease gain/loss Debit Account not Found',
          'POST 1050:  Error: Lease gain/loss Debit Account not Found',
          'POST 1050: PP_GL_TRANSACTION threw an error. Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1050 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1050 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1051';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1051',
          'POST 1051:  Error: Lease gain/loss Credit Account not Found',
          'POST 1051:  Error: Lease gain/loss Credit Account not Found',
          'POST 1051: PP_GL_TRANSACTION threw an error. Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1051 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1051 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1052';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1052',
          'POST 1052:  Error: Cannot process gain/loss as depr expense adjust after the Depreciation has been approved',
          'POST 1052:  Error: Cannot process gain/loss as depr expense adjust after the Depreciation has been approved',
          'POST 1052: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1052 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1052 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1053';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1053',
          'POST 1053:  Error: Transfer FROM Reserve Account not Found',
          'POST 1053:  Error: Transfer FROM Reserve Account not Found', 'POST 1053: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1053 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1053 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1054';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1054',
          'POST 1054:  Error: Transfer TO Reserve Account not Found',
          'POST 1054:  Error: Transfer TO Reserve Account not Found', 'POST 1054: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1054 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1054 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1055';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1055',
          'POST 1055:  Error: Cannot Find Receivable GL account ',
          'POST 1055:  Error: Cannot Find Receivable GL account ', 'POST 1055: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1055 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1055 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1056';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1056',
          'POST 1056:  Error: Cannot Find Payable GL account',
          'POST 1056:  Error: Cannot Find Payable GL account', 'POST 1056: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1056 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1056 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1057';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1057',
          'POST 1057:  Error: Lease Transfer_to Debit Account not Found',
          'POST 1057:  Error: Lease Transfer_to Debit Account not Found', 'POST 1057: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1057 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1057 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1058';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1058',
          'POST 1058:  Error: Transfer_from Credit Account not Found',
          'POST 1058:  Error: Transfer_from Credit Account not Found', 'POST 1058: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1058 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1058 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1059';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1059',
          'POST 1059:  Error: Lease Intercompany Transfer_to Debit Account not Found',
          'POST 1059:  Error: Lease Intercompany Transfer_to Debit Account not Found',
          'POST 1059: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1059 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1059 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1060';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1060',
          'POST 1060: Error: Lessee transactions (subledger_indicator = -100) can only be URET, URGL, UTRF, or UTRT activity codes. ',
          'POST 1060: Error: Lessee transactions (subledger_indicator = -100) can only be URET, URGL, UTRF, or UTRT activity codes. ',
          'POST 1060: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1060 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1060 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1061';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1061',
          'POST 1061: Error: Transfers of leased assets must have the "Transfer" FERC Activity Code. TO FERC Activity Code: ### FROM FERC Activity Code: ###',
          'POST 1061: Error: Transfers of leased assets must have the "Transfer" FERC Activity Code. TO FERC Activity Code: ### FROM FERC Activity Code: ###',
          'POST 1061: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1061 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1061 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1064';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1064',
          'POST 1064: Error: Another copy of Post is already launching company-specific Post versions. ',
          'POST 1064: Error: Another copy of Post is already launching company-specific Post versions. ',
          'POST 1064: If a session disconnected improperly, a DBA may need to KILL the session. Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1064 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1064 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1065';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1065',
          'POST 1065: Error: PP_CPR_PKG.F_GET_ASSET_ACTIVITY_ID returned a NULL value. ',
          'POST 1065: Error: PP_CPR_PKG.F_GET_ASSET_ACTIVITY_ID returned a NULL value. ',
          'POST 1065: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1065 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1065 error already exists in pp_system_errors table.');
   end if;

   select count(1) into V_COUNT from PP_SYSTEM_ERRORS where UPPER(trim(DESCRIPTION)) = 'POST 1066';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_ERRORS
         (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
      values
         ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1066',
          'POST 1066: Error: PP_CPR_PKG.F_GET_ASSET_ACTIVITY_ID returned an invalid asset_activity_id:  ###',
          'POST 1066: Error: PP_CPR_PKG.F_GET_ASSET_ACTIVITY_ID returned an invalid asset_activity_id:  ###',
          'POST 1066: Call PPC.');

      DBMS_OUTPUT.PUT_LINE('POST 1066 error inserted successfully into pp_system_errors.');
   else
      DBMS_OUTPUT.PUT_LINE('POST 1066 error already exists in pp_system_errors table.');
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (557, 0, 10, 4, 1, 0, 29908, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029908_sys_add_new_post_errors.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
