/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_052042_lessee_03_MLA_query_new_cols_DML.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0   07/20/2018   AJSmith      Add many new columns to Asset Schedule queries 
 ||============================================================================
 */ 

DECLARE

  query_id number;
  
BEGIN
-- Lease Asset Schedule By MLA
  select id into query_id from pp_any_query_criteria 
  WHERE description = 'Lease Asset Schedule By MLA';
  
-- Principal Remeasurement
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'principal_remeasurement',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Principal Remeasurement',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'principal_remeasurement')
	;
     
-- Liability Remeasurement
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'liability_remeasurement',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Liability Remeasurement',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'liability_remeasurement')
	;
     
--Beg Net ROU Asset
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_net_rou_asset',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg Net ROU Asset',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_net_rou_asset')
	;

-- ROU Asset Remeasurement
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'rou_asset_remeasurement',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'ROU Asset Remeasurement',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'rou_asset_remeasurement')
	;

-- End Net ROU Asset
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_net_rou_asset',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Net ROU Asset',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_net_rou_asset')
	;

-- Initial Direct Cost
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'initial_direct_cost',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Initial Direct Cost',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'initial_direct_cost')
	;

-- Incentive Amount 
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'incentive_amount',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Incentive Amount',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'incentive_amount')
	;

-- Beg Prepaid Rent
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_prepaid_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg Prepaid Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_prepaid_rent')
	;

-- Prepay Amortization
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'prepay_amortization',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Prepay Amortization',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'prepay_amortization')
	;

-- Prepaid Rent
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'prepaid_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Prepaid Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'prepaid_rent')
	;

-- End Prepay Rent
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_prepaid_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Prepaid Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_prepaid_rent')
	;

-- Beg Arrears Accrual
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_arrears_accrual',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg Arrears Accrual',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_arrears_accrual')
	;

-- Arrears Accrual
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'arrears_accrual',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Arrears Accrual',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'arrears_accrual')
	;

-- End Arrears Accrual
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_arrears_accrual',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Arrears Accrual',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_arrears_accrual')
	;

-- Executory Adjust
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'executory_adjust',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Executory Adjust',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'executory_adjust')
	;

-- Contingent Adjust
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'contingent_adjust',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Contingent Adjust',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'contingent_adjust')
	;

-- Begin Reserve
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'begin_reserve',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Begin Reserve',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'begin_reserve')
	;

-- Depr Expense
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'depr_expense',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Depr Expense',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'depr_expense')
	;

-- Depr Expense Adjust
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'depr_exp_alloc_adjust',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Depr Expense Adjust',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'depr_exp_alloc_adjust')
	;

-- End Reserve
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_reserve',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Reserve',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_reserve')
	;


  update pp_any_query_criteria
  set SQL = 'SELECT la.ls_asset_id,la.leased_asset_number,co.company_id,co.description AS company_description,
     ilr.ilr_id,ilr.ilr_number,ll.lease_id,ll.lease_number,lct.description AS lease_cap_type,al.long_description AS location,
     las.REVISION,las.SET_OF_BOOKS_ID,TO_CHAR(las.MONTH, ''yyyymm'') AS monthnum,lcurt.DESCRIPTION AS currency_type,
     las.iso_code currency,las.currency_display_symbol,las.BEG_CAPITAL_COST,las.END_CAPITAL_COST,las.BEG_OBLIGATION,
     las.END_OBLIGATION,las.BEG_LT_OBLIGATION,las.END_LT_OBLIGATION,las.INTEREST_ACCRUAL,las.PRINCIPAL_ACCRUAL,
     las.INTEREST_PAID,las.PRINCIPAL_PAID,las.EXECUTORY_ACCRUAL1,las.EXECUTORY_ACCRUAL2,las.EXECUTORY_ACCRUAL3,
     las.EXECUTORY_ACCRUAL4,las.EXECUTORY_ACCRUAL5,las.EXECUTORY_ACCRUAL6,las.EXECUTORY_ACCRUAL7,las.EXECUTORY_ACCRUAL8,
     las.EXECUTORY_ACCRUAL9,las.EXECUTORY_ACCRUAL10,las.EXECUTORY_PAID1,las.EXECUTORY_PAID2,las.EXECUTORY_PAID3,
     las.EXECUTORY_PAID4,las.EXECUTORY_PAID5,las.EXECUTORY_PAID6,las.EXECUTORY_PAID7,las.EXECUTORY_PAID8,
     las.EXECUTORY_PAID9,las.EXECUTORY_PAID10,las.CONTINGENT_ACCRUAL1,las.CONTINGENT_ACCRUAL2,las.CONTINGENT_ACCRUAL3,
     las.CONTINGENT_ACCRUAL4,las.CONTINGENT_ACCRUAL5,las.CONTINGENT_ACCRUAL6,las.CONTINGENT_ACCRUAL7,
     las.CONTINGENT_ACCRUAL8,las.CONTINGENT_ACCRUAL9,las.CONTINGENT_ACCRUAL10,las.CONTINGENT_PAID1,
     las.CONTINGENT_PAID2,las.CONTINGENT_PAID3,las.CONTINGENT_PAID4,las.CONTINGENT_PAID5,las.CONTINGENT_PAID6,
     las.CONTINGENT_PAID7,las.CONTINGENT_PAID8,las.CONTINGENT_PAID9,las.CONTINGENT_PAID10,las.IS_OM,
     las.CURRENT_LEASE_COST,las.RESIDUAL_AMOUNT,las.TERM_PENALTY,las.BPO_PRICE,las.BEG_DEFERRED_RENT,
     las.DEFERRED_RENT,las.END_DEFERRED_RENT,las.BEG_LIABILITY,las.END_LIABILITY,las.BEG_LT_LIABILITY, las.END_LT_LIABILITY,
     las.principal_remeasurement, las.liability_remeasurement, las.beg_net_rou_asset, las.rou_asset_remeasurement, las.end_net_rou_asset, 
     las.initial_direct_cost, las.incentive_amount, las.beg_prepaid_rent, las.prepay_amortization, las.prepaid_rent, las.end_prepaid_rent, 
     las.beg_arrears_accrual, las.arrears_accrual, las.end_arrears_accrual,las.executory_adjust, las.contingent_adjust, 
     ldf.begin_reserve, ldf.depr_expense, ldf.depr_exp_alloc_adjust, ldf.end_reserve
    FROM ls_asset la,ls_ilr ilr,ls_lease ll,company co,asset_location al,
      ls_lease_cap_type lct,ls_ilr_options ilro,v_ls_asset_schedule_fx_vw las,ls_lease_currency_type lcurt, ls_depr_forecast ldf
    WHERE la.ilr_id             = ilr.ilr_id
    AND ilr.lease_id            = ll.lease_id
    AND ilr.ilr_id              = ilro.ilr_id
    AND ilr.current_revision    = ilro.revision
    AND ilro.lease_cap_type_id  = lct.ls_lease_cap_type_id
    AND la.asset_location_id    = al.asset_location_id
    AND la.company_id           = co.company_id
    AND las.ls_asset_id         = la.ls_asset_id
    AND las.ls_cur_type         = lcurt.ls_currency_type_id
    AND las.revision            = la.approved_revision
    and ldf.ls_asset_id (+) = las.ls_asset_id and ldf.revision (+) = las.revision
    and ldf.set_of_books_id (+) = las.set_of_books_id and ldf.month (+) = las.month 
    AND upper(ll.lease_number) IN 
      (SELECT upper(filter_value)
      FROM pp_any_required_filter
      WHERE upper(trim(column_name)) = ''LEASE NUMBER''
      )
    AND UPPER(TRIM(lcurt.DESCRIPTION)) IN
      (SELECT UPPER(filter_value)
      FROM pp_any_required_filter
      WHERE Upper(Trim(column_name)) = ''CURRENCY TYPE'')'
  where id=query_id
  ;
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8503, 0, 2017, 4, 0, 0, 52042, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052042_lessee_03_MLA_query_new_cols_DML.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

