/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044550_cpr_asset_transfer_sob_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 08/03/2015 Anand R 		 	insert new value for reserve and adjusted reserve
||                                          when assets transfers with companies with different set of books.
||========================================================================================
*/


CREATE OR REPLACE TRIGGER PWRPLANT."PEND_TRANSACTION_SET_OF_BOOKS"
   before update or insert on PWRPLANT."PEND_TRANSACTION_SET_OF_BOOKS"
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANSACTION_SET_OF_BOOKS
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 2015.2.0 08/03/2015 Anand R          PP-44550
   ||============================================================================
   */
declare 
rcount integer;     
book1res number;
book1amount number;
begin   
   
  if updating then
    :NEW.user_id := SYS_CONTEXT ('USERENV', 'SESSION_USER');    :NEW.time_stamp := SYSDATE; 
  end if;

  if inserting then
     if :NEW.activity_code = 'UTRT' then
      select count(*) into rcount 
      from company_set_of_books 
      where company_id = (select company_id from pend_transaction where pend_trans_id = 
                             (select ldg_asset_id from pend_transaction where pend_trans_id = :NEW.pend_trans_id) ) and
            set_of_books_id = :NEW.set_of_books_id ;
            
       if rcount = 0 then
         select reserve, posting_amount into book1res, book1amount 
         from pend_transaction
         where pend_trans_id = (select ldg_asset_id from pend_transaction where pend_trans_id = :NEW.pend_trans_id) ;
      
         :NEW.reserve := :NEW.posting_amount * (book1res / book1amount );
         
         :NEW.adjusted_reserve := :NEW.posting_amount * (book1res / book1amount );
        
         :NEW.user_id := SYS_CONTEXT ('USERENV', 'SESSION_USER');    :NEW.time_stamp := SYSDATE;

       end if;
     end if; 
  end if;
  
end;

/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2736, 0, 2015, 2, 0, 0, 044550, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044550_cpr_asset_transfer_sob_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;