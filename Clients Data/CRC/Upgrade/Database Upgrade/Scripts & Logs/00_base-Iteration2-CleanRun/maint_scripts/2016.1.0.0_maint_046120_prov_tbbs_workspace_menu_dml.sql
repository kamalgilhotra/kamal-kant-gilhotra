/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046120_prov_tbbs_workspace_menu_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version	  Date	     Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 09/22/2016 Jared Watkins  Update the menu order for the TBBS workspaces
||============================================================================
*/
update ppbase_menu_items set item_order = 5
where module = 'provision' and menu_identifier = 'tbbs_admin';

update ppbase_menu_items set menu_level = 1, item_order = 4, parent_menu_identifier = null
where module = 'provision' and menu_identifier = 'tbbs_assignment_view';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3294, 0, 2016, 1, 0, 0, 046120, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046120_prov_tbbs_workspace_menu_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;