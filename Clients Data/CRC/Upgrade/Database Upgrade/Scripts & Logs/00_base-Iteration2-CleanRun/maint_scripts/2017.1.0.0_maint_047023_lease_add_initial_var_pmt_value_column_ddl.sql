/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047023_lease_add_initial_var_pmt_value_column_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2017.1.0.0 03/09/2017 David Haupt		 Add column to ls_ilr_payment_term_var_paymnt
||											 to store VP initial value
||============================================================================
*/ 

alter table ls_ilr_payment_term_var_paymnt add initial_var_payment_value number(22,2);

COMMENT ON COLUMN LS_ILR_PAYMENT_TERM_VAR_PAYMNT.INITIAL_VAR_PAYMENT_VALUE is 'Location to store initially calculated value for a variable payment included in initial measure';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3385, 0, 2017, 1, 0, 0, 47023, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047023_lease_add_initial_var_pmt_value_column_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	