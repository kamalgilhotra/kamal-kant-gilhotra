/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009126_sys_charge_type_flags.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   08/20/2013 Stephen Motter  Point Release
||============================================================================
*/

alter table ESTIMATE_CHARGE_TYPE
   add (UNIT_EST    number(1) default 1,
        MONTHLY_EST number(1) default 1);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    COLUMN_RANK)
values
   ('unit_est', 'estimate_charge_type', 'yes_no', 'p', 'Show item in unit estimates',
    'Specifies whether the estimate charge type shows up in Unit Estimates.', 61);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    COLUMN_RANK)
values
   ('monthly_est', 'estimate_charge_type', 'yes_no', 'p', 'Show item in grid estimates',
    'Specifies whether the estimate charge type shows up in Grid Estimates.', 62);

comment on column ESTIMATE_CHARGE_TYPE.UNIT_EST is 'Specifies whether the estimate charge type shows up in Unit Estimates. 1 = Y, 0 = N.';
comment on column ESTIMATE_CHARGE_TYPE.MONTHLY_EST is 'Specifies whether the estimate charge type shows up in Grid Estimates. 1 = Y, 0 = N.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (520, 0, 10, 4, 1, 0, 9126, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009126_sys_charge_type_flags.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
