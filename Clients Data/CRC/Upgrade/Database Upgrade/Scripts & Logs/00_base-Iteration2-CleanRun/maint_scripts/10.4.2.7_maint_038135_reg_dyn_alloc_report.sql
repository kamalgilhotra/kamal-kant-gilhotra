/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038135_reg_dyn_alloc_report.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.7 09/23/2014 Shane Ward
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID)
   select max(REPORT_ID) + 1,
          'Case Dynamic Allocator Report',
          'This report displays the supporting detail by regulatory account for dynamic allocators used in a case for selected allocator_category and effective date',
          'Regulatory',
          'dw_reg_report_case_alloc_dyn',
          'CA - 103',
          101,
          408,
          107,
          1,
          1,
          3
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1402, 0, 10, 4, 2, 7, 38135, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038135_reg_dyn_alloc_report.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;