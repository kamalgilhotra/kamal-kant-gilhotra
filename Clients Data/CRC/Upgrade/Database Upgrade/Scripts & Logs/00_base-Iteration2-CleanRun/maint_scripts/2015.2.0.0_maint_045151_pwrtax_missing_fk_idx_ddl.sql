/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045151_pwrtax_missing_fk_idx_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/29/2015 Lee Quinn      Add indexes that support foriegn keys to help 
||                                      performance of deletes.
||============================================================================
*/

create unique index TAX_INTERFACE_COS_UIDX
   on TAX_INTERFACE_COS (COMPANY_ID, lower(INTERFACE_OBJECT), lower(USER_ID))
      tablespace PWRPLANT_IDX;
      
create index BASIS_AMOUNTS_IDX
   on BASIS_AMOUNTS(TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;
      
create index BASIS_AMOUNTS_INC_IDX
   on BASIS_AMOUNTS_INC(TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;
      
create index DEFERRED_INCOME_TAX_IDX
   on DEFERRED_INCOME_TAX(TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;
      
create index DEFERRED_INCOME_TAX_INC_IDX 
   on DEFERRED_INCOME_TAX_INC(TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;
      
create index TAX_BOOK_RECONCILE_INC_IDX 
   on TAX_BOOK_RECONCILE_INC(TAX_RECORD_ID) 
      tablespace PWRPLANT_IDX;
      
create index TAX_BOOK_RECONCILE_XFER_IDX
   on TAX_BOOK_RECONCILE_TRANSFER(TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;
      
create index TAX_DEPRECIATION_TRANSFER_IDX
   on TAX_DEPRECIATION_TRANSFER(TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;
      
create index TAX_RECORD_DOCUMENT_IDX
   on TAX_RECORD_DOCUMENT(TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;
      
create index TAX_TRANSFER_CONTROL_IDX
   on TAX_TRANSFER_CONTROL(TO_TRID)
      tablespace PWRPLANT_IDX;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2948, 0, 2015, 2, 0, 0, 45151, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045151_pwrtax_missing_fk_idx_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;