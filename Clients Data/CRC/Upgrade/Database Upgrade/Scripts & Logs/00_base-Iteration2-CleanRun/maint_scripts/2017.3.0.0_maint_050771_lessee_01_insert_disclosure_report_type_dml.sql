/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050771_lessee_01_insert_disclosure_report_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 3/28/2018 Alex Healey    Add new report type for lessee disclosure reports
||============================================================================
*/
INSERT INTO PP_REPORT_TYPE
            (report_type_id,
             description,
             sort_order)
VALUES      (311,
             'Lessee - Disclosure',
             4); 
			 
		 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4242, 0, 2017, 3, 0, 0, 50771, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050771_lessee_01_insert_disclosure_report_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		 		 
