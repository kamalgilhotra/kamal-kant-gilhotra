/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008489_cr_interco_batch_derivations.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.1.0 01/31/2013 Joseph King    Point Release
||============================================================================
*/

alter table CR_BATCH_DERIVATION_CONTROL add INTERCOMPANY_ACCOUNTING number(22,0);

update CR_BATCH_DERIVATION_CONTROL set INTERCOMPANY_ACCOUNTING = nvl(INTERCOMPANY_ACCOUNTING, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (280, 0, 10, 4, 1, 0, 8489, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_008489_cr_interco_batch_derivations.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
