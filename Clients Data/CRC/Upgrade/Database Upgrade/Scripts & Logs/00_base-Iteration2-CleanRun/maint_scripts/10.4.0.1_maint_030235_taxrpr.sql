/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030235_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.1   06/18/2013 Alex P.         Patch Release
||============================================================================
*/

alter table REPAIR_WORK_ORDER_SEGMENTS
   add (ALLOC_EXPENSE_PCT           number(22,8),
        RELATION_ADD_COST           number(22,2),
        RELATION_RETIRE_COST        number(22,2),
        RELATION_MY_REPAIR_ELIG_AMT number(22,2));

alter table REPAIR_WO_SEG_REPORTING
   add (ALLOC_EXPENSE_PCT           number(22,8),
        RELATION_ADD_COST           number(22,2),
        RELATION_RETIRE_COST        number(22,2),
        RELATION_MY_REPAIR_ELIG_AMT number(22,2));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (413, 0, 10, 4, 0, 1, 30235, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_030235_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;