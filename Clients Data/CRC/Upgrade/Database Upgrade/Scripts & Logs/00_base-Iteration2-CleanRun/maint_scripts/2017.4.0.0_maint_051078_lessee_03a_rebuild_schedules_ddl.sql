/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051078_lessee_03a_rebuild_schedules_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/29/2018 Sarah Byers      Rebuild asset and ILR schedules to populate new ROU Asset columns
||============================================================================
*/

-- Asset
create table ls_asset_schedule_temp_load 
as
select  ls_asset_id, revision, set_of_books_id, month, residual_amount, term_penalty, bpo_price, beg_capital_cost, end_capital_cost, 
       beg_obligation, end_obligation, beg_lt_obligation, end_lt_obligation, interest_accrual, principal_accrual, interest_paid, 
       principal_paid, executory_accrual1, executory_accrual2, executory_accrual3, executory_accrual4, executory_accrual5, 
       executory_accrual6, executory_accrual7, executory_accrual8, executory_accrual9, executory_accrual10, executory_paid1, 
       executory_paid2, executory_paid3, executory_paid4, executory_paid5, executory_paid6, executory_paid7, executory_paid8, 
       executory_paid9, executory_paid10, contingent_accrual1, contingent_accrual2, contingent_accrual3, contingent_accrual4, 
       contingent_accrual5, contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10, 
       contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4, contingent_paid5, contingent_paid6, contingent_paid7, 
       contingent_paid8, contingent_paid9, contingent_paid10, user_id, time_stamp, is_om, current_lease_cost, beg_deferred_rent, 
       deferred_rent, end_deferred_rent, beg_st_deferred_rent, end_st_deferred_rent, beg_liability, end_liability, beg_lt_liability, 
       end_lt_liability, principal_remeasurement, liability_remeasurement, initial_direct_cost, incentive_amount, beg_prepaid_rent, 
       prepay_amortization, prepaid_rent, end_prepaid_rent, idc_math_amount, incentive_math_amount, 
       beg_capital_cost as beg_net_rou_asset, end_capital_cost as end_net_rou_asset
  from ls_asset_schedule;

-- Drop ls_asset_schedule
drop table ls_asset_schedule;
  
-- Rename to ls_asset_schedule
alter table ls_asset_schedule_temp_load rename to ls_asset_schedule;

-- Primary Key
alter table ls_asset_schedule add constraint 
pk_ls_asset_schedule primary key (ls_asset_id, revision, set_of_books_id, month) using index tablespace pwrplant_idx;

-- Indexes
CREATE INDEX LS_ASSET_SCHEDULE_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (REVISION , LS_ASSET_ID , SET_OF_BOOKS_ID ) ;
CREATE INDEX LS_ASSET_SCHEDULE_TMTH_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (TRUNC(MONTH,'fmmonth')) ;
CREATE INDEX LS_ASSET_SCHEDULE_MANY_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (LS_ASSET_ID, REVISION, MONTH, SET_OF_BOOKS_ID, TRUNC(MONTH,'fmmonth'));

-- ILR
create table ls_ilr_schedule_temp_load 
as
select ilr_id, revision, set_of_books_id, month, residual_amount, term_penalty, bpo_price, beg_capital_cost, end_capital_cost, 
       beg_obligation, end_obligation, beg_lt_obligation, end_lt_obligation, interest_accrual, principal_accrual, interest_paid, 
       principal_paid, executory_accrual1, executory_accrual2, executory_accrual3, executory_accrual4, executory_accrual5, 
       executory_accrual6, executory_accrual7, executory_accrual8, executory_accrual9, executory_accrual10, executory_paid1, 
       executory_paid2, executory_paid3, executory_paid4, executory_paid5, executory_paid6, executory_paid7, executory_paid8, 
       executory_paid9, executory_paid10, contingent_accrual1, contingent_accrual2, contingent_accrual3, contingent_accrual4, 
       contingent_accrual5, contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10, 
       contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4, contingent_paid5, contingent_paid6, contingent_paid7, 
       contingent_paid8, contingent_paid9, contingent_paid10, user_id, time_stamp, is_om, current_lease_cost, beg_deferred_rent, 
       deferred_rent, end_deferred_rent, beg_st_deferred_rent, end_st_deferred_rent, beg_liability, end_liability, beg_lt_liability, 
       end_lt_liability, principal_remeasurement, liability_remeasurement, initial_direct_cost, incentive_amount, beg_prepaid_rent, 
       prepay_amortization, prepaid_rent, end_prepaid_rent, idc_math_amount, incentive_math_amount, 
       beg_capital_cost as beg_net_rou_asset, end_capital_cost as end_net_rou_asset
  from ls_ilr_schedule;

-- Drop ls_ilr_schedule
drop table ls_ilr_schedule;
  
-- Rename to ls_ilr_schedule
alter table ls_ilr_schedule_temp_load rename to ls_ilr_schedule;

-- Primary Key
alter table ls_ilr_schedule add constraint 
pk_ls_ilr_schedule primary key (ilr_id, revision, set_of_books_id, month) using index tablespace pwrplant_idx;

-- Indexes
CREATE INDEX LS_ILR_SCHEDULE_IDX ON PWRPLANT.LS_ILR_SCHEDULE (REVISION , ILR_ID , SET_OF_BOOKS_ID ) ;
CREATE INDEX LS_ILR_SCHEDULE_MTH_IDX ON PWRPLANT.LS_ILR_SCHEDULE (MONTH) ;
CREATE INDEX LS_ILR_SCHEDULE_TMTH_IDX ON PWRPLANT.LS_ILR_SCHEDULE (TRUNC(MONTH,'fmmonth')) ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5962, 0, 2017, 4, 0, 0, 51078, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051078_lessee_03a_rebuild_schedules_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

            
