/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048623_lease_08_update_rates_default_and_add_triggers_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/15/2017 Andrew Hill      Update logic to store default rates & update fx views to read from new dense rates table
||============================================================================
*/

drop trigger refresh_mv_currency_rates_in;
drop trigger refresh_mv_currency_rates_del;
drop trigger refresh_mv_currency_rates_up;
drop materialized view mv_currency_rates;

create or replace view v_currency_rate_default_dense as
with min_months as (SELECT min(TRUNC(month, 'MONTH')) as min_month
                      FROM MV_MULTICURR_LS_ASSET_INNER
                      union
                      select min(trunc(month, 'MONTH')) as min_month
                      from mv_multicurr_lis_inner_amounts
                      union
                      SELECT  min(trunc(exchange_date, 'MONTH')) as min_month
                      FROM CURRENCY_RATE_DEFAULT
  ),
  max_months as (select max(TRUNC(month, 'MONTH')) as max_month
                      FROM MV_MULTICURR_LS_ASSET_INNER
                      union
                      select max(trunc(month, 'MONTH')) as max_month
                      from mv_multicurr_lis_inner_amounts
                      union
                      SELECT  MAX(TRUNC(exchange_date, 'MONTH')) AS max_month
                      FROM currency_rate_default
  ),
  date_range as (select max(max_month) as max_month, min(min_month) as min_month, months_between(max(max_month), min(min_month)) as diff
                  FROM min_months
                  CROSS JOIN max_months
                  )
SELECT month as exchange_date,
       currency_from,
       currency_to,
       exchange_rate_type_id,
       LAST_VALUE(rate IGNORE NULLS) over (partition by exchange_rate_type_id,
                                                        currency_from, 
                                                        currency_to
                                           order BY MONTH rows BETWEEN UNBOUNDED PRECEDING and current ROW) as rate
FROM (SELECT  dates.month,
              crd.currency_from,
              crd.currency_to,
              crd.exchange_rate_type_id,
              crd.rate
      from (SELECT ADD_MONTHS(min_month, LEVEL - 1) AS month
            FROM date_range
            connect by level <= diff + 1) dates
      left outer join (select exchange_date, 
                              currency_from, 
                              currency_to, 
                              exchange_rate_type_id, 
                              rate
                        from (select  exchange_date, 
                                      currency_from, 
                                      currency_to, 
                                      exchange_rate_type_id,
                                      rate, 
                                      ROW_NUMBER() over (partition BY currency_from, 
                                                                      currency_to, 
                                                                      exchange_rate_type_id, 
                                                                      trunc(exchange_date, 'MONTH') 
                                                         order BY exchange_date) rn
                              from currency_rate_default crd)
                       where rn = 1) crd partition by (currency_from, currency_to, exchange_rate_type_id) on trunc(crd.exchange_date, 'MONTH') = dates.month);
                       

create table currency_rate_default_dense( exchange_date date not null, 
                                          currency_from number(22,0) not null, 
                                          currency_to number(22,0) not null, 
                                          exchange_rate_type_id number(22,0) not null, 
                                          time_stamp date,
                                          user_id varchar2(18),
                                          rate number(22,8) not null);
                                          
alter table currency_rate_default_dense add(constraint currency_rate_dflt_dense_pk
                                              primary key (exchange_date, currency_from, currency_to, exchange_rate_type_id));

create index currency_rate_dflt_dense_idx on currency_rate_default_dense( trunc(exchange_date,'MONTH'), 
                                                                          exchange_rate_type_id, 
                                                                          currency_from, 
                                                                          currency_to) tablespace pwrplant_idx compute statistics;

create or replace procedure p_refresh_curr_rate_dflt_dense as
begin                         
  merge into currency_rate_default_dense a
  using (select coalesce(y.exchange_date, x.exchange_date) as exchange_date,
                coalesce(y.currency_from, x.currency_from) as currency_from,
                coalesce(y.currency_to, x.currency_to) as currency_to,
                coalesce(y.exchange_rate_type_id, x.exchange_rate_type_id) as exchange_rate_type_id,
                coalesce(y.rate, -1) as rate,
                case when y.rate is null then 'KILL' else 'KEEP' end as status
          from currency_rate_default_dense x
          full join v_currency_rate_default_dense y  
            on (x.exchange_date = y.exchange_date 
                and x.currency_from = y.currency_from
                and x.currency_to = y.currency_to
                and x.exchange_rate_type_id = y.exchange_rate_type_id)) b on (a.exchange_date = b.exchange_date 
                                                                              and a.currency_from = b.currency_from
                                                                              and a.currency_to = b.currency_to
                                                                              and a.exchange_rate_type_id = b.exchange_rate_type_id)
  when matched then
    update set a.rate = b.rate
    where decode(a.rate, b.rate, 1, 0) = 0
    delete where b.status = 'KILL'
  when not matched then
    insert (a.exchange_date, a.currency_from, a.currency_to, a.exchange_rate_type_id, a.rate)
    values (b.exchange_date, b.currency_from, b.currency_to, b.exchange_rate_type_id, b.rate);
end;
/



create or replace trigger curr_rate_dflt_refresh_dense after insert or update or delete on currency_rate_default
begin
    p_refresh_curr_rate_dflt_dense;
end;
/

create index mv_mc_ls_asset_in_trnc_mth_idx on mv_multicurr_ls_asset_inner(trunc(month, 'MONTH')) tablespace pwrplant_idx compute statistics;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3653, 0, 2017, 1, 0, 0, 48623, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048623_lease_08_update_rates_default_and_add_triggers_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;