/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051355_lessee_02_disc_rpt_turn_off_multi_thread_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.2  09/14/2018 Sarah Byers      Turn off multi-threading for disclosure reports
||============================================================================
*/

update pp_reports
   set turn_off_multi_thread = 1
 where report_number like 'Lessee - 2%';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(9663, 0, 2017, 4, 0, 2, 51355, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.2_maint_051355_lessee_02_disc_rpt_turn_off_multi_thread_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;