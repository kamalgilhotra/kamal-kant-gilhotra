/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_042157_depr_descr_uix_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1     01/26/2015 Aaron Smith      Make sure forecast depr group descriptions are unique
||============================================================================
*/

SET SERVEROUTPUT ON
SET LINESIZE 200

declare
   PPCMSG  varchar2(10) := 'PPC' || '-MSG> ';
   PPCERR  varchar2(10) := 'PPC' || '-ERR> ';
   NEWLINE varchar2(10) := CHR(10);

   L_SKIP_CHECK boolean := false;
   L_COUNT      number;
   L_ERROR      boolean := false;
   L_MSG        varchar2(256);

begin
  --find duplicate fcst_depr_group descriptions
  select count(*)
	into L_COUNT
	from (
		SELECT description, Count(*) 
		FROM fcst_depr_group
		GROUP BY description
		HAVING Count(*) > 1
		)
	;

  if L_COUNT > 0 then
	 L_ERROR := true;
	 L_MSG   := L_COUNT || ' Forecast Depreciation Group descriptions are not unique.  All descriptions must be unique before creating the unique constraint.';
	 DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
  end if;

  -- find duplicate fcst_depr_group_version descriptions
  select count(*)
	into L_COUNT
	from (
		SELECT description, fcst_depr_version_id,Count(*) 
		FROM fcst_depr_group_version
		GROUP BY description, fcst_depr_version_id
		HAVING Count(*) > 1
		)
	;

  if L_COUNT > 0 then
	 L_ERROR := true;
	 L_MSG   := L_COUNT || ' Forecast Depreciation Group-Version descriptions are not unique. All descriptions must be unique before creating the unique constraint.';
	 DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
  end if;

  if L_ERROR = true then
	 -- Raise application error
	 DBMS_OUTPUT.PUT_LINE(NEWLINE);
	 DBMS_OUTPUT.PUT_LINE('     Please review and correct the duplicate descriptions in the forecast depreciation system with the client. ');
	 DBMS_OUTPUT.PUT_LINE(NEWLINE);
	 DBMS_OUTPUT.PUT_LINE('     Please edit the script and read the comments at the bottom ');
	 DBMS_OUTPUT.PUT_LINE('     for additional scripts to locate the duplicate descriptions ');
	 DBMS_OUTPUT.PUT_LINE('     and a default clean-up script to use as a last resort.');
	 DBMS_OUTPUT.PUT_LINE(NEWLINE);
	 RAISE_APPLICATION_ERROR(-20000, 'Read the above comments and make the necessary changes to the system.');
  end if;
end;
/

--  These queries will return the specific descriptions that need to be corrected:
--
--SELECT description, Count(*) FROM fcst_depr_group
--GROUP BY description
--HAVING Count(*) > 1

--SELECT description, fcst_depr_version_id , Count(*) 
--FROM fcst_depr_group_version
--GROUP BY description, fcst_depr_version_id
--HAVING Count(*) > 1
--;

--  Example clean up script to make all descriptions unique by prepending the fcst-depr-group-id to the description:
--
--UPDATE fcst_depr_group
--SET description = SubStr('('||fcst_depr_group_id||')'||description, 1, 35)
--WHERE description IN
--(SELECT z.description
--FROM fcst_depr_group z
--GROUP BY z.description
--HAVING Count(*) > 1);
--  
--UPDATE fcst_depr_group_version
--SET description = SubStr('('||fcst_depr_group_id||')'||description, 1, 35)
--WHERE (description, fcst_depr_version_id) IN
--(SELECT z.description, z.fcst_depr_version_id
--FROM fcst_depr_group_version z
--GROUP BY z.description, z.fcst_depr_version_id
--HAVING Count(*) > 1);
--  

ALTER TABLE fcst_depr_group
ADD CONSTRAINT fcst_dg_descr_uix UNIQUE (description)
USING INDEX TABLESPACE PWRPLANT_IDX
;

ALTER TABLE fcst_depr_group_version
ADD CONSTRAINT fcst_dgv_desc_uix UNIQUE (fcst_depr_version_id, description)
USING INDEX TABLESPACE PWRPLANT_IDX
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2219, 0, 2015, 1, 0, 0, 42157, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042157_depr_descr_uix_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;