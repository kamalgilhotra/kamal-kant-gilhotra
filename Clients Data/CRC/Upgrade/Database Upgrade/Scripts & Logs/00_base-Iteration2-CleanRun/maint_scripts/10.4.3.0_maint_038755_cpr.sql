/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038755_cpr.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 07/15/2014 Daniel Motter    Creation
||============================================================================
*/

insert into GL_JE_CONTROL X
   (X.PROCESS_ID, X.JE_ID, X.JE_TABLE, X.JE_COLUMN, X.DR_TABLE, X.DR_COLUMN, X.CR_TABLE, X.CR_COLUMN, X.CUSTOM_CALC,
    X.DR_ACCOUNT, X.CR_ACCOUNT)
   select 'CPR DEPR AUTO RETIRE' PROCESS_ID,
          JE.JE_ID,
          JE.JE_TABLE,
          JE.JE_COLUMN,
          JE.DR_TABLE,
          JE.DR_COLUMN,
          JE.CR_TABLE,
          JE.CR_COLUMN,
          JE.CUSTOM_CALC,
          JE.DR_ACCOUNT,
          JE.CR_ACCOUNT
     from GL_JE_CONTROL JE
    where trim(UPPER(JE.PROCESS_ID)) = 'AUTOMATIC RETIREMENTS'
      and not exists (select 1 from GL_JE_CONTROL Z where trim(UPPER(Z.PROCESS_ID)) = 'CPR DEPR AUTO RETIRE');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1259, 0, 10, 4, 3, 0, 38755, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038755_cpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
