/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030545_depr_00_calc_stg.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/09/2013 B. Beck        Point release
||============================================================================
*/

create global temporary table DEPR_CALC_STG
(
 -- Primary Key
 DEPR_GROUP_ID               number(22,0),
 SET_OF_BOOKS_ID             number(22,0),
 GL_POST_MO_YR               date,
 CALC_MONTH                  date,
 -- COLUMNS FOR THE DEPR CALC
 DEPR_CALC_STATUS            number(2,0),
 DEPR_CALC_MESSAGE           varchar2(2000),
 TRF_IN_EST_ADDS             varchar2(35),
 INCLUDE_RWIP_IN_NET         varchar2(35),
 DNSA_COR_BAL                number(22,2),
 DNSA_SALV_BAL               number(22,2),
 COR_YTD                     number(22,2),
 SALV_YTD                    number(22,2),
 FISCALYEAROFFSET            number(2,0),
 FISCALYEARSTART             date,
 -- DEPR GROUP VALUES
 COMPANY_ID                  number(22,0),
 SUBLEDGER_TYPE_ID           number(22,0),
 DESCRIPTION                 varchar2(254),
 DEPR_METHOD_ID              number(22,0),
 MID_PERIOD_CONV             number(22,0),
 MID_PERIOD_METHOD           varchar2(35),
 DG_EST_ANN_NET_ADDS         number(22,8),
 TRUE_UP_CPR_DEPR            number(22,0),
 -- DEPR METHOD VALUES
 RATE                        number(22,8),
 NET_GROSS                   number(22,0),
 OVER_DEPR_CHECK             number(22,0),
 NET_SALVAGE_PCT             number(22,8),
 RATE_USED_CODE              number(22,0),
 END_OF_LIFE                 number(22,0),
 EXPECTED_AVERAGE_LIFE       number(22,0),
 RESERVE_RATIO_ID            number(22,0),
 DMR_COST_OF_REMOVAL_RATE    number(22,8),
 COST_OF_REMOVAL_PCT         number(22,8),
 DMR_SALVAGE_RATE            number(22,8),
 INTEREST_RATE               number(22,8),
 AMORTIZABLE_LIFE            number(22,0),
 COR_TREATMENT               number(1,0),
 SALVAGE_TREATMENT           number(1,0),
 NET_SALVAGE_AMORT_LIFE      number(22,0),
 ALLOCATION_PROCEDURE        varchar2(5),
 -- DEPR LEDGER VALUES
 DEPR_LEDGER_STATUS          number(22,0),
 BEGIN_RESERVE               number(22,2),
 END_RESERVE                 number(22,2),
 RESERVE_BAL_PROVISION       number(22,2),
 RESERVE_BAL_COR             number(22,2),
 SALVAGE_BALANCE             number(22,2),
 RESERVE_BAL_ADJUST          number(22,2),
 RESERVE_BAL_RETIREMENTS     number(22,2),
 RESERVE_BAL_TRAN_IN         number(22,2),
 RESERVE_BAL_TRAN_OUT        number(22,2),
 RESERVE_BAL_OTHER_CREDITS   number(22,2),
 RESERVE_BAL_GAIN_LOSS       number(22,2),
 RESERVE_ALLOC_FACTOR        number(22,2),
 BEGIN_BALANCE               number(22,2),
 ADDITIONS                   number(22,2),
 RETIREMENTS                 number(22,2),
 TRANSFERS_IN                number(22,2),
 TRANSFERS_OUT               number(22,2),
 ADJUSTMENTS                 number(22,2),
 DEPRECIATION_BASE           number(22,2),
 END_BALANCE                 number(22,2),
 DEPRECIATION_RATE           number(22,8),
 DEPRECIATION_EXPENSE        number(22,2),
 DEPR_EXP_ADJUST             number(22,2),
 DEPR_EXP_ALLOC_ADJUST       number(22,2),
 COST_OF_REMOVAL             number(22,2),
 RESERVE_RETIREMENTS         number(22,2),
 SALVAGE_RETURNS             number(22,2),
 SALVAGE_CASH                number(22,2),
 RESERVE_CREDITS             number(22,2),
 RESERVE_ADJUSTMENTS         number(22,2),
 RESERVE_TRAN_IN             number(22,2),
 RESERVE_TRAN_OUT            number(22,2),
 GAIN_LOSS                   number(22,2),
 VINTAGE_NET_SALVAGE_AMORT   number(22,2),
 VINTAGE_NET_SALVAGE_RESERVE number(22,2),
 CURRENT_NET_SALVAGE_AMORT   number(22,2),
 CURRENT_NET_SALVAGE_RESERVE number(22,2),
 IMPAIRMENT_RESERVE_BEG      number(22,2),
 IMPAIRMENT_RESERVE_ACT      number(22,2),
 IMPAIRMENT_RESERVE_END      number(22,2),
 EST_ANN_NET_ADDS            number(22,2),
 RWIP_ALLOCATION             number(22,2),
 COR_BEG_RESERVE             number(22,2),
 COR_EXPENSE                 number(22,2),
 COR_EXP_ADJUST              number(22,2),
 COR_EXP_ALLOC_ADJUST        number(22,2),
 COR_RES_TRAN_IN             number(22,2),
 COR_RES_TRAN_OUT            number(22,2),
 COR_RES_ADJUST              number(22,2),
 COR_END_RESERVE             number(22,2),
 COST_OF_REMOVAL_RATE        number(22,8),
 COST_OF_REMOVAL_BASE        number(22,2),
 RWIP_COST_OF_REMOVAL        number(22,2),
 RWIP_SALVAGE_CASH           number(22,2),
 RWIP_SALVAGE_RETURNS        number(22,2),
 RWIP_RESERVE_CREDITS        number(22,2),
 SALVAGE_RATE                number(22,8),
 SALVAGE_BASE                number(22,2),
 SALVAGE_EXPENSE             number(22,2),
 SALVAGE_EXP_ADJUST          number(22,2),
 SALVAGE_EXP_ALLOC_ADJUST    number(22,2),
 RESERVE_BAL_SALVAGE_EXP     number(22,2),
 RESERVE_BLENDING_ADJUSTMENT number(22,2),
 RESERVE_BLENDING_TRANSFER   number(22,2),
 COR_BLENDING_ADJUSTMENT     number(22,2),
 COR_BLENDING_TRANSFER       number(22,2),
 IMPAIRMENT_ASSET_AMOUNT     number(22,2),
 IMPAIRMENT_EXPENSE_AMOUNT   number(22,2),
 RESERVE_BAL_IMPAIRMENT      number(22,2)
) on commit preserve rows;

alter table DEPR_CALC_STG
   add (BEGIN_BALANCE_YEAR     number(22,2),
        BEGIN_RESERVE_YEAR     number(22,2),
        BEGIN_BALANCE_QTR      number(22,2),
        BEGIN_RESERVE_QTR      number(22,2),
        PLANT_ACTIVITY_2       number(22,2),
        RESERVE_ACTIVITY_2     number(22,2),
        EST_NET_ADDS           number(22,2),
        CUMULATIVE_TRANSFERS   number(22,2),
        RESERVE_DIFF           number(22,2),
        FISCAL_MONTH           number(2,0),
        FISCALQTRSTART         number(22,2),
        TYPE_2_EXIST           number(1,0),
        CUM_UOP_DEPR           number(22,2),
        CUM_UOP_DEPR_2         number(22,2),
        PRODUCTION             number(22,2),
        ESTIMATED_PRODUCTION   number(22,2),
        PRODUCTION_2           number(22,2),
        ESTIMATED_PRODUCTION_2 number(22,2));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (448, 0, 10, 4, 1, 0, 30545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030545_depr_00_calc_stg.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
