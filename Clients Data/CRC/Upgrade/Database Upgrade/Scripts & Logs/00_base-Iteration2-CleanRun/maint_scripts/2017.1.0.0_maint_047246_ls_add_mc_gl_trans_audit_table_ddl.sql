/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047246_ls_add_mc_gl_trans_audit_table_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 07/10/2017 Jared Watkins  add audit tables for multicurrency GL transactions
||                                      in order to track contract/company amounts and rates used
||============================================================================
*/

--create the audit table
create table ls_mc_gl_transaction_audit (
  company_id    number(22,0) not null,
  gl_posting_mo_yr date not null,
  gl_trans_id   number(22,0) not null,
  trans_type    number(22,0),
  gl_je_code    char(18) not null,
  from_currency number(22,0) not null,
  to_currency   number(22,0) not null,
  exchange_rate number(22,8),
  calculated_amount number(22,2),
  translated_amount number(22,2)
)
;

--add the PK
alter table ls_mc_gl_transaction_audit
add constraint ls_mc_gl_trans_audit_pk
primary key (gl_trans_id)
using index tablespace PWRPLANT_IDX
;

--FK to ls_process_control for the company_id and gl_posting_mo_yr
alter table ls_mc_gl_transaction_audit
add constraint ls_mc_gl_trans_proc_ctrl_fk
foreign key (company_id, gl_posting_mo_yr)
references ls_process_control (company_id, gl_posting_mo_yr)
;

--FK to gl_transaction for gl_trans_id
alter table ls_mc_gl_transaction_audit
add constraint ls_mc_gl_transaction_fk
foreign key (gl_trans_id)
references gl_transaction (gl_trans_id)
;

--FK to je_trans_type for trans_type
alter table ls_mc_gl_transaction_audit
add constraint ls_mc_gl_trans_type
foreign key (trans_type)
references je_trans_type (trans_type)
;

--FK for from_currency to the currency table
alter table ls_mc_gl_transaction_audit
add constraint ls_mc_gl_trans_from_curr_fk
foreign key (from_currency)
references currency (currency_id)
;

--FK for to_currency from the currency table
alter table ls_mc_gl_transaction_audit
add constraint ls_mc_gl_trans_to_curr_fk
foreign key (to_currency)
references currency (currency_id)
;

--add table/column comments
comment on table ls_mc_gl_transaction_audit is '(O)[06] The Lease Multicurrency GL Transaction Audit table is used to track any journal entries created with multicurrency that have to be translated from the contract to company amounts. This way we can display the original calculated amount as well as the exchange rate used for every given GL transaction if there are any questions/issues with calculations.';
comment on column ls_mc_gl_transaction_audit.company_id is 'The company associated to the translated GL transaction';
comment on column ls_mc_gl_transaction_audit.gl_posting_mo_yr is 'The posting month the transaction is included within';
comment on column ls_mc_gl_transaction_audit.gl_trans_id is 'The ID of the specific GL Transaction we will be creating with the translated company currency value rather than the contract currency';
comment on column ls_mc_gl_transaction_audit.trans_type is 'The type of the GL Transaction we have translated';
comment on column ls_mc_gl_transaction_audit.gl_je_code is 'The GL JE Code attached to the GL Transaction we have translated';
comment on column ls_mc_gl_transaction_audit.from_currency is 'The ID of the contract currency we are translating the amount from';
comment on column ls_mc_gl_transaction_audit.to_currency is 'The ID of the company currency we are translating the amount into';
comment on column ls_mc_gl_transaction_audit.exchange_rate is 'The exchange rate used to convert the amounts when converting/sending this GL transaction';
comment on column ls_mc_gl_transaction_audit.calculated_amount is 'The original amount calculated (in contract currency)';
comment on column ls_mc_gl_transaction_audit.translated_amount is 'The translated amount of the transaction (in company currency)';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3564, 0, 2017, 1, 0, 0, 47246, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047246_ls_add_mc_gl_trans_audit_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;