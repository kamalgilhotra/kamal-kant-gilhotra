/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010249_sys_pp_columns.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/06/2013 Stephen Motter Point Release
||============================================================================
*/

--Replace the old column 'expand_dates' with two new columns 'Expand Dates Back' and 'Expand Dates Forward'
delete from POWERPLANT_COLUMNS where COLUMN_NAME like 'expand_dates%';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('expand_dates_bak', 'wo_est_hierarchy', 'yes_no', 'p', 'Expand Dates Back', 2, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('expand_dates_fwd', 'wo_est_hierarchy', 'yes_no', 'p', 'Expand Dates Forward', 2, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (496, 0, 10, 4, 1, 0, 10249, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010249_sys_pp_columns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
