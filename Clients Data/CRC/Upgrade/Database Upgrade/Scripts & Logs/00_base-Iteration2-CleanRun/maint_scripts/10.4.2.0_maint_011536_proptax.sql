/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_011536_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   11/11/2013 Julia Breuer
||============================================================================
*/

--
-- Update the PT Parcel Appeal and PT Parcel Appeal Event tables to be stored by assessment year instead of tax year.
-- We only need to store it on the Appeal table.
--
alter table PT_PARCEL_APPEAL add ASSESSMENT_YEAR_ID number(22,0);

alter table pt_parcel_appeal
   add constraint PT_PRCL_APPL_ASMT_YEAR_FK
       foreign key (ASSESSMENT_YEAR_ID)
       references PT_ASSESSMENT_YEAR;

update PT_PARCEL_APPEAL PA
   set PA.ASSESSMENT_YEAR_ID =
        (select TY.ASSESSMENT_YEAR_ID from PROPERTY_TAX_YEAR TY where TY.TAX_YEAR = PA.TAX_YEAR);

alter table PT_PARCEL_APPEAL modify ASSESSMENT_YEAR_ID not null;

comment on column pt_parcel_appeal.assessment_year_id is 'System-assigned identifier of a particular assessment year.';

--
-- Change the primary key to be by parcel_id and appeal_id.  Do not include assessment_year_id, as we
-- want to allow users to change it.
--
alter table PT_PARCEL_APPEAL_EVENT drop constraint PT_PRCL_APPL_EV_PRCL_APPL_FK;

alter table PT_PARCEL_APPEAL_EVENT drop primary key drop index;

alter table PT_PARCEL_APPEAL drop primary key drop index;

alter table PT_PARCEL_APPEAL
   add constraint PT_PARCEL_APPEAL_PK
       primary key (PARCEL_ID, APPEAL_ID)
       using index tablespace PWRPLANT_IDX;

alter table PT_PARCEL_APPEAL_EVENT
   add constraint PT_PARCEL_APPEAL_EVENT_PK
       primary key (PARCEL_ID, APPEAL_ID, EVENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table PT_PARCEL_APPEAL_EVENT
   add constraint PT_PRCL_APPL_EV_PRCL_APPL_FK
       foreign key (PARCEL_ID, APPEAL_ID)
       references PT_PARCEL_APPEAL (PARCEL_ID, APPEAL_ID);

alter table PT_PARCEL_APPEAL drop constraint PT_PRCL_APPL_TAX_YEAR_FK;
alter table PT_PARCEL_APPEAL drop column TAX_YEAR;
alter table PT_PARCEL_APPEAL_EVENT drop column TAX_YEAR;

--
-- Create a table for appeal status, and add the column to the appeal table.
--
create table pt_parcel_appeal_status
(
 appeal_status_id number(22,0) not null,
 time_stamp       date,
 user_id          varchar2(18),
 description      varchar2(35) not null,
 long_description varchar2(254)
);

alter table pt_parcel_appeal_status
   add constraint PT_PARCEL_APPEAL_STATUS_PK
       primary key (APPEAL_STATUS_ID)
       using index tablespace PWRPLANT_IDX;

insert into PT_PARCEL_APPEAL_STATUS ( APPEAL_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID ) values ( 1, 'Initiated', 'Initiated', sysdate, user );
insert into PT_PARCEL_APPEAL_STATUS ( APPEAL_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID ) values ( 2, 'In Progress', 'In Progress', sysdate, user );
insert into PT_PARCEL_APPEAL_STATUS ( APPEAL_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID ) values ( 3, 'Completed - Won', 'Completed - Won', sysdate, user );
insert into PT_PARCEL_APPEAL_STATUS ( APPEAL_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID ) values ( 4, 'Completed - Lost', 'Completed - Lost', sysdate, user );

alter table PT_PARCEL_APPEAL add APPEAL_STATUS_ID number(22,0);
alter table PT_PARCEL_APPEAL
   add constraint PT_PRCL_APPL_APPL_STAT_FK
       foreign key (APPEAL_STATUS_ID)
       references PT_PARCEL_APPEAL_STATUS;

update PT_PARCEL_APPEAL set APPEAL_STATUS_ID = 1 where APPEAL_STATUS_ID is null;

alter table PT_PARCEL_APPEAL modify APPEAL_STATUS_ID not null;

comment on table pt_parcel_appeal_status is '(F) [07] The PT Parcel Appeal Status table contains the valid appeal statuses used on the PT Parcel Appeal table.';
comment on column pt_parcel_appeal_status.appeal_status_id is 'System-assigned identifier of an appeal status.';
comment on column pt_parcel_appeal_status.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column pt_parcel_appeal_status.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column pt_parcel_appeal_status.description is 'Records a short description of an appeal status.';
comment on column pt_parcel_appeal_status.long_description is 'Records an optional, more detailed description of an appeal status.';

--
-- Update the parcel appeal details report to take in assessment year.
--
update PP_REPORTS set PP_REPORT_TIME_OPTION_ID = 33 where REPORT_ID = 505002;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (718, 0, 10, 4, 2, 0, 11536, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_011536_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;