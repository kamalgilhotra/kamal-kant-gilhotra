/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030690_lease_approval_notification.sql
|| Description: Inserts into approval_notification to populate approval emails
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/11/2013 Ryan Oliveria   Point Release
||============================================================================
*/

insert into APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('MLA APPROVAL MESSAGE', -1,
    'MLA: <col1>:<col2> has been submitted for your approval.  Please login to PowerPlant and approve the MLA',
    null, null, sysdate, user,
    'select mla.lease_number, mla.description from ls_lease mla where mla.lease_id = <arg1>', null,
    null, 'MLA Awaiting Approval');

insert into APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('MLA APPROVED NOTICE MESSAGE', -1, 'MLA: <col1>:<col2> has been approved', null, null, sysdate,
    user, 'select mla.lease_number, mla.description from ls_lease mla where mla.lease_id = <arg1>',
    null, null, 'MLA approved');

insert into APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('MLA REJECTION MESSAGE', -1, 'MLA: <col1>:<col2> has been rejected', null, null, sysdate, user,
    'select mla.lease_number, mla.description from ls_lease mla where mla.lease_id = <arg1>', null,
    null, 'MLA Rejected');

insert into APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('ILR APPROVAL MESSAGE', -1,
    'ILR: <col1> under MLA: <col2> has been submitted for your approval.  Please purchase our mobile software to approve your ILR',
    null, null, sysdate, user,
    'select ilr.ilr_number, mla.lease_number from ls_ilr ilr, ls_lease mla where ilr.ilr_id = <arg1> and ilr.lease_id = mla.lease_id',
    null, null, 'ILR Awaiting Approval');

insert into APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('ILR APPROVED NOTICE MESSAGE', -1, 'ILR: <col1> under MLA: <col2> has been approved', null,
    null, sysdate, user,
    'select ilr.ilr_number, mla.lease_number from ls_ilr ilr, ls_lease mla where ilr.ilr_id = <arg1> and ilr.lease_id = mla.lease_id',
    null, null, 'ILR Approved');

insert into APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('ILR REJECTION MESSAGE', -1, 'ILR: <col1> under MLA: <col2> has been rejected', null, null,
    sysdate, user,
    'select ilr.ilr_number, mla.lease_number from ls_ilr ilr, ls_lease mla where ilr.ilr_id = <arg1> and ilr.lease_id = mla.lease_id',
    null, null, 'ILR Rejected');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (472, 0, 10, 4, 1, 0, 30690, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030690_lease_approval_notification.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
