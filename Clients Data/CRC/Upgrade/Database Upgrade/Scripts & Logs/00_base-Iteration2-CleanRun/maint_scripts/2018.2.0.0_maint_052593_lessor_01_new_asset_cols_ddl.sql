/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052593_lessor_01_new_asset_cols_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 01/04/2019  Anand R          Add new columns to lsr_asset table
||============================================================================
*/

alter table lsr_asset
add (
estimated_residual_amount NUMBER(22,2) NULL,
estimated_res_amt_comp_curr NUMBER(22,2) NULL
);

comment on column lsr_asset.estimated_residual_amount is 'The estimated residual amount in contract currency converted from the company currency amount.';
comment on column lsr_asset.estimated_res_amt_comp_curr is 'The user-entered estimated residual amount in company currency.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13462, 0, 2018, 2, 0, 0, 52593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052593_lessor_01_new_asset_cols_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;