/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048904_lessor_01_add_accrual_jes_currency_gl_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/30/2018 Anand R        Add JE trans type for Lessor currency gain loss
||============================================================================
*/

/* Add trans types 4051, 4052 for Lessor gain loss  */

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4051, 'Lessor Currency Gain/Loss Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4052, 'Lessor Currency Gain/Loss Credit');

/* Add GL_JE_CODE for Lessor gain loss  */

insert into standard_journal_entries (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
values ( (select nvl((max(je_id) + 1),1) from standard_journal_entries), 'Lessor Curr G/L', 'Lessor Curr G/L', 'Lessor Curr G/L', 'Lessor Curr G/L');

insert into gl_je_control (PROCESS_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN, CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
values ('Lessor Curr G/L', (select max(je_id) from standard_journal_entries ), null, null, 'NONE', 'NONE', 'NONE', 'NONE', null, null, null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4256, 0, 2017, 3, 0, 0, 48904, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_048904_lessor_01_add_accrual_jes_currency_gl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 