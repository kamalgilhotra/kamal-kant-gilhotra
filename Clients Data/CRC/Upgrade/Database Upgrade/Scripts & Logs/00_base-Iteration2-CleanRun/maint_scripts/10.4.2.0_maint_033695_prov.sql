SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033695_prov.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   11/21/2013 Nathan Hollis
||============================================================================
*/

--prelim -- drop unnecessary primary key on tax_accrual_rep_cons_rows (because its existence could cause issues and it is unused anyway)
alter table TAX_ACCRUAL_REP_CONS_ROWS drop primary key drop index;

--51004 solution - update consolidating type to Fed Current - grid
update TAX_ACCRUAL_REP_SPECIAL_NOTE
   set REP_CONS_CATEGORY_ID = 1
 where UPPER(DESCRIPTION) = 'CONSOLIDATING_FED_CURR';

--54530 solution - update to correct consolidating category ID and update incorrect computed field
update TAX_ACCRUAL_REP_SPECIAL_NOTE
   set REP_CONS_CATEGORY_ID = 4
 where UPPER(DESCRIPTION) = 'ETR_PERIOD_CONS';

--delete duplicate special_note and rebuild if necessary
declare
   COUNTER             number;
   NEW_SPECIAL_NOTE_ID number;
begin
   select count(*)
     into COUNTER
     from TAX_ACCRUAL_REP_SPECIAL_NOTE
    where DESCRIPTION = 'ETR_PERIOD_COMPARE';

   if COUNTER > 1 then
      delete from TAX_ACCRUAL_REP_SPECIAL_NOTE where DESCRIPTION = 'ETR_PERIOD_COMPARE';

      select count(*)
        into COUNTER
        from TAX_ACCRUAL_REP_SPECIAL_NOTE
       where DESCRIPTION = 'ETR_PERIOD_COMPARE';

      if COUNTER = 0 then
         NEW_SPECIAL_NOTE_ID := 66;
      else
         select max(SPECIAL_NOTE_ID) + 1
           into NEW_SPECIAL_NOTE_ID
           from TAX_ACCRUAL_REP_SPECIAL_NOTE;
      end if;

      insert into TAX_ACCRUAL_REP_SPECIAL_NOTE
         (SPECIAL_NOTE_ID, DESCRIPTION, REPORT_OPTION_ID, MONTH_REQUIRED, COMPARE_CASE,
          M_ROLLUP_REQUIRED, CALC_RATES, REPORT_TYPE, WINDOW_NAME, DW_PARAMETER_LIST,
          JE_TEMP_TABLE_IND, NS2FAS109_TEMP_TABLE_IND, REP_ROLLUP_GROUP_ID, "DEFAULT_ACCT_ROLLUP",
          REP_CONS_CATEGORY_ID)
      values
         (NEW_SPECIAL_NOTE_ID, 'ETR_PERIOD_COMPARE', 21, 0, 1, 1, 3, 0, null, 10, 0, 0, 6, null, 0);
   end if;
end;
/

--changed from m_item_total, an invalid field identifier
update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'compute_37'
 where REP_CONS_TYPE_ID in (21, 22, 23)
   and ROW_ID = 22;

--51052 solution - need to update incorrect column names
update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'dit_act_total'
 where REP_CONS_TYPE_ID in
       (select distinct REP_CONS_TYPE_ID
          from PP_REPORTS R, TAX_ACCRUAL_REP_SPECIAL_NOTE S, TAX_ACCRUAL_REP_CONS_TYPE T
         where R.REPORT_ID = 51052
           and UPPER(S.DESCRIPTION) = UPPER(R.SPECIAL_NOTE)
           and T.REP_CONS_CATEGORY_ID = S.REP_CONS_CATEGORY_ID
           and UPPER(T.DESCRIPTION) like '%DEF TAX%'
           and T.BALANCES_IND = 1)
   and ROW_ID = 7;

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'm_activity_total'
 where REP_CONS_TYPE_ID in
       (select distinct REP_CONS_TYPE_ID
          from PP_REPORTS R, TAX_ACCRUAL_REP_SPECIAL_NOTE S, TAX_ACCRUAL_REP_CONS_TYPE T
         where R.REPORT_ID = 51052
           and UPPER(S.DESCRIPTION) = UPPER(R.SPECIAL_NOTE)
           and T.REP_CONS_CATEGORY_ID = S.REP_CONS_CATEGORY_ID
           and UPPER(T.DESCRIPTION) like '%M ITEM%'
           and T.BALANCES_IND = 1)
   and ROW_ID = 7;

--***********************************************************************************
--Begin sync rep_Cons_type, rep_Cons_rows, rep_Cons_cols to fix a few of the reports
--***********************************************************************************

--create staging tables
create table D1_TAX_ACCRUAL_REP_CONS_ROWS
   as (select * from TAX_ACCRUAL_REP_CONS_ROWS where 1=0);

create table D1_TAX_ACCRUAL_REP_CONS_COLS
   as (select * from TAX_ACCRUAL_REP_CONS_COLS where 1=0);

create table D1_TAX_ACCRUAL_REP_CONS_TYPE
   as (select * from TAX_ACCRUAL_REP_CONS_TYPE where 1=0);

--insert into staging tables
declare
   NULL_VALUE char(1) := null;
   STATEMENT1 char(325);
   STATEMENT2 char(185);
   STATEMENT4 char(180);
begin
      statement1 := 'INSERT INTO d1_TAX_ACCRUAL_REP_CONS_ROWS ("REP_CONS_TYPE_ID","ROW_ID","ROW_TYPE","GROUP_ID","RANK","COLOR_ID","VALUE_TYPE","IS_REPORT_TITLE","ROW_VALUE","LABEL_VALUE_TYPE","LABEL_VALUE","COL_FORMAT","DISPLAY_IND_FIELD","TOTAL_COL_FIELDNAME") VALUES (:0, :1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13)';
      EXECUTE IMMEDIATE statement1 USING 8, 1, 'H', -1, 1, 1, 'TEXT', 1, 'Account Activity Report by Oper', 'TEXT', 'Account Activity Report by Month', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, null_value, null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 3, 'H', -1, 3, 1, 'TEXT', 0, ' ', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'ta_version_id', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'month_descr', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 6, 'H', -1, 6, 0, 'FILLER', 0, null_value, null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 7, 'H', 0, 4, 0, 'TEXT', 0, null_value, 'FILLER', null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 8, 'F', 3, 1, 1, 'COL_VALUE_NUM', 0, 'compute_15', 'COL_VALUE_TEXT', 'group_2_descr_1', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 9, 'D', 1, 2, 0, 'COL_VALUE_NUM', 0, 'total_dr_cr', 'COL_VALUE_TEXT', 'detail_descr', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 11, 'H', 3, 1, 1, 'TEXT', 0, null_value, 'COL_VALUE_TEXT', 'group_2_descr_1', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 12, 'H', 2, 1, 1, 'TEXT', 0, null_value, 'COL_VALUE_TEXT', 'group_3_descr', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 13, 'F', 2, 1, 1, 'COL_VALUE_NUM', 0, 'compute_4', 'COL_VALUE_TEXT', 'group_3_descr', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 14, 'F', 3, 2, 0, 'TEXT', 0, null_value, 'FILLER', null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 15, 'F', 2, 2, 0, 'TEXT', 0, null_value, 'FILLER', null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 8, 16, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'compute_26', 'TEXT', 'Total Journal Entry Check', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 1, 'H', -1, 1, 1, 'TEXT', 1, 'Deferred Tax Rollforward by Company', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, null_value, null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, null_value, null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'gl_month_descr', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 6, 'H', -1, 6, 0, 'FILLER', 0, null_value, null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 7, 'D', 1, 1, 0, 'COL_VALUE_NUM', 0, 'end_dit_balance', 'COL_VALUE_TEXT', 'detail_description', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 8, 'H', 2, 1, 0, 'TEXT', 0, 'dit_account', 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 9, 'F', 2, 1, 1, 'TEXT', 0, null_value, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 10, 'F', 2, 2, 0, 'TEXT', 0, null_value, 'FILLER', null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 15, 11, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'compute_45', 'TEXT', 'Grand Total', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 1, 'H', -1, 1, 1, 'TEXT', 1, 'Deferred Tax Rollforward by Company', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, null_value, null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, null_value, null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'gl_month_descr', null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 6, 'H', -1, 6, 0, 'FILLER', 0, null_value, null_value, null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 7, 'D', 1, 1, 0, 'COL_VALUE_NUM', 0, 'm_balance_end', 'COL_VALUE_TEXT', 'detail_description', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 8, 'H', 2, 1, 0, 'TEXT', 0, null_value, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 9, 'F', 2, 1, 1, 'TEXT', 0, null_value, 'COL_VALUE_TEXT', 'dit_account_m_rollup', 'T', 'show_m_roll_visible', null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 10, 'F', 2, 2, 0, 'TEXT', 0, null_value, 'FILLER', null_value, 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 16, 11, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, 'm_balance_end_tot', 'TEXT', 'Grand Total', 'T', null_value, null_value;
      EXECUTE IMMEDIATE statement1 USING 33, 24, 'F', 99, 4, 0, 'COL_VALUE_TRANS', 0, 'comp_dtl_tte_after_discretes_tt + comp_dtl_tte_after_discretes_tt_adj - total_tax_expense', 'TEXT', 'Difference', 'T', null_value, null_value;

      statement2 := 'INSERT INTO d1_TAX_ACCRUAL_REP_CONS_TYPE ("REP_CONS_TYPE_ID","DESCRIPTION","DATAWINDOW","SORT_SPEC","REP_CONS_CATEGORY_ID","BALANCES_IND") VALUES (:0, :1, :2, :3, :4, :5)';
      EXECUTE IMMEDIATE statement2 USING 8, 'JE Detail By Oper', 'dw_tax_accrual_je_activity_rpt_cons', 'parent_id,oper_ind,group_2_descr,group_3_descr,tax_return_key,detail_descr', 2, 0;
      EXECUTE IMMEDIATE statement2 USING 15, 'Def Tax by Company', 'dw_tax_accrual_def_tax_rpt2_cons', null_value, 3, 0;
      EXECUTE IMMEDIATE statement2 USING 16, 'M Item by Company', 'dw_tax_accrual_def_tax_rpt2_cons', null_value, 3, 0;
      EXECUTE IMMEDIATE statement2 USING 30, 'ETR Footnote by Company', 'dw_tax_accrual_etr_footnote_grid', 'report_section, ms_summarized, rollup_description, detail_description', 5, 0;
      EXECUTE IMMEDIATE statement2 USING 31, 'ETR Footnote by Oper', 'dw_tax_accrual_etr_footnote_grid', 'report_section, ms_summarized, rollup_description, detail_description', 5, 0;
      EXECUTE IMMEDIATE statement2 USING 32, 'ETR Footnote by Month', 'dw_tax_accrual_etr_footnote_grid', 'report_section, ms_summarized, rollup_description, detail_description', 5, 0;
      EXECUTE IMMEDIATE statement2 USING 33, 'ETR Footnote by Month Type', 'dw_tax_accrual_etr_footnote_grid', 'report_section, ms_summarized, rollup_description, detail_description', 5, 0;

      statement4 := 'INSERT INTO d1_TAX_ACCRUAL_REP_CONS_COLS ("REP_CONS_TYPE_ID","CONS_COLNAME","RANK","CONS_COLTYPE","CONS_DESCR_COLNAME","GROUP_ID") VALUES (:0, :1, :2, :3, :4, :5)';
      EXECUTE IMMEDIATE statement4 USING 8, 'detail_descr', 1, 'string', 'detail_descr', 1;
      EXECUTE IMMEDIATE statement4 USING 8, 'group_2_descr', 1, 'string', 'group_2_descr', 3;
      EXECUTE IMMEDIATE statement4 USING 8, 'group_3_descr', 1, 'string', 'group_3_descr', 2;
      EXECUTE IMMEDIATE statement4 USING 8, 'oper_descr', 1, 'string', 'oper_descr', 0;
      EXECUTE IMMEDIATE statement4 USING 15, 'detail_description', 1, 'string', 'detail_description', 1;
      EXECUTE IMMEDIATE statement4 USING 15, 'dit_account', 1, 'string', 'dit_account', 2;
      EXECUTE IMMEDIATE statement4 USING 15, 'parent_descr', 1, 'string', 'parent_descr', 0;
      EXECUTE IMMEDIATE statement4 USING 16, 'detail_description', 1, 'string', 'detail_description', 1;
      EXECUTE IMMEDIATE statement4 USING 16, 'dit_account', 1, 'string', 'dit_account', 2;
      EXECUTE IMMEDIATE statement4 USING 16, 'm_roll_descr', 1, 'string', 'm_roll_descr', 3;
      EXECUTE IMMEDIATE statement4 USING 16, 'parent_descr', 1, 'string', 'parent_descr', 0;
      EXECUTE IMMEDIATE statement4 USING 30, 'detail_description', 1, 'string', 'detail_description', 1;
      EXECUTE IMMEDIATE statement4 USING 30, 'parent_id', 1, 'number', 'consol_desc', 0;
      EXECUTE IMMEDIATE statement4 USING 30, 'report_section', 1, 'number', 'report_section', 3;
      EXECUTE IMMEDIATE statement4 USING 30, 'rollup_description', 1, 'string', 'rollup_description', 2;
      EXECUTE IMMEDIATE statement4 USING 31, 'detail_description', 1, 'string', 'detail_description', 1;
      EXECUTE IMMEDIATE statement4 USING 31, 'oper_ind', 1, 'number', 'oper_descr', 0;
      EXECUTE IMMEDIATE statement4 USING 31, 'report_section', 1, 'number', 'report_section', 3;
      EXECUTE IMMEDIATE statement4 USING 31, 'rollup_description', 1, 'string', 'rollup_description', 2;
      EXECUTE IMMEDIATE statement4 USING 32, 'detail_description', 1, 'string', 'detail_description', 1;
      EXECUTE IMMEDIATE statement4 USING 32, 'month_id', 1, 'number', 'month_descr_dtl', 0;
      EXECUTE IMMEDIATE statement4 USING 32, 'report_section', 1, 'number', 'report_section', 3;
      EXECUTE IMMEDIATE statement4 USING 32, 'rollup_description', 1, 'string', 'rollup_description', 2;
      EXECUTE IMMEDIATE statement4 USING 33, 'detail_description', 1, 'string', 'detail_description', 1;
      EXECUTE IMMEDIATE statement4 USING 33, 'month_id', 1, 'number', 'month_descr_dtl', 0;
      EXECUTE IMMEDIATE statement4 USING 33, 'report_section', 1, 'number', 'report_section', 3;
      EXECUTE IMMEDIATE statement4 USING 33, 'rollup_description', 1, 'string', 'rollup_description', 2;
      commit;
end;
/

--**************************************
--cursor through staging tables and add
--**************************************

--D1_TAX_ACCRUAL_REP_CONS_ROWS
begin
   for COUNTER in (select distinct REP_CONS_TYPE_ID, ROW_ID from D1_TAX_ACCRUAL_REP_CONS_ROWS)
   loop
      begin
         insert into TAX_ACCRUAL_REP_CONS_ROWS
            select *
              from D1_TAX_ACCRUAL_REP_CONS_ROWS
             where REP_CONS_TYPE_ID = COUNTER.REP_CONS_TYPE_ID
               and ROW_ID = COUNTER.ROW_ID;
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE('PROVISION: Did not insert into TAX_ACCRUAL_REP_CONS_ROWS for REP_CONS_TYPE_ID: ' ||
                                 COUNTER.REP_CONS_TYPE_ID || ' and ROW_ID: ' || COUNTER.ROW_ID);
      end;
   end loop;
end;
/
--D1_TAX_ACCRUAL_REP_CONS_COLS
begin
   for COUNTER in (select REP_CONS_TYPE_ID, CONS_COLNAME from D1_TAX_ACCRUAL_REP_CONS_COLS)
   loop
      begin
         insert into TAX_ACCRUAL_REP_CONS_COLS
            select *
              from D1_TAX_ACCRUAL_REP_CONS_COLS
             where REP_CONS_TYPE_ID = COUNTER.REP_CONS_TYPE_ID
               and CONS_COLNAME = COUNTER.CONS_COLNAME;
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE('PROVISION: Did not insert into TAX_ACCRUAL_REP_CONS_COLS for REP_CONS_TYPE_ID: ''' ||
                                 COUNTER.REP_CONS_TYPE_ID || ''' and CONS_COLNAME: ''' ||
                                 COUNTER.CONS_COLNAME || '''');
      end;
   end loop;
end;
/

--D1_TAX_ACCRUAL_REP_CONS_TYPE
begin
   for COUNTER in (select REP_CONS_TYPE_ID, REP_CONS_CATEGORY_ID, ROWNUM ROW_INDEX
                     from D1_TAX_ACCRUAL_REP_CONS_TYPE)
   loop
      begin
         insert into TAX_ACCRUAL_REP_CONS_TYPE
            select *
              from D1_TAX_ACCRUAL_REP_CONS_TYPE
             where REP_CONS_TYPE_ID = COUNTER.REP_CONS_TYPE_ID
               and REP_CONS_CATEGORY_ID = COUNTER.REP_CONS_CATEGORY_ID;
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE('PROVISION: Did not insert into TAX_ACCRUAL_REP_CONS_TYPE for REP_CONS_TYPE_ID: ''' ||
                                 COUNTER.REP_CONS_TYPE_ID || ''' and REP_CONS_CATEGORY_ID: ''' ||
                                 COUNTER.REP_CONS_CATEGORY_ID || '''');
      end;
   end loop;
end;
/

--drop staging tables
drop table D1_TAX_ACCRUAL_REP_CONS_ROWS;
drop table D1_TAX_ACCRUAL_REP_CONS_COLS;
drop table D1_TAX_ACCRUAL_REP_CONS_TYPE;

--****************************************
--update TAX_ACCRUAL_REP_CONS_TYPE
--****************************************
declare
   NULL_VALUE char(1) := null;
   STATEMENT3 char(180);
begin
   STATEMENT3 := 'UPDATE TAX_ACCRUAL_REP_CONS_TYPE SET "DESCRIPTION"=:0, "DATAWINDOW"=:1, "SORT_SPEC"=:2, "REP_CONS_CATEGORY_ID"=:3, "BALANCES_IND"=:4 WHERE "REP_CONS_TYPE_ID"=:w0';

   for I in 1 .. 14
   loop
      begin
         case I
            when 1 then
               execute immediate STATEMENT3
                  using 'Fed Current By Oper', 'dw_tax_accrual_fed_provision_report_cons', 'oper_ind, m_type_id, tax_return_key, detail_descr', 1, 0, 1;
            when 2 then
               execute immediate STATEMENT3
                  using 'Fed Current By Month', 'dw_tax_accrual_fed_provision_report_cons', NULL_VALUE, 1, 0, 2;
            when 3 then
               execute immediate STATEMENT3
                  using 'Fed Current By Month Type', 'dw_tax_accrual_fed_provision_report_cons', NULL_VALUE, 1, 0, 3;
            when 4 then
               execute immediate STATEMENT3
                  using 'JE Detail By Month', 'dw_tax_accrual_je_activity_rpt_cons', 'month_period,account', 2, 0, 4;
            when 5 then
               execute immediate STATEMENT3
                  using 'JE Detail By Month Type', 'dw_tax_accrual_je_activity_rpt_cons', 'month_period,account', 2, 0, 5;
            when 6 then
               execute immediate STATEMENT3
                  using 'JE Detail By Company', 'dw_tax_accrual_je_activity_rpt_cons', NULL_VALUE, 2, 0, 6;
            when 7 then
               execute immediate STATEMENT3
                  using 'Fed Current By Company', 'dw_tax_accrual_fed_provision_report_cons', NULL_VALUE, 1, 0, 7;
            when 8 then
               execute immediate STATEMENT3
                  using 'Def Tax by Month', 'dw_tax_accrual_def_tax_rpt2_cons', NULL_VALUE, 3, 1, 11;
            when 9 then
               execute immediate STATEMENT3
                  using 'M Item by Month', 'dw_tax_accrual_def_tax_rpt2_cons', NULL_VALUE, 3, 1, 12;
            when 10 then
               execute immediate STATEMENT3
                  using 'Def Tax by Month Type', 'dw_tax_accrual_def_tax_rpt2_cons', NULL_VALUE, 3, 1, 13;
            when 11 then
               execute immediate STATEMENT3
                  using 'M Item by Month Type', 'dw_tax_accrual_def_tax_rpt2_cons', NULL_VALUE, 3, 1, 14;
            when 12 then
               execute immediate STATEMENT3
                  using 'Total Tax by Month', 'dw_tax_accrual_total_tax_all_cons', NULL_VALUE, 4, 0, 21;
            when 13 then
               execute immediate STATEMENT3
                  using 'Total Tax by Month Type', 'dw_tax_accrual_total_tax_all_cons', NULL_VALUE, 4, 0, 22;
            when 14 then
               execute immediate STATEMENT3
                  using 'Total Tax by Company', 'dw_tax_accrual_total_tax_all_cons', NULL_VALUE, 4, 0, 23;
         end case;
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE('Error updating TAX_ACCRUAL_REP_CONS_TYPE. Item number ' || I ||
                                 ' (see bottom of maint_033695_prov.sql)');
      end;
   end loop;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (768, 0, 10, 4, 2, 0, 33695, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033695_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

