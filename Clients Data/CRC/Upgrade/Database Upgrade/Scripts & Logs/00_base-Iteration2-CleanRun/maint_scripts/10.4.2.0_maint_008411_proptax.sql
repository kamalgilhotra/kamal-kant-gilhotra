/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_008411_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   12/10/2013 Julia Breuer
||============================================================================
*/

insert into PWRPLANT.PPBASE_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Payment Center - Details - Auto-Save Changes When Changing Records', sysdate, user, 'Whether the system should automatically save un-saved changes when scrolling to a new record in the Payment Details window (''Yes'') or should prompt the user to save changes (''No'').', 0, 'No', null, 1 );
insert into PWRPLANT.PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Payment Center - Details - Auto-Save Changes When Changing Records', 'No', sysdate, user );
insert into PWRPLANT.PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Payment Center - Details - Auto-Save Changes When Changing Records', 'Yes', sysdate, user );
insert into PWRPLANT.PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payment Center - Details - Auto-Save Changes When Changing Records', 'proptax', sysdate, user );
insert into PWRPLANT.PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payment Center - Details - Auto-Save Changes When Changing Records', 'payment_search', sysdate, user );
insert into PWRPLANT.PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payment Center - Details - Auto-Save Changes When Changing Records', 'payment_home', sysdate, user );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (812, 0, 10, 4, 2, 0, 8411, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_008411_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;