SET DEFINE OFF
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009958_te_reports.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.1   05/11/2012 Sunjin Cone    Point Release
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: Network Run Results',
          'T&D Network Repairs:  Process Run Results for Non-Related Work Orders for the In Process Batch',
          'dw_repair_netwk_process_report',
          '',
          'NETWK - 0027',
          null,
          13,
          13,
          0,
          1,
          1,
          'NETWK - 0027',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs(R): Network Run Results',
          'T&D Network Repairs:  Process Run Results for Related Work Orders for the In Process Batch',
          'dw_repair_netwk_process_related_report',
          '',
          'NETWK - 0028',
          null,
          13,
          13,
          0,
          1,
          1,
          'NETWK - 0028',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: Network Circuit De Min',
          'T&D Network Repairs:  Circuit Threshold De Minimis Test Results for Non-Related WOs for the In Process Batch',
          'dw_repair_netwk_circuit_thres_report',
          '',
          'NETWK - 0029',
          null,
          13,
          13,
          0,
          1,
          1,
          'NETWK - 0029',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs(R): Network Circuit De Min',
          'T&D Network Repairs:  Circuit Threshold De Minimis Test Results for Related WOs for the In Process Batch',
          'dw_repair_netwk_circuit_thres_related_report',
          '',
          'NETWK - 0030',
          null,
          13,
          13,
          0,
          1,
          1,
          'NETWK - 0030',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: Network WO De Min',
          'T&D Network Repairs:  WO Threshold De Minimis Test Results for Non-Related WOs for the In Process Batch',
          'dw_repair_netwk_wo_thres_report',
          '',
          'NETWK - 0031',
          null,
          13,
          13,
          0,
          1,
          1,
          'NETWK - 0031',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs(R): Network WO De Min',
          'T&D Network Repairs:  WO Threshold De Minimis Test Results for Related WOs for the In Process Batch',
          'dw_repair_netwk_wo_thres_related_report',
          '',
          'NETWK - 0032',
          null,
          13,
          13,
          0,
          1,
          1,
          'NETWK - 0032',
          3,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (138, 0, 10, 3, 4, 1, 9958, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.1_maint_009958_te_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON