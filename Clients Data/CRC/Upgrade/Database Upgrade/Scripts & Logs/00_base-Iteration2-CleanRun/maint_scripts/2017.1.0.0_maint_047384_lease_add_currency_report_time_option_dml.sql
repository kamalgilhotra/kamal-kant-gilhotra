/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047384_lease_add_currency_report_time_option_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/10/2017 Jared Watkins    add new report time option for currency and month number
||============================================================================
*/

insert into pp_reports_time_option(pp_report_time_option_id, description, parameter_uo_name, dwname1, label1, keycolumn1)
values (202, 'Lease Currency Single Month', 'uo_ppbase_report_parms_dddw_mnum', 'dw_ls_currency_options_dddw', 'Currency', 'currency_id');

update pp_reports set pp_report_time_option_id = 202 where datawindow = 'dw_ls_rpt_future_est_min_payment';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3431, 0, 2017, 1, 0, 0, 47384, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047384_lease_add_currency_report_time_option_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;