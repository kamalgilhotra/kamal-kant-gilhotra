/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049292_lease_02_field_lease_dgcc_sys_ctrl_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

insert into pp_system_control(control_id, control_name, control_value, description, long_description, company_id)
select pwrplant1.nextval, 'Lease Depr Group Class Code', 'None', 'None;MLA;ILR;Asset',
   'Determine if the lease module uses the depr group class code to find the depr group and at which level. Default is "None", and lease module will not use depr group class code functionality.',
-1 from dual
where not exists(
   select 1
   from pp_system_control
   where control_name = 'Lease Depr Group Class Code')
   ;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4008, 0, 2017, 1, 0, 0, 49292, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049292_lease_02_field_lease_dgcc_sys_ctrl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;