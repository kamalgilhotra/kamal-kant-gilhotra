/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_040266_depr_PKG_DEPR_INCREMENTAL.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 03/31/2014 Charlie Shilling
||============================================================================
*/

create or replace package PKG_DEPR_INCREMENTAL
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PKG_DEPR_INCREMENTAL
|| Description: Loads and handles the results for Incremental Depreciation
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0     02/10/2014 Brandon Beck     Create
||============================================================================
*/
 as
   /*
   *  This procedure stages data from fcst_depr_ledger_fp into the staging table for depreciation calculation
   *
   */
   procedure P_DEPRSTAGE
   (
      A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
      A_FP_IDS PKG_PP_COMMON.NUM_TABTYPE, A_REVISIONS PKG_PP_COMMON.NUM_TABTYPE,
      A_MONTHS PKG_PP_COMMON.DATE_TABTYPE
   );

   /*
   *  Handles the results of the incremental depreciation calculation
   */
   procedure P_HANDLERESULTS;
end PKG_DEPR_INCREMENTAL;
/


create or replace package body PKG_DEPR_INCREMENTAL as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --       VARIABLES
   --**************************************************************
   G_VERSION_ID      FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%type;
   G_MONTHS          PKG_PP_COMMON.DATE_TABTYPE;
   G_RTN           NUMBER;

   --**************************************************************
   --       PROCEDURES
   --**************************************************************

   --**************************************************************************
   --                            P_DEPRSTAGE
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation forecast.
   -- THE Load is based on fcst depr ledger for a version id
   -- AND a single month
   --
   procedure P_DEPRSTAGE(
      A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
      A_FP_IDS PKG_PP_COMMON.NUM_TABTYPE, A_REVISIONS PKG_PP_COMMON.NUM_TABTYPE,
      A_MONTHS PKG_PP_COMMON.DATE_TABTYPE
   ) is
   my_first_month date;
   my_last_month date;
   begin
      PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_INCREMENTAL.P_DEPRSTAGE');

      my_first_month := a_months( a_months.FIRST );
      my_last_month := a_months( a_months.LAST );

      FORALL indx in indices of A_FP_IDS
         insert into DEPR_CALC_STG
         (
            DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
            DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, SMOOTH_CURVE,
            DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
            DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD,
            RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
            END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
            COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
            SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
            BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
            RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
            RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
            ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
            END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
            DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
            SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
            GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
            CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
            IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
            COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
            COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
            COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
            RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
            SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
            RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
            IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
            ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
            ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
            ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE, impairment_asset_activity_salv, impairment_asset_begin_balance,
            FCST_DEPR_VERSION_ID, curve_trueup_adj, CURVE_TRUEUP_ADJ_SALV, CURVE_TRUEUP_ADJ_COR,
            SPREAD_FACTOR_ID, FUNDING_WO_ID, REVISION
         )
         select DL.FCST_DEPR_GROUP_ID, --  depr_group_id                   NULL,
            DL.SET_OF_BOOKS_ID, --  set_of_books_id              NUMBER(22,0)   NULL,
            DL.GL_POST_MO_YR, --  gl_post_mo_yr                DATE           NULL,
            DL.GL_POST_MO_YR,   --  calc_month                   DATE           NULL,
            1, --  depr_calc_status             NUMBER(2,0)    NULL,
            '', --  depr_calc_message            VARCHAR2(2000) NULL,
            LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                              DGV.COMPANY_ID),
                  'no')), --  trf_in_est_adds              VARCHAR2(35)   NULL,
            LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                              DGV.COMPANY_ID),
                  'no')), --  include_rwip_in_net          VARCHAR2(35)   NULL,
            LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
                                              DGV.COMPANY_ID),
                  '0')), -- smooth_curve
            0, --  dnsa_cor_bal                 NUMBER(22,2)   NULL,
            0, --  dnsa_salv_bal                NUMBER(22,2)   NULL,
            0, --  cor_ytd                      NUMBER(22,2)   NULL,
            0, --  salv_ytd                     NUMBER(22,2)   NULL,
            DGV.COMPANY_ID, --  company_id                   NUMBER(22,0)   NULL,
            DGV.SUBLEDGER_TYPE_ID, --  subledger_type_id            NUMBER(22,0)   NULL,
            DGV.DESCRIPTION, --  description                  VARCHAR2(254)  NULL,
            DGV.FCST_DEPR_METHOD_ID, --  depr_method_id               NUMBER(22,0)   NULL,
            DGV.MID_PERIOD_CONV, --  mid_period_conv              NUMBER(22,2)   NULL,
            lower(trim(DGV.MID_PERIOD_METHOD)), --  mid_period_method            VARCHAR2(35)   NULL,
            DMR.RATE, --  rate                         NUMBER(22,8)   NULL,
            DMR.NET_GROSS, --  net_gross                    NUMBER(22,0)   NULL,
            NVL(DMR.OVER_DEPR_CHECK, 0), --  over_depr_check              NUMBER(22,0)   NULL,
            DMR.NET_SALVAGE_PCT, --  net_salvage_pct              NUMBER(22,8)   NULL,
            NVL(DMR.RATE_USED_CODE, 0), --  rate_used_code               NUMBER(22,0)   NULL,
            DMR.END_OF_LIFE, --  end_of_life                  NUMBER(22,0)   NULL,
            NVL(DMR.EXPECTED_AVERAGE_LIFE, 0), --  expected_average_life        NUMBER(22,0)   NULL,
            NVL(DMR.RESERVE_RATIO_ID, 0), --  reserve_ratio_id             NUMBER(22,0)   NULL,
            DMR.COST_OF_REMOVAL_RATE, --  dmr_cost_of_removal_rate     NUMBER(22,8)   NULL,
            DMR.COST_OF_REMOVAL_PCT, --  cost_of_removal_pct          NUMBER(22,8)   NULL,
            DMR.SALVAGE_RATE, --  dmr_salvage_rate             NUMBER(22,8)   NULL,
            DMR.INTEREST_RATE, --  interest_rate                NUMBER(22,8)   NULL,
            NVL(DMR.AMORTIZABLE_LIFE, 0), --  amortizable_life             NUMBER(22,0)   NULL,
            DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1), --  cor_treatment                NUMBER(1,0)    NULL,
            DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1), --  salvage_treatment            NUMBER(1,0)    NULL,
            NVL(DMR.NET_SALVAGE_AMORT_LIFE, 0), --  net_salvage_amort_life       NUMBER(22,0)   NULL,
            DMR.ALLOCATION_PROCEDURE, --  allocation_procedure         VARCHAR2(5)    NULL,
            DL.DEPR_LEDGER_STATUS, --  depr_ledger_status           NUMBER(22,0)   NULL,
            nvl(DL.BEGIN_RESERVE,0), --  begin_reserve                NUMBER(22,2)   NULL,
            0 AS END_RESERVE, --  end_reserve                  NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_PROVISION, --  reserve_bal_provision        NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_COR, --  reserve_bal_cor              NUMBER(22,2)   NULL,
            DL.SALVAGE_BALANCE, --  salvage_balance              NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_ADJUST, --  reserve_bal_adjust           NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements      NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in          NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out         NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_OTHER_CREDITS, --  reserve_bal_other_credits    NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss        NUMBER(22,2)   NULL,
            DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor         NUMBER(22,2)   NULL,
            DL.BEGIN_BALANCE, --  begin_balance                NUMBER(22,2)   NULL,
            DL.ADDITIONS, --  additions
            DL.RETIREMENTS, --  retirements                  NUMBER(22,2)   NULL,
            DL.TRANSFERS_IN, --  transfers_in                 NUMBER(22,2)   NULL,
            DL.TRANSFERS_OUT, --  transfers_out                NUMBER(22,2)   NULL,
            DL.ADJUSTMENTS, --  adjustments                  NUMBER(22,2)   NULL,
            0 AS DEPRECIATION_BASE, --  depreciation_base            NUMBER(22,2)
            0 AS END_BALANCE, --  end_balance                  NUMBER(22,2)   NULL
            decode(DL.DEPR_LEDGER_STATUS, 1, DL.DEPRECIATION_RATE, DMR.RATE), --  depreciation_rate            NUMBER(22,8)   NULL,
            0 AS DEPRECIATION_EXPENSE, --  depreciation_expense         NUMBER(22,2)   NULL,
            DL.DEPR_EXP_ADJUST, --  depr_exp_adjust              NUMBER(22,2)   NULL,
            0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust        NUMBER(22,2)   NULL,
            DL.COST_OF_REMOVAL, --  cost_of_removal              NUMBER(22,2)   NULL,
            DL.RESERVE_RETIREMENTS, --  reserve_retirements          NUMBER(22,2)   NULL,
            DL.SALVAGE_RETURNS, --  salvage_returns              NUMBER(22,2)   NULL,
            DL.SALVAGE_CASH, --  salvage_cash                 NUMBER(22,2)   NULL,
            DL.RESERVE_CREDITS, --  reserve_credits              NUMBER(22,2)   NULL,
            DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments          NUMBER(22,2)   NULL,
            DL.RESERVE_TRAN_IN, --  reserve_tran_in              NUMBER(22,2)   NULL,
            DL.RESERVE_TRAN_OUT, --  reserve_tran_out             NUMBER(22,2)   NULL,
            DL.GAIN_LOSS, --  gain_loss                    NUMBER(22,2)   NULL,
            DL.VINTAGE_NET_SALVAGE_AMORT, --  vintage_net_salvage_amort    NUMBER(22,2)   NULL,
            DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
            DL.CURRENT_NET_SALVAGE_AMORT, --  current_net_salvage_amort    NUMBER(22,2)   NULL,
            DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
            DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg       NUMBER(22,2)   NULL,
            DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act       NUMBER(22,2)   NULL,
            DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end       NUMBER(22,2)   NULL,
            DL.EST_ANN_NET_ADDS, --  est_ann_net_adds             NUMBER(22,2)   NULL,
            DL.RWIP_ALLOCATION, --  rwip_allocation              NUMBER(22,2)   NULL,
            DL.COR_BEG_RESERVE, --  cor_beg_reserve              NUMBER(22,2)   NULL,
            0 AS COR_EXPENSE, --  cor_expense                  NUMBER(22,2)   NULL,
            NVL(DL.COR_EXP_ADJUST,0), --  cor_exp_adjust               NUMBER(22,2)   NULL,
            0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust         NUMBER(22,2)   NULL,
            DL.COR_RES_TRAN_IN, --  cor_res_tran_in              NUMBER(22,2)   NULL,
            DL.COR_RES_TRAN_OUT, --  cor_res_tran_out             NUMBER(22,2)   NULL,
            DL.COR_RES_ADJUST, --  cor_res_adjust               NUMBER(22,2)   NULL,
            0 AS COR_END_RESERVE, --  cor_end_reserve              NUMBER(22,2)   NULL,
            DL.COST_OF_REMOVAL_RATE, --  cost_of_removal_rate         NUMBER(22,8)   NULL,
            DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base         NUMBER(22,2)   NULL,
            nvl(DL.RWIP_COST_OF_REMOVAL,0), --  rwip_cost_of_removal         NUMBER(22,2)   NULL,
            nvl(DL.RWIP_SALVAGE_CASH,0), --  rwip_salvage_cash            NUMBER(22,2)   NULL,
            nvl(DL.RWIP_SALVAGE_RETURNS,0), --  rwip_salvage_returns         NUMBER(22,2)   NULL,
            nvl(DL.RWIP_RESERVE_CREDITS,0), --  rwip_reserve_credits         NUMBER(22,2)   NULL,
            nvl(DL.SALVAGE_RATE,0), --  salvage_rate                 NUMBER(22,8)   NULL,
            nvl(SALVAGE_BASE,0), --  salvage_base                 NUMBER(22,2)   NULL,
            nvl(SALVAGE_EXPENSE,0), --  salvage_expense              NUMBER(22,2)   NULL,
            nvl(DL.SALVAGE_EXP_ADJUST,0), --  salvage_exp_adjust           NUMBER(22,2)   NULL,
            nvl(SALVAGE_EXP_ALLOC_ADJUST,0), --  salvage_exp_alloc_adjust     NUMBER(22,2)   NULL,
            nvl(DL.RESERVE_BAL_SALVAGE_EXP,0), --  reserve_bal_salvage_exp      NUMBER(22,2)   NULL,
            nvl(DL.RESERVE_BLENDING_ADJUSTMENT,0), --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
            nvl(DL.RESERVE_BLENDING_TRANSFER,0), --  reserve_blending_transfer    NUMBER(22,2)   NULL,
            nvl(DL.COR_BLENDING_ADJUSTMENT,0), --  cor_blending_adjustment      NUMBER(22,2)   NULL,
            nvl(DL.COR_BLENDING_TRANSFER,0), --  cor_blending_transfer        NUMBER(22,2)   NULL,
            nvl(DL.IMPAIRMENT_ASSET_AMOUNT,0), --  impairment_asset_amount      NUMBER(22,2)   NULL,
            nvl(DL.IMPAIRMENT_EXPENSE_AMOUNT,0), --  impairment_expense_amount    NUMBER(22,2)   NULL,
            nvl(DL.RESERVE_BAL_IMPAIRMENT,0), --  reserve_bal_impairment       NUMBER(22,2)   NUll,
            0, --ORIG_DEPR_EXPENSE
            0, --  orig_depr_exp_alloc_adjust   NUMBER(22,2)   NULL,
            0, --  orig_salv_expense            NUMBER(22,2)   NULL,
            0, --  orig_salv_exp_alloc_adjust   NUMBER(22,2)   NULL,
            0, --  orig_cor_exp_alloc_adjust    NUMBER(22,2)   NULL,
            0, --  orig_cor_expense             NUMBER(22,2)   NULL
            nvl(DL.BEGIN_RESERVE,0), --orig_begin_reserve         NUMBER(22,2) DEFAULT 0  NULL,
            DL.COR_BEG_RESERVE, --  orig_cor_begin_reserve     NUMBER(22,2) DEFAULT 0 NULL
            dl.impairment_asset_activity_salv, dl.impairment_asset_begin_balance,
            A_VERSION_ID,
            0 as curve_trueup_adj,
            0 as CURVE_TRUEUP_ADJ_SALV,
            0 as CURVE_TRUEUP_ADJ_COR,
            DGV.FACTOR_ID,
            DL.FUNDING_WO_ID, DL.REVISION
         from FCST_DEPR_METHOD_RATES DMR, FCST_DEPR_LEDGER_FP DL, FCST_DEPR_GROUP_VERSION DGV
         where DMR.FCST_DEPR_METHOD_ID = DGV.FCST_DEPR_METHOD_ID
          and DMR.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
          and DMR.EFFECTIVE_DATE =
            (select max(DMR1.EFFECTIVE_DATE)
              from FCST_DEPR_METHOD_RATES DMR1
             where DMR.FCST_DEPR_METHOD_ID = DMR1.FCST_DEPR_METHOD_ID
               and DMR.FCST_DEPR_VERSION_ID = DMR1.FCST_DEPR_VERSION_ID
               and DMR.SET_OF_BOOKS_ID = DMR1.SET_OF_BOOKS_ID
               and DMR1.EFFECTIVE_DATE <= DL.GL_POST_MO_YR)
          and DL.FCST_DEPR_GROUP_ID = DGV.FCST_DEPR_GROUP_ID
          and DL.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
          and DL.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
          and DGV.FCST_DEPR_VERSION_ID = A_VERSION_ID
          and DL.FUNDING_WO_ID = a_fp_ids(indx)
          and DL.REVISION = a_revisions(indx)
          and dl.gl_post_mo_yr between my_first_month and my_last_month
          --Either we don't ignore inactive depr groups, or the depr group is active
          and (
            LOWER(trim(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',DGV.COMPANY_ID),'no'))) = 'no'
            or NVL(DGV.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
            DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
            DL.COR_BEG_RESERVE <> 0
          )
          ;

      G_RTN := analyze_table('DEPR_CALC_STG',100);

      PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
      when others then
         PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_DEPRSTAGE;

      procedure P_HANDLERESULTS
   is
   begin
      PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_INCREMENTAL.P_HANDLERESULTS');

      update fcst_depr_ledger_fp d set (
         begin_reserve, end_reserve, begin_balance, end_balance,
         depreciation_base, depreciation_rate, depreciation_expense, depr_exp_alloc_adjust,
         salvage_base, salvage_rate, salvage_expense, salvage_exp_alloc_adjust, salvage_balance,
         cost_of_removal_base, cost_of_removal_rate, cor_expense, cor_exp_alloc_adjust, cor_beg_reserve, cor_end_reserve,
         impairment_reserve_end, reserve_bal_impairment,  reserve_bal_provision,
         reserve_bal_cor,  reserve_bal_adjust, reserve_bal_retirements,
         reserve_bal_tran_in, reserve_bal_tran_out, reserve_bal_gain_loss,
         reserve_bal_salvage_exp, est_ann_net_adds,
         cor_blending_adjustment, cor_blending_transfer,
         impairment_asset_amount, impairment_expense_amount,
         reserve_blending_adjustment, reserve_blending_transfer,
         rwip_cost_of_removal, rwip_reserve_credits, rwip_salvage_cash, rwip_salvage_returns,
         salvage_exp_adjust
       ) =
      (  select s.begin_reserve, s.end_reserve, s.begin_balance, s.end_balance,
         s.depreciation_base, s.depreciation_rate, s.depreciation_expense, (s.depr_exp_alloc_adjust + s.over_depr_adj + s.retro_depr_adj + s.curve_trueup_adj),
         s.salvage_base, s.salvage_rate, s.salvage_expense, (s.salvage_exp_alloc_adjust + s.over_depr_adj_salv + s.retro_salv_adj + s.curve_trueup_adj_salv), s.salvage_balance,
         s.cost_of_removal_base, s.cost_of_removal_rate, s.cor_expense,
         (s.cor_exp_alloc_adjust + s.over_depr_Adj_cor + s.retro_cor_adj + s.curve_trueup_adj_Cor), s.cor_beg_reserve, s.cor_end_reserve,
         s.impairment_reserve_end, s.reserve_bal_impairment,  s.reserve_bal_provision,
         s.reserve_bal_cor,  s.reserve_bal_adjust, s.reserve_bal_retirements,
         s.reserve_bal_tran_in, s.reserve_bal_tran_out, s.reserve_bal_gain_loss,
         s.reserve_bal_salvage_exp, s.est_ann_net_adds,
         s.cor_blending_adjustment, s.cor_blending_transfer,
         s.impairment_asset_amount, s.impairment_expense_amount,
         s.reserve_blending_adjustment, s.reserve_blending_transfer,
         s.rwip_cost_of_removal, s.rwip_reserve_credits, s.rwip_salvage_cash, s.rwip_salvage_returns,
         s.salvage_exp_adjust
         from depr_calc_stg s
         where d.fcst_depr_group_id = s.depr_group_id
         and d.set_of_books_id = s.set_of_books_id
         and d.gl_post_mo_yr = s.gl_post_mo_yr
         and d.fcst_depr_version_id = s.fcst_depr_version_id
         and s.depr_calc_status = 9
      )
      where exists (
         select 1
         from depr_calc_stg s
         where d.fcst_depr_group_id = s.depr_group_id
         and d.set_of_books_id = s.set_of_books_id
         and d.gl_post_mo_yr = s.gl_post_mo_yr
         and d.fcst_depr_version_id = s.fcst_depr_version_id
         and d.funding_wo_id = s.funding_wo_id
         and d.revision = s.revision
         and s.depr_calc_status = 9
      );

      PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
      when others then
         PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_HANDLERESULTS;

end PKG_DEPR_INCREMENTAL;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1513, 0, 10, 4, 2, 41, 40266, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.41_maint_040266_depr_PKG_DEPR_INCREMENTAL.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;