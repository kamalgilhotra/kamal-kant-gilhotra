/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035187_lease_improved_mec.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/23/2014  Kyle Peterson Point Release
||============================================================================
*/

create table LS_MONTHLY_DEPR_STG
(
 LS_ASSET_ID          number(22,0) not null,
 SET_OF_BOOKS_ID      number(22,0) not null,
 GL_POSTING_MO_YR     date not null,
 DEPRECIATION_EXPENSE number(22,2)
);

alter table LS_MONTHLY_DEPR_STG
   add constraint PK_LS_MONTHLY_DEPR_STG
       primary key (LS_ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR)
       using index tablespace PWRPLANT_IDX;

alter table LS_MONTHLY_DEPR_STG
   add constraint FK_LS_MONTHLY_DEPR_STG1
       foreign key (LS_ASSET_ID)
       references LS_ASSET(LS_ASSET_ID);

alter table LS_MONTHLY_DEPR_STG
   add constraint FK_LS_MONTHLY_DEPR_STG2
       foreign key (SET_OF_BOOKS_ID)
       references SET_OF_BOOKS(SET_OF_BOOKS_ID);

comment on table LS_MONTHLY_DEPR_STG is ' An informational table that shows calculated depreciation expense at an asset level. Only used in Month End Close in Lessee.';
comment on column LS_MONTHLY_DEPR_STG.LS_ASSET_ID is 'System assigned identifier of a particular leased asset.';
comment on column LS_MONTHLY_DEPR_STG.SET_OF_BOOKS_ID is 'The set of books.';
comment on column LS_MONTHLY_DEPR_STG.GL_POSTING_MO_YR is 'The month of the depreciation expense will be posted in.';
comment on column LS_MONTHLY_DEPR_STG.DEPRECIATION_EXPENSE is 'The dollar amount of depreciation expense for this particular asset.';

create table LS_MEC_CONFIG
(
 COMPANY_ID number(22,0) not null,
 PROCESS_1  number(1,0),
 PROCESS_2  number(1,0),
 PROCESS_3  number(1,0)
);

alter table LS_MEC_CONFIG
   add constraint PK_LS_MEC_CONFIG
       primary key (COMPANY_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_MEC_CONFIG
   add constraint FK_LS_MEC_CONFIG1
       foreign key (COMPANY_ID)
       references COMPANY_SETUP(COMPANY_ID);

alter table LS_MEC_CONFIG
   add constraint FK_LS_MEC_CONFIG2
       foreign key (PROCESS_1)
       references YES_NO(YES_NO_ID);

alter table LS_MEC_CONFIG
   add constraint FK_LS_MEC_CONFIG3
       foreign key (PROCESS_2)
       references YES_NO(YES_NO_ID);

alter table LS_MEC_CONFIG
   add constraint FK_LS_MEC_CONFIG4
       foreign key (PROCESS_3)
       references YES_NO(YES_NO_ID);

comment on table LS_MEC_CONFIG is ' A table for configuring which Lessee Month End processes are required.';
comment on column LS_MEC_CONFIG.COMPANY_ID is 'System generated ID for a particular company.';
comment on column LS_MEC_CONFIG.PROCESS_1 is 'Controls whether or not Depreciation shows up in Month End Close. 1 is yes, 0 is no.';
comment on column LS_MEC_CONFIG.PROCESS_2 is 'Controls whether or not Accruals control shows up in Month End Close. 1 is yes, 0 is no.';
comment on column LS_MEC_CONFIG.PROCESS_3 is 'Controls whether or not Payments control shows up in Month End Close. 1 is yes, 0 is no.';

insert into LS_MEC_CONFIG
   (COMPANY_ID, PROCESS_1, PROCESS_2, PROCESS_3)
   (select COMPANY_ID, 1, 1, 1 from COMPANY_SETUP where IS_LEASE_COMPANY = 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (896, 0, 10, 4, 2, 0, 35187, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035187_lease_improved_mec.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;