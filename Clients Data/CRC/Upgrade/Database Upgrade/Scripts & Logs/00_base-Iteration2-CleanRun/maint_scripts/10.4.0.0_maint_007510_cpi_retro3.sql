/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007510_cpi_retro3.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/8/2013  Sunjin Cone    Point Release
||============================================================================
*/

insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('cpi_retro_cpr_assets_insert', 1, '/*+rule*/ ');
 
update CPI_RETRO_CLOSINGS set UNIT_CLOSED_MONTH_NUMBER = 0 where UNIT_CLOSED_MONTH_NUMBER is null;

alter table CPI_RETRO_CLOSINGS drop primary key drop index;

alter table CPI_RETRO_CLOSINGS
   add constraint PK_CPI_RETRO_CLOSINGS
       primary key (BATCH_ID, COMPANY_ID, WORK_ORDER_ID, CLOSED_MONTH_NUMBER, UNIT_CLOSED_MONTH_NUMBER)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (343, 0, 10, 4, 0, 0, 7510, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_007510_cpi_retro3.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;