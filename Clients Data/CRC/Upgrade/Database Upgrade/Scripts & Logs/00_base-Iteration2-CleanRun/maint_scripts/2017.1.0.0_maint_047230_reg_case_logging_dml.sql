/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_047230_reg_case_logging_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2017.1.0.0 03/02/2017 Sarah Byers		 Add process id for RMS Case logging
||============================================================================
*/

insert into pp_processes (
	process_id, description, long_description, executable_file, allow_concurrent)
select max(nvl(process_id,0)) + 1,
		 'RMS Case Processing',
		 'RMS Case Processing',
		 null,
		 1
  from pp_processes;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3381, 0, 2017, 1, 0, 0, 47230, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047230_reg_case_logging_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;