/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047281_05_lease_add_asset_contract_currency_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/24/2017 Jared Watkins    add contract currency to Lease assets
||============================================================================
*/

ALTER TABLE ls_asset
ADD contract_currency_id number(22,0)
;

ALTER TABLE ls_asset
ADD CONSTRAINT ls_asset_contract_curr_fk
FOREIGN KEY (contract_currency_id)
REFERENCES currency (currency_id)
;

COMMENT ON COLUMN ls_asset.contract_currency_id is 'The contract currency used by this Lease asset; must match the contract currency of the related MLA.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3448, 0, 2017, 1, 0, 0, 47281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047281_05_lease_add_asset_contract_currency_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
