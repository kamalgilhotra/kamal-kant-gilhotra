SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034411_depr_stg_column_changes.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 01/21/2014 Charlie Shilling Add and remove columns from depr_calc_stg and depr_calc_stg_arc
||============================================================================
*/

declare
   procedure DROP_COLUMN(V_TABLE_NAME varchar2,
                         V_COL_NAME   varchar2) is
   begin
      execute immediate 'alter table ' || V_TABLE_NAME || ' drop column ' || V_COL_NAME;
      DBMS_OUTPUT.PUT_LINE('Sucessfully dropped column ' || V_COL_NAME || ' from table ' ||
                           V_TABLE_NAME || '.');
   exception
      when others then
         if sqlcode = -904 then
            --904 is invalid identifier, which means that the table already does not have this column, so do nothing
            DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' on table ' || V_TABLE_NAME ||
                                 ' was already removed. No action necessary.');
         else
            RAISE_APPLICATION_ERROR(-20000,
                                    'Could not drop column ' || V_COL_NAME || ' from ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
         end if;
   end DROP_COLUMN;

   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                           V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                                    V_TABLE_NAME || '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' ||
                                       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                           V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('  Sucessfully added the comment to column ' || V_COL_NAME ||
                              ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
BEGIN
   --drop fiscalyearoffset
   DROP_COLUMN('depr_calc_stg', 'fiscalyearoffset');
   DROP_COLUMN('depr_calc_stg_arc', 'fiscalyearoffset');
   DROP_COLUMN('fcst_depr_calc_stg_arc', 'fiscalyearoffset');

   --drop true_up_cpr_depr
   DROP_COLUMN('depr_calc_stg', 'true_up_cpr_depr');
   DROP_COLUMN('depr_calc_stg_arc', 'true_up_cpr_depr');
   DROP_COLUMN('fcst_depr_calc_stg_arc', 'true_up_cpr_depr');

   --drop less_year_uop_depr
   DROP_COLUMN('depr_calc_stg', 'less_year_uop_depr');
   DROP_COLUMN('depr_calc_stg_arc', 'less_year_uop_depr');
   DROP_COLUMN('fcst_depr_calc_stg_arc', 'less_year_uop_depr');

   --add uop_exp_adj
   ADD_COLUMN('depr_group_uop',
              'uop_exp_adj',
              'number(22,2)',
              'The adjustment to depreciation expense from the UOP meeting maximum and minimum values. Used in determining future adjustments when required.');
   ADD_COLUMN('depr_calc_stg',
              'uop_exp_adj',
              'number(22,2)',
              'The adjustment to depreciation expense from the UOP meeting maximum and minimum values.');
   ADD_COLUMN('depr_calc_stg_arc',
              'uop_exp_adj',
              'number(22,2)',
              'The adjustment to depreciation expense from the UOP meeting maximum and minimum values.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'uop_exp_adj',
              'number(22,2)',
              'The adjustment to depreciation expense from the UOP meeting maximum and minimum values.');

   --backfill uop_exp_adj on depr_group_oup with 0s
   execute immediate 'update DEPR_GROUP_UOP set UOP_EXP_ADJ = 0 where UOP_EXP_ADJ is null';

   --add cum_rate_depr
   ADD_COLUMN('depr_calc_stg',
              'cum_rate_depr',
              'number(22,2)',
              'The total rate depreciation over the life of the depreciation group. Used as the minimum when MIN_CALC_OPTION = 3.');
   ADD_COLUMN('depr_calc_stg_arc',
              'cum_rate_depr',
              'number(22,2)',
              'The total rate depreciation over the life of the depreciation group. Used as the minimum when MIN_CALC_OPTION = 3.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'cum_rate_depr',
              'number(22,2)',
              'The total rate depreciation over the life of the depreciation group. Used as the minimum when MIN_CALC_OPTION = 3.');

   --add ytd_rate_depr
   ADD_COLUMN('depr_calc_stg',
              'ytd_rate_depr',
              'number(22,2)',
              'The total rate depreciation since the beginning of the fiscal year. Used as the minimum when MIN_CALC_OPTION = 2.');
   ADD_COLUMN('depr_calc_stg_arc',
              'ytd_rate_depr',
              'number(22,2)',
              'The total rate depreciation since the beginning of the fiscal year. Used as the minimum when MIN_CALC_OPTION = 2.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'ytd_rate_depr',
              'number(22,2)',
              'The total rate depreciation since the beginning of the fiscal year. Used as the minimum when MIN_CALC_OPTION = 2.');

   --add curr_rate_exp
   ADD_COLUMN('depr_calc_stg',
              'curr_rate_exp',
              'number(22,2)',
              'The total rate depreciation calculated for this month.');
   ADD_COLUMN('depr_calc_stg_arc',
              'curr_rate_exp',
              'number(22,2)',
              'The total rate depreciation calculated for this month.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'curr_rate_exp',
              'number(22,2)',
              'The total rate depreciation calculated for this month.');

   --add min_annual_expense
   ADD_COLUMN('depr_calc_stg',
              'min_annual_expense',
              'number(22,2)',
              'The minimum depreciation expense to take each year for UOP method with MIN_CALC_OPTION = 1.');
   ADD_COLUMN('depr_calc_stg_arc',
              'min_annual_expense',
              'number(22,2)',
              'The minimum depreciation expense to take each year for UOP method with MIN_CALC_OPTION = 1.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'min_annual_expense',
              'number(22,2)',
              'The minimum depreciation expense to take each year for UOP method with MIN_CALC_OPTION = 1.');

   --add max_annual_expense
   ADD_COLUMN('depr_calc_stg',
              'max_annual_expense',
              'number(22,2)',
              'The maximum depreciation expense to take each year for UOP method with MIN_CALC_OPTION = 1.');
   ADD_COLUMN('depr_calc_stg_arc',
              'max_annual_expense',
              'number(22,2)',
              'The maximum depreciation expense to take each year for UOP method with MIN_CALC_OPTION = 1.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'max_annual_expense',
              'number(22,2)',
              'The maximum depreciation expense to take each year for UOP method with MIN_CALC_OPTION = 1.');

   --add ytd_ledger_depr_expense
   ADD_COLUMN('depr_calc_stg',
              'ytd_ledger_depr_expense',
              'number(22,2)',
              'The year-to-date LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ from DEPR_GROUP_UOP.');
   ADD_COLUMN('depr_calc_stg_arc',
              'ytd_ledger_depr_expense',
              'number(22,2)',
              'The year-to-date LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ from DEPR_GROUP_UOP.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'ytd_ledger_depr_expense',
              'number(22,2)',
              'The year-to-date LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ from DEPR_GROUP_UOP.');

   --add cum_ledger_depr_expense
   ADD_COLUMN('depr_calc_stg',
              'cum_ledger_depr_expense',
              'number(22,2)',
              'The cumulative LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ from DEPR_GROUP_UOP for the depreciation group and set of books.');
   ADD_COLUMN('depr_calc_stg_arc',
              'cum_ledger_depr_expense',
              'number(22,2)',
              'The cumulative LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ from DEPR_GROUP_UOP for the depreciation group and set of books.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'cum_ledger_depr_expense',
              'number(22,2)',
              'The cumulative LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ from DEPR_GROUP_UOP for the depreciation group and set of books.');

   --add min_cum_exp
   ADD_COLUMN('depr_calc_stg',
              'min_cum_exp',
              'number(22,2)',
              'The minimum amount to compare the year-to-date or cumulative expense to.');
   ADD_COLUMN('depr_calc_stg_arc',
              'min_cum_exp',
              'number(22,2)',
              'The minimum amount to compare the year-to-date or cumulative expense to.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'min_cum_exp',
              'number(22,2)',
              'The minimum amount to compare the year-to-date or cumulative expense to.');

   --add effective_cum_uop_expense
   ADD_COLUMN('depr_calc_stg',
              'effective_cum_uop_expense',
              'number(22,2)',
              'The cumulative expense used in determining the uop_exp_adj.');
   ADD_COLUMN('depr_calc_stg_arc',
              'effective_cum_uop_expense',
              'number(22,2)',
              'The cumulative expense used in determining the uop_exp_adj.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'effective_cum_uop_expense',
              'number(22,2)',
              'The cumulative expense used in determining the uop_exp_adj.');

end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (911, 0, 10, 4, 2, 0, 34411, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034411_depr_stg_column_changes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;