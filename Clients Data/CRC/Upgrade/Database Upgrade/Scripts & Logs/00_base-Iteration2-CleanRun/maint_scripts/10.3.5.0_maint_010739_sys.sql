/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010739_sys.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/26/2012 Lee Quinn      Point Release
||============================================================================
*/

SET SERVEROUTPUT ON
begin
   -- Ignore if the column already exists
   execute immediate 'alter table PP_SCHEMA_CHANGE_LOG add SCRIPT_LOG clob';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('SCRIPT_LOG column already exists.');
end;
/

alter table PP_SCHEMA_CHANGE_LOG modify script_path  varchar2(512);
alter table PP_SCHEMA_CHANGE_LOG modify script_name  varchar2(512);
alter table PP_SCHEMA_CHANGE_LOG modify os_user      varchar2(512);
alter table PP_SCHEMA_CHANGE_LOG modify terminal     varchar2(512);
alter table PP_SCHEMA_CHANGE_LOG modify service_name varchar2(512);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (192, 0, 10, 3, 5, 0, 10739, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010739_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
