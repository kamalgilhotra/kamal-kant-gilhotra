/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034102_lease_Invoice_line_notes.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/17/2014 Kyle Peterson
||============================================================================
*/

alter table LS_INVOICE_LINE
   add (DESCRIPTION varchar2(35),
        NOTES       varchar2(4000));

comment on column LS_INVOICE_LINE.DESCRIPTION is 'A description for the invoice line.';
comment on column LS_INVOICE_LINE.NOTES is 'A column for notes about a particular invoice line.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (873, 0, 10, 4, 2, 0, 34102, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034102_lease_Invoice_line_notes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;