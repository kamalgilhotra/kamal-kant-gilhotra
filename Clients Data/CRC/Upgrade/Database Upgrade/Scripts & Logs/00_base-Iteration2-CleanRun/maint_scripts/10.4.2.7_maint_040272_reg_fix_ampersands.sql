SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040272_reg_fix_ampersands.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 10/09/2014 Kyle Peterson
||============================================================================
*/

update PPBASE_WORKSPACE  set LABEL = 'Dept Budget (O&&M)' where MODULE = 'REG' and WORKSPACE_IDENTIFIER = 'uo_reg_fcst_cr_int_grid_map';
update PPBASE_MENU_ITEMS set LABEL = 'Reports && Charts'  where MODULE = 'REG' and MENU_IDENTIFIER = 'REPORTS';
update PPBASE_MENU_ITEMS set LABEL = 'Dept Budget (O&&M)' where MODULE = 'REG' and MENU_IDENTIFIER = 'FORE_OM';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1502, 0, 10, 4, 2, 7, 40272, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_040272_reg_fix_ampersands.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON