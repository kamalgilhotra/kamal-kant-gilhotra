 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_052595_sys_02_add_package_version_validation_dml.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2017.4.0.3  10/30/2018 C.Yura    Adding a new table for use in Package Version Validation
 ||============================================================================
 */
insert into pp_package_versions
(package_name, version)
select distinct name, '2017.4.0.3' From sys.all_source source
where type = 'PACKAGE' 
and owner = 'PWRPLANT'
and not exists (select 1 from pp_package_versions pp_pack where pp_pack.package_name = source.name) 
and (name like '%DEPR%' or name like '%LEASE%' or name like '%LESSOR%' or name = 'PKG_FINANCIAL_CALCS')
and name not like '%TAX%';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11223, 0, 2017, 4, 0, 3, 52595, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052595_sys_02_add_package_version_validation_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;