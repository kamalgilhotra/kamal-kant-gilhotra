
 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044747_prov_20152_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2 09/09/2015 	Jarrett Skov   Corrects some existing system option values and adds a system option for signoff_ind
 ||============================================================================
 */ 

update tax_accrual_system_options set system_option_id = 'CASE_CONFIGS_CBX_CC_ESTIMATE' where system_option_id = 'CASE_CONFIGS_CC_CBX_ESTIMATE';

update tax_accrual_system_options set pp_default_value = 0, option_value = 0 where system_option_id = 'CASE_CONFIGS_CBX_CC_NON_CURRENT_ADJ';

insert into tax_accrual_system_options (system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option)
values ('CASE_CONFIGS_CBX_CC_SIGNOFF_IND', '1 or 0 indicator that specifies whether this checkbox should be checked', 0, 1, 1, 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2855, 0, 2015, 2, 0, 0, 044747, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044747_prov_20152_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;