/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052135_lessee_01_sent_to_ap_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/02/2018 K Powers       Track payments to AP
||============================================================================
*/

CREATE TABLE LS_PAYMENTS_SENT_TO_AP
(PAYMENT_TYPE_ID	          NUMBER(22,0) 	NOT NULL,	
LS_ASSET_ID	                NUMBER(22,0)	NOT NULL,
SET_OF_BOOKS_ID	            NUMBER(22,0)	NOT NULL,
GL_POSTING_MO_YR            DATE	        NOT NULL,	
AP_PAID_AMOUNT              NUMBER(22,2)	NOT NULL,
USER_ID                     VARCHAR2(18),                
TIME_STAMP                  DATE 
) TABLESPACE PWRPLANT_IDX;

ALTER TABLE LS_PAYMENTS_SENT_TO_AP 
ADD CONSTRAINT PK_LS_PAYMENTS_SENT_TO_AP 
PRIMARY KEY (PAYMENT_TYPE_ID,LS_ASSET_ID,SET_OF_BOOKS_ID,GL_POSTING_MO_YR) using index tablespace pwrplant_idx;

ALTER TABLE LS_PAYMENTS_SENT_TO_AP 
ADD CONSTRAINT FK_LS_PAYMENTS_SENT_TO_AP_1 
  FOREIGN KEY (PAYMENT_TYPE_ID)
  REFERENCES LS_PAYMENT_TYPE(PAYMENT_TYPE_ID);
  
ALTER TABLE LS_PAYMENTS_SENT_TO_AP 
ADD CONSTRAINT FK_LS_PAYMENTS_SENT_TO_AP_2 
  FOREIGN KEY (LS_ASSET_ID)
  REFERENCES LS_ASSET(LS_ASSET_ID);
  
ALTER TABLE LS_PAYMENTS_SENT_TO_AP 
ADD CONSTRAINT FK_LS_PAYMENTS_SENT_TO_AP_3
  FOREIGN KEY (SET_OF_BOOKS_ID)
  REFERENCES SET_OF_BOOKS(SET_OF_BOOKS_ID);

-- table comment
COMMENT ON TABLE LS_PAYMENTS_SENT_TO_AP IS 'The LS Payments Sent to AP table contains the summated payment amounts by asset that have already been sent to AP and is populated when a payment is unapproved.';
COMMENT ON COLUMN LS_PAYMENTS_SENT_TO_AP.PAYMENT_TYPE_ID IS 'A number indicating the payment type for a payment line (interest, executory, principal etc).';
COMMENT ON COLUMN LS_PAYMENTS_SENT_TO_AP.LS_ASSET_ID IS 'System assigned identifier of a leased asset.';
COMMENT ON COLUMN LS_PAYMENTS_SENT_TO_AP.SET_OF_BOOKS_ID IS 'System assigned identifier of a set of books.';
COMMENT ON COLUMN LS_PAYMENTS_SENT_TO_AP.GL_POSTING_MO_YR IS 'The accounting month of a particular payment line.';
COMMENT ON COLUMN LS_PAYMENTS_SENT_TO_AP.AP_PAID_AMOUNT IS 'The total amount that has been sent to AP (includes payment amount and any adjustments for the particular payment line).';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(10062, 0, 2018, 1, 0, 0, 52135, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052135_lessee_01_sent_to_ap_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;