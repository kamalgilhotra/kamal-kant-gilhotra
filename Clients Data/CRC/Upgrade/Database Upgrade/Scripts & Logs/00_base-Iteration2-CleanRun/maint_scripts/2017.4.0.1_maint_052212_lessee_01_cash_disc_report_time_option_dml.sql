/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_052212_lessee_01_cash_disc_report_time_option_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.4.0.1  8/17/2018  C. Yura	  allow Lessee Cash Paid Disclosure Report to run for start/end month
||============================================================================
*/

update pp_reports set pp_report_time_option_id = 208 where lower(datawindow) = 'dw_ls_rpt_disclosure_cash_paid';

update pp_reports set pp_report_time_option_id = 201 where lower(datawindow) = 'dw_ls_rpt_disclosure_cash_paid_consol';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9247, 0, 2017, 4, 0, 1, 52212, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052212_lessee_01_cash_disc_report_time_option_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		   