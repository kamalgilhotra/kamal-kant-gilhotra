/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043352_pcm_backout_dev_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/24/2015 Alex P.          Postpone these features to a later version
||============================================================================
*/

-- Do not use the following tables in 2015.1
truncate table wo_est_fcst_options_version;
truncate table wo_est_fcst_options_details;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2441, 0, 2015, 1, 0, 0, 043352, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043352_pcm_backout_dev_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;