/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037099_depr_PKG_PP_DEPR_ACTIVITY.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 03/17/2014 Brandon Beck
||============================================================================
*/

create or replace package PKG_PP_DEPR_ACTIVITY
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PKG_PP_DEPR_ACTIVITY
|| Description: Depr Activity functions and procedures for PowerPlant application.
||============================================================================
|| Copyright (C) 2013 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0      04/17/2013 Brandon Beck   initial creation
||============================================================================
*/
 as
   function F_RECURRINGACTIVITY(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
                                A_MSG         out varchar2) return number;
end PKG_PP_DEPR_ACTIVITY;
/


create or replace package body PKG_PP_DEPR_ACTIVITY as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --       VARIABLES
   --**************************************************************

   --**************************************************************
   --       PROCEDURES
   --**************************************************************

   --**************************************************************
   --       FUNCTIONS
   --**************************************************************
   -- removes recurring activity
   function F_REMOVERECURRINGACTIVITY(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                      A_MONTH       date,
                                      A_MSG         out varchar2) return number is

   begin
      pkg_pp_log.p_write_message( 'REMOVING recurring activity for the month' );

      FORALL i in INDICES OF a_company_ids
      merge into depr_ledger a using
      (
         select *
         from
         (
            select da.depr_group_id, da.set_of_books_id, da.gl_post_mo_yr, da.description,
               sum(da.amount) as amount
            from depr_activity da, depr_group dg
            where da.depr_group_id = dg.depr_group_id
            and dg.company_id = a_company_ids(i)
            and da.gl_post_mo_yr = a_month
            and da.post_period_months is not null
            group by da.depr_group_id, da.set_of_books_id, da.gl_post_mo_yr, da.description
         ) b
         model
         partition by (b.depr_group_id, b.set_of_books_id, b.gl_post_mo_yr)
         dimension by (b.description)
         measures
         (
            amount,
            0 as depr_exp_adjust, 0 as cost_of_removal, 0 as salvage_cash,
            0 as salvage_returns, 0 as reserve_credits, 0 as reserve_tran_in,
            0 as reserve_tran_out, 0 as cor_res_tran_out, 0 as cor_res_tran_in,
            0 as cor_res_adjust, 0 as cor_exp_adjust, 0 as gain_loss,
            0 as reserve_adjustments, 0 as impairment_reserve_act, 0 as salvage_exp_adjust
         )
         rules update
         (
            depr_exp_adjust['DEPR_EXP_ADJUST'] = amount['DEPR_EXP_ADJUST'],
            cost_of_removal['COST_OF_REMOVAL'] = amount['COST_OF_REMOVAL'],
            salvage_cash['SALVAGE_CASH'] = amount['SALVAGE_CASH'],
            salvage_returns['SALVAGE_RETURNS'] = amount['SALVAGE_RETURNS'],
            reserve_credits['RESERVE_CREDITS'] = amount['RESERVE_CREDITS'],
            reserve_tran_in['RESERVE_TRANS_IN'] = amount['RESERVE_TRANS_IN'],
            reserve_tran_out['RESERVE_TRANS_OUT'] = amount['RESERVE_TRANS_OUT'],
            cor_res_tran_out['COST_OF_REMOVAL_TRANS_OUT'] = amount['COST_OF_REMOVAL_TRANS_OUT'],
            cor_res_tran_in['COST_OF_REMOVAL_TRANS_IN'] = amount['COST_OF_REMOVAL_TRANS_IN'],
            cor_res_adjust['COST_OF_REMOVAL_RES_ADJUST'] = amount['COST_OF_REMOVAL_RES_ADJUST'],
            cor_exp_adjust['COST_OF_REMOVAL_EXP_ADJUST'] = amount['COST_OF_REMOVAL_EXP_ADJUST'],
            gain_loss['GAIN_LOSS'] = amount['GAIN_LOSS'],
            reserve_adjustments['RESERVE_ADJUSTMENT'] = amount['RESERVE_ADJUSTMENT'],
            impairment_reserve_act['IMPAIRMENT_ACT'] = amount['IMPAIRMENT_ACT'],
            salvage_exp_adjust['SALVAGE_EXP_ADJUST'] = amount['SALVAGE_EXP_ADJUST']
         )
      ) b
      on
      (
         a.depr_group_id = b.depr_group_id
         and a.set_of_books_id = b.set_of_books_id
         and a.gl_post_mo_yr = b.gl_post_mo_yr
      )
      when matched then update
      set
         a.depr_exp_adjust = a.depr_exp_adjust - nvl(b.depr_exp_adjust, 0),
         a.cost_of_removal = a.cost_of_removal - nvl(b.cost_of_removal, 0),
         a.salvage_cash = a.salvage_cash - nvl(b.salvage_cash, 0),
         a.salvage_returns = a.salvage_returns - nvl(b.salvage_returns, 0),
         a.reserve_credits = a.reserve_credits - nvl(b.reserve_credits, 0),
         a.reserve_tran_in = a.reserve_tran_in - nvl(b.reserve_tran_in, 0),
         a.reserve_tran_out = a.reserve_tran_out - nvl(b.reserve_tran_out, 0),
         a.cor_res_tran_out = a.cor_res_tran_out - nvl(b.cor_res_tran_out, 0),
         a.cor_res_tran_in = a.cor_res_tran_in - nvl(b.cor_res_tran_in, 0),
         a.cor_res_adjust = a.cor_res_adjust - nvl(b.cor_res_adjust, 0),
         a.cor_exp_adjust = a.cor_exp_adjust - nvl(b.cor_exp_adjust, 0),
         a.gain_loss = a.gain_loss - nvl(b.gain_loss, 0),
         a.reserve_adjustments = a.reserve_adjustments - nvl(b.reserve_adjustments, 0),
         a.impairment_reserve_act = a.impairment_reserve_act - nvl(b.impairment_reserve_act, 0),
         a.salvage_exp_adjust = a.salvage_exp_adjust - nvl(b.salvage_exp_adjust, 0)
      ;

      if sqlCode <> 0 then
         a_msg := substr('ERROR Resetting Recurring Activity: '||sqlerrm, 1, 2000);
         return -1;
      end if;

      FORALL i in INDICES OF a_company_ids
      delete
      from depr_activity da
      where depr_group_id in
      (
         select dg.depr_group_id
         from depr_group dg
         where dg.company_id = a_company_ids(i)
      )
      and da.gl_post_mo_yr = a_month
      and da.post_period_months is not null
      ;

      if sqlCode <> 0 then
         a_msg := substr('ERROR Removing Recurring Activity: '||sqlerrm, 1, 2000);
         return -1;
      end if;

      return 1;

   exception when others then
      /* this catches all SQL errors, including no_data_found */
      a_msg := substr('ERROR Recurring Activity: '||sqlerrm, 1, 2000);
      return -1;
   end F_REMOVERECURRINGACTIVITY;

   function F_LOADRECURRINGACTIVITY(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                    A_MONTH       date,
                                    A_MSG         out varchar2) return number is

   begin
      pkg_pp_log.p_write_message( 'LOADING recurring activity for the month' );
      -- insert into depr activity
      FORALL i in INDICES OF a_company_ids
      insert into depr_activity
      (
         depr_activity_id, depr_group_id, set_of_books_id, gl_post_mo_yr,
         user_id, user_id1, user_id2, related_activity_id, depr_activity_code_id,
         amount, description, long_description, gl_je_code, gl_post_flag,
         credit_account, debit_account, post_period_months, asset_id, subledger_item_id
      )
      (
         select
            pwrplant1.nextval, da.depr_group_id, da.set_of_books_id, a_month,
            da.user_id, da.user_id1, da.user_id2, da.related_activity_id, da.depr_activity_code_id,
            da.amount, da.description, da.long_description, da.gl_je_code, da.gl_post_flag,
            da.credit_account, da.debit_account, da.post_period_months, da.asset_id, da.subledger_item_id
         from depr_activity_recurring da, depr_group dg
         where da.depr_group_id = dg.depr_group_id
         and dg.company_id = a_company_ids(i)
         and da.gl_post_mo_yr <= a_month
         and da.gl_post_mo_yr_end >= a_month
         and mod(months_between(a_month, da.gl_post_mo_yr), da.post_period_months) = 0
      );
      if sqlCode <> 0 then
         a_msg := substr('ERROR Inserting Recurring Activity: '||sqlerrm, 1, 2000);
         return -1;
      end if;

      -- summarize into depr ledger
      FORALL i in INDICES OF a_company_ids
      merge into depr_ledger a using
      (
         select *
         from
         (
            select da.depr_group_id, da.set_of_books_id, a_month as gl_post_mo_yr, da.description,
               sum(da.amount) as amount
            from depr_activity_recurring da, depr_group dg
            where da.depr_group_id = dg.depr_group_id
            and dg.company_id = a_company_ids(i)
            and da.gl_post_mo_yr <= a_month
            and da.gl_post_mo_yr_end >= a_month
            and mod(months_between(a_month, da.gl_post_mo_yr), da.post_period_months) = 0
            group by da.depr_group_id, da.set_of_books_id, da.gl_post_mo_yr, da.description
         ) b
         model
         partition by (b.depr_group_id, b.set_of_books_id, b.gl_post_mo_yr)
         dimension by (b.description)
         measures
         (
            amount,
            0 as depr_exp_adjust, 0 as cost_of_removal, 0 as salvage_cash,
            0 as salvage_returns, 0 as reserve_credits, 0 as reserve_tran_in,
            0 as reserve_tran_out, 0 as cor_res_tran_out, 0 as cor_res_tran_in,
            0 as cor_res_adjust, 0 as cor_exp_adjust, 0 as gain_loss,
            0 as reserve_adjustments, 0 as impairment_reserve_act, 0 as salvage_exp_adjust
         )
         rules update
         (
            depr_exp_adjust['DEPR_EXP_ADJUST'] = amount['DEPR_EXP_ADJUST'],
            cost_of_removal['COST_OF_REMOVAL'] = amount['COST_OF_REMOVAL'],
            salvage_cash['SALVAGE_CASH'] = amount['SALVAGE_CASH'],
            salvage_returns['SALVAGE_RETURNS'] = amount['SALVAGE_RETURNS'],
            reserve_credits['RESERVE_CREDITS'] = amount['RESERVE_CREDITS'],
            reserve_tran_in['RESERVE_TRANS_IN'] = amount['RESERVE_TRANS_IN'],
            reserve_tran_out['RESERVE_TRANS_OUT'] = amount['RESERVE_TRANS_OUT'],
            cor_res_tran_out['COST_OF_REMOVAL_TRANS_OUT'] = amount['COST_OF_REMOVAL_TRANS_OUT'],
            cor_res_tran_in['COST_OF_REMOVAL_TRANS_IN'] = amount['COST_OF_REMOVAL_TRANS_IN'],
            cor_res_adjust['COST_OF_REMOVAL_RES_ADJUST'] = amount['COST_OF_REMOVAL_RES_ADJUST'],
            cor_exp_adjust['COST_OF_REMOVAL_EXP_ADJUST'] = amount['COST_OF_REMOVAL_EXP_ADJUST'],
            gain_loss['GAIN_LOSS'] = amount['GAIN_LOSS'],
            reserve_adjustments['RESERVE_ADJUSTMENT'] = amount['RESERVE_ADJUSTMENT'],
            impairment_reserve_act['IMPAIRMENT_ACT'] = amount['IMPAIRMENT_ACT'],
            salvage_exp_adjust['SALVAGE_EXP_ADJUST'] = amount['SALVAGE_EXP_ADJUST']
         )
      ) b
      on
      (
         a.depr_group_id = b.depr_group_id
         and a.set_of_books_id = b.set_of_books_id
         and a.gl_post_mo_yr = b.gl_post_mo_yr
      )
      when matched then update
      set
         a.depr_exp_adjust = a.depr_exp_adjust + nvl(b.depr_exp_adjust, 0),
         a.cost_of_removal = a.cost_of_removal + nvl(b.cost_of_removal, 0),
         a.salvage_cash = a.salvage_cash + nvl(b.salvage_cash, 0),
         a.salvage_returns = a.salvage_returns + nvl(b.salvage_returns, 0),
         a.reserve_credits = a.reserve_credits + nvl(b.reserve_credits, 0),
         a.reserve_tran_in = a.reserve_tran_in + nvl(b.reserve_tran_in, 0),
         a.reserve_tran_out = a.reserve_tran_out + nvl(b.reserve_tran_out, 0),
         a.cor_res_tran_out = a.cor_res_tran_out + nvl(b.cor_res_tran_out, 0),
         a.cor_res_tran_in = a.cor_res_tran_in + nvl(b.cor_res_tran_in, 0),
         a.cor_res_adjust = a.cor_res_adjust + nvl(b.cor_res_adjust, 0),
         a.cor_exp_adjust = a.cor_exp_adjust + nvl(b.cor_exp_adjust, 0),
         a.gain_loss = a.gain_loss + nvl(b.gain_loss, 0),
         a.reserve_adjustments = a.reserve_adjustments + nvl(b.reserve_adjustments, 0),
         a.impairment_reserve_act = a.impairment_reserve_act + nvl(b.impairment_reserve_act, 0),
         a.salvage_exp_adjust = a.salvage_exp_adjust + nvl(b.salvage_exp_adjust, 0)
      ;
      if sqlCode <> 0 then
         a_msg := substr('ERROR Loading Recurring Activity: '||sqlerrm, 1, 2000);
         return -1;
      end if;

      return 1;

   exception when others then
      /* this catches all SQL errors, including no_data_found */
      a_msg := substr('ERROR Recurring Activity: '||sqlerrm, 1, 2000);
      return -1;
   end f_loadRecurringActivity;

   -- Performs recurring activity
   -- remove recurring activity
   -- then load recurring activity
   function F_RECURRINGACTIVITY(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
                                A_MSG         out varchar2) return number is

      my_num number;

   begin
      my_num := 1;
      for i in 1 .. a_months.COUNT loop
         if f_removeRecurringActivity(a_company_ids, a_months(i), a_msg) = -1 then
            my_num := -1;
            exit;
         end if;
         if f_loadRecurringActivity(a_company_ids, a_months(i), a_msg) = -1 then
            my_num := -1;
            exit;
         end if;
      end loop;
      return my_num;

   exception when others then
      /* this catches all SQL errors, including no_data_found */
      a_msg := substr('ERROR loading recurring activity: '|| sqlerrm, 1, 2000);
      return -1;
   end F_RECURRINGACTIVITY;

end PKG_PP_DEPR_ACTIVITY;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1050, 0, 10, 4, 2, 0, 37099, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037099_depr_PKG_PP_DEPR_ACTIVITY.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;