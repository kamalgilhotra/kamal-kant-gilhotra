/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050891_lessor_01_update_schedule_types_remove_compounded_rates_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/17/2018 Andrew Hill    Remove compounded rates from schedule result types
||============================================================================
*/

DROP TYPE lsr_ilr_sales_sch_result_tab;

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT AUTHID current_user IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER,
                                                          interest_income_received 		 NUMBER,
                                                          interest_income_accrued 		 NUMBER,
                                                          principal_accrued            NUMBER,
                                                          begin_receivable 				     NUMBER,
                                                          end_receivable 					     NUMBER,
                                                          begin_lt_receivable          NUMBER,
                                                          end_lt_receivable            NUMBER,
                                                          initial_direct_cost          NUMBER,
                                                          executory_accrual1           NUMBER,
                                                          executory_accrual2           NUMBER,
                                                          executory_accrual3           NUMBER,
                                                          executory_accrual4           NUMBER,
                                                          executory_accrual5           NUMBER,
                                                          executory_accrual6           NUMBER,
                                                          executory_accrual7           NUMBER,
                                                          executory_accrual8           NUMBER,
                                                          executory_accrual9           NUMBER,
                                                          executory_accrual10          NUMBER,
                                                          executory_paid1              NUMBER,
                                                          executory_paid2              NUMBER,
                                                          executory_paid3              NUMBER,
                                                          executory_paid4              NUMBER,
                                                          executory_paid5              NUMBER,
                                                          executory_paid6              NUMBER,
                                                          executory_paid7              NUMBER,
                                                          executory_paid8              NUMBER,
                                                          executory_paid9              NUMBER,
                                                          executory_paid10             NUMBER,
                                                          contingent_accrual1          NUMBER,
                                                          contingent_accrual2          NUMBER,
                                                          contingent_accrual3          NUMBER,
                                                          contingent_accrual4          NUMBER,
                                                          contingent_accrual5          NUMBER,
                                                          contingent_accrual6          NUMBER,
                                                          contingent_accrual7          NUMBER,
                                                          contingent_accrual8          NUMBER,
                                                          contingent_accrual9          NUMBER,
                                                          contingent_accrual10         NUMBER,
                                                          contingent_paid1             NUMBER,
                                                          contingent_paid2             NUMBER,
                                                          contingent_paid3             NUMBER,
                                                          contingent_paid4             NUMBER,
                                                          contingent_paid5             NUMBER,
                                                          contingent_paid6             NUMBER,
                                                          contingent_paid7             NUMBER,
                                                          contingent_paid8             NUMBER,
                                                          contingent_paid9             NUMBER,
                                                          contingent_paid10            NUMBER,
                                                          begin_unguaranteed_residual NUMBER,
                                                          int_on_unguaranteed_residual NUMBER,
                                                          end_unguaranteed_residual NUMBER,
                                                          begin_net_investment NUMBER,
                                                          int_on_net_investment NUMBER,
                                                          end_net_investment NUMBER,
                                                          rate_implicit FLOAT,
                                                          discount_rate FLOAT,
                                                          rate_implicit_ni FLOAT,
                                                          discount_rate_ni FLOAT,
                                                          begin_lease_receivable number,
                                                          npv_lease_payments NUMBER,
                                                          npv_guaranteed_residual NUMBER,
                                                          npv_unguaranteed_residual NUMBER,
                                                          selling_profit_loss NUMBER,
                                                          cost_of_goods_sold NUMBER,
                                                          schedule_rates t_lsr_ilr_schedule_all_rates);
/

CREATE TYPE lsr_ilr_sales_sch_result_tab AS TABLE OF lsr_ilr_sales_sch_result;
/

DROP TYPE lsr_ilr_df_schedule_result_tab;

CREATE OR REPLACE TYPE lsr_ilr_df_schedule_result AUTHID current_user IS OBJECT(MONTH DATE,
                                                            principal_received NUMBER,
                                                            interest_income_received 		 NUMBER,
                                                            interest_income_accrued 		 NUMBER,
                                                            principal_accrued            NUMBER,
                                                            begin_receivable 				     NUMBER,
                                                            end_receivable 					     NUMBER,
                                                            begin_lt_receivable          NUMBER,
                                                            end_lt_receivable            NUMBER,
                                                            initial_direct_cost          NUMBER,
                                                            executory_accrual1           NUMBER,
                                                            executory_accrual2           NUMBER,
                                                            executory_accrual3           NUMBER,
                                                            executory_accrual4           NUMBER,
                                                            executory_accrual5           NUMBER,
                                                            executory_accrual6           NUMBER,
                                                            executory_accrual7           NUMBER,
                                                            executory_accrual8           NUMBER,
                                                            executory_accrual9           NUMBER,
                                                            executory_accrual10          NUMBER,
                                                            executory_paid1              NUMBER,
                                                            executory_paid2              NUMBER,
                                                            executory_paid3              NUMBER,
                                                            executory_paid4              NUMBER,
                                                            executory_paid5              NUMBER,
                                                            executory_paid6              NUMBER,
                                                            executory_paid7              NUMBER,
                                                            executory_paid8              NUMBER,
                                                            executory_paid9              NUMBER,
                                                            executory_paid10             NUMBER,
                                                            contingent_accrual1          NUMBER,
                                                            contingent_accrual2          NUMBER,
                                                            contingent_accrual3          NUMBER,
                                                            contingent_accrual4          NUMBER,
                                                            contingent_accrual5          NUMBER,
                                                            contingent_accrual6          NUMBER,
                                                            contingent_accrual7          NUMBER,
                                                            contingent_accrual8          NUMBER,
                                                            contingent_accrual9          NUMBER,
                                                            contingent_accrual10         NUMBER,
                                                            contingent_paid1             NUMBER,
                                                            contingent_paid2             NUMBER,
                                                            contingent_paid3             NUMBER,
                                                            contingent_paid4             NUMBER,
                                                            contingent_paid5             NUMBER,
                                                            contingent_paid6             NUMBER,
                                                            contingent_paid7             NUMBER,
                                                            contingent_paid8             NUMBER,
                                                            contingent_paid9             NUMBER,
                                                            contingent_paid10            NUMBER,
                                                            begin_unguaranteed_residual NUMBER,
                                                            int_on_unguaranteed_residual NUMBER,
                                                            end_unguaranteed_residual NUMBER,
                                                            begin_net_investment NUMBER,
                                                            int_on_net_investment NUMBER,
                                                            end_net_investment NUMBER,
                                                            begin_deferred_profit number,
                                                            recognized_profit number,
                                                            end_deferred_profit number,
                                                            rate_implicit FLOAT,
                                                            discount_rate FLOAT,
                                                            rate_implicit_ni FLOAT,
                                                            discount_rate_ni FLOAT,
                                                            begin_lease_receivable number,
                                                            npv_lease_payments NUMBER,
                                                            npv_guaranteed_residual NUMBER,
                                                            npv_unguaranteed_residual NUMBER,
                                                            selling_profit_loss NUMBER,
                                                            cost_of_goods_sold NUMBER,
                                                            schedule_rates t_lsr_ilr_schedule_all_rates);
/

CREATE TYPE lsr_ilr_df_schedule_result_tab AS TABLE OF lsr_ilr_df_schedule_result;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4628, 0, 2017, 3, 0, 0, 50891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050891_lessor_01_update_schedule_types_remove_compounded_rates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;