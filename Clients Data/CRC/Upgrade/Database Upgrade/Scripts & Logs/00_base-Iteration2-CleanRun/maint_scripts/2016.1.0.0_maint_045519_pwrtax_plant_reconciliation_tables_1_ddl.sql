/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045519_pwrtax_plant_reconciliation_tables_1_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 03/21/2016 Anand R			 Change column name on a table   
||                                           TAX_PLANT_RECON_ADJ
||============================================================================
*/ 

alter table TAX_PLANT_RECON_ADJ rename column DESCRITION to DESCRIPTION;

comment on column PWRPLANT.TAX_PLANT_RECON_ADJ.DESCRIPTION is 'Description of a tax plant reconciliation adjustment.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3113, 0, 2016, 1, 0, 0, 045519, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045519_pwrtax_plant_reconciliation_tables_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;