/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044168_cr_remove_pp_translate_dml.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 2015.2   06/19/2015 Daniel Mendel        Remove PP Translate from PP Int Components
||========================================================================================
*/

delete from pp_integration_components where lower(trim(component)) = 'pp translate';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2620, 0, 2015, 2, 0, 0, 044168, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044168_cr_remove_pp_translate_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;