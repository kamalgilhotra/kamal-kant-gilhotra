/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044588_taxrpr_rpt_1310.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 2015.2     08/28/2015 Andrew Scott    This datawindow was changed to not be hidden.
||                                       reverses maint_031889_taxrpr.sql
||============================================================================
*/

update PP_REPORTS set PP_REPORT_ENVIR_ID = 3 where lower(trim(DATAWINDOW)) = 'dw_rpr_rpt_cwip_gen_blend';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2833, 0, 2015, 2, 0, 0, 044588, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044588_taxrpr_rpt_1310.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;