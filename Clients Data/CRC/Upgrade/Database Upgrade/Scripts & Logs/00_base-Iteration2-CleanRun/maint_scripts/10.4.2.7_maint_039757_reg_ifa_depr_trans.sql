/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039757_reg_ifa_depr_trans.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/26/2014 Ryan Oliveria
||============================================================================
*/

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS modify FCST_DEPR_VERSION_ID null;
alter table REG_INCREMENTAL_DEPR_ADJ_TRANS modify SET_OF_BOOKS_ID      null;
alter table REG_INCREMENTAL_DEPR_ADJ_TRANS modify FCST_DEPR_GROUP_ID   null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1398, 0, 10, 4, 2, 7, 39757, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039757_reg_ifa_depr_trans.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
