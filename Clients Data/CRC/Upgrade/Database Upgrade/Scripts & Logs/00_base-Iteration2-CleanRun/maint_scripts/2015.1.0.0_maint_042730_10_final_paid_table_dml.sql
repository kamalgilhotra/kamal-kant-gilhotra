/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_10_final_paid_table_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

delete from ls_funding_status;

insert into ls_funding_status
(funding_status_id, description, long_description)
select 0, 'New', 'No Invoices Allocated Yet Under ILR' from dual union
select 1, 'Interim Submit', 'At Least one Invoice is Associated but None are Posted' from dual union
select 2, 'Interim Paid', 'All Invoices on ILR are Posted' from dual union
select 3, 'Final Submit', 'There is at Least One Unposted Invoice Under this ILR' from dual union
select 4, 'Final Paid', 'All Invoices on this ILR are Posted and the ILR has Been Marked Final' from dual;

update ls_ilr set funding_status_id = 0;




update ls_ilr ilr set funding_status_id = 1
where ilr_status_id <3
  and funding_status_id <> 4
  and exists (select 1
              from ls_component_charge cc, ls_component c, ls_asset la
              where cc.component_id = c.component_id
                and c.ls_asset_id = la.ls_asset_id
                and la.ilr_id=ilr.ilr_id
                and cc.interim_interest_start_date is null)
  and not exists (select 1
                  from ls_component_charge cc, ls_component c, ls_asset la
              where cc.component_id = c.component_id
                and c.ls_asset_id = la.ls_asset_id
                and la.ilr_id=ilr.ilr_id
                and cc.interim_interest_start_date is not null);

update ls_ilr ilr set funding_status_id = 2
where ilr_status_id <3
  and funding_status_id <> 4
  and exists (select 1
              from ls_component_charge cc, ls_component c, ls_asset la
              where cc.component_id = c.component_id
                and c.ls_asset_id = la.ls_asset_id
                and la.ilr_id=ilr.ilr_id
                and cc.interim_interest_start_date is not null)
  and not exists (select 1
                  from ls_component_charge cc, ls_component c, ls_asset la
              where cc.component_id = c.component_id
                and c.ls_asset_id = la.ls_asset_id
                and la.ilr_id=ilr.ilr_id
                and cc.interim_interest_start_date is null);

update ls_ilr ilr set funding_status_id = 3
where ilr_status_id <3
  and funding_status_id <> 4
  and exists (select 1
              from ls_component_charge cc, ls_component c, ls_asset la
              where cc.component_id = c.component_id
                and c.ls_asset_id = la.ls_asset_id
                and la.ilr_id=ilr.ilr_id
                and cc.interim_interest_start_date is not null)
  and exists (select 1
                  from ls_component_charge cc, ls_component c, ls_asset la
              where cc.component_id = c.component_id
                and c.ls_asset_id = la.ls_asset_id
                and la.ilr_id=ilr.ilr_id
                and cc.interim_interest_start_date is null);

update ls_ilr set funding_status_id = 4
where ilr_status_id = 3;

commit;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2409, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_10_final_paid_table_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;