/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050180_lessor_07_modify_ilr_rate_datatype_ddl.sql
|| Description:	Create view to display ILR rates compounded by frequencies
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/20/2018 Andrew Hill    Change rate datatype to Float for greater precision (note Oracle Float type is NOT IEEE floating point)
||============================================================================
*/

alter table lsr_ilr_rates modify (rate float);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4229, 0, 2017, 3, 0, 0, 50180, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050180_lessor_07_modify_ilr_rate_datatype_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;