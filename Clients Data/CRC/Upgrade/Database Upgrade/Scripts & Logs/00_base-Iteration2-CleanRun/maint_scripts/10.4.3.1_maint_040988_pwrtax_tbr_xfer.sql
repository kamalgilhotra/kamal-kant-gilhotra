/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040988_pwrtax_tbr_xfer.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.1 11/04/2014 Andrew Scott        update tbr xfer amt to be 0, not null.
||
||========================================================================================
*/

update tax_book_reconcile
set basis_amount_transfer = 0 
where basis_amount_transfer is null;


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (2005, 0, 10, 4, 3, 1, 40988, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_040988_pwrtax_tbr_xfer.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;