/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_19_component_import_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

alter table ls_import_component
add (invoice_number varchar2(35), invoice_date varchar2(35), invoice_amount number(22,2), unique_component_identifier varchar2(35))
;
alter table ls_import_component_archive
add (invoice_number varchar2(35), invoice_date varchar2(35), invoice_amount number(22,2), unique_component_identifier varchar2(35))
;

COMMENT ON COLUMN ls_import_component.invoice_number IS 'Invoice number associated with component to be imported.';
COMMENT ON COLUMN ls_import_component.invoice_date IS 'Invoice date associated with invoice to be imported.';
COMMENT ON COLUMN ls_import_component.invoice_amount IS 'Invoice amount associated with invoice to be imported.';
COMMENT ON COLUMN ls_import_component.unique_component_identifier IS 'Unique component identifier used to allow multiple invoice lines per component.';

COMMENT ON COLUMN ls_import_component_archive.invoice_number IS 'Invoice number associated with component to be imported.';
COMMENT ON COLUMN ls_import_component_archive.invoice_date IS 'Invoice date associated with invoice to be imported.';
COMMENT ON COLUMN ls_import_component_archive.invoice_amount IS 'Invoice amount associated with invoice to be imported.';
COMMENT ON COLUMN ls_import_component_archive.unique_component_identifier IS 'Unique component identifier used to allow multiple invoice lines per component.';




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2399, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_19_component_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;