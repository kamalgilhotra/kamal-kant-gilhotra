/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_051349_lessee_04_new_approval_status_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2017.4.0.0 07/03/2018 Sarah Byers      New Approval Status to support payment shift
||============================================================================
*/

insert into ls_ilr_status (ilr_status_id, description, long_description)
values (6, 'Approved, Pending In Service', 'Approved, Pending In Service');

insert into ls_asset_status (ls_asset_status_id, description, long_description)
values (5, 'Approved, Pending In Service', 'Approved, Pending In Service');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7562, 0, 2017, 4, 0, 0, 51349, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051349_lessee_04_new_approval_status_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;