/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049517_lessor_03_reestablish_ilr_schedule_links_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/09/2017 Andrew Hill      Reestablish keys, views, etc. referencing ls_ilr_schedule
||============================================================================
*/

ALTER TABLE lsr_ilr_schedule_sales_direct ADD CONSTRAINT r_lsr_ilr_sched_sales_direct3 FOREIGN KEY ( ilr_id,revision,set_of_books_id,MONTH )
    REFERENCES LSR_ILR_SCHEDULE ( ILR_ID,REVISION,SET_OF_BOOKS_ID,MONTH );

CREATE MATERIALIZED VIEW LOG ON LSR_ILR_SCHEDULE WITH
  ROWID
  INCLUDING NEW VALUES;

CREATE MATERIALIZED VIEW MV_LSR_ILR_MC_SCHEDULE_AMOUNTS (
  ILR_ID,
  ILR_NUMBER,
  CURRENT_REVISION,
  REVISION,
  SET_OF_BOOKS_ID,
  MONTH,
  INTEREST_INCOME_RECEIVED,
  INTEREST_INCOME_ACCRUED,
  BEG_DEFERRED_REV,
  DEFERRED_REV_ACTIVITY,
  END_DEFERRED_REV,
  BEG_RECEIVABLE,
  END_RECEIVABLE,
  beg_long_term_receivable,
  end_long_term_receivable,
  initial_direct_cost,
  EXECUTORY_ACCRUAL1,
  EXECUTORY_ACCRUAL2,
  EXECUTORY_ACCRUAL3,
  EXECUTORY_ACCRUAL4,
  EXECUTORY_ACCRUAL5,
  EXECUTORY_ACCRUAL6,
  EXECUTORY_ACCRUAL7,
  EXECUTORY_ACCRUAL8,
  EXECUTORY_ACCRUAL9,
  EXECUTORY_ACCRUAL10,
  EXECUTORY_PAID1,
  EXECUTORY_PAID2,
  EXECUTORY_PAID3,
  EXECUTORY_PAID4,
  EXECUTORY_PAID5,
  EXECUTORY_PAID6,
  EXECUTORY_PAID7,
  EXECUTORY_PAID8,
  EXECUTORY_PAID9,
  EXECUTORY_PAID10,
  CONTINGENT_ACCRUAL1,
  CONTINGENT_ACCRUAL2,
  CONTINGENT_ACCRUAL3,
  CONTINGENT_ACCRUAL4,
  CONTINGENT_ACCRUAL5,
  CONTINGENT_ACCRUAL6,
  CONTINGENT_ACCRUAL7,
  CONTINGENT_ACCRUAL8,
  CONTINGENT_ACCRUAL9,
  CONTINGENT_ACCRUAL10,
  CONTINGENT_PAID1,
  CONTINGENT_PAID2,
  CONTINGENT_PAID3,
  CONTINGENT_PAID4,
  CONTINGENT_PAID5,
  CONTINGENT_PAID6,
  CONTINGENT_PAID7,
  CONTINGENT_PAID8,
  CONTINGENT_PAID9,
  CONTINGENT_PAID10,
  PRINCIPAL_RECEIVED,
  PRINCIPAL_ACCRUED,
  BEG_UNGUARANTEED_RESIDUAL,
  INTEREST_UNGUARANTEED_RESIDUAL,
  ENDING_UNGUARANTEED_RESIDUAL,
  BEG_NET_INVESTMENT,
  INTEREST_NET_INVESTMENT,
  ENDING_NET_INVESTMENT,
  LEASE_ID,
  COMPANY_ID,
  IN_SERVICE_EXCHANGE_RATE,
  PURCHASE_OPTION_AMT,
  TERMINATION_AMT,
  LISROWID,
  OPTROWID,
  ILRROWID,
  LEASEROWID,
  SALESROWID
) BUILD IMMEDIATE REFRESH FAST ON COMMIT AS 
SELECT schedule.ilr_id,
       ilr.ilr_number,
       ilr.current_revision,
       schedule.revision,
       schedule.set_of_books_id,
       schedule.month,
       schedule.interest_income_received,
       schedule.interest_income_accrued,
       schedule.beg_deferred_rev,
       schedule.deferred_rev_activity,
       schedule.end_deferred_rev,
       schedule.beg_receivable,
       schedule.end_receivable,
       schedule.beg_lt_receivable,
       schedule.end_lt_receivable,
       schedule.initial_direct_cost,
       schedule.executory_accrual1,
       schedule.executory_accrual2,
       schedule.executory_accrual3,
       schedule.executory_accrual4,
       schedule.executory_accrual5,
       schedule.executory_accrual6,
       schedule.executory_accrual7,
       schedule.executory_accrual8,
       schedule.executory_accrual9,
       schedule.executory_accrual10,
       schedule.executory_paid1,
       schedule.executory_paid2,
       schedule.executory_paid3,
       schedule.executory_paid4,
       schedule.executory_paid5,
       schedule.executory_paid6,
       schedule.executory_paid7,
       schedule.executory_paid8,
       schedule.executory_paid9,
       schedule.executory_paid10,
       schedule.contingent_accrual1,
       schedule.contingent_accrual2,
       schedule.contingent_accrual3,
       schedule.contingent_accrual4,
       schedule.contingent_accrual5,
       schedule.contingent_accrual6,
       schedule.contingent_accrual7,
       schedule.contingent_accrual8,
       schedule.contingent_accrual9,
       schedule.contingent_accrual10,
       schedule.contingent_paid1,
       schedule.contingent_paid2,
       schedule.contingent_paid3,
       schedule.contingent_paid4,
       schedule.contingent_paid5,
       schedule.contingent_paid6,
       schedule.contingent_paid7,
       schedule.contingent_paid8,
       schedule.contingent_paid9,
       schedule.contingent_paid10,
       st_schedule.principal_received,
       st_schedule.principal_accrued,
       st_schedule.beg_unguaranteed_residual,
       st_schedule.interest_unguaranteed_residual,
       st_schedule.ending_unguaranteed_residual,
       st_schedule.beg_net_investment,
       st_schedule.interest_net_investment,
       st_schedule.ending_net_investment,
       ilr.lease_id,
       ilr.company_id,
       options.in_service_exchange_rate,
       options.purchase_option_amt,
       options.termination_amt,
       schedule.rowid AS lisrowid,
       options.rowid AS optrowid,
       ilr.rowid AS ilrrowid,
       lease.rowid AS leaserowid,
       st_schedule.rowid AS salesrowid
FROM lsr_ilr_schedule schedule,
     lsr_ilr_options options,
     lsr_ilr ilr,
     lsr_lease lease,
     lsr_ilr_schedule_sales_direct st_schedule
WHERE schedule.ilr_id = options.ilr_id
  AND schedule.revision = options.revision
  AND schedule.ilr_id = ilr.ilr_id
  AND ilr.lease_id = lease.lease_id
  AND st_schedule.ilr_id (+) = schedule.ilr_id
  AND st_schedule.revision (+) = schedule.revision
  AND st_schedule.month (+) = schedule.month
  AND st_schedule.set_of_books_id (+) = schedule.set_of_books_id;

CREATE INDEX MV_LSR_ILR_MC_SCH_AMT_IDX ON
  MV_LSR_ILR_MC_SCHEDULE_AMOUNTS (
    ILR_ID,
    REVISION,
    MONTH,
    set_of_books_id
  ) tablespace pwrplant_idx;

CREATE INDEX mv_lsr_ilr_mc_sch_amt_mth_idx ON
  MV_LSR_ILR_MC_SCHEDULE_AMOUNTS ( MONTH ) tablespace pwrplant_idx;

CREATE INDEX mv_lsr_ilr_mc_sch_amt_cl_idx ON
  MV_LSR_ILR_MC_SCHEDULE_AMOUNTS ( COMPANY_ID,LEASE_ID ) tablespace pwrplant_idx;

CREATE INDEX mv_lsr_ilr_mc_sch_amt_ls_idx ON
  MV_LSR_ILR_MC_SCHEDULE_AMOUNTS ( LEASE_ID ) tablespace pwrplant_idx;

CREATE INDEX mv_lsr_ilr_mc_sch_lisrid_idx ON
  MV_LSR_ILR_MC_SCHEDULE_AMOUNTS ( LISROWID ) tablespace pwrplant_idx;

CREATE INDEX MV_LSR_ILR_MC_SCH_OPTRID_IDX ON
  MV_LSR_ILR_MC_SCHEDULE_AMOUNTS ( OPTROWID ) tablespace pwrplant_idx;

CREATE INDEX mv_lsr_ilr_mc_sch_ilrrid_idx ON
  MV_LSR_ILR_MC_SCHEDULE_AMOUNTS ( ILRROWID ) tablespace pwrplant_idx;

CREATE INDEX MV_LSR_ILR_MC_SCH_LSRID_IDX ON
  mv_lsr_ilr_mc_schedule_amounts ( leaserowid ) TABLESPACE pwrplant_idx;

CREATE INDEX mv_lsr_ilr_mc_sch_slsrid_idx ON
  mv_lsr_ilr_mc_schedule_amounts ( salesrowid ) TABLESPACE pwrplant_idx;

COMMENT ON MATERIALIZED VIEW MV_LSR_ILR_MC_SCHEDULE_AMOUNTS IS
  'snapshot table for snapshot PWRPLANT.MV_LSR_ILR_MC_SCHEDULE_AMOUNTS';

drop table lsr_ilr_schedule_old;

CREATE OR REPLACE VIEW v_lsr_ilr_mc_schedule AS 
WITH cur AS ( SELECT ls_currency_type_id AS ls_cur_type,
       currency_id,
       currency_display_symbol,
       iso_code,
         CASE
      ls_currency_type_id
      WHEN
        1
      THEN
        1
      ELSE
        NULL
    END
  AS contract_approval_rate
FROM currency
  CROSS JOIN ls_lease_currency_type
),open_month AS ( SELECT company_id,
       MIN(gl_posting_mo_yr) open_month
FROM lsr_process_control WHERE open_next IS NULL GROUP BY
  company_id
),calc_rate AS ( SELECT a.company_id,
       a.contract_currency_id,
       a.company_currency_id,
       a.accounting_month,
       a.exchange_date,
       a.rate,
       b.rate prev_rate
FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = add_months(b.accounting_month,1)
),rate_now AS ( SELECT currency_from,
       currency_to,
       rate
FROM ( SELECT currency_from,
         currency_to,
         rate,
         ROW_NUMBER() OVER(PARTITION BY
      currency_from,
      currency_to
      ORDER BY
        exchange_date
      DESC
    ) AS rn
  FROM currency_rate_default_dense WHERE trunc(exchange_date,'MONTH') <= trunc(SYSDATE,'MONTH')
    AND exchange_rate_type_id = 1
  )
WHERE rn = 1 ) SELECT schedule.ilr_id ilr_id,
       schedule.ilr_number,
       lease.lease_id,
       lease.lease_number,
       schedule.current_revision,
       schedule.revision revision,
       schedule.set_of_books_id set_of_books_id,
       schedule.month month,
       open_month.company_id,
       open_month.open_month,
       cur.ls_cur_type AS ls_cur_type,
       rates.exchange_date,
       calc_rate.exchange_date prev_exchange_date,
       lease.contract_currency_id,
       cur.currency_id display_currency_id,
       rates.rate,
       calc_rate.rate calculated_rate,
       calc_rate.prev_rate previous_calculated_rate,
       cur.iso_code,
       cur.currency_display_symbol,
       schedule.interest_income_received * nvl(
    calc_rate.rate,
    rates.rate
  ) interest_income_received,
       schedule.interest_income_accrued * nvl(
    calc_rate.rate,
    rates.rate
  ) interest_income_accrued,
       schedule.beg_deferred_rev * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_deferred_rev,
       schedule.deferred_rev_activity * nvl(
    calc_rate.rate,
    rates.rate
  ) deferred_rev_activity,
       schedule.end_deferred_rev * nvl(
    calc_rate.rate,
    rates.rate
  ) end_deferred_rev,
       schedule.beg_receivable * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_receivable,
       schedule.end_receivable * nvl(
    calc_rate.rate,
    rates.rate
  ) end_receivable,
         schedule.beg_long_term_receivable * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_long_term_receivable,
       schedule.end_long_term_receivable * nvl(
    calc_rate.rate,
    rates.rate
  ) end_long_term_receivable,
       schedule.initial_direct_cost * nvl(
    calc_rate.rate,
    rates.rate
  ) initial_direct_cost,
       schedule.executory_accrual1 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual1,
       schedule.executory_accrual2 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual2,
       schedule.executory_accrual3 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual3,
       schedule.executory_accrual4 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual4,
       schedule.executory_accrual5 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual5,
       schedule.executory_accrual6 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual6,
       schedule.executory_accrual7 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual7,
       schedule.executory_accrual8 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual8,
       schedule.executory_accrual9 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual9,
       schedule.executory_accrual10 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual10,
       schedule.executory_paid1 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid1,
       schedule.executory_paid2 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid2,
       schedule.executory_paid3 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid3,
       schedule.executory_paid4 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid4,
       schedule.executory_paid5 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid5,
       schedule.executory_paid6 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid6,
       schedule.executory_paid7 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid7,
       schedule.executory_paid8 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid8,
       schedule.executory_paid9 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid9,
       schedule.executory_paid10 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid10,
       schedule.contingent_accrual1 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual1,
       schedule.contingent_accrual2 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual2,
       schedule.contingent_accrual3 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual3,
       schedule.contingent_accrual4 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual4,
       schedule.contingent_accrual5 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual5,
       schedule.contingent_accrual6 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual6,
       schedule.contingent_accrual7 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual7,
       schedule.contingent_accrual8 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual8,
       schedule.contingent_accrual9 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual9,
       schedule.contingent_accrual10 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual10,
       schedule.contingent_paid1 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid1,
       schedule.contingent_paid2 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid2,
       schedule.contingent_paid3 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid3,
       schedule.contingent_paid4 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid4,
       schedule.contingent_paid5 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid5,
       schedule.contingent_paid6 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid6,
       schedule.contingent_paid7 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid7,
       schedule.contingent_paid8 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid8,
       schedule.contingent_paid9 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid9,
       schedule.contingent_paid10 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid10,
       schedule.principal_received * nvl(
    calc_rate.rate,
    rates.rate
  ) principal_received,
       schedule.principal_accrued * nvl(
    calc_rate.rate,
    rates.rate
  ) principal_accrued,
       schedule.beg_unguaranteed_residual * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_unguaranteed_residual,
       schedule.interest_unguaranteed_residual * nvl(
    calc_rate.rate,
    rates.rate
  ) interest_unguaranteed_residual,
       schedule.ending_unguaranteed_residual * nvl(
    calc_rate.rate,
    rates.rate
  ) ending_unguaranteed_residual,
       schedule.beg_net_investment * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_net_investment,
       schedule.interest_net_investment * nvl(
    calc_rate.rate,
    rates.rate
  ) interest_net_investment,
       schedule.ending_net_investment * nvl(
    calc_rate.rate,
    rates.rate
  ) ending_net_investment,
       schedule.beg_receivable * ( nvl(calc_rate.rate,0) - nvl(calc_rate.prev_rate,0) ) gain_loss_fx
FROM mv_lsr_ilr_mc_schedule_amounts schedule
  INNER JOIN lsr_lease lease ON schedule.lease_id = lease.lease_id
  INNER JOIN currency_schema cs ON schedule.company_id = cs.company_id
  INNER JOIN cur ON cur.currency_id =
    CASE
      cur.ls_cur_type
      WHEN
        1
      THEN
        lease.contract_currency_id
      WHEN
        2
      THEN
        cs.currency_id
    END
  INNER JOIN open_month ON schedule.company_id = open_month.company_id
  INNER JOIN currency_rate_default_dense rates ON cur.currency_id = rates.currency_to
  AND lease.contract_currency_id = rates.currency_from
  AND trunc(rates.exchange_date,'MONTH') = trunc(schedule.month,'MONTH')
  INNER JOIN rate_now ON cur.currency_id = rate_now.currency_to
  AND lease.contract_currency_id = rate_now.currency_from
  LEFT OUTER JOIN calc_rate ON lease.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND schedule.company_id = calc_rate.company_id
  AND schedule.month = calc_rate.accounting_month
WHERE cs.currency_type_id = 1
  AND rates.exchange_rate_type_id = 1;
  
CREATE OR REPLACE VIEW v_lsr_pseudo_asset_schedule AS
SELECT lsr_asset_id,
       revision,
       set_of_books_id,
       month,
       interest_income_received * allocation AS interest_income_received,
       interest_income_accrued * allocation AS interest_income_accrued,
       interest_rental_recvd_spread * allocation AS interest_rental_recvd_spread,
       beg_deferred_rev * allocation AS beg_deferred_rev,
       deferred_rev_activity * allocation AS deferred_rev_activity,
       end_deferred_rev * allocation AS end_deferred_rev,
       beg_receivable * allocation AS beg_receivable,
       end_receivable * allocation AS end_receivable,
       beg_lt_receivable * allocation AS beg_lt_receivable,
       end_lt_receivable * allocation AS end_lt_receivable,
       initial_direct_cost * allocation as initial_direct_cost,
       executory_accrual1 * allocation AS executory_accrual1,
       executory_accrual2 * allocation AS executory_accrual2,
       executory_accrual3 * allocation AS executory_accrual3,
       executory_accrual4 * allocation AS executory_accrual4,
       executory_accrual5 * allocation AS executory_accrual5,
       executory_accrual6 * allocation AS executory_accrual6,
       executory_accrual7 * allocation AS executory_accrual7,
       executory_accrual8 * allocation AS executory_accrual8,
       executory_accrual9 * allocation AS executory_accrual9,
       executory_accrual10 * allocation AS executory_accrual10,
       executory_paid1 * allocation AS executory_paid1,
       executory_paid2 * allocation AS executory_paid2,
       executory_paid3 * allocation AS executory_paid3,
       executory_paid4 * allocation AS executory_paid4,
       executory_paid5 * allocation AS executory_paid5,
       executory_paid6 * allocation AS executory_paid6,
       executory_paid7 * allocation AS executory_paid7,
       executory_paid8 * allocation AS executory_paid8,
       executory_paid9 * allocation AS executory_paid9,
       executory_paid10 * allocation AS executory_paid10,
       contingent_accrual1 * allocation AS contingent_accrual1,
       contingent_accrual2 * allocation AS contingent_accrual2,
       contingent_accrual3 * allocation AS contingent_accrual3,
       contingent_accrual4 * allocation AS contingent_accrual4,
       contingent_accrual5 * allocation AS contingent_accrual5,
       contingent_accrual6 * allocation AS contingent_accrual6,
       contingent_accrual7 * allocation AS contingent_accrual7,
       contingent_accrual8 * allocation AS contingent_accrual8,
       contingent_accrual9 * allocation AS contingent_accrual9,
       contingent_accrual10 * allocation AS contingent_accrual10,
       contingent_paid1 * allocation AS contingent_paid1,
       contingent_paid2 * allocation AS contingent_paid2,
       contingent_paid3 * allocation AS contingent_paid3,
       contingent_paid4 * allocation AS contingent_paid4,
       contingent_paid5 * allocation AS contingent_paid5,
       contingent_paid6 * allocation AS contingent_paid6,
       contingent_paid7 * allocation AS contingent_paid7,
       contingent_paid8 * allocation AS contingent_paid8,
       contingent_paid9 * allocation AS contingent_paid9,
       contingent_paid10 * allocation AS contingent_paid10
FROM ( SELECT asset.lsr_asset_id,
         sch.revision,
         sch.set_of_books_id,
         sch.month,
         sch.interest_income_received,
         sch.interest_income_accrued,
         sch.interest_rental_recvd_spread,
         sch.beg_deferred_rev,
         sch.deferred_rev_activity,
         sch.end_deferred_rev,
         sch.beg_receivable,
         sch.end_receivable,
         sch.beg_lt_receivable,
         sch.end_lt_receivable,
         sch.initial_direct_cost,
         sch.executory_accrual1,
         sch.executory_accrual2,
         sch.executory_accrual3,
         sch.executory_accrual4,
         sch.executory_accrual5,
         sch.executory_accrual6,
         sch.executory_accrual7,
         sch.executory_accrual8,
         sch.executory_accrual9,
         sch.executory_accrual10,
         sch.executory_paid1,
         sch.executory_paid2,
         sch.executory_paid3,
         sch.executory_paid4,
         sch.executory_paid5,
         sch.executory_paid6,
         sch.executory_paid7,
         sch.executory_paid8,
         sch.executory_paid9,
         sch.executory_paid10,
         sch.contingent_accrual1,
         sch.contingent_accrual2,
         sch.contingent_accrual3,
         sch.contingent_accrual4,
         sch.contingent_accrual5,
         sch.contingent_accrual6,
         sch.contingent_accrual7,
         sch.contingent_accrual8,
         sch.contingent_accrual9,
         sch.contingent_accrual10,
         sch.contingent_paid1,
         sch.contingent_paid2,
         sch.contingent_paid3,
         sch.contingent_paid4,
         sch.contingent_paid5,
         sch.contingent_paid6,
         sch.contingent_paid7,
         sch.contingent_paid8,
         sch.contingent_paid9,
         sch.contingent_paid10,
         coalesce(
      RATIO_TO_REPORT(
        asset.fair_market_value
      ) OVER(PARTITION BY
        asset.ilr_id,
        asset.revision,
        sch.month,
        sch.set_of_books_id
      ),
      0
    ) AS allocation
  FROM lsr_ilr_schedule sch
    JOIN lsr_asset asset ON sch.ilr_id = asset.ilr_id
    AND sch.revision = asset.revision
  );

drop type lsr_ilr_op_sch_result_tab;

CREATE OR REPLACE TYPE lsr_ilr_op_sch_result IS OBJECT( MONTH DATE,
                                                        interest_income_received 		 NUMBER(22,2),
                                                        interest_income_accrued 		 NUMBER(22,2),
                                                        interest_rental_recvd_spread NUMBER(22,2),
                                                        begin_deferred_rev 				   NUMBER(22,2),
                                                        deferred_rev 					       NUMBER(22,2),
                                                        end_deferred_rev 				     NUMBER(22,2),
                                                        begin_receivable 				     NUMBER(22,2),
                                                        end_receivable 					     NUMBER(22,2),
                                                        begin_lt_receivable          NUMBER(22,2),
                                                        end_lt_receivable            NUMBER(22,2),
                                                        initial_direct_cost          NUMBER(22,2),
                                                        executory_accrual1           NUMBER(22,2),
                                                        executory_accrual2           NUMBER(22,2),
                                                        executory_accrual3           NUMBER(22,2),
                                                        executory_accrual4           NUMBER(22,2),
                                                        executory_accrual5           NUMBER(22,2),
                                                        executory_accrual6           NUMBER(22,2),
                                                        executory_accrual7           NUMBER(22,2),
                                                        executory_accrual8           NUMBER(22,2),
                                                        executory_accrual9           NUMBER(22,2),
                                                        executory_accrual10          NUMBER(22,2),
                                                        executory_paid1              NUMBER(22,2),
                                                        executory_paid2              NUMBER(22,2),
                                                        executory_paid3              NUMBER(22,2),
                                                        executory_paid4              NUMBER(22,2),
                                                        executory_paid5              NUMBER(22,2),
                                                        executory_paid6              NUMBER(22,2),
                                                        executory_paid7              NUMBER(22,2),
                                                        executory_paid8              NUMBER(22,2),
                                                        executory_paid9              NUMBER(22,2),
                                                        executory_paid10             NUMBER(22,2),
                                                        contingent_accrual1          NUMBER(22,2),
                                                        contingent_accrual2          NUMBER(22,2),
                                                        contingent_accrual3          NUMBER(22,2),
                                                        contingent_accrual4          NUMBER(22,2),
                                                        contingent_accrual5          NUMBER(22,2),
                                                        contingent_accrual6          NUMBER(22,2),
                                                        contingent_accrual7          NUMBER(22,2),
                                                        contingent_accrual8          NUMBER(22,2),
                                                        contingent_accrual9          NUMBER(22,2),
                                                        contingent_accrual10         NUMBER(22,2),
                                                        contingent_paid1             NUMBER(22,2),
                                                        contingent_paid2             NUMBER(22,2),
                                                        contingent_paid3             NUMBER(22,2),
                                                        contingent_paid4             NUMBER(22,2),
                                                        contingent_paid5             NUMBER(22,2),
                                                        contingent_paid6             NUMBER(22,2),
                                                        contingent_paid7             NUMBER(22,2),
                                                        contingent_paid8             NUMBER(22,2),
                                                        contingent_paid9             NUMBER(22,2),
                                                        contingent_paid10            NUMBER(22,2));
/
                                                       
CREATE TYPE lsr_ilr_op_sch_result_tab AS TABLE OF lsr_ilr_op_sch_result;
/

drop type lsr_ilr_sales_sch_result_tab;

create or replace TYPE lsr_ilr_sales_sch_result IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER(22,2),
                                                          interest_income_received 		 NUMBER(22,2),
                                                          interest_income_accrued 		 NUMBER(22,2),
                                                          principal_accrued            NUMBER(22,2),
                                                          begin_receivable 				     NUMBER(22,2),
                                                          end_receivable 					     NUMBER(22,2),
                                                          begin_lt_receivable          NUMBER(22,2),
                                                          end_lt_receivable            NUMBER(22,2),
                                                          initial_direct_cost          NUMBER(22,2),
                                                          executory_accrual1           NUMBER(22,2),
                                                          executory_accrual2           NUMBER(22,2),
                                                          executory_accrual3           NUMBER(22,2),
                                                          executory_accrual4           NUMBER(22,2),
                                                          executory_accrual5           NUMBER(22,2),
                                                          executory_accrual6           NUMBER(22,2),
                                                          executory_accrual7           NUMBER(22,2),
                                                          executory_accrual8           NUMBER(22,2),
                                                          executory_accrual9           NUMBER(22,2),
                                                          executory_accrual10          NUMBER(22,2),
                                                          executory_paid1              NUMBER(22,2),
                                                          executory_paid2              NUMBER(22,2),
                                                          executory_paid3              NUMBER(22,2),
                                                          executory_paid4              NUMBER(22,2),
                                                          executory_paid5              NUMBER(22,2),
                                                          executory_paid6              NUMBER(22,2),
                                                          executory_paid7              NUMBER(22,2),
                                                          executory_paid8              NUMBER(22,2),
                                                          executory_paid9              NUMBER(22,2),
                                                          executory_paid10             NUMBER(22,2),
                                                          contingent_accrual1          NUMBER(22,2),
                                                          contingent_accrual2          NUMBER(22,2),
                                                          contingent_accrual3          NUMBER(22,2),
                                                          contingent_accrual4          NUMBER(22,2),
                                                          contingent_accrual5          NUMBER(22,2),
                                                          contingent_accrual6          NUMBER(22,2),
                                                          contingent_accrual7          NUMBER(22,2),
                                                          contingent_accrual8          NUMBER(22,2),
                                                          contingent_accrual9          NUMBER(22,2),
                                                          contingent_accrual10         NUMBER(22,2),
                                                          contingent_paid1             NUMBER(22,2),
                                                          contingent_paid2             NUMBER(22,2),
                                                          contingent_paid3             NUMBER(22,2),
                                                          contingent_paid4             NUMBER(22,2),
                                                          contingent_paid5             NUMBER(22,2),
                                                          contingent_paid6             NUMBER(22,2),
                                                          contingent_paid7             NUMBER(22,2),
                                                          contingent_paid8             NUMBER(22,2),
                                                          contingent_paid9             NUMBER(22,2),
                                                          contingent_paid10            NUMBER(22,2),
                                                          begin_unguaranteed_residual NUMBER(22,2),
                                                          int_on_unguaranteed_residual NUMBER(22,2),
                                                          end_unguaranteed_residual NUMBER(22,2),
                                                          begin_net_investment NUMBER(22,2),
                                                          int_on_net_investment NUMBER(22,2),
                                                          end_net_investment NUMBER(22,2),
                                                          initial_rate NUMBER(22,8),
                                                          rate NUMBER(22,8),
                                                          begin_lease_receivable number(22,2),
                                                          npv_lease_payments NUMBER(22,2),
                                                          npv_guaranteed_residual NUMBER(22,2),
                                                          npv_unguaranteed_residual NUMBER(22,2),
                                                          selling_profit_loss NUMBER(22,2),
                                                          cost_of_goods_sold NUMBER(22,2));
/

CREATE TYPE lsr_ilr_sales_sch_result_tab AS TABLE OF lsr_ilr_sales_sch_result;
/

drop table lsr_asset_schedule_tmp;

CREATE GLOBAL TEMPORARY TABLE LSR_ASSET_SCHEDULE_TMP (
  lsr_asset_id                   NUMBER(22,0),
  revision                       NUMBER(22,0),
  set_of_books_id                NUMBER(22,0),
  MONTH                          DATE,
  interest_income_received       NUMBER(22,2),
  interest_income_accrued        NUMBER(22,2),
  interest_rental_recvd_spread   NUMBER(22,2),
  beg_deferred_rev               NUMBER(22,2),
  deferred_rev_activity          NUMBER(22,2),
  end_deferred_rev               NUMBER(22,2),
  beg_receivable                 NUMBER(22,2),
  end_receivable                 NUMBER(22,2),
  beg_lt_receivable              NUMBER(22,2),
  end_lt_receivable              NUMBER(22,2),
  initial_direct_cost            NUMBER(22,2),
  executory_accrual1             NUMBER(22,2),
  EXECUTORY_ACCRUAL2             NUMBER(22,2),
  EXECUTORY_ACCRUAL3             NUMBER(22,2),
  EXECUTORY_ACCRUAL4             NUMBER(22,2),
  EXECUTORY_ACCRUAL5             NUMBER(22,2),
  EXECUTORY_ACCRUAL6             NUMBER(22,2),
  EXECUTORY_ACCRUAL7             NUMBER(22,2),
  EXECUTORY_ACCRUAL8             NUMBER(22,2),
  EXECUTORY_ACCRUAL9             NUMBER(22,2),
  EXECUTORY_ACCRUAL10            NUMBER(22,2),
  EXECUTORY_PAID1                NUMBER(22,2),
  EXECUTORY_PAID2                NUMBER(22,2),
  EXECUTORY_PAID3                NUMBER(22,2),
  EXECUTORY_PAID4                NUMBER(22,2),
  EXECUTORY_PAID5                NUMBER(22,2),
  EXECUTORY_PAID6                NUMBER(22,2),
  EXECUTORY_PAID7                NUMBER(22,2),
  EXECUTORY_PAID8                NUMBER(22,2),
  EXECUTORY_PAID9                NUMBER(22,2),
  EXECUTORY_PAID10               NUMBER(22,2),
  CONTINGENT_ACCRUAL1            NUMBER(22,2),
  CONTINGENT_ACCRUAL2            NUMBER(22,2),
  CONTINGENT_ACCRUAL3            NUMBER(22,2),
  CONTINGENT_ACCRUAL4            NUMBER(22,2),
  CONTINGENT_ACCRUAL5            NUMBER(22,2),
  CONTINGENT_ACCRUAL6            NUMBER(22,2),
  CONTINGENT_ACCRUAL7            NUMBER(22,2),
  CONTINGENT_ACCRUAL8            NUMBER(22,2),
  CONTINGENT_ACCRUAL9            NUMBER(22,2),
  CONTINGENT_ACCRUAL10           NUMBER(22,2),
  CONTINGENT_PAID1               NUMBER(22,2),
  CONTINGENT_PAID2               NUMBER(22,2),
  CONTINGENT_PAID3               NUMBER(22,2),
  CONTINGENT_PAID4               NUMBER(22,2),
  CONTINGENT_PAID5               NUMBER(22,2),
  CONTINGENT_PAID6               NUMBER(22,2),
  CONTINGENT_PAID7               NUMBER(22,2),
  CONTINGENT_PAID8               NUMBER(22,2),
  CONTINGENT_PAID9               NUMBER(22,2),
  CONTINGENT_PAID10              NUMBER(22,2)
) ON COMMIT DELETE ROWS;

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."LSR_ASSET_ID" IS
  'The Lessor Asset ID';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."REVISION" IS
  'The revision with which this asset is associated';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."SET_OF_BOOKS_ID" IS
  'The Set of Books which which the values are associated';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."MONTH" IS
  'The month with which the values are associated';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."INTEREST_INCOME_RECEIVED" IS
  'The interest/income received in this period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."INTEREST_INCOME_ACCRUED" IS
  'The interest/income accrued in this period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."INTEREST_RENTAL_RECVD_SPREAD" IS
  'The interest/income spread in this period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."BEG_DEFERRED_REV" IS
  'The deferred revenue at the beginning of the period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."DEFERRED_REV_ACTIVITY" IS
  'The deferred revenue activity in the period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."END_DEFERRED_REV" IS
  'The deferred revenue at the end of the period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."BEG_RECEIVABLE" IS
  'The receivable amount at the beginning of the period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."END_RECEIVABLE" IS
  'The receviable amount at the end of the period';
  
COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."BEG_LT_RECEIVABLE" IS
  'The long-term receivable amount at the beginning of the period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."END_LT_RECEIVABLE" IS
  'The long-term receviable amount at the end of the period';
  
COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."INITIAL_DIRECT_COST" IS
  'The initial direct cost associated with this period';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL1" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL2" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL3" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL4" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL5" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL6" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL7" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL8" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL9" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_ACCRUAL10" IS
  'Executory Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID1" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID2" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID3" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID4" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID5" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID6" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID7" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID8" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID9" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."EXECUTORY_PAID10" IS
  'Executory Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL1" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL2" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL3" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL4" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL5" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL6" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL7" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL8" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL9" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_ACCRUAL10" IS
  'Contingent Accrual Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID1" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID2" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID3" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID4" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID5" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID6" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID7" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID8" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID9" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON COLUMN "LSR_ASSET_SCHEDULE_TMP"."CONTINGENT_PAID10" IS
  'Contingent Paid (Received) Buckets';

COMMENT ON TABLE "LSR_ASSET_SCHEDULE_TMP" IS
  'Tempoary "asset schedule" (allocated from ILR Scheduled based on FMV) created for use in lessor variable payments calculations';

CREATE INDEX LSR_ASSET_SCH_TMP_MAIN_IDX ON
  LSR_ASSET_SCHEDULE_TMP (
    LSR_ASSET_ID,
    REVISION,
    SET_OF_BOOKS_ID,
    MONTH
  );

CREATE INDEX LSR_ASSET_SCH_TMP_MTH_IDX ON
  LSR_ASSET_SCHEDULE_TMP ( MONTH );

CREATE INDEX LSR_ASSET_SCH_TMP_SCH_IDX ON
  LSR_ASSET_SCHEDULE_TMP ( LSR_ASSET_ID,SET_OF_BOOKS_ID,REVISION );

CREATE INDEX lsr_asset_sch_tmp_tmth_idx ON
  LSR_ASSET_SCHEDULE_TMP ( trunc(MONTH,'fmmonth') );
  
CREATE OR REPLACE TYPE lsr_init_direct_cost_info AS OBJECT (idc_group_id NUMBER, 
                                                            date_incurred DATE, 
                                                            amount NUMBER(22,2),
                                                            description varchar2(254));
/

CREATE OR REPLACE TYPE lsr_init_direct_cost_info_tab AS TABLE OF lsr_init_direct_cost_info;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3929, 0, 2017, 1, 0, 0, 49517, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049517_lessor_03_reestablish_ilr_schedule_links_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
/
