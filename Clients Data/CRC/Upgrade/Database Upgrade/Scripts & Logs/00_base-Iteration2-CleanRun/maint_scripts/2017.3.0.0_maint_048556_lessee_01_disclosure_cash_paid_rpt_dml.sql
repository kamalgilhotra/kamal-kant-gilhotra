/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048556_lessee_01_disclosure_cash_paid_rpt_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.3.0.0  03/29/2018 Sarah Byers      Add new lease disclosure cash paid report and time option
||============================================================================
*/

INSERT INTO PP_REPORTS_TIME_OPTION
            (pp_report_time_option_id,
             description,
             parameter_uo_name,
             dwname1,
             label1,
             keycolumn1)
VALUES      (206,
             'Lease SOB Single Month',
             'uo_ppbase_report_parms_dddw_mnum',
             'dw_ls_set_of_books',
             'Set of Books',
             'set_of_books_id');

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             special_note,
             report_type,
             time_option,
             report_number,
             input_window,
             filter_option,
             status,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
             documentation,
             user_comment,
             last_approved_date,
             pp_report_number,
             old_report_number,
             dynamic_dw,
             turn_off_multi_thread)
VALUES      ( ( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Cash Paid',
             'Disclosure: Cash Paid for Amounts included in Measurement of Lease Liabilities by Lease Group',
             'Lessee',
             'dw_ls_rpt_disclosure_cash_paid',
             NULL,
             NULL,
             NULL,
             'Lessee - 2005',
             NULL,
             NULL,
             NULL,
             11,
             311,
             206,
             103,
             1,
             3,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4245, 0, 2017, 3, 0, 0, 48556, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_048556_lessee_01_disclosure_cash_paid_rpt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;