/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041290_anlyt_06_system.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.1 11/17/2014 Chad Theilman    Asset Analytics - System
||============================================================================
*/


declare 
  doesTableExist number := 0;
  
  begin
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CHART_DATAVIEW_SHORTCUTS','PWRPLANT');
    
    if doesTableExist = 0 then 
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CHART_DATAVIEW_SHORTCUTS ("ID" NUMBER (22,0) NOT NULL ENABLE, "DASHBOARDLINK" VARCHAR2 (100), 
                  "DATAVIEWNAME" VARCHAR2 (100) NOT NULL ENABLE, "DATAVIEWDESCRIPTION" VARCHAR2 (250), "DASHBOARDID" NUMBER (22,0) NOT NULL ENABLE, 
                  "CHARTID" NUMBER (22,0) NOT NULL ENABLE, "CHARTATTR" VARCHAR2 (4000), "TIMESTAMP" DATE  NOT NULL ENABLE, "USERS" VARCHAR2 (50), 
                  "CHARTSQL" CLOB , PRIMARY KEY (ID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CASES','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CASES ("ACTIVE" NUMBER(22,0) NOT NULL ENABLE, "CASEID" NUMBER (22,0) NOT NULL ENABLE, "ASSIGNEE" VARCHAR2 (50), 
                  "TITLE" VARCHAR2 (50), "DESCRIPTION" VARCHAR2 (1000), "PRIORITY" VARCHAR2 (50), "DASHBOARDID" NUMBER (22,0), "COMPLETED" NUMBER (22,0), 
                  "STATUS" VARCHAR2 (50), "CHARTTYPE" VARCHAR2 (50), "CHARTCONFIG" CLOB , "CHARTWIDTH" NUMBER (22,0), "CHARTHEIGHT" NUMBER (22,0), 
                  "RESOLUTIONNOTE" VARCHAR2 (255), "UPDATEDDATE" DATE , "CHARTID" NUMBER (22,0), "FILTERS" CLOB , "CREATEDATE" DATE , 
                  "GRIDDATASOURCEID" NUMBER (22,0), "CHARTFILTERS" VARCHAR2 (2000), "GRIDFILTERS" VARCHAR2 (2000), 
                  "DRILLURL" VARCHAR2(500), "XAXISNAME" VARCHAR2 (50), "CREATEDBY" VARCHAR2 (50) NOT NULL ENABLE, PRIMARY KEY (CASEID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      
      end; --MCM MADE THE DESCRIPTION FIELD LARGER TO 1000, UP FROM 50
    end if;
	
	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_USER_CHARTS','PWRPLANT');
	
	if doesTableExist = 0 then
		begin
			execute immediate 'CREATE TABLE "PWRPLANT"."PA_USER_CHARTS" 
							   (	"USERNAME" VARCHAR2(50) NOT NULL ENABLE, 
								"CHARTID" NUMBER NOT NULL ENABLE, 
								"CHARTCONFIG" CLOB NOT NULL ENABLE, 
								"SEQUENCE" NUMBER NOT NULL ENABLE, 
								"CHARTTYPE" VARCHAR2(100) NOT NULL ENABLE, 
								"DRILLURL" VARCHAR2(200), 
								"ID" VARCHAR2(50), 
								"CHARTCAPTION" VARCHAR2(100), 
								"CHARTIMAGE" BLOB, 
								"USERCHARTID" NUMBER, 
								 CONSTRAINT "PK_PA_USER_CHARTS" PRIMARY KEY ("USERCHARTID")
							  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
							  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
							  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
							  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
							  TABLESPACE "USERS"  ENABLE
							   )' ;
		end;
	end if;
	
	
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CASES_COMMENTS','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CASES_COMMENTS ("COMMENTID" NUMBER (22,0) NOT NULL ENABLE, "CASEID" NUMBER (22,0) NOT NULL ENABLE, 
                  "USERNAME" VARCHAR2 (50) NOT NULL ENABLE, "CREATEDDATE" VARCHAR2 (50) NOT NULL ENABLE, "CASECOMMENT" VARCHAR2 (1000), 
                  PRIMARY KEY (COMMENTID,CASEID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';

      end; --MCM MADE 'CASECOMMENT' NULLABLE...A COMMENT SHOULD NOT BE REQUIRED TO CREATE THE CASE
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_DASHBOARD_SHORTCUTS','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_DASHBOARD_SHORTCUTS ("ID" NUMBER (22,0) NOT NULL ENABLE, "SHORTCUTNAME" VARCHAR2 (100) NOT NULL ENABLE, 
                  "SHORTCUTDESCRIPTION" VARCHAR2 (250), "SHORTCUTLINK" VARCHAR2 (100) NOT NULL ENABLE, "SHORTCUTPAGEATTR" VARCHAR2 (4000), 
                  "TIMESTAMP" DATE  NOT NULL ENABLE, "SHORTCUTFILTERS" CLOB , "XAXISCOLUMN" VARCHAR2(50), "GRIDSELECTIONS" CLOB, "CHARTSELECTIONS" CLOB,
                  "USERS" VARCHAR2 (50), PRIMARY KEY (ID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
                  
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CHART', 'PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CHART ("CHARTID" NUMBER (22,0) NOT NULL ENABLE, "CHARTNAME" VARCHAR2 (50) NOT NULL ENABLE, 
                  "CHARTTYPE" VARCHAR2 (50) NOT NULL ENABLE, "CHARTBUILDER" VARCHAR2 (50) NOT NULL ENABLE, 
                  "CHARTCAPTION" VARCHAR2 (100) NOT NULL ENABLE, "CATEGORYNAME" VARCHAR2 (50) NOT NULL ENABLE, 
                  "DATAPOINTDEFINITIONID" NUMBER (22,0), "DATASOURCEID" NUMBER (22,0), "COLUMNSPACING" NUMBER (22,0),
                   "NUMBEROFVISIBLECOLUMNS" NUMBER (22,0), "CUSTOMSERIESLOADER" VARCHAR2 (50), "PALETTECOLORS" VARCHAR2 (100), 
                   "NUMBERPREFIX" VARCHAR2 (1), "NUMBERSUFFIX" VARCHAR2 (1), "SNUMBERPREFIX" VARCHAR2 (1), "SNUMBERSUFFIX" VARCHAR2 (1),
                   "XAXISLABEL" varchar2(50), "YAXISLABEL" varchar2(50),
                    PRIMARY KEY (CHARTID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CHART_SERIES','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CHART_SERIES ("CHARTSERIESID" NUMBER (22,0) NOT NULL ENABLE, "SERIESNAME" VARCHAR2 (50) NOT NULL ENABLE, 
                  "RENDERAS" VARCHAR2 (50), "PARENTYAXIS" VARCHAR2 (50), "LINETHICKNESS" NUMBER (22,0), "ANCHORRADIUS" NUMBER (22,0),
                   "SERIESCOLOR" VARCHAR2 (7), "SERIESLABEL" VARCHAR2 (50), "CHARTID" NUMBER (22,0) NOT NULL ENABLE, 
                   "DATAPOINTID" NUMBER (22,0) NOT NULL ENABLE, "YAXISLABEL" VARCHAR2(50), PRIMARY KEY (CHARTSERIESID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_COLUMN_FORMAT', 'PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_COLUMN_FORMAT ("ID" NUMBER (22,0) NOT NULL ENABLE, "COLUMNNAME" VARCHAR2 (50) NOT NULL ENABLE, 
                  "FORMAT" VARCHAR2 (20) NOT NULL ENABLE, PRIMARY KEY (ID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
    
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CONFIG','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CONFIG ("ID" NUMBER (22,0) NOT NULL ENABLE, "NAME" VARCHAR2 (50) NOT NULL ENABLE, 
                "VALUE" VARCHAR2 (50), PRIMARY KEY (ID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if; --MCM MADE 'VALUE' FIELD NULLABLE
        
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_DASHBOARD','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_DASHBOARD ("ID" NUMBER (22,0) NOT NULL ENABLE, "DASHBOARDID" NUMBER (22,0) NOT NULL ENABLE, 
                "DASHBOARDNAME" VARCHAR2 (100) NOT NULL ENABLE, "DASHBOARDDESCRIPTION" VARCHAR2 (1000) NOT NULL ENABLE, 
                "DASHBOARDTITLE" VARCHAR2 (1000), "DASHBOARDLINK" VARCHAR2 (100), "VISIBILITYFLAG" CHAR (1), "DASHBOARDPAGETITLE" VARCHAR2 (250), 
                "DASHBOARDDISPLAYNAME" VARCHAR2 (50), PRIMARY KEY (ID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_DASHBOARD_CHARTS','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_DASHBOARD_CHARTS ("DASHBOARDCHARTID" NUMBER (22,0) NOT NULL ENABLE,
                   "DASHBOARDID" NUMBER (22,0) NOT NULL ENABLE, "CHARTID" NUMBER (22,0) NOT NULL ENABLE, 
                   "DASHBOARDSEQUENCE" NUMBER (22,0), PRIMARY KEY (DASHBOARDCHARTID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_COLUMNGROUP','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_COLUMNGROUP ("COLUMNGROUPID" NUMBER (22,0) NOT NULL ENABLE, "COLUMNGROUPDESC" VARCHAR2 (100) NOT NULL ENABLE, 
                  "COLUMNLIST" VARCHAR2 (250) NOT NULL ENABLE, "DEFAULTCOLUMN" VARCHAR2 (50) NOT NULL ENABLE, PRIMARY KEY (COLUMNGROUPID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
    
    
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_DATAPOINT','PWRPLANT');

    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_DATAPOINT ("DATAPOINTID" NUMBER (22,0) NOT NULL ENABLE, "DATAPOINTLABEL" VARCHAR2 (50) NOT NULL ENABLE, 
                  "DATAPOINTVALUE" VARCHAR2 (100) NOT NULL ENABLE, "DATAPOINTCOLOR" VARCHAR2 (6) NOT NULL ENABLE, "HEATMAPCOLUMNID" VARCHAR2 (50),
                  "HEATMAPROWID" VARCHAR2 (50), "GRADIENTID" NUMBER (22,0), "STARTLABEL" VARCHAR2 (50), "ENDLABEL" VARCHAR2 (50), "STARTCOLOR" VARCHAR2 (6), PRIMARY KEY (DATAPOINTID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_DATASOURCE','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_DATASOURCE ("DATASOURCEID" NUMBER (22,0) NOT NULL ENABLE, "DATASOURCENAME" VARCHAR2 (50) NOT NULL ENABLE, 
                  "SELECTEXPRESSION" VARCHAR2 (2000) NOT NULL ENABLE, "FROMEXPRESSION" VARCHAR2 (2000) NOT NULL ENABLE, 
                  "HAVINGEXPRESSION" VARCHAR2 (2000), "GROUPBYEXPRESSION" VARCHAR2 (2000), "ORDERBYEXPRESSION" VARCHAR2 (2000),
                  "WHEREEXPRESSION" VARCHAR2 (2000), "WHERECOLUMN" VARCHAR2 (100), "ROWLIMIT" NUMBER (22,0), "CUSTOMDATALOADER" VARCHAR2 (50),
                  "FILTERGROUPID" NUMBER (22,0), PRIMARY KEY (DATASOURCEID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXPLORATION','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_EXPLORATION ("CHARTID" NUMBER (22,0) NOT NULL ENABLE, "EXPLORATIONCHARTID" NUMBER (22,0) NOT NULL ENABLE, 
                  "GRIDDATASOURCEID" NUMBER (22,0) NOT NULL ENABLE, "DETAILDATASOURCEID" NUMBER (22,0) NOT NULL ENABLE, 
                  "COLUMNGROUPID" NUMBER (22,0) NOT NULL ENABLE, PRIMARY KEY (CHARTID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
        
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXPLORE_DATASOURCE','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_EXPLORE_DATASOURCE ("ID" NUMBER (22,0) NOT NULL ENABLE, "EXPLORECHARTID" NUMBER (22,0) NOT NULL ENABLE, 
                  "GRIDID" NUMBER (22,0) NOT NULL ENABLE, "GRIDNAME" VARCHAR2 (50) NOT NULL ENABLE, "GRIDDATASOURCEID" NUMBER (22,0) NOT NULL ENABLE, 
                  PRIMARY KEY (ID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXPLORE_GRIDFILTERS','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_EXPLORE_GRIDFILTERS ("GRIDID" NUMBER (22,0) NOT NULL ENABLE, "FILTERID" NUMBER (22,0) NOT NULL ENABLE, 
                  PRIMARY KEY (GRIDID,FILTERID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CASE_STATUSES','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CASE_STATUSES ("STATUSID" NUMBER (22,0) NOT NULL ENABLE, "STATUSDESCRIPTION" VARCHAR2 (100) NOT NULL ENABLE, 
                  "SEQUENCE" NUMBER (22,0), PRIMARY KEY (STATUSID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
    
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_FILTER2','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_FILTER2 ("FILTERID" NUMBER (22,0) NOT NULL ENABLE, "FILTERNAME" VARCHAR2 (50) NOT NULL ENABLE, 
                  "FILTERCOLUMN" VARCHAR2 (50), "LOOKUPNAME" VARCHAR2 (50) NOT NULL ENABLE, "LOOKUPPROMPT" VARCHAR2 (50) NOT NULL ENABLE, 
                  "LOOKUPSQL" VARCHAR2 (200) NOT NULL ENABLE, "DISPLAY" VARCHAR2 (50) NOT NULL ENABLE, "VALUE" VARCHAR2 (50) NOT NULL ENABLE, 
                  "CONTROLTYPE" VARCHAR2 (50), "WHERETYPE" VARCHAR2 (50), "ISREQUIRED" NUMBER (22,0), "QUOTEVALUES" NUMBER (22,0),
                  "CATEGORY" VARCHAR2 (50), "ISFORMULA" NUMBER (22,0), "FORMULAEXPRESSION" VARCHAR2 (250), "ACTIVE" NUMBER (22,0), 
                  "FILTEREXPRESSION" VARCHAR2 (250), "DEFAULTVALUE" VARCHAR2 (50), PRIMARY KEY (FILTERID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_FILTERGROUP','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_FILTERGROUP ("FILTERGROUPID" NUMBER (22,0) NOT NULL ENABLE, "FILTERGROUPNAME" VARCHAR2 (50) NOT NULL ENABLE, 
                  PRIMARY KEY (FILTERGROUPID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_FILTERGROUP_FILTERS','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_FILTERGROUP_FILTERS ("ID" NUMBER (22,0) NOT NULL ENABLE, "FILTERGROUPID" NUMBER (22,0) NOT NULL ENABLE, 
				  "ALLOWEDASXAXIS" NUMBER, "FILTERID" NUMBER (22,0) NOT NULL ENABLE, PRIMARY KEY (ID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CASE_PRIORITIES','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CASE_PRIORITIES ("PRIORITYDESCRIPTION" VARCHAR2 (50), "PRIORITYID" NUMBER (22,0) NOT NULL ENABLE, 
                  "SEQUENCE" NUMBER (22,0) NOT NULL ENABLE, PRIMARY KEY (PRIORITYID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_HEATMAP_COLOR','PWRPLANT');
    
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_HEATMAP_COLOR ("ID" NUMBER (22,0) NOT NULL ENABLE, "CHARTID" NUMBER (22,0) NOT NULL ENABLE, 
                  "COLOR" VARCHAR2 (7) NOT NULL ENABLE, "LABEL" VARCHAR2 (50), "MINVALUE" NUMBER (22,0), "MAXVALUE" NUMBER (22,0), 
                  "DATAPOINTID" NUMBER (22,0), PRIMARY KEY (ID)) NOCOMPRESS NOLOGGING TABLESPACE PWRPLANT';
      end;
    end if;
    
    end;
	/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2024, 0, 10, 4, 3, 1, 41290, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_041290_anlyt_06_system.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;