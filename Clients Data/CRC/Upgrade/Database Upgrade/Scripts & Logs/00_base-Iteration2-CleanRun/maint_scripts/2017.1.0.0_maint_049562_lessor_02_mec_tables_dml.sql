/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049562_lessor_02_mec_tables_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/01/2017 Sarah Byers      Update wksp uo to point to common Lease wksp
||============================================================================
*/

update ppbase_workspace
   set workspace_uo_name = 'uo_ls_mecntr_wksp'
 where module = 'LESSOR'
   and workspace_uo_name = 'uo_lsr_mecntr_wksp';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3850, 0, 2017, 1, 0, 0, 49562, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049562_lessor_02_mec_tables_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;