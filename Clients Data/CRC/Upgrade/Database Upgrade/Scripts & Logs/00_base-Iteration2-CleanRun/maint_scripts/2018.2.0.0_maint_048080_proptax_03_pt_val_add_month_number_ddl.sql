/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048080_proptax_03_pt_val_add_month_number_ddl.sql
 ||============================================================================
 || Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2018.2.0.0 06/06/2017 Josh Sandler   Add month_number to Data Center results
 ||============================================================================
*/

ALTER TABLE pt_val_scenario_result
  DROP PRIMARY KEY DROP INDEX;

ALTER TABLE pt_val_scenario_result
  ADD CONSTRAINT pt_val_scn_result_pk PRIMARY KEY (
    scenario_id,
    value_template_id,
    variable_id,
    value_timeframe_id,
    pk_string,
    month_number
  )
 using index tablespace pwrplant_idx;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14884, 0, 2018, 2, 0, 0, 48080, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_048080_proptax_03_pt_val_add_month_number_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;