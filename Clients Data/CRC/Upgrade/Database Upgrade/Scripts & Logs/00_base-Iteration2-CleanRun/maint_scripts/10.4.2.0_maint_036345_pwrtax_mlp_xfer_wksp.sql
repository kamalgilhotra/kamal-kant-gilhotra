/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036345_pwrtax_mlp_xfer_wksp.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/03/2014 Andrew Scott        Transfers, Drop Down workspace
||                                         on the base gui to allow
||                                         deletions, reprocessing of
||                                         transfers, drop downs
||============================================================================
*/

create table TAX_EVENT
(
 TAX_EVENT_ID number(22,0) not null,
 TIME_STAMP   date,
 USER_ID      varchar2(18),
 DESCRIPTION  varchar2(50)
);

alter table TAX_EVENT
   add constraint TAX_EVENT_PK
       primary key (TAX_EVENT_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  TAX_EVENT is '(F) [09] The Tax Event table holds the fixed definitions of events that triggers various processes in PowerTax.';
comment on column TAX_EVENT.TAX_EVENT_ID is 'system-assigned identifier of a particular tax event.';
comment on column TAX_EVENT.TIME_STAMP is 'standard system-assigned time stamp used for audit purposes.';
comment on column TAX_EVENT.USER_ID is 'standard system-assigned user id used for audit purposes.';
comment on column TAX_EVENT.DESCRIPTION is 'Description of the tax event.';

insert into TAX_EVENT (TAX_EVENT_ID, DESCRIPTION) values (1, 'Transfer-Interfaced');
insert into TAX_EVENT (TAX_EVENT_ID, DESCRIPTION) values (2, 'Transfer-Manual');
insert into TAX_EVENT (TAX_EVENT_ID, DESCRIPTION) values (3, 'Drop Down');

alter table TAX_LAYER add TAX_EVENT_ID number(22,0);

--at this time, tax_layer was only populated with drop down events.
update TAX_LAYER set TAX_EVENT_ID = 3;

alter table TAX_LAYER
   add constraint TAX_LAYER_TE_FK
       foreign key (TAX_EVENT_ID)
       references tax_event;

----enhance the use of "tax package control" for transfer
----back up the table before doing anything.
create table D1_TAX_PACKAGE_CONTROL
   as select * from TAX_PACKAGE_CONTROL;

------make 22,4 versus 22,2 because these could hold off the saved percentages
------ 66.67% would be .6667.  Any decimal less than this could affect the recalc.

alter table TAX_PACKAGE_CONTROL
   add (TAX_YEAR                number(22,2),
        TOT_BOOK_AMOUNT         number(22,4),
        TOT_PROCEEDS            number(22,4),
        INPUT_FORMAT            number(22,0),
        SPREAD_METHOD           number(22,0),
        COMPANY_ID              number(22,0),
        TAX_CLASS_ID            number(22,0),
        CALC_DEFERRED_GAIN      number(22,0),
        DEF_GAIN_VINTAGE_ID     number(22,0),
        TAX_DEPR_SCHEMA_ID_FROM number(22,0),
        TAX_DEPR_SCHEMA_ID_TO   number(22,0),
        IN_SERVICE_MONTH        date,
        TAX_LAYER_ID            number(22,0),
        TAX_EVENT_ID            number(22,0));

comment on column TAX_PACKAGE_CONTROL.TOT_BOOK_AMOUNT is 'The tax year of the package.';
comment on column TAX_PACKAGE_CONTROL.TOT_BOOK_AMOUNT is 'The input book amount for the total transfer of the package.';
comment on column TAX_PACKAGE_CONTROL.TOT_PROCEEDS is 'The input proceeds for the total transfer of the package.';
comment on column TAX_PACKAGE_CONTROL.INPUT_FORMAT is 'Whether the input is a percentage or dollar amount. 1 = using dollars, 2 = percentage.';
comment on column TAX_PACKAGE_CONTROL.SPREAD_METHOD is 'Whether the input spread occurrs over the original book amount or the net amount.  Most often this will be net.';
comment on column TAX_PACKAGE_CONTROL.COMPANY_ID is 'The input company of the package.';
comment on column TAX_PACKAGE_CONTROL.TAX_CLASS_ID is 'The input tax class of the package.';
comment on column TAX_PACKAGE_CONTROL.CALC_DEFERRED_GAIN is 'The input calc deferred gain flag of the package.';
comment on column TAX_PACKAGE_CONTROL.DEF_GAIN_VINTAGE_ID is 'The input deferred gain vintage id of the package.';
comment on column TAX_PACKAGE_CONTROL.TAX_DEPR_SCHEMA_ID_FROM is 'The input tax depr schema "from" of the package.';
comment on column TAX_PACKAGE_CONTROL.TAX_DEPR_SCHEMA_ID_TO is 'The input tax depr schema "to" of the package.';
comment on column TAX_PACKAGE_CONTROL.IN_SERVICE_MONTH is 'The input in service month of the package.';
comment on column TAX_PACKAGE_CONTROL.TAX_LAYER_ID is 'The input tax layer of the package.';
comment on column TAX_PACKAGE_CONTROL.TAX_EVENT_ID is 'The event or input source of the package. Either a manual tranfer, a drop down, or the transfers interface.';

insert into TAX_PACKAGE_CONTROL
   (PACKAGE_ID, VERSION_ID, TAX_YEAR, DESCRIPTION)
   select TTC.PACKAGE_ID, TTC.VERSION_ID, TTC.TAX_YEAR, max(DESCR_VIEW.DESCRIPTION)
     from TAX_TRANSFER_CONTROL TTC,
          (select distinct PACKAGE_ID, VERSION_ID, DESCRIPTION from TAX_PACKAGE_CONTROL) DESCR_VIEW
    where TTC.PACKAGE_ID = DESCR_VIEW.PACKAGE_ID
      and TTC.VERSION_ID = DESCR_VIEW.VERSION_ID
      and not exists (select 1
             from TAX_PACKAGE_CONTROL TPC
            where TTC.PACKAGE_ID = TPC.PACKAGE_ID
              and TTC.VERSION_ID = TPC.VERSION_ID
              and TTC.TAX_YEAR = TPC.TAX_YEAR)
    group by TTC.PACKAGE_ID, TTC.VERSION_ID, TTC.TAX_YEAR;

delete from TAX_PACKAGE_CONTROL where TAX_YEAR is null;


declare
   PK_TO_DROP varchar2(50);
begin
   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS
    where LOWER(TABLE_NAME) = 'tax_package_control'
      and CONSTRAINT_TYPE = 'P';

   if PK_TO_DROP is not null then
      execute immediate 'alter table tax_package_control drop constraint ' || PK_TO_DROP;
      DBMS_OUTPUT.PUT_LINE('pkey ' || PK_TO_DROP || ' dropped.');
   end if;

exception
   when NO_DATA_FOUND then
      --do nothing.
      DBMS_OUTPUT.PUT_LINE('No pkey on tax_package_control to drop.');
end;
/

alter table TAX_PACKAGE_CONTROL
   add constraint TAX_PACKAGE_CONTROL_PK
       primary key (PACKAGE_ID, VERSION_ID, TAX_YEAR)
       using index tablespace PWRPLANT_IDX;

alter table TAX_PACKAGE_CONTROL
   add constraint TAX_PACKAGE_CONTROL_CO_FK
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

alter table TAX_PACKAGE_CONTROL
   add constraint TAX_PACKAGE_CONTROL_TC_FK
       foreign key (TAX_CLASS_ID)
       references TAX_CLASS;

alter table TAX_PACKAGE_CONTROL
   add constraint TAX_PACKAGE_CONTROL_TL_FK
       foreign key (TAX_LAYER_ID)
       references TAX_LAYER;

alter table TAX_PACKAGE_CONTROL
   add constraint TAX_PACKAGE_CONTROL_TE_FK
       foreign key (TAX_EVENT_ID)
       references TAX_EVENT;

alter table TAX_PACKAGE_CONTROL
   add constraint TAX_PACKAGE_CONTROL_V_FK
       foreign key (DEF_GAIN_VINTAGE_ID)
       references VINTAGE(VINTAGE_ID);

alter table TAX_PACKAGE_CONTROL
   add constraint TAX_PACKAGE_CONTROL_SCHF_FK
       foreign key (TAX_DEPR_SCHEMA_ID_FROM)
       references TAX_DEPR_SCHEMA(TAX_DEPR_SCHEMA_ID);

alter table TAX_PACKAGE_CONTROL
   add constraint TAX_PACKAGE_CONTROL_SCHT_FK
       foreign key (TAX_DEPR_SCHEMA_ID_TO)
       references TAX_DEPR_SCHEMA(TAX_DEPR_SCHEMA_ID);

----the filter will need tax event to be populated for all.
----If package_id = -1, set to 1.  set the rest to be
----manual xfer.

update TAX_PACKAGE_CONTROL set TAX_EVENT_ID = 1 where PACKAGE_ID = -1;

update TAX_PACKAGE_CONTROL set TAX_EVENT_ID = 2 where PACKAGE_ID is null;

----this is not needed.  the equivalent on tax transfer control
----was needed by the interface.
drop index TAX_TEMP_XFER_CTRL_COMBO_IDX;

----
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (59, 'Powertax - Transfer Packages', 'tax_package_control', 'uo_ppbase_tab_filter_dynamic');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (124, 'Tax Year', 'dw', 'tax_package_control.tax_year', 'dw_tax_year_by_version_filter',
    'tax_year', 'tax_year', 'N', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (125, 'Company', 'dw', 'tax_package_control.company_id', 'dw_tax_company_by_version_filter',
    'company_id', 'description', 'N', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (126, 'Tax Class', 'dw', 'tax_package_control.tax_class_id', 'dw_tax_class_by_version_filter',
    'tax_class_id', 'description', 'N', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (127, 'Description', 'sle', 'tax_package_control.description', '', '', '', '', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (128, 'Def Gain Vintage', 'dw', 'tax_package_control.def_gain_vintage_id',
    'dw_tax_vintage_by_version_filter', 'vintage_id', 'description', 'N', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (129, 'Tax Depr Schema From', 'dw', 'tax_package_control.tax_depr_schema_id_from',
    'dw_tax_depr_schema_filter', 'tax_depr_schema_id', 'description', 'N', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (130, 'Tax Depr Schema To', 'dw', 'tax_package_control.tax_depr_schema_id_to',
    'dw_tax_depr_schema_filter', 'tax_depr_schema_id', 'description', 'N', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (131, 'In Service Month', 'dw', 'to_char(tax_package_control.in_service_month,''mm'')',
    'dw_tax_month_filter', 'month_id', 'month_name', 'C', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (132, 'Tax Layer', 'dw', 'tax_package_control.tax_layer_id', 'dw_tax_layer_filter',
    'tax_layer_id', 'description', 'N', 0, 0, 0, 0, 0);
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (133, 'Tax Event', 'dw', 'tax_package_control.tax_event_id', 'dw_tax_event_filter',
    'tax_event_id', 'description', 'N', 0, 0, 0, 0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   select 59, FILTER_ID from PP_DYNAMIC_FILTER where FILTER_ID between 124 and 133;

----rename to indicate that transfers are maintained, and not just manual transfers
update PPBASE_MENU_ITEMS
   set MENU_IDENTIFIER = 'depr_input_transfer_maint'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_manual_transfers';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_input_transfer_maint', 'Maintain Transfers',
    'uo_tax_depr_act_wksp_pkg_search', 'Maintain Transfers');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'depr_input_transfer_maint', LABEL = 'Maintain Transfers',
       ENABLE_YN = 1
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_transfer_maint';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1006, 0, 10, 4, 2, 0, 36345, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036345_pwrtax_mlp_xfer_wksp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;