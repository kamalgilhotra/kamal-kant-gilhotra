/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044918_pwrtax_partial_year_depr_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 09/17/2015 Sarah Byers    Partial Year Depreciation Transfers
||============================================================================
*/

-- New System Option
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, is_base_option, allow_company_override)
values (
	'Partial Year Depreciation Transfers', 
	'Allows for depreciation to be maintained on the company that is transferring the asset for the number of months in the calendar year the company held the asset',
	0, 'No', 1, 1);

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Partial Year Depreciation Transfers', 'powertax');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Partial Year Depreciation Transfers', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Partial Year Depreciation Transfers', 'No');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2880, 0, 2015, 2, 0, 0, 44918, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044918_pwrtax_partial_year_depr_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;