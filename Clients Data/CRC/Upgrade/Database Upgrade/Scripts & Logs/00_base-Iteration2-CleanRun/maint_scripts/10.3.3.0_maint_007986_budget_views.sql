SET LINESIZE 600
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007986_budget_views.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/31/2011 Chris Mardis   Point Release
||============================================================================
*/

create or replace view BUDG_MONTHLY_DATA_DEESC_SV as
select W.BUDGET_MONTHLY_ID BUDGET_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.BUDGET_ID BUDGET_ID,
       W.BUDGET_VERSION_ID BUDGET_VERSION_ID,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) - NVL(WE.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) - NVL(WE.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) - NVL(WE.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) - NVL(WE.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) - NVL(WE.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) - NVL(WE.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) - NVL(WE.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) - NVL(WE.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) - NVL(WE.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) - NVL(WE.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) - NVL(WE.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) - NVL(WE.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) - NVL(WE.TOTAL, 0) TOTAL,
       W.BUDGET_PLANT_CLASS_ID BUDGET_PLANT_CLASS_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID
  from BUDGET_MONTHLY_DATA W, BUDGET_MONTHLY_DATA_ESCALATION WE
 where W.BUDGET_MONTHLY_ID = WE.BUDGET_MONTHLY_ID(+)
   and W.YEAR = WE.YEAR(+);


create or replace view BUDG_MONTHLY_DATA_FY_DEESC_SV as
select BUDGET_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       BUDGET_ID,
       BUDGET_VERSION_ID,
       year,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       AMOUNT_PERIOD1,
       AMOUNT_PERIOD2,
       AMOUNT_PERIOD3,
       AMOUNT_PERIOD4,
       AMOUNT_PERIOD5,
       AMOUNT_PERIOD6,
       AMOUNT_PERIOD7,
       AMOUNT_PERIOD8,
       AMOUNT_PERIOD9,
       AMOUNT_PERIOD10,
       AMOUNT_PERIOD11,
       AMOUNT_PERIOD12,
       AMOUNT_PERIOD1 + AMOUNT_PERIOD2 + AMOUNT_PERIOD3 + AMOUNT_PERIOD4 + AMOUNT_PERIOD5 +
       AMOUNT_PERIOD6 + AMOUNT_PERIOD7 + AMOUNT_PERIOD8 + AMOUNT_PERIOD9 + AMOUNT_PERIOD10 +
       AMOUNT_PERIOD11 + AMOUNT_PERIOD12 TOTAL,
       BUDGET_PLANT_CLASS_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WORK_ORDER_ID,
       JOB_TASK_ID
  from (select W.BUDGET_MONTHLY_ID BUDGET_MONTHLY_ID,
               max(W.TIME_STAMP) TIME_STAMP,
               max(W.USER_ID) USER_ID,
               W.BUDGET_ID BUDGET_ID,
               W.BUDGET_VERSION_ID BUDGET_VERSION_ID,
               DECODE(SIGN(P.FISCAL_PERIOD - P.MONTH_NUM), 1, W.YEAR, W.YEAR + 1) year,
               max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
               max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
               max(W.DEPARTMENT_ID) DEPARTMENT_ID,
               sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period1,
               sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period2,
               sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period3,
               sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period4,
               sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period5,
               sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period6,
               sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period7,
               sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period8,
               sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period9,
               sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period10,
               sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period11,
               sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period12,
               max(W.BUDGET_PLANT_CLASS_ID) BUDGET_PLANT_CLASS_ID,
               max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
               0 FUTURE_DOLLARS,
               0 HIST_ACTUALS,
               max(W.WORK_ORDER_ID) WORK_ORDER_ID,
               max(W.JOB_TASK_ID) JOB_TASK_ID
          from BUDGET_MONTHLY_DATA W, PP_TABLE_MONTHS P, BUDGET_MONTHLY_DATA_ESCALATION WE
         where W.BUDGET_MONTHLY_ID = WE.BUDGET_MONTHLY_ID(+)
           and W.YEAR = WE.YEAR(+)
         group by W.BUDGET_MONTHLY_ID,
                  W.BUDGET_ID,
                  W.BUDGET_VERSION_ID,
                  DECODE(SIGN(P.FISCAL_PERIOD - P.MONTH_NUM), 1, W.YEAR, W.YEAR + 1));


create or replace view BUDG_MONTHLY_DATA_FY_SV as
select BUDGET_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       BUDGET_ID,
       BUDGET_VERSION_ID,
       year,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       AMOUNT_PERIOD1,
       AMOUNT_PERIOD2,
       AMOUNT_PERIOD3,
       AMOUNT_PERIOD4,
       AMOUNT_PERIOD5,
       AMOUNT_PERIOD6,
       AMOUNT_PERIOD7,
       AMOUNT_PERIOD8,
       AMOUNT_PERIOD9,
       AMOUNT_PERIOD10,
       AMOUNT_PERIOD11,
       AMOUNT_PERIOD12,
       AMOUNT_PERIOD1 + AMOUNT_PERIOD2 + AMOUNT_PERIOD3 + AMOUNT_PERIOD4 + AMOUNT_PERIOD5 +
       AMOUNT_PERIOD6 + AMOUNT_PERIOD7 + AMOUNT_PERIOD8 + AMOUNT_PERIOD9 + AMOUNT_PERIOD10 +
       AMOUNT_PERIOD11 + AMOUNT_PERIOD12 TOTAL,
       BUDGET_PLANT_CLASS_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WORK_ORDER_ID,
       JOB_TASK_ID
  from (select W.BUDGET_MONTHLY_ID BUDGET_MONTHLY_ID,
               max(W.TIME_STAMP) TIME_STAMP,
               max(W.USER_ID) USER_ID,
               W.BUDGET_ID BUDGET_ID,
               W.BUDGET_VERSION_ID BUDGET_VERSION_ID,
               DECODE(SIGN(P.FISCAL_PERIOD - P.MONTH_NUM), 1, W.YEAR, W.YEAR + 1) year,
               max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
               max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
               max(W.DEPARTMENT_ID) DEPARTMENT_ID,
               sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period1,
               sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period2,
               sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period3,
               sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period4,
               sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period5,
               sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period6,
               sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period7,
               sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period8,
               sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period9,
               sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period10,
               sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period11,
               sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period12,
               max(W.BUDGET_PLANT_CLASS_ID) BUDGET_PLANT_CLASS_ID,
               max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
               0 FUTURE_DOLLARS,
               0 HIST_ACTUALS,
               max(W.WORK_ORDER_ID) WORK_ORDER_ID,
               max(W.JOB_TASK_ID) JOB_TASK_ID
          from BUDGET_MONTHLY_DATA W, PP_TABLE_MONTHS P
         group by W.BUDGET_MONTHLY_ID,
                  W.BUDGET_ID,
                  W.BUDGET_VERSION_ID,
                  DECODE(SIGN(P.FISCAL_PERIOD - P.MONTH_NUM), 1, W.YEAR, W.YEAR + 1));


create or replace view BUDG_MONTHLY_DATA_SV as
select W.BUDGET_MONTHLY_ID BUDGET_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.BUDGET_ID BUDGET_ID,
       W.BUDGET_VERSION_ID BUDGET_VERSION_ID,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) TOTAL,
       W.BUDGET_PLANT_CLASS_ID BUDGET_PLANT_CLASS_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID
  from BUDGET_MONTHLY_DATA W;


create or replace view BUDGET_AFUDC_CALC_FY_SV as
select BAC.WORK_ORDER_ID,
       BAC.REVISION,
       BAC.EXPENDITURE_TYPE_ID,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       BAC.AMOUNT,
       BAC.AMOUNT_AFUDC,
       BAC.AMOUNT_CPI,
       BAC.IN_SERVICE_DATE,
       BAC.AFUDC_TYPE_ID,
       BAC.CLOSING_PATTERN_ID,
       BAC.CLOSING_OPTION_ID,
       BAC.ELIGIBLE_FOR_AFUDC,
       BAC.ELIGIBLE_FOR_CPI,
       BAC.AFUDC_STOP_DATE,
       BAC.AFUDC_START_DATE,
       BAC.AFUDC_DEBT_RATE,
       BAC.AFUDC_EQUITY_RATE,
       BAC.CPI_RATE,
       BAC.AFUDC_COMPOUND_IND,
       BAC.CPI_COMPOUND_IND,
       BAC.CLOSINGS_PCT,
       BAC.FUNDING_WO_INDICATOR,
       BAC.BEG_CWIP,
       BAC.BEG_CWIP_AFUDC,
       BAC.BEG_CWIP_CPI,
       BAC.CURRENT_MONTH_CHARGES_AFUDC,
       BAC.CURRENT_MONTH_CHARGES_CPI,
       BAC.CURRENT_MONTH_CLOSINGS,
       BAC.CURRENT_MONTH_CLOSINGS_AFUDC,
       BAC.CURRENT_MONTH_CLOSINGS_CPI,
       BAC.UNCOMPOUNDED_AFUDC,
       BAC.UNCOMPOUNDED_CPI,
       BAC.COMPOUNDED_AFUDC,
       BAC.COMPOUNDED_CPI,
       BAC.AFUDC_BASE,
       BAC.CPI_BASE,
       BAC.AFUDC_DEBT,
       BAC.AFUDC_EQUITY,
       BAC.CPI,
       BAC.END_CWIP,
       BAC.END_CWIP_AFUDC,
       BAC.END_CWIP_CPI,
       BAC.EST_MONTHLY_ID_DEBT,
       BAC.EST_MONTHLY_ID_EQUITY,
       BAC.EST_MONTHLY_ID_CPI,
       BAC.EST_MONTHLY_ID_CWIPBAL_CWIP,
       BAC.EST_MONTHLY_ID_CWIPBAL_TAX,
       BAC.EST_MONTHLY_ID_CWIPBAL_REMOVAL,
       BAC.EST_MONTHLY_ID_CWIPBAL_SALVAGE,
       BAC.EST_MONTHLY_ID_CLOSINGS_CWIP,
       BAC.EST_MONTHLY_ID_CLOSINGS_TAX,
       BAC.EST_MONTHLY_ID_CLOSINGS_RMVL,
       BAC.EST_MONTHLY_ID_CLOSINGS_SLVG,
       BAC.DEBT_EST_CHG_TYPE_ID,
       BAC.EQUITY_EST_CHG_TYPE_ID,
       BAC.CPI_EST_CHG_TYPE_ID,
       BAC.TIME_STAMP,
       BAC.USER_ID,
       BAC.CAP_INTEREST_ADJUSTMENT,
       BAC.CAP_INTEREST_ADJ_RATIO,
       BAC.CPI_BALANCE,
       BAC.CPI_CLOSINGS,
       BAC.TAX_STATUS_ID,
       BAC.FP_PERCENT_EXPENSE,
       BAC.TAX_EXPENSE_AMOUNT,
       BAC.TAX_EXPENSE_AMOUNT_BAL,
       BAC.TAX_EXPENSE_AMOUNT_CLOSED
  from BUDGET_AFUDC_CALC BAC, PP_CALENDAR PP
 where BAC.MONTH_NUMBER = PP.MONTH_NUMBER;


create or replace view BUDGET_AFUDC_CALC_SV as
select BAC.WORK_ORDER_ID,
       BAC.REVISION,
       BAC.EXPENDITURE_TYPE_ID,
       BAC.MONTH_NUMBER,
       BAC.AMOUNT,
       BAC.AMOUNT_AFUDC,
       BAC.AMOUNT_CPI,
       BAC.IN_SERVICE_DATE,
       BAC.AFUDC_TYPE_ID,
       BAC.CLOSING_PATTERN_ID,
       BAC.CLOSING_OPTION_ID,
       BAC.ELIGIBLE_FOR_AFUDC,
       BAC.ELIGIBLE_FOR_CPI,
       BAC.AFUDC_STOP_DATE,
       BAC.AFUDC_START_DATE,
       BAC.AFUDC_DEBT_RATE,
       BAC.AFUDC_EQUITY_RATE,
       BAC.CPI_RATE,
       BAC.AFUDC_COMPOUND_IND,
       BAC.CPI_COMPOUND_IND,
       BAC.CLOSINGS_PCT,
       BAC.FUNDING_WO_INDICATOR,
       BAC.BEG_CWIP,
       BAC.BEG_CWIP_AFUDC,
       BAC.BEG_CWIP_CPI,
       BAC.CURRENT_MONTH_CHARGES_AFUDC,
       BAC.CURRENT_MONTH_CHARGES_CPI,
       BAC.CURRENT_MONTH_CLOSINGS,
       BAC.CURRENT_MONTH_CLOSINGS_AFUDC,
       BAC.CURRENT_MONTH_CLOSINGS_CPI,
       BAC.UNCOMPOUNDED_AFUDC,
       BAC.UNCOMPOUNDED_CPI,
       BAC.COMPOUNDED_AFUDC,
       BAC.COMPOUNDED_CPI,
       BAC.AFUDC_BASE,
       BAC.CPI_BASE,
       BAC.AFUDC_DEBT,
       BAC.AFUDC_EQUITY,
       BAC.CPI,
       BAC.END_CWIP,
       BAC.END_CWIP_AFUDC,
       BAC.END_CWIP_CPI,
       BAC.EST_MONTHLY_ID_DEBT,
       BAC.EST_MONTHLY_ID_EQUITY,
       BAC.EST_MONTHLY_ID_CPI,
       BAC.EST_MONTHLY_ID_CWIPBAL_CWIP,
       BAC.EST_MONTHLY_ID_CWIPBAL_TAX,
       BAC.EST_MONTHLY_ID_CWIPBAL_REMOVAL,
       BAC.EST_MONTHLY_ID_CWIPBAL_SALVAGE,
       BAC.EST_MONTHLY_ID_CLOSINGS_CWIP,
       BAC.EST_MONTHLY_ID_CLOSINGS_TAX,
       BAC.EST_MONTHLY_ID_CLOSINGS_RMVL,
       BAC.EST_MONTHLY_ID_CLOSINGS_SLVG,
       BAC.DEBT_EST_CHG_TYPE_ID,
       BAC.EQUITY_EST_CHG_TYPE_ID,
       BAC.CPI_EST_CHG_TYPE_ID,
       BAC.TIME_STAMP,
       BAC.USER_ID,
       BAC.CAP_INTEREST_ADJUSTMENT,
       BAC.CAP_INTEREST_ADJ_RATIO,
       BAC.CPI_BALANCE,
       BAC.CPI_CLOSINGS,
       BAC.TAX_STATUS_ID,
       BAC.FP_PERCENT_EXPENSE,
       BAC.TAX_EXPENSE_AMOUNT,
       BAC.TAX_EXPENSE_AMOUNT_BAL,
       BAC.TAX_EXPENSE_AMOUNT_CLOSED
  from BUDGET_AFUDC_CALC BAC;


create or replace view BUDGET_AMOUNTS_SV as
select * from BUDGET_AMOUNTS BA;


create or replace view BUDGET_SV as
select B.*,
       C.GL_COMPANY_NO,
       C.OWNED,
       C.DESCRIPTION             COMPANY_DESCRIPTION,
       C.STATUS_CODE_ID,
       C.SHORT_DESCRIPTION       COMPANY_SHORT_DESCRIPTION,
       C.COMPANY_SUMMARY_ID,
       C.AUTO_LIFE_MONTH,
       C.AUTO_CURVE_MONTH,
       C.AUTO_CLOSE_WO_NUM,
       C.PARENT_COMPANY_ID,
       C.TAX_RETURN_ID,
       C.CLOSING_ROLLUP_ID,
       C.AUTO_LIFE_MONTH_MONTHLY,
       C.COMPANY_TYPE,
       C.COMPANY_EIN,
       C.CWIP_ALLOCATION_MONTHS,
       C.PROP_TAX_COMPANY_ID,
       C.POWERTAX_COMPANY_ID
  from BUDGET B, COMPANY C
 where B.COMPANY_ID = C.COMPANY_ID;


create or replace view BUDGET_VERSION_FUND_PROJ_SV as
select BVFP.WORK_ORDER_ID, BVFP.REVISION, BVFP.BUDGET_VERSION_ID, BVFP.ACTIVE
  from BUDGET_VERSION_FUND_PROJ BVFP
union all
select WOC.WORK_ORDER_ID, WOC.CURRENT_REVISION, -9 BUDGET_VERSION_ID, 1 ACTIVE
  from WORK_ORDER_CONTROL WOC
 where WOC.FUNDING_WO_INDICATOR = 1
   and NVL(WOC.CURRENT_REVISION, 0) <> 0
union all
select WOC.WORK_ORDER_ID, max(WOA.REVISION), -8 BUDGET_VERSION_ID, 1 ACTIVE
  from WORK_ORDER_CONTROL WOC, WORK_ORDER_APPROVAL WOA
 where WOC.FUNDING_WO_INDICATOR = 1
   and WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
 group by WOC.WORK_ORDER_ID;


create or replace view BUDGET_VERSION_SV as
select BUDGET_VERSION_ID,
       DESCRIPTION,
       LONG_DESCRIPTION,
       LOCKED,
       START_YEAR,
       CURRENT_YEAR,
       END_YEAR,
       ACTUALS_MONTH,
       DATE_FINALIZED,
       CURRENT_VERSION,
       BUDGET_ONLY,
       OPEN_FOR_ENTRY,
       EXTERNAL_BUDGET_VERSION,
       BRING_IN_SUBS,
       OUTLOOK_NUMBER,
       LEVEL_ID,
       REPORTING_CURRENCY_ID,
       BUDGET_VERSION_TYPE_ID,
       COMPANY_ID,
       3 SORT,
       'fp,bi' FP_BI
  from BUDGET_VERSION BV
 where (BV.COMPANY_ID is null or BV.COMPANY_ID in (select COMPANY_ID from COMPANY))
union all
select -9 BUDGET_VERSION_ID,
       'Current Approved' DESCRIPTION,
       'Current Approved' LONG_DESCRIPTION,
       null LOCKED,
       null START_YEAR,
       null CURRENT_YEAR,
       null END_YEAR,
       null ACTUALS_MONTH,
       null DATE_FINALIZED,
       null CURRENT_VERSION,
       null BUDGET_ONLY,
       null OPEN_FOR_ENTRY,
       null EXTERNAL_BUDGET_VERSION,
       null BRING_IN_SUBS,
       null OUTLOOK_NUMBER,
       null LEVEL_ID,
       null REPORTING_CURRENCY_ID,
       null BUDGET_VERSION_TYPE_ID,
       null COMPANY_ID,
       1 SORT,
       'fp' FP_BI
  from DUAL
union all
select -8 BUDGET_VERSION_ID,
       'Latest Revision' DESCRIPTION,
       'Latest Revision' LONG_DESCRIPTION,
       null LOCKED,
       null START_YEAR,
       null CURRENT_YEAR,
       null END_YEAR,
       null ACTUALS_MONTH,
       null DATE_FINALIZED,
       null CURRENT_VERSION,
       null BUDGET_ONLY,
       null OPEN_FOR_ENTRY,
       null EXTERNAL_BUDGET_VERSION,
       null BRING_IN_SUBS,
       null OUTLOOK_NUMBER,
       null LEVEL_ID,
       null REPORTING_CURRENCY_ID,
       null BUDGET_VERSION_TYPE_ID,
       null COMPANY_ID,
       2 SORT,
       'fp' FP_BI
  from DUAL;


create or replace view COMMITMENTS_BI_FY_SV as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       CV.AMOUNT,
       CV.HOURS,
       CV.QUANTITY,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       C.COMMITMENT_ID,
       C.COMMITMENT_TYPE_ID,
       C.EST_CHG_TYPE_ID COMM_EST_CHG_TYPE_ID,
       C.DESCRIPTION,
       C.EST_START_DATE,
       C.EST_COMP_DATE,
       C.PO_NUMBER,
       C.EXPENDITURE_TYPE_ID,
       C.JOB_TASK_ID,
       C.NOTES,
       C.ID_NUMBER,
       C.UNITS,
       C.REFERENCE_NUMBER,
       C.VENDOR_INFORMATION,
       C.WORK_ORDER_ID,
       C.BUS_SEGMENT_ID,
       C.DEPARTMENT_ID,
       C.COMPANY_ID,
       C.TIME_STAMP,
       C.USER_ID,
       C.STCK_KEEP_UNIT_ID,
       C.RETIREMENT_UNIT_ID,
       C.CHARGE_MO_YR,
       C.UTILITY_ACCOUNT_ID,
       C.SUB_ACCOUNT_ID,
       C.PAYMENT_DATE,
       C.LOADING_AMOUNT,
       C.CHARGE_AUDIT_ID,
       C.JOURNAL_CODE,
       C.COST_ELEMENT_ID,
       C.EXTERNAL_GL_ACCOUNT,
       C.GL_ACCOUNT_ID,
       C.STATUS,
       C.NON_UNITIZED_STATUS,
       C.EXCLUDE_FROM_OVERHEADS,
       C.CLOSED_MONTH_NUMBER,
       C.ORIGINAL_CURRENCY,
       C.ORIGINAL_AMOUNT,
       C.SERIAL_NUMBER
  from COMMITMENTS C,
       COMMITMENT_NET_VIEW CV,
       WORK_ORDER_CONTROL WOC,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB,
       PP_CALENDAR PP
 where C.COMMITMENT_ID = CV.COMMITMENT_ID
   and C.COMMITMENT_TYPE_ID in (1, 2)
   and C.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and C.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CT.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and C.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and C.MONTH_NUMBER = PP.MONTH_NUMBER
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where C.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 2)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where C.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 2);


create or replace view COMMITMENTS_BI_SV as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE     EST_CHG_TYPE_ID,
       CV.AMOUNT,
       CV.HOURS,
       CV.QUANTITY,
       C.MONTH_NUMBER,
       C.COMMITMENT_ID,
       C.COMMITMENT_TYPE_ID,
       C.EST_CHG_TYPE_ID        COMM_EST_CHG_TYPE_ID,
       C.DESCRIPTION,
       C.EST_START_DATE,
       C.EST_COMP_DATE,
       C.PO_NUMBER,
       C.EXPENDITURE_TYPE_ID,
       C.JOB_TASK_ID,
       C.NOTES,
       C.ID_NUMBER,
       C.UNITS,
       C.REFERENCE_NUMBER,
       C.VENDOR_INFORMATION,
       C.WORK_ORDER_ID,
       C.BUS_SEGMENT_ID,
       C.DEPARTMENT_ID,
       C.COMPANY_ID,
       C.TIME_STAMP,
       C.USER_ID,
       C.STCK_KEEP_UNIT_ID,
       C.RETIREMENT_UNIT_ID,
       C.CHARGE_MO_YR,
       C.UTILITY_ACCOUNT_ID,
       C.SUB_ACCOUNT_ID,
       C.PAYMENT_DATE,
       C.LOADING_AMOUNT,
       C.CHARGE_AUDIT_ID,
       C.JOURNAL_CODE,
       C.COST_ELEMENT_ID,
       C.EXTERNAL_GL_ACCOUNT,
       C.GL_ACCOUNT_ID,
       C.STATUS,
       C.NON_UNITIZED_STATUS,
       C.EXCLUDE_FROM_OVERHEADS,
       C.CLOSED_MONTH_NUMBER,
       C.ORIGINAL_CURRENCY,
       C.ORIGINAL_AMOUNT,
       C.SERIAL_NUMBER
  from COMMITMENTS C,
       COMMITMENT_NET_VIEW CV,
       WORK_ORDER_CONTROL WOC,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB
 where C.COMMITMENT_ID = CV.COMMITMENT_ID
   and C.COMMITMENT_TYPE_ID in (1, 2)
   and C.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and C.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CT.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and C.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where C.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 2)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where C.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 2);


create or replace view COMMITMENTS_FP_FY_SV as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       CV.AMOUNT,
       CV.HOURS,
       CV.QUANTITY,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       C.COMMITMENT_ID,
       C.COMMITMENT_TYPE_ID,
       C.EST_CHG_TYPE_ID COMM_EST_CHG_TYPE_ID,
       C.DESCRIPTION,
       C.EST_START_DATE,
       C.EST_COMP_DATE,
       C.PO_NUMBER,
       C.EXPENDITURE_TYPE_ID,
       C.JOB_TASK_ID,
       C.NOTES,
       C.ID_NUMBER,
       C.UNITS,
       C.REFERENCE_NUMBER,
       C.VENDOR_INFORMATION,
       C.WORK_ORDER_ID,
       C.BUS_SEGMENT_ID,
       C.DEPARTMENT_ID,
       C.COMPANY_ID,
       C.TIME_STAMP,
       C.USER_ID,
       C.STCK_KEEP_UNIT_ID,
       C.RETIREMENT_UNIT_ID,
       C.CHARGE_MO_YR,
       C.UTILITY_ACCOUNT_ID,
       C.SUB_ACCOUNT_ID,
       C.PAYMENT_DATE,
       C.LOADING_AMOUNT,
       C.CHARGE_AUDIT_ID,
       C.JOURNAL_CODE,
       C.COST_ELEMENT_ID,
       C.EXTERNAL_GL_ACCOUNT,
       C.GL_ACCOUNT_ID,
       C.STATUS,
       C.NON_UNITIZED_STATUS,
       C.EXCLUDE_FROM_OVERHEADS,
       C.CLOSED_MONTH_NUMBER,
       C.ORIGINAL_CURRENCY,
       C.ORIGINAL_AMOUNT,
       C.SERIAL_NUMBER
  from COMMITMENTS C,
       COMMITMENT_NET_VIEW CV,
       WORK_ORDER_CONTROL WOC,
       WORK_ORDER_ACCOUNT FPA,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       WO_EST_HIERARCHY_MAP WEH_MAP,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB,
       PP_CALENDAR PP
 where C.COMMITMENT_ID = CV.COMMITMENT_ID
   and C.COMMITMENT_TYPE_ID in (1, 2)
   and C.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.FUNDING_WO_ID = FPA.WORK_ORDER_ID
   and FPA.WO_EST_HIERARCHY_ID = WEH_MAP.WO_EST_HIERARCHY_ID
   and C.COST_ELEMENT_ID = WEH_MAP.COST_ELEMENT_ID
   and WEH_MAP.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and C.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and C.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and C.MONTH_NUMBER = PP.MONTH_NUMBER
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where C.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 1)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where C.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 1);


create or replace view COMMITMENTS_FP_SV as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE     EST_CHG_TYPE_ID,
       CV.AMOUNT,
       CV.HOURS,
       CV.QUANTITY,
       C.MONTH_NUMBER,
       C.COMMITMENT_ID,
       C.COMMITMENT_TYPE_ID,
       C.EST_CHG_TYPE_ID        COMM_EST_CHG_TYPE_ID,
       C.DESCRIPTION,
       C.EST_START_DATE,
       C.EST_COMP_DATE,
       C.PO_NUMBER,
       C.EXPENDITURE_TYPE_ID,
       C.JOB_TASK_ID,
       C.NOTES,
       C.ID_NUMBER,
       C.UNITS,
       C.REFERENCE_NUMBER,
       C.VENDOR_INFORMATION,
       C.WORK_ORDER_ID,
       C.BUS_SEGMENT_ID,
       C.DEPARTMENT_ID,
       C.COMPANY_ID,
       C.TIME_STAMP,
       C.USER_ID,
       C.STCK_KEEP_UNIT_ID,
       C.RETIREMENT_UNIT_ID,
       C.CHARGE_MO_YR,
       C.UTILITY_ACCOUNT_ID,
       C.SUB_ACCOUNT_ID,
       C.PAYMENT_DATE,
       C.LOADING_AMOUNT,
       C.CHARGE_AUDIT_ID,
       C.JOURNAL_CODE,
       C.COST_ELEMENT_ID,
       C.EXTERNAL_GL_ACCOUNT,
       C.GL_ACCOUNT_ID,
       C.STATUS,
       C.NON_UNITIZED_STATUS,
       C.EXCLUDE_FROM_OVERHEADS,
       C.CLOSED_MONTH_NUMBER,
       C.ORIGINAL_CURRENCY,
       C.ORIGINAL_AMOUNT,
       C.SERIAL_NUMBER
  from COMMITMENTS C,
       COMMITMENT_NET_VIEW CV,
       WORK_ORDER_CONTROL WOC,
       WORK_ORDER_ACCOUNT FPA,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       WO_EST_HIERARCHY_MAP WEH_MAP,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB
 where C.COMMITMENT_ID = CV.COMMITMENT_ID
   and C.COMMITMENT_TYPE_ID in (1, 2)
   and C.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.FUNDING_WO_ID = FPA.WORK_ORDER_ID
   and FPA.WO_EST_HIERARCHY_ID = WEH_MAP.WO_EST_HIERARCHY_ID
   and C.COST_ELEMENT_ID = WEH_MAP.COST_ELEMENT_ID
   and WEH_MAP.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and C.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and C.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where C.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 1)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where C.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 1);


create or replace view CWIP_BI_FY_SV as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       CC.CHARGE_ID,
       CC.CHARGE_TYPE_ID,
       CC.JOB_TASK_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.WORK_ORDER_ID,
       CC.TIME_STAMP,
       CC.USER_ID,
       CC.STCK_KEEP_UNIT_ID,
       CC.RETIREMENT_UNIT_ID,
       CC.CHARGE_MO_YR,
       CC.DESCRIPTION,
       CC.QUANTITY,
       CC.UTILITY_ACCOUNT_ID,
       CC.BUS_SEGMENT_ID,
       CC.AMOUNT,
       CC.SUB_ACCOUNT_ID,
       CC.HOURS,
       CC.PAYMENT_DATE,
       CC.NOTES,
       CC.ID_NUMBER,
       CC.UNITS,
       CC.LOADING_AMOUNT,
       CC.REFERENCE_NUMBER,
       CC.VENDOR_INFORMATION,
       CC.DEPARTMENT_ID,
       CC.CHARGE_AUDIT_ID,
       CC.JOURNAL_CODE,
       CC.COST_ELEMENT_ID,
       CC.EXTERNAL_GL_ACCOUNT,
       CC.GL_ACCOUNT_ID,
       CC.STATUS,
       CC.COMPANY_ID,
       CC.NON_UNITIZED_STATUS,
       CC.EXCLUDE_FROM_OVERHEADS,
       CC.CLOSED_MONTH_NUMBER,
       CC.ORIGINAL_CURRENCY,
       CC.ORIGINAL_AMOUNT,
       CC.ASSET_LOCATION_ID,
       CC.SERIAL_NUMBER,
       CC.PO_NUMBER,
       CC.UNIT_CLOSED_MONTH_NUMBER,
       CC.TAX_ORIG_MONTH_NUMBER
  from CWIP_CHARGE CC,
       WORK_ORDER_CONTROL WOC,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB,
       PP_CALENDAR PP
 where CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and CC.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CT.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and CC.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and CC.MONTH_NUMBER = PP.MONTH_NUMBER
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where CC.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 2)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where CC.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 2);


create or replace view CWIP_BI_SV as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       CC.MONTH_NUMBER,
       CC.CHARGE_ID,
       CC.CHARGE_TYPE_ID,
       CC.JOB_TASK_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.WORK_ORDER_ID,
       CC.TIME_STAMP,
       CC.USER_ID,
       CC.STCK_KEEP_UNIT_ID,
       CC.RETIREMENT_UNIT_ID,
       CC.CHARGE_MO_YR,
       CC.DESCRIPTION,
       CC.QUANTITY,
       CC.UTILITY_ACCOUNT_ID,
       CC.BUS_SEGMENT_ID,
       CC.AMOUNT,
       CC.SUB_ACCOUNT_ID,
       CC.HOURS,
       CC.PAYMENT_DATE,
       CC.NOTES,
       CC.ID_NUMBER,
       CC.UNITS,
       CC.LOADING_AMOUNT,
       CC.REFERENCE_NUMBER,
       CC.VENDOR_INFORMATION,
       CC.DEPARTMENT_ID,
       CC.CHARGE_AUDIT_ID,
       CC.JOURNAL_CODE,
       CC.COST_ELEMENT_ID,
       CC.EXTERNAL_GL_ACCOUNT,
       CC.GL_ACCOUNT_ID,
       CC.STATUS,
       CC.COMPANY_ID,
       CC.NON_UNITIZED_STATUS,
       CC.EXCLUDE_FROM_OVERHEADS,
       CC.CLOSED_MONTH_NUMBER,
       CC.ORIGINAL_CURRENCY,
       CC.ORIGINAL_AMOUNT,
       CC.ASSET_LOCATION_ID,
       CC.SERIAL_NUMBER,
       CC.PO_NUMBER,
       CC.UNIT_CLOSED_MONTH_NUMBER,
       CC.TAX_ORIG_MONTH_NUMBER
  from CWIP_CHARGE CC,
       WORK_ORDER_CONTROL WOC,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB
 where CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and CC.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CT.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and CC.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where CC.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 2)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where CC.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 2);


create or replace view CWIP_FP_FY_SV as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       PP.FISCAL_YEAR * 100 + PP.FISCAL_MONTH MONTH_NUMBER,
       CC.CHARGE_ID,
       CC.CHARGE_TYPE_ID,
       CC.JOB_TASK_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.WORK_ORDER_ID,
       CC.TIME_STAMP,
       CC.USER_ID,
       CC.STCK_KEEP_UNIT_ID,
       CC.RETIREMENT_UNIT_ID,
       CC.CHARGE_MO_YR,
       CC.DESCRIPTION,
       CC.QUANTITY,
       CC.UTILITY_ACCOUNT_ID,
       CC.BUS_SEGMENT_ID,
       CC.AMOUNT,
       CC.SUB_ACCOUNT_ID,
       CC.HOURS,
       CC.PAYMENT_DATE,
       CC.NOTES,
       CC.ID_NUMBER,
       CC.UNITS,
       CC.LOADING_AMOUNT,
       CC.REFERENCE_NUMBER,
       CC.VENDOR_INFORMATION,
       CC.DEPARTMENT_ID,
       CC.CHARGE_AUDIT_ID,
       CC.JOURNAL_CODE,
       CC.COST_ELEMENT_ID,
       CC.EXTERNAL_GL_ACCOUNT,
       CC.GL_ACCOUNT_ID,
       CC.STATUS,
       CC.COMPANY_ID,
       CC.NON_UNITIZED_STATUS,
       CC.EXCLUDE_FROM_OVERHEADS,
       CC.CLOSED_MONTH_NUMBER,
       CC.ORIGINAL_CURRENCY,
       CC.ORIGINAL_AMOUNT,
       CC.ASSET_LOCATION_ID,
       CC.SERIAL_NUMBER,
       CC.PO_NUMBER,
       CC.UNIT_CLOSED_MONTH_NUMBER,
       CC.TAX_ORIG_MONTH_NUMBER
  from CWIP_CHARGE CC,
       WORK_ORDER_CONTROL WOC,
       WORK_ORDER_ACCOUNT FPA,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       WO_EST_HIERARCHY_MAP WEH_MAP,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB,
       PP_CALENDAR PP
 where CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.FUNDING_WO_ID = FPA.WORK_ORDER_ID
   and FPA.WO_EST_HIERARCHY_ID = WEH_MAP.WO_EST_HIERARCHY_ID
   and CC.COST_ELEMENT_ID = WEH_MAP.COST_ELEMENT_ID
   and WEH_MAP.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and CC.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CC.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and CC.MONTH_NUMBER = PP.MONTH_NUMBER
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where CC.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 1)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where CC.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 1);


create or replace view CWIP_FP_SV as
select WOC.BUDGET_ID,
       WOC.FUNDING_WO_ID,
       ECT.FUNDING_CHG_TYPE EST_CHG_TYPE_ID,
       CC.MONTH_NUMBER,
       CC.CHARGE_ID,
       CC.CHARGE_TYPE_ID,
       CC.JOB_TASK_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.WORK_ORDER_ID,
       CC.TIME_STAMP,
       CC.USER_ID,
       CC.STCK_KEEP_UNIT_ID,
       CC.RETIREMENT_UNIT_ID,
       CC.CHARGE_MO_YR,
       CC.DESCRIPTION,
       CC.QUANTITY,
       CC.UTILITY_ACCOUNT_ID,
       CC.BUS_SEGMENT_ID,
       CC.AMOUNT,
       CC.SUB_ACCOUNT_ID,
       CC.HOURS,
       CC.PAYMENT_DATE,
       CC.NOTES,
       CC.ID_NUMBER,
       CC.UNITS,
       CC.LOADING_AMOUNT,
       CC.REFERENCE_NUMBER,
       CC.VENDOR_INFORMATION,
       CC.DEPARTMENT_ID,
       CC.CHARGE_AUDIT_ID,
       CC.JOURNAL_CODE,
       CC.COST_ELEMENT_ID,
       CC.EXTERNAL_GL_ACCOUNT,
       CC.GL_ACCOUNT_ID,
       CC.STATUS,
       CC.COMPANY_ID,
       CC.NON_UNITIZED_STATUS,
       CC.EXCLUDE_FROM_OVERHEADS,
       CC.CLOSED_MONTH_NUMBER,
       CC.ORIGINAL_CURRENCY,
       CC.ORIGINAL_AMOUNT,
       CC.ASSET_LOCATION_ID,
       CC.SERIAL_NUMBER,
       CC.PO_NUMBER,
       CC.UNIT_CLOSED_MONTH_NUMBER,
       CC.TAX_ORIG_MONTH_NUMBER
  from CWIP_CHARGE CC,
       WORK_ORDER_CONTROL WOC,
       WORK_ORDER_ACCOUNT FPA,
       COST_ELEMENT CE,
       CHARGE_TYPE CT,
       WO_EST_HIERARCHY_MAP WEH_MAP,
       ESTIMATE_CHARGE_TYPE ECT,
       (select count(1) CNT_SOB from TEMP_SET_OF_BOOKS where ROWNUM = 1) SOB
 where CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID
   and WOC.FUNDING_WO_ID = FPA.WORK_ORDER_ID
   and FPA.WO_EST_HIERARCHY_ID = WEH_MAP.WO_EST_HIERARCHY_ID
   and CC.COST_ELEMENT_ID = WEH_MAP.COST_ELEMENT_ID
   and WEH_MAP.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
   and CC.COST_ELEMENT_ID = CE.COST_ELEMENT_ID
   and CE.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
   and CC.EXPENDITURE_TYPE_ID in (1, 2, 3, 4)
   and CT.PROCESSING_TYPE_ID not in (1, 5, 9, 10)
   and NVL(CT.EXCLUDE_FROM_TOTAL_CHARGES, 0) = 0
   and DECODE(SOB.CNT_SOB, 0, -1, CT.CHARGE_TYPE_ID) in
       (select CHARGE_TYPE_ID
          from TEMP_SET_OF_BOOKS
        union all
        select -1 from DUAL)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCLUSION A
         where CC.COST_ELEMENT_ID = A.COST_ELEMENT_ID
           and a."LEVEL" = 1)
   and not exists (select 1
          from UPDATE_WITH_ACTUALS_EXCL_ET A
         where CC.EXPENDITURE_TYPE_ID = A.EXPENDITURE_TYPE_ID
           and a."LEVEL" = 1);


create or replace view MONTH_PERIODS_FY_SV as
select max(DECODE(P.FISCAL_PERIOD, 1, P.MONTH, '')) MONTH_PERIOD1,
       max(DECODE(P.FISCAL_PERIOD, 2, P.MONTH, '')) MONTH_PERIOD2,
       max(DECODE(P.FISCAL_PERIOD, 3, P.MONTH, '')) MONTH_PERIOD3,
       max(DECODE(P.FISCAL_PERIOD, 4, P.MONTH, '')) MONTH_PERIOD4,
       max(DECODE(P.FISCAL_PERIOD, 5, P.MONTH, '')) MONTH_PERIOD5,
       max(DECODE(P.FISCAL_PERIOD, 6, P.MONTH, '')) MONTH_PERIOD6,
       max(DECODE(P.FISCAL_PERIOD, 7, P.MONTH, '')) MONTH_PERIOD7,
       max(DECODE(P.FISCAL_PERIOD, 8, P.MONTH, '')) MONTH_PERIOD8,
       max(DECODE(P.FISCAL_PERIOD, 9, P.MONTH, '')) MONTH_PERIOD9,
       max(DECODE(P.FISCAL_PERIOD, 10, P.MONTH, '')) MONTH_PERIOD10,
       max(DECODE(P.FISCAL_PERIOD, 11, P.MONTH, '')) MONTH_PERIOD11,
       max(DECODE(P.FISCAL_PERIOD, 12, P.MONTH, '')) MONTH_PERIOD12
  from PP_TABLE_MONTHS P;


create or replace view MONTH_PERIODS_SV as
select 'January' MONTH_PERIOD1,
       'February' MONTH_PERIOD2,
       'March' MONTH_PERIOD3,
       'April' MONTH_PERIOD4,
       'May' MONTH_PERIOD5,
       'June' MONTH_PERIOD6,
       'July' MONTH_PERIOD7,
       'August' MONTH_PERIOD8,
       'September' MONTH_PERIOD9,
       'October' MONTH_PERIOD10,
       'November' MONTH_PERIOD11,
       'December' MONTH_PERIOD12
  from DUAL;


create or replace view WO_EST_MONTHLY_DEESC_SV as
select W.EST_MONTHLY_ID EST_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.REVISION REVISION,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) - NVL(WE.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) - NVL(WE.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) - NVL(WE.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) - NVL(WE.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) - NVL(WE.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) - NVL(WE.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) - NVL(WE.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) - NVL(WE.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) - NVL(WE.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) - NVL(WE.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) - NVL(WE.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) - NVL(WE.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) - NVL(WE.TOTAL, 0) TOTAL,
       W.UTILITY_ACCOUNT_ID UTILITY_ACCOUNT_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WO_WORK_ORDER_ID WO_WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID,
       W.SUBSTITUTION_ID SUBSTITUTION_ID,
       NVL(W.HRS_JAN, 0) HRS_PERIOD1,
       NVL(W.HRS_FEB, 0) HRS_PERIOD2,
       NVL(W.HRS_MAR, 0) HRS_PERIOD3,
       NVL(W.HRS_APR, 0) HRS_PERIOD4,
       NVL(W.HRS_MAY, 0) HRS_PERIOD5,
       NVL(W.HRS_JUN, 0) HRS_PERIOD6,
       NVL(W.HRS_JUL, 0) HRS_PERIOD7,
       NVL(W.HRS_AUG, 0) HRS_PERIOD8,
       NVL(W.HRS_SEP, 0) HRS_PERIOD9,
       NVL(W.HRS_OCT, 0) HRS_PERIOD10,
       NVL(W.HRS_NOV, 0) HRS_PERIOD11,
       NVL(W.HRS_DEC, 0) HRS_PERIOD12,
       NVL(W.HRS_TOTAL, 0) HRS_TOTAL,
       NVL(W.QTY_JAN, 0) QTY_PERIOD1,
       NVL(W.QTY_FEB, 0) QTY_PERIOD2,
       NVL(W.QTY_MAR, 0) QTY_PERIOD3,
       NVL(W.QTY_APR, 0) QTY_PERIOD4,
       NVL(W.QTY_MAY, 0) QTY_PERIOD5,
       NVL(W.QTY_JUN, 0) QTY_PERIOD6,
       NVL(W.QTY_JUL, 0) QTY_PERIOD7,
       NVL(W.QTY_AUG, 0) QTY_PERIOD8,
       NVL(W.QTY_SEP, 0) QTY_PERIOD9,
       NVL(W.QTY_OCT, 0) QTY_PERIOD10,
       NVL(W.QTY_NOV, 0) QTY_PERIOD11,
       NVL(W.QTY_DEC, 0) QTY_PERIOD12,
       NVL(W.QTY_TOTAL, 0) QTY_TOTAL
  from WO_EST_MONTHLY W, WO_EST_MONTHLY_ESCALATION WE
 where W.EST_MONTHLY_ID = WE.EST_MONTHLY_ID(+)
   and W.YEAR = WE.YEAR(+);


create or replace view WO_EST_MONTHLY_FY_DEESC_SV as
select EST_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       WORK_ORDER_ID,
       REVISION,
       YEAR,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       AMOUNT_PERIOD1,
       AMOUNT_PERIOD2,
       AMOUNT_PERIOD3,
       AMOUNT_PERIOD4,
       AMOUNT_PERIOD5,
       AMOUNT_PERIOD6,
       AMOUNT_PERIOD7,
       AMOUNT_PERIOD8,
       AMOUNT_PERIOD9,
       AMOUNT_PERIOD10,
       AMOUNT_PERIOD11,
       AMOUNT_PERIOD12,
       AMOUNT_PERIOD1 + AMOUNT_PERIOD2 + AMOUNT_PERIOD3 + AMOUNT_PERIOD4 + AMOUNT_PERIOD5 +
       AMOUNT_PERIOD6 + AMOUNT_PERIOD7 + AMOUNT_PERIOD8 + AMOUNT_PERIOD9 + AMOUNT_PERIOD10 +
       AMOUNT_PERIOD11 + AMOUNT_PERIOD12 TOTAL,
       UTILITY_ACCOUNT_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID,
       HRS_PERIOD1,
       HRS_PERIOD2,
       HRS_PERIOD3,
       HRS_PERIOD4,
       HRS_PERIOD5,
       HRS_PERIOD6,
       HRS_PERIOD7,
       HRS_PERIOD8,
       HRS_PERIOD9,
       HRS_PERIOD10,
       HRS_PERIOD11,
       HRS_PERIOD12,
       HRS_PERIOD1 + HRS_PERIOD2 + HRS_PERIOD3 + HRS_PERIOD4 + HRS_PERIOD5 + HRS_PERIOD6 +
       HRS_PERIOD7 + HRS_PERIOD8 + HRS_PERIOD9 + HRS_PERIOD10 + HRS_PERIOD11 + HRS_PERIOD12 HRS_TOTAL,
       QTY_PERIOD1,
       QTY_PERIOD2,
       QTY_PERIOD3,
       QTY_PERIOD4,
       QTY_PERIOD5,
       QTY_PERIOD6,
       QTY_PERIOD7,
       QTY_PERIOD8,
       QTY_PERIOD9,
       QTY_PERIOD10,
       QTY_PERIOD11,
       QTY_PERIOD12,
       QTY_PERIOD1 + QTY_PERIOD2 + QTY_PERIOD3 + QTY_PERIOD4 + QTY_PERIOD5 + QTY_PERIOD6 +
       QTY_PERIOD7 + QTY_PERIOD8 + QTY_PERIOD9 + QTY_PERIOD10 + QTY_PERIOD11 + QTY_PERIOD12 QTY_TOTAL
  from (
   select W.EST_MONTHLY_ID EST_MONTHLY_ID,
          max(W.TIME_STAMP) TIME_STAMP,
          max(W.USER_ID) USER_ID,
          W.WORK_ORDER_ID WORK_ORDER_ID,
          W.REVISION REVISION,
          decode(sign(p.fiscal_period - p.month_num),1,w.year,w.year + 1) year,
          max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
          max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
          max(W.DEPARTMENT_ID) DEPARTMENT_ID,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,nvl(w.january,0) - nvl(we.january,0),2,nvl(w.february,0) - nvl(we.february,0),3,nvl(w.march,0) - nvl(we.march,0),4,nvl(w.april,0) - nvl(we.april,0),5,nvl(w.may,0) - nvl(we.may,0),6,nvl(w.june,0) - nvl(we.june,0),7,nvl(w.july,0) - nvl(we.july,0),8,nvl(w.august,0) - nvl(we.august,0),9,nvl(w.september,0) - nvl(we.september,0),10,nvl(w.october,0) - nvl(we.october,0),11,nvl(w.november,0) - nvl(we.november,0),12,nvl(w.december,0) - nvl(we.december,0)),0),0)) amount_period12,
          max(W.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
          max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
          0 FUTURE_DOLLARS,
          0 HIST_ACTUALS,
          max(W.WO_WORK_ORDER_ID) WO_WORK_ORDER_ID,
          max(W.JOB_TASK_ID) JOB_TASK_ID,
          max(W.SUBSTITUTION_ID) SUBSTITUTION_ID,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period12,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period12
     from WO_EST_MONTHLY W, PP_TABLE_MONTHS P, WO_EST_MONTHLY_ESCALATION WE
    where W.EST_MONTHLY_ID = WE.EST_MONTHLY_ID (+)
      and W.YEAR = WE.YEAR (+)
    group by W.EST_MONTHLY_ID, W.WORK_ORDER_ID, W.REVISION, decode(sign(P.FISCAL_PERIOD - P.MONTH_NUM),1,W.YEAR,W.YEAR + 1)
   );


create or replace view WO_EST_MONTHLY_FY_SV as
select EST_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       WORK_ORDER_ID,
       REVISION,
       year,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       AMOUNT_PERIOD1,
       AMOUNT_PERIOD2,
       AMOUNT_PERIOD3,
       AMOUNT_PERIOD4,
       AMOUNT_PERIOD5,
       AMOUNT_PERIOD6,
       AMOUNT_PERIOD7,
       AMOUNT_PERIOD8,
       AMOUNT_PERIOD9,
       AMOUNT_PERIOD10,
       AMOUNT_PERIOD11,
       AMOUNT_PERIOD12,
       AMOUNT_PERIOD1 + AMOUNT_PERIOD2 + AMOUNT_PERIOD3 + AMOUNT_PERIOD4 + AMOUNT_PERIOD5 +
       AMOUNT_PERIOD6 + AMOUNT_PERIOD7 + AMOUNT_PERIOD8 + AMOUNT_PERIOD9 + AMOUNT_PERIOD10 +
       AMOUNT_PERIOD11 + AMOUNT_PERIOD12 TOTAL,
       UTILITY_ACCOUNT_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID,
       HRS_PERIOD1,
       HRS_PERIOD2,
       HRS_PERIOD3,
       HRS_PERIOD4,
       HRS_PERIOD5,
       HRS_PERIOD6,
       HRS_PERIOD7,
       HRS_PERIOD8,
       HRS_PERIOD9,
       HRS_PERIOD10,
       HRS_PERIOD11,
       HRS_PERIOD12,
       HRS_PERIOD1 + HRS_PERIOD2 + HRS_PERIOD3 + HRS_PERIOD4 + HRS_PERIOD5 + HRS_PERIOD6 +
       HRS_PERIOD7 + HRS_PERIOD8 + HRS_PERIOD9 + HRS_PERIOD10 + HRS_PERIOD11 + HRS_PERIOD12 HRS_TOTAL,
       QTY_PERIOD1,
       QTY_PERIOD2,
       QTY_PERIOD3,
       QTY_PERIOD4,
       QTY_PERIOD5,
       QTY_PERIOD6,
       QTY_PERIOD7,
       QTY_PERIOD8,
       QTY_PERIOD9,
       QTY_PERIOD10,
       QTY_PERIOD11,
       QTY_PERIOD12,
       QTY_PERIOD1 + QTY_PERIOD2 + QTY_PERIOD3 + QTY_PERIOD4 + QTY_PERIOD5 + QTY_PERIOD6 +
       QTY_PERIOD7 + QTY_PERIOD8 + QTY_PERIOD9 + QTY_PERIOD10 + QTY_PERIOD11 + QTY_PERIOD12 QTY_TOTAL
  from (
   select W.EST_MONTHLY_ID EST_MONTHLY_ID,
          max(W.TIME_STAMP) TIME_STAMP,
          max(W.USER_ID) USER_ID,
          W.WORK_ORDER_ID WORK_ORDER_ID,
          W.REVISION REVISION,
          decode(sign(p.fiscal_period - p.month_num),1,w.year,w.year + 1) year,
          max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
          max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
          max(W.DEPARTMENT_ID) DEPARTMENT_ID,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.january,2,w.february,3,w.march,4,w.april,5,w.may,6,w.june,7,w.july,8,w.august,9,w.september,10,w.october,11,w.november,12,w.december),0),0)) amount_period12,
          max(W.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
          max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
          0 FUTURE_DOLLARS,
          0 HIST_ACTUALS,
          max(W.WO_WORK_ORDER_ID) WO_WORK_ORDER_ID,
          max(W.JOB_TASK_ID) JOB_TASK_ID,
          max(W.SUBSTITUTION_ID) SUBSTITUTION_ID,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.hrs_jan,2,w.hrs_feb,3,w.hrs_mar,4,w.hrs_apr,5,w.hrs_may,6,w.hrs_jun,7,w.hrs_jul,8,w.hrs_aug,9,w.hrs_sep,10,w.hrs_oct,11,w.hrs_nov,12,w.hrs_dec),0),0)) hrs_period12,
          sum(nvl(decode(p.fiscal_period,1,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period1,
          sum(nvl(decode(p.fiscal_period,2,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period2,
          sum(nvl(decode(p.fiscal_period,3,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period3,
          sum(nvl(decode(p.fiscal_period,4,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period4,
          sum(nvl(decode(p.fiscal_period,5,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period5,
          sum(nvl(decode(p.fiscal_period,6,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period6,
          sum(nvl(decode(p.fiscal_period,7,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period7,
          sum(nvl(decode(p.fiscal_period,8,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period8,
          sum(nvl(decode(p.fiscal_period,9,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period9,
          sum(nvl(decode(p.fiscal_period,10,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period10,
          sum(nvl(decode(p.fiscal_period,11,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period11,
          sum(nvl(decode(p.fiscal_period,12,decode(p.month_num,1,w.qty_jan,2,w.qty_feb,3,w.qty_mar,4,w.qty_apr,5,w.qty_may,6,w.qty_jun,7,w.qty_jul,8,w.qty_aug,9,w.qty_sep,10,w.qty_oct,11,w.qty_nov,12,w.qty_dec),0),0)) qty_period12
     from WO_EST_MONTHLY W, PP_TABLE_MONTHS P
    group by W.EST_MONTHLY_ID, W.WORK_ORDER_ID, W.REVISION, decode(sign(P.FISCAL_PERIOD - P.MONTH_NUM),1,W.YEAR,W.YEAR + 1)
   );


create or replace view WO_EST_MONTHLY_SV as
select W.EST_MONTHLY_ID EST_MONTHLY_ID,
       W.TIME_STAMP TIME_STAMP,
       W.USER_ID USER_ID,
       W.WORK_ORDER_ID WORK_ORDER_ID,
       W.REVISION REVISION,
       W.YEAR year,
       W.EXPENDITURE_TYPE_ID EXPENDITURE_TYPE_ID,
       W.EST_CHG_TYPE_ID EST_CHG_TYPE_ID,
       W.DEPARTMENT_ID DEPARTMENT_ID,
       NVL(W.JANUARY, 0) AMOUNT_PERIOD1,
       NVL(W.FEBRUARY, 0) AMOUNT_PERIOD2,
       NVL(W.MARCH, 0) AMOUNT_PERIOD3,
       NVL(W.APRIL, 0) AMOUNT_PERIOD4,
       NVL(W.MAY, 0) AMOUNT_PERIOD5,
       NVL(W.JUNE, 0) AMOUNT_PERIOD6,
       NVL(W.JULY, 0) AMOUNT_PERIOD7,
       NVL(W.AUGUST, 0) AMOUNT_PERIOD8,
       NVL(W.SEPTEMBER, 0) AMOUNT_PERIOD9,
       NVL(W.OCTOBER, 0) AMOUNT_PERIOD10,
       NVL(W.NOVEMBER, 0) AMOUNT_PERIOD11,
       NVL(W.DECEMBER, 0) AMOUNT_PERIOD12,
       NVL(W.TOTAL, 0) TOTAL,
       W.UTILITY_ACCOUNT_ID UTILITY_ACCOUNT_ID,
       W.LONG_DESCRIPTION LONG_DESCRIPTION,
       0 FUTURE_DOLLARS,
       0 HIST_ACTUALS,
       W.WO_WORK_ORDER_ID WO_WORK_ORDER_ID,
       W.JOB_TASK_ID JOB_TASK_ID,
       W.SUBSTITUTION_ID SUBSTITUTION_ID,
       NVL(W.HRS_JAN, 0) HRS_PERIOD1,
       NVL(W.HRS_FEB, 0) HRS_PERIOD2,
       NVL(W.HRS_MAR, 0) HRS_PERIOD3,
       NVL(W.HRS_APR, 0) HRS_PERIOD4,
       NVL(W.HRS_MAY, 0) HRS_PERIOD5,
       NVL(W.HRS_JUN, 0) HRS_PERIOD6,
       NVL(W.HRS_JUL, 0) HRS_PERIOD7,
       NVL(W.HRS_AUG, 0) HRS_PERIOD8,
       NVL(W.HRS_SEP, 0) HRS_PERIOD9,
       NVL(W.HRS_OCT, 0) HRS_PERIOD10,
       NVL(W.HRS_NOV, 0) HRS_PERIOD11,
       NVL(W.HRS_DEC, 0) HRS_PERIOD12,
       NVL(W.HRS_TOTAL, 0) HRS_TOTAL,
       NVL(W.QTY_JAN, 0) QTY_PERIOD1,
       NVL(W.QTY_FEB, 0) QTY_PERIOD2,
       NVL(W.QTY_MAR, 0) QTY_PERIOD3,
       NVL(W.QTY_APR, 0) QTY_PERIOD4,
       NVL(W.QTY_MAY, 0) QTY_PERIOD5,
       NVL(W.QTY_JUN, 0) QTY_PERIOD6,
       NVL(W.QTY_JUL, 0) QTY_PERIOD7,
       NVL(W.QTY_AUG, 0) QTY_PERIOD8,
       NVL(W.QTY_SEP, 0) QTY_PERIOD9,
       NVL(W.QTY_OCT, 0) QTY_PERIOD10,
       NVL(W.QTY_NOV, 0) QTY_PERIOD11,
       NVL(W.QTY_DEC, 0) QTY_PERIOD12,
       NVL(W.QTY_TOTAL, 0) QTY_TOTAL
  from WO_EST_MONTHLY W;


create or replace view WORK_ORDER_CONTROL_SV as
select WOC.*,
       C.GL_COMPANY_NO,
       C.OWNED,
       C.DESCRIPTION             COMPANY_DESCRIPTION,
       C.STATUS_CODE_ID,
       C.SHORT_DESCRIPTION       COMPANY_SHORT_DESCRIPTION,
       C.COMPANY_SUMMARY_ID,
       C.AUTO_LIFE_MONTH,
       C.AUTO_CURVE_MONTH,
       C.AUTO_CLOSE_WO_NUM,
       C.PARENT_COMPANY_ID,
       C.TAX_RETURN_ID,
       C.CLOSING_ROLLUP_ID,
       C.AUTO_LIFE_MONTH_MONTHLY,
       C.COMPANY_TYPE,
       C.COMPANY_EIN,
       C.CWIP_ALLOCATION_MONTHS,
       C.PROP_TAX_COMPANY_ID,
       C.POWERTAX_COMPANY_ID
  from WORK_ORDER_CONTROL WOC,
       COMPANY C,
       (select count(1) CNT_WOT from PP_WORK_ORDER_TYPE_GROUPS where ROWNUM = 1) WOT_SEC
 where WOC.COMPANY_ID = C.COMPANY_ID
   and DECODE(WOT_SEC.CNT_WOT, 0, -1, WOC.WORK_ORDER_TYPE_ID) in
       (select PPWOTG.WORK_ORDER_TYPE_ID
          from PP_SECURITY_USERS_GROUPS PPUG, PP_WORK_ORDER_TYPE_GROUPS PPWOTG
         where LOWER(user) = LOWER(PPUG.USERS)
           and PPUG.GROUPS = PPWOTG.GROUPS
        union all
        select -1 from DUAL);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (28, 0, 10, 3, 3, 0, 7986, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007986_budget_views.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
