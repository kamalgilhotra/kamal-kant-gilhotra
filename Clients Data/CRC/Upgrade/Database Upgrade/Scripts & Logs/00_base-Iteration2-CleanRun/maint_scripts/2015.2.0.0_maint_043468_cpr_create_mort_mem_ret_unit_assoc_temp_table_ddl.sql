/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043468_cpr_create_mort_mem_ret_unit_assoc_temp_table_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 09/11/2015 Charlie Shilling maint-43468
||============================================================================
*/
CREATE GLOBAL TEMPORARY TABLE mort_mem_ret_unit_assoc_temp (
	row_num						NUMBER(22,0),
	retirement_unit_id			NUMBER(22,0)
)
ON COMMIT DELETE ROWS;

ALTER TABLE mort_mem_ret_unit_assoc_temp
  ADD CONSTRAINT pk_mm_rua_temp PRIMARY KEY (
    row_num
  )
/

CREATE UNIQUE INDEX mm_rua_uix
  ON mort_mem_ret_unit_assoc_temp (
    retirement_unit_id
  )
/

COMMENT ON TABLE mort_mem_ret_unit_assoc_temp IS '(T) [01]
The MORT_MEM_RET_UNIT_ASSOC_TEMP table is used during the mortality memory rebuild process to make sure that all assets that have associated retirement units are factored up using the same factor.';

COMMENT ON COLUMN mort_mem_ret_unit_assoc_temp.row_num IS 'Identifier used during the process that builds this table.';
COMMENT ON COLUMN mort_mem_ret_unit_assoc_temp.retirement_unit_id IS 'System-assigned identifier of a retirement unit that is directly or indirectly assiated with the other in the table. ';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2864, 0, 2015, 2, 0, 0, 043468, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043468_cpr_create_mort_mem_ret_unit_assoc_temp_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;