/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011064_sys_pp_system_errors.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   07/26/2012 C. Shilling    Point Release
||============================================================================
*/

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1001',
    'POST 1001: Error: Total projected mortality not found.',
    'POST 1001: Error: Total projected mortality not found.',
    'POST 1001: Make sure mortality memory records exist for the matching assets.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1002',
    'POST 1002: Error: Cannot retire zero or a negative quantity. Asset: (...) Quantity: (...)',
    'POST 1002: Error: Cannot retire zero or a negative quantity. Asset: (...) Quantity: (...)',
    'POST 1002: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1003',
    'POST 1003: Error: Cannot retire from an asset with zero quantity.',
    'POST 1003: Error: Cannot retire from an asset with zero quantity.', 'POST 1003: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1005',
    'POST 1005: Error: Cannot retire a quantity of zero',
    'POST 1005: Error: Cannot retire a quantity of zero', 'POST 1005: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1006',
    'POST 1006: Error: The quantity of assets retired (...) does not equal the total quantity to retire (...)',
    'POST 1006: Error: The quantity of assets retired (...) does not equal the total quantity to retire (...)',
    'POST 1006: Check that the total mortality of matching assets is greater than the total quantity to retire.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1007',
    'POST 1007: Error: There are more than 100 sets_of_books pend_transaction_set_of_books.',
    'POST 1007: Error: There are more than 100 sets_of_books pend_transaction_set_of_books.',
    'POST 1007: Post is only able to process 100 sets of books. ');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1008',
    'POST 1008: Error: Could not find row in cpr_post_basis_amounts_view for pend_trans_id (...), books_schema_id  (...), and set_of_books_id  (...)',
    'POST 1008: Error: Could not find row in cpr_post_basis_amounts_view for pend_trans_id (...), books_schema_id  (...), and set_of_books_id  (...)',
    'POST 1008: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1009',
    'POST 1009: Error: Gain loss expected for set_of_books_id ..., but no gain_loss was given.',
    'POST 1009: Error: Gain loss expected for set_of_books_id ..., but no gain_loss was given.',
    'POST 1009: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1010',
    'POST 1010: Error: Could not find row in cpr_post_ledger_basis_view for pend_trans_id (...), books_schema_id (...), and set_of_books_id (...)',
    'POST 1010: Error: Could not find row in cpr_post_ledger_basis_view for pend_trans_id (...), books_schema_id (...), and set_of_books_id (...)',
    'POST 1010: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1011',
    'POST 1011: Error: Total projected mortality for matching assets is zero.',
    'POST 1011: Error: Total projected mortality for matching assets is zero.',
    'POST 1011: Make sure that mortality_memory records exist for all matching assets.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 122',
    'POST 122: Error: Depr Ledger reserve transfers in ... not equal transfers out ... for the transfer companies ',
    'POST 122: Error: Depr Ledger reserve transfers in ... not equal transfers out ... for the transfer companies ',
    'POST 122: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 205',
    'POST 205: Internal Error: asset ... ledger cost ... <> activity cost ...',
    'POST 205: Internal Error: asset ... ledger cost ... <> activity cost ...',
    'POST 205: Call PPC. The accum_cost on cpr_ledger does not equal the sum of the activity costs on cpr_activity for the asset.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 206',
    'POST 206: Internal Error: asset ... ledger quantity ... <> activity quantity ...',
    'POST 206: Internal Error: asset ... ledger quantity ... <> activity quantity ...',
    'POST 206: Call PPC. The accum_quantity on cpr_ledger does not equal the sum of the activity quantities on cpr_activity for the asset.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 207',
    'POST 207: Internal Error: ledger basis  ... <> activity basis ...',
    'POST 207: Internal Error: ledger basis  ... <> activity basis ...',
    'POST 207: Call PPC. The sum of the basis costs on cpr_ldg_basis does not equal the sum of the basis costs on cpr_act_basis for the asset.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 208',
    'POST 208: Internal Error: asset ... depr ledger  ... <> account summ ...',
    'POST 208: Internal Error: asset ... depr ledger  ... <> account summ ...',
    'POST 208: Call PPC. ');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 209',
    'POST 209: Internal Error: ledger cost ... <> ledger basis ...',
    'POST 209: Internal Error: ledger cost ... <> ledger basis ...', 'POST 209: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 210',
    'POST 210: Internal Error:   ledger cost ... <> activity cost ...',
    'POST 210: Internal Error:   ledger cost ... <> activity cost ...', 'POST 210: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 211',
    'POST 211: Internal Error:   ledger quantity ... <> activity quantity ...',
    'POST 211: Internal Error:   ledger quantity ... <> activity quantity ...', 'POST 211: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 212',
    'POST 212: Internal Error:   activity cost  ... <> activity basis ...',
    'POST 212: Internal Error:   activity cost  ... <> activity basis ...', 'POST 212: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 344a',
    'POST 344a: Error: Mortality Memory Table record missing for asset ...',
    'POST 344a: Error: Mortality Memory Table record missing for asset ...', 'POST 344a: Call PPC. ');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 345a',
    'POST 345a: Error: projected mortality is NULL or negative',
    'POST 345a: Error: projected mortality is NULL or negative', 'POST 345a: Call PPC. ');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 347a',
    'POST 347a: Error:   mortality rate is NULL ', 'POST 347a: Error:   mortality rate is NULL ',
    'POST 347a: Call PPC. ');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 356a',
    'POST 356a: ERROR  :   Remaining amount  ... = ... - ... is negative',
    'POST 356a: ERROR  :   Remaining amount  ... = ... - ... is negative', 'POST 356a: Call PPC. ');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 357a',
    'POST 357a: ERROR  :   Remaining quantity  ... = ... - ... is negative',
    'POST 357a: ERROR  :   Remaining quantity  ... = ... - ... is negative', 'POST 357a: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 358a',
    'POST 358a: Error: The posting quantity cannot make the asset quantity negative',
    'POST 358a: Error: The posting quantity cannot make the asset quantity negative',
    'POST 358a: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 359a',
    'POST 359a: Error: The posting amount cannot make the asset amount  negative',
    'POST 359a: Error: The posting amount cannot make the asset amount  negative',
    'POST 359a: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 439',
    'POST 439: ERROR : number of retirements ... must be an integer',
    'POST 439: ERROR : number of retirements ... must be an integer', 'POST 439: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 442a',
    'POST 442a: Error: Cannot transfer. wip_computation_id = NULL.',
    'POST 442a: Error: Cannot transfer. wip_computation_id = NULL.', 'POST 442a: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 442b',
    'POST 442b: Error: Cannot transfer. UTRT non_unitized gl_account = ... for WIP Comp asset does not match WIP Comp Configuration = ...',
    'POST 442b: Error: Cannot transfer. UTRT non_unitized gl_account = ... for WIP Comp asset does not match WIP Comp Configuration = ...',
    'POST 442b: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 442c',
    'POST 442c: Error: Cannot transfer. Cannot determine non_unit_gl_account for 106 WIP Comp asset.',
    'POST 442c: Error: Cannot transfer. Cannot determine non_unit_gl_account for 106 WIP Comp asset.',
    'POST 442c: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 453 - 101(A)',
    'POST 453 - 101(A): Error: CWIP 107 ... not equal to Pend Transactions Posting Amount ...',
    'POST 453 - 101(A): Error: CWIP 107 ... not equal to Pend Transactions Posting Amount ...',
    'POST 453 - 101(A): Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 453 - 101(B)',
    'POST 453 - 101(B): Error: CWIP 107 ... not equal to Pend Transactions Posting Amount ...',
    'POST 453 - 101(B): Error: CWIP 107 ... not equal to Pend Transactions Posting Amount ...',
    'POST 453 - 101(B): Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 453 - 106(A)',
    'POST 453 - 106(A): Error: CWIP 107 ... not equal to Pend Transactions Basis Amounts ...',
    'POST 453 - 106(A): Error: CWIP 107 ... not equal to Pend Transactions Basis Amounts ...',
    'POST 453 - 106(A): Call PPC. ');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 453 - 106(B)',
    'POST 453 - 106(B): Error: CWIP 107 ... not equal to Pend Transactions Posting Amount ...',
    'POST 453 - 106(B): Error: CWIP 107 ... not equal to Pend Transactions Posting Amount ...',
    'POST 453 - 106(B): Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 455a',
    'POST 455a: Error: Mass asset ... has fractional quantity ... ',
    'POST 455a: Error: Mass asset ... has fractional quantity ... ', 'POST 455a: Call PPC. ');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 500',
    'POST 500: ERROR: Cannot find the ARO book summary entry ...',
    'POST 500: ERROR: Cannot find the ARO book summary entry ...', 'POST 500: Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 512',
    'POST 512: Error: the retire method is specific and cannot find an asset id',
    'POST 512: Error: the retire method is specific and cannot find an asset id',
    'POST 512: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1012a',
    'POST 1012a: ERROR: Number of matching CPR assets (...) does not equal the number of matching assets with mortality memory (...).',
    'POST 1012a: ERROR: Number of matching CPR assets (...) does not equal the number of matching assets with mortality memory (...).',
    'POST 1012a: Make sure mortality memory is built for all assets.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1013a',
    'POST 1013a: ERROR: Total projected mortality from matching CPR assets is 0.',
    'POST 1013a: ERROR: Total projected mortality from matching CPR assets is 0.',
    'POST 1013a: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1014a',
    'POST 1014a: ERROR: The total projected mortality (...) is less than than the number to retire (...).',
    'POST 1014a: ERROR: The total projected mortality (...) is less than than the number to retire (...).',
    'POST 1014a: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1012b',
    'POST 1012b: ERROR: Number of matching CPR assets (...) does not equal the number of matching assets with mortality memory (...).',
    'POST 1012b: ERROR: Number of matching CPR assets (...) does not equal the number of matching assets with mortality memory (...).',
    'POST 1012b: Make sure mortality memory is built for all assets.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1013b',
    'POST 1013b: ERROR: Total projected mortality from matching CPR assets is 0.',
    'POST 1013b: ERROR: Total projected mortality from matching CPR assets is 0.',
    'POST 1013b: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1014b',
    'POST 1014b: ERROR: The total projected mortality (...) is less than than the number to retire (...).',
    'POST 1014b: ERROR: The total projected mortality (...) is less than than the number to retire (...).',
    'POST 1014b: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1012c',
    'POST 1012c: ERROR: Number of matching CPR assets (...) does not equal the number of matching assets with mortality memory (...).',
    'POST 1012c: ERROR: Number of matching CPR assets (...) does not equal the number of matching assets with mortality memory (...).',
    'POST 1012c: Make sure mortality memory is built for all assets.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1013c',
    'POST 1013c: ERROR: Total projected mortality from matching CPR assets is 0.',
    'POST 1013c: ERROR: Total projected mortality from matching CPR assets is 0.',
    'POST 1013c: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1014c',
    'POST 1014c: ERROR: The total projected mortality (...) is less than than the number to retire (...).',
    'POST 1014c: ERROR: The total projected mortality (...) is less than than the number to retire (...).',
    'POST 1014c: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1012d',
    'POST 1012d: ERROR: Number of matching CPR assets (...) does not equal the number of matching assets with mortality memory (...).',
    'POST 1012d: ERROR: Number of matching CPR assets (...) does not equal the number of matching assets with mortality memory (...).',
    'POST 1012d: Make sure mortality memory is built for all assets.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1013d',
    'POST 1013d: ERROR: Total projected mortality from matching CPR assets is 0.',
    'POST 1013d: ERROR: Total projected mortality from matching CPR assets is 0.',
    'POST 1013d: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1014d',
    'POST 1014d: ERROR: The total projected mortality (...) is less than than the number to retire (...).',
    'POST 1014d: ERROR: The total projected mortality (...) is less than than the number to retire (...).',
    'POST 1014d: Call PPC.');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (258, 0, 10, 4, 0, 0, 11064, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011064_sys_pp_system_errors.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
