/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029436_system_workflow.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   04/21/2013 Julia Breuer   Point Release
||============================================================================
*/

--
-- Add columns to workflow subsystem.
--
alter table PWRPLANT.WORKFLOW_SUBSYSTEM add SEND_EMAILS       number(22,0);
alter table PWRPLANT.WORKFLOW_SUBSYSTEM add CASCADE_APPROVALS number(22,0);

--
-- Add columns to workflow detail.
--
alter table PWRPLANT.WORKFLOW_DETAIL add GROUP_APPROVAL number(22,0);
alter table PWRPLANT.WORKFLOW_DETAIL add SQL_USERS      varchar2(4000);

--
-- Add columns to workflow rule.
--
alter table PWRPLANT.WORKFLOW_RULE add GROUP_APPROVAL number(22,0);

--
-- Increase the notes field on workflow detail.
--
alter table PWRPLANT.WORKFLOW_DETAIL modify NOTES varchar2(4000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (353, 0, 10, 4, 1, 0, 29436, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029436_system_workflow.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
