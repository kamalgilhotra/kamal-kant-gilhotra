/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047566_lease_02_mla_import_add_contract_currency_dml.sql
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2017.1.0.0 06/08/2017 Jared Watkins  Add the pp import column/lookup for contract currency for the
||                                      MLA import; also update existing templates to include the new field
||============================================================================
*/

insert into pp_import_lookup(import_lookup_id, description, long_description, 
       column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name)
select 2509, 'Currency.ISO Code', 'The passed in value corresponds to the Currency: ISO Code field. Translate to the Currency ID using the ISO Code column on the Currency table.',
       'contract_currency_id', '( select curr.currency_id from currency curr where upper( trim( <importfield> ) ) = upper( trim( curr.iso_code ) ) )', 0, 'currency', 'iso_code'
from dual
where not exists (select 1 
                  from pp_import_lookup 
                  where import_lookup_id = 2509 
                  and column_name = 'contract_currency_id')
;

insert into pp_import_column(import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type)
select 251, 'contract_currency_id', 'Currency Id', 'contract_currency_xlate', 1, 1, 'number(22,0)'
from dual
where not exists (select 1 
                  from pp_import_column 
                  where import_type_id = 251 
                  and column_name = 'contract_currency_id')
;

insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
select 251, 'contract_currency_id', 2509
from dual
where not exists (select 1 
                  from pp_import_column_lookup 
                  where import_type_id = 251 
                  and column_name = 'contract_currency_id' 
                  and import_lookup_id = 2509)
;

insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
select import_template_id, max(field_id) + 1, 251, 'contract_currency_id', 2509
from pp_import_template_fields a
where import_type_id = 251
      and not exists (select 1 
                      from pp_import_template_fields b 
                      where a.import_template_id = b.import_template_id 
                      and b.import_type_id = 251 
                      and b.column_name = 'contract_currency_id')
group by import_template_id
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3521, 0, 2017, 1, 0, 0, 47566, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047566_lease_02_mla_import_add_contract_currency_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;