/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043036_pcm_fp_close_auth_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/25/2015 Sarah Byers      Add menu item for w_wo_close_approval_list
||============================================================================
*/

insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values (
	'pcm', 'fp_close_auth', 'Pending Closures (FP)', 'w_wo_close_approval_list', 'Pending Funding Project Closure Authorizations', 3);

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values (
	'pcm', 'fp_close_auth', 2, 5, 'Pending Closures', 'Pending Funding Project Closure Authorizations', 'funding_projects', 'fp_close_auth', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2325, 0, 2015, 1, 0, 0, 043036, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043036_pcm_fp_close_auth_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;