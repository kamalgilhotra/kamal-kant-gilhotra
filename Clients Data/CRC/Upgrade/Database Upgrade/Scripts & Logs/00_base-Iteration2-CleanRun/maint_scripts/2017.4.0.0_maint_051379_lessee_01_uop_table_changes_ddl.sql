/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051379_lessee_01_uop_table_changes_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/29/2018 Charlie Shilling Need a new column and three new tables to support lessee UOP depr calc
||============================================================================
*/
--add DEPR_CALC_METHOD to LS_ILR_OPTIONS
ALTER TABLE ls_ilr_options
ADD depr_calc_method NUMBER(22,0);

COMMENT ON COLUMN ls_ilr_options.depr_calc_method IS '0: Straight Line/FERC, 1: Units of Production';

--create LS_ASSET_UOP
CREATE TABLE ls_asset_uop(
	ls_asset_id				NUMBER(22,0)	NOT NULL,
	gl_posting_mo_yr		DATE			NOT NULL,
	user_id					VARCHAR2(18)	NULL,
	time_stamp				DATE			NULL,
	production				NUMBER(22,0)	NOT NULL,
	estimated_production	NUMBER(22,0)	NOT NULL,
	locked					NUMBER(22,0)	NOT NULL
);

ALTER TABLE ls_asset_uop
ADD CONSTRAINT ls_asset_uop_pk PRIMARY KEY (
	ls_asset_id, gl_posting_mo_yr
) using index tablespace pwrplant_idx;

ALTER TABLE ls_asset_uop
ADD CONSTRAINT ls_asset_uop_fk1
	FOREIGN KEY (ls_asset_id)
	REFERENCES ls_asset (ls_asset_id);


COMMENT ON TABLE ls_asset_uop IS '(O) [06] The LS Asset UOP table contains asset-level parameters used in the Lessee unit of production depreciation calculation.  The Estimated Production will be decreased each month based on the prior month''s Production but can be overridden by the user.';

COMMENT ON COLUMN ls_asset_uop.ls_asset_id IS 'System-assigned identifier of a particular leased asset.';
COMMENT ON COLUMN ls_asset_uop.gl_posting_mo_yr IS 'Accounting month and year for the calculation and associated depreciation ledger record.';
COMMENT ON COLUMN ls_asset_uop.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_asset_uop.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_asset_uop.production IS 'Amount of the estimated production consumed in the current month.';
COMMENT ON COLUMN ls_asset_uop.estimated_production IS 'The Estimated Production is the total remaining production at the beginning of the month and is updated in the next month based on the production consumed.';
COMMENT ON COLUMN ls_asset_uop.locked IS '0: Unlocked, 1: Locked. Rates will be locked during the Depreciation Approval process in Month End calculations.';

--create LS_IMPORT_ASSET_UOP
CREATE TABLE ls_import_asset_uop(
	import_run_id			NUMBER(22,0)	NOT NULL,
	line_id					NUMBER(22,0)	NOT NULL,
	user_id					VARCHAR2(18)	NULL,
	time_stamp				DATE			NULL,
	ls_asset_id				NUMBER(22,0)	NULL,
	ls_asset_xlate			VARCHAR2(254)	NOT NULL,
	gl_posting_mo_yr		NUMBER(22,0)	NOT NULL,
	production				NUMBER(22,0)	NOT NULL,
	estimated_production	NUMBER(22,0)	NULL,
	rates_locked			NUMBER(22,0)	NULL,
	error_message			VARCHAR2(4000)	NULL
);

ALTER TABLE ls_import_asset_uop
ADD CONSTRAINT ls_import_asset_uop_pk PRIMARY KEY (
	import_run_id, line_id
) using index tablespace pwrplant_idx;

ALTER TABLE ls_import_asset_uop
ADD CONSTRAINT ls_import_asset_uop_fk1
	FOREIGN KEY (import_run_id)
	REFERENCES pp_import_run (import_run_id);

COMMENT ON TABLE ls_import_asset_uop IS '(S) [06] The LS Import Asset UOP table is an API table used to import Asset UOP data.';

COMMENT ON COLUMN ls_import_asset_uop.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN ls_import_asset_uop.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN ls_import_asset_uop.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_import_asset_uop.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_import_asset_uop.ls_asset_id IS 'System-assigned identifier of a particular leased asset.';
COMMENT ON COLUMN ls_import_asset_uop.ls_asset_xlate IS 'Translation field for determining the leased asset.';
COMMENT ON COLUMN ls_import_asset_uop.gl_posting_mo_yr IS 'Accounting month and year for the calculation and associated depreciation ledger record.';
COMMENT ON COLUMN ls_import_asset_uop.production IS 'Amount of the estimated production consumed in the current month.';
COMMENT ON COLUMN ls_import_asset_uop.estimated_production IS 'The Estimated Production is the total remaining production at the beginning of the month and is updated in the next month based on the production consumed.';
COMMENT ON COLUMN ls_import_asset_uop.rates_locked IS 'The Estimated Production is the total remaining production at the beginning of the month and is updated in the next month based on the production consumed.';
COMMENT ON COLUMN ls_import_asset_uop.error_message IS 'Error messages resulting from data valdiation in the import process.';


--create LS_IMPORT_ASSET_UOP_ARCHVIE
CREATE TABLE ls_import_asset_uop_archive(
	import_run_id			NUMBER(22,0)	NOT NULL,
	line_id					NUMBER(22,0)	NOT NULL,
	user_id					VARCHAR2(18)	NULL,
	time_stamp				DATE			NULL,
	ls_asset_id				NUMBER(22,0)	NULL,
	ls_asset_xlate			VARCHAR2(254)	NOT NULL,
	gl_posting_mo_yr		NUMBER(22,0)	NOT NULL,
	production				NUMBER(22,0)	NOT NULL,
	estimated_production	NUMBER(22,0)	NULL,
	rates_locked			NUMBER(22,0)	NULL,
	error_message			VARCHAR2(4000)	NULL
);

ALTER TABLE ls_import_asset_uop_archive
ADD CONSTRAINT ls_import_asset_uop_archive_pk PRIMARY KEY (
	import_run_id, line_id
) using index tablespace pwrplant_idx;

ALTER TABLE ls_import_asset_uop_archive
ADD CONSTRAINT ls_import_asset_uop_arc_fk1
	FOREIGN KEY (import_run_id)
	REFERENCES pp_import_run (import_run_id);

COMMENT ON TABLE ls_import_asset_uop_archive IS '(S) [06] The LS Import Asset UOP table is an API table used to import Asset UOP data.';

COMMENT ON COLUMN ls_import_asset_uop_archive.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN ls_import_asset_uop_archive.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN ls_import_asset_uop_archive.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_import_asset_uop_archive.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_import_asset_uop_archive.ls_asset_id IS 'System-assigned identifier of a particular leased asset.';
COMMENT ON COLUMN ls_import_asset_uop_archive.ls_asset_xlate IS 'Translation field for determining the leased asset.';
COMMENT ON COLUMN ls_import_asset_uop_archive.gl_posting_mo_yr IS 'Accounting month and year for the calculation and associated depreciation ledger record.';
COMMENT ON COLUMN ls_import_asset_uop_archive.production IS 'Amount of the estimated production consumed in the current month.';
COMMENT ON COLUMN ls_import_asset_uop_archive.estimated_production IS 'The Estimated Production is the total remaining production at the beginning of the month and is updated in the next month based on the production consumed.';
COMMENT ON COLUMN ls_import_asset_uop_archive.rates_locked IS 'The Estimated Production is the total remaining production at the beginning of the month and is updated in the next month based on the production consumed.';
COMMENT ON COLUMN ls_import_asset_uop_archive.error_message IS 'Error messages resulting from data valdiation in the import process.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6002, 0, 2017, 4, 0, 0, 51379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051379_lessee_01_uop_table_changes_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;