SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029827_lease_powerplant_tables_inserts_5.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/15/2013 Lee Quinn      Point Release
||============================================================================
*/

declare
   TABLE_DOES_NOT_EXIST exception;
   pragma exception_init(TABLE_DOES_NOT_EXIST, -00942);

   LOOPING_CHAIN_OF_SYNONYMS exception;
   pragma exception_init(LOOPING_CHAIN_OF_SYNONYMS, -01775);

   type CV_TYP is ref cursor;
   CV CV_TYP;

   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   L_COUNT             number := 1;
   LB_LEASE_BEING_USED boolean := true;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   begin
      open CV for 'select count(*) from LS_ASSET where ROWNUM < 10';
      fetch CV
         into L_COUNT;
      close CV;

   if L_COUNT = 0 then
      LB_LEASE_BEING_USED := false;
   end if;

   exception
      when TABLE_DOES_NOT_EXIST then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'LS_ASSET table Doesn''t exist so Lease Module can be refreshed.');
         LB_LEASE_BEING_USED := false;
      when LOOPING_CHAIN_OF_SYNONYMS then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Looping Chain of Synonyms - LS_ASSET table Doesn''t exist so the Lease Module can be refreshed.');
         LB_LEASE_BEING_USED := false;
   end;

   if LB_LEASE_BEING_USED then
      DBMS_OUTPUT.PUT_LINE(PPCMSG ||
                           'Lease is being used so deletes from PP_TABLE_GROUPS, PWRPLANT_TABLES and PWRPLANT_COLUMNS will not be performed.');
   else
      delete from PP_TABLE_GROUPS where TABLE_NAME in
         (select TABLE_NAME from POWERPLANT_TABLES where UPPER(TABLE_NAME) like'LS\_%'escape'\');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || sql%rowcount ||
                           ' Lease rows deleted from PP_TABLE_GROUPS.');
      delete from POWERPLANT_COLUMNS where UPPER(TABLE_NAME) like 'LS\_%' escape '\';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || sql%rowcount ||
                           ' Lease rows deleted from POWERPLANT_COLUMNS.');
      delete from POWERPLANT_TABLES where UPPER(TABLE_NAME) like 'LS\_%' escape '\';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || sql%rowcount || ' Lease rows deleted from POWERPLANT_TABLES.');
   end if;
end;
/

-- If Lease exists then comment out these inserts
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_accounting_defaults', sysdate, user, 's', 'LS Accounting Defaults', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_activity_type', sysdate, user, 'f', 'LS Activity Type', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset', sysdate, user, 'o', 'LS Asset', '<Long>', 'lease', null, 'lease', 'lease', null,
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_activity', sysdate, user, 'c', 'LS Asset Activity', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_activity_lt', sysdate, user, 'c', 'LS Asset Activity Local Taxes', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_component', sysdate, user, 'o', 'LS Asset Component', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_cpr_fields', sysdate, user, 'o', 'LS Asset CPR Fields', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_flex_control', sysdate, user, 's', 'LS Asset Flex Control', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_flex_fields', sysdate, user, 'o', 'LS Asset Flex Fields', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_flex_values',
    sysdate, user, 's',
    'LS Asset Flex values',
    '<Long>', 'lease', null, 'lease', 'lease', null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_loc_local_tax_distri', sysdate, user, 'spl*', 'LS Asset Loc Local Tax Districts',
    '<Long>', 'lease', null, 'w_lease_local_tax_maint', 'lease', null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_local_tax', sysdate, user, 's', 'LS Asset Local Tax', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_status', sysdate, user, 'f', 'LS Asset Status', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_asset_transfer_history', sysdate, user, 'c', 'LS Asset Transfer History', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_cancelable_type', sysdate, user, 'f', 'LS Cancelable Type', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_column_usage', sysdate, user, 'f', 'LS Column Usage', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_component_charge', sysdate, user, 'c', 'LS Component Charge', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_component_status', sysdate, user, 'f', 'LS Component Status', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_cost_element_valid', sysdate, user, 's', 'LS Valid Cost Elements', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_dist_def', sysdate, user, 'o', 'LS Distribution Definition', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_dist_def_control', sysdate, user, 's', 'LS Distribution Definition Control', '<Long>',
    'lease', null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_dist_def_parms', sysdate, user, 's', 'LS Distrib Definition Validation ', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_dist_def_type', sysdate, user, 's', 'LS Distribution Definition Type', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_dist_department', sysdate, user, 's', 'LS Distribution Dept', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_dist_gl_account', sysdate, user, 's', 'LS Distribution GL Account', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_dist_line', sysdate, user, 'c', 'LS Distribution Line', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_distribution_type', sysdate, user, 's', 'LS Distribution Type', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_extended_rental_type', sysdate, user, 'f', 'LS Extended Rental Type', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_gl_transaction', sysdate, user, 'c', 'LS GL Transaction', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_grandfather_level', sysdate, user, 'f', 'LS Grandfather Level', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_ilr', sysdate, user, 'o', 'LS Individual Lease Record (ILR)', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_ilr_payment_term', sysdate, user, 'o', 'LS ILR Payment Terms', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_ilr_status', sysdate, user, 'f', 'LS ILR Status', '<Long>', 'lease', null, 'lease', 'lease',
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_allocation', sysdate, user, 'c', 'LS JE Allocation', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_allocation_credits', sysdate, user, 'c', 'LS JE Allocation Credits', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_asset_activity', sysdate, user, 'c', 'LS JE Asset Activity', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_dist_def', sysdate, user, 'c', 'LS JE Dist Def', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_exchange', sysdate, user, 'c', 'LS JE Exchange', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_method', sysdate, user, 'c', 'Journal Entry Method', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_rounding', sysdate, user, 'c', 'LS JE Rounding', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_rounding2', sysdate, user, 'c', 'LS JE Rounding2', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_je_staging', sysdate, user, 'c', 'LS JE Staging', '<Long>', 'lease', null, 'lease', 'lease',
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease', sysdate, user, 'o', 'Lease', '<Long>', 'lease', null, 'lease', 'lease', null, null,
    null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_air', sysdate, user, 'o', 'Lease AIR', '<Long>', 'lease', null, 'lease', 'lease',
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_approval', sysdate, user, 'f', 'Lease Approval', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_calc_control', sysdate, user, 'c', 'Lease Calculation Control', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_calc_role', sysdate, user, 'f', 'Lease Calc Role', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_calc_role_user', sysdate, user, 'spl*', 'Lease Calc Role User', '<Long>', 'lease',
    null, 'w_lease_lease_calc_role_maint', 'lease', null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_calc_staging', sysdate, user, 'c', 'Lease Calc Staging', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_calc_status', sysdate, user, 'f', 'Lease Calculation Status', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_calc_type', sysdate, user, 'f', 'Lease Calc Type', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_calc_user_approver', sysdate, user, 'spl*', 'Lease Calc User Approvers', '<Long>',
    'lease', null, 'w_lease_lease_calc_role_maint', 'lease', null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_company', sysdate, user, 's', 'Lease Company', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_disposition_code', sysdate, user, 's', 'Lease Dispostion Codes', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_expense', sysdate, user, 'c', 'Lease Expense', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_payment_term', sysdate, user, 'o', 'Lease Payment Term', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_rule', sysdate, user, 's', 'Lease Rule', '<Long>', 'lease', null, 'lease', 'lease',
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_status', sysdate, user, 'f', 'Lease Calc Status', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_type', sysdate, user, 'f', 'Lease Type', '<Long>', 'lease', null, 'lease', 'lease',
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lease_type_tolerance', sysdate, user, 's', 'Lease Type Tolerance', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lessor', sysdate, user, 's', 'Lessor', '<Long>', 'lease', null, 'lease', 'lease', null,
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_lessor_state', sysdate, user, 's', 'Lessor State', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_local_tax', sysdate, user, 'spl*', 'LS Local Tax', '<Long>', 'lease', null,
    'w_lease_local_tax_maint', 'lease', null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_local_tax_default', sysdate, user, 'f', 'LS Local Tax Default', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_local_tax_district', sysdate, user, 'spl*', 'LS Local Tax District', '<Long>', 'lease',
    null, 'w_lease_local_tax_maint', 'lease', null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_local_tax_elig', sysdate, user, 'spl*', 'LS Local Tax Eligibility', '<Long>', 'lease', null,
    'w_lease_local_tax_maint', 'lease', null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_local_tax_rates', sysdate, user, 'o', 'LS Local Tax Rates', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_local_tax_snapshot', sysdate, user, 'c', 'Local Tax Snapshot', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_local_tax_snapshot_state', sysdate, user, 'c', 'Local Tax Snapshot State', '<Long>',
    'lease', null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_mass_change_control', sysdate, user, 'o', 'LS Mass Change Control', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_payment_freq', sysdate, user, 'f', 'LS Payment Frequency', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_payment_term_type', sysdate, user, 'f', 'LS Payment Term Type', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_post_cpr_find', sysdate, user, 'c', 'Post CPR Find', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_process_control', sysdate, user, 'o', 'LS Process Control', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_purchase_option_type', sysdate, user, 'f', 'LS Purchase Option Type', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_renewal_option_type', sysdate, user, 'f', 'LS Renewal Option Type', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_state_grandfather_date', sysdate, user, 'spl*', 'LS State Grandfather Date', '<Long>',
    'lease', null, 'w_lease_local_tax_maint', 'lease', null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_state_local_tax_rates', sysdate, user, 'o', 'LS State Local Tax Rates', '<Long>', 'lease',
    null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_summary_local_tax', sysdate, user, 'spl*', 'LS Summary Local Tax', '<Long>', 'lease', null,
    'w_lease_local_tax_maint', 'lease', null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_temp_asset', sysdate, user, 'c', 'LS Temp Asset', '<Long>', 'lease', null, 'lease', 'lease',
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_temp_component', sysdate, user, 'c', 'LS Temp Component', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_temp_ilr', sysdate, user, 'c', 'LS Temp ILR', '<Long>', 'lease', null, 'lease', 'lease',
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_temp_lease', sysdate, user, 'c', 'LS Temp Lease', '<Long>', 'lease', null, 'lease', 'lease',
    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_valid_company_po', sysdate, user, 'c', 'LS Valid Company PO', '<Long>', 'lease', null,
    'lease', 'lease', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_valid_funding_projects', sysdate, user, 's', 'Lease Valid Funding Projects', '<Long>',
    'lease', null, 'lease', 'lease', null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('ls_set_of_books', sysdate, user, 's', 'Lease Set of Books', '<Long>', 'lease', null, 'lease',
    'lease', null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (361, 0, 10, 4, 0, 0, 29827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029827_lease_powerplant_tables_inserts_5.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
