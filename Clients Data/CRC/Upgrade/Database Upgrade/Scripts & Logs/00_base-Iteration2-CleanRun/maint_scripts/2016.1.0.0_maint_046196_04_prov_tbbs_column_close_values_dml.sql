 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046196_04_prov_tbbs_column_close_values_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 09/09/2016 Jared Watkins  Harvest into base; add necessary items to
 ||					TBBS tables and remove deprecated entries
 ||============================================================================
 */ 

DELETE FROM ppbase_workspace_links WHERE LOWER(MODULE) = 'provision';

Insert into ppbase_workspace_links (MODULE,WORKSPACE_IDENTIFIER,LINK_ORDER,TIME_STAMP,USER_ID,LINKED_WORKSPACE_IDENTIFIER,LINKED_WINDOW,LINKED_DATAWINDOW,LINKED_WINDOW_LABEL,LINKED_WINDOW_MICROHELP,LINKED_DATAWINDOW_LABEL,LINKED_DATAWINDOW_MICROHELP,LINKED_WINDOW_OPENSHEET) values ('provision','tbbs_reports',1,to_date('20-JUL-16','DD-MON-RR'),'PWRPLANT','tbbs_report_display',null,null,null,null,null,null,0);

MERGE INTO tbbs_column_close A
using ( SELECT 1 AS column_id, 'book_amount' AS column_name, NULL AS line_item_id
        FROM dual
        UNION ALL
        SELECT 2, 'book_rc', NULL
        from dual
        UNION ALL
        SELECT 3, 'book_adj', NULL
        FROM dual
        union all
        SELECT 4, 'm_beg_bal_py',NULL
        FROM dual
        UNION ALL
        select 5, 'm_activity_py',NULL
        FROM dual
        UNION ALL
        SELECT 6, 'm_adjust_py', NULL
        FROM dual
        UNION ALL
        SELECT 7, 'm_variance_py', NULL
        FROM dual
        UNION ALL
        SELECT 8, 'm_beg_bal_cy', NULL
        FROM dual
        UNION ALL
        SELECT 9, 'm_activity_cy', NULL
        FROM dual
        UNION ALL
        SELECT 10, 'm_adjust_cy', NULL
        FROM dual
        UNION ALL
        SELECT 11, 'm_rta_cy', NULL
        FROM dual
        UNION ALL
        SELECT 12, 'm_variance_cy', NULL
        FROM dual
        UNION ALL
        SELECT 13, 'm_m13_py', NULL
        FROM dual
        UNION ALL
        SELECT 14, 'm_unbooked_rta', NULL
        FROM dual
        UNION ALL
        SELECT 15, 'tax_m_rc', NULL
        FROM dual
        UNION ALL
        SELECT 16, 'tax_m_adj', NULL
        FROM dual
        UNION ALL
        SELECT 17, 'm_def_tax_bal', NULL
        FROM dual
        UNION ALL
        SELECT 18, 'm_def_tax_bal_reg', NULL
        FROM dual
        UNION ALL
        SELECT 19, 'tax_def_rc', NULL
        FROM dual) b ON (a.column_id = b.column_id)
WHEN MATCHED THEN
  UPDATE SET a.column_name = b.column_name
  WHERE a.column_name <> b.column_name
WHEN NOT MATCHED THEN
  INSERT(a.column_id, a.column_name, a.line_item_id)
  VALUES(b.column_id, b.column_name, b.line_item_id);
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3285, 0, 2016, 1, 0, 0, 046196, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046196_04_prov_tbbs_column_close_values_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;