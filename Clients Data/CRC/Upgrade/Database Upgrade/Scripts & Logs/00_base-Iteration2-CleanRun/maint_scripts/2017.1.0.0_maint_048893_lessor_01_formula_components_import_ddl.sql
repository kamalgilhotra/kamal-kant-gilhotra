/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048893_lessor_01_formula_components_import_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/2/2017 Alex Healey      create the staging table(s) needed for the Lessor Index/Rate import
||============================================================================
*/
--create the staging table to hold the imported values
CREATE TABLE LSR_IMPORT_VP_INDEX_COMP_AMTS
  (
     import_run_id           NUMBER(22, 0) NOT NULL,
     line_id                 NUMBER(22, 0) NOT NULL,
     error_message           VARCHAR2(4000),
     time_stamp              DATE,
     user_id                 VARCHAR2(18),
     formula_component_id    NUMBER(22, 0),
     formula_component_xlate VARCHAR2(254),
     effective_date          VARCHAR2(254),
     amount                  VARCHAR2(254)
  );

ALTER TABLE LSR_IMPORT_VP_INDEX_COMP_AMTS
  ADD CONSTRAINT lsr_import_vp_comp_amt_pk PRIMARY KEY (import_run_id, line_id) USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE LSR_IMPORT_VP_INDEX_COMP_AMTS
  ADD CONSTRAINT lsr_import_vp_comp_amt_fk1 FOREIGN KEY (import_run_id) REFERENCES PP_IMPORT_RUN (import_run_id);

COMMENT ON TABLE LSR_IMPORT_VP_INDEX_COMP_AMTS IS '(S) [06] The LSR Import VP Comp Amts table is an API table used to import Formula Component Amounts.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.error_message IS 'Error messages resulting from data validation in the import process.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.formula_component_id IS 'The internal Formula Component id within PowerPlant.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.formula_component_xlate IS 'Translation field for determining the Formula Component.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.effective_date IS 'The date upon which the amount for the Index/Rate becomes effective.';

COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.amount IS 'The amount we are loading for the specific Formula Component.';

--create the archive staging table
CREATE TABLE LSR_IMPRT_VP_INDX_CMP_AMTS_ARC
  (
     import_run_id           NUMBER(22, 0) NOT NULL,
     line_id                 NUMBER(22, 0) NOT NULL,
     time_stamp              DATE,
     user_id                 VARCHAR2(18),
     formula_component_id    NUMBER(22, 0),
     formula_component_xlate VARCHAR2(254),
     effective_date          VARCHAR2(254),
     amount                  VARCHAR2(254)
  );

COMMENT ON TABLE LSR_IMPRT_VP_INDX_CMP_AMTS_ARC IS '(S) [06] The LSR Import VP Comp Amts Archive table holds records of previous Formula Component Amount imports.';

COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.formula_component_id IS 'The internal Formula Component id within PowerPlant.';

COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.formula_component_xlate IS 'Translation field for determining the Formula Component.';

COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.effective_date IS 'The date upon which the amount for the Index/Rate becomes effective.';

COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.amount IS 'The amount we are loading for the specific Formula Component.'; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3964, 0, 2017, 1, 0, 0, 48893, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048893_lessor_01_formula_components_import_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
