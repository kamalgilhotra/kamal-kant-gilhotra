/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030250_cwip_CWIP_CHARGE_SOB_VIEW.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   06/18/2013 Sunjin Cone     Point Release
||============================================================================
*/

create or replace view CWIP_CHARGE_SOB_VIEW
(CHARGE_ID, CHARGE_TYPE_ID, JOB_TASK_ID, EXPENDITURE_TYPE_ID, WORK_ORDER_ID, TIME_STAMP, USER_ID, STCK_KEEP_UNIT_ID,
 RETIREMENT_UNIT_ID, CHARGE_MO_YR, DESCRIPTION, QUANTITY, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, AMOUNT,SUB_ACCOUNT_ID,
 HOURS, PAYMENT_DATE, NOTES, ID_NUMBER, UNITS, LOADING_AMOUNT, REFERENCE_NUMBER, VENDOR_INFORMATION, DEPARTMENT_ID,
 CHARGE_AUDIT_ID, JOURNAL_CODE, COST_ELEMENT_ID, EXTERNAL_GL_ACCOUNT, GL_ACCOUNT_ID, STATUS, COMPANY_ID, MONTH_NUMBER,
 NON_UNITIZED_STATUS, EXCLUDE_FROM_OVERHEADS, CLOSED_MONTH_NUMBER, ORIGINAL_CURRENCY, ORIGINAL_AMOUNT, ASSET_LOCATION_ID,
 SERIAL_NUMBER, UNIT_CLOSED_MONTH_NUMBER, PO_NUMBER, TAX_ORIG_MONTH_NUMBER)
as
select CWIP_CHARGE.CHARGE_ID,
       CWIP_CHARGE.CHARGE_TYPE_ID,
       CWIP_CHARGE.JOB_TASK_ID,
       CWIP_CHARGE.EXPENDITURE_TYPE_ID,
       CWIP_CHARGE.WORK_ORDER_ID,
       CWIP_CHARGE.TIME_STAMP,
       CWIP_CHARGE.USER_ID,
       CWIP_CHARGE.STCK_KEEP_UNIT_ID,
       CWIP_CHARGE.RETIREMENT_UNIT_ID,
       CWIP_CHARGE.CHARGE_MO_YR,
       CWIP_CHARGE.DESCRIPTION,
       CWIP_CHARGE.QUANTITY,
       CWIP_CHARGE.UTILITY_ACCOUNT_ID,
       CWIP_CHARGE.BUS_SEGMENT_ID,
       CWIP_CHARGE.AMOUNT,
       CWIP_CHARGE.SUB_ACCOUNT_ID,
       CWIP_CHARGE.HOURS,
       CWIP_CHARGE.PAYMENT_DATE,
       CWIP_CHARGE.NOTES,
       CWIP_CHARGE.ID_NUMBER,
       CWIP_CHARGE.UNITS,
       CWIP_CHARGE.LOADING_AMOUNT,
       CWIP_CHARGE.REFERENCE_NUMBER,
       CWIP_CHARGE.VENDOR_INFORMATION,
       CWIP_CHARGE.DEPARTMENT_ID,
       CWIP_CHARGE.CHARGE_AUDIT_ID,
       CWIP_CHARGE.JOURNAL_CODE,
       CWIP_CHARGE.COST_ELEMENT_ID,
       CWIP_CHARGE.EXTERNAL_GL_ACCOUNT,
       CWIP_CHARGE.GL_ACCOUNT_ID,
       CWIP_CHARGE.STATUS,
       CWIP_CHARGE.COMPANY_ID,
       CWIP_CHARGE.MONTH_NUMBER,
       CWIP_CHARGE.NON_UNITIZED_STATUS,
       CWIP_CHARGE.EXCLUDE_FROM_OVERHEADS,
       CWIP_CHARGE.CLOSED_MONTH_NUMBER,
       CWIP_CHARGE.ORIGINAL_CURRENCY,
       CWIP_CHARGE.ORIGINAL_AMOUNT,
       CWIP_CHARGE.ASSET_LOCATION_ID,
       CWIP_CHARGE.SERIAL_NUMBER,
       CWIP_CHARGE.UNIT_CLOSED_MONTH_NUMBER,
       CWIP_CHARGE.PO_NUMBER,
       CWIP_CHARGE.TAX_ORIG_MONTH_NUMBER
  from CWIP_CHARGE, TEMP_SET_OF_BOOKS
 where CWIP_CHARGE.CHARGE_TYPE_ID = TEMP_SET_OF_BOOKS.CHARGE_TYPE_ID;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (464, 0, 10, 4, 1, 0, 30250, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030250_cwip_CWIP_CHARGE_SOB_VIEW.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;