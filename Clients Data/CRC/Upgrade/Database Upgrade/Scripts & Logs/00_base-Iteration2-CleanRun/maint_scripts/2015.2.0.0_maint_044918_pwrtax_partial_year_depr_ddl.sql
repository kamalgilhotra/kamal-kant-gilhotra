/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044918_pwrtax_partial_year_depr_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 09/17/2015 Sarah Byers    Partial Year Depreciation Transfers
||============================================================================
*/

-- TAX_BOOK_TRANSFERS_GRP
alter table tax_book_transfers_grp add (gl_posting_mo_yr date);
comment on column tax_book_transfers_grp.gl_posting_mo_yr is 'Book posting month and year of the transfer.';

-- TAX_TRANS_AUDIT_TRAIL_GRP 
alter table tax_trans_audit_trail_grp add (gl_posting_mo_yr date);
comment on column tax_trans_audit_trail_grp.gl_posting_mo_yr is 'Book posting month and year of the transfer.';

-- TAX_TRANSFER_CONTROL
alter table tax_transfer_control add (transfer_month date);
comment on column tax_transfer_control.transfer_month is 'The month and year of the transfer that is used for calculating partial year depreciation.';

-- TAX_TEMP_TRANSFER_CONTROL
alter table tax_temp_transfer_control add (transfer_month date);
comment on column tax_temp_transfer_control.transfer_month is 'The month and year of the transfer that is used for calculating partial year depreciation.';

-- TAX_DEPRECIATION_TRANSFER
alter table tax_depreciation_transfer add (additions number(22,2));
comment on column tax_depreciation_transfer.additions is 'Current year tax basis additions in dollars starting with the transfer month specified for partial depreciation transfers.';

alter table tax_depreciation_transfer add (calc_depreciation number(22,2));
comment on column tax_depreciation_transfer.calc_depreciation is 'Offset depreciation amount to apply to the tax records involved in the transfer.';

-- TAX_BOOK_RECONCILE_TRANSFER
alter table tax_book_reconcile_transfer add (additions number(22,2));
comment on column tax_book_reconcile_transfer.additions is 'Current year tax basis additions in dollars starting with the transfer month specified for partial depreciation transfers.';

-- TAX_DEPRECIATION
alter table tax_depreciation add (transfer_depr number(22,2));
comment on column tax_depreciation.transfer_depr is 'Offset depreciation amount to apply to the tax records involved in the transfer.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2879, 0, 2015, 2, 0, 0, 44918, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044918_pwrtax_partial_year_depr_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

