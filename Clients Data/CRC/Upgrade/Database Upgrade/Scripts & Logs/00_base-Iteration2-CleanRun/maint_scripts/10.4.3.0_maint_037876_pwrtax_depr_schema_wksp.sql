/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037876_pwrtax_depr_schema_wksp.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/14/2014 ALex P.
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_input_iface_tax_depr_schema', 'Depreciation Schema', 'uo_tax_depr_act_wksp_intfc_depr',
    'Tax Depreciation Schema');

update PPBASE_MENU_ITEMS
   set LABEL = 'Depreciation Schema', WORKSPACE_IDENTIFIER = 'depr_input_iface_tax_depr_schema'
 where MODULE = 'powertax'
   and LABEL = 'Tax Depreciation Schema';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1258, 0, 10, 4, 3, 0, 37876, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037876_pwrtax_depr_schema_wksp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

