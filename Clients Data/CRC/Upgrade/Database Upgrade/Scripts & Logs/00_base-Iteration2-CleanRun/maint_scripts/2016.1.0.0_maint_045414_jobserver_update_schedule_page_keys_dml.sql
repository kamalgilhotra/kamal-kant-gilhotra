/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045414_jobserver_update_schedule_page_keys_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 05/18/2016 Jared Watkins  Update web security keys to more code-friendly format
||============================================================================
*/

update pp_web_security_perm_control set objects = 'JobService.Workflow.Schedule.BudgetProcesses' where objects = 'IntegrationManager.Schedule.AddCloneBudgetProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Schedule.CRSystemProcesses' where objects = 'IntegrationManager.Schedule.AddCloneCRSystemProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Schedule.CustomProcesses' where objects = 'IntegrationManager.Schedule.AddCloneCustomProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Schedule.Link' where objects = 'IntegrationManager.Schedule.AddCloneJobFlowJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Schedule.MonthEndProcesses' where objects = 'IntegrationManager.Schedule.AddCloneMonthEndProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Schedule.Notification' where objects = 'IntegrationManager.Schedule.AddCloneNotification';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Schedule.SQL' where objects = 'IntegrationManager.Schedule.AddCloneSQLJobs';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Reschedule.BudgetProcesses' where objects = 'IntegrationManager.Schedule.EditBudgetProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Reschedule.CRSystemProcesses' where objects = 'IntegrationManager.Schedule.EditCRSystemProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Reschedule.CustomProcesses' where objects = 'IntegrationManager.Schedule.EditCustomProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Reschedule.Link' where objects = 'IntegrationManager.Schedule.EditJobFlowJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Reschedule.MonthEndProcesses' where objects = 'IntegrationManager.Schedule.EditMonthEndProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Reschedule.Notification' where objects = 'IntegrationManager.Schedule.EditNotification';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Reschedule.SQL' where objects = 'IntegrationManager.Schedule.EditSQLJobs';

update pp_web_security_perm_control set objects = 'JobService.Schedule.Delete.BudgetProcesses' where objects = 'IntegrationManager.Schedule.DeleteBudgetProcesses';
update pp_web_security_perm_control set objects = 'JobService.Schedule.Delete.CRSystemProcesses' where objects = 'IntegrationManager.Schedule.DeleteCRSystemProcesses';
update pp_web_security_perm_control set objects = 'JobService.Schedule.Delete.CustomProcesses' where objects = 'IntegrationManager.Schedule.DeleteCustomProcesses';
update pp_web_security_perm_control set objects = 'JobService.Schedule.Delete.Link' where objects = 'IntegrationManager.Schedule.DeleteJobFlowJobs';
update pp_web_security_perm_control set objects = 'JobService.Schedule.Delete.MonthEndProcesses' where objects = 'IntegrationManager.Schedule.DeleteMonthEndProcesses';
update pp_web_security_perm_control set objects = 'JobService.Schedule.Delete.Notification' where objects = 'IntegrationManager.Schedule.DeleteNotification';
update pp_web_security_perm_control set objects = 'JobService.Schedule.Delete.SQL' where objects = 'IntegrationManager.Schedule.DeleteSQLJobs';

update pp_web_security_permission set objects = 'JobService.Workflow.Schedule.BudgetProcesses' where objects = 'IntegrationManager.Schedule.AddCloneBudgetProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Schedule.CRSystemProcesses' where objects = 'IntegrationManager.Schedule.AddCloneCRSystemProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Schedule.CustomProcesses' where objects = 'IntegrationManager.Schedule.AddCloneCustomProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Schedule.Link' where objects = 'IntegrationManager.Schedule.AddCloneJobFlowJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Schedule.MonthEndProcesses' where objects = 'IntegrationManager.Schedule.AddCloneMonthEndProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Schedule.Notification' where objects = 'IntegrationManager.Schedule.AddCloneNotification';
update pp_web_security_permission set objects = 'JobService.Workflow.Schedule.SQL' where objects = 'IntegrationManager.Schedule.AddCloneSQLJobs';

update pp_web_security_permission set objects = 'JobService.Workflow.Reschedule.BudgetProcesses' where objects = 'IntegrationManager.Schedule.EditBudgetProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Reschedule.CRSystemProcesses' where objects = 'IntegrationManager.Schedule.EditCRSystemProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Reschedule.CustomProcesses' where objects = 'IntegrationManager.Schedule.EditCustomProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Reschedule.Link' where objects = 'IntegrationManager.Schedule.EditJobFlowJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Reschedule.MonthEndProcesses' where objects = 'IntegrationManager.Schedule.EditMonthEndProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Reschedule.Notification' where objects = 'IntegrationManager.Schedule.EditNotification';
update pp_web_security_permission set objects = 'JobService.Workflow.Reschedule.SQL' where objects = 'IntegrationManager.Schedule.EditSQLJobs';

update pp_web_security_permission set objects = 'JobService.Schedule.Delete.BudgetProcesses' where objects = 'IntegrationManager.Schedule.DeleteBudgetProcesses';
update pp_web_security_permission set objects = 'JobService.Schedule.Delete.CRSystemProcesses' where objects = 'IntegrationManager.Schedule.DeleteCRSystemProcesses';
update pp_web_security_permission set objects = 'JobService.Schedule.Delete.CustomProcesses' where objects = 'IntegrationManager.Schedule.DeleteCustomProcesses';
update pp_web_security_permission set objects = 'JobService.Schedule.Delete.Link' where objects = 'IntegrationManager.Schedule.DeleteJobFlowJobs';
update pp_web_security_permission set objects = 'JobService.Schedule.Delete.MonthEndProcesses' where objects = 'IntegrationManager.Schedule.DeleteMonthEndProcesses';
update pp_web_security_permission set objects = 'JobService.Schedule.Delete.Notification' where objects = 'IntegrationManager.Schedule.DeleteNotification';
update pp_web_security_permission set objects = 'JobService.Schedule.Delete.SQL' where objects = 'IntegrationManager.Schedule.DeleteSQLJobs';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3191, 0, 2016, 1, 0, 0, 45414, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045414_jobserver_update_schedule_page_keys_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;