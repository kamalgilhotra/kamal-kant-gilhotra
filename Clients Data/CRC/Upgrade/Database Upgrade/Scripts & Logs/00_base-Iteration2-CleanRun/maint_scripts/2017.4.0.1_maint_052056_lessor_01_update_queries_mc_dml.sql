/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052056_lessor_01_update_queries_mc_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.1 8/9/2018 	  Shane "C" Ward      Need to update include_in_select_criteria and hide_from_results columns on currency field for Lessor
||============================================================================
*/

update pp_any_query_criteria_fields 
set include_in_select_criteria = 0, hide_from_filters = 0
where lower(detail_field) = 'currency'
and id in (select id from pp_any_query_criteria 
					where description in ('Disclosure: Lease Renewal Options',
											'Disclosure: Lease Summary by ILR',
											'Disclosure: Variable Lease Payments',
											'Future Estimated Executory Receipts',
											'Future Estimated Receipts',
											'Lessor ILR by Set of Books (Current Revision)',
											'Disclosure: Cash Receipts',
											'Disclosure: Non-Lease Components',
											'Lessor Schedule by ILR'));

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9142, 0, 2017, 4, 0, 1, 52056, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052056_lessor_01_update_queries_mc_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;