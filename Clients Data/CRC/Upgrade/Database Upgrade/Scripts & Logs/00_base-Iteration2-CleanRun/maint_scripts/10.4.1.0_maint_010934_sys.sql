/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010934_sys.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Lee Quinn      Point release
||============================================================================
*/

update POWERPLANT_TABLES
   set PP_TABLE_TYPE_ID = 'o'
 where LOWER(TABLE_NAME) = 'life_analysis_parameters'
   and PP_TABLE_TYPE_ID = 's';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (432, 0, 10, 4, 1, 0, 10934, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010934_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
