/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009366_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/01/2012 Julia Breuer   Point Release
||============================================================================
*/

----
----  Add 'Active' columns to the state and county tables.
----
alter table pwrplant.state add active_yn number(22,0);
alter table pwrplant.state add ( constraint state_act_yn_fk foreign key ( active_yn ) references pwrplant.yes_no ( yes_no_id ) );

alter table pwrplant.county add active_yn number(22,0);
alter table pwrplant.county add ( constraint county_act_yn_fk foreign key ( active_yn ) references pwrplant.yes_no ( yes_no_id ) );

----
----  Update all existing rows to be marked as active.
----
update pwrplant.state set active_yn = 1 where active_yn is null;
update pwrplant.county set active_yn = 1 where active_yn is null;

----
----  Mark the column not null.
----
alter table pwrplant.state modify active_yn not null;
alter table pwrplant.county modify active_yn not null;

----
----  Update powerplant_columns.
----
insert into pwrplant.powerplant_columns (column_name, table_name, time_stamp, user_id, dropdown_name, pp_edit_type_id, description, column_rank, read_only) values ('active_yn', 'county', sysdate, user, 'yes_no', 'p', 'Active', 11, 0);
insert into pwrplant.powerplant_columns (column_name, table_name, time_stamp, user_id, dropdown_name, pp_edit_type_id, description, column_rank, read_only) values ('active_yn', 'state', sysdate, user, 'yes_no', 'p', 'Active', 8, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (101, 0, 10, 3, 4, 0, 9366, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009366_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
