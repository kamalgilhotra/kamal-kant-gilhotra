/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Web System
|| File Name:   maint_044658_websys_websecurity_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------  -----------------------------------------------
|| 2015.2     06/22/2015 Khamchanh Sisouphanh Creating new tables for Web Security
||============================= =============================================================
*/

SET serveroutput ON size 30000;

declare
	doesTableExist number := 0;
begin

	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_SECURITY_GROUPS','PWRPLANT');
	if doesTableExist = 0 then
        BEGIN
			dbms_output.put_line('Creating table PP_WEB_SECURITY_GROUPS');
			EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_WEB_SECURITY_GROUPS"							
							   (	
									"GROUPS" VARCHAR2(35 BYTE), 
									"TIME_STAMP" DATE, 
									"USER_ID" VARCHAR2(18 BYTE), 
									"DESCRIPTION" VARCHAR2(254 BYTE), 
									"CUSTOM_WEB" NUMBER(22,0)
							    ) TABLESPACE PWRPLANT';								
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_GROUPS"."GROUPS" IS ''Short group name.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_GROUPS"."TIME_STAMP" IS ''Standard System-assigned timestamp used for audit purposes.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_GROUPS"."USER_ID" IS ''Standard System-assigned user id used for audit purposes.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_GROUPS"."DESCRIPTION" IS ''Records a short description of the PP Web Security Group.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_GROUPS"."CUSTOM_WEB" IS ''Indicates whether this group is custom''';
			EXECUTE IMMEDIATE 'COMMENT ON TABLE  "PWRPLANT"."PP_WEB_SECURITY_GROUPS"  IS ''(S)  [15] The PP Web Security Groups data table contains a list of the security groups established by the company.  Individual users can be assigned to multiple groups.''';
			EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_WEB_SECURITY_GROUPS" ADD CONSTRAINT "PK_PP_WEB_SECURITY_GROUPS" PRIMARY KEY ("GROUPS")';											
		END;
	end if;

	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_SECURITY_USERS','PWRPLANT');
	if doesTableExist = 0 then
		BEGIN
			dbms_output.put_line('Creating table PP_WEB_SECURITY_USERS');
			EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_WEB_SECURITY_USERS"
							   (	"USERS" VARCHAR2(18 BYTE), 
									"TIME_STAMP" DATE, 
									"USER_ID" VARCHAR2(18 BYTE), 
									"MAIL_ID" VARCHAR2(70 BYTE), 
									"DESCRIPTION" VARCHAR2(254 BYTE), 
									"PHONE_NUMBER" CHAR(18 BYTE), 
									"CHANGE_DATE" DATE, 
									"STATUS" NUMBER(5,0), 
									"LAST_NAME" VARCHAR2(35 BYTE), 
									"FIRST_NAME" VARCHAR2(35 BYTE)
								) TABLESPACE PWRPLANT';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."USERS" IS ''User_id.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."TIME_STAMP" IS ''Standard System-assigned timestamp used for audit purposes.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."USER_ID" IS ''Standard System-assigned user id used for audit purposes.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."MAIL_ID" IS ''E-mail address of user.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."DESCRIPTION" IS ''User title and other information.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."PHONE_NUMBER" IS ''Phone number of the user.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."CHANGE_DATE" IS ''Last date that the password was changed.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."STATUS" IS ''System-assigned identifier of the users status''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."LAST_NAME" IS ''Last name of user.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS"."FIRST_NAME" IS ''First name of user.''';
			EXECUTE IMMEDIATE 'COMMENT ON TABLE  "PWRPLANT"."PP_WEB_SECURITY_GROUPS"  IS ''(S)  [15] The PP Security Users data table lists all of the users of PowerPlan for web security purposes.''';      
			--------------------------------------------------------
			--  DDL for Index PK_PP_WEB_SECURITY_USERS
			--------------------------------------------------------
			EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX "PWRPLANT"."PK_PP_WEB_SECURITY_USERS" ON "PWRPLANT"."PP_WEB_SECURITY_USERS" ("USERS")';
			--------------------------------------------------------
			--  Constraints for Table PP_WEB_SECURITY_USERS
			--------------------------------------------------------
			EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_WEB_SECURITY_USERS" ADD CONSTRAINT "PK_PP_WEB_SECURITY_USERS" PRIMARY KEY ("USERS")';
			EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_WEB_SECURITY_USERS" MODIFY ("USERS" NOT NULL ENABLE)';
		END;
	end if;

	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_SECURITY_USERS_GROUPS','PWRPLANT');
	if doesTableExist = 0 then
		BEGIN
			dbms_output.put_line('Creating table PP_WEB_SECURITY_USERS_GROUPS');
			EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_WEB_SECURITY_USERS_GROUPS"
								(	"USERS" VARCHAR2(18 BYTE), 
									"GROUPS" VARCHAR2(35 BYTE), 
									"TIME_STAMP" DATE, 
									"MAIL_ID" VARCHAR2(70 BYTE),
									CONSTRAINT PP_WEB_SECURITY_USERS_GROUPS    PRIMARY KEY    ("USERS", "GROUPS"),
									CONSTRAINT FK_PP_WEBSECURITYUSERS_GROUPSU FOREIGN KEY ("USERS") REFERENCES "PP_WEB_SECURITY_USERS" ("USERS"),
									CONSTRAINT FK_PP_WEBSECURITYUSERS_GROUPSG FOREIGN KEY ("GROUPS") REFERENCES "PP_WEB_SECURITY_GROUPS" ("GROUPS")
								) TABLESPACE PWRPLANT' ;
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS_GROUPS"."USERS" IS ''User id.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS_GROUPS"."GROUPS" IS ''Security group.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS_GROUPS"."TIME_STAMP" IS ''Standard System-assigned timestamp used for audit purposes.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_USERS_GROUPS"."MAIL_ID" IS ''Group mail id if desired.''';
			EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_WEB_SECURITY_USERS_GROUPS"  IS ''(S)  [15] The PP Security Users Groups data table associates PowerPlan users with security groups.''';

		END;
	end if;	

--End of begin
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2816, 0, 2015, 2, 0, 0, 044658, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044658_websys_websecurity_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;