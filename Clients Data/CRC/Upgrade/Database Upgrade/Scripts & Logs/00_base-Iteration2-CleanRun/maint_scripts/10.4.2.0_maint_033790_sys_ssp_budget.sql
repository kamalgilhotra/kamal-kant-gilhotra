/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033790_sys_ssp_budget.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/29/2014 Stephen Motter Point Release
||============================================================================
*/

update PP_PROCESSES
   set EXECUTABLE_FILE = 'ssp_budget.exe'
 where EXECUTABLE_FILE like 'budget_%'
   and PROCESS_ID > 50;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (929, 0, 10, 4, 2, 0, 33790, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033790_sys_ssp_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;