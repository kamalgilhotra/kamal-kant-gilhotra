/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047775_lessee_02_mc_disc_rpt_ls_not_commenced_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/7/2018  Alex Healey    Point Lessee-2013 report to use Multi Currency Time Option
||============================================================================
*/

update pp_reports
set pp_report_time_option_id = 208
where report_number = 'Lessee - 2013';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(5186, 0, 2017, 4, 0, 0, 47775, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_047775_lessee_02_mc_disc_rpt_ls_not_commenced_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;