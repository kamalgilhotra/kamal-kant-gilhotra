/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_006458_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/02/2011 Blake Andrews  Point Release
||============================================================================
*/

--###MAINT(6458)

--add Save (All) to PDF function to Reports

insert into TAX_ACCRUAL_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION)
   select 'Reporting - Save as Separate PDF Files Default',
          '1 or 0 indicates if the reports should be saved to separate PDF files or all merged into one PDF file.',
          0,
          1,
          1,
          1
     from DUAL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (40, 0, 10, 3, 3, 0, 6458, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_006458_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
