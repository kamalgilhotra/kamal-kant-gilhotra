/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045428_projects_report_time_argument_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2016.1   02/15/2016 Josh Sandler   Add report time argument to Project 3101, 3102, and 3103 reports.
||============================================================================
*/

update  PP_REPORTS
set PP_REPORT_TIME_OPTION_ID = 1
where DATAWINDOW in ('dw_functional_cwip_report', 'dw_functional_rwip_report', 'dw_functional_salvage_report')
and PP_REPORT_TIME_OPTION_ID = 0;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3063, 0, 2016, 1, 0, 0, 045428, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045428_projects_report_time_argument_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;