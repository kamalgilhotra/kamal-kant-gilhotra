/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052861_lessee_03_import_default_disc_rates_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 01/29/2019 Sarah Byers      Fix import column config issue
||============================================================================
*/

update pp_import_column
   set import_column_name = null
 where import_type_id = 271
   and column_name = 'rate';
   
--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14362, 0, 2018, 2, 0, 0, 52861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052861_lessee_03_import_default_disc_rates_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
