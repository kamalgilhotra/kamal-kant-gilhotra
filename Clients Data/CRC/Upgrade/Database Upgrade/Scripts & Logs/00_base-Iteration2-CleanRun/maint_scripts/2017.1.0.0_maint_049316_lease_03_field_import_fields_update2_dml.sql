/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049316_lease_03_field_import_fields_update2_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

insert into pp_import_column_lookup(import_type_id, column_Name, import_lookup_id)
select 
  (select import_type_id from pp_import_type where lower(import_table_name) = 'ls_import_ilr') AS import_type_id,
  'payment_term_type_id' as column_name,
  (select import_lookup_id from pp_import_lookup where lookup_table_name = 'ls_payment_term_type' and lower(description) = 'payment term type.description')
from dual;

--Needed to reorder after removing fields
update pp_import_template_fields z
set field_id = (
select the_row 
from (
select import_template_id, field_id, row_number() over(partition by a.import_template_id order by a.field_id) the_row
from pp_import_template_fields a
where a.import_type_id = 252) a
where a.import_template_id = z.import_template_id
and a.field_id = z.field_id
)
where import_type_id = 252 --hardcoded in PB to 252 for ILRs
;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4012, 0, 2017, 1, 0, 0, 49316, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049316_lease_03_field_import_fields_update2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;