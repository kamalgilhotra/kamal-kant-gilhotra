/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031459_create_api_tables.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   09/03/2013 Stephen Motter   Point Release
||============================================================================
*/

--Table 1
alter table PP_PROCESSES
   add (KICKOUT_WINDOW varchar2(254),
        KICKOUT_ARG    varchar2(254));

comment on column PP_PROCESSES.KICKOUT_WINDOW is 'The name of the window that handles kickouts for this process.';
comment on column PP_PROCESSES.KICKOUT_ARG is 'The arguments required to be passed to the kickout window.';

--Table 2
create table PP_PROCESSES_KICKOUT_CONTROL
(
 PROCESS  varchar2(100) not null,
 COMPONENT varchar2(100) not null
);

alter table PP_PROCESSES_KICKOUT_CONTROL
   add constraint PP_PROCESSES_KICKOUT_CTL_PK
       primary key (PROCESS, COMPONENT)
       using index tablespace PWRPLANT_IDX;

comment on table PP_PROCESSES_KICKOUT_CONTROL is '(S) [10] This table maps processes to components.';
comment on column PP_PROCESSES_KICKOUT_CONTROL.PROCESS is 'The name of a process.';
comment on column PP_PROCESSES_KICKOUT_CONTROL.COMPONENT is 'The name of a component.';

--Table 3
create table PP_PROCESSES_KICKOUT_TABLE
(
 KICKOUT_TABLE_ID    number(22,0) not null,
 COMPONENT           varchar2(100),
 TABLE_NAME          varchar2(64) not null,
 ARC_TABLE_NAME      varchar2(64),
 BASE_INDICATOR      number(1,0),
 DESCRIPTION         varchar2(254),
 REVIEW_WHERE_CLAUSE varchar2(254),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table PP_PROCESSES_KICKOUT_TABLE
   add constraint PP_PROCESSES_KICKOUT_TABLE_PK
       primary key (KICKOUT_TABLE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_PROCESSES_KICKOUT_TABLE
   add constraint FK_PP_PROCESSES_KICKOUT_TABLE
       foreign key (COMPONENT)
       references PP_INTEGRATION_COMPONENTS (COMPONENT);

comment on table PP_PROCESSES_KICKOUT_TABLE is '(S) [10] Stores information on the tables used to handle kickouts.';
comment on column PP_PROCESSES_KICKOUT_TABLE.KICKOUT_TABLE_ID is 'A unique identifier for each kickout table.';
comment on column PP_PROCESSES_KICKOUT_TABLE.COMPONENT is 'The component that this kickout table is responsible for.';
comment on column PP_PROCESSES_KICKOUT_TABLE.TABLE_NAME is 'The name of the table that is affected by this kickout.';
comment on column PP_PROCESSES_KICKOUT_TABLE.ARC_TABLE_NAME is 'The name of the archive table.';
comment on column PP_PROCESSES_KICKOUT_TABLE.BASE_INDICATOR is 'Specifies whether or not this table is base or custom code.';
comment on column PP_PROCESSES_KICKOUT_TABLE.DESCRIPTION is 'Describes the function of the table mentioned in the Table_Name column.';
comment on column PP_PROCESSES_KICKOUT_TABLE.REVIEW_WHERE_CLAUSE is 'Specifies which rows are shown in the kickout. (e.g. ''where error_message is not null'')';
comment on column PP_PROCESSES_KICKOUT_TABLE.TIME_STAMP is 'System assigned time stamp.';
comment on column PP_PROCESSES_KICKOUT_TABLE.USER_ID is 'System assigned user id.';

--Table 4
create table PP_PROCESSES_KICKOUT_COLUMNS
(
 KICKOUT_TABLE_ID  number(22,0) not null,
 KICKOUT_COLUMN_ID number(22,0) not null,
 COLUMN_NAME       varchar2(64) not null,
 EDIT_INDICATOR    number(1,0),
 TIME_STAMP        date,
 USER_ID           varchar2(18)
);
alter table PP_PROCESSES_KICKOUT_COLUMNS
   add constraint PP_PROCESSES_KICKOUT_COLS_PK
       primary key (KICKOUT_COLUMN_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_PROCESSES_KICKOUT_COLUMNS
   add constraint FK_PP_PROCESSES_KICKOUT_COLS
       foreign key (KICKOUT_TABLE_ID)
       references PP_PROCESSES_KICKOUT_TABLE (KICKOUT_TABLE_ID);

comment on table PP_PROCESSES_KICKOUT_COLUMNS is '(S) [10] Stores information on the columns shown in the kickout tables.';
comment on column PP_PROCESSES_KICKOUT_COLUMNS.KICKOUT_TABLE_ID is 'The unique id of the table this columns belongs to.';
comment on column PP_PROCESSES_KICKOUT_COLUMNS.KICKOUT_COLUMN_ID is 'The unique id of this column.';
comment on column PP_PROCESSES_KICKOUT_COLUMNS.COLUMN_NAME is 'The name of the column.';
comment on column PP_PROCESSES_KICKOUT_COLUMNS.EDIT_INDICATOR is 'Specifies whether or not this column is editable.';
comment on column PP_PROCESSES_KICKOUT_COLUMNS.TIME_STAMP is 'System assigned time stamp.';
comment on column PP_PROCESSES_KICKOUT_COLUMNS.USER_ID is 'System assigned user id.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (558, 0, 10, 4, 1, 0, 31459, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031459_create_api_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;