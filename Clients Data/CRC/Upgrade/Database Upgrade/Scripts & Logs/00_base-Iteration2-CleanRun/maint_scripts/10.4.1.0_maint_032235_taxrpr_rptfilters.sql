/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032235_taxrpr_rptfilters.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.1.0 09/09/2013 Andrew Scott        Reports' Filter Changes
||============================================================================
*/

update PP_REPORTS
   set PP_REPORT_FILTER_ID = 35
 where (REPORT_NUMBER like 'RPR%070' or REPORT_NUMBER like 'RPR%071' or
       REPORT_NUMBER like 'RPR%090' or REPORT_NUMBER like 'RPR%091')
   and PP_REPORT_ENVIR_ID <> 7
   and PP_REPORT_FILTER_ID = 34;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (601, 0, 10, 4, 1, 0, 32235, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032235_taxrpr_rptfilters.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
