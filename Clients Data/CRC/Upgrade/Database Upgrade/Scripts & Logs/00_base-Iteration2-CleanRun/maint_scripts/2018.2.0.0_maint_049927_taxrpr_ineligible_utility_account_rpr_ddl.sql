/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049927_taxrpr_ineligible_utility_account_rpr_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 11/10/2017 Eric Berger    DML multi-batch reporting.
||============================================================================
*/

--add the new column and constraint to repair_schema
BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_schema ADD calc_by_util_acct NUMBER(22,0) DEFAULT 0 NOT NULL';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column calc_by_util_acct already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_schema ADD CONSTRAINT rs_yn FOREIGN KEY (calc_by_util_acct) REFERENCES yes_no (yes_no_id)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint rs_yn already exists.');
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13737, 0, 2018, 2, 0, 0, 49927, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049927_taxrpr_ineligible_utility_account_rpr_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;