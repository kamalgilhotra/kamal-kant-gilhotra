/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_039297_pwrtax_archive.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/04/2014 Anand Rajashekar    Archive table updates
||========================================================================================
*/

-- Add the following columns for case Archive

alter table ARC_TAX_DEPRECIATION add TAX_LAYER_ID number(22, 0);
comment on column ARC_TAX_DEPRECIATION.TAX_LAYER_ID is 'System-assigned identifier for each Tax Layer.';

alter table ARC_TAX_RECORD_CONTROL add K1_EXPORT_ID number(22, 0);
comment on column ARC_TAX_RECORD_CONTROL.K1_EXPORT_ID is 'System-assigned identifier for each K1 export.';

alter table ARC_TAX_RECORD_CONTROL add ASSET_ID number(22, 0);
comment on column ARC_TAX_RECORD_CONTROL.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1376, 0, 10, 4, 3, 0, 39297, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039297_pwrtax_archive.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;