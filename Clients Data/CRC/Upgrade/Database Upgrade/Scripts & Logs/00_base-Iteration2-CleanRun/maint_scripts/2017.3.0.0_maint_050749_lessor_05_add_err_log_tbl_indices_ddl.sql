/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050749_lessor_05_add_err_log_tbl_indices_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/04/2018 Andrew Hill       Add indices to error logging tables
||============================================================================
*/

CREATE INDEX err$lsr_ilr_amt_numidrev_idx ON err$_lsr_ilr_amounts(to_number(ilr_id), to_number(revision)) TABLESPACE pwrplant_idx;
CREATE INDEX err$lsr_ilr_rates_numidrev_idx ON err$_lsr_ilr_rates(to_number(ilr_id), to_number(revision)) TABLESPACE pwrplant_idx;
CREATE INDEX err$lsr_ilr_sch_numidrev_idx ON err$_lsr_ilr_schedule(to_number(ilr_id), to_number(revision)) TABLESPACE pwrplant_idx;
CREATE INDEX err$lsr_ilrslssch_numidrev_idx ON err$_lsr_ilr_sch_sales_direct(to_number(ilr_id), to_number(revision)) TABLESPACE pwrplant_idx;
CREATE INDEX err$lsr_ilr_dfsch_numidrev_idx ON err$_lsr_ilr_sch_direct_fin(to_number(ilr_id), to_number(revision)) TABLESPACE pwrplant_idx;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4266, 0, 2017, 3, 0, 0, 50749, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050749_lessor_05_add_err_log_tbl_indices_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;