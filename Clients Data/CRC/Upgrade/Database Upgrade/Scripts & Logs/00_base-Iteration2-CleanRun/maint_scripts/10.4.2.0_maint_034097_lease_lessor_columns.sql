/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034097_lease_lessor_columns.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/17/2014 Kyle Peterson  Point Release
||============================================================================
*/

alter table LS_LESSOR
   modify (COUNTY_ID  char(18) null,
           STATE_ID   char(18) null,
           ZIP        number(5,0) null,
           COUNTRY_ID char(18) null);

update PP_IMPORT_COLUMN
   set IS_REQUIRED = 0
 where COLUMN_NAME in ('county_id', 'state_id', 'zip', 'country_id')
   and IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: Lessor%');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (870, 0, 10, 4, 2, 0, 34097, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034097_lease_lessor_columns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;