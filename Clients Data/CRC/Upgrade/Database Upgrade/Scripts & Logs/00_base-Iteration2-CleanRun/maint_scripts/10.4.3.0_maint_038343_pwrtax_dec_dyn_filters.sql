/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038343_pwrtax_dec_dyn_filters.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date            Revised By           Reason for Change
|| -------- --------------- -------------------  -----------------------------
|| 10.4.3.0 01/13/2014      Andrew Scott         Edited base gui dynamic filters to work if the
||                                               id field is a decimal.  Previously it was only
||                                               a long, so the filter fails on short tax years.
||============================================================================
*/

--change the tax year id datatype from a number (long) to a decimal for those
--using tax year as the id.
update PP_DYNAMIC_FILTER
   set DW_ID_DATATYPE = 'D'
 where FILTER_ID in (select FILTER_ID
                       from PP_DYNAMIC_FILTER_MAPPING
                      where PP_REPORT_FILTER_ID in (select PP_REPORT_FILTER_ID from PP_REPORTS_FILTER))
   and LOWER(LABEL) like '%tax%year%'
   and LOWER(DW_ID) = 'tax_year';

--change the comment on the column to indicate that "D" can be used for decimal ids.

----original comment:
----comment on column PP_DYNAMIC_FILTER.dw_id_datatype
----  is 'data type of dw_id column. Allowed values: "N" for numeric type, "C" for a character type.';
----new comment:
comment on column PP_DYNAMIC_FILTER.DW_ID_DATATYPE
  is 'data type of dw_id column. Allowed values: "N" for numeric (long) type, "D" for numeric (decimal) type, "C" for a character type.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1176, 0, 10, 4, 3, 0, 38343, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038343_pwrtax_dec_dyn_filters.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
