/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037351_prov.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.2.0 03/28/2014 Nathan Hollis
||=================================================================================
*/

update PP_REPORTS
   set SUBSYSTEM = 'Tax Accruala'
 where REPORT_NUMBER in ('Tax Accrual - 51037', 'Tax Accrual - 51055', 'Tax Accrual - 51060');

delete from PP_REPORTS where REPORT_NUMBER in ('Tax Accrual - 51009', 'Tax Accrual - 54518');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1074, 0, 10, 4, 2, 0, 37351, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037351_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;