/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029581_powertax_fast_tax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   03/14/2013 Roger Roach      Changed the Fast Tax to use Oracle Packages
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    USER_DISABLED, COMPANY_ID, CONTROL_TYPE)
   select NVL(max(CONTROL_ID), 0) + 1,
          'Fast Tax Depreciation PLSQL',
          'Yes',
          TO_TIMESTAMP('16-07-2008 13:26:20', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          'dw_yes_no;1',
          'If "YES" Using PLSQL Code for Calculating Tax Depreciation.  The default is "Yes"',
          null,
          -1,
          null
     from PP_SYSTEM_CONTROL_COMPANY;
insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    USER_DISABLED, COMPANY_ID, CONTROL_TYPE)
   select NVL(max(CONTROL_ID), 0) + 1,
          'Fast Tax Deferred PLSQL',
          'Yes',
          TO_TIMESTAMP('16-07-2008 13:26:20', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          'dw_yes_no;1',
          'If "YES" Using PLSQL Code for Calculating Tax Deferred.  The default is "Yes"',
          null,
          -1,
          null
     from PP_SYSTEM_CONTROL_COMPANY;
insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    USER_DISABLED, COMPANY_ID, CONTROL_TYPE)
   select NVL(max(CONTROL_ID), 0) + 1,
          'Fast Tax Forecast PLSQL',
          'Yes',
          TO_TIMESTAMP('16-07-2008 13:26:20', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          'dw_yes_no;1',
          'If "YES" Using PLSQL Code for Calculating Tax Forecast.  The default is "Yes"',
          null,
          -1,
          null
     from PP_SYSTEM_CONTROL_COMPANY;

create table TAX_JOB_LOG
(
 JOB_NO     number(22,0),
 LINE_NO    number(22,0),
 LOG_DATE   date,
 ERROR_TYPE varchar2(22) ,
 ERROR_CODE number(22,0),
 MSG        varchar2(2048)
);

alter table TAX_JOB_LOG
   add constraint PK_TAX_JOB_LOG
       primary key (JOB_NO, LINE_NO)
       using index tablespace PWRPLANT_IDX;

-- Create TABLE_LIST_ID_TYPE
create type TABLE_LIST_ID_TYPE is table of number(22, 0);
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (406, 0, 10, 4, 1, 0, 29581, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029581_pwrtax_fast_tax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
