/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045735_pwrtax_add_pr_threshold_system_option_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 06/020/2016 Charlie Shilling add system option for out of balance threshold on PR forms
||============================================================================
*/
INSERT INTO ppbase_system_options (system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
VALUES ('Plant Reconciliation Out of Balance Threshold',
	'Indicates the allowable amount of difference before the Status for a section changes to Red and highlights an out of balance. If no threshold value is provide the default will be $1. Meaning if values are within a $1 when reconciling, no out of balance will appear.',
	0, --system only option
	1, --default value = $1
	NULL, --option value = $1
	1,
	0 --company override - double check with product
);

INSERT INTO ppbase_system_options_module (system_option_id, MODULE)
VALUES ('Plant Reconciliation Out of Balance Threshold','powertax')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3225, 0, 2016, 1, 0, 0, 045735, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045735_pwrtax_add_pr_threshold_system_option_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;