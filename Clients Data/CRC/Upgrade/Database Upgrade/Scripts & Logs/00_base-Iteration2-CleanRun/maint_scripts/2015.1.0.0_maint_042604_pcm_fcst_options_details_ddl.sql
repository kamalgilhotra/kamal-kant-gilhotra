/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042604_pcm_fcst_options_details_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/02/2015 Sarah Byers      New table to support N level of details
||============================================================================
*/
-- New table
create table wo_est_fcst_options_details (
users varchar2(18) not null,
template_name varchar2(254) not null,
template_level varchar2(35) not null,
attribute_name varchar2(35) not null,
detail_level number(22,0) not null,
subtotal number(22,0) not null,
user_id varchar2(18),
time_stamp date);

-- PK
alter table wo_est_fcst_options_details add (
constraint pk_wo_est_fcst_options_details primary key (users, template_name, template_level, attribute_name, detail_level)
using index tablespace pwrplant_idx);

-- FKs
alter table wo_est_fcst_options_details
add constraint r_wo_est_fcst_options_details1
foreign key (users, template_name, template_level)
references wo_est_forecast_options (users, template_name, template_level);

alter table wo_est_fcst_options_details
add constraint r_wo_est_fcst_options_details2
foreign key (attribute_name)
references wo_est_forecast_attributes (attribute_name);

-- Comments
comment on table wo_est_fcst_options_details is '(O)  [13]The WO Est Forecast Options Details table stores the detailed attributes and level for the Funding Project forecasting window for a specific user.';
comment on column wo_est_fcst_options_details.users is 'System assigned identifier for the user assigned to the record.';
comment on column wo_est_fcst_options_details.template_name is 'The user-assigned name of the template used to identify which set of forecast options the user would like to use while updating project forecasts';
comment on column wo_est_fcst_options_details.template_level is 'Indicates the level to which this forecast template applies (FP/WO/BI/BV; that is, Funding Project, Work Order, Budget Item, Budget Version)';
comment on column wo_est_fcst_options_details.attribute_name is 'Name of the project attribute that will be displayed on the estimate workspace.';
comment on column wo_est_fcst_options_details.detail_level is 'Sort order for the project attribute that will be displayed on the estimate workspace.';
comment on column wo_est_fcst_options_details.subtotal is 'Identifies whether the attribute should be subtotaled on the estimate workspace.';
comment on column wo_est_fcst_options_details.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column wo_est_fcst_options_details.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2240, 0, 2015, 1, 0, 0, 042604, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042604_pcm_fcst_options_details_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;