/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_17_v_las_fx_vw_def_rent_ddl.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding columns for deferred rent
||========================================================================================
*/

CREATE OR REPLACE VIEW v_ls_asset_schedule_fx_vw (
  ilr_id,
  ls_asset_id,
  revision,
  set_of_books_id,
  "MONTH",
  company_id,
  open_month,
  ls_cur_type,
  exchange_date,
  prev_exchange_date,
  contract_currency_id,
  currency_id,
  rate,
  calculated_rate,
  previous_calculated_rate,
  iso_code,
  currency_display_symbol,
  residual_amount,
  term_penalty,
  bpo_price,
  beg_capital_cost,
  end_capital_cost,
  beg_obligation,
  end_obligation,
  beg_lt_obligation,
  end_lt_obligation,
  interest_accrual,
  principal_accrual,
  interest_paid,
  principal_paid,
  executory_accrual1,
  executory_accrual2,
  executory_accrual3,
  executory_accrual4,
  executory_accrual5,
  executory_accrual6,
  executory_accrual7,
  executory_accrual8,
  executory_accrual9,
  executory_accrual10,
  executory_paid1,
  executory_paid2,
  executory_paid3,
  executory_paid4,
  executory_paid5,
  executory_paid6,
  executory_paid7,
  executory_paid8,
  executory_paid9,
  executory_paid10,
  contingent_accrual1,
  contingent_accrual2,
  contingent_accrual3,
  contingent_accrual4,
  contingent_accrual5,
  contingent_accrual6,
  contingent_accrual7,
  contingent_accrual8,
  contingent_accrual9,
  contingent_accrual10,
  contingent_paid1,
  contingent_paid2,
  contingent_paid3,
  contingent_paid4,
  contingent_paid5,
  contingent_paid6,
  contingent_paid7,
  contingent_paid8,
  contingent_paid9,
  contingent_paid10,
  current_lease_cost,
  beg_deferred_rent,
  deferred_rent,
  end_deferred_rent,
  beg_st_deferred_rent,
  end_st_deferred_rent,
  gain_loss_fx,
  depr_expense,
  begin_reserve,
  end_reserve,
  depr_exp_alloc_adjust,
  asset_description,
  leased_asset_number,
  fmv,
  is_om,
  approved_revision,
  lease_cap_type_id,
  ls_asset_status_id,
  retirement_date
) AS
WITH
cur AS (
  SELECT /*+ materialize */ ls_cur_type, currency_id, currency_display_symbol, iso_code, contract_approval_rate
  FROM (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
      contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 contract_approval_rate
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id,
      company_cur.currency_display_symbol, company_cur.iso_code, NULL
    FROM currency company_cur
  )
),
open_month AS (
  SELECT /*+ materialize */ company_id, Min(gl_posting_mo_yr) open_month
  FROM ls_process_control
  WHERE open_next IS NULL
  GROUP BY company_id
),
cr AS (
  SELECT /*+ materialize */ exchange_date, currency_from, currency_to, rate
  FROM currency_rate_default a
  WHERE exchange_date = (select max(exchange_date)
                          from currency_rate_default b
                          where to_char(a.exchange_date, 'yyyymm') = to_char(b.exchange_date, 'yyyymm')
                          and a.currency_from = b.currency_from and a.currency_to = b.currency_to)
),
calc_rate AS (
  SELECT /*+ materialize */ a.company_id, a.contract_currency_id, a.company_currency_id,
    a.accounting_month, a.exchange_date, a.rate, b.rate prev_rate
  FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b
  ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = Add_Months(b.accounting_month, 1)
),
cr_now as (
  SELECT /*+ materialize */ a.exchange_date, a.currency_from, a.currency_to, a.rate
  FROM currency_rate_default a
  WHERE a.exchange_date = (
    SELECT Max(b.exchange_date)
    FROM currency_rate_default b
    WHERE a.currency_from = b.currency_from
    AND a.currency_to = b.currency_to
    AND To_Char(b.exchange_date, 'YYYYMMDD') <= To_Char(SYSDATE, 'YYYYMMDD')
  )
)
SELECT /*+ no_merge */
  las.ilr_id ilr_id,
  las.ls_asset_id ls_asset_id,
  las.revision revision,
  las.set_of_books_id set_of_books_id,
  las.MONTH MONTH,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  cr.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  las.contract_currency_id,
  cur.currency_id,
  cr.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  las.residual_amount * nvl(calc_rate.rate, cr.rate) residual_amount,
  las.term_penalty * nvl(calc_rate.rate, cr.rate) term_penalty,
  las.bpo_price * nvl(calc_rate.rate, cr.rate) bpo_price,
  las.beg_capital_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) beg_capital_cost,
  las.end_capital_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) end_capital_cost,
  las.beg_obligation * nvl(calc_rate.rate, cr.rate) beg_obligation,
  las.end_obligation * nvl(calc_rate.rate, cr.rate) end_obligation,
  las.beg_lt_obligation * nvl(calc_rate.rate, cr.rate) beg_lt_obligation,
  las.end_lt_obligation * nvl(calc_rate.rate, cr.rate) end_lt_obligation,
  las.interest_accrual * nvl(calc_rate.rate, cr.rate) interest_accrual,
  las.principal_accrual * nvl(calc_rate.rate, cr.rate) principal_accrual,
  las.interest_paid * nvl(calc_rate.rate, cr.rate) interest_paid,
  las.principal_paid * nvl(calc_rate.rate, cr.rate) principal_paid,
  las.executory_accrual1 * nvl(calc_rate.rate, cr.rate) executory_accrual1,
  las.executory_accrual2 * nvl(calc_rate.rate, cr.rate) executory_accrual2,
  las.executory_accrual3 * nvl(calc_rate.rate, cr.rate) executory_accrual3,
  las.executory_accrual4 * nvl(calc_rate.rate, cr.rate) executory_accrual4,
  las.executory_accrual5 * nvl(calc_rate.rate, cr.rate) executory_accrual5,
  las.executory_accrual6 * nvl(calc_rate.rate, cr.rate) executory_accrual6,
  las.executory_accrual7 * nvl(calc_rate.rate, cr.rate) executory_accrual7,
  las.executory_accrual8 * nvl(calc_rate.rate, cr.rate) executory_accrual8,
  las.executory_accrual9 * nvl(calc_rate.rate, cr.rate) executory_accrual9,
  las.executory_accrual10 * nvl(calc_rate.rate, cr.rate) executory_accrual10,
  las.executory_paid1 * nvl(calc_rate.rate, cr.rate) executory_paid1,
  las.executory_paid2 * nvl(calc_rate.rate, cr.rate) executory_paid2,
  las.executory_paid3 * nvl(calc_rate.rate, cr.rate) executory_paid3,
  las.executory_paid4 * nvl(calc_rate.rate, cr.rate) executory_paid4,
  las.executory_paid5 * nvl(calc_rate.rate, cr.rate) executory_paid5,
  las.executory_paid6 * nvl(calc_rate.rate, cr.rate) executory_paid6,
  las.executory_paid7 * nvl(calc_rate.rate, cr.rate) executory_paid7,
  las.executory_paid8 * nvl(calc_rate.rate, cr.rate) executory_paid8,
  las.executory_paid9 * nvl(calc_rate.rate, cr.rate) executory_paid9,
  las.executory_paid10 * nvl(calc_rate.rate, cr.rate) executory_paid10,
  las.contingent_accrual1 * nvl(calc_rate.rate, cr.rate) contingent_accrual1,
  las.contingent_accrual2 * nvl(calc_rate.rate, cr.rate) contingent_accrual2,
  las.contingent_accrual3 * nvl(calc_rate.rate, cr.rate) contingent_accrual3,
  las.contingent_accrual4 * nvl(calc_rate.rate, cr.rate) contingent_accrual4,
  las.contingent_accrual5 * nvl(calc_rate.rate, cr.rate) contingent_accrual5,
  las.contingent_accrual6 * nvl(calc_rate.rate, cr.rate) contingent_accrual6,
  las.contingent_accrual7 * nvl(calc_rate.rate, cr.rate) contingent_accrual7,
  las.contingent_accrual8 * nvl(calc_rate.rate, cr.rate) contingent_accrual8,
  las.contingent_accrual9 * nvl(calc_rate.rate, cr.rate) contingent_accrual9,
  las.contingent_accrual10 * nvl(calc_rate.rate, cr.rate) contingent_accrual10,
  las.contingent_paid1 * nvl(calc_rate.rate, cr.rate) contingent_paid1,
  las.contingent_paid2 * nvl(calc_rate.rate, cr.rate) contingent_paid2,
  las.contingent_paid3 * nvl(calc_rate.rate, cr.rate) contingent_paid3,
  las.contingent_paid4 * nvl(calc_rate.rate, cr.rate) contingent_paid4,
  las.contingent_paid5 * nvl(calc_rate.rate, cr.rate) contingent_paid5,
  las.contingent_paid6 * nvl(calc_rate.rate, cr.rate) contingent_paid6,
  las.contingent_paid7 * nvl(calc_rate.rate, cr.rate) contingent_paid7,
  las.contingent_paid8 * nvl(calc_rate.rate, cr.rate) contingent_paid8,
  las.contingent_paid9 * nvl(calc_rate.rate, cr.rate) contingent_paid9,
  las.contingent_paid10 * nvl(calc_rate.rate, cr.rate) contingent_paid10,
  las.current_lease_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) current_lease_cost,
  las.beg_deferred_rent * nvl(calc_rate.rate, cr.rate) beg_deferred_rent,
  las.deferred_rent * nvl(calc_rate.rate, cr.rate) deferred_rent,
  las.end_deferred_rent * nvl(calc_rate.rate, cr.rate) end_deferred_rent,
  las.beg_st_deferred_rent * nvl(calc_rate.rate, cr.rate) beg_st_deferred_rent,
  las.end_st_deferred_rent * nvl(calc_rate.rate, cr.rate) end_st_deferred_rent,
  decode(calc_rate.rate, NULL, 0, ( las.beg_obligation * ( calc_rate.rate - coalesce(calc_rate.prev_rate, las.in_service_exchange_rate, calc_rate.rate, 0 ) ) ) ) gain_loss_fx,
  las.depr_expense * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) depr_expense,
  las.begin_reserve * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) begin_reserve,
  las.end_reserve * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) end_reserve,
  las.depr_exp_alloc_adjust * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) depr_exp_alloc_adjust,
  las.asset_description,
  las.leased_asset_number,
  las.fmv * nvl(calc_rate.rate, cr.rate) fmv,
  las.is_om,
  las.approved_revision,
  las.lease_cap_type_id,
  las.ls_asset_status_id,
  las.retirement_date
FROM v_multicurrency_ls_asset_inner las
INNER JOIN currency_schema cs
  ON las.company_id = cs.company_id
INNER JOIN cur
  ON (cur.ls_cur_type = 1 AND cur.currency_id = las.contract_currency_id)
  OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
INNER JOIN open_month
  ON las.company_id = open_month.company_id
INNER JOIN cr
  ON cur.currency_id = cr.currency_to
  AND las.contract_currency_id = cr.currency_from
  AND cr.exchange_date < Add_Months(las.month, 1)
INNER JOIN cr_now
  ON cur.currency_id = cr_now.currency_to
  AND las.contract_currency_id = cr_now.currency_from
LEFT OUTER JOIN calc_rate
  ON las.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND las.company_id = calc_rate.company_id
  AND las.month = calc_rate.accounting_month
WHERE cr.exchange_date = (
  SELECT Max(exchange_date)
  FROM cr cr2
  WHERE cr.currency_from = cr2.currency_from
  AND cr.currency_to = cr2.currency_to
  AND cr2.exchange_date < Add_Months(las.month, 1)
)
AND cs.currency_type_id = 1
/

GRANT DELETE,INSERT,SELECT,UPDATE ON v_ls_asset_schedule_fx_vw TO pwrplant_role_dev;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4033, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_17_v_las_fx_vw_def_rent_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;