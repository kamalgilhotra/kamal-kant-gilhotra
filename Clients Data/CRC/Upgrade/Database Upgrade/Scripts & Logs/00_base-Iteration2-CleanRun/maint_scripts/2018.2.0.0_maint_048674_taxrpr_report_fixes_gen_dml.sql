/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048674_taxrpr_report_fixes_gen_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 08/01/2017 Eric Berger    Updating the datawindow values in
||                                      reporting tables to reflect the new
||                                      tax repairs standard.
||============================================================================
*/

--for report GEN - 1310:

UPDATE pp_reports
SET datawindow = 'dw_rpr_1310'
WHERE report_number = 'GEN - 1310';

--for report GEN - 1330

UPDATE pp_reports
SET datawindow = 'dw_rpr_1330'
WHERE report_number = 'GEN - 1330';

--report GEN - 1350

UPDATE pp_reports
SET datawindow = 'dw_rpr_1350'
WHERE report_number = 'GEN - 1350';

--report GEN - 1370

UPDATE pp_reports
SET datawindow = 'dw_rpr_1370'
WHERE report_number = 'GEN - 1370';

--insert into pp_reports report 1311
   insert into pp_reports
     (REPORT_ID,
      DESCRIPTION,
      LONG_DESCRIPTION,
      SUBSYSTEM,
      DATAWINDOW,
      SPECIAL_NOTE,
      REPORT_TYPE,
      TIME_OPTION,
      REPORT_NUMBER,
      INPUT_WINDOW,
      FILTER_OPTION,
      STATUS,
      PP_REPORT_SUBSYSTEM_ID,
      REPORT_TYPE_ID,
      PP_REPORT_TIME_OPTION_ID,
      PP_REPORT_FILTER_ID,
      PP_REPORT_STATUS_ID,
      PP_REPORT_ENVIR_ID,
      DOCUMENTATION,
      USER_COMMENT,
      LAST_APPROVED_DATE,
      PP_REPORT_NUMBER,
      OLD_REPORT_NUMBER,
      DYNAMIC_DW,
      TURN_OFF_MULTI_THREAD)
     SELECT (SELECT MAX(REPORT_ID)+1  FROM PP_REPORTS),
            'General/Blended-Repair Only Amounts',
            'General Method: Displays the tax repair only amount calculated for a specific batch for work orders tested using the General or Blended methods',
            null,
            'dw_rpr_1311',
            'REPAIRS',
            null,
            null,
            'GEN - 1311',
            null,
            null,
            null,
            13,
            101,
            40,
            34,
            1,
            1,
            null,
            null,
            null,
            null,
            null,
            0,
            1
       FROM DUAL
       WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_1311');

--insert into pp_report report 1312
   insert into pp_reports
     (REPORT_ID,
      DESCRIPTION,
      LONG_DESCRIPTION,
      SUBSYSTEM,
      DATAWINDOW,
      SPECIAL_NOTE,
      REPORT_TYPE,
      TIME_OPTION,
      REPORT_NUMBER,
      INPUT_WINDOW,
      FILTER_OPTION,
      STATUS,
      PP_REPORT_SUBSYSTEM_ID,
      REPORT_TYPE_ID,
      PP_REPORT_TIME_OPTION_ID,
      PP_REPORT_FILTER_ID,
      PP_REPORT_STATUS_ID,
      PP_REPORT_ENVIR_ID,
      DOCUMENTATION,
      USER_COMMENT,
      LAST_APPROVED_DATE,
      PP_REPORT_NUMBER,
      OLD_REPORT_NUMBER,
      DYNAMIC_DW,
      TURN_OFF_MULTI_THREAD)
     SELECT (SELECT MAX(REPORT_ID)+1  FROM PP_REPORTS),
            'General/Blended-Tax Only Amounts',
            'General Method: Displays the tax only amounts calculated for a specific batch for work orders tested using the General or Blended methods',
            null,
            'dw_rpr_1312',
            'REPAIRS',
            null,
            null,
            'GEN - 1312',
            null,
            null,
            null,
            13,
            101,
            40,
            34,
            1,
            1,
            null,
            null,
            null,
            null,
            null,
            0,
            1
       FROM DUAL
       WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_1312');

--insert into pp_reports report 1313
       insert into pp_reports
         (REPORT_ID,
          DESCRIPTION,
          LONG_DESCRIPTION,
          SUBSYSTEM,
          DATAWINDOW,
          SPECIAL_NOTE,
          REPORT_TYPE,
          TIME_OPTION,
          REPORT_NUMBER,
          INPUT_WINDOW,
          FILTER_OPTION,
          STATUS,
          PP_REPORT_SUBSYSTEM_ID,
          REPORT_TYPE_ID,
          PP_REPORT_TIME_OPTION_ID,
          PP_REPORT_FILTER_ID,
          PP_REPORT_STATUS_ID,
          PP_REPORT_ENVIR_ID,
          DOCUMENTATION,
          USER_COMMENT,
          LAST_APPROVED_DATE,
          PP_REPORT_NUMBER,
          OLD_REPORT_NUMBER,
          DYNAMIC_DW,
          TURN_OFF_MULTI_THREAD)
         SELECT (SELECT MAX(REPORT_ID)+1  FROM PP_REPORTS),
                'General/Blended-Retire Rev Only',
                'General Method: Displays the retirement reversal amounts calculated for a specific batch for work orders tested using the General or Blended methods',
                null,
                'dw_rpr_1313',
                'REPAIRS',
                null,
                null,
                'GEN - 1313',
                null,
                null,
                null,
                13,
                101,
                40,
                34,
                1,
                1,
                null,
                null,
                null,
                null,
                null,
                0,
                1
           FROM DUAL
           WHERE NOT EXISTS (
           SELECT 1 FROM pp_reports
           WHERE datawindow = 'dw_rpr_1313');
		   
--insert the new line into pp_reports for the 0191 report
insert into pp_reports
  (REPORT_ID,
   DESCRIPTION,
   LONG_DESCRIPTION,
   SUBSYSTEM,
   DATAWINDOW,
   SPECIAL_NOTE,
   REPORT_TYPE,
   TIME_OPTION,
   REPORT_NUMBER,
   INPUT_WINDOW,
   FILTER_OPTION,
   STATUS,
   PP_REPORT_SUBSYSTEM_ID,
   REPORT_TYPE_ID,
   PP_REPORT_TIME_OPTION_ID,
   PP_REPORT_FILTER_ID,
   PP_REPORT_STATUS_ID,
   PP_REPORT_ENVIR_ID,
   DOCUMENTATION,
   USER_COMMENT,
   LAST_APPROVED_DATE,
   PP_REPORT_NUMBER,
   OLD_REPORT_NUMBER,
   DYNAMIC_DW,
   TURN_OFF_MULTI_THREAD)
  select (select max(report_id) + 1 from pp_reports),
         'General/Blended Method Details (TY)',
         'Tax Repair testing analysis for work orders that were tested using the General or Blended methods.  Tax Repairs entries must be posted to display in this report.',
         null,
         'dw_rpr_0191',
         null,
         null,
         null,
         'RPR - 0191',
         null,
         null,
         null,
         6,
         100,
         44,
         34,
         1,
         3,
         null,
         null,
         to_date('19-12-2011 16:49:28', 'dd-mm-yyyy hh24:mi:ss'),
         null,
         null,
         0,
         1
    from dual
    WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_0191');

--insert new line into pp_reports for the 0031 report
insert into pp_reports
  (REPORT_ID,
   DESCRIPTION,
   LONG_DESCRIPTION,
   SUBSYSTEM,
   DATAWINDOW,
   SPECIAL_NOTE,
   REPORT_TYPE,
   TIME_OPTION,
   REPORT_NUMBER,
   INPUT_WINDOW,
   FILTER_OPTION,
   STATUS,
   PP_REPORT_SUBSYSTEM_ID,
   REPORT_TYPE_ID,
   PP_REPORT_TIME_OPTION_ID,
   PP_REPORT_FILTER_ID,
   PP_REPORT_STATUS_ID,
   PP_REPORT_ENVIR_ID,
   DOCUMENTATION,
   USER_COMMENT,
   LAST_APPROVED_DATE,
   PP_REPORT_NUMBER,
   OLD_REPORT_NUMBER,
   DYNAMIC_DW,
   TURN_OFF_MULTI_THREAD)
select
  (select max(report_id) + 1 from pp_reports),
   'Posted CPR Tax Repairs (Tax Year)',
   'Shows CPR tax expense and retirement reversal with a tax origination month within the selected tax year. Displays the Tax Orig Month and the accounting month. Tax Repairs entries must be posted to display in this report.',
   null,
   'dw_rpr_0031',
   'REPAIRS',
   null,
   null,
   'RPR - 0031',
  null,
   null,
   null,
   13,
   100,
   44,
   37,
   1,
   3,
   null,
   null,
   null,
   null,
   null,
   0,
   1 from dual
   WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_0031');
	   
insert into pp_reports
      (REPORT_ID,
            DESCRIPTION,
            LONG_DESCRIPTION,
            SUBSYSTEM,
            DATAWINDOW,
            SPECIAL_NOTE,
            REPORT_TYPE,
            TIME_OPTION,
            REPORT_NUMBER,
            INPUT_WINDOW,
            FILTER_OPTION,
            STATUS,
            PP_REPORT_SUBSYSTEM_ID,
            REPORT_TYPE_ID,
            PP_REPORT_TIME_OPTION_ID,
            PP_REPORT_FILTER_ID,
            PP_REPORT_STATUS_ID,
            PP_REPORT_ENVIR_ID,
            DOCUMENTATION,
            USER_COMMENT,
            LAST_APPROVED_DATE,
            PP_REPORT_NUMBER,
            OLD_REPORT_NUMBER,
            DYNAMIC_DW,
            TURN_OFF_MULTI_THREAD)
select
  (select max(report_id) + 1 from pp_reports),
            'General/Blended Method Expense (TY)',
            'Tax Repair testing status and amounts for work orders that were tested using the General or Blended methods for a tax year.  Tax Repairs entries must be posted to display in this report.',
            null,
            'dw_rpr_0201',
            null,
            null,
            null,
            'RPR - 0201',
            null,
            null,
            null,
            6,
            100,
            44,
            34,
            1,
            1,
            null,
            null,
            to_date('19-12-2011 14:56:57', 'dd-mm-yyyy hh24:mi:ss'),
            null,
            null,
            0,
            1
			from dual
   WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_0201');

insert into pp_reports
      (REPORT_ID,
            DESCRIPTION,
            LONG_DESCRIPTION,
            SUBSYSTEM,
            DATAWINDOW,
            SPECIAL_NOTE,
            REPORT_TYPE,
            TIME_OPTION,
            REPORT_NUMBER,
            INPUT_WINDOW,
            FILTER_OPTION,
            STATUS,
            PP_REPORT_SUBSYSTEM_ID,
            REPORT_TYPE_ID,
            PP_REPORT_TIME_OPTION_ID,
            PP_REPORT_FILTER_ID,
            PP_REPORT_STATUS_ID,
            PP_REPORT_ENVIR_ID,
            DOCUMENTATION,
            USER_COMMENT,
            LAST_APPROVED_DATE,
            PP_REPORT_NUMBER,
            OLD_REPORT_NUMBER,
            DYNAMIC_DW,
            TURN_OFF_MULTI_THREAD)
select
  (select max(report_id) + 1 from pp_reports),
		'UOP Run Results', 
		'UOP Tax Repairs:  Process Run Results for Non-Related Work Orders for the In Process Batch', 
		'', 
		'dw_rpr_2270', 
		'', 
		'', 
		'', 
		'UOP - 2270', 
		'', 
		'', 
		'', 
		13, 
		102, 
		204, 
		30, 
		1, 
		3, 
		'', 
		'', 
		TO_DATE('','mm-dd-yyyy'), 
		'NETWK - 0027', 
		'', 
		0, 
		NULL  
		from dual
   WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_2270');
	   
insert into pp_reports
      (REPORT_ID,
            DESCRIPTION,
            LONG_DESCRIPTION,
            SUBSYSTEM,
            DATAWINDOW,
            SPECIAL_NOTE,
            REPORT_TYPE,
            TIME_OPTION,
            REPORT_NUMBER,
            INPUT_WINDOW,
            FILTER_OPTION,
            STATUS,
            PP_REPORT_SUBSYSTEM_ID,
            REPORT_TYPE_ID,
            PP_REPORT_TIME_OPTION_ID,
            PP_REPORT_FILTER_ID,
            PP_REPORT_STATUS_ID,
            PP_REPORT_ENVIR_ID,
            DOCUMENTATION,
            USER_COMMENT,
            LAST_APPROVED_DATE,
            PP_REPORT_NUMBER,
            OLD_REPORT_NUMBER,
            DYNAMIC_DW,
            TURN_OFF_MULTI_THREAD)
select
  (select max(report_id) + 1 from pp_reports),
		  'UOP Related WO Run Results', 
		  'UOP Tax Repairs:  Process Run Results for Related Work Orders for the In Process Batch', 
		  '', 
		  'dw_rpr_2280', 
		  '', 
		  '', 
		  '', 
		  'UOP - 2280', 
		  '', 
		  '', 
		  '', 
		  13, 
		  102, 
		  204, 
		  30, 
		  1, 
		  3, 
		  '', 
		  '', 
		  TO_DATE('','mm-dd-yyyy'), 'NETWK - 0028', '', 
		  0, 
		  NULL
          from dual
   WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_2280');
	   
	   
insert into pp_reports
      (REPORT_ID,
            DESCRIPTION,
            LONG_DESCRIPTION,
            SUBSYSTEM,
            DATAWINDOW,
            SPECIAL_NOTE,
            REPORT_TYPE,
            TIME_OPTION,
            REPORT_NUMBER,
            INPUT_WINDOW,
            FILTER_OPTION,
            STATUS,
            PP_REPORT_SUBSYSTEM_ID,
            REPORT_TYPE_ID,
            PP_REPORT_TIME_OPTION_ID,
            PP_REPORT_FILTER_ID,
            PP_REPORT_STATUS_ID,
            PP_REPORT_ENVIR_ID,
            DOCUMENTATION,
            USER_COMMENT,
            LAST_APPROVED_DATE,
            PP_REPORT_NUMBER,
            OLD_REPORT_NUMBER,
            DYNAMIC_DW,
            TURN_OFF_MULTI_THREAD)			
select
  (select max(report_id) + 1 from pp_reports),
		  'UOP: Retirement Reversal Amount', 'UOP: Displays any retirement reversal dollars that have been recorded for a repair method.', 
		  '', 
		  'dw_rpr_2380', 
		  '', 
		  '', 
		  '', 
		  'UOP - 2380', 
		  '', 
		  '', 
		  '', 
		  13, 
		  102, 
		  204, 
		  30, 
		  1, 
		  3, 
		  '', 
		  '', 
		  TO_DATE('','mm-dd-yyyy'), 'NETWK - 0038', '', 
		  0, 
		  NULL
		  from dual
   WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_2380');
	   
	   
insert into pp_reports
      (REPORT_ID,
            DESCRIPTION,
            LONG_DESCRIPTION,
            SUBSYSTEM,
            DATAWINDOW,
            SPECIAL_NOTE,
            REPORT_TYPE,
            TIME_OPTION,
            REPORT_NUMBER,
            INPUT_WINDOW,
            FILTER_OPTION,
            STATUS,
            PP_REPORT_SUBSYSTEM_ID,
            REPORT_TYPE_ID,
            PP_REPORT_TIME_OPTION_ID,
            PP_REPORT_FILTER_ID,
            PP_REPORT_STATUS_ID,
            PP_REPORT_ENVIR_ID,
            DOCUMENTATION,
            USER_COMMENT,
            LAST_APPROVED_DATE,
            PP_REPORT_NUMBER,
            OLD_REPORT_NUMBER,
            DYNAMIC_DW,
            TURN_OFF_MULTI_THREAD)    
select
  (select max(report_id) + 1 from pp_reports),
        'Repairs (R): Retirmnt Reversal Amt', 
        'T' || chr(38) || 'D Network Repairs: Displays any retirement reversal dollars that have been recorded for a repair method for related work orders.', 
        '', 
        'dw_rpr_2390', 
        '', 
        '', 
        '', 
        'UOP - 2390', 
        '', 
        '', 
        '', 
        13, 
        102, 
        204, 
        30, 
        1, 
        3, 
        '', 
        '', 
        TO_DATE('','mm-dd-yyyy'), 'NETWK - 0039', 
        '', 
        0, 
        NULL
        from dual
   WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_2390');
	   


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13725, 0, 2018, 2, 0, 0, 48674, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_048674_taxrpr_report_fixes_gen_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;