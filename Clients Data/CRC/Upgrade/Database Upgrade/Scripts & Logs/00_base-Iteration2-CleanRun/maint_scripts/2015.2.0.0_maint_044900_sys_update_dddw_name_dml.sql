 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044900_sys_update_dddw_name_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2   09/09/2015 Lucy Lenhardt  Update dropdown name in datawindow
 ||============================================================================
 */ 
update powerplant_dddw set dropdown_name = 'cr_element' where dropdown_name = 'cr_elements';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2851, 0, 2015, 2, 0, 0, 044900, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044900_sys_update_dddw_name_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;