/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033912_pwrtax_ARCHIVE_TAX.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- --------------------------------------
|| 10.4.2.0 12/13/2013 Andrew Scott   MLP changes to combine tax_control,
||                                    tax_depreciation, and tax_depr_adjust
||                                    all into tax_depreciation.
||============================================================================
*/

CREATE OR REPLACE PACKAGE ARCHIVE_TAX

--  **********************************************************
--	*                                                     *
--	*   Author Roger Roach                                *
--	*   Creation Date  1/25/99                            *
--	*   Comments  Updated 12/06/13                        *
--	*                                                     *
--  ***********************************************************
-- ver 6.00 add short year
-- ver 6.01 add ex_gain_loss and ex_retire_res_impact 6/16/00
-- ver 6.02 renumbering tax record ids 7/6/00
-- ver 7.00 added support transfer tables and dynamic columns 4/11/02
-- ver 7.17 fixed a problem with Oracle 8.0 and in and out params 5/28/02
-- ver 7.23 job_status ;= status after a label gives an error
-- ver 7.25 fixed a problem with copy tax_transfer_control data to the arc_tax_transfer_control table
-- ver 7.28 fixed select problems on tax_transfer_control
-- ver 7.29 added code to turn off auditing
-- ver 7.30 a fixed job_status was 6 now is 0 before incomplete in procedure archive
-- ver 7.31 check the record count before deleting any records
-- ver 7.32 fixed the order of retrieving tax_transfer_control
-- Ver 7.33 made the exec_sql functinon a non public function
-- Ver 7.34 renumber the tax_transfer_ids on restoring the data
-- Ver 7.35 fixed column missing in tax_transfer_ids on restoring
-- Ver 7.36 check for null on max tax_transfer_id while restoring tax_transfer_control
-- Ver 7.37 added arc_tax_renumber_tids table to restore tax_tranfer tables 3/16/11
-- Ver 7.38 analyze arc_tax_renumber_id table 8/16/11
-- Ver 7.39 change write_log to user PRAGMA AUTONOMOUS_TRANSACTION, so it can commit immedaitely
-- Ver 7.40 changed the l_default length to 400 in the check_table_columns  12/7/13
-- Ver 7.41 MLP changes to combine tax_control, tax_depreciation, and tax_depr_adjust into tax_depreciation

AS
g_job_no pls_integer;
TYPE cons_struct_rec IS RECORD (
	name varchar2(40),
	cons varchar2(40),
    cols varchar2(2000),
    ref_name varchar2(40),
    ref_cons varchar2(40),
    ref_cols varchar2(2000),
    cons_type char(1));
TYPE trace_struct_rec IS RECORD (
	str varchar2(4000),
        trace_type integer,
        trace_date date);
TYPE cons_list_s IS TABLE OF cons_struct_rec INDEX BY BINARY_INTEGER;
type table_list_type is table of varchar2(40) INDEX BY BINARY_INTEGER;
type trace_list_type is table of trace_struct_rec
                index by binary_integer;
type sql_list_type is table of varchar2(2048) INDEX BY BINARY_INTEGER;

function  get_version return number;

procedure delete_case( version_number number, owner_name in varchar2, table2 in varchar2);
procedure restore(version_number number);
procedure archive( version_number number, owner_name in varchar2, table2 in varchar2);
procedure delete_archive(version_number number);
procedure delete_fcst_case( fcst_version number,version_number number);
procedure restore_fcst(fcst_version number,version_number number);
procedure archive_fcst( fcst_version number,version_number number);
procedure delete_fcst_archive(fcst_version number,version_number number) ;
function delete_table(table_name_str  varchar,owner_name varchar, where_clause varchar,err_msg out varchar) return integer;
function get_constraints(a_table_names in table_list_type, a_count in integer ,a_cons_list out cons_list_s ,
                 owner_name in varchar2 := 'PWRPLANT', a_subsystem in varchar2 := 'unknown') return integer;
function  drop_constraints(a_cons_list in cons_list_s ,  a_trace_list in out trace_list_type,
          owner_name in varchar2 := 'PWRPLANT')  return integer;
function  set_constraints(a_cons_list in cons_list_s ,  a_trace_list in out trace_list_type,
          owner_name in varchar2 := 'PWRPLANT')  return integer;
procedure trace_save(a_trace_list in out trace_list_type,a_job_no integer,a_line_no in out integer );
procedure trace_write(a_trace_list in out trace_list_type,a_str in varchar2 ,a_type integer);
procedure start_job(a_version_number in number,a_fcst_number in number,a_arc_type in varchar2 ,a_jobno  in out number ,a_logid in out number);
procedure stop_job(a_version_number in number,a_arc_type in varchar2 ,a_jobno  in  number ,a_logid in number,a_status in varchar2);
function  get_columns(a_table_name in varchar2 ,a_table_abbrev in varchar2,a_owner_name in varchar2,
           a_ex_table_list in table_list_type,a_column_str in out varchar2, a_err_msg out varchar2) return integer;
function create_colunn(a_table in varchar2 ,a_column  in  varchar2 ) return integer;
function create_table(a_table in varchar2  ) return integer;
function dupicate_table(a_from_table in varchar2,a_new_table in varchar2,a_alter_extra in varchar2,  a_err_msg in out varchar2  ) return integer;
function check_arc_table(a_table1 in varchar2,a_table2 in varchar2,a_add_sql in  varchar2) return integer;
function check_table_columns(a_table1 in varchar2,a_table2 in varchar2,a_err_msg in out varchar2) return integer;
function get_column_format(a_name varchar2, a_field varchar2, a_precision number,a_scale number,a_width number ,
        a_nullable varchar2,a_default varchar2,a_sqls in out varchar2) return integer;
function check_record_count(a_version_number in number,a_arc_type in varchar2,a_logid number  )return integer;
function get_table_list(a_tables in out table_list_type, a_arc_tables out table_list_type,a_fcst_tables in out table_list_type,
                     a_arc_fcst_tables out table_list_type,a_fcst_arc_fcst_tables out table_list_type ) return integer;
END;



/


CREATE OR REPLACE  PACKAGE BODY PWRPLANT.ARCHIVE_TAX

AS


function exec_sql (a_sql in varchar2,a_err_msg in out varchar2) return integer;
function get_version return  number
as
begin
return 7.41;

end get_version;
/*********************************************** Archive ********************************************************/

procedure archive( version_number number , owner_name in varchar2, table2 in varchar2 )
AS
	 status long(4000);
	 j int;
	 i int;
	 warnings int := 0;
	 line_no int := 0;
	 i_jobno int := 0;
     i_logid int := 0;
	 str varchar2(1024);
	 sql2 varchar2(1024);
	 version_key number(22,2);
	 code int;

     fcst_status_code int;
     fcst_version int;
     version_code int;
     INCOMPLETE_ARCHIVE int := 10;
     ARCHIVE_ERROR int := 6;
     job_status int;
     sqls varchar(8048);
     column_str varchar(8048);
     column_str2 varchar(8048);
     user_cursor integer;
     num int;
     num2 int;
     --table_names table_list_type := table_list_type('TAX_RECORD_CONTROL','TAX_DEPRECIATION','TAX_BOOK_RECONCILE',
     --    8.0 stuff                          'BASIS_AMOUNTS','DEFERRED_INCOME_TAX','TAX_ANNOTATION');
     table_names table_list_type;
     table_arc_names table_list_type;
     table_fcst_names table_list_type;
     table_fcst_arc_names table_list_type;
     table_fcst_arc_fcst_names table_list_type;
     ex_column_list table_list_type;
     err_msg varchar2(8000);
     cons_list cons_list_s;
     table_count integer := 8;
     trace_list trace_list_type;
     status_flag varchar(30);
     rtn_status int;
     cursor fcst_cur is  select tax_forecast_version_id
               from tax_forecast_version where
                      status = fcst_status_code and
                      version_id = version_code;
Begin
	--update tax_archive_status set description = 'Start Archiving' where version_id = version_number;
    version_code := version_number;
    dbms_output.put_line ('test');
    begin
	    code := audit_table_pkg.SetContext('audit','no');
	    exception
	        when others then
	          code := 0;
    end;
    code := get_table_list(table_names, table_arc_names,table_fcst_names,
                     table_fcst_arc_names,table_fcst_arc_fcst_names);

    start_job(version_number,0,'Tax',i_jobno ,i_logid );
    commit;

    select status into job_status from version WHERE version_id = version_number;
    if job_status = INCOMPLETE_ARCHIVE then
        goto incomplete_start;
    end if;

    select count(*) into num from tax_record_control where version_id = version_number;
    if num = 0 then
       goto incomplete_copy;
    end if;
    job_status := ARCHIVE_ERROR;

    str := 'Max Tax Record Control';
    SELECT min(tax_record_id) into version_key
	    FROM pwrplant.tax_record_control
	            WHERE version_id = version_number;
    if( nvl(version_key,-1) = -1 ) then
	 str := 'Version ID not found in  TAX_RECORD_CONTROL';
	 goto qquit;
	 return;
     end if;

     trace_write(trace_list,'Minimum Tax Record ID ' || to_char(version_key) ,1);
     trace_save(trace_list,i_jobno,line_no);
	 /* set status to archiving */
	 str := 'Updating version table status. ';
	 update version set status = 1 WHERE version_id = version_number;
	 if(sqlcode <> 0)   then
	     code := sqlcode;
	     str := 'Updating version table status. code = ' || sqlerrm(code) ;
	     goto qquit;
	 end if;
     /* truncate the table used to renumber tax_record_ids */
    /*truncate table arc_tax_renumber_id; */
    str := 'Truncating arc_tax_renumber_id. ';
    sqls := 'truncate table arc_tax_renumber_id';
    trace_write(trace_list,str ,1);
    trace_save(trace_list,i_jobno,line_no);
    if exec_sql(sqls,err_msg) = -1 then
           str := err_msg;
           goto qquit;
	end if;

     /* get old  tax_record_ids */
     trace_write(trace_list,'inserting arc_tax_renumber_id table' ,1);
     insert into arc_tax_renumber_id(tax_record_id_old,tax_record_id_new)
           select tax_record_id,rownum from tax_record_control
                  where version_id = version_number  ;
     if(sqlcode <> 0)   then
	     code := sqlcode;
	     str := 'inserting arc_tax_renumber_id table status. code = ' || sqlerrm(code) ;
	     goto qquit;
     end if;
     trace_save(trace_list,i_jobno,line_no);
     commit;
	 /*analyze  arc_tax_renumber_id; */
    str := 'analyze arc_tax_renumber_id. ';
	begin
        code := analyze_table('arc_tax_renumber_id',-1);
		exception
	      when others then
               str := 'error analyze arc_tax_renumber_id. code=' || to_char(code) || ' ' ||  sqlerrm(code);
	end;
    trace_write(trace_list,str ,1);
    trace_save(trace_list,i_jobno,line_no);

   for i in 1..table_names.count loop
         dbms_output.put_line (table_names(i));
         code := check_arc_table(table_names(i),table_arc_names(i),' add (version_id number(22,0))');
	 if code <> 0 then
	        str := 'Error updating columns ' ||  table_arc_names(i) || ' code =  ' || code  ;
		goto qquit;
	 end if;
	 ex_column_list.delete;
	 num := get_columns(table_names(i) ,'a',owner_name ,ex_column_list,column_str , err_msg );
	 num2 := get_columns(table_arc_names(i) ,'',owner_name ,ex_column_list,column_str2 , err_msg );
	 if num <> num2 then
	       	  str := 'Table Columns are not equal ' ||  table_arc_names(i) || ' = ' || num2 || ' std=' || num  ;
		  goto qquit;
	 end if;
   end loop;


       /****  tax reconcile *******/

    for i in 1..table_names.count loop
            str := 'Insert into Arc ' || table_names(i);
	    trace_write(trace_list,str ,1);
	    trace_save(trace_list,i_jobno,line_no);
	    if lower(table_names(i)) <> 'tax_record_control' then
	       code := check_arc_table(table_names(i),table_arc_names(i),' add column(version_id(22,0))');
	    else     --add column(status number(22,0))
	       code := check_arc_table(table_names(i),table_arc_names(i),' ');
	    end if;
	    if code <> 0 then
	        str := 'Error updating columns ' ||  table_arc_names(i) || ' code =  ' || code  ;
		goto qquit;
	    end if;
	    ex_column_list(1) := 'to_trid';
	    ex_column_list(2) := 'from_trid';
	    num := get_columns(table_names(i) ,'a',owner_name ,ex_column_list,column_str , err_msg );
	    num2 := get_columns(table_arc_names(i) ,'',owner_name ,ex_column_list,column_str2 , err_msg );
	    ex_column_list.delete;
	    if num <> num2 then
	       	     str := 'Table Columns are not equal ' ||  table_arc_names(i) || ' = ' || num2 || ' std=' || num  ;
		     goto qquit;
	    end if;

	    if lower(table_names(i)) = 'tax_transfer_control' then
	       sqls := 'insert into ' || table_arc_names(i) || '(version_id,to_trid,from_trid,' || column_str2 || ')' ||
	        ' Select ' || version_number || ', d.tax_record_id_new ,d2.tax_record_id_new, ' || column_str ||
	        ' FROM ' || table_names(i) || ' a,tax_record_control c,arc_tax_renumber_id d, ' ||
	                      '  arc_tax_renumber_id d2 ' ||
			   	' WHERE a.to_trid = d.tax_record_id_old and ' ||
	                               'a.from_trid = d2.tax_record_id_old and ' ||
	                               'a.from_trid = c.tax_record_id and ' ||
	                               'c.version_id = ' ||  version_number;
	    else
		    sqls := 'insert into ' || table_arc_names(i) || '(version_id,tax_record_id,' || column_str2 || ')' ||
		        ' Select ' || version_number || ', d.tax_record_id_new ,' || column_str ||
		        ' FROM ' || table_names(i) || ' a,tax_record_control c,arc_tax_renumber_id d ' ||
				   				' WHERE a.tax_record_id = c.tax_record_id and ' ||
		                               'c.tax_record_id = d.tax_record_id_old and ' ||
		                               'c.version_id = ' ||  version_number;
	    end if;
	    code := exec_sql(sqls,err_msg);
	    if code <> -1403 and  code <> 0  then
		  str := 'Inserting ' || table_arc_names(i) || ' table status. code = ' || code || ' msg=' || err_msg  ;
	         goto qquit;
	    end if;


    end loop;
    trace_write(trace_list,str ,1);
    trace_write(trace_list,'Start Forecast' ,1);
    trace_save(trace_list,i_jobno,line_no);
    if check_record_count(version_number,'Archive',i_logid) = -1 then
         code := sqlcode;
	 str := 'Archive and Tax Record are not equal code = ' || sqlerrm(code) ;
         goto qquit;
    end if;

    <<incomplete_copy>>
    -- Delete Forecast versions
    fcst_status_code := 13;   -- Deleting
    open fcst_cur;
    loop
	   fetch fcst_cur into fcst_version;
	   if(fcst_cur%NOTFOUND) then
	        trace_write(trace_list,'No Forecast' ,1);
	        exit;
	    end if;

	    for i in 1..table_fcst_names.count loop
	        trace_write(trace_list,'delete ' || table_fcst_names(i) ,1);
	        sqls := 'delete ' || table_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
	        if exec_sql(sqls,err_msg) <> 0 then
		    str := 'deleting tax_forecast_output  code = ' || err_msg  ||  '  fcst_version' || to_char(fcst_version);
		    goto qquit;
		end if;
	    end loop;
        delete tax_forecast_version where tax_forecast_version_id = fcst_version;
        code := sqlcode;
        if code <> 0 then
	          str := 'deleting tax_forecast_version  code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	end if;
    end loop;
    close fcst_cur;
     -- Delete Archive Forecast versions
    trace_write(trace_list,'Start Del  Forecast' ,1);
    trace_save(trace_list,i_jobno,line_no);
    fcst_status_code := 14;    -- Deleting Forecast Archive
    open fcst_cur;
	loop
	   fetch fcst_cur into fcst_version;
	   if(fcst_cur%NOTFOUND) then
	        trace_write(trace_list,'No Del Archive Forecast' ,1);
	        exit;
	    end if;
	    trace_write(trace_list,'delete arc_tax_forecast_output' ,1);
	    delete arc_fcst_tax_forecast_output where tax_forecast_version_id = fcst_version;
	    code := sqlcode;
	    if code <> 0 then
		          str := 'deleting arc_fcst_tax_forecast_output  code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
		     	  goto qquit;
             end if;
             trace_write(trace_list,'arc_fcst_tax_forecast_input' ,1);
	     delete arc_fcst_tax_forecast_input where tax_forecast_version_id = fcst_version;
	     code := sqlcode;
	     if code <> 0 then
		          str := 'deleting arc_fcst_tax_forecast_input  code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
		     	  goto qquit;
		     end if;
		    trace_write(trace_list,'arc_fcst_dfit_forecast_output' ,1);
	      delete arc_fcst_dfit_forecast_output where tax_forecast_version_id = fcst_version;
	      code := sqlcode;
	      if code <> 0 then
		          str := 'deleting arc_fcst_dfit_forecast_output  code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
		     	  goto qquit;
	        end if;
		trace_write(trace_list,'delete tax_forecast_version' ,1);
	        delete tax_forecast_version where tax_forecast_version_id = fcst_version;
	        code := sqlcode;
	        if code <> 0 then
		          str := 'deleting tax_forecast_version  code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
		     	  goto qquit;
	     end if;
	end loop;
    close fcst_cur;
-- Archive Tax Forecast versions
     trace_write(trace_list,'Start Archive  Forecast' ,1);
    trace_save(trace_list,i_jobno,line_no);
    fcst_status_code := 1;  -- Archive Tax
    open fcst_cur;
	loop
	   fetch fcst_cur into fcst_version;
	   if(fcst_cur%NOTFOUND) then
	         trace_write(trace_list,'No Archive  Forecast' ,1);
	        exit;
	    end if;
	   for i in 1..table_fcst_names.count loop
	        num := get_columns(table_fcst_names(i) ,'a',owner_name ,ex_column_list,column_str , err_msg );
	        num2 := get_columns(table_fcst_arc_names(i) ,'',owner_name ,ex_column_list,column_str2 , err_msg );
	        code := check_arc_table(table_fcst_names(i),table_fcst_arc_names(i),' ');
		if code <> 0 then
		        str := 'Error updating columns ' ||  table_fcst_arc_names(i) || ' code =  ' || code  ;
			goto qquit;
	        end if;


	        sqls := 'insert into ' || table_fcst_arc_names(i) || '(tax_record_id,' || column_str2 || ')' ||
	           ' Select d.tax_record_id_new ,' || column_str ||
	           ' from '|| table_fcst_names(i) || ' a,arc_tax_renumber_id d' ||
			 	 ' WHERE  tax_forecast_version_id = ' || fcst_version || ' and ' ||
	                         ' tax_record_id = tax_record_id_old ';

	        if exec_sql(sqls,err_msg) <> 0 then
			    str := 'Inserting ' || table_fcst_names(i) || '  code = ' || err_msg  ||  '  fcst_version ' || to_char(fcst_version);
			    goto qquit;
		end if;
	        trace_write(trace_list,'insert into  '|| table_fcst_names(i) ,1);
	        sqls := 'delete '|| table_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
	        if exec_sql(sqls,err_msg) <> 0 then
		          str := 'deleting '|| table_fcst_names(i) || '  code = ' || err_msg  ||  '  fcst_version ' || to_char(fcst_version);
		     	  goto qquit;
		end if;
	end loop;
        update tax_forecast_version set status =  2 where tax_forecast_version_id = fcst_version;
        code := sqlcode;
        if code <> 0 then
	          str := 'updating tax_forecast_version code = ' || sqlerrm(code)  ||  '  fcst_version ' || to_char(fcst_version);
	     	  goto qquit;
	     end if;
    end loop;
    close fcst_cur;
    trace_write(trace_list,'End Archive  Forecast' ,1);
    trace_write(trace_list,'Start Del  Archive  Forecast' ,1);
    trace_save(trace_list,i_jobno,line_no);
 -- Archive Fcst to  Tax Forecast versions
    fcst_status_code := 19;  -- Archive Tax
    open fcst_cur;
	loop
	   fetch fcst_cur into fcst_version;
	   if(fcst_cur%NOTFOUND) then
	        trace_write(trace_list,'No Del  Archive  Forecast' ,1);
	        exit;
	   end if;

           for i in 1..table_fcst_names.count loop
                trace_write(trace_list,'insert into ' || table_fcst_arc_fcst_names(i)  ,1);
                num := get_columns(table_fcst_arc_fcst_names(i) ,'a',owner_name ,ex_column_list,column_str , err_msg );
	        num2 := get_columns(table_fcst_arc_names(i) ,'',owner_name ,ex_column_list,column_str2 , err_msg );
	        if num <> num2 then
	       	     str := 'Table Columns are not equal ' || table_fcst_arc_fcst_names(i) || ' arc = ' || num || ' std=' || num2  ;
		     goto qquit;
	        end if;

	        sqls := 'insert into ' || table_fcst_arc_names(i) || '(tax_record_id,' || column_str2 || ')' ||
	           ' Select d.tax_record_id_new, ' || column_str ||
	           ' from ' || table_fcst_arc_fcst_names(i) || ' a, arc_tax_renumber_id d' ||
			 	 ' WHERE  tax_forecast_version_id = ' || fcst_version || ' and ' ||
	                         ' tax_record_id = tax_record_id_old ';
	        if exec_sql(sqls,err_msg) = -1 then
		    str := 'Inserting ' || table_fcst_arc_names(i) ||' status. code = ' || err_msg  ;
	            goto qquit;
	        end if;



		trace_write(trace_list,'delete ' || table_fcst_arc_names(i) ,1);
	        sqls := 'delete ' || table_fcst_arc_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
	        if exec_sql(sqls,err_msg) = -1 then
	             str := 'deleting ' || table_fcst_arc_names(i) ||' status. code = ' || err_msg  ;
	            goto qquit;
	        end if;
        end loop;
        update tax_forecast_version set status =  2 where tax_forecast_version_id = fcst_version;
        code := sqlcode;
        if code <> 0 then
	          str := 'updating tax_forecast_version code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	     end if;
	end loop;
    close fcst_cur;
   trace_write(trace_list,'End Del  Archive  Forecast' ,1);
   trace_save(trace_list,i_jobno,line_no);
--    commit;
 --- The Last thing to do is remove the data from table
          /* Get Table Constraints */
         job_status := 1;
  <<incomplete_start>>
   --     job_status := INCOMPLETE_ARCHIVE;
        trace_write(trace_list,'Update Version ' ,1);
        trace_save(trace_list,i_jobno,line_no);
        update version set status = job_status where version_id = version_number;
        trace_save(trace_list,i_jobno,line_no);
        trace_write(trace_list,'Update Forecast Version ' ,1);
	update tax_forecast_version set status =  0 where version_id = version_number and
		       ( status = 1 or status = 13);
        trace_write(trace_list,'Update Forecast2 Version ' ,1);
        trace_save(trace_list,i_jobno,line_no);
        job_status := INCOMPLETE_ARCHIVE;
	update tax_forecast_version set status =  11 where version_id = version_number and
		       ( status = 19 or status = 14);
		commit;
	trace_write(trace_list,'Start Getting Constraints ' ,1);
    	trace_save(trace_list,i_jobno,line_no);
	rtn_status := get_constraints(table_names, table_count ,cons_list , owner_name , 'Tax');
	if cons_list.count > 0 then
		    for i in 1..cons_list.count loop
		             trace_write(trace_list,'Cons ' || cons_list(i).name || ' ' || cons_list(i).cons || ' ' ||
		                                     cons_list(i).ref_name || ' ' || cons_list(i).ref_cons || ' ' ||
		                                     cons_list(i).cons_type || ' ' || cons_list(i).cols   || ' ' ||
		                                           cons_list(i).ref_cols ,4);
		    end loop;
	    end if;
	    trace_write(trace_list,'End Getting Constraints ' ,1);
	    trace_save(trace_list,i_jobno,line_no);
	    if rtn_status <> 0 then
	       err_msg := 'Error getting Constraints ' || sqlerrm(code) ;
	       goto qquit;
	    end if;
	    warnings := warnings + drop_constraints(cons_list,trace_list, owner_name );

	    for i in 1..table_names.count loop
	          if  lower(table_names(i)) = 'tax_transfer_control'   then
		       sql2 := '  to_trid in (select tax_record_id from tax_record_control where '
		              || ' version_id = ' || to_char(version_number) || ')';
		  else
		       sql2 := '  tax_record_id in (select tax_record_id from tax_record_control where '
		                  || ' version_id = ' || to_char(version_number) || ')';
		  end if;
	          str := 'delete from ' || owner_name || '.' || table_names(i) || ' where ' || sql2;
	          trace_write(trace_list,str ,1);
	          if exec_sql(str,err_msg) = -1 then
	               -- data has copy so mark this as a warning
		           str := err_msg;
		           trace_write(trace_list,str ,3);

		           update tax_forecast_version set status =  2 where version_id = version_number and
				       ( status = 1 or status = 13);
				   update tax_forecast_version set status =  2 where version_id = version_number and
				       ( status = 19 or status = 14);
				   update tax_archive_status set  description = 'Finish'  where  version_id = version_number;
			err_msg := str;
	       		goto qquit;
		  end if;
	    end loop;
	    warnings := warnings + set_constraints(cons_list,trace_list, owner_name );

	str := 'Update Version ';
	update version set status = 2 where version_id = version_number;
	if(sqlcode <> 0)   then
	     code := sqlcode;
	     str := 'Updating version table status. code = ' || sqlerrm(code);
	    -- goto qquit;

	end if;
   trace_save(trace_list,i_jobno,line_no);
    update tax_forecast_version set status =  2 where version_id = version_number and   ( status = 1 or status = 13);
    update tax_forecast_version set status =  2 where version_id = version_number and   ( status = 19 or status = 14);
    update tax_archive_status set  description = 'Finish'  where  version_id = version_number;
    commit;
    trace_write(trace_list,'Finish' ,1);
    trace_save(trace_list,i_jobno,line_no);
    stop_job(version_number,'Tax' ,i_jobno,i_logid,'Finished' );
    commit;
    return;
<<qquit>>
code := sqlcode;
rollback;
update version set status = job_status where version_id = version_number;
update tax_forecast_version set status =  0 where version_id = version_number and
       ( status = 1 or status = 13);
update tax_forecast_version set status =  11 where version_id = version_number and
       ( status = 19 or status = 14);
update tax_archive_status set  description = str  where  version_id = version_number;
commit;
trace_write(trace_list,str ,3);
trace_save(trace_list,i_jobno,line_no);
stop_job(version_number,'Tax' ,i_jobno,i_logid,to_char(code) );
commit;
exception
	when others then
		rollback;
            code := sqlcode;
	        str := str || sqlerrm(code)  ;
			update version set status = job_status where version_id = version_number;
            update tax_forecast_version set status =  0 where version_id = version_number and
                       ( status = 1 or status = 13);
            update tax_forecast_version set status =  11 where version_id = version_number and
                       ( status = 19 or status = 14);
			update tax_archive_status set  description = substr(str,1999)
                        where  version_id =version_number;
          trace_write(trace_list,str ,3);
          trace_save(trace_list,i_jobno,line_no);
          stop_job(version_number,'Tax' ,i_jobno,i_logid,to_char(code) );
commit;
end archive;
--************************************************** Restore *******************************************************
procedure restore(version_number number) as
       status long(4000);
       i int;
       j int;
       version_key int;
       diff int;
       diff2 int;
       new_key int;
	   str varchar2(1024);
 	   sql2 varchar2(1024);
       code int;
       num int;
       num2 int;
       sqls varchar2(8096);
       err_msg varchar2(8096);
       column_str varchar2(8096);
       column_str2 varchar2(8096);
       owner_name varchar2(20);
       fcst_version int;
       fcst_status_code int;
       version_code int;
       i_jobno int := 0;
       i_logid int := 0;
       line_no int := 0;
       trace_list trace_list_type;
       table_names table_list_type;
       table_arc_names table_list_type;
       table_fcst_names table_list_type;
       table_fcst_arc_names table_list_type;
       table_fcst_arc_fcst_names table_list_type;
       ex_column_list table_list_type;
       cursor fcst_cur is  select tax_forecast_version_id
               from tax_forecast_version f where
                           version_id = version_code and
			                  status = fcst_status_code;
begin
-- THIS USER EVENT CONTAINS THE SCRIPT TO
-- RESTORE A TAX CASE
    owner_name := 'PWRPLANT';
    version_code := version_number;


    start_job(version_number,0,'Tax',i_jobno, i_logid );
    begin
	    code := audit_table_pkg.SetContext('audit','no');
	    exception
	        when others then
	          code := 0;
    end;
    code := get_table_list(table_names, table_arc_names,table_fcst_names,
                     table_fcst_arc_names,table_fcst_arc_fcst_names);
    /* check to see if we need to just complete this job */



    str := 'Max Tax Record Control';
	select max(tax_record_id) into new_key from tax_record_control;
	if sqlcode <> 0 then
	     code := sqlcode;
	     str := 'Error selecting from tax_record_control ' || sqlerrm(code);
	     goto qquit;
	end if;
	trace_write(trace_list,str ,1);
	diff := new_key;
    str := 'Max Tax Transfer ID ';
	select max(tax_transfer_id) into new_key from tax_transfer_control;
	if sqlcode <> 0 then
	     code := sqlcode;
	     str := 'Error selecting from tax_transfer_control ' || sqlerrm(code);
	     goto qquit;
	end if;
	if new_key is null then
	     new_key := 0;
	 end if;
	trace_write(trace_list,str ,1);
	diff2 := new_key;
	-- Add appropriate records to tax_record_control
	-- test of new archive  procedures
	 sqls := 'truncate table arc_tax_renumber_tids';
    trace_write(trace_list,str ,1);
    trace_save(trace_list,i_jobno,line_no);
    if exec_sql(sqls,err_msg) = -1 then
           str := err_msg;
           goto qquit;
	end if;
	insert into arc_tax_renumber_tids(tax_transfer_id_old,tax_transfer_id_new)
	    ( select tax_transfer_id,rownum + diff2
		   from  tax_transfer_control where version_id = version_number );
	if sqlcode <> 0 then
	     code := sqlcode;
	     str := 'Error inserting into arc_tax_renumber_tids ' || sqlerrm(code);
	     goto qquit;
	end if;

    str := 'Update Version';
    trace_write(trace_list,str ,1);
	update version set status = 3 where version_id = version_number;
	if sqlcode <> 0 then
	     --update  tax_archive_status  set description = 'Version ID not found in  version table ' where version_id = version_number;
	     code := sqlcode;
	     str := 'Version ID not found in  version table ' || sqlerrm(code);
	     goto qquit;
	end if;
	trace_save(trace_list,i_jobno,line_no);
	commit;
	 /*analyze  arc_tax_renumber_id; */
    str := 'analyze arc_tax_renumber_id. ';
	begin
        code := analyze_table('arc_tax_renumber_id',-1);
		exception
	      when others then
               str := 'error analyze arc_tax_renumber_id. code=' || to_char(code) || ' ' ||  sqlerrm(code);
	end;
    trace_write(trace_list,str ,1);
    trace_save(trace_list,i_jobno,line_no);
	--      Tax Record Control
    str := 'Insert into Tax Record Control';
    trace_write(trace_list,str ,1);

    for i in reverse 1..table_names.count loop
	            str := 'Insert into Arc ' || table_names(i);
		    trace_write(trace_list,str ,1);

		    if lower(table_names(i)) <> 'tax_record_control' then
		       code := check_arc_table(table_names(i),table_arc_names(i),' add column(status number(22,0),version_id(22,0))');
		    else
		       code := check_arc_table(table_names(i),table_arc_names(i),' add column(status number(22,0))');
		    end if;
		    if code <> 0 then
		        str := 'Error updating columns ' ||  table_arc_names(i) || ' code =  ' || code  ;
			goto qquit;
		    end if;
		    if lower(table_names(i)) = 'tax_transfer_control' then
		        ex_column_list(1) := 'to_trid';
	    		ex_column_list(2) := 'from_trid';
	    		ex_column_list(3) := 'tax_transfer_id';
		    elsif lower(table_names(i)) = 'tax_depreciation_transfer' then
	    		ex_column_list(1) := 'tax_transfer_id';
		    elsif lower(table_names(i)) = 'tax_book_reconcile_transfer' then
	    		ex_column_list(1) := 'tax_transfer_id';
		    elsif lower(table_names(i)) = 'deferred_income_tax_transfer' then
	    		ex_column_list(1) := 'tax_transfer_id';
	    	    end if;
		    num := get_columns(table_names(i) ,'',owner_name ,ex_column_list,column_str , err_msg );
		    num2 := get_columns(table_arc_names(i) ,'a',owner_name,ex_column_list,column_str2 , err_msg );

		    ex_column_list.delete;
		    if num <> num2 then
		       	     str := 'Table Columns are not equal ' ||  table_arc_names(i) || ' = ' || num2 || ' std=' || num  ;
			     goto qquit;
		    end if;
		   if lower(table_names(i)) = 'tax_transfer_control' then
		       sqls := 'insert into ' || table_names(i) || '(to_trid,from_trid,tax_transfer_id,' || column_str || ')' ||
		        ' Select   a.to_trid + ' ||  diff || ', a.from_trid + ' ||  diff || ',b.tax_transfer_id_new '  || ',' || column_str2 ||
		        ' FROM ' || table_arc_names(i) || ' a , arc_tax_renumber_tids b' ||
				   	' WHERE version_id = ' ||  version_number  || ' and a.tax_transfer_id = b.tax_transfer_id_old ';
		   elsif table_names(i) = 'TAX_RECORD_CONTROL' then
		       sqls := 'insert into ' || table_names(i) || '(version_id,tax_record_id,' || column_str || ')' ||
			        ' Select a.version_id '|| ', a.tax_record_id + ' ||  diff  || ','|| column_str2 ||
			        ' FROM ' || table_arc_names(i)  || ' a ' ||
			 	' WHERE  version_id ='  || version_number;
		   elsif lower(table_names(i)) = 'tax_depreciation_transfer' then
		       sqls := 'insert into ' || table_names(i) || '(tax_record_id,tax_transfer_id,' || column_str || ')' ||
			        ' Select  '|| ' a.tax_record_id + ' ||  diff  || ', b.tax_transfer_id_new,'|| column_str2 ||
			        ' FROM ' || table_arc_names(i)  || ' a, arc_tax_renumber_tids b' ||
			 	' WHERE a.version_id = ' ||  version_number || ' and a.tax_transfer_id = b.tax_transfer_id_old';
		   elsif lower(table_names(i)) = 'tax_book_reconcile_transfer' then
		       sqls := 'insert into ' || table_names(i) || '(tax_record_id,tax_transfer_id,' || column_str || ')' ||
			        ' Select  '|| ' a.tax_record_id + ' ||  diff  || ', b.tax_transfer_id_new,'|| column_str2 ||
			        ' FROM ' || table_arc_names(i)  || ' a, arc_tax_renumber_tids b' ||
			 	' WHERE a.version_id = ' ||  version_number || ' and a.tax_transfer_id = b.tax_transfer_id_old';
		   elsif lower(table_names(i)) = 'deferred_income_tax_transfer' then
		       sqls := 'insert into ' || table_names(i) || '(tax_record_id,tax_transfer_id,' || column_str || ')' ||
			        ' Select  '|| ' a.tax_record_id + ' ||  diff  || ', b.tax_transfer_id_new,'|| column_str2 ||
			        ' FROM ' || table_arc_names(i)  || ' a, arc_tax_renumber_tids b' ||
			 	' WHERE a.version_id = ' ||  version_number || ' and a.tax_transfer_id = b.tax_transfer_id_old';
			else
		       sqls := 'insert into ' || table_names(i) || '(tax_record_id,' || column_str || ')' ||
			        ' Select  '|| ' a.tax_record_id + ' ||  diff  || ','|| column_str2 ||
			        ' FROM ' || table_arc_names(i)  || ' a ' ||
			 	' WHERE  version_id ='  || version_number;
		   end if;

		   if exec_sql(sqls,err_msg) = -1 then
			  str := 'Inserting ' || table_names(i)  || '  table status. code = ' || sqls  || err_msg  ;
		         goto qquit;
		   end if;
		   sqls := 'delete ' || table_arc_names(i)  || ' where version_id  ='  || version_number;
		   if exec_sql(sqls,err_msg) = -1 then
			 str := 'Deleting ' || table_arc_names(i)  || '  table status. code =   ' || err_msg  ;
			 goto qquit;
		   end if;
       end loop;



   if check_record_count(version_number,'Restore',i_logid) = -1 then
         code := sqlcode;
	 str := 'Archive and Tax Record are not equal code = ' || sqlerrm(code) ;
         goto qquit;
    end if;

   -- Restore Tax Archived Forecast versions
    fcst_status_code := 3;  -- Archive Tax Archived Forecast
    open fcst_cur;
	loop
	   fetch fcst_cur into fcst_version;
	   if(fcst_cur%NOTFOUND) then
	        exit;
	    end if;
	  trace_write(trace_list,'insert into tax_forecast_output' ,1);
          for i in 1..table_fcst_names.count loop

	          num := get_columns(table_fcst_arc_names(i) ,'',owner_name ,ex_column_list,column_str , err_msg );
		  num2 := get_columns(table_fcst_names(i) ,'',owner_name ,ex_column_list,column_str2 , err_msg );
		  if num <> num2 then
		       	     str := 'Table Columns are not equal ' || table_fcst_arc_names(i) || ' arc = ' || num || ' std=' || num2  ;
			     goto qquit;
		  end if;

		  sqls := 'insert into ' || table_fcst_names(i) || '(tax_record_id,' || column_str || ')' ||
		        ' Select  tax_record_id + ' ||  diff  || ','|| column_str2 ||
		        ' FROM ' || table_fcst_arc_names(i) ||
			 	' WHERE  tax_forecast_version_id  ='  || fcst_version;
		  if exec_sql(sqls,err_msg) = -1 then
			  str := 'Inserting tax_forecast_output table status. code = ' || err_msg  ;
		         goto qquit;
		  end if;
		  trace_write(trace_list,'delete ' || table_fcst_arc_names(i) ,1);
                  sqls := 'delete ' || table_fcst_arc_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
	          if exec_sql(sqls,err_msg) = -1 then
		          str := 'deleting arc_tax_forecast_output  code = ' || err_msg ||  '  fcst_version' || to_char(fcst_version);
		     	  goto qquit;
	          end if;
           end loop;
        update tax_forecast_version set status =  0 where tax_forecast_version_id = fcst_version;
        code := sqlcode;
        if code <> 0 then
	          str := 'updating tax_forecast_version code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	     end if;
	end loop;
    str := 'Last Update Version ';
    trace_write(trace_list,str ,1);
	update version set status = 0 where version_id = version_number;
    update tax_forecast_version set status = 0 where version_id = version_number;
    trace_save(trace_list,i_jobno,line_no);
    stop_job(version_number,'Tax' ,i_jobno,i_logid ,'Finished');
	commit;
	return;
	--
<<qquit>>
	rollback;
    code := sqlcode;
	update version set status = 7 where version_id = version_number;
    update tax_forecast_version set status = 2 where version_id = version_number;
	update  tax_archive_status  set description = str where  version_id = version_number;
    trace_write(trace_list,str ,3);
	trace_save(trace_list,i_jobno,line_no);
    stop_job(version_number,'Tax' ,i_jobno,i_logid,to_char(code) );
	commit;
exception
	when others then
		code := sqlcode;
	    str := str || sqlerrm(code)  ;
        rollback;
		update version set status = 7 where version_id = version_number;
		update  tax_archive_status  set description = str where  version_id = version_number;
		update tax_forecast_version set status = 2 where version_id = version_number;
        trace_write(trace_list,str ,3);
		trace_save(trace_list,i_jobno,line_no);
         stop_job(version_number,'Tax' ,i_jobno,i_logid,to_char(code) );
		commit;
end restore;
--    ********************************************** Delete Archive ********************************************
procedure delete_archive(version_number number)
	IS
       	i int;
       	j int;
        version_key int;
        diff int;
        new_key int;
		str varchar2(1024);
 		sql2 varchar2(1024);
        code int;
        fcst_status_code int;
        fcst_version int;
        version_code int;
        i_jobno int := 0;
        i_logid int := 0;
        line_no int := 0;
        err_msg varchar2(8000);
        sqls varchar2(8096);
        table_names table_list_type;
        table_arc_names table_list_type;
        table_fcst_names table_list_type;
        table_fcst_arc_names table_list_type;
        table_fcst_arc_fcst_names table_list_type;
        trace_list trace_list_type;
        cursor fcst_cur is  select tax_forecast_version_id
               from tax_forecast_version f where
                           version_id = version_code and
			                  status = fcst_status_code;
begin
-- RESTORE A TAX CASE
    version_code := version_number;
    begin
	    code := audit_table_pkg.SetContext('audit','no');
	    exception
	        when others then
	          code := 0;
    end;
    start_job(version_number,0,'Tax',i_jobno ,i_logid );

    str := 'Max Tax Record Control ';
    trace_write(trace_list,str ,1);
	select max(tax_record_id) into new_key from tax_record_control;
	if sqlcode <> 0 then
         code := sqlcode;
	     str :=  'Error selecting from tax_archive_status ' || sqlerrm(code);
	     goto qquit;
	end if;
	diff := new_key;
    trace_write(trace_list,'New key ' || to_char(diff) ,1);
	-- Add appropriate records to tax_record_control
	-- test of new archive  procedures
	str := 'Update Version ';
    trace_write(trace_list,str ,1);
	update version set status = 5 where version_id = version_number;
	if sqlcode <> 0 then
         code := sqlcode;
	     str :=  'Version ID not found in  version table ' || sqlerrm(code);
	     goto qquit;
	end if;
	commit;

    code := get_table_list(table_names, table_arc_names,table_fcst_names,
                     table_fcst_arc_names,table_fcst_arc_fcst_names);
     -- Delete Archive Forecast versions
    fcst_status_code :=  5;    -- Deleting Forecast Archive
    open fcst_cur;
    loop
	   fetch fcst_cur into fcst_version;
	   if(fcst_cur%NOTFOUND) then
	        exit;
	   end if;
	   for i in 1..table_fcst_names.count loop
	         trace_write(trace_list,'delete arc_tax_forecast_output' ,1);
	 	 sqls := 'delete ' || table_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
	         if exec_sql(sqls,err_msg) = -1 then
		          str := 'deleting ' || table_fcst_names(i) || '  code = ' ||err_msg  ||  '  fcst_version' || to_char(fcst_version);
		     	  goto qquit;
		 end if;
	         trace_write(trace_list,'delete ' || table_fcst_names(i)  ,1);
	    end loop;
    end loop;
    close fcst_cur;
    trace_write(trace_list,'delete tax_year_version' ,1);
    delete  tax_year_version  where version_id = version_number;
    if(sqlcode <> 0)   then
		     code := sqlcode;
		     str := 'Deleting tax_year_version table status. code = ' || sqlerrm(code)  ;
		     trace_write(trace_list,str ,2);
    end if;
    trace_write(trace_list,'delete version' ,1);
    delete  version  where version_id = version_number;
    if(sqlcode <> 0)   then
		     code := sqlcode;
		     str := 'Deleting version table status. code = ' || sqlerrm(code)  ;
		     trace_write(trace_list,str ,2);
	end if;
	commit;
    trace_save(trace_list,i_jobno,line_no);
    stop_job(version_number,'Tax' ,i_jobno,i_logid,'Finsihed' );
	return;
	--
<<qquit>>
    code := sqlcode;
	rollback;
	update version set status = 9 where version_id = version_number;
    update tax_forecast_version set status = 2 where version_id = version_number;
	update  tax_archive_status  set description = str where  version_id = version_number;
    trace_write(trace_list,str ,3);
    trace_save(trace_list,i_jobno,line_no);
    stop_job(version_number,'Tax' ,i_jobno,i_logid,to_char(code) );
	commit;
exception
	when others then
		code := sqlcode;
	    str := str || sqlerrm(code)  ;
        dbms_output.put_line (str);
        rollback;
		update version set status = 9 where version_id = version_number;
        update tax_forecast_version set status = 2 where version_id = version_number;
		update  tax_archive_status  set description = str where  version_id = version_number;
        trace_write(trace_list,str ,3);
        trace_save(trace_list,i_jobno,line_no);
        stop_job(version_number,'Tax' ,i_jobno,i_logid,to_char(code) );
		commit;
end delete_archive;
--      *****************************************************************************************************
--	************************************************    Delete Case *************************************
--      *****************************************************************************************************
procedure delete_case( version_number number, owner_name in varchar2, table2 in varchar2) as
	 status long(4000);
	 j int;
	 i int;
	 diff int;
	 str varchar2(1024);
	 sql2 varchar2(1024);
	 version_key number(22,2);
	 code int;
     i_jobno int := 0 ;
     i_logid int := 0;
     line_no int := 0;
     err_msg varchar2(8000);
     sqls varchar2(8096);
     table_names table_list_type;
     table_arc_names table_list_type;
     table_fcst_names table_list_type;
     table_fcst_arc_names table_list_type;
     table_fcst_arc_fcst_names table_list_type;
     trace_list trace_list_type;
     fcst_status_code int;
     fcst_version int;
      version_code int;
        cursor fcst_cur is  select tax_forecast_version_id
               from tax_forecast_version f where
                           version_id = version_code and
			                  status = fcst_status_code;
  begin
    version_code := version_number;

    start_job(version_number,0,'Tax',i_jobno ,i_logid );
    begin
	    code := audit_table_pkg.SetContext('audit','no');
	    exception
	        when others then
	          code := 0;
    end;
	str := 'Tax Archive Status ';
    trace_write(trace_list,str ,1);
	update tax_archive_status set description = 'Deleting Case' where version_id = version_number;
	commit;
	 /* add appropriate records to tax_record_control test of new archive procedures */
     str := 'Update Version ';
     dbms_output.put_line (str);
     update version set status = 4 WHERE version_id = version_number;
     if(sqlcode <> 0)   then
	     code := sqlcode;
	     str := 'Updating version table status. code = ' || sqlerrm(code) ;
	     goto qquit;
      end if;
       /*  tax reconcile */
      commit;

      code := get_table_list(table_names, table_arc_names,table_fcst_names,
                     table_fcst_arc_names,table_fcst_arc_fcst_names);

    	for i in 1..table_names.count loop
	    if upper(table_names(i)) = 'TAX_TRANSFER_CONTROL' then
	        sql2 := '  to_trid in (select tax_record_id from tax_record_control where '
	      		|| ' version_id = ' || to_char(version_number) || ')';
	    elsif upper(table_names(i)) = 'TAX_RECORD_CONTROL' then
	        sql2 :=  ' version_id = ' || to_char(version_number);

	    else
	        sql2 := '  tax_record_id in (select tax_record_id from tax_record_control where '
	      		|| ' version_id = ' || to_char(version_number) || ')';
	   end if;
	   code := archive_tax.delete_table(table_names(i) ,owner_name,sql2,err_msg);
           if code <> 0 then
		str := 'deleting ' || table_names(i) || ' code = ' || sqlerrm(code)  || ' ' || err_msg ;
		goto qquit;
	   end if;
        end loop;

     -- Delete Forecast versions

    fcst_status_code := 4;    -- Deleting Forecast Archive
    open fcst_cur;
	loop
	   fetch fcst_cur into fcst_version;
	   if(fcst_cur%NOTFOUND) then
	        exit;
	    end if;

            for i in 1..table_fcst_names.count loop
	    	sqls := 'delete ' || table_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
	    	if exec_sql(sqls,err_msg) = -1 then
		          str := 'deleting ' || table_fcst_names(i) || '  code = ' ||err_msg  ||  '  fcst_version' || to_char(fcst_version);
		     	  goto qquit;
		end if;
                trace_write(trace_list,'delete ' || table_fcst_names(i) ,1);
             end loop;
        trace_write(trace_list,'delete tax_forecast_version ' ,1);
        delete tax_forecast_version where tax_forecast_version_id = fcst_version;
        code := sqlcode;
        if code <> 0 then
	          str := 'deleting tax_forecast_version  code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	     end if;
	end loop;
    close fcst_cur;
     -- Delete Archived Forecast versions
    fcst_status_code := 11;    -- Deleting Forecast Archive
    open fcst_cur;
	loop
	   fetch fcst_cur into fcst_version;
	   if(fcst_cur%NOTFOUND) then
	        exit;
	   end if;
	   for i in 1..table_fcst_arc_fcst_names.count loop
	    	sqls := 'delete ' || table_fcst_arc_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
	    	if exec_sql(sqls,err_msg) = -1 then
		          str := 'deleting ' || table_fcst_arc_fcst_names(i) || '  code = ' ||err_msg  ||  '  fcst_version' || to_char(fcst_version);
		     	  goto qquit;
		end if;
                trace_write(trace_list,'delete ' || table_fcst_names(i) ,1);
            end loop;
        trace_write(trace_list,'delete tax_forecast_version ' ,1);
        delete tax_forecast_version where tax_forecast_version_id = fcst_version;
        code := sqlcode;
        if code <> 0 then
	          str := 'deleting arc_fcst_tax_forecast_version  code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	     end if;
	end loop;
    close fcst_cur;
    trace_write(trace_list,'delete  tax_year_version' ,1);
    delete  tax_year_version  where version_id = version_number;
    if(sqlcode <> 0)   then
		     code := sqlcode;
		     str := 'Deleting tax_year_version table status. code = ' || sqlerrm(code)  ;
		     trace_write(trace_list,'delete tax_forecast_output' ,2);
	end if;
    trace_write(trace_list,'delete  version' ,1);
	delete  version  where version_id = version_number;
	if(sqlcode <> 0)   then
	     code := sqlcode;
	     str := 'Deleting version table status. code = ' || sqlerrm(code);
	     trace_write(trace_list,'delete tax_forecast_output' ,2);
	end if;

    trace_save(trace_list,i_jobno,line_no);
    stop_job(version_number,'Tax' ,i_jobno,i_logid,'Finished' );
    commit;
	return;
<<qquit>>
    code := sqlcode;
	rollback;
	update version set status = 8 where version_id = version_number;
    update tax_forecast_version set status = 0 where version_id = version_number and status = 4;
    update tax_forecast_version set status = 11 where version_id = version_number and status = 14;
	update tax_archive_status set  description = str
	                where  version_id =version_number;
    trace_write(trace_list,str ,3);
    trace_save(trace_list,i_jobno,line_no);
    stop_job(version_number,'Tax' ,i_jobno,i_logid,to_char(code) );
	commit;
exception
	when others then
		code := sqlcode;
	    str := str || sqlerrm(code)  ;
        rollback;
		update version set status = 8 where version_id = version_number;
        update tax_forecast_version set status = 0 where version_id = version_number and status = 4;
        update tax_forecast_version set status = 11 where version_id = version_number and status = 14;
		update  tax_archive_status  set description = str where  version_id = version_number;
        trace_write(trace_list,str ,3);
        trace_save(trace_list,i_jobno,line_no);
        stop_job(version_number,'Tax' ,i_jobno,i_logid,to_char(code) );
		commit;
end delete_case;
/*********************************** Archive Forcast **************************************************/
procedure archive_fcst( fcst_version number,version_number number)
IS
	 status long(4000);
	 j int;
	 i int;
	 diff int;
	 str varchar2(1024);
	 sql2 varchar2(1024);
	 version_key number(22,2);
	 code int;
	 err_msg varchar2(1024);
	 sqls varchar(8048);
	 table_names table_list_type;
	 table_arc_names table_list_type;
	 table_fcst_names table_list_type;
	 table_fcst_arc_names table_list_type;
	 table_fcst_arc_fcst_names table_list_type;
     i_jobno int := 0;
     i_logid int := 0;
     line_no int := 0;
     trace_list trace_list_type;
Begin

      code := get_table_list(table_names, table_arc_names,table_fcst_names,
                     table_fcst_arc_names,table_fcst_arc_fcst_names);

      begin
	    code := audit_table_pkg.SetContext('audit','no');
	    exception
	        when others then
	          code := 0;
      end;
      start_job(version_number,fcst_version,'Fcst',i_jobno ,i_logid );


      for i in 1..table_fcst_names.count loop
            sqls := 'insert into ' || table_fcst_arc_fcst_names(i) || '  select * from '  ||
                    table_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
            if exec_sql(sqls,err_msg) = -1 then
                  str := 'inserting ' || table_fcst_names(i) || '  code = ' || err_msg  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	    end if;
            sqls := 'delete ' || table_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
            if exec_sql(sqls,err_msg) = -1 then
	          str := 'deleting ' || table_fcst_names(i) || '  code = ' || err_msg  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	    end if;
        end loop;

        trace_write(trace_list,'update tax_forecast_version' ,1);
        update tax_forecast_version set status =  11 where tax_forecast_version_id = fcst_version;
        code := sqlcode;
        if code <> 0 then
	          str := 'updating tax_forecast_version code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
	     	  trace_write(trace_list,str ,2);
	     end if;
    trace_save(trace_list,i_jobno,line_no);
    stop_job(fcst_version,'Fcst' ,i_jobno,i_logid,'Finish' );
    commit;
    return;
<<qquit>>
rollback;
code := sqlcode;
update tax_forecast_version set status = 15 where tax_forecast_version_id = fcst_version;
update tax_archive_status set  description = str
                where  version_id =version_number;
trace_write(trace_list,str ,3);
trace_save(trace_list,i_jobno,line_no);
stop_job(fcst_version,'Fcst' ,i_jobno,i_logid,to_char(code) );
commit;
exception
	when others then
		rollback;
            code := sqlcode;
	        str := str || sqlerrm(code)  ;
            trace_write(trace_list,str ,3);
            trace_save(trace_list,i_jobno,line_no);
			update tax_forecast_version set status = 15 where tax_forecast_version_id = fcst_version;
			update tax_archive_status set  description = str
                        where  version_id =version_number;
            stop_job(fcst_version,'Fcst' ,i_jobno,i_logid,to_char(code) );
commit;
end archive_fcst;
/************************************************** Restore *******************************************************/
procedure restore_fcst(fcst_version number,version_number number) as
       status long(4000);
       i int;
       j int;
       version_key int;
       diff int;
       err_msg varchar2(1024);
       sqls varchar(8048);
       table_names table_list_type;
       table_arc_names table_list_type;
       table_fcst_names table_list_type;
       table_fcst_arc_names table_list_type;
       table_fcst_arc_fcst_names table_list_type;
       new_key int;
	   str varchar2(1024);
 	   sql2 varchar2(1024);
       code int;
       i_jobno int := 0;
       i_logid int := 0;
       line_no int := 0;
       trace_list trace_list_type;
begin
-- THIS USER EVENT CONTAINS THE SCRIPT TO
-- RESTORE A TAX CASE
     start_job(version_number,fcst_version,'Fcst',i_jobno ,i_logid );

     str := 'Max Tax Record Control';
    trace_write(trace_list,str ,1);
    begin
	    code := audit_table_pkg.SetContext('audit','no');
	    exception
	        when others then
	          code := 0;
    end;

    code := get_table_list(table_names, table_arc_names,table_fcst_names,
                     table_fcst_arc_names,table_fcst_arc_fcst_names);


    for i in 1..table_fcst_names.count loop
           sqls := 'insert into ' || table_fcst_names(i) || '  select * from '  ||
                    table_fcst_arc_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
            if exec_sql(sqls,err_msg) = -1 then
                  str := 'inserting ' || table_fcst_arc_fcst_names(i) || '  code = ' || err_msg  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	    end if;
            sqls := 'delete ' || table_fcst_arc_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
            if exec_sql(sqls,err_msg) = -1 then
	          str := 'deleting ' || table_fcst_arc_fcst_names(i) || '  code = ' || err_msg  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	    end if;
     end loop;

     trace_write(trace_list,'update tax_forecast_version ' ,1);
     update tax_forecast_version set status =  0 where tax_forecast_version_id = fcst_version;
     code := sqlcode;
     if code <> 0 then
	          str := 'updating tax_forecast_version code = ' || sqlerrm(code)  ||  '  fcst_version' || to_char(fcst_version);
	     	  trace_write(trace_list,str ,2);
	     end if;
    trace_save(trace_list,i_jobno,line_no);
    stop_job(fcst_version,'Fcst' ,i_jobno,i_logid,'Finished' );
    commit;
	return;
	--
<<qquit>>
    code := sqlcode;
	rollback;
	update tax_forecast_version set status = 16 where tax_forecast_version_id = fcst_version;
	update  tax_archive_status  set description = str where  version_id = version_number;
    trace_write(trace_list,str ,3);
    trace_save(trace_list,i_jobno,line_no);
    stop_job(fcst_version,'Fcst' ,i_jobno,i_logid ,to_char(code));
	commit;
exception
	when others then
		code := sqlcode;
	    str := str || sqlerrm(code)  ;
        dbms_output.put_line (str);
        rollback;
		update tax_forecast_version set status = 16 where tax_forecast_version_id = fcst_version;
		update  tax_archive_status  set description = str where  version_id = version_number;
        trace_write(trace_list,str ,3);
        trace_save(trace_list,i_jobno,line_no);
        stop_job(fcst_version,'Fcst' ,i_jobno,i_logid,to_char(code) );
		commit;
end restore_fcst;
--    ********************************************** Delete Archive ********************************************
procedure delete_fcst_archive(fcst_version number,version_number number)
	IS
       	i int;
       	j int;
        version_key int;
        diff int;
        new_key int;
		str varchar2(1024);
 		sql2 varchar2(1024);
        code int;
        i_jobno int := 0;
        i_logid int := 0;
       line_no int := 0;
       	 err_msg varchar2(1024);
       	 sqls varchar(8048);
	table_names table_list_type;
        table_arc_names table_list_type;
        table_fcst_names table_list_type;
        table_fcst_arc_names table_list_type;
        table_fcst_arc_fcst_names table_list_type;
       trace_list trace_list_type;
begin
    str := 'Tax Fcst Archive Status ';
    start_job(version_number,fcst_version,'Fcst',i_jobno ,i_logid );
    begin
	    code := audit_table_pkg.SetContext('audit','no');
	    exception
	        when others then
	          code := 0;
    end;
   code := get_table_list(table_names, table_arc_names,table_fcst_names,
                     table_fcst_arc_names,table_fcst_arc_fcst_names);



    trace_write(trace_list,str ,1);
    update tax_archive_status set description = 'Deleting Case' where version_id = fcst_version;
    commit;

	 /* add appropriate records to tax_record_control test of new archive procedures */
    for i in 1..table_fcst_names.count loop
        sqls := 'delete ' || table_fcst_arc_fcst_names(i) || ' where tax_forecast_version_id = '  || fcst_version;
         if exec_sql(sqls,err_msg) = -1 then
	          str := 'deleting ' || table_fcst_arc_fcst_names(i) || '  code = ' || err_msg  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	 end if;
    end loop;


    trace_write(trace_list,'delete tax_forecast_version' ,1);
	delete  tax_forecast_version  where tax_forecast_version_id = fcst_version;
	if(sqlcode <> 0)   then
	     code := sqlcode;
	     str := 'Deleting tax forecast version table status. code = ' || sqlerrm(code);
         trace_write(trace_list,str ,2);
	end if;
    commit;
    trace_save(trace_list,i_jobno,line_no);
    stop_job(fcst_version,'Fcst' ,i_jobno,i_logid ,'Finished');
    commit;
	return;
<<qquit>>
    code := sqlcode;
	rollback;
	update tax_forecast_version set status = 18 where tax_forecast_version_id = fcst_version;
	update tax_archive_status set  description = str
	                where  version_id =version_number;
    trace_write(trace_list,str ,3);
    trace_save(trace_list,i_jobno,line_no);
    stop_job(fcst_version,'Fcst' ,i_jobno,i_logid,to_char(code) );
	commit;
exception
	when others then
		code := sqlcode;
	    str := str || sqlerrm(code)  ;
        rollback;
		update tax_forecast_version set status = 18 where tax_forecast_version_id = fcst_version;
		update tax_archive_status  set description = str where  version_id = version_number;
        trace_write(trace_list,str ,3);
        trace_save(trace_list,i_jobno,line_no);
        stop_job(fcst_version,'Fcst' ,i_jobno,i_logid,to_char(code) );
		commit;
end delete_fcst_archive;
--      *****************************************************************************************************
--	************************************************    Delete Forecast  Case *************************************
--      *****************************************************************************************************
procedure delete_fcst_case( fcst_version number,version_number number) as
	 status long(4000);
	 j int;
	 i int;
	 diff int;
	 str varchar2(1024);
	 sql2 varchar2(1024);
	 version_key number(22,2);
	 sqls varchar(8048);
	 table_names table_list_type;
         table_arc_names table_list_type;
         table_fcst_names table_list_type;
         table_fcst_arc_names table_list_type;
         table_fcst_arc_fcst_names table_list_type;
	 code int;
	 err_msg varchar2(1024);
     i_jobno int := 0;
     i_logid int := 0;
     line_no int := 0;
     trace_list trace_list_type;
Begin
    start_job(version_number,fcst_version,'Fcst',i_jobno ,i_logid );

    begin
	    code := audit_table_pkg.SetContext('audit','no');
	    exception
	        when others then
	          code := 0;
    end;
    str := 'Tax Fcst Archive Status ';
    trace_write(trace_list,str ,1);
	update tax_archive_status set description = 'Deleting Case' where version_id = version_number;
	commit;

     /* remove records from forecast tables */
    code := get_table_list(table_names, table_arc_names,table_fcst_names,
                     table_fcst_arc_names,table_fcst_arc_fcst_names);


    for i in 1..table_fcst_names.count loop
         trace_write(trace_list,'delete ' || table_fcst_names(i)  ,1);
         sqls := ' delete ' || table_fcst_names(i) || ' where tax_forecast_version_id = ' || fcst_version;
         if exec_sql(sqls,err_msg) = -1 then
	          str := 'deleting ' || table_fcst_names(i) || '  code = ' || err_msg  ||  '  fcst_version' || to_char(fcst_version);
	     	  goto qquit;
	 end if;
    end loop;


    trace_write(trace_list,'delete tax_forecast_version' ,1);
    delete tax_forecast_version where tax_forecast_version_id = fcst_version;
	if(sqlcode <> 0)   then
	     code := sqlcode;
	     str := 'Deleting tax forecast version table status. code = ' || sqlerrm(code);
	     trace_write(trace_list,str ,2);
	end if;
    trace_save(trace_list,i_jobno,line_no);
    stop_job(fcst_version,'Fcst' ,i_jobno,i_logid ,'Finished');
    commit;
    return;
<<qquit>>
    rollback;
    update tax_forecast_version set status = 17 where tax_forecast_version_id = fcst_version;
    update tax_archive_status set  description = str
	                where  version_id =version_number;
    trace_write(trace_list,str ,3);
    trace_save(trace_list,i_jobno,line_no);
    stop_job(fcst_version,'Fcst' ,i_jobno,i_logid ,to_char(code));
	commit;
exception
	when others then
		code := sqlcode;
	    str := str || sqlerrm(code)  ;
        rollback;
		update tax_forecast_version set status = 17 where tax_forecast_version_id = fcst_version;
		update tax_archive_status  set description = str where  version_id = version_number;
        trace_write(trace_list,str ,3);
        trace_save(trace_list,i_jobno,line_no);
        stop_job(fcst_version,'Fcst' ,i_jobno,i_logid,to_char(code) );
		commit;
end delete_fcst_case;
/************************************************* Delete Table ***********************************************/
function delete_table( table_name_str  varchar,owner_name varchar, where_clause varchar,err_msg out varchar)
   RETURN	integer
IS
   ref_tables_cons varchar(2048);
   table_name_cons varchar(2048);
   cursor tab_cur is  select lower(c.constraint_name),
		     lower(c2.constraint_name),
                     c2.table_name
               from all_constraints c, all_constraints c2  where
                           c.constraint_name=c2.r_constraint_name and
			   c.owner = owner_name and
                           c.table_name = upper(table_name_str) and c.constraint_type = 'P';
   count2 int;
   col_count int;
   i int;
   j int;
   num int;
   ref_table varchar(2048);
   code int;
   cons_name varchar(2048);
   to_cons_name varchar(2048);
   column_str varchar(2048);
   sqls varchar(2048);
   user_cursor integer;
   TYPE col_list IS TABLE OF varchar(40) INDEX BY BINARY_INTEGER;
   TYPE table_struct_rec IS RECORD (
	name varchar2(40),
	cons varchar2(40));
   TYPE table_list_s IS TABLE OF table_struct_rec INDEX BY BINARY_INTEGER;
   table_list table_list_s;
   ref_tables table_list_s;
begin
	select count(*) into count2 from all_tables where owner = upper(owner_name) and
	              table_name = upper(table_name);
	count2 := 0;
	open tab_cur;
	loop
	   fetch tab_cur into cons_name,to_cons_name,ref_table;
	   if(tab_cur%NOTFOUND) then
	     exit;
	    end if;
	    count2 := count2 + 1;
	    ref_tables(count2).cons := to_cons_name;
	    ref_tables(count2).name := ref_table;
	    table_list(count2).cons := cons_name;
	end loop;
	close tab_cur;
	for i in 1..count2 loop
       begin
    		sqls := 'alter table ' ||  owner_name || '.' || ref_tables(i).name || ' disable constraint '
    		            || ref_tables(i).cons;
    		 user_cursor := dbms_sql.open_cursor;
    	  	 dbms_sql.parse(user_cursor,sqls,dbms_sql.v7);
    	  	 num := dbms_sql.execute(user_cursor);
    	  	 dbms_sql.close_cursor(user_cursor);
         exception
            when others then
                  sqls := '';
        end;
	end loop;
	sqls := 'delete from ' || owner_name || '.' || table_name_str || ' where ' ||  where_clause;
	user_cursor := dbms_sql.open_cursor;
	dbms_sql.parse(user_cursor,sqls,dbms_sql.v7);
	num := dbms_sql.execute(user_cursor);
	code := sqlcode;
	dbms_sql.close_cursor(user_cursor);
	if( code <> 0 ) then
	 -- code := sqlcode;
	   err_msg := 'code = ' || sqlerrm(code) || ' ' ||  sqls;
	   return -1;
	end if;
	for i in 1..count2 loop
       begin
    		sqls := 'alter table ' ||  owner_name || '.' || ref_tables(i).name || ' enable constraint   '
    		      || ref_tables(i).cons;
    		 user_cursor := dbms_sql.open_cursor;
    	  	 dbms_sql.parse(user_cursor,sqls,dbms_sql.v7);
    	  	 num := dbms_sql.execute(user_cursor);
    	  	 dbms_sql.close_cursor(user_cursor);
             exception
               when others then
                  sqls := '';
        end;
	end loop;
	return 0;
exception
	when others then
	   err_msg := 'code = ' || sqlerrm(code) || ' ' ||  sqls;
	   return -1;
end delete_table;
/*********************************************************** Get Indexes **********************************************************/
function  get_columns(a_table_name in varchar2 ,a_table_abbrev in varchar2,a_owner_name in varchar2,
        a_ex_table_list in table_list_type,a_column_str in out varchar2, a_err_msg out varchar2) return integer
as
cursor col_cur is  SELECT lower(column_name)
         FROM  all_tab_columns
                 WHERE
                    table_name=upper(a_table_name)
                    and owner=upper(a_owner_name) order by column_name;
count1 int;
str varchar(2000);
col_name varchar(2000);
cols table_list_type;
code int;
skip boolean;
i int;
begin
count1 := 0;
open col_cur;
loop
    fetch col_cur into col_name;

    if(col_cur%NOTFOUND) then
	      exit;
    end if;
    if col_name <> 'version_id' and col_name <> 'time_stamp' and col_name <> 'user_id' and col_name <> 'tax_record_id' and col_name <> 'status' then
        skip := false;
        if a_ex_table_list.count > 0 then
	        for i in 1..a_ex_table_list.count loop
	            if a_ex_table_list(i) = col_name then
	              skip := true;
	            end if;
	        end loop;
	end if;
        if skip = false then
             count1 := count1 + 1;
             cols(count1) := col_name;
        end if;
    end if;
end loop;
close col_cur;
a_column_str := '';
for i in 1..count1 loop
    --dbms_output.put_line('str=' || a_column_str);
    if length(a_table_abbrev) > 0  then
        a_column_str := a_column_str ||  a_table_abbrev ||  '.' || cols(i);
    else
        a_column_str := a_column_str || cols(i);
    end if;
    if i <> count1 then
       a_column_str := a_column_str || ',';
    end if;
end loop;
dbms_output.put_line('count=' || count1);
a_err_msg := '';
return count1;
exception
     when others then
             code := sqlcode;
             a_err_msg := 'code = ' || sqlerrm(code) ;
	     return code;
end get_columns;
/************************************************* Get Constraints ************************************************************/
function  get_constraints(a_table_names in table_list_type, a_count in integer ,a_cons_list out cons_list_s ,
          owner_name in varchar2 := 'PWRPLANT', a_subsystem in varchar2 := 'unknown') return integer
as
i int;
l_table_name varchar(40);
to_table varchar(40);
cons_name varchar2(40);
to_cons_name varchar2(40);
ref_table varchar2(40);
str varchar(2000);
col_name varchar2(40);
count1 integer;
cursor ref_cons_cur is  select lower(c.constraint_name),
		                  lower(c2.constraint_name),
                          c2.table_name
               from all_constraints c, all_constraints c2  where
                           c.constraint_name=c2.r_constraint_name and
			               c.owner = owner_name and
                           c.table_name = upper(l_table_name) and
                           (c.constraint_type = 'P' );

cursor cons_cur is select  lower(c2.constraint_name),
		                lower(c.constraint_name),
                        c.table_name
                  from all_constraints c, all_constraints c2  where
                           c.constraint_name=c2.r_constraint_name and
			               c.owner = owner_name and
                           c2.table_name = upper(l_table_name) and
                          ( c.constraint_type = 'P' or c.constraint_type = 'R');
cursor cols_cur is SELECT lower(column_name)
           FROM all_cons_columns
                 WHERE
                       owner=upper(owner_name)
                       and table_name=upper(to_table)
		               and constraint_name=upper(cons_name)
                       order by position;
Begin
i := 1;
/* Tables that reference the Archive Tables */
count1 := 0;
for i in 1..a_table_names.count loop
  begin
	l_table_name := a_table_names(i);
	open ref_cons_cur;
	loop
	   fetch ref_cons_cur into cons_name,to_cons_name,ref_table;
	   if(ref_cons_cur%NOTFOUND) then
	     exit;
	    end if;
	    count1 := count1 + 1;
        a_cons_list(count1).ref_name := l_table_name;
	    a_cons_list(count1).ref_cons := cons_name;
	    a_cons_list(count1).name := ref_table;
	    a_cons_list(count1).cons := to_cons_name;
        a_cons_list(count1).cons_type := 'F';
        /* Ref columns */
        to_table := l_table_name;
        open cols_cur;
        str := '';
        fetch cols_cur into col_name;
        loop
           str := str || col_name;
           fetch cols_cur into col_name;
           if(cols_cur%NOTFOUND) then
	         exit;
	       end if;

           str := str || ',';
        end loop;
        close cols_cur;
        a_cons_list(count1).ref_cols := str;
        /*  columns */
        to_table := ref_table;
        cons_name := to_cons_name;
        open cols_cur;
        str := '';
        fetch cols_cur into col_name;
        loop
           str := str || col_name;
           fetch cols_cur into col_name;
           if(cols_cur%NOTFOUND) then
	         exit;
	       end if;

           str := str || ',';
        end loop;
        close cols_cur;
        a_cons_list(count1).cols := str;

	end loop;
	close ref_cons_cur;
    end;
  end loop;
/* Get Foreign Keys for this Table */
for i in 1..a_table_names.count loop
  begin
	l_table_name := a_table_names(i);
	open cons_cur;
	loop
	   fetch cons_cur into cons_name,to_cons_name,ref_table;
	   if(cons_cur%NOTFOUND) then
	     exit;
	    end if;
	    count1 := count1 + 1;
        a_cons_list(count1).name := l_table_name;
	    a_cons_list(count1).cons := cons_name;
	    a_cons_list(count1).ref_name := ref_table;
	    a_cons_list(count1).ref_cons := to_cons_name;
        a_cons_list(count1).cons_type := 'R';
        /* Ref columns */
        to_table := l_table_name;
        open cols_cur;
        str := '';
        fetch cols_cur into col_name;
        loop
           str := str || col_name;
           fetch cols_cur into col_name;
           if(cols_cur%NOTFOUND) then
	         exit;
	       end if;
           str := str || ',';
        end loop;
        close cols_cur;
        a_cons_list(count1).cols := str;
         /*  columns */
        to_table := ref_table;
        cons_name := to_cons_name;
        open cols_cur;
        str := '';
        fetch cols_cur into col_name;
        loop
           str := str || col_name;
           fetch cols_cur into col_name;
           if(cols_cur%NOTFOUND) then
	         exit;
	       end if;

           str := str || ',';
        end loop;
        close cols_cur;
        a_cons_list(count1).ref_cols := str;
	end loop;
	close cons_cur;
    end;
  end loop;
  return 0;
exception
    when NO_DATA_FOUND then
      return 0;
	when others then
       return -1;
/*sql = 'alter table ' + table_ref_list[i]  + ' add  ( foreign key (  ' +
							col_str + ')' +
						  ' REFERENCES '  +  table_list[i] + ' (' +
							col_str + '));'
*/
end get_constraints;
/********************************************** drop constraints *******************************************************/
function  drop_constraints(a_cons_list in cons_list_s ,  a_trace_list in out trace_list_type,
          owner_name in varchar2 := 'PWRPLANT') return integer
as
i int;
j int;
err_msg varchar(2000);
sqls varchar(2000);
count1 integer;
temp_name_new varchar2(40);
name varchar2(40);
code int;
warnings int;
user_cursor int;
num int;
Begin
warnings := 0;
/* Drop constraint */
count1 := 0;
for i in 1..a_cons_list.count loop
      sqls := 'alter table ' || a_cons_list(i).name  || ' drop constraint ' ||
							a_cons_list(i).cons;
       trace_write(a_trace_list,sqls,1);
       Begin
	       --Execute Immediate sqls;
           user_cursor := dbms_sql.open_cursor;
    	   dbms_sql.parse(user_cursor,sqls,dbms_sql.v7);
    	   num := dbms_sql.execute(user_cursor);
    	   dbms_sql.close_cursor(user_cursor);
	       Exception
	            when others then
	                code := sqlcode;
	                if code <> -2443 then
	                	err_msg := 'code = ' || sqlerrm(code) ;
	       	        	trace_write(a_trace_list,err_msg,2);
	       	        	warnings := warnings + 1;
	       	        end if;
       end;
end loop;
return warnings;
end drop_constraints;
/********************************************** Set Constraints *******************************************************/
function set_constraints(a_cons_list in cons_list_s ,  a_trace_list in out trace_list_type,
          owner_name in varchar2 := 'PWRPLANT') return integer
as
i int;
j int;
err_msg varchar(2000);
sqls varchar(2000);
count1 integer;
temp_name_new varchar2(40);
name varchar2(40);
code int;
warnings int;
user_cursor int;
num int;
Begin
warnings := 0;
/* Get Primary Key and Tables that Refernne it */
count1 := 0;
for i in 1..a_cons_list.count loop
      sqls := 'alter table ' || a_cons_list(i).name  || ' add  ( foreign key (  ' ||
							a_cons_list(i).cols || ')' ||
						  ' REFERENCES '  ||  a_cons_list(i).ref_name || ' (' ||
							a_cons_list(i).ref_cols || '))';
       trace_write(a_trace_list,sqls,4);
       Begin
	       --Execute Immediate sqls;
           user_cursor := dbms_sql.open_cursor;
    	   dbms_sql.parse(user_cursor,sqls,dbms_sql.v7);
    	   num := dbms_sql.execute(user_cursor);
    	   dbms_sql.close_cursor(user_cursor);
	       Exception
	            when others then
	                code := sqlcode;
	                if code <> -2275 then
		                err_msg := 'code = ' || sqlerrm(code) ;
		       	        trace_write(a_trace_list,err_msg,2);
		       	        warnings := warnings + 1;
		       	end if;
       end;
end loop;
return warnings;
end set_constraints;
/********************************************** Exec SQL *******************************************************/
function exec_sql(a_sql in varchar2,a_err_msg in out varchar2 ) return integer
as
user_cursor integer;
code int;
num int;
begin
    --Execute Immediate a_sql;
    user_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(user_cursor,a_sql,dbms_sql.v7);
    num := dbms_sql.execute(user_cursor);
    dbms_sql.close_cursor(user_cursor);
    return 0;
    Exception
        when NO_DATA_FOUND then
	      return 0;

        when others then
              code := sqlcode;
              if code = 0 then
                 return 0;
              end if;
	      a_err_msg := 'code = ' || sqlerrm(code) ;
	      return -1;
end exec_sql;
/********************************************** Trace Write *******************************************************/
/*  type 1 info                  */
/*  type 2 warning               */
/*  type 3 error                 */
/*  type 4 constraint            */
/*  type 5 index                 */
procedure trace_write(a_trace_list in out TRACE_LIST_TYPE,
                         a_str        in varchar2,
                         a_type       integer) as
PRAGMA AUTONOMOUS_TRANSACTION;
      I int;
   begin
     select count(*) into i from  ARCHIVE_TRACE;
     insert into ARCHIVE_TRACE
            (JOB_NO, LINE_NO, LINE, TRACE_DATE, TRACE_TYPE)
         values
            (G_JOB_NO, I + 1, A_STR, sysdate,
            a_type);
      commit;

      return;
   exception
      when others then
         return;
 end TRACE_WRITE;
/********************************************** Trace Save *******************************************************/
procedure trace_save(a_trace_list in out trace_list_type,a_job_no integer,a_line_no in out integer )
as
i int;
begin

commit;

return ;
exception
	when others then
       return ;
end trace_save;

/**********************************************  Save Job Info Start *******************************************************/
procedure start_job(a_version_number in number,a_fcst_number in number,a_arc_type in varchar2 ,a_jobno  in out number ,a_logid in out number)
as
num_rec number;
num_rec_arc number;
begin
/* Get Job No */
    while  a_jobno = 0  loop
      select nvl(jobid,0) into a_jobno from tax_archive_status where  version_id = a_version_number ;
    end loop;
    g_job_no := a_jobno;
    if a_arc_type = 'Tax' then
       select count(*) into num_rec from tax_record_control where version_id = a_version_number;
       select count(*) into num_rec_arc from arc_tax_record_control where version_id = a_version_number;
    else /* must be a forecast type of archive */
       select count(*) into num_rec from tax_forecast_output where tax_forecast_version_id = a_fcst_number;
       select count(*) into num_rec_arc from arc_fcst_tax_forecast_output where tax_forecast_version_id = a_fcst_number;
    end if;
    select max(log_id) into a_logid from archive_system_log where job_no = a_jobno;
    update  archive_system_log set start_time=sysdate ,start_active_rec =num_rec ,start_archive_rec=num_rec_arc, status='Start'
                            where log_id = a_logid;
    exception
	when others then
       return ;
 end start_job;


 /********************************************** Save Job Info End *******************************************************/
procedure stop_job(a_version_number in number,a_arc_type in varchar2 ,a_jobno  in  number ,a_logid in number,a_status in varchar2)
as
num_rec number;
num_rec_arc number;
begin

    if a_arc_type = 'Tax' then
       select count(*) into num_rec from tax_record_control where version_id = a_version_number;
       select count(*) into num_rec_arc from arc_tax_record_control where version_id = a_version_number;
    else /* must be a forecast type of archive */
       select count(*) into num_rec from tax_forecast_output where tax_forecast_version_id = a_version_number;
       select count(*) into num_rec_arc from arc_fcst_tax_forecast_output where tax_forecast_version_id = a_version_number;
    end if;
    update  archive_system_log set end_time=sysdate ,end_active_rec =num_rec ,end_archive_rec=num_rec_arc, status='Finish'
                            where log_id = a_logid;
    commit;
    exception
	when others then
       return ;
 end stop_job;

 /********************************************** Save Job Info End *******************************************************/
function check_record_count(a_version_number in number,a_arc_type in varchar2, a_logid number )return integer
as
num_rec number;
num_rec_arc number;
begin

    if a_arc_type = 'Archive' then
       select count(*) into num_rec from tax_record_control where version_id = a_version_number;
       select count(*) into num_rec_arc from arc_tax_record_control where version_id = a_version_number;
    else /* Restore */
       select count(*) into num_rec from tax_record_control where version_id = a_version_number;
       select start_archive_rec into num_rec_arc from archive_system_log  where log_id = a_logid;
    end if;
    if num_rec <> num_rec_arc then
        return -1;
    end if;
    return 0;
    exception
	when others then
       return -1;
 end check_record_count;
 /********************************************** Add new column End *******************************************************/
function  create_colunn(a_table in varchar2 ,a_column  in  varchar2 ) return integer
as
num_rec number;
num_rec_arc number;
code int;
ascale number;
afield varchar2(40);
aprecision number;
awidth number;
anullable varchar(4);
sqls varchar(2028);
adefault varchar(100);
begin

sqls := 'alter table ' ||   a_table || ' add ( ' ||  a_column ||  ' ';
select data_type, data_precision,data_scale,data_length,nullable,data_default
  into  afield, aprecision,ascale,awidth,anullable,adefault
	      from all_tab_columns
			   where table_name = a_table
			       and column_name = upper(a_column);


	if lower(afield) = 'varchar' then
	        sqls := sqls ||  ' varchar(' ||  to_char(awidth) || ') ';
	elsif lower(afield) = 'varchar2' then
		sqls := sqls ||  ' varchar2(' ||  to_char(awidth) || ') ';
	elsif lower(afield) = 'char' then
		sqls := sqls ||  ' char(' ||  to_char(awidth) || ') ';

	elsif lower(afield) = 'number'  then
		if not awidth is null   and  not ascale is null then
			sqls := sqls ||  ' number(' || to_char(awidth) ||
								 ',' || to_char(ascale) || ')';
		elsif not awidth is null and ascale is null  then
			sqls := sqls ||  ' number(' || to_char(awidth) || ')';
		else
			sqls := sqls ||  ' number ';
		end if;

	elsif lower(afield) = 'float'  then
		if not ascale is null  and aprecision  <> 0 then
			sqls := sqls ||  ' float(' ||  to_char(aprecision) || ')';
		else
			sqls := sqls ||  ' float ';
		end if;
	elsif lower(afield) = 'long'  then
		sqls := sqls ||  ' long ';
	elsif lower(afield) = 'long raw'  then
		sqls := sqls ||  ' long raw';
	elsif lower(afield) = 'rowid'  then
		sqls := sqls ||  ' rowid ';
	elsif lower(afield) = 'raw'  then
	        sqls := sqls ||  ' raw ';
	elsif lower(afield) = 'date'  then
		sqls := sqls || ' date ';
	else
		--	messagebox('Error','Unknown type ' + field)
			return 0;
	end if;


	if  adefault is null then
		--if IsNumber ( adefault)  = false then
			 sqls := sqls || ' default ' || ''' || adefault || ''';
		--else
		--	 sqls := sqls || ' default ' || adefault;
		--end if;
	end if;

	if  anullable is null then
		sqls := sqls || ' not null ';
	end if;
return code;
exception
    when NO_DATA_FOUND then
        return 0;
     when others then
       code := sqlcode;
       return code;

end create_colunn;
/********************************************** Check Arc Table *******************************************************/
function check_arc_table(a_table1 in varchar2,a_table2 in varchar2,a_add_sql in  varchar2) return integer
as
code int;
num int;
err_msg varchar(2048);
begin
select count(*) into num from all_tables where table_name = upper(a_table2) and rownum = 1 and owner='PWRPLANT';
if num < 1 then

   code := dupicate_table(a_table1,a_table2,a_add_sql,err_msg);
else
   code := check_table_columns(a_table1,a_table2,err_msg);
end if;
return code;
exception
    when NO_DATA_FOUND then

        return 0;
     when others then
       code := sqlcode;
       return code;
end check_arc_table;
/********************************************** Column   End *******************************************************/
function  check_table_columns(a_table1 in varchar2,a_table2 in varchar2, a_err_msg in out varchar2) return integer
as
cursor tab_cur is select column_name,data_type, data_precision,data_scale,data_length,nullable,data_default
	      from all_tab_columns
	           where table_name = a_table1 and owner='PWRPLANT'
	               order by column_id;
l_name varchar2(100);
l_field varchar2(40);
l_nullable varchar2(100);
l_default varchar2(400);
l_precision number;
l_scale number;
l_width number;
l_name_arc varchar2(100);
l_field_arc varchar2(400);
l_nullable_arc varchar2(100);
l_default_arc varchar2(400);
l_precision_arc number;
l_scale_arc number;
l_width_arc number;
l_width_new number;
sql_list sql_list_type;
sql_col varchar2(248);
code int;
num int;
k int;
begin
open tab_cur;
num := 0;
loop
    fetch tab_cur into  l_name, l_field, l_precision,l_scale,l_width,l_nullable,l_default;
    if(tab_cur%NOTFOUND) then
           exit;
    end if;
    begin
	 select column_name,data_type, data_precision,data_scale,data_length,nullable,data_default
	       into  l_name_arc, l_field_arc, l_precision_arc,l_scale_arc,l_width_arc,l_nullable_arc,l_default_arc
             from all_tab_columns
	  	           where table_name = a_table2 and column_name = upper(l_name) and rownum = 1
	  	             and owner='PWRPLANT';
	    exception
	     when NO_DATA_FOUND then
          l_name_arc := '';
       when others then
         begin
           code := sqlcode;
           dbms_output.put_line ('code= ' || code);
           return code;
         end;
   end;
   <<nodata>>

   if  l_name_arc is null then

         dbms_output.put_line ('new col2 ' );
         num := num + 1;
         code := get_column_format(l_name, l_field, l_precision,l_scale,l_width,l_nullable,l_default,sql_col);
          dbms_output.put_line (sql_col );
         sql_list(num) := 'alter table ' || a_table2  || ' add ( ' || sql_col || ')';
   else
       --dbms_output.put_line ('exist col ' || l_name);
  if lower(l_field) = 'varchar' and lower(l_field_arc) = 'varchar' then
          if l_width > l_width_arc then
               num := num + 1;
               sql_list(num) := 'alter table ' || a_table2  || ' modify column (( ' ||  l_name_arc || ' varchar(' ||  to_char(l_width) || '))) ';
          end if;
  elsif lower(l_field) = 'varchar2' and lower(l_field_arc) = 'varchar2' then
          if l_width > l_width_arc then
               num := num + 1;
               sql_list(num) := 'alter table ' || a_table2  || ' modify column (( ' ||  l_name_arc || ' varchar2(' ||  to_char(l_width) || ')) ';
          end if;
  elsif lower(l_field) = 'char' and lower(l_field_arc) = 'char' then
          if l_width > l_width_arc then
               num := num + 1;
               sql_list(num) := 'alter table ' || a_table2  || ' modify column ((' ||  l_name_arc || '   char(' ||  to_char(l_width) || ')) ';
          end if;
  elsif lower(l_field) = 'number'  then
          if lower(l_field) = 'number' and lower(l_field_arc) = 'number' then
           if  not l_width is null and not l_scale is null  then
             if l_scale > l_scale_arc or l_scale_arc is null then
                l_width_new := l_width_arc + l_scale;
                if l_width_new < l_width then
                    l_width_new := l_width;
                end if;
                num := num + 1;
          sql_list(num) := 'alter table ' || a_table2  || ' modify column (( ' ||  l_name_arc ||
                        ' number(' || to_char(l_width_new) || ',' || to_char(l_scale) || ')';
              end if;
             elsif not l_width is null and l_scale is null  then
             k := 0;--a_sqls := a_sqls || l_name_arc || ' number(' || to_char(a_width) || ')';
             else
             k := 0;--a_sqls := a_sqls || l_name_arc || ' number ';
             end if;
           end if;
         end if;  -- number
       end if; -- end elese
 end loop;
 close tab_cur;
 dbms_output.put_line ('line col ' || sql_list.count);
 for i in 1..sql_list.count loop
    code := exec_sql(sql_list(i),a_err_msg);
    if code <> 0 then
      return code;
    end if;
 end loop;

return code;
exception
    when NO_DATA_FOUND then
        return 0;
     when others then
       code := sqlcode;
       return code;
end check_table_columns;
/********************************************** Add Table  End *******************************************************/
function create_table(a_table in varchar2  ) return integer
as
cursor tab_cur is select column_name,data_type, data_precision,data_scale,data_length,nullable,data_default
        from all_tab_columns
             where table_name = a_table
                 order by column_id;
sqls varchar2(2048);
str varchar2(2048);
i int;
num int;
name varchar2(100);
field varchar2(40);
anullable varchar2(10);
datadefault varchar2(40);
aprecision number;
ascale number;
awidth number;
sql_col varchar2(248);
table_owner varchar2(100);
init int;
code int;
begin
select count(*) into num from all_tables where table_name = a_table;
if num < 1 then
  return 0;
end if;

open tab_cur;
str := '';

init := 1;
sqls := 'create table ' || table_owner || '.' || a_table || '(';
loop
        fetch tab_cur into name, field, aprecision,ascale,awidth,anullable,datadefault;
        if(tab_cur%NOTFOUND) then
           exit;
        end if;
        code := get_column_format(name, field, aprecision,ascale,awidth,anullable,datadefault,sql_col);
        if init <> 0 then
           sqls := sqls + sql_col;
        end if;
        init := 0;


end loop;
close tab_cur;
sqls := sqls || ')';
exception
    when NO_DATA_FOUND then
        return 0;
     when others then
       code := sqlcode;
       return code;
end create_table;
/*************************************** get Column format *****************************************************/
function get_column_format(a_name varchar2, a_field varchar2, a_precision number,a_scale number,a_width number ,
        a_nullable varchar2,a_default varchar2,a_sqls in out varchar2) return integer
as

code int;
begin
dbms_output.put_line ('field ' || a_field );
if lower(a_field) = 'varchar' then
  a_sqls := a_sqls || a_name || ' varchar(' ||  to_char(a_width) || ') ';

elsif lower(a_field) = 'varchar2' then
  a_sqls := a_sqls || a_name || ' varchar2(' ||  to_char(a_width) || ') ';

elsif lower(a_field) = 'char'  then
  a_sqls := a_sqls || a_name || ' char(' ||  to_char(a_width) || ') ';

elsif lower(a_field) = 'number'  then
  if  not a_width is null and not a_scale is null  then
    a_sqls := a_sqls || a_name || ' number(' || to_char(a_width) ||
             ',' || to_char(a_scale) || ')';
  elsif not a_width is null and a_scale is null  then
    a_sqls := a_sqls || a_name || ' number(' || to_char(a_width) || ')';
  else
    a_sqls := a_sqls || a_name || ' number ';
  end if;
        dbms_output.put_line ('field ' || a_sqls );
elsif lower(a_field) = 'float'  then
  if not a_scale is null  and a_precision  <> 0 then
    a_sqls := a_sqls || a_name || ' float(' ||  to_char(a_precision) || ')';
  else
    a_sqls := a_sqls || a_name || ' float ';
  end if;
elsif lower(a_field) = 'long'  then
  a_sqls := a_sqls || a_name || ' long ';
elsif lower(a_field) = 'long raw'  then
  a_sqls := a_sqls || a_name || ' long raw';
elsif lower(a_field) = 'rowid'  then
  a_sqls := a_sqls || a_name || ' rowid ';
elsif lower(a_field) = 'raw'  then
  a_sqls := a_sqls || a_name || ' raw ';
elsif lower(a_field) = 'date'  then
  a_sqls := a_sqls || a_name || ' date ';
else

  return -1;
end if;

if not a_default is null  then
  --if IsNumber ( default)  = false then
     a_sqls := a_sqls || ' default ' || '''' || a_default || '''';
  --else
  --   sqls := sqls || ' default ' || datadefault
  --end if
end if;
if a_nullable  = 'N' then
  a_sqls := a_sqls || ' not null ';
end if;
dbms_output.put_line ('field ' || a_sqls );
return 0;
exception
    when NO_DATA_FOUND then
        return 0;
     when others then
       code := sqlcode;
       return code;
end get_column_format;
/********************************************** Add Table  End *******************************************************/
function dupicate_table(a_from_table in varchar2,a_new_table in varchar2,a_alter_extra in varchar2, a_err_msg in out varchar2  ) return integer
as
sqls varchar2(2048);
code int;
table_owner varchar2(100);
begin
sqls := 'create table ' || a_new_table || ' as select * from ' || a_from_table  || ' where rownum = 0';
dbms_output.put_line ('table2= ' || sqls);
code :=exec_sql(sqls,a_err_msg);
sqls := 'alter table ' || a_new_table || ' ' || a_alter_extra;
dbms_output.put_line ('alter= ' || sqls);
code := exec_sql(sqls,a_err_msg);
return code;
end dupicate_table;

function get_table_list(a_tables in out table_list_type, a_arc_tables out table_list_type,a_fcst_tables in out table_list_type,
                     a_arc_fcst_tables out table_list_type,a_fcst_arc_fcst_tables out table_list_type ) return integer
as
sqls varchar2(2048);
code int;
i int;
table_owner varchar2(100);
begin

a_tables(1) := 'TAX_DEPRECIATION';
a_tables(2) := 'TAX_BOOK_RECONCILE';
a_tables(3) := 'BASIS_AMOUNTS';
a_tables(4) := 'DEFERRED_INCOME_TAX';
a_tables(5) := 'TAX_ANNOTATION';
a_tables(6) := 'TAX_BOOK_RECONCILE_TRANSFER';
a_tables(7) := 'DEFERRED_INCOME_TAX_TRANSFER';
a_tables(8) := 'TAX_DEPRECIATION_TRANSFER';
a_tables(9) := 'TAX_TRANSFER_CONTROL';
a_tables(10) := 'TAX_RECORD_CONTROL';
for i in 1..a_tables.count loop
      if length(a_tables(i)) > 25 then
                a_arc_tables(i) :=     'ARC_' || substr(a_tables(i),1,26);
      else
                a_arc_tables(i)  :=     'ARC_' || a_tables(i);
      end if;
end loop;

a_fcst_tables(1) := 'TAX_FORECAST_OUTPUT';
a_fcst_tables(2) := 'TAX_FORECAST_INPUT';
a_fcst_tables(3) := 'DFIT_FORECAST_OUTPUT';

for i in 1..a_fcst_tables.count loop
         if length(a_fcst_tables(i)) > 25 then
                a_arc_fcst_tables(i) :=     'ARC_' || substr(a_fcst_tables(i),1,26);
         else
                a_arc_fcst_tables(i)  :=     'ARC_' || a_fcst_tables(i);
         end if;
         if length(a_fcst_tables(i)) > 20 then
                a_fcst_arc_fcst_tables(i) :=     'ARC_FCST_' || substr(a_fcst_tables(i),1,21);
         else
                a_fcst_arc_fcst_tables(i)  :=    'ARC_FCST_' || a_fcst_tables(i);
         end if;
end loop;
return 0;
end get_table_list;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (822, 0, 10, 4, 2, 0, 33912, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033912_pwrtax_ARCHIVE_TAX.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;