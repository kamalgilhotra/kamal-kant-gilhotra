/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037729_reg_audit_trail_1.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.5 04/18/2014 Shane "C" Ward Reg Audit Trail Script 1 *MUST BE RUN BEFORE SCRIPT 2*
||========================================================================================
*/

create table PP_AUDITS_REG_TRAIL
(
 SEQUENCE_KEY       number(22,0)   not null,
 TABLE_NAME         varchar2(35)   not null,
 COLUMN_NAME        varchar2(35)   not null,
 PRIMARY_KEY        varchar2(2000) not null,
 PK_DISPLAY         varchar2(2000) null,
 OLD_VALUE          varchar2(4000) null,
 NEW_VALUE          varchar2(4000) null,
 MODIFIED_USER_ID   varchar2(18)   not null,
 MODIFIED_TIMESTAMP date           not null,
 OLD_DISPLAY        varchar2(4000) null,
 NEW_DISPLAY        varchar2(4000) null,
 ACTION             varchar2(16)   null,
 MONTH_NUMBER       number(22,0)   null,
 COMMENTS           varchar2(254)  null,
 PROGRAM            varchar2(60)   null,
 PROCESS            varchar2(60)   null,
 ACTIVE_WINDOW      varchar2(60)   null,
 WINDOW_TITLE       varchar2(254)  null
);

alter table PP_AUDITS_REG_TRAIL
   add constraint PK_AUDITS_REG_TRAIL
       primary key (SEQUENCE_KEY)
       using index tablespace PWRPLANT_IDX;

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_acct', 'reg_acct_master', 'reg_acct_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_case', 'reg_case', 'reg_case_id', 'case_name');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_jurisdiction', 'reg_jurisdiction', 'reg_jurisdiction_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_jur_template', 'reg_jur_template', 'reg_jur_template_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_depr_component', 'reg_depr_family_components', 'reg_component_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_family', 'reg_family', 'reg_family_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_annualization', 'reg_annualization_type', 'reg_annualization_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_acct_type', 'reg_acct_type', 'reg_acct_type_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_sub_acct_type', 'reg_sub_acct_type', 'sub_acct_type_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_depr_family_member', 'reg_depr_family_member', 'reg_member_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_ferc_account', 'reg_ferc_account', 'ferc_acct', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_historic_version', 'reg_historic_version', 'historic_version_id', 'long_description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_forecast_version', 'reg_forecast_version', 'forecast_version_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_depr_component_activity', 'reg_depr_component_activity', 'reg_activity_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_cr_tables', 'reg_cr_tables', 'cr_table_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_cr_pull_type', 'reg_cr_pull_type', 'pull_type_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('cr_amount_type', 'cr_amount_type', 'amount_type_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_cr_combos', 'reg_cr_combos', 'cr_combo_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('cr_elements', 'cr_elements', 'element_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_cr_combos_lists', 'reg_cr_combos_lists', 'cr_list_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_source', 'reg_source', 'reg_source_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_ext_pull_type', 'reg_ext_pull_type', 'pull_type_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_company', 'reg_company', 'reg_company_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_ext_source', 'reg_ext_source', 'ext_source_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_tax_entity', 'reg_tax_entity', 'tax_entity_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_alloc_category', 'reg_alloc_category', 'reg_alloc_category_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_alloc_target', 'reg_alloc_target', 'reg_alloc_target_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_fin_monitor_summary', 'reg_fin_monitor_summary', 'reg_fin_monitor_summ_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_hist_Recon_items', 'reg_hist_Recon_items', 'recon_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_allocator', 'reg_allocator', 'reg_allocator_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_capital_structure_control', 'reg_capital_structure_control', 'capital_structure_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_recovery_class', 'reg_recovery_class', 'recovery_class_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_column', 'reg_column', 'column_summary_id', 'column_name');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_budget_member_rule', 'reg_budget_member_rule', 'reg_member_rule_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_budget_member_element', 'reg_budget_member_element', 'reg_member_element_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_capital_struct_indicator', 'reg_capital_struct_indicator', 'capital_struct_indicator_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_input_scale', 'reg_input_scale', 'scale_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_cwip_component_element', 'reg_cwip_component_element', 'reg_component_element_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_cwip_source', 'reg_cwip_source', 'reg_cwip_source_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_cwip_member_rule', 'reg_cwip_member_rule', 'reg_member_rule_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_cwip_member_element', 'reg_cwip_member_element', 'reg_member_element_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('cr_structures', 'cr_structures', 'structure_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_hist_recon_group', 'reg_hist_recon_group', 'recon_group_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('tax_rollup_detail', 'tax_rollup_detail', 'tax_rollup_detail_Id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_adjustment_disposition', 'reg_adjustment_disposition', 'disposition_id', 'description');

INSERT INTO powerPlant_dddw (dropdown_name, table_name, code_col, display_col)
VALUES ('reg_interface', 'reg_interface', 'reg_interface_id', 'description');


insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_COMPONENT_ACTIVITY', 'DESCRIPTION', 'pp_au_reg_depr_comp_act', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_COMPONENT_ACTIVITY', 'REG_ACTIVITY_ID', 'pp_au_reg_depr_comp_act', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_COMPONENT_ACTIVITY', 'REG_COMPONENT_ID', 'pp_au_reg_depr_comp_act', 'IUD',
    'reg_depr_component', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_COMPONENT_ACTIVITY', 'REG_STATUS_ID', 'pp_au_reg_depr_comp_act', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'ACCT_GOOD_FOR', 'pp_au_reg_depr_fam_components', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'ACT_DEBIT_CREDIT_BALANCE', 'pp_au_reg_depr_fam_components',
    'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'DESCRIPTION', 'pp_au_reg_depr_fam_components', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'FERC_ACCT_REF', 'pp_au_reg_depr_fam_components', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'LONG_DESCRIPTION', 'pp_au_reg_depr_fam_components', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'REG_ACCT_TYPE_DEFAULT', 'pp_au_reg_depr_fam_components', 'IUD',
    'reg_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'REG_ANNUALIZATION_ID', 'pp_au_reg_depr_fam_components', 'IUD',
    'reg_annualization', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'REG_COMPONENT_ID', 'pp_au_reg_depr_fam_components', 'IUD',
    'reg_depr_component', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'REG_FAMILY_ID', 'pp_au_reg_depr_fam_components', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_COMPONENTS', 'SUB_ACCT_TYPE_ID', 'pp_au_reg_depr_fam_components', 'IUD',
    'reg_sub_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_LEDGER_MAP_ACT', 'HISTORIC_VERSION_ID', 'pp_au_reg_depr_ledger_map_act', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_LEDGER_MAP_ACT', 'REG_ACTIVITY_ID', 'pp_au_reg_depr_ledger_map_act', 'IUD',
    'reg_depr_component_activity', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_LEDGER_MAP_ACT', 'REG_COMPONENT_ID', 'pp_au_reg_depr_ledger_map_act', 'IUD',
    'reg_depr_component', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_FAM_MEM_MAP', 'FCST_DEPR_GROUP_ID', 'pp_au_reg_fcst_depr_fam_mem_ma', 'IUD',
    'fcst_depr_group', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER', 'DESCRIPTION', 'pp_au_reg_depr_fam_member', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER', 'FERC_ACCT', 'pp_au_reg_depr_fam_member', 'IUD', 'reg_ferc_account',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER', 'LONG_DESCRIPTION', 'pp_au_reg_depr_fam_member', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER', 'REG_FAMILY_ID', 'pp_au_reg_depr_fam_member', 'IUD', 'reg_family',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER', 'REG_MEMBER_ID', 'pp_au_reg_depr_fam_member', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER_MAP', 'DEPR_GROUP_ID', 'pp_au_reg_depr_fam_member_map', 'IUD',
    'pt_depr_group_filter', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER_MAP', 'HISTORIC_VERSION_ID', 'pp_au_reg_depr_fam_member_map', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER_MAP', 'REG_FAMILY_ID', 'pp_au_reg_depr_fam_member_map', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_FAMILY_MEMBER_MAP', 'REG_MEMBER_ID', 'pp_au_reg_depr_fam_member_map', 'IUD',
    'reg_depr_family_member', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_LEDGER_MAP', 'COLUMN_NAME', 'pp_au_reg_depr_ledger_map', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_LEDGER_MAP', 'HISTORIC_VERSION_ID', 'pp_au_reg_depr_ledger_map', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_LEDGER_MAP', 'REG_COMPONENT_ID', 'pp_au_reg_depr_ledger_map', 'IUD',
    'reg_depr_component', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_DEPR_LEDGER_MAP_ACT', 'COLUMN_NAME', 'pp_au_reg_depr_ledger_map_act', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'REG_CASE_ID', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD', 'reg_case',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'REG_RETURN_TAG_ID', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'SUBTOTAL_NAME', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_PARAMETER', 'PARM_VALUE', 'PP_AU_REG_CASE_PARAMETER', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_PARAMETER', 'REG_CASE_ID', 'PP_AU_REG_CASE_PARAMETER', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_PARAMETER', 'REG_PARM_ID', 'PP_AU_REG_CASE_PARAMETER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COLUMN', 'COLUMN_NAME', 'PP_AU_REG_COLUMN', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COLUMN', 'COLUMN_SUMMARY_ID', 'PP_AU_REG_COLUMN', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COLUMN', 'DESCRIPTION', 'PP_AU_REG_COLUMN', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COLUMN', 'REPORT_DW', 'PP_AU_REG_COLUMN', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COMPANY', 'DESCRIPTION', 'PP_AU_REG_COMPANY', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COMPANY', 'HISTORIC_VERSION_ID', 'PP_AU_REG_COMPANY', 'IUD', 'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COMPANY', 'REG_COMPANY_ID', 'PP_AU_REG_COMPANY', 'IUD', 'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COMPANY', 'STATUS_CODE_ID', 'PP_AU_REG_COMPANY', 'IUD', 'status_code', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COMPANY_SECURITY', 'VALID_COMPANY_ID', 'PP_AU_REG_COMPANY_SECURITY', 'IUD', 'reg_company',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_COMPANY_SECURITY', 'VALID_USER_ID', 'PP_AU_REG_COMPANY_SECURITY', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CO_INPUT_SCALE', 'INPUT_SCALE', 'PP_AU_REG_CO_INPUT_SCALE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CO_INPUT_SCALE', 'REG_COMPANY_ID', 'PP_AU_REG_CO_INPUT_SCALE', 'IUD', 'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CO_INPUT_SCALE', 'SCALE_ID', 'PP_AU_REG_CO_INPUT_SCALE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER', 'DESCRIPTION', 'PP_AU_REG_CWIP_FAMILY_MEMBER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER', 'HIST_OR_FCST', 'PP_AU_REG_CWIP_FAMILY_MEMBER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER', 'LONG_DESCRIPTION', 'PP_AU_REG_CWIP_FAMILY_MEMBER', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER', 'REG_FAMILY_ID', 'PP_AU_REG_CWIP_FAMILY_MEMBER', 'IUD', 'reg_family',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER', 'REG_MEMBER_ID', 'PP_AU_REG_CWIP_FAMILY_MEMBER', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_ELEMENT', 'DESCRIPTION', 'PP_AU_REG_CWIP_COMPONEMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_ELEMENT', 'DESCRIPTION_COLUMN', 'PP_AU_REG_CWIP_COMPONEMENT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_ELEMENT', 'DESCRIPTION_TABLE', 'PP_AU_REG_CWIP_COMPONEMENT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_ELEMENT', 'ID_COLUMN_NAME', 'PP_AU_REG_CWIP_COMPONEMENT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_ELEMENT', 'REG_COMPONENT_ELEMENT_ID', 'PP_AU_REG_CWIP_COMPONEMENT', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMP_MEMBER_MAP', 'FERC_ACCT', 'PP_AU_REG_CWIP_COMP_MEMBER_MAP', 'IUD',
    'reg_ferc_account', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMP_MEMBER_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_CWIP_COMP_MEMBER_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMP_MEMBER_MAP', 'REG_COMPONENT_ID', 'PP_AU_REG_CWIP_COMP_MEMBER_MAP', 'IUD',
    '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMP_MEMBER_MAP', 'REG_FAMILY_ID', 'PP_AU_REG_CWIP_COMP_MEMBER_MAP', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMP_MEMBER_MAP', 'REG_MEMBER_ID', 'PP_AU_REG_CWIP_COMP_MEMBER_MAP', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_MEMBER_RULE', 'DESCRIPTION', 'PP_AU_REG_CWIP_MEMBER_RULE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_MEMBER_RULE', 'REG_MEMBER_RULE_ID', 'PP_AU_REG_CWIP_MEMBER_RULE', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_VALUES', 'ID_VALUE', 'PP_AU_REG_CWIP_COMPONLUES', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_VALUES', 'REG_COMPONENT_ELEMENT_ID', 'PP_AU_REG_CWIP_COMPONLUES', 'IUD',
    'reg_cwip_component_element', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_VALUES', 'REG_COMPONENT_ID', 'PP_AU_REG_CWIP_COMPONLUES', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_COMPONENT_VALUES', 'REG_FAMILY_ID', 'PP_AU_REG_CWIP_COMPONLUES', 'IUD', 'reg_family',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'ACCT_GOOD_FOR', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'DESCRIPTION', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'HIST_OR_FCST', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'LONG_DESCRIPTION', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'REG_ACCT_TYPE_DEFAULT', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD',
    'reg_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'REG_ANNUALIZATION_ID', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD',
    'reg_annualization', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'REG_COMPONENT_ID', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'REG_CWIP_SOURCE_ID', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD',
    'reg_cwip_source', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'REG_FAMILY_ID', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD', 'reg_family',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD',
    'reg_sub_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_COMPONENT', 'USED_BY_CLIENT', 'PP_AU_REG_CWIP_FAMILYNENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_CWIP_FAMILYR_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER_MAP', 'REG_FAMILY_ID', 'PP_AU_REG_CWIP_FAMILYR_MAP', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER_MAP', 'REG_MEMBER_ID', 'PP_AU_REG_CWIP_FAMILYR_MAP', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_FAMILY_MEMBER_MAP', 'REG_MEMBER_RULE_ID', 'PP_AU_REG_CWIP_FAMILYR_MAP', 'IUD',
    'reg_cwip_member_rule', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_MEMBER_RULE_VALUES', 'CLASS_CODE_VALUE', 'PP_AU_REG_CWIP_MEMBERVALUES', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_MEMBER_RULE_VALUES', 'HISTORIC_VERSION_ID', 'PP_AU_REG_CWIP_MEMBERVALUES', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_MEMBER_RULE_VALUES', 'LOWER_VALUE', 'PP_AU_REG_CWIP_MEMBERVALUES', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_MEMBER_RULE_VALUES', 'REG_MEMBER_ELEMENT_ID', 'PP_AU_REG_CWIP_MEMBERVALUES', 'IUD',
    'reg_cwip_member_element', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_MEMBER_RULE_VALUES', 'REG_MEMBER_RULE_ID', 'PP_AU_REG_CWIP_MEMBERVALUES', 'IUD',
    'reg_cwip_member_rule', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CWIP_MEMBER_RULE_VALUES', 'UPPER_VALUE', 'PP_AU_REG_CWIP_MEMBERVALUES', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FIN_MONITOR_SUMMARY', 'DESCRIPTION', 'PP_AU_REG_FIN_MONITOR_SUMMARY', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FIN_MONITOR_SUMMARY', 'REG_FIN_MONITOR_KEY_ID', 'PP_AU_REG_FIN_MONITOR_SUMMARY', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FIN_MONITOR_SUMMARY', 'REG_FIN_MONITOR_SUMM_ID', 'PP_AU_REG_FIN_MONITOR_SUMMARY', 'IUD',
    'reg_fin_monitor_summary', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FIN_MONITOR_SUMMARY', 'SORT_ORDER', 'PP_AU_REG_FIN_MONITOR_SUMMARY', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'ADJUST_AMOUNT', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'ADJUST_ID', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'ADJUST_MONTH', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'COMMENT', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'FORECAST_VERSION_ID', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'GL_MONTH', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'PARENT_ADJUST_ID', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'REG_ACCT_ID', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'REG_COMPANY_ID', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD',
    'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'REG_SOURCE_ID', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD',
    'reg_source', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'REVERSE_FLAG', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_LEDGER_ADJUST', 'REVERSE_MONTH', 'PP_AU_REG_FORECAST_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'ACTUALS_THRU_MONTH', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'BUDGET_ACTUALS_MONTH', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'BUDGET_VERSION_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD',
    'budget_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'BUDGET_VERSION_TIME', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'COPIED_FROM_DATE', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'COPIED_FROM_VERSION_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'CR_BUDGET_ACTUALS_MONTH', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'CR_BUDGET_VERSION_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'CR_BUDGET_VERSION_TIME', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'DESCRIPTION', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'END_MONTH', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'FCST_DEPR_ACTUALS_MONTH', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'FCST_DEPR_VERSION_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'FCST_DEPR_VERSION_TIME', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'FORECAST_VERSION_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'HISTORIC_VERSION_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'LONG_DESCRIPTION', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'REG_STATUS_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'START_MONTH', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'TA_VERSION_ACTUALS_MONTH', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'TA_VERSION_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'TA_VERSION_TIME', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'TAX_VERSION_ACTUALS_MONTH', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'TAX_VERSION_ID', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'TAX_VERSION_TIME', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FORECAST_VERSION', 'VALIDATION_STATUS', 'PP_AU_REG_FORECAST_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_VERSION', 'COMMENTS', 'PP_AU_REG_HISTORIC_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_VERSION', 'END_DATE', 'PP_AU_REG_HISTORIC_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_VERSION', 'HISTORIC_VERSION_ID', 'PP_AU_REG_HISTORIC_VERSION', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_VERSION', 'INHERIT_FROM', 'PP_AU_REG_HISTORIC_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_VERSION', 'LONG_DESCRIPTION', 'PP_AU_REG_HISTORIC_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_VERSION', 'REG_STATUS_ID', 'PP_AU_REG_HISTORIC_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_VERSION', 'START_DATE', 'PP_AU_REG_HISTORIC_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_VERSION', 'USE_IN_UPDATE', 'PP_AU_REG_HISTORIC_VERSION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'ADJUST_AMOUNT', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'ADJUST_ID', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'ADJUST_MONTH', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'COMMENT', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'GL_MONTH', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'HISTORIC_VERSION_ID', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'PARENT_ADJUST_ID', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'REG_ACCT_ID', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', 'reg_acct',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'REG_COMPANY_ID', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD',
    'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'REG_SOURCE_ID', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', 'reg_source',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'REVERSE_FLAG', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORY_LEDGER_ADJUST', 'REVERSE_MONTH', 'PP_AU_REG_HISTORY_LEDJUST', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_GROUP', 'RECON_GROUP_ID', 'PP_AU_REG_HIST_RECON_GROUP', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_ITEMS', 'DESCRIPTION', 'PP_AU_REG_HIST_RECON_ITEMS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_ITEMS', 'RECON_GROUP_ID', 'PP_AU_REG_HIST_RECON_ITEMS', 'IUD',
    'reg_hist_recon_group', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_COMBOS', 'CR_BALANCES_COMBO_ID', 'PP_AU_REG_HIST_RECON_CR_COMBOS', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_COMBOS', 'CR_ELEMENT_ID', 'PP_AU_REG_HIST_RECON_CR_COMBOS', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_COMBOS', 'DESCRIPTION', 'PP_AU_REG_HIST_RECON_CR_COMBOS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_COMBOS', 'LOWER_VALUE', 'PP_AU_REG_HIST_RECON_CR_COMBOS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_COMBOS', 'PULL_TYPE_ID', 'PP_AU_REG_HIST_RECON_CR_COMBOS', 'IUD',
    'reg_ext_pull_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_COMBOS', 'STRUCTURE_ID', 'PP_AU_REG_HIST_RECON_CR_COMBOS', 'IUD',
    'cr_structures', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_COMBOS', 'UPPER_VALUE', 'PP_AU_REG_HIST_RECON_CR_COMBOS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_MAP', 'CR_BALANCES_COMBO_ID', 'PP_AU_REG_HIST_RECON_CR_MAP', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_HIST_RECON_CR_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_CR_MAP', 'RECON_ID', 'PP_AU_REG_HIST_RECON_CR_MAP', 'IUD',
    'reg_hist_Recon_items', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_GROUP', 'DESCRIPTION', 'PP_AU_REG_HIST_RECON_GROUP', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_FAM_MEM_MAP', 'FORECAST_VERSION_ID', 'pp_au_reg_fcst_depr_fam_mem_ma', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_FAM_MEM_MAP', 'REG_FAMILY_ID', 'pp_au_reg_fcst_depr_fam_mem_ma', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_FAM_MEM_MAP', 'REG_MEMBER_ID', 'pp_au_reg_fcst_depr_fam_mem_ma', 'IUD',
    'reg_depr_family_member', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_LEDGER_MAP', 'COLUMN_NAME', 'pp_au_reg_fcst_depr_ledger_map', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_LEDGER_MAP', 'FORECAST_VERSION_ID', 'pp_au_reg_fcst_depr_ledger_map', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_LEDGER_MAP', 'REG_COMPONENT_ID', 'pp_au_reg_fcst_depr_ledger_map', 'IUD',
    'reg_depr_component', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_LEDGER_MAP_ACT', 'COLUMN_NAME', 'pp_au_reg_fcst_depr_led_map_ac', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_LEDGER_MAP_ACT', 'FORECAST_VERSION_ID', 'pp_au_reg_fcst_depr_led_map_ac', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_LEDGER_MAP_ACT', 'REG_ACTIVITY_ID', 'pp_au_reg_fcst_depr_led_map_ac', 'IUD',
    'reg_depr_component_activity', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FCST_DEPR_LEDGER_MAP_ACT', 'REG_COMPONENT_ID', 'pp_au_reg_fcst_depr_led_map_ac', 'IUD',
    'reg_depr_component', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_DEPR_SET_OF_BOOKS', 'HISTORIC_VERSION_ID', 'pp_au_reg_hist_depr_sob', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_DEPR_SET_OF_BOOKS', 'SET_OF_BOOKS_ID', 'pp_au_reg_hist_depr_sob', 'IUD',
    'set_of_books', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS', 'CR_AMT_TYPE', 'pp_au_reg_cr_combos', 'IUD', 'cr_amount_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS', 'CR_COMBO_ID', 'pp_au_reg_cr_combos', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS', 'CR_COMPANY', 'pp_au_reg_cr_combos', 'IUD', 'cr_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS', 'CR_TABLE_ID', 'pp_au_reg_cr_combos', 'IUD', 'reg_cr_tables', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS', 'DESCRIPTION', 'pp_au_reg_cr_combos', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS', 'HIST_COMBO_ID', 'pp_au_reg_cr_combos', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS', 'PULL_TYPE_ID', 'pp_au_reg_cr_combos', 'IUD', 'reg_cr_pull_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS', 'CR_ACCOUNT_FIELD', 'pp_au_reg_cr_combos_elements', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS', 'CR_COMPANY_FIELD', 'pp_au_reg_cr_combos_elements', 'IUD',
    'cr_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS', 'CR_ELEMENT_ID', 'pp_au_reg_cr_combos_elements', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS', 'DEFAULT_STRUCTURE_ID', 'pp_au_reg_cr_combos_elements', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS', 'DESCRIPTION', 'pp_au_reg_cr_combos_elements', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS', 'ELEMENT_ORDER', 'pp_au_reg_cr_combos_elements', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS_BDG', 'CR_ACCOUNT_FIELD', 'PP_AU_REG_CR_COMBOS_ES_BDG', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS_BDG', 'CR_COMPANY_FIELD', 'PP_AU_REG_CR_COMBOS_ES_BDG', 'IUD',
    'cr_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS_BDG', 'CR_ELEMENT_ID', 'PP_AU_REG_CR_COMBOS_ES_BDG', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS_BDG', 'DEFAULT_STRUCTURE_ID', 'PP_AU_REG_CR_COMBOS_ES_BDG', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS_BDG', 'DESCRIPTION', 'PP_AU_REG_CR_COMBOS_ES_BDG', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_ELEMENTS_BDG', 'ELEMENT_ORDER', 'PP_AU_REG_CR_COMBOS_ES_BDG', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'CR_COMBO_ID', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD', 'reg_cr_combos',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'CR_ELEMENT_ID', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'CR_FIELD_ID', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'CR_LIST_ID', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD',
    'reg_cr_combos_lists', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'CR_SOURCE_ID', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'HISTORIC_VERSION_ID', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'LOWER_VALUE', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'NOT_INDICATOR', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'STRUCTURE_ID', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS', 'UPPER_VALUE', 'PP_AU_REG_CR_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'CR_COMBO_ID', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD',
    'reg_cr_combos', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'CR_ELEMENT_ID', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'CR_FIELD_ID', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'CR_SOURCE_ID', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'FORECAST_VERSION_ID', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'LOWER_VALUE', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'NOT_INDICATOR', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'STRUCTURE_ID', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_FIELDS_BDG', 'UPPER_VALUE', 'PP_AU_REG_CR_COMBOS_FIELDS_BDG', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_LISTS', 'CR_ELEMENT_ID', 'PP_AU_REG_CR_COMBOS_LISTS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_LISTS', 'CR_LIST_ID', 'PP_AU_REG_CR_COMBOS_LISTS', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_LISTS', 'DESCRIPTION', 'PP_AU_REG_CR_COMBOS_LISTS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_LISTS', 'ELEMENT_LIST', 'PP_AU_REG_CR_COMBOS_LISTS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_LISTS', 'STRUCTURE_ID', 'PP_AU_REG_CR_COMBOS_LISTS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_MAP', 'CR_COMBO_ID', 'PP_AU_REG_CR_COMBOS_MAP', 'IUD', 'reg_cr_combos', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_CR_COMBOS_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_MAP', 'REG_ACCT_ID', 'PP_AU_REG_CR_COMBOS_MAP', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_MAP_BDG', 'CR_COMBO_ID', 'PP_AU_REG_CR_COMBOS_MAP_BDG', 'IUD', 'reg_cr_combos',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_MAP_BDG', 'FORECAST_VERSION_ID', 'PP_AU_REG_CR_COMBOS_MAP_BDG', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_MAP_BDG', 'REG_ACCT_ID', 'PP_AU_REG_CR_COMBOS_MAP_BDG', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_SOURCES_FIELDS', 'CR_FIELD_ID', 'PP_AU_REG_CR_COMBOS_S_FIELDS', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_SOURCES_FIELDS', 'CR_SOURCE_ID', 'PP_AU_REG_CR_COMBOS_S_FIELDS', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_SOURCES_FIELDS', 'DESCRIPTION', 'PP_AU_REG_CR_COMBOS_S_FIELDS', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_COMBOS_SOURCES_FIELDS', 'FIELD_ORDER', 'PP_AU_REG_CR_COMBOS_S_FIELDS', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE', 'CR_COMPANY', 'PP_AU_REG_CR_WHERE', 'IUD', 'cr_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE', 'FORECAST_VERSION_ID', 'PP_AU_REG_CR_WHERE', 'IUD', 'reg_forecast_version',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE', 'HISTORIC_VERSION_ID', 'PP_AU_REG_CR_WHERE', 'IUD', 'reg_historic_version',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE', 'WHERE_CLAUSE_ID', 'PP_AU_REG_CR_WHERE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'AND_OR', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'BETWEEN_AND', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'COLUMN_NAME', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'LEFT_PAREN', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'OPERATOR', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'RIGHT_PAREN', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'ROW_ID', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'VALUE1', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'VALUE2', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CR_WHERE_CLAUSE', 'WHERE_CLAUSE_ID', 'PP_AU_REG_CR_WHERE_CLAUSE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE', 'DESCRIPTION', 'PP_AU_REG_EXT_SOURCE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE', 'EXT_SOURCE_ID', 'PP_AU_REG_EXT_SOURCE', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE', 'FORECAST_USE', 'PP_AU_REG_EXT_SOURCE', 'IUD', 'yes_no', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE', 'HISTORIC_USE', 'PP_AU_REG_EXT_SOURCE', 'IUD', 'yes_no', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE', 'REG_ACCT_SOURCE_ID', 'PP_AU_REG_EXT_SOURCE', 'IUD', 'reg_source', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE', 'REG_SOURCE_ID', 'PP_AU_REG_EXT_SOURCE', 'IUD', 'reg_source', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE', 'SOURCE_TYPE', 'PP_AU_REG_EXT_SOURCE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE', 'TABLE_NAME', 'PP_AU_REG_EXT_SOURCE', 'IUD', 'table_names', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'COLUMN_NAME', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'DATA_TYPE', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'DESCRIPTION', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'EXT_SOURCE_ID', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', 'reg_ext_source',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'FIELD_DECIMAL', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'FIELD_ID', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'SORT_ORDER', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'STAGE_FIELD', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'TRANSLATE_USE', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_SOURCE_FIELD', 'WIDTH', 'PP_AU_REG_EXT_SOURCE_FIELD', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMPANY_MAP', 'COMPANY', 'PP_AU_REG_EXT_COMPANY_MAP', 'IUD', 'company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMPANY_MAP', 'EXT_SOURCE_ID', 'PP_AU_REG_EXT_COMPANY_MAP', 'IUD', 'reg_ext_source',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMPANY_MAP', 'REG_COMPANY_ID', 'PP_AU_REG_EXT_COMPANY_MAP', 'IUD', 'reg_company',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_MAP', 'EXT_COMBO_ID', 'PP_AU_REG_EXT_COMBOS_MAP', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_MAP', 'FORECAST_VERSION_ID', 'PP_AU_REG_EXT_COMBOS_MAP', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_EXT_COMBOS_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_MAP', 'REG_ACCT_ID', 'PP_AU_REG_EXT_COMBOS_MAP', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_COMPANY', 'REG_COMPANY_ID', 'PP_AU_REG_ACCT_COMPANY', 'IUD', 'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_COMPANY', 'REG_MONITOR_ALLOC_ID', 'PP_AU_REG_ACCT_COMPANY', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'ACCT_GOOD_FOR', 'PP_AU_REG_ACCT_MASTER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'DESCRIPTION', 'PP_AU_REG_ACCT_MASTER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'FERC_ACCT', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_ferc_account', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'LONG_DESCRIPTION', 'PP_AU_REG_ACCT_MASTER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'REG_ACCT_ID', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'REG_ACCT_TYPE_DEFAULT', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_FIELDS', 'EXT_COMBO_ID', 'PP_AU_REG_EXT_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_FIELDS', 'EXT_SOURCE_ID', 'PP_AU_REG_EXT_COMBOS_FIELDS', 'IUD',
    'reg_ext_source', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_FIELDS', 'FIELD_ID', 'PP_AU_REG_EXT_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_FIELDS', 'FORECAST_VERSION_ID', 'PP_AU_REG_EXT_COMBOS_FIELDS', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_FIELDS', 'HISTORIC_VERSION_ID', 'PP_AU_REG_EXT_COMBOS_FIELDS', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_FIELDS', 'LOWER_VALUE', 'PP_AU_REG_EXT_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_FIELDS', 'NOT_INDICATOR', 'PP_AU_REG_EXT_COMBOS_FIELDS', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS_FIELDS', 'UPPER_VALUE', 'PP_AU_REG_EXT_COMBOS_FIELDS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS', 'COMPANY', 'PP_AU_REG_EXT_COMBOS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS', 'DESCRIPTION', 'PP_AU_REG_EXT_COMBOS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS', 'EXT_COMBO_ID', 'PP_AU_REG_EXT_COMBOS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS', 'EXT_SOURCE_ID', 'PP_AU_REG_EXT_COMBOS', 'IUD', 'reg_ext_source', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_EXT_COMBOS', 'PULL_TYPE_ID', 'PP_AU_REG_EXT_COMBOS', 'IUD', 'reg_ext_pull_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_COMPANY', 'REG_ACCT_ID', 'PP_AU_REG_ACCT_COMPANY', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'REG_ANNUALIZATION_ID', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_annualization',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'REG_COMPONENT_ID', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_depr_component',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'REG_FAMILY_ID', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'REG_MEMBER_ID', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_depr_family_member',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'REG_SOURCE_ID', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_source', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_MASTER', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_ACCT_MASTER', 'IUD', 'reg_sub_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_TAX_ENTITY', 'REG_ACCT_ID', 'PP_AU_REG_ACCT_TAX_ENTITY', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ACCT_TAX_ENTITY', 'TAX_ENTITY_ID', 'PP_AU_REG_ACCT_TAX_ENTITY', 'IUD', 'reg_tax_entity',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT_ACCT', 'ADJUSTMENT_ID', 'PP_AU_REG_ADJUST_JUR_T_ACCT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT_ACCT', 'REG_ACCT_ID', 'PP_AU_REG_ADJUST_JUR_T_ACCT', 'IUD', 'reg_acct',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT_ACCT', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_ADJUST_JUR_T_ACCT', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT_ACCT', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_ADJUST_JUR_T_ACCT', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT_ACCT', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_ADJUST_JUR_T_ACCT', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FIN_MONITOR_SUB_ACCT', 'REG_ACCT_TYPE_ID', 'PP_AU_REG_FIN_MONITOR_SUB_ACCT', 'IUD',
    'reg_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FIN_MONITOR_SUB_ACCT', 'REG_COMPANY_ID', 'PP_AU_REG_FIN_MONITOR_SUB_ACCT', 'IUD',
    'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FIN_MONITOR_SUB_ACCT', 'REG_FIN_MONITOR_SUMM_ID', 'PP_AU_REG_FIN_MONITOR_SUB_ACCT', 'IUD',
    'reg_fin_monitor_summary', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_FIN_MONITOR_SUB_ACCT', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_FIN_MONITOR_SUB_ACCT', 'IUD',
    'reg_sub_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_REGACCT_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_HIST_RECON_T_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_REGACCT_MAP', 'RECON_ID', 'PP_AU_REG_HIST_RECON_T_MAP', 'IUD',
    'reg_hist_Recon_items', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_REGACCT_MAP', 'REG_ACCT_ID', 'PP_AU_REG_HIST_RECON_T_MAP', 'IUD', 'reg_acct',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ACCT_DEFAULT', 'REG_ACCT_ID', 'PP_AU_REG_JUR_ACCT_DEFAULT', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ACCT_DEFAULT', 'REG_ACCT_TYPE_ID', 'PP_AU_REG_JUR_ACCT_DEFAULT', 'IUD',
    'reg_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ACCT_DEFAULT', 'REG_ANNUALIZATION_ID', 'PP_AU_REG_JUR_ACCT_DEFAULT', 'IUD',
    'reg_annualization', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ACCT_DEFAULT', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_ACCT_DEFAULT', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ACCT_DEFAULT', 'RELATION_ID', 'PP_AU_REG_JUR_ACCT_DEFAULT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ACCT_DEFAULT', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_JUR_ACCT_DEFAULT', 'IUD',
    'reg_sub_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_CALC_INPUT_ACCT', 'ADJUSTMENT_ID', 'PP_AU_REG_JUR_ADJ_CALT_ACCT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_CALC_INPUT_ACCT', 'REG_ACCT_ID', 'PP_AU_REG_JUR_ADJ_CALT_ACCT', 'IUD', 'reg_acct',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_CALC_INPUT_ACCT', 'REG_ALLOC_ACCT_ID', 'PP_AU_REG_JUR_ADJ_CALT_ACCT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_CALC_INPUT_ACCT', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_ADJ_CALT_ACCT', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'LONG_DESCRIPTION', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'PARENT_REG_ALLOC_ACCT_ID', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'REG_ACCT_ID', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'REG_ALLOC_ACCT_ID', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'REG_ALLOCATOR_ID', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD',
    'reg_allocator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'REG_CALC_STATUS_ID', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_ACCOUNT', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_ALLOC_ACCOUNT', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SUB_ACCT_TYPE', 'CAPITAL_STRUCTURE_ID', 'PP_AU_REG_SUB_ACCT_TYPE', 'IUD',
    'reg_capital_structure_control', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SUB_ACCT_TYPE', 'DESCRIPTION', 'PP_AU_REG_SUB_ACCT_TYPE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SUB_ACCT_TYPE', 'LONG_DESCRIPTION', 'PP_AU_REG_SUB_ACCT_TYPE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SUB_ACCT_TYPE', 'REG_ACCT_TYPE_ID', 'PP_AU_REG_SUB_ACCT_TYPE', 'IUD', 'reg_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SUB_ACCT_TYPE', 'SORT_ORDER', 'PP_AU_REG_SUB_ACCT_TYPE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SUB_ACCT_TYPE', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_SUB_ACCT_TYPE', 'IUD', 'reg_sub_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SUB_ACCT_TYPE', 'TAX_CONTROL_ID', 'PP_AU_REG_SUB_ACCT_TYPE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'ADJUST_TYPE_ID', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'ADJUSTMENT_ID', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'ADJUSTMENT_PERIODICITY', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'ADJUSTMENT_TIMING', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'CALC_DW', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'COLUMN_SUMMARY_ID', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD',
    'reg_column', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'DESCRIPTION', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'DOC_LINK', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'FORECAST', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'HISTORIC', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'LONG_DESCRIPTION', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'ORDER_OF_PROCESS', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'RECOVERY_CLASS_ID', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD',
    'reg_recovery_class', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'SET_OF_BOOKS_ID', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD',
    'set_of_books', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ADJUST_JUR_DEFAULT', 'STATUS_CODE_ID', 'PP_AU_REG_ADJUST_JUR_DEFAULT', 'IUD',
    'status_code', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR', 'ALLOC_FACTOR_CODE', 'PP_AU_REG_ALLOCATOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR', 'CAPITAL_STRUCTURE_ID', 'PP_AU_REG_ALLOCATOR', 'IUD',
    'reg_capital_structure_control', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR', 'DESCRIPTION', 'PP_AU_REG_ALLOCATOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR', 'FACTOR_TYPE', 'PP_AU_REG_ALLOCATOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR', 'REG_ALLOCATOR_ID', 'PP_AU_REG_ALLOCATOR', 'IUD', 'reg_allocator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR', 'RELATED_REG_ALLOC_ID', 'PP_AU_REG_ALLOCATOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR', 'UPDATE_INTERVAL', 'PP_AU_REG_ALLOCATOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR_DYN', 'REG_ACCT_ID', 'PP_AU_REG_ALLOCATOR_DYN', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR_DYN', 'REG_ACCT_TYPE_ID', 'PP_AU_REG_ALLOCATOR_DYN', 'IUD', 'reg_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR_DYN', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_ALLOCATOR_DYN', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR_DYN', 'REG_ALLOCATOR_ID', 'PP_AU_REG_ALLOCATOR_DYN', 'IUD', 'reg_allocator',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR_DYN', 'ROW_NUMBER', 'PP_AU_REG_ALLOCATOR_DYN', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR_DYN', 'SIGN', 'PP_AU_REG_ALLOCATOR_DYN', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOCATOR_DYN', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_ALLOCATOR_DYN', 'IUD', 'reg_sub_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_CATEGORY', 'DESCRIPTION', 'PP_AU_REG_ALLOC_CATEGORY', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_CATEGORY', 'LONG_DESCRIPTION', 'PP_AU_REG_ALLOC_CATEGORY', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_CATEGORY', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_ALLOC_CATEGORY', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_TARGET', 'DESCRIPTION', 'PP_AU_REG_ALLOC_TARGET', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_TARGET', 'EXT_ALLOC_TARGET', 'PP_AU_REG_ALLOC_TARGET', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_TARGET', 'LONG_DESCRIPTION', 'PP_AU_REG_ALLOC_TARGET', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_TARGET', 'RECOVERY_CLASS_ID', 'PP_AU_REG_ALLOC_TARGET', 'IUD', 'reg_recovery_class',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_TARGET', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_ALLOC_TARGET', 'IUD', 'reg_alloc_target',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_TARGET', 'REG_JURISDICTION_ID', 'PP_AU_REG_ALLOC_TARGET', 'IUD', 'reg_jurisdiction',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_ALLOC_TARGET', 'TARGET_SUMMARY', 'PP_AU_REG_ALLOC_TARGET', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_COMP_MEMBER_MAP', 'FERC_ACCT', 'PP_AU_REG_BUDGET_COMPR_MAP', 'IUD',
    'reg_ferc_account', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_COMP_MEMBER_MAP', 'FORECAST_VERSION_ID', 'PP_AU_REG_BUDGET_COMPR_MAP', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_COMP_MEMBER_MAP', 'REG_COMPONENT_ID', 'PP_AU_REG_BUDGET_COMPR_MAP', 'IUD',
    'reg_depr_component', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_COMP_MEMBER_MAP', 'REG_FAMILY_ID', 'PP_AU_REG_BUDGET_COMPR_MAP', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_COMP_MEMBER_MAP', 'REG_MEMBER_ID', 'PP_AU_REG_BUDGET_COMPR_MAP', 'IUD',
    'reg_depr_family_member', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_FAMILY_MEMBER_MAP', 'FORECAST_VERSION_ID', 'PP_AU_REG_BUDGET_FAMIBER_MAP', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_FAMILY_MEMBER_MAP', 'REG_FAMILY_ID', 'PP_AU_REG_BUDGET_FAMIBER_MAP', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_FAMILY_MEMBER_MAP', 'REG_MEMBER_ID', 'PP_AU_REG_BUDGET_FAMIBER_MAP', 'IUD',
    'reg_depr_family_member', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_FAMILY_MEMBER_MAP', 'REG_MEMBER_RULE_ID', 'PP_AU_REG_BUDGET_FAMIBER_MAP', 'IUD',
    'reg_budget_member_rule', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_MEMBER_RULE', 'DESCRIPTION', 'PP_AU_REG_BUDGET_MEMBER_RULE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_MEMBER_RULE', 'REG_MEMBER_RULE_ID', 'PP_AU_REG_BUDGET_MEMBER_RULE', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_MEMBER_RULE_VALUES', 'CLASS_CODE_VALUE', 'PP_AU_REG_BUDGET_MEMBE_VALUES', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_MEMBER_RULE_VALUES', 'FORECAST_VERSION_ID', 'PP_AU_REG_BUDGET_MEMBE_VALUES', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_MEMBER_RULE_VALUES', 'LOWER_VALUE', 'PP_AU_REG_BUDGET_MEMBE_VALUES', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_MEMBER_RULE_VALUES', 'REG_MEMBER_ELEMENT_ID', 'PP_AU_REG_BUDGET_MEMBE_VALUES',
    'IUD', 'reg_budget_member_element', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_MEMBER_RULE_VALUES', 'REG_MEMBER_RULE_ID', 'PP_AU_REG_BUDGET_MEMBE_VALUES', 'IUD',
    'reg_budget_member_rule', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_BUDGET_MEMBER_RULE_VALUES', 'UPPER_VALUE', 'PP_AU_REG_BUDGET_MEMBE_VALUES', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CAPITAL_STRUCTURE_CONTROL', 'CAPITAL_STRUCT_INDICATOR_ID', 'PP_AU_REG_CAPITAL_STR_CONTROL',
    'IUD', 'reg_capital_struct_indicator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CAPITAL_STRUCTURE_CONTROL', 'CAPITAL_STRUCTURE_ID', 'PP_AU_REG_CAPITAL_STR_CONTROL', 'IUD',
    'reg_capital_structure_control', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CAPITAL_STRUCTURE_CONTROL', 'DESCRIPTION', 'PP_AU_REG_CAPITAL_STR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CAPITAL_STRUCTURE_CONTROL', 'RELATED_EXPENSE_CAP_STRUCT_ID',
    'PP_AU_REG_CAPITAL_STR_CONTROL', 'IUD', 'reg_capital_structure_control', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CAPITAL_STRUCTURE_CONTROL', 'SORT_ORDER', 'PP_AU_REG_CAPITAL_STR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'CASE_MANAGER_USER_ID', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'CASE_NAME', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'COST_OF_CAPITAL_LEVEL', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'CUSTOMER_COS_YN', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'JURISDICTIONAL_COS_YN', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'LAST_UPDATE', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'LONG_DESCRIPTION', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'NOTES', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'PARENT_CASE_ID', 'PP_AU_REG_CASE', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'PERIOD_END', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'PERIOD_START', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'REG_CASE_ID', 'PP_AU_REG_CASE', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'REG_CASE_TYPE', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_CASE', 'IUD', 'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'REG_JURISDICTION_ID', 'PP_AU_REG_CASE', 'IUD', 'reg_jurisdiction', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'REG_STATUS_ID', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'THEORETICAL_CAP_STRUCT', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE', 'USE_EQUITY_RETURN_METHOD', 'PP_AU_REG_CASE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ATTACHMENT', 'ATTACH_ID', 'PP_AU_REG_CASE_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ATTACHMENT', 'FILE_NAME', 'PP_AU_REG_CASE_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ATTACHMENT', 'FILE_SIZE', 'PP_AU_REG_CASE_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ATTACHMENT', 'REG_CASE_ID', 'PP_AU_REG_CASE_ATTACHMENT', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COST_OF_CAPITAL_RATES', 'CAPITAL_STRUCTURE_ID', 'PP_AU_REG_CASE_COST_OTAL_RATES',
    'IUD', 'reg_capital_structure_control', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COST_OF_CAPITAL_RATES', 'HIGH_BAND_RATE', 'PP_AU_REG_CASE_COST_OTAL_RATES', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COST_OF_CAPITAL_RATES', 'INPUT_PERCENT_OF_CAP_TME', 'PP_AU_REG_CASE_COST_OTAL_RATES',
    'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COST_OF_CAPITAL_RATES', 'LOW_BAND_RATE', 'PP_AU_REG_CASE_COST_OTAL_RATES', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COST_OF_CAPITAL_RATES', 'MONTH_YEAR', 'PP_AU_REG_CASE_COST_OTAL_RATES', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COST_OF_CAPITAL_RATES', 'RATE', 'PP_AU_REG_CASE_COST_OTAL_RATES', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COST_OF_CAPITAL_RATES', 'REG_CASE_ID', 'PP_AU_REG_CASE_COST_OTAL_RATES', 'IUD',
    'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COVERAGE_VERSIONS', 'COVERAGE_ID', 'PP_AU_REG_CASE_COVERASIONS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COVERAGE_VERSIONS', 'COVERAGE_YR_ENDED', 'PP_AU_REG_CASE_COVERASIONS', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COVERAGE_VERSIONS', 'END_DATE', 'PP_AU_REG_CASE_COVERASIONS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COVERAGE_VERSIONS', 'FORE_VERSION_ID', 'PP_AU_REG_CASE_COVERASIONS', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COVERAGE_VERSIONS', 'HIST_VERSION_ID', 'PP_AU_REG_CASE_COVERASIONS', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COVERAGE_VERSIONS', 'REG_CASE_ID', 'PP_AU_REG_CASE_COVERASIONS', 'IUD', 'reg_case',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COVERAGE_VERSIONS', 'START_DATE', 'PP_AU_REG_CASE_COVERASIONS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_COVERAGE_VERSIONS', 'TEST_YR_FLAG', 'PP_AU_REG_CASE_COVERASIONS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_INTEREST_SYNC_CONTROL', 'CAPITAL_STRUCTURE_ID', 'PP_AU_REG_CASE_INTEREC_CONTROL',
    'IUD', 'reg_capital_structure_control', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_INTEREST_SYNC_CONTROL', 'REG_CASE_ID', 'PP_AU_REG_CASE_INTEREC_CONTROL', 'IUD',
    'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'ADJUSTED_DOLLARS', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'CONTROL_ORDER', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'DESCRIPTION', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'REG_ADJUSTMENT_IDS', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_MONITOR_CONTROL', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_CASE_MONITOR_CONTROL', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER', 'REG_FAMILY_ID', 'PP_AU_REG_TA_FAMILY_MEMBER', 'IUD', 'reg_family',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER', 'REG_MEMBER_ID', 'PP_AU_REG_TA_FAMILY_MEMBER', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER_MAP', 'COMPANY_ID', 'PP_AU_REG_TA_FAMILY_MEMBER_MAP', 'IUD', 'company',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TA_FAMILY_MEMBER_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER_MAP', 'M_ITEM_ID', 'PP_AU_REG_TA_FAMILY_MEMBER_MAP', 'IUD',
    'tax_accrual_m_master', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER_MAP', 'OPER_IND', 'PP_AU_REG_TA_FAMILY_MEMBER_MAP', 'IUD',
    'tax_accrual_oper_ind', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER_MAP', 'REG_FAMILY_ID', 'PP_AU_REG_TA_FAMILY_MEMBER_MAP', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER_MAP', 'REG_MEMBER_ID', 'PP_AU_REG_TA_FAMILY_MEMBER_MAP', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER_MAP', 'REG_TA_MEMBER_MAP_ID', 'PP_AU_REG_TA_FAMILY_MEMBER_MAP', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_M_TYPE_ENTITY_MAP', 'ENTITY_ID', 'PP_AU_REG_TA_M_TYPE_ENTITY_MAP', 'IUD',
    'tax_accrual_entity', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_M_TYPE_ENTITY_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TA_M_TYPE_ENTITY_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_M_TYPE_ENTITY_MAP', 'M_TYPE_ID', 'PP_AU_REG_TA_M_TYPE_ENTITY_MAP', 'IUD',
    'tax_accrual_m_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_M_TYPE_ENTITY_MAP', 'TAX_CONTROL_ID', 'PP_AU_REG_TA_M_TYPE_ENTITY_MAP', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_SPLIT_RULE_AMOUNT', 'AMOUNT', 'PP_AU_REG_TA_SPLIT_RULE_AMOUNT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_SPLIT_RULE_AMOUNT', 'MONTH', 'PP_AU_REG_TA_SPLIT_RULE_AMOUNT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_SPLIT_RULE_AMOUNT', 'REG_TA_SPLIT_RULE_ID', 'PP_AU_REG_TA_SPLIT_RULE_AMOUNT', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_SPLIT_RULES', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TA_SPLIT_RULES', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_SPLIT_RULES', 'PROV_REG_ACCT_ID', 'PP_AU_REG_TA_SPLIT_RULES', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_SPLIT_RULES', 'REG_TA_SPLIT_RULE_ID', 'PP_AU_REG_TA_SPLIT_RULES', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_SPLIT_RULES', 'TAX_REG_ACCT_ID', 'PP_AU_REG_TA_SPLIT_RULES', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_VERSION_CONTROL', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TA_VERSION_CONTROL', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_VERSION_CONTROL', 'TA_VERSION_ID', 'PP_AU_REG_TA_VERSION_CONTROL', 'IUD',
    'tax_accrual_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'NOTE', 'PP_AU_REG_CASE_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'PREPARER', 'PP_AU_REG_CASE_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'REG_ACCT_ID', 'PP_AU_REG_CASE_ACCT', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'REG_ACCT_TYPE_ID', 'PP_AU_REG_CASE_ACCT', 'IUD', 'reg_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'REG_ANNUALIZATION_ID', 'PP_AU_REG_CASE_ACCT', 'IUD', 'reg_annualization',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'REG_CASE_ID', 'PP_AU_REG_CASE_ACCT', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'RELATION_ID', 'PP_AU_REG_CASE_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'STATUS_CODE_ID', 'PP_AU_REG_CASE_ACCT', 'IUD', 'status_code', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_CASE_ACCT', 'IUD', 'reg_sub_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ACCT', 'WITNESS', 'PP_AU_REG_CASE_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT_MONTHLY', 'INPUT_AMOUNT', 'PP_AU_REG_CASE_ADJUSTMONTHLY', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT_MONTHLY', 'MONTH_YEAR', 'PP_AU_REG_CASE_ADJUSTMONTHLY', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT_MONTHLY', 'REG_ACCT_ID', 'PP_AU_REG_CASE_ADJUSTMONTHLY', 'IUD',
    'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT_MONTHLY', 'REG_ALLOC_ACCT_ID', 'PP_AU_REG_CASE_ADJUSTMONTHLY', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT_MONTHLY', 'REG_CASE_ID', 'PP_AU_REG_CASE_ADJUSTMONTHLY', 'IUD',
    'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT_MONTHLY', 'TEST_YR_ENDED', 'PP_AU_REG_CASE_ADJUSTMONTHLY', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_CALC_EXT', 'ADJUSTMENT_ID', 'PP_AU_REG_CASE_ADJUST_CALC_EXT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_CALC_EXT', 'ATTACHMENT_TYPE', 'PP_AU_REG_CASE_ADJUST_CALC_EXT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'ADJ_STATUS_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', 'status_code',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'ADJUSTMENT_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'ADJUSTMENT_PERIODICITY', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'ADJUSTMENT_TIMING', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'ADJUSTMENT_TYPE_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'CALC_DW', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'COLUMN_SUMMARY_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', 'reg_column',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'DESCRIPTION', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'DISPOSITION_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD',
    'reg_adjustment_disposition', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'DOC_LINK', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'FCST_DEPR_VERSION_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'LONG_DESCRIPTION', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'OM_BUDGET_VERSION_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD',
    'budget_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'ORDER_OF_PROCESS', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'PT_VERSION_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'RECOVERY_CLASS_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD',
    'reg_recovery_class', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'REG_CASE_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'RESPONSIBLE_PREPARER', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'SET_OF_BOOKS_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', 'set_of_books',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'SPREADSHEET_BACKUP', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'TAX_VERSION_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', 'version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUSTMENT', 'WITNESS_ID', 'PP_AU_REG_CASE_ADJUSTMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT', 'ADJUSTMENT_ID', 'PP_AU_REG_CASE_ADJUST_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT', 'INPUT_AMOUNT', 'PP_AU_REG_CASE_ADJUST_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT', 'INPUT_PERCENTAGE', 'PP_AU_REG_CASE_ADJUST_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT', 'REG_ACCT_ID', 'PP_AU_REG_CASE_ADJUST_ACCT', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT', 'REG_ALLOC_ACCT_ID', 'PP_AU_REG_CASE_ADJUST_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT', 'REG_CASE_ID', 'PP_AU_REG_CASE_ADJUST_ACCT', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT', 'TEST_YR_ENDED', 'PP_AU_REG_CASE_ADJUST_ACCT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_ACCT_MONTHLY', 'ADJUSTMENT_ID', 'PP_AU_REG_CASE_ADJUSTMONTHLY', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_CALC_EXT', 'CASE_YEAR', 'PP_AU_REG_CASE_ADJUST_CALC_EXT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_CALC_EXT', 'FILE_NAME', 'PP_AU_REG_CASE_ADJUST_CALC_EXT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_CALC_EXT', 'FILE_PATH', 'PP_AU_REG_CASE_ADJUST_CALC_EXT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_CALC_EXT', 'FILE_SIZE', 'PP_AU_REG_CASE_ADJUST_CALC_EXT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJUST_CALC_EXT', 'REG_CASE_ID', 'PP_AU_REG_CASE_ADJUST_CALC_EXT', 'IUD', 'reg_case',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_ATTACHMENT', 'ADJUSTMENT_ID', 'PP_AU_REG_CASE_ADJ_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_ATTACHMENT', 'ATTACH_ID', 'PP_AU_REG_CASE_ADJ_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_ATTACHMENT', 'FILE_NAME', 'PP_AU_REG_CASE_ADJ_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_ATTACHMENT', 'FILE_SIZE', 'PP_AU_REG_CASE_ADJ_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_ATTACHMENT', 'REG_CASE_ID', 'PP_AU_REG_CASE_ADJ_ATTACHMENT', 'IUD', 'reg_case',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_CALC_INPUT_ACCT', 'ADJUSTMENT_ID', 'PP_AU_REG_CASE_ADJ_CAUT_ACCT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_CALC_INPUT_ACCT', 'REG_ACCT_ID', 'PP_AU_REG_CASE_ADJ_CAUT_ACCT', 'IUD',
    'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_CALC_INPUT_ACCT', 'REG_ALLOC_ACCT_ID', 'PP_AU_REG_CASE_ADJ_CAUT_ACCT', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ADJ_CALC_INPUT_ACCT', 'REG_CASE_ID', 'PP_AU_REG_CASE_ADJ_CAUT_ACCT', 'IUD',
    'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'ANNUAL_REG_FACTOR', 'PP_AU_REG_CASE_ALLOCAN_FACTOR', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'LAST_UPDATED', 'PP_AU_REG_CASE_ALLOCAN_FACTOR', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'LAST_UPDATED_BY', 'PP_AU_REG_CASE_ALLOCAN_FACTOR', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_CASE_ALLOCAN_FACTOR', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'REG_ALLOCATOR_ID', 'PP_AU_REG_CASE_ALLOCAN_FACTOR', 'IUD',
    'reg_allocator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'REG_CASE_ID', 'PP_AU_REG_CASE_ALLOCAN_FACTOR', 'IUD',
    'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'STATISTICAL_VALUE', 'PP_AU_REG_CASE_ALLOCAN_FACTOR', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'TEST_YR_END', 'PP_AU_REG_CASE_ALLOCAN_FACTOR', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'LONG_DESCRIPTION', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'PARENT_REG_ALLOC_ACCT_ID', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'REG_ACCT_ID', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD', 'reg_acct',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'REG_ALLOC_ACCT_ID', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'REG_ALLOCATOR_ID', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD',
    'reg_allocator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'REG_CALC_STATUS_ID', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_ACCOUNT', 'REG_CASE_ID', 'PP_AU_REG_CASE_ALLOC_ACCOUNT', 'IUD', 'reg_case',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'ANNUAL_REG_FACTOR', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'BASE_YEAR_END', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'EFFECTIVE_DATE', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'LAST_UPDATED', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'LAST_UPDATED_BY', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'REG_ALLOCATOR_ID', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD',
    'reg_allocator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'REG_CASE_ID', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'STATISTICAL_VALUE', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_FACTOR', 'UNIT_OF_MEASURE', 'PP_AU_REG_CASE_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_STEPS', 'CLASS_COST_OF_SERVICE_FLAG', 'PP_AU_REG_CASE_ALLOC_STEPS', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_STEPS', 'JUR_RESULT_FLAG', 'PP_AU_REG_CASE_ALLOC_STEPS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_STEPS', 'RECOVERY_CLASS_FLAG', 'PP_AU_REG_CASE_ALLOC_STEPS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_STEPS', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_CASE_ALLOC_STEPS', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_STEPS', 'REG_CASE_ID', 'PP_AU_REG_CASE_ALLOC_STEPS', 'IUD', 'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_STEPS', 'STEP_ORDER', 'PP_AU_REG_CASE_ALLOC_STEPS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_TAX_CONTROL', 'CALC_TAX_ON_ADJUST', 'PP_AU_REG_CASE_ALLOC_NTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_TAX_CONTROL', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_CASE_ALLOC_NTROL', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_TAX_CONTROL', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_CASE_ALLOC_NTROL', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_TAX_CONTROL', 'REG_CASE_ID', 'PP_AU_REG_CASE_ALLOC_NTROL', 'IUD', 'reg_case',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_ALLOC_TAX_CONTROL', 'TAX_CALC_ID', 'PP_AU_REG_CASE_ALLOC_NTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_CATEGORY_ALLOCATOR', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_CASE_CATEGOOCATOR', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_CATEGORY_ALLOCATOR', 'REG_ALLOCATOR_ID', 'PP_AU_REG_CASE_CATEGOOCATOR', 'IUD',
    'reg_allocator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_CATEGORY_ALLOCATOR', 'REG_CASE_ID', 'PP_AU_REG_CASE_CATEGOOCATOR', 'IUD', 'reg_case',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_TARGET_FORWARD', 'CARRY_FORWARD_DOLLARS', 'PP_AU_REG_CASE_TARGET_FORWARD', 'IUD',
    null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_TARGET_FORWARD', 'CREATE_ALLOC_ACCTS', 'PP_AU_REG_CASE_TARGET_FORWARD', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_TARGET_FORWARD', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_CASE_TARGET_FORWARD', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_TARGET_FORWARD', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_CASE_TARGET_FORWARD', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_TARGET_FORWARD', 'REG_CASE_ID', 'PP_AU_REG_CASE_TARGET_FORWARD', 'IUD', 'reg_case',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_CASE_TARGET_FORWARD', 'TARGETS_MAJOR_MINOR', 'PP_AU_REG_CASE_TARGET_FORWARD', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'CR_BALANCES_AMT', 'PP_AU_REG_HISTORIC_REDGER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'GL_MONTH', 'PP_AU_REG_HISTORIC_REDGER', 'IUD', 'yes_no', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'HIST_REG_ACCT_AMT', 'PP_AU_REG_HISTORIC_REDGER', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'HISTORIC_VERSION_ID', 'PP_AU_REG_HISTORIC_REDGER', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'RECON_ADJ_AMOUNT', 'PP_AU_REG_HISTORIC_REDGER', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'RECON_ADJ_STATUS', 'PP_AU_REG_HISTORIC_REDGER', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'RECON_COMMENT', 'PP_AU_REG_HISTORIC_REDGER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'RECON_ID', 'PP_AU_REG_HISTORIC_REDGER', 'IUD',
    'reg_hist_Recon_items', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HISTORIC_RECON_LEDGER', 'REG_COMPANY_ID', 'PP_AU_REG_HISTORIC_REDGER', 'IUD',
    'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'APPROVED', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'FORECAST_VERSION_ID', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'HISTORIC_VERSION_ID', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'LAST_RUN_BY', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'LAST_RUN_DATE', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'MONTH_YEAR', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'RECONCILED', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'REG_COMPANY_ID', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD', 'reg_company',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_INTERFACE_CONTROL', 'REG_INTERFACE_ID', 'PP_AU_REG_INTERFACE_CONTROL', 'IUD',
    'reg_interface', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_ITEMS', 'RECON_ID', 'PP_AU_REG_HIST_RECON_ITEMS', 'IUD', 'reg_hist_Recon_items',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_HIST_RECON_ITEMS', 'STATUS', 'PP_AU_REG_HIST_RECON_ITEMS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JURISDICTION', 'DESCRIPTION', 'PP_AU_REG_JURISDICTION', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JURISDICTION', 'REG_COMPANY_ID', 'PP_AU_REG_JURISDICTION', 'IUD', 'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JURISDICTION', 'REG_JURISDICTION_ID', 'PP_AU_REG_JURISDICTION', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JURISDICTION', 'STATUS_CODE_ID', 'PP_AU_REG_JURISDICTION', 'IUD', 'status_code', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJUST_CALC_EXT', 'ADJUSTMENT_ID', 'PP_AU_REG_JUR_ADJUST_CALC_EXT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJUST_CALC_EXT', 'ATTACHMENT_TYPE', 'PP_AU_REG_JUR_ADJUST_CALC_EXT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJUST_CALC_EXT', 'FILE_NAME', 'PP_AU_REG_JUR_ADJUST_CALC_EXT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJUST_CALC_EXT', 'FILE_PATH', 'PP_AU_REG_JUR_ADJUST_CALC_EXT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJUST_CALC_EXT', 'FILE_SIZE', 'PP_AU_REG_JUR_ADJUST_CALC_EXT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJUST_CALC_EXT', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_ADJUST_CALC_EXT', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_ATTACHMENT', 'ADJUSTMENT_ID', 'PP_AU_REG_JUR_ADJ_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_ATTACHMENT', 'ATTACH_ID', 'PP_AU_REG_JUR_ADJ_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_ATTACHMENT', 'FILE_NAME', 'PP_AU_REG_JUR_ADJ_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_ATTACHMENT', 'FILE_SIZE', 'PP_AU_REG_JUR_ADJ_ATTACHMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ADJ_ATTACHMENT', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_ADJ_ATTACHMENT', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'ANNUAL_REG_FACTOR', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'BASE_YEAR_END', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'EFFECTIVE_DATE', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'LAST_UPDATED', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'LAST_UPDATED_BY', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'REG_ALLOCATOR_ID', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD',
    'reg_allocator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'STATISTICAL_VALUE', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_FACTOR', 'UNIT_OF_MEASURE', 'PP_AU_REG_JUR_ALLOC_FACTOR', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_STEPS', 'CLASS_COST_OF_SERVICE_FLAG', 'PP_AU_REG_JUR_ALLOC_STEPS', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_STEPS', 'JUR_RESULT_FLAG', 'PP_AU_REG_JUR_ALLOC_STEPS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_STEPS', 'RECOVERY_CLASS_FLAG', 'PP_AU_REG_JUR_ALLOC_STEPS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_STEPS', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_JUR_ALLOC_STEPS', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_STEPS', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_ALLOC_STEPS', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_STEPS', 'STEP_ORDER', 'PP_AU_REG_JUR_ALLOC_STEPS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_TAX_CONTROL', 'CALC_TAX_ON_ADJUST', 'PP_AU_REG_JUR_ALLOC_TTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_TAX_CONTROL', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_JUR_ALLOC_TTROL', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_TAX_CONTROL', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_JUR_ALLOC_TTROL', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_TAX_CONTROL', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_ALLOC_TTROL', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_ALLOC_TAX_CONTROL', 'TAX_CALC_ID', 'PP_AU_REG_JUR_ALLOC_TTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_CATEGORY_ALLOCATOR', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_JUR_CATEGORCATOR', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_CATEGORY_ALLOCATOR', 'REG_ALLOCATOR_ID', 'PP_AU_REG_JUR_CATEGORCATOR', 'IUD',
    'reg_allocator', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_CATEGORY_ALLOCATOR', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_CATEGORCATOR', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_INPUT_SCALE', 'INPUT_SCALE', 'PP_AU_REG_JUR_INPUT_SCALE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_INPUT_SCALE', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_INPUT_SCALE', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_INPUT_SCALE', 'SCALE_ID', 'PP_AU_REG_JUR_INPUT_SCALE', 'IUD', 'reg_input_scale', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'ADJUSTED_DOLLARS', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'CONTROL_ORDER', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'DESCRIPTION', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'REG_ADJUSTMENT_IDS', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'REG_RETURN_TAG_ID', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_MONITOR_CONTROL', 'SUBTOTAL_NAME', 'PP_AU_REG_JUR_MONITOR_CONTROL', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TARGET_FORWARD', 'CARRY_FORWARD_DOLLARS', 'PP_AU_REG_JUR_TARGET_FORWARD', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TARGET_FORWARD', 'CREATE_ALLOC_ACCTS', 'PP_AU_REG_JUR_TARGET_FORWARD', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TARGET_FORWARD', 'REG_ALLOC_CATEGORY_ID', 'PP_AU_REG_JUR_TARGET_FORWARD', 'IUD',
    'reg_alloc_category', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TARGET_FORWARD', 'REG_ALLOC_TARGET_ID', 'PP_AU_REG_JUR_TARGET_FORWARD', 'IUD',
    'reg_alloc_target', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TARGET_FORWARD', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_TARGET_FORWARD', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TARGET_FORWARD', 'TARGETS_MAJOR_MINOR', 'PP_AU_REG_JUR_TARGET_FORWARD', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TAX_APPORTIONMENT', 'APPORTIONMENT_PCT', 'PP_AU_REG_JUR_TAX_APPMENT', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TAX_APPORTIONMENT', 'EFF_DATE', 'PP_AU_REG_JUR_TAX_APPMENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TAX_APPORTIONMENT', 'REG_COMPANY_ID', 'PP_AU_REG_JUR_TAX_APPMENT', 'IUD',
    'reg_company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TAX_APPORTIONMENT', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_TAX_APPMENT', 'IUD',
    'reg_jur_template', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TAX_APPORTIONMENT', 'TAX_ENTITY_ID', 'PP_AU_REG_JUR_TAX_APPMENT', 'IUD',
    'reg_tax_entity', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TEMPLATE', 'COST_OF_CAPITAL_LEVEL', 'PP_AU_REG_JUR_TEMPLATE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TEMPLATE', 'DESCRIPTION', 'PP_AU_REG_JUR_TEMPLATE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TEMPLATE', 'LONG_DESCRIPTION', 'PP_AU_REG_JUR_TEMPLATE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TEMPLATE', 'REG_JUR_TEMPLATE_ID', 'PP_AU_REG_JUR_TEMPLATE', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_JUR_TEMPLATE', 'REG_JURISDICTION_ID', 'PP_AU_REG_JUR_TEMPLATE', 'IUD', 'reg_jurisdiction',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_MONITOR_JUR_CASE', 'REG_COMPANY_ID', 'PP_AU_REG_MONITOR_JUR_CASE', 'IUD', 'reg_company',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_MONITOR_JUR_CASE', 'REG_JURISDICTION_ID', 'PP_AU_REG_MONITOR_JUR_CASE', 'IUD',
    'reg_jurisdiction', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_MONITOR_JUR_CASE', 'REG_LAST_APPROVED_CASE_ID', 'PP_AU_REG_MONITOR_JUR_CASE', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_MONITOR_JUR_CASE', 'REG_MONITORING_CASE_ID', 'PP_AU_REG_MONITOR_JUR_CASE', 'IUD',
    'reg_case', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_RECOVERY_CLASS', 'DESCRIPTION', 'PP_AU_REG_RECOVERY_CLASS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_RECOVERY_CLASS', 'JURISDICTION_TYPE', 'PP_AU_REG_RECOVERY_CLASS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_RECOVERY_CLASS', 'LONG_DESCRIPTION', 'PP_AU_REG_RECOVERY_CLASS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_RECOVERY_CLASS', 'RECOVERY_CLASS_ID', 'PP_AU_REG_RECOVERY_CLASS', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_RECOVERY_CLASS', 'REG_JURISDICTION_ID', 'PP_AU_REG_RECOVERY_CLASS', 'IUD',
    'reg_jurisdiction', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_RECOVERY_CLASS', 'REG_STATUS_ID', 'PP_AU_REG_RECOVERY_CLASS', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SOURCE', 'DESCRIPTION', 'PP_AU_REG_SOURCE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SOURCE', 'NON_STANDARD', 'PP_AU_REG_SOURCE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_SOURCE', 'REG_SOURCE_ID', 'PP_AU_REG_SOURCE', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'ACCT_GOOD_FOR', 'PP_AU_REG_TAX_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'DESCRIPTION', 'PP_AU_REG_TAX_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'LONG_DESCRIPTION', 'PP_AU_REG_TAX_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'REG_ACCT_TYPE_ID', 'PP_AU_REG_TAX_COMPONENT', 'IUD', 'reg_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'REG_ANNUALIZATION_ID', 'PP_AU_REG_TAX_COMPONENT', 'IUD',
    'reg_annualization', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'REG_COMPONENT_ID', 'PP_AU_REG_TAX_COMPONENT', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'REG_FAMILY_ID', 'PP_AU_REG_TAX_COMPONENT', 'IUD', 'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_TAX_COMPONENT', 'IUD', 'reg_sub_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMPONENT', 'USED_BY_CLIENT', 'PP_AU_REG_TAX_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMP_MEMBER_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TAX_COMP_MEMBER_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMP_MEMBER_MAP', 'REG_COMPONENT_ID', 'PP_AU_REG_TAX_COMP_MEMBER_MAP', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMP_MEMBER_MAP', 'REG_FAMILY_ID', 'PP_AU_REG_TAX_COMP_MEMBER_MAP', 'IUD',
    'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_COMP_MEMBER_MAP', 'REG_MEMBER_ID', 'PP_AU_REG_TAX_COMP_MEMBER_MAP', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_ENTITY', 'DESCRIPTION', 'PP_AU_REG_TAX_ENTITY', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_ENTITY', 'TAX_ENTITY_ID', 'PP_AU_REG_TAX_ENTITY', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER', 'DESCRIPTION', 'PP_AU_REG_TAX_FAMILY_MEMBER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER', 'REG_FAMILY_ID', 'PP_AU_REG_TAX_FAMILY_MEMBER', 'IUD', 'reg_family',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER', 'REG_MEMBER_ID', 'PP_AU_REG_TAX_FAMILY_MEMBER', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_VERSION_CONTROL', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TAX_VERSION_CONTROL', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_VERSION_CONTROL', 'VERSION_ID', 'PP_AU_REG_TAX_VERSION_CONTROL', 'IUD', 'version',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'ACCT_GOOD_FOR', 'PP_AU_REG_TA_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'DESCRIPTION', 'PP_AU_REG_TA_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'FEDERAL', 'PP_AU_REG_TA_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'LOAD_OFFSETS', 'PP_AU_REG_TA_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'LONG_DESCRIPTION', 'PP_AU_REG_TA_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'REG_ACCT_TYPE_ID', 'PP_AU_REG_TA_COMPONENT', 'IUD', 'reg_acct_type', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER_MAP', 'COMPANY_ID', 'PP_AU_REG_TAX_FAMILY__MAP', 'IUD', 'company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TAX_FAMILY__MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER_MAP', 'NORMALIZATION_ID', 'PP_AU_REG_TAX_FAMILY__MAP', 'IUD',
    'normalization_schema', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER_MAP', 'REG_FAMILY_ID', 'PP_AU_REG_TAX_FAMILY__MAP', 'IUD', 'reg_family',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER_MAP', 'REG_MEMBER_ID', 'PP_AU_REG_TAX_FAMILY__MAP', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER_MAP', 'REG_TAX_MEMBER_MAP_ID', 'PP_AU_REG_TAX_FAMILY__MAP', 'IUD', null,
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER_MAP', 'TAX_CLASS_ID', 'PP_AU_REG_TAX_FAMILY__MAP', 'IUD', 'tax_class',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_FAMILY_MEMBER_MAP', 'TAX_ROLLUP_DETAIL_ID', 'PP_AU_REG_TAX_FAMILY__MAP', 'IUD',
    'tax_rollup_detail', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_RATE', 'EFF_DATE', 'PP_AU_REG_TAX_RATE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_RATE', 'TAX_ENTITY_ID', 'PP_AU_REG_TAX_RATE', 'IUD', 'reg_tax_entity', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_RATE', 'TAX_RATE', 'PP_AU_REG_TAX_RATE', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TAX_VERSION_CONTROL', 'FORECAST_VERSION_ID', 'PP_AU_REG_TAX_VERSION_CONTROL', 'IUD',
    'reg_forecast_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'REG_ANNUALIZATION_ID', 'PP_AU_REG_TA_COMPONENT', 'IUD',
    'reg_annualization', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'REG_COMPONENT_ID', 'PP_AU_REG_TA_COMPONENT', 'IUD', '<none>', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'REG_FAMILY_ID', 'PP_AU_REG_TA_COMPONENT', 'IUD', 'reg_family', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'SUB_ACCT_TYPE_ID', 'PP_AU_REG_TA_COMPONENT', 'IUD', 'reg_sub_acct_type',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMPONENT', 'USED_BY_CLIENT', 'PP_AU_REG_TA_COMPONENT', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMP_MEMBER_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TA_COMP_MEMBER_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMP_MEMBER_MAP', 'REG_COMPONENT_ID', 'PP_AU_REG_TA_COMP_MEMBER_MAP', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMP_MEMBER_MAP', 'REG_FAMILY_ID', 'PP_AU_REG_TA_COMP_MEMBER_MAP', 'IUD', 'reg_family',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_COMP_MEMBER_MAP', 'REG_MEMBER_ID', 'PP_AU_REG_TA_COMP_MEMBER_MAP', 'IUD', '<none>',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_CONTROL_MAP', 'COMPANY_ID', 'PP_AU_REG_TA_CONTROL_MAP', 'IUD', 'company', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_CONTROL_MAP', 'HISTORIC_VERSION_ID', 'PP_AU_REG_TA_CONTROL_MAP', 'IUD',
    'reg_historic_version', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_CONTROL_MAP', 'M_ITEM_ID', 'PP_AU_REG_TA_CONTROL_MAP', 'IUD', 'tax_accrual_m_master',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_CONTROL_MAP', 'OPER_IND', 'PP_AU_REG_TA_CONTROL_MAP', 'IUD', 'tax_accrual_oper_ind',
    'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_CONTROL_MAP', 'REG_ACCT_ID', 'PP_AU_REG_TA_CONTROL_MAP', 'IUD', 'reg_acct', 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_CONTROL_MAP', 'REG_TA_CONTROL_ID', 'PP_AU_REG_TA_CONTROL_MAP', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_CONTROL_MAP', 'TAX_CONTROL_ID', 'PP_AU_REG_TA_CONTROL_MAP', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );
insert into Pp_Table_Audits
   (Table_Name, Column_Name, Trigger_Name, Action, Dropdown_Name, Subsystem, user_id, timestamp)
values
   ('REG_TA_FAMILY_MEMBER', 'DESCRIPTION', 'PP_AU_REG_TA_FAMILY_MEMBER', 'IUD', null, 'REG', 'pwrplant', to_date('04/22/2014', 'mm/dd/yyyy') );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1134, 0, 10, 4, 2, 5, 37729, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.5_maint_037729_reg_audit_trail_1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;