/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_053347_lessee_import_purchase_option_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.1.0 05/13/2019 Hyunook KWon   Imports using lessee purchase option description
||============================================================================
*/

UPDATE pp_import_lookup
SET lookup_column_name = 'description'
WHERE import_lookup_id = 1117;

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
  (17842, 0, 2018, 2, 1, 0, 53347, 'C:\BitBucketRepos\classic_pb\scripts\00_base\maint_scripts', '2018.2.1.0_maint_053347_lessee_import_purchase_option_dml.sql', 1,
  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;