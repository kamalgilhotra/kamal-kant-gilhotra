/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044510_taxrpr_temp_wos_schema_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 2015.2   07/23/2015  Andrew Hill     Tax Repairs debug workspace needs access
//                                        to the TEMP_WOS_IN_SCHEMA table
||============================================================================
*/
UPDATE repair_tables SET order_idx = order_idx+1 WHERE usage_flag IN ('BLANKET', 'CPR');

INSERT INTO repair_tables (table_name, usage_flag, order_idx)
VALUES ('TEMP_WOS_IN_SCHEMA', 'BLANKET', 1);

INSERT INTO repair_tables (table_name, usage_flag, order_idx)
VALUES ('TEMP_WOS_IN_SCHEMA', 'CPR', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2711, 0, 2015, 2, 0, 0, 044510, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044510_taxrpr_temp_wos_schema_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;