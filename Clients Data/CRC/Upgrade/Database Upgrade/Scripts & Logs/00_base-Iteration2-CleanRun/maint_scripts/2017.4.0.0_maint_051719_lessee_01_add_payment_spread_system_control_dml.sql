/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051719_lessee_01_add_payment_spread_system_control_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/22/2018 Josh Sandler     Add payment spread system control
||============================================================================
*/

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
SELECT *
FROM (select
      max(control_id)+1 AS control_id,
      'Lease Non-Monthly Payment Spread' AS control_name,
      'no' AS control_value,
      'dw_yes_no;1' AS description,
      'Determine if prepayment amortization and arrears accrual should be calculated and used in deferred/prepaid rent calculation.' AS long_description,
      -1 AS company_id
      from pp_system_control_company
)
where not exists(
  select 1
  from pp_system_control_company
  where control_name = 'Lease Non-Monthly Payment Spread'
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7062, 0, 2017, 4, 0, 0, 51719, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051719_lessee_01_add_payment_spread_system_control_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;