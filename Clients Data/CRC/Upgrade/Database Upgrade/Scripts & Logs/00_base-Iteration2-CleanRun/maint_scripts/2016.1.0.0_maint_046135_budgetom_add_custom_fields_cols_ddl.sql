 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046135_budgetom_add_custom_fields_cols_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 09/08/2016 Jared Watkins  Add missing columns to the cr_budget_labor_resource table.
 ||============================================================================
 */

alter table cr_budget_labor_resource add (field_1 varchar2(254));
alter table cr_budget_labor_resource add (field_2 varchar2(254));
alter table cr_budget_labor_resource add (field_3 varchar2(254));
alter table cr_budget_labor_resource add (field_4 varchar2(254));
alter table cr_budget_labor_resource add (field_5 varchar2(254));
alter table cr_budget_labor_resource add (field_6 varchar2(254));
alter table cr_budget_labor_resource add (field_7 varchar2(254));
alter table cr_budget_labor_resource add (field_8 varchar2(254));
alter table cr_budget_labor_resource add (field_9 varchar2(254));
alter table cr_budget_labor_resource add (field_10 varchar2(254));
alter table cr_budget_labor_resource add (field_11 varchar2(254));
alter table cr_budget_labor_resource add (field_12 varchar2(254));
alter table cr_budget_labor_resource add (field_13 varchar2(254));
alter table cr_budget_labor_resource add (field_14 varchar2(254));
alter table cr_budget_labor_resource add (field_15 varchar2(254));

COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_1 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_2 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_3 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_4 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_5 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_6 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_7 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_8 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_9 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_10 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_11 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_12 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_13 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_14 IS 'An additional labor resource field whose use is specified by the client configuration.';
COMMENT ON COLUMN CR_BUDGET_LABOR_RESOURCE.FIELD_15 IS 'An additional labor resource field whose use is specified by the client configuration.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3280, 0, 2016, 1, 0, 0, 046135, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046135_budgetom_add_custom_fields_cols_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;