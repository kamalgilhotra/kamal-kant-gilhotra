/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029593_pwrtax_filtering.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/27/2014 Julia Breuer
||============================================================================
*/

--
-- Add Asset ID and Tax Layer as filtering options.
--
insert into PWRPLANT.PP_DYNAMIC_FILTER ( FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE ) values (99, 'Asset ID', 'dw', 'tax_record_control.asset_id', '', 'dw_tax_asset_by_version_filter', 'asset_id', 'asset_id', 'N', 0, 0, '', '', user, sysdate, 0, 1, 0 );
insert into PWRPLANT.PP_DYNAMIC_FILTER ( FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE ) values (100, 'Tax Layer', 'dw', 'tax_record_control.tax_layer_id', '', 'dw_tax_layer_by_version_filter', 'tax_layer_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );

--
-- Create a new filter to use for Individual Asset Depreciation.
--
insert into PWRPLANT.PP_REPORTS_FILTER ( PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME ) values ( 49, sysdate, user, 'PowerTax - Asset Grid - Indiv Depr', 'tax_record_control', 'uo_ppbase_tab_filter_dynamic' );

insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 101, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 102, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 103, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 104, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 107, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 108, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 109, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 110, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 99, user, sysdate );
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING ( PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP ) values ( 49, 100, user, sysdate );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (916, 0, 10, 4, 2, 0, 29593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029593_pwrtax_filtering.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;