/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052875_lessee_03_tax_calc_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 02/05/2018 Sarah Byers    Drop existing ls_monthly_tax and replace with the conversion table
||============================================================================
*/

-- Drop Current table
drop table ls_monthly_tax;

-- Rename conversion table
alter table ls_monthly_tax_conversion rename to ls_monthly_tax;

-- Rename conversion table pk
alter table ls_monthly_tax rename constraint pk_ls_monthly_tax_conversion to pk_ls_monthly_tax;

-- Re-create foreign keys for ls_monthly_tax
alter table ls_monthly_tax
add constraint fk_ls_monthly_tax1
foreign key (ls_asset_id)
references ls_asset (ls_asset_id);

alter table ls_monthly_tax
add constraint fk_ls_monthly_tax2
foreign key (set_of_books_id)
references set_of_books (set_of_books_id);

alter table ls_monthly_tax
add constraint fk_ls_monthly_tax3
foreign key (tax_local_id)
references ls_tax_local (tax_local_id);

-- Drop other conversion tables
drop table ls_tax_conv_pay_terms;
drop table ls_tax_conv_pay_months;
drop table ls_tax_conv_accrue_months;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13068, 0, 2018, 2, 0, 0, 52875, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052875_lessee_03_tax_calc_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;