/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007349_projects_rev2.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   12/09/2011 Chris Mardis   Point Release
||============================================================================
*/

update PP_ANY_QUERY_CRITERIA
set SQL =
   'select to_woc.work_order_number to_funding_project,'||chr(13)||chr(10)||
   '  to_woc.description to_fp_description,'||chr(13)||chr(10)||
   '  from_woc.work_order_number from_funding_project,'||chr(13)||chr(10)||
   '  from_woc.description from_fp_description,'||chr(13)||chr(10)||
   '  bs.to_dollars amount,'||chr(13)||chr(10)||
   '  psu.last_name||'', ''||psu.first_name initiated_by,'||chr(13)||chr(10)||
   '  psu2.last_name||'', ''||psu2.first_name pending_approval_by,'||chr(13)||chr(10)||
   '  bs.budget_sub_id budget_sub_id'||chr(13)||chr(10)||
   'from budget_substitutions bs,'||chr(13)||chr(10)||
   '  work_order_control from_woc,'||chr(13)||chr(10)||
   '  work_order_control to_woc,'||chr(13)||chr(10)||
   '  pp_security_users psu,'||chr(13)||chr(10)||
   '  pp_security_users psu2,'||chr(13)||chr(10)||
   '  workflow w,'||chr(13)||chr(10)||
   '  workflow_detail wd,'||chr(13)||chr(10)||
   '  company from_c,'||chr(13)||chr(10)||
   '  company to_c'||chr(13)||chr(10)||
   'where bs.from_work_order_id = from_woc.work_order_id'||chr(13)||chr(10)||
   'and bs.to_work_order_id = to_woc.work_order_id'||chr(13)||chr(10)||
   'and lower(bs.initiated_user) = lower(psu.users)'||chr(13)||chr(10)||
   'and bs.budget_sub_id = to_number(w.id_field1)'||chr(13)||chr(10)||
   'and w.workflow_id = wd.workflow_id'||chr(13)||chr(10)||
   'and lower(wd.users) = lower(psu2.users)'||chr(13)||chr(10)||
   'and from_woc.company_id = from_c.company_id'||chr(13)||chr(10)||
   'and to_woc.company_id = to_c.company_id'||chr(13)||chr(10)||
   'and bs.status = 3'||chr(13)||chr(10)||
   'and upper(w.subsystem) = ''SUBS_REVIEW'''||chr(13)||chr(10)||
   'and wd.approval_status_id = 2'||chr(13)||chr(10)||
   'order by bs.budget_sub_id, to_woc.work_order_number'
where DESCRIPTION = 'Pending Substitutions';

update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_WIDTH = 600
 where DETAIL_FIELD = 'to_funding_project'
   and ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Pending Substitutions');
update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_WIDTH = 900
 where DETAIL_FIELD = 'to_fp_description'
   and ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Pending Substitutions');
update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_WIDTH = 600
 where DETAIL_FIELD = 'from_funding_project'
   and ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Pending Substitutions');
update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_WIDTH = 900
 where DETAIL_FIELD = 'from_fp_description'
   and ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Pending Substitutions');
update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_WIDTH = 600
 where DETAIL_FIELD = 'amount'
   and ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Pending Substitutions');
update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_WIDTH = 750
 where DETAIL_FIELD = 'initiated_by'
   and ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Pending Substitutions');
update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_WIDTH = 750
 where DETAIL_FIELD = 'pending_approval_by'
   and ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Pending Substitutions');
update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_WIDTH = 450
 where DETAIL_FIELD = 'budget_sub_id'
   and ID = (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION = 'Pending Substitutions');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (66, 0, 10, 3, 3, 0, 7349, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007349_projects_rev2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
