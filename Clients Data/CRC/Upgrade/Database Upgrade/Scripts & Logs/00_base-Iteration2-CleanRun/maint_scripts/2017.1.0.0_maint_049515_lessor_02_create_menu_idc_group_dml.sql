/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049515_lessor_02_create_menu_idc_group_dml.sql.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/09/2017 Anand R        Add new menu under Admin for Indirect cost group
||============================================================================
*/

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'admin_initial_direct_cost_group', 'Initial Direct Cost Group', 'uo_lsr_admincntr_idc_group_wksp', NULL, 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_initial_direct_cost_group', 2, 8, 'Initial Direct Cost Group', NULL, 'menu_wksp_admin', 'admin_initial_direct_cost_group', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3919, 0, 2017, 1, 0, 0, 49515, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049515_lessor_02_create_menu_idc_group_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;