/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045081_fp_selection_grid_from_bud_version_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2.0 10/14/2015 Chris Mardis   Prime the FP Selection Grids when drilling from the BV Details screen to the same as the regular FP selection grids.
 ||============================================================================
 */

insert into select_tabs_grids (dw_name, subsystem, description)
select dw_name, 'fp_bv_filter' subsystem, description
from select_tabs_grids
where subsystem = 'funding_project';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2925, 0, 2015, 2, 0, 0, 45081, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045081_fp_selection_grid_from_bud_version_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;