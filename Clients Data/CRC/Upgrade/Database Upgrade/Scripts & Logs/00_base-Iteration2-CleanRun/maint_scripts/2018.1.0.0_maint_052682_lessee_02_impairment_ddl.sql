/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052682_lessee_02_impairment_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.1.0.0 12/03/18   Shane "C" Ward   Remove unecessary columns from Impairment Table
||============================================================================
*/

alter table ls_ilr_impair 
      drop primary key 
      drop index
;

ALTER TABLE ls_ilr_impair 
      ADD CONSTRAINT ls_ilr_impair_pk 
      PRIMARY KEY (ilr_id, set_of_books_id, month)
      USING INDEX TABLESPACE pwrplant_idx
;

ALTER TABLE ls_ilr_impair DROP COLUMN orig_revision;
ALTER TABLE ls_ilr_impair DROP COLUMN processed_revision;
ALTER TABLE ls_ilr_impair DROP COLUMN processed_flag;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12563, 0, 2018, 1, 0, 0, 52682, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052682_lessee_02_impairment_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;