/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046770_pwrtax_plant_recon_form_status_col_resize_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 11/14/2016 Anand R        Increase size of message from 254 to 2000
||
||============================================================================
*/

ALTER TABLE TAX_PLANT_RECON_FORM_STATUS
MODIFY (MESSAGE VARCHAR2(2000) );


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3337, 0, 2016, 1, 0, 0, 046770, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046770_pwrtax_plant_recon_form_status_col_resize_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;