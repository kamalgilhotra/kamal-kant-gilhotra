/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045356_reg_case_security_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 02/17/2016 Shane Ward  	Case Security for Regulatory
||============================================================================
*/

ALTER TABLE reg_case ADD security_type NUMBER(22,0);

comment on column reg_case.security_type is 'Security Type Assinged to Case (0= Public  1 = Private )';

CREATE TABLE reg_case_security_user(
reg_case_id NUMBER(22,0),
assigned_User VARCHAR2(18),
user_id VARCHAR2(18),
time_stamp date );

comment on table reg_case_security_user is 'Table holds user assignment to cases with Private (1) as the security type on reg_case';
comment on column reg_case_security_user.reg_case_id is 'System assigned ID of Case';
comment on column reg_case_security_user.assigned_user is 'Username assigned to Case';
COMMENT ON COLUMN reg_case_security_user.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN reg_case_security_user.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

-- Add the primary key
ALTER TABLE reg_case_security_user
ADD CONSTRAINT pk_reg_case_security_user PRIMARY KEY (
reg_case_id, assigned_user
)
USING INDEX
TABLESPACE pwrplant_idx
;

-- Add the foreign keys
ALTER TABLE reg_case_security_user
ADD CONSTRAINT fk_reg_case_security_user1 FOREIGN KEY (
reg_case_id
) REFERENCES reg_case(
reg_case_id
)
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3061, 0, 2016, 1, 0, 0, 045356, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045356_reg_case_security_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;