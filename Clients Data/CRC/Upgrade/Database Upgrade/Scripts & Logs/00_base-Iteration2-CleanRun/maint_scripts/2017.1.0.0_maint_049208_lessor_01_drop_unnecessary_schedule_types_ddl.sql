/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049208_lessor_01_drop_unnecessary_schedule_types_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/13/2017 Andrew Hill    Drop types no longer used for lessor ilr schedule
||============================================================================
*/

DROP TYPE lsr_ilr_op_sch_result_full_tab;
DROP TYPE lsr_ilr_op_sch_result_full;
DROP TYPE lsr_ilr_sales_sch_res_full_tab;
DROP TYPE lsr_ilr_sales_sch_res_full;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3792, 0, 2017, 1, 0, 0, 49208, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049208_lessor_01_drop_unnecessary_schedule_types_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;