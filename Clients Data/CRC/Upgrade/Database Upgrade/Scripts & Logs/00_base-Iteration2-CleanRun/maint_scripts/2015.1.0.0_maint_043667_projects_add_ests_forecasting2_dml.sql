/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043667_projects_add_ests_forecasting2_dml.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     04/25/2015 Chris Mardis   Additional estimate attributes requires flexible number of detail levels
||============================================================================
*/

update wo_est_forecast_options
set subtotal_details = to_char(subtotal_detail_1) ||
   decode(detail_level2,'N/A',null,'||'||to_char(subtotal_detail_2)) ||
   decode(detail_level3,'N/A',null,'||'||to_char(subtotal_detail_3)) ||
   decode(detail_level4,'N/A',null,'||'||to_char(subtotal_detail_4)) ||
   decode(detail_level5,'N/A',null,'||'||to_char(subtotal_detail_5));
update wo_est_forecast_options
set detail_levels = detail_levels ||
   decode(detail_level2,'N/A',null,'||'||detail_level2) ||
   decode(detail_level3,'N/A',null,'||'||detail_level3) ||
   decode(detail_level4,'N/A',null,'||'||detail_level4) ||
   decode(detail_level5,'N/A',null,'||'||detail_level5);
update wo_est_forecast_options
set detail_level2 = null,
   detail_level3 = null,
   detail_level4 = null,
   detail_level5 = null;
update wo_est_forecast_options
set subtotal_detail_1 = null,
   subtotal_detail_2 = null,
   subtotal_detail_3 = null,
   subtotal_detail_4 = null,
   subtotal_detail_5 = null;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2528, 0, 2015, 1, 0, 0, 43667, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043667_projects_add_ests_forecasting2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;