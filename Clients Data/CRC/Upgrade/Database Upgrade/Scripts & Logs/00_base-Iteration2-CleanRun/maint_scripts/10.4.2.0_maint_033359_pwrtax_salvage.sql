/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033359_pwrtax_salvage.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/30/2013 Nathan Hollis
||============================================================================
*/

alter table UTILITY_ACCOUNT_DEPRECIATION add ACTUAL_RESERVE_CREDITS number(22,2) DEFAULT 0;

comment on column UTILITY_ACCOUNT_DEPRECIATION.ACTUAL_RESERVE_CREDITS is 'As part of maint 33359, we are splitting salvage into three columns from two columns';

insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
values
   ((select max(CONTROL_ID) + 1 from PP_SYSTEM_CONTROL), 'Treat Salvage_Cash as Salvage', 1,
    'dw_yes_no;1',
    'Specifies whether Salvage_Cash will be factored into salvage or depreciation calculation.', -1);

insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
values
   ((select max(CONTROL_ID) + 1 from PP_SYSTEM_CONTROL), 'Treat Salvage_Returns as Salvage', 1,
    'dw_yes_no;1',
    'Specifies whether Salvage_Returns will be factored into salvage or depreciation calculation.',
    -1);

insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
values
   ((select max(CONTROL_ID) + 1 from PP_SYSTEM_CONTROL), 'Treat Reserve_Credits as Salvage', 1,
    'dw_yes_no;1',
    'Specifies whether Reserve_Credits will be factored into salvage or depreciation calculation.',
    -1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (711, 0, 10, 4, 2, 0, 33359, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033359_pwrtax_salvage.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
