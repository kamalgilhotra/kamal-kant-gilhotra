/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048371_lease_gl_acct_bal_query_add_multicurrency_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/26/2017 Josh Sandler     Update Lease GL Account Balances by Asset for Multicurrency in Company currency
||============================================================================
*/

UPDATE pp_any_query_criteria_fields
SET column_order = column_order + 2
WHERE id = (SELECT id FROM pp_any_query_criteria WHERE description = 'Lease GL Account Balances by Asset')
AND column_order >= 6;

INSERT INTO pp_any_query_criteria_fields
  (id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field, hide_from_results, hide_from_filters)
VALUES
  ((SELECT id FROM pp_any_query_criteria WHERE description = 'Lease GL Account Balances by Asset'), 'currency_display_symbol', 6, 0, 0, 'Currency Symbol', 300, 'VARCHAR2', 0, 1, 1)
;

INSERT INTO pp_any_query_criteria_fields
  (id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field, hide_from_results, hide_from_filters)
VALUES
  ((SELECT id FROM pp_any_query_criteria WHERE description = 'Lease GL Account Balances by Asset'), 'currency', 7, 0, 0, 'Currency', 300, 'VARCHAR2', 0, 0, 1)
;

UPDATE pp_any_query_criteria
SET description = 'Lease GL Account Balances by Asset (Company Currency)',
SQL = 'select ls_asset_id,
       company.description as company_description,
       company.company_id as company_id,
       (select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM'') as monthnum,
       gl.external_account_code,
       c.currency_display_symbol,
       c.iso_code AS currency,
       nvl(a.beg_cost,0) as beginning_cost,
       nvl(a.current_mo_adds, 0) as additions,
       nvl(a.current_mo_retirements,0) as retirements,
       nvl(a.current_mo_trf_to,0) as transfers_to,
       nvl(a.current_mo_trf_from,0) as transfers_from,
       nvl(a.end_cost,0) as ending_cost,
       decode(union_group, 2, sum(a.end_cost) over(partition by ls_asset_id),0) as GAAP
from gl_account gl, company, currency c, currency_schema cs,
     (select ls_asset.ls_asset_id, company.company_id, cpr.gl_account_id,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), 0,activity_cost)) beg_cost,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             decode(trim(activity_code), ''UADD'', activity_cost,0),0)) current_mo_adds,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             case when trim(activity_code) in (''URGL'', ''URET'') THEN  activity_cost ELSE 0 END,0)) current_mo_retirements,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             decode(trim(activity_code), ''UTRT'', activity_cost,0),0)) current_mo_trf_to,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             decode(trim(activity_code), ''UTRF'', activity_cost,0),0)) current_mo_trf_from,
             sum(cpra.activity_cost) end_cost,
             1 as union_group
      from cpr_activity cpra, ls_cpr_asset_map map, ls_asset, company, cpr_ledger cpr, ls_ilr, ls_lease
      where ls_asset.company_id = company.company_id
        and cpra.asset_id = map.asset_id
        and cpra.asset_id = cpr.asset_id
        and map.ls_asset_id = ls_asset.ls_asset_id
		and ls_asset.ilr_id = ls_ilr.ilr_id
		and ls_ilr.lease_id = ls_lease.lease_id
        and (ls_asset.ls_asset_status_id = 3
             or (ls_asset.ls_asset_status_id = 4 and ls_asset.retirement_date >=to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''), ''yyyymm'')))
        and cpra.gl_posting_mo_yr <= to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
        and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
       group by ls_asset.ls_asset_id,company.company_id, cpr.gl_account_id
UNION ALL
/****************************************************************************/',
sql2 = 'select ls_asset.ls_asset_id,company.company_id, ilra.st_oblig_account_id,
       -1 * nvl(sum(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_obligation - las.end_lt_obligation)),0) as beg_cost,
       -1 * sum(case when (las.month=to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''), ''yyyymm'') and nvl(is_trf,0)=0 and nvl(is_trft,0) = 0 and nvl(ls_asset.retirement_date, to_date(1500,''yyyy'')) <> las.month)
                                      then (las.end_obligation - las.end_lt_obligation) - decode(las.beg_capital_cost,0,0, las.beg_obligation - las.beg_lt_obligation)else 0 end) as current_mo_adds,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and las.month = ls_asset.retirement_date and nvl(is_trf,0) <> 1)
                                      then (las.end_obligation - las.end_lt_obligation) - decode(las.beg_capital_cost,0,0, las.beg_obligation - las.beg_lt_obligation) else 0 end) as current_mo_retirements,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trft,0) = 1)
                                      then (las.end_obligation - las.end_lt_obligation) - decode(las.beg_capital_cost,0,0, las.beg_obligation - las.beg_lt_obligation) else 0 end) as current_mo_trf_to,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and las.month = ls_asset.retirement_date and nvl(is_trf,0) = 1)
                                      then (las.end_obligation - las.end_lt_obligation) - decode(las.beg_capital_cost,0,0, las.beg_obligation - las.beg_lt_obligation) else 0 end) as current_mo_trf_from,
       -1 * sum(decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), (las.end_obligation - las.end_lt_obligation))) as end_cost,
       2 as union_group
    from ls_ilr_account ilra, ls_ilr, ls_asset, v_ls_asset_schedule_fx_vw las, ls_ilr_options ilro, ls_lease_cap_type lct, company, ls_lease,
        (select a.*, 1 is_trf from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trf,
        (select a.*, 1 is_trft from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trft
    where las.ls_asset_id = ls_asset.ls_asset_id
      and las.revision = ls_asset.approved_revision
      and ls_asset.ilr_id = ls_ilr.ilr_id
      and ls_ilr.ilr_id = ilra.ilr_id
      and ls_ilr.ilr_id = ilro.ilr_id
      and ls_ilr.current_revision = ilro.revision
	  and ls_ilr.lease_id = ls_lease.lease_id
      and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
      and lct.is_om = 0
      and ls_asset.company_id = company.company_id
      and las.month in (to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1))
      and ls_asset.ls_asset_id = trf.from_ls_asset_id (+)
      and ls_asset.ls_asset_id = trft.to_ls_asset_id (+)
      and ls_asset.ls_asset_status_id >=3
      and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      AND las.ls_cur_type = 2
  group by ls_asset.ls_asset_id,ilra.st_oblig_account_id, company.company_id
UNION ALL
/****************************************************************************/',
sql3 = '      select ls_asset.ls_asset_id,company.company_id, ilra.lt_oblig_account_id,
       -1 * nvl(sum(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_obligation,0)),0) as beg_cost,
       -1 * sum(case when (las.month=to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''), ''yyyymm'') and nvl(is_trf,0)=0 and nvl(is_trft,0) = 0 and nvl(ls_asset.retirement_date, to_date(1500,''yyyy'')) <> las.month)
                                      then las.end_lt_obligation - decode(las.beg_capital_cost,0,0, las.beg_lt_obligation)else 0 end) as current_mo_adds,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and las.month = ls_asset.retirement_date and nvl(is_trf,0) <> 1)
                                      then las.end_lt_obligation - decode(las.beg_capital_cost,0,0, las.beg_lt_obligation) else 0 end) as current_mo_retirements,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trft,0) = 1)
                                      then las.end_lt_obligation - decode(las.beg_capital_cost,0,0, las.beg_lt_obligation) else 0 end) as current_mo_trf_to,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and las.month = ls_asset.retirement_date and nvl(is_trf,0) = 1)
                                      then las.end_lt_obligation - decode(las.beg_capital_cost,0,0,las.beg_lt_obligation) else 0 end) as current_mo_trf_from,
       -1 * nvl(sum(decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_lt_obligation)),0) as end_cost,
       3 as union_group
    from ls_ilr_account ilra, ls_ilr, ls_asset, v_ls_asset_schedule_fx_vw las, ls_ilr_options ilro, ls_lease_cap_type lct, company, ls_lease,
        (select a.*, 1 is_trf from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trf,
        (select a.*, 1 is_trft from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trft
    where las.ls_asset_id = ls_asset.ls_asset_id
      and las.revision = ls_asset.approved_revision
      and ls_asset.ilr_id = ls_ilr.ilr_id
      and ls_ilr.ilr_id = ilra.ilr_id
      and ls_ilr.ilr_id = ilro.ilr_id
	  and ls_ilr.lease_id = ls_lease.lease_id
      and ls_ilr.current_revision = ilro.revision
      and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
      and lct.is_om = 0
      and ls_asset.company_id = company.company_id
      and las.month in (to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1))
      and ls_asset.ls_asset_id = trf.from_ls_asset_id (+)
      and ls_asset.ls_asset_id = trft.to_ls_asset_id (+)
      and ls_asset.ls_asset_status_id >=3
      and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      AND las.ls_cur_type = 2
  group by ls_asset.ls_asset_id,ilra.lt_oblig_account_id, company.company_id
UNION ALL
/****************************************************************************/',
sql4 = 'select ls_asset.ls_asset_id,company.company_id, dg.reserve_acct_id,
       -1 *  nvl(sum(nvl(cprd.beg_reserve_month,0)),0) as beg_cost,
       -1 * sum(nvl(curr_depr_expense,0) + nvl(depr_exp_alloc_adjust,0)) as current_mo_adds,
       -1 * sum(nvl(retirements,0) + nvl(gain_loss,0)) as current_mo_retirements,
       -1 * sum(nvl(reserve_trans_in,0)) as current_mo_trf_to,
       -1 * sum(nvl(reserve_trans_out,0)) as current_mo_trf_from,
       -1 * sum(nvl(depr_reserve,0)) as end_cost,
       4 as union_group
from company, cpr_depr cprd, cpr_ledger cpr, ls_cpr_asset_map map, depr_group dg, ls_asset, ls_ilr, ls_lease
where company.company_id = cpr.company_id
  and map.asset_id = cpr.asset_id
  and cpr.asset_id = cprd.asset_id
  and cprd.gl_posting_mo_yr = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
  and cpr.depr_group_id = dg.depr_group_id
  and ls_asset.ls_asset_id = map.ls_asset_id
  and ls_asset.ilr_id = ls_ilr.ilr_id
   and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  and ls_ilr.lease_id = ls_lease.lease_id
group by ls_asset.ls_asset_id,company.company_id, dg.reserve_acct_id
) a
where a.gl_account_id = gl.gl_account_id
  and a.company_id = company.company_id
  and cs.company_id = company.company_id
  and cs.currency_id = c.currency_id
  and cs.currency_type_id = 1
order by external_account_code'
WHERE id = (SELECT id FROM pp_any_query_criteria WHERE description = 'Lease GL Account Balances by Asset');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3610, 0, 2017, 1, 0, 0, 48371, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048371_lease_gl_acct_bal_query_add_multicurrency_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;