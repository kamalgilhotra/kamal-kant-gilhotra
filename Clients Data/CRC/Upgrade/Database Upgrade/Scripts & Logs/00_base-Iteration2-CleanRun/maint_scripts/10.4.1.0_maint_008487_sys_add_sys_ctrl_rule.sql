/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008487_sys_add_sys_ctrl_rule.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/12/2013 David Nichols  Point Release
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_VALUE, CONTROL_NAME, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
values
   ((select max(CONTROL_ID) from PP_SYSTEM_CONTROL_COMPANY) + 1, 'yes',
    'wocomplete - insrv < complete rule', 'dw_yes_no;1',
    'use system control to determine whether to allow in_service_date to be > completion_date', -1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (503, 0, 10, 4, 1, 0, 8487, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_008487_sys_add_sys_ctrl_rule.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

