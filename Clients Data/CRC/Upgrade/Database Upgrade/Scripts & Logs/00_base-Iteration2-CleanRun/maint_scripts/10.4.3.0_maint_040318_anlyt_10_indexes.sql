set serveroutput on;
/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_anlyt_10_indexes.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
||  1.0     09/06/2013 Scott Moody      Initial
|| 10.4.3.0 10/24/2014 Chad Theilman    Asset Analytics - Indexes
||============================================================================
*/

declare 
  doesIndexExist number := 0;
  
  begin
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_LOCATION_STATE','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_LOCATION_STATE ON PWRANLYT.PA_LOCATIONS (STATE)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_LOCATION_COUNTY','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_LOCATION_COUNTY ON PWRANLYT.PA_LOCATIONS (COUNTY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_COMP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_LDGR_COMP ON PWRANLYT.PA_CPR_LEDGER(COMPANY_ID)';
		end;
	end if;
	
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_LOC','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_LDGR_LOC ON PWRANLYT.PA_CPR_LEDGER(ASSET_LOCATION_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_ACCT','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_LDGR_ACCT ON PWRANLYT.PA_CPR_LEDGER(ACCT_KEY)';
		end;
	end if;
	
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_PROP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_LDGR_PROP ON PWRANLYT.PA_CPR_LEDGER(PROP_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_DEPR','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_LDGR_DEPR ON PWRANLYT.PA_CPR_LEDGER(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_VY','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_LDGR_VY ON PWRANLYT.PA_CPR_LEDGER(VINTAGE_YEAR)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_EF','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_LDGR_EF ON PWRANLYT.PA_CPR_LEDGER(EXCLUDE_FLAG)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_LOCN','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_LDGR_LOCN ON PWRANLYT.PA_CPR_LEDGER(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_COMP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_VINTSPR_COMP ON PWRANLYT.PA_VINTAGE_SPREADS(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_ACCT','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_VINTSPR_ACCT ON PWRANLYT.PA_VINTAGE_SPREADS(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_LOCN','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_VINTSPR_LOCN ON PWRANLYT.PA_VINTAGE_SPREADS(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_DEPR','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_VINTSPR_DEPR ON PWRANLYT.PA_VINTAGE_SPREADS(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_VY','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_VINTSPR_VY ON PWRANLYT.PA_VINTAGE_SPREADS(VINTAGE_YEAR)';
		end;	
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_COMP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_COMP ON PWRANLYT.PA_CPR_ACTIVITY(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_LOC','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_LOC ON PWRANLYT.PA_CPR_ACTIVITY(ASSET_LOCATION_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_LOCN','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_LOCN ON PWRANLYT.PA_CPR_ACTIVITY(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_ACCT','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_ACCT ON PWRANLYT.PA_CPR_ACTIVITY(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_PROP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_PROP ON PWRANLYT.PA_CPR_ACTIVITY(PROP_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_DEPR','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_DEPR ON PWRANLYT.PA_CPR_ACTIVITY(DEPR_GROUP_ID)';
		end;
	end if;
	
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_GL_POST','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_GL_POST ON PWRANLYT.PA_CPR_ACTIVITY(GL_POSTING_DATE)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_FERC','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_FERC ON PWRANLYT.PA_CPR_ACTIVITY(FERC_ACTIVITY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_EF','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACT_EF ON PWRANLYT.PA_CPR_ACTIVITY(EXCLUDE_FLAG)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_COMP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACTSUM_COMP ON PWRANLYT.PA_CPR_ACTSUM(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_LOCN','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACTSUM_LOCN ON PWRANLYT.PA_CPR_ACTSUM(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_ACCT','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACTSUM_ACCT ON PWRANLYT.PA_CPR_ACTSUM(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_DEPR','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACTSUM_DEPR ON PWRANLYT.PA_CPR_ACTSUM(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_GL_POST','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACTSUM_GL_POST ON PWRANLYT.PA_CPR_ACTSUM(GL_POSTING_YEAR)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_FERC','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_CPR_ACTSUM_FERC ON PWRANLYT.PA_CPR_ACTSUM(FERC_ACTIVITY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_COMP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_AVGAGES_COMP ON PWRANLYT.PA_AVERAGE_AGES(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_LOC','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_AVGAGES_LOC ON PWRANLYT.PA_AVERAGE_AGES(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_ACCT','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_AVGAGES_ACCT ON PWRANLYT.PA_AVERAGE_AGES(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_DEPR','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_AVGAGES_DEPR ON PWRANLYT.PA_AVERAGE_AGES(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_ASOF','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_AVGAGES_ASOF ON PWRANLYT.PA_AVERAGE_AGES(PROCESS_YEAR)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_COMP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_CPRBAL_COMP ON PWRANLYT.PA_CPR_BALANCES(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_LOC','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_CPRBAL_LOC ON PWRANLYT.PA_CPR_BALANCES(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_ACCT','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_CPRBAL_ACCT ON PWRANLYT.PA_CPR_BALANCES(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_DEPR','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_CPRBAL_DEPR ON PWRANLYT.PA_CPR_BALANCES(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_ASOF','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_CPRBAL_ASOF ON PWRANLYT.PA_CPR_BALANCES(PROCESS_YEAR)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_RETWO_WONUM','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_RETWO_WONUM ON PWRANLYT.PA_RETIRE_WO(WO_NUMBER)';  
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_RETWO_COMP','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_RETWO_COMP ON PWRANLYT.PA_RETIRE_WO(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_RETWO_BSEG','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_RETWO_BSEG ON PWRANLYT.PA_RETIRE_WO(BUS_SEGMENT)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_RETWO_STAT','PWRANLYT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRANLYT.IDX_PA_RETWO_STAT ON PWRANLYT.PA_RETIRE_WO(WO_STATUS)';
		end;
	end if;
  end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1575, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_anlyt_10_indexes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

