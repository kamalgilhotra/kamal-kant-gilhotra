 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045705_pwrtax_tax_include_activity_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 05/12/2016 Rob Burns      Initial script to switch these to description in Table Maintenance
 ||============================================================================
 */ 

update powerplant_columns set pp_edit_type_id = 'p', dropdown_name = 'tax_include' where column_name = 'tax_include_id' and table_name = 'tax_include_activity';
update powerplant_columns set pp_edit_type_id = 'p', dropdown_name = 'tax_book' where column_name = 'tax_book_id' and table_name = 'tax_include_activity';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3192, 0, 2016, 1, 0, 0, 045705, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045705_pwrtax_tax_include_activity_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;