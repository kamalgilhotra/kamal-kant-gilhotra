/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_20_component_import_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

update pp_import_template_fields set field_id = field_id+1
where import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_component')
;

insert into pp_import_column(import_type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_component') AS import_type_id,
   'unique_component_identifier' AS column_name,
   'Unique Component Identifier' AS description,
   1 AS is_required,
   1 AS processing_order,
   'varchar2(35)' AS column_type,
   1 AS is_on_table
from dual
union
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_component') AS import_type_id,
   'invoice_number' AS column_name,
   'Invoice Number' AS description,
   0 AS is_required,
   1 AS processing_order,
   'varchar2(35)' AS column_type,
   1 AS is_on_table
from dual
union
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_component') AS import_type_id,
   'invoice_date' AS column_name,
   'Invoice Date' AS description,
   0 AS is_required,
   1 AS processing_order,
   'varchar2(35)' AS column_type,
   1 AS is_on_table
from dual
union
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_component') AS import_type_id,
   'invoice_amount' AS column_name,
   'Invoice Amount' AS description,
   0 AS is_required,
   1 AS processing_order,
   'number(22,2)' AS column_type,
   1 AS is_on_table
from dual
;
insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name)
select
pit.import_template_id, 1, pit.import_type_id, 'unique_component_identifier'
from pp_import_template pit
where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_component')
union
select
pit.import_template_id, 12, pit.import_type_id, 'invoice_number'
from pp_import_template pit
where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_component')
union
select
pit.import_template_id, 13, pit.import_type_id, 'invoice_date'
from pp_import_template pit
where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_component')
union
select
pit.import_template_id, 14, pit.import_type_id, 'invoice_amount'
from pp_import_template pit
where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_component')
;

insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
select
   import_type_id,
   f.column_name,
   il.import_lookup_id
from pp_import_template_fields f, pp_import_lookup il
where f.column_name = 'company_id'
and import_type_id in (select import_type_id from pp_import_type where import_table_name like 'ls%')
and f.column_name = il.column_name
and il.lookup_constraining_columns = 'is_lease_company';

update pp_Import_lookup set lookup_sql = replace(lookup_sql,'and is_lease_company = 1 ',''),
lookup_constraining_columns = null
where lookup_constraining_columns = 'is_lease_company'
;

commit;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2398, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_20_component_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;