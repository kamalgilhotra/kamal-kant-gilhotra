/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038605_lease_tax_district_table_maint.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.3.0 06/19/2014 Kyle Peterson
||========================================================================================
*/

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY,
    SUBSYSTEM)
values
   ('tax_district', 's', 'Tax District Admin', '', 'lessee', '', 'lessee', 'lessee');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('tax_district_id', 'tax_district', '', 's', 'Tax District', 1, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('county_id', 'tax_district', 'county', 'p', 'County', 2, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('state_id', 'tax_district', 'state', 'p', 'State', 3, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('description', 'tax_district', '', 'e', 'Description', 4, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('long_description', 'tax_district', '', 'e', 'Long Description', 5, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('time_stamp', 'tax_district', '', 'e', 'Time Stamp', 6, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('user_id', 'tax_district', '', 'e', 'User', 7, 0);

insert into PP_TABLE_GROUPS (TABLE_NAME, GROUPS) values ('tax_district', 'system');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1218, 0, 10, 4, 3, 0, 38605, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038605_lease_tax_district_table_maint.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
