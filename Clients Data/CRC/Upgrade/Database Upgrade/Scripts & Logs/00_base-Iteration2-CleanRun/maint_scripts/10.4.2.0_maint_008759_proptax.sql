/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008759_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By      Reason for Change
|| -------- ---------- --------------- ---------------------------------------
|| 10.4.2.0 12/20/2013 Julia Breuer    Point Release
||============================================================================
*/

insert into PPBASE_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Bills Center - Copy - Mark Bills as Estimated Default', sysdate, user, 'When copying bills to another year, whether the mark bills as ''Estimated'' option is selected by default or not.', 0, 'No', null, 1 );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Mark Bills as Estimated Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Mark Bills as Estimated Default', 'Yes', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Mark Bills as Estimated Default', 'No', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Mark Bills as Estimated Default', 'bills_search', sysdate, user );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (815, 0, 10, 4, 2, 0, 8759, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_008759_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;