/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048808_lessor_02_populate_payment_month_freq_col_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/21/2017 Charlie Shilling populate new database column
||============================================================================
*/
--update the payment months frequency
UPDATE ls_payment_freq
SET payment_month_freq =
	Decode(Lower(description),
		'annual', 12,
		'semi-annual', 6,
		'quarterly', 3,
		'monthly', 1,
		NULL)
;

DECLARE
	l_null_count 	INT;
BEGIN
	SELECT Count(1)
	INTO l_null_count
	FROM ls_payment_freq
	WHERE payment_month_freq IS NULL;

	IF l_null_count <> 0 THEN
		Raise_Application_Error(-20000, 'An unexpected entry exists in the LS_PAYMENT_FREQ table. Please remove payment frequencies other than ''Annual'', ''Semi-Annual'', ''Quarterly'', and ''Monthly''.');
	END IF ;
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3736, 0, 2017, 1, 0, 0, 48808, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048808_lessor_02_populate_payment_month_freq_col_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
