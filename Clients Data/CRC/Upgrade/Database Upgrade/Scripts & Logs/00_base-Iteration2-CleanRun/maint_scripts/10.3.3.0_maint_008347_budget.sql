/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008347_budget.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/17/2011 Chris Mardis   Point Release
||============================================================================
*/

--
-- Maint 8347 - Load Budget Dollars Maintenance
--

begin
   execute immediate 'drop table WO_EST_LOAD_BUDGETS_TEMP';
   DBMS_OUTPUT.PUT_LINE('Table WO_EST_LOAD_BUDGETS_TEMP dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table WO_EST_LOAD_BUDGETS_TEMP NOT dropped.');
end;
/

create global temporary table WO_EST_LOAD_BUDGETS_TEMP
(
 BUDGET_ID             number(22) not null,
 BUDGET_VERSION_ID     number(22) not null,
 OBEY_REVIEW           number(22),
 REVIEW_ANY_REVISION   number(22),
 UPDATE_HEADER_FROM_FP number(22)
) on commit preserve rows;

alter table WO_EST_LOAD_BUDGETS_TEMP
   add constraint PK_WO_EST_LOAD_BUD_TEMP
       primary key (BUDGET_ID, BUDGET_VERSION_ID);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (51, 0, 10, 3, 3, 0, 8347, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008347_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
