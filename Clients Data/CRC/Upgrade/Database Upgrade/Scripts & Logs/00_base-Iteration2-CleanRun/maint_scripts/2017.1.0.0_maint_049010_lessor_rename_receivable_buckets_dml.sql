/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049010_lessor_rename_receivable_buckets_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 09/18/2017 Shane "C" Ward   Fix naming of Receivable Buckets Menu Item
||============================================================================
*/
 
UPDATE ppbase_Menu_items SET label = 'Receivable Buckets' WHERE menu_identifier = 'admin_receive_bucket' and module = 'LESSOR';

UPDATE ppbase_workspace SET label = 'Receivable Buckets' WHERE workspace_identifier = 'admin_receivables_bucket' and module = 'LESSOR';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3719, 0, 2017, 1, 0, 0, 49010, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049010_lessor_rename_receivable_buckets_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;