/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030236_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.1.0 08/26/2013 Andrew Scott        Rename some Tax Repair Reports
||============================================================================
*/

----The following reports were specified by Blake to be disabled.
----RPR - 0210
----RPR - 0220
----RPR - 0230
----RPR - 0250
----RPR - 0260
----UOPWT - 3010
----UOPWT - 3020
----The other, older tax repairs reports were disabled by changing
----the pp_report_envir_id to equal 7 (All Hide), so follow this
----process.

update PP_REPORTS
   set PP_REPORT_ENVIR_ID = 7
 where REPORT_NUMBER in ('RPR - 0210',
                         'RPR - 0220',
                         'RPR - 0230',
                         'RPR - 0250',
                         'RPR - 0260',
                         'UOPWT - 3010',
                         'UOPWT - 3020')
   and NVL(PP_REPORT_ENVIR_ID, 0) <> 7
   and REPORT_TYPE_ID in
       (select REPORT_TYPE_ID from PP_REPORT_TYPE where DESCRIPTION like 'Tax Repair%');

----update other descriptions, long descriptions based on what Blake provided in his spreadsheet

update PP_REPORTS set DESCRIPTION = 'Book Additions and Tax Repairs', long_description = 'Shows Summary of added Assets and associated tax expense for a time span.  Tax Repairs entries must be posted to display in this report.' where report_number = 'RPR - 0010' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'Posted CPR Tax Repairs', long_description = 'Shows CPR tax expense with a tax origination month within the selected time span.  Displays the Tax Orig Month and the accounting month.  Tax Repairs entries must be posted to display in this report.' where report_number = 'RPR - 0030' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'CPR Additions by Batch', long_description = 'CPR Additions and Tax Expense Activity for Prior Years in a Tax Repairs Batch for work orders tax expense amount <> 0 (Only Posted status batches will show Tax Expense dollars in the CPR Tax Expense column.)' where report_number = 'RPR - 0040' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'CPR Additions by Tax Exp Test', long_description = 'CPR Additions and Tax Expense Activity for WOs Assigned to Tax Repairs Test for work orders with tax expense amount <> 0.  Tax Repairs entries must be posted to display in this report.' where report_number = 'RPR - 0050' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'CPR Additions', long_description = 'CPR Additions and Tax Expense Activity for a Time Span for work orders with tax expense amount <> 0.  Tax Repairs entries must be posted to display in this report.' where report_number = 'RPR - 0060' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'CWIP Roll Forward', long_description = 'Tax Repairs CWIP Roll Forward grouped by Company.  Tax Repairs entries must be posted to display in this report.' where report_number = 'RPR - 0080' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'CWIP/InService Analysis', long_description = 'Tax Repairs CWIP and In-Service Analysis for all work orders grouped by Company for a span of time.  Tax Repairs entries must be posted to display in this report.' where report_number = 'RPR - 0090' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'General/Blended Method Details', long_description = 'Tax Repair testing analysis for work orders that were tested using the General or Blended methods.  Tax Repairs entries must be posted to display in this report.' where report_number = 'RPR - 0190' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'General/Blended Method Expense', long_description = 'Tax Repair testing status and amounts for work orders that were tested using the General or Blended methods.  Tax Repairs entries must be posted to display in this report.' where report_number = 'RPR - 0200' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'General/Blended Method Status', long_description = 'General Method: Displays WOs that have changed Tax Status within the timeframe selected.' where report_number = 'GEN - 1330' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'Replacement Costs', long_description = 'General/Blended Method: Displays the replacement costs for each repair location and unit of property.' where report_number = 'GEN - 1350' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'Replacement Cost Changes', long_description = 'General/Blended Method: Displays the change in replacement costs for each repair location and unit of property.' where report_number = 'GEN - 1370' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP Run Results', long_description = 'UOP Tax Repairs:  Process Run Results for Non-Related Work Orders for the In Process Batch' where report_number = 'UOP - 2270' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP Related WO Run Results', long_description = 'UOP Tax Repairs:  Process Run Results for Related Work Orders for the In Process Batch' where report_number = 'UOP - 2280' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP:  Circuit Pretest', long_description = 'UOP Tax Repairs:  Circuit Threshold Pre-Test Results for Non-Related WOs for the In Process Batch' where report_number = 'UOP - 2290' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP: Related WO Circuit Pretest', long_description = 'UOP Tax Repairs:  Circuit Threshold Pre-Test Results for Related WOs for the In Process Batch' where report_number = 'UOP - 2300' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP: WO Pretest', long_description = 'UOP:  WO Threshold Pre-Test Results for Non-Related WOs for the In Process Batch' where report_number = 'UOP - 2310' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP: Related WO Pretest', long_description = 'UOP:  WO Threshold Pre-Test Results for Related WOs for the In Process Batch' where report_number = 'UOP - 2320' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP: Replacement Quantities', long_description = 'UOP: Displays the replacement quantities for each circuit and repair unit of property.' where report_number = 'UOP - 2340' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP: Replacement Qty Change', long_description = 'UOP: Displays the change in replacement quantities for each circuit and repair unit of property.' where report_number = 'UOP - 2360' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'UOP: Retirement Reversal Amount', long_description = 'UOP: Displays any retirement reversal dollars that have been recorded for a repair method.' where report_number = 'UOP - 2380' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'Alloc: Test Results', long_description = 'Blanket Network Repairs:  Part 1 of 2 Process Run Results for Blanket Work Orders using Pro-Rata or Proporional.  Displays quantity values tested for each work order number, repair unit, and location.' where report_number = 'ALLOC - 4010' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');
update PP_REPORTS set DESCRIPTION = 'Alloc: Run Results', long_description = 'Blanket Network Repairs:  Part 2 of 2 Process Run Results for Blanket Work Orders using Pro-Rata or Proporional.  Display dollar values for each work order and repair unit.' where report_number = 'ALLOC - 4020' and nvl(pp_report_envir_id,0) <> 7 and report_type_id in ( select report_type_id from pp_report_type where description like 'Tax Repair%');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (543, 0, 10, 4, 1, 0, 30236, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030236_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;