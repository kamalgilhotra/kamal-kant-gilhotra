/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043018_pcm_fcst_options_default_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/02/2015 Alex P.          Cannot save information into 'default' template
||============================================================================
*/

ALTER TABLE wo_est_fcst_options_details DISABLE CONSTRAINT R_WO_EST_FCST_OPTIONS_DETAILS1;
ALTER TABLE wo_est_fcst_options_version DISABLE CONSTRAINT R_WO_EST_FCST_OPTIONS_VERSION1;

update wo_est_fcst_options_details
set users = lower(users);

update wo_est_fcst_options_version
set users = lower(users);

update wo_est_forecast_options
set users = lower(users);

ALTER TABLE wo_est_fcst_options_details ENABLE CONSTRAINT R_WO_EST_FCST_OPTIONS_DETAILS1;
ALTER TABLE wo_est_fcst_options_version ENABLE CONSTRAINT R_WO_EST_FCST_OPTIONS_VERSION1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2334, 0, 2015, 1, 0, 0, 043018, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043018_pcm_fcst_options_default_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;