/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_043402_reg_recon_adjust_ddl.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version	Date	   Revised By	  Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 04/03/2015   Shane Ward	   Allow users to save adjustments when performing recon
||============================================================================
*/

CREATE GLOBAL TEMPORARY TABLE reg_recon_adjust_temp (
version_id NUMBER(22,0),
recon_id NUMBER(22,0),
gl_month NUMBER(22,0),
reg_company_id NUMBER(22,0),
recon_adj_status NUMBER(22,0),
recon_comment VARCHAR2(1000),
adj_amount NUMBER(22,0)) ON COMMIT DELETE rows;

comment on table reg_recon_adjust_temp is 'Temp table for storing recon adjustments when user chooses to save adjustments when reperforming recon';
comment on column reg_recon_adjust_temp.version_id is 'Historic or Forecast Version Id depending on which recon is being performed';
comment on column reg_recon_adjust_temp.recon_id is 'Id of Recon Item';
comment on column reg_recon_adjust_temp.gl_month is 'General Ledger Month on Recon Ledger';
comment on column reg_recon_adjust_temp.reg_company_id is 'System assigned identifier of Regulatory Company';
comment on column reg_recon_adjust_temp.recon_adj_status is 'Adjustment status of recon item for gl month and reg company (-1, 0, 1, 2)';
comment on column reg_recon_adjust_temp.recon_comment is 'User defined comment to give more information on adjustment';
comment on column reg_recon_adjust_temp.adj_amount is 'Amount of adjustment if adjustment is being performed on Recon Ledger and not Historic/Forecast Ledger';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2482, 0, 2015, 1, 0, 0, 43402, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043402_reg_recon_adjust_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;