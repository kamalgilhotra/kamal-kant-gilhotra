/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046493_pwrtax_add_source_reports_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 10/04/2016 Jared Watkins	Create source report relational table for
||                                      the plant recon workspace
||============================================================================
*/

create table tax_plant_recon_source_report (
    tax_pr_section_id   number(22,0) not null,
    sort_order          number(22,0) not null,
    recon_source        number(22,0) not null,
    source_report       varchar2(35) not null,
    time_stamp          date,
    user_id             varchar2(35)
)
/

ALTER TABLE tax_plant_recon_source_report
  ADD CONSTRAINT tx_plnt_recon_src_rpt_pk PRIMARY KEY (
    tax_pr_section_id,
    sort_order,
    recon_source
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

COMMENT ON TABLE tax_plant_recon_source_report IS '(S)[09] The Tax Plant Recon Source Report Table relates individual line items from the plant recon report/workspace to the source report for the data.';

COMMENT ON COLUMN tax_plant_recon_source_report.tax_pr_section_id IS 'System assigned identifier for a tax plant reconciliation section.';
COMMENT ON COLUMN tax_plant_recon_source_report.sort_order IS 'Display order for the summary items.';
COMMENT ON COLUMN tax_plant_recon_source_report.recon_source IS '1 = GL; 2 = PowerTax; 3 = PowerPlan';
COMMENT ON COLUMN tax_plant_recon_source_report.source_report IS 'PowerPlan report used as the data source for this line item';
COMMENT ON COLUMN tax_plant_recon_source_report.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN tax_plant_recon_source_report.user_id IS 'Standard system-assigned user id used for audit purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3305, 0, 2016, 1, 0, 0, 046493, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046493_pwrtax_add_source_reports_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;