/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045506_reg_inactive_acct_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 02/17/2016 Shane Ward  	Inactivate Case/Jur Accounts
||============================================================================
*/

--Add Status Code
ALTER TABLE reg_jur_acct_default ADD status_code_id NUMBER(22,0);

--Default all to active for now
UPDATE reg_jur_acct_default SET status_code_id = 1;

--Make it not null able
ALTER TABLE reg_jur_acct_default MODIFY status_code_id NUMBER(22,0) NOT null;

ALTER TABLE reg_jur_acct_default
  ADD CONSTRAINT r_reg_case_acct6 FOREIGN KEY (
    status_code_id
  ) REFERENCES status_code (
    status_code_id
  );
  
Comment on column reg_jur_acct_default.status_code_id is 'Identifier of whether an account is Active (1) or inactive (2)';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3085, 0, 2016, 1, 0, 0, 045506, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045506_reg_inactive_acct_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;