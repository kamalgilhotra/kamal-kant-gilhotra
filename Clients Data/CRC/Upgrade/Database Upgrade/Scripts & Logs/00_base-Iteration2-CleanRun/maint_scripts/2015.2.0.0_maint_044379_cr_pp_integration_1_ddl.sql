/*
||=============================================================================
|| Application: PowerPlant
|| File Name:   maint_044379_cr_pp_integration_1_ddl.sql
||=============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- -------------------------------------
|| 2015.2     08/11/2015 Kevin Dettbarn   Created tables for PP Integration
||                                        Import / Export tool.
||=============================================================================
*/

create table pp_int_file_import (
	batch_name varchar2(35),
	file_path varchar2(254),
	file_name varchar2(254),
	table_name varchar2(35),
	data_start_row number(22,0),
	direct number(22,0),
	append number(22,0),
	is_fixed_width number(22,0),
	delimiter varchar2(35),
	archive_file_path varchar2(254),
	archive_file_name varchar2(254),
	time_stamp date,
	user_id varchar2(18)
);

ALTER TABLE PP_INT_FILE_IMPORT ADD CONSTRAINT PP_INT_FILE_IMPORT_PK PRIMARY KEY (BATCH_NAME)
using index tablespace PWRPLANT_IDX ;

create table pp_int_file_import_col (
	batch_name varchar2(35),
	column_name varchar2(35),
	start_pos number(22,0),
	end_pos number(22,0),
	column_order number(22,0) NOT NULL,
	time_stamp date,
	user_id varchar2(18)
);

ALTER TABLE PP_INT_FILE_IMPORT_COL ADD CONSTRAINT PP_INT_FILE_IMPORT_COL_PK PRIMARY KEY (BATCH_NAME, COLUMN_NAME)
using index tablespace PWRPLANT_IDX ;

ALTER TABLE PP_INT_FILE_IMPORT_COL ADD CONSTRAINT PP_IMPT_IMPT_COL FOREIGN KEY ( BATCH_NAME ) REFERENCES PP_INT_FILE_IMPORT ;

CREATE TABLE PP_INT_FILE_EXPORT(
	BATCH_NAME VARCHAR2(35),
	FILE_PATH VARCHAR2(254),
	FILE_NAME VARCHAR2(254),
	TABLE_NAME VARCHAR2(35),
	SQL CLOB,
	INCLUDE_HEADER_ROW NUMBER(22,0),
	IS_FIXED_WIDTH NUMBER(22,0),
	DELIMITER varchar2(35),
	TIME_STAMP DATE,
	USER_ID VARCHAR2(18)
);

ALTER TABLE PP_INT_FILE_EXPORT ADD CONSTRAINT PP_INT_FILE_EXPORT_PK PRIMARY KEY (BATCH_NAME)
using index tablespace PWRPLANT_IDX ;

CREATE TABLE PP_INT_FILE_EXPORT_COL (
	BATCH_NAME VARCHAR2(35),
	COLUMN_NAME VARCHAR2(35),
	START_POS NUMBER(22,0),
	END_POS NUMBER(22,0),
	COLUMN_ORDER NUMBER(22,0),
	TIME_STAMP DATE,
	USER_ID VARCHAR2(18)
);

ALTER TABLE PP_INT_FILE_EXPORT_COL ADD CONSTRAINT PP_INT_FILE_EXPORT_COL_PK PRIMARY KEY ( BATCH_NAME, COLUMN_NAME) 
using index tablespace PWRPLANT_IDX;

ALTER TABLE PP_INT_FILE_EXPORT_COL ADD CONSTRAINT PP_EXP_EXP_COL FOREIGN KEY ( BATCH_NAME ) REFERENCES PP_INT_FILE_EXPORT  ;

comment on table pp_int_file_import is 'Table for text files to import in PP Integration';
comment on column pp_int_file_import.batch_name is 'Name of batch in pp_integration';
comment on column pp_int_file_import.file_path is 'Location of file to import';
comment on column pp_int_file_import.file_name is 'Name of file to import';
comment on column pp_int_file_import.table_name is 'Staging table';
comment on column pp_int_file_import.data_start_row is 'Row to begin importing data from in file';
comment on column pp_int_file_import.direct is 'Boolean indicating if overwriting data in the staging table';
comment on column pp_int_file_import.append is 'Boolean indicating if appending data to the end of the staging table';
comment on column pp_int_file_import.is_fixed_width is 'Fixed width or delimited file';
comment on column pp_int_file_import.delimiter is 'The delimiter between fields in the file';
comment on column pp_int_file_import.archive_file_path is 'Location to archive file';
comment on column pp_int_file_import.archive_file_name is 'Name of archive file';

comment on table pp_int_file_import_col is 'Table with columns of a file being imported in PP Integration';
comment on column pp_int_file_import_col.batch_name is 'Name of batch in pp_integration';
comment on column pp_int_file_import_col.column_name is 'Name of column in file';
comment on column pp_int_file_import_col.start_pos is 'Start position';
comment on column pp_int_file_import_col.end_pos is 'End Position';
comment on column pp_int_file_import_col.column_order is 'Position of column in table';

comment on table PP_INT_FILE_EXPORT is 'Table for text files to export in PP Integration';
comment on column pp_int_file_export.batch_name is 'Name of batch in pp_integration';
comment on column pp_int_file_export.file_path is 'Location of file to export';
comment on column pp_int_file_export.file_name is 'Name of file to export';
comment on column pp_int_file_export.table_name is 'Staging table';
comment on column pp_int_file_export.sql is 'SQL to customize data being exported';
comment on column pp_int_file_export.include_header_row is 'Boolean indicating if column name header will be exported';
comment on column pp_int_file_export.is_fixed_width is 'Fixed width or delimited file';
comment on column pp_int_file_export.delimiter is 'The delimiter between fields in the file';

comment on table PP_INT_FILE_EXPORT_COL is 'Table with columns of a file being exported in PP Integration';
comment on column pp_int_file_export_col.batch_name is 'Name of batch in pp_integration';
comment on column pp_int_file_export_col.column_name is 'Name of column in file';
comment on column pp_int_file_export_col.start_pos is 'Start position';
comment on column pp_int_file_export_col.end_pos is 'End Position';
comment on column pp_int_file_export_col.column_order is 'Position of column in table';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2767, 0, 2015, 2, 0, 0, 044379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044379_cr_pp_integration_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;