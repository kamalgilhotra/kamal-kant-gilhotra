/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051062_lessor_01_add_deferred_and_accrued_rent_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/23/2018 Andrew Hill      Add deferred and accrued rent columns to schedule tables
||============================================================================
*/

ALTER TABLE lsr_ilr_schedule ADD (beg_deferred_rent NUMBER(22,2),
                                  deferred_rent     NUMBER(22,2),
                                  end_deferred_rent NUMBER(22,2),
                                  beg_accrued_rent  NUMBER(22,2),
                                  accrued_rent      NUMBER(22,2),
                                  end_accrued_rent  NUMBER(22,2));
                                  
UPDATE lsr_ilr_schedule 
SET beg_deferred_rent = 0,
    deferred_rent = 0,
    end_deferred_rent = 0,
    beg_accrued_rent = 0,
    accrued_rent = 0,
    end_accrued_rent = 0;
    
alter table lsr_ilr_schedule modify ( beg_deferred_rent NOT NULL,
                                      deferred_rent     NOT NULL,
                                      end_deferred_rent NOT NULL,
                                      beg_accrued_rent  NOT NULL,
                                      accrued_rent      NOT NULL,
                                      end_accrued_rent  NOT NULL);
									  
--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5982, 0, 2017, 4, 0, 0, 51062, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051062_lessor_01_add_deferred_and_accrued_rent_columns_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;									  