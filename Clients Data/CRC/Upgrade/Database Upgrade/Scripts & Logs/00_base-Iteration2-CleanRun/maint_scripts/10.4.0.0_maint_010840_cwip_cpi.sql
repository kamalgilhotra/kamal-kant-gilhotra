/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010840_cwip_cpi.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.4.0.0    10/30/2012 Sunjin Cone    Point Release
||============================================================================
*/

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK)
   select 'ignore_current_month_cpi', TABLE_NAME, 'e', 'Curr Month CPI Convention', COLUMN_RANK
     from POWERPLANT_COLUMNS
    where TABLE_NAME = 'afudc_control'
      and COLUMN_NAME = 'ignore_current_month';

update POWERPLANT_COLUMNS
   set DROPDOWN_NAME = null, PP_EDIT_TYPE_ID = 'e', LONG_DESCRIPTION = null,
       DESCRIPTION = 'Curr Month AFC Convention'
 where TABLE_NAME = 'afudc_control'
   and COLUMN_NAME = 'ignore_current_month';

alter table AFUDC_CONTROL add A_COLUMN_TEMP number(22,2);
alter table AFUDC_CONTROL add IGNORE_CURRENT_MONTH_CPI number(22,2);

begin
   update AFUDC_CONTROL
      set A_COLUMN_TEMP = DECODE(LOWER(IGNORE_CURRENT_MONTH),
                                  LOWER('full'),
                                  1,
                                  LOWER('yes'),
                                  0,
                                  LOWER('no'),
                                  .5,
                                  '1',
                                  0,
                                  '0',
                                  .5,
                                  .5);

   update AFUDC_CONTROL set IGNORE_CURRENT_MONTH = null;
   commit;
end;
/

begin
   execute immediate 'alter table AFUDC_CONTROL modify IGNORE_CURRENT_MONTH number(22,2)';

   update AFUDC_CONTROL set IGNORE_CURRENT_MONTH = A_COLUMN_TEMP;

   execute immediate 'alter table AFUDC_CONTROL drop column A_COLUMN_TEMP';

   update AFUDC_CONTROL set IGNORE_CURRENT_MONTH_CPI = IGNORE_CURRENT_MONTH;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (215, 0, 10, 4, 0, 0, 10840, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_010840_cwip_cpi.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
