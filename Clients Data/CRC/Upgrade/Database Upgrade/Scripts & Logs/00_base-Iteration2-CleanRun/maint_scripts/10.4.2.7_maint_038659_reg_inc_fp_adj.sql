/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038659_reg_inc_fp_adj.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 07/07/2014 Shane Ward       Data tables for Incremental FP Adjustments
||============================================================================
*/

--INCREMENTAL_FP_ADJ_LABEL
create table INCREMENTAL_FP_ADJ_LABEL
(
 LABEL_ID         number(22,0) not null,
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254),
 SORT_ORDER       number(22,0) not null,
 USER_ID          varchar2(18),
 TIMESTAMP        date
);

alter table INCREMENTAL_FP_ADJ_LABEL
   add constraint PK_INCREMENTAL_FP_ADJ_LABEL
       primary key(LABEL_ID)
       using index tablespace PWRPLANT_IDX;

comment on table INCREMENTAL_FP_ADJ_LABEL is 'Stores parent level descriptions for the types of data displayed for the incremental forecast data.';
comment on column INCREMENTAL_FP_ADJ_LABEL.LABEL_ID is 'System assigned identifier for the incremental data label.';
comment on column INCREMENTAL_FP_ADJ_LABEL.DESCRIPTION is 'Description of the incremental data label.';
comment on column INCREMENTAL_FP_ADJ_LABEL.LONG_DESCRIPTION is 'long description of the incremental data label.';
comment on column INCREMENTAL_FP_ADJ_LABEL.SORT_ORDER is 'The display order for the labels.';

--INCREMENTAL_FP_ADJ_ITEM
create table INCREMENTAL_FP_ADJ_ITEM
(
 LABEL_ID         number(22,0) not null,
 ITEM_ID          number(22,0) not null,
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254),
 AMOUNT_TYPE      number(22,0) not null,
 SORT_ORDER       number(22,0) not null,
 JURISDICTION_ID  number(22,0),
 USER_ID          varchar2(18),
 TIMESTAMP        date
);

alter table INCREMENTAL_FP_ADJ_ITEM
   add constraint PK_INCREMENTAL_FP_ADJ_ITEM
       primary key(LABEL_ID, ITEM_ID)
       using index tablespace PWRPLANT_IDX;

alter table INCREMENTAL_FP_ADJ_ITEM
   add constraint FK_INCREMENATL_FP_ADJ_ITEM1
       foreign key(LABEL_ID)
       references INCREMENTAL_FP_ADJ_LABEL(LABEL_ID);

alter table INCREMENTAL_FP_ADJ_ITEM
   add constraint FK_INCREMENATL_FP_ADJ_ITEM2
       foreign key(JURISDICTION_ID)
       references JURISDICTION(JURISDICTION_ID);

comment on table INCREMENTAL_FP_ADJ_ITEM is 'Stores child level descriptions related to each parent label for the types of data displayed for incremental forecast data';
comment on column INCREMENTAL_FP_ADJ_ITEM.LABEL_ID is 'System assigned identifier for the incremental data label';
comment on column INCREMENTAL_FP_ADJ_ITEM.ITEM_ID is 'System assigned identifier for the incremental data item';
comment on column INCREMENTAL_FP_ADJ_ITEM.DESCRIPTION is 'Description of the incremental data item';
comment on column INCREMENTAL_FP_ADJ_ITEM.LONG_DESCRIPTION is 'Long description of the incremental data item';
comment on column INCREMENTAL_FP_ADJ_ITEM.AMOUNT_TYPE is 'The amount types are used to help determine whether to pull the amount from the first month of the year (2 = Beginning Balance), end month of the year (3 = Ending Balance), or the sum of all months in the year (1 = Activity)';
comment on column INCREMENTAL_FP_ADJ_ITEM.SORT_ORDER is 'The display order of the items';
comment on column INCREMENTAL_FP_ADJ_ITEM.JURISDICTION_ID is 'System assigned identifier of a PowerTax jurisdiction which will only be populated for PowerTax related labels';

--INCREMENTAL_FP_ADJ_DATA
create table INCREMENTAL_FP_ADJ_DATA
(
 LABEL_ID             number(22,0) not null,
 ITEM_ID              number(22,0) not null,
 WORK_ORDER_ID        number(22,0) not null,
 REVISION             number(22,0) not null,
 BUDGET_VERSION_ID    number(22,0) not null,
 FCST_DEPR_VERSION_ID number(22,0) not null,
 VERSION_ID           number(22,0) not null,
 MONTH_YEAR           number(22,0) not null,
 AMOUNT               number(22,2) not null,
 FCST_DEPR_GROUP_ID   number(22,0),
 SET_OF_BOOKS_ID      number(22,0),
 EXPENDITURE_TYPE_ID  number(22,0),
 EST_CHG_TYPE_ID      number(22,0),
 WIP_COMPUTATION_ID   number(22,0),
 WIP_ACCOUNT_ID       number(22,0),
 TAX_CLASS_ID         number(22,0),
 NORMALIZATION_ID     number(22,0),
 JURISDICTION_ID      number(22,0),
 USER_ID              varchar2(18),
 TIMESTAMP            date
);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint PK_INCREMENTAL_FP_ADJ_DATA
       primary key(LABEL_ID,
                   ITEM_ID,
                   WORK_ORDER_ID,
                   REVISION,
                   BUDGET_VERSION_ID,
                   FCST_DEPR_VERSION_ID,
                   VERSION_ID,
                   MONTH_YEAR)
       using index tablespace PWRPLANT_IDX;

alter table INCREMENTAL_FP_ADJ_DATA
      add constraint FK_INCREMENTAL_FP_ADJ_DATA1
          foreign key(LABEL_ID, ITEM_ID)
          references INCREMENTAL_FP_ADJ_ITEM(LABEL_ID, ITEM_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA2
       foreign key(WORK_ORDER_ID, REVISION)
       references WORK_ORDER_APPROVAL(WORK_ORDER_ID, REVISION);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA3
       foreign key(BUDGET_VERSION_ID)
       references BUDGET_VERSION(BUDGET_VERSION_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA4
       foreign key(FCST_DEPR_VERSION_ID)
       references FCST_DEPR_VERSION(FCST_DEPR_VERSION_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA5
       foreign key(FCST_DEPR_GROUP_ID)
       references FCST_DEPR_GROUP(FCST_DEPR_GROUP_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA6
       foreign key(SET_OF_BOOKS_ID)
       references SET_OF_BOOKS(SET_OF_BOOKS_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA7
       foreign key(EXPENDITURE_TYPE_ID)
       references EXPENDITURE_TYPE(EXPENDITURE_TYPE_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA8
       foreign key(EST_CHG_TYPE_ID)
       references ESTIMATE_CHARGE_TYPE(EST_CHG_TYPE_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA9
       foreign key(TAX_CLASS_ID)
       references TAX_CLASS(TAX_CLASS_ID);


alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA10
      foreign key(NORMALIZATION_ID)
      references NORMALIZATION_SCHEMA(NORMALIZATION_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA11
       foreign key(JURISDICTION_ID)
       references JURISDICTION(JURISDICTION_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA12
       foreign key(VERSION_ID)
       references VERSION(VERSION_ID);

comment on table INCREMENTAL_FP_ADJ_DATA is 'Stores incremental forecast data by label, item, funding project, and revision.';
comment on column INCREMENTAL_FP_ADJ_DATA.LABEL_ID is 'System assigned identifier for the incremental data label.';
comment on column INCREMENTAL_FP_ADJ_DATA.ITEM_ID is 'System assigned identifier for the incremental data item.';
comment on column INCREMENTAL_FP_ADJ_DATA.WORK_ORDER_ID is 'System assigned identifier for the funding project.';
comment on column INCREMENTAL_FP_ADJ_DATA.REVISION is 'System assigned identifier for the funding project revision.';
comment on column INCREMENTAL_FP_ADJ_DATA.BUDGET_VERSION_ID is 'System assigned identifier for the capital budget version.';
comment on column INCREMENTAL_FP_ADJ_DATA.FCST_DEPR_VERSION_ID is 'System assigned identifier for the depreciation forecast version.';
comment on column INCREMENTAL_FP_ADJ_DATA.VERSION_ID is 'System assigned identifier for the PowerTax Case.';
comment on column INCREMENTAL_FP_ADJ_DATA.MONTH_YEAR is 'Month in YYYYMM format.';
comment on column INCREMENTAL_FP_ADJ_DATA.AMOUNT is 'Monthly incremental financial amount in dollars.';
comment on column INCREMENTAL_FP_ADJ_DATA.FCST_DEPR_GROUP_ID is 'System assigned identifier for the forecast depreciation group for labels/items that involve depreciation.';
comment on column INCREMENTAL_FP_ADJ_DATA.SET_OF_BOOKS_ID is 'System assigned identifier for the Set of Books for labels/items that involve depreciation.';
comment on column INCREMENTAL_FP_ADJ_DATA.EXPENDITURE_TYPE_ID is 'System assigned identifier for the expenditure type for the funding project.';
comment on column INCREMENTAL_FP_ADJ_DATA.EST_CHG_TYPE_ID is 'System assigned identifier for the estimate charge type for the funding project.';
comment on column INCREMENTAL_FP_ADJ_DATA.WIP_COMPUTATION_ID is 'System assigned identifier for the WIP computation for labels/items that involve WIP Computations.';
comment on column INCREMENTAL_FP_ADJ_DATA.WIP_ACCOUNT_ID is 'System assigned identifier for the GL account other than CWIP for labels/items that involve WIP Computations.';
comment on column INCREMENTAL_FP_ADJ_DATA.TAX_CLASS_ID is 'System assigned identifier for the tax class for labels/items that involve deferred taxes.';
comment on column INCREMENTAL_FP_ADJ_DATA.NORMALIZATION_ID is 'System assigned identifier for the normalization schema for labels/items that involve deferred taxes.';
comment on column INCREMENTAL_FP_ADJ_DATA.JURISDICTION_ID is 'System assigned identifier for the jurisdiction for labels/items that involve deferred taxes.';

--Populate defualt data
--incremental_fp_adj_label
insert into INCREMENTAL_FP_ADJ_LABEL (LABEL_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER) values (1, 'CWIP', null, 1);

insert into INCREMENTAL_FP_ADJ_LABEL
   (LABEL_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER)
values
   (2, 'Regulatory Accounts', null, 2);

insert into INCREMENTAL_FP_ADJ_LABEL
   (LABEL_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER)
values
   (3, 'Plant In Service', null, 3);

insert into INCREMENTAL_FP_ADJ_LABEL
   (LABEL_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER)
values
   (4, 'Book Depreciation', null, 4);

insert into INCREMENTAL_FP_ADJ_LABEL
   (LABEL_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER)
values
   (6, 'Accum Deferred Tax - In Svc (APB11)', null, 6);

insert into INCREMENTAL_FP_ADJ_LABEL
   (LABEL_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER)
values
   (7, 'Tax Expense', null, 7);

--incremental_fp_adj_item
insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (1, 1, 'Beginning Balance', 'Beginning Balance', 2, 1);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (1, 2, '(CWIP) Expenditures', '(CWIP) Expenditures', 1, 2);
insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (1, 3, 'AFUDC Debt', 'AFUDC Debt', 1, 3);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (1, 4, 'AFUDC Equity', 'AFUDC Equity', 1, 4);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (1, 5, 'CIAC', 'CIAC', 1, 5);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (1, 6, 'Closings', 'Closings', 1, 6);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (1, 7, 'Ending Balance', 'Ending Balance', 3, 7);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (1, 8, 'Average Balance', 'Average Balance', 3, 8);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (2, 1, 'Beginning Balance', 'Beginning Balance', 2, 1);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (2, 2, 'Activity', 'Activity', 1, 2);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (2, 3, 'Amortization', 'Amortization', 1, 3);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (2, 4, 'Ending Balance', 'Ending Balance', 3, 4);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (2, 5, 'Average Balance', 'Average Balance', 3, 5);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (3, 1, 'Beginning Balance', 'Beginning Balance', 2, 1);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (3, 2, 'Additions', 'Additions', 1, 2);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (3, 3, 'Retirements', 'Retirements', 1, 3);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (3, 4, 'Ending Balance', 'Ending Balance', 3, 4);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (3, 5, 'Average Balance', 'Average Balance', 3, 5);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 1, 'Begin Reserve (Life)', 'Begin Reserve (Life)', 2, 1);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 2, 'Depreciation Expense (Life)', 'Depreciation Expense (Life)', 1, 2);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 3, 'Salvage', 'Salvage', 1, 3);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 4, 'Retirements', 'Retirements', 1, 4);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 5, 'End Reserve (Life)', 'End Reserve (Life)', 3, 5);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 6, 'Average Reserve Balance (Life)', 'Average Reserve Balance (Life)', 3, 6);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 7, 'Begin Reserve (COR)', 'Begin Reserve (COR)', 2, 7);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 8, 'Depreciation Expense (COR)', 'Depreciation Expense (COR)', 1, 8);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 9, 'Cost of Removal', 'Cost of Removal', 1, 9);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 10, 'End Reserve (COR)', 'End Reserve (COR)', 3, 10);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (4, 11, 'Average Reserve Balance (COR)', 'Average Reserve Balance (COR)', 3, 11);

insert into INCREMENTAL_FP_ADJ_ITEM
   (LABEL_ID, ITEM_ID, DESCRIPTION, LONG_DESCRIPTION, AMOUNT_TYPE, SORT_ORDER)
values
   (7, 1, 'Tax Depreciation (Annual)', 'Tax Depreciation', 3, 1);

--Menu Items
delete from ppbase_workspace where workspace_identifier = 'uo_reg_inc_adj_ws';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_inc_adj_ws', 'IFA - Select and Review', 'uo_reg_inc_adj_ws', 'IFA - Select and Review');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'IFA_TOP', 2, 8, 'Incremental Fcst Adjustment', 'Incremental Forecast Adjustment', 'MONTHLY', null, 1);

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'IFA_SELECT', 3, 1, 'IFA - Select and Review', 'IFA - Select and Review', 'IFA_TOP', 'uo_reg_inc_adj_ws', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1173, 0, 10, 4, 2, 7, 38659, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038659_reg_inc_fp_adj.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;