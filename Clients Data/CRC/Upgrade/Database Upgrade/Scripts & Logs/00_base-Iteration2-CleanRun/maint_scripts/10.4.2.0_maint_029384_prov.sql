/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029384_prov.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/14/2013 Nathan Hollis  Point Release
||============================================================================
*/

alter table TAX_ACCRUAL_ENTITY_DEDUCT
   add (M_ITEM_ID_PRIOR    number(22,0),
        M_ITEM_ID_FT_PRIOR number(22,0));

alter table TAX_ACCRUAL_ENTITY_DEDUCT add INCLUDE_CREDITS number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (700, 0, 10, 4, 2, 0, 29384, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029384_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
