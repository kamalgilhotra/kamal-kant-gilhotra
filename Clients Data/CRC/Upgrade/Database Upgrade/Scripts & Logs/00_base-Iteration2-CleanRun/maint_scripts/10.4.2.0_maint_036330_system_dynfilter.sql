/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036330_system_dynfilter.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/27/2014 Alex Pivoshenko
||============================================================================
*/

create table PP_DYNAMIC_FILTER_VALUES_DW
(
 LABEL      varchar2(35) not null,
 USERS      varchar2(18) not null,
 USER_ID    varchar2(18),
 TIME_STAMP date,
 VALUE_ID   varchar2(254) not null,
 VALUE_DESC varchar2(254)
);

alter table PP_DYNAMIC_FILTER_VALUES_DW
   add constraint PK_DYNAMIC_FILTER_VALUES_DW
       primary key (LABEL, USERS, VALUE_ID)
       using index tablespace PWRPLANT_IDX;

comment on table PP_DYNAMIC_FILTER_VALUES_DW
  is '(S)  [10]
The PP Dynamic Filter Values DW table stores filter selections when the dynamic filter is used for the values selected from datawindows.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.LABEL is 'Label for a filter entry with selected values.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.USERS is 'User currently using the filter.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.VALUE_ID is 'System identifier for a selected value';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.VALUE_DESC is 'Description for a selected value.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.USER_ID is 'Standard System-assigned user id used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';

create table PP_DYN_FILTER_SAVED_VALUES_DW
(
 SAVED_FILTER_ID NUMBER(22,0) NOT NULL,
 LABEL           VARCHAR2(35) NOT NULL,
 VALUE_ID        VARCHAR2(254) NOT NULL,
 USER_ID         VARCHAR2(18),
 TIME_STAMP      DATE,
 VALUE_DESC      VARCHAR2(254)
);

alter table PP_DYN_FILTER_SAVED_VALUES_DW
   add constraint PK_DYN_FILTER_SAVED_VALUES_DW
       primary key (SAVED_FILTER_ID, LABEL, VALUE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_DYN_FILTER_SAVED_VALUES_DW
   add constraint FK_PDFSVD_SAVED_FILTER_ID
       foreign key (SAVED_FILTER_ID)
       references PP_DYNAMIC_FILTER_SAVED;

comment on table PP_DYN_FILTER_SAVED_VALUES_DW
  is '(S)  [10]
The PP Dynamic Filter Values DW table stores filter selections when the dynamic filter is saved for the values selected from datawindows.';
comment on column PP_DYN_FILTER_SAVED_VALUES_DW.SAVED_FILTER_ID is 'System saved filter identifier.';
comment on column PP_DYN_FILTER_SAVED_VALUES_DW.LABEL is 'Label for a filter entry with saved values.';
comment on column PP_DYN_FILTER_SAVED_VALUES_DW.VALUE_ID is 'System identifier for a saved value';
comment on column PP_DYN_FILTER_SAVED_VALUES_DW.VALUE_DESC is 'Description for a saved value.';
comment on column PP_DYN_FILTER_SAVED_VALUES_DW.USER_ID is 'Standard System-assigned user id used for audit purposes.';
comment on column PP_DYN_FILTER_SAVED_VALUES_DW.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';

-- Recreate table PP_DYNAMIC_FILTER_VALUES as a non-temp table. Add users column, drop batch_id
drop table PP_DYNAMIC_FILTER_VALUES;
create table PP_DYNAMIC_FILTER_VALUES
(
 LABEL       varchar2(35) not null,
 USERS       varchar2(18) not null,
 OPERATOR    varchar2(35),
 START_VALUE varchar2(2000) not null,
 END_VALUE   varchar2(35),
 USER_ID     varchar2(18),
 TIME_STAMP  date,
 SEARCH_TYPE number(1) not null
);

-- Add comments to the table
comment on table PP_DYNAMIC_FILTER_VALUES
  is '(S)  [10]
The PP Dynamic Filter Values table stores filter selections when the dynamic filter is used.';
-- Add comments to the columns
comment on column PP_DYNAMIC_FILTER_VALUES.LABEL is 'Label for a filter entry with selected values.';
comment on column PP_DYNAMIC_FILTER_VALUES.USERS is 'User currently using the filter.';
comment on column PP_DYNAMIC_FILTER_VALUES.OPERATOR is 'Operator used for a filter entry with selected values.';
comment on column PP_DYNAMIC_FILTER_VALUES.START_VALUE is 'First selected values';
comment on column PP_DYNAMIC_FILTER_VALUES.END_VALUE is 'Optional second selected value. This is typically used for operations like "between" that have s start and end value.';
comment on column PP_DYNAMIC_FILTER_VALUES.USER_ID is 'Standard System-assigned user id used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_VALUES.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_VALUES.SEARCH_TYPE is 'Numeric identifier defining the type of object that contains values. 1 = datawindow, 2 = single line edit, 3 = daterange, 4 = operation. Note that datawindow values are stored in PP_DYNAMIC_FILTER_VALUES_DW table.';

-- Create/Recreate primary, unique and foreign key constraints
alter table PP_DYNAMIC_FILTER_VALUES
   add constraint PK_DYNAMIC_FILTER_VALUES
       primary key (LABEL, USERS)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (998, 0, 10, 4, 2, 0, 36330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036330_system_dynfilter.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;