/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009732_sys_add_wo_val_type_args.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/12/2013 Jon Joury      Point Release
||============================================================================
*/

-- For the unit estimate dynamic validations, only 2 arguments are now passed in for this in f_wo_estimate_update (work order id and revision).
-- But when you go to the dynamic validation config screen, many arguments are available for the unit estimate validations.

update WO_VALIDATION_TYPE
   set COL3 = null, COL4 = null, COL5 = null, COL6 = null, COL7 = null, COL8 = null, COL9 = null,
       COL10 = null, COL11 = null, COL12 = null, COL13 = null, COL14 = null, COL15 = null,
       COL16 = null, COL17 = null, COL18 = null, COL19 = null, COL20 = null
 where WO_VALIDATION_TYPE_ID = 3;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (504, 0, 10, 4, 1, 0, 9732, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009732_sys_add_wo_val_type_args.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
