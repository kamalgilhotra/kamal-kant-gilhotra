SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_031642_lease_cleanup_tables.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/08/2013 B.Beck         Original Version
||============================================================================
*/


begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   for CSR_CREATE_SQL in (select 'drop public synonym "' || TABLE_NAME || '"' DROP_SYNONYM,
                                 'drop table PWRPLANT."' || TABLE_NAME || '" cascade constraints' DROP_TABLE,
                                 TABLE_NAME
                            from ALL_TABLES
                           where OWNER = 'PWRPLANT'
                             and TABLE_NAME in ('LS_CONTINGENT_BUCKETS',
                                                'LS_CONT_RENT_FLG',
                                                'LS_CONT_RENT_ILR_AMOUNT',
                                                'LS_CONT_RENT_ILR_MAP',
                                                'LS_CONT_RENT_VARIABLE',
                                                'LS_CONT_RENT_VAR_EXT',
                                                'LS_CONT_RENT_VAR_TYPE',
                                                'LS_CONT_RENT_VAR_USAGE',
                                                'LS_DISTRIBUTION_TYPE',
                                                'LS_EXECUTORY_BUCKETS',
                                                'LS_IBR',
                                                'LS_ILR_ASSET_REL',
                                                'LS_ILR_FIXED_PRIN_TERM',
                                                'LS_ILR_GROUP_AIR',
                                                'LS_ILR_PAYMENTS',
                                                'LS_LEASE_AIR',
                                                'LS_LEASE_DISPOSITION_CODE',
                                                'LS_LEASE_ILR_REL',
                                                'LS_LEASE_INVOICE',
                                                'LS_LEASE_PAYMENT_TERM',
                                                'LS_LEASE_RECURRING_VOUCHER',
                                                'LS_LEASE_VENDOR_REL',
                                                'LS_LESSOR_INVOICE_LINE',
                                                'LS_LESSOR_STATE',
                                                'LS_LESSOR_TAX_YN_SW',
                                                'LS_LOCAL_TAX_ELIG',
                                                'LS_LOCAL_TAX_RATES',
                                                'LS_LOCAL_TAX_SNAPSHOT_STATE',
                                                'LS_MASS_CHANGE_CONTROL',
                                                'LS_MONTHLY_ACCRUALS_STG',
                                                'LS_OPERATING_EXPENSE_METHOD',
                                                'LS_OPERATING_JE_FLG',
                                                'LS_PAD_SIDE',
                                                'LS_PAYMENT_MONTH_INDICATOR',
                                                'LS_PRE_API_ASSET',
                                                'LS_PRE_API_ASSET_ATTRIBUTES',
                                                'LS_PRE_API_CODE_BLOCK',
                                                'LS_PRE_API_ILR',
                                                'LS_PRE_API_LESSOR',
                                                'LS_PRE_API_PAYMENT_INFO',
                                                'LS_RECURRING_VOUCHER',
                                                'LS_STATE_GRANDFATHER_DATE',
                                                'LS_STATE_LOCAL_TAX_RATES',
                                                'LS_VENDOR_INVOICE_LINE',
                                                'LS_VENDOR_INVOICE_LINE_LEVEL',
                                                'LS_VENDOR_INVOICE_LINE_MAP',
                                                'LS_VENDOR_INVOICE_MAP',
                                                'LS_VENDOR_INVOICE_STATUS',
                                                'LS_VOUCHER_APPROVAL_RECORDS',
                                                'LS_VOUCHER_DETAIL',
                                                'LS_VOUCHER_INVOICE',
                                                'LS_VOUCHER_LINE',
                                                'LS_GRANDFATHER_LEVEL',
                                                'LS_INVOICE_LINE_LEVEL',
                                                'LS_LESSOR_INVOICE',
                                                'LS_LESSOR_INVOICE_STATUS',
                                                'LS_LOCAL_TAX',
                                                'LS_LOCAL_TAX_DEFAULT',
                                                'LS_LOCAL_TAX_DISTRICT',
                                                'LS_SUMMARY_LOCAL_TAX',
                                                'LS_VENDOR_INVOICE',
                                                'LS_VOUCHER',
                                                'LS_VOUCHER_STATUS')
                           order by TABLE_NAME)
   loop
      -- Drop SYNONYM
      begin
         execute immediate CSR_CREATE_SQL.DROP_SYNONYM;
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.TABLE_NAME || ' synonym dropped');
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.TABLE_NAME || ' synonym did not exist');
      end;
      -- Drop TABLE
      begin
         execute immediate CSR_CREATE_SQL.DROP_TABLE;
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.TABLE_NAME || ' table dropped');
      end;
   end loop;
end;
/

create index ls_calc_sch_ndx
on ls_ilr_asset_schedule_calc_stg (ls_asset_id, revision, id, month)
tablespace pwrplant_idx
;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (593, 0, 10, 4, 1, 0, 31642, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031642_lease_cleanup_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
