/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049027_lessor_02_update_mc_schedule_views_add_sales_type_fields_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ------------------------------------
|| 2017.1.0.0 10/09/2017 Shane "C" Ward Recreate Multi Curr View for new amount datatypes
||============================================================================
*/

DROP materialized VIEW mv_lsr_ilr_mc_schedule_amounts;

CREATE MATERIALIZED VIEW MV_LSR_ILR_MC_SCHEDULE_AMOUNTS
REFRESH FAST ON COMMIT
AS
SELECT
  schedule.ilr_id,
  ilr.ilr_number,
  ilr.current_revision,
  schedule.revision,
  schedule.set_of_books_id,
  schedule.month,
  schedule.interest_income_received,
  schedule.interest_income_accrued,
  schedule.beg_deferred_rev,
  schedule.deferred_rev_activity,
  schedule.end_deferred_rev,
  schedule.beg_receivable,
  schedule.end_receivable,
  schedule.executory_accrual1,
  schedule.executory_accrual2,
  schedule.executory_accrual3,
  schedule.executory_accrual4,
  schedule.executory_accrual5,
  schedule.executory_accrual6,
  schedule.executory_accrual7,
  schedule.executory_accrual8,
  schedule.executory_accrual9,
  schedule.executory_accrual10,
  schedule.executory_paid1,
  schedule.executory_paid2,
  schedule.executory_paid3,
  schedule.executory_paid4,
  schedule.executory_paid5,
  schedule.executory_paid6,
  schedule.executory_paid7,
  schedule.executory_paid8,
  schedule.executory_paid9,
  schedule.executory_paid10,
  schedule.contingent_accrual1,
  schedule.contingent_accrual2,
  schedule.contingent_accrual3,
  schedule.contingent_accrual4,
  schedule.contingent_accrual5,
  schedule.contingent_accrual6,
  schedule.contingent_accrual7,
  schedule.contingent_accrual8,
  schedule.contingent_accrual9,
  schedule.contingent_accrual10,
  schedule.contingent_paid1,
  schedule.contingent_paid2,
  schedule.contingent_paid3,
  schedule.contingent_paid4,
  schedule.contingent_paid5,
  schedule.contingent_paid6,
  schedule.contingent_paid7,
  schedule.contingent_paid8,
  schedule.contingent_paid9,
  schedule.contingent_paid10,
  schedule.current_lease_cost,
  st_schedule.principal_received,
  st_schedule.principal_accrued,
  st_schedule.beg_long_term_receivable,
  st_schedule.end_long_term_receivable,
  st_schedule.beg_unguaranteed_residual,
  st_schedule.interest_unguaranteed_residual,
  st_schedule.ending_unguaranteed_residual,
  st_schedule.beg_net_investment,
  st_schedule.interest_net_investment,
  st_schedule.ending_net_investment,
  ilr.lease_id,
  ilr.company_id,
  options.in_service_exchange_rate,
  options.purchase_option_amt,
  options.termination_amt,
  schedule.ROWID AS lisrowid,
  options.ROWID AS optrowid,
  ilr.ROWID AS ilrrowid,
  lease.rowid as leaserowid,
  st_schedule.rowid as salesrowid
FROM lsr_ilr_schedule schedule,
  lsr_ilr_options options,
  lsr_ilr ilr,
  lsr_lease lease,
  lsr_ilr_schedule_sales_direct st_schedule
WHERE schedule.ilr_id = options.ilr_id
AND schedule.revision = options.revision
AND schedule.ilr_id = ilr.ilr_id
AND ilr.lease_id = lease.lease_id
AND st_schedule.ilr_id (+) = schedule.ilr_id
AND st_schedule.revision (+) = schedule.revision
AND st_schedule.month (+) = schedule.month
AND st_schedule.set_of_books_id (+) = schedule.set_of_books_id
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3778, 0, 2017, 1, 0, 0, 49027, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049027_lessor_02_update_mc_schedule_views_add_sales_type_fields_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;