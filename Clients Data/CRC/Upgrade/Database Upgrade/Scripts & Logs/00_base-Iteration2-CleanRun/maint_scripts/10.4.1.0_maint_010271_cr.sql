SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010271_cr.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.4.1.0    08/19/2013 Lee Quinn      Point Release - Marc Zawko
||============================================================================
*/

begin
   execute immediate 'alter table CR_STRUCTURE_VALUES2_BDG_TEMP
                         add (RANGE_START varchar2(35),
                              RANGE_END   varchar2(35))';
   DBMS_OUTPUT.PUT_LINE('RANGE_START, RANGE_END columns added to CR_STRUCTURE_VALUES2_BDG_TEMP');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('RANGE_START, RANGE_END columns NOT added to CR_STRUCTURE_VALUES2_BDG_TEMP');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (517, 0, 10, 4, 1, 0, 10271, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010271_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
