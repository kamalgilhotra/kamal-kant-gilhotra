/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050585_lessor_02_return_lsr_ilr_rates_to_annual_rates_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 03/26/2018 Andrew Hill       Save rates as "annualized" rates and update rate view
||============================================================================
*/

UPDATE lsr_ilr_rates SET rate = NULL;

ALTER TABLE lsr_ilr_rates MODIFY rate FLOAT(10);

COMMENT ON TABLE lsr_ilr_rates IS '(C) [06] The Lessor ILR Rates table holds applicable "annualized" rates for an ILR for a specific Revision';
COMMENT ON COLUMN lsr_ilr_rates.rate is 'Percentage Rate to be used for the Rate Type assigned. Stored in Decimal format not Percent. Stored as "annualized" version of rate';

CREATE OR REPLACE VIEW v_lsr_ilr_rates_compounded AS
SELECT  lsr_ilr_rates.ilr_id,
        lsr_ilr_rates.revision,
        lsr_ilr_rates.rate_type_id,
        lsr_ilr_rate_types.DESCRIPTION AS rate_type_description,
        pkg_lessor_schedule.f_annual_to_implicit_rate(lsr_ilr_rates.rate) AS rate_implicit,
        (POWER(1 + pkg_lessor_schedule.f_annual_to_implicit_rate(lsr_ilr_rates.rate), 3) - 1)/3 AS quarterly_compounded_rate, 
        (POWER(1 + pkg_lessor_schedule.f_annual_to_implicit_rate(lsr_ilr_rates.rate), 6) - 1)/6 AS  semiannual_compounded_rate,
        (POWER(1 + pkg_lessor_schedule.f_annual_to_implicit_rate(lsr_ilr_rates.rate), 12) - 1)/12 AS  annual_compounded_rate,
        rate AS annual_discount_rate
FROM lsr_ilr_rates
JOIN lsr_ilr_rate_types ON lsr_ilr_rates.rate_type_id = lsr_ilr_rate_types.rate_type_id;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4250, 0, 2017, 3, 0, 0, 50585, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050585_lessor_02_return_lsr_ilr_rates_to_annual_rates_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;