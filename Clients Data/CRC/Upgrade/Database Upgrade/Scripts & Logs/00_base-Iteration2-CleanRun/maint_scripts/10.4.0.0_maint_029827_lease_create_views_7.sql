SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029827_lease_create_views_7.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/25/2013 Lee Quinn      Point Release
||============================================================================
*/

declare
   TABLE_DOES_NOT_EXIST exception;
   pragma exception_init(TABLE_DOES_NOT_EXIST, -00942);

   LOOPING_CHAIN_OF_SYNONYMS exception;
   pragma exception_init(LOOPING_CHAIN_OF_SYNONYMS, -01775);

   type CV_TYP is ref cursor;
   CV CV_TYP;

   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   L_COUNT             number := 1;
   LB_LEASE_BEING_USED boolean := true;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   begin
      open CV for 'select count(*) from LS_ASSET where ROWNUM < 10';
      fetch CV
         into L_COUNT;
      close CV;

   if L_COUNT = 0 then
      LB_LEASE_BEING_USED := false;
   end if;

   exception
      when TABLE_DOES_NOT_EXIST then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'LS_ASSET table Doesn''t exist so Lease Module can be refreshed.');
      when LOOPING_CHAIN_OF_SYNONYMS then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Looping Chain of Synonyms - LS_ASSET table Doesn''t exist so the Lease Module can be refreshed.');
   end;

   if LB_LEASE_BEING_USED then
      DBMS_OUTPUT.PUT_LINE('Lease is being used so no Views will be recreated.');
   else
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create view LS_ASSET_CPR_FIELDS_VIEW.');
      execute immediate 'create or replace view LS_ASSET_CPR_FIELDS_VIEW as
                         select LS_ASSET.LS_ASSET_ID LS_ASSET_ID,
                                LS_ASSET.ASSET_ID ASSET_ID,
                                NVL(CPR_LEDGER.PROPERTY_GROUP_ID, LS_ASSET_CPR_FIELDS.PROPERTY_GROUP_ID) PROPERTY_GROUP_ID,
                                NVL(CPR_LEDGER.DEPR_GROUP_ID, LS_ASSET_CPR_FIELDS.DEPR_GROUP_ID) DEPR_GROUP_ID,
                                NVL(CPR_LEDGER.BOOKS_SCHEMA_ID, LS_ASSET_CPR_FIELDS.BOOKS_SCHEMA_ID) BOOKS_SCHEMA_ID,
                                NVL(CPR_LEDGER.RETIREMENT_UNIT_ID, LS_ASSET_CPR_FIELDS.RETIREMENT_UNIT_ID) RETIREMENT_UNIT_ID,
                                NVL(CPR_LEDGER.BUS_SEGMENT_ID, LS_ASSET_CPR_FIELDS.BUS_SEGMENT_ID) BUS_SEGMENT_ID,
                                NVL(CPR_LEDGER.COMPANY_ID, LS_ASSET_CPR_FIELDS.COMPANY_ID) COMPANY_ID,
                                NVL(CPR_LEDGER.FUNC_CLASS_ID, LS_ASSET_CPR_FIELDS.FUNC_CLASS_ID) FUNC_CLASS_ID,
                                NVL(CPR_LEDGER.UTILITY_ACCOUNT_ID, LS_ASSET_CPR_FIELDS.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
                                NVL(CPR_LEDGER.GL_ACCOUNT_ID, LS_ASSET_CPR_FIELDS.GL_ACCOUNT_ID) GL_ACCOUNT_ID,
                                NVL(CPR_LEDGER.ASSET_LOCATION_ID, LS_ASSET_CPR_FIELDS.ASSET_LOCATION_ID) ASSET_LOCATION_ID,
                                NVL(CPR_LEDGER.SUB_ACCOUNT_ID, LS_ASSET_CPR_FIELDS.SUB_ACCOUNT_ID) SUB_ACCOUNT_ID,
                                LS_ASSET_CPR_FIELDS.WORK_ORDER_ID WORK_ORDER_ID,
                                NVL(CPR_LEDGER.WORK_ORDER_NUMBER, LS_ASSET_CPR_FIELDS.WORK_ORDER_NUMBER) WORK_ORDER_NUMBER,
                                NVL(CPR_LEDGER.IN_SERVICE_YEAR, LS_ASSET_CPR_FIELDS.IN_SERVICE_YEAR) IN_SERVICE_YEAR,
                                NVL(CPR_LEDGER.ENG_IN_SERVICE_YEAR, LS_ASSET_CPR_FIELDS.ENG_IN_SERVICE_YEAR) ENG_IN_SERVICE_YEAR,
                                LS_ASSET_CPR_FIELDS.STATE_ID STATE_ID,
                                NVL(CPR_LEDGER.SUBLEDGER_INDICATOR, LS_ASSET_CPR_FIELDS.SUBLEDGER_INDICATOR) SUBLEDGER_INDICATOR
                           from LS_ASSET, LS_ASSET_CPR_FIELDS, CPR_LEDGER
                          where LS_ASSET.ASSET_ID = CPR_LEDGER.ASSET_ID(+)
                            and LS_ASSET.LS_ASSET_ID = LS_ASSET_CPR_FIELDS.LS_ASSET_ID';

      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create view LS_ASSET_LEASE_NUMBER.');
      execute immediate 'create or replace view LS_ASSET_LEASE_NUMBER as
                         select A.LS_ASSET_ID, C.LEASE_NUMBER, C.LEASE_ID
                           from (select A.LS_ASSET_ID, B.LEASE_ID from LS_ASSET A, LS_ILR B where A.ILR_ID = B.ILR_ID(+)) A,
                                LS_LEASE C
                          where A.LEASE_ID = C.LEASE_ID(+)';

      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create view LS_DIST_DEF_CONTROL_VIEW.');
      execute immediate 'create or replace view LS_DIST_DEF_CONTROL_VIEW as
                         select ELEMENT_ID, DECODE(COLUMN_USAGE_ID, 1, 0, 1) INCLUDE_YN from LS_DIST_DEF_CONTROL';
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (363, 0, 10, 4, 0, 0, 29827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029827_lease_create_views_7.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
