/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_30_lease_import_alloc_rows.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

insert into pp_import_type(import_type_id, description, long_description, import_table_name, archive_table_name,
   allow_updates_on_add, delegate_object_name)
select
   261 AS import_type_id,
   'Add: Lease JE Allocation Rows' AS description,
   'Lease JE Allocation Rows' AS long_description,
   'ls_import_alloc_rows' AS import_table_name,
   'ls_import_alloc_rows_archive' AS archive_table_name,
   1 AS allow_updates_on_add,
   'nvo_ls_logic_import' AS delegate_object_name
from dual;

insert into pp_import_type_subsystem(import_type_id, import_subsystem_id)
select 261, 8 from dual;

insert into pp_import_template(import_template_id, import_type_id, description, long_description, created_by,
   created_date, do_update_with_add, is_autocreate_template)
select
   (select max(import_template_id) + 1 from pp_import_template) AS import_Template_id,
   261 AS import_type_id,
   'Lease JE Allocation Rows Add' AS description,
   'Lease JE Allocation Rows Add' AS long_description,
   'PWRPLANT' AS created_by,
   sysdate AS created_date,
   0 AS do_update_with_add,
   0 AS is_autocreate_template
from dual
;

insert into pp_import_column(import_type_id, column_name, description, import_column_name, is_required, processing_order,
   column_type, parent_table, is_on_table, parent_table_pk_column)
select 261, 'ls_asset_id', 'LS Asset ID', 'ls_asset_xlate', 1, 2, 'number(22,0)', 'ls_asset', 1, 'ls_asset_id' from dual union
select 261, 'ilr_id', 'ILR ID', 'ilr_xlate', 1, 2, 'number(22,0)', 'ls_ilr', 1, 'ilr_id' from dual union
select 261, 'company_id', 'Company ID', 'company_xlate', 1, 1, 'number(22,0)', 'company_setup', 1, 'company_id' from dual union
select 261, 'trans_type', 'Trans Type', 'trans_type_xlate', 1, 1, 'number(22,0)', 'je_trans_type', 1, 'trans_type' from dual union
select 261, 'tax_local_id', 'Tax Local ID', 'tax_local_xlate', 0, 1, 'number(22,0)', 'ls_tax_local', 1, 'tax_local_id' from dual union
select 261, 'percent', 'Percent', null, 1, 1, 'number(22,8)', null, 1, null from dual union
select 261, 'element_1', 'Element 1', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_2', 'Element 2', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_3', 'Element 3', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_4', 'Element 4', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_5', 'Element 5', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_6', 'Element 6', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_7', 'Element 7', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_8', 'Element 8', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_9', 'Element 9', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_10', 'Element 10', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_11', 'Element 11', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_12', 'Element 12', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_13', 'Element 13', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_14', 'Element 14', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_15', 'Element 15', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_16', 'Element 16', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_17', 'Element 17', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_18', 'Element 18', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_19', 'Element 19', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_20', 'Element 20', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_21', 'Element 21', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_22', 'Element 22', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_23', 'Element 23', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_24', 'Element 24', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_25', 'Element 25', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_26', 'Element 26', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_27', 'Element 27', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_28', 'Element 28', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_29', 'Element 29', null, 0, 1, 'varchar2(254)', null, 1, null from dual union
select 261, 'element_30', 'Element 30', null, 0, 1, 'varchar2(254)', null, 1, null from dual
;

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
   is_derived, lookup_table_name, lookup_column_name, lookup_values_alternate_sql)
select
   (select max(import_lookup_id) + 1 from pp_import_lookup) AS import_lookup_id,
   'JE Trans Type.Description' AS description,
   'The passed in value corresponds to the JE Trans Type: Description field. Translate to the Trans Type using the Description column on the JE Trans Type table.' AS long_Description,
   'trans_type' AS column_name,
   '( select je.trans_type from je_trans_type je where upper( trim( <importfield> ) ) = upper( trim( je.description ) ) )' AS lookup_sql,
   0 AS is_derived,
   'je_trans_type' AS lookup_table_name,
   'description' AS lookup_column_name,
   'select description from je_trans_type where trans_type >= 3000' AS lookup_values_alternate_sql
from dual
where not exists(
   select 1
   from pp_import_lookup
   where lookup_table_name = 'je_trans_type'
   and lookup_column_name = 'description');

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
   is_derived, lookup_table_name, lookup_column_name, lookup_values_alternate_sql)
select
   (select max(import_lookup_id) + 1 from pp_import_lookup) AS import_lookup_id,
   'LS Tax Local.Description' AS description,
   'The passed in value corresponds to the LS Tax Local: Description field. Translate to the Tax Local ID using the Description column on the LS Tax Local table.' AS long_Description,
   'tax_local_id' AS column_name,
   '( select tax.tax_local_id from ls_tax_local tax where upper( trim( <importfield> ) ) = upper( trim( tax.description ) ) )' AS lookup_sql,
   0 AS is_derived,
   'ls_tax_local' AS lookup_table_name,
   'description' AS lookup_column_name,
   null AS lookup_values_alternate_sql
from dual
where not exists(
   select 1
   from pp_import_lookup
   where lookup_table_name = 'ls_tax_local'
   and lookup_column_name = 'description');

declare
import_temp_id number;
asset_lookup   number;
jett_lookup    number;
tax_lookup     number;
company_lookup number;
ilr_lookup     number;

begin

select import_template_id
into import_temp_id
from pp_import_template where import_type_id = 261;

select import_lookup_id
into asset_lookup
from pp_import_lookup where column_name = 'ls_asset_id'
  and lookup_column_name = 'leased_asset_number';

select import_lookup_id
into jett_lookup
from pp_import_lookup where lookup_table_name = 'je_trans_type';

select import_lookup_id
into tax_lookup
from pp_import_lookup where lookup_table_name = 'ls_tax_local';

select import_lookup_id
into company_lookup
from pp_import_lookup
where column_name = 'company_id' and lookup_column_name = 'gl_company_no' and lookup_values_alternate_sql is null;

/* This is returning no values */
select import_lookup_id
into ilr_lookup
from pp_import_lookup
where column_name = 'ilr_id' and lookup_column_name = 'ilr_number' and lookup_constraining_columns = 'company_id';


insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
select import_temp_id, 1, 261, 'ls_asset_id', asset_lookup from dual union
select import_temp_id, 2, 261, 'ilr_id', ilr_lookup from dual union
select import_temp_id, 3, 261, 'company_id', company_lookup from dual union
select import_temp_id, 4, 261, 'trans_type', jett_lookup from dual union
select import_temp_id, 5, 261, 'tax_local_id', tax_lookup from dual union
select import_temp_id, 6, 261, 'percent', null from dual union
select import_temp_id, 7, 261, 'element_1', null from dual union
select import_temp_id, 8, 261, 'element_2', null from dual union
select import_temp_id, 9, 261, 'element_3', null from dual union
select import_temp_id, 10, 261, 'element_4', null from dual union
select import_temp_id, 11, 261, 'element_5', null from dual union
select import_temp_id, 12, 261, 'element_6', null from dual union
select import_temp_id, 13, 261, 'element_7', null from dual union
select import_temp_id, 14, 261, 'element_8', null from dual union
select import_temp_id, 15, 261, 'element_9', null from dual union
select import_temp_id, 16, 261, 'element_10', null from dual union
select import_temp_id, 17, 261, 'element_11', null from dual union
select import_temp_id, 18, 261, 'element_12', null from dual union
select import_temp_id, 19, 261, 'element_13', null from dual union
select import_temp_id, 20, 261, 'element_14', null from dual union
select import_temp_id, 21, 261, 'element_15', null from dual union
select import_temp_id, 22, 261, 'element_16', null from dual union
select import_temp_id, 23, 261, 'element_17', null from dual union
select import_temp_id, 24, 261, 'element_18', null from dual union
select import_temp_id, 25, 261, 'element_19', null from dual union
select import_temp_id, 26, 261, 'element_20', null from dual union
select import_temp_id, 27, 261, 'element_21', null from dual union
select import_temp_id, 28, 261, 'element_22', null from dual union
select import_temp_id, 29, 261, 'element_23', null from dual union
select import_temp_id, 30, 261, 'element_24', null from dual union
select import_temp_id, 31, 261, 'element_25', null from dual union
select import_temp_id, 32, 261, 'element_26', null from dual union
select import_temp_id, 33, 261, 'element_27', null from dual union
select import_temp_id, 34, 261, 'element_28', null from dual union
select import_temp_id, 35, 261, 'element_29', null from dual union
select import_temp_id, 36, 261, 'element_30', null from dual
;

end;
/

update pp_import_column col set description = (
   select description
   from cr_elements
   where 'element_'||"ORDER" = col.column_name)
where import_type_id = 261
and column_name like 'element%'
and to_number(replace(column_name,'element_','')) <= (select max("ORDER") from cr_elements)
and exists
(
	select 1
	from cr_elements
	where 'element_'||"ORDER" = col.column_name
)
;


commit;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2420, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_30_lease_import_alloc_rows.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;