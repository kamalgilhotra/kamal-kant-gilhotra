/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045319_sys_add_constraint_tmp_comp_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 07/20/2016 Jared Watkins  Update TMP_COMPANIES to add a unique constraint to the company_id column
||============================================================================
*/

alter table tmp_companies
  add constraint unique_tmp_companies unique (
    company_id
  )
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3244, 0, 2016, 1, 0, 0, 045319, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045319_sys_add_constraint_tmp_comp_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;