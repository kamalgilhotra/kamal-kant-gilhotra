/*
||========================================================================================
|| Application: PowerPlan
|| Module:      PCM
|| File Name:   maint_041909_pcm_maint_info_update.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   01/06/2015 Alex P.             Maint Info workspace required field
||
||========================================================================================
*/

/*
 *	Set up required field for Program
 */
insert into pp_required_table_column( id, table_name, column_name, objectpath, description ) values ( 1012, 'work_order_control', 'budget_id', 'uo_pcm_maint_wksp_info.dw_detail', 'Program' );


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2150, 0, 2015, 1, 0, 0, 041909, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041909_pcm_maint_info_update.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;