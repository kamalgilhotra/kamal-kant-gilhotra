/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032541_lease_queries.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Brandon Beck   Point Release
||============================================================================
*/

update PP_ANY_QUERY_CRITERIA set SQL =
'select
   (select s.description from set_of_books s where s.set_of_books_id = sch2.set_of_books_id) as set_of_books,
   c.description,
   (select lg.description from ls_lease_group lg where mla.lease_group_id = lg.lease_group_id) as lease_group,
   (select ls.description from ls_lessor ls where ls.lessor_id = mla.lessor_id) as lessor,
   mla.lease_number as lease_number, mla.description as lease_description,
   case when mla.pre_payment_sw = 1 then ''Prepay'' else ''Arrears'' end as prepay,
   (select ig.description from ls_ilr_group ig where ilr.ilr_group_id = ig.ilr_group_id) as ilr_group,
   ilr.ilr_number as ilr_number,
   (select lct.description from ls_lease_cap_type lct where lct.ls_lease_cap_type_id = ilro.lease_cap_type_id) as capitalization_type,
   la.description as asset_description, la.serial_number as serial_number,
   al.ext_asset_location as ext_asset_location, al.long_description as asset_loc_description,
   (select ua.description from utility_account ua where ua.utility_account_id = la.utility_account_id and ua.bus_segment_id = la.bus_segment_id) as utility_account,
   (select ru.description from retirement_unit ru where ru.retirement_unit_id = la.retirement_unit_id) as retirement_unit,
   nvl(td.county_id, al.county_id) as county,
   nvl(td.state_id, al.state_id) as state,
   td.tax_district_code as tax_district_code,
   case when sch2.the_row = 1 then la.fmv else 0 end as fair_market_value,
   sch2.month_number as month_number,
   case when sch2.the_row = 1 then sch2.beg_capital_cost else 0 end as beg_capital_cost,
   nvl(sch2.residual_amount, 0) as residual_amount, nvl(sch2.termination_penalty_amount, 0) as termination_penalty_amount,
   nvl(sch2.bargain_purchase_amount, 0) as bargain_purchase_amount,
   nvl(sch2.beg_obligation, 0) as beg_obligation, nvl(sch2.beg_lt_obligation, 0) as beg_lt_obligation,
   nvl(sch2.interest_accrual, 0) as interest_accrual, nvl(sch2.principal_accrual, 0) as principal_accrual,
   nvl(sch2.interest_paid, 0) as interest_paid, nvl(sch2.principal_paid, 0) as principal_paid
from
   ls_lease mla, ls_lease_company lsc,
   ls_ilr ilr, ls_ilr_options ilro,
   ls_asset la,
   (
      select row_number() over( partition by sch.ls_asset_id, sch.revision, sch.set_of_books_id order by sch.month ) as the_row,
         sch.set_of_books_id, sch.ls_asset_id, sch.revision,
         to_number(to_char(sch.month, ''yyyymm'')) as month_number,
         sch.residual_amount as residual_amount, sch.term_penalty as termination_penalty_amount,
         sch.bpo_price as bargain_purchase_amount, sch.beg_capital_cost as beg_capital_cost,
         sch.beg_obligation as beg_obligation, sch.beg_lt_obligation as beg_lt_obligation,
         sch.interest_accrual as interest_accrual, sch.principal_accrual as principal_accrual,
         sch.interest_paid as interest_paid, sch.principal_paid as principal_paid
      from ls_asset_schedule sch
   ) sch2,
   company c,
   asset_location al, prop_tax_district td
where mla.lease_id = lsc.lease_id
and lsc.company_id = c.company_id
and lsc.lease_id = ilr.lease_id (+)
and lsc.company_id = ilr.company_id (+)
and ilr.ilr_id = la.ilr_id (+)
and ilr.ilr_id = ilro.ilr_id (+)
and ilr.current_revision = ilro.revision (+)
and la.ls_asset_id = sch2.ls_asset_id
and la.approved_revision = sch2.revision
and la.asset_location_id = al.asset_location_id (+)
and al.tax_district_id = td.tax_district_id (+)
order by 1, 2, 3, 5, 8, 9, 11, 21'
where subsystem = 'lessee'
and description = 'Future Minimum Lease Payments';

delete from PP_ANY_QUERY_CRITERIA_FIELDS
  where id in (select ID from PP_ANY_QUERY_CRITERIA where SUBSYSTEM = 'lessee')
    and DETAIL_FIELD like '%obligation%';

delete from PP_ANY_QUERY_CRITERIA_FIELDS
  where id in (select ID from PP_ANY_QUERY_CRITERIA where SUBSYSTEM = 'lessee')
    and DETAIL_FIELD = 'ilr_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (637, 0, 10, 4, 1, 1, 32541, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032541_lease_queries.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
