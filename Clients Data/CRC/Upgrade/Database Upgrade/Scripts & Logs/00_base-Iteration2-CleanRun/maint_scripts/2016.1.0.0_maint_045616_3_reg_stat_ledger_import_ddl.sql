/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045616_3_reg_stat_ledger_import_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2016.1.0.0 04/05/2016 Sarah Byers		Create RMS Statistic Ledger Impor Tables
||============================================================================
*/

-- REG_IMPORT_STATISTIC_STG
create table reg_import_statistic_stg (
import_run_id number(22,0) not null,
line_id number(22,0) not null,
error_message varchar2(4000) null,
reg_stat_id number(22,0) null,
description varchar2(100) null,
long_description varchar2(2000) null,
unit_of_measure varchar2(70) null,
stat_used_for number(1,0) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_import_statistic_stg add (
constraint pk_reg_import_statistic_stg primary key (import_run_id, line_id) using index tablespace pwrplant_idx);

comment on table reg_import_statistic_stg is 'The Reg Import Statistic Stg Table is the staging table used to import Statistics.';
comment on column reg_import_statistic_stg.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column reg_import_statistic_stg.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column reg_import_statistic_stg.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column reg_import_statistic_stg.reg_stat_id is 'System assigned identifier of a Regulatory Statistic.';
comment on column reg_import_statistic_stg.description is 'Description of the Regulatory Statistic.';
comment on column reg_import_statistic_stg.long_description is 'Long Description of the Regulatory Statistic.';
comment on column reg_import_statistic_stg.unit_of_measure is 'Unit of Measure for the Regulatory Statistic, e.g. kW, USD';
comment on column reg_import_statistic_stg.stat_used_for is 'Determines whether the Regulatory Statistic is used for Historic and/or Forecast purposes.';
comment on column reg_import_statistic_stg.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_import_statistic_stg.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_IMPORT_STATISTIC_ARC
create table reg_import_statistic_arc (
import_run_id number(22,0) not null,
line_id number(22,0) not null,
error_message varchar2(4000) null,
reg_stat_id number(22,0) null,
description varchar2(100) null,
long_description varchar2(2000) null,
unit_of_measure varchar2(70) null,
stat_used_for number(1,0) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_import_statistic_arc add (
constraint pk_reg_import_statistic_arc primary key (import_run_id, line_id) using index tablespace pwrplant_idx);

comment on table reg_import_statistic_arc is 'The Reg Import Statistic Arc Table is used to archive imported Statistics.';
comment on column reg_import_statistic_arc.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column reg_import_statistic_arc.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column reg_import_statistic_arc.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column reg_import_statistic_arc.reg_stat_id is 'System assigned identifier of a Regulatory Statistic.';
comment on column reg_import_statistic_arc.description is 'Description of the Regulatory Statistic.';
comment on column reg_import_statistic_arc.long_description is 'Long Description of the Regulatory Statistic.';
comment on column reg_import_statistic_arc.unit_of_measure is 'Unit of Measure for the Regulatory Statistic, e.g. kW, USD';
comment on column reg_import_statistic_arc.stat_used_for is 'Determines whether the Regulatory Statistic is used for Historic and/or Forecast purposes.';
comment on column reg_import_statistic_arc.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_import_statistic_arc.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_IMPORT_STAT_LEDGER_STG
create table reg_import_stat_ledger_stg (
import_run_id number(22,0) not null,
line_id number(22,0) not null,
error_message varchar2(4000) null,
historic_version_id number(22,0) null,
historic_version_id_xlate varchar2(254) null,
forecast_version_id number(22,0) null,
forecast_version_id_xlate varchar2(254) null,
reg_company_id number(22,0) null,
reg_company_id_xlate varchar2(254) null,
reg_stat_id number(22,0) null,
reg_stat_id_xlate varchar2(254) null,
month_number number(22,0) null,
stat_value number(22,16) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_import_stat_ledger_stg add (
constraint pk_reg_import_stat_ledger_stg primary key (import_run_id, line_id) using index tablespace pwrplant_idx);

comment on table reg_import_stat_ledger_stg is 'The Reg Import Stat Ledger Stg Table is the staging table used to import Statistic Ledger Values.';
comment on column reg_import_stat_ledger_stg.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column reg_import_stat_ledger_stg.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column reg_import_stat_ledger_stg.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column reg_import_stat_ledger_stg.historic_version_id is 'System assigned identifier of a Regulatory Historic Ledger.';
comment on column reg_import_stat_ledger_stg.historic_version_id_xlate is 'Description of a Regulatory Historic Ledger used for translation purposes.';
comment on column reg_import_stat_ledger_stg.forecast_version_id is 'System assigned identifier of a Regulatory Forecast Ledger.';
comment on column reg_import_stat_ledger_stg.forecast_version_id_xlate is 'Description of a Regulatory Forecast Ledger used for translation purposes.';
comment on column reg_import_stat_ledger_stg.reg_company_id is 'System assigned identifier of a Regulatory Company.';
comment on column reg_import_stat_ledger_stg.reg_company_id_xlate is 'Description of a Regulatory Company used for translation purposes.';
comment on column reg_import_stat_ledger_stg.reg_stat_id is 'System assigned identifier of a Regulatory Statistic.';
comment on column reg_import_stat_ledger_stg.reg_stat_id_xlate is 'Description of a Regulatory Statistic used for translation purposes.';
comment on column reg_import_stat_ledger_stg.month_number is 'The month associated with the Regulatory Statistic Value; format YYYYMM.';
comment on column reg_import_stat_ledger_stg.stat_value is 'The Regulatory Statistic value.';
comment on column reg_import_stat_ledger_stg.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_import_stat_ledger_stg.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';

-- REG_IMPORT_STAT_LEDGER_ARC
create table reg_import_stat_ledger_arc (
import_run_id number(22,0) not null,
line_id number(22,0) not null,
error_message varchar2(4000) null,
historic_version_id number(22,0) null,
historic_version_id_xlate varchar2(254) null,
forecast_version_id number(22,0) null,
forecast_version_id_xlate varchar2(254) null,
reg_company_id number(22,0) null,
reg_company_id_xlate varchar2(254) null,
reg_stat_id number(22,0) null,
reg_stat_id_xlate varchar2(254) null,
month_number number(22,0) null,
stat_value number(22,16) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_import_stat_ledger_arc add (
constraint pk_reg_import_stat_ledger_arc primary key (import_run_id, line_id) using index tablespace pwrplant_idx);

comment on table reg_import_stat_ledger_arc is 'The Reg Import Stat Ledger Arc Table is used to archive imported Statistic Ledger Values.';
comment on column reg_import_stat_ledger_arc.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column reg_import_stat_ledger_arc.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column reg_import_stat_ledger_arc.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column reg_import_stat_ledger_arc.historic_version_id is 'System assigned identifier of a Regulatory Historic Ledger.';
comment on column reg_import_stat_ledger_arc.historic_version_id_xlate is 'Description of a Regulatory Historic Ledger used for translation purposes.';
comment on column reg_import_stat_ledger_arc.forecast_version_id is 'System assigned identifier of a Regulatory Forecast Ledger.';
comment on column reg_import_stat_ledger_arc.forecast_version_id_xlate is 'Description of a Regulatory Forecast Ledger used for translation purposes.';
comment on column reg_import_stat_ledger_stg.reg_company_id is 'System assigned identifier of a Regulatory Company.';
comment on column reg_import_stat_ledger_stg.reg_company_id_xlate is 'Description of a Regulatory Company used for translation purposes.';
comment on column reg_import_stat_ledger_arc.reg_stat_id is 'System assigned identifier of a Regulatory Statistic.';
comment on column reg_import_stat_ledger_arc.reg_stat_id_xlate is 'Description of a Regulatory Statistic used for translation purposes.';
comment on column reg_import_stat_ledger_arc.month_number is 'The month associated with the Regulatory Statistic Value; format YYYYMM.';
comment on column reg_import_stat_ledger_arc.stat_value is 'The Regulatory Statistic value.';
comment on column reg_import_stat_ledger_arc.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_import_stat_ledger_arc.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3156, 0, 2016, 1, 0, 0, 045616, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045616_3_reg_stat_ledger_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;