/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044206_budgetcap_process_level_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/02/2015 Alex P.        Creation
||============================================================================
*/

-- Update null budget_version.process_level with the values from the system control for specific companies.
update budget_version bv
set process_level = (select control_value
                     from pp_system_control_company pscc
                     where lower(trim(control_name)) = 'budget processing level'
                       and bv.company_id = pscc.company_id
                     )
where bv.process_level is null
  and exists (select control_value
               from pp_system_control_company pscc
               where lower(trim(control_name)) = 'budget processing level'
                 and bv.company_id = pscc.company_id
                 );

-- Update null budget_version.process_level with the values from the system control for all companies.                 
update budget_version bv
set process_level = (select control_value
                     from pp_system_control_company pscc
                     where lower(trim(control_name)) = 'budget processing level'
                       and pscc.company_id = -1
                     )
where bv.process_level is null; 

-- Delete obsolete 'Budget Processing Level' system control.
delete from pp_system_control_company where lower(control_name) = 'budget processing level';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2668, 0, 2015, 2, 0, 0, 044206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044206_budgetcap_process_level_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;