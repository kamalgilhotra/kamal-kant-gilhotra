/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033545_lease_COMPANY.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/21/2013 Ryan Oliveria
||============================================================================
*/

create or replace view COMPANY (
  COMPANY_ID,
  TIME_STAMP,
  USER_ID,
  GL_COMPANY_NO,
  OWNED,
  DESCRIPTION,
  STATUS_CODE_ID,
  SHORT_DESCRIPTION,
  COMPANY_SUMMARY_ID,
  AUTO_LIFE_MONTH,
  AUTO_CURVE_MONTH,
  AUTO_CLOSE_WO_NUM,
  PARENT_COMPANY_ID,
  TAX_RETURN_ID,
  CLOSING_ROLLUP_ID,
  AUTO_LIFE_MONTH_MONTHLY,
  COMPANY_TYPE,
  COMPANY_EIN,
  CWIP_ALLOCATION_MONTHS,
  PROP_TAX_COMPANY_ID,
  POWERTAX_COMPANY_ID,
  IS_LEASE_COMPANY
) as
select COMPANY_SETUP.COMPANY_ID,
       COMPANY_SETUP.TIME_STAMP,
       COMPANY_SETUP.USER_ID,
       COMPANY_SETUP.GL_COMPANY_NO,
       COMPANY_SETUP.OWNED,
       COMPANY_SETUP.DESCRIPTION,
       COMPANY_SETUP.STATUS_CODE_ID,
       COMPANY_SETUP.SHORT_DESCRIPTION,
       COMPANY_SETUP.COMPANY_SUMMARY_ID,
       COMPANY_SETUP.AUTO_LIFE_MONTH,
       COMPANY_SETUP.AUTO_CURVE_MONTH,
       COMPANY_SETUP.AUTO_CLOSE_WO_NUM,
       COMPANY_SETUP.PARENT_COMPANY_ID,
       COMPANY_SETUP.TAX_RETURN_ID,
       COMPANY_SETUP.CLOSING_ROLLUP_ID,
       COMPANY_SETUP.AUTO_LIFE_MONTH_MONTHLY,
       COMPANY_SETUP.COMPANY_TYPE,
       COMPANY_SETUP.COMPANY_EIN,
       COMPANY_SETUP.CWIP_ALLOCATION_MONTHS,
       COMPANY_SETUP.PROP_TAX_COMPANY_ID,
       COMPANY_SETUP.POWERTAX_COMPANY_ID,
       COMPANY_SETUP.IS_LEASE_COMPANY
  from PWRPLANT.COMPANY_SETUP, PWRPLANT.PP_COMPANY_SECURITY
 where COMPANY_SETUP.COMPANY_ID = PP_COMPANY_SECURITY.COMPANY_ID
   and USERS = LOWER(user)
   and 1 <> (select count(*) from PWRPLANT.PP_COMPANY_SECURITY_TEMP where ROWNUM = 1)
union all
select PP_COMPANY_SECURITY_TEMP.COMPANY_ID,
       PP_COMPANY_SECURITY_TEMP.TIME_STAMP,
       PP_COMPANY_SECURITY_TEMP.USER_ID,
       PP_COMPANY_SECURITY_TEMP.GL_COMPANY_NO,
       PP_COMPANY_SECURITY_TEMP.OWNED,
       PP_COMPANY_SECURITY_TEMP.DESCRIPTION,
       PP_COMPANY_SECURITY_TEMP.STATUS_CODE_ID,
       PP_COMPANY_SECURITY_TEMP.SHORT_DESCRIPTION,
       PP_COMPANY_SECURITY_TEMP.COMPANY_SUMMARY_ID,
       PP_COMPANY_SECURITY_TEMP.AUTO_LIFE_MONTH,
       PP_COMPANY_SECURITY_TEMP.AUTO_CURVE_MONTH,
       PP_COMPANY_SECURITY_TEMP.AUTO_CLOSE_WO_NUM,
       PP_COMPANY_SECURITY_TEMP.PARENT_COMPANY_ID,
       PP_COMPANY_SECURITY_TEMP.TAX_RETURN_ID,
       PP_COMPANY_SECURITY_TEMP.CLOSING_ROLLUP_ID,
       PP_COMPANY_SECURITY_TEMP.AUTO_LIFE_MONTH_MONTHLY,
       PP_COMPANY_SECURITY_TEMP.COMPANY_TYPE,
       PP_COMPANY_SECURITY_TEMP.COMPANY_EIN,
       PP_COMPANY_SECURITY_TEMP.CWIP_ALLOCATION_MONTHS,
       PP_COMPANY_SECURITY_TEMP.PROP_TAX_COMPANY_ID,
       PP_COMPANY_SECURITY_TEMP.POWERTAX_COMPANY_ID,
       PP_COMPANY_SECURITY_TEMP.IS_LEASE_COMPANY
  from PWRPLANT.PP_COMPANY_SECURITY_TEMP;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (720, 0, 10, 4, 1, 1, 33545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033545_lease_COMPANY.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
