/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044928_pwrtax_edit_rets_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date          Revised By         Reason for Change
 || -------- ------------- ------------------ ----------------------------------------
 || 2015.2   09/09/2015    Michael Bradley    Allows users to manually edit or allocate retirements easier
 ||============================================================================
 */ 

INSERT INTO ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) VALUES (
	'powertax',
	'edit_retirements',
	'Edit Retirements',
	'w_book_retirements_and_ex',
	'Edit Retirements',
	2);
INSERT INTO ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) VALUES (
	'powertax',
	'alloc_retirements',
	'Alloc Retirements',
	'w_input_book_retirements',
	'Alloc Retirements',
	2);
INSERT INTO ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
select distinct 'powertax',	'edit_retirements',	4,	max(item_order)+1,	'Edit Retirements',	NULL,	'depr_input_book',	'edit_retirements',	1 from ppbase_menu_items where parent_menu_identifier='depr_input_book' and menu_level=4;
INSERT INTO ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
select 'powertax',	'alloc_retirements',	4,	max(item_order)+1,	'Alloc Retirements',	NULL,	'depr_input_book',	'alloc_retirements',	1 from ppbase_menu_items where parent_menu_identifier='depr_input_book' and menu_level=4;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2853, 0, 2015, 2, 0, 0, 044928, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044928_pwrtax_edit_rets_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;