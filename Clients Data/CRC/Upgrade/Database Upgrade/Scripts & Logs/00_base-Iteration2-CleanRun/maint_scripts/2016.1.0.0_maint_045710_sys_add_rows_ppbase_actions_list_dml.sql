/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045710_sys_add_rows_ppbase_actions_list_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2016.1.0  06/07/2016 Anand R       Add rows for actions drop down for Alerts
||============================================================================
*/

insert into PPBASE_ACTIONS_LIST (ID, ID_TYPE, DRILL_TO_WINDOW, TASK_ID ) VALUES ( 1, 'work_order_id', 'drill_to_actuals', null);
insert into PPBASE_ACTIONS_LIST (ID, ID_TYPE, DRILL_TO_WINDOW, TASK_ID ) VALUES ( 2, 'work_order_id', 'drill_to_estimate', null);
insert into PPBASE_ACTIONS_LIST (ID, ID_TYPE, DRILL_TO_WINDOW, TASK_ID ) VALUES ( 3, 'work_order_id', 'drill_to_forecast', null);
insert into PPBASE_ACTIONS_LIST (ID, ID_TYPE, DRILL_TO_WINDOW, TASK_ID ) VALUES ( 4, 'work_order_id', 'drill_to_commitments', null);

insert into PPBASE_ACTIONS_LIST (ID, ID_TYPE, DRILL_TO_WINDOW, TASK_ID ) VALUES ( 5, 'work_order_id', 'drill_to_fp_details', null);
insert into PPBASE_ACTIONS_LIST (ID, ID_TYPE, DRILL_TO_WINDOW, TASK_ID ) VALUES ( 6, 'work_order_id', 'drill_to_fp_dashboard', null);
insert into PPBASE_ACTIONS_LIST (ID, ID_TYPE, DRILL_TO_WINDOW, TASK_ID ) VALUES ( 7, 'work_order_id', 'drill_to_completion_window', null);
insert into PPBASE_ACTIONS_LIST (ID, ID_TYPE, DRILL_TO_WINDOW, TASK_ID ) VALUES ( 8, 'work_order_id', 'drill_to_cr', null);


insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values (32, 'ADMIN', 'drill_to_actuals', 'Actuals', 1, 'ue_drilldown');

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values (33, 'ADMIN', 'drill_to_estimate', 'Estimates', 1, 'ue_drilldown');

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values (34, 'ADMIN', 'drill_to_forecast', 'Forecast', 1, 'ue_drilldown');

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values (35, 'ADMIN', 'drill_to_commitments', 'Commitments', 1, 'ue_drilldown');

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values (36, 'ADMIN', 'drill_to_fp_details', 'FP Details', 1, 'ue_drilldown');

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values (37, 'ADMIN', 'drill_to_fp_dashboard', 'FP Dashboard', 1, 'ue_drilldown');

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values (38, 'ADMIN', 'drill_to_completion_window', 'Completion Wind', 1, 'ue_drilldown');

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values (39, 'ADMIN', 'drill_to_cr', 'CR', 1, 'ue_drilldown');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3214, 0, 2016, 1, 0, 0, 045710, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045710_sys_add_rows_ppbase_actions_list_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;