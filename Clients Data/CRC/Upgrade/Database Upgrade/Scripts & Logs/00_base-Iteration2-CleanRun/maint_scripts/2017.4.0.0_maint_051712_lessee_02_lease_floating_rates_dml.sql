 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051712_lessee_02_lease_floating_rates_dml.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.4.0.0 06/26/2018 Shane "C" Ward  Script for priming ls_lease_floating_rates from ls_ilr_group_floating_rates
 ||============================================================================
 */

declare
  PPCMSG varchar2(10) := 'PPC-MSG> ';
  PPCERR varchar2(10) := 'PPC-ERR> ';
  PPCSQL varchar2(10) := 'PPC-SQL> ';
  PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

  LL_BAD_COUNT number;
  LB_SKIP_CHECK boolean := false;
  -- change value of LB_SKIP_CHECK to TRUE if you determine there is data that is OK but will always
  -- show up as an exception. If you disable the check without fixing the data condition you will get a PK error
  -- If you decide to skip this script and your client uses Floating Rates, then none of the configuration will be carried over to the new table
  LB_SKIP_CHECK2 boolean := false;
  -- change value of LB_SKIP_CHECK2 to TRUE if you determine that the Floating Rates with no Lease assigned do not need to be brought over
begin
  DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

	if not LB_SKIP_CHECK then
		--Find Same Leases in ls_ilr_group_floating_rates with differing rates on the same effective date
		select bad_count into LL_BAD_COUNT FROM (SELECT Count(1) bad_count FROM (
		SELECT Count(rate) bad_count, lease_id, eff_date
		FROM (
		SELECT DISTINCT lease_id, To_Date(To_Char(effective_date, 'mmddyyyy') , 'mmddyyyy') eff_date, rate FROM ls_ilr_group_floating_rates)
		GROUP BY lease_id, eff_date
		HAVING Count(rate) > 1));
		
		if LL_BAD_COUNT > 0 then
		  DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA' ||
							  '-02000 - Contact PPC support for help.  Read message below.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  Please edit the script and read the comments at ');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  the bottom to see how to evaluate the data issues.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  To skip checking this in the future.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  change this line: LB_SKIP_CHECK boolean := false;');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  to:               LB_SKIP_CHECK boolean := true;');

		  RAISE_APPLICATION_ERROR(-20000, 'ILR Floating Rates Issue, contact PPC support for help.');
		else
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || 'ILR Floating Rates Setup is OK.');
		end if;
	else
		--No Validations
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'ILR Floating Rates check 1 not performed.');
	end IF;
	
	--Reset our checker
	LL_BAD_COUNT  :=0;
	
	if not LB_SKIP_CHECK2 then
		--Find Same Leases in ls_ilr_group_floating_rates with differing rates on the same effective date
		SELECT Count(1) bad_count
		into LL_BAD_COUNT 
		FROM ls_ilr_group_floating_rates
		where lease_id is null or lease_id not in (select lease_id from ls_lease);
		
		if LL_BAD_COUNT > 0 then
		  DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA' ||
							  '-02000 - Contact PPC support for help.  Read message below.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  Please edit the script and read the comments at ');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  the bottom to see how to evaluate the data issues.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  To skip checking this in the future.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  change this line: LB_SKIP_CHECK boolean := false;');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  to:               LB_SKIP_CHECK boolean := true;');

		  RAISE_APPLICATION_ERROR(-20000, 'ILR Floating Rates Issue, contact PPC support for help.');
		else
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || 'ILR Floating Rates Setup is OK.');
		end if;
	else
		--No Validations
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'ILR Floating Rates check 2 not performed.');
	end IF;


end;
/
				 
/*
Validation is for clients leveraging ILR Floating Rates Calculations

LS_ILR_GROUP_FLOATING_RATES is no longer used for maintaining floating rates, going forward it is ls_lease_floating_rates.

PK for ls_lease_floating_rate is lease_id and effective_date, for LS_ILR_GROUP_FLOATING_RATES it is lease_id, ilr_id, and effective_date
The FIRST check looks for differing rates by lease_id and effective_date. If an effective_date for a Lease has differing Rates, the rate will need to made to match or
the client/implementer will skip this data priming script. If this script is skipped, then clients leveraging Floating rates will need to configure Floating Rates through the MLA Details

The SECOND check looks for Floating rates in ls_ilr_group_floating_rates with no/invalid lease_id assigned. If you deem that those rates do not need to be brought over, 
LB_SKIP_CHECK2 can be set to true. The data with invalid Lease_ids or no lease_id will not be brought over.
*/

--Checks performed, Load our new table from ls_ilr_group_floating_rates
INSERT INTO ls_lease_floating_rates (lease_id, effective_date, rate)
SELECT DISTINCT lease_id, effective_date, rate
FROM ls_ilr_group_floating_Rates
WHERE lease_id IN (SELECT lease_id FROM ls_lease);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7623, 0, 2017, 4, 0, 0, 51712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051712_lessee_02_lease_floating_rates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	