 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041901_pcm_dynamic_filters_dml.sql
|| Description: Create "Work Order" Dynamic Filter
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	12/17/2014 Ryan Oliveria  New Module
||============================================================================
*/

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION)
values
	(259, 'Work Order', 'sle', 'work_order_control.description || '' '' || work_order_control.work_order_number');

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 259) /*Work Order*/;

delete from PP_DYNAMIC_FILTER_MAPPING
 where FILTER_ID = 53
   and PP_REPORT_FILTER_ID = 87; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2123, 0, 2015, 1, 0, 0, 041901, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041901_pcm_dynamic_filters_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;