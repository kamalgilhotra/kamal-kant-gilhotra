/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035317_taxrpr_menu_consolidate2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/15/2014 Alex P.        Point Release
||============================================================================
*/

update REPAIR_UNIT_CODE
   set QUANTITY_VS_COST = 2
 where exists (select 1
          from REPAIR_THRESHOLDS
         where REPLACEMENT_COST is not null
           and REPLACEMENT_QUANTITY is null
           and REPAIR_THRESHOLDS.REPAIR_UNIT_CODE_ID = REPAIR_UNIT_CODE.REPAIR_UNIT_CODE_ID)
   and REPAIR_UNIT_CODE.QUANTITY_VS_COST is null;

update REPAIR_UNIT_CODE set QUANTITY_VS_COST = 1 where QUANTITY_VS_COST is null;

alter table REPAIR_UNIT_CODE modify quantity_vs_cost number(2, 0) not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (857, 0, 10, 4, 2, 0, 35317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035317_taxrpr_menu_consolidate2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;