/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_043084_cwip_ocr_02_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   3/10/2015   Sunjin Cone     CPI Retro Calc Input adjustments
||============================================================================
*/ 
 
insert into  CPI_RETRO_ADJUST_TYPE (cpi_retro_adjust_type_id, description)
values (1, 'New for Retro Calc');

insert into  CPI_RETRO_ADJUST_TYPE (cpi_retro_adjust_type_id, description)
values (2, 'Copied from Month End');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2363, 0, 2015, 1, 0, 0, 42819, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042819_cwip_cpiretro_02_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;