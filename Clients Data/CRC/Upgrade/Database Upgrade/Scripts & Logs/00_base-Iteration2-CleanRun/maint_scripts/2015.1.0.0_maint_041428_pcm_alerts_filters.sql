/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041428_pcm_alerts_filters.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Created By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   11/25/2015 Ryan Oliveria 	Save/Restore Alerts Filter
||============================================================================
*/

insert into PP_DYNAMIC_FILTER_TYPE (FILTER_TYPE_ID, DESCRIPTION) values (4, 'Alerts');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2071, 0, 2015, 1, 0, 0, 041428, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041428_pcm_alerts_filters.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;