/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045051_cwip_ocr_wo_est_build_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By            Reason for Change
|| -------- ---------- --------------------  ---------------------------------
|| 2015.2   10/26/2015 Anand R              Add new row to pp_wo_est_customize
||============================================================================
*/ 

-- Add row for the ocr_api window

insert into pp_wo_est_customize (COMPANY_ID, TAB, COLUMN_NAME, DISPLAY, COLUMN_ORDER, COLUMN_WIDTH, CR_ELEMENT_ID, DDDW_SQL)
values (-1, 1, 'ocr_api', 1, 105, 250, null, null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2938, 0, 2015, 2, 0, 0, 45051, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045051_cwip_ocr_wo_est_build_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;