/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045140_lease_build_forecast_wksp_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/23/2015 Ryan Oliveria  Drop FK to allow -1 company_id
||============================================================================
*/

alter table ls_forecast_rates
drop constraint ls_forecast_rates_fk2;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2939, 0, 2015, 2, 0, 0, 45140, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045140_lease_build_forecast_wksp_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;