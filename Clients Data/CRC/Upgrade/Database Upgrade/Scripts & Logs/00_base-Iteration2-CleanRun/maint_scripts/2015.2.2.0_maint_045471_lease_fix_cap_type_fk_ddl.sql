/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045471_lease_fix_cap_type_fk_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 02/26/2016 Will Davis 	 Fix bad lease cap type foreign key
|| 2015.2.2.0 04/14/2016 David Haupt	 Backporting to 2015.2.2
||============================================================================
*/

ALTER TABLE LS_LEASE_CAP_TYPE DROP CONSTRAINT FK1_LS_TYPE_BK_SUMMARY;

ALTER TABLE LS_LEASE_CAP_TYPE
  ADD CONSTRAINT FK1_LS_TYPE_BK_SUMMARY FOREIGN KEY (
    BOOK_SUMMARY_ID
  ) REFERENCES BOOK_SUMMARY (
    BOOK_SUMMARY_ID
  )
;

COMMENT ON COLUMN ls_lease_cap_type.ls_lease_cap_type_id IS 'The internal PowerPlan lease capital type grouping';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3071, 0, 2015, 2, 2, 0, 045471, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045471_lease_fix_cap_type_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;