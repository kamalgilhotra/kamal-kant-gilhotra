/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038458_lease_drop_tax_company.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 07/09/2014 Kyle Peterson
||========================================================================================
*/

delete from LS_TAX_DISTRICT_RATES
 where (TAX_LOCAL_ID, LS_TAX_DISTRICT_ID, COMPANY_ID) in
       (select TAX_LOCAL_ID, LS_TAX_DISTRICT_ID, COMPANY_ID
          from (select TAX_LOCAL_ID,
                       LS_TAX_DISTRICT_ID,
                       COMPANY_ID,
                       ROW_NUMBER() OVER(partition by TAX_LOCAL_ID, LS_TAX_DISTRICT_ID order by COMPANY_ID) as NUM
                  from LS_TAX_DISTRICT_RATES)
         where NUM > 1);

delete from LS_TAX_STATE_RATES
 where (TAX_LOCAL_ID, STATE_ID, COMPANY_ID) in (select TAX_LOCAL_ID, STATE_ID, COMPANY_ID
                                                  from (select TAX_LOCAL_ID,
                                                               STATE_ID,
                                                               COMPANY_ID,
                                                               ROW_NUMBER() OVER(partition by TAX_LOCAL_ID, STATE_ID order by COMPANY_ID) as NUM
                                                          from LS_TAX_STATE_RATES)
                                                 where NUM > 1);


alter table LS_TAX_DISTRICT_RATES drop constraint FK_LS_TAX_DISTRICT_RATES3;

alter table LS_TAX_DISTRICT_RATES drop constraint PK_LS_TAX_DISTRICT_RATES;

alter table LS_TAX_STATE_RATES drop constraint FK_LS_TAX_STATE_RATES3;

alter table LS_TAX_STATE_RATES drop primary key drop index;

alter table LS_TAX_DISTRICT_RATES drop column COMPANY_ID;

alter table LS_TAX_DISTRICT_RATES
   add constraint PK_LS_TAX_DISTRICT_RATES
       primary key (TAX_LOCAL_ID, LS_TAX_DISTRICT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_TAX_STATE_RATES drop column COMPANY_ID;

alter table LS_TAX_STATE_RATES
   add constraint PK_LS_TAX_STATE_RATES
       primary key (TAX_LOCAL_ID, STATE_ID)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1244, 0, 10, 4, 3, 0, 38458, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038458_lease_drop_tax_company.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
