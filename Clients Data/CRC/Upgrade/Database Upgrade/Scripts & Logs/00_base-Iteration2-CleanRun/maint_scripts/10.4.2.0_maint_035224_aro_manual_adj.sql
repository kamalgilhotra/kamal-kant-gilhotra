/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035224_aro_manual_adj.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/20/2014 Ryan Oliveria
||============================================================================
*/

alter table ARO_LIABILITY_ADJ add DEPR_LEDGER_INCLUDE number(1,0);

alter table ARO_LIABILITY_ADJ
   add constraint ARO_LIAB_ADJ_DL_INC_YES_NO
       foreign key (DEPR_LEDGER_INCLUDE)
       references YES_NO (YES_NO_ID);

comment on column ARO_LIABILITY_ADJ.DEPR_LEDGER_INCLUDE is 'Indicates whether or not this adjustment will be included in the depr ledger''s cost of removal.  This should only be 1 for regulated ARO''s.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (884, 0, 10, 4, 2, 0, 35224, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035224_aro_manual_adj.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;