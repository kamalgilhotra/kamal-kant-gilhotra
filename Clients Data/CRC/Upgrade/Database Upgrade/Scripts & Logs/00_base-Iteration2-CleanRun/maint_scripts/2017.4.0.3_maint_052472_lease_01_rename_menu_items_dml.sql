/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052472_lease_01_rename_menu_items_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.3 10/9/2018 C Yura 	Rename Initiate Menu Items so PPBase Security will work on them
||============================================================================
*/

update ppbase_menu_items 
set label = label || ' Approvals'
WHERE Upper(MODULE) IN ('LESSEE', 'LESSOR')
and Upper(parent_menu_identifier) = 'MENU_WKSP_APPROVAL'
AND LOWER(LABEL) IN ('mla');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10282, 0, 2017, 4, 0, 3, 52472, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052472_lease_01_rename_menu_items_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
