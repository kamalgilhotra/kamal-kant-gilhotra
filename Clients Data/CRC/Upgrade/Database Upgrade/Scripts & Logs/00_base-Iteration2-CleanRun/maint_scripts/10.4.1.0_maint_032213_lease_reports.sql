/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032213_lease_reports.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/09/2013 Ryan Oliveria  Point Release
||============================================================================
*/

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (41, 'Lessee - Set of Books', 'set_of_books', 'uo_ls_selecttabs_setofbooks');

update PP_REPORTS set PP_REPORT_FILTER_ID = 41 where DATAWINDOW = 'dw_ls_rpt_asset_retirement';

insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER)
values
   (307, 'Lessee - Audits', 1);

insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER)
values
   (308, 'Lessee - Schedules', 2);

insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER)
values
   (309, 'Lessee - Other', 99);

update PP_REPORTS
   set REPORT_TYPE_ID = 307, REPORT_TYPE = 'Lessee - Audits'
 where DATAWINDOW in ('dw_ls_rpt_mla_without_ilr', 'dw_ls_rpt_asset_without_ilr');

update PP_REPORTS
   set REPORT_TYPE_ID = 309, REPORT_TYPE = 'Lessee - Other'
 where REPORT_TYPE_ID = 306;

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (42, 'Lessee - Schedule', 'company_setup', 'uo_ls_selecttabs_schedule');

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Schedule by Asset',
          'Schedule by Asset',
          'Lessee',
          'dw_ls_rpt_schedule_by_asset',
          'already has report time',
          'Lessee - Schedules',
          'Span',
          'Lessee - 1014',
          null,
          null,
          null,
          11,
          308, --Report Type
          2, --Time Option
          42, --Filter ID
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1014',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Schedule by ILR',
          'Schedule by ILR',
          'Lessee',
          'dw_ls_rpt_schedule_by_ilr',
          'already has report time',
          'Lessee - Schedules',
          'Span',
          'Lessee - 1015',
          null,
          null,
          null,
          11,
          308, --Report Type
          2, --Time Option
          42, --Filter ID
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1015',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Schedule by MLA',
          'Schedule by MLA',
          'Lessee',
          'dw_ls_rpt_schedule_by_mla',
          'already has report time',
          'Lessee - Schedules',
          'Span',
          'Lessee - 1016',
          null,
          null,
          null,
          11,
          308, --Report Type
          2, --Time Option
          42, --Filter ID
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1016',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Schedule by Company',
          'Schedule by Company',
          'Lessee',
          'dw_ls_rpt_schedule_by_company',
          'already has report time',
          'Lessee - Schedules',
          'Span',
          'Lessee - 1017',
          null,
          null,
          null,
          11,
          308, --Report Type
          2, --Time Option
          42, --Filter ID
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1017',
          null,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (597, 0, 10, 4, 1, 0, 32213, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032213_lease_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;