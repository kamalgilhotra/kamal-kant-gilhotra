/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053128_lessee_01_backfill_po_type.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.2 02/26/2019  C yura           backfill purch/term option fields
||============================================================================
*/

SET SERVEROUTPUT ON

-- Raise exception if there is more than 1 likely option
declare
   L_COUNT number;
begin

   L_COUNT := 0;
		select count(*) into L_COUNT
		from LS_ILR_OPTIONS a,  ls_ilr_purchase_options b, ls_ilr c
		where a.ilr_id = b.ilr_id
		and a.revision = b.revision
		and a.ilr_id = c.ilr_id
		and a.revision = c.current_revision
		and b.ILR_PURCHASE_PROBABILITY_ID = 1
		and nvl(a.purchase_option_type_id,1) <> nvl(b.purchase_option_type,1);
    
    
    if L_COUNT > 0 then
      RAISE_APPLICATION_ERROR(-20000,
                              'There are ILRs where current revision ls_ilr_options.purchase_option_type does not equal the type of the Likely option in ls_ilr_purchase_Options. Review and resolve manually.');
   end if;
	
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15484, 0, 2018, 1, 0, 2, 53128, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053128_lessee_01_backfill_po_type.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;