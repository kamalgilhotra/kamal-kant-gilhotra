--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042917_taxrpr_ltd_prfmc3_20151_ddl.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1   03/10/2015 A Scott            Improve LTD performance
--||                                        Exact same as 10.4.3.4 script. Made
--||                                        re-runnable because 10.4.3.4 may be 
--||                                        released after 2015.1.  So this ensures
--||                                        the fix goes in regardless of which code
--||                                        is used by the client.
--||============================================================================
--*/

begin
   execute immediate 'DROP INDEX CPR_ACT_RPR_MO_IX';
   DBMS_OUTPUT.PUT_LINE('Index CPR_ACT_RPR_MO_IX dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Index CPR_ACT_RPR_MO_IX does not exist or drop index priv not granted.');
end;
/

create index CPR_ACT_RPR_MO_IX on CPR_ACTIVITY (nvl( tax_orig_month_number, month_number ))
  tablespace PWRPLANT_IDX;

--declare
--   CODE number(22, 0);
--begin
--   CODE := ANALYZE_TABLE('CPR_ACTIVITY', -1);
--   DBMS_OUTPUT.PUT_LINE('CPR_ACTIVITY analyze return code : ' || CODE);
--end;
--
--/

begin
   execute immediate 'DROP TABLE TEMP_WOS_IN_SCHEMA';
   DBMS_OUTPUT.PUT_LINE('Table TEMP_WOS_IN_SCHEMA dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TEMP_WOS_IN_SCHEMA does not exist or drop table priv not granted.');
end;
/

CREATE TABLE TEMP_WOS_IN_SCHEMA
(
   WORK_ORDER_NUMBER varchar2(35),
   REPAIR_LOCATION_ID number(22,0),
   REPAIR_LOCATION_METHOD_ID number(22,0),
   ADD_RETIRE_TOLERANCE_PCT number(22,8),
   EXPENSE_PERCENT number(22,8),
   ADD_RETIRE_CIRCUIT_PCT number(22,8),
   BLANKET_METHOD number(22,0),
   WORK_ORDER_ID number(22,0)
);

alter table TEMP_WOS_IN_SCHEMA
add constraint TEMP_WOS_IN_SCHEMA_PK
primary key (WORK_ORDER_NUMBER)
using index 
tablespace PWRPLANT_IDX;


comment on table TEMP_WOS_IN_SCHEMA
  is '(C)  [18] The Temp Wos In Schema table is a staging table used in tax repairs processing used to hold the work orders and their attributes of a repair schema.';

comment on column TEMP_WOS_IN_SCHEMA.WORK_ORDER_NUMBER is 'Work order number of the work order.';
comment on column TEMP_WOS_IN_SCHEMA.REPAIR_LOCATION_ID is 'The tax repair location associated with this work order.';
comment on column TEMP_WOS_IN_SCHEMA.REPAIR_LOCATION_METHOD_ID is 'System-assigned identifier of a repair location. This will be used to default the repair location method id for work orders that inherit from this work order type.';
comment on column TEMP_WOS_IN_SCHEMA.ADD_RETIRE_TOLERANCE_PCT is 'Add retire tolerance percentage specified on the repair test.';
comment on column TEMP_WOS_IN_SCHEMA.EXPENSE_PERCENT is 'The percent threshold for a work order to qualify for repairs (10% in the Electric T and D case for example).';
comment on column TEMP_WOS_IN_SCHEMA.ADD_RETIRE_CIRCUIT_PCT is 'The add/retire circuit percent specified on the repair test.';
comment on column TEMP_WOS_IN_SCHEMA.BLANKET_METHOD is 'The work order''s blanket method (Pro-rata, Proportional, Cost, All Cost).';
comment on column TEMP_WOS_IN_SCHEMA.WORK_ORDER_ID is 'System-assigned identifier of the work order.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2367, 0, 2015, 1, 0, 0, 42917, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042917_taxrpr_ltd_prfmc3_20151_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;