/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039946_lease_table_column_comments.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/11/2014 Daniel Motter    Adding table and column comments
||============================================================================
*/

comment on table LS_RECONCILE_TYPE is 'Lookup table for reconciliation level';

comment on column LS_RECONCILE_TYPE.LS_RECONCILE_TYPE_ID is 'Identifier for the reconciliation level. 1 - Asset, 2 - ILR, 3 - MLA';
comment on column LS_RECONCILE_TYPE.DESCRIPTION is 'A description of the reconciliation level (Asset, ILR, or MLA)';
comment on column LS_RECONCILE_TYPE.USER_ID is 'The user who added the row';
comment on column LS_RECONCILE_TYPE.TIME_STAMP is 'The time the row was added';

comment on column LS_IMPORT_S_TAX_RATES_ARCHIVE.STATE_XLATE is 'Translation field for the state identifier';
comment on column LS_IMPORT_S_TAX_RATES_ARCHIVE.STATE_ID is 'Identifier for the state';

comment on column LS_IMPORT_INVOICE.ILR_ID is 'Identifier for which ILR is associated with the invoice';
comment on column LS_IMPORT_INVOICE.ILR_XLATE is 'Translation field for the ILR identifier';

comment on column LS_IMPORT_INVOICE_ARCHIVE.ILR_ID is 'Identifier for which ILR is associated with the invoice';
comment on column LS_IMPORT_INVOICE_ARCHIVE.ILR_XLATE is 'Translation field for the ILR identifier';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1418, 0, 10, 4, 3, 0, 39946, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039946_lease_table_column_comments.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
