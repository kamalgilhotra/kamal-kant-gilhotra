/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050748_lessor_01_add_mass_approval_workspace_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------  ------------------------------------
|| 2017.3.0.0 04/05/2018 Jared Watkins   Add the new menu item and workspace identifier for the Mass Approvals wksp
||============================================================================
*/

insert into ppbase_workspace(module, workspace_identifier, label, workspace_uo_name, object_type_id)
values('LESSOR', 'import_mass_approval_wksp', 'Mass Approval Tool', 'uo_lsr_mass_approval_wksp', 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
values('LESSOR', 'ilr_mass_approval', 2, 2, 'Mass Approval Tool', 'menu_wksp_import', 'import_mass_approval_wksp', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4274, 0, 2017, 3, 0, 0, 50748, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050748_lessor_01_add_mass_approval_workspace_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;