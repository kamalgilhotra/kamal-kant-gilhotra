/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052280_lessee_lessor_01_add_gl_co_import_option_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.3 9/27/2018  C Yura           Add translate option for GL Company No to company fields in lessee/lessor imports
||============================================================================
*/


INSERT INTO
       pp_import_lookup
        (
                import_lookup_id           ,
                time_stamp                 ,
                user_id                    ,
                description                ,
                long_description           ,
                column_name                ,
                lookup_sql                 ,
                is_derived                 ,
                lookup_table_name          ,
                lookup_column_name         ,
                lookup_constraining_columns,
                lookup_values_alternate_sql,
                derived_autocreate_yn
        )
        VALUES
        (
                4516                                    ,
                SYSDATE                                 ,
                USER                                    ,
                'Lease Company.GL Company No - No Constraint'           ,
                'The passed in value corresponds to the Company: GL Company Number field.  Translate to the Company ID using the GL Company No column on the Company table.',
                'company_id'                  ,
                '( select co.company_id from company co where upper( trim( <importfield> ) ) = upper( trim( co.gl_company_no ) ) and is_lease_company = 1 )',
			        	0,
                'company',
                'gl_company_no',
                null                   ,
                'select description from company where is_lease_company = 1'                   ,
                NULL
        );

insert into PP_IMPORT_COLUMN_LOOKUP
(import_Type_id, column_name, import_lookup_id)
select c.import_type_id, c.column_name, d.import_lookup_id
from pp_import_subsystem a,  PP_IMPORT_TYPE_SUBSYSTEM b, pp_import_column c, pp_import_lookup d
where a.description in ('Lessee','Lessor')
and a.import_subsystem_id = b.import_subsystem_id
and b.import_type_id = c.import_type_id
and lower(c.column_name) = 'company_id'
and d.import_lookup_id = 4516
and not exists (select 1 from PP_IMPORT_COLUMN_LOOKUP e where e.import_type_id = c.import_Type_id and c.column_name = e.column_name and e.import_lookup_id = d.import_lookup_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10023, 0, 2017, 4, 0, 3, 52280, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052280_lessee_lessor_01_add_gl_co_import_option_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

