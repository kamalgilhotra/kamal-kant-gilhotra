/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_044208_budgetcap_overheads_active_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/02/2015 Ryan Oliveria  Redesign Overhead Setup
||============================================================================
*/

alter table CLEARING_WO_CONTROL_BDG add ACTIVE number(1,0);

comment on column CLEARING_WO_CONTROL_BDG.ACTIVE is 'Yes (1), No (0,null). If ''No'', this overhead will be ignored for processing.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2680, 0, 2015, 2, 0, 0, 044208, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044208_budgetcap_overheads_active_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;