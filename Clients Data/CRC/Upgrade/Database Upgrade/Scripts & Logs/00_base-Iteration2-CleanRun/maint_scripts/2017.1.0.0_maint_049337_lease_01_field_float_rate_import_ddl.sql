/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049337_lease_01_field_float_rate_import_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/


alter table ls_import_float_rates
add (lease_xlate varchar2(254), lease_id number(22,0))
;
alter table ls_import_float_rates_archive
add (lease_xlate varchar2(254), lease_id number(22,0))
;

COMMENT ON COLUMN ls_import_float_rates.lease_xlate IS 'Translation field for determining the MLA.';
COMMENT ON COLUMN ls_import_float_rates.lease_id IS 'The internal lease id within PowerPlant.';

COMMENT ON COLUMN ls_import_float_rates_archive.lease_xlate IS 'Translation field for determining the MLA.';
COMMENT ON COLUMN ls_import_float_rates_archive.lease_id IS 'The internal lease id within PowerPlant.';
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4017, 0, 2017, 1, 0, 0, 49337, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049337_lease_01_field_float_rate_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;