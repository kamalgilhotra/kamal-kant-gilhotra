/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041551_pcm_forecast_attributes_ddl.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	12/02/2014 Chris Mardis	       Dynamic project forecasting attributes
||========================================================================================
*/

create table wo_est_forecast_attributes (
   attribute_name varchar2(60),
   user_id varchar2(18),
   time_stamp date,
   id_column varchar2(4000),
   desc_column varchar2(4000),
   group_by_column varchar2(4000),
   column_name varchar2(60),
   id_column_act varchar2(4000),
   desc_column_act varchar2(4000),
   group_by_column_act varchar2(4000),
   column_header varchar2(60),
   project_where_clause varchar2(4000),
   estimate_where_clause varchar2(4000),
   active number(22)
   );

alter table wo_est_forecast_attributes add constraint wo_est_fcst_attr_pk primary key (attribute_name);

comment on table wo_est_forecast_attributes is 'Holds the master list of project attributes that can be used in the Project Forecasting screen. Only those attributes that are active (active=1) will appear in the list of available attributes.';

comment on column wo_est_forecast_attributes.attribute_name is 'Name of the project attribute that will be displayed in the list of available attributes.';
comment on column wo_est_forecast_attributes.user_id is 'Standard system-assigned user id for audit purposes.';
comment on column wo_est_forecast_attributes.time_stamp is 'Standard system-assigned time stamp for audit purposes.';
comment on column wo_est_forecast_attributes.id_column is 'SQL used to retrieve the system-assigned attribute identifier for Estimates and Forecasts records.';
comment on column wo_est_forecast_attributes.desc_column is 'SQL used to retrieve the attribute description for Estimates and Forecasts records.';
comment on column wo_est_forecast_attributes.group_by_column is 'SQL used to group the attribute identifiers and descriptions for Estimates and Forecasts records.';
comment on column wo_est_forecast_attributes.column_name is 'Name of the attribute column used to generate the editable grid.';
comment on column wo_est_forecast_attributes.id_column_act is 'SQL used to retrieve the system-assigned attributed identifier for Actuals and Commitments records.';
comment on column wo_est_forecast_attributes.desc_column_act is 'SQL used to retrieve the attribute description for Actuals and Commitments records.';
comment on column wo_est_forecast_attributes.group_by_column_act is 'SQL used to group the attribute identifiers and descriptions for Actuals and Commitments records.';
comment on column wo_est_forecast_attributes.column_header is 'Header of the attribute column to be displayed at the top of the editable grid.';
comment on column wo_est_forecast_attributes.project_where_clause is 'SQL used to identify the project-level records to be updated.';
comment on column wo_est_forecast_attributes.estimate_where_clause is 'SQL used to identify the estimate-level records to be updated.';
comment on column wo_est_forecast_attributes.active is 'Flag indicating whether the project attribute should appear in the list of available attributes. 1=Yes / 0=No.';

alter table wo_est_forecast_options add (
   template_locked number(22)
   );

comment on column wo_est_forecast_options.template_locked is 'Flag indicating whether a given forecast template is locked for users that do not have access to the "Lock Template" flag.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2101, 0, 2015, 1, 0, 0, 41551, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041551_pcm_forecast_attributes_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;