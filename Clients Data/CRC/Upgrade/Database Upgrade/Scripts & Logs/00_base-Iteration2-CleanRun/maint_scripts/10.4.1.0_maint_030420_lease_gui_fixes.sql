/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030420_lease_gui_fixes.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Kyle Peterson  Point release
||============================================================================
*/

alter table LS_LEASE
   drop (TAX_CAPITAL_YN_SW, IFRS_CAPITAL_YN_SW, CAPITAL_YN_SW, RIDER_AGREEMENT_DATE, NOTIFICATION_DATE);

alter table LS_LEASE
   add INITIATION_DATE date;

update LS_ILR_STATUS
   set DESCRIPTION = 'Initiated', LONG_DESCRIPTION = 'Initiated'
 where ILR_STATUS_ID = 1;

update LS_ILR_STATUS
   set DESCRIPTION = 'Pending Approval', LONG_DESCRIPTION = 'Pending Approval'
 where ILR_STATUS_ID = 2;

update LS_ILR_STATUS
   set DESCRIPTION = 'Rejected', LONG_DESCRIPTION = 'Rejected'
 where ILR_STATUS_ID = 3;

update LS_ILR_STATUS
   set DESCRIPTION = 'Open', LONG_DESCRIPTION = 'Open'
 where ILR_STATUS_ID = 4;

update LS_ILR_STATUS
   set DESCRIPTION = 'Closed', LONG_DESCRIPTION = 'Closed'
 where ILR_STATUS_ID = 5;

insert into LS_ILR_STATUS
   (ILR_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (6, 'Old Revision', 'Old Revision');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (439, 0, 10, 4, 1, 0, 30420, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030420_lease_gui_fixes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
