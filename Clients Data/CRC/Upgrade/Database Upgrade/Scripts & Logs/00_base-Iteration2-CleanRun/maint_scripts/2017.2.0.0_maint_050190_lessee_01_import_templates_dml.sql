/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_050190_lessee_01_import_templates_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.2.0.0  02/14/2018 Sarah Byers    New Lease Import Templates
||============================================================================
*/

/* Create Lease Import Base Templates to Include All Fields */
-- Lessor
INSERT INTO "PP_IMPORT_TEMPLATE" ("IMPORT_TEMPLATE_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "DESCRIPTION", "LONG_DESCRIPTION", "CREATED_BY", "CREATED_DATE", 
"DO_UPDATE_WITH_ADD", "UPDATES_IMPORT_LOOKUP_ID", "FILTER_CLAUSE", "IS_AUTOCREATE_TEMPLATE") VALUES ((SELECT Max(import_template_id) +1 FROM pp_import_template), to_date('2018-01-11 13:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'),
 'Lessor Add - All Fields', 'Lessor Add - All Fields', 'PWRPLANT', to_date('2018-01-11 13:39:39', 'yyyy-mm-dd hh24:mi:ss'), 1, null, null, 0) ;
-- MLA
INSERT INTO "PP_IMPORT_TEMPLATE" ("IMPORT_TEMPLATE_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "DESCRIPTION", "LONG_DESCRIPTION", "CREATED_BY", "CREATED_DATE", 
"DO_UPDATE_WITH_ADD", "UPDATES_IMPORT_LOOKUP_ID", "FILTER_CLAUSE", "IS_AUTOCREATE_TEMPLATE") VALUES ((SELECT Max(import_template_id) +1 FROM pp_import_template), to_date('2018-01-19 13:26:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'),
 'MLA Add - All Fields', 'MLA Add - All Fields', 'PWRPLANT', to_date('2018-01-19 13:26:50', 'yyyy-mm-dd hh24:mi:ss'), 1, null, null, 0) ;
-- ILR
INSERT INTO "PP_IMPORT_TEMPLATE" ("IMPORT_TEMPLATE_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "DESCRIPTION", "LONG_DESCRIPTION", "CREATED_BY", "CREATED_DATE", 
"DO_UPDATE_WITH_ADD", "UPDATES_IMPORT_LOOKUP_ID", "FILTER_CLAUSE", "IS_AUTOCREATE_TEMPLATE") VALUES ((SELECT Max(import_template_id) +1 FROM pp_import_template), to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'),
 'ILR Add - All Fields', 'ILR Add - All Fields', 'PWRPLANT', to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 1, null, null, 0) ;
-- Leased Asset
INSERT INTO "PP_IMPORT_TEMPLATE" ("IMPORT_TEMPLATE_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "DESCRIPTION", "LONG_DESCRIPTION", "CREATED_BY", "CREATED_DATE", 
"DO_UPDATE_WITH_ADD", "UPDATES_IMPORT_LOOKUP_ID", "FILTER_CLAUSE", "IS_AUTOCREATE_TEMPLATE") VALUES ((SELECT Max(import_template_id) +1 FROM pp_import_template), to_date('2018-01-19 13:30:49', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'),
 'Leased Asset Add - All Fields', 'Leased Asset Add - All Fields', 'PWRPLANT', to_date('2018-01-19 13:30:49', 'yyyy-mm-dd hh24:mi:ss'), 1, null, null, 0) ;


/* Configure Fields for the New Import Templates */
-- Lessor
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 1, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'unique_lessor_identifier', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 2, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'description', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 3, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'long_description', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 4, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'zip', null, null, null, null) ; 	
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 5, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'county_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee/Lessor County ID'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 6, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'state_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'State.Long Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 7, to_date('2018-01-12 14:57:24', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'country_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Country Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 8, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'external_lessor_number', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 9, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'multi_vendor_yn_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 10, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'lease_group_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Lease Group Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 11, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'address1', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 12, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'address2', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 13, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'address3', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 14, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'address4', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 15, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'postal', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 16, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'city', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 17, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'phone_number', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 18, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'extension', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 19, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'fax_number', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 20, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'site_code', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 21, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'vendor_description', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 22, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'vendor_long_description', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 23, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'external_vendor_number', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 24, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'remit_addr', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 25, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'primary', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Add - All Fields'), 26, to_date('2018-01-11 14:26:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'), 
'unique_vendor_indentifier', null, null, null, null) ; 
-- MLA
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 1, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'unique_lease_identifier', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 2, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'lease_number', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 3, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'description', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 4, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'long_description', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 5, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'lessor_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Lessor Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 6, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'lease_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Lease Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 7, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'payment_due_day', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 8, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'pre_payment_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 9, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'lease_group_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Lease Group Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 10, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'lease_cap_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Lease Cap Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 11, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'workflow_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Workflow Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 12, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'master_agreement_date', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 13, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'notes', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 14, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'purchase_option_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Purchase Option Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 15, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'purchase_option_amt', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 16, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'renewal_option_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Renewal Option Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 17, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'cancelable_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Cancelable Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 18, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'itc_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 19, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'partial_retire_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 20, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'sublet_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 21, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'company_id', (select Min(import_lookup_id) from pp_import_lookup where upper(description) = 'LEASE COMPANY.DESCRIPTION'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 22, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'specialized_asset', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 23, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'intent_to_purchase', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 24, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'sublease', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 25, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'sublease_lease_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Lessor Lease/MLA.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 26, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'max_lease_line', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 27, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'vendor_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Vendor Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 28, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'payment_pct', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 29, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id1', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 30, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value1', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 31, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id2', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 32, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value2', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 33, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id3', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 34, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value3', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 35, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id4', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 36, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value4', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 37, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id5', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 38, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value5', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 39, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id6', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 40, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value6', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 41, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id7', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 42, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value7', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 43, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id8', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 44, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value8', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 45, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id9', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 46, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value9', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 47, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id10', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 48, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value10', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 49, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id11', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 50, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value11', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 51, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id12', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 52, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value12', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 53, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id13', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 54, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value13', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 55, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id14', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 56, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value14', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 57, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id15', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 58, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value15', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 59, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id16', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 60, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value16', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 61, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id17', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 62, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value17', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 63, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id18', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 64, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value18', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 65, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id19', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 66, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value19', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 67, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_id20', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 68, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'class_code_value20', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 69, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'days_in_year', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 70, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'cut_off_day', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 71, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'lease_end_date', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 72, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'days_in_month_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 73, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'contract_currency_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Currency.ISO Code'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 74, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'auto_approve', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 75, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'auto_generate_invoices', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 76, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'ls_reconcile_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'LS Reconcile Type.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 77, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'tax_rate_option_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'LS Tax Rate Option.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'MLA Add - All Fields'), 78, to_date('2018-01-19 13:27:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'), 
'tax_summary_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'LS Tax Summary.Description'), null, null, null) ; 
-- ILR
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 1, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'unique_ilr_identifier', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 2, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'ilr_number', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 3, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'lease_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Ls Lease.Lease Number'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 4, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'company_id', (select Min(import_lookup_id) from pp_import_lookup where upper(description) = 'LEASE COMPANY.DESCRIPTION'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 5, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'est_in_svc_date', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 6, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'ilr_group_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Ilr Group Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 7, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'workflow_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Workflow Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 8, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'external_ilr', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 9, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'notes', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 10, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'intent_to_purch', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 11, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'specialized_asset', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 12, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'sublease_flag', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 13, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'sublease_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Lessor Lease/MLA.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 14, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'intercompany_lease', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 15, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'intercompany_company', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'The passed in value corresponds to the Lease company description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 16, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'inception_air', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 17, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'purchase_option_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Purchase Option Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 18, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'purchase_option_amt', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 19, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'renewal_option_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Renewal Option Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 20, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'cancelable_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Cancelable Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 21, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'itc_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 22, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'partial_retire_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 23, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'sublet_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 24, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'lease_cap_type_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Lease Cap Type Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 25, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'termination_amt', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 26, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'payment_term_id', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 27, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'payment_term_date', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 28, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'payment_freq_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Payment Freq Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 29, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'number_of_terms', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 30, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'paid_amount', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 31, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id1', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 32, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value1', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 33, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id2', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 34, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value2', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 35, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id3', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 36, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value3', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 37, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id4', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 38, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value4', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 39, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id5', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 40, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value5', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 41, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id6', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 42, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value6', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 43, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id7', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 44, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value7', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 45, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id8', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 46, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value8', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 47, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id9', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 48, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value9', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 49, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id10', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 50, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value10', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 51, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id11', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 52, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value11', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 53, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id12', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 54, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value12', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 55, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id13', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 56, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value13', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 57, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id14', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 58, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value14', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 59, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id15', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 60, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value15', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 61, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id16', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 62, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value16', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 63, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id17', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 64, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value17', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 65, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id18', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 66, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value18', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 67, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id19', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 68, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value19', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 69, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_id20', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 70, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'class_code_value20', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 71, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'funding_status_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'ILR Funding Status.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 72, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'payment_term_type_id', (select Min(import_lookup_id) from pp_import_lookup where description = 'Payment Term Type.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 73, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_1', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 74, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_2', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 75, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_3', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 76, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_4', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 77, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_5', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 78, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_6', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 79, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_7', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 80, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_8', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 81, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_9', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 82, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'c_bucket_10', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 83, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_1', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 84, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_2', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 85, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_3', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 86, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_4', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 87, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_5', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 88, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_6', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 89, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_7', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 90, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_8', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 91, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_9', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'ILR Add - All Fields'), 92, to_date('2018-01-19 13:30:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'), 
'e_bucket_10', null, null, null, null) ; 
-- Leased Asset
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 1, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'unique_asset_identifier', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 2, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'leased_asset_number', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 3, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'ilr_id', (select Min(import_lookup_id) from pp_import_lookup where description = 'LS ILR.ILR Number'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 4, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'description', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 5, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'long_description', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 6, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'quantity', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 7, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'fmv', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 8, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'company_id', (select Min(import_lookup_id) from pp_import_lookup where upper(description) = 'LEASE COMPANY.DESCRIPTION'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 9, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'bus_segment_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Business Segment.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 10, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'utility_account_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Utility Account.Description (for given Business Segment)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 11, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'sub_account_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Sub Account.Description (for given Utility Account and Bus Segment)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 12, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'retirement_unit_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Retirement Unit.Description' and long_description = 'The passed in value corresponds to the Retirement Unit: Description field.  Translate to the Retirement Unit ID using the Description column on the Retirement Unit table.'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 13, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'property_group_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Property Group.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 14, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'work_order_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Work Order Id'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 15, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'asset_location_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Asset Location.Long Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 16, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'guaranteed_residual_amount', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 17, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'expected_life', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 18, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'economic_life', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 19, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'serial_number', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 20, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'notes', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 21, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id1', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 22, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value1', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 23, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id2', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 24, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value2', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 25, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id3', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 26, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value3', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 27, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id4', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 28, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value4', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 29, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id5', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 30, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value5', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 31, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id6', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 32, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value6', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 33, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id7', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 34, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value7', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 35, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id8', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 36, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value8', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 37, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id9', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 38, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value9', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 39, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id10', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 40, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value10', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 41, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id11', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 42, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value11', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 43, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id12', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 44, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value12', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 45, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id13', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 46, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value13', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 47, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id14', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 48, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value14', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 49, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id15', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 50, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value15', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 51, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id16', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 52, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value16', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 53, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id17', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 54, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value17', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 55, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id18', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 56, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value18', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 57, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id19', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 58, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value19', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 59, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_id20', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Class Code.Description (Lease)'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 60, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'class_code_value20', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 61, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'used_yn_sw', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 62, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'contract_currency_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Currency.ISO Code'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 63, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'department_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Department.Description'), null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 64, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'property_tax_date', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 65, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'property_tax_amount', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 66, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'estimated_residual', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 67, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'tax_asset_location_id', null, null, null, null) ; 
INSERT INTO "PP_IMPORT_TEMPLATE_FIELDS" ("IMPORT_TEMPLATE_ID", "FIELD_ID", "TIME_STAMP", "USER_ID", "IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", 
"AUTOCREATE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_TEMPLATE_ID", "DERIVE_IMPORT_FIELD_ID") VALUES ((SELECT import_template_id FROM pp_import_template WHERE description = 'Leased Asset Add - All Fields'), 68, to_date('2018-01-19 13:39:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'), 
'tax_summary_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'LS Tax Summary.Description'), null, null, null) ; 

/* PP Import Columns - Fix New Columns */
UPDATE pp_import_column
SET parent_table = 'currency', is_on_table = 1, parent_table_pk_column = 'currency_id'
WHERE import_type_id IN ((SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets')) 
AND Lower(column_name) = 'contract_currency_id'
;

DELETE FROM pp_import_column WHERE import_type_id = (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s') 
AND  column_name = 'intent_to_purchase';

/* PP Import Column Lookup */
INSERT INTO pp_import_column_lookup (import_type_id, column_name, import_lookup_id) 
select (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'),'work_order_id',(SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Work Order Id')
FROM dual
WHERE NOT EXISTS (SELECT 1 FROM pp_import_column_lookup WHERE import_type_id = (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets') AND column_name = 'work_order_id' AND import_lookup_id = (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Work Order Id'))
;

INSERT INTO pp_import_column_lookup (import_type_id, column_name, import_lookup_id)
SELECT (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets'),'ilr_id', (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'LS ILR.ILR Number')
FROM dual
where NOT EXISTS (SELECT 1 FROM pp_import_column_lookup WHERE import_type_id = (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets') AND column_name = 'ilr_id' AND import_lookup_id = (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'LS ILR.ILR Number'));

/* PP Import Fields - Assign Correct Import Lookup IDs */
UPDATE pp_import_template_fields 
SET import_lookup_id = (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Yes No.Description (Yes or No)')
WHERE import_type_id = (SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s')
AND column_name IN ('auto_approve','auto_generate_invoices');

UPDATE pp_import_template_fields 
SET import_lookup_id = (SELECT import_lookup_id FROM pp_import_lookup WHERE long_description = 'Lessee Country Id')
WHERE import_type_id IN ((SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets')) 
AND column_name = 'country_id';

UPDATE pp_import_template_fields 
SET import_lookup_id = (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'LS Reconcile Type.Description')
WHERE import_type_id IN ((SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets')) 
AND column_name = 'ls_reconcile_type_id';

UPDATE pp_import_template_fields 
SET import_lookup_id = (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'LS Tax Summary.Description')
WHERE import_type_id IN ((SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets')) 
AND column_name = 'tax_summary_id';

UPDATE pp_import_template_fields 
SET import_lookup_id = (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'LS Tax Rate Option.Description')
WHERE import_type_id IN ((SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets')) 
AND column_name = 'tax_rate_option_id';

UPDATE pp_import_template_fields 
SET import_lookup_id = (SELECT import_lookup_id FROM pp_import_lookup WHERE description = 'Department.Description')
WHERE import_type_id IN ((SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Lessors'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee MLA''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee ILR''s'),(SELECT import_type_id FROM pp_import_type WHERE long_description = 'Lessee Assets')) 
AND column_name = 'department_id';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4138, 0, 2017, 2, 0, 0, 50190, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050190_lessee_01_import_templates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;