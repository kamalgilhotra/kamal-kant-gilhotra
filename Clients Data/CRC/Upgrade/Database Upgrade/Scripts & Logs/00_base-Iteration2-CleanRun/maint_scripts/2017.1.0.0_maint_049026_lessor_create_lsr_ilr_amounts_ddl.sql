/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049026_lessor_create_lsr_ilr_amounts_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/28/2017 Josh Sandler     Create LSR_ILR_AMOUNTS table
||============================================================================
*/

create table LSR_ILR_AMOUNTS
(
  ilr_id                      NUMBER(22,0) NOT NULL,
  revision                    NUMBER(22,0) NOT NULL,
  time_stamp                  DATE,
  user_id                     VARCHAR2(18),
  npv_lease_payments          NUMBER(22,2) NOT NULL,
  npv_guaranteed_residual     NUMBER(22,2) NOT NULL,
  npv_unguaranteed_residual   NUMBER(22,2) NOT NULL,
  selling_profit_loss         NUMBER(22,2) NOT NULL,
  beginning_lease_receivable  NUMBER(22,2) NOT NULL,
  beginning_net_investment    NUMBER(22,2) NOT NULL,
  cost_of_goods_sold          NUMBER(22,2) NOT NULL
);

comment on table LSR_ILR_AMOUNTS is '(C)  [06] The Lessor ILR Amounts table contains the zzz for the associated ILR.';
comment on column LSR_ILR_AMOUNTS.ilr_id is 'System-assigned identifier of a particular ILR.';
comment on column LSR_ILR_AMOUNTS.revision is 'The revision.';
comment on column LSR_ILR_AMOUNTS.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column LSR_ILR_AMOUNTS.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column LSR_ILR_AMOUNTS.npv_lease_payments is 'The net present value of the lease payments in contract currency.';
comment on column LSR_ILR_AMOUNTS.npv_guaranteed_residual is 'The net present value of the guaranteed residual in contract currency.';
comment on column LSR_ILR_AMOUNTS.npv_unguaranteed_residual is 'The net present value of the unguaranteed residul in contract currency.';
comment on column LSR_ILR_AMOUNTS.selling_profit_loss is 'The selling profit or loss in contract currency.';
comment on column LSR_ILR_AMOUNTS.beginning_lease_receivable is 'The beginning lease receivable amount in contract currency.';
comment on column LSR_ILR_AMOUNTS.beginning_net_investment is 'The beginning net investement amount in contract currency.';
comment on column LSR_ILR_AMOUNTS.cost_of_goods_sold is 'The cost of goods sold in contract currency.';

alter table LSR_ILR_AMOUNTS
  add constraint PK_LSR_ILR_AMOUNTS primary key (ILR_ID, REVISION)
  using index
  tablespace PWRPLANT_IDX;


ALTER TABLE LSR_ILR_AMOUNTS
  ADD CONSTRAINT r_lsr_ilr_amounts1 FOREIGN KEY (
    ilr_id,
    revision
  ) REFERENCES lsr_ilr_approval (
    ilr_id,
    revision
  )
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3745, 0, 2017, 1, 0, 0, 49026, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049026_lessor_create_lsr_ilr_amounts_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;