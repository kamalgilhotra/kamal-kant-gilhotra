/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053343_lessee_01_approval_delegation_control_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 04/04/2019 Shane "C" Ward System control for displaying Lease Approval Delegations
||============================================================================
*/

INSERT INTO pp_system_control_company (control_id, control_name, control_value, description, long_description, company_id, control_type)
SELECT Max(control_id) + 1, 'Lease Enable Approval Delegations', 'no', 'dw_yes_no;1', 'If yes, allow for users to access Approval Delegations workspace for Lease', -1, 'system only'
FROM pp_system_control_company;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16602, 0, 2018, 2, 0, 0, 53343, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053343_lessee_01_approval_delegation_control_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;