/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035329_taxrpr_uop_status.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/27/2014 Alex P.        Point Release
||============================================================================
*/

insert into WO_TAX_STATUS
   (TAX_STATUS_ID, DESCRIPTION, PRIORITY, EXPENSE_YN)
values
   (-999, 'Tested at UOP Level', -999, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (914, 0, 10, 4, 2, 0, 35329, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035329_taxrpr_uop_status.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;