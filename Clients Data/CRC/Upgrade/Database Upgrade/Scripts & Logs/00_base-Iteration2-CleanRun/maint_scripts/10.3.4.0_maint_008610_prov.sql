/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008610_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   02/16/2012 Blake Andrews  Point Release
||============================================================================
*/

declare
   COUNTER number;
   SQLS    varchar2(2000);

begin
   select count(*)
     into COUNTER
     from SYS.ALL_TRIGGERS
    where TRIGGER_NAME = 'TAX_ACCRUAL_SUBLEDGERTR';

   if COUNTER >= 1 then
      SQLS := 'drop trigger PWRPLANT.TAX_ACCRUAL_SUBLEDGERTR ';

      execute immediate SQLS;
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (96, 0, 10, 3, 4, 0, 8610, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_008610_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;