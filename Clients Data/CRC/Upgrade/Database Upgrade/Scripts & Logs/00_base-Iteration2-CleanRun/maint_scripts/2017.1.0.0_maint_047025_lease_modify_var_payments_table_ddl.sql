/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047025_lease_modify_var_payments_table_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 03/09/2017 Charlie Shilling payment preview option and accrual preview option
||											are being merged into a single option
||============================================================================
*/
ALTER TABLE ls_variable_payment
DROP COLUMN accrual_preview_option;

ALTER TABLE ls_variable_payment
RENAME COLUMN payment_preview_option TO preview_option;

COMMENT ON COLUMN ls_variable_payment.preview_option IS 'The number of months for which to calculate the formula preview; -1 to calculate until end of lease life.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3383, 0, 2017, 1, 0, 0, 47025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047025_lease_modify_var_payments_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	