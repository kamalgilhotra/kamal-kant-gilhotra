/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032877_lease_dept.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Ryan Oliveria  Patch Release
||============================================================================
*/

alter table LS_ASSET add DEPARTMENT_ID number(22,0);

alter table LS_ASSET
   add constraint LS_ASSET_DEPT_FK
       foreign key (DEPARTMENT_ID)
      references DEPARTMENT (DEPARTMENT_ID);

comment on column LS_ASSET.DEPARTMENT_ID is 'An internal PowerPlant ID linking to the department owning the leased asset.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (645, 0, 10, 4, 1, 1, 32877, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032877_lease_dept.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
