/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044934_pwrtax_misc_config_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date          Revised By         Reason for Change
 || -------- ------------- ------------------ ----------------------------------------
 || 2015.2   09/09/2015    Michael Bradley    Fixes label for Load Book Depr, Gives users the ability to run 6 step DIT
 || 														manually, includes sys options in new gui, gives users access to
 || 														w_tax_transfer_Tab, fix norm schema basis diff filter
 ||============================================================================
 */ 

update ppbase_workspace set label = 'Load Depr/COR/Salvage' where label ='Load Book Depreciation' and module='powertax'; 
update ppbase_menu_items set label = 'Load Depr/COR/Salvage' where label ='Load Book Depreciation' and module='powertax'; 
insert into ppbase_workspace (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp, object_type_id) values ('powertax', 'clear_book_depr', null, null, 'Clear Book Depr', 'w_tax_depr_refresh', 'Clear Book Depr', 2);
insert into ppbase_workspace (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp, object_type_id) values ('powertax', 'alloc_book_depr', null, null, 'Allocate Book Depr', 'w_group_depr_allocate_one_loop', 'Allocate Book Depr', 2);
insert into ppbase_menu_items (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax', 'clear_book_depr', null, null, 3, 4, 'Clear Book Depr', 'Clear Book Depr', 'deferred_input','clear_book_depr', 1);
insert into ppbase_menu_items (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax', 'alloc_book_depr', null, null, 3, 5, 'Allocate Book Depr', 'Allocate Book Depr', 'deferred_input','alloc_book_depr', 1);
insert into ppbase_system_options_wksp (system_option_id, workspace_identifier, time_Stamp, user_id) values ('Reverse Early In Service Months', 'depr_input_book_additions', null, null);
insert into ppbase_system_options_wksp (system_option_id, workspace_identifier, time_Stamp, user_id) values ('Forward Early In Service Months', 'depr_input_book_additions', null, null);

insert into ppbase_workspace (module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp, object_type_id) values ('powertax', 'transfer_tab', null, null, 'Transfer Tab', 'w_tax_transfer_tab', 'Transfer Tab', 2);
update ppbase_menu_items set item_order =6 where parent_menu_identifier='depr_input' and module='powertax' and label='Budget Activity';
insert into ppbase_menu_items (module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax', 'transfer_tab', null, null, 3, 5, 'Transfer Tab', 'Transfer Tab', 'depr_input','transfer_tab', 1);

update pp_dynamic_filter set sqls_column_expression = 'reconcile_item_id',
dw = 'dw_tax_reconcile_item_filter',
dw_id = 'reconcile_item_id'
where label = 'Normalized Basis Difference';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2857, 0, 2015, 2, 0, 0, 044934, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044934_pwrtax_misc_config_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;