/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030213_pwrtax_fasttax_columns2.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/19/2013 Jason Cone     Point Release
||============================================================================
*/

update TAX_ACTIVITY_CODE
   set TAX_ACTIVITY_TYPE_ID = TAX_ACTIVITY_CODE_ID
 where TAX_ACTIVITY_TYPE_ID is null
   and TAX_ACTIVITY_CODE_ID in (1, 2, 3, 4, 5, 6, 12);

update TAX_LIMIT set COMPARE_RATE = 0 where COMPARE_RATE is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (628, 0, 10, 4, 1, 0, 30213, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030213_pwrtax_fasttax_columns2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

commit;