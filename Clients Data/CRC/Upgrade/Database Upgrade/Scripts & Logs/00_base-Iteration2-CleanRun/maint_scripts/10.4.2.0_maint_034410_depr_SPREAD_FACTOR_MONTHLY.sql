/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034410_depr_SPREAD_FACTOR_MONTHLY.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 01/16/2014 Charlie Shilling create view
||============================================================================
*/

create or replace view SPREAD_FACTOR_MONTHLY as
select FACTOR_ID, DESCRIPTION, 1 as FISCAL_MONTH, FACTOR_1 as FACTOR
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 2, FACTOR_2
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 3, FACTOR_3
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 4, FACTOR_4
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 5, FACTOR_5
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 6, FACTOR_6
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 7, FACTOR_7
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 8, FACTOR_8
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 9, FACTOR_9
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 10, FACTOR_10
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 11, FACTOR_11
  from SPREAD_FACTOR
union all
select FACTOR_ID, DESCRIPTION, 12, FACTOR_12
  from SPREAD_FACTOR;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (881, 0, 10, 4, 2, 0, 34410, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034410_depr_SPREAD_FACTOR_MONTHLY.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;