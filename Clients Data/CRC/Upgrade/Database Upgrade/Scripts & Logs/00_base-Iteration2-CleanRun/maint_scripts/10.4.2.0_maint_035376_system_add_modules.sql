/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035376_system_add_modules.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/27/2014 Ron Ferentini  Point Release
||============================================================================
*/

insert into PP_MODULES
   (MODULE_ID, MODULE_NAME, disable, TIME_STAMP, USER_ID, VERSION)
   select max(MODULE_ID) + 1, 'Tax Repairs', 0, sysdate, user, '10.4.2.0' from PP_MODULES;

insert into PP_MODULES
   (MODULE_ID, MODULE_NAME, disable, TIME_STAMP, USER_ID, VERSION)
   select max(MODULE_ID) + 1, 'Regulatory', 0, sysdate, user, '10.4.2.0' from PP_MODULES;

update PP_MODULES
set VERSION = '10.4.2.0';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (912, 0, 10, 4, 2, 0, 35376, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035376_system_add_modules.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;