/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037875_pwrtax_gui_ret_rules.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 05/05/2014 Andrew Scott        Base GUI workspaces for Tax Retire Rules
||============================================================================
*/

----
----  set up the new workspace to be accessible in powertax base gui
----

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_input_iface_tax_ret_rules', 'Tax Retire Rules', 'uo_tax_depr_act_wksp_retire_rules',
    'Tax Retire Rules');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'depr_input_iface_tax_ret_rules'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_iface_tax_ret_rules';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1159, 0, 10, 4, 3, 0, 37875, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037875_pwrtax_gui_ret_rules.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

