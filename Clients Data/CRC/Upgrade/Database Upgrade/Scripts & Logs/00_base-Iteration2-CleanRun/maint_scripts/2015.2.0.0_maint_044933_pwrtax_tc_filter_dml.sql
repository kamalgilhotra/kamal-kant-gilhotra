/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044933_pwrtax_tc_filter_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date          Revised By         Reason for Change
 || -------- ------------- ------------------ ----------------------------------------
 || 2015.2   09/09/2015    Michael Bradley    Allows tax class filtering on COR/Salvage Alloc and M Item Alloc
 ||============================================================================
 */ 

insert into pp_dynamic_filter (filter_id, label, input_Type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_Description, dw_id_datatype, required, single_Select_only, valid_operators, data,  exclude_from_where, not_retrieve_immediate, invisible)
 values (90, 'Tax Class', 'dw', 'tax_class.tax_class_id', null, 'dw_tax_class_by_version_filter', 'tax_class_id', 'description', 'N', 0, 0, null, null, 0, 0, 0);
INSERT INTO pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (67, 90);
INSERT INTO pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (70, 90);

update pp_Dynamic_filter set dw = 'dw_tax_company_by_version_filter' where filter_id=13;
update pp_Dynamic_filter set dw = 'dw_tax_company_by_version_filter' where filter_id=171;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2856, 0, 2015, 2, 0, 0, 044933, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044933_pwrtax_tc_filter_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;