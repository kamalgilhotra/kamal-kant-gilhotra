/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051050_lessee_02_disc_consol_rpts_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/21/2018 Sarah Byers      Create new view for converting to consolidated currencies
||============================================================================
*/

create or replace view V_LS_COMPANY_FX (
       company_id, company_description, exchange_date, currency_from, currency_to, currency_id,
       act_rate, bal_rate, iso_code, currency_display_symbol
 ) as
WITH sc_act AS (
        select company_id, control_value
          from pp_system_control_companies
         where lower(trim(control_name)) = 'lease consolidated is curr type'),
     sc_bal AS (
        select company_id, control_value
          from pp_system_control_companies
         where lower(trim(control_name)) = 'lease consolidated bs curr type'),
     cur AS (
         select currency_id, currency_display_symbol, iso_code
           from currency),
     calc_rate AS (
         select a.company_id, a.contract_currency_id, a.company_currency_id, a.accounting_month, a.exchange_date, a.rate,
                a.exchange_rate_type_id
           from LS_LEASE_CALCULATED_DATE_RATES a)
  SELECT c.company_id                         company_id,
         c.description                        company_description,
         cr_act.exchange_date                 exchange_date,
         cr_act.currency_from                 currency_from,
         cr_bal.currency_to                   currency_to,
         cur.currency_id                      currency_id,
         nvl(act_calc_rate.rate, cr_act.rate) act_rate,
         nvl(bal_calc_rate.rate, cr_bal.rate) bal_rate,
         cur.iso_code                         iso_code,
         cur.currency_display_symbol          currency_display_symbol
  FROM   company c
         inner join sc_act
                 on c.company_id = sc_act.company_id
         inner join sc_bal
                 on c.company_id = sc_bal.company_id
         inner join currency_schema cs
                 on c.company_id = cs.company_id
         inner join cur
                 on cur.currency_id = cs.currency_id
         inner join currency_rate_default_dense cr_act
                 on cur.currency_id = cr_act.currency_to
                and cs.currency_id = cr_act.currency_to
                and cr_act.exchange_rate_type_id = to_number(sc_act.control_value)
         inner join currency_rate_default_dense cr_bal
                 on cur.currency_id = cr_bal.currency_to
                and cs.currency_id = cr_bal.currency_to
                and cr_bal.exchange_rate_type_id = to_number(sc_bal.control_value)
         left outer join calc_rate act_calc_rate
                      on cur.currency_id = act_calc_rate.company_currency_id
                     and c.company_id = act_calc_rate.company_id
                     and act_calc_rate.contract_currency_id = cr_act.currency_from
                     and act_calc_rate.exchange_date = cr_act.exchange_date
                     and act_calc_rate.exchange_rate_type_id = cr_act.exchange_rate_type_id
         left outer join calc_rate bal_calc_rate
                      on cur.currency_id = bal_calc_rate.company_currency_id
                     and c.company_id = bal_calc_rate.company_id
                     and bal_calc_rate.contract_currency_id = cr_bal.currency_from
                     and bal_calc_rate.exchange_date = cr_bal.exchange_date
                     and bal_calc_rate.exchange_rate_type_id = cr_bal.exchange_rate_type_id
 where cs.currency_type_id = 1
   and cr_act.exchange_date = cr_bal.exchange_date
   and cr_act.currency_from = cr_bal.currency_from
   and cr_act.currency_to = cr_bal.currency_to;

   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7884, 0, 2017, 4, 0, 0, 51050, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051050_lessee_02_disc_consol_rpts_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;   