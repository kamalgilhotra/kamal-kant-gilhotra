/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045173_reg_schedule2_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 11/03/2015 Shane "C" Ward  New Schedule Export DDL
||============================================================================
*/

--Add Schedule Menu Item and Clean Up Reports Parent Menu
INSERT INTO ppbase_workspace (MODULE, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
VALUES ('REG', 'uo_reg_ferc_line_item_ws', 'Schedule Configuration', 'uo_reg_ferc_line_item_ws', 'Schedule Configuration', 1);

INSERT INTO ppbase_workspace (MODULE, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
VALUES ('REG', 'uo_reg_ferc_export', 'Schedule Export', 'uo_reg_ferc_export', 'Schedule Export', 1);

INSERT INTO ppbase_menu_items (MODULE, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, enable_yn, workspace_identifier)
VALUES ('REG', 'SCHED_CONFIG', 2, 3, 'Schedule Configuration', 'Schedule Configuration', 'REPORTS', 1, 'uo_reg_ferc_line_item_ws');

UPDATE ppbase_menu_items a SET item_order = (SELECT item_order - 1 FROM ppbase_menu_items b
where a.workspace_identifier = b.workspace_identifier)
WHERE (workspace_identifier = 'uo_reg_charting_ws' AND item_order > 4)
OR (workspace_identifier = 'uo_reg_analysis_ws' AND item_order > 5)
AND item_order > 5
;

INSERT INTO ppbase_menu_items (MODULE, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, enable_yn, workspace_identifier)
VALUES ('REG', 'SCHED_EXPORT', 2, 6, 'Schedule Export', 'Schedule Configuration', 'REPORTS', 1, 'uo_reg_ferc_export');

UPDATE ppbase_menu_items SET enable_yn = 0 WHERE menu_identifier = 'CHART_DASHBOARD';
UPDATE ppbase_menu_items SET enable_yn = 0 WHERE menu_identifier = 'ANALYSIS';

--Insert pre-configured stuff
INSERT INTO REG_SCHEDULE_COL_TYPE (col_type_id, description) VALUES (
	1,
	'Line Item');
INSERT INTO REG_SCHEDULE_COL_TYPE (col_type_id, description) VALUES (
	2,
	'Line Number');
INSERT INTO REG_SCHEDULE_COL_TYPE (col_type_id, description) VALUES (
	3,
	'Results');
INSERT INTO REG_SCHEDULE_COL_TYPE (col_type_id, description) VALUES (
	4,
	'Formula');

INSERT INTO REG_SCHEDULE_COL_TYPE (col_type_id, description) VALUES (
	5,
	'Filtered');

INSERT INTO REG_SCHEDULE_COL_FORMAT (col_format_id, description, format_mask) VALUES (
	1,
	'Number',
	NULL);
	
INSERT INTO REG_SCHEDULE_COL_FORMAT (col_format_id, description, format_mask) VALUES (
	2,
	'Currency',
	NULL);
INSERT INTO REG_SCHEDULE_COL_FORMAT (col_format_id, description, format_mask) VALUES (
	3,
	'Percentage',
	NULL);
INSERT INTO REG_SCHEDULE_COL_FORMAT (col_format_id, description, format_mask) VALUES (
	4,
	'Text',
	NULL);
INSERT INTO REG_SCHEDULE_COL_FORMAT (col_format_id, description, format_mask) VALUES (
	5,
	'Date',
	NULL);

INSERT INTO REG_SCHEDULE_COLUMN_TAG (column_tag_id, description) VALUES (
	1,
	'Current Year Actuals');
INSERT INTO REG_SCHEDULE_COLUMN_TAG (column_tag_id, description) VALUES (
	2,
	'Current Year Forecast');
INSERT INTO REG_SCHEDULE_COLUMN_TAG (column_tag_id, description) VALUES (
	3,
	'Prior Year 1 Actuals');
INSERT INTO REG_SCHEDULE_COLUMN_TAG (column_tag_id, description) VALUES (
	4,
	'Prior Year 2 Actuals');
INSERT INTO REG_SCHEDULE_COLUMN_TAG (column_tag_id, description) VALUES (
	5,
	'Test Period 1 Forecast');
INSERT INTO REG_SCHEDULE_COLUMN_TAG (column_tag_id, description) VALUES (
	6,
	'Test Period 2 Forecast');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2958, 0, 2015, 2, 1, 0, 45173, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045173_reg_schedule2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;