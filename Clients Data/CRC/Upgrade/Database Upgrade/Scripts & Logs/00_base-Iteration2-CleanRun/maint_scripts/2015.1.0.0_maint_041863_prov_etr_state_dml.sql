/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041863_prov_etr_state_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 12/26/2014 Blake Andrews    Create new ETR State and State grid report
||============================================================================
*/

--
--SECTION 1:  Re-write of the 54512 report
--

--Make sure this is re-runnable in case we have to local patch a client with it
delete from pp_reports where datawindow = 'dw_tax_accrual_etr_footnote_state';
delete from tax_accrual_rep_special_note where description = 'ETR_FOOTNOTE_STATE';
delete from tax_accrual_report_option_dtl where report_option_id = 37;
delete from tax_accrual_report_option where report_option_id = 37;
delete from tax_accrual_rep_rollup_dtl where rep_rollup_group_id = 14;
delete from tax_accrual_rep_rollup_group where rep_rollup_group_id = 14;

update pp_reports
set subsystem = 'Tax Accruala'
where report_id = 54512
	or report_number = 'Tax Accrual - 54512';

update pp_reports
set report_id =	(	select max(report_id)
							from pp_reports
						) + 1
where report_id = 54512;

insert into pp_reports
(	report_id, 
	description,
	long_description,
	subsystem,
	datawindow,
	special_note,
	report_number
)
values
(	54512,
	'ETR Footnote - State',
	'Provides supporting detail for the State Tax Expense line in the ETR Footnote report.',
	'Tax Accrual',
	'dw_tax_accrual_etr_footnote_state',
	'ETR_FOOTNOTE_STATE',
	'Tax Accrual - 54512'
);

insert into tax_accrual_report_option
(	report_option_id,
	description
)
values
(	37,
	'ETR FOOTNOTE - STATE'
);

insert into tax_accrual_rep_rollup_group
(	rep_rollup_group_id,
	description
)
values
(	14,
	'M Roll,M Partial,Entity,Oper Ind'
);

insert into tax_accrual_rep_rollup_dtl
(	rep_rollup_group_id,
	rep_rollup_id
)
values
(	14,
	1 --M Rollup
);

insert into tax_accrual_rep_rollup_dtl
(	rep_rollup_group_id,
	rep_rollup_id
)
values
(	14,
	6 --Oper Ind
);

insert into tax_accrual_rep_rollup_dtl
(	rep_rollup_group_id,
	rep_rollup_id
)
values
(	14,
	7 -- Entity
);

insert into tax_accrual_rep_rollup_dtl
(	rep_rollup_group_id,
	rep_rollup_id
)
values
(	14,
	8 -- M Roll Partial
);

insert into tax_accrual_rep_special_note
(	special_note_id,
	description,
	report_option_id,
	month_required,
	compare_case,
	m_rollup_required,
	calc_rates,
	report_type,
	dw_parameter_list,
	je_temp_table_ind,
	ns2fas109_temp_table_ind,
	rep_rollup_group_id,
	default_acct_rollup,
	rep_cons_category_id
)
values
(	79,
	'ETR_FOOTNOTE_STATE',
	37,
	1,
	0,
	0,
	0,
	0,
	5,
	0,
	0,
	14,
	null,
	0
);

insert into tax_accrual_report_option_dtl
(	report_option_id,
	report_object_id
)
select	37,
			report_object_id
from tax_accrual_report_object
where report_object_id in	(	3, --Tabpage GL Account
										52, --Tabpage Advanced
										4, --Case Compare Tab
										59, --Consolidating Type
										33, 12, --M Rollup Group
										15, 51, --M Item
										34, --CB Select Rollup
										38 --CB Filter
									);

--
--SECTION 2:  Creation of the state ETR grid report
--
delete from pp_reports where datawindow = 'dw_tax_accrual_etr_state_grid';
delete from tax_accrual_rep_special_note where description = 'ETR_STATE_GRID';
delete from tax_accrual_rep_cons_cols where rep_cons_type_id between 46 and 50;
delete from tax_accrual_rep_cons_rows where rep_cons_type_id between 46 and 50;
delete from tax_accrual_rep_cons_type where rep_cons_type_id between 46 and 50;


insert into pp_reports
(	report_id, 
	description,
	long_description,
	subsystem,
	datawindow,
	special_note,
	report_number
)
values
(	54514,
	'Income Tax State ETR - Grid',
	'Support for the ETR Footnote Grid, showing the detail behind the state tax expense line.',
	'Tax Accrual',
	'dw_tax_accrual_etr_state_grid',
	'ETR_STATE_GRID',
	'Tax Accrual - 54514'
);

insert into tax_accrual_rep_special_note
(	special_note_id,
	description,
	report_option_id,
	month_required,
	compare_case,
	m_rollup_required,
	calc_rates,
	report_type,
	dw_parameter_list,
	je_temp_table_ind,
	ns2fas109_temp_table_ind,
	rep_rollup_group_id,
	default_acct_rollup,
	rep_cons_category_id
)
values
(	80,
	'ETR_STATE_GRID',
	31,
	1,
	0,
	0,
	0,
	0,
	5,
	0,
	0,
	13,
	null,
	9
);

insert into tax_accrual_rep_cons_type
(	rep_cons_type_id,
	description,
	datawindow,
	sort_spec,
	rep_cons_category_id,
	balances_ind
)
values
(	46,
	'ETR State by Company',
	'dw_tax_accrual_etr_state_grid',
	'report_section, rollup_description, detail_description',
	9,
	0
);

insert into tax_accrual_rep_cons_type
(	rep_cons_type_id,
	description,
	datawindow,
	sort_spec,
	rep_cons_category_id,
	balances_ind
)
values
(	47,
	'ETR State by Oper',
	'dw_tax_accrual_etr_state_grid',
	'report_section, rollup_description, detail_description',
	9,
	0
);

insert into tax_accrual_rep_cons_type
(	rep_cons_type_id,
	description,
	datawindow,
	sort_spec,
	rep_cons_category_id,
	balances_ind
)
values
(	48,
	'ETR State by State',
	'dw_tax_accrual_etr_state_grid',
	'report_section, rollup_description, detail_description',
	9,
	0
);

insert into tax_accrual_rep_cons_type
(	rep_cons_type_id,
	description,
	datawindow,
	sort_spec,
	rep_cons_category_id,
	balances_ind
)
values
(	49,
	'ETR State by Month',
	'dw_tax_accrual_etr_state_grid',
	'report_section, rollup_description, detail_description',
	9,
	0
);

insert into tax_accrual_rep_cons_type
(	rep_cons_type_id,
	description,
	datawindow,
	sort_spec,
	rep_cons_category_id,
	balances_ind
)
values
(	50,
	'ETR State by Month Type',
	'dw_tax_accrual_etr_state_grid',
	'report_section, rollup_description, detail_description',
	9,
	0
);

--*********************************************
--Add the Rows for the "By Company" grid
--*********************************************
insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	46,
	'parent_id',
	1,
	'number',
	'consol_desc',
	0
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	46,
	'detail_description',
	1,
	'string',
	'detail_description',
	1
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	46,
	'rollup_description',
	1,
	'string',
	'rollup_description',
	2
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	46,
	'report_section',
	1,
	'number',
	'report_section',
	3
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	1, --row_id
	'H', --row_type
	-1, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	1, --is_report_title
	'Tax State ETR Report - By Company', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	2, --row_id
	'H', --row_type
	-1, --group_id
	2, --rank
	1, --color_id
	'USER_TITLE', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	3, --row_id
	'H', --row_type
	-1, --group_id
	3, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'case_description', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	4, --row_id
	'H', --row_type
	-1, --group_id
	4, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'oper_ind_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	5, --row_id
	'H', --row_type
	-1, --group_id
	5, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'entity_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	6, --row_id
	'H', --row_type
	-1, --group_id
	6, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'month_descr', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	7, --row_id
	'H', --row_type
	-1, --group_id
	7, --rank
	1, --color_id
	'FILLER', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	8, --row_id
	'H', --row_type
	0, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'ptbi', --row_value
	'TEXT', --label_value_type
	'Book Income', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	9, --row_id
	'H', --row_type
	0, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'tax_items	', --row_value
	'TEXT', --label_value_type
	'Tax Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	10, --row_id
	'H', --row_type
	0, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_h1_bibit', --row_value
	'TEXT', --label_value_type
	'Book Income Before Income Taxes (Adjusted for Tax Items)', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	11, --row_id
	'H', --row_type
	0, --group_id
	4, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	12, --row_id
	'D', --row_type
	1, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tt', --row_value
	'COL_VALUE_TEXT', --label_value_type
	'detail_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	13, --row_id
	'D', --row_type
	1, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'deduct_state', --row_value
	'TEXT', --label_value_type
	'Deduction Due to Federal and Other State Tax', --label_value
	'T', --col_format
	'state_deduct_display', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	14, --row_id
	'F', --row_type
	2, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	15, --row_id
	'F', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	16, --row_id
	'H', --row_type
	2, --group_id
	1, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'if(report_section=10,0,display_detail_row)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	17, --row_id
	'H', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'TEXT', --label_value_type
	' ', --label_value
	'T', --col_format
	'if(report_section=10,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	18, --row_id
	'F', --row_type
	3, --group_id
	1, --rank
	1, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_b4_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Tax Expense (Benefit) Before Discrete Items', --label_value
	'T', --col_format
	'if(report_section=40,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	19, --row_id
	'F', --row_type
	99, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'curtax_not_expense_state', --row_value
	'TEXT', --label_value_type
	'Current Tax Not Booked to Expense', --label_value
	'T', --col_format
	'if(report_section=40,show_curtax_not_expense_cons,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	20, --row_id
	'F', --row_type
	99, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_after_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Total Tax Expense (Benefit) With Discrete Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	21, --row_id
	'F', --row_type
	99, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_tax_expense_state', --row_value
	'TEXT', --label_value_type
	'Tax Expense Booked', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	22, --row_id
	'F', --row_type
	99, --group_id
	4, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_tt_difference', --row_value
	'TEXT', --label_value_type
	'Difference', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	23, --row_id
	'F', --row_type
	99, --group_id
	5, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_co_etr', --row_value
	'TEXT', --label_value_type
	'Standalone ETR Percentage', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	46, --rep_cons_type_id
	24, --row_id
	'F', --row_type
	99, --group_id
	6, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'all_co_etr_total', --row_value
	'TEXT', --label_value_type
	'ETR Impact on Consolidated Group', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

--*********************************************
--Add the Rows for the "By Oper" grid
--*********************************************
insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	47,
	'oper_ind',
	1,
	'number',
	'oper_ind_label',
	0
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	47,
	'detail_description',
	1,
	'string',
	'detail_description',
	1
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	47,
	'rollup_description',
	1,
	'string',
	'rollup_description',
	2
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	47,
	'report_section',
	1,
	'number',
	'report_section',
	3
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	1, --row_id
	'H', --row_type
	-1, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	1, --is_report_title
	'Tax State ETR Report - By Oper Ind', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	2, --row_id
	'H', --row_type
	-1, --group_id
	2, --rank
	1, --color_id
	'USER_TITLE', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	3, --row_id
	'H', --row_type
	-1, --group_id
	3, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'case_description', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	4, --row_id
	'H', --row_type
	-1, --group_id
	4, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'consol_desc', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	5, --row_id
	'H', --row_type
	-1, --group_id
	5, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'entity_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	6, --row_id
	'H', --row_type
	-1, --group_id
	6, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'month_descr', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	7, --row_id
	'H', --row_type
	-1, --group_id
	7, --rank
	1, --color_id
	'FILLER', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	8, --row_id
	'H', --row_type
	0, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'ptbi', --row_value
	'TEXT', --label_value_type
	'Book Income', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	9, --row_id
	'H', --row_type
	0, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'tax_items	', --row_value
	'TEXT', --label_value_type
	'Tax Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	10, --row_id
	'H', --row_type
	0, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_h1_bibit', --row_value
	'TEXT', --label_value_type
	'Book Income Before Income Taxes (Adjusted for Tax Items)', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	11, --row_id
	'H', --row_type
	0, --group_id
	4, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	12, --row_id
	'D', --row_type
	1, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tt', --row_value
	'COL_VALUE_TEXT', --label_value_type
	'detail_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	13, --row_id
	'D', --row_type
	1, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'deduct_state', --row_value
	'TEXT', --label_value_type
	'Deduction Due to Federal and Other State Tax', --label_value
	'T', --col_format
	'state_deduct_display', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	14, --row_id
	'F', --row_type
	2, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	15, --row_id
	'F', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	16, --row_id
	'H', --row_type
	2, --group_id
	1, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'if(report_section=10,0,display_detail_row)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	17, --row_id
	'H', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'TEXT', --label_value_type
	' ', --label_value
	'T', --col_format
	'if(report_section=10,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	18, --row_id
	'F', --row_type
	3, --group_id
	1, --rank
	1, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_b4_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Tax Expense (Benefit) Before Discrete Items', --label_value
	'T', --col_format
	'if(report_section=40,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	19, --row_id
	'F', --row_type
	99, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'curtax_not_expense_state', --row_value
	'TEXT', --label_value_type
	'Current Tax Not Booked to Expense', --label_value
	'T', --col_format
	'if(report_section=40,show_curtax_not_expense_cons,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	20, --row_id
	'F', --row_type
	99, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_after_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Total Tax Expense (Benefit) With Discrete Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	21, --row_id
	'F', --row_type
	99, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_tax_expense_state', --row_value
	'TEXT', --label_value_type
	'Tax Expense Booked', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	22, --row_id
	'F', --row_type
	99, --group_id
	4, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_tt_difference', --row_value
	'TEXT', --label_value_type
	'Difference', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	23, --row_id
	'F', --row_type
	99, --group_id
	5, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_co_etr', --row_value
	'TEXT', --label_value_type
	'Standalone ETR Percentage', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	47, --rep_cons_type_id
	24, --row_id
	'F', --row_type
	99, --group_id
	6, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'all_co_etr_total', --row_value
	'TEXT', --label_value_type
	'ETR Impact Across all Oper Inds', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

--*********************************************
--Add the Rows for the "By Entity" grid
--*********************************************
insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	48,
	'entity_id',
	1,
	'number',
	'entity_label',
	0
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	48,
	'detail_description',
	1,
	'string',
	'detail_description',
	1
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	48,
	'rollup_description',
	1,
	'string',
	'rollup_description',
	2
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	48,
	'report_section',
	1,
	'number',
	'report_section',
	3
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	1, --row_id
	'H', --row_type
	-1, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	1, --is_report_title
	'Tax State ETR Report - By Entity', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	2, --row_id
	'H', --row_type
	-1, --group_id
	2, --rank
	1, --color_id
	'USER_TITLE', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	3, --row_id
	'H', --row_type
	-1, --group_id
	3, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'case_description', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	4, --row_id
	'H', --row_type
	-1, --group_id
	4, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'consol_desc', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	5, --row_id
	'H', --row_type
	-1, --group_id
	5, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'oper_ind_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	6, --row_id
	'H', --row_type
	-1, --group_id
	6, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'month_descr', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	7, --row_id
	'H', --row_type
	-1, --group_id
	7, --rank
	1, --color_id
	'FILLER', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	8, --row_id
	'H', --row_type
	0, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'ptbi', --row_value
	'TEXT', --label_value_type
	'Book Income', --label_value
	'T', --col_format
	null, --display_ind_field
	'ptbi' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	9, --row_id
	'H', --row_type
	0, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'tax_items	', --row_value
	'TEXT', --label_value_type
	'Tax Items', --label_value
	'T', --col_format
	null, --display_ind_field
	'tax_items' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	10, --row_id
	'H', --row_type
	0, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_h1_bibit', --row_value
	'TEXT', --label_value_type
	'Book Income Before Income Taxes (Adjusted for Tax Items)', --label_value
	'T', --col_format
	null, --display_ind_field
	'comp_h1_bibit' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	11, --row_id
	'H', --row_type
	0, --group_id
	4, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	12, --row_id
	'D', --row_type
	1, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tt', --row_value
	'COL_VALUE_TEXT', --label_value_type
	'detail_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	13, --row_id
	'D', --row_type
	1, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'deduct_state', --row_value
	'TEXT', --label_value_type
	'Deduction Due to Federal and Other State Tax', --label_value
	'T', --col_format
	'state_deduct_display', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	14, --row_id
	'F', --row_type
	2, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	15, --row_id
	'F', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	16, --row_id
	'H', --row_type
	2, --group_id
	1, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'if(report_section=10,0,display_detail_row)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	17, --row_id
	'H', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'TEXT', --label_value_type
	' ', --label_value
	'T', --col_format
	'if(report_section=10,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	18, --row_id
	'F', --row_type
	3, --group_id
	1, --rank
	1, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_b4_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Tax Expense (Benefit) Before Discrete Items', --label_value
	'T', --col_format
	'if(report_section=40,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	19, --row_id
	'F', --row_type
	99, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'curtax_not_expense_state', --row_value
	'TEXT', --label_value_type
	'Current Tax Not Booked to Expense', --label_value
	'T', --col_format
	'if(report_section=40,show_curtax_not_expense_cons,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	20, --row_id
	'F', --row_type
	99, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_after_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Total Tax Expense (Benefit) With Discrete Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	21, --row_id
	'F', --row_type
	99, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_tax_expense_state', --row_value
	'TEXT', --label_value_type
	'Tax Expense Booked', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	22, --row_id
	'F', --row_type
	99, --group_id
	4, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_tt_difference', --row_value
	'TEXT', --label_value_type
	'Difference', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	48, --rep_cons_type_id
	23, --row_id
	'F', --row_type
	99, --group_id
	5, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'all_co_etr_total', --row_value
	'TEXT', --label_value_type
	'ETR Impact Across all Entities', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

--*********************************************
--Add the Rows for the "By Month" grid
--*********************************************
insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	49,
	'gl_month',
	1,
	'number',
	'month_descr',
	0
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	49,
	'detail_description',
	1,
	'string',
	'detail_description',
	1
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	49,
	'rollup_description',
	1,
	'string',
	'rollup_description',
	2
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	49,
	'report_section',
	1,
	'number',
	'report_section',
	3
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	1, --row_id
	'H', --row_type
	-1, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	1, --is_report_title
	'Tax State ETR Report - By Month', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	2, --row_id
	'H', --row_type
	-1, --group_id
	2, --rank
	1, --color_id
	'USER_TITLE', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	3, --row_id
	'H', --row_type
	-1, --group_id
	3, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'case_description', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	4, --row_id
	'H', --row_type
	-1, --group_id
	4, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'consol_desc', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	5, --row_id
	'H', --row_type
	-1, --group_id
	5, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'oper_ind_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	6, --row_id
	'H', --row_type
	-1, --group_id
	6, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'entity_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	7, --row_id
	'H', --row_type
	-1, --group_id
	7, --rank
	1, --color_id
	'FILLER', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	8, --row_id
	'H', --row_type
	0, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'ptbi', --row_value
	'TEXT', --label_value_type
	'Book Income', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	9, --row_id
	'H', --row_type
	0, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'tax_items	', --row_value
	'TEXT', --label_value_type
	'Tax Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	10, --row_id
	'H', --row_type
	0, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_h1_bibit', --row_value
	'TEXT', --label_value_type
	'Book Income Before Income Taxes (Adjusted for Tax Items)', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	11, --row_id
	'H', --row_type
	0, --group_id
	4, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	12, --row_id
	'D', --row_type
	1, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tt', --row_value
	'COL_VALUE_TEXT', --label_value_type
	'detail_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	13, --row_id
	'D', --row_type
	1, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'deduct_state', --row_value
	'TEXT', --label_value_type
	'Deduction Due to Federal and Other State Tax', --label_value
	'T', --col_format
	'state_deduct_display', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	14, --row_id
	'F', --row_type
	2, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	15, --row_id
	'F', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	16, --row_id
	'H', --row_type
	2, --group_id
	1, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'if(report_section=10,0,display_detail_row)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	17, --row_id
	'H', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'TEXT', --label_value_type
	' ', --label_value
	'T', --col_format
	'if(report_section=10,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	18, --row_id
	'F', --row_type
	3, --group_id
	1, --rank
	1, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_b4_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Tax Expense (Benefit) Before Discrete Items', --label_value
	'T', --col_format
	'if(report_section=40,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	19, --row_id
	'F', --row_type
	99, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'curtax_not_expense_state', --row_value
	'TEXT', --label_value_type
	'Current Tax Not Booked to Expense', --label_value
	'T', --col_format
	'if(report_section=40,show_curtax_not_expense_cons,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	20, --row_id
	'F', --row_type
	99, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_after_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Total Tax Expense (Benefit) With Discrete Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	21, --row_id
	'F', --row_type
	99, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_tax_expense_state', --row_value
	'TEXT', --label_value_type
	'Tax Expense Booked', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	22, --row_id
	'F', --row_type
	99, --group_id
	4, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_tt_difference', --row_value
	'TEXT', --label_value_type
	'Difference', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	23, --row_id
	'F', --row_type
	99, --group_id
	5, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_co_etr', --row_value
	'TEXT', --label_value_type
	'ETR by Month', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	49, --rep_cons_type_id
	24, --row_id
	'F', --row_type
	99, --group_id
	6, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'all_co_etr_total', --row_value
	'TEXT', --label_value_type
	'Impact on Total ETR for the Period', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

--*********************************************
--Add the Rows for the "By Month Type" grid
--*********************************************
insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	50,
	'gl_month',
	1,
	'number',
	'month_descr',
	0
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	50,
	'detail_description',
	1,
	'string',
	'detail_description',
	1
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	50,
	'rollup_description',
	1,
	'string',
	'rollup_description',
	2
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	50,
	'report_section',
	1,
	'number',
	'report_section',
	3
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	1, --row_id
	'H', --row_type
	-1, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	1, --is_report_title
	'Tax State ETR Report - By Month Type', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	2, --row_id
	'H', --row_type
	-1, --group_id
	2, --rank
	1, --color_id
	'USER_TITLE', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	3, --row_id
	'H', --row_type
	-1, --group_id
	3, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'case_description', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	4, --row_id
	'H', --row_type
	-1, --group_id
	4, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'consol_desc', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	5, --row_id
	'H', --row_type
	-1, --group_id
	5, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'oper_ind_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	6, --row_id
	'H', --row_type
	-1, --group_id
	6, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'entity_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	7, --row_id
	'H', --row_type
	-1, --group_id
	7, --rank
	1, --color_id
	'FILLER', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	8, --row_id
	'H', --row_type
	0, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'ptbi', --row_value
	'TEXT', --label_value_type
	'Book Income', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	9, --row_id
	'H', --row_type
	0, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'tax_items	', --row_value
	'TEXT', --label_value_type
	'Tax Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	10, --row_id
	'H', --row_type
	0, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_h1_bibit', --row_value
	'TEXT', --label_value_type
	'Book Income Before Income Taxes (Adjusted for Tax Items)', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	11, --row_id
	'H', --row_type
	0, --group_id
	4, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	12, --row_id
	'D', --row_type
	1, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tt', --row_value
	'COL_VALUE_TEXT', --label_value_type
	'detail_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	13, --row_id
	'D', --row_type
	1, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'deduct_state', --row_value
	'TEXT', --label_value_type
	'Deduction Due to Federal and Other State Tax', --label_value
	'T', --col_format
	'state_deduct_display', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	14, --row_id
	'F', --row_type
	2, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	15, --row_id
	'F', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	16, --row_id
	'H', --row_type
	2, --group_id
	1, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'if(report_section=10,0,display_detail_row)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	17, --row_id
	'H', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'TEXT', --label_value_type
	' ', --label_value
	'T', --col_format
	'if(report_section=10,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	18, --row_id
	'F', --row_type
	3, --group_id
	1, --rank
	1, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_b4_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Tax Expense (Benefit) Before Discrete Items', --label_value
	'T', --col_format
	'if(report_section=40,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	19, --row_id
	'F', --row_type
	99, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'curtax_not_expense_state', --row_value
	'TEXT', --label_value_type
	'Current Tax Not Booked to Expense', --label_value
	'T', --col_format
	'if(report_section=40,show_curtax_not_expense_cons,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	20, --row_id
	'F', --row_type
	99, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_after_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Total Tax Expense (Benefit) With Discrete Items', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	21, --row_id
	'F', --row_type
	99, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_tax_expense_state', --row_value
	'TEXT', --label_value_type
	'Tax Expense Booked', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	22, --row_id
	'F', --row_type
	99, --group_id
	4, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_tt_difference', --row_value
	'TEXT', --label_value_type
	'Difference', --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	23, --row_id
	'F', --row_type
	99, --group_id
	5, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_co_etr', --row_value
	'TEXT', --label_value_type
	'ETR by Month Type', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	50, --rep_cons_type_id
	24, --row_id
	'F', --row_type
	99, --group_id
	6, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'all_co_etr_total', --row_value
	'TEXT', --label_value_type
	'Impact on Total ETR for the Period', --label_value
	'P', --col_format
	null, --display_ind_field
	'all_co_etr_total_total' --total_col_fieldname
);

commit;


--This is a clean-up item that should have been done previously.  This will make the "Advanced" tab appear for the 54513
delete from tax_accrual_report_option_dtl
where report_option_id = 33
	and report_object_id = 52;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2147, 0, 2015, 1, 0, 0, 41863, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041863_prov_etr_state_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;