/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050304_lessor_01_drop_col_book_summary_id_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------  ------------------------------------
|| 2017.3.0.0 04/16/2018 Anand R        drop column book_summary_id from lsr_cap_type
||============================================================================
*/

alter table lsr_cap_type
drop column book_summary_id;

INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4509, 0, 2017, 3, 0, 0, 50304, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050304_lessor_01_drop_col_book_summary_id_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;