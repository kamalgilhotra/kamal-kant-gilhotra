/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049278_lessor_02_require_currency_type_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/31/2017 Josh Sandler     Make currency type required
||============================================================================
*/

UPDATE pp_any_query_criteria_fields
SET required_filter = 1,
required_one_mult = 2
WHERE detail_field = 'currency_type'
AND id IN (SELECT id FROM pp_any_query_criteria WHERE description IN ('Non Disclosure: Future Estimated Receipts', 'Non Disclosure: Future Estimated Executory Receipts', 'Disclosure: Undiscounted Cash Flows (Current Revision)'))
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3854, 0, 2017, 1, 0, 0, 49278, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049278_lessor_02_require_currency_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
