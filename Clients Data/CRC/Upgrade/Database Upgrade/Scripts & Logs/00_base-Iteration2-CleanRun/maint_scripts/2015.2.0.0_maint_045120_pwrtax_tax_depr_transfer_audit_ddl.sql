/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045120_pwrtax_tax_depr_transfer_audit_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/29/2015 Sarah Byers    Add audit table for verifying partial depr calc
||============================================================================
*/

create table tax_depr_transfer_audit (
tax_depr_index number(22,0) not null,
tax_record_id number(22,0) not null,
tax_year number(22,2) not null,
tax_book_id number(22,0) not null,
transfer_direction varchar2(4),
short_months number(22,0),
transfer_month number(22,0),
add_ratio number(22,0),
salvage_conv number(22,0),
tax_rates_rows number(22,0),
net_gross varchar2(35),
rlife number(22,0),
start_method number(22,0),
vintage number(22,2),
yr1_fraction number(22,8),
yr2_fraction number(22,8),
xfer_fraction number(22,8),
depr_rate number(22,8),
i number(22,0),
tax_balance_transfer number(22,2),
additions_transfer number(22,2),
depreciable_base number(22,2),
reserve number(22,2),
calc_depr_transfer number(22,2),
user_id varchar2(18),
time_stamp date,
calc_xfer_fraction number(22,8));

alter table tax_depr_transfer_audit add (
constraint pk_tax_depr_transfer_audit primary key (tax_depr_index, tax_record_id, tax_year, tax_book_id) using index tablespace pwrplant_idx);

comment on table tax_depr_transfer_audit is '(O)  [09] {C} The Tax Depreciation Transfer Audit table contains data used to verify the partial year tax depreciation calculation used for transfers.';
comment on column tax_depr_transfer_audit.tax_depr_index is 'System assigned identifier audit record.';
comment on column tax_depr_transfer_audit.tax_record_id is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables. Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).  Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';
comment on column tax_depr_transfer_audit.tax_year is 'Calendar tax year associated with the tax records depreciation calculation and stored balances.  The fraction is a sequential number for dealing with short tax years.';
comment on column tax_depr_transfer_audit.tax_book_id is 'System-assigned identifier for each set of tax books  a utility maintains.';
comment on column tax_depr_transfer_audit.transfer_direction is 'The direction of the transfer; either FROM or TO.';
comment on column tax_depr_transfer_audit.short_months is 'The number of months included in the tax year.';
comment on column tax_depr_transfer_audit.transfer_month is 'The month and year of the transfer that is used for calculating partial year depreciation.';
comment on column tax_depr_transfer_audit.add_ratio is 'The ratio applied to additions during the depreciation calculation';
comment on column tax_depr_transfer_audit.salvage_conv is 'System-assigned identifier to the treatment of salvage proceeds (Salvage Convention table).';
comment on column tax_depr_transfer_audit.tax_rates_rows is 'The number of tax rates records.';
comment on column tax_depr_transfer_audit.net_gross is 'Determines if the rate is applied to a Gross (unadjusted balance) or Net (adjusted for the reserve) balance.';
comment on column tax_depr_transfer_audit.rlife is 'A Yes/No indicator describing whether a remaining life calculation is desired.  The PowerTax logic will automatically calculate the remaining life rate  and maintain the remaining life itself on the tax depreciation table';
comment on column tax_depr_transfer_audit.start_method is 'System-assigned identifier of a depreciation method.';
comment on column tax_depr_transfer_audit.yr1_fraction is 'The ratio used to determine the year 1 rate based on the number of months in the tax year.';
comment on column tax_depr_transfer_audit.yr2_fraction is 'The ratio used to determine the year 2 rate based on the number of months in the tax year.';
comment on column tax_depr_transfer_audit.xfer_fraction is 'The fraction calculated to ratio the depreciation between the FROM and TO sides based on the transfer month.';
comment on column tax_depr_transfer_audit.depr_rate is 'The depreciation rate for the year (after the first year convention has been applied) expressed as a decimal.  In the case of Vintage year rate, this is the whole year rate.';
comment on column tax_depr_transfer_audit.i is 'System assigned identifier audit record.';
comment on column tax_depr_transfer_audit.tax_balance_transfer is 'Tax basis amount of this tax asset record in dollars, not reduced for ordinary (ADR) retirements being transferred.';
comment on column tax_depr_transfer_audit.additions_transfer is 'Current year tax basis additions in dollars starting with the transfer month specified for partial depreciation transfers.';
comment on column tax_depr_transfer_audit.depreciable_base is 'The calculated depreciable base to which the tax depreciation rates are  applied in order to calculate tax depreciation.';
comment on column tax_depr_transfer_audit.reserve is 'Accumulated tax depreciation reserve amount of this tax asset transfer.';
comment on column tax_depr_transfer_audit.calc_depr_transfer is 'System assigned identifier audit record.';
comment on column tax_depr_transfer_audit.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_depr_transfer_audit.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_depr_transfer_audit.calc_xfer_fraction is 'The fraction calculated to ratio the depreciation between the FROM and TO sides based on the transfer month.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2945, 0, 2015, 2, 0, 0, 45120, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045120_pwrtax_tax_depr_transfer_audit_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
