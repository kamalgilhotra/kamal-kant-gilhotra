/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031484_pwrtax_PKG_TAX_INT_XFER.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 10.4.2.0 10/18/2013 Andrew Scott        Speed up powertax transfers interface
||============================================================================
*/

create or replace package PKG_TAX_INT_XFER as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_TAX_INT_XFER
   || Description: Moved/Enhanced PowerTax Transfers Interface logic to PLSQL
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.2.0 10/18/2013 Andrew Scott   Original Version
   ||============================================================================
   */

   type CO_IDS is table of number(22, 0) index by binary_integer;

   -- function the return the process id for transfers interface
   function F_GETPROCESS return number;

   -- function to process a batch of data (each iteration could run many batches of transfer froms)
   function F_RUN_BATCH(A_TAX_YEAR   number,
                        A_START_MO   number,
                        A_END_MO     number,
                        A_VERSION_ID number,
                        A_ITERATION  number,
                        A_BATCH      number) return varchar2;

   -- after the looping, go through some final calculations to:
   -- 1.  allocate the "proceeds from" per trans group however the amount was allocated.
   -- 2.  find and update the transfer "tos" for the processed transfer "froms"
   function F_FINAL_CALCS(A_TAX_YEAR   number,
                          A_VERSION_ID number) return varchar2;

   -- function to start the log, process the transfers, end the log
   function F_POWERTAX_XFERS(A_TAX_YEAR   number,
                             A_START_MO   number,
                             A_END_MO     number,
                             A_VERSION_ID number) return varchar2;

end PKG_TAX_INT_XFER;
/


create or replace package body PKG_TAX_INT_XFER as

   --****************************************************************************************************
   --               VARIABLES
   --****************************************************************************************************
   L_PROCESS_ID number;
   L_DEBUG_CODE number;

   --****************************************************************************************************
   --               Start Body
   --****************************************************************************************************

   --****************************************************************************************************
   --               PROCEDURES
   --****************************************************************************************************

   --procedure grabs the transfers interface process id for the online logs
   procedure P_SETPROCESS is
   begin
      select PROCESS_ID
        into L_PROCESS_ID
        from PP_PROCESSES
       where UPPER(DESCRIPTION) = 'POWERTAX-TRANSFERS';
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         L_PROCESS_ID := -1;
   end P_SETPROCESS;

   --****************************************************************************************************
   --                FUNCTIONS
   --****************************************************************************************************

   -- function the return the process id for transfers interface
   function F_GETPROCESS return number is
   begin
      return L_PROCESS_ID;
   end F_GETPROCESS;

   -- function to process a batch of data (each iteration could run many batches of transfer froms)
   function F_RUN_BATCH(A_TAX_YEAR   number,
                        A_START_MO   number,
                        A_END_MO     number,
                        A_VERSION_ID number,
                        A_ITERATION  number,
                        A_BATCH      number) return varchar2 is
      L_MSG        varchar2(2000);
      NUM_ROWS     number;
      ANALYZE_CODE number(22, 0);
      type TTC_REC_TYPE is record(
         FROM_TRID   TAX_TRANSFER_CONTROL.FROM_TRID%type,
         TAX_YEAR    TAX_TRANSFER_CONTROL.TAX_YEAR%type,
         COMPANY_ID  TAX_TRANSFER_CONTROL.COMPANY_ID%type,
         BOOK_AMOUNT TAX_TRANSFER_CONTROL.BOOK_AMOUNT%type);
      type TTC_TBL_TYPE is table of TTC_REC_TYPE;
      TTC_TBL TTC_TBL_TYPE;

   begin

      L_MSG := 'Regathering stats for tax_book_transfers_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_book_transfers_grp');

      L_MSG := 'Regathering stats for tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_transfer_control');

      --if a_iteration = 2 then
      -- L_DEBUG_CODE := 1;
      --end if;
      --if L_DEBUG_CODE = 1 then
      --    commit;
      --         L_MSG := 'Debug flag triggered...';
      --         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      --         return 'ZERO ROWS';
      --end if;

      L_MSG := 'Inserting into tax_trans_audit_trail_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      insert into TAX_TRANS_AUDIT_TRAIL_GRP
         (TRANS_GRP_ID, TAX_YEAR, FROM_TRID, TAX_ACTIVITY_CODE_ID, IN_SERVICE_YEAR, START_MONTH,
          END_MONTH, TBT_GROUP_ID, START_EFF_BOOK_VINTAGE, END_EFF_BOOK_VINTAGE, BOOK_BALANCE,
          HOLD_SUM_BOOK_BALANCE, ALLOC_AMOUNT, TRANS_AMOUNT, TRANS_SALES_AMOUNT, VINTAGE_YEAR,
          TRANSFER_STATUS, FROM_TRID_VINTAGE_YEAR, ALLOC_SALES_AMOUNT, CREATE_TO_TRID, BATCH,
          ITERATION, TO_TRID, COMPANY_ID_TO, TBT_GROUP_ID_TO)
         select TRANS_GRP_ID,
                TAX_YEAR,
                TAX_RECORD_ID          FROM_TRID,
                TAX_ACTIVITY_CODE_ID,
                IN_SERVICE_YEAR,
                START_MONTH,
                END_MONTH,
                TBT_GROUP_ID,
                START_EFF_BOOK_VINTAGE,
                END_EFF_BOOK_VINTAGE,
                BOOK_BALANCE,
                BALANCE,
                NEW_XFER               ALLOC_AMOUNT,
                TRANS_AMOUNT,
                TRANS_SALES_AMOUNT,
                BOOK_VINTAGE_YEAR,
                TRANSFER_STATUS,
                VINTAGE_YEAR,
                0                      ALLOC_SALES_AMOUNT,
                1                      CREATE_TO_TRID,
                BATCH,
                ITERATION,
                -1                     TO_TRID,
                COMPANY_ID_TO,
                TBT_GROUP_ID_TO
           from (select COMPANY_ID,
                        TAX_ACTIVITY_CODE_ID,
                        IN_SERVICE_YEAR,
                        TAX_YEAR,
                        START_MONTH,
                        END_MONTH,
                        TRANS_GRP_ID,
                        START_EFF_BOOK_VINTAGE,
                        END_EFF_BOOK_VINTAGE,
                        BOOK_BALANCE,
                        BALANCE,
                        DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               NEW_XFER,
                               DECODE(SIGN(BALANCE - NEW_XFER), -1, BALANCE, NEW_XFER)) NEW_XFER,
                        TRANS_AMOUNT,
                        TRANS_SALES_AMOUNT,
                        BOOK_VINTAGE_YEAR,
                        TAX_RECORD_ID,
                        DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               ITERATION,
                               DECODE(SIGN(BALANCE - NEW_XFER), -1, -98, ITERATION)) TRANSFER_STATUS,
                        VINTAGE_YEAR,
                        (A_BATCH) BATCH,
                        ITERATION,
                        TBT_GROUP_ID,
                        ALLOC_SUBTOTAL,
                        FIFO,
                        COMPANY_ID_TO,
                        TBT_GROUP_ID_TO,
                        max(TRID_RANK) OVER(partition by TRANS_GRP_ID) TRANS_RANK
                   from (select distinct TBT.COMPANY_ID_FROM COMPANY_ID,
                                         TBT.TAX_ACTIVITY_CODE_ID_FROM TAX_ACTIVITY_CODE_ID,
                                         TBT.IN_SERVICE_YEAR_FROM IN_SERVICE_YEAR,
                                         TBT.TAX_YEAR TAX_YEAR,
                                         (A_START_MO) START_MONTH,
                                         (A_END_MO) END_MONTH,
                                         TBT.TRANS_GRP_ID TRANS_GRP_ID,
                                         TBX.START_EFF_BOOK_VINTAGE,
                                         TBX.END_EFF_BOOK_VINTAGE,
                                         TD.BOOK_BALANCE,
                                         (NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0)) BALANCE,
                                         DECODE((sum(NVL(TD.BOOK_BALANCE, 0))
                                                 OVER(partition by TRANS_GRP_ID) +
                                                 sum(NVL(XFER_TAKEN_FROM.AMOUNT, 0))
                                                 OVER(partition by TRANS_GRP_ID)),
                                                0,
                                                0,
                                                DECODE(TRR.FIFO,
                                                       0,
                                                       (NVL(TD.BOOK_BALANCE, 0) +
                                                       NVL(XFER_TAKEN_FROM.AMOUNT, 0)) /
                                                       (sum(NVL(TD.BOOK_BALANCE, 0) +
                                                            NVL(XFER_TAKEN_FROM.AMOUNT, 0))
                                                        OVER(partition by TRANS_GRP_ID)) *
                                                       TBT.AMOUNT_UNPROCESSED_FROM,
                                                       DECODE(DENSE_RANK()
                                                              OVER(partition by TBT.TRANS_GRP_ID order by
                                                                   V.YEAR,
                                                                   ABS(TD.BOOK_BALANCE) desc,
                                                                   TRC.TAX_RECORD_ID),
                                                              1,
                                                              1,
                                                              0) * TBT.AMOUNT_UNPROCESSED_FROM)) NEW_XFER,
                                         TBT.AMOUNT_FROM TRANS_AMOUNT,
                                         TBT.BOOK_SALE_AMOUNT_FROM TRANS_SALES_AMOUNT,
                                         TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR_FROM, 'YYYY')) BOOK_VINTAGE_YEAR,
                                         TRC.TAX_RECORD_ID TAX_RECORD_ID,
                                         A_ITERATION ITERATION,
                                         TBT.TBT_GROUP_ID_FROM TBT_GROUP_ID,
                                         V.YEAR VINTAGE_YEAR,
                                         TBT.AMOUNT_UNPROCESSED_FROM,
                                         sum(NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0)) OVER(partition by TRANS_GRP_ID) ALLOC_SUBTOTAL,
                                         TRR.FIFO,
                                         TBT.COMPANY_ID_TO,
                                         TBT.TBT_GROUP_ID_TO,
                                         RANK() OVER(partition by TRC.TAX_RECORD_ID order by DECODE(SIGN(TBT.AMOUNT_FROM), -1, 1, 2) asc, TBT.AMOUNT_FROM desc) TRID_RANK

                           from TAX_BOOK_TRANSFERS_GRP TBT,
                                TAX_BOOK_TRANSLATE_GROUP_MAP TBX,
                                COMPANY C,
                                (select COMPANY_ID,
                                        ITERATION,
                                        VINTAGE_FORWARD,
                                        VINTAGE_BACK,
                                        SORT_ORDER,
                                        GFORMAT_STRING,
                                        FFORMAT_STRING,
                                        FIFO,
                                        MIN_VINTAGE
                                   from TAX_RETIRE_RULES TRR
                                 union
                                 select distinct COMPANY_ID,
                                                 1 ITERATION,
                                                 0 VINTAGE_FORWARD,
                                                 0 VINTAGE_BACK,
                                                 'book_balance D' SORT_ORDER,
                                                 null GFORMAT_STRING,
                                                 null FFORMAT_STRING,
                                                 0 FIFO,
                                                 1969 MIN_VINTAGE
                                   from (select distinct COMPANY_ID_FROM COMPANY_ID
                                           from TAX_BOOK_TRANSFERS_GRP
                                         minus
                                         select COMPANY_ID
                                           from TAX_RETIRE_RULES)) TRR,
                                TAX_DEPRECIATION TD,
                                TAX_CONTROL TC,
                                TAX_RECORD_CONTROL TRC,
                                VINTAGE V,
                                (select FROM_TRID TAX_RECORD_ID,
                                        TAX_YEAR,
                                        (sum(BOOK_AMOUNT) * -1) AMOUNT
                                   from TAX_TRANSFER_CONTROL TTC
                                  where TTC.VERSION_ID = A_VERSION_ID
                                    and TTC.TAX_YEAR = A_TAX_YEAR
                                  group by FROM_TRID, TAX_YEAR) XFER_TAKEN_FROM
                          where TBT.TBT_GROUP_ID_FROM = TBX.TBT_GROUP_ID(+)
                            and TBT.TAX_YEAR = A_TAX_YEAR
                            and TBT.COMPANY_ID_FROM = C.COMPANY_ID
                            and TBT.COMPANY_ID_FROM = TRR.COMPANY_ID
                            and TBT.AMOUNT_FROM <> 0
                            and TBT.TAX_ACTIVITY_CODE_ID_FROM in (6)
                            and TBT.TRANSFER_STATUS_FROM in (-99, -98)
                            and TRR.ITERATION = A_ITERATION
                            and TD.TAX_RECORD_ID = TRC.TAX_RECORD_ID
                            and TC.TAX_RECORD_ID = TRC.TAX_RECORD_ID
                            and TD.TAX_BOOK_ID = TC.TAX_BOOK_ID
                            and TRC.COMPANY_ID = C.COMPANY_ID
                            and TRC.VINTAGE_ID = V.VINTAGE_ID
                            and TD.TAX_RECORD_ID = XFER_TAKEN_FROM.TAX_RECORD_ID(+)
                            and TD.TAX_YEAR = XFER_TAKEN_FROM.TAX_YEAR(+)
                            and TBT.COMPANY_ID_FROM = TRC.COMPANY_ID
                            and TBX.TAX_CLASS_ID = TRC.TAX_CLASS_ID
                            and TBT.TAX_YEAR = TD.TAX_YEAR
                            and V.YEAR <= DECODE(SIGN(TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR_FROM,
                                                                        'YYYY')) - MIN_VINTAGE),
                                                 -1,
                                                 MIN_VINTAGE,
                                                 TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR_FROM, 'YYYY'))) +
                                TRR.VINTAGE_FORWARD
                            and V.YEAR >= DECODE(SIGN(TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR_FROM,
                                                                        'YYYY')) - MIN_VINTAGE),
                                                 -1,
                                                 MIN_VINTAGE,
                                                 TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR_FROM, 'YYYY'))) -
                                TRR.VINTAGE_FORWARD
                            and 1 =
                                DECODE(SIGN(TBT.AMOUNT_FROM),
                                       -1,
                                       1,
                                       SIGN(NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0)))
                            and TD.TAX_BOOK_ID = 10
                            and TRC.VERSION_ID = A_VERSION_ID)
                  where DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               NEW_XFER,
                               DECODE(SIGN(BALANCE - NEW_XFER), -1, BALANCE, NEW_XFER)) <> 0)
          where TRANS_RANK = 1;

      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows inserted into tax_trans_audit_trail_grp.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      if NUM_ROWS = 0 then
         L_MSG := 'Exiting function on 0 rows inserts';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return 'ZERO ROWS';
      end if;

      L_MSG := 'Updating tax_book_transfers_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_BOOK_TRANSFERS_GRP TBT
         set TRANSFER_STATUS_FROM = NVL((select max(TRANSFER_STATUS)
                                           from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
                                          where TTAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                                            and TTAT.TAX_YEAR = TBT.TAX_YEAR
                                            and TBT.TAX_YEAR = A_TAX_YEAR),
                                         -99),
             AMOUNT_UNPROCESSED_FROM = AMOUNT_UNPROCESSED_FROM -
                                        NVL((select sum(ALLOC_AMOUNT)
                                              from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
                                             where TTAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                                               and TTAT.TAX_YEAR = TBT.TAX_YEAR
                                               and TBT.TAX_YEAR = A_TAX_YEAR
                                               and TTAT.ITERATION = A_ITERATION
                                               and TTAT.BATCH = A_BATCH),
                                            0)
       where TAX_YEAR = A_TAX_YEAR
         and TRANSFER_STATUS_FROM in (-99, -98)
         and exists (select 1
                from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
               where TTAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                 and TTAT.TAX_YEAR = TBT.TAX_YEAR
                 and TTAT.ITERATION = A_ITERATION
                 and TBT.TAX_YEAR = A_TAX_YEAR);
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_book_transfers_grp.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TTAT.FROM_TRID, TTAT.TAX_YEAR, TTAT.COMPANY_ID_TO, sum(TTAT.ALLOC_AMOUNT) bulk collect
        into TTC_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
       where TTAT.TAX_YEAR = A_TAX_YEAR
         and TTAT.ITERATION = A_ITERATION
         and TTAT.BATCH = A_BATCH
       group by TTAT.FROM_TRID, TTAT.TAX_YEAR, TTAT.COMPANY_ID_TO;

      forall I in TTC_TBL.FIRST .. TTC_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set BOOK_AMOUNT = BOOK_AMOUNT + TTC_TBL(I).BOOK_AMOUNT
          where FROM_TRID = TTC_TBL(I).FROM_TRID
            and TAX_YEAR = TTC_TBL(I).TAX_YEAR
            and VERSION_ID = A_VERSION_ID
            and COMPANY_ID = TTC_TBL(I).COMPANY_ID;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Inserting into tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      insert into TAX_TRANSFER_CONTROL TTC
         (TAX_TRANSFER_ID, FROM_TRID, TAX_YEAR, BOOK_AMOUNT, PACKAGE_ID, VERSION_ID, TO_TRID,
          CREATE_TO_TRID, COMPANY_ID, PROCEEDS)
         select MAX_ID + ROWNUM,
                FROM_TRID,
                TAX_YEAR,
                BOOK_AMOUNT,
                -1,
                A_VERSION_ID,
                -1,
                1,
                COMPANY_ID_TO,
                0
           from (select TTAT.FROM_TRID,
                        TTAT.TAX_YEAR,
                        TTAT.COMPANY_ID_TO,
                        sum(TTAT.ALLOC_AMOUNT) BOOK_AMOUNT
                   from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
                  where TTAT.TAX_YEAR = A_TAX_YEAR
                    and TTAT.ITERATION = A_ITERATION
                    and TTAT.BATCH = A_BATCH
                    and not exists (select 1
                           from TAX_TRANSFER_CONTROL TTC
                          where TTC.FROM_TRID = TTAT.FROM_TRID
                            and TTC.TAX_YEAR = TTAT.TAX_YEAR
                            and TTC.VERSION_ID = A_VERSION_ID
                            and TTC.COMPANY_ID = TTAT.COMPANY_ID_TO)
                  group by TTAT.FROM_TRID, TTAT.TAX_YEAR, COMPANY_ID_TO),
                (select NVL(max(TAX_TRANSFER_ID), 0) MAX_ID from TAX_TRANSFER_CONTROL);
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows inserted into tax_transfer_control.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_RUN_BATCH;

   -- function to process the transfer froms
   function F_RUN_ITERATION(A_TAX_YEAR   number,
                            A_START_MO   number,
                            A_END_MO     number,
                            A_VERSION_ID number) return varchar2 is
      L_MSG     varchar2(2000);
      NUM_ROWS  number;
      MAX_ITER  number;
      BATCH_NO  number;
      ZERO_ROWS boolean;
      SQLS_CHAR varchar2(10000);
   begin

      L_MSG := 'Retrieving max number of iterations to run...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      select max(NVL(ITERATION, 0)) into MAX_ITER from TAX_RETIRE_RULES TRR;

      if MAX_ITER < 1 or MAX_ITER is null then
         L_MSG := 'Error on selecting the max iteration from tax retire rules.';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
      end if;

      for I in 1 .. MAX_ITER
      loop
         L_MSG := 'Entering iteration loop ' || I || ' of ' || MAX_ITER || '...';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         BATCH_NO  := 1;
         ZERO_ROWS := false;

         while ZERO_ROWS = false
         loop

            if BATCH_NO > 100000 then
               L_MSG := 'Infinite Loop Error Encountered! Exiting!';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               return L_MSG;
            end if;

            L_MSG := ' ';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := 'Entering batch function. Iteration ' || I || ', Batch ' || BATCH_NO || '...';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := F_RUN_BATCH(A_TAX_YEAR, A_START_MO, A_END_MO, A_VERSION_ID, I, BATCH_NO);
            if L_MSG = 'ZERO ROWS' then
               ZERO_ROWS := true;
            elsif L_MSG <> 'OK' then
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               return L_MSG;
            end if;

            BATCH_NO := BATCH_NO + 1;

         end loop;
      end loop;

      return 'OK';

   exception
      when others then
         return L_MSG;
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         SQLS_CHAR := SUBSTR('SQLS : ' || TO_CHAR(SQLS_CHAR), 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(SQLS_CHAR);
         return L_MSG;
   end F_RUN_ITERATION;

   -- after the looping, go through some final calculations to:
   -- 1.  allocate the "proceeds from" per trans group however the amount was allocated.
   -- 2.  find and update the transfer "tos" for the processed transfer "froms"
   -- 3.  update the calc_deferred_gain, transfer type, and status comment
   --
   function F_FINAL_CALCS(A_TAX_YEAR   number,
                          A_VERSION_ID number) return varchar2 is
      L_MSG     varchar2(2000);
      NUM_ROWS  number;
      MAX_ITER  number;
      BATCH_NO  number;
      ZERO_ROWS boolean;
      SQLS_CHAR varchar2(10000);

      type AUDIT_SELECT_REC_TYPE is record(
         TRANS_GRP_ID       TAX_TRANS_AUDIT_TRAIL_GRP.TRANS_GRP_ID%type,
         TAX_YEAR           TAX_TRANS_AUDIT_TRAIL_GRP.TAX_YEAR%type,
         FROM_TRID          TAX_TRANS_AUDIT_TRAIL_GRP.FROM_TRID%type,
         BATCH              TAX_TRANS_AUDIT_TRAIL_GRP.BATCH%type,
         ITERATION          TAX_TRANS_AUDIT_TRAIL_GRP.ITERATION%type,
         ALLOC_SALES_AMOUNT TAX_TRANS_AUDIT_TRAIL_GRP.ALLOC_SALES_AMOUNT%type);
      type AUDIT_SELECT_TBL_TYPE is table of AUDIT_SELECT_REC_TYPE;
      AUDIT_SELECT_TBL AUDIT_SELECT_TBL_TYPE;

      type TTAT_GRP_REC_TYPE is record(
         TRANS_GRP_ID TAX_TRANS_AUDIT_TRAIL_GRP.TRANS_GRP_ID%type,
         TAX_YEAR     TAX_TRANS_AUDIT_TRAIL_GRP.TAX_YEAR%type,
         TO_TRID      TAX_TRANS_AUDIT_TRAIL_GRP.TO_TRID%type,
         FROM_TRID    TAX_TRANS_AUDIT_TRAIL_GRP.FROM_TRID%type);
      type TTAT_GRP_TBL_TYPE is table of TTAT_GRP_REC_TYPE;
      TTAT_GRP_TBL TTAT_GRP_TBL_TYPE;

      type TTC_REC_TYPE is record(
         TAX_YEAR  TAX_TRANSFER_CONTROL.TAX_YEAR%type,
         TO_TRID   TAX_TRANSFER_CONTROL.TO_TRID%type,
         FROM_TRID TAX_TRANSFER_CONTROL.FROM_TRID%type,
         PROCEEDS  TAX_TRANSFER_CONTROL.PROCEEDS%type);
      type TTC_TBL_TYPE is table of TTC_REC_TYPE;
      TTC_TBL      TTC_TBL_TYPE;
      ANALYZE_CODE number(22, 0);

      type TTC_TCLASS_REC_TYPE is record(
         TAX_TRANSFER_ID TAX_TRANSFER_CONTROL.TAX_TRANSFER_ID%type,
         TAX_CLASS_ID    TAX_TRANSFER_CONTROL.TAX_CLASS_ID%type);
      type TTC_TCLASS_TBL_TYPE is table of TTC_TCLASS_REC_TYPE;
      TTC_TCLASS_TBL TTC_TCLASS_TBL_TYPE;

   begin

      L_MSG := 'Regathering stats for tax_trans_audit_trail_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_trans_audit_trail_grp');

      L_MSG := 'Regathering stats for tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_transfer_control');

      -- 1.  allocate the "proceeds from" per trans group however the amount was allocated.
      L_MSG := 'Updating alloc_sales_amount on tax_trans_audit_trail_grp ...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TTAT.TRANS_GRP_ID,
             TAX_YEAR,
             FROM_TRID,
             BATCH,
             ITERATION,
             DECODE(AMOUNT_PROCESSED, 0, 0, ALLOC_AMOUNT / AMOUNT_PROCESSED) *
             (TRANS_SALES_AMOUNT * NVL(PCNT_ALLOCATED, 1)) bulk collect
        into AUDIT_SELECT_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT,
             (select TRANS_GRP_ID,
                     ((AMOUNT_FROM - AMOUNT_UNPROCESSED_FROM) / AMOUNT_FROM) PCNT_ALLOCATED,
                     (AMOUNT_FROM - AMOUNT_UNPROCESSED_FROM) AMOUNT_PROCESSED
                from TAX_BOOK_TRANSFERS_GRP
               where TAX_YEAR = A_TAX_YEAR
                 and NVL(AMOUNT_FROM, 0) <> 0) PPV /*we need this view to mimic however much the book amount was transferred, for the sales amount.*/
      /*if there's a $100 transfer, with $50 sales amount, but only $90 of the trans was processed,*/
      /*then we should only process $45 of the sales amount.  If users/product ever decides to exclude*/
      /*the sales corresponding to trans amounts not fully processed, then this view can just be stripped out.*/
       where TAX_YEAR = A_TAX_YEAR
         and NVL(TRANS_SALES_AMOUNT, 0) <> 0
         and TTAT.TRANS_GRP_ID = PPV.TRANS_GRP_ID(+);

      forall I in AUDIT_SELECT_TBL.FIRST .. AUDIT_SELECT_TBL.LAST
         update TAX_TRANS_AUDIT_TRAIL_GRP
            set ALLOC_SALES_AMOUNT = AUDIT_SELECT_TBL(I).ALLOC_SALES_AMOUNT
          where TAX_YEAR = A_TAX_YEAR
            and NVL(TRANS_SALES_AMOUNT, 0) <> 0
            and TRANS_GRP_ID = AUDIT_SELECT_TBL(I).TRANS_GRP_ID
            and TAX_YEAR = AUDIT_SELECT_TBL(I).TAX_YEAR
            and FROM_TRID = AUDIT_SELECT_TBL(I).FROM_TRID
            and BATCH = AUDIT_SELECT_TBL(I).BATCH
            and ITERATION = AUDIT_SELECT_TBL(I).ITERATION;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_trans_audit_trail_grp with allocated sales amount.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating alloc_sales_amount on tax_transfer_control ...';
      select TTAT.TAX_YEAR, 0 TO_TRID, TTAT.FROM_TRID, sum(ALLOC_SALES_AMOUNT) bulk collect
        into TTC_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
       where TTAT.TAX_YEAR = A_TAX_YEAR
         and NVL(ALLOC_SALES_AMOUNT, 0) <> 0
       group by TTAT.TAX_YEAR, TTAT.FROM_TRID;

      forall I in TTC_TBL.FIRST .. TTC_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set PROCEEDS = TTC_TBL(I).PROCEEDS
          where FROM_TRID = TTC_TBL(I).FROM_TRID
            and TAX_YEAR = TTC_TBL(I).TAX_YEAR
            and TAX_YEAR = A_TAX_YEAR
            and VERSION_ID = A_VERSION_ID;

      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with proceeds.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- 2.  find and update the transfer "tos" for the processed transfer "froms"
      L_MSG := 'Updating to_trid on tax_trans_audit_trail_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TTAT.TRANS_GRP_ID,
             TTAT.TAX_YEAR,
             TRC_TO.TAX_RECORD_ID TO_TRID,
             TTAT.FROM_TRID       bulk collect
        into TTAT_GRP_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT,
             TAX_RECORD_CONTROL TRC_FROM,
             (select TAX_RECORD_ID,
                     LISTAGG(TO_CHAR(TAX_BOOK_ID) || ':') WITHIN group(order by TAX_RECORD_ID) TAX_BOOKS,
                     LISTAGG(TO_CHAR(TAX_RATE_ID) || ':') WITHIN group(order by TAX_RECORD_ID) TAX_RATES
                from TAX_CONTROL
               group by TAX_RECORD_ID) BOOKS_RATES_FROM,
             TAX_BOOK_TRANSFERS_GRP TBT,
             TAX_BOOK_TRANSLATE_GROUP_MAP TBX,
             TAX_RECORD_CONTROL TRC_TO,
             (select TAX_RECORD_ID,
                     LISTAGG(TO_CHAR(TAX_BOOK_ID) || ':') WITHIN group(order by TAX_RECORD_ID) TAX_BOOKS,
                     LISTAGG(TO_CHAR(TAX_RATE_ID) || ':') WITHIN group(order by TAX_RECORD_ID) TAX_RATES
                from TAX_CONTROL
               group by TAX_RECORD_ID) BOOKS_RATES_TO
       where TTAT.TAX_YEAR = A_TAX_YEAR
         and TTAT.FROM_TRID = TRC_FROM.TAX_RECORD_ID
         and TRC_FROM.TAX_RECORD_ID = BOOKS_RATES_FROM.TAX_RECORD_ID
         and TTAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
         and TBT.TAX_YEAR = A_TAX_YEAR
         and TBT.TBT_GROUP_ID_TO = TBX.TBT_GROUP_ID
         and TBT.TAX_ACTIVITY_CODE_ID_TO in (5)
         and TBX.TAX_CLASS_ID = TRC_TO.TAX_CLASS_ID
         and TRC_TO.COMPANY_ID = TBT.COMPANY_ID_TO
         and TRC_TO.VINTAGE_ID = TRC_FROM.VINTAGE_ID /*the from and to must have the same vintage*/
         and TRC_TO.TAX_RECORD_ID = BOOKS_RATES_TO.TAX_RECORD_ID
         and BOOKS_RATES_TO.TAX_BOOKS = BOOKS_RATES_FROM.TAX_BOOKS /*the from and to records must have the same books*/
         and BOOKS_RATES_TO.TAX_RATES = BOOKS_RATES_FROM.TAX_RATES /*the from and to records must have the same rates*/
         and TRC_FROM.VERSION_ID = A_VERSION_ID
         and TRC_TO.VERSION_ID = A_VERSION_ID;

      forall I in TTAT_GRP_TBL.FIRST .. TTAT_GRP_TBL.LAST
         update TAX_TRANS_AUDIT_TRAIL_GRP
            set TO_TRID = TTAT_GRP_TBL(I).TO_TRID, CREATE_TO_TRID = 0
          where TRANS_GRP_ID = TTAT_GRP_TBL(I).TRANS_GRP_ID
            and FROM_TRID = TTAT_GRP_TBL(I).FROM_TRID
            and TAX_YEAR = TTAT_GRP_TBL(I).TAX_YEAR
            and TO_TRID < 0;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_trans_audit_trail_grp with found "to" tax records.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating to_trid on tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TTAT.TAX_YEAR, TTAT.TO_TRID TO_TRID, TTAT.FROM_TRID, 0 bulk collect
        into TTC_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
       where TTAT.TAX_YEAR = A_TAX_YEAR
         and NVL(TO_TRID, -1) > 0;

      forall I in TTC_TBL.FIRST .. TTC_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set TO_TRID = TTC_TBL(I).TO_TRID, CREATE_TO_TRID = 0
          where FROM_TRID = TTC_TBL(I).FROM_TRID
            and TAX_YEAR = TTC_TBL(I).TAX_YEAR
            and TAX_YEAR = A_TAX_YEAR
            and VERSION_ID = A_VERSION_ID
            and TO_TRID < 0;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with found "to" tax records.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- 3.  update the calc_deferred_gain, transfer type, and status comment
      L_MSG := 'Updating calc_deferred_gain, transfer type, and status comment (part 1)...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_TRANSFER_CONTROL TTC
         set CALC_DEFERRED_GAIN = 1, TRANSFER_TYPE = 'INTERCO',
             STATUS_COMMENT = 'OK, Inter-Company Deferred Gain Transfer'
       where TAX_YEAR = A_TAX_YEAR
         and VERSION_ID = A_VERSION_ID
         and NVL(PROCEEDS, 0) <> 0
         and exists (select 1
                from TAX_RECORD_CONTROL TRC_FROM
               where TRC_FROM.TAX_RECORD_ID = TTC.FROM_TRID
                 and TTC.COMPANY_ID <> TRC_FROM.COMPANY_ID);
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating calc_deferred_gain, transfer type, and status comment (part 2)...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_TRANSFER_CONTROL TTC
         set CALC_DEFERRED_GAIN = 0, TRANSFER_TYPE = 'SEC351',
             STATUS_COMMENT = 'OK, Section 351 Transfer'
       where TAX_YEAR = A_TAX_YEAR
         and VERSION_ID = A_VERSION_ID
         and CALC_DEFERRED_GAIN is null;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- 4.  update the tax_class_id to use for newly created tax records
      L_MSG := 'Updating tax_class_id on tax_transfer_control for rows that will require new tax records (where active found) ...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      select TTC.TAX_TRANSFER_ID, max(TBTGM.TAX_CLASS_ID) bulk collect
        into TTC_TCLASS_TBL
        from TAX_TRANSFER_CONTROL         TTC,
             TAX_TRANS_AUDIT_TRAIL_GRP    TTAT,
             TAX_BOOK_TRANSLATE_GROUP_MAP TBTGM
       where TTC.TAX_YEAR = A_TAX_YEAR
         and TTAT.TAX_YEAR = A_TAX_YEAR
         and TTC.VERSION_ID = A_VERSION_ID
         and TTC.CREATE_TO_TRID = 1
         and TTC.FROM_TRID = TTAT.FROM_TRID
         and TTC.COMPANY_ID = TTAT.COMPANY_ID_TO
         and TTAT.TBT_GROUP_ID_TO = TBTGM.TBT_GROUP_ID
         and TBTGM.ACTIVE <> 0
         and NVL(TTC.TAX_CLASS_ID, -999) = -999
       group by TTC.TAX_TRANSFER_ID;

      forall I in TTC_TCLASS_TBL.FIRST .. TTC_TCLASS_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set TAX_CLASS_ID = TTC_TCLASS_TBL(I).TAX_CLASS_ID
          where TAX_TRANSFER_ID = TTC_TCLASS_TBL(I).TAX_TRANSFER_ID;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with tax class ids needed for new tax records (where active found).';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating tax_class_id on tax_transfer_control for rows that will require new tax records (remaining rows) ...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      select TTC.TAX_TRANSFER_ID, max(TBTGM.TAX_CLASS_ID) bulk collect
        into TTC_TCLASS_TBL
        from TAX_TRANSFER_CONTROL         TTC,
             TAX_TRANS_AUDIT_TRAIL_GRP    TTAT,
             TAX_BOOK_TRANSLATE_GROUP_MAP TBTGM
       where TTC.TAX_YEAR = A_TAX_YEAR
         and TTAT.TAX_YEAR = A_TAX_YEAR
         and TTC.VERSION_ID = A_VERSION_ID
         and TTC.CREATE_TO_TRID = 1
         and TTC.FROM_TRID = TTAT.FROM_TRID
         and TTC.COMPANY_ID = TTAT.COMPANY_ID_TO
         and TTAT.TBT_GROUP_ID_TO = TBTGM.TBT_GROUP_ID
         and NVL(TTC.TAX_CLASS_ID, -999) = -999
       group by TTC.TAX_TRANSFER_ID;

      forall I in TTC_TCLASS_TBL.FIRST .. TTC_TCLASS_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set TAX_CLASS_ID = TTC_TCLASS_TBL(I).TAX_CLASS_ID
          where TAX_TRANSFER_ID = TTC_TCLASS_TBL(I).TAX_TRANSFER_ID;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with tax class ids needed for new tax records (remaining rows).';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return 'OK';

   exception
      when others then
         return L_MSG;
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         SQLS_CHAR := SUBSTR('SQLS : ' || TO_CHAR(SQLS_CHAR), 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(SQLS_CHAR);
         return L_MSG;
   end F_FINAL_CALCS;

   -- function to start the log, process the transfers, end the log
   function F_POWERTAX_XFERS(A_TAX_YEAR   number,
                             A_START_MO   number,
                             A_END_MO     number,
                             A_VERSION_ID number) return varchar2 is
      L_MSG     varchar2(2000);
      NUM_ROWS  number;
      MAX_ITER  number;
      BATCH_NO  number;
      ZERO_ROWS boolean;
      I         number;

   begin

      if L_PROCESS_ID = -1 or L_PROCESS_ID is null then
         P_SETPROCESS();
      end if;

      PKG_PP_LOG.P_START_LOG(L_PROCESS_ID);

      PKG_PP_LOG.P_WRITE_MESSAGE('Starting...');
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing with the following variable definitions:');
      PKG_PP_LOG.P_WRITE_MESSAGE('Tax Year : ' || A_TAX_YEAR);

      PKG_PP_LOG.P_WRITE_MESSAGE('Version ID : ' || A_VERSION_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE('Entering processing function...');
      L_MSG := F_RUN_ITERATION(A_TAX_YEAR, A_START_MO, A_END_MO, A_VERSION_ID);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);
      PKG_PP_LOG.P_WRITE_MESSAGE('Entering final calculations function...');
      L_MSG := F_FINAL_CALCS(A_TAX_YEAR, A_VERSION_ID);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);
      PKG_PP_LOG.P_WRITE_MESSAGE('Finished...');
      PKG_PP_LOG.P_END_LOG();
      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
   end F_POWERTAX_XFERS;

end PKG_TAX_INT_XFER;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (761, 0, 10, 4, 2, 0, 31484, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_031484_pwrtax_PKG_TAX_INT_XFER.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
