/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_045523_pwrtax_pr_table_pkg_parms_ddl.sql
||========================================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2016.1.0.0 03/17/2016 David Haupt     	   Renaming table used for plant recon package args
||========================================================================================
*/

alter table tax_job_form_refresh_params
	rename to tax_job_pr_form_params;
	
alter table tax_job_pr_form_params
	add tax_version_id  number(22,0);
	
comment on column tax_job_pr_form_params.tax_version_id is 'The tax_version_id used by the PKG_TAX_PLANT_RECON package';

alter table tax_job_pr_form_params
	modify refresh_plant number(1,0) null;
	
alter table tax_job_pr_form_params
	modify refresh_depr number(1,0) null;
	
alter table tax_job_pr_form_params
	modify refresh_tax number(1,0) null;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3115, 0, 2016, 1, 0, 0, 045523, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045523_pwrtax_pr_table_pkg_parms_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;