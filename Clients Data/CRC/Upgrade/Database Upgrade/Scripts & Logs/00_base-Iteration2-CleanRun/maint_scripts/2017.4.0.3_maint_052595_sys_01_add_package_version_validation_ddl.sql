 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_052595_sys_01_add_package_version_validation_ddl.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2017.4.0.3  10/30/2018 C.Yura    Adding a new table for use in Package Version Validation
 ||============================================================================
 */

CREATE TABLE pp_package_versions
(
      package_name VARCHAR2(254),
      version VARCHAR2(35),
      user_id VARCHAR2(18),
      time_stamp date
);


ALTER TABLE pp_package_versions
  ADD CONSTRAINT pk_pp_package_versions
  PRIMARY KEY (package_name)
  USING INDEX TABLESPACE pwrplant_idx;

COMMENT ON TABLE pp_package_versions IS 'The pp_package_versions table is for version control on database packages. Version in this table must match the package version.';
COMMENT ON COLUMN pp_package_versions.package_name   IS 'Database Package Name';
COMMENT ON COLUMN pp_package_versions.version      IS 'Version of Database Package';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11222, 0, 2017, 4, 0, 3, 52595, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052595_sys_01_add_package_version_validation_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;