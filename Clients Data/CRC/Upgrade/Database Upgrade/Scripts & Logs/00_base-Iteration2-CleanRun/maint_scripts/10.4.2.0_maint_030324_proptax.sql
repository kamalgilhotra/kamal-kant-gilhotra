/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030324_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/05/2014 Julia Breuer
||============================================================================
*/

alter table PT_VAL_SCENARIO_RESULT   drop column FORECAST_YEAR;
alter table PT_VAL_TEMPLATE_EQUATION drop column FORECAST_YN;
alter table PT_VAL_TEMPLATE_EQUATION drop column FORECAST_YEARS;
alter table PT_VAL_TEMPLATE          drop column FORECAST_YN;
alter table PT_VAL_TEMPLATE          drop column FORECAST_YEARS;
alter table PT_TEMP_SCENARIO_FORMULA drop column FORECAST_YEAR;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (824, 0, 10, 4, 2, 0, 30324, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_030324_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;