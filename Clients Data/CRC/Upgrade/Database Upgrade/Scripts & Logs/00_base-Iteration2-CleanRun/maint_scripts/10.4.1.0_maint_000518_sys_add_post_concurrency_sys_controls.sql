/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_000518_sys_add_post_concurrency_sys_controls.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 10.4.1.0   09/05/2013 Charlie Shilling maint 518
||============================================================================
*/

declare
   V_COUNT number(22, 0);
begin
   select count(1)
     into V_COUNT
     from PP_SYSTEM_CONTROL_COMPANY
    where UPPER(trim(CONTROL_NAME)) = 'POST CONCURRENT PROCESSING DEFAULT';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_CONTROL_COMPANY
         (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID,
          CONTROL_TYPE)
      values
         ((select NVL(max(CONTROL_ID), 0) + 1 from PP_SYSTEM_CONTROL_COMPANY),
          'POST CONCURRENT PROCESSING DEFAULT', 'NO', 'dw_yes_no;1',
          'Controls whether Post launches one executable for all companies or spawns separate copies for each company.',
          -1, 'system only');
   end if;

   select count(1)
     into V_COUNT
     from PP_SYSTEM_CONTROL_COMPANY
    where UPPER(trim(CONTROL_NAME)) = 'POST MAX CONCURRENT COPIES';

   if V_COUNT = 0 then
      insert into PP_SYSTEM_CONTROL_COMPANY
         (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID,
          CONTROL_TYPE)
      values
         ((select NVL(max(CONTROL_ID), 0) + 1 from PP_SYSTEM_CONTROL_COMPANY),
          'POST MAX CONCURRENT COPIES', '4', null,
          'Controls how many separate copies of Post can run concurrently.', -1, 'system only');
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (585, 0, 10, 4, 1, 0, 518, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_000518_sys_add_post_concurrency_sys_controls.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
