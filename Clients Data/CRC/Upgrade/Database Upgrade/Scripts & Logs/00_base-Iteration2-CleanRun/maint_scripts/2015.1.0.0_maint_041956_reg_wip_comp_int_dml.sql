 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041956_reg_wip_comp_int_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	12/31/2014 Shane Ward     New Integration Piece
||============================================================================
*/

--New Source for CWIP/Cap Budget 
INSERT INTO reg_cwip_source
	(reg_cwip_source_id, description, table_name)
VALUES
	(7, 'WIP Comp Balance', 'wip_comp_charges');

--CWIP Integration
INSERT INTO reg_cwip_component_element
	(reg_component_element_id,
	 description,
	 id_column_name,
	 description_column,
	 description_table)
	SELECT 9,
				 'WIP Computation Balance',
				 id_column_name,
				 description_column,
				 description_table
		FROM reg_cwip_component_element
	 WHERE reg_component_element_id = 8;

UPDATE reg_cwip_component_element
	 SET description = 'WIP Computation Activity'
 WHERE reg_component_element_id = 8;

--Capital Budget Integration
INSERT INTO reg_budget_component_element
	(reg_component_element_id,
	 description,
	 id_column_name,
	 description_column,
	 description_table)
	SELECT 9,
				 'WIP Computation Balance',
				 id_column_name,
				 description_column,
				 description_table
		FROM reg_cwip_component_element
	 WHERE reg_component_element_id = 8;

UPDATE reg_budget_component_element
	 SET description = 'WIP Computation Activity'
 WHERE reg_component_element_id = 8;
 
 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2141, 0, 2015, 1, 0, 0, 41956, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041956_reg_wip_comp_int_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;