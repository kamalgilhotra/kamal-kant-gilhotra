/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043009_anlyt_update_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.3 2/18/2015 Matt Mikulka      Update Analytics System Data
||============================================================================
*/


--Update Datasourceid = 1067
update pa_datasource
set selectexpression = '
bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_qty) TOTAL_QUANTITY, 
nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, 
(case when sum(curr_qty) = 0 then 0 else round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)end) ZERO_COST_PCT, 
(case when sum(curr_qty) = 0 then 0 else (1-round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4))end) NONZERO_COST_PCT
'
where datasourceid =  1067;

--Update Datasourceid = 1052
update pa_datasource
set selectexpression = '
COMPANY, sum(curr_qty) TOTAL_QUANTITY,
nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, 
(case when sum(curr_qty) = 0 then 0 else round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)*100 end) ZERO_COST_PCT,
(case when sum(curr_qty) = 0 then 0 else (1 - round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4))*100 end) NONZERO_COST_PCT
'
where datasourceid =  1052;

--Update Datasourceid = 1069
update pa_datasource
set selectexpression = '
company, sum(curr_qty) TOTAL_QUANTITY, 
nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, 
(case when sum(curr_qty) = 0 then 0 else round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)end) ZERO_COST_PCT, 
(case when sum(curr_qty) = 0 then 0 else (1-round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4))end) NONZERO_COST_PCT
'
where datasourceid =  1069;

--Update Datasourceid = 1070
update pa_datasource
set selectexpression = '
COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR,
sum(curr_qty) TOTAL_QTY, 
nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, 
(case when sum(curr_qty) = 0 then 0 else round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)end) ZERO_COST_PCT, 
(case when sum(curr_qty) = 0 then 0 else (1-round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4))end) NONZERO_COST_PCT
'
where datasourceid =  1070;

--Update Datasourceid = 1068
update pa_datasource
set selectexpression = '
state, major_location, sum(curr_qty) TOTAL_QUANTITY, 
nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, 
(case when sum(curr_qty) = 0 then 0 else round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)end) ZERO_COST_PCT, 
(case when sum(curr_qty) = 0 then 0 else (1-round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4))end) NONZERO_COST_PCT
'
where datasourceid =  1068;

--Update Datasourceid = 1053
update pa_datasource
set selectexpression = '
bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_cost) TOTAL_COST,
nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COST,
(case when sum(curr_cost) = 0 then 0 else round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)end) ZERO_QTY_PCT, 
(case when sum(curr_cost) = 0 then 0 else (1-round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4))end) NONZERO_QTY_PCT
'
where datasourceid =  1053;

--Update Datasourceid = 1048
update pa_datasource
set selectexpression = '
COMPANY, sum(curr_cost) TOTAL_COSTS, 
nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COSTS, 
(case when sum(curr_cost) = 0 then 0 else round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)*100 end) ZERO_QTY_PCT, 
(case when sum(curr_cost) = 0 then 0 else (1 - round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4))*100 end) NONZERO_QTY_PCT
'
where datasourceid =  1048;

--Update Datasourceid = 1055
update pa_datasource
set selectexpression = '
company, sum(curr_cost) TOTAL_COST, 
nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COST, 
(case when sum(curr_cost) = 0 then 0 else round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)end) ZERO_QTY_PCT, 
(case when sum(curr_cost) = 0 then 0 else (1-round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4))end) NONZERO_QTY_PCT
'
where datasourceid =  1055;

--Update Datasourceid = 1056
update pa_datasource
set selectexpression = '
COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR,
sum(curr_cost) TOTAL_COST, 
nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COST, 
(case when sum(curr_cost) = 0 then 0 else round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)end) ZERO_QTY_PCT, 
(case when sum(curr_cost) = 0 then 0 else (1-round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4))end) NONZERO_QTY_PCT
'
where datasourceid =  1056;

--Update Datasourceid = 1054
update pa_datasource
set selectexpression = '
state, major_location, sum(curr_cost) TOTAL_COST, 
nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COST, 
(case when sum(curr_cost) = 0 then 0 else round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)end) ZERO_QTY_PCT, 
(case when sum(curr_cost) = 0 then 0 else (1-round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4))end) NONZERO_QTY_PCT
'
where datasourceid =  1054;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2312, 0, 10, 4, 3, 3, 043009, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.3_maint_043009_anlyt_update_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;