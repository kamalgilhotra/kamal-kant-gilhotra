/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044421_pcm_subs_dvs_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/15/2015 A. McKinlay  	 Add DV Types for substitutions
||============================================================================
*/

INSERT INTO wo_validation_type (wo_validation_type_id, description, long_description, FUNCTION, find_company, col1, hard_edit)
SELECT 91, 'FP Subs Accept (Pre)', 'FP Subs Accept (Pre)', 'f_workflow_base', 
'select company_id from work_order_control where work_order_id = (select max(to_work_order_id) from budget_substitutions where budget_sub_id = <arg1>)',
'budget_sub_id', 1
FROM dual WHERE NOT EXISTS (SELECT 1 FROM wo_validation_type WHERE wo_validation_type_id = 91);

INSERT INTO wo_validation_type (wo_validation_type_id, description, long_description, FUNCTION, find_company, col1, hard_edit)
SELECT 92, 'FP Subs Accept (Post)', 'FP Subs Accept (Post)', 'f_workflow_base', 
'select company_id from work_order_control where work_order_id = (select max(to_work_order_id) from budget_substitutions where budget_sub_id = <arg1>)',
'budget_sub_id', 1
FROM dual WHERE NOT EXISTS (SELECT 1 FROM wo_validation_type WHERE wo_validation_type_id = 92);

INSERT INTO wo_validation_type (wo_validation_type_id, description, long_description, FUNCTION, find_company, col1, hard_edit)
SELECT 93, 'FP Subs Reject (Pre)', 'FP Subs Reject (Pre)', 'f_workflow_base', 
'select company_id from work_order_control where work_order_id = (select max(to_work_order_id) from budget_substitutions where budget_sub_id = <arg1>)',
'budget_sub_id', 1
FROM dual WHERE NOT EXISTS (SELECT 1 FROM wo_validation_type WHERE wo_validation_type_id = 93);

INSERT INTO wo_validation_type (wo_validation_type_id, description, long_description, FUNCTION, find_company, col1, hard_edit)
SELECT 94, 'FP Subs Reject (Post)', 'FP Subs Reject (Post)', 'f_workflow_base', 
'select company_id from work_order_control where work_order_id = (select max(to_work_order_id) from budget_substitutions where budget_sub_id = <arg1>)',
'budget_sub_id', 1
FROM dual WHERE NOT EXISTS (SELECT 1 FROM wo_validation_type WHERE wo_validation_type_id = 94);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2694, 0, 2015, 2, 0, 0, 044421, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044421_pcm_subs_dvs_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;