/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029489_cr_budget_templates.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   03/14/2013 Stephen Wicks    Point Release
||============================================================================
*/

alter table CR_BUDGET_TEMPLATES add NO_VALIDATION number(22,0);

alter table CR_BUDGET_TEMPLATES add EDIT_MONTHS_ONLY number(22,0);

update CR_BUDGET_TEMPLATES set NO_VALIDATION = 0, EDIT_MONTHS_ONLY = 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (322, 0, 10, 4, 1, 0, 29489, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029489_cr_budget_templates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;