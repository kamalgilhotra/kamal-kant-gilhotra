/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050423_lessor_03_add_performance_indices_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 02/15/2018 Jared Watkins    Add new indices to Lessor tables for performance
||============================================================================
*/

CREATE INDEX lsr_ilr_st_schedule_mth_idx
  ON lsr_ilr_schedule_sales_direct(month)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_st_schedule_tmth_idx
  ON lsr_ilr_schedule_sales_direct(TRUNC(month,'fmmonth'))
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_st_sch_ilr_rev_mth_idx
  ON lsr_ilr_schedule_sales_direct(ilr_id, revision, month)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_df_schedule_mth_idx
  ON lsr_ilr_schedule_direct_fin(month)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_df_schedule_tmth_idx
  ON lsr_ilr_schedule_direct_fin(TRUNC(month,'fmmonth'))
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_df_sch_ilr_rev_mth_idx
  ON lsr_ilr_schedule_direct_fin(ilr_id, revision, month)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_lease_id_num_idx
  ON lsr_lease(lease_id, lease_number)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_ilr_ls_cmp_ilnum_idx
  ON lsr_ilr(ilr_id, lease_id, company_id, ilr_number)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_payment_term_idx
  ON lsr_ilr_payment_term(payment_term_type_id, ilr_id, revision)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_pay_trm_ilr_rev_dt_idx
  ON lsr_ilr_payment_term(ilr_id, revision, payment_term_date)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_cap_type_id_descrip_idx
  ON lsr_cap_type(cap_type_id, description)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_id_rev_num_idx
  ON lsr_ilr(ilr_id, current_revision, ilr_number)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_ilr_options_many_idx
  ON lsr_ilr_options(ilr_id, revision, lease_cap_type_id, in_service_exchange_rate)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_rent_bucket_admin_many_idx
  ON lsr_receivable_bucket_admin(receivable_type, bucket_number, bucket_name)
TABLESPACE pwrplant_idx; 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4141, 0, 2017, 3, 0, 0, 50423, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050423_lessor_03_add_performance_indices_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	