 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046442_prov_add_types_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 09/23/2016 Andrew Hill	 Add varchar2 table type
 ||============================================================================
 */
 create type t_varchar2_array is table of varchar2(4000); 
 /

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3296, 0, 2016, 1, 0, 0, 046442, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046442_prov_add_types_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;