SET DEFINE OFF
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037396_lease_PKG_LEASE_SCHEDULE.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.1.0 05/07/2013 B.Beck           Original Version
|| 10.4.1.0 06/21/2013 Brandon Beck     Added BPO and Term Penalty.
||                                      Also correct the end obligation for guaranteed residual
|| 10.4.1.0 07/05/2013 Brandon Beck     Load schedule tables from the calc tables
|| 10.4.1.0 07/15/2013 B.Beck           Update IRR and NPV onto ls_ilr
|| 10.4.1.2 10/02/2013 Kyle Peterson    Interim Interest
|| 10.4.2.0 01/07/2014 Kyle Peterson
|| 10.4.2.0 03/17/2014 Brandon Beck
||============================================================================
*/

create or replace package PKG_LEASE_SCHEDULE as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LEASE_SCHEDULE_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 06/21/2013 Brandon Beck   Added BPO and Term Penalty.
   ||                                    Also correct the end obligation for guaranteed residual
   || 10.4.1.0 07/05/2013 Brandon Beck   Load schedule tables from the calc tables
   || 10.4.1.0 07/15/2013 B.Beck         Update IRR and NPV onto ls_ilr
   || 10.4.1.1 10/02/2013 Kyle Peterson  Interim Interest
   ||============================================================================
   */

   -- Function to process the asset (allocate, find NBV)
   function F_PROCESS_ASSETS return varchar2;

   -- Loads the ilr_scheduling table for an entire LEASE
   function F_PROCESS_ILRS(A_LEASE_ID number) return varchar2;

   --Loads LS_ILR_STG all at once to avoid PK violations
   function F_MASS_PROCESS_ILRS(A_ILR_IDS   PKG_LEASE_IMPORT.NUM_ARRAY,
                                A_REVISIONS PKG_LEASE_IMPORT.NUM_ARRAY) return varchar2;

   -- Loads the ilr_scheduling table for a single ILR
   function F_PROCESS_ILR(A_ILR_ID   number,
                          A_REVISION number) return varchar2;

   -- Builds forecast revisions for all ILR's
   function F_PROCESS_ILRS_FCST(A_CAP_TYPE number) return varchar2;

   /*
   *  Loads from the calc staging table to the "real" tables
   */
   function F_SAVE_SCHEDULES return varchar2;

   /*
   *  This function is used for Asset transfers to create new asset schedules based on the transfer
   */
   function F_PROCESS_ASSET_TRF(A_FROM_ASSET_ID     number,
                                A_TO_ASSET_ID       number,
                                A_FROM_ILR_REVISION number,
                                A_TO_ILR_REVISION   number,
                                A_PERCENT           number,
                                A_FROM_ILR_ID       number,
                                A_TO_ILR_ID         number) return varchar2;

   function F_GET_II_BEGIN_DATE(A_ILR_ID   number,
                                A_REVISION number) return date;

   function F_MAKE_II(A_ILR_ID   number,
                      A_REVISION number) return number;

end PKG_LEASE_SCHEDULE;
/

create or replace package body PKG_LEASE_SCHEDULE as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LEASE_SCHEDULE_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 06/21/2013 Brandon Beck   Added BPO and Term Penalty.
   ||                                    Also correct the end obligation for guaranteed residual
   || 10.4.1.0 07/05/2013 Brandon Beck   Load schedule tables from the calc tables
   || 10.4.1.0 07/15/2013 B.Beck         Update IRR and NPV onto ls_ilr
   ||============================================================================
   */

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   --**************************************************************************
   --                            P_CHECK_RECALC_NPV
   -- Recalc NPV after determing IRR for any ILRs that have an IRR
   -- set the process_npv to be 0 where irr is null
   --**************************************************************************
   procedure P_CHECK_RECALC_NPV is

   begin
      -- do not re-calculate NPV if OM
      update LS_ILR_STG set PROCESS_NPV = 0 where IS_OM = 1;

      -- do not recalc NPV if IRR was not found
      update LS_ILR_STG
         set PROCESS_NPV = 0
       where IRR is null
         and PROCESS_NPV = 1;

      -- do not recalc NPV if ANNUAL IRR not between 0 and 100
      update LS_ILR_STG A
         set A.PROCESS_NPV = 0, A.VALIDATION_MESSAGE = 'IRR Calculation not between 0 and 100%'
       where A.PROCESS_NPV = 1
         and (POWER((1 + A.IRR), 12) - 1) not between 0 and 1;

      update LS_ILR_SCHEDULE_STG S
         set PROCESS_NPV = 0
       where exists (select 1
                from LS_ILR_STG L
               where L.ILR_ID = S.ILR_ID
                 and L.REVISION = S.REVISION
                 and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                 and L.PROCESS_NPV = 0);

      -- set the rate to be the irr for the schedule
      update LS_ILR_SCHEDULE_STG S
         set RATE =
              (select L.IRR
                 from LS_ILR_STG L
                where L.ILR_ID = S.ILR_ID
                  and L.REVISION = S.REVISION
                  and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                  and L.PROCESS_NPV = 1)
       where exists (select 1
                from LS_ILR_STG L
               where L.ILR_ID = S.ILR_ID
                 and L.REVISION = S.REVISION
                 and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                 and L.PROCESS_NPV = 1);
   end P_CHECK_RECALC_NPV;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   --**************************************************************************
   --                            F_ASSET_ALLOC_NPV
   -- This function will allocate the NPV of the schedule down to the assets
   --**************************************************************************
   function F_ASSET_ALLOC_NPV return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      insert into LS_ILR_ASSET_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, CURRENT_LEASE_COST, ALLOC_NPV,
          RESIDUAL_AMOUNT, RESIDUAL_NPV, NPV_MINUS_RESIDUAL_NPV, TERM_PENALTY, BPO_PRICE, IS_OM)
         with LS_ASSET_VIEW as
          (select LA.ILR_ID,
                  LA.LS_ASSET_ID,
                  LA.FMV as CURRENT_LEASE_COST,
                  NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0) as RESIDUAL_AMOUNT,
                  LS.SET_OF_BOOKS_ID,
                  LS.REVISION,
                  NVL(RATIO_TO_REPORT(LA.FMV)
                      OVER(partition by LS.ILR_ID, LS.SET_OF_BOOKS_ID, LS.REVISION),
                      1) as PCT_SPREAD
             from LS_ASSET LA, LS_ILR_STG LS, LS_ILR_ASSET_MAP M
            where LA.ILR_ID = LS.ILR_ID
              and M.ILR_ID = LS.ILR_ID
              and M.REVISION = LS.REVISION
              and M.LS_ASSET_ID = LA.LS_ASSET_ID),
         LS_MONTHS_VIEW as
          (select Z.ILR_ID, Z.THE_LENGTH + 1 as THE_LENGTH, Z.RATE, Z.SET_OF_BOOKS_ID, Z.REVISION
             from (select MONTHS_BETWEEN(max(month)
                                         OVER(partition by ILR_ID, SET_OF_BOOKS_ID, REVISION),
                                         month) as THE_LENGTH,
                          ID,
                          month,
                          ILR_ID,
                          RATE,
                          SET_OF_BOOKS_ID,
                          REVISION
                     from LS_ILR_SCHEDULE_STG) Z
            where Z.ID = 1)
         select ROW_NUMBER() OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc, L.LS_ASSET_ID) as ID,
                S.ILR_ID,
                S.REVISION,
                L.LS_ASSET_ID,
                S.SET_OF_BOOKS_ID,
                L.CURRENT_LEASE_COST,
                ROUND(S.NPV * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.NPV - sum(ROUND(S.NPV * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0) as ALLOC_PAYMENT_WITH_PLUG,
                L.RESIDUAL_AMOUNT,
                ROUND(L.RESIDUAL_AMOUNT / POWER(1 + M.RATE, M.THE_LENGTH - S.PREPAY_SWITCH), 2),
                ROUND(S.NPV * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.NPV - sum(ROUND(S.NPV * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0) -
                ROUND(L.RESIDUAL_AMOUNT / POWER(1 + M.RATE, M.THE_LENGTH - S.PREPAY_SWITCH), 2),
                ROUND(S.TERM_PENALTY * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.TERM_PENALTY - sum(ROUND(S.TERM_PENALTY * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                ROUND(S.BPO_PRICE * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.BPO_PRICE - sum(ROUND(S.BPO_PRICE * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                S.IS_OM
           from LS_ILR_STG S, LS_ASSET_VIEW L, LS_MONTHS_VIEW M
          where S.ILR_ID = L.ILR_ID
            and S.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and S.REVISION = L.REVISION
            and S.ILR_ID = M.ILR_ID
            and S.SET_OF_BOOKS_ID = M.SET_OF_BOOKS_ID
            and S.REVISION = M.REVISION;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ASSET_ALLOC_NPV;

   --**************************************************************************
   --                            F_ALLOCATE_ASSET_PAYMENTS
   -- This function will allocate the payments to the assets
   -- Spreads the payments based on pct current lease obligation
   --**************************************************************************
   function F_ALLOCATE_ASSET_PAYMENTS return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting Allocation of payment to Assets';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_SCHEDULE_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, PREPAY_SWITCH, RATE,
          PAYMENT_MONTH, MONTHS_TO_ACCRUE, CURRENT_LEASE_COST, AMOUNT, RESIDUAL_AMOUNT,
          BEG_CAPITAL_COST, BEG_OBLIGATION, TERM_PENALTY, BPO_PRICE, CONTINGENT_PAID1,
          CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
          CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, EXECUTORY_PAID1,
          EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
          EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, IS_OM,
          PAYMENT_TERM_TYPE_ID)
         with LS_ASSET_VIEW as
          (select LA.ILR_ID,
                  LA.LS_ASSET_ID,
                  LA.CURRENT_LEASE_COST,
                  LA.RESIDUAL_AMOUNT as RESIDUAL_AMOUNT,
                  LA.ALLOC_NPV,
                  RATIO_TO_REPORT(LA.NPV_MINUS_RESIDUAL_NPV) OVER(partition by LA.ILR_ID, LA.REVISION, LA.SET_OF_BOOKS_ID) as PCT_SPREAD,
                  LA.TERM_PENALTY,
                  LA.BPO_PRICE,
                  LA.REVISION,
                  LA.SET_OF_BOOKS_ID
             from LS_ILR_ASSET_STG LA)
         select ROW_NUMBER() OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, L.LS_ASSET_ID order by S.MONTH) as ID,
                S.ILR_ID,
                S.REVISION,
                L.LS_ASSET_ID,
                S.SET_OF_BOOKS_ID,
                S.MONTH,
                S.PREPAY_SWITCH,
                S.RATE,
                S.PAYMENT_MONTH,
                S.MONTHS_TO_ACCRUE,
                L.CURRENT_LEASE_COST,
                ROUND(S.AMOUNT * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.AMOUNT - sum(ROUND(S.AMOUNT * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0) as ALLOC_PAYMENT_WITH_PLUG,
                DECODE(S.RESIDUAL_AMOUNT, 0, 0, L.RESIDUAL_AMOUNT) as RESIDUAL,
                L.ALLOC_NPV,
                L.ALLOC_NPV,
                DECODE(S.TERM_PENALTY, 0, 0, L.TERM_PENALTY),
                DECODE(S.BPO_PRICE, 0, 0, L.BPO_PRICE),
                ROUND(S.CONTINGENT_PAID1 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID1 - sum(ROUND(S.CONTINGENT_PAID1 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID2 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID2 - sum(ROUND(S.CONTINGENT_PAID2 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID3 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID3 - sum(ROUND(S.CONTINGENT_PAID3 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID4 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID4 - sum(ROUND(S.CONTINGENT_PAID4 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID5 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID5 - sum(ROUND(S.CONTINGENT_PAID5 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID6 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID6 - sum(ROUND(S.CONTINGENT_PAID6 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID7 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID7 - sum(ROUND(S.CONTINGENT_PAID7 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID8 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID8 - sum(ROUND(S.CONTINGENT_PAID8 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID9 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID9 - sum(ROUND(S.CONTINGENT_PAID9 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID10 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID10 - sum(ROUND(S.CONTINGENT_PAID10 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID1 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID1 - sum(ROUND(S.EXECUTORY_PAID1 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID2 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID2 - sum(ROUND(S.EXECUTORY_PAID2 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID3 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID3 - sum(ROUND(S.EXECUTORY_PAID3 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID4 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID4 - sum(ROUND(S.EXECUTORY_PAID4 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID5 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID5 - sum(ROUND(S.EXECUTORY_PAID5 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID6 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID6 - sum(ROUND(S.EXECUTORY_PAID6 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID7 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID7 - sum(ROUND(S.EXECUTORY_PAID7 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID8 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID8 - sum(ROUND(S.EXECUTORY_PAID8 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID9 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID9 - sum(ROUND(S.EXECUTORY_PAID9 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID10 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID10 - sum(ROUND(S.EXECUTORY_PAID10 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                S.IS_OM,
                S.PAYMENT_TERM_TYPE_ID
           from LS_ILR_SCHEDULE_STG S, LS_ASSET_VIEW L
          where S.ILR_ID = L.ILR_ID
            and S.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and S.REVISION = L.REVISION;

      -- make sure month with residual is set to 1
      L_STATUS := 'Setting the payment month for residual amounts';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ILR_ASSET_SCHEDULE_STG S set S.PAYMENT_MONTH = 1 where RESIDUAL_AMOUNT <> 0;

      -- update the last payment month to be a 2
      update LS_ILR_ASSET_SCHEDULE_STG S
         set S.PAYMENT_MONTH = 2
       where (S.LS_ASSET_ID, S.MONTH) in (select A.LS_ASSET_ID, A.MONTH
                                            from (select B.LS_ASSET_ID,
                                                         B.MONTH,
                                                         ROW_NUMBER() OVER(partition by B.LS_ASSET_ID order by B.MONTH desc) as THE_ROW
                                                    from LS_ILR_ASSET_SCHEDULE_STG B) A
                                           where THE_ROW = 1);

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_ASSET_PAYMENTS;

   --**************************************************************************
   --                            F_ALLOCATE_CONT_EXEC
   -- This function allocates the executory paid and contingent paid amounts
   -- To be accrued based on payment frequency
   --**************************************************************************
   function F_ALLOCATE_CONT_EXEC return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_CHECK  number;
   begin
      L_STATUS := 'Starting to determine contingent and executory accrual amounts';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_SCHEDULE_BUCKET_STG;

      L_CHECK := 0;
      select count(1)
        into L_CHECK
        from LS_ILR_ASSET_SCHEDULE_STG S2
       where S2.PAYMENT_MONTH in (1, 2)
         and (CONTINGENT_PAID1 <> 0 or CONTINGENT_PAID2 <> 0 or CONTINGENT_PAID3 <> 0 or
             CONTINGENT_PAID4 <> 0 or CONTINGENT_PAID5 <> 0 or CONTINGENT_PAID6 <> 0 or
             CONTINGENT_PAID7 <> 0 or CONTINGENT_PAID8 <> 0 or CONTINGENT_PAID9 <> 0 or
             CONTINGENT_PAID10 <> 0 or EXECUTORY_PAID1 <> 0 or EXECUTORY_PAID2 <> 0 or
             EXECUTORY_PAID3 <> 0 or EXECUTORY_PAID4 <> 0 or EXECUTORY_PAID5 <> 0 or
             EXECUTORY_PAID6 <> 0 or EXECUTORY_PAID7 <> 0 or EXECUTORY_PAID8 <> 0 or
             EXECUTORY_PAID9 <> 0 or EXECUTORY_PAID10 <> 0);

      if L_CHECK > 0 then
         L_STATUS := 'Insert into ls_ilr_schedule_bucket_stg';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         insert into LS_ILR_SCHEDULE_BUCKET_STG
            select BB.ID,
                   BB.ILR_ID,
                   BB.SET_OF_BOOKS_ID,
                   BB.MONTH,
                   BB.LS_ASSET_ID,
                   BB.C1,
                   BB.C2,
                   BB.C3,
                   BB.C4,
                   BB.C5,
                   BB.C6,
                   BB.C7,
                   BB.C8,
                   BB.C9,
                   BB.C10,
                   BB.E1,
                   BB.E2,
                   BB.E3,
                   BB.E4,
                   BB.E5,
                   BB.E6,
                   BB.E7,
                   BB.E8,
                   BB.E9,
                   BB.E10
              from (with THE_MONTHS as (select LS_ASSET_ID,
                                               REVISION,
                                               SET_OF_BOOKS_ID,
                                               month,
                                               PREPAY_SWITCH,
                                               MONTHS_TO_ACCRUE,
                                               ADD_MONTHS(month,
                                                          case
                                                             when PREPAY_SWITCH = 1 then
                                                              MONTHS_TO_ACCRUE - 1
                                                             else
                                                              -1 * (MONTHS_TO_ACCRUE - 1)
                                                          end) as NEXT_MONTH,
                                               ROW_NUMBER() OVER(partition by LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID order by month desc) as THE_ROW,
                                               S2.CONTINGENT_PAID1 / S2.MONTHS_TO_ACCRUE as C1,
                                               S2.CONTINGENT_PAID2 / S2.MONTHS_TO_ACCRUE as C2,
                                               S2.CONTINGENT_PAID3 / S2.MONTHS_TO_ACCRUE as C3,
                                               S2.CONTINGENT_PAID4 / S2.MONTHS_TO_ACCRUE as C4,
                                               S2.CONTINGENT_PAID5 / S2.MONTHS_TO_ACCRUE as C5,
                                               S2.CONTINGENT_PAID6 / S2.MONTHS_TO_ACCRUE as C6,
                                               S2.CONTINGENT_PAID7 / S2.MONTHS_TO_ACCRUE as C7,
                                               S2.CONTINGENT_PAID8 / S2.MONTHS_TO_ACCRUE as C8,
                                               S2.CONTINGENT_PAID9 / S2.MONTHS_TO_ACCRUE as C9,
                                               S2.CONTINGENT_PAID10 / S2.MONTHS_TO_ACCRUE as C10,
                                               S2.EXECUTORY_PAID1 / S2.MONTHS_TO_ACCRUE as E1,
                                               S2.EXECUTORY_PAID2 / S2.MONTHS_TO_ACCRUE as E2,
                                               S2.EXECUTORY_PAID3 / S2.MONTHS_TO_ACCRUE as E3,
                                               S2.EXECUTORY_PAID4 / S2.MONTHS_TO_ACCRUE as E4,
                                               S2.EXECUTORY_PAID5 / S2.MONTHS_TO_ACCRUE as E5,
                                               S2.EXECUTORY_PAID6 / S2.MONTHS_TO_ACCRUE as E6,
                                               S2.EXECUTORY_PAID7 / S2.MONTHS_TO_ACCRUE as E7,
                                               S2.EXECUTORY_PAID8 / S2.MONTHS_TO_ACCRUE as E8,
                                               S2.EXECUTORY_PAID9 / S2.MONTHS_TO_ACCRUE as E9,
                                               S2.EXECUTORY_PAID10 / S2.MONTHS_TO_ACCRUE as E10
                                          from LS_ILR_ASSET_SCHEDULE_STG S2
                                         where PAYMENT_MONTH in (1, 2))
                      select SS.ID,
                             SS.ILR_ID,
                             SS.SET_OF_BOOKS_ID,
                             SS.MONTH,
                             SS.LS_ASSET_ID,
                             S.C1,
                             S.C2,
                             S.C3,
                             S.C4,
                             S.C5,
                             S.C6,
                             S.C7,
                             S.C8,
                             S.C9,
                             S.C10,
                             S.E1,
                             S.E2,
                             S.E3,
                             S.E4,
                             S.E5,
                             S.E6,
                             S.E7,
                             S.E8,
                             S.E9,
                             S.E10
                        from THE_MONTHS S, LS_ILR_ASSET_SCHEDULE_STG SS
                       where S.LS_ASSET_ID = SS.LS_ASSET_ID
                         and S.REVISION = SS.REVISION
                         and S.SET_OF_BOOKS_ID = SS.SET_OF_BOOKS_ID
                         and case
                                when SS.PREPAY_SWITCH = 1 and SS.MONTHS_TO_ACCRUE > 1 and
                                     S.THE_ROW = 1 then
                                 0
                                when SS.PREPAY_SWITCH = 1 and SS.MONTH between S.MONTH and
                                     S.NEXT_MONTH then
                                 1
                                when SS.PREPAY_SWITCH = 0 and SS.MONTH between S.NEXT_MONTH and
                                     S.MONTH then
                                 1
                                else
                                 0
                             end = 1) BB;


         L_STATUS := 'Setting amounts on ls_ilr_asset_schedule_stg';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         update LS_ILR_ASSET_SCHEDULE_STG SOUT
            set (CONTINGENT_ACCRUAL1,
                  CONTINGENT_ACCRUAL2,
                  CONTINGENT_ACCRUAL3,
                  CONTINGENT_ACCRUAL4,
                  CONTINGENT_ACCRUAL5,
                  CONTINGENT_ACCRUAL6,
                  CONTINGENT_ACCRUAL7,
                  CONTINGENT_ACCRUAL8,
                  CONTINGENT_ACCRUAL9,
                  CONTINGENT_ACCRUAL10,
                  EXECUTORY_ACCRUAL1,
                  EXECUTORY_ACCRUAL2,
                  EXECUTORY_ACCRUAL3,
                  EXECUTORY_ACCRUAL4,
                  EXECUTORY_ACCRUAL5,
                  EXECUTORY_ACCRUAL6,
                  EXECUTORY_ACCRUAL7,
                  EXECUTORY_ACCRUAL8,
                  EXECUTORY_ACCRUAL9,
                  EXECUTORY_ACCRUAL10) =
                 (select BB.C1,
                         BB.C2,
                         BB.C3,
                         BB.C4,
                         BB.C5,
                         BB.C6,
                         BB.C7,
                         BB.C8,
                         BB.C9,
                         BB.C10,
                         BB.E1,
                         BB.E2,
                         BB.E3,
                         BB.E4,
                         BB.E5,
                         BB.E6,
                         BB.E7,
                         BB.E8,
                         BB.E9,
                         BB.E10
                    from LS_ILR_SCHEDULE_BUCKET_STG BB
                   where SOUT.ILR_ID = BB.ILR_ID
                     and SOUT.LS_ASSET_ID = BB.LS_ASSET_ID
                     and SOUT.SET_OF_BOOKS_ID = BB.SET_OF_BOOKS_ID
                     and SOUT.MONTH = BB.MONTH);

         L_STATUS := 'True Up Executory and Contingent Amounts';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         update LS_ILR_ASSET_SCHEDULE_STG SOUT
            set (CONTINGENT_ACCRUAL1,
                  CONTINGENT_ACCRUAL2,
                  CONTINGENT_ACCRUAL3,
                  CONTINGENT_ACCRUAL4,
                  CONTINGENT_ACCRUAL5,
                  CONTINGENT_ACCRUAL6,
                  CONTINGENT_ACCRUAL7,
                  CONTINGENT_ACCRUAL8,
                  CONTINGENT_ACCRUAL9,
                  CONTINGENT_ACCRUAL10,
                  EXECUTORY_ACCRUAL1,
                  EXECUTORY_ACCRUAL2,
                  EXECUTORY_ACCRUAL3,
                  EXECUTORY_ACCRUAL4,
                  EXECUTORY_ACCRUAL5,
                  EXECUTORY_ACCRUAL6,
                  EXECUTORY_ACCRUAL7,
                  EXECUTORY_ACCRUAL8,
                  EXECUTORY_ACCRUAL9,
                  EXECUTORY_ACCRUAL10) =
                 (select BB.C1,
                         BB.C2,
                         BB.C3,
                         BB.C4,
                         BB.C5,
                         BB.C6,
                         BB.C7,
                         BB.C8,
                         BB.C9,
                         BB.C10,
                         BB.E1,
                         BB.E2,
                         BB.E3,
                         BB.E4,
                         BB.E5,
                         BB.E6,
                         BB.E7,
                         BB.E8,
                         BB.E9,
                         BB.E10
                    from (with ILR_PAID as (select s."MONTH",
                                                   S.ILR_ID,
                                                   S.LS_ASSET_ID,
                                                   S.CONTINGENT_ACCRUAL1 as C1,
                                                   S.CONTINGENT_ACCRUAL2 as C2,
                                                   S.CONTINGENT_ACCRUAL3 as C3,
                                                   S.CONTINGENT_ACCRUAL4 as C4,
                                                   S.CONTINGENT_ACCRUAL5 as C5,
                                                   S.CONTINGENT_ACCRUAL6 as C6,
                                                   S.CONTINGENT_ACCRUAL7 as C7,
                                                   S.CONTINGENT_ACCRUAL8 as C8,
                                                   S.CONTINGENT_ACCRUAL9 as C9,
                                                   S.CONTINGENT_ACCRUAL10 as C10,
                                                   S.EXECUTORY_ACCRUAL1 as E1,
                                                   S.EXECUTORY_ACCRUAL2 as E2,
                                                   S.EXECUTORY_ACCRUAL3 as E3,
                                                   S.EXECUTORY_ACCRUAL4 as E4,
                                                   S.EXECUTORY_ACCRUAL5 as E5,
                                                   S.EXECUTORY_ACCRUAL6 as E6,
                                                   S.EXECUTORY_ACCRUAL7 as E7,
                                                   S.EXECUTORY_ACCRUAL8 as E8,
                                                   S.EXECUTORY_ACCRUAL9 as E9,
                                                   S.EXECUTORY_ACCRUAL10 as E10,
                                                   ROW_NUMBER() OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by "MONTH") as THE_NUM,
                                                   S.REVISION,
                                                   S.SET_OF_BOOKS_ID
                                              from LS_ILR_ASSET_SCHEDULE_STG S)
                            select S.ILR_ID,
                                   S.LS_ASSET_ID,
                                   s."MONTH",
                                   S.REVISION,
                                   S.SET_OF_BOOKS_ID,
                                   S.CONTINGENT_ACCRUAL1 + S.CONTINGENT_PAID1 - sum(D.C1) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C1,
                                   S.CONTINGENT_ACCRUAL2 + S.CONTINGENT_PAID2 - sum(D.C2) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C2,
                                   S.CONTINGENT_ACCRUAL3 + S.CONTINGENT_PAID3 - sum(D.C3) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C3,
                                   S.CONTINGENT_ACCRUAL4 + S.CONTINGENT_PAID4 - sum(D.C4) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C4,
                                   S.CONTINGENT_ACCRUAL5 + S.CONTINGENT_PAID5 - sum(D.C5) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C5,
                                   S.CONTINGENT_ACCRUAL6 + S.CONTINGENT_PAID6 - sum(D.C6) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C6,
                                   S.CONTINGENT_ACCRUAL7 + S.CONTINGENT_PAID7 - sum(D.C7) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C7,
                                   S.CONTINGENT_ACCRUAL8 + S.CONTINGENT_PAID8 - sum(D.C8) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C8,
                                   S.CONTINGENT_ACCRUAL9 + S.CONTINGENT_PAID9 - sum(D.C9) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C9,
                                   S.CONTINGENT_ACCRUAL10 + S.CONTINGENT_PAID10 - sum(D.C10) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as C10,
                                   S.EXECUTORY_ACCRUAL1 + S.EXECUTORY_PAID1 - sum(D.E1) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E1,
                                   S.EXECUTORY_ACCRUAL2 + S.EXECUTORY_PAID2 - sum(D.E2) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E2,
                                   S.EXECUTORY_ACCRUAL3 + S.EXECUTORY_PAID3 - sum(D.E3) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E3,
                                   S.EXECUTORY_ACCRUAL4 + S.EXECUTORY_PAID4 - sum(D.E4) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E4,
                                   S.EXECUTORY_ACCRUAL5 + S.EXECUTORY_PAID5 - sum(D.E5) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E5,
                                   S.EXECUTORY_ACCRUAL6 + S.EXECUTORY_PAID6 - sum(D.E6) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E6,
                                   S.EXECUTORY_ACCRUAL7 + S.EXECUTORY_PAID7 - sum(D.E7) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E7,
                                   S.EXECUTORY_ACCRUAL8 + S.EXECUTORY_PAID8 - sum(D.E8) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E8,
                                   S.EXECUTORY_ACCRUAL9 + S.EXECUTORY_PAID9 - sum(D.E9) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E9,
                                   S.EXECUTORY_ACCRUAL10 + S.EXECUTORY_PAID10 - sum(D.E10) OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.LS_ASSET_ID order by D.THE_NUM ROWS between S.MONTHS_TO_ACCRUE - 1 PRECEDING and 0 PRECEDING) as E10
                              from LS_ILR_ASSET_SCHEDULE_STG S, ILR_PAID D
                             where S.ILR_ID = D.ILR_ID
                               and S.LS_ASSET_ID = D.LS_ASSET_ID
                               and S.REVISION = D.REVISION
                               and S.SET_OF_BOOKS_ID = D.SET_OF_BOOKS_ID
                               and s."MONTH" = d."MONTH") BB
                             where SOUT.ILR_ID = BB.ILR_ID
                               and SOUT.LS_ASSET_ID = BB.LS_ASSET_ID
                               and SOUT.REVISION = BB.REVISION
                               and SOUT.SET_OF_BOOKS_ID = BB.SET_OF_BOOKS_ID
                               and sout."MONTH" = bb."MONTH"
                  )
          where SOUT.PAYMENT_MONTH in (1, 2);
      end if;
      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_CONT_EXEC;

   --**************************************************************************
   --                            F_ALLOCATE_TO_ASSETS
   -- This function will allocate the payments to the assets
   -- Spreads the payments based on pct current lease cost
   --**************************************************************************
   function F_ALLOCATE_TO_ASSETS return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting Allocation to Assets';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      -- allocate the NPV of the ILR to the ASSETS underneath
      L_MSG := F_ASSET_ALLOC_NPV;
      if L_MSG = 'OK' then
         -- Allocate the payments to the assets
         PKG_PP_LOG.P_WRITE_MESSAGE('ALLOCATE Payments');
         L_MSG := F_ALLOCATE_ASSET_PAYMENTS;
         if L_MSG = 'OK' then
            -- allocate the executory and contingent paid amounts
            -- to be accrued based on payment frequency
            PKG_PP_LOG.P_WRITE_MESSAGE('ALLOCATE contingents and executory buckets');
            return F_ALLOCATE_CONT_EXEC;
         end if;
      end if;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_TO_ASSETS;


   procedure P_HANDLE_OM
   is
   type MONTHS_ACCRUAL_REC is record
   (
      ID                LS_ILR_ASSET_SCHEDULE_CALC_STG.ID%type,
      ILR_ID            LS_ILR_ASSET_SCHEDULE_CALC_STG.ILR_ID%type,
      LS_ASSET_ID       LS_ILR_ASSET_SCHEDULE_CALC_STG.LS_ASSET_ID%type,
      REVISION          LS_ILR_ASSET_SCHEDULE_CALC_STG.REVISION%type,
      SET_OF_BOOKS_ID   LS_ILR_ASSET_SCHEDULE_CALC_STG.SET_OF_BOOKS_ID%type,
      MONTH             LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTH%type,
      INTEREST_ACCRUAL  LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type,
      MONTHS_TO_ACCRUE  LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTHS_TO_ACCRUE%type,
      PREPAY_SWITCH     LS_ILR_ASSET_SCHEDULE_CALC_STG.PREPAY_SWITCH%type,
      INTEREST_AMT      LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type,
      ROUNDER           LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type
   );

   type MONTHS_ACCRUAL_TAB is table of MONTHS_ACCRUAL_REC index by PLS_INTEGER;

   L_ACCRUAL_MONTHS MONTHS_ACCRUAL_TAB;

   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('Handle Operating Leases');

      -- for O&M records
      -- everything is interest expense not principal (move it).  Also no long term and short term obligations
      update LS_ILR_ASSET_SCHEDULE_CALC_STG
         set BEG_CAPITAL_COST = 0, END_CAPITAL_COST = 0,
             INTEREST_ACCRUAL = INTEREST_ACCRUAL + PRINCIPAL_ACCRUAL,
             INTEREST_PAID = INTEREST_PAID + PRINCIPAL_PAID, PRINCIPAL_ACCRUAL = 0,
             PRINCIPAL_PAID = 0, BEG_LT_OBLIGATION = 0, END_LT_OBLIGATION = 0
       where IS_OM = 1;

      -- spread the interest accrual to be correct
      PKG_PP_LOG.P_WRITE_MESSAGE('Spread Operating Lease Rental Accrual Amounts');

      select *
      bulk collect
      into L_ACCRUAL_MONTHS
      from
      (
         with ACCRUAL_MONTHS as
         (
           select ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID,
            MONTH, INTEREST_ACCRUAL, MONTHS_TO_ACCRUE, PREPAY_SWITCH,
            round(INTEREST_ACCRUAL / MONTHS_TO_ACCRUE, 2) as INTEREST_AMT,
            INTEREST_ACCRUAL - ( MONTHS_TO_ACCRUE * round(INTEREST_ACCRUAL / MONTHS_TO_ACCRUE, 2)) as ROUNDER
           from LS_ILR_ASSET_SCHEDULE_CALC_STG
           where INTEREST_ACCRUAL <> 0
           and MONTHS_TO_ACCRUE <> 1
           and IS_OM = 1
           and MONTH is not null
           and (ILR_ID, REVISION) in (select bb.ILR_ID, bb.REVISION from LS_ILR_ASSET_SCHEDULE_STG bb)
         )
         select a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
            a.MONTH, a.INTEREST_ACCRUAL, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
            am.INTEREST_AMT + case when a.MONTH = am.MONTH then am.ROUNDER else 0 end,
            am.ROUNDER
         from LS_ILR_ASSET_SCHEDULE_CALC_STG a, ACCRUAL_MONTHS am
         --where a.month between am.month and add_months(am.month, am.months_to_accrue - 1)
         where a.month between add_months(am.MONTH, -1 * (am.MONTHS_TO_ACCRUE - 1)) and am.month
         and a.MONTHS_TO_ACCRUE <> 1
         and a.ILR_ID = am.ILR_ID
         and a.LS_ASSET_ID = am.LS_ASSET_ID
         and a.REVISION = am.REVISION
         and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
         and a.IS_OM = 1
         and a.MONTH is not null
         and (a.ILR_ID, a.REVISION) in (select bb.ILR_ID, bb.REVISION from LS_ILR_ASSET_SCHEDULE_STG bb)
      )
      ;

      PKG_PP_LOG.P_WRITE_MESSAGE('Interest Accrual to Respread: '|| to_char(L_ACCRUAL_MONTHS.COUNT));

      forall ndx in indices of L_ACCRUAL_MONTHS
         update LS_ILR_ASSET_SCHEDULE_CALC_STG s
         set s.INTEREST_ACCRUAL = L_ACCRUAL_MONTHS(ndx).INTEREST_AMT
         where s.ID = L_ACCRUAL_MONTHS(ndx).ID
         and s.ILR_ID = L_ACCRUAL_MONTHS(ndx).ILR_ID
         and s.LS_ASSET_ID = L_ACCRUAL_MONTHS(ndx).LS_ASSET_ID
         and s.REVISION = L_ACCRUAL_MONTHS(ndx).REVISION
         and s.SET_OF_BOOKS_ID = L_ACCRUAL_MONTHS(ndx).SET_OF_BOOKS_ID
         ;



   end P_HANDLE_OM;

   --**************************************************************************
   --                            F_CALC_ASSET_SCHEDULE
   -- This function will Build the asset schedule out
   --**************************************************************************
   function F_CALC_ASSET_SCHEDULE return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Undo prior asset schedule calculations';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_SCHEDULE_CALC_STG
       where (ILR_ID, REVISION) in (select A.ILR_ID, A.REVISION from LS_ILR_ASSET_SCHEDULE_STG A);
      L_STATUS := 'Starting to build asset schedule';
      insert into LS_ILR_ASSET_SCHEDULE_CALC_STG S
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT,
          PREPAY_SWITCH, PAYMENT_MONTH, MONTHS_TO_ACCRUE, RATE, NPV, BEG_CAPITAL_COST,
          END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL,
          INTEREST_PAID, PRINCIPAL_PAID, PENNY_ROUNDER, PENNY_PRIN_ROUNDER, PENNY_INT_ROUNDER,
          PENNY_END_ROUNDER, PRINCIPAL_ACCRUED, INTEREST_ACCRUED, PRIN_ROUND_ACCRUED,
          INT_ROUND_ACCRUED, TERM_PENALTY, BPO_PRICE, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, PAYMENT_TERM_TYPE_ID)
         select ID,
                ILR_ID,
                REVISION,
                LS_ASSET_ID,
                SET_OF_BOOKS_ID,
                month,
                AMT,
                RES_AMT,
                PREPAY_SWITCH,
                PAYMENT_MONTH,
                MONTHS_TO_ACCRUE,
                RATE,
                NPV,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                INT_ACCRUAL,
                PRIN_ACCRUAL,
                INT_PAID,
                PRIN_PAID,
                ADD_A_PENNY,
                ADD_A_PENNY_PRIN_ACCRUAL,
                ADD_A_PENNY_INT_ACCRUAL,
                ADD_A_PENNY_END,
                PRIN_ACCRUED,
                INT_ACCRUED,
                PRIN_ACCRUED_ROUND,
                INT_ACCRUED_ROUND,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                IS_OM,
                PAYMENT_TERM_TYPE_ID
           from LS_ILR_ASSET_SCHEDULE_STG MODEL partition by(ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID) DIMENSION by(ROW_NUMBER() OVER(partition by ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID order by month) as ID) MEASURES(BEG_OBLIGATION, AMOUNT as AMT, RESIDUAL_AMOUNT as RES_AMT, PREPAY_SWITCH, PAYMENT_MONTH, MONTHS_TO_ACCRUE, 0 as ADD_A_PENNY, 0 as ADD_A_PENNY_END, 0 as ADD_A_PENNY_PRIN_ACCRUAL, 0 as ADD_A_PENNY_INT_ACCRUAL, RATE, BEG_CAPITAL_COST, AMOUNT as AMOUNT, INTEREST_ACCRUAL as INT_ACCRUAL, 0 as INT_ACCRUED, INTEREST_PAID as INT_PAID, PRINCIPAL_ACCRUAL as PRIN_ACCRUAL, 0 as PRIN_ACCRUED, PRINCIPAL_PAID as PRIN_PAID, END_CAPITAL_COST, END_OBLIGATION, month, NPV, 0 as PRIN_ACCRUED_ROUND, 0 as INT_ACCRUED_ROUND, MONTHS_BETWEEN(max(month) OVER(partition by ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID), month) as MONTH_ID, TERM_PENALTY, BPO_PRICE, BEG_LT_OBLIGATION, END_LT_OBLIGATION, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, IS_OM, PAYMENT_TERM_TYPE_ID, AMOUNT as PRIN_FIXED)
         IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ]
         (
                --
                -- iteration_number + 1 = the current month processing.
                -- iteration_number = prior month
                -- iteration_number - 11 = 12 months prior
                --
                -- if prepaid, the first months amount goes into the period zero accrual
                 PAYMENT_MONTH [ 0 ] = 1,
             PRIN_ACCRUAL [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             PRIN_ACCRUED [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             INT_ACCRUAL [ 0 ] = 0,
             INT_ACCRUED [ 0 ] = 0,
             PRIN_ACCRUED_ROUND [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             INT_ACCRUED_ROUND [ 0 ] = 0,

            -- IFF the current processing month is a paymeny month AND it is prepaid,
            -- then reset the accural amount for last month to be based on current months payment amount (minus last months interest)
            -- ELSE lease prin_accrual for last month alone
            PRIN_ACCRUAL[ ITERATION_NUMBER ] = DECODE(PAYMENT_MONTH [ CV(ID) + 1 ], 0, PRIN_ACCRUAL[ CV(ID) ],
                                          DECODE(PREPAY_SWITCH[CV(ID) + 1], 1, AMOUNT[ CV(ID) + 1 ] - INT_ACCRUAL[CV(ID)], PRIN_ACCRUAL[CV(ID)] ) ),

                -- set the rate to be based on my number of months accrued (to account for monthly interest accrued during periods
                 RATE [ ITERATION_NUMBER + 1 ] = (POWER(1 + RATE [ CV(ID) ], MONTHS_TO_ACCRUE [ CV(ID) ]) - 1) / MONTHS_TO_ACCRUE [ CV(ID) ],
                -- If this is the month after the payment, then accrued amount is equal to 0 if arrears or accrual amount of prior period if prepaid.
                --  All other months are equal to prior accrued amount + prior accrual
                INT_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0, INT_ACCRUED [ CV(ID) - 1 ] + INT_ACCRUAL [ CV(ID) - 1 ],
                                             PREPAY_SWITCH [ CV(ID) ] * INT_ACCRUAL [ CV(ID) - 1 ]),
            PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0, PRIN_ACCRUED [ CV(ID) - 1 ] + PRIN_ACCRUAL [ CV(ID) - 1 ],
                                             PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUAL [ CV(ID) - 1 ]),
            INT_ACCRUED_ROUND [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0,
                                                INT_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(INT_ACCRUAL [ CV(ID) - 1 ], 2),
                                                PREPAY_SWITCH [ CV(ID) ] * ROUND(INT_ACCRUAL [ CV(ID) - 1 ], 2)),
            PRIN_ACCRUED_ROUND [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0,
                                                PRIN_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(PRIN_ACCRUAL [ CV(ID) - 1 ], 2),
                                                PREPAY_SWITCH [ CV(ID) ] * ROUND(PRIN_ACCRUAL [ CV(ID) - 1 ], 2)),

                -- if prepaid, the end balance is used to determine the accrual
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] =
               case when PAYMENT_TERM_TYPE_ID[CV(ID)] = 1 then
                  (last_day(F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION))) - F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION)))/
                   (last_day(F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION))) - last_day(add_months(F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION)), -1)))
                   * RATE [ CV(ID) ]
                   * (BEG_OBLIGATION [ CV(ID) ] - DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUED [ CV(ID) ]))
               else RATE [ CV(ID) ] *
                  (BEG_OBLIGATION [ CV(ID) ] - DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUED [ CV(ID) ]))
               end,
            AMOUNT[ ITERATION_NUMBER + 1] = case
               when F_MAKE_II(CV(ILR_ID),CV(REVISION)) = 1 and PAYMENT_TERM_TYPE_ID[CV(ID)] = 1 then
                  INT_ACCRUAL[ CV(ID) ]
               else
                  AMOUNT[ CV(ID)]
               end,

                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = case
                  when PAYMENT_TERM_TYPE_ID[ CV(ID) ] = 3 then
                    PRIN_FIXED[ CV(ID) ]
                  else
               -- if this is a payment month... and it is prepaid... then use just negative interest.
               DECODE(PAYMENT_MONTH [CV(ID)], 0, AMOUNT [CV(ID)],
                     DECODE(PREPAY_SWITCH[CV(ID)], 1, 0, AMOUNT [CV(ID)]) )
               - INT_ACCRUAL [CV(ID)]
                  end,
                -- for prepaid, the accrual from the prior period is used for the payment amount
                -- for prin, the first period will include the full first payment amount
                 INT_PAID [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0,(1 - PREPAY_SWITCH [ CV(ID) ]) * INT_ACCRUAL [ CV(ID) ] + INT_ACCRUED [ CV(ID) ]), PRIN_PAID [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0,(1 - PREPAY_SWITCH [ CV(ID) ]) * PRIN_ACCRUAL [ CV(ID) ] + PRIN_ACCRUED [ CV(ID) ]),

                -- after setting the paid amount, set the accrued amounts to be zero
                 PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, PRIN_ACCRUED [ CV(ID) ], 0), INT_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, INT_ACCRUED [ CV(ID) ], 0),

                -- TRUEUP rounding that may come into play around accrued balanced <> paid balances
                -- take the prior months accrued rounding.  And add the accrual (if arrears).  Else do not add anything
                 ADD_A_PENNY_PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, DECODE(CV(ID), 1, 0, ROUND(PRIN_PAID [ CV(ID) ], 2) - PRIN_ACCRUED_ROUND [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ]) * ROUND(PRIN_ACCRUAL [ CV(ID) ], 2))), ADD_A_PENNY_INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, DECODE(CV(ID), 1, 0, ROUND(INT_PAID [ CV(ID) ], 2) - INT_ACCRUED_ROUND [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ]) * ROUND(INT_ACCRUAL [ CV(ID) ], 2))),

                -- trueup principal accrual for rounding between periods for arrears
                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) ]),

                -- prepaids update prior month accrual.  And current month accrued
                 PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) + 1 ]), PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) + 1 ]), INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) ]),

                -- end capital cost and obligation
                 END_CAPITAL_COST [ ITERATION_NUMBER + 1 ] = BEG_CAPITAL_COST [ CV(ID) ], END_OBLIGATION [ ITERATION_NUMBER + 1 ] = BEG_OBLIGATION [ CV(ID) ] - PRIN_PAID [ CV(ID) ],

                -- check if a penny needs to be added to principal and subtracted from interest
                -- if the last month.  Make sure end obligation is zero
                 ADD_A_PENNY [ ITERATION_NUMBER + 1 ] = ROUND(BEG_OBLIGATION [ CV(ID) ], 2) - ROUND(PRIN_PAID [ CV(ID) ], 2) - ROUND(END_OBLIGATION [ CV(ID) ], 2),

                -- apply from current month accrual if arrears
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] - ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY [ CV(ID) ]), PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY [ CV(ID) ]),

                -- for prepaid, apply to prior period accrual and current month accrued
                 INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) + 1 ]), PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) + 1 ]), INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) ]), PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) ]),

                -- Apply to current period payment
                 INT_PAID [ ITERATION_NUMBER + 1 ] = INT_PAID [ CV(ID) ] - ADD_A_PENNY [ CV(ID) ], PRIN_PAID [ ITERATION_NUMBER + 1 ] = PRIN_PAID [ CV(ID) ] + ADD_A_PENNY [ CV(ID) ],

                -- in the month where end obligation should go to zero (payment_month = 2).
                -- MAKE sure this happens by applying rounding
                -- check beginning balance minus payment minus res - bpo - termination
                 ADD_A_PENNY_END [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, ROUND(BEG_OBLIGATION [ CV(ID) ], 2) - ROUND(PRIN_PAID [ CV(ID) ], 2) - RES_AMT [ CV(ID) ] - BPO_PRICE [ CV(ID) ] - TERM_PENALTY [ CV(ID) ], 0),

                -- if arrears, then odify accrual for current month
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] - ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_END [ CV(ID) ]), PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_END [ CV(ID) ]),

                -- for prepaid, apply to prior period accrual and current month accrued
                 INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) + 1 ]), PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) + 1 ]), INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) ]), PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) ]),

                -- Apply to current period payment
                 INT_PAID [ ITERATION_NUMBER + 1 ] = INT_PAID [ CV(ID) ] - ADD_A_PENNY_END [ CV(ID) ], PRIN_PAID [ ITERATION_NUMBER + 1 ] = PRIN_PAID [ CV(ID) ] + ADD_A_PENNY_END [ CV(ID) ],

                -- set end obligation to be residual + bpo + termination if last month.
                 END_OBLIGATION [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, RES_AMT [ CV(ID) ] + BPO_PRICE [ CV(ID) ] + TERM_PENALTY [ CV(ID) ], END_OBLIGATION [ CV(ID) ]),

                 BEG_OBLIGATION [ ITERATION_NUMBER + 2 ] = END_OBLIGATION [ CV(ID) - 1 ],

                -- if last payment month and it is a prepaid, then set the accrual to be the prepaid payment amount (stored in prin_accrued for period 0)
                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, DECODE(PREPAY_SWITCH [ CV(ID) ], 1, PRIN_ACCRUED [ 0 ], PRIN_ACCRUAL [ CV(ID) ]), PRIN_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, DECODE(PREPAY_SWITCH [ CV(ID) ], 1, 0, INT_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ CV(ID) ]),

                -- end lt obligation is equal to the end obligation in 12 months
                 BEG_LT_OBLIGATION [ ITERATION_NUMBER + 1 ] = 0, END_LT_OBLIGATION [ ITERATION_NUMBER + 1 ] = 0, BEG_LT_OBLIGATION [ ITERATION_NUMBER - 11 ] = BEG_OBLIGATION [ ITERATION_NUMBER + 1 ], END_LT_OBLIGATION [ ITERATION_NUMBER - 11 ] = END_OBLIGATION [ ITERATION_NUMBER + 1 ]);

      P_HANDLE_OM;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_CALC_ASSET_SCHEDULE;

   --**************************************************************************
   --                            F_LOAD_ASSET_SCHEDULE
   -- This function will Build the asset schedule out
   --**************************************************************************
   function F_LOAD_ASSET_SCHEDULE return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting to load asset schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      merge into LS_ILR_ASSET_SCHEDULE_STG S
      using (select * from LS_ILR_ASSET_SCHEDULE_CALC_STG) B
      on (B.ILR_ID = S.ILR_ID and B.LS_ASSET_ID = S.LS_ASSET_ID and B.REVISION = S.REVISION and B.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID and B.MONTH = S.MONTH)
      when matched then
         update
            set S.BEG_CAPITAL_COST = ROUND(B.BEG_CAPITAL_COST, 2),
                S.END_CAPITAL_COST = ROUND(B.END_CAPITAL_COST, 2),
                S.BEG_OBLIGATION = ROUND(B.BEG_OBLIGATION, 2),
                S.END_OBLIGATION = ROUND(B.END_OBLIGATION, 2),
                S.BEG_LT_OBLIGATION = ROUND(B.BEG_LT_OBLIGATION, 2),
                S.END_LT_OBLIGATION = ROUND(B.END_LT_OBLIGATION, 2),
                S.INTEREST_ACCRUAL = ROUND(B.INTEREST_ACCRUAL, 2),
                S.PRINCIPAL_ACCRUAL = ROUND(B.PRINCIPAL_ACCRUAL, 2),
                S.INTEREST_PAID = ROUND(B.INTEREST_PAID, 2),
                S.PRINCIPAL_PAID = ROUND(B.PRINCIPAL_PAID, 2),
                S.CONTINGENT_PAID1 = ROUND(B.CONTINGENT_PAID1, 2),
                S.CONTINGENT_PAID2 = ROUND(B.CONTINGENT_PAID2, 2),
                S.CONTINGENT_PAID3 = ROUND(B.CONTINGENT_PAID3, 2),
                S.CONTINGENT_PAID4 = ROUND(B.CONTINGENT_PAID4, 2),
                S.CONTINGENT_PAID5 = ROUND(B.CONTINGENT_PAID5, 2),
                S.CONTINGENT_PAID6 = ROUND(B.CONTINGENT_PAID6, 2),
                S.CONTINGENT_PAID7 = ROUND(B.CONTINGENT_PAID7, 2),
                S.CONTINGENT_PAID8 = ROUND(B.CONTINGENT_PAID8, 2),
                S.CONTINGENT_PAID9 = ROUND(B.CONTINGENT_PAID9, 2),
                S.CONTINGENT_PAID10 = ROUND(B.CONTINGENT_PAID10, 2),
                S.EXECUTORY_PAID1 = ROUND(B.EXECUTORY_PAID1, 2),
                S.EXECUTORY_PAID2 = ROUND(B.EXECUTORY_PAID2, 2),
                S.EXECUTORY_PAID3 = ROUND(B.EXECUTORY_PAID3, 2),
                S.EXECUTORY_PAID4 = ROUND(B.EXECUTORY_PAID4, 2),
                S.EXECUTORY_PAID5 = ROUND(B.EXECUTORY_PAID5, 2),
                S.EXECUTORY_PAID6 = ROUND(B.EXECUTORY_PAID6, 2),
                S.EXECUTORY_PAID7 = ROUND(B.EXECUTORY_PAID7, 2),
                S.EXECUTORY_PAID8 = ROUND(B.EXECUTORY_PAID8, 2),
                S.EXECUTORY_PAID9 = ROUND(B.EXECUTORY_PAID9, 2),
                S.EXECUTORY_PAID10 = ROUND(B.EXECUTORY_PAID10, 2),
                S.CONTINGENT_ACCRUAL1 = ROUND(B.CONTINGENT_ACCRUAL1, 2),
                S.CONTINGENT_ACCRUAL2 = ROUND(B.CONTINGENT_ACCRUAL2, 2),
                S.CONTINGENT_ACCRUAL3 = ROUND(B.CONTINGENT_ACCRUAL3, 2),
                S.CONTINGENT_ACCRUAL4 = ROUND(B.CONTINGENT_ACCRUAL4, 2),
                S.CONTINGENT_ACCRUAL5 = ROUND(B.CONTINGENT_ACCRUAL5, 2),
                S.CONTINGENT_ACCRUAL6 = ROUND(B.CONTINGENT_ACCRUAL6, 2),
                S.CONTINGENT_ACCRUAL7 = ROUND(B.CONTINGENT_ACCRUAL7, 2),
                S.CONTINGENT_ACCRUAL8 = ROUND(B.CONTINGENT_ACCRUAL8, 2),
                S.CONTINGENT_ACCRUAL9 = ROUND(B.CONTINGENT_ACCRUAL9, 2),
                S.CONTINGENT_ACCRUAL10 = ROUND(B.CONTINGENT_ACCRUAL10, 2),
                S.EXECUTORY_ACCRUAL1 = ROUND(B.EXECUTORY_ACCRUAL1, 2),
                S.EXECUTORY_ACCRUAL2 = ROUND(B.EXECUTORY_ACCRUAL2, 2),
                S.EXECUTORY_ACCRUAL3 = ROUND(B.EXECUTORY_ACCRUAL3, 2),
                S.EXECUTORY_ACCRUAL4 = ROUND(B.EXECUTORY_ACCRUAL4, 2),
                S.EXECUTORY_ACCRUAL5 = ROUND(B.EXECUTORY_ACCRUAL5, 2),
                S.EXECUTORY_ACCRUAL6 = ROUND(B.EXECUTORY_ACCRUAL6, 2),
                S.EXECUTORY_ACCRUAL7 = ROUND(B.EXECUTORY_ACCRUAL7, 2),
                S.EXECUTORY_ACCRUAL8 = ROUND(B.EXECUTORY_ACCRUAL8, 2),
                S.EXECUTORY_ACCRUAL9 = ROUND(B.EXECUTORY_ACCRUAL9, 2),
                S.EXECUTORY_ACCRUAL10 = ROUND(B.EXECUTORY_ACCRUAL10, 2);

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_LOAD_ASSET_SCHEDULE;

   --**************************************************************************
   --                            F_PROCESS_ASSETS
   -- This function will process the assets under the ILRS
   -- IT will allocate the payments to the assets
   -- then calculate NBV for the asset
   --**************************************************************************
   function F_PROCESS_ASSETS return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('PROCESSING Assets');

      L_STATUS := 'CALLING F_ALLOCATE_TO_ASSETS';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_ALLOCATE_TO_ASSETS;
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_ASSET_SCHEDULE';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_ASSET_SCHEDULE;
         if L_MSG = 'OK' then
            L_STATUS := 'CALLING F_LOAD_ASSET_SCHEDULE';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            L_MSG := F_LOAD_ASSET_SCHEDULE;
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

            return L_MSG;
         end if;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ASSETS;

   --**************************************************************************
   --                            F_CALC_IRR
   -- This function will calculate the internal rate of return
   -- For ILRs where NPV > FMV (net present value is greater than fair market value)
   --**************************************************************************
   function F_CALC_IRR return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting IRR Calculation';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      merge into LS_ILR_STG S
      using (select ILR_ID, REVISION, SET_OF_BOOKS_ID, 1 / X - 1 as THE_IRR
               from (select *
                       from (select ILR_ID, REVISION, SET_OF_BOOKS_ID, month, sum(AMOUNT) as AMOUNT
                               from (select L.ILR_ID,
                                            S.REVISION,
                                            S.SET_OF_BOOKS_ID,
                                            S.MONTH,
                                            S.AMOUNT + S.RESIDUAL_AMOUNT + S.BPO_PRICE + S.TERM_PENALTY as AMOUNT
                                       from LS_ILR_SCHEDULE_STG S, LS_ILR_STG L
                                      where L.ILR_ID = S.ILR_ID
                                        and L.REVISION = S.REVISION
                                        and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                                        and L.NPV > L.FMV
                                        and L.IS_OM = 0
                                     union all
                                     select L.ILR_ID,
                                            S.REVISION,
                                            S.SET_OF_BOOKS_ID,
                                            min(ADD_MONTHS(S.MONTH, L.PREPAY_SWITCH - 1)),
                                            -1 * L.FMV
                                       from LS_ILR_SCHEDULE_STG S, LS_ILR_STG L
                                      where L.ILR_ID = S.ILR_ID
                                        and L.REVISION = S.REVISION
                                        and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                                        and L.NPV > L.FMV
                                        and L.IS_OM = 0
                                      group by L.ILR_ID, L.FMV, S.REVISION, S.SET_OF_BOOKS_ID)
                              group by ILR_ID, month, REVISION, SET_OF_BOOKS_ID
                              order by 1, 2) MODEL partition by(ILR_ID, REVISION, SET_OF_BOOKS_ID) DIMENSION by(ROW_NUMBER() OVER(partition by ILR_ID, REVISION, SET_OF_BOOKS_ID order by month) as THE_ROW) MEASURES(MONTHS_BETWEEN(month, FIRST_VALUE(month) OVER(partition by ILR_ID, REVISION, SET_OF_BOOKS_ID order by month)) as month, AMOUNT S, 0 SS, 0 F_A, 0 F_B, 0 F_X, 0 A, 1 B, 0 X, 0 ITER) RULES ITERATE(10000) UNTIL(ABS(F_X [ 1 ]) < POWER(10, -20))(SS [ any ] = S [ CV() ] * POWER(A [ 1 ], month [ CV() ]), F_A [ 1 ] = sum(SS) [ any ], SS [ any ] = S [ CV() ] * POWER(B [ 1 ], month [ CV() ]), F_B [ 1 ] = sum(SS) [ any ], X [ 1 ] = A [ 1 ] - F_A [ 1 ] * (B [ 1 ] - A [ 1 ]) / (F_B [ 1 ] - F_A [ 1 ]), SS [ any ] = S [ CV() ] * POWER(X [ 1 ], month [ CV() ]), F_X [ 1 ] = sum(SS) [ any ], A [ 1 ] = DECODE(SIGN(F_A [ 1 ] * F_X [ 1 ]), 1, X [ 1 ], A [ 1 ]), B [ 1 ] = DECODE(SIGN(F_A [ 1 ] * F_X [ 1 ]), 1, B [ 1 ], X [ 1 ]), ITER [ 1 ] = ITERATION_NUMBER + 1)) B
              where B.THE_ROW = 1
                and B.X <> 0) I
      on (I.ILR_ID = S.ILR_ID and I.REVISION = S.REVISION and I.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID)
      when matched then
         update set S.IRR = I.THE_IRR;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_CALC_IRR;

   --**************************************************************************
   --                            F_NET_PRESENT_VALUE
   -- Determine the NET PRESENT VALUE of the future cash flow
   -- Based on the rate (either the discount rate entered or the IRR)
   --**************************************************************************
   function F_NET_PRESENT_VALUE return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting NBV by payment';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ILR_SCHEDULE_STG S
         set NPV =
              (select B.NPV
                 from (select ID, ILR_ID, REVISION, SET_OF_BOOKS_ID, month, NPV, PAYMENT_TERM_TYPE_ID
                         from LS_ILR_SCHEDULE_STG
                        where PROCESS_NPV = 1 MODEL partition by(ILR_ID, REVISION, SET_OF_BOOKS_ID)
                        DIMENSION
                        by(ROW_NUMBER()
                                 OVER(partition by ILR_ID, REVISION, SET_OF_BOOKS_ID order by month) as ID)
                        MEASURES(AMOUNT,
                                       0 NPV,
                                       RATE,
                                       month,
                                       PREPAY_SWITCH,
                                       RESIDUAL_AMOUNT,
                                       BPO_PRICE,
                                       TERM_PENALTY,
                                       PAYMENT_TERM_TYPE_ID)
                        RULES(NPV [ any ] = case
                                    when PAYMENT_TERM_TYPE_ID[CV(ID)] = 3 then
                                      (AMOUNT [ CV(ID) ] + RESIDUAL_AMOUNT [ CV(ID) ] + BPO_PRICE [
                                      CV(ID) ] + TERM_PENALTY [ CV(ID) ])
                                    else
                                      (AMOUNT [ CV(ID) ] + RESIDUAL_AMOUNT [ CV(ID) ] + BPO_PRICE [
                                      CV(ID) ] + TERM_PENALTY [ CV(ID) ]) /
                                      POWER(1 + RATE [ CV(ID) ], CV(ID) - PREPAY_SWITCH [ CV(ID) ])
                                    end
                             )
                      ) B
                where B.ILR_ID = S.ILR_ID
                  and B.REVISION = S.REVISION
                  and B.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                  and B.MONTH = S.MONTH)
       where PROCESS_NPV = 1;

      L_STATUS := 'Updating NBV for ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ILR_STG S
         set NPV =
              (select ROUND(sum(B.NPV), 2)
                 from LS_ILR_SCHEDULE_STG B
                where B.ILR_ID = S.ILR_ID
                  and B.REVISION = S.REVISION
                  and B.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID)
       where PROCESS_NPV = 1;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_NET_PRESENT_VALUE;

   --**************************************************************************
   --                            F_LOAD_ILR_STG
   -- Loads the ILR_STG table for a single ILR ID
   --**************************************************************************
   function F_LOAD_ILR_STG(A_ILR_ID   number,
                           A_REVISION number) return varchar2 is
      L_MSG    varchar2(2000);
      L_SQLS   varchar2(2000);
      L_IS_CAP number(1, 0);

   begin
      for L_SOBS in (select L.ILR_ID as ILR_ID,
                            LO.REVISION as REVISION,
                            LL.PRE_PAYMENT_SW as PREPAY_SWITCH,
                            LO.INCEPTION_AIR / 1200 as DISCOUNT_RATE,
                            sum(NVL(LA.FMV, 0)) as FMV,
                            sum(NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0)) as RESIDUAL_AMOUNT,
                            NVL(LO.PURCHASE_OPTION_AMT, 0) as BPO_PRICE,
                            NVL(LO.TERMINATION_AMT, 0) as TERM_PENALTY,
                            C.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                            LLCT.BOOK_SUMMARY_ID as BOOK_SUMMARY_ID
                       from LS_ILR               L,
                            LS_LEASE             LL,
                            LS_ASSET             LA,
                            LS_ILR_OPTIONS       LO,
                            COMPANY_SET_OF_BOOKS C,
                            LS_LEASE_CAP_TYPE    LLCT,
                            LS_ILR_ASSET_MAP M
                      where L.LEASE_ID = LL.LEASE_ID
                        and M.LS_ASSET_ID = LA.LS_ASSET_ID
                        and M.ILR_ID = A_ILR_ID
                        and M.REVISION = A_REVISION
                        and L.ILR_ID = LA.ILR_ID(+)
                        and LA.LS_ASSET_STATUS_ID <> 4
                        and L.ILR_ID = A_ILR_ID
                        and LO.ILR_ID = L.ILR_ID
                        and LO.REVISION = A_REVISION
                        and L.COMPANY_ID = C.COMPANY_ID
                        and LO.LEASE_CAP_TYPE_ID = LLCT.LS_LEASE_CAP_TYPE_ID
                      group by L.ILR_ID,
                               LL.PRE_PAYMENT_SW,
                               LO.INCEPTION_AIR,
                               LO.PURCHASE_OPTION_AMT,
                               LO.TERMINATION_AMT,
                               LO.REVISION,
                               C.SET_OF_BOOKS_ID,
                               LLCT.BOOK_SUMMARY_ID)
      loop
         L_MSG := 'PROCESSING set_of_books: ' || L_SOBS.SET_OF_BOOKS_ID;

         L_SQLS   := 'select basis_' || TO_CHAR(L_SOBS.BOOK_SUMMARY_ID) ||
                     '_indicator from set_of_books s where s.set_of_books_id = ' ||
                     TO_CHAR(L_SOBS.SET_OF_BOOKS_ID);
         L_IS_CAP := TO_NUMBER(PP_MISC_PKG.DYNAMIC_SELECT(L_SQLS));

         insert into LS_ILR_STG
            (ILR_ID, REVISION, PREPAY_SWITCH, DISCOUNT_RATE, FMV, RESIDUAL_AMOUNT, PROCESS_NPV,
             BPO_PRICE, TERM_PENALTY, SET_OF_BOOKS_ID, IS_OM)
         values
            (L_SOBS.ILR_ID, L_SOBS.REVISION, L_SOBS.PREPAY_SWITCH, L_IS_CAP * nvl(L_SOBS.DISCOUNT_RATE,0),
             L_SOBS.FMV, L_SOBS.RESIDUAL_AMOUNT, 1, L_SOBS.BPO_PRICE, L_SOBS.TERM_PENALTY,
             L_SOBS.SET_OF_BOOKS_ID, 1 - L_IS_CAP);
      end loop;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILR_STG;


   --**************************************************************************
   --                            F_LOAD_ILRS_STG
   -- Loads the ILR_STG table for a single LEASE ID
   --**************************************************************************
   function F_LOAD_ILRS_STG(A_LEASE_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_SQLS   varchar2(2000);
      L_IS_CAP number(1, 0);

   begin
      for L_SOBS in (select L.ILR_ID as ILR_ID,
                            LO.REVISION as REVISION,
                            LL.PRE_PAYMENT_SW as PREPAY_SWITCH,
                            LO.INCEPTION_AIR / 1200 as DISCOUNT_RATE,
                            sum(NVL(LA.FMV, 0)) as FMV,
                            sum(NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0)) as RESIDUAL_AMOUNT,
                            NVL(LO.PURCHASE_OPTION_AMT, 0) as BPO_PRICE,
                            NVL(LO.TERMINATION_AMT, 0) as TERM_PENALTY,
                            C.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                            LLCT.BOOK_SUMMARY_ID as BOOK_SUMMARY_ID
                       from LS_ILR               L,
                            LS_LEASE             LL,
                            LS_ASSET             LA,
                            LS_ILR_OPTIONS       LO,
                            COMPANY_SET_OF_BOOKS C,
                            LS_LEASE_CAP_TYPE    LLCT
                      where L.LEASE_ID = LL.LEASE_ID
                        and L.ILR_ID = LA.ILR_ID(+)
                        and LA.LS_ASSET_STATUS_ID <> 4
                        and LL.LEASE_ID = A_LEASE_ID
                        and LO.ILR_ID = L.ILR_ID
                        and LO.REVISION = L.CURRENT_REVISION
                        and L.COMPANY_ID = C.COMPANY_ID
                        and LO.LEASE_CAP_TYPE_ID = LLCT.LS_LEASE_CAP_TYPE_ID
                      group by L.ILR_ID,
                               LL.PRE_PAYMENT_SW,
                               LO.INCEPTION_AIR,
                               LO.PURCHASE_OPTION_AMT,
                               LO.TERMINATION_AMT,
                               LO.REVISION,
                               C.SET_OF_BOOKS_ID,
                               LLCT.BOOK_SUMMARY_ID)
      loop
         L_SQLS   := 'select basis_' || TO_CHAR(L_SOBS.BOOK_SUMMARY_ID) ||
                     '_indicator from set_of_books s where s.set_of_books_id = ' ||
                     TO_CHAR(L_SOBS.SET_OF_BOOKS_ID);
         L_IS_CAP := TO_NUMBER(PP_MISC_PKG.DYNAMIC_SELECT(L_SQLS));

         insert into LS_ILR_STG
            (ILR_ID, REVISION, PREPAY_SWITCH, DISCOUNT_RATE, FMV, RESIDUAL_AMOUNT, PROCESS_NPV,
             BPO_PRICE, TERM_PENALTY, SET_OF_BOOKS_ID, IS_OM)
         values
            (L_SOBS.ILR_ID, L_SOBS.REVISION, L_SOBS.PREPAY_SWITCH, L_IS_CAP * L_SOBS.DISCOUNT_RATE,
             L_SOBS.FMV, L_SOBS.RESIDUAL_AMOUNT, 1, L_SOBS.BPO_PRICE, L_SOBS.TERM_PENALTY,
             L_SOBS.SET_OF_BOOKS_ID, 1 - L_IS_CAP);
      end loop;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILRS_STG;

   --**************************************************************************
   --                            F_LOAD_ILR_SCHEDULE_STG
   -- Builds out the cash flow based on payment terms
   -- Take into consideration whether or not the payment is
   -- Paid at the beginning of the period or at the end of the period
   --**************************************************************************
   function F_LOAD_ILR_SCHEDULE_STG return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Loading ls_ilr_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_SCHEDULE_STG
         (ID, ILR_ID, REVISION, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT, RATE,
          PREPAY_SWITCH, PROCESS_NPV, PAYMENT_MONTH, MONTHS_TO_ACCRUE, BPO_PRICE, TERM_PENALTY,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, PAYMENT_TERM_TYPE_ID)
         with N as
          (
           -- create a "table" with 10000 rows
           select ROWNUM as THE_ROW from DUAL connect by level <= 10000),
         LP as
          (
           -- find the payment_term_id with the latest payment_term_date
           select L2.ILR_ID,
                   L2.PAYMENT_TERM_ID,
                   L2.REVISION,
                   L1.SET_OF_BOOKS_ID,
                   ROW_NUMBER() OVER(partition by L2.ILR_ID, L1.SET_OF_BOOKS_ID, L2.REVISION order by L2.PAYMENT_TERM_DATE desc) as THE_MAX
             from LS_ILR_PAYMENT_TERM L2, LS_ILR_STG L1
            where L1.ILR_ID = L2.ILR_ID
              and L1.REVISION = L2.REVISION)
         select ROW_NUMBER() OVER(partition by P.ILR_ID, L.REVISION, L.SET_OF_BOOKS_ID order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)),
                P.ILR_ID,
                L.REVISION,
                L.SET_OF_BOOKS_ID,
                ADD_MONTHS(P.PAYMENT_TERM_DATE, THE_ROW - 1),
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    P.PAID_AMOUNT
                   else
                    0
                end as PAID_AMOUNT,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.RESIDUAL_AMOUNT
                   else
                    0
                end as RESIDUAL_AMOUNT,
                L.DISCOUNT_RATE,
                L.PREPAY_SWITCH,
                1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    1
                   else
                    0
                end as PAYMENT_MONTH,
                DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) as MONTHS_TO_ACCRUE,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.BPO_PRICE
                   else
                    0
                end as BPO_PRICE,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.TERM_PENALTY
                   else
                    0
                end as TERM_PENALTY,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_1, 0)
                   else
                    0
                end as CONTINGENT_PAID1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_2, 0)
                   else
                    0
                end as CONTINGENT_PAID2,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_3, 0)
                   else
                    0
                end as CONTINGENT_PAID3,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_4, 0)
                   else
                    0
                end as CONTINGENT_PAID4,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_5, 0)
                   else
                    0
                end as CONTINGENT_PAID5,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_6, 0)
                   else
                    0
                end as CONTINGENT_PAID6,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_7, 0)
                   else
                    0
                end as CONTINGENT_PAID7,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_8, 0)
                   else
                    0
                end as CONTINGENT_PAID8,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_9, 0)
                   else
                    0
                end as CONTINGENT_PAID9,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_10, 0)
                   else
                    0
                end as CONTINGENT_PAID10,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_1, 0)
                   else
                    0
                end as EXECUTORY_PAID1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_2, 0)
                   else
                    0
                end as EXECUTORY_PAID2,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_3, 0)
                   else
                    0
                end as EXECUTORY_PAID3,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_4, 0)
                   else
                    0
                end as EXECUTORY_PAID4,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_5, 0)
                   else
                    0
                end as EXECUTORY_PAID5,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_6, 0)
                   else
                    0
                end as EXECUTORY_PAID6,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_7, 0)
                   else
                    0
                end as EXECUTORY_PAID7,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_8, 0)
                   else
                    0
                end as EXECUTORY_PAID8,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_9, 0)
                   else
                    0
                end as EXECUTORY_PAID9,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_10, 0)
                   else
                    0
                end as EXECUTORY_PAID10,
                L.IS_OM,
                P.PAYMENT_TERM_TYPE_ID
           from LS_ILR_PAYMENT_TERM P, LS_ILR_STG L, LP, N
          where P.ILR_ID = L.ILR_ID
            and P.REVISION = L.REVISION
            and LP.ILR_ID = L.ILR_ID
            and LP.REVISION = L.REVISION
            and LP.THE_MAX = 1
            and LP.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and N.THE_ROW <= P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
          order by 2;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILR_SCHEDULE_STG;

   --**************************************************************************
   --                            F_CALC_SCHEDULES
   --**************************************************************************
   function F_CALC_SCHEDULES return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'CALLING F_LOAD_ILR_SCHEDULE_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILR_SCHEDULE_STG;
      if L_MSG = 'OK' then
         -- Determine the Net Present Value based on the
         -- entered discount rate and the cash flow
         L_STATUS := 'CALLING F_NET_PRESENT_VALUE';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_NET_PRESENT_VALUE;
         if L_MSG = 'OK' then
            -- Call the function to calculate internal rate of return
            -- the function will only update ILRs where NPV > FMV
            L_STATUS := 'CALLING F_CALC_IRR';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            L_MSG := F_CALC_IRR;
            if L_MSG = 'OK' then
               L_STATUS := 'DO NOT REPROCESS NPV FOR SOME ILRs';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
               P_CHECK_RECALC_NPV;

               L_STATUS := 'CALLING F_NET_PRESENT_VALUE AFTER IRR';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
               L_MSG := F_NET_PRESENT_VALUE;
               return L_MSG;
            end if;
         end if;
      end if;

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_CALC_SCHEDULES;

   --**************************************************************************
   --                            F_PROCESS_ILR
   --**************************************************************************
   function F_PROCESS_ILR(A_ILR_ID   number,
                          A_REVISION number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING ilr_id: ' || TO_CHAR(A_ILR_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING revision: ' || TO_CHAR(A_REVISION));

      L_STATUS := 'CALLING F_LOAD_ILR_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILR_STG(A_ILR_ID, A_REVISION);
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILR;

   --**************************************************************************
   --                            F_PROCESS_ILRS
   --**************************************************************************
   function F_PROCESS_ILRS(A_LEASE_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING lease_id: ' || TO_CHAR(A_LEASE_ID));

      L_STATUS := 'CALLING F_LOAD_ILRS_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILRS_STG(A_LEASE_ID);
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILRS;

   --**************************************************************************
   --                            F_MASS_PROCESS_ILRS
   --**************************************************************************
   function F_MASS_PROCESS_ILRS(A_ILR_IDS   PKG_LEASE_IMPORT.NUM_ARRAY,
                                A_REVISIONS PKG_LEASE_IMPORT.NUM_ARRAY) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

      for I in 1 .. A_ILR_IDS.COUNT
      loop
         PKG_PP_LOG.P_WRITE_MESSAGE('STARTING ILR: ' || TO_CHAR(A_ILR_IDS(I))||' REVISION:'||TO_CHAR(A_REVISIONS(I)));
         L_STATUS := 'CALLING F_LOAD_ILRS_STG';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_LOAD_ILR_STG(A_ILR_IDS(I), A_REVISIONS(I));
         if L_MSG <> 'OK' then
            return L_MSG;
         end if;
      end loop;
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_MASS_PROCESS_ILRS;

   --**************************************************************************
   --                            F_PROCESS_ILRS_FCST
   --**************************************************************************
   function F_PROCESS_ILRS_FCST(A_CAP_TYPE number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING ILR FORECAST');

      -- Delete anything related to revision 9999's
      delete from LS_ASSET_SCHEDULE where REVISION = 9999;
      delete from LS_ILR_SCHEDULE where REVISION = 9999;
      delete from LS_ILR_PAYMENT_TERM where REVISION = 9999;
      delete from LS_ILR_ASSET_MAP where REVISION = 9999;
      delete from LS_ILR_OPTIONS where REVISION = 9999;
      delete from LS_ILR_APPROVAL where REVISION = 9999;

      -- Create revision 10,000's for all In-Service ILR's
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select ILR_ID, 9999, 4, 1
           from LS_ILR_APPROVAL
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_APPROVAL.ILR_ID);

      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT, PAYMENT_SHIFT)
         select ILR_ID,
                9999,
                PURCHASE_OPTION_TYPE_ID,
                PURCHASE_OPTION_AMT,
                RENEWAL_OPTION_TYPE_ID,
                CANCELABLE_TYPE_ID,
                ITC_SW,
                PARTIAL_RETIRE_SW,
                SUBLET_SW,
                MUNI_BO_SW,
                INCEPTION_AIR,
                A_CAP_TYPE,
                TERMINATION_AMT,
                PAYMENT_SHIFT
           from LS_ILR_OPTIONS
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_OPTIONS.ILR_ID);

      insert into LS_ILR_ASSET_MAP
         (ILR_ID, LS_ASSET_ID, REVISION)
         select ILR_ID, LS_ASSET_ID, 9999
           from LS_ILR_ASSET_MAP
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_ASSET_MAP.ILR_ID);

      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE, PAYMENT_FREQ_ID,
          NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT, CURRENCY_TYPE_ID,
          C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6, C_BUCKET_7,
          C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3, E_BUCKET_4,
          E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10, REVISION)
         select ILR_ID,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10,
                9999
           from LS_ILR_PAYMENT_TERM
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_PAYMENT_TERM.ILR_ID);

      -- Build schedules
      L_STATUS := 'CALLING F_LOAD_ILR_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      for ILR in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
      loop
         L_MSG := F_LOAD_ILR_STG(ILR.ILR_ID, 9999);
         exit when L_MSG <> 'OK';
      end loop;
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILRS_FCST;

   --**************************************************************************
   --                            F_SAVE_SCHEDULES
   -- This function saves the scehdules frm the calc table to the stg tables
   --**************************************************************************
   function F_SAVE_SCHEDULES return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('SAVING SCHEDULE');

      L_STATUS := 'DELETING ls_ilr_schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_SCHEDULE
       where (ILR_ID, REVISION) in (select A.ILR_ID, A.REVISION from LS_ILR_ASSET_SCHEDULE_STG A);

      L_STATUS := 'LOADING ls_ilr_schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM)
         select ILR_ID,
                REVISION,
                SET_OF_BOOKS_ID,
                month,
                sum(RESIDUAL_AMOUNT),
                sum(TERM_PENALTY),
                sum(BPO_PRICE),
                sum(BEG_CAPITAL_COST),
                sum(END_CAPITAL_COST),
                sum(BEG_OBLIGATION),
                sum(END_OBLIGATION),
                sum(BEG_LT_OBLIGATION),
                sum(END_LT_OBLIGATION),
                sum(INTEREST_ACCRUAL),
                sum(PRINCIPAL_ACCRUAL),
                sum(INTEREST_PAID),
                sum(PRINCIPAL_PAID),
                sum(CONTINGENT_ACCRUAL1),
                sum(CONTINGENT_ACCRUAL2),
                sum(CONTINGENT_ACCRUAL3),
                sum(CONTINGENT_ACCRUAL4),
                sum(CONTINGENT_ACCRUAL5),
                sum(CONTINGENT_ACCRUAL6),
                sum(CONTINGENT_ACCRUAL7),
                sum(CONTINGENT_ACCRUAL8),
                sum(CONTINGENT_ACCRUAL9),
                sum(CONTINGENT_ACCRUAL10),
                sum(EXECUTORY_ACCRUAL1),
                sum(EXECUTORY_ACCRUAL2),
                sum(EXECUTORY_ACCRUAL3),
                sum(EXECUTORY_ACCRUAL4),
                sum(EXECUTORY_ACCRUAL5),
                sum(EXECUTORY_ACCRUAL6),
                sum(EXECUTORY_ACCRUAL7),
                sum(EXECUTORY_ACCRUAL8),
                sum(EXECUTORY_ACCRUAL9),
                sum(EXECUTORY_ACCRUAL10),
                sum(CONTINGENT_PAID1),
                sum(CONTINGENT_PAID2),
                sum(CONTINGENT_PAID3),
                sum(CONTINGENT_PAID4),
                sum(CONTINGENT_PAID5),
                sum(CONTINGENT_PAID6),
                sum(CONTINGENT_PAID7),
                sum(CONTINGENT_PAID8),
                sum(CONTINGENT_PAID9),
                sum(CONTINGENT_PAID10),
                sum(EXECUTORY_PAID1),
                sum(EXECUTORY_PAID2),
                sum(EXECUTORY_PAID3),
                sum(EXECUTORY_PAID4),
                sum(EXECUTORY_PAID5),
                sum(EXECUTORY_PAID6),
                sum(EXECUTORY_PAID7),
                sum(EXECUTORY_PAID8),
                sum(EXECUTORY_PAID9),
                sum(EXECUTORY_PAID10),
                IS_OM
           from LS_ILR_ASSET_SCHEDULE_STG
          group by ILR_ID, REVISION, month, SET_OF_BOOKS_ID, IS_OM;

      L_STATUS := 'DELETING ls_asset_schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ASSET_SCHEDULE
       where (LS_ASSET_ID, REVISION) in
             (select A.LS_ASSET_ID, A.REVISION from LS_ILR_ASSET_SCHEDULE_STG A);

      L_STATUS := 'LOADING ls_asset_schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM)
         select LS_ASSET_ID,
                REVISION,
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                IS_OM
           from LS_ILR_ASSET_SCHEDULE_STG;

      -- update term penalty on ls_asset
      L_STATUS := 'UPDATING asset termination penalty amount';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ASSET A
         set A.TERMINATION_PENALTY_AMOUNT =
              (select min(M.TERM_PENALTY) from LS_ILR_ASSET_STG M where M.LS_ASSET_ID = A.LS_ASSET_ID)
       where exists (select 1 from LS_ILR_ASSET_STG M where M.LS_ASSET_ID = A.LS_ASSET_ID);

      -- update the ls_ilr_amounts_set_of_books
      L_STATUS := 'UPDATING ls_ilr_amounts_set_of_books';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_AMOUNTS_SET_OF_BOOKS
       where (ILR_ID, REVISION) in (select A.ILR_ID, A.REVISION from LS_ILR_ASSET_SCHEDULE_STG A);

      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          IS_OM, CURRENT_LEASE_COST)
         select L.ILR_ID,
                L.REVISION,
                L.SET_OF_BOOKS_ID,
                NVL(L.NPV, 0),
                NVL(100 * (POWER((1 + L.IRR), 12) - 1), 0),
                sum(A.BEG_CAPITAL_COST),
                L.IS_OM,
                L.FMV
           from LS_ILR_ASSET_SCHEDULE_STG A, LS_ILR_STG L
          where A.ID = 1
            and L.ILR_ID = A.ILR_ID
            and L.REVISION = A.REVISION
            and L.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
          group by L.ILR_ID,
                   L.REVISION,
                   L.SET_OF_BOOKS_ID,
                   NVL(L.NPV, 0),
                   NVL(100 * (POWER((1 + L.IRR), 12) - 1), 0),
                   L.IS_OM,
                   L.FMV;

      L_STATUS := 'CLEARING OUT ls_ilr_asset_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_SCHEDULE_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_asset_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_SCHEDULE_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_STG;


      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_SAVE_SCHEDULES;

   function F_PROCESS_ASSET_TRF(A_FROM_ASSET_ID     number,
                                A_TO_ASSET_ID       number,
                                A_FROM_ILR_REVISION number,
                                A_TO_ILR_REVISION   number,
                                A_PERCENT           number,
                                A_FROM_ILR_ID       number,
                                A_TO_ILR_ID         number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('Prcoessing Asset Transfer');
      PKG_PP_LOG.P_WRITE_MESSAGE('   FROM ASSET: ' || TO_CHAR(A_FROM_ASSET_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('   TO ASSET: ' || TO_CHAR(A_TO_ASSET_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('   FROM ILR: ' || TO_CHAR(A_FROM_ILR_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('   TO ILR: ' || TO_CHAR(A_TO_ILR_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('   FROM ILR Revision: ' || TO_CHAR(A_FROM_ILR_REVISION));
      PKG_PP_LOG.P_WRITE_MESSAGE('   TO ILR Revision: ' || TO_CHAR(A_TO_ILR_REVISION));
      PKG_PP_LOG.P_WRITE_MESSAGE('   PERCENT: ' || TO_CHAR(A_PERCENT));

      L_STATUS := 'STAGING ILR';
      insert into LS_ILR_STG
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NPV, FMV, IRR, IS_OM)
         select LS.ILR_ID,
                A_TO_ILR_REVISION,
                LS.SET_OF_BOOKS_ID,
                LS.NET_PRESENT_VALUE,
                LS.CURRENT_LEASE_COST,
                case
                   when LS.INTERNAL_RATE_RETURN = 0 then
                    LO.INCEPTION_AIR
                   else
                    LS.INTERNAL_RATE_RETURN
                end / 1200,
                LS.IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS LS, LS_ILR_OPTIONS LO, LS_ILR ILR
          where LS.ILR_ID = A_FROM_ILR_ID
            and LS.REVISION = A_TO_ILR_REVISION
            and LS.ILR_ID = LO.ILR_ID
            and LS.REVISION = LO.REVISION
            and ILR.ILR_ID = A_FROM_ILR_ID;

      L_STATUS := 'STAGING assets on the ILR pre transfer';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_STG
         (ID, ILR_ID, SET_OF_BOOKS_ID, REVISION, LS_ASSET_ID, TERM_PENALTY, IS_OM)
         select ROW_NUMBER() OVER(partition by LS.ILR_ID, LS.REVISION, LS.SET_OF_BOOKS_ID order by LS.FMV desc, LA.LS_ASSET_ID),
                LS.ILR_ID,
                LS.SET_OF_BOOKS_ID,
                LS.REVISION,
                LA.LS_ASSET_ID,
                LA.TERMINATION_PENALTY_AMOUNT,
                LS.IS_OM
           from LS_ILR_STG LS, LS_ASSET LA
          where LS.ILR_ID = LA.ILR_ID
            and exists (select 1
                   from LS_ASSET_SCHEDULE LSCH
                  where LSCH.REVISION = A_FROM_ILR_REVISION
                    and LSCH.LS_ASSET_ID = LA.LS_ASSET_ID);

      -- allocate amounts to NEW and OLD ASSET
      L_STATUS := 'ALLOCATE amounts between NEW and OLD ASSET';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_SCHEDULE_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, RATE, PREPAY_SWITCH,
          PAYMENT_MONTH, MONTHS_TO_ACCRUE, AMOUNT, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, CONTINGENT_ACCRUAL1,
          CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5,
          CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9,
          CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3,
          EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7,
          EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, CONTINGENT_PAID1,
          CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
          CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, EXECUTORY_PAID1,
          EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
          EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, IS_OM,
          PAYMENT_TERM_TYPE_ID)
         select A.ID,
                V.ILR_ID,
                V.REVISION,
                V.LS_ASSET_ID,
                A.SET_OF_BOOKS_ID,
                A.MONTH,
                A.RATE,
                A.PREPAY_SWITCH,
                A.PAYMENT_MONTH,
                A.MONTHS_TO_ACCRUE,
                ROUND(A.AMOUNT * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.AMOUNT - sum(ROUND(A.AMOUNT * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as AMOUNT,
                ROUND(A.RESIDUAL_AMOUNT * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.RESIDUAL_AMOUNT - sum(ROUND(A.RESIDUAL_AMOUNT * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as RESIDUAL_AMOUNT,
                ROUND(A.TERM_PENALTY * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.TERM_PENALTY - sum(ROUND(A.TERM_PENALTY * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as TERM_PENALTY,
                ROUND(A.BPO_PRICE * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.BPO_PRICE - sum(ROUND(A.BPO_PRICE * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as BPO_PRICE,
                ROUND(A.BEG_CAPITAL_COST * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.BEG_CAPITAL_COST - sum(ROUND(A.BEG_CAPITAL_COST * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as BEG_CAPITAL_COST,
                ROUND(A.END_CAPITAL_COST * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.END_CAPITAL_COST - sum(ROUND(A.END_CAPITAL_COST * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as END_CAPITAL_COST,
                ROUND(A.BEG_OBLIGATION * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.BEG_OBLIGATION - sum(ROUND(A.BEG_OBLIGATION * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as BEG_OBLIGATION,
                ROUND(A.END_OBLIGATION * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.END_OBLIGATION - sum(ROUND(A.END_OBLIGATION * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as END_OBLIGATION,
                ROUND(A.CONTINGENT_ACCRUAL1 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL1 - sum(ROUND(A.CONTINGENT_ACCRUAL1 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL1,
                ROUND(A.CONTINGENT_ACCRUAL2 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL2 - sum(ROUND(A.CONTINGENT_ACCRUAL2 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL2,
                ROUND(A.CONTINGENT_ACCRUAL3 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL3 - sum(ROUND(A.CONTINGENT_ACCRUAL3 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL3,
                ROUND(A.CONTINGENT_ACCRUAL4 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL4 - sum(ROUND(A.CONTINGENT_ACCRUAL4 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL4,
                ROUND(A.CONTINGENT_ACCRUAL5 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL5 - sum(ROUND(A.CONTINGENT_ACCRUAL5 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL5,
                ROUND(A.CONTINGENT_ACCRUAL6 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL6 - sum(ROUND(A.CONTINGENT_ACCRUAL6 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL6,
                ROUND(A.CONTINGENT_ACCRUAL7 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL7 - sum(ROUND(A.CONTINGENT_ACCRUAL7 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL7,
                ROUND(A.CONTINGENT_ACCRUAL8 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL8 - sum(ROUND(A.CONTINGENT_ACCRUAL8 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL8,
                ROUND(A.CONTINGENT_ACCRUAL9 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL9 - sum(ROUND(A.CONTINGENT_ACCRUAL9 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL9,
                ROUND(A.CONTINGENT_ACCRUAL10 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL10 - sum(ROUND(A.CONTINGENT_ACCRUAL10 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL10,
                ROUND(A.EXECUTORY_ACCRUAL1 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL1 - sum(ROUND(A.EXECUTORY_ACCRUAL1 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL1,
                ROUND(A.EXECUTORY_ACCRUAL2 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL2 - sum(ROUND(A.EXECUTORY_ACCRUAL2 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL2,
                ROUND(A.EXECUTORY_ACCRUAL3 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL3 - sum(ROUND(A.EXECUTORY_ACCRUAL3 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL3,
                ROUND(A.EXECUTORY_ACCRUAL4 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL4 - sum(ROUND(A.EXECUTORY_ACCRUAL4 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL4,
                ROUND(A.EXECUTORY_ACCRUAL5 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL5 - sum(ROUND(A.EXECUTORY_ACCRUAL5 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL5,
                ROUND(A.EXECUTORY_ACCRUAL6 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL6 - sum(ROUND(A.EXECUTORY_ACCRUAL6 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL6,
                ROUND(A.EXECUTORY_ACCRUAL7 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL7 - sum(ROUND(A.EXECUTORY_ACCRUAL7 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL7,
                ROUND(A.EXECUTORY_ACCRUAL8 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL8 - sum(ROUND(A.EXECUTORY_ACCRUAL8 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL8,
                ROUND(A.EXECUTORY_ACCRUAL9 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL9 - sum(ROUND(A.EXECUTORY_ACCRUAL9 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL9,
                ROUND(A.EXECUTORY_ACCRUAL10 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL10 - sum(ROUND(A.EXECUTORY_ACCRUAL10 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL10,
                ROUND(A.CONTINGENT_PAID1 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID1 - sum(ROUND(A.CONTINGENT_PAID1 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID1,
                ROUND(A.CONTINGENT_PAID2 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID2 - sum(ROUND(A.CONTINGENT_PAID2 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID2,
                ROUND(A.CONTINGENT_PAID3 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID3 - sum(ROUND(A.CONTINGENT_PAID3 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID3,
                ROUND(A.CONTINGENT_PAID4 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID4 - sum(ROUND(A.CONTINGENT_PAID4 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID4,
                ROUND(A.CONTINGENT_PAID5 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID5 - sum(ROUND(A.CONTINGENT_PAID5 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID5,
                ROUND(A.CONTINGENT_PAID6 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID6 - sum(ROUND(A.CONTINGENT_PAID6 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID6,
                ROUND(A.CONTINGENT_PAID7 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID7 - sum(ROUND(A.CONTINGENT_PAID7 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID7,
                ROUND(A.CONTINGENT_PAID8 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID8 - sum(ROUND(A.CONTINGENT_PAID8 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID8,
                ROUND(A.CONTINGENT_PAID9 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID9 - sum(ROUND(A.CONTINGENT_PAID9 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID9,
                ROUND(A.CONTINGENT_PAID10 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID10 - sum(ROUND(A.CONTINGENT_PAID10 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID10,
                ROUND(A.EXECUTORY_PAID1 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID1 - sum(ROUND(A.EXECUTORY_PAID1 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID1,
                ROUND(A.EXECUTORY_PAID2 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID2 - sum(ROUND(A.EXECUTORY_PAID2 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID2,
                ROUND(A.EXECUTORY_PAID3 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID3 - sum(ROUND(A.EXECUTORY_PAID3 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID3,
                ROUND(A.EXECUTORY_PAID4 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID4 - sum(ROUND(A.EXECUTORY_PAID4 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID4,
                ROUND(A.EXECUTORY_PAID5 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID5 - sum(ROUND(A.EXECUTORY_PAID5 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID5,
                ROUND(A.EXECUTORY_PAID6 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID6 - sum(ROUND(A.EXECUTORY_PAID6 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID6,
                ROUND(A.EXECUTORY_PAID7 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID7 - sum(ROUND(A.EXECUTORY_PAID7 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID7,
                ROUND(A.EXECUTORY_PAID8 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID8 - sum(ROUND(A.EXECUTORY_PAID8 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID8,
                ROUND(A.EXECUTORY_PAID9 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID9 - sum(ROUND(A.EXECUTORY_PAID9 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID9,
                ROUND(A.EXECUTORY_PAID10 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID10 - sum(ROUND(A.EXECUTORY_PAID10 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID10,
                A.IS_OM,
                A.PAYMENT_TERM_TYPE_ID
           from LS_ILR_ASSET_SCHEDULE_CALC_STG A,
                LS_ILR_ASSET_STG S,
                (select A_TO_ILR_ID as ILR_ID,
                        A_TO_ASSET_ID as LS_ASSET_ID,
                        A_PERCENT as PCT_SPREAD,
                        case
                           when A_TO_ILR_ID = A_FROM_ILR_ID then
                            A_TO_ILR_REVISION
                           else
                            1
                        end as REVISION
                   from DUAL
                 union all
                 select A_FROM_ILR_ID as ILR_ID,
                        A_FROM_ASSET_ID as LS_ASSET_ID,
                        1 - A_PERCENT as PCT_SPREAD,
                        A_TO_ILR_REVISION
                   from DUAL) V
          where S.LS_ASSET_ID = A.LS_ASSET_ID
            and A.REVISION = A_FROM_ILR_REVISION
            and A.MONTH is not null
            and A.ID > 0
            and S.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
            and S.LS_ASSET_ID = A_FROM_ASSET_ID;

      L_STATUS := 'LOAD remainder of assets for the schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_SCHEDULE_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT,
          TERM_PENALTY, BPO_PRICE, PREPAY_SWITCH, PAYMENT_MONTH, MONTHS_TO_ACCRUE, RATE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, CONTINGENT_ACCRUAL1,
          CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5,
          CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9,
          CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3,
          EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7,
          EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, CONTINGENT_PAID1,
          CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
          CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, EXECUTORY_PAID1,
          EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
          EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, IS_OM,
          PAYMENT_TERM_TYPE_ID)
         select A.ID,
                A.ILR_ID,
                S.REVISION,
                A.LS_ASSET_ID,
                A.SET_OF_BOOKS_ID,
                A.MONTH,
                A.AMOUNT,
                A.RESIDUAL_AMOUNT,
                A.TERM_PENALTY,
                A.BPO_PRICE,
                A.PREPAY_SWITCH,
                A.PAYMENT_MONTH,
                A.MONTHS_TO_ACCRUE,
                A.RATE,
                A.BEG_CAPITAL_COST,
                A.END_CAPITAL_COST,
                A.BEG_OBLIGATION,
                A.END_OBLIGATION,
                A.CONTINGENT_ACCRUAL1,
                A.CONTINGENT_ACCRUAL2,
                A.CONTINGENT_ACCRUAL3,
                A.CONTINGENT_ACCRUAL4,
                A.CONTINGENT_ACCRUAL5,
                A.CONTINGENT_ACCRUAL6,
                A.CONTINGENT_ACCRUAL7,
                A.CONTINGENT_ACCRUAL8,
                A.CONTINGENT_ACCRUAL9,
                A.CONTINGENT_ACCRUAL10,
                A.EXECUTORY_ACCRUAL1,
                A.EXECUTORY_ACCRUAL2,
                A.EXECUTORY_ACCRUAL3,
                A.EXECUTORY_ACCRUAL4,
                A.EXECUTORY_ACCRUAL5,
                A.EXECUTORY_ACCRUAL6,
                A.EXECUTORY_ACCRUAL7,
                A.EXECUTORY_ACCRUAL8,
                A.EXECUTORY_ACCRUAL9,
                A.EXECUTORY_ACCRUAL10,
                A.CONTINGENT_PAID1,
                A.CONTINGENT_PAID2,
                A.CONTINGENT_PAID3,
                A.CONTINGENT_PAID4,
                A.CONTINGENT_PAID5,
                A.CONTINGENT_PAID6,
                A.CONTINGENT_PAID7,
                A.CONTINGENT_PAID8,
                A.CONTINGENT_PAID9,
                A.CONTINGENT_PAID10,
                A.EXECUTORY_PAID1,
                A.EXECUTORY_PAID2,
                A.EXECUTORY_PAID3,
                A.EXECUTORY_PAID4,
                A.EXECUTORY_PAID5,
                A.EXECUTORY_PAID6,
                A.EXECUTORY_PAID7,
                A.EXECUTORY_PAID8,
                A.EXECUTORY_PAID9,
                A.EXECUTORY_PAID10,
                A.IS_OM,
                A.PAYMENT_TERM_TYPE_ID
           from LS_ILR_ASSET_SCHEDULE_CALC_STG A, LS_ILR_ASSET_STG S
          where S.LS_ASSET_ID = A.LS_ASSET_ID
            and A.REVISION = A_FROM_ILR_REVISION
            and A.MONTH is not null
            and A.ID > 0
            and S.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
            and S.LS_ASSET_ID <> A_FROM_ASSET_ID;

      --clear out the from asset if it's 100%
      if A_PERCENT = 1 then
         delete from LS_ILR_ASSET_SCHEDULE_STG
         where LS_ASSET_ID = A_FROM_ASSET_ID;

         delete from LS_ILR_ASSET_STG
         where LS_ASSET_ID = A_FROM_ASSET_ID;
      end if;

      L_STATUS := F_PROCESS_ASSETS;
      if L_STATUS = 'OK' then
         -- close connection since f_save_schedule creates its own connections
         -- before saving to the schedule
         -- need to insert correct values into ls_ilr_stg
         -- for the from and to ilrs
         -- since those have changed. (only if ILRs are different)
         if A_FROM_ILR_ID <> A_TO_ILR_ID then
            L_STATUS := 'LOAD ls_ilr_stg for NEW ilr';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            insert into LS_ILR_STG
               (ILR_ID, REVISION, SET_OF_BOOKS_ID, NPV, FMV, IRR, IS_OM)
               select LS.ILR_ID,
                      1,
                      LS.SET_OF_BOOKS_ID,
                      LS.NET_PRESENT_VALUE,
                      LS.CURRENT_LEASE_COST,
                      case
                         when LS.INTERNAL_RATE_RETURN = 0 then
                          LO.INCEPTION_AIR
                         else
                          LS.INTERNAL_RATE_RETURN
                      end / 1200,
                      LS.IS_OM
                 from LS_ILR_AMOUNTS_SET_OF_BOOKS LS, LS_ILR_OPTIONS LO, LS_ILR ILR
                where LS.ILR_ID = A_TO_ILR_ID
                  and LS.REVISION = 1
                  and LS.ILR_ID = LO.ILR_ID
                  and LS.REVISION = LO.REVISION
                  and ILR.ILR_ID = A_TO_ILR_ID;
         end if;


         L_STATUS := F_SAVE_SCHEDULES;
         return L_STATUS;
      else
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
      end if;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ASSET_TRF;

   function F_GET_II_BEGIN_DATE(A_ILR_ID   number,
                                A_REVISION number) return date is
      RTN_DATE date;
   begin
      select INTERIM_INTEREST_BEGIN_DATE
        into RTN_DATE
        from LS_ILR_PAYMENT_TERM
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and PAYMENT_TERM_TYPE_ID = 1;

      return RTN_DATE;

   exception
      when NO_DATA_FOUND then
         return 'JUL-4-1776';
      when TOO_MANY_ROWS then
         return 'JUL-4-1776';

   end F_GET_II_BEGIN_DATE;

   function F_MAKE_II(A_ILR_ID   number,
                      A_REVISION number) return number is
      RTN number;
   begin
      select MAKE_II_PAYMENT
        into RTN
        from LS_ILR_PAYMENT_TERM
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and PAYMENT_TERM_TYPE_ID = 1;

      return RTN;

   end F_MAKE_II;

end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1080, 0, 10, 4, 2, 0, 37396, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037396_lease_PKG_LEASE_SCHEDULE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON
