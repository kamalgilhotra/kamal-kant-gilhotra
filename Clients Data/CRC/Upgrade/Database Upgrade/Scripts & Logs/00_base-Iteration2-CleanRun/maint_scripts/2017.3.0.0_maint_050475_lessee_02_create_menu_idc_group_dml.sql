/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_02_create_menu_idc_group_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/25/2018 Shane Ward    		Lessee Initial Direct Cost Admin menu item
||============================================================================
*/

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSEE', 'admin_ls_idc_incentive_group', 'IDC/Incentive Group', 'uo_ls_admincntr_idc_incentive_group_wksp', NULL, 1);

UPDATE ppbase_menu_items SET item_order = item_order + 1 WHERE MODULE = 'LESSEE' AND PARENT_MENU_IDENTIFIER = 'menu_wksp_admin' AND item_order > 8;

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSEE', 'admin_ls_idc_incentive_group', 2, 9, 'IDC/Incentive Group', NULL, 'menu_wksp_admin', 'admin_ls_idc_incentive_group', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4235, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_02_create_menu_idc_group_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;