/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_08_add_idc_incentive_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/11/2018 Shane "C" Ward 	Add IDC and Incentive Trans Types for Lease
||============================================================================
*/

--More columns for holding non display amounts for use during calculations and posting to simplify that.
ALTER TABLE LS_ASSET_SCHEDULE ADD IDC_MATH_AMOUNT NUMBER(22,2);
ALTER TABLE LS_ASSET_SCHEDULE ADD INCENTIVE_MATH_AMOUNT NUMBER(22,2);

COMMENT ON COLUMN ls_asset_schedule.idc_math_amount IS 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_asset_schedule.incentive_math_amount IS 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';

--Modify Import tables for new Incentive and IDC Accounts
ALTER TABLE LS_IMPORT_ILR
  ADD incentive_account_xlate VARCHAR2(254);

ALTER TABLE LS_IMPORT_ILR
  ADD incentive_account_id NUMBER(22);

COMMENT ON COLUMN LS_IMPORT_ILR.incentive_account_xlate IS 'Translation field for determining Incentive account.';

COMMENT ON COLUMN LS_IMPORT_ILR.incentive_account_id IS 'The Incentive account.';

ALTER TABLE LS_IMPORT_ILR_ARCHIVE
  ADD incentive_account_xlate VARCHAR2(254);

ALTER TABLE LS_IMPORT_ILR_ARCHIVE
  ADD incentive_account_id NUMBER(22);

COMMENT ON COLUMN LS_IMPORT_ILR_ARCHIVE.incentive_account_xlate IS 'Translation field for determining Incentive account.';

COMMENT ON COLUMN LS_IMPORT_ILR_ARCHIVE.incentive_account_id IS 'The Incentive account.';


ALTER TABLE LS_IMPORT_ILR
  ADD init_direct_cost_acct_xlate VARCHAR2(254);

ALTER TABLE LS_IMPORT_ILR
  ADD init_direct_cost_account_id NUMBER(22);

COMMENT ON COLUMN LS_IMPORT_ILR.init_direct_cost_acct_xlate IS 'Translation field for determining Initial Direct Cost account.';

COMMENT ON COLUMN LS_IMPORT_ILR.init_direct_cost_account_id IS 'The Initial Direct Cost account.';

ALTER TABLE LS_IMPORT_ILR_ARCHIVE
  ADD init_direct_cost_acct_xlate VARCHAR2(254);

ALTER TABLE LS_IMPORT_ILR_ARCHIVE
  ADD init_direct_cost_account_id NUMBER(22);

COMMENT ON COLUMN LS_IMPORT_ILR_ARCHIVE.init_direct_cost_acct_xlate IS 'Translation field for determining Initial Direct Cost account.';

COMMENT ON COLUMN LS_IMPORT_ILR_ARCHIVE.init_direct_cost_account_id IS 'The Initial Direct Cost account.';



ALTER TABLE LS_SUMMARY_FORECAST
  ADD initial_direct_cost NUMBER(22, 2) NULL;

ALTER TABLE LS_SUMMARY_FORECAST
  ADD incentive_amount NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_SUMMARY_FORECAST.initial_direct_cost IS 'Holds Initial Direct Cost Amount for ILR Schedule.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.incentive_amount IS 'Holds Incentive Amount for ILR Schedule.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4625, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_08_add_idc_incentive_columns_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;