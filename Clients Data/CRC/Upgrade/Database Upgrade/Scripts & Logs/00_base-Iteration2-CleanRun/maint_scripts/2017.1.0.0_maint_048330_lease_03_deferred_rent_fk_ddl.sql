/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_03_deferred_rent_fk_ddl.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding foreign keys for deferred rent
||========================================================================================
*/

ALTER TABLE ls_ilr_group
  ADD CONSTRAINT ls_ilr_group_fk14 FOREIGN KEY (
    st_deferred_account_id
  ) REFERENCES gl_account (
    gl_account_id
  )
/

ALTER TABLE ls_ilr_group
  ADD CONSTRAINT ls_ilr_group_fk15 FOREIGN KEY (
    lt_deferred_account_id
  ) REFERENCES gl_account (
    gl_account_id
  )
/

ALTER TABLE ls_ilr_account
  ADD CONSTRAINT ls_ilr_account_fk14 FOREIGN KEY (
    st_deferred_account_id
  ) REFERENCES gl_account (
    gl_account_id
  )
/

ALTER TABLE ls_ilr_account
  ADD CONSTRAINT ls_ilr_account_fk15 FOREIGN KEY (
    lt_deferred_account_id
  ) REFERENCES gl_account (
    gl_account_id
  )
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3542, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_03_deferred_rent_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;