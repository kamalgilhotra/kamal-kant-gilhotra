/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045522_pwrtax_plant_recon_filter_ids_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 03/09/2016 David Haupt    Filter DB info for Plant Recon wksp
||============================================================================
*/

insert into pp_reports_filter
(pp_report_filter_id, description, filter_uo_name)
values
(98, 'PowerTax - Plant Recon Form', 'uo_ppbase_tab_filter_dynamic');

insert into pp_dynamic_filter
(filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, 
dw_id_datatype, required, single_select_only, valid_operators, data, exclude_from_where, 
not_retrieve_immediate, invisible)
values
(282, 'Jurisdiction', 'dw', null, null, 'dw_jurisdiction_tax_book_filter', 'jurisdiction_id', 'description',
'N', 1, 0, null, null, 1, 0, 0);

insert into pp_dynamic_filter
(filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, 
dw_id_datatype, required, single_select_only, valid_operators, data, exclude_from_where, 
not_retrieve_immediate, invisible)
values
(283, 'Tax Year - Unlocked', 'dw', null, null, 'dw_tax_year_unlock_by_version_filter', 'tax_year_version_id', 'tax_year',
'N', 1, 1, null, null, 1, 0, 0);

insert into pp_dynamic_filter_mapping
(pp_report_filter_id, filter_id)
values
(98, 171);

insert into pp_dynamic_filter_mapping
(pp_report_filter_id, filter_id)
values
(98, 282);

insert into pp_dynamic_filter_mapping
(pp_report_filter_id, filter_id)
values
(98, 283);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3100, 0, 2016, 1, 0, 0, 45522, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045522_pwrtax_plant_recon_filter_ids_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;