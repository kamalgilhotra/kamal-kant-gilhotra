/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041983_pcm_user_commments_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 01/19/2015 Sarah Byers      Porting the User Comments and Attachments functionality
||============================================================================
*/

alter table work_order_document add (lock_on_approval number(22,0));
comment on column work_order_document.lock_on_approval is '1: will prevent a user from deleting or updating a database-stored document attached  to a Funding Project that has been Approved or Sent for Approval; 0: will allow the document to be deleted';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2192, 0, 2015, 1, 0, 0, 041983, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041983_pcm_user_commments_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;