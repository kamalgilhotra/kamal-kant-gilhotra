/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030587_lease_ppbase_tables.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/09/2013 B. Beck        Point release
||============================================================================
*/

delete from PPBASE_MENU_ITEMS where MODULE = 'LESSEE';
delete from PPBASE_WORKSPACE where MODULE = 'LESSEE';

-- PPBASE_WORKSPACE
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'initiate_mla', null, null, 'MLA', 'uo_ls_lscntr_wksp_init', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'initiate_asset', null, null, 'Asset', 'uo_ls_assetcntr_wksp_init', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'initiate_ilr', null, null, 'ILR', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'initiate_ilr_new', null, null, 'New', 'uo_ls_ilrcntr_wksp_init_lease', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'initiate_ilr_from_assets', null, null, 'from Assets', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'menu_wksp_details', null, null, 'Details', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'details_mla', null, null, 'MLA', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'details_asset', null, null, 'Asset', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'details_ilr', null, null, 'ILR', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'details_schedules', null, null, 'Schedules', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'menu_wksp_control', null, null, 'Monthly Control', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'control_payments', null, null, 'Payments', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'control_invoices', null, null, 'Invoices', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'control_monthend', null, null, 'Month End', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'menu_wksp_approval', null, null, 'Approvals', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'approval_mla', null, null, 'MLA', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'approval_assets', null, null, 'Assets and Schedules', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'approval_trans_ret', null, null, 'Transfers and Retirements', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'approval_payments', null, null, 'Payments', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'menu_wksp_reports', null, null, 'Reports', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'menu_wksp_admin', null, null, 'Admin', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'admin_lessors', null, null, 'Lessors', 'uo_ls_lssrcntr_wksp_lessors', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'admin_vendors', null, null, 'Vendors', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'admin_taxes', null, null, 'Taxes', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'admin_system_controls', null, null, 'System Controls', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'menu_wksp_import', null, null, 'Import Tools', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'import_mla', null, null, 'MLA', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'import_assets', null, null, 'Assets', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'import_lessors', null, null, 'Lessors', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'import_invoices', null, null, 'Invoices', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'search_mla', null, null, 'MLA', 'uo_ls_lscntr_wksp_search', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'search_asset', null, null, 'Asset', 'uo_ls_assetcntr_wksp_search', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'search_ilr', null, null, 'ILR', 'uo_ls_ilrcntr_wksp_search', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'menu_wksp_initiate', null, null, 'Initiate', ' ', null);
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'search_schedules', null, null, 'Schedules', ' ', null);

-- PPBASE_MENU_ITEMS
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_initiate', null, null, 1, 1, 'Initiate', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_mla', null, null, 2, 1, 'MLA', null, 'menu_wksp_initiate', 'initiate_mla',
    1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_asset', null, null, 2, 2, 'Asset', null, 'menu_wksp_initiate',
    'initiate_asset', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_ilr', null, null, 2, 3, 'ILR', null, 'menu_wksp_initiate', null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_ilr_new', null, null, 3, 1, 'New', null, 'initiate_ilr', 'initiate_ilr_new',
    1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_ilr_from_assets', null, null, 3, 2, 'from Assets', null, 'initiate_ilr',
    'initiate_ilr_from_assets', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_details', null, null, 1, 2, 'Details', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_mla', null, null, 2, 1, 'MLA', null, 'menu_wksp_details', 'search_mla', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_asset', null, null, 2, 2, 'Asset', null, 'menu_wksp_details', 'search_asset',
    1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_ilr', null, null, 2, 3, 'ILR', null, 'menu_wksp_details', 'search_ilr', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_schedules', null, null, 2, 4, 'Schedules', null, 'menu_wksp_details',
    'search_schedules', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_control', null, null, 1, 4, 'Monthly Control', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'control_payments', null, null, 2, 1, 'Payments', null, 'menu_wksp_control',
    'control_payments', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'control_invoices', null, null, 2, 2, 'Invoices', null, 'menu_wksp_control',
    'control_invoices', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'control_monthend', null, null, 2, 3, 'Month End', null, 'menu_wksp_control',
    'control_monthend', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_approval', null, null, 1, 3, 'Approvals', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'approval_mla', null, null, 2, 1, 'MLA', null, 'menu_wksp_approval', 'approval_mla',
    1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'approval_assets', null, null, 2, 2, 'Assets and Schedules', null,
    'menu_wksp_approval', 'approval_assets', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'approval_trans_ret', null, null, 2, 3, 'Transfers and Retirements', null,
    'menu_wksp_approval', 'approval_trans_ret', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'approval_payments', null, null, 2, 4, 'Payments', null, 'menu_wksp_approval',
    'approval_payments', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_reports', null, null, 1, 5, 'Reports', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_admin', null, null, 1, 6, 'Admin', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_lessors', null, null, 2, 1, 'Lessors', null, 'menu_wksp_admin',
    'admin_lessors', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_vendors', null, null, 2, 2, 'Vendors', null, 'menu_wksp_admin',
    'admin_vendors', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_taxes', null, null, 2, 3, 'Taxes', null, 'menu_wksp_admin', 'admin_taxes', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_system_controls', null, null, 2, 4, 'System Controls', null, 'menu_wksp_admin',
    'admin_system_controls', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_import', null, null, 1, 7, 'Import Tools', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'import_mla', null, null, 2, 1, 'MLA', null, 'menu_wksp_import', 'import_mla', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'import_assets', null, null, 2, 2, 'Assets', null, 'menu_wksp_import',
    'import_assets', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'import_lessors', null, null, 2, 3, 'Lessors', null, 'menu_wksp_import',
    'import_lessors', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'import_invoices', null, null, 2, 4, 'Invoices', null, 'menu_wksp_import',
    'import_invoices', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (454, 0, 10, 4, 1, 0, 30587, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030587_lease_ppbase_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
