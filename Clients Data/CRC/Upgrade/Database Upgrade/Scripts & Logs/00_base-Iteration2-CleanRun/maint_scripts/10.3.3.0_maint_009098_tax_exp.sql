/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009098_tax_exp.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   12/12/2011 Elhadj Bah     Point Release
||============================================================================
*/

--** Maint-9098 -- Create a column to utility_account with 1 being overhead and 2 being underground.
--                 Change the code to look at this column to figure out whether these ovh to udg conversions
--                 are occurring. Need to add this column to table maintenance

alter table UTILITY_ACCOUNT add OVH_TO_UDG number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (69, 0, 10, 3, 3, 0, 9098, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_009098_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
