/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011108_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/13/2012 Chris Mardis   Point Release
||============================================================================
*/

create table WORK_ORDER_ALTERNATIVES
(
 WORK_ORDER_ID  number(22) not null,
 USER_ID        varchar2(18),
 TIME_STAMP     date,
 ALTERNATIVE_ID number(22)
);

alter table WORK_ORDER_ALTERNATIVES
   add constraint WO_ALTERNATIVES_PK
       primary key (WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

create table WORK_ORDER_PREREQS
(
 WORK_ORDER_ID        number(22) not null,
 PREREQ_WORK_ORDER_ID number(22) not null,
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table WORK_ORDER_PREREQS
   add constraint WO_PREREQS_PK
       primary key (WORK_ORDER_ID, PREREQ_WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

alter table BV_PROJ_RANKING_RESULTS
   add CHECKED number(22) default 1;

update BV_PROJ_RANKING_RESULTS set CHECKED = 1 where CHECKED is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (252, 0, 10, 4, 0, 0, 11108, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011108_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
