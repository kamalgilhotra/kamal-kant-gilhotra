/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033574_projects_require_bi_fp_init.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/05/2014 Chris Mardis   Point Release
||============================================================================
*/

alter table WORK_ORDER_TYPE add FP_REQUIRE_BUDGET_ITEM number(22,0);

comment on column WORK_ORDER_TYPE.FP_REQUIRE_BUDGET_ITEM is 'Flag indicating whether to require a Budget Item to be selected during FP Initiation';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('fp_require_budget_item', 'work_order_type',
    TO_DATE('2014-01-08 14:49:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'yes_no', 'p', null,
    'Require Bud Item on FP Init', 98, '5 Budget', null, null, null, null, null, 0, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (944, 0, 10, 4, 2, 0, 33574, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033574_projects_require_bi_fp_init.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;