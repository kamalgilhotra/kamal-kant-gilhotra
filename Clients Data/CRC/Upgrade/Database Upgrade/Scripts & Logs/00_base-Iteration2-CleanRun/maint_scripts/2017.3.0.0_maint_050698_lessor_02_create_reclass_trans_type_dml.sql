/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050698_lessor_02_create_reclass_trans_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 03/30/2018 Johnny Sisouphanh Create the new Transaction types we need for Lessor Reclass
||============================================================================
*/

/* Add new trans type 4027 - Lessor ST Receivable Reclass Debit */
insert into je_trans_type (trans_type, description)
values (4027, 'Lessor ST Receivable Reclass Debit');

/* Add new trans type 4028 - Lessor LT Receivable Reclass Debit */
insert into je_trans_type (trans_type, description)
values (4028, 'Lessor LT Receivable Reclass Credit');

/* Relate the new trans types to the delivered JE Method */
insert into je_method_trans_type(je_method_id, trans_type)
select 1, trans_type from je_trans_type 
where trans_type in (4027,4028);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4247, 0, 2017, 3, 0, 0, 50698, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050698_lessor_02_create_reclass_trans_type_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;