--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042975_pcm_bdg_afc_fp_rpt03_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     02/24/2015 Andrew Scott     Budget - AFUDC FP 03 Report Conversion.
--||============================================================================
--*/

----New Base Gui Time Options
insert into pp_mypp_time
(mypp_time_id, description, long_description)
values
(10, 'Life to Date', 'Life to Date');

insert into pp_mypp_time
(mypp_time_id, description, long_description)
values
(11, 'Total', 'Total');

----clear out everything previously done (making it re-runnable).
----verify ids with master database before final merge and run.
delete from pp_reports where report_id = 300001;

delete from pp_dynamic_filter_mapping
where pp_report_filter_id = 88;
delete from pp_dynamic_filter
where filter_id in ( 264 );
delete from pp_reports_filter
where pp_report_filter_id = 88;


------New report filter for Budget FP reports.
insert into pp_reports_filter
(pp_report_filter_id, description, table_name, filter_uo_name)
values
(88, 'PCM - Rpt Budget FP 1', '', 'uo_ppbase_tab_filter_dynamic');

---- New Dynamic Filters
---- Budget Version
insert into pp_dynamic_filter
(filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required)
values
(264, 'Budget Version', 'dw', 'budget_version.budget_version_id', '', 'dw_pp_budget_version_filter', 'budget_version_id', 'description', 'N', 1);
---- Company
---- use existing filter_id = 3.  It joins into the company view, is not required, and allows multiple to be selected.

----Link the Dynamic Filters to the Report Filter
---- Budget Version
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (88, 264);
---- Company 
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (88, 3);

----Apply restrictions to some.
----not needed for this report

----New Report
insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID)
values
   (300001, 'Budg vs. Act AFUDC Comp/FP',
    'For a selected budget version, the report will display budget vs. actual debt and equity AFUDC by company and funding project.',
    'dw_pcm_rpt_bdgt_afudc_fp_03', 'ALREADY HAS REPORT TIME, SUBTOTAL-FP',
    'PCM - Budget - AFUDC FP 03', 5, 35, 2, 88, 1, 3);

commit;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2321, 0, 2015, 1, 0, 0, 42975, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042975_pcm_bdg_afc_fp_rpt03_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;