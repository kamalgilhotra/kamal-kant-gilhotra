/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010013_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    04/27/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Enable importing of updates to existing data.
--

-- Add the fields controlling the add/update capabilities.
alter table PT_IMPORT_TYPE add ALLOW_UPDATES_ON_ADD number(22,0);

update PT_IMPORT_TYPE set ALLOW_UPDATES_ON_ADD = 0;
update PT_IMPORT_TYPE set ALLOW_UPDATES_ON_ADD = 1 where IMPORT_TYPE_ID in ( 6, 14, 15, 18 );

alter table PT_IMPORT_TYPE modify ALLOW_UPDATES_ON_ADD not null;

alter table PT_IMPORT_TEMPLATE add DO_UPDATE_WITH_ADD number(22,0);
update PT_IMPORT_TEMPLATE set DO_UPDATE_WITH_ADD = 0 where IMPORT_TYPE_ID in ( select IMPORT_TYPE_ID from PT_IMPORT_TYPE where ALLOW_UPDATES_ON_ADD = 1 );

alter table PT_IMPORT_TEMPLATE add UPDATES_IMPORT_LOOKUP_ID number(22,0);

alter table PT_IMPORT_TEMPLATE
   add constraint PT_IMPT_TEMPLT_IMPT_LOOKUP_FK
       foreign key ( UPDATES_IMPORT_LOOKUP_ID )
       references PT_IMPORT_LOOKUP;

-- Add the table controlling the valid lookups (i.e., columns) for finding update rows for import types where updates are allowed..
create table PT_IMPORT_TYPE_UPDATES_LOOKUP
(
 IMPORT_TYPE_ID   number(22,0) not null,
 IMPORT_LOOKUP_ID number(22,0) not null,
 USER_ID          varchar2(18),
 TIME_STAMP       date
);

alter table pt_import_type_updates_lookup
   add constraint PT_IMPORT_TYPE_UPDATES_LOOK_PK
       primary key ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID )
       using index tablespace PWRPLANT_IDX;

alter table PT_IMPORT_TYPE_UPDATES_LOOKUP
   add constraint PT_IMPT_TY_UPD_LOOK_IMPT_TY_FK
       foreign key ( IMPORT_TYPE_ID )
       references PT_IMPORT_TYPE;

alter table PT_IMPORT_TYPE_UPDATES_LOOKUP
   add constraint PT_IMPT_TY_UPD_LOOK_IMPT_LK_FK
       foreign key ( IMPORT_LOOKUP_ID )
       references PT_IMPORT_LOOKUP;

-- Add a lookup that we were missing.
insert into PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED ) values ( 129, sysdate, user, 'PT Parcel Responsible Entity.Entity Number', 'The passed in value corresponds to the PT Parcel Responsible Entity: Entity Number field.  Translate to the Responsible Entity ID using the Entity Number column on the PT Parcel Responsible Entity table.', 'responsible_entity_id', '( select re.responsible_entity_id from pt_parcel_responsible_entity re where upper( trim( <importfield> ) ) = upper( trim( re.entity_number ) ) )', 0, 0 );


insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 14, 118 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 14, 119 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 14, 128 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 14, 129 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 9 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 10 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 11 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 12 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 13 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 14 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 15 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 16 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 17 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 18 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 70 );
insert into PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID ) values ( 6, 71 );

-- Add fields for marking changing rows on updatable add imports.
alter table PT_IMPORT_PRCL_GEO     add IS_MODIFIED NUMBER(22,0);
alter table PT_IMPORT_PRCL_RSP_ENT add IS_MODIFIED NUMBER(22,0);
alter table PT_IMPORT_PRCL_RSP     add IS_MODIFIED NUMBER(22,0);
alter table PT_IMPORT_PARCEL       add IS_MODIFIED NUMBER(22,0);

-- Add columns to the import lookup table to track the table/column of the lookup field.
alter table PT_IMPORT_LOOKUP add LOOKUP_TABLE_NAME           varchar2(35);
alter table PT_IMPORT_LOOKUP add LOOKUP_COLUMN_NAME          varchar2(35);
alter table PT_IMPORT_LOOKUP add LOOKUP_CONSTRAINING_COLUMNS varchar2(254);

update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'state', lookup_column_name = 'long_description' where import_lookup_id = 1;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'state', lookup_column_name = 'state_code' where import_lookup_id = 2;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_company', lookup_column_name = 'description' where import_lookup_id = 3;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_company', lookup_column_name = 'legal_name' where import_lookup_id = 4;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_company', lookup_column_name = 'gl_company_no' where import_lookup_id = 5;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_location', lookup_column_name = 'description' where import_lookup_id = 7;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_location', lookup_column_name = 'location_code' where import_lookup_id = 8;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'parcel_number' where import_lookup_id = 9;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'parcel_number' where import_lookup_id = 10;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'description' where import_lookup_id = 11;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'description' where import_lookup_id = 12;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'long_description' where import_lookup_id = 13;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'long_description' where import_lookup_id = 14;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'company_grid_number' where import_lookup_id = 15;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'company_grid_number' where import_lookup_id = 16;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'state_grid_number' where import_lookup_id = 17;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'state_grid_number' where import_lookup_id = 18;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'company', lookup_column_name = 'description' where import_lookup_id = 19;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'company', lookup_column_name = 'short_description' where import_lookup_id = 20;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'company', lookup_column_name = 'gl_company_no' where import_lookup_id = 21;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'gl_account', lookup_column_name = 'description' where import_lookup_id = 22;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'gl_account', lookup_column_name = 'long_description' where import_lookup_id = 23;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'gl_account', lookup_column_name = 'external_account_code' where import_lookup_id = 24;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'business_segment', lookup_column_name = 'description' where import_lookup_id = 25;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'business_segment', lookup_column_name = 'external_bus_segment' where import_lookup_id = 26;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'utility_account', lookup_column_name = 'description' where import_lookup_id = 27;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'utility_account', lookup_column_name = 'description' where import_lookup_id = 28;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'utility_account', lookup_column_name = 'external_account_code' where import_lookup_id = 29;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'utility_account', lookup_column_name = 'external_account_code' where import_lookup_id = 30;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_type_data', lookup_column_name = 'description' where import_lookup_id = 31;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_type_data', lookup_column_name = 'description' where import_lookup_id = 32;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_type_data', lookup_column_name = 'external_code' where import_lookup_id = 33;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_type_data', lookup_column_name = 'external_code' where import_lookup_id = 34;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_year', lookup_column_name = 'description' where import_lookup_id = 35;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_year', lookup_column_name = 'long_description' where import_lookup_id = 36;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_year', lookup_column_name = 'valuation_year' where import_lookup_id = 37;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_year', lookup_column_name = 'year' where import_lookup_id = 38;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_adjust', lookup_column_name = 'description' where import_lookup_id = 39;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_adjust', lookup_column_name = 'long_description' where import_lookup_id = 40;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'depr_group', lookup_column_name = 'description' where import_lookup_id = 41;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'depr_group', lookup_column_name = 'external_depr_code' where import_lookup_id = 42;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'county', lookup_column_name = 'county_id' where import_lookup_id = 43;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_location', lookup_column_name = 'description' where import_lookup_id = 44;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_location', lookup_column_name = 'location_code' where import_lookup_id = 45;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_allo_definition', lookup_column_name = 'description' where import_lookup_id = 46;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_allo_definition', lookup_column_name = 'long_description' where import_lookup_id = 47;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_escalated_value_type', lookup_column_name = 'description' where import_lookup_id = 48;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_escalated_value_type', lookup_column_name = 'long_description' where import_lookup_id = 49;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_levy_class', lookup_column_name = 'description' where import_lookup_id = 50;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_levy_class', lookup_column_name = 'long_description' where import_lookup_id = 51;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'description' where import_lookup_id = 52;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'long_description' where import_lookup_id = 53;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'tax_authority_code' where import_lookup_id = 54;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'description' where import_lookup_id = 55;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'long_description' where import_lookup_id = 56;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'tax_authority_code' where import_lookup_id = 57;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'description' where import_lookup_id = 58;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'long_description' where import_lookup_id = 59;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'property_tax_authority', lookup_column_name = 'tax_authority_code' where import_lookup_id = 60;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'county', lookup_column_name = 'description' where import_lookup_id = 61;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'county', lookup_column_name = 'county_code' where import_lookup_id = 62;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'county', lookup_column_name = 'description' where import_lookup_id = 63;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'county', lookup_column_name = 'county_code' where import_lookup_id = 64;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_case', lookup_column_name = 'description' where import_lookup_id = 65;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_case', lookup_column_name = 'long_description' where import_lookup_id = 66;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_assessor', lookup_column_name = 'description' where import_lookup_id = 67;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_assessor', lookup_column_name = 'name_line_1' where import_lookup_id = 68;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_assessor', lookup_column_name = 'ext_assessor_code' where import_lookup_id = 69;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'parcel_number' where import_lookup_id = 70;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel', lookup_column_name = 'parcel_number' where import_lookup_id = 71;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_type', lookup_column_name = 'description' where import_lookup_id = 72;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_type', lookup_column_name = 'long_description' where import_lookup_id = 73;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_type_code', lookup_column_name = 'description' where import_lookup_id = 74;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_type_code', lookup_column_name = 'long_description' where import_lookup_id = 75;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_type_code', lookup_column_name = 'external_code' where import_lookup_id = 76;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'description' where import_lookup_id = 79;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'long_description' where import_lookup_id = 80;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'tax_district_code' where import_lookup_id = 81;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'description' where import_lookup_id = 82;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'long_description' where import_lookup_id = 83;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'tax_district_code' where import_lookup_id = 84;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'description' where import_lookup_id = 85;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'long_description' where import_lookup_id = 86;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'tax_district_code' where import_lookup_id = 87;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'long_description' where import_lookup_id = 88;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'ext_asset_location' where import_lookup_id = 89;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'grid_coordinate' where import_lookup_id = 90;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'grid_coordinate1' where import_lookup_id = 91;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'grid_coordinate2' where import_lookup_id = 92;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'address' where import_lookup_id = 93;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'long_description' where import_lookup_id = 94;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'ext_asset_location' where import_lookup_id = 95;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'grid_coordinate' where import_lookup_id = 96;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'grid_coordinate1' where import_lookup_id = 97;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'grid_coordinate2' where import_lookup_id = 98;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'asset_location', lookup_column_name = 'address' where import_lookup_id = 99;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'grid_coordinate' where import_lookup_id = 105;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'grid_coordinate' where import_lookup_id = 106;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'prop_tax_district', lookup_column_name = 'grid_coordinate' where import_lookup_id = 107;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_assessment_group', lookup_column_name = 'description' where import_lookup_id = 108;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_assessment_group', lookup_column_name = 'long_description' where import_lookup_id = 109;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'work_order_control', lookup_column_name = 'work_order_number' where import_lookup_id = 110;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'work_order_control', lookup_column_name = 'work_order_number' where import_lookup_id = 111;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_responsible_entity', lookup_column_name = 'description' where import_lookup_id = 118;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_responsible_entity', lookup_column_name = 'long_description' where import_lookup_id = 119;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_responsibility_type', lookup_column_name = 'description' where import_lookup_id = 120;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_responsibility_type', lookup_column_name = 'long_description' where import_lookup_id = 121;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_val_variable', lookup_column_name = 'description' where import_lookup_id = 122;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_val_variable', lookup_column_name = 'long_description' where import_lookup_id = 123;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_val_timeframe', lookup_column_name = 'description' where import_lookup_id = 124;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_val_timeframe', lookup_column_name = 'long_description' where import_lookup_id = 125;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_geography_type', lookup_column_name = 'description' where import_lookup_id = 126;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_geography_type', lookup_column_name = 'long_description' where import_lookup_id = 127;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_responsible_entity', lookup_column_name = 'external_code' where import_lookup_id = 128;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_parcel_responsible_entity', lookup_column_name = 'entity_number' where import_lookup_id = 129;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_reserve_factors', lookup_column_name = 'description' where import_lookup_id = 160;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_reserve_factors', lookup_column_name = 'long_description' where import_lookup_id = 161;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_statement_group', lookup_column_name = 'description' where import_lookup_id = 151;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_statement_group', lookup_column_name = 'description' where import_lookup_id = 152;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_statement_group', lookup_column_name = 'description' where import_lookup_id = 153;
update PT_IMPORT_LOOKUP set LOOKUP_TABLE_NAME = 'pt_statement_group', lookup_column_name = 'description' where import_lookup_id = 154;

update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 99;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 95;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 96;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 97;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 98;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 94;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 64;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 43;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 63;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, county_id' where import_lookup_id = 58;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 55;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, county_id' where import_lookup_id = 59;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 56;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, county_id' where import_lookup_id = 60;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 57;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 44;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 45;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, prop_tax_company_id' where import_lookup_id = 16;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, prop_tax_company_id' where import_lookup_id = 12;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, prop_tax_company_id' where import_lookup_id = 14;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, prop_tax_company_id' where import_lookup_id = 10;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, prop_tax_company_id, assessor_id' where import_lookup_id = 71;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, prop_tax_company_id, county_id' where import_lookup_id = 70;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, prop_tax_company_id' where import_lookup_id = 18;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, county_id' where import_lookup_id = 85;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 82;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, county_id' where import_lookup_id = 107;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 106;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, county_id' where import_lookup_id = 86;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 83;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, county_id' where import_lookup_id = 87;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 84;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 32;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 34;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'bus_segment_id' where import_lookup_id = 28;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'bus_segment_id' where import_lookup_id = 30;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'company_id' where import_lookup_id = 111;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id, prop_tax_company_id' where import_lookup_id = 152;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'state_id' where import_lookup_id = 153;
update PT_IMPORT_LOOKUP set LOOKUP_CONSTRAINING_COLUMNS = 'prop_tax_company_id' where import_lookup_id = 154;

-- Add the field on the columns table for tracking those columns actually present on the table (i.e., exclude those that are translate only).
alter table PT_IMPORT_COLUMN add IS_ON_TABLE number(22,0);

update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'address_1';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'address_2';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'assessor_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'assignment_indicator';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'city';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'company_grid_number';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'county_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'description';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_1';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_10';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_11';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_12';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_13';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_14';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_15';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_16';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_17';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_18';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_19';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_2';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_20';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_21';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_22';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_23';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_3';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_4';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_5';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_6';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_7';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_8';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'flex_9';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'grantor';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'grid_coordinate';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'improve_sq_ft';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'is_parcel_one_to_one';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'land_acreage';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'legal_description';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'legal_description_b';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'legal_description_c';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'legal_description_d';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'long_description';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'mineral_acreage';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'notes';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'parcel_number';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'parcel_type_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'prop_tax_company_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'retired_date';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'state_grid_number';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'state_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'tax_district_code';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'tax_district_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'type_code_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'use_composite_authority_yn';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'x_coordinate';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'y_coordinate';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 6 and COLUMN_NAME = 'zip_code';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'address_1';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'address_2';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'address_3';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'city';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'contact_name';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'description';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'entity_number';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'external_code';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'long_description';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'notes';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'phone_1';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'phone_2';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'prop_tax_company_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'state';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'tax_id_code';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'tax_id_number';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 14 and COLUMN_NAME = 'zip_code';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'assessor_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'county_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'entity_is_lessee';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'entity_is_lessor';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'lease_end_date';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'lease_start_date';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'parcel_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'parcel_type_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'percent_responsible';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'prop_tax_company_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'reference_number';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'responsibility_type_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'responsible_entity_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'state_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'tax_district_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 15 and COLUMN_NAME = 'tax_year';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'assessor_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'county_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'event_date';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'notes';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'parcel_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'parcel_type_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'prop_tax_company_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'state_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 16 and COLUMN_NAME = 'tax_district_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'assessor_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'county_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'geography_type_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'parcel_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'parcel_type_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'prop_tax_company_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'state_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 0 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'tax_district_id';
update PT_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID = 18 and COLUMN_NAME = 'value';

--
-- Miscellaneous.
--

-- Set the filter option for ledger and preallo adjustment imports.
update PT_IMPORT_TYPE set PP_REPORT_FILTER_ID = 7 where IMPORT_TYPE_ID = 8;
update PT_IMPORT_TYPE set PP_REPORT_FILTER_ID = 16 where IMPORT_TYPE_ID = 10;

-- Correct the parent table for depr group ID.
update PT_IMPORT_COLUMN set PARENT_TABLE = 'depr_group' where PARENT_TABLE = 'depreciation_group';

-- Delete the lookup for PT Adjustment.Long Description, since the property_tax_adjust table does not have a long description
delete from PT_IMPORT_COLUMN_LOOKUP where IMPORT_LOOKUP_ID = 40;
delete from PT_IMPORT_LOOKUP        where IMPORT_LOOKUP_ID = 40;

-- Correct the column type on columns with varchar instead of varchar2
update PT_IMPORT_COLUMN set COLUMN_TYPE = 'varchar2(4000)' where COLUMN_TYPE = 'varchar(4000)';

-- Create system option for the default value for the display option on the 'Import' workspace.
insert into PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Admin Center - Import - Default Row Display', sysdate, user, 'The default selection for the display of rows - All Rows or Only Rows with Errors.  This specifies the inital selection when the workspace is opened.', 0, 'All Rows', null, 1 );
insert into PTC_SYSTEM_OPTIONS_CENTERS ( SYSTEM_OPTION_ID, CENTER_NAME, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Row Display', 'admin', sysdate, user );
insert into PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Row Display', 'All Rows', sysdate, user );
insert into PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Row Display', 'Only Rows with Errors', sysdate, user );

-- Create system option for the default value for the review option on the 'Import' workspace.
insert into PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Admin Center - Import - Default Review Option', sysdate, user, 'The default selection for the reviewing the file before loading.  This specifies whether the user will be given the opportunity to review the data on the Import File tab before it is loaded into the database.', 0, 'No', null, 1 );
insert into PTC_SYSTEM_OPTIONS_CENTERS ( SYSTEM_OPTION_ID, CENTER_NAME, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Review Option', 'admin', sysdate, user );
insert into PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Review Option', 'Yes', sysdate, user );
insert into PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Review Option', 'No', sysdate, user );

-- Create system option for the default value for the 'Apply to All' option on the 'Import' workspace.
insert into PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Admin Center - Import - Default Apply Edits Option', sysdate, user, 'The default values for the ''Apply Edits to All Like Values'' option..  This specifies whether an edit made in the Import File will be applied to all rows with the same value.', 0, 'No', null, 1 );
insert into PTC_SYSTEM_OPTIONS_CENTERS ( SYSTEM_OPTION_ID, CENTER_NAME, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Apply Edits Option', 'admin', sysdate, user );
insert into PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Apply Edits Option', 'Yes', sysdate, user );
insert into PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Apply Edits Option', 'No', sysdate, user );

-- Create a table to store user edits so that they may be applied to future imports.
create table PT_IMPORT_TEMPLATE_EDITS
(
 IMPORT_TEMPLATE_ID number(22,0)   not null,
 SEQUENCE_NUMBER    number(22,0)   not null,
 USER_ID            varchar2(18),
 TIME_STAMP         date,
 COLUMN_NAME        varchar2(35)   not null,
 ORIGINAL_VALUE     varchar2(4000) not null,
 NEW_VALUE          varchar2(4000) not null,
 MADE_BY            varchar2(18),
 MADE_AT            date
);

alter table PT_IMPORT_TEMPLATE_EDITS
   add constraint PT_IMPT_TEMP_EDIT_PK
       primary key ( import_template_id, sequence_number )
       using index tablespace PWRPLANT_IDX;

alter table PT_IMPORT_TEMPLATE_EDITS
   add constraint PT_IMPT_TEMP_EDIT_TEMP_FK
       foreign key ( IMPORT_TEMPLATE_ID )
       references PT_IMPORT_TEMPLATE;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (144, 0, 10, 3, 5, 0, 10013, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010013_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
