/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047967_update_fx_views_remove_rounding_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan,Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/02/2017 Andrew Hill      Remove rounding from currency trans amounts
||============================================================================
*/
CREATE OR replace FORCE VIEW "PWRPLANT"."V_LS_PAYMENT_HDR_FX"
( "PAYMENT_ID", "LEASE_ID", "VENDOR_ID", "COMPANY_ID", "ORIGINAL_AMOUNT",
"AMOUNT", "DESCRIPTION", "GL_POSTING_MO_YR", "PAYMENT_STATUS_ID", "AP_STATUS_ID"
, "LS_ASSET_ID", "ILR_ID", "CONTRACT_CURRENCY_ID", "COMPANY_CURRENCY_ID",
"LS_CUR_TYPE", "EXCHANGE_DATE", "RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL" )
AS
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code,
                  1                                    historic_rate
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code,
                  0
           FROM   currency company_cur)
  SELECT lph.payment_id,
         lph.lease_id,
         lph.vendor_id,
         lph.company_id,
         lph.amount                           original_amount,
         lph.amount * Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                                          1)  amount,
         lph.description,
         lph.gl_posting_mo_yr,
         lph.payment_status_id,
         lph.ap_status_id,
         lph.ls_asset_id,
         lph.ilr_id,
         lease.contract_currency_id,
         cs.currency_id                       company_currency_id,
         cur.ls_cur_type                      AS ls_cur_type,
         Nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
         Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                             1)               rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_payment_hdr lph
         inner join ls_lease lease
                 ON lph.lease_id = lease.lease_id
         inner join currency_schema cs
                 ON lph.company_id = cs.company_id
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = lease.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         left outer join ls_lease_calculated_date_rates cr
                      ON ( cr.company_id = lph.company_id
                           AND cr.contract_currency_id =
                               lease.contract_currency_id
                           AND cr.company_currency_id = cs.currency_id
                           AND cr.accounting_month = lph.gl_posting_mo_yr )
  WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1;

CREATE OR replace FORCE VIEW "PWRPLANT"."V_LS_PAYMENT_LINE_FX"
("PAYMENT_ID", "PAYMENT_LINE_NUMBER", "PAYMENT_TYPE_ID", "LS_ASSET_ID", "AMOUNT"
, "GL_POSTING_MO_YR", "DESCRIPTION", "SET_OF_BOOKS_ID", "ADJUSTMENT_AMOUNT",
"CONTRACT_CURRENCY_ID", "COMPANY_CURRENCY_ID", "LS_CUR_TYPE", "EXCHANGE_DATE",
"RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL")
AS
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code,
                  1                                    historic_rate
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code,
                  0
           FROM   currency company_cur)
  SELECT lpl.payment_id,
         lpl.payment_line_number,
         lpl.payment_type_id,
         lpl.ls_asset_id,
         lpl.amount * Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                                          1)                      amount,
         lpl.gl_posting_mo_yr,
         lpl.description,
         lpl.set_of_books_id,
         Round(lpl.adjustment_amount * Decode(ls_cur_type, 2,
                                       Nvl(rate, historic_rate),
                                                           1), 2)
         adjustment_amount,
         lease.contract_currency_id,
         cs.currency_id
         company_currency_id,
         cur.ls_cur_type                                          AS ls_cur_type
         ,
         Nvl(cr.exchange_date, '01-Jan-1900')
         exchange_date,
         Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                             1)                                   rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_payment_line lpl
         inner join ls_payment_hdr lph
                 ON lph.payment_id = lpl.payment_id
         inner join ls_lease lease
                 ON lph.lease_id = lease.lease_id
         inner join currency_schema cs
                 ON lph.company_id = cs.company_id
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = lease.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         left outer join ls_lease_calculated_date_rates cr
                      ON ( cr.company_id = lph.company_id
                           AND cr.contract_currency_id =
                               lease.contract_currency_id
                           AND cr.company_currency_id = cs.currency_id
                           AND cr.accounting_month = lph.gl_posting_mo_yr )
  WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1;

CREATE OR replace FORCE VIEW "PWRPLANT"."V_LS_INVOICE_LINE_FX"
( "INVOICE_ID", "INVOICE_LINE_NUMBER", "PAYMENT_TYPE_ID", "LS_ASSET_ID",
"GL_POSTING_MO_YR", "AMOUNT", "DESCRIPTION", "NOTES", "CONTRACT_CURRENCY_ID",
"COMPANY_CURRENCY_ID", "LS_CUR_TYPE", "EXCHANGE_DATE", "RATE", "ISO_CODE",
"CURRENCY_DISPLAY_SYMBOL" )
AS
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code,
                  1                                    historic_rate
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code,
                  0
           FROM   currency company_cur)
  SELECT lil.invoice_id,
         lil.invoice_line_number,
         lil.payment_type_id,
         lil.ls_asset_id,
         lil.gl_posting_mo_yr,
         lil.amount * Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                                          1)  amount,
         lil.description,
         lil.notes,
         lease.contract_currency_id,
         cs.currency_id                       company_currency_id,
         cur.ls_cur_type                      AS ls_cur_type,
         Nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
         Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                             1)               rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_invoice_line lil
         inner join ls_invoice li
                 ON ( li.invoice_id = lil.invoice_id )
         inner join ls_lease lease
                 ON li.lease_id = lease.lease_id
         inner join currency_schema cs
                 ON li.company_id = cs.company_id
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = lease.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         left outer join ls_lease_calculated_date_rates cr
                      ON ( cr.company_id = li.company_id
                           AND cr.contract_currency_id =
                               lease.contract_currency_id
                           AND cr.company_currency_id = cs.currency_id
                           AND cr.accounting_month = lil.gl_posting_mo_yr )
  WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1;

CREATE OR replace FORCE VIEW "PWRPLANT"."V_LS_MONTHLY_TAX_FX"
( "LS_ASSET_ID", "COMPANY_ID", "GL_POSTING_MO_YR", "SET_OF_BOOKS_ID",
"TAX_LOCAL_ID", "ACCRUAL", "SCHEDULE_MONTH", "TAX_DISTRICT_ID", "TAX_RATE",
"AMOUNT", "ADJUSTMENT_AMOUNT", "TAX_BASE", "CONTRACT_CURRENCY_ID",
"COMPANY_CURRENCY_ID", "LS_CUR_TYPE", "EXCHANGE_DATE", "RATE", "ISO_CODE",
"CURRENCY_DISPLAY_SYMBOL" )
AS
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code
           FROM   currency company_cur),
       cr
       AS (SELECT /*+ materialize */ exchange_date,
                                     currency_from,
                                     currency_to,
                                     rate
           FROM   currency_rate_default a
           WHERE  exchange_date = (SELECT Max(exchange_date)
                                   FROM   currency_rate_default b
                                   WHERE  To_char(a.exchange_date, 'yyyymm') =
                                          To_char(b.exchange_date, 'yyyymm')
                                          AND a.currency_from = b.currency_from
                                          AND a.currency_to = b.currency_to)),
       calc_rate
       AS (SELECT /*+ materialize */ a.company_id,
                                     a.contract_currency_id,
                                     a.company_currency_id,
                                     a.accounting_month,
                                     a.exchange_date,
                                     a.rate,
                                     b.rate prev_rate
           FROM   ls_lease_calculated_date_rates a
                  left outer join ls_lease_calculated_date_rates b
                               ON a.company_id = b.company_id
                                  AND a.contract_currency_id =
                                      b.contract_currency_id
                                  AND a.accounting_month =
                                      Add_months(b.accounting_month,
                                      1))
  SELECT lmt.ls_asset_id,
         ass.company_id,
         lmt.gl_posting_mo_yr,
         lmt.set_of_books_id,
         lmt.tax_local_id,
         lmt.accrual,
         lmt.schedule_month,
         lmt.tax_district_id,
         lmt.rate                                       tax_rate,
         lmt.amount * Decode(ls_cur_type, 2, cr.rate,
                                          1)            amount,
         lmt.adjustment_amount * Decode(ls_cur_type, 2, cr.rate,
                                                     1) adjustment_amount,
         lmt.tax_base * Decode(ls_cur_type, 2, cr.rate,
                                            1)          tax_base,
         lease.contract_currency_id,
         cs.currency_id                                 company_currency_id,
         cur.ls_cur_type                                AS ls_cur_type,
         cr.exchange_date,
         Decode(ls_cur_type, 2, cr.rate,
                             1)                         rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_monthly_tax lmt
         inner join ls_asset ass
                 ON ass.ls_asset_id = lmt.ls_asset_id
         inner join currency_schema cs
                 ON ass.company_id = cs.company_id
         inner join ls_ilr ilr
                 ON ( ilr.ilr_id = ass.ilr_id
                      AND ilr.company_id = ass.company_id )
         inner join ls_lease lease
                 ON ( ilr.lease_id = lease.lease_id )
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = lease.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         inner join cr
                 ON cur.currency_id = cr.currency_to
                    AND lease.contract_currency_id = cr.currency_from
                    AND cr.exchange_date < Add_months(lmt.gl_posting_mo_yr, 1)
         left outer join calc_rate
                      ON lease.contract_currency_id =
                         calc_rate.contract_currency_id
                         AND cur.currency_id = calc_rate.company_currency_id
                         AND ass.company_id = calc_rate.company_id
                         AND lmt.gl_posting_mo_yr = calc_rate.accounting_month
  WHERE  cr.exchange_date = (SELECT Max(exchange_date)
                             FROM   cr cr2
                             WHERE  cr.currency_from = cr2.currency_from
                                    AND cr.currency_to = cr2.currency_to
                                    AND cr2.exchange_date <
                                        Add_months(lmt.gl_posting_mo_yr, 1))
         AND cs.currency_type_id = 1;

CREATE OR replace FORCE VIEW "PWRPLANT"."V_LS_MONTHLY_DEPR_STG_FX"
( "LS_ASSET_ID", "DEPRECIATION_EXPENSE", "GL_POSTING_MO_YR", "SET_OF_BOOKS_ID",
"CONTRACT_CURRENCY_ID", "COMPANY_CURRENCY_ID", "LS_CUR_TYPE", "EXCHANGE_DATE",
"RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL" )
AS
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code
           FROM   currency company_cur)
  SELECT lmds.ls_asset_id,
         lmds.depreciation_expense * Decode(ls_cur_type, 2, rate,
                                                         1) depreciation_expense
         ,
         lmds.gl_posting_mo_yr,
         lmds.set_of_books_id,
         la.contract_currency_id,
         cs.currency_id                                     company_currency_id,
         cur.ls_cur_type                                    AS ls_cur_type,
         cr.exchange_date,
         Decode(ls_cur_type, 2, rate,
                             1)                             rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_monthly_depr_stg lmds
         inner join ls_asset la
                 ON lmds.ls_asset_id = la.ls_asset_id
         inner join currency_schema cs
                 ON la.company_id = cs.company_id
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = la.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         inner join ls_lease_calculated_date_rates cr
                 ON ( cr.company_id = la.company_id
                      AND cr.contract_currency_id = la.contract_currency_id
                      AND cr.company_currency_id = cs.currency_id
                      AND cr.accounting_month = lmds.gl_posting_mo_yr )
  WHERE  cr.exchange_rate_type_id = 1;

CREATE OR replace FORCE VIEW "PWRPLANT"."V_LS_MONTHLY_ACCRUAL_STG_FX"
( "ACCRUAL_ID", "ACCRUAL_TYPE_ID", "LS_ASSET_ID", "AMOUNT", "GL_POSTING_MO_YR",
"SET_OF_BOOKS_ID", "CONTRACT_CURRENCY_ID", "COMPANY_CURRENCY_ID", "LS_CUR_TYPE",
"EXCHANGE_DATE", "RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL" )
AS
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code
           FROM   currency company_cur)
  SELECT lmas.accrual_id,
         lmas.accrual_type_id,
         lmas.ls_asset_id,
         lmas.amount * Decode(ls_cur_type, 2, rate,
                                           1) amount,
         lmas.gl_posting_mo_yr,
         lmas.set_of_books_id,
         la.contract_currency_id,
         cs.currency_id                       company_currency_id,
         cur.ls_cur_type                      AS ls_cur_type,
         cr.exchange_date,
         Decode(ls_cur_type, 2, rate,
                             1)               rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_monthly_accrual_stg lmas
         inner join ls_asset la
                 ON lmas.ls_asset_id = la.ls_asset_id
         inner join currency_schema cs
                 ON la.company_id = cs.company_id
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = la.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         inner join ls_lease_calculated_date_rates cr
                 ON ( cr.company_id = la.company_id
                      AND cr.contract_currency_id = la.contract_currency_id
                      AND cr.company_currency_id = cs.currency_id
                      AND cr.accounting_month = lmas.gl_posting_mo_yr )
  WHERE  cr.exchange_rate_type_id = 1;

CREATE OR replace FORCE VIEW "PWRPLANT"."V_LS_INVOICE_LINE_FX"
( "INVOICE_ID", "INVOICE_LINE_NUMBER", "PAYMENT_TYPE_ID", "LS_ASSET_ID",
"GL_POSTING_MO_YR", "AMOUNT", "DESCRIPTION", "NOTES", "CONTRACT_CURRENCY_ID",
"COMPANY_CURRENCY_ID", "LS_CUR_TYPE", "EXCHANGE_DATE", "RATE", "ISO_CODE",
"CURRENCY_DISPLAY_SYMBOL" )
AS
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code,
                  1                                    historic_rate
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code,
                  0
           FROM   currency company_cur)
  SELECT lil.invoice_id,
         lil.invoice_line_number,
         lil.payment_type_id,
         lil.ls_asset_id,
         lil.gl_posting_mo_yr,
         lil.amount * Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                                          1)  amount,
         lil.description,
         lil.notes,
         lease.contract_currency_id,
         cs.currency_id                       company_currency_id,
         cur.ls_cur_type                      AS ls_cur_type,
         Nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
         Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                             1)               rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_invoice_line lil
         inner join ls_invoice li
                 ON ( li.invoice_id = lil.invoice_id )
         inner join ls_lease lease
                 ON li.lease_id = lease.lease_id
         inner join currency_schema cs
                 ON li.company_id = cs.company_id
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = lease.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         left outer join ls_lease_calculated_date_rates cr
                      ON ( cr.company_id = li.company_id
                           AND cr.contract_currency_id =
                               lease.contract_currency_id
                           AND cr.company_currency_id = cs.currency_id
                           AND cr.accounting_month = lil.gl_posting_mo_yr )
  WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1; 

  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3621, 0, 2017, 1, 0, 0, 47967, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047967_update_fx_views_remove_rounding_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  
  