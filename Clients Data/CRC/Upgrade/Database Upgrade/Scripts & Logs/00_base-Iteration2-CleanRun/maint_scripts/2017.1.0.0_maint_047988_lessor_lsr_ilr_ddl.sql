 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_047988_lessor_lsr_ilr_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 09/05/2017 Shane "C" Ward    New tables for Lessor ILRs
 ||============================================================================
 */

CREATE TABLE lsr_ilr (
  ilr_id            NUMBER(22,0)   NOT NULL,
  ilr_number        VARCHAR2(35)   NOT NULL,
  lease_id          NUMBER(22,0)   NOT NULL,
  company_id        NUMBER(22,0)   NOT NULL,
  est_in_svc_date   DATE           NOT NULL,
  external_ilr      VARCHAR2(35)   NULL,
  ilr_status_id     NUMBER(22,0)   NOT NULL,
  ilr_group_id      NUMBER(22,0)   NULL,
  notes             VARCHAR2(4000) NULL,
  current_revision  NUMBER(22,0)   NULL,
  workflow_type_id  NUMBER(22,0)   NULL,
  rate_group_id     NUMBER(22,0)   NULL,
  funding_status_id NUMBER(1,0)    DEFAULT 0 NULL,
  time_stamp        DATE           NULL,
  user_id           VARCHAR2(18)   NULL
);

ALTER TABLE lsr_ilr
  ADD CONSTRAINT pk_lsr_ilr PRIMARY KEY (
    ilr_id
  );

ALTER TABLE lsr_ilr
  ADD CONSTRAINT lsr_ilr_number_company_uk UNIQUE (
    ilr_number,
    company_id
  );

ALTER TABLE lsr_ilr
  ADD CONSTRAINT r_lsr_ilr1 FOREIGN KEY (
    ilr_status_id
  ) REFERENCES ls_ilr_status (
    ilr_status_id
  );

ALTER TABLE lsr_ilr
  ADD CONSTRAINT r_lsr_ilr2 FOREIGN KEY (
    ilr_group_id
  ) REFERENCES lsr_ilr_group (
    ilr_group_id
  );

ALTER TABLE lsr_ilr
  ADD CONSTRAINT r_lsr_ilr3 FOREIGN KEY (
    funding_status_id
  ) REFERENCES ls_funding_status (
    funding_status_id
  );


COMMENT ON TABLE lsr_ilr IS '(O)  [06]
The Lessor ILR table contains Individual Lease Records that serve as the legal documents between the Lessor and the lessee company.  ILR''s are the vehicle used to request payment of vendor invoices from the Lessor.  Additionally, monthly invoices from the Lessor are presented at the ILR level.  From a hierarchical standpoint, several leased assets may possess the same ILR, and a master lease agreement may possess numerous ILR''s.';

COMMENT ON COLUMN lsr_ilr.ilr_id IS 'System-assigned identifier of a specific Individual Lease Record.';
COMMENT ON COLUMN lsr_ilr.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_ilr.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_ilr.ilr_number IS 'Contains the legal ILR number, as accepted by the Lessor and the lessee.';
COMMENT ON COLUMN lsr_ilr.lease_id IS 'System-assigned identifier of a particular lease.';
COMMENT ON COLUMN lsr_ilr.company_id IS 'System-assigned identifier of a particular lease company.';
COMMENT ON COLUMN lsr_ilr.est_in_svc_date IS 'Serves as an estimate of when the ILR''s final asset will be placed in service.';
COMMENT ON COLUMN lsr_ilr.external_ilr IS 'Contains a referential pointer to the ILR, the External ILR can be used to store its identifier.';
COMMENT ON COLUMN lsr_ilr.ilr_status_id IS 'System-assigned identifier of a particular ILR status.';
COMMENT ON COLUMN lsr_ilr.ilr_group_id IS 'Refers to a specific ilr group.';
COMMENT ON COLUMN lsr_ilr.notes IS 'A freeform field for entering notes.';
COMMENT ON COLUMN lsr_ilr.current_revision IS 'The current revision.';
COMMENT ON COLUMN lsr_ilr.workflow_type_id IS 'An internal id used for routing records to approval.';
COMMENT ON COLUMN lsr_ilr.rate_group_id IS 'The rate group associated with the ILR.  The rate groups are defined on the ls_lease_rate_group table';
COMMENT ON COLUMN lsr_ilr.funding_status_id IS 'Funding status to determine state of components related to ILR.';

--Lessor ILR Approval
CREATE TABLE lsr_ilr_approval (
  ilr_id             NUMBER(22,0)  NOT NULL,
  revision           NUMBER(22,0)  NOT NULL,
  time_stamp         DATE          NULL,
  user_id            VARCHAR2(18)  NULL,
  approval_type_id   NUMBER(22,0)  NULL,
  approval_status_id NUMBER(22,0)  NULL,
  approver           VARCHAR2(18)  NULL,
  approval_date      DATE          NULL,
  rejected           NUMBER(1,0)   NULL,
  revision_desc      VARCHAR2(35)  NULL,
  revision_long_desc VARCHAR2(254) NULL
)
;

ALTER TABLE lsr_ilr_approval
  ADD CONSTRAINT pk_lsr_ilr_approval PRIMARY KEY (
    ilr_id,
    revision
  )
;

ALTER TABLE lsr_ilr_approval
  ADD CONSTRAINT fk1_lsr_ilr_approval FOREIGN KEY (
    ilr_id
  ) REFERENCES lsr_ilr (
    ilr_id
  );

COMMENT ON TABLE lsr_ilr_approval IS '(C)  [06]
The Lessor ILR Approval table maintains the status of ILR approvals by revision.';

COMMENT ON COLUMN lsr_ilr_approval.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN lsr_ilr_approval.revision IS 'The revision.';
COMMENT ON COLUMN lsr_ilr_approval.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_approval.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_approval.approval_type_id IS 'The approval type.';
COMMENT ON COLUMN lsr_ilr_approval.approval_status_id IS 'Whether or not the revision is initiated, pending approval, approved, or rejected.';
COMMENT ON COLUMN lsr_ilr_approval.approver IS 'User or group approving.';
COMMENT ON COLUMN lsr_ilr_approval.approval_date IS 'Date approver approved.';
COMMENT ON COLUMN lsr_ilr_approval.rejected IS 'If the approver rejects the revision.';
COMMENT ON COLUMN lsr_ilr_approval.revision_desc IS 'A description for the revision.';
COMMENT ON COLUMN lsr_ilr_approval.revision_long_desc IS 'A longer description for the revision.';


CREATE TABLE lsr_ilr_document (
  ilr_id        NUMBER(22,0)   NOT NULL,
  document_data BLOB           NULL,
  document_id   NUMBER(22,0)   NOT NULL,
  notes         VARCHAR2(2000) NULL,
  description   VARCHAR2(35)   NULL,
  file_name     VARCHAR2(100)  NOT NULL,
  time_stamp    DATE           NULL,
  user_id       VARCHAR2(18)   NULL,
  filesize      NUMBER(22,0)   NULL,
  file_link     VARCHAR2(2000) NULL
)
;

ALTER TABLE lsr_ilr_document
  ADD CONSTRAINT pk_lsr_ilr_document PRIMARY KEY (
    ilr_id,
    document_id
 );
 
 ALTER TABLE lsr_ilr_document
  ADD CONSTRAINT fk1_lsr_ilr_document FOREIGN KEY (
    ilr_id
  ) REFERENCES lsr_ilr (
    ilr_id
  );

COMMENT ON TABLE lsr_ilr_document IS '(S)  [06]
The Lessor ILR Document table is a table that holds documents for ILRs.';

COMMENT ON COLUMN lsr_ilr_document.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN lsr_ilr_document.document_data IS 'The stored document.';
COMMENT ON COLUMN lsr_ilr_document.document_id IS 'The internal document id within PowerPlant.';
COMMENT ON COLUMN lsr_ilr_document.notes IS 'A freeform field for entering notes.';
COMMENT ON COLUMN lsr_ilr_document.description IS 'a description field for the table.';
COMMENT ON COLUMN lsr_ilr_document.file_name IS 'The file name.';
COMMENT ON COLUMN lsr_ilr_document.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_document.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_document.filesize IS 'The size of the file.';
COMMENT ON COLUMN lsr_ilr_document.file_link IS 'A link to an external file.';

CREATE TABLE lsr_ilr_class_code (
  class_code_id NUMBER(22,0)  NOT NULL,
  ilr_id        NUMBER(22,0)  NOT NULL,
  time_stamp    DATE          NULL,
  user_id       VARCHAR2(18)  NULL,
  value         VARCHAR2(254) NOT NULL
)
;

ALTER TABLE lsr_ilr_class_code
  ADD CONSTRAINT pk_lsr_ilr_class_code PRIMARY KEY (
    ilr_id,
    class_code_id
  );
ALTER TABLE lsr_ilr_class_code
  ADD CONSTRAINT fk1_lsr_ilr_class_code FOREIGN KEY (
    ilr_id
  ) REFERENCES lsr_ilr (
    ilr_id
  );

ALTER TABLE lsr_ilr_class_code
  ADD CONSTRAINT fk2_lsr_ilr_class_code FOREIGN KEY (
    class_code_id
  ) REFERENCES class_code (
    class_code_id
  );


COMMENT ON TABLE lsr_ilr_class_code IS '(S)  [06]
The Lessor ILR Class Code table is a class coded table for ILRs.';

COMMENT ON COLUMN lsr_ilr_class_code.class_code_id IS 'The internal class code id within PowerPlant .';
COMMENT ON COLUMN lsr_ilr_class_code.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN lsr_ilr_class_code.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_class_code.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_class_code.value IS 'The class code value .';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3701, 0, 2017, 1, 0, 0, 47988, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047988_lessor_lsr_ilr_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
