/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038906_version-updates_proptax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.2 07/11/2014 Lee Quinn        10.4.2.2 version
||============================================================================
*/

update PP_VERSION
   set PROP_TAX_VERSION = '10.4.2.2'
where PP_VERSION_ID = 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1282, 0, 10, 4, 2, 2, 38906, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038906_version-updates_proptax.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'),
    SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
