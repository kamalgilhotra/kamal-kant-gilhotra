 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_049047_lease_ilr_mla_sublease_fk_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 09/21/2017 Shane "C" Ward  Add FK for Sublease on ILR and
 ||
 ||============================================================================
 */

ALTER TABLE lsr_ilr_options DROP CONSTRAINT fk_lsr_ilr_sublease;
ALTER TABLE ls_ilr_options DROP CONSTRAINT fk_ls_ilr_sublease;

ALTER TABLE LSR_ILR_OPTIONS
  ADD CONSTRAINT fk_lsr_ilr_sublease FOREIGN KEY ( sublease_id ) REFERENCES LS_LEASE ( lease_id );

ALTER TABLE LS_ILR_OPTIONS
  ADD CONSTRAINT fk_ls_ilr_sublease FOREIGN KEY ( sublease_id ) REFERENCES LS_LEASE ( lease_id );

ALTER TABLE LSR_LEASE_OPTIONS
  ADD CONSTRAINT fk_lsr_lease_sublease FOREIGN KEY ( sublease_lease_id ) REFERENCES LS_LEASE ( lease_id );

  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3733, 0, 2017, 1, 0, 0, 49047, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049047_lease_ilr_mla_sublease_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  