/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052953_lessee_01_sys_control_description_updates_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.1 01/11/2019 Sarah Byers    Clean up system control description values
||============================================================================
*/

update pp_system_control
   set description = null
 where lower(trim(control_name)) in ('lease consolidated bs curr type', 'lease consolidated is curr type', 'lease specific gl trans status');
 
 update pp_system_control
   set description = 'dw_yes_no;0'
 where lower(trim(control_name)) in ('lease fast payment approvals', 'lease catch up principal', 'hide lease department in cpr panel', 'lease debug execute immediate');

 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13822, 0, 2018, 2, 0, 0, 52953, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052953_lessee_01_sys_control_description_updates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
