/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053179_lessee_04_add_uop_action_to_table_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/29/2018 Charlie Shilling Need to add new action to Lessee Asset dropdown
||============================================================================
*/
INSERT INTO ppbase_actions_windows (id, MODULE, action_identifier, action_text, action_order, action_event)
SELECT *
FROM (SELECT
   		Nvl(Max(id), 0) + 1 AS control_id,
   		'LESSEE',
   		'view_uop',
   		'View Asset UOP',
   		9,
   		'ue_viewUOP'
	FROM ppbase_actions_windows
)
WHERE NOT EXISTS(
   SELECT 1
   FROM ppbase_actions_windows
   WHERE action_identifier = 'view_uop')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6162, 0, 2017, 4, 0, 0, 53179, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_053179_lessee_04_add_uop_action_to_table_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;