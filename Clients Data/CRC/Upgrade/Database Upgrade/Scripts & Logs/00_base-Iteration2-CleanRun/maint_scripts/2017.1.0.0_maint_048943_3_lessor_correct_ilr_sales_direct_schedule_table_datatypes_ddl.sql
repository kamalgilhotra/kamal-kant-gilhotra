/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048943_3_lessor_correct_ilr_sales_direct_schedule_table_datatypes_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/09/2017 Andrew Hill      Correct data types on lsr_ilr_schedule_sales_direct
||============================================================================
*/

--This is a new table and shouldn't have any data loaded during the execution of upgrade scripts
DELETE FROM lsr_ilr_schedule_sales_direct;

ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY principal_received NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY principal_accrued NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY beg_long_term_receivable NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY end_long_term_receivable NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY beg_unguaranteed_residual NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY interest_unguaranteed_residual NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY ending_unguaranteed_residual NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY beg_net_investment NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY interest_net_investment NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY ending_net_investment NUMBER(22,2);
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY calculated_initial_rate NUMBER(22,8); 
ALTER TABLE lsr_ilr_schedule_sales_direct MODIFY calculated_rate number(22,8);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3774, 0, 2017, 1, 0, 0, 48943, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048943_3_lessor_correct_ilr_sales_direct_schedule_table_datatypes_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;