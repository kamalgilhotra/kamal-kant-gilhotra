/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010464_sys.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   10/08/2012 Alex Pivoshenko Universal Filter
||============================================================================
*/

create table PP_DYNAMIC_FILTER
(
 FILTER_ID              number(22,0) not null,
 LABEL                  varchar2(35) not null,
 INPUT_TYPE             varchar2(35) not null,
 SQLS_COLUMN_EXPRESSION varchar2(254),
 SQLS_WHERE_CLAUSE      varchar2(2000),
 DW                     varchar2(254),
 DW_ID                  varchar2(35),
 DW_DESCRIPTION         varchar2(35),
 DW_ID_DATATYPE         varchar2(1),
 REQUIRED               number(1,0),
 SINGLE_SELECT_ONLY     number(1,0),
 VALID_OPERATORS        varchar2(35),
 DATA                   varchar2(1),
 USER_ID                varchar2(18),
 TIME_STAMP             date,
 EXCLUDE_FROM_WHERE     number(1,0),
 NOT_RETRIEVE_IMMEDIATE number(1,0)
);

alter table PP_DYNAMIC_FILTER
   add constraint PK_DYNAMIC_FILTER_ID
       primary key (FILTER_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_DYNAMIC_FILTER
   add constraint FK_PPDF_SINGLE_SELECT_ONLY
       foreign key (SINGLE_SELECT_ONLY)
       references YES_NO;

alter table PP_DYNAMIC_FILTER
   add constraint FK_PPDF_REQUIRED
       foreign key (REQUIRED)
       references YES_NO;

alter table PP_DYNAMIC_FILTER
   add constraint FK_PPDF_EXCLUDE_WHERE
       foreign key (EXCLUDE_FROM_WHERE)
       references YES_NO;

alter table PP_DYNAMIC_FILTER
   add constraint FK_PPDF_NOT_RETRIEVE
       foreign key (NOT_RETRIEVE_IMMEDIATE)
       references YES_NO;

create table PP_DYNAMIC_FILTER_MAPPING
(
 PP_REPORT_FILTER_ID number(22,0) not null,
 FILTER_ID           number(22,0) not null,
 USER_ID             varchar2(18),
 TIME_STAMP          date
);

alter table PP_DYNAMIC_FILTER_MAPPING
   add constraint PK_PPDFM_FILTER_IDS
       primary key (PP_REPORT_FILTER_ID, FILTER_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_DYNAMIC_FILTER_MAPPING
   add constraint FK_PDFM_PP_REPORT_FILTER_ID
       foreign key (PP_REPORT_FILTER_ID)
       references PP_REPORTS_FILTER;

alter table PP_DYNAMIC_FILTER_MAPPING
   add constraint FK_PDFM_FILTER_ID
       foreign key (FILTER_ID)
       references PP_DYNAMIC_FILTER;

create table PP_DYNAMIC_FILTER_RESTRICTIONS
(
 RESTRICTION_ID         number(22,0) not null,
 FILTER_ID              number(22,0) not null,
 RESTRICT_BY_FILTER_ID  number(22,0) not null,
 SQLS_COLUMN_EXPRESSION varchar2(254),
 SQLS_WHERE_CLAUSE      varchar2(2000),
 USER_ID                varchar2(18),
 TIME_STAMP             date
);

alter table PP_DYNAMIC_FILTER_RESTRICTIONS
   add constraint PK_DYNAMIC_FILTER_RESTRICTIONS
       primary key (RESTRICTION_ID)
       using index tablespace PWRPLANT_IDX;

create global temporary table PP_DYNAMIC_FILTER_VALUES
(
 LABEL           varchar2(35) not null,
 OPERATOR        varchar2(35),
 START_VALUE     varchar2(512) not null,
 END_VALUE       varchar2(35),
 BATCH_REPORT_ID number(22,0),
 USER_ID         varchar2(18),
 TIME_STAMP      date
) on commit preserve rows;

alter table PP_DYNAMIC_FILTER_VALUES
   add constraint PK_DYNAMIC_FILTER_VALUES
       primary key (LABEL);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (296, 0, 10, 4, 0, 0, 10464, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_010464_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;