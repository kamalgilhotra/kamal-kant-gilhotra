/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050218_lessor_02_remove_days_in_month_sw_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.3.0.0  05/03/2018 Sisouphanh       Remove Days in Year from the MLA Import
||============================================================================
*/

DECLARE
  l_id number;
BEGIN
  FOR FIELD IN (SELECT import_template_id, field_id
                FROM pp_import_template_fields
                WHERE LOWER(TRIM(column_name)) = 'days_in_month_sw'
                AND import_type_id = 501)
  LOOP
    DELETE FROM pp_import_template_fields
    WHERE import_template_id = FIELD.import_template_id
    AND field_id = field.field_id;
    
    UPDATE pp_import_template_fields
    SET field_id = field_id - 1
    WHERE import_template_id = FIELD.import_template_id
    AND field_id > FIELD.field_id;
  END LOOP;
END;
/

DELETE FROM pp_import_column
WHERE LOWER(TRIM(column_name)) = 'days_in_month_sw'
AND import_type_id = 501;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5183, 0, 2017, 3, 0, 0, 50218, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050218_lessor_02_remove_days_in_month_sw_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;