/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009025_web_tables.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/12/2012 Roger Roach    Point Release
||============================================================================
*/

create table PP_WEB_DATATYPES
(
 NAME   varchar2(200) not null,
 EXT    varchar2(10),
 BINARY number(22,0)
);

alter table PP_WEB_DATATYPES
   add constraint PK_PP_WEB_DATATYPES
       primary key (NAME)
       using index tablespace PWRPLANT_IDX;


create table PP_WEB_DATA
(
 NAME     varchar2(200) not null,
 DATATYPE varchar2(100),
 DATALEN  number(22),
 DATA     blob,
 TEXTDATA clob
);

alter table PP_WEB_DATA
   add constraint PK_PP_WEB_DATA
       primary key (NAME)
       using index tablespace PWRPLANT_IDX;

create table PP_WEB_SITES
(
 NAME varchar2(200) not null,
 SITE varchar(2000)
);

alter table PP_WEB_SITES
   add constraint PK_PP_WEB_SITES
       primary key (NAME)
       using index tablespace PWRPLANT_IDX;


insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('text/cmd', 'cmd', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('text/css', 'css', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('text/csv', 'csv', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('text/htm', 'htm', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('text/html', 'html', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('application/javascript', 'js', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('text/plain', 'txt', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('text/xml', 'xml', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('image/gif', 'gif', 1);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('image/jpeg', 'jpg', 1);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('image/png', 'pbg', 1);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('image/vnd.microsoft.icon', 'ico', 1);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('image/tiff', 'tif', 1);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('application/pdf', 'pdf', 1);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('application/postscript', 'ps', 0);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('application/x-shockware-flash', 'swf', 1);
insert into PP_WEB_DATATYPES (NAME, EXT, BINARY) values ('application/json', 'json', 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (113, 0, 10, 3, 4, 0, 9025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009025_web_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;