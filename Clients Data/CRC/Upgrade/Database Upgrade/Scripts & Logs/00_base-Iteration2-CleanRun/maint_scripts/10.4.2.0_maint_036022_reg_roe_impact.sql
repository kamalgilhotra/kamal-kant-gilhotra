/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036022_reg_roe_impact.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 02/07/2014 Shane "C" Ward
||============================================================================
*/

create global temporary table REG_MONITOR_CONTROL_ADJ_TEMP
(
 REG_CASE_ID         number(22,0) not null,
 REG_RETURN_TAG_ID   number(22,0) not null,
 ADJUSTMENT_ID       number(22,0) not null
) on commit delete rows;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (954, 0, 10, 4, 2, 0, 36022, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036022_reg_roe_impact.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;