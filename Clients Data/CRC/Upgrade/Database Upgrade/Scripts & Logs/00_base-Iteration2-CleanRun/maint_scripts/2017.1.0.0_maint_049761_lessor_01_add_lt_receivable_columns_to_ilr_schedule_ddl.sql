/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049761_lessor_01_add_lt_receivable_columns_to_ilr_schedule_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/09/2017 Andrew Hill      Add lt receivable columns to ilr schedule (in expected order)
||============================================================================
*/

DROP MATERIALIZED VIEW mv_lsr_ilr_mc_schedule_amounts;
drop materialized view log on lsr_ilr_schedule;

ALTER TABLE lsr_ilr_schedule RENAME TO lsr_ilr_schedule_old;

CREATE TABLE lsr_ilr_schedule ( ilr_id                         NUMBER(22,0) NOT NULL ENABLE,
                                revision                       NUMBER(22,0) NOT NULL ENABLE,
                                set_of_books_id                NUMBER(22,0) NOT NULL ENABLE,
                                month                          DATE NOT NULL ENABLE,
                                user_id                        VARCHAR2(18 BYTE),
                                time_stamp                     DATE,
                                interest_income_received       NUMBER(22,2) NOT NULL ENABLE,
                                interest_income_accrued        NUMBER(22,2) NOT NULL ENABLE,
                                interest_rental_recvd_spread   NUMBER(22,2) NOT NULL ENABLE,
                                beg_deferred_rev               NUMBER(22,2) NOT NULL ENABLE,
                                deferred_rev_activity          NUMBER(22,2) NOT NULL ENABLE,
                                end_deferred_rev               NUMBER(22,2) NOT NULL ENABLE,
                                beg_receivable                 NUMBER(22,2) NOT NULL ENABLE,
                                end_receivable                 NUMBER(22,2) NOT NULL ENABLE,
                                beg_lt_receivable              NUMBER(22,2) NOT NULL ENABLE,
                                end_lt_receivable              NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual1             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual2             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual3             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual4             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual5             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual6             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual7             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual8             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual9             NUMBER(22,2) NOT NULL ENABLE,
                                executory_accrual10            NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid1                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid2                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid3                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid4                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid5                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid6                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid7                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid8                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid9                NUMBER(22,2) NOT NULL ENABLE,
                                executory_paid10               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual1            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual2            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual3            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual4            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual5            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual6            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual7            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual8            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual9            NUMBER(22,2) NOT NULL ENABLE,
                                contingent_accrual10           NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid1               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid2               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid3               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid4               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid5               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid6               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid7               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid8               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid9               NUMBER(22,2) NOT NULL ENABLE,
                                contingent_paid10              NUMBER(22,2) NOT NULL ENABLE);
                                
COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."ILR_ID" IS
  'System-assigned identifier of a particular ILR.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."REVISION" IS
  'The revision.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."SET_OF_BOOKS_ID" IS
  'The internal set of books id within PowerPlant.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."MONTH" IS
  'The month being processed.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."USER_ID" IS
  'Standard System-assigned user id used for audit purposes.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."TIME_STAMP" IS
  'Standard System-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."INTEREST_INCOME_RECEIVED" IS
  'The interest/income received in this period';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."INTEREST_INCOME_ACCRUED" IS
  'The interest/income accrued as of this period';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."INTEREST_RENTAL_RECVD_SPREAD" IS
  'The interest/rental received spread of this period';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."BEG_DEFERRED_REV" IS
  'The deferred revenue as of the start of the period';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."DEFERRED_REV_ACTIVITY" IS
  'The deferred revenue activity in this period';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."END_DEFERRED_REV" IS
  'The total deferred revenue as of the end of this period';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."BEG_RECEIVABLE" IS
  'The receivable amount as of the beginning of this period';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."END_RECEIVABLE" IS
  'The receivable amount as of the end of this period';
  
COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."BEG_LT_RECEIVABLE" IS
  'The long-term receivable amount as of the beginning of this period';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."END_LT_RECEIVABLE" IS
  'The long-term receivable amount as of the end of this period';  

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL1" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL2" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL3" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL4" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL5" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL6" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL7" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL8" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL9" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_ACCRUAL10" IS
  'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID1" IS
  'The executory paid amount associated with the executory bucket number 1.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID2" IS
  'The executory paid amount associated with the executory bucket number 2.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID3" IS
  'The executory paid amount associated with the executory bucket number 3.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID4" IS
  'The executory paid amount associated with the executory bucket number 4.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID5" IS
  'The executory paid amount associated with the executory bucket number 5.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID6" IS
  'The executory paid amount associated with the executory bucket number 6.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID7" IS
  'The executory paid amount associated with the executory bucket number 7.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID8" IS
  'The executory paid amount associated with the executory bucket number 8.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID9" IS
  'The executory paid amount associated with the executory bucket number 9.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."EXECUTORY_PAID10" IS
  'The executory paid amount associated with the executory bucket number 10.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL1" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL2" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL3" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL4" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL5" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL6" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL7" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL8" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL9" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_ACCRUAL10" IS
  'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID1" IS
  'The contingent paid amount associated with the contingent bucket number 1.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID2" IS
  'The contingent paid amount associated with the contingent bucket number 2.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID3" IS
  'The contingent paid amount associated with the contingent bucket number 3.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID4" IS
  'The contingent paid amount associated with the contingent bucket number 4.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID5" IS
  'The contingent paid amount associated with the contingent bucket number 5.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID6" IS
  'The contingent paid amount associated with the contingent bucket number 6.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID7" IS
  'The contingent paid amount associated with the contingent bucket number 7.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID8" IS
  'The contingent paid amount associated with the contingent bucket number 8.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID9" IS
  'The contingent paid amount associated with the contingent bucket number 9.';

COMMENT ON COLUMN "LSR_ILR_SCHEDULE"."CONTINGENT_PAID10" IS
  'The contingent paid amount associated with the contingent bucket number 10.';

COMMENT ON TABLE "LSR_ILR_SCHEDULE" IS
  '(C)  [06] The ILR Schedule table records the ILR''s payment schedule.  The ILR''s schedule is allocated to its leased assets as part of the monthly lease expense calc process.'
;
                                
ALTER TABLE lsr_ilr_schedule_sales_direct DROP CONSTRAINT r_lsr_ilr_sched_sales_direct3;

ALTER TABLE lsr_ilr_schedule_old DROP CONSTRAINT pk_lsr_ilr_schedule;
ALTER TABLE lsr_ilr_schedule ADD CONSTRAINT pk_lsr_ilr_schedule PRIMARY KEY ( ilr_id,revision,set_of_books_id,MONTH ) USING INDEX TABLESPACE pwrplant_idx;
ALTER TABLE lsr_ilr_schedule_old DROP CONSTRAINT r_lsr_ilr_schedule1;
ALTER TABLE lsr_ilr_schedule ADD CONSTRAINT r_lsr_ilr_schedule1 FOREIGN KEY ( ilr_id,revision ) REFERENCES pwrplant.lsr_ilr_options ( ilr_id,revision );
ALTER TABLE lsr_ilr_schedule_old DROP CONSTRAINT r_lsr_ilr_schedule2;
ALTER TABLE lsr_ilr_schedule ADD CONSTRAINT r_lsr_ilr_schedule2 FOREIGN KEY ( set_of_books_id ) REFERENCES PWRPLANT.SET_OF_BOOKS ( SET_OF_BOOKS_ID );

COMMENT ON COLUMN PWRPLANT.LSR_ILR_SCHEDULE.ILR_ID IS
  'System-assigned identifier of a particular ILR.';

COMMENT ON COLUMN PWRPLANT.LSR_ILR_SCHEDULE.REVISION IS
  'The revision.';

COMMENT ON COLUMN PWRPLANT.LSR_ILR_SCHEDULE.SET_OF_BOOKS_ID IS
  'The internal set of books id within PowerPlant.';

COMMENT ON COLUMN PWRPLANT.LSR_ILR_SCHEDULE.MONTH IS
  'The month being processed.';

DECLARE
  cnt number;
BEGIN

  SELECT COUNT(1) INTO cnt
  FROM all_indexes
  WHERE owner = 'PWRPLANT'
  AND index_name = 'LSR_ILR_SCHEDULE_MONTH_IDX';

  IF cnt = 1 THEN
    execute immediate 'drop index lsr_ilr_schedule_month_idx';
  END IF;
END;
/

CREATE INDEX lsr_ilr_schedule_month_idx ON lsr_ilr_schedule ( MONTH ) TABLESPACE pwrplant_idx;

drop index lsr_ilr_schedule_tmth_idx;
CREATE INDEX lsr_ilr_schedule_tmth_idx ON lsr_ilr_schedule ( TRUNC(MONTH,'fmmonth') ) TABLESPACE pwrplant_idx;

DROP INDEX lsr_ilr_sch_ilr_rev_mth_idx;
CREATE INDEX lsr_ilr_sch_ilr_rev_mth_idx ON lsr_ilr_schedule (ilr_id, revision, MONTH) tablespace pwrplant_idx;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3903, 0, 2017, 1, 0, 0, 49761, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049761_lessor_01_add_lt_receivable_columns_to_ilr_schedule_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
