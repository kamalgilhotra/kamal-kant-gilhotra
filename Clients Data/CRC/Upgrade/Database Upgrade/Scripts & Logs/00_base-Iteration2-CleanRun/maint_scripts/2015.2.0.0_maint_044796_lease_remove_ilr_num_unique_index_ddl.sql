/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044796_lease_remove_ilr_num_unique_index_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2.0   08/24/2015 Will Davis       Allow duplicate ILR numbers
||============================================================================
*/


DECLARE
counter number;
begin
select count(1)
into counter
from user_indexes
where index_name = 'LS_ILR_NUM_NDX'
and table_name = 'LS_ILR';
if counter = 1 then
  execute immediate 'drop index LS_ILR_NUM_NDX';
end if;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2822, 0, 2015, 2, 0, 0, 044796, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044796_lease_remove_ilr_num_unique_index_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;