/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049760_aro_add_reg_entry_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.1.0.0 03/08/2018 Colin T		  Add Accretion Adjustment reg entry type.
||============================================================================
*/

merge INTO REGULATORY_ENTRY_TYPE a
USING ( SELECT 6                          entry_type_id,
               'Accretion Adjust Expense' description,
               'ARO_LIABILITY'            table_name,
               'MONTH_YR'                 month_column,
               'ACCRETION_ADJUST'         column_expression,
               'EXPENSE'                  expense_asset,
               'ARO'                      aro_depr
        FROM   DUAL ) b
ON (a.entry_type_id = b.entry_type_id)
WHEN matched THEN
  UPDATE SET a.description = b.description,
             a.table_name = b.table_name,
             a.month_column = b.month_column,
             a.column_expression = b.column_expression,
             a.expense_asset = b.expense_asset,
             a.aro_depr = b.aro_depr
WHEN NOT matched THEN
  INSERT (a.entry_type_id,
          a.description,
          a.table_name,
          a.month_column,
          a.column_expression,
          a.expense_asset,
          a.aro_depr)
  VALUES (b.entry_type_id,
          b.description,
          b.table_name,
          b.month_column,
          b.column_expression,
          b.expense_asset,
          b.aro_depr); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4173, 0, 2018, 1, 0, 0, 049760, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_049760_aro_add_reg_entry_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;