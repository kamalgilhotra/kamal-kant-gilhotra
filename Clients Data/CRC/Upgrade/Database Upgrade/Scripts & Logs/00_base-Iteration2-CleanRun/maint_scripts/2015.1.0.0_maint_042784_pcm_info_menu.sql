/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042784_pcm_info_menu.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/09/2015 Alex P.          Differentiate between Info(WO) and Info(FP)
||============================================================================
*/

update ppbase_workspace
set label = 'Information (Prog)'
where workspace_identifier = 'prog_maint_details'
and module = 'pcm';

update ppbase_workspace
set label = 'Information (FP)'
where workspace_identifier = 'fp_maint_details'
and module = 'pcm';

update ppbase_workspace
set label = 'Information (WO)'
where workspace_identifier = 'wo_maint_details'
and module = 'pcm';

update ppbase_workspace
set label = 'Information (JT)'
where workspace_identifier = 'jt_maint_details'
and module = 'pcm';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2270, 0, 2015, 1, 0, 0, 042784, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042784_pcm_info_menu.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;