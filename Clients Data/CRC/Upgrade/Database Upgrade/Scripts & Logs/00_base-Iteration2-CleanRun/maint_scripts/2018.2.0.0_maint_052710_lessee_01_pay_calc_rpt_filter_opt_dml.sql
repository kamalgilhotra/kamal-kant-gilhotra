/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052710_lessee_01_pay_calc_rpt_filter_opt_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.2.0.0  3/1/2019    C Yura          add filter options for MLA, ILR, SOB for 1008 rpt
||============================================================================
*/

update pp_reports 
set pp_report_filter_id = 42
where lower(trim(datawindow)) like '%dw_ls_rpt_lease_calc_payment%';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15622, 0, 2018, 2, 0, 0, 52710, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052710_lessee_01_pay_calc_rpt_filter_opt_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
