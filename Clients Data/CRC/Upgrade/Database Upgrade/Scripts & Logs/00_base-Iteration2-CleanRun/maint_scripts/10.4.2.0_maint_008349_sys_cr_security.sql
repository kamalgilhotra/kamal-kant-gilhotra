SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_008349_sys_cr_security.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/08/2013 Stephen Motter
||============================================================================
*/

declare
   CTRL_VAL       CR_SYSTEM_CONTROL.CONTROL_VALUE%type;
   SECURITY_GROUP CR_SYSTEM_CONTROL.CONTROL_VALUE%type;
   GROUP_TYPE     PP_SECURITY_GROUPS.RESTRICTED%type;

begin
   select UPPER(trim(NVL(max(CONTROL_VALUE), 'YES')))
     into CTRL_VAL
     from CR_SYSTEM_CONTROL
    where UPPER(trim(CONTROL_NAME)) = UPPER('All D JE - TBB');

   if (CTRL_VAL = 'NO') then
      --migrate the security options
      select CONTROL_VALUE
        into SECURITY_GROUP
        from CR_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = UPPER('CR All Details JE Security Group');

      select NVL(RESTRICTED, 0)
        into GROUP_TYPE
        from PP_SECURITY_GROUPS
       where UPPER(trim(GROUPS)) = UPPER(trim(SECURITY_GROUP));

      if (GROUP_TYPE = 0) then
         --standard security: blacklist
         insert into PP_SECURITY_OBJECTS
            (GROUPS, OBJECTS)
            select GROUPS, 'm_cr_all_details.m_activity.m_adjustingje'
              from PP_SECURITY_GROUPS
             where GROUPS <> SECURITY_GROUP;
         DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' rows inserted into PP_SECURITY_OBJECTS blacklist.');
      else
         --restricted or mixed security: whitelist
         insert into PP_SECURITY_OBJECTS
            (GROUPS, OBJECTS)
         values
            (SECURITY_GROUP, 'm_cr_all_details.m_activity.m_adjustingje');
         DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' rows inserted into PP_SECURITY_OBJECTS whitelist.');
      end if;
   end if;
   --remove the obsolete system controls
   delete from CR_SYSTEM_CONTROL
    where UPPER(trim(CONTROL_NAME)) = UPPER('All D JE - TBB')
       or UPPER(trim(CONTROL_NAME)) = UPPER('CR All Details JE Security Group');
   DBMS_OUTPUT.PUT_LINE(SQL%ROWCOUNT || ' rows deleted from CR_SYSTEM_CONTROL.');
   commit;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (683, 0, 10, 4, 2, 0, 8349, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_008349_sys_cr_security.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
