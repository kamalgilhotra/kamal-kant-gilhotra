/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_038995_sys_GET_PEND_SOB_RESERVE.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/18/2014 Ryan Oliveria
|| 10.4.3.0 06/04/2014 Kyle Peterson  Modifications for lease gain/loss
||============================================================================
*/

create or replace function GET_PEND_SOB_RESERVE(P_ASSET_ID           number,
                                                P_SET_OF_BOOKS_ID    number,
                                                P_GL_POSTING_MO_YR   date,
                                                P_SOB_ACTIVITY_CODE  varchar2,
                                                P_SOB_POSTING_AMOUNT number,
                                                P_POSTING_QUANTITY   PEND_TRANSACTION.POSTING_QUANTITY%type)
   return number is

   V_ACCUM_QUANTITY      CPR_LEDGER.ACCUM_QUANTITY%type;
   V_RATIO               number(22, 8);
   V_SUBLEDGER_INDICATOR PEND_TRANSACTION.SUBLEDGER_INDICATOR%type;
   V_ACTIVITY_CODE       PEND_TRANSACTION.ACTIVITY_CODE%type;
   V_RESERVE_AMT         PEND_TRANSACTION.RESERVE%type;
   V_DEPR_INDICATOR      SUBLEDGER_CONTROL.DEPRECIATION_INDICATOR%type;
   V_COMPANY_ID          COMPANY_SETUP.COMPANY_ID%type;
   V_DEPR_GROUP_ID       DEPR_GROUP_CONTROL.DEPR_GROUP_ID%type;
   V_VINTAGE             DEPR_RES_ALLO_FACTORS.VINTAGE%type;

   V_CALC_PLANT           number(22, 2);
   V_CALC_RESERVE         number(22, 2);
   V_DEPR_APPROVED_ADJUST number(22, 2);

begin

   V_ACTIVITY_CODE := trim(UPPER(P_SOB_ACTIVITY_CODE));

   -- Our reserve will always be 0 except when processing a gain_loss or transfer
   if V_ACTIVITY_CODE <> 'URGL' and V_ACTIVITY_CODE <> 'SAGL' and V_ACTIVITY_CODE <> 'UTRF' and
      V_ACTIVITY_CODE <> 'UTRT' then
      return 0;
   end if;

   -- check that asset from ldg_asset_id exists and pull asset values in case we need them later
   begin
      select NVL(A.SUBLEDGER_INDICATOR, 0),
             A.ACCUM_QUANTITY,
             C.DEPRECIATION_INDICATOR,
             A.COMPANY_ID,
             A.DEPR_GROUP_ID,
             TO_NUMBER(TO_CHAR(A.ENG_IN_SERVICE_YEAR, 'YYYY'))
        into V_SUBLEDGER_INDICATOR,
             V_ACCUM_QUANTITY,
             V_DEPR_INDICATOR,
             V_COMPANY_ID,
             V_DEPR_GROUP_ID,
             V_VINTAGE
        from CPR_LEDGER A, SUBLEDGER_CONTROL C
       where A.SUBLEDGER_INDICATOR = C.SUBLEDGER_TYPE_ID
         and A.ASSET_ID = P_ASSET_ID;
   exception
      when NO_DATA_FOUND then
         RAISE_APPLICATION_ERROR(-2004,
                                 'No CPR asset found for asset_id passed to ' ||
                                 'GET_PEND_SOB_RESERVE. asset_id: ' || P_ASSET_ID);
   end;

   if V_SUBLEDGER_INDICATOR = 0 then
      --If group depreciation then get the allocation factors * sob amount
      begin
         --abs(p_sob_posting_amount) because an IF statement at the end of this function takes care of applying the correct sign.
         select ROUND(B.FACTOR * ABS(P_SOB_POSTING_AMOUNT), 2)
           into V_RESERVE_AMT
           from DEPR_RES_ALLO_FACTORS B
          where B.VINTAGE = V_VINTAGE
            and B.DEPR_GROUP_ID = V_DEPR_GROUP_ID
            and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and B.MONTH = P_GL_POSTING_MO_YR;
      exception
         when NO_DATA_FOUND then
            RAISE_APPLICATION_ERROR(-20009,
                                    'GET_PEND_SOB_RESERVE function could not find ' ||
                                    'a DEPR_RES_ALLO_FACTORS record. (depr_group_id: ' ||
                                    V_DEPR_GROUP_ID || ', set of book: ' || P_SET_OF_BOOKS_ID ||
                                    ', gl_posting_mo_yr ' || P_GL_POSTING_MO_YR || ', vintage: ' ||
                                    V_VINTAGE || ')');
      end;
   elsif V_DEPR_INDICATOR = 3 then
      --Individually depreciated get from cpr depr by pct.

      --get calc plant from cpr_depr columns
      begin
         select NVL(BEG_ASSET_DOLLARS, 0) + NVL(NET_ADDS_AND_ADJUST, 0) + NVL(RETIREMENTS, 0) +
                NVL(TRANSFERS_IN, 0) + NVL(TRANSFERS_OUT, 0) CALC_PLANT,
                NVL(BEG_RESERVE_MONTH, 0) + NVL(SALVAGE_DOLLARS, 0) + NVL(RESERVE_ADJUSTMENT, 0) +
                NVL(COST_OF_REMOVAL, 0) + NVL(RETIREMENTS, 0) + NVL(RESERVE_TRANS_IN, 0) +
                NVL(RESERVE_TRANS_OUT, 0) + NVL(DEPR_EXP_ADJUST, 0) +
                NVL(OTHER_CREDITS_AND_ADJUST, 0) + NVL(GAIN_LOSS, 0) CALC_RESERVE,
                NVL(CURR_DEPR_EXPENSE, 0) + NVL(DEPR_EXP_ALLOC_ADJUST, 0) DEPR_APPROVED_ADJUST
           into V_CALC_PLANT, V_CALC_RESERVE, V_DEPR_APPROVED_ADJUST
           from CPR_DEPR
          where ASSET_ID = P_ASSET_ID
            and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and GL_POSTING_MO_YR = P_GL_POSTING_MO_YR;
      exception
         when NO_DATA_FOUND then
            return 0;
      end;

      --calculate ratio
      if V_CALC_PLANT = 0 and V_ACCUM_QUANTITY = 0 then
         V_RATIO := 1;
      elsif V_CALC_PLANT = 0 then
         V_RATIO := ABS(P_POSTING_QUANTITY / V_ACCUM_QUANTITY);
      else
         V_RATIO := ABS(P_SOB_POSTING_AMOUNT / V_CALC_PLANT);
      end if;

      --IF depr has been approved, add in the depr_approved_offset (curr_depr_expense and depr_exp_alloc_adjust)
      declare
         V_DEPR_APPROVED date;
      begin
      if V_SUBLEDGER_INDICATOR <> -100 then
         select DEPR_APPROVED
           into V_DEPR_APPROVED
           from CPR_CONTROL
          where COMPANY_ID = V_COMPANY_ID
            and ACCOUNTING_MONTH = P_GL_POSTING_MO_YR;
      else
         select DEPR_APPROVED
         into V_DEPR_APPROVED
         from LS_PROCESS_CONTROL
         where COMPANY_ID = V_COMPANY_ID
         and GL_POSTING_MO_YR = P_GL_POSTING_MO_YR;
      end if;

         if V_DEPR_APPROVED is not null then
            V_CALC_RESERVE := V_CALC_RESERVE + V_DEPR_APPROVED_ADJUST;
         end if;
      exception
         when NO_DATA_FOUND then
            RAISE_APPLICATION_ERROR(-20009,
                                    'No cpr_control record found for company_id ' || V_COMPANY_ID ||
                                    ' and accounting month ' ||
                                    TO_CHAR(P_GL_POSTING_MO_YR, 'MM-DD-YYYY'));
      end;

      V_RESERVE_AMT := V_CALC_RESERVE * V_RATIO;
   else
      -- old-style subledger - reserve not calculated by set of books
      V_RESERVE_AMT := 0;
   end if;

   -- URGL, SAGL and UTRT need sign flipped
   if V_ACTIVITY_CODE = 'UTRF' then
      V_RESERVE_AMT := -V_RESERVE_AMT;
   end if;

   return V_RESERVE_AMT;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1297, 0, 10, 4, 2, 2, 38995, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038995_sys_GET_PEND_SOB_RESERVE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
