/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052869_lessee_01_enhanced_mec_processes_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 03/22/2019 Shane "C" Ward    New Processes for Mass calls
||============================================================================
*/

INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
SELECT Max(process_id)+1, 'Lease Calc - MEC Mass Calculate', 'Lease Calculations - MEC Mass Calculate', 'ssp_lease_control.exe MASS_CALCULATE', '2018.2.0.0', 0
FROM PP_PROCESSES;

INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
SELECT Max(process_id)+1, 'Lease Calc - MEC Send JEs', 'Lease Calculations - MEC Mass Send JEs', 'ssp_lease_control.exe SEND_ALL', '2018.2.0.0', 0
FROM PP_PROCESSES;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16124, 0, 2018, 2, 0, 0, 52869, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052869_lessee_01_enhanced_mec_processes_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
