/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030592_lease_mla_ilr_asset_ccs.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/09/2013 B. Beck        Point release
||============================================================================
*/

alter table CLASS_CODE
   add (MLA_INDICATOR   number(1,0),
        ILR_INDICATOR   number(1,0),
        ASSET_INDICATOR number(1,0));

create table LS_ASSET_CLASS_CODE
(
 CLASS_CODE_ID number(22,0) not null,
 LS_ASSET_ID   number(22,0) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 VALUE         varchar2(254) not null
);

alter table LS_ASSET_CLASS_CODE
   add constraint PK_LS_ASSET_CLASS_CODE
       primary key (LS_ASSET_ID, CLASS_CODE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_ASSET_CLASS_CODE
   add constraint FK1_LS_ASSET_CLASS_CODE
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

alter table LS_ASSET_CLASS_CODE
   add constraint FK2_LS_ASSET_CLASS_CODE
       foreign key (CLASS_CODE_ID)
       references CLASS_CODE (CLASS_CODE_ID);

create table LS_MLA_CLASS_CODE
(
 CLASS_CODE_ID number(22,0) not null,
 LEASE_ID      number(22,0) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 VALUE         varchar2(254) not null
);

alter table LS_MLA_CLASS_CODE
   add constraint PK_LS_MLA_CLASS_CODE
       primary key (LEASE_ID, CLASS_CODE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_MLA_CLASS_CODE
   add constraint FK1_LS_MLA_CLASS_CODE
       foreign key (LEASE_ID)
       references LS_LEASE (LEASE_ID);

 alter table LS_MLA_CLASS_CODE
   add constraint FK2_LS_MLA_CLASS_CODE
       foreign key (CLASS_CODE_ID)
       references CLASS_CODE (CLASS_CODE_ID);

create table LS_ILR_CLASS_CODE
(
 CLASS_CODE_ID number(22,0) not null,
 ILR_ID        number(22,0) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 VALUE         varchar2(254) not null
);

alter table LS_ILR_CLASS_CODE
   add constraint PK_LS_ilr_CLASS_CODE
       primary key (ILR_ID, CLASS_CODE_ID);

alter table LS_ILR_CLASS_CODE
   add constraint FK1_LS_ILR_CLASS_CODE
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);

 alter table LS_ILR_CLASS_CODE
   add constraint FK2_LS_ilr_CLASS_CODE
       foreign key (CLASS_CODE_ID)
       references CLASS_CODE (CLASS_CODE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (456, 0, 10, 4, 1, 0, 30592, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030592_lease_mla_ilr_asset_ccs.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
