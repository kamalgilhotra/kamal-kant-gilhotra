 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_043572_lease_comp_import_ponum_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/09/2015 	Jared Schwantz   Update PO Number on Lease Component Import to be a required field.
 ||============================================================================
 */ 
 
update pp_import_column
set is_required = 1
where import_type_id = 255
and column_name = 'po_number';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2488, 0, 2015, 1, 0, 0, 43572, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043572_lease_comp_import_ponum_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;