/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_034798_pwrtax_mlp_reports.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/05/2014 Anand Rajashekar    Add two new reports to MLP
||========================================================================================
*/

-- Report 300 dashboard - 403016

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (85, TO_DATE('18-AUG-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - Dashboard', null, 'uo_ppbase_tab_filter_dynamic');

insert into PWRPLANT.PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403016, sysdate, user, 'PowerTax Dashboard', 'PowerTax Dashboard', 'PowerTax', 'dw_tax_rpt_dashboard', '', '', '',
    'PwrTax - 300', '', '', '', 1, 130, 50, 85, 1, 3, '', '', null, '', '', 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (85, 171, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (85, 167, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'));

-- Report 310 - 403017

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403017, 'PWRPLANT', TO_DATE('01-JUL-14', 'DD-MON-RR'), 'Schedule G Overlay Report-K1 Export',
    'Depreciation detail report for compliance filing with tax books overlayed grouped by K1 Export Id', 'PowerTax',
    'dw_tax_rpt_sched_g_overlay_k1export', null, 'PowerTax_Rollup', null, 'PwrTax - 301', null, null, null, 1, 130, 52,
    53, 1, 3, null, null, null, null, null, 0);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1373, 0, 10, 4, 3, 0, 34798, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_034798_pwrtax_mlp_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
