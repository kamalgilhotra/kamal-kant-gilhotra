 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_041083_sys_tblgrp_clean.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2015.1.0.0 11/06/2014 A Scott        some earlier maintenance scripts inserted
 ||                                      into pp table groups for "system" when setting 
 ||                                      up for table maint.  This should not have been 
 ||                                      done.  If a client does not use table security, 
 ||                                      having a single row in the table turns it on.
 ||============================================================================
 */ 

SET SERVEROUTPUT ON

----check for count of distinct "groups" in pp table groups.
----if there's only one distinct group in pp table groups.
----  if that group is "system"
----     if there are less than 10 rows of data in pp_table_groups THEN
----        delete from pp_table_groups;
----     end if
----  end if
----end if
----So do nothing if any of the above if statements are not true.
declare
   COUNT_GROUPS number(22, 0);
   GROUP_NAME   varchar2(35);
   COUNT_ROWS   number(22, 0);
   THRESHOLD    number(22, 0) := 10;
   ROWS_DELETED number(22, 0);
begin

   select count(distinct GROUPS) into COUNT_GROUPS from PP_TABLE_GROUPS;

   DBMS_OUTPUT.PUT_LINE(COUNT_GROUPS || ' distinct group(s) used in pp table groups.');

   if COUNT_GROUPS = 1 then
   
      select LOWER(trim(GROUPS)) into GROUP_NAME from PP_TABLE_GROUPS where ROWNUM = 1;
   
      DBMS_OUTPUT.PUT_LINE('The only group in use is "' || GROUP_NAME || '".');
   
      if GROUP_NAME = 'system' then
      
         select count(*) into COUNT_ROWS from PP_TABLE_GROUPS;
      
         DBMS_OUTPUT.PUT_LINE('There are ' || COUNT_ROWS || ' rows in pp table groups.');
      
         if COUNT_ROWS <= THRESHOLD then
            DBMS_OUTPUT.PUT_LINE('This is less than or equal to the threshold of 10.');
            DBMS_OUTPUT.PUT_LINE('This means that previous scripts erroneously inserted into pp table groups ');
            DBMS_OUTPUT.PUT_LINE('when table security was not in use. This is being corrected now.');
         
            delete from PP_TABLE_GROUPS;
         
            ROWS_DELETED := sql%rowcount;
            DBMS_OUTPUT.PUT_LINE(ROWS_DELETED || ' rows were deleted from pp table groups.');
         
         end if;
      
      end if;
   
   end if;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Error discovered in checks used to determine in pp table groups is not in used and should be cleared.');
end;
/



--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (2008, 0, 2015, 1, 0, 0, 41083, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041083_sys_tblgrp_clean.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
