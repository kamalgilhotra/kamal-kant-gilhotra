/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010915_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/18/2012 Julia Breuer   Point Release
||============================================================================
*/

delete from PWRPLANT.PP_IMPORT_COLUMN_LOOKUP
 where IMPORT_LOOKUP_ID = 6
   and IMPORT_TYPE_ID in (14, 15, 16, 18, 26, 38);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (224, 0, 10, 4, 1, 0, 10915, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010915_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
