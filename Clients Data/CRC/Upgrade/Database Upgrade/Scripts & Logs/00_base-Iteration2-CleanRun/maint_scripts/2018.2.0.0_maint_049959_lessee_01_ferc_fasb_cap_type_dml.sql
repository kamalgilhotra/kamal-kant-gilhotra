/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049959_lessee_01_ferc_fasb_cap_type_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 01/14/2019 Sarah Byers    Add FERC Depr FASB Cap Type
||============================================================================
*/

insert into ls_fasb_cap_type(fasb_cap_type_id, description)
select 3, 'FERC Depreciation'
from dual
where not exists(
   select 1
   from ls_fasb_cap_type
   where fasb_cap_type_id = 3);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13902, 0, 2018, 2, 0, 0, 49959, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049959_lessee_01_ferc_fasb_cap_type_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
