/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047035_lease_add_number_format_component_types_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 03/02/2017 Jared Watkins  Add the values for number_format and ls_component_type tables
||============================================================================
*/

delete from number_format;

--insert the number_format values we support
insert into number_format(number_format_id, description)
values(1, 'Decimal');
insert into number_format(number_format_id, description)
values(2, 'Integer');
insert into number_format(number_format_id, description)
values(3, 'Percent');

delete from ls_component_type;

--insert the ls_component_type values we support
insert into ls_component_type(ls_component_type_id, description)
values(1,'Index/Rate');
insert into ls_component_type(ls_component_type_id, description)
values(2, 'Variable');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3380, 0, 2017, 1, 0, 0, 47035, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047035_lease_add_number_format_component_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	