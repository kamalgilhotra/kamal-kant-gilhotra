/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		 Job Server
|| File Name:   maint_043346_jobserver_inc_20150323P_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/25/2014 Paul Cordero        Creating new column 'CATEGORY' for 'PP_JOB_EXECUTABLE' table
||==========================================================================================
*/


SET SERVEROUTPUT ON

begin
   execute immediate 'ALTER TABLE PP_JOB_EXECUTABLE ADD CATEGORY VARCHAR2(50)';
   DBMS_OUTPUT.PUT_LINE('Table PP_JOB_EXECUTABLE altered.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table PP_JOB_EXECUTABLE already has CATEGORY or table alter priv not granted.');
end;
/

COMMENT ON COLUMN PP_JOB_EXECUTABLE.CATEGORY IS 'Used to further organize executables.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2486, 0, 2015, 1, 0, 0, 43346, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043346_jobserver_inc_20150323P_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;