/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030189_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   07/25/2013 Julia Breuer    Patch Release
||============================================================================
*/

--
-- Drop require_review from the wo_tax_status table.
--
alter table WO_TAX_STATUS drop column REQUIRE_REVIEW;

--
-- Remove require_review from the tax status import and any existing templates.
--
update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID + 100
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 108)
   and exists (select 'x'
          from PP_IMPORT_TEMPLATE_FIELDS INFLDS
         where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
           and INFLDS.COLUMN_NAME = 'require_review')
   and FIELD_ID > (select FIELD_ID
                     from PP_IMPORT_TEMPLATE_FIELDS INFLDS
                    where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
                      and INFLDS.COLUMN_NAME = 'require_review');

delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME = 'require_review'
   and IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 108);

update PP_IMPORT_TEMPLATE_FIELDS
   set FIELD_ID = FIELD_ID - 101
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 108)
   and FIELD_ID > 100;

delete from PP_IMPORT_COLUMN_LOOKUP
 where IMPORT_TYPE_ID = 108
   and COLUMN_NAME = 'require_review';

delete from PP_IMPORT_COLUMN
 where IMPORT_TYPE_ID = 108
   and COLUMN_NAME = 'require_review';

alter table RPR_IMPORT_TAX_STATUS     drop column REQUIRE_REVIEW;
alter table RPR_IMPORT_TAX_STATUS_ARC drop column REQUIRE_REVIEW;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (483, 0, 10, 4, 1, 0, 30189, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030189_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;