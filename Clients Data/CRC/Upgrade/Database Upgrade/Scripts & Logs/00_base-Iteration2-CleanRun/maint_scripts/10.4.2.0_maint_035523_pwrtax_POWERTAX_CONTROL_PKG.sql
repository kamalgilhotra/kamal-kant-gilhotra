/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035523_pwrtax_POWERTAX_CONTROL_PKG.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/10/2014 Julia Breuer   Point Release
||============================================================================
*/

create or replace package POWERTAX_CONTROL_PKG as
--||============================================================================
--|| Application: PowerPlant
--|| Object Name: POWERTAX_CONTROL_PKG
--|| Description: Modifies and retrieves PowerTax information for the
--||              current database session.
--||============================================================================
--|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
--||============================================================================
--|| Version  Date       Revised By     Reason for Change
--|| -------- ---------- -------------- ----------------------------------------
--|| 10.4.2.0 01/17/2014 Julia Breuer   Initial Creation
--||============================================================================

   -- Sets the global version_id variable
   procedure P_SET_VERSION_ID(A_VERSION_ID in VERSION.VERSION_ID%type);

   -- Returns the current version_id
   function F_GET_VERSION_ID return number;

end POWERTAX_CONTROL_PKG;
/


create or replace package body POWERTAX_CONTROL_PKG as

   G_VERSION_ID VERSION.VERSION_ID%type;

   --**************************************************************************************
   -- p_set_version_id
   --**************************************************************************************
   procedure P_SET_VERSION_ID(A_VERSION_ID in VERSION.VERSION_ID%type) is

   begin
      G_VERSION_ID := A_VERSION_ID;
   end P_SET_VERSION_ID;

   --**************************************************************************************
   -- f_get_version_id
   --**************************************************************************************
   function F_GET_VERSION_ID return number is

   begin
      return NVL(G_VERSION_ID, 0);
   end F_GET_VERSION_ID;

end POWERTAX_CONTROL_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (892, 0, 10, 4, 2, 0, 35523, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035523_pwrtax_POWERTAX_CONTROL_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;