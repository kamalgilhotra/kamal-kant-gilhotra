/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049340_lease_field_import_fields_update_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

delete from pp_import_template_fields
where column_name in ('est_executory_cost','contingent_amount');

delete from pp_import_column
where column_name in ('est_executory_cost','contingent_amount');

UPDATE pp_import_column
SET is_required = 0
WHERE import_type_id = (SELECT DISTINCT import_type_id FROM pp_import_type WHERE description = 'Add: Leased Assets ')
AND column_name = 'department_id';
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4020, 0, 2017, 1, 0, 0, 49340, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049340_lease_field_import_fields_update_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;