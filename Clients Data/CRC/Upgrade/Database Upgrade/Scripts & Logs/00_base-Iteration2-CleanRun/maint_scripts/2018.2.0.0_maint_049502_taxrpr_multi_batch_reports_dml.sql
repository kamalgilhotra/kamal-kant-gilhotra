/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049502_taxrpr_multi_batch_reports_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 10/10/2017 Eric Berger    DML multi-batch reporting.
||============================================================================
*/

--creating the new time option id's for general/uop method batch reporting
INSERT INTO pp_reports_time_option ("PP_REPORT_TIME_OPTION_ID", "DESCRIPTION", "PARAMETER_UO_NAME", "DWNAME1", "LABEL1", "KEYCOLUMN1", "DWNAME2", "LABEL2", "KEYCOLUMN2", "DWNAME3", "LABEL3", "KEYCOLUMN3")
	SELECT 209, 'RPR - Multi Batch CPR', 'uo_ppbase_report_parms_multigrid', 'dw_rpr_batch_control_cpr', 'CPR Batches', 'batch_id', '', '', '', '', '', ''
    FROM dual
    WHERE NOT EXISTS (
        SELECT 1
        FROM pp_reports_time_option
        WHERE description = 'RPR - Multi Batch CPR');
		
INSERT INTO pp_reports_time_option ("PP_REPORT_TIME_OPTION_ID", "DESCRIPTION", "PARAMETER_UO_NAME", "DWNAME1", "LABEL1", "KEYCOLUMN1", "DWNAME2", "LABEL2", "KEYCOLUMN2", "DWNAME3", "LABEL3", "KEYCOLUMN3")
	SELECT 210, 'RPR - Multi Batch', 'uo_ppbase_report_parms_multigrid', 'dw_rpr_batch_control', 'Repairs Batches', 'batch_id', '', '', '', '', '', ''
    FROM dual
    WHERE NOT EXISTS (
        SELECT 1
        FROM pp_reports_time_option
        WHERE description = 'RPR - Multi Batch');

--update the time options on the reports
UPDATE pp_reports
SET pp_report_time_option_id = (SELECT Max(pp_report_time_option_id) FROM pp_reports_time_option WHERE description = 'RPR - Multi Batch')
WHERE datawindow = 'dw_rpr_1310'
;

--update the time options on the reports
UPDATE pp_reports
SET pp_report_time_option_id = (SELECT Max(pp_report_time_option_id) FROM pp_reports_time_option WHERE description = 'RPR - Multi Batch CPR')
WHERE datawindow = 'dw_rpr_2270'
;

--update the time options on the reports
UPDATE pp_reports
SET pp_report_time_option_id = (SELECT Max(pp_report_time_option_id) FROM pp_reports_time_option WHERE description = 'RPR - Multi Batch CPR')
WHERE datawindow = 'dw_rpr_2280'
;

--update the time options on the reports
UPDATE pp_reports
SET pp_report_time_option_id = (SELECT Max(pp_report_time_option_id) FROM pp_reports_time_option WHERE description = 'RPR - Multi Batch CPR')
WHERE datawindow = 'dw_rpr_2380'
;

--update the time options on the reports
UPDATE pp_reports
SET pp_report_time_option_id = (SELECT Max(pp_report_time_option_id) FROM pp_reports_time_option WHERE description = 'RPR - Multi Batch CPR')
WHERE datawindow = 'dw_rpr_2390'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13735, 0, 2018, 2, 0, 0, 49502, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049502_taxrpr_multi_batch_reports_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;