/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044504_sys_pp_statistics_seq_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.3.5   07/24/2015 D. Motter   	 create new sequence for pp_statistics
||============================================================================
*/

declare
    type numbers is table of number;
    burn            numbers;
    start_id        number;
    curr_id         number;
    trigger_count   number;
    seq_exists      number;
begin
    -- If the sequence already exists, this script has already been run
    select count(*) into seq_exists from all_sequences where lower(sequence_name) = 'pp_statistics_seq';
    
    if seq_exists = 0 then 
        -- Create new sequence
        execute immediate 'create sequence pwrplant.pp_statistics_seq start with 1';

        -- Create temporary table used for id conversion
        execute immediate 'create table temp_pp_stats_convert as select * from pp_statistics';
        execute immediate 'alter table temp_pp_stats_convert add constraint temp_pp_stats_conv_pk primary key (id)';
        execute immediate 'alter table temp_pp_stats_convert add new_id number(22,0)';

        -- Disable the audit trigger temporarily so we preserve the old data there.
        trigger_count := 0;
        execute immediate 'select count(*) from dba_triggers where upper(trigger_name) = ''PP_STATISTICS'' and upper(owner) = ''PWRPLANT''' into trigger_count;
        if trigger_count > 0 then
            execute immediate 'alter trigger PWRPLANT.PP_STATISTICS disable';
        end if;
        
        -- Populate temporary conversion table with new ids
        execute immediate 
            'merge into temp_pp_stats_convert a '||
            'using (select id, dense_rank() over (order by id) as drank from temp_pp_stats_convert) b '||
            'on (b.id = a.id) '||
            'when matched then '||
                'update set a.new_id = b.drank';
        
        -- Repopulate pp_statistics using temp table
        execute immediate 'truncate table pp_statistics';
        
        execute immediate 
            'insert into pp_statistics (id,record_type,record_data,users,start_time,end_time,elapsed_time,user_id,time_stamp) '||
            'select new_id,record_type,record_data,users,start_time,end_time,elapsed_time,user_id,time_stamp '||
              'from temp_pp_stats_convert';
        
        -- Re-enable audit triggers on pp_statistics
        if trigger_count > 0 then
            execute immediate 'alter trigger PWRPLANT.PP_STATISTICS enable';
        end if;
        
        -- Get rid of temporary conversion table
        execute immediate 'drop table temp_pp_stats_convert';
        
        -- Increment the nextval on sequence to max + 1 on pp_statistics.
        select nvl(max(id), 0) into start_id from pp_statistics;
        
        execute immediate 'select pp_statistics_seq.nextval from dual' into curr_id ;
        
        if start_id > curr_id then
            execute immediate
                'select pp_statistics_seq.nextval from dual '||
                'connect by level <= :start_id - :curr_id'
            bulk collect into burn
            using start_id, curr_id;
        end if;
    end if;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2713, 0, 10, 4, 3, 5, 044504, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.5_maint_044504_sys_pp_statistics_seq_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;