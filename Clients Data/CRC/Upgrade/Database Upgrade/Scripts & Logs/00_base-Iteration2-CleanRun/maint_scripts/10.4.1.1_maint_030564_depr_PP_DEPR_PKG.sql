/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030564_depr_PP_DEPR_PKG.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/09/2013 Brandon Beck   Point Release
||============================================================================
*/

create or replace package PP_DEPR_PKG
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PP_DEPR_PKG
|| Description: Depr functions and procedures for PowerPlant application.
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0     07/18/2011 David Liss     Create
|| 2.0     04/03/2013 Brandon Beck   Adding Depreciation Calculation Functions
|| 3.0     10/08/13   Brandon Beck   Add Over Depr Check
||============================================================================
*/
 as
   procedure P_LEASEDEPRCALC(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                             A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE);

   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS         PKG_PP_COMMON.DATE_TABTYPE,
                                 A_LOAD_RECURRING number);

   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH          date,
                                 A_LOAD_RECURRING number);

   procedure P_STAGEMONTHENDDEPR(A_MONTHS PKG_PP_COMMON.DATE_TABTYPE);

   procedure P_STAGEMONTHENDDEPR(A_MONTH date);

   function F_PREPDEPRCALC return number;


   /*
   *  A Wrapper for the f_overdepr(params) function.  This executed over depr adjustment for all rows in
   *  depr_calc_stg
   */
   function f_overdepr return number;

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_find_depr_group
   || Description: Finds the depr group under the depr group control lookup.
   || None of the parameters can be null before entering the function.
   || Make sure you check sqlca.sqlcode when calling this function.
   || Return Values: -1 - No data found
   ||             dg_id - Valid depr group ID
   ||                -9 - Invalid depr group
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     07/18/2011 David Liss     Create
   || 2.0     04/03/2013 Brandon Beck   Adding Depreciation Calculation Functions
   ||============================================================================
   */
   function F_FIND_DEPR_GROUP(A_COMPANY_ID         number,
                              A_GL_ACCOUNT_ID      number,
                              A_MAJOR_LOCATION_ID  number,
                              A_UTILITY_ACCOUNT_ID number,
                              A_BUS_SEGMENT_ID     number,
                              A_SUB_ACCOUNT_ID     number,
                              A_SUBLEDGER_TYPE_ID  number,
                              A_BOOK_VINTAGE       number,
                              A_ASSET_LOCATION_ID  number,
                              A_LOCATION_TYPE_ID   number,
                              A_RETIREMENT_UNIT_ID number,
                              A_PROPERTY_UNIT_ID   number,
                              A_CLASS_CODE_ID      number,
                              A_CC_VALUE           varchar2)

    return number;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_get_dg_cc_id
   || Description: Finds the depr group class code.
   || Make sure you check sqlca.sqlcode when calling this function.
   || Return Values: 0 - No data found
   ||                dg_cc_id - DEPR GROUP CONTROL class code id
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     08/12/2011 David Liss     Create
   ||============================================================================
   */
   function F_GET_DG_CC_ID return number;

   --Check to validate depr group status
   procedure P_DG_STATUS_CHECK(A_ENABLE boolean);

   --Check to validate depr group business segment
   procedure P_CHECK_DG_BS;

   G_DG_STATUS_CHECK boolean := true;
   G_CHECK_DG_BS     varchar2(10);

end PP_DEPR_PKG;
/


create or replace package body PP_DEPR_PKG as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --       VARIABLES
   --**************************************************************
   G_IGNOREINACTIVEYES PKG_PP_COMMON.NUM_TABTYPE;
   G_IGNOREINACTIVENO  PKG_PP_COMMON.NUM_TABTYPE;

   --**************************************************************
   --       PROCEDURES
   --**************************************************************

   --**************************************************************************
   --                            P_LEASEDEPRCALC
   --**************************************************************************
   -- Depr Calc for Lease
   procedure P_LEASEDEPRCALC(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                             A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE) is
   begin
      PKG_PP_CPR_DEPR.P_STARTMONTHENDDEPR(A_COMPANY_IDS, A_MONTHS, -100);
   end P_LEASEDEPRCALC;

   --**************************************************************************
   --                            P_SET_FISCALYEARSTART
   --**************************************************************************
   -- Sets the beginning of the fiscal year
   procedure P_SET_FISCALYEARSTART is

   begin
      update DEPR_CALC_STG STG
         set FISCALYEARSTART =
              (select FY.MONTH
                 from PP_CALENDAR C, PP_CALENDAR FY
                where C.MONTH = STG.GL_POST_MO_YR
                  and C.FISCAL_YEAR = FY.FISCAL_YEAR
                  and FY.FISCAL_MONTH = 1)
       where STG.DEPR_CALC_STATUS = 1;

   end P_SET_FISCALYEARSTART;

   --**************************************************************************
   --                            P_CHECKINACTP_CORSALVAMORTVEDEPR
   --**************************************************************************
   --
   -- Get COR and SALV amort
   -- PREREQ: Staging table must be filled in
   --
   procedure P_CORSALVAMORT is

   begin
      update DEPR_CALC_STG Z
         set (DNSA_COR_BAL, DNSA_SALV_BAL) =
              (select sum(COST_OF_REMOVAL_BAL - COST_OF_REMOVAL_RESERVE),
                      sum(SALVAGE_BAL - SALVAGE_RESERVE)
                 from DEPR_NET_SALVAGE_AMORT D
                where D.DEPR_GROUP_ID = Z.DEPR_GROUP_ID
                  and D.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                  and D.GL_POST_MO_YR = ADD_MONTHS(Z.GL_POST_MO_YR, -1))
       where COR_TREATMENT + SALVAGE_TREATMENT > 0
         and DEPR_CALC_STATUS = 1;

      update DEPR_CALC_STG Z
         set (COR_YTD, SALV_YTD) =
              (select sum(L.COST_OF_REMOVAL), sum(L.SALVAGE_CASH - L.SALVAGE_RETURNS)
                 from DEPR_LEDGER L
                where L.DEPR_GROUP_ID = Z.DEPR_GROUP_ID
                  and L.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                  and L.GL_POST_MO_YR between Z.FISCALYEARSTART and ADD_MONTHS(Z.GL_POST_MO_YR, -1))
       where COR_TREATMENT + SALVAGE_TREATMENT > 0
         and DEPR_CALC_STATUS = 1;

   end P_CORSALVAMORT;

   --**************************************************************************
   --                            P_CHECKINACTIVEDEPR
   --**************************************************************************
   --
   -- Set the companies that are ignoring inactive status
   --
   procedure P_CHECKINACTIVEDEPR(A_COMPANY_ID number,
                                 A_INDEX      number) is

   begin
      if LOWER(trim(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',
                                                                          A_COMPANY_ID),
                        'no'))) = 'no' then
         G_IGNOREINACTIVENO(A_INDEX) := A_COMPANY_ID;
      else
         G_IGNOREINACTIVEYES(A_INDEX) := A_COMPANY_ID;
      end if;
   end P_CHECKINACTIVEDEPR;

   --**************************************************************************
   --                            P_MISSINGSETSOFBOOKS
   --**************************************************************************
   --
   -- Checks for missing sets of books.
   -- Set the depr calc status to be -1 if a set of books is missing for the company
   -- PREREQ: Staging Table must be filled in
   --
   procedure P_MISSINGSETSOFBOOKS is

   begin
      update DEPR_CALC_STG Z
         set DEPR_CALC_STATUS = -1, DEPR_CALC_MESSAGE = 'Missing Sets of Books in Depr Ledger'
       where COMPANY_ID in (select YY.COMPANY_ID
                              from (select CS.COMPANY_ID, CS.SET_OF_BOOKS_ID
                                      from COMPANY_SET_OF_BOOKS CS
                                    minus
                                    select ZZ.COMPANY_ID, ZZ.SET_OF_BOOKS_ID
                                      from DEPR_CALC_STG ZZ) YY);


   end P_MISSINGSETSOFBOOKS;

   --**************************************************************************
   --                            P_STARTMONTHENDDEPR
   --**************************************************************************
   --
   -- WRAPPER for the start depr calc to allow single month depr calculations
   --
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH          date,
                                 A_LOAD_RECURRING number) is

      MY_MONTHS PKG_PP_COMMON.DATE_TABTYPE;

   begin
      MY_MONTHS(1) := A_MONTH;
      P_STARTMONTHENDDEPR(A_COMPANY_IDS, MY_MONTHS, A_LOAD_RECURRING);
   end P_STARTMONTHENDDEPR;

   --**************************************************************************
   --                            P_STARTMONTHENDDEPR
   --**************************************************************************
   --
   --  Start a depr calc for an array of companies and an array of months
   -- Start Logging
   -- Load Global Temp Staging table
   -- Calculate Deprecation
   -- Backfill results to depr ledger
   -- End logging
   --
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS         PKG_PP_COMMON.DATE_TABTYPE,
                                 A_LOAD_RECURRING number) is
      MY_RET number;
      MY_STR varchar2(2000);
   begin
      --
      -- START THE LOGGING and log what companys and months are being processed
      --
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_PP_DEPR_COMMON.F_GETDEPRPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('PROCESSING the following company_ids:');

      --
      -- LOOP over the companies and prep the company for processing
      -- Get company system controls to determine whether to include inactive depr groups
      --
      for I in 1 .. A_COMPANY_IDS.COUNT
      loop
         PKG_PP_LOG.P_WRITE_MESSAGE('   ' || TO_CHAR(A_COMPANY_IDS(I)));
         -- load the arrays for companies ignoring inactive depr groups
         P_CHECKINACTIVEDEPR(A_COMPANY_IDS(I), I);
      end loop;

      if A_LOAD_RECURRING = 1 then
         if PKG_PP_DEPR_ACTIVITY.F_RECURRINGACTIVITY(A_COMPANY_IDS, A_MONTHS, MY_STR) = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(MY_STR);
            return;
         end if;
      end if;

      P_STAGEMONTHENDDEPR(A_MONTHS);

      -- start depreciation calculation
      if F_PREPDEPRCALC() = -1 then
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR During Depreciation Calculation');
      end if;

      --
      --  END LOGGING
      --
      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');
      PKG_PP_LOG.P_END_LOG();


   end P_STARTMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  WRAPPER for the prep function to pass in an array of months.
   --
   procedure P_STAGEMONTHENDDEPR(A_MONTHS PKG_PP_COMMON.DATE_TABTYPE) is

   begin
      for I in A_MONTHS.FIRST .. A_MONTHS.LAST
      loop
         P_STAGEMONTHENDDEPR(A_MONTHS(I));
      end loop;
   end P_STAGEMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation.
   -- THE Load is based on depr ledger for a passed in array of company ids.
   -- AND a single month
   --
   procedure P_STAGEMONTHENDDEPR(A_MONTH date) is

   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the calculation staging table for month:' ||
                                 TO_CHAR(A_MONTH, 'yyyymm'));

      forall I in indices of G_IGNOREINACTIVENO
         insert into DEPR_CALC_STG
            (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
             DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, FISCALYEAROFFSET, SMOOTH_CURVE,
             DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
             DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS,
             TRUE_UP_CPR_DEPR, RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
             END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
             COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
             SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
             BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
             RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
             RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
             ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
             END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
             DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
             SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
             GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
             CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
             IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
             COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
             COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
             COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
             RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
             SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
             RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
             IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT)
            select DL.DEPR_GROUP_ID,
                   DL.SET_OF_BOOKS_ID,
                   DL.GL_POST_MO_YR,
                   DL.GL_POST_MO_YR,
                   1,
                   '',
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                               DG.COMPANY_ID),
                             'no')),
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                                               DG.COMPANY_ID),
                             'no')),
                   TO_NUMBER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('FISCAL YEAR OFFSET',
                                                                                   DG.COMPANY_ID),
                                 '0')),
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
                                                                               DG.COMPANY_ID),
                             '0')),
                   0,
                   0,
                   0,
                   0,
                   DG.COMPANY_ID,
                   DG.SUBLEDGER_TYPE_ID,
                   DG.DESCRIPTION,
                   DG.DEPR_METHOD_ID,
                   DG.MID_PERIOD_CONV,
                   DG.MID_PERIOD_METHOD,
                   DG.EST_ANN_NET_ADDS,
                   DG.TRUE_UP_CPR_DEPR,
                   DMR.RATE,
                   DMR.NET_GROSS,
                   DMR.OVER_DEPR_CHECK,
                   DMR.NET_SALVAGE_PCT,
                   DMR.RATE_USED_CODE,
                   DMR.END_OF_LIFE,
                   DMR.EXPECTED_AVERAGE_LIFE,
                   DMR.RESERVE_RATIO_ID,
                   DMR.COST_OF_REMOVAL_RATE,
                   DMR.COST_OF_REMOVAL_PCT,
                   DMR.SALVAGE_RATE,
                   DMR.INTEREST_RATE,
                   DMR.AMORTIZABLE_LIFE,
                   DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1),
                   DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1),
                   DMR.NET_SALVAGE_AMORT_LIFE,
                   DMR.ALLOCATION_PROCEDURE,
                   DL.DEPR_LEDGER_STATUS,
                   DL.BEGIN_RESERVE,
                   DL.END_RESERVE,
                   DL.RESERVE_BAL_PROVISION,
                   DL.RESERVE_BAL_COR,
                   DL.SALVAGE_BALANCE,
                   DL.RESERVE_BAL_ADJUST,
                   DL.RESERVE_BAL_RETIREMENTS,
                   DL.RESERVE_BAL_TRAN_IN,
                   DL.RESERVE_BAL_TRAN_OUT,
                   DL.RESERVE_BAL_OTHER_CREDITS,
                   DL.RESERVE_BAL_GAIN_LOSS,
                   DL.RESERVE_ALLOC_FACTOR,
                   DL.BEGIN_BALANCE,
                   DL.ADDITIONS,
                   DL.RETIREMENTS,
                   DL.TRANSFERS_IN,
                   DL.TRANSFERS_OUT,
                   DL.ADJUSTMENTS,
                   DL.DEPRECIATION_BASE,
                   DL.END_BALANCE,
                   DL.DEPRECIATION_RATE,
                   DL.DEPRECIATION_EXPENSE,
                   DL.DEPR_EXP_ADJUST,
                   DL.DEPR_EXP_ALLOC_ADJUST,
                   DL.COST_OF_REMOVAL,
                   DL.RESERVE_RETIREMENTS,
                   DL.SALVAGE_RETURNS,
                   DL.SALVAGE_CASH,
                   DL.RESERVE_CREDITS,
                   DL.RESERVE_ADJUSTMENTS,
                   DL.RESERVE_TRAN_IN,
                   DL.RESERVE_TRAN_OUT,
                   DL.GAIN_LOSS,
                   DL.VINTAGE_NET_SALVAGE_AMORT,
                   DL.VINTAGE_NET_SALVAGE_RESERVE,
                   DL.CURRENT_NET_SALVAGE_AMORT,
                   DL.CURRENT_NET_SALVAGE_RESERVE,
                   DL.IMPAIRMENT_RESERVE_BEG,
                   DL.IMPAIRMENT_RESERVE_ACT,
                   DL.IMPAIRMENT_RESERVE_END,
                   DL.EST_ANN_NET_ADDS,
                   DL.RWIP_ALLOCATION,
                   DL.COR_BEG_RESERVE,
                   DL.COR_EXPENSE,
                   DL.COR_EXP_ADJUST,
                   DL.COR_EXP_ALLOC_ADJUST,
                   DL.COR_RES_TRAN_IN,
                   DL.COR_RES_TRAN_OUT,
                   DL.COR_RES_ADJUST,
                   DL.COR_END_RESERVE,
                   DL.COST_OF_REMOVAL_RATE,
                   DL.COST_OF_REMOVAL_BASE,
                   DL.RWIP_COST_OF_REMOVAL,
                   DL.RWIP_SALVAGE_CASH,
                   DL.RWIP_SALVAGE_RETURNS,
                   DL.RWIP_RESERVE_CREDITS,
                   DL.SALVAGE_RATE,
                   DL.SALVAGE_BASE,
                   DL.SALVAGE_EXPENSE,
                   DL.SALVAGE_EXP_ADJUST,
                   DL.SALVAGE_EXP_ALLOC_ADJUST,
                   DL.RESERVE_BAL_SALVAGE_EXP,
                   DL.RESERVE_BLENDING_ADJUSTMENT,
                   DL.RESERVE_BLENDING_TRANSFER,
                   DL.COR_BLENDING_ADJUSTMENT,
                   DL.COR_BLENDING_TRANSFER,
                   DL.IMPAIRMENT_ASSET_AMOUNT,
                   DL.IMPAIRMENT_EXPENSE_AMOUNT,
                   DL.RESERVE_BAL_IMPAIRMENT
              from DEPR_LEDGER DL,
                   DEPR_GROUP DG,
                   (select DD.*,
                           ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
                      from DEPR_METHOD_RATES DD
                     where DD.EFFECTIVE_DATE <= A_MONTH) DMR
             where DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
               and DMR.THE_ROW = 1
               and DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
               and DMR.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
               and DL.GL_POST_MO_YR = A_MONTH
               and DG.COMPANY_ID = G_IGNOREINACTIVENO(I);

      -- statement to load the groups that are ignoring inactive
      forall I in indices of G_IGNOREINACTIVEYES
         insert into DEPR_CALC_STG
            (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
             DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, FISCALYEAROFFSET, SMOOTH_CURVE,
             DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
             DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS,
             TRUE_UP_CPR_DEPR, RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
             END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
             COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
             SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
             BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
             RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
             RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
             ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
             END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
             DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
             SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
             GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
             CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
             IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
             COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
             COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
             COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
             RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
             SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
             RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
             IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT)
            select DL.DEPR_GROUP_ID,
                   DL.SET_OF_BOOKS_ID,
                   DL.GL_POST_MO_YR,
                   DL.GL_POST_MO_YR,
                   1,
                   '',
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                               DG.COMPANY_ID),
                             'no')),
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                                               DG.COMPANY_ID),
                             'no')),
                   TO_NUMBER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('FISCAL YEAR OFFSET',
                                                                                   DG.COMPANY_ID),
                                 '0')),
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
                                                                               DG.COMPANY_ID),
                             '0')),
                   0,
                   0,
                   0,
                   0,
                   DG.COMPANY_ID,
                   DG.SUBLEDGER_TYPE_ID,
                   DG.DESCRIPTION,
                   DG.DEPR_METHOD_ID,
                   DG.MID_PERIOD_CONV,
                   DG.MID_PERIOD_METHOD,
                   DG.EST_ANN_NET_ADDS,
                   DG.TRUE_UP_CPR_DEPR,
                   DMR.RATE,
                   DMR.NET_GROSS,
                   DMR.OVER_DEPR_CHECK,
                   DMR.NET_SALVAGE_PCT,
                   DMR.RATE_USED_CODE,
                   DMR.END_OF_LIFE,
                   DMR.EXPECTED_AVERAGE_LIFE,
                   DMR.RESERVE_RATIO_ID,
                   DMR.COST_OF_REMOVAL_RATE,
                   DMR.COST_OF_REMOVAL_PCT,
                   DMR.SALVAGE_RATE,
                   DMR.INTEREST_RATE,
                   DMR.AMORTIZABLE_LIFE,
                   DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1),
                   DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1),
                   DMR.NET_SALVAGE_AMORT_LIFE,
                   DMR.ALLOCATION_PROCEDURE,
                   DL.DEPR_LEDGER_STATUS,
                   DL.BEGIN_RESERVE,
                   DL.END_RESERVE,
                   DL.RESERVE_BAL_PROVISION,
                   DL.RESERVE_BAL_COR,
                   DL.SALVAGE_BALANCE,
                   DL.RESERVE_BAL_ADJUST,
                   DL.RESERVE_BAL_RETIREMENTS,
                   DL.RESERVE_BAL_TRAN_IN,
                   DL.RESERVE_BAL_TRAN_OUT,
                   DL.RESERVE_BAL_OTHER_CREDITS,
                   DL.RESERVE_BAL_GAIN_LOSS,
                   DL.RESERVE_ALLOC_FACTOR,
                   DL.BEGIN_BALANCE,
                   DL.ADDITIONS,
                   DL.RETIREMENTS,
                   DL.TRANSFERS_IN,
                   DL.TRANSFERS_OUT,
                   DL.ADJUSTMENTS,
                   DL.DEPRECIATION_BASE,
                   DL.END_BALANCE,
                   DL.DEPRECIATION_RATE,
                   DL.DEPRECIATION_EXPENSE,
                   DL.DEPR_EXP_ADJUST,
                   DL.DEPR_EXP_ALLOC_ADJUST,
                   DL.COST_OF_REMOVAL,
                   DL.RESERVE_RETIREMENTS,
                   DL.SALVAGE_RETURNS,
                   DL.SALVAGE_CASH,
                   DL.RESERVE_CREDITS,
                   DL.RESERVE_ADJUSTMENTS,
                   DL.RESERVE_TRAN_IN,
                   DL.RESERVE_TRAN_OUT,
                   DL.GAIN_LOSS,
                   DL.VINTAGE_NET_SALVAGE_AMORT,
                   DL.VINTAGE_NET_SALVAGE_RESERVE,
                   DL.CURRENT_NET_SALVAGE_AMORT,
                   DL.CURRENT_NET_SALVAGE_RESERVE,
                   DL.IMPAIRMENT_RESERVE_BEG,
                   DL.IMPAIRMENT_RESERVE_ACT,
                   DL.IMPAIRMENT_RESERVE_END,
                   DL.EST_ANN_NET_ADDS,
                   DL.RWIP_ALLOCATION,
                   DL.COR_BEG_RESERVE,
                   DL.COR_EXPENSE,
                   DL.COR_EXP_ADJUST,
                   DL.COR_EXP_ALLOC_ADJUST,
                   DL.COR_RES_TRAN_IN,
                   DL.COR_RES_TRAN_OUT,
                   DL.COR_RES_ADJUST,
                   DL.COR_END_RESERVE,
                   DL.COST_OF_REMOVAL_RATE,
                   DL.COST_OF_REMOVAL_BASE,
                   DL.RWIP_COST_OF_REMOVAL,
                   DL.RWIP_SALVAGE_CASH,
                   DL.RWIP_SALVAGE_RETURNS,
                   DL.RWIP_RESERVE_CREDITS,
                   DL.SALVAGE_RATE,
                   DL.SALVAGE_BASE,
                   DL.SALVAGE_EXPENSE,
                   DL.SALVAGE_EXP_ADJUST,
                   DL.SALVAGE_EXP_ALLOC_ADJUST,
                   DL.RESERVE_BAL_SALVAGE_EXP,
                   DL.RESERVE_BLENDING_ADJUSTMENT,
                   DL.RESERVE_BLENDING_TRANSFER,
                   DL.COR_BLENDING_ADJUSTMENT,
                   DL.COR_BLENDING_TRANSFER,
                   DL.IMPAIRMENT_ASSET_AMOUNT,
                   DL.IMPAIRMENT_EXPENSE_AMOUNT,
                   DL.RESERVE_BAL_IMPAIRMENT
              from DEPR_LEDGER DL,
                   DEPR_GROUP DG,
                   (select DD.*,
                           ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
                      from DEPR_METHOD_RATES DD
                     where DD.EFFECTIVE_DATE <= A_MONTH) DMR
             where DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
               and DMR.THE_ROW = 1
               and DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
               and DMR.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
               and DL.GL_POST_MO_YR = A_MONTH
               and DG.COMPANY_ID = G_IGNOREINACTIVEYES(I)
               and (NVL(DG.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
                   DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
                   DL.COR_BEG_RESERVE <> 0);


   end P_STAGEMONTHENDDEPR;

   --**************************************************************************
   --                            P_DEPRCALC
   --**************************************************************************
   procedure P_DEPRCALC is
      L_STATUS varchar2(254);
   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING Depreciation Calculation');

      update DEPR_CALC_STG set DEPR_CALC_STATUS = 2, DEPR_CALC_MESSAGE = 'Starting to Calc';

      --
      -- IF SMD and end of life provided, the net_gross has to be 1
      -- IF mid_period_method is 'End of Life', then end_of_life has to be provided and net_gross has to be 1
      -- IF mid_period_method is Monthly then end_of_life does not get used
      --
      merge into DEPR_CALC_STG A
      using (select *
               from DEPR_CALC_STG B
              where B.DEPR_CALC_STATUS = 2
                and B.SUBLEDGER_TYPE_ID = 0 MODEL partition
              by(DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR) DIMENSION
              by(MID_PERIOD_METHOD) MEASURES(ADDITIONS,
                             RETIREMENTS,
                             TRANSFERS_IN,
                             TRANSFERS_OUT,
                             ADJUSTMENTS,
                             IMPAIRMENT_ASSET_AMOUNT,
                             SALVAGE_CASH,
                             SALVAGE_RETURNS,
                             CURRENT_NET_SALVAGE_AMORT,
                             RESERVE_CREDITS,
                             RESERVE_TRAN_IN,
                             RESERVE_TRAN_OUT,
                             RESERVE_ADJUSTMENTS,
                             IMPAIRMENT_EXPENSE_AMOUNT,
                             COST_OF_REMOVAL,
                             COR_RES_TRAN_IN,
                             COR_RES_TRAN_OUT,
                             COR_RES_ADJUST,
                             MID_PERIOD_CONV,
                             RATE,
                             NET_SALVAGE_PCT,
                             COST_OF_REMOVAL_PCT,
                             RWIP_SALVAGE_CASH,
                             RWIP_SALVAGE_RETURNS,
                             RWIP_RESERVE_CREDITS,
                             RWIP_COST_OF_REMOVAL,
                             COR_BEG_RESERVE,
                             BEGIN_BALANCE,
                             BEGIN_RESERVE,
                             COR_TREATMENT,
                             SALVAGE_TREATMENT,
                             COR_YTD,
                             SALV_YTD,
                             DNSA_COR_BAL,
                             DNSA_SALV_BAL,
                             END_BALANCE,
                             DEPRECIATION_RATE,
                             DEPRECIATION_BASE,
                             DEPRECIATION_EXPENSE,
                             CALC_MONTH,
                             END_OF_LIFE,
                             NET_GROSS,
                             TRF_IN_EST_ADDS,
                             INCLUDE_RWIP_IN_NET,
                             SALVAGE_BASE,
                             SALVAGE_RATE,
                             SALVAGE_EXPENSE,
                             DMR_SALVAGE_RATE,
                             COST_OF_REMOVAL_BASE,
                             COST_OF_REMOVAL_RATE,
                             COR_EXPENSE,
                             DMR_COST_OF_REMOVAL_RATE,
                             0 as PLANT_ACTIVITY,
                             0 as RESERVE_ACTIVITY,
                             0 as RWIP_SALVAGE,
                             0 as COR_ACTIVITY,
                             0 as REMAINING_MONTHS,
                             0 as RWIP_COR,
                             -- new vars
                             BEGIN_BALANCE_QTR,
                             BEGIN_RESERVE_QTR,
                             FISCALQTRSTART,
                             BEGIN_BALANCE_YEAR,
                             BEGIN_RESERVE_YEAR,
                             FISCALYEARSTART,
                             SMOOTH_CURVE,
                             GAIN_LOSS,
                             0 as PLANT_ACTIVITY_2,
                             0 as RESERVE_ACTIVITY_2,
                             0 as COR_ACTIVITY_2,
                             EST_NET_ADDS,
                             0 as CUMULATIVE_TRANSFERS,
                             0 as CUM_UOP_DEPR,
                             0 as CUM_UOP_DEPR_2,
                             TYPE_2_EXIST,
                             RESERVE_DIFF,
                             PRODUCTION,
                             ESTIMATED_PRODUCTION,
                             PRODUCTION_2,
                             ESTIMATED_PRODUCTION_2,
                             CURR_UOP_EXP,
                             CURR_UOP_EXP_2,
                             MIN_CALC,
                             MIN_UOP_EXP,
                             YTD_UOP_DEPR,
                             LESS_YEAR_UOP_DEPR,
                             DEPRECIATION_UOP_RATE,
                             DEPRECIATION_UOP_RATE_2,
                             DEPRECIATION_BASE_UOP,
                             DEPRECIATION_BASE_UOP_2,
                             0 as COR_END_RESERVE,
                             0 as END_RESERVE) RULES
              update(
                           -- prep activity and begin balances based on flags and settings
                           -- applies to ALL mid period methods.
                           PLANT_ACTIVITY [ any ] = case
                              when TRF_IN_EST_ADDS [ CV(MID_PERIOD_METHOD) ] = 'no' then
                               ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                               ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                               CV(MID_PERIOD_METHOD) ]
                              else
                               ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                               ] + TRANSFERS_IN [ CV(MID_PERIOD_METHOD) ] + TRANSFERS_OUT [
                               CV(MID_PERIOD_METHOD) ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD) ]
                           end,
                           -- reserve activity needs to take into account salvage amort if it has any (multiply salvage by the salvage treatment
                           --  So if salvage treatment is yes (1) the cash and returns are included.  If salvage treatment is no (0 they are not).
                           --  Since 1 times anything is anything.  And 0 times anything is zero
                           RESERVE_ACTIVITY [ any ] = case
                              when TRF_IN_EST_ADDS [ CV(MID_PERIOD_METHOD) ] = 'no' then
                               RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD)
                               ] + SALVAGE_RETURNS [ CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                               ] - CURRENT_NET_SALVAGE_AMORT [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD)
                               ] - (SALVAGE_TREATMENT [ CV(MID_PERIOD_METHOD)
                                ] * (SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                                 CV(MID_PERIOD_METHOD) ]))
                              else
                               RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD)
                               ] + SALVAGE_RETURNS [ CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_TRAN_IN [ CV(MID_PERIOD_METHOD)
                               ] + RESERVE_TRAN_OUT [ CV(MID_PERIOD_METHOD) ] + RESERVE_ADJUSTMENTS [
                               CV(MID_PERIOD_METHOD) ] - CURRENT_NET_SALVAGE_AMORT [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD)
                               ] - (SALVAGE_TREATMENT [ CV(MID_PERIOD_METHOD)
                                ] * (SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                                 CV(MID_PERIOD_METHOD) ]))
                           end,
                           COR_ACTIVITY [ any ] = case
                              when TRF_IN_EST_ADDS [ CV(MID_PERIOD_METHOD) ] = 'no' then
                               COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_ADJUST [
                               CV(MID_PERIOD_METHOD) ]
                              else
                               COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_IN [
                               CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_OUT [ CV(MID_PERIOD_METHOD)
                               ] + COR_RES_ADJUST [ CV(MID_PERIOD_METHOD) ]
                           end,

                           PLANT_ACTIVITY_2 [ MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = case
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                               ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                               CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] != 'no' then
                               ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                               ] + TRANSFERS_IN [ CV(MID_PERIOD_METHOD) ] + TRANSFERS_OUT [
                               CV(MID_PERIOD_METHOD) ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] != 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               ((ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                                ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                                CV(MID_PERIOD_METHOD) ]) /
                               (3 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALQTRSTART [ CV(MID_PERIOD_METHOD) ]))) * 3 --multiply by 3 when smooth_curve is YES
                              else
                               ((ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                                ] + TRANSFERS_IN [ CV(MID_PERIOD_METHOD) ] + TRANSFERS_OUT [
                                CV(MID_PERIOD_METHOD) ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                                ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD) ]) /
                               (3 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALQTRSTART [ CV(MID_PERIOD_METHOD) ]))) * 3 --multiply by 3 when smooth_curve is YES
                           end,
                           PLANT_ACTIVITY_2 [ MID_PERIOD_METHOD in ('Yearly', 'SFY') ] = case
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                               ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                               CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] != 'no' then
                               ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                               ] + TRANSFERS_IN [ CV(MID_PERIOD_METHOD) ] + TRANSFERS_OUT [
                               CV(MID_PERIOD_METHOD) ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] != 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               ((ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                                ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                                CV(MID_PERIOD_METHOD) ]) /
                               (12 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALYEARSTART [ CV(MID_PERIOD_METHOD) ]))) * 12 --multiply by 12 when smooth_curve = YES
                              else
                               ((ADDITIONS [ CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(MID_PERIOD_METHOD)
                                ] + TRANSFERS_IN [ CV(MID_PERIOD_METHOD) ] + TRANSFERS_OUT [
                                CV(MID_PERIOD_METHOD) ] + ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                                ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD) ]) /
                               (12 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALYEARSTART [ CV(MID_PERIOD_METHOD) ]))) * 12 --multiply by 12 when smooth_curve = YES
                           end,

                           RESERVE_ACTIVITY_2 [ MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = case
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [ CV(MID_PERIOD_METHOD)
                               ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [ CV(MID_PERIOD_METHOD)
                               ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_EXPENSE_AMOUNT [
                               CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] != 'no' then
                               RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [ CV(MID_PERIOD_METHOD)
                               ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [ CV(MID_PERIOD_METHOD)
                               ] + RESERVE_TRAN_IN [ CV(MID_PERIOD_METHOD) ] + RESERVE_TRAN_OUT [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] != 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               ((RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [ CV(MID_PERIOD_METHOD)
                                ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                                CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [ CV(MID_PERIOD_METHOD)
                                ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                                CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD) ]) /
                               (3 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALQTRSTART [ CV(MID_PERIOD_METHOD) ]))) * 3 --multiply by 3 when smooth_curve is YES
                              else
                               ((RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [ CV(MID_PERIOD_METHOD)
                                ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                                CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [ CV(MID_PERIOD_METHOD)
                                ] + RESERVE_TRAN_IN [ CV(MID_PERIOD_METHOD) ] + RESERVE_TRAN_OUT [
                                CV(MID_PERIOD_METHOD) ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                                ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD)
                                ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD) ]) /
                               (3 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALQTRSTART [ CV(MID_PERIOD_METHOD) ]))) * 3 --multiply by 3 when smooth_curve is YES
                           end,
                           RESERVE_ACTIVITY_2 [ MID_PERIOD_METHOD in ('Yearly', 'SFY') ] = case
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [ CV(MID_PERIOD_METHOD)
                               ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [ CV(MID_PERIOD_METHOD)
                               ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                               CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] != 'no' then
                               RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [ CV(MID_PERIOD_METHOD)
                               ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [ CV(MID_PERIOD_METHOD)
                               ] + RESERVE_TRAN_IN [ CV(MID_PERIOD_METHOD) ] + RESERVE_TRAN_OUT [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD)
                               ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] != 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               ((RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [ CV(MID_PERIOD_METHOD)
                                ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                                CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [ CV(MID_PERIOD_METHOD)
                                ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                                CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD) ]) /
                               (12 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALYEARSTART [ CV(MID_PERIOD_METHOD) ]))) * 12 --multiply by 12 when smooth_curve = YES
                              else
                               ((RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [ CV(MID_PERIOD_METHOD)
                                ] + SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + SALVAGE_RETURNS [
                                CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [ CV(MID_PERIOD_METHOD)
                                ] + RESERVE_TRAN_IN [ CV(MID_PERIOD_METHOD) ] + RESERVE_TRAN_OUT [
                                CV(MID_PERIOD_METHOD) ] + RESERVE_ADJUSTMENTS [ CV(MID_PERIOD_METHOD)
                                ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD)
                                ] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD) ]) /
                               (12 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALYEARSTART [ CV(MID_PERIOD_METHOD) ]))) * 12 --multiply by 12 when smooth_curve = YES
                           end,

                           COR_ACTIVITY_2 [ MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = case
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_ADJUST [
                               CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] != 'no' then
                               COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_IN [
                               CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_OUT [ CV(MID_PERIOD_METHOD)
                               ] + COR_RES_ADJUST [ CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] != 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               (COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_ADJUST [
                                CV(MID_PERIOD_METHOD) ]) /
                               (3 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALQTRSTART [ CV(MID_PERIOD_METHOD) ]))
                              else
                               (COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_IN [
                                CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_OUT [ CV(MID_PERIOD_METHOD)
                                ] + COR_RES_ADJUST [ CV(MID_PERIOD_METHOD) ]) /
                               (3 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALQTRSTART [ CV(MID_PERIOD_METHOD) ]))
                           end,
                           COR_ACTIVITY_2 [ MID_PERIOD_METHOD in ('Yearly', 'SFY') ] = case
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_ADJUST [
                               CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] = 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] != 'no' then
                               COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_IN [
                               CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_OUT [ CV(MID_PERIOD_METHOD)
                               ] + COR_RES_ADJUST [ CV(MID_PERIOD_METHOD) ]
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] != 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] = 'no' then
                               (COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_ADJUST [
                                CV(MID_PERIOD_METHOD) ]) /
                               (12 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALYEARSTART [ CV(MID_PERIOD_METHOD) ]))
                              when SMOOTH_CURVE [ CV(MID_PERIOD_METHOD) ] != 'no' and TRF_IN_EST_ADDS [
                               CV(MID_PERIOD_METHOD) ] != 'no' then
                               (COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_IN [
                                CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_OUT [ CV(MID_PERIOD_METHOD)
                                ] + COR_RES_ADJUST [ CV(MID_PERIOD_METHOD) ]) /
                               (12 -
                               MONTHS_BETWEEN(CV(GL_POST_MO_YR), FISCALYEARSTART [ CV(MID_PERIOD_METHOD) ]))
                           end,

                           -- begin reserve needs to take into account cor and salvage treatment.
                           BEGIN_RESERVE [ any ] = case
                              when TRF_IN_EST_ADDS [ CV(MID_PERIOD_METHOD) ] = 'no' then
                               BEGIN_RESERVE [ CV(MID_PERIOD_METHOD) ] + RESERVE_TRAN_IN [
                               CV(MID_PERIOD_METHOD) ] + RESERVE_TRAN_OUT [ CV(MID_PERIOD_METHOD)
                               ] + (COR_TREATMENT [ CV(MID_PERIOD_METHOD)
                                ] * (COR_BEG_RESERVE [ CV(MID_PERIOD_METHOD) ] - DNSA_COR_BAL [
                                 CV(MID_PERIOD_METHOD) ] - COR_YTD [ CV(MID_PERIOD_METHOD) ])) -
                               (SALVAGE_TREATMENT [ CV(MID_PERIOD_METHOD)
                                ] *
                                (DNSA_SALV_BAL [ CV(MID_PERIOD_METHOD) ] + SALV_YTD [ CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_RESERVE [ CV(MID_PERIOD_METHOD)
                               ] + (COR_TREATMENT [ CV(MID_PERIOD_METHOD)
                                ] * (COR_BEG_RESERVE [ CV(MID_PERIOD_METHOD) ] - DNSA_COR_BAL [
                                 CV(MID_PERIOD_METHOD) ] - COR_YTD [ CV(MID_PERIOD_METHOD) ])) -
                               (SALVAGE_TREATMENT [ CV(MID_PERIOD_METHOD)
                                ] *
                                (DNSA_SALV_BAL [ CV(MID_PERIOD_METHOD) ] + SALV_YTD [ CV(MID_PERIOD_METHOD) ]))
                           end,
                           BEGIN_BALANCE [ any ] = case
                              when TRF_IN_EST_ADDS [ CV(MID_PERIOD_METHOD) ] = 'no' then
                               BEGIN_BALANCE [ CV(MID_PERIOD_METHOD) ] + TRANSFERS_IN [
                               CV(MID_PERIOD_METHOD) ] + TRANSFERS_OUT [ CV(MID_PERIOD_METHOD) ]
                              else
                               BEGIN_BALANCE [ CV(MID_PERIOD_METHOD) ]
                           end,

                           --
                           --  Certain methods require net_gross = 1 if end of life is given
                           --
                           NET_GROSS [ MID_PERIOD_METHOD in ('Monthly', 'End of Life', 'SMD') ] = case
                              when END_OF_LIFE [ CV(MID_PERIOD_METHOD) ] is not null then
                               1
                              else
                               NET_GROSS [ CV(MID_PERIOD_METHOD) ]
                           end,

                           -- if including RWIP and net_gross is 1
                           RWIP_SALVAGE [ any ] = case
                              when INCLUDE_RWIP_IN_NET [ CV(MID_PERIOD_METHOD) ] = 'yes' and NET_GROSS [
                               CV(MID_PERIOD_METHOD) ] = 1 then
                               RWIP_SALVAGE_CASH [ CV(MID_PERIOD_METHOD) ] + RWIP_SALVAGE_RETURNS [
                               CV(MID_PERIOD_METHOD) ] + RWIP_RESERVE_CREDITS [ CV(MID_PERIOD_METHOD) ]
                              else
                               0
                           end,
                           RWIP_COR [ any ] = case
                              when INCLUDE_RWIP_IN_NET [ CV(MID_PERIOD_METHOD) ] = 'yes' and NET_GROSS [
                               CV(MID_PERIOD_METHOD) ] = 1 then
                               RWIP_COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD) ]
                              else
                               0
                           end,

                           -- remaining months if end of life provided (min value of 1)
                           -- ELSE use a 0
                           REMAINING_MONTHS [ any ] = case
                              when END_OF_LIFE [ CV(MID_PERIOD_METHOD) ] is not null then
                               GREATEST(1,
                                        1 +
                                        MONTHS_BETWEEN(TO_DATE(TO_CHAR(END_OF_LIFE [ CV(MID_PERIOD_METHOD) ]),
                                                               'yyyymm'),
                                                       CALC_MONTH [ CV(MID_PERIOD_METHOD) ]))
                              else
                               0
                           end,

                           -- Start mid_period_method rate calcs

                           -- GET The depreciation rates for MONTHLY.
                           DEPRECIATION_RATE [ 'Monthly' ] = RATE [ 'Monthly' ],
                           SALVAGE_RATE [ 'Monthly' ] = DMR_SALVAGE_RATE [ 'Monthly' ],
                           COST_OF_REMOVAL_RATE [ 'Monthly' ] = DMR_COST_OF_REMOVAL_RATE [ 'Monthly' ],

                           -- GET The depreciation rates for END OF LIFE.  Salv rate and cor rate are same
                           DEPRECIATION_RATE [ 'End of Life'
                           ] = 12 / GREATEST(1, REMAINING_MONTHS [ 'End of Life' ]),
                           SALVAGE_RATE [ 'End of Life' ] = DEPRECIATION_RATE [ 'End of Life' ],
                           COST_OF_REMOVAL_RATE [ 'End of Life' ] = DEPRECIATION_RATE [ 'End of Life' ],

                           -- GET the depreciation rates for SMD
                           DEPRECIATION_RATE [ 'SMD' ] = case
                              when END_OF_LIFE [ CV(MID_PERIOD_METHOD) ] is null then
                               RATE [ CV(MID_PERIOD_METHOD) ]
                              else
                               12 * (REMAINING_MONTHS [ 'SMD' ] / ((REMAINING_MONTHS [ 'SMD'
                                 ] * (REMAINING_MONTHS [ 'SMD' ] + 1)) / 2))
                           end,
                           SALVAGE_RATE [ 'SMD' ] = case
                              when END_OF_LIFE [ CV(MID_PERIOD_METHOD) ] is null then
                               DMR_SALVAGE_RATE [ CV(MID_PERIOD_METHOD) ]
                              else -- same as depreciation rate
                               DEPRECIATION_RATE [ 'SMD' ]
                           end,
                           COST_OF_REMOVAL_RATE [ 'SMD' ] = case
                              when END_OF_LIFE [ CV(MID_PERIOD_METHOD) ] is null then
                               DMR_COST_OF_REMOVAL_RATE [ CV(MID_PERIOD_METHOD) ]
                              else -- same as depreciation rate
                               DEPRECIATION_RATE [ 'SMD' ]
                           end,

                           -- take into account net salvage now for end of life provided
                           DEPRECIATION_RATE [ MID_PERIOD_METHOD in ('End of Life', 'SMD') ] = case
                              when END_OF_LIFE [ CV(MID_PERIOD_METHOD) ] is not null and NET_SALVAGE_PCT [
                               CV(MID_PERIOD_METHOD) ] <> 1 then
                               DEPRECIATION_RATE [ CV(MID_PERIOD_METHOD)
                               ] / (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ])
                              else
                               DEPRECIATION_RATE [ CV(MID_PERIOD_METHOD) ]
                           end,
                           SALVAGE_RATE [ MID_PERIOD_METHOD in ('End of Life', 'SMD') ] = case
                              when END_OF_LIFE [ CV(MID_PERIOD_METHOD) ] is not null then
                               case
                                  when NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ] <> 1 then
                                   SALVAGE_RATE [ CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [
                                   CV(MID_PERIOD_METHOD)
                                   ] * -1 / (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ])
                                  else
                                   0
                               end
                              else
                               SALVAGE_RATE [ CV(MID_PERIOD_METHOD) ]
                           end,

                           --GET the depreciation rates for Quarterly, QTR
                           DEPRECIATION_RATE [ MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = RATE [
                           CV(MID_PERIOD_METHOD) ],
                           SALVAGE_RATE [ MID_PERIOD_METHOD in ('Quarterly', 'QTR')
                           ] = DMR_SALVAGE_RATE [ CV(MID_PERIOD_METHOD) ],
                           COST_OF_REMOVAL_RATE [ MID_PERIOD_METHOD in ('Quarterly', 'QTR')
                           ] = DMR_COST_OF_REMOVAL_RATE [ CV(MID_PERIOD_METHOD) ],

                           --GET the depreciation rates for Yearly
                           DEPRECIATION_RATE [ 'Yearly' ] = RATE [ 'Yearly' ],
                           SALVAGE_RATE [ 'Yearly' ] = DMR_SALVAGE_RATE [ 'Yearly' ],
                           COST_OF_REMOVAL_RATE [ 'Yearly' ] = DMR_COST_OF_REMOVAL_RATE [ 'Yearly' ],

                           --GET the depreciation rates for SFY
                           DEPRECIATION_RATE [ 'SFY' ] = RATE [ 'SFY' ],
                           SALVAGE_RATE [ 'SFY' ] = DMR_SALVAGE_RATE [ 'SFY' ],
                           COST_OF_REMOVAL_RATE [ 'SFY' ] = DMR_COST_OF_REMOVAL_RATE [ 'SFY' ],

                           --GET the depreciation rates for curve
                           DEPRECIATION_RATE [ 'curve' ] = RATE [ 'curve' ],
                           SALVAGE_RATE [ 'curve' ] = DMR_SALVAGE_RATE [ 'curve' ],
                           COST_OF_REMOVAL_RATE [ 'curve' ] = DMR_COST_OF_REMOVAL_RATE [ 'curve' ],

                           --GET the depreciation rates for curve_no_true_up
                           DEPRECIATION_RATE [ 'curve_no_true_up' ] = RATE [ 'curve_no_true_up' ],
                           SALVAGE_RATE [ 'curve_no_true_up' ] = DMR_SALVAGE_RATE [
                           'curve_no_true_up' ],
                           COST_OF_REMOVAL_RATE [ 'curve_no_true_up' ] = DMR_COST_OF_REMOVAL_RATE [
                           'curve_no_true_up' ],

                           --GET the depreciation rates for UOP
                           DEPRECIATION_RATE [ 'UOP' ] = case
                              when END_OF_LIFE [ CV(MID_PERIOD_METHOD) ] is not null and NET_GROSS [
                               CV(MID_PERIOD_METHOD) ] = 1 then
                              --if net/gross is net then end-of-life will be used to derive rate
                               12 / REMAINING_MONTHS [ 'UOP' ]
                              else
                              --if net/gross is gross then end-of-life is not used
                               RATE [ 'UOP' ]
                           end,

                           DEPRECIATION_UOP_RATE [ 'UOP' ] = case
                              when ESTIMATED_PRODUCTION [ CV(MID_PERIOD_METHOD) ] <> 0 then
                               (PRODUCTION [ CV(MID_PERIOD_METHOD) ] / ESTIMATED_PRODUCTION [
                                CV(MID_PERIOD_METHOD) ]) * 12
                              else
                               12
                           end,

                           DEPRECIATION_UOP_RATE_2 [ 'UOP' ] = case
                              when TYPE_2_EXIST [ CV(MID_PERIOD_METHOD) ] = 1 and ESTIMATED_PRODUCTION_2 [
                               CV(MID_PERIOD_METHOD) ] <> 0 then
                               (PRODUCTION_2 [ CV(MID_PERIOD_METHOD) ] / ESTIMATED_PRODUCTION_2 [
                                CV(MID_PERIOD_METHOD) ]) * 12
                              when TYPE_2_EXIST [ CV(MID_PERIOD_METHOD) ] = 1 and ESTIMATED_PRODUCTION_2 [
                               CV(MID_PERIOD_METHOD) ] = 0 then
                               12
                              else
                               null
                           end,
                           -- unsure of salvage_rate and cor_rate for UOP
                           SALVAGE_RATE [ 'UOP' ] = DMR_SALVAGE_RATE [ 'UOP' ],
                           COST_OF_REMOVAL_RATE [ 'UOP' ] = DMR_COST_OF_REMOVAL_RATE [ 'UOP' ],

                           -- Start mid_period_method base calcs

                           --GET the depreciation bases for Monthly, End of Life, and SMD
                           DEPRECIATION_BASE [ MID_PERIOD_METHOD in ('Monthly', 'End of Life', 'SMD')
                           ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE [ CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - BEGIN_RESERVE [
                                CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD)
                                 ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - RESERVE_ACTIVITY [
                                 CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE [ CV(MID_PERIOD_METHOD)
                               ] + (PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(MID_PERIOD_METHOD) ])
                           end,
                           -- salvage base is the same as depreciation base
                           SALVAGE_BASE [ MID_PERIOD_METHOD in ('Monthly', 'End of Life', 'SMD')
                           ] = DEPRECIATION_BASE [ CV(MID_PERIOD_METHOD) ],
                           COST_OF_REMOVAL_BASE [
                           MID_PERIOD_METHOD in ('Monthly', 'End of Life', 'SMD') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE [ CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                CV(MID_PERIOD_METHOD) ] - COR_BEG_RESERVE [ CV(MID_PERIOD_METHOD)
                                ] - RWIP_COR [ CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                 CV(MID_PERIOD_METHOD) ] - COR_ACTIVITY [ CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE [ CV(MID_PERIOD_METHOD)
                               ] + (PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(MID_PERIOD_METHOD) ])
                           end,

                           --GET the depreciation bases for Quarterly, QTR
                           DEPRECIATION_BASE [ MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE_QTR [ CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - BEGIN_RESERVE_QTR [
                                CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY_2 [ CV(MID_PERIOD_METHOD)
                                 ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - RESERVE_ACTIVITY_2 [
                                 CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE_QTR [ CV(MID_PERIOD_METHOD)
                               ] + (PLANT_ACTIVITY_2 [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(MID_PERIOD_METHOD) ])
                           end,
                           SALVAGE_BASE [ MID_PERIOD_METHOD in ('Quarterly', 'QTR')
                           ] = DEPRECIATION_BASE [ CV(MID_PERIOD_METHOD) ],
                           COST_OF_REMOVAL_BASE [ MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE_QTR [ CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                CV(MID_PERIOD_METHOD) ] - BEGIN_RESERVE_QTR [ CV(MID_PERIOD_METHOD)
                                ] - RWIP_COR [ CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY_2 [ CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                 CV(MID_PERIOD_METHOD) ] - COR_ACTIVITY_2 [ CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE_QTR [ CV(MID_PERIOD_METHOD)
                               ] + (PLANT_ACTIVITY_2 [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(MID_PERIOD_METHOD) ])
                           end,

                           --GET the depreciation bases for Yearly, SFY
                           DEPRECIATION_BASE [ MID_PERIOD_METHOD in ('Yearly', 'SFY') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE_YEAR [ CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - BEGIN_RESERVE_YEAR [
                                CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY_2 [ CV(MID_PERIOD_METHOD)
                                 ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - RESERVE_ACTIVITY_2 [
                                 CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE_YEAR [ CV(MID_PERIOD_METHOD)
                               ] + (PLANT_ACTIVITY_2 [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(MID_PERIOD_METHOD) ])
                           end,
                           SALVAGE_BASE [ MID_PERIOD_METHOD in ('Yearly', 'SFY')
                           ] = DEPRECIATION_BASE [ CV(MID_PERIOD_METHOD) ],
                           COST_OF_REMOVAL_BASE [ MID_PERIOD_METHOD in ('Yearly', 'SFY') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE_YEAR [ CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                CV(MID_PERIOD_METHOD) ] -
                               --not sure if cor_beg_reserve is right. Might need some sort of "year" equivalent
                                COR_BEG_RESERVE [ CV(MID_PERIOD_METHOD) ] - RWIP_COR [ CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY_2 [ CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                 CV(MID_PERIOD_METHOD) ] - COR_ACTIVITY_2 [ CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE_YEAR [ CV(MID_PERIOD_METHOD)
                               ] + (PLANT_ACTIVITY_2 [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(MID_PERIOD_METHOD) ])
                           end,

                           --GET the depreciation bases for curve, curve_no_true_up
                           DEPRECIATION_BASE [ MID_PERIOD_METHOD in ('curve', 'curve_no_true_up') ] = case
                              when TRF_IN_EST_ADDS [ CV(MID_PERIOD_METHOD) ] = 'no' then
                               BEGIN_BALANCE_YEAR [ CV(MID_PERIOD_METHOD) ] + CUMULATIVE_TRANSFERS [
                               CV(MID_PERIOD_METHOD) ] + (EST_NET_ADDS [ CV(MID_PERIOD_METHOD)
                                ] * MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD) ])
                              else
                               BEGIN_BALANCE_YEAR [ CV(MID_PERIOD_METHOD)
                               ] + (EST_NET_ADDS [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(MID_PERIOD_METHOD) ])
                           end,
                           SALVAGE_BASE [ MID_PERIOD_METHOD in ('curve', 'curve_no_true_up')
                           ] = DEPRECIATION_BASE [ CV(MID_PERIOD_METHOD) ],
                           --not sure of curve, curve_no_true_up COR base so using depr base for now.
                           COST_OF_REMOVAL_BASE [ MID_PERIOD_METHOD in ('curve', 'curve_no_true_up')
                           ] = DEPRECIATION_BASE [ CV(MID_PERIOD_METHOD) ],

                           --GET the depreciation bases for UOP
                           DEPRECIATION_BASE [ MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE [ CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - BEGIN_RESERVE [
                                CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD)
                                 ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - RESERVE_ACTIVITY [
                                 CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE [ CV(MID_PERIOD_METHOD)
                               ] + (PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(MID_PERIOD_METHOD) ])
                           end,

                           DEPRECIATION_BASE_UOP [ MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE [ CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - RESERVE_DIFF [
                                CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(MID_PERIOD_METHOD)
                                ] - CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD)
                                 ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - RESERVE_ACTIVITY [
                                 CV(MID_PERIOD_METHOD) ]))
                           end,

                           DEPRECIATION_BASE_UOP_2 [ MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               case
                                  when TYPE_2_EXIST [ CV(MID_PERIOD_METHOD) ] = 1 then
                                   (BEGIN_BALANCE [ CV(MID_PERIOD_METHOD)
                                    ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - RESERVE_DIFF [
                                    CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(MID_PERIOD_METHOD)
                                    ] - CUM_UOP_DEPR_2 [ CV(MID_PERIOD_METHOD) ]) +
                                   (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD)
                                    ] * (PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD)
                                     ] * (1 - NET_SALVAGE_PCT [ CV(MID_PERIOD_METHOD) ]) - RESERVE_ACTIVITY [
                                     CV(MID_PERIOD_METHOD) ]))
                               end
                           end,

                           SALVAGE_BASE [ MID_PERIOD_METHOD in ('UOP') ] = DEPRECIATION_BASE [
                           CV(MID_PERIOD_METHOD) ],
                           --not sure of UOP COR base so using depr base for now.
                           COST_OF_REMOVAL_BASE [ MID_PERIOD_METHOD in ('UOP') ] = DEPRECIATION_BASE [
                           CV(MID_PERIOD_METHOD) ],

                           --BEGIN THE CALCS TO DETERMINE WHICH UOP BASE AND RATE TO USE
                           -- uop rate determining calc is here instead of up with rates because curr_uop_exp (_2) need all the bases and rates calculated before they can be calculated
                           CURR_UOP_EXP [ MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                               DEPRECIATION_BASE_UOP [ 'UOP' ] * (DEPRECIATION_UOP_RATE [ 'UOP' ] / 12)
                              else
                               DEPRECIATION_BASE [ 'UOP' ] * (DEPRECIATION_UOP_RATE [ 'UOP' ] / 12)
                           end,

                           CURR_UOP_EXP_2 [ MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 and TYPE_2_EXIST [
                               CV(MID_PERIOD_METHOD) ] = 1 then
                               DEPRECIATION_BASE_UOP_2 [ 'UOP' ] * (DEPRECIATION_UOP_RATE_2 [ 'UOP' ] / 12)
                              when NET_GROSS [ CV(MID_PERIOD_METHOD) ] <> 1 and TYPE_2_EXIST [
                               CV(MID_PERIOD_METHOD) ] = 1 then
                               DEPRECIATION_BASE [ 'UOP' ] * (DEPRECIATION_UOP_RATE_2 [ 'UOP' ] / 12)
                           end,

                           DEPRECIATION_RATE [ MID_PERIOD_METHOD in ('UOP') ] = case
                              when ((NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1) and
                                   ((MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 1 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] = YTD_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 2 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] = CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 3 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] = CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ] - LESS_YEAR_UOP_DEPR [
                                    CV(MID_PERIOD_METHOD) ]))) then
                               DEPRECIATION_UOP_RATE [ 'UOP' ]
                              when ((NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 and TYPE_2_EXIST [
                                    CV(MID_PERIOD_METHOD) ] = 1) and
                                   ((MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 1 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] <> YTD_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 2 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] <> CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 3 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] <> CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ] - LESS_YEAR_UOP_DEPR [
                                    CV(MID_PERIOD_METHOD) ]))) then
                               DEPRECIATION_UOP_RATE_2 [ 'UOP' ]
                              else
                               DEPRECIATION_RATE [ 'UOP' ]
                           end,

                           DEPRECIATION_BASE [ MID_PERIOD_METHOD in ('UOP') ] = case
                              when ((NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1) and
                                   ((MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 1 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] = YTD_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 2 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] = CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 3 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] = CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ] - LESS_YEAR_UOP_DEPR [
                                    CV(MID_PERIOD_METHOD) ]))) then
                               DEPRECIATION_BASE_UOP [ 'UOP' ]
                              when ((NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 and TYPE_2_EXIST [
                                    CV(MID_PERIOD_METHOD) ] = 1) and
                                   ((MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 1 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] <> YTD_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 2 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] <> CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(MID_PERIOD_METHOD) ] = 3 and MIN_UOP_EXP [
                                    CV(MID_PERIOD_METHOD) ] <> CUM_UOP_DEPR [ CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(MID_PERIOD_METHOD) ] - LESS_YEAR_UOP_DEPR [
                                    CV(MID_PERIOD_METHOD) ]))) then
                               DEPRECIATION_BASE_UOP_2 [ 'UOP' ]
                              else
                               DEPRECIATION_BASE [ 'UOP' ]
                           end,

                           -- ALL Mid Period Methods
                           END_BALANCE [ any ] = BEGIN_BALANCE [ CV(MID_PERIOD_METHOD)
                           ] + PLANT_ACTIVITY [ CV(MID_PERIOD_METHOD) ],
                           DEPRECIATION_EXPENSE [ any ] = DEPRECIATION_BASE [ CV(MID_PERIOD_METHOD)
                           ] * DEPRECIATION_RATE [ CV(MID_PERIOD_METHOD) ] / 12,
                           SALVAGE_EXPENSE [ any ] = SALVAGE_BASE [ CV(MID_PERIOD_METHOD)
                           ] * SALVAGE_RATE [ CV(MID_PERIOD_METHOD) ] / 12,
                           COR_EXPENSE [ any ] = COST_OF_REMOVAL_BASE [ CV(MID_PERIOD_METHOD)
                           ] * COST_OF_REMOVAL_RATE [ CV(MID_PERIOD_METHOD) ] / 12,
                           END_RESERVE [ any ] = BEGIN_RESERVE [ CV(MID_PERIOD_METHOD)
                           ] + RESERVE_ACTIVITY[ CV(MID_PERIOD_METHOD) ] + DEPRECIATION_EXPENSE [ CV(MID_PERIOD_METHOD) ] + SALVAGE_EXPENSE [ CV(MID_PERIOD_METHOD) ],
                           COR_END_RESERVE [ any ] = COR_BEG_RESERVE [ CV(MID_PERIOD_METHOD)
                           ] + COR_EXPENSE [ CV(MID_PERIOD_METHOD) ] + COR_ACTIVITY [ CV(MID_PERIOD_METHOD) ])) B
      on (A.DEPR_GROUP_ID = B.DEPR_GROUP_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POST_MO_YR = B.GL_POST_MO_YR)
      when matched then
         update
            set END_BALANCE = B.END_BALANCE, DEPRECIATION_BASE = B.DEPRECIATION_BASE,
                END_RESERVE = B.END_RESERVE,
                COR_END_RESERVE = B.COR_END_RESERVE,
                DEPRECIATION_RATE = B.DEPRECIATION_RATE,
                DEPRECIATION_EXPENSE = B.DEPRECIATION_EXPENSE, SALVAGE_BASE = B.SALVAGE_BASE,
                SALVAGE_RATE = B.SALVAGE_RATE, SALVAGE_EXPENSE = B.SALVAGE_EXPENSE,
                COST_OF_REMOVAL_BASE = B.COST_OF_REMOVAL_BASE,
                COST_OF_REMOVAL_RATE = B.COST_OF_REMOVAL_RATE, COR_EXPENSE = B.COR_EXPENSE,
                DEPR_CALC_STATUS = 5, DEPR_CALC_MESSAGE = 'Calculated'
         --, plant_activity_2 = b.plant_activity_2, reserve_activity_2 = b.reserve_activity_2, cor_activity_2 = b.cor_activity_2
         ;
      --'depr_exp_alloc_adjust', depr_exp_adj + retro_depr_exp_ad
      --'salvage_exp_alloc_adjust', salv_exp_adj + retro_salv_exp_adj
      --'cor_end_reserve', (begin_cor_month + cor_act + cor_prov + retro_cor_exp_adj + cor_exp_adj + cor_exp_adjust_in)
      --'cor_exp_alloc_adjust', retro_cor_exp_adj + cor_exp_adj
      update DEPR_CALC_STG set DEPR_CALC_STATUS = 9, DEPR_CALC_MESSAGE = 'End of Calc';
   exception
      when others then
         L_STATUS := sqlerrm;
         update DEPR_CALC_STG
            set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Calc: ' || L_STATUS;
   end P_DEPRCALC;


  function F_OVERDEPR_STAGE return number
   is
      L_STATUS varchar2(254);
   begin

    l_status := 'Starting Over Depr Check (LIFE)';
    insert into depr_calc_over_stg
      (DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, TYPE,
      END_BALANCE, END_RESERVE, NET_SALVAGE_PCT, EXPENSE,
      RETRO_EXPENSE, SIGN_BALANCE, SIGN_RESERVE, SIGN_EXPENSE)
    select DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, 'LIFE',
          END_BALANCE, END_RESERVE, NET_SALVAGE_PCT, DEPRECIATION_EXPENSE+SALVAGE_EXPENSE,
          0 as RETRO_EXPENSE, sign(END_BALANCE), sign(END_RESERVE), sign(DEPRECIATION_EXPENSE+SALVAGE_EXPENSE)
      from DEPR_CALC_STG
      where OVER_DEPR_CHECK in (1,2)
        and DEPRECIATION_EXPENSE + SALVAGE_EXPENSE <> 0;

      l_status := 'Starting Over Depr Check (COR)';
    insert into depr_calc_over_stg
      (DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, TYPE,
      END_BALANCE, END_RESERVE, NET_SALVAGE_PCT, EXPENSE,
      RETRO_EXPENSE, SIGN_BALANCE, SIGN_RESERVE, SIGN_EXPENSE)
    select DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, 'COR',
          END_BALANCE, COR_END_RESERVE, 1-COST_OF_REMOVAL_PCT, COR_EXPENSE,
          0 as RETRO_EXPENSE, sign(END_BALANCE), sign(COR_END_RESERVE), sign(COR_EXPENSE)
      from DEPR_CALC_STG
      where OVER_DEPR_CHECK in (1,4)
        and COR_EXPENSE <> 0;

      l_status := 'Starting Over Depr Check (COMBINED)';
      insert into depr_calc_over_stg
      (DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, TYPE,
      END_BALANCE, END_RESERVE, NET_SALVAGE_PCT, EXPENSE,
      RETRO_EXPENSE, SIGN_BALANCE, SIGN_RESERVE, SIGN_EXPENSE)
    select DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, 'COMB',
          END_BALANCE, END_RESERVE+COR_END_RESERVE, NET_SALVAGE_PCT-COST_OF_REMOVAL_PCT, DEPRECIATION_EXPENSE+SALVAGE_EXPENSE+COR_EXPENSE,
          0 as RETRO_EXPENSE, sign(END_BALANCE), sign(END_RESERVE+COR_END_RESERVE), sign(DEPRECIATION_EXPENSE+SALVAGE_EXPENSE+COR_EXPENSE)
      from DEPR_CALC_STG
      where OVER_DEPR_CHECK in (3)
        and DEPRECIATION_EXPENSE + SALVAGE_EXPENSE + COR_EXPENSE <> 0;

    --This eliminates a lot of conditions...
    l_status := 'Updating balance sign';
    update DEPR_CALC_OVER_STG
      set SIGN_BALANCE = SIGN_RESERVE
      where SIGN_BALANCE = 0;

    l_status := 'Updating reserve sign';
    update DEPR_CALC_OVER_STG
      set SIGN_RESERVE = SIGN_BALANCE
      where SIGN_RESERVE = 0;

    l_status := 'First delete';
    delete from DEPR_CALC_OVER_STG
      where SIGN_BALANCE = SIGN_RESERVE
      and SIGN_BALANCE <> SIGN_EXPENSE;

    l_status := 'Second delete';
    delete from DEPR_CALC_OVER_STG
      where SIGN_BALANCE <> SIGN_RESERVE
      and SIGN_BALANCE = SIGN_EXPENSE;

    return 1;

  exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr Stage: ' || L_STATUS;
      return -1;
   end F_OVERDEPR_STAGE;


  function F_OVERDEPR_CALC return number
   is
      L_STATUS varchar2(254);
   begin

    L_STATUS := 'Starting Over Depr Calc';
    update DEPR_CALC_OVER_STG
    set OVER_DEPR_AMOUNT =
      case
        when SIGN_BALANCE + SIGN_RESERVE + SIGN_EXPENSE = 3 then
          least(0, END_BALANCE*(1-NET_SALVAGE_PCT) - END_RESERVE)
        when SIGN_BALANCE + SIGN_RESERVE + SIGN_EXPENSE = -3 then
          greatest(0, END_BALANCE*(1-NET_SALVAGE_PCT) - END_RESERVE)
        else
          -1 * sign(EXPENSE) * least(abs(EXPENSE+RETRO_EXPENSE), abs(END_RESERVE))
      end
    ;

    L_STATUS := 'Updating over depr check amount';
    update DEPR_CALC_OVER_STG
    set OVER_DEPR_CHECK_AMOUNT = greatest(abs(EXPENSE), abs(EXPENSE+RETRO_EXPENSE))
    where abs(SIGN_BALANCE + SIGN_RESERVE + SIGN_EXPENSE) = 3
    and OVER_DEPR_AMOUNT <> 0
    ;

    L_STATUS := 'Updating over depr amount';
    update DEPR_CALC_OVER_STG
    set OVER_DEPR_AMOUNT = -1 * SIGN_EXPENSE * OVER_DEPR_CHECK_AMOUNT
    where abs(OVER_DEPR_AMOUNT) > abs(OVER_DEPR_CHECK_AMOUNT)
    and OVER_DEPR_AMOUNT <> 0
    ;


    return 1;

  exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr Calc: ' || L_STATUS;
      return -1;
   end F_OVERDEPR_CALC;


  function F_OVERDEPR_BACKFILL return number
  is
    L_STATUS varchar2(254);
   begin

    L_STATUS := 'Starting over depr backfill';
    update DEPR_CALC_STG s1
    set (OVER_DEPR_ADJ, OVER_DEPR_ADJ_COR) =
    (
      select
        sum(case when "TYPE" = 'COR' then 0 else OVER_DEPR_AMOUNT end),
        sum(case when "TYPE" = 'COR' then OVER_DEPR_AMOUNT else 0 end)
      from DEPR_CALC_OVER_STG s2
      where s2.DEPR_GROUP_ID = s1.DEPR_GROUP_ID
        and s2.GL_POST_MO_YR = s1.GL_POST_MO_YR
        and s2.SET_OF_BOOKS_ID = s1.SET_OF_BOOKS_ID
    )
    where s1.OVER_DEPR_CHECK in (1,2,3,4)
    ;

    return 1;

  exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr Backfill: ' || L_STATUS;
      return -1;
   end F_OVERDEPR_BACKFILL;


  function F_OVERDEPR_PRORATE return number
   is
      L_STATUS varchar2(254);
   begin

      -- prorate between COR and LIFE when combined check (over_depr = 3)
      l_status := 'PRORATE COR for combined';
      update depr_calc_stg
      set over_depr_adj_cor = over_depr_adj * ( cor_expense / (cor_expense + salvage_expense + depreciation_expense) )
      where over_depr_adj <> 0
      and cor_expense <> 0
      and over_depr_check = 3
      ;

      l_status := 'PRORATE COR for combined (2)';
      update depr_calc_stg
      set over_depr_adj = over_depr_adj - over_depr_adj_cor
      where over_depr_adj_cor <> 0
      and over_depr_check = 3
      ;

      -- PRORATE THE SALVAGE when trueup LIFE if a salvage expense calculated (1, 2, 3)
      l_status := 'PRORATE SALVAGE';
      update depr_calc_stg
      set over_depr_adj_salv = over_depr_adj * ( salvage_expense / (salvage_expense + depreciation_expense) )
      where over_depr_adj <> 0
      and salvage_expense <> 0
      and over_depr_check in (1, 2, 3)
      ;

      l_status := 'PRORATE SALVAGE (2)';
      update depr_calc_stg
      set over_depr_adj = over_depr_adj - over_depr_adj_salv
      where over_depr_adj_salv <> 0
      and over_depr_check in (1, 2, 3)
      ;

    return 1;

  exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr Prorate: ' || L_STATUS;
      return -1;
   end F_OVERDEPR_PRORATE;


  /*
  PLANT  RESERVE  EXPENSE  COMMENT                          CASE

  -1       -1      -1      Valid Combination                Case 2
  -1       -1       0      Removed Because Expense is 0
  -1       -1       1      Removed in first delete
  -1        0      -1      Removed in second delete
  -1        0       0      Removed Because Expense is 0
  -1        0       1      Valid Combination                Case Else
  -1        1      -1      Removed in second delete
  -1        1       0      Removed Because Expense is 0
  -1        1       1      Valid Combination                Case Else
   0       -1      -1      Valid Combination                Case Else
   0       -1       0      Removed Because Expense is 0
   0       -1       1      Valid Combination                Case Else
   0        0      -1      Removed in first delete
   0        0       0      Removed Because Expense is 0
   0        0       1      Removed in first delete
   0        1      -1      Valid Combination                Case Else
   0        1       0      Removed Because Expense is 0
   0        1       1      Valid Combination                Case Else
   1       -1      -1      Valid Combination                Case Else
   1       -1       0      Removed Because Expense is 0
   1       -1       1      Removed in second delete
   1        0      -1      Valid Combination                Case Else
   1        0       0      Removed Because Expense is 0
   1        0       1      Removed in second delete
   1        1      -1      Removed in first delete
   1        1       0      Removed Because Expense is 0
   1        1       1      Valid Combination                Case 1
  */
  function F_OVERDEPR return number
   is
      L_STATUS varchar2(254);
   begin
      --
      -- BSB TODO: Add a retro adj column for COR and LIFE,  ADD OVER_DEPR_ADJ columns (life, cor, salvage)
      --    LOOKUP THE COLUMNS FOR COR END values
      --    MAKE SURE THE END BALANCE, END RESERVE are updated as part of the MODEL statement
      --

    if F_OVERDEPR_STAGE <> 1 then
      return -1;
    end if;

    if F_OVERDEPR_CALC <> 1 then
      return -1;
    end if;

    if F_OVERDEPR_BACKFILL <> 1 then
      return -1;
    end if;

    if F_OVERDEPR_PRORATE <> 1 then
      return -1;
    end if;

    return 1;

   exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr: ' || L_STATUS;
      return -1;
   end F_OVERDEPR;

   function F_BACKFILLDATA return number is
      L_STATUS varchar2(254);
   begin
      --fiscal qtrs based on pp_calendars instead of pp_table_months
      update DEPR_CALC_STG STG
         set (FISCALQTRSTART, FISCAL_MONTH) =
              (select ADD_MONTHS(STG.FISCALYEARSTART, (CAL.FISCAL_QUARTER - 1) * 3), CAL.FISCAL_MONTH
                 from PP_CALENDAR CAL
                where CAL.MONTH = STG.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('QTR', 'Quarterly');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      -- begin_balance_qtr and begin_reserve_qtr for Quarterly
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_QTR, BEGIN_RESERVE_QTR) =
              (select NVL(DL.BEGIN_BALANCE, 0), NVL(DL.BEGIN_RESERVE, 0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALQTRSTART)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('QTR', 'Quarterly');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      -- begin_balance_year and begin_reserve_year for Yearly, SFY
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR) =
              (select NVL(DL.BEGIN_BALANCE, 0), NVL(DL.BEGIN_RESERVE, 0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALYEARSTART)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('Yearly', 'SFY');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      -- begin_balance_year and begin_reserve_year for curve, curve_no_true_up
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_YEAR) =
              (select NVL(DL.BEGIN_BALANCE, 0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALYEARSTART)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('curve', 'curve_no_true_up');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      -- est_net_adds for curve_no_true_up and curve
      update DEPR_CALC_STG STG
         set EST_NET_ADDS =
              (select NVL(EST_ANN_NET_ADDS, 0)
                 from DEPR_GROUP DG
                where DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('curve_no_true_up', 'curve');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      -- overwrite est_net_adds for curve if in fiscal month = 12
      update DEPR_CALC_STG STG
         set EST_NET_ADDS =
              (select case
                         when STG.TRF_IN_EST_ADDS = 'no' then
                          sum(ADDITIONS + RETIREMENTS + ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT)
                         else
                          sum(ADDITIONS + RETIREMENTS + TRANSFERS_IN + TRANSFERS_OUT + ADJUSTMENTS +
                              IMPAIRMENT_ASSET_AMOUNT)
                      end
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and STG.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and FISCAL_MONTH = 12
         and MID_PERIOD_METHOD in ('curve');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --cumulative_transfers for curve, curve_no_true_up
      update DEPR_CALC_STG STG
         set CUMULATIVE_TRANSFERS =
              (select sum(DL.TRANSFERS_IN + DL.TRANSFERS_OUT)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and
                      ADD_MONTHS(STG.GL_POST_MO_YR, -1))
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('curve', 'curve_no_true_up')
         and TRF_IN_EST_ADDS = 'no';

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --type_2_exist for UOP
      update DEPR_CALC_STG STG
         set TYPE_2_EXIST = NVL((select case
                                           when UOP_TYPE_ID2 > 0 then
                                            1
                                           else
                                            0
                                        end
                                   from DEPR_METHOD_UOP M, DEPR_GROUP_UOP G
                                  where G.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                                    and G.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                                    and G.GL_POST_MO_YR = STG.GL_POST_MO_YR
                                    and M.DEPR_METHOD_ID = STG.DEPR_METHOD_ID
                                    and M.SET_OF_BOOKS_ID = G.SET_OF_BOOKS_ID
                                    and M.GL_POST_MO_YR = G.GL_POST_MO_YR),
                                 0)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --cum_uop_depr, cum_uop_depr_2 for UOP
      update DEPR_CALC_STG STG
         set (CUM_UOP_DEPR, CUM_UOP_DEPR_2) =
              (select NVL(sum(UOP_DEPR_EXPENSE), 0), NVL(sum(UOP_DEPR_EXPENSE_2), 0)
                 from DEPR_GROUP_UOP DGO
                where DGO.GL_POST_MO_YR < STG.GL_POST_MO_YR
                  and DGO.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DGO.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --reserve_diff for UOP
      update DEPR_CALC_STG STG
         set STG.RESERVE_DIFF =
              (select STG.BEGIN_RESERVE -
                      (select sum(DGO.LEDGER_DEPR_EXPENSE)
                         from DEPR_GROUP_UOP DGO
                        where DGO.GL_POST_MO_YR < STG.GL_POST_MO_YR
                          and DGO.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                          and DGO.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
                 from DUAL)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --production, estimated_production, production_2, estimated_production_2, min_calc for UOP
      update DEPR_CALC_STG STG
         set (PRODUCTION, ESTIMATED_PRODUCTION, PRODUCTION_2, ESTIMATED_PRODUCTION_2, MIN_CALC) =
              (select PRODUCTION,
                      ESTIMATED_PRODUCTION,
                      PRODUCTION_2,
                      ESTIMATED_PRODUCTION_2,
                      MIN_CALC_OPTION
                 from DEPR_METHOD_UOP M, DEPR_GROUP_UOP G
                where G.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and G.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and G.GL_POST_MO_YR = STG.GL_POST_MO_YR
                  and M.DEPR_METHOD_ID = STG.DEPR_METHOD_ID
                  and M.SET_OF_BOOKS_ID = G.SET_OF_BOOKS_ID
                  and M.GL_POST_MO_YR = G.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --ytd_uop_depr, ytd_uop_depr_2 for UOP
      update DEPR_CALC_STG STG
         set (YTD_UOP_DEPR, YTD_UOP_DEPR_2) =
              (select NVL(sum(UOP_DEPR_EXPENSE), 0), NVL(sum(UOP_DEPR_EXPENSE_2), 0)
                 from DEPR_GROUP_UOP DG
                where DG.GL_POST_MO_YR < STG.GL_POST_MO_YR
                  and DG.GL_POST_MO_YR >= STG.FISCALYEARSTART
                  and DG.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --less_year_uop_depr for UOP
      update DEPR_CALC_STG STG
         set LESS_YEAR_UOP_DEPR =
              (select NVL(sum(LEDGER_DEPR_EXPENSE), 0)
                 from DEPR_GROUP_UOP DG
                where DG.GL_POST_MO_YR < STG.FISCALYEARSTART
                  and DG.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP')
         and MIN_CALC in (3);

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      -- have the information for min_uop_exp. get min_uop_exp for UOP for mincalc 1 or 2
      update DEPR_CALC_STG
         set MIN_UOP_EXP = GREATEST(YTD_UOP_DEPR + CURR_UOP_EXP, YTD_UOP_DEPR_2 + CURR_UOP_EXP_2)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP')
         and MIN_CALC in (1, 2);

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      -- have the information for min_uop_exp. get min_uop_exp for UOP for mincalc 3
      update DEPR_CALC_STG
         set MIN_UOP_EXP = GREATEST(CUM_UOP_DEPR + CURR_UOP_EXP - LESS_YEAR_UOP_DEPR,
                                     CUM_UOP_DEPR_2 + CURR_UOP_EXP_2 - LESS_YEAR_UOP_DEPR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP')
         and MIN_CALC in (3);

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      return 1;
   exception
      when others then
         L_STATUS := sqlerrm;
         update DEPR_CALC_STG set DEPR_CALC_STATUS = -25, DEPR_CALC_MESSAGE = L_STATUS;

         return -1;
   end F_BACKFILLDATA;

   --**************************************************************************
   --                            F_PREPDEPRCALC
   --**************************************************************************
   --
   -- This function performs the depreciation calculation
   -- PREREQS: The staging table depr_calc_stg is already populated by some other means
   -- ALLOWING: this function to calculate depreciation independently of type of calc or what process initiated
   --

   function F_PREPDEPRCALC return number is

   begin
      P_MISSINGSETSOFBOOKS();
      P_SET_FISCALYEARSTART();
      if F_BACKFILLDATA() = -1 then
         return - 1;
      end if;
      P_CORSALVAMORT();
      P_DEPRCALC();
      if F_OVERDEPR <> 1 then
        return -1;
      end if;
      return 1;

   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return - 1;
   end F_PREPDEPRCALC;

   --**************************************************************************
   --                            F_FIND_DEPR_GROUP
   --**************************************************************************
   function F_FIND_DEPR_GROUP(A_COMPANY_ID         number,
                              A_GL_ACCOUNT_ID      number,
                              A_MAJOR_LOCATION_ID  number,
                              A_UTILITY_ACCOUNT_ID number,
                              A_BUS_SEGMENT_ID     number,
                              A_SUB_ACCOUNT_ID     number,
                              A_SUBLEDGER_TYPE_ID  number,
                              A_BOOK_VINTAGE       number,
                              A_ASSET_LOCATION_ID  number,
                              A_LOCATION_TYPE_ID   number,
                              A_RETIREMENT_UNIT_ID number,
                              A_PROPERTY_UNIT_ID   number,
                              A_CLASS_CODE_ID      number,
                              A_CC_VALUE           varchar2) return number is
      DG_ID  number;
      STATUS number;
      CC_VAL varchar2(35);

   begin

      if A_CC_VALUE is null then
         CC_VAL := 'NO CLASS CODE';
      else
         CC_VAL := A_CC_VALUE;
      end if;

      select DEPR_GROUP_ID, STATUS_ID
        into DG_ID, STATUS
        from (select DEPR_GROUP_CONTROL.DEPR_GROUP_ID DEPR_GROUP_ID, NVL(STATUS_ID, 1) STATUS_ID
                from DEPR_GROUP_CONTROL, DEPR_GROUP
               where DEPR_GROUP.COMPANY_ID = A_COMPANY_ID
                 and DEPR_GROUP_CONTROL.BUS_SEGMENT_ID = A_BUS_SEGMENT_ID
                 and DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID = A_UTILITY_ACCOUNT_ID
                 and NVL(DEPR_GROUP_CONTROL.GL_ACCOUNT_ID, A_GL_ACCOUNT_ID) = A_GL_ACCOUNT_ID
                 and NVL(DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID, A_MAJOR_LOCATION_ID) =
                     A_MAJOR_LOCATION_ID
                 and NVL(DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID, A_SUB_ACCOUNT_ID) = A_SUB_ACCOUNT_ID
                 and NVL(TO_NUMBER(TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY')), A_BOOK_VINTAGE) =
                     A_BOOK_VINTAGE
                 and NVL(DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID, A_SUBLEDGER_TYPE_ID) =
                     A_SUBLEDGER_TYPE_ID
                 and NVL(DEPR_GROUP_CONTROL.ASSET_LOCATION_ID, A_ASSET_LOCATION_ID) =
                     A_ASSET_LOCATION_ID
                 and NVL(DEPR_GROUP_CONTROL.LOCATION_TYPE_ID, A_LOCATION_TYPE_ID) =
                     A_LOCATION_TYPE_ID
                 and NVL(DEPR_GROUP_CONTROL.PROPERTY_UNIT_ID, A_PROPERTY_UNIT_ID) =
                     A_PROPERTY_UNIT_ID
                 and NVL(DEPR_GROUP_CONTROL.RETIREMENT_UNIT_ID, A_RETIREMENT_UNIT_ID) =
                     A_RETIREMENT_UNIT_ID
                 and NVL(DEPR_GROUP_CONTROL.CLASS_CODE_ID, A_CLASS_CODE_ID) = A_CLASS_CODE_ID
                 and NVL(DEPR_GROUP_CONTROL.CC_VALUE, CC_VAL) = CC_VAL
                 and DEPR_GROUP_CONTROL.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID
                 and DECODE(G_CHECK_DG_BS,
                            'YES',
                            to_char(DEPR_GROUP_CONTROL.BUS_SEGMENT_ID),
                            'NO_BUS_SEG_CHECK') =
                     DECODE(G_CHECK_DG_BS, 'YES', to_char(DEPR_GROUP.BUS_SEGMENT_ID), 'NO_BUS_SEG_CHECK')
               order by DEPR_GROUP.COMPANY_ID,
                        DEPR_GROUP_CONTROL.BUS_SEGMENT_ID,
                        DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.GL_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID,
                        DEPR_GROUP_CONTROL.ASSET_LOCATION_ID,
                        DEPR_GROUP_CONTROL.LOCATION_TYPE_ID,
                        DEPR_GROUP_CONTROL.PROPERTY_UNIT_ID,
                        DEPR_GROUP_CONTROL.RETIREMENT_UNIT_ID,
                        TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY'),
                        DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID,
                        DEPR_GROUP_CONTROL.CLASS_CODE_ID,
                        DEPR_GROUP_CONTROL.CC_VALUE)
       where ROWNUM = 1;

      if STATUS = 1 then
         return DG_ID;
      else
         /* invalid depr group */
         if G_DG_STATUS_CHECK then
            return - 9;
         else
            return DG_ID;
         end if;
      end if;
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return - 1;
   end;

   --**************************************************************************
   --                            F_GET_DG_CC_ID
   --**************************************************************************
   function F_GET_DG_CC_ID return number is
      DG_CC_DESC varchar2(35);
      DG_CC_ID   number;

   begin

      select CONTROL_VALUE
        into DG_CC_DESC
        from PP_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = 'DEPR GROUP CLASS CODE';

      select CLASS_CODE_ID
        into DG_CC_ID
        from CLASS_CODE
       where UPPER(trim(DESCRIPTION)) = UPPER(trim(DG_CC_DESC));

      return DG_CC_ID;

   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return 0;
   end;

   --**************************************************************************
   --                            P_DG_STATUS_CHECK
   --**************************************************************************
   procedure P_DG_STATUS_CHECK(A_ENABLE boolean) is

   begin
      G_DG_STATUS_CHECK := A_ENABLE;
   end P_DG_STATUS_CHECK;

   --**************************************************************************
   --                            P_CHECK_DG_BS
   --**************************************************************************
   procedure P_CHECK_DG_BS is
      PP_CONTROL_VALUE varchar2(35);

   begin
      select UPPER(trim(CONTROL_VALUE))
        into PP_CONTROL_VALUE
        from PP_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = 'POST VALIDATE DEPR BUSINESS SEGMENT';

      if PP_CONTROL_VALUE = 'YES' then
         G_CHECK_DG_BS := 'YES';
      else
         G_CHECK_DG_BS := 'NO';
      end if;
   end P_CHECK_DG_BS;

--**************************************************************************************
-- Initialization procedure (Called the first time this package is opened in a session)
-- Initialize the VALIDATE DEPR BUSINESS SEGMENT variable
--**************************************************************************************
begin
   P_CHECK_DG_BS;

end PP_DEPR_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (670, 0, 10, 4, 1, 1, 30564, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_030564_depr_PP_DEPR_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
