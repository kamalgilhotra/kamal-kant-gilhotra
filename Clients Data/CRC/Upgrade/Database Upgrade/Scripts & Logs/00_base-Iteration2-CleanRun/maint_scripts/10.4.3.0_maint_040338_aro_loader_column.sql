/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040338_aro_loader_column.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/15/2014 Ryan Oliveria
||============================================================================
*/

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select aro.aro_id from aro where upper(trim(aro.description)) = upper(trim(<importfield>)))'
 where LOWER(trim(DESCRIPTION)) = 'aro description'
   and LOWER(trim(COLUMN_NAME)) = 'aro_id'
   and LOWER(trim(LOOKUP_TABLE_NAME)) = 'aro'
   and LOWER(trim(LOOKUP_COLUMN_NAME)) = 'description';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1541, 0, 10, 4, 3, 0, 40338, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040338_aro_loader_column.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;