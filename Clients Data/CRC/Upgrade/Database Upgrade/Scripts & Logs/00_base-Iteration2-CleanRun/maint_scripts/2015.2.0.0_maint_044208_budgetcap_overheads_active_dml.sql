/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_044208_budgetcap_overheads_active_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/02/2015 Ryan Oliveria  Redesign Overhead Setup
||============================================================================
*/

update CLEARING_WO_CONTROL_BDG set ACTIVE = 1 where ACTIVE is null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2681, 0, 2015, 2, 0, 0, 044208, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044208_budgetcap_overheads_active_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;