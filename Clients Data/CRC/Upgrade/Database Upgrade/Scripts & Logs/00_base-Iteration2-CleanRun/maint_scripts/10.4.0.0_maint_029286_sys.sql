/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029286_sys.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- --------------   ------------------------------------
|| 10.4.0.0   02/20/2013 Charlie Shilling Point Release
||============================================================================
*/

alter table PEND_TRANSACTION         add ADJUSTED_RESERVE number(22,2);
alter table PEND_TRANSACTION_ARCHIVE add ADJUSTED_RESERVE number(22,2);
alter table PEND_TRANSACTION_TEMP    add ADJUSTED_RESERVE number(22,2);

update PEND_TRANSACTION set ADJUSTED_RESERVE = RESERVE;

alter table PEND_TRANSACTION_SET_OF_BOOKS add ADJUSTED_RESERVE number(22,2);
alter table PEND_TRANSACTION_SOB_ARC      add ADJUSTED_RESERVE number(22,2);

update PEND_TRANSACTION_SET_OF_BOOKS set ADJUSTED_RESERVE = RESERVE;

alter table PEND_TRANSACTION_SET_OF_BOOKS drop column FERC_ACTIVITY_CODE;
alter table PEND_TRANSACTION_SOB_ARC      drop column FERC_ACTIVITY_CODE;

alter table PEND_TRANSACTION_SET_OF_BOOKS drop column POSTING_QUANTITY;
alter table PEND_TRANSACTION_SOB_ARC      drop column POSTING_QUANTITY;


create or replace trigger PEND_TRANS_CHECK_UPDATE
   after update on PEND_TRANSACTION
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANS_CHECK_UPDATE
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   ||============================================================================
   */

begin
   if :NEW.RESERVE <> :OLD.RESERVE then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating RESERVE on PEND_TRANSACTION is not permitted. Use ADJUSTED_RESERVE instead.');
   elsif :NEW.COST_OF_REMOVAL <> :OLD.COST_OF_REMOVAL then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating COST_OF_REMOVAL on PEND_TRANSACTION is not permitted. Use ADJUSTED_COST_OF_REMOVAL instead.');
   elsif :NEW.SALVAGE_CASH <> :OLD.SALVAGE_CASH then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating SALVAGE_CASH on PEND_TRANSACTION is not permitted. Use ADJUSTED_SALVAGE_CASH instead.');
   elsif :NEW.SALVAGE_RETURNS <> :OLD.SALVAGE_RETURNS then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating SALVAGE_RETURNS on PEND_TRANSACTION is not permitted. Use ADJUSTED_SALVAGE_RETURNS instead.');
   elsif :NEW.RESERVE_CREDITS <> :OLD.RESERVE_CREDITS then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating RESERVE_CREDITS on PEND_TRANSACTION is not permitted. Use ADJUSTED_RESERVE_CREDITS instead.');
   end if;
end;
/


create or replace trigger PEND_TRANS_SOB_CHECK_UPDATE
   after update on PEND_TRANSACTION_SET_OF_BOOKS
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANS_SOB_CHECK_UPDATE
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   ||============================================================================
   */

begin
   if :NEW.RESERVE <> :OLD.RESERVE then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating RESERVE on PEND_TRANSACTION_SET_OF_BOOKS is not permitted. Use ADJUSTED_RESERVE instead.');
   end if;
end;
/


create or replace trigger PEND_TRANS_ADD_SOB
   after update or insert on PEND_BASIS
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANS_ADD_SOB
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   ||============================================================================
   */

declare
   V_RTN              number(22, 0);
   V_DEPR_GROUP_ID    number(22, 0);
   V_ASSET_ID         number(22, 0);
   V_VINTAGE          number(22, 0);
   V_GL_POSTING_MO_YR date;
   V_ACTIVITY_CODE    varchar2(10);

begin
   update PEND_TRANSACTION
      set (ADJUSTED_COST_OF_REMOVAL,
            ADJUSTED_SALVAGE_CASH,
            ADJUSTED_SALVAGE_RETURNS,
            ADJUSTED_RESERVE_CREDITS) =
           (select A.COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE,
                   A.RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE
              from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
             where A.COMPANY_ID = B.COMPANY_ID
               and B.SET_OF_BOOKS_ID = 1
               and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID)
    where PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   update PEND_TRANSACTION_SET_OF_BOOKS A
      set POSTING_AMOUNT =
           (select sum(NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                       NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                       NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                       NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                       NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                       NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                       NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                       NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                       NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                       NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                       NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                       NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                       NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                       NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                       NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                       NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                       NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                       NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                       NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                       NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                       NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                       NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                       NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                       NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                       NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                       NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                       NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                       NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                       NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                       NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                       NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                       NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                       NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                       NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                       NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                       NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                       NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                       NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                       NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                       NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                       NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                       NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                       NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                       NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                       NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                       NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                       NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                       NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                       NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                       NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                       NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                       NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                       NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                       NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                       NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                       NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                       NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                       NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                       NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                       NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                       NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                       NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                       NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                       NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                       NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                       NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                       NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                       NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                       NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                       NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR)
              from SET_OF_BOOKS B
             where A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID)
    where A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
      and exists (select 1
             from PEND_TRANSACTION C, COMPANY_SET_OF_BOOKS D
            where C.COMPANY_ID = D.COMPANY_ID
              and A.PEND_TRANS_ID = C.PEND_TRANS_ID
              and A.SET_OF_BOOKS_ID = D.SET_OF_BOOKS_ID
              and D.INCLUDE_INDICATOR = 1);

   insert into PEND_TRANSACTION_SET_OF_BOOKS
      (PEND_TRANS_ID, SET_OF_BOOKS_ID, ACTIVITY_CODE, ADJUSTED_COST_OF_REMOVAL,
       ADJUSTED_SALVAGE_CASH, ADJUSTED_SALVAGE_RETURNS, GAIN_LOSS, RESERVE, ADJUSTED_RESERVE,
       ADJUSTED_RESERVE_CREDITS, REPLACEMENT_AMOUNT, GAIN_LOSS_REVERSAL, POSTING_AMOUNT,
       IMPAIRMENT_EXPENSE_AMOUNT)
      select PEND_TRANS_ID,
             SET_OF_BOOKS_ID,
             case
                when NVL(GAIN_LOSS, 0) <> 0 and
                     (trim(ACTIVITY_CODE) = 'URET' or trim(ACTIVITY_CODE) = 'URGL') then
                 'URGL'
                when NVL(GAIN_LOSS, 0) = 0 and
                     (trim(ACTIVITY_CODE) = 'URET' or trim(ACTIVITY_CODE) = 'URGL') then
                 'URET'
                when NVL(GAIN_LOSS, 0) <> 0 and
                     (trim(ACTIVITY_CODE) = 'SALE' or trim(ACTIVITY_CODE) = 'SAGL') then
                 'SAGL'
                when NVL(GAIN_LOSS, 0) = 0 and
                     (trim(ACTIVITY_CODE) = 'SALE' or trim(ACTIVITY_CODE) = 'SAGL') then
                 'SALE'
                else
                 ACTIVITY_CODE
             end ACTIVITY_CODE,
             COST_OF_REMOVAL,
             SALVAGE_CASH,
             SALVAGE_RETURNS,
             GAIN_LOSS,
             RESERVE,
             RESERVE,
             RESERVE_CREDITS,
             REPLACEMENT_AMOUNT,
             GAIN_LOSS_REVERSAL,
             POSTING_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT
        from (select PEND_TRANS_ID,
                     SET_OF_BOOKS_ID,
                     ACTIVITY_CODE,
                     COST_OF_REMOVAL,
                     SALVAGE_CASH,
                     SALVAGE_RETURNS,
                     GET_PEND_SOB_GL(PEND_TRANS_ID, SET_OF_BOOKS_ID, RESERVE, POSTING_AMOUNT) GAIN_LOSS,
                     RESERVE,
                     RESERVE_CREDITS,
                     REPLACEMENT_AMOUNT,
                     GAIN_LOSS_REVERSAL,
                     POSTING_AMOUNT,
                     POSTING_QUANTITY,
                     IMPAIRMENT_EXPENSE_AMOUNT
                from (select A.PEND_TRANS_ID,
                             B.SET_OF_BOOKS_ID,
                             A.ACTIVITY_CODE,
                             NVL(COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE, 0) COST_OF_REMOVAL,
                             NVL(SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_CASH,
                             NVL(SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_RETURNS,
                             GET_PEND_SOB_RESERVE(PEND_TRANS_ID, B.SET_OF_BOOKS_ID) RESERVE,
                             NVL(RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE, 0) RESERVE_CREDITS,
                             NVL(REPLACEMENT_AMOUNT, 0) REPLACEMENT_AMOUNT,
                             NVL(GAIN_LOSS_REVERSAL, 0) GAIN_LOSS_REVERSAL,
                             (select NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                                     NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                                     NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                                     NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                                     NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                                     NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                                     NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                                     NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                                     NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                                     NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                                     NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                                     NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                                     NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                                     NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                                     NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                                     NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                                     NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                                     NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                                     NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                                     NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                                     NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                                     NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                                     NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                                     NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                                     NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                                     NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                                     NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                                     NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                                     NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                                     NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                                     NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                                     NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                                     NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                                     NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                                     NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                                     NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                                     NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                                     NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                                     NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                                     NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                                     NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                                     NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                                     NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                                     NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                                     NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                                     NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                                     NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                                     NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                                     NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                                     NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                                     NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                                     NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                                     NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                                     NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                                     NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                                     NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                                     NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                                     NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                                     NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                                     NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                                     NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                                     NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                                     NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                                     NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                                     NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                                     NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                                     NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                                     NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                                     NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                                     NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR
                                from SET_OF_BOOKS
                               where SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID) POSTING_AMOUNT,
                             NVL(POSTING_QUANTITY, 0) POSTING_QUANTITY,
                             DECODE(trim(ACTIVITY_CODE),
                                    'IMPA',
                                    (A.IMPAIRMENT_EXPENSE_AMOUNT *
                                    DECODE(B.SET_OF_BOOKS_ID,
                                            (select max(SET_OF_BOOKS_ID)
                                               from CPR_IMPAIRMENT_EVENT
                                              where IMPAIRMENT_ID =
                                                    (select TO_NUMBER(SUBSTR(WORK_ORDER_NUMBER, 4, 100))
                                                       from PEND_TRANSACTION
                                                      where PEND_TRANS_ID = :NEW.PEND_TRANS_ID)),
                                            1,
                                            0)),
                                    0) IMPAIRMENT_EXPENSE_AMOUNT
                        from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
                       where A.COMPANY_ID = B.COMPANY_ID
                         and B.SET_OF_BOOKS_ID <> 1
                         and B.INCLUDE_INDICATOR = 1
                         and LOWER(trim(DECODE(A.FERC_ACTIVITY_CODE, 2, A.DESCRIPTION, 'not unretire'))) <>
                             'unretire'
                         and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                         and (PEND_TRANS_ID, SET_OF_BOOKS_ID) in
                             (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                                from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
                               where A.COMPANY_ID = B.COMPANY_ID
                                 and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                              minus
                              select PEND_TRANS_ID, SET_OF_BOOKS_ID
                                from PEND_TRANSACTION_SET_OF_BOOKS
                               where PEND_TRANS_ID = :NEW.PEND_TRANS_ID)));
end;
/


create or replace procedure POST_CHECK_SET_OF_BOOKS(A_COMMIT_FLAG in integer) is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: POST_CHECK_SET_OF_BOOKS
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   ||============================================================================
   */

   MISSING_PEND_TRANS_IDS PEND_TRANS_IDS_T;
   L_MISSING_COUNT        integer;

begin
   DBMS_OUTPUT.PUT_LINE('    Begin post_check_set_of_books Oracle procedure');

   select distinct PEND_TRANS_ID bulk collect
     into MISSING_PEND_TRANS_IDS
     from (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
             from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B, PEND_BASIS C
            where A.COMPANY_ID = B.COMPANY_ID
              and A.PEND_TRANS_ID = C.PEND_TRANS_ID
              and B.INCLUDE_INDICATOR = 1
              and A.POSTING_STATUS = 2
           minus (select A.PEND_TRANS_ID, 1 SET_OF_BOOKS_ID
                   from PEND_TRANSACTION A
                  where A.POSTING_STATUS = 2
                 union all
                 select B.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                   from PEND_TRANSACTION A, PEND_TRANSACTION_SET_OF_BOOKS B
                  where A.PEND_TRANS_ID = B.PEND_TRANS_ID
                    and A.POSTING_STATUS = 2))
    order by PEND_TRANS_ID;

   L_MISSING_COUNT := MISSING_PEND_TRANS_IDS.COUNT();

   DBMS_OUTPUT.PUT_LINE('      Post found ' || L_MISSING_COUNT ||
                        ' transactions with at least one missing set_of_books.');

   if L_MISSING_COUNT > 0 then
      begin
         update PEND_TRANSACTION
            set POSTING_STATUS = 3,
                POSTING_ERROR = 'POST 1019: ERROR: This transaction is missing one or more set_of_books from pend_transaction_set_of_books.'
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));
      exception
         when others then
            update PEND_TRANSACTION
               set POSTING_STATUS = 3
             where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));
      end;

      update PEND_TRANSACTION A
         set POSTING_STATUS = 3
       where POSTING_STATUS = 2
         and exists
       (select 1
                from PEND_TRANSACTION B
               where A.WORK_ORDER_NUMBER = B.WORK_ORDER_NUMBER
                 and A.COMPANY_ID = B.COMPANY_ID
                 and A.GL_JE_CODE = B.GL_JE_CODE
                 and B.PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS)));

      if A_COMMIT_FLAG = 1 then
         commit; --commit here to make sure that the eror gets saved if the next few statements error.
      end if;

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_transaction_temp');
      insert into PEND_TRANSACTION_TEMP
         (PEND_TRANS_ID, LDG_ASSET_ID, LDG_ACTIVITY_ID, TIME_STAMP, USER_ID, LDG_DEPR_GROUP_ID,
          BOOKS_SCHEMA_ID, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, FUNC_CLASS_ID,
          SUB_ACCOUNT_ID, ASSET_LOCATION_ID, GL_ACCOUNT_ID, COMPANY_ID, GL_POSTING_MO_YR,
          SUBLEDGER_INDICATOR, ACTIVITY_CODE, GL_JE_CODE, WORK_ORDER_NUMBER, POSTING_QUANTITY,
          USER_ID1, POSTING_AMOUNT, USER_ID2, IN_SERVICE_YEAR, DESCRIPTION, LONG_DESCRIPTION,
          PROPERTY_GROUP_ID, RETIRE_METHOD_ID, POSTING_ERROR, POSTING_STATUS, COST_OF_REMOVAL,
          SALVAGE_CASH, SALVAGE_RETURNS, GAIN_LOSS, RESERVE, MISC_DESCRIPTION, FERC_ACTIVITY_CODE,
          SERIAL_NUMBER, RESERVE_CREDITS, REPLACEMENT_AMOUNT, GAIN_LOSS_REVERSAL, USER_ID3,
          DISPOSITION_CODE, MINOR_PEND_TRAN_ID, WIP_COMP_TRANSACTION, WIP_COMPUTATION_ID,
          TAX_ORIG_MONTH_NUMBER, IMPAIRMENT_EXPENSE_AMOUNT, ADJUSTED_COST_OF_REMOVAL,
          ADJUSTED_SALVAGE_CASH, ADJUSTED_SALVAGE_RETURNS, ADJUSTED_RESERVE_CREDITS,
          ADJUSTED_RESERVE)
         select PEND_TRANS_ID,
                LDG_ASSET_ID,
                LDG_ACTIVITY_ID,
                TIME_STAMP,
                USER_ID,
                LDG_DEPR_GROUP_ID,
                BOOKS_SCHEMA_ID,
                RETIREMENT_UNIT_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                FUNC_CLASS_ID,
                SUB_ACCOUNT_ID,
                ASSET_LOCATION_ID,
                GL_ACCOUNT_ID,
                COMPANY_ID,
                GL_POSTING_MO_YR,
                SUBLEDGER_INDICATOR,
                ACTIVITY_CODE,
                GL_JE_CODE,
                WORK_ORDER_NUMBER,
                POSTING_QUANTITY,
                USER_ID1,
                POSTING_AMOUNT,
                USER_ID2,
                IN_SERVICE_YEAR,
                DESCRIPTION,
                LONG_DESCRIPTION,
                PROPERTY_GROUP_ID,
                RETIRE_METHOD_ID,
                POSTING_ERROR,
                POSTING_STATUS,
                COST_OF_REMOVAL,
                SALVAGE_CASH,
                SALVAGE_RETURNS,
                GAIN_LOSS,
                RESERVE,
                MISC_DESCRIPTION,
                FERC_ACTIVITY_CODE,
                SERIAL_NUMBER,
                RESERVE_CREDITS,
                REPLACEMENT_AMOUNT,
                GAIN_LOSS_REVERSAL,
                USER_ID3,
                DISPOSITION_CODE,
                MINOR_PEND_TRAN_ID,
                WIP_COMP_TRANSACTION,
                WIP_COMPUTATION_ID,
                TAX_ORIG_MONTH_NUMBER,
                IMPAIRMENT_EXPENSE_AMOUNT,
                ADJUSTED_COST_OF_REMOVAL,
                ADJUSTED_SALVAGE_CASH,
                ADJUSTED_SALVAGE_RETURNS,
                ADJUSTED_RESERVE_CREDITS,
                ADJUSTED_RESERVE
           from PEND_TRANSACTION
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_basis_temp');
      insert into PEND_BASIS_TEMP
         (PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, GL_ACCT_1, BASIS_2, GL_ACCT_2, BASIS_3,
          GL_ACCT_3, BASIS_4, GL_ACCT_4, BASIS_5, GL_ACCT_5, BASIS_6, GL_ACCT_6, BASIS_7, GL_ACCT_7,
          BASIS_8, GL_ACCT_8, BASIS_9, GL_ACCT_9, BASIS_10, GL_ACCT_10, BASIS_11, GL_ACCT_11,
          BASIS_12, GL_ACCT_12, BASIS_13, GL_ACCT_13, BASIS_14, GL_ACCT_14, BASIS_15, GL_ACCT_15,
          BASIS_16, GL_ACCT_16, BASIS_17, GL_ACCT_17, BASIS_18, GL_ACCT_18, BASIS_19, GL_ACCT_19,
          BASIS_20, GL_ACCT_20, BASIS_21, GL_ACCT_21, BASIS_22, GL_ACCT_22, BASIS_23, GL_ACCT_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70, GL_ACCT_24, GL_ACCT_25, GL_ACCT_26, GL_ACCT_27, GL_ACCT_28, GL_ACCT_29,
          GL_ACCT_30, GL_ACCT_31, GL_ACCT_32, GL_ACCT_33, GL_ACCT_34, GL_ACCT_35, GL_ACCT_36,
          GL_ACCT_37, GL_ACCT_38, GL_ACCT_39, GL_ACCT_40, GL_ACCT_41, GL_ACCT_42, GL_ACCT_43,
          GL_ACCT_44, GL_ACCT_45, GL_ACCT_46, GL_ACCT_47, GL_ACCT_48, GL_ACCT_49, GL_ACCT_50,
          GL_ACCT_51, GL_ACCT_52, GL_ACCT_53, GL_ACCT_54, GL_ACCT_55, GL_ACCT_56, GL_ACCT_57,
          GL_ACCT_58, GL_ACCT_59, GL_ACCT_60, GL_ACCT_61, GL_ACCT_62, GL_ACCT_63, GL_ACCT_64,
          GL_ACCT_65, GL_ACCT_66, GL_ACCT_67, GL_ACCT_68, GL_ACCT_69, GL_ACCT_70)
         select PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                BASIS_1,
                GL_ACCT_1,
                BASIS_2,
                GL_ACCT_2,
                BASIS_3,
                GL_ACCT_3,
                BASIS_4,
                GL_ACCT_4,
                BASIS_5,
                GL_ACCT_5,
                BASIS_6,
                GL_ACCT_6,
                BASIS_7,
                GL_ACCT_7,
                BASIS_8,
                GL_ACCT_8,
                BASIS_9,
                GL_ACCT_9,
                BASIS_10,
                GL_ACCT_10,
                BASIS_11,
                GL_ACCT_11,
                BASIS_12,
                GL_ACCT_12,
                BASIS_13,
                GL_ACCT_13,
                BASIS_14,
                GL_ACCT_14,
                BASIS_15,
                GL_ACCT_15,
                BASIS_16,
                GL_ACCT_16,
                BASIS_17,
                GL_ACCT_17,
                BASIS_18,
                GL_ACCT_18,
                BASIS_19,
                GL_ACCT_19,
                BASIS_20,
                GL_ACCT_20,
                BASIS_21,
                GL_ACCT_21,
                BASIS_22,
                GL_ACCT_22,
                BASIS_23,
                GL_ACCT_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70,
                GL_ACCT_24,
                GL_ACCT_25,
                GL_ACCT_26,
                GL_ACCT_27,
                GL_ACCT_28,
                GL_ACCT_29,
                GL_ACCT_30,
                GL_ACCT_31,
                GL_ACCT_32,
                GL_ACCT_33,
                GL_ACCT_34,
                GL_ACCT_35,
                GL_ACCT_36,
                GL_ACCT_37,
                GL_ACCT_38,
                GL_ACCT_39,
                GL_ACCT_40,
                GL_ACCT_41,
                GL_ACCT_42,
                GL_ACCT_43,
                GL_ACCT_44,
                GL_ACCT_45,
                GL_ACCT_46,
                GL_ACCT_47,
                GL_ACCT_48,
                GL_ACCT_49,
                GL_ACCT_50,
                GL_ACCT_51,
                GL_ACCT_52,
                GL_ACCT_53,
                GL_ACCT_54,
                GL_ACCT_55,
                GL_ACCT_56,
                GL_ACCT_57,
                GL_ACCT_58,
                GL_ACCT_59,
                GL_ACCT_60,
                GL_ACCT_61,
                GL_ACCT_62,
                GL_ACCT_63,
                GL_ACCT_64,
                GL_ACCT_65,
                GL_ACCT_66,
                GL_ACCT_67,
                GL_ACCT_68,
                GL_ACCT_69,
                GL_ACCT_70
           from PEND_BASIS
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Delete from pend_basis');
      delete from PEND_BASIS
       where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_basis');
      insert into PEND_BASIS
         (PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, GL_ACCT_1, BASIS_2, GL_ACCT_2, BASIS_3,
          GL_ACCT_3, BASIS_4, GL_ACCT_4, BASIS_5, GL_ACCT_5, BASIS_6, GL_ACCT_6, BASIS_7, GL_ACCT_7,
          BASIS_8, GL_ACCT_8, BASIS_9, GL_ACCT_9, BASIS_10, GL_ACCT_10, BASIS_11, GL_ACCT_11,
          BASIS_12, GL_ACCT_12, BASIS_13, GL_ACCT_13, BASIS_14, GL_ACCT_14, BASIS_15, GL_ACCT_15,
          BASIS_16, GL_ACCT_16, BASIS_17, GL_ACCT_17, BASIS_18, GL_ACCT_18, BASIS_19, GL_ACCT_19,
          BASIS_20, GL_ACCT_20, BASIS_21, GL_ACCT_21, BASIS_22, GL_ACCT_22, BASIS_23, GL_ACCT_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70, GL_ACCT_24, GL_ACCT_25, GL_ACCT_26, GL_ACCT_27, GL_ACCT_28, GL_ACCT_29,
          GL_ACCT_30, GL_ACCT_31, GL_ACCT_32, GL_ACCT_33, GL_ACCT_34, GL_ACCT_35, GL_ACCT_36,
          GL_ACCT_37, GL_ACCT_38, GL_ACCT_39, GL_ACCT_40, GL_ACCT_41, GL_ACCT_42, GL_ACCT_43,
          GL_ACCT_44, GL_ACCT_45, GL_ACCT_46, GL_ACCT_47, GL_ACCT_48, GL_ACCT_49, GL_ACCT_50,
          GL_ACCT_51, GL_ACCT_52, GL_ACCT_53, GL_ACCT_54, GL_ACCT_55, GL_ACCT_56, GL_ACCT_57,
          GL_ACCT_58, GL_ACCT_59, GL_ACCT_60, GL_ACCT_61, GL_ACCT_62, GL_ACCT_63, GL_ACCT_64,
          GL_ACCT_65, GL_ACCT_66, GL_ACCT_67, GL_ACCT_68, GL_ACCT_69, GL_ACCT_70)
         select PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                BASIS_1,
                GL_ACCT_1,
                BASIS_2,
                GL_ACCT_2,
                BASIS_3,
                GL_ACCT_3,
                BASIS_4,
                GL_ACCT_4,
                BASIS_5,
                GL_ACCT_5,
                BASIS_6,
                GL_ACCT_6,
                BASIS_7,
                GL_ACCT_7,
                BASIS_8,
                GL_ACCT_8,
                BASIS_9,
                GL_ACCT_9,
                BASIS_10,
                GL_ACCT_10,
                BASIS_11,
                GL_ACCT_11,
                BASIS_12,
                GL_ACCT_12,
                BASIS_13,
                GL_ACCT_13,
                BASIS_14,
                GL_ACCT_14,
                BASIS_15,
                GL_ACCT_15,
                BASIS_16,
                GL_ACCT_16,
                BASIS_17,
                GL_ACCT_17,
                BASIS_18,
                GL_ACCT_18,
                BASIS_19,
                GL_ACCT_19,
                BASIS_20,
                GL_ACCT_20,
                BASIS_21,
                GL_ACCT_21,
                BASIS_22,
                GL_ACCT_22,
                BASIS_23,
                GL_ACCT_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70,
                GL_ACCT_24,
                GL_ACCT_25,
                GL_ACCT_26,
                GL_ACCT_27,
                GL_ACCT_28,
                GL_ACCT_29,
                GL_ACCT_30,
                GL_ACCT_31,
                GL_ACCT_32,
                GL_ACCT_33,
                GL_ACCT_34,
                GL_ACCT_35,
                GL_ACCT_36,
                GL_ACCT_37,
                GL_ACCT_38,
                GL_ACCT_39,
                GL_ACCT_40,
                GL_ACCT_41,
                GL_ACCT_42,
                GL_ACCT_43,
                GL_ACCT_44,
                GL_ACCT_45,
                GL_ACCT_46,
                GL_ACCT_47,
                GL_ACCT_48,
                GL_ACCT_49,
                GL_ACCT_50,
                GL_ACCT_51,
                GL_ACCT_52,
                GL_ACCT_53,
                GL_ACCT_54,
                GL_ACCT_55,
                GL_ACCT_56,
                GL_ACCT_57,
                GL_ACCT_58,
                GL_ACCT_59,
                GL_ACCT_60,
                GL_ACCT_61,
                GL_ACCT_62,
                GL_ACCT_63,
                GL_ACCT_64,
                GL_ACCT_65,
                GL_ACCT_66,
                GL_ACCT_67,
                GL_ACCT_68,
                GL_ACCT_69,
                GL_ACCT_70
           from PEND_BASIS_TEMP;

      DBMS_OUTPUT.PUT_LINE('      Update adjusted columns on pend_transaction');
      update PEND_TRANSACTION A
         set (ADJUSTED_COST_OF_REMOVAL,
               ADJUSTED_SALVAGE_CASH,
               ADJUSTED_SALVAGE_RETURNS,
               ADJUSTED_RESERVE_CREDITS) =
              (select ADJUSTED_COST_OF_REMOVAL,
                      ADJUSTED_SALVAGE_CASH,
                      ADJUSTED_SALVAGE_RETURNS,
                      ADJUSTED_RESERVE_CREDITS
                 from PEND_TRANSACTION_TEMP B
                where A.PEND_TRANS_ID = B.PEND_TRANS_ID)
       where exists (select 1 from PEND_TRANSACTION_TEMP B where A.PEND_TRANS_ID = B.PEND_TRANS_ID);

      update PEND_TRANSACTION
         set POSTING_ERROR = 'POST 1020: ERROR: Post created new set_of_books data for this transaction. Please review and re-approve to Post.'
       where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      if A_COMMIT_FLAG = 1 then
         commit;
      end if;
   end if;
end POST_CHECK_SET_OF_BOOKS;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (304, 0, 10, 4, 0, 0, 29286, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029286_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;