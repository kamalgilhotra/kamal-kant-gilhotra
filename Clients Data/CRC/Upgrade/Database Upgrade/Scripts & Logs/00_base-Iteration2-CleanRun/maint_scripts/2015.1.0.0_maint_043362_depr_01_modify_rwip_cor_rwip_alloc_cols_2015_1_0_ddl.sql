/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043362_depr_01_modify_rwip_cor_rwip_alloc_cols_2015_1_0_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/24/2015 Charlie Shilling columns have incorrect decimal size
||============================================================================
*/

--changing the precision of a column can throw an O R A - 1440,
--so we need to:
-- 1. save off the data in a temporary table,
-- 2. trucate the old table,
-- 3. modify the old table,
-- 4. reload the old table (in subsequent script), and
-- 5. delete the temp table (in subsequent script)

--NOTE only the arc table has permanent data. the stg table is global temp,
-- so we don't need to worry about saving its data.

--
-- #1
--
CREATE TABLE depr_calc_combined_temp AS
SELECT *
FROM depr_calc_combined_arc;

--
-- #2
--
TRUNCATE TABLE depr_calc_combined_arc;

--
-- #3
--
ALTER TABLE depr_calc_combined_stg
MODIFY rwip_cost_of_removal NUMBER(22,2) DEFAULT 0;

ALTER TABLE depr_calc_combined_stg
MODIFY rwip_allocation NUMBER(22,2) DEFAULT 0;

ALTER TABLE depr_calc_combined_arc
MODIFY rwip_cost_of_removal NUMBER(22,2) DEFAULT 0;

ALTER TABLE depr_calc_combined_arc
MODIFY rwip_allocation NUMBER(22,2) DEFAULT 0;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2442, 0, 2015, 1, 0, 0, 043362, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043362_depr_01_modify_rwip_cor_rwip_alloc_cols_2015_1_0_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;