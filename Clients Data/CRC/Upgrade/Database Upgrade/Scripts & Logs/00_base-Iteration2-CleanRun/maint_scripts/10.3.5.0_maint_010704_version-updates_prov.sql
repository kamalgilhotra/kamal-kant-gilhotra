/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010704_version-updates_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/21/2012 Blake Andrews  10.3.5.0 version
||============================================================================
*/

update TAX_ACCRUAL_SYSTEM_CONTROL
   set CONTROL_VALUE = '10.3.5'
 where CONTROL_NAME = 'Provision DB Version';

update PP_VERSION set PROVISION_VERSION = '10.3.5.0' where PP_VERSION_ID = 1;


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (202, 0, 10, 3, 5, 0, 10704, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010704_version-updates_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
