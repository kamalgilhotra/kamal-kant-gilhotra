/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051688_lessor_03_modify_receivable_remeasurement_not_null_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.1.0.0  09/13/2018 Jared Watkins    Make the receivable remeasurement non-nullable
||============================================================================
*/

ALTER TABLE lsr_ilr_schedule 
MODIFY (receivable_remeasurement NOT NULL);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9804, 0, 2018, 1, 0, 0, 51668, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051688_lessor_03_modify_receivable_remeasurement_not_null_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;