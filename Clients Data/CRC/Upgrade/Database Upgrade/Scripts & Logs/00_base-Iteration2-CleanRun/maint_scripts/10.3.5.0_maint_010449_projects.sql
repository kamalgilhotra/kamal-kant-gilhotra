/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010449_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   06/11/2012 Chris Mardis   Point Release
||============================================================================
*/

create table BV_PROJ_RANKING_FILTER_CONTROL
(
 FILTER           varchar2(35),
 ACT_WHERE_CLAUSE varchar2(4000),
 EST_WHERE_CLAUSE varchar2(4000),
 FUNDING_WO       number(22)
);

alter table BV_PROJ_RANKING_FILTER_CONTROL
   add constraint BV_PROJ_RANK_FILTER_CTRL_PK
       primary key (FILTER)
       using index tablespace PWRPLANT_IDX;

create table BV_PROJ_RANKING_FILTERS
(
 scenario_id number(22),
 filter varchar2(35),
 filter_list varchar2(4000)
);

alter table BV_PROJ_RANKING_FILTERS
   add constraint BV_PROJ_RANK_FILT_PK
       primary key (SCENARIO_ID, FILTER)
       using index tablespace PWRPLANT_IDX;

alter table BV_PROJ_RANKING_FILTERS
   add constraint BV_PROJ_RANK_FILT_FILTER_FK
       foreign key (FILTER)
       references BV_PROJ_RANKING_FILTER_CONTROL;

alter table BV_PROJ_RANKING_FILTER_CONTROL
   add (DW_NAME     varchar2(60),
        DATA_COLUMN varchar2(60),
        SQL         varchar2(4000));

insert into BV_PROJ_RANKING_FILTER_CONTROL
   (FILTER, ACT_WHERE_CLAUSE, EST_WHERE_CLAUSE, FUNDING_WO, DW_NAME, DATA_COLUMN, sql)
values
   ('Expenditure Type', 'and cc.expenditure_type_id in (<filter_list>)',
    'and wem.expenditure_type_id in (<filter_list>)', 1, 'dw_expenditure_type',
    'expenditure_type_id', null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (175, 0, 10, 3, 5, 0, 10449, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010449_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
