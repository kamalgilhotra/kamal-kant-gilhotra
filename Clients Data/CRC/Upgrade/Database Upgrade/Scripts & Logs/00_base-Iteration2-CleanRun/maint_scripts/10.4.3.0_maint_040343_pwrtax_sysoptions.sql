/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040343_pwrtax_sysoptions.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 10/23/2014 Andrew Scott        Remove the adds expense system controls, now should be
||                                         using values in tax_repairs_add_expense.
||                                         Remove the adds early in service months system controls, now should be
||                                         using values in pp base system options.
||========================================================================================
*/

delete from PP_SYSTEM_CONTROL_COMPANY
 where UPPER(trim(CONTROL_NAME)) in ('TAX ADD EXPENSE REVERSE MO',
                                     'TAX ADD EXPENSE INCLUDE MO',
                                     'TAX ADD EARLY IN SERVICE REVERSE MO',
                                     'TAX ADD EARLY IN SERVICE INCLUDE MO');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1563, 0, 10, 4, 3, 0, 40343, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040343_pwrtax_sysoptions.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;