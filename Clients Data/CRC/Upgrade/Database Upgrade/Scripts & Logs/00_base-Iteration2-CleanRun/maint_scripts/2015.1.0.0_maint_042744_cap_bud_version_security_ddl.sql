/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042744_cap_bud_version_security_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 3/14/2015  Chris Mardis     Capital Budget Version Security
||============================================================================
*/

--
-- Restructure tables and create view for Budget Version Security
--

-- Budget Version Control
alter table budget_version rename to budget_version_control;

create or replace public synonym budget_version_control for pwrplant.budget_version_control;
grant all on budget_version_control to pwrplant_role_dev;

-- Budget Version Security
create table pwrplant.budget_version_security (
   users varchar2(18),
   budget_version_id number(22),
   user_id varchar2(18),
   time_stamp date
   );

alter table pwrplant.budget_version_security add constraint bv_security_pk
   primary key (users, budget_version_id);
alter table pwrplant.budget_version_security add constraint bv_security_bvid_fk
   foreign key (budget_version_id) references budget_version_control;

create or replace public synonym budget_version_security for pwrplant.budget_version_security;
grant all on budget_version_security to pwrplant_role_dev;

-- Budget Version Security All
create table budget_version_security_all (
   budget_version_id number(22,0) not null,
   time_stamp date, 
   user_id varchar2(18)) ;

alter table budget_version_security_all add constraint pk_budget_version_security 
   primary key (budget_version_id);
alter table pwrplant.budget_version_security_all add constraint bv_sec_all_bvid_fk
   foreign key (budget_version_id) references budget_version_control;

create or replace public synonym budget_version_security_all for pwrplant.budget_version_security_all;
grant all on budget_version_security_all to pwrplant_role_dev;

-- Budget Version - no longer a table, but instead a view
create or replace view budget_version as
SELECT
   bvc.BUDGET_VERSION_ID,
   bvc.TIME_STAMP,
   bvc.USER_ID,
   bvc.DESCRIPTION,
   bvc.LONG_DESCRIPTION,
   bvc.LOCKED,
   bvc.START_YEAR,
   bvc.CURRENT_YEAR,
   bvc.END_YEAR,
   bvc.ACTUALS_MONTH,
   bvc.DATE_FINALIZED,
   bvc.CURRENT_VERSION,
   bvc.OPEN_FOR_ENTRY,
   bvc.BUDGET_ONLY,
   bvc.EXTERNAL_BUDGET_VERSION,
   bvc.BRING_IN_SUBS,
   bvc.OUTLOOK_NUMBER,
   bvc.LEVEL_ID,
   bvc.REPORTING_CURRENCY_ID,
   bvc.BUDGET_VERSION_TYPE_ID,
   bvc.COMPANY_ID,
   bvc.PROCESS_LEVEL
FROM BUDGET_VERSION_CONTROL bvc
where (
   bvc.budget_version_id in (
      select bvs.budget_version_id
      from budget_version_security bvs
      where lower(bvs.users) = lower(USER)
      )
   OR
   bvc.budget_version_id in (
      select bvsa.budget_version_id
      from budget_version_security_all bvsa
      )
   );

create or replace public synonym budget_version for pwrplant.budget_version;
grant all on budget_version to pwrplant_role_dev;

--
-- Tables for Budget Version Archiving
--

-- Budget Version Security Arch
create table pwrplant.budget_version_security_arch (
   users varchar2(18),
   budget_version_id number(22),
   user_id varchar2(18),
   time_stamp date
   );

alter table pwrplant.budget_version_security_arch add constraint bv_security_arch_pk
   primary key (users, budget_version_id);
alter table pwrplant.budget_version_security_arch add constraint bv_security_arch_bvid_fk
   foreign key (budget_version_id) references budget_version_arch;

create or replace public synonym budget_version_security_arch for pwrplant.budget_version_security_arch;
grant all on budget_version_security_arch to pwrplant_role_dev;

-- Budget Version Sec All Arch
create table budget_version_sec_all_arch (
   budget_version_id number(22,0) not null,
   time_stamp date, 
   user_id varchar2(18)) ;

alter table budget_version_sec_all_arch add constraint pk_budget_version_sec_all
   primary key (budget_version_id);
alter table pwrplant.budget_version_sec_all_arch add constraint bv_sec_all_arch_bvid_fk
   foreign key (budget_version_id) references budget_version_arch;

create or replace public synonym budget_version_sec_all_arch for pwrplant.budget_version_sec_all_arch;
grant all on budget_version_sec_all_arch to pwrplant_role_dev;

--
-- Create Sequence dedicated to numbering new budget versions
--
declare
   sqls varchar2(2000);
begin
   select
      'create sequence pwrplant.budget_version_seq '||
      'minvalue 0 '||
      'start with '||
      to_char(nvl(max(budget_version_id),0)+1)||' '||
   'increment by 1 '
   into sqls
   from (
      select nvl(max(budget_version_id),0) budget_version_id from budget_version_control
      union all
      select nvl(max(budget_version_id),0) budget_version_id from budget_version_arch
      );
   execute immediate sqls;
end;
/

create or replace public synonym budget_version_seq for pwrplant.budget_version_seq;
grant all on budget_version_seq to pwrplant_role_dev;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2377, 0, 2015, 1, 0, 0, 42744, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042744_cap_bud_version_security_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;