/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007510_cpi_retro2.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   02/12/2013 Sunjin Cone    Point Release
||============================================================================
*/

alter table CPI_RETRO_CLOSINGS rename column ADJUSTED_WO_CLOSE_AMOUNT to WO_CLOSE_AMOUNT;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (300, 0, 10, 4, 0, 0, 7510, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_007510_cpi_retro2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;