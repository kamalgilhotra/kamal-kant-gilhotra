/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029136_pp_integration.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.0.0 02/05/2013 Joseph King    Point Release
||============================================================================
*/

insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE)
values
   ('Create Funding Project', 'Company Id (-1 = all companies)', 'Batch / Instance');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (294, 0, 10, 4, 0, 0, 29136, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029136_pp_integration.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;