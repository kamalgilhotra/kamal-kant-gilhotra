/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044727_cr_modify_ppint_comp_names_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2     08/19/2015 Jared Watkins    The names originally given to the new 
||					  PP Integration components were not
||					  descriptive enough; same for argument1
||============================================================================
*/

UPDATE pp_integration_components 
	SET component = 'PP Text File Import' 
	WHERE component = 'PP File Import';

UPDATE pp_integration_components 
	SET component = 'PP Text File Export' 
	WHERE component = 'PP File Export';

UPDATE pp_integration_components 
	SET argument1_title = 'API Data Set (case sensitive)' 
	WHERE component LIKE '%Import' OR component LIKE '%Export';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2813, 0, 2015, 2, 0, 0, 044727, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044727_cr_modify_ppint_comp_names_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;