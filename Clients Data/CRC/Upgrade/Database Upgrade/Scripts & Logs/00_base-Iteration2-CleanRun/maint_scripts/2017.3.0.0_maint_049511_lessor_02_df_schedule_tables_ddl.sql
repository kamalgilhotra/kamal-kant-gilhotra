/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049511_lessor_02_df_schedule_tables_ddl.sql
|| Description:	Prepare tables for DF Schedule
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.2.0.0 01/30/2018 Andrew Hill    Prepare schedule tables for DF Schedule
||============================================================================
*/

ALTER TABLE lsr_ilr_schedule_sales_direct DROP COLUMN calculated_initial_rate;
ALTER TABLE lsr_ilr_schedule_sales_direct DROP COLUMN calculated_rate;

CREATE TABLE lsr_ilr_schedule_direct_fin (ilr_id number(22,0),
                                              revision NUMBER(22,0),
                                              set_of_books_id NUMBER(22,0),
                                              MONTH DATE,
                                              user_id VARCHAR2(18),
                                              time_stamp DATE,
                                              begin_deferred_profit NUMBER(22,2),
                                              recognized_profit NUMBER(22,2),
                                              end_deferred_profit NUMBER(22,2));

ALTER TABLE lsr_ilr_schedule_direct_fin ADD CONSTRAINT lsr_ilr_schedule_direct_fin_pk PRIMARY KEY (ilr_id, revision, set_of_books_id, MONTH);
ALTER TABLE lsr_ilr_schedule_direct_fin ADD CONSTRAINT lsr_ilr_sch_direct_fin_sch_fk 
  FOREIGN KEY (ilr_id, revision, set_of_books_id, MONTH) REFERENCES lsr_ilr_schedule_sales_direct(ilr_id, revision, set_of_books_id, MONTH);
  
COMMENT ON TABLE lsr_ilr_schedule_direct_fin IS 'Holds Direct-Finance-Type-Specifc ILR Schedule Fields';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.ilr_id IS 'ILR to which values apply';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.revision IS 'ILR revision to which values apply';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.set_of_books_id IS 'Set of Books to which values apply';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.MONTH IS 'Month of schedule to which values apply';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.user_id is'Standard system-assigned user id used for audit purposes';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.time_stamp IS 'Standard system-assigned timestamp used for audit purposes';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.begin_deferred_profit IS 'The deferred profit balance at the begnning of the period';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.recognized_profit IS 'The profit recognized in the period.';
COMMENT ON COLUMN lsr_ilr_schedule_direct_fin.end_deferred_profit is 'The deferred profit balance at the end of the period';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4095, 0, 2017, 3, 0, 0, 49511, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_049511_lessor_02_df_schedule_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 
