/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052109_lessee_01_rebuild_schedules_to_zero_om_liability_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.1  08/03/2018 Josh Sandler     Rebuild the schedules to zero liability on OM
||============================================================================
*/

-- Asset Schedule
create table rebuild_ls_asset_schedule as (
select LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
       BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
       INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
       EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
       EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4,
       EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
       CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5,
       CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
       CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
       CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM,
       CURRENT_LEASE_COST, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
       BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT,
       INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT,
       IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, ROU_ASSET_REMEASUREMENT,
       PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, BEG_ARREARS_ACCRUAL,
       ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, PARTIAL_MONTH_PERCENT, REMAINING_PRINCIPAL
  from (
      select LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
             BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
             INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
             EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
             EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4,
             EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
             CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5,
             CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
             CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
             CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM,
             CURRENT_LEASE_COST, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
             0 BEG_LIABILITY, 0 END_LIABILITY, 0 BEG_LT_LIABILITY, 0 END_LT_LIABILITY, PRINCIPAL_REMEASUREMENT, 0 LIABILITY_REMEASUREMENT,
             INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT,
             IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, ROU_ASSET_REMEASUREMENT,
             PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, BEG_ARREARS_ACCRUAL,
             ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, PARTIAL_MONTH_PERCENT, REMAINING_PRINCIPAL
        from ls_asset_schedule
       where is_om = 1
       union
      select LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
             BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
             INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
             EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
             EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4,
             EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
             CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5,
             CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
             CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
             CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM,
             CURRENT_LEASE_COST, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
             BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT,
             INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT,
             IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, ROU_ASSET_REMEASUREMENT,
             PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, BEG_ARREARS_ACCRUAL,
             ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, PARTIAL_MONTH_PERCENT, REMAINING_PRINCIPAL
        from ls_asset_schedule
       where is_om = 0
      )
);

-- Drop ls_asset_schedule
drop table ls_asset_schedule;

-- Rename to ls_asset_schedule
alter table rebuild_ls_asset_schedule rename to ls_asset_schedule;

-- Primary Key
alter table ls_asset_schedule add constraint
pk_ls_asset_schedule primary key (ls_asset_id, revision, set_of_books_id, month) using index tablespace pwrplant_idx;

-- Indexes
CREATE INDEX LS_ASSET_SCHEDULE_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (REVISION , LS_ASSET_ID , SET_OF_BOOKS_ID ) ;
CREATE INDEX LS_ASSET_SCHEDULE_TMTH_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (TRUNC(MONTH,'fmmonth')) ;
CREATE INDEX LS_ASSET_SCHEDULE_MANY_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (LS_ASSET_ID, REVISION, MONTH, SET_OF_BOOKS_ID, TRUNC(MONTH,'fmmonth'));

-- Comments
comment on table LS_ASSET_SCHEDULE is '(C)  [06]
The LS Asset Schedule table is holds the interest, principal, executory, and contingent amounts accrued and paid by period by set of books for a leased asset.';

comment on column LS_ASSET_SCHEDULE.ADDITIONAL_ROU_ASSET is 'Calculation of ROU Asset based on Quantity Retirement Method';
comment on column LS_ASSET_SCHEDULE.ARREARS_ACCRUAL is 'The arrears accrual amount for the period.';
comment on column LS_ASSET_SCHEDULE.BEG_ARREARS_ACCRUAL is 'The beginning arrears accrual balance for the period.';
comment on column LS_ASSET_SCHEDULE.BEG_CAPITAL_COST is 'The capital amount.';
comment on column LS_ASSET_SCHEDULE.BEG_DEFERRED_RENT is 'The beginning deferred rent amount for the period.';
comment on column LS_ASSET_SCHEDULE.BEG_LIABILITY is 'The beginning liability for the period.';
comment on column LS_ASSET_SCHEDULE.BEG_LT_LIABILITY is 'The beginning long term liability for the period.';
comment on column LS_ASSET_SCHEDULE.BEG_LT_OBLIGATION is 'The beginning long term obligation for the period.';
comment on column LS_ASSET_SCHEDULE.BEG_NET_ROU_ASSET is 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
comment on column LS_ASSET_SCHEDULE.BEG_OBLIGATION is 'The beginning obligation for the period.';
comment on column LS_ASSET_SCHEDULE.BEG_PREPAID_RENT is 'The beginning prepaid rent for the period.';
comment on column LS_ASSET_SCHEDULE.BEG_ST_DEFERRED_RENT is 'The beginning short term deferred rent amount for the period.';
comment on column LS_ASSET_SCHEDULE.BPO_PRICE is 'The bargain purchase amount.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL1 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL10 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL2 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL3 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL4 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL5 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL6 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL7 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL8 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ACCRUAL9 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_ADJUST is 'Summated value of all Contingent Adjusted values, by ILR';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID1 is 'The contingent paid amount associated with the contingent bucket number 1.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID10 is 'The contingent paid amount associated with the contingent bucket number 10.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID2 is 'The contingent paid amount associated with the contingent bucket number 2.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID3 is 'The contingent paid amount associated with the contingent bucket number 3.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID4 is 'The contingent paid amount associated with the contingent bucket number 4.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID5 is 'The contingent paid amount associated with the contingent bucket number 5.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID6 is 'The contingent paid amount associated with the contingent bucket number 6.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID7 is 'The contingent paid amount associated with the contingent bucket number 7.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID8 is 'The contingent paid amount associated with the contingent bucket number 8.';
comment on column LS_ASSET_SCHEDULE.CONTINGENT_PAID9 is 'The contingent paid amount associated with the contingent bucket number 9.';
comment on column LS_ASSET_SCHEDULE.CURRENT_LEASE_COST is 'The total amount funded on the leased asset in the given month';
comment on column LS_ASSET_SCHEDULE.DEFERRED_RENT is 'The deferred rent amount for the period.';
comment on column LS_ASSET_SCHEDULE.END_ARREARS_ACCRUAL is 'The ending arrears accrual balance for the period.';
comment on column LS_ASSET_SCHEDULE.END_CAPITAL_COST is 'The capital amount.';
comment on column LS_ASSET_SCHEDULE.END_DEFERRED_RENT is 'The ending deferred rent amount for the period.';
comment on column LS_ASSET_SCHEDULE.END_LIABILITY is 'The ending liability for the period.';
comment on column LS_ASSET_SCHEDULE.END_LT_LIABILITY is 'The ending long term liability for the period.';
comment on column LS_ASSET_SCHEDULE.END_LT_OBLIGATION is 'The ending long term obligation for the period.';
comment on column LS_ASSET_SCHEDULE.END_NET_ROU_ASSET is 'End Capital Cost less End Reserve for all Assets on ILR';
comment on column LS_ASSET_SCHEDULE.END_OBLIGATION is 'The ending obligation for the period.';
comment on column LS_ASSET_SCHEDULE.END_PREPAID_RENT is 'The ending prepaid rent for the period.';
comment on column LS_ASSET_SCHEDULE.END_ST_DEFERRED_RENT is 'The ending short term deferred rent amount for the period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL1 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL10 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL2 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL3 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL4 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL5 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL6 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL7 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL8 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ACCRUAL9 is 'The amount of executory costs accrued in this period.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_ADJUST is 'Summated value of all Executory Adjusted values, by ILR';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID1 is 'The executory paid amount associated with the executory bucket number 1.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID10 is 'The executory paid amount associated with the executory bucket number 10.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID2 is 'The executory paid amount associated with the executory bucket number 2.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID3 is 'The executory paid amount associated with the executory bucket number 3.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID4 is 'The executory paid amount associated with the executory bucket number 4.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID5 is 'The executory paid amount associated with the executory bucket number 5.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID6 is 'The executory paid amount associated with the executory bucket number 6.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID7 is 'The executory paid amount associated with the executory bucket number 7.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID8 is 'The executory paid amount associated with the executory bucket number 8.';
comment on column LS_ASSET_SCHEDULE.EXECUTORY_PAID9 is 'The executory paid amount associated with the executory bucket number 9.';
comment on column LS_ASSET_SCHEDULE.IDC_MATH_AMOUNT is 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
comment on column LS_ASSET_SCHEDULE.INCENTIVE_AMOUNT is 'Holds Incentive Amount for Asset Schedule.';
comment on column LS_ASSET_SCHEDULE.INCENTIVE_MATH_AMOUNT is 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
comment on column LS_ASSET_SCHEDULE.INITIAL_DIRECT_COST is 'Holds Initial Direct Cost Amount for Asset Schedule.';
comment on column LS_ASSET_SCHEDULE.INTEREST_ACCRUAL is 'The amount of interest accrued in this period .';
comment on column LS_ASSET_SCHEDULE.INTEREST_PAID is 'The amount of interest paid in this period .';
comment on column LS_ASSET_SCHEDULE.IS_OM is '1 means O and M.  0 means Capital. 1 means O and M.  0 means Capital.';
comment on column LS_ASSET_SCHEDULE.LIABILITY_REMEASUREMENT is 'The liability adjustment in the first period of a remeasurement.';
comment on column LS_ASSET_SCHEDULE.LS_ASSET_ID is 'System-assigned identifier of a particular leased asset. The internal leased asset id within PowerPlant.';
comment on column LS_ASSET_SCHEDULE.MONTH is 'The month being processed.';
comment on column LS_ASSET_SCHEDULE.PARTIAL_MONTH_PERCENT is 'The percentage of the first month in the payment term to pay when the payment type is Partial Month.';
comment on column LS_ASSET_SCHEDULE.PARTIAL_TERM_GAIN_LOSS is 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
comment on column LS_ASSET_SCHEDULE.PREPAID_RENT is 'The prepaid rent amount for the period.';
comment on column LS_ASSET_SCHEDULE.PREPAY_AMORTIZATION is 'The prepayment amortization amount for the period.';
comment on column LS_ASSET_SCHEDULE.PRINCIPAL_ACCRUAL is 'The amount of principal accrued in this period.';
comment on column LS_ASSET_SCHEDULE.PRINCIPAL_PAID is 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
comment on column LS_ASSET_SCHEDULE.PRINCIPAL_REMEASUREMENT is 'The principal/obligation adjustment in the first period of a remeasurement.';
comment on column LS_ASSET_SCHEDULE.REMAINING_PRINCIPAL is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc';
comment on column LS_ASSET_SCHEDULE.RESIDUAL_AMOUNT is 'The guaranteed residual amount.';
comment on column LS_ASSET_SCHEDULE.REVISION is 'The revision.';
comment on column LS_ASSET_SCHEDULE.ROU_ASSET_REMEASUREMENT is 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
comment on column LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID is 'The internal set of books id within PowerPlant.';
comment on column LS_ASSET_SCHEDULE.TERM_PENALTY is 'The termination penalty amount.';
comment on column LS_ASSET_SCHEDULE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LS_ASSET_SCHEDULE.USER_ID is 'Standard system-assigned user id used for audit purposes.';

-- ILR Schedule
create table rebuild_ls_ilr_schedule as (
select ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST,
       BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID,
       PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5,
       EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1,
       EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
       EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
       CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
       CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7,
       CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, CURRENT_LEASE_COST, BEG_DEFERRED_RENT,
       DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
       BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
       PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION,
       PREPAID_RENT, END_PREPAID_RENT, IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET,
       ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST,
       BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL
  from (
      select ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST,
             BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID,
             PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5,
             EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1,
             EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
             EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
             CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
             CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7,
             CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, CURRENT_LEASE_COST, BEG_DEFERRED_RENT,
             DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
             0 BEG_LIABILITY, 0 END_LIABILITY, 0 BEG_LT_LIABILITY, 0 END_LT_LIABILITY,
             PRINCIPAL_REMEASUREMENT, 0 LIABILITY_REMEASUREMENT, INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION,
             PREPAID_RENT, END_PREPAID_RENT, IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET,
             ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST,
             BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL
        from ls_ilr_schedule
       where is_om = 1
       union
      select ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST,
             BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID,
             PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5,
             EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1,
             EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
             EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
             CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
             CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7,
             CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, CURRENT_LEASE_COST, BEG_DEFERRED_RENT,
             DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
             BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
             PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION,
             PREPAID_RENT, END_PREPAID_RENT, IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET,
             ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST,
             BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL
        from ls_ilr_schedule
       where is_om = 0
      )
);


-- Drop ls_ilr_schedule
drop table ls_ilr_schedule;

-- Rename to ls_ilr_schedule
alter table rebuild_ls_ilr_schedule rename to ls_ilr_schedule;

-- Primary Key
alter table ls_ilr_schedule add constraint
pk_ls_ilr_schedule primary key (ilr_id, revision, set_of_books_id, month) using index tablespace pwrplant_idx;

-- Indexes
CREATE INDEX LS_ILR_SCHEDULE_IDX ON PWRPLANT.LS_ILR_SCHEDULE (REVISION , ILR_ID , SET_OF_BOOKS_ID ) ;
CREATE INDEX LS_ILR_SCHEDULE_MTH_IDX ON PWRPLANT.LS_ILR_SCHEDULE (MONTH) ;
CREATE INDEX LS_ILR_SCHEDULE_TMTH_IDX ON PWRPLANT.LS_ILR_SCHEDULE (TRUNC(MONTH,'fmmonth')) ;

-- Comments
comment on table LS_ILR_SCHEDULE is '(C)  [06]
The ILR Schedule table records the ILR''s payment schedule.  The ILR''s schedule is allocated to its leased assets as part of the monthly lease expense calc process.';

comment on column LS_ILR_SCHEDULE.ADDITIONAL_ROU_ASSET is 'Calculation of ROU Asset based on Quantity Retirement Method';
comment on column LS_ILR_SCHEDULE.ARREARS_ACCRUAL is 'The arrears accrual amount for the period.';
comment on column LS_ILR_SCHEDULE.BEG_ARREARS_ACCRUAL is 'The beginning arrears accrual balance for the period.';
comment on column LS_ILR_SCHEDULE.BEG_CAPITAL_COST is 'The capital amount.';
comment on column LS_ILR_SCHEDULE.BEG_DEFERRED_RENT is 'The beginning deferred rent amount for the period.';
comment on column LS_ILR_SCHEDULE.BEG_LIABILITY is 'The beginning liability for the period.';
comment on column LS_ILR_SCHEDULE.BEG_LT_LIABILITY is 'The beginning long term liability for the period.';
comment on column LS_ILR_SCHEDULE.BEG_LT_OBLIGATION is 'The beginning long term obligation for the period.';
comment on column LS_ILR_SCHEDULE.BEG_NET_ROU_ASSET is 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
comment on column LS_ILR_SCHEDULE.BEG_OBLIGATION is 'The beginning obligation for the period.';
comment on column LS_ILR_SCHEDULE.BEG_PREPAID_RENT is 'The beginning prepaid rent for the period.';
comment on column LS_ILR_SCHEDULE.BEG_ST_DEFERRED_RENT is 'The beginning short term deferred rent amount for the period.';
comment on column LS_ILR_SCHEDULE.BPO_PRICE is 'The bargain purchase amount.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL1 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL10 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL2 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL3 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL4 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL5 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL6 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL7 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL8 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ACCRUAL9 is 'The amount of contingent costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_ADJUST is 'Summated value of all Contingent Adjusted values, by ILR';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID1 is 'The contingent paid amount associated with the contingent bucket number 1.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID10 is 'The contingent paid amount associated with the contingent bucket number 10.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID2 is 'The contingent paid amount associated with the contingent bucket number 2.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID3 is 'The contingent paid amount associated with the contingent bucket number 3.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID4 is 'The contingent paid amount associated with the contingent bucket number 4.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID5 is 'The contingent paid amount associated with the contingent bucket number 5.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID6 is 'The contingent paid amount associated with the contingent bucket number 6.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID7 is 'The contingent paid amount associated with the contingent bucket number 7.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID8 is 'The contingent paid amount associated with the contingent bucket number 8.';
comment on column LS_ILR_SCHEDULE.CONTINGENT_PAID9 is 'The contingent paid amount associated with the contingent bucket number 9.';
comment on column LS_ILR_SCHEDULE.CURRENT_LEASE_COST is 'The total amount funded on the ILR in the given month';
comment on column LS_ILR_SCHEDULE.DEFERRED_RENT is 'The deferred rent amount for the period.';
comment on column LS_ILR_SCHEDULE.END_ARREARS_ACCRUAL is 'The ending arrears accrual balance for the period.';
comment on column LS_ILR_SCHEDULE.END_CAPITAL_COST is 'The capital amount.';
comment on column LS_ILR_SCHEDULE.END_DEFERRED_RENT is 'The ending deferred rent amount for the period.';
comment on column LS_ILR_SCHEDULE.END_LIABILITY is 'The ending liability for the period.';
comment on column LS_ILR_SCHEDULE.END_LT_LIABILITY is 'The ending long term liability for the period.';
comment on column LS_ILR_SCHEDULE.END_LT_OBLIGATION is 'The ending long term obligation for the period.';
comment on column LS_ILR_SCHEDULE.END_NET_ROU_ASSET is 'End Capital Cost less End Reserve for all Assets on ILR';
comment on column LS_ILR_SCHEDULE.END_OBLIGATION is 'The ending obligation for the period.';
comment on column LS_ILR_SCHEDULE.END_PREPAID_RENT is 'The ending prepaid rent for the period.';
comment on column LS_ILR_SCHEDULE.END_ST_DEFERRED_RENT is 'The ending short term deferred rent amount for the period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL1 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL10 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL2 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL3 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL4 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL5 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL6 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL7 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL8 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ACCRUAL9 is 'The amount of executory costs accrued in this period.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_ADJUST is 'Summated value of all Executory Adjusted values, by ILR';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID1 is 'The executory paid amount associated with the executory bucket number 1.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID10 is 'The executory paid amount associated with the executory bucket number 10.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID2 is 'The executory paid amount associated with the executory bucket number 2.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID3 is 'The executory paid amount associated with the executory bucket number 3.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID4 is 'The executory paid amount associated with the executory bucket number 4.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID5 is 'The executory paid amount associated with the executory bucket number 5.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID6 is 'The executory paid amount associated with the executory bucket number 6.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID7 is 'The executory paid amount associated with the executory bucket number 7.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID8 is 'The executory paid amount associated with the executory bucket number 8.';
comment on column LS_ILR_SCHEDULE.EXECUTORY_PAID9 is 'The executory paid amount associated with the executory bucket number 9.';
comment on column LS_ILR_SCHEDULE.IDC_MATH_AMOUNT is 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
comment on column LS_ILR_SCHEDULE.ILR_ID is 'System-assigned identifier of a particular ILR.';
comment on column LS_ILR_SCHEDULE.INCENTIVE_AMOUNT is 'Holds Incentive Amount for ILR Schedule.';
comment on column LS_ILR_SCHEDULE.INCENTIVE_MATH_AMOUNT is 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
comment on column LS_ILR_SCHEDULE.INITIAL_DIRECT_COST is 'Holds Initial Direct Cost Amount for ILR Schedule.';
comment on column LS_ILR_SCHEDULE.INTEREST_ACCRUAL is 'Records the amount of interest accrued.  Accruals occur every month regardless of payment schedule.';
comment on column LS_ILR_SCHEDULE.INTEREST_PAID is 'The amount of interest paid in this period .';
comment on column LS_ILR_SCHEDULE.IS_OM is '1 means O and M.  0 means Capital.';
comment on column LS_ILR_SCHEDULE.LIABILITY_REMEASUREMENT is 'The liability adjustment in the first period of a remeasurement.';
comment on column LS_ILR_SCHEDULE.MONTH is 'The month being processed.';
comment on column LS_ILR_SCHEDULE.PARTIAL_TERM_GAIN_LOSS is 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
comment on column LS_ILR_SCHEDULE.PREPAID_RENT is 'The prepaid rent amount for the period.';
comment on column LS_ILR_SCHEDULE.PREPAY_AMORTIZATION is 'The prepayment amortization amount for the period.';
comment on column LS_ILR_SCHEDULE.PRINCIPAL_ACCRUAL is 'Records the amount of principal accrued.  Accruals occur every month regardless of payment schedule.';
comment on column LS_ILR_SCHEDULE.PRINCIPAL_PAID is 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
comment on column LS_ILR_SCHEDULE.PRINCIPAL_REMEASUREMENT is 'The principal/obligation adjustment in the first period of a remeasurement.';
comment on column LS_ILR_SCHEDULE.REMAINING_PRINCIPAL is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc';
comment on column LS_ILR_SCHEDULE.RESIDUAL_AMOUNT is 'The guaranteed residual amount.';
comment on column LS_ILR_SCHEDULE.REVISION is 'The revision.';
comment on column LS_ILR_SCHEDULE.ROU_ASSET_REMEASUREMENT is 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
comment on column LS_ILR_SCHEDULE.SET_OF_BOOKS_ID is 'The internal set of books id within PowerPlant.';
comment on column LS_ILR_SCHEDULE.TERM_PENALTY is 'The termination penalty amount.';
comment on column LS_ILR_SCHEDULE.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column LS_ILR_SCHEDULE.USER_ID is 'Standard System-assigned user id used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8822, 0, 2017, 4, 0, 1, 52109, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052109_lessee_01_rebuild_schedules_to_zero_om_liability_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;