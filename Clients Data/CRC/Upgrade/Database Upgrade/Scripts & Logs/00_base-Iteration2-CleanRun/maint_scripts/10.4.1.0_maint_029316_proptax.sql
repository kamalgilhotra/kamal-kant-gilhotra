/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029316_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   02/25/2013 Julia Breuer   Point Release
||============================================================================
*/

--
-- Add an option for Spread by Rules to be spread by quantity or quantity factored.
--
alter table PWRPLANT.PT_STATISTICS_SPREAD_RULES add SPREAD_ON_QTY number(22,0) default 0;
alter table PWRPLANT.PT_STATISTICS_SPREAD_RULES add SPREAD_ON_QTY_FACTORED number(22,0) default 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (308, 0, 10, 4, 1, 0, 29316, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029316_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
