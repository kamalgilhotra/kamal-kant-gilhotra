/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037706_reg_REG_HISTORY_LEDGER_SV.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.5 04/09/2014 Shane "C" Ward Update reg_history_ledger_sv to include new columns
||========================================================================================
*/

create or replace view REG_HISTORY_LEDGER_SV
(REG_COMPANY, REG_ACCOUNT, REG_ACCOUNT_TYPE, SUB_ACCOUNT_TYPE, REG_SOURCE, HISTORIC_LEDGER,
 GL_MONTH, ACT_AMOUNT, ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH, RECON_ADJ_AMOUNT, RECON_ADJ_COMMENT)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       HISTORIC_LEDGER,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       RECON_ADJ_AMOUNT,
       RECON_ADJ_COMMENT
  from (select C.DESCRIPTION       REG_COMPANY,
               M.DESCRIPTION       REG_ACCOUNT,
               T.DESCRIPTION       REG_ACCOUNT_TYPE,
               S.DESCRIPTION       SUB_ACCOUNT_TYPE,
               R.DESCRIPTION       REG_SOURCE,
               V.LONG_DESCRIPTION  HISTORIC_LEDGER,
               L.GL_MONTH          GL_MONTH,
               L.ACT_AMOUNT        ACT_AMOUNT,
               L.ANNUALIZED_AMT    ANNUALIZED_AMT,
               L.ADJ_AMOUNT        ADJ_AMOUNT,
               L.ADJ_MONTH         ADJ_MONTH,
               L.RECON_ADJ_AMOUNT  RECON_ADJ_AMOUNT,
               L.RECON_ADJ_COMMENT RECON_ADJ_COMMENT
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER L,
               REG_ACCT_TYPE T,
               REG_SUB_ACCT_TYPE S,
               REG_SOURCE R
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER, COLUMN_WIDTH,
    QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select ID, 'RECON_ADJ_AMOUNT', 12, 1, 1, 'Recon Adj Amount', 313, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA
    where SOURCE_ID = 1002
      and TABLE_NAME = 'reg_history_ledger_sv';

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER, COLUMN_WIDTH,
    QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select ID, 'RECON_ADJ_COMMENT', 13, 0, 1, 'Recon Adj Comment', 1501, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA
    where SOURCE_ID = 1002
      and TABLE_NAME = 'reg_history_ledger_sv';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1132, 0, 10, 4, 2, 5, 37706, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.5_maint_037706_reg_REG_HISTORY_LEDGER_SV.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
