/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009367_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.2   02/06/2012 Sunjin Cone    Point Release
||============================================================================
*/

alter table REPAIR_BLANKET_PROCESSING
   modify (PROP_TOTAL_CIRCUIT_PERCENT number (22,8),
           EXPENSE_PERCENTAGE         number (22,8));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (86, 0, 10, 3, 3, 2, 9367, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.2_maint_009367_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
