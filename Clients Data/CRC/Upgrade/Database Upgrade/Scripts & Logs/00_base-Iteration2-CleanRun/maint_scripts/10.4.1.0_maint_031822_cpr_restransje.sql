SET SERVEROUTPUT ON
SET LINESIZE 200

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031822_cpr_restransje.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   08/28/2013 Sunjin Cone   Point Release
||============================================================================
*/

declare
   PPCMSG  varchar2(10) := 'PPC' || '-MSG> ';
   PPCERR  varchar2(10) := 'PPC' || '-ERR> ';
   NEWLINE varchar2(10) := CHR(10);

   L_IGNORE boolean := false;
   L_COUNT  number;

begin
   select count(*)
     into L_COUNT
     from PP_SYSTEM_CONTROL_COMPANY
    where trim(UPPER(CONTROL_NAME)) = 'POST RESERVE TRANSFER'
      and trim(UPPER(CONTROL_VALUE)) = 'NO';

   delete from PP_SYSTEM_CONTROL_COMPANY where trim(UPPER(CONTROL_NAME)) = 'POST RESERVE TRANSFER';
   if sql%rowcount > 0 then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'System control deleted.');
      DBMS_OUTPUT.PUT_LINE(NEWLINE);
   else
      L_COUNT := 1;
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'System control did not exist.');
      DBMS_OUTPUT.PUT_LINE(NEWLINE);
   end if;
   commit;

   if L_COUNT > 0 then
      DBMS_OUTPUT.PUT_LINE(PPCMSG ||
                           'Maint 31822:  The system control called, POST RESERVE TRANSFER, is now obsolete and has been removed.');
      DBMS_OUTPUT.PUT_LINE(CHR(10));
      DBMS_OUTPUT.PUT_LINE(PPCMSG ||
                           'This database is showing that the control, POST RESERVE TRANSFER, was setup for the');
      DBMS_OUTPUT.PUT_LINE(PPCMSG ||
                           'Release Journals button on CPR control to generate the reserve transfer journal entries.');
      DBMS_OUTPUT.PUT_LINE(PPCMSG ||
                           'The Release Journals button will no longer have code in it to create reserve transfer journals.');
      DBMS_OUTPUT.PUT_LINE(PPCMSG ||
                           'The client journal entries configuration in PP_GL_TRANSACTION and F_AUTOGEN_JE_ACCOUNT needs to be modified');
      DBMS_OUTPUT.PUT_LINE(PPCMSG ||
                           'to create the reserve transfer journal entries for both CPR Asset Transfer and Depr Input Transfer Postings.');
      DBMS_OUTPUT.PUT_LINE(NEWLINE);
      DBMS_OUTPUT.PUT_LINE('WARNING: Failure to obey this instruction will result in missing journal entries for reserve transfers. ');
      DBMS_OUTPUT.PUT_LINE(NEWLINE);

      if L_IGNORE then
         --Just print Warning
         DBMS_OUTPUT.PUT_LINE(PPCERR ||
                              'Please read the above comments and verify the system is setup correctly.');
      else
         -- Raise application error
         DBMS_OUTPUT.PUT_LINE(NEWLINE);
         DBMS_OUTPUT.PUT_LINE('Once the system has been configured, edit this script and set the L_IGNORE variable to "true"');
         DBMS_OUTPUT.PUT_LINE('to turn off the raise application error.');
         DBMS_OUTPUT.PUT_LINE(NEWLINE);
         RAISE_APPLICATION_ERROR(-20000,
                                 'Read the above comments and make the necessary changes to the system.');
      end if;
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (553, 0, 10, 4, 1, 0, 31822, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031822_cpr_restransje.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;