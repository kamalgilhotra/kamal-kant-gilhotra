/*
||============================================================================
|| Application: PowerPlan
|| File Name:   2018.2.1.1_maint_053739_cr_hotfix_version_updates.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.1.1 6/21/2019    C Yura          CR Derivation PP-53738 hot fix 
||============================================================================
*/

update pp_version_exes set executable_version = '2018.2.1.1'
where lower(trim(executable_file)) = 'cr_batch_derivation.exe'
and pp_version like '2018.2.1.1%';


update pp_processes 
set version = '2018.2.1.1'
where lower(trim(executable_file)) = 'cr_batch_derivation.exe';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(18742, 0, 2018, 2, 1, 1, 53739, 'C:\BitBucketRepos\classic_pb\scripts\00_base\maint_scripts', '2018.2.1.1_maint_053739_cr_hotfix_version_updates.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;