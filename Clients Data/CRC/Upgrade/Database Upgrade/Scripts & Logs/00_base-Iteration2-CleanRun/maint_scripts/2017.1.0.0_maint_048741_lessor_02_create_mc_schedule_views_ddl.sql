/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048741_lessor_02_create_mc_schedule_views_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ------------------------------------
|| 2017.1.0.0 09/19/2017 Jared Watkins  Create the multicurrency views needed for the ILR schedule 
||============================================================================
*/
--these need to exist for some reason - replicated from Lessee
create materialized view log on lsr_lease with rowid including new values;
create materialized view log on lsr_ilr_schedule with rowid including new values;
create materialized view log on lsr_ilr with rowid including new values;
create materialized view log on lsr_ilr_options with rowid including new values;

--create the materialized view to grab the fields we need from the view
CREATE MATERIALIZED VIEW MV_LSR_ILR_MC_SCHEDULE_AMOUNTS
REFRESH FAST ON COMMIT
AS
SELECT  
  schedule.ilr_id,
  ilr.ilr_number,
  ilr.current_revision,
  schedule.revision,
  schedule.set_of_books_id,
  schedule.month,
  schedule.interest_income_received,
  schedule.interest_income_accrued,
  schedule.beg_deferred_rev,
  schedule.deferred_rev_activity,
  schedule.end_deferred_rev,
  schedule.beg_receivable,
  schedule.end_receivable,
  schedule.executory_accrual1,
  schedule.executory_accrual2,
  schedule.executory_accrual3,
  schedule.executory_accrual4,
  schedule.executory_accrual5,
  schedule.executory_accrual6,
  schedule.executory_accrual7,
  schedule.executory_accrual8,
  schedule.executory_accrual9,
  schedule.executory_accrual10,
  schedule.executory_paid1,
  schedule.executory_paid2,
  schedule.executory_paid3,
  schedule.executory_paid4,
  schedule.executory_paid5,
  schedule.executory_paid6,
  schedule.executory_paid7,
  schedule.executory_paid8,
  schedule.executory_paid9,
  schedule.executory_paid10,
  schedule.contingent_accrual1,
  schedule.contingent_accrual2,
  schedule.contingent_accrual3,
  schedule.contingent_accrual4,
  schedule.contingent_accrual5,
  schedule.contingent_accrual6,
  schedule.contingent_accrual7,
  schedule.contingent_accrual8,
  schedule.contingent_accrual9,
  schedule.contingent_accrual10,
  schedule.contingent_paid1,
  schedule.contingent_paid2,
  schedule.contingent_paid3,
  schedule.contingent_paid4,
  schedule.contingent_paid5,
  schedule.contingent_paid6,
  schedule.contingent_paid7,
  schedule.contingent_paid8,
  schedule.contingent_paid9,
  schedule.contingent_paid10,
  schedule.current_lease_cost,
  ilr.lease_id,
  ilr.company_id,
  options.in_service_exchange_rate,
  options.purchase_option_amt,
  options.termination_amt,
  schedule.ROWID AS lisrowid,
  options.ROWID AS optrowid,
  ilr.ROWID AS ilrrowid,
  lease.rowid as leaserowid
FROM lsr_ilr_schedule schedule,
  lsr_ilr_options options,
  lsr_ilr ilr,
  lsr_lease lease
WHERE schedule.ilr_id = options.ilr_id
AND schedule.revision = options.revision
AND schedule.ilr_id = ilr.ilr_id
AND ilr.lease_id = lease.lease_id
;

--replicate the indices added to the Lessee materialized view
CREATE INDEX mv_mc_lsr_ilr_amounts_idx ON mv_lsr_ilr_mc_schedule_amounts(ilr_id, revision, MONTH, set_of_books_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lsr_ilr_amounts_rev_id ON mv_lsr_ilr_mc_schedule_amounts(revision) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lsr_ilr_amts_month_idx ON mv_lsr_ilr_mc_schedule_amounts(MONTH) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lsr_ilr_amts_idx2 ON mv_lsr_ilr_mc_schedule_amounts(company_id, lease_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lsr_ilr_amts_lease_idx ON mv_lsr_ilr_mc_schedule_amounts(lease_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;

CREATE INDEX mv_mc_lsr_ilr_in_tmth_idx on mv_lsr_ilr_mc_schedule_amounts (trunc("MONTH", 'fmmonth')) tablespace pwrplant_idx COMPUTE STATISTICS;

CREATE INDEX mv_mc_lsr_ilr_amt_lisrowid_idx on mv_lsr_ilr_mc_schedule_amounts(lisrowid) tablespace pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lsr_ilr_amt_optrowid_idx on mv_lsr_ilr_mc_schedule_amounts(optrowid) tablespace pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lsr_ilr_amt_ilrrowid_idx on mv_lsr_ilr_mc_schedule_amounts(ilrrowid) tablespace pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lsr_ilr_amt_lsrowid_idx on mv_lsr_ilr_mc_schedule_amounts(leaserowid) tablespace pwrplant_idx COMPUTE STATISTICS;
                  
CREATE INDEX lsr_ilr_schedule_tmth_idx ON lsr_ilr_schedule(TRUNC(MONTH, 'MONTH')) TABLESPACE pwrplant_idx COMPUTE STATISTICS;

--create the actual multicurrency view to display company/contract currency values
--any 'LS' tables in this view are master data tables shared by Lessee and Lessor
CREATE OR REPLACE VIEW V_LSR_ILR_MC_SCHEDULE AS
WITH cur AS (
  SELECT ls_currency_type_id AS ls_cur_type,
    currency_id,
    currency_display_symbol,
    iso_code,
    CASE ls_currency_type_id 
      WHEN 1 THEN 1 
      ELSE NULL 
    END AS contract_approval_rate
  FROM currency
  CROSS JOIN ls_lease_currency_type
),
open_month AS (
  SELECT company_id,
    MIN(gl_posting_mo_yr) open_month
  FROM lsr_process_control 
  WHERE open_next IS NULL 
  GROUP BY company_id
),
calc_rate AS (
  SELECT a.company_id,
    a.contract_currency_id,
    a.company_currency_id,
    a.accounting_month,
    a.exchange_date,
    a.rate,
    b.rate prev_rate
  FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b 
    ON a.company_id = b.company_id
    AND a.contract_currency_id = b.contract_currency_id
    AND a.accounting_month = add_months(b.accounting_month,1)
),
rate_now AS (
  SELECT currency_from,
    currency_to,
    rate
  FROM (
    SELECT currency_from,
      currency_to,
      rate,
      ROW_NUMBER() OVER (PARTITION BY currency_from, currency_to ORDER BY exchange_date DESC) AS rn
    FROM currency_rate_default_dense 
    WHERE trunc(exchange_date,'MONTH') <= trunc(SYSDATE,'MONTH')
    AND exchange_rate_type_id = 1
  )
  WHERE rn = 1
) 
SELECT schedule.ilr_id ilr_id,
  schedule.ilr_number,
  lease.lease_id,
  lease.lease_number,
  schedule.current_revision,
  schedule.revision revision,
  schedule.set_of_books_id set_of_books_id,
  schedule.month month,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  rates.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  lease.contract_currency_id,
  cur.currency_id display_currency_id,
  rates.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  schedule.interest_income_received * nvl(calc_rate.rate, rates.rate) interest_income_received,
  schedule.interest_income_accrued * nvl(calc_rate.rate, rates.rate) interest_income_accrued,
  schedule.beg_deferred_rev * nvl(calc_rate.rate, rates.rate) beg_deferred_rev,
  schedule.deferred_rev_activity * nvl(calc_rate.rate, rates.rate) deferred_rev_activity,
  schedule.end_deferred_rev * nvl(calc_rate.rate, rates.rate) end_deferred_rev,
  schedule.beg_receivable * nvl(calc_rate.rate, rates.rate) beg_receivable,
  schedule.end_receivable * nvl(calc_rate.rate, rates.rate) end_receivable,
  schedule.executory_accrual1 * nvl(calc_rate.rate, rates.rate) executory_accrual1,
  schedule.executory_accrual2 * nvl(calc_rate.rate, rates.rate) executory_accrual2,
  schedule.executory_accrual3 * nvl(calc_rate.rate, rates.rate) executory_accrual3,
  schedule.executory_accrual4 * nvl(calc_rate.rate, rates.rate) executory_accrual4,
  schedule.executory_accrual5 * nvl(calc_rate.rate, rates.rate) executory_accrual5,
  schedule.executory_accrual6 * nvl(calc_rate.rate, rates.rate) executory_accrual6,
  schedule.executory_accrual7 * nvl(calc_rate.rate, rates.rate) executory_accrual7,
  schedule.executory_accrual8 * nvl(calc_rate.rate, rates.rate) executory_accrual8,
  schedule.executory_accrual9 * nvl(calc_rate.rate, rates.rate) executory_accrual9,
  schedule.executory_accrual10 * nvl(calc_rate.rate, rates.rate) executory_accrual10,
  schedule.executory_paid1 * nvl(calc_rate.rate, rates.rate) executory_paid1,
  schedule.executory_paid2 * nvl(calc_rate.rate, rates.rate) executory_paid2,
  schedule.executory_paid3 * nvl(calc_rate.rate, rates.rate) executory_paid3,
  schedule.executory_paid4 * nvl(calc_rate.rate, rates.rate) executory_paid4,
  schedule.executory_paid5 * nvl(calc_rate.rate, rates.rate) executory_paid5,
  schedule.executory_paid6 * nvl(calc_rate.rate, rates.rate) executory_paid6,
  schedule.executory_paid7 * nvl(calc_rate.rate, rates.rate) executory_paid7,
  schedule.executory_paid8 * nvl(calc_rate.rate, rates.rate) executory_paid8,
  schedule.executory_paid9 * nvl(calc_rate.rate, rates.rate) executory_paid9,
  schedule.executory_paid10 * nvl(calc_rate.rate, rates.rate) executory_paid10,
  schedule.contingent_accrual1 * nvl(calc_rate.rate, rates.rate) contingent_accrual1,
  schedule.contingent_accrual2 * nvl(calc_rate.rate, rates.rate) contingent_accrual2,
  schedule.contingent_accrual3 * nvl(calc_rate.rate, rates.rate) contingent_accrual3,
  schedule.contingent_accrual4 * nvl(calc_rate.rate, rates.rate) contingent_accrual4,
  schedule.contingent_accrual5 * nvl(calc_rate.rate, rates.rate) contingent_accrual5,
  schedule.contingent_accrual6 * nvl(calc_rate.rate, rates.rate) contingent_accrual6,
  schedule.contingent_accrual7 * nvl(calc_rate.rate, rates.rate) contingent_accrual7,
  schedule.contingent_accrual8 * nvl(calc_rate.rate, rates.rate) contingent_accrual8,
  schedule.contingent_accrual9 * nvl(calc_rate.rate, rates.rate) contingent_accrual9,
  schedule.contingent_accrual10 * nvl(calc_rate.rate, rates.rate) contingent_accrual10,
  schedule.contingent_paid1 * nvl(calc_rate.rate, rates.rate) contingent_paid1,
  schedule.contingent_paid2 * nvl(calc_rate.rate, rates.rate) contingent_paid2,
  schedule.contingent_paid3 * nvl(calc_rate.rate, rates.rate) contingent_paid3,
  schedule.contingent_paid4 * nvl(calc_rate.rate, rates.rate) contingent_paid4,
  schedule.contingent_paid5 * nvl(calc_rate.rate, rates.rate) contingent_paid5,
  schedule.contingent_paid6 * nvl(calc_rate.rate, rates.rate) contingent_paid6,
  schedule.contingent_paid7 * nvl(calc_rate.rate, rates.rate) contingent_paid7,
  schedule.contingent_paid8 * nvl(calc_rate.rate, rates.rate) contingent_paid8,
  schedule.contingent_paid9 * nvl(calc_rate.rate, rates.rate) contingent_paid9,
  schedule.contingent_paid10 * nvl(calc_rate.rate, rates.rate) contingent_paid10,
  schedule.current_lease_cost * nvl( nvl(cur.contract_approval_rate, schedule.in_service_exchange_rate), rate_now.rate ) current_lease_cost,
  schedule.beg_receivable * ( nvl(calc_rate.rate,0) - nvl(calc_rate.prev_rate,0) ) gain_loss_fx
FROM mv_lsr_ilr_mc_schedule_amounts schedule
  INNER JOIN lsr_lease lease 
    ON schedule.lease_id = lease.lease_id
  INNER JOIN currency_schema cs 
    ON schedule.company_id = cs.company_id
  INNER JOIN cur 
    ON cur.currency_id =
      CASE cur.ls_cur_type
        WHEN 1 THEN lease.contract_currency_id
        WHEN 2 THEN cs.currency_id
        ELSE NULL
      END
  INNER JOIN open_month 
    ON schedule.company_id = open_month.company_id
  INNER JOIN currency_rate_default_dense rates 
    ON cur.currency_id = rates.currency_to
    AND lease.contract_currency_id = rates.currency_from
    AND trunc(rates.exchange_date,'MONTH') = trunc(schedule.month,'MONTH')
  INNER JOIN rate_now 
    ON cur.currency_id = rate_now.currency_to
    AND lease.contract_currency_id = rate_now.currency_from
  LEFT OUTER JOIN calc_rate 
    ON lease.contract_currency_id = calc_rate.contract_currency_id
    AND cur.currency_id = calc_rate.company_currency_id
    AND schedule.company_id = calc_rate.company_id
    AND schedule.month = calc_rate.accounting_month
WHERE cs.currency_type_id = 1
AND rates.exchange_rate_type_id = 1
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3727, 0, 2017, 1, 0, 0, 48741, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048741_lessor_02_create_mc_schedule_views_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;