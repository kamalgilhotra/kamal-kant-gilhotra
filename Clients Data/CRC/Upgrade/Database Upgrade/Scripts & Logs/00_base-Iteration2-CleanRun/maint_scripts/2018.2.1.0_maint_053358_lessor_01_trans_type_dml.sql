/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_053358_lessor_01_trans_type_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 04/17/2019 B. Beck    	Add two new trans types for Lessor Reclassification JEs
||============================================================================
*/

insert into je_trans_type ( trans_type, description )
select 4084, '4084 - 	Lessor LT Receivable Termination Debit'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4084);

insert into je_trans_type ( trans_type, description )
select 4085, '4085 - 	Lessor LT Receivable Termination Credit'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4085);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type 
from je_trans_type
where trans_type IN (4084, 4085)
and not exists
(
	select 1 
	from je_method_trans_type 
	where trans_type IN (4084, 4085) 
	and je_method_id = 1
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (17122, 0, 2018, 2, 1, 0, 53358, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.1.0_maint_053358_lessor_01_trans_type_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;