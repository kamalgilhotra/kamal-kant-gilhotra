/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033393_sys_budget_uwa_exe.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   11/05/2013 Stephen Motter
||============================================================================
*/

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE)
   select max(PROCESS_ID) + 1,
          'Budget Update with Actuals',
          'Budget Update with Actuals',
          'budget_uwa.exe'
     from PP_PROCESSES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (715, 0, 10, 4, 2, 0, 33393, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033393_sys_budget_uwa_exe.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
