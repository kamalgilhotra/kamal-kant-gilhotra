/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048897_lessor_04_workflow_amt_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 10/04/2017 Shane "C" Ward		Set up Lessor Lease Import Tables
||============================================================================
*/

INSERT INTO workflow_amount_sql (subsystem, base_sql, SQL)
VALUES ('lessor_mla_approval', 1, 'select nvl(sum(max_lease_line),0) from LS_LEASE_COMPANY where lease_id = <<id_field1>>');

INSERT INTO workflow_amount_sql (subsystem, base_sql, SQL)
VALUES ('lessor_ilr_approval', 1, 'select nvl(max(beg_obligation),0) from LS_ILR_SCHEDULE
where ilr_id = <<id_field1>> and revision = <<id_field2>>
and month = (select min(month) from ls_ilr_schedule
where ilr_id = <<id_field1>> and revision = <<id_field2>>)');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3770, 0, 2017, 1, 0, 0, 48897, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048897_lessor_04_workflow_amt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
