/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042988_reg_hist_ext_src_recon_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     03/27/2015 Shane Ward   	Historic External Source Recon
||============================================================================
*/

ALTER TABLE reg_ext_source
ADD hist_recon_use NUMBER(22,0);

comment on column reg_ext_source.hist_recon_use is 'Identifier of whether the External Source is used for Historic Ledger Reconciliation. 1 = Yes 0 = No';

CREATE TABLE reg_hist_recon_setup (
  historic_version_id  NUMBER(22,0) NOT NULL,
  recon_source         NUMBER(22,0) NOT NULL,
  ext_source_id        NUMBER(22,0) NULL,
  ext_source_version   VARCHAR2(35) NULL,
  time_stamp           DATE         NULL,
  user_id              VARCHAR2(18) NULL
)
;

ALTER TABLE reg_hist_recon_setup
  ADD CONSTRAINT pk_reg_hist_recon_setup PRIMARY KEY (
    historic_version_id,
    recon_source
  )
  USING INDEX
    TABLESPACE pwrplant_idx
;

ALTER TABLE reg_hist_recon_setup
  ADD CONSTRAINT fk_reg_hist_recon_hist_vers FOREIGN KEY (
    historic_version_id
  ) REFERENCES reg_historic_version (
    historic_version_id
  )
;

ALTER TABLE reg_hist_recon_setup
  ADD CONSTRAINT fk_reg_hist_recon_source FOREIGN KEY (
    ext_source_id
  ) REFERENCES reg_ext_source (
    ext_source_id
  )
;

COMMENT ON TABLE reg_hist_recon_setup IS 'The Reg Hist Recon Setup table identifies the source of data that is used as the Historic Recon Balance; the options are the CR Module or a Regulatory Historic External Source.';

COMMENT ON COLUMN reg_hist_recon_setup.historic_version_id IS 'System assigned identifier for the historic ledger (version)';
COMMENT ON COLUMN reg_hist_recon_setup.recon_source IS 'Indicates which source of data should be used for the historic Reconciliation; 0 = CR; 1 = External Source';
COMMENT ON COLUMN reg_hist_recon_setup.time_stamp IS 'Standard system-assigned timestamp used for audit purposes';
COMMENT ON COLUMN reg_hist_recon_setup.user_id IS 'Standard system-assigned user id used for audit purposes';
COMMENT ON COLUMN reg_hist_recon_setup.ext_source_id IS 'System assigned identifier for the external source';
COMMENT ON COLUMN reg_hist_recon_setup.ext_source_version IS 'Specifies the version value to use as the Historic Reconciliation Balance source';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2461, 0, 2015, 1, 0, 0, 42988, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042988_reg_hist_ext_src_recon_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;