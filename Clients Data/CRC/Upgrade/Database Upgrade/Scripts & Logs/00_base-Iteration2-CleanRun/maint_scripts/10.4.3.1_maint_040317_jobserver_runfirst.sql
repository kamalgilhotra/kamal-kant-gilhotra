/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_040317_jobserver_runfirst.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.1   11/19/2014 Paul Cordero    	 Creation
||==========================================================================================
*/

set serveroutput on;

declare 
  doesTableColumnExist number := 0;
  begin
       begin
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('IS_INACTIVE', 'PP_JOB_WORKFLOW','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column IS_INACTIVE for PP_JOB_WORKFLOW table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW" 
              ADD 
              (
                "IS_INACTIVE" CHAR(1 BYTE) DEFAULT ''0'' CONSTRAINT PP_JOB_WORKFLOW03 NOT NULL ENABLE
              )';        
            end;
          else
            begin
              dbms_output.put_line('Column IS_INACTIVE for PP_JOB_WORKFLOW table exists');
            end;
          end if;     
          
          
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('ARGUMENTS', 'PP_JOB_EXECUTABLE','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column ARGUMENTS for PP_JOB_EXECUTABLE table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTABLE" 
              ADD 
              (
                "ARGUMENTS" VARCHAR2(4000 BYTE)
              )';        
            end;
          else
            begin
              dbms_output.put_line('Column ARGUMENTS for PP_JOB_EXECUTABLE table exists');
            end;
          end if;     
          
          
          
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('JOB_WORKFLOW_ID', 'PP_JOB_EXECUTION','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column JOB_WORKFLOW_ID for PP_JOB_EXECUTION table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION" 
              ADD 
              (
                "JOB_WORKFLOW_ID" NUMBER(16,0) CONSTRAINT PP_JOB_EXECUTION03 NOT NULL
              )';        
            end;
          else
            begin
              dbms_output.put_line('Column JOB_WORKFLOW_ID for PP_JOB_EXECUTION table exists');
            end;
          end if;     
          
          
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('JOB_WORKFLOW_JOB_ID', 'PP_JOB_EXECUTION_JOB','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column JOB_WORKFLOW_JOB_ID for PP_JOB_EXECUTION_JOB table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
              ADD 
              (
                "JOB_WORKFLOW_JOB_ID" NUMBER(16,0) CONSTRAINT PP_JOB_EXECUTION_JOB07 NOT NULL
              )';        
            end;
          else
            begin
              dbms_output.put_line('Column JOB_WORKFLOW_JOB_ID for PP_JOB_EXECUTION_JOB table exists');
            end;
          end if;     
          
          
          
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('JOB_EXECUTABLE_ID', 'PP_JOB_EXECUTABLE','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column JOB_EXECUTABLE_ID for PP_JOB_EXECUTABLE table');
              
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTABLE" 
              ADD 
              (
                "JOB_EXECUTABLE_ID" number(16,0) CONSTRAINT PP_JOB_EXECUTABLE03 NOT NULL
              )';
              
              dbms_output.put_line('Creating sequence PP_JOB_EXECUTABLE_SEQ');
              
              execute immediate 'CREATE SEQUENCE PP_JOB_EXECUTABLE_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
              
              dbms_output.put_line('Creating synonym PP_JOB_EXECUTABLE for PWRPLANT.PP_JOB_EXECUTABLE');
              
              execute immediate 'CREATE or REPLACE PUBLIC SYNONYM PP_JOB_EXECUTABLE for PWRPLANT.PP_JOB_EXECUTABLE';
              execute immediate 'GRANT ALL on PP_JOB_EXECUTABLE to pwrplant_role_dev';
              
              dbms_output.put_line('Creating trigger for PWRPLANT.PP_JOB_EXECUTABLE table');
              
              execute immediate 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_EXECUTABLE
                BEFORE INSERT ON PP_JOB_EXECUTABLE
                FOR EACH ROW
              BEGIN
                  :new.JOB_EXECUTABLE_ID := PP_JOB_EXECUTABLE_SEQ.nextval;
              END;';
  
            end;
          else
            begin
              dbms_output.put_line('Column JOB_EXECUTABLE_ID for PP_JOB_EXECUTABLE table exists');
            end;
          end if; 
       
          
          
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('ARGUMENTS_OBJECT', 'PP_JOB_WORKFLOW_JOB','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column ARGUMENTS_OBJECT for PP_JOB_WORKFLOW_JOB table');
              
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
              ADD 
              (
                "ARGUMENTS_OBJECT" VARCHAR2 ( 3000 )
              )';
              
            end;
          else
            begin
              dbms_output.put_line('Column ARGUMENTS_OBJECT for PP_JOB_WORKFLOW_JOB table exists');
            end;
          end if; 
          
          
          
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('ARGUMENTS', 'PP_JOB_WORKFLOW_JOB', 'PWRPLANT');
          
          if doesTableColumnExist = 1 then
            begin
              dbms_output.put_line('Altering column ARGUMENTS for PP_JOB_WORKFLOW_JOB table');
              
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
              MODIFY
              (
                "ARGUMENTS" VARCHAR2 ( 2000 )
              )';
              
            end;
          else
            begin
              dbms_output.put_line('Column ARGUMENTS for PP_JOB_WORKFLOW_JOB not found');
            end;
          end if; 
          
          
          
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('ARGUMENTS', 'PP_JOB_EXECUTION_JOB', 'PWRPLANT');
          
          if doesTableColumnExist = 1 then
            begin
              dbms_output.put_line('Altering column ARGUMENTS for PP_JOB_EXECUTION_JOB table');
              
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
              MODIFY
              (
                "ARGUMENTS" VARCHAR2 ( 2000 )
              )';
              
            end;
          else
            begin
              dbms_output.put_line('Column ARGUMENTS for PP_JOB_EXECUTION_JOB not found');
            end;
          end if; 
          
          
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('ARGUMENTS_OBJECT', 'PP_JOB_WORKFLOW_JOB', 'PWRPLANT');
          
          if doesTableColumnExist = 1 then
            begin
              dbms_output.put_line('Altering column ARGUMENTS_OBJECT for PP_JOB_WORKFLOW_JOB table');
              
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
              MODIFY
              (
                "ARGUMENTS_OBJECT" VARCHAR2 ( 3000 )
              )';
              
            end;
          else
            begin
              dbms_output.put_line('Column ARGUMENTS_OBJECT for PP_JOB_WORKFLOW_JOB not found');
            end;
          end if; 

	   end;
  end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2037, 0, 10, 4, 3, 1, 040317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_040317_jobserver_runfirst.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;