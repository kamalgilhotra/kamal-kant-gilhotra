SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029411_cpr_pend_basis_triggers_and_functions.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   04/02/2013 Charlie Shilling
||============================================================================
*/

-- Drop/alter objects that must be dropped/altered.
begin
   -- drop pend_trans_add_sob trigger
   begin
      execute immediate ('drop trigger PEND_TRANS_ADD_SOB');
      DBMS_OUTPUT.PUT_LINE('PEND_TRANS_ADD_SOB trigger dropped from database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PEND_TRANS_ADD_SOB trigger does not need to be dropped from database.');
   end;
   -- drop get_cpr_sob_amt function
   begin
      execute immediate ('drop function GET_CPR_SOB_AMT');
      DBMS_OUTPUT.PUT_LINE('GET_CPR_SOB_AMT function dropped from the database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('GET_CPR_SOB_AMT function does not need to be dropped from the database.');
   end;
   -- drop get_cpr_sob_reserve function
   begin
      execute immediate ('drop function GET_CPR_SOB_RESERVE');
      DBMS_OUTPUT.PUT_LINE('GET_CPR_SOB_RESERVE function dropped from the database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('GET_CPR_SOB_RESERVE function does not need to be dropped from the database.');
   end;
   -- drop get_pend_sob_amt function
   begin
      execute immediate ('drop function GET_PEND_SOB_AMT');
      DBMS_OUTPUT.PUT_LINE('GET_PEND_SOB_AMT function dropped from the database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('GET_PEND_SOB_AMT function does not need to be dropped from the database.');
   end;
   -- drop get_pend_sob_gl function
   begin
      execute immediate ('drop function GET_PEND_SOB_GL');
      DBMS_OUTPUT.PUT_LINE('GET_PEND_SOB_GL function dropped from the database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('GET_PEND_SOB_GL function does not need to be dropped from the database.');
   end;
   -- drop pend_trans_all_sob_v view
   begin
      execute immediate ('drop view PEND_TRANS_ALL_SOB_V');
      DBMS_OUTPUT.PUT_LINE('PEND_TRANS_ALL_SOB_V view dropped from the database.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('PEND_TRANS_ALL_SOB_V view does not need to be dropped from the database.');
   end;
end;
/

create or replace procedure POST_CHECK_SET_OF_BOOKS(A_COMMIT_FLAG         in integer,
                                                    A_COMMIT_SINGLE_TRANS in varchar2) is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: POST_CHECK_SET_OF_BOOKS
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   || 10.4.0.0 03/25/2013 Charlie Shilling Obey 'POST COMMIT EACH MANUAL TRANSACTION' sys control
   ||============================================================================
   */

   MISSING_PEND_TRANS_IDS PEND_TRANS_IDS_T;
   L_MISSING_COUNT        integer;

   L_UNITIZATION_COUNT integer;

begin
   DBMS_OUTPUT.PUT_LINE('    Begin post_check_set_of_books Oracle procedure');

   select distinct PEND_TRANS_ID bulk collect
     into MISSING_PEND_TRANS_IDS
     from (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
             from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B, PEND_BASIS C
            where A.COMPANY_ID = B.COMPANY_ID
              and A.PEND_TRANS_ID = C.PEND_TRANS_ID
              and B.INCLUDE_INDICATOR = 1
              and A.POSTING_STATUS = 2
           minus (select A.PEND_TRANS_ID, 1 SET_OF_BOOKS_ID
                   from PEND_TRANSACTION A
                  where A.POSTING_STATUS = 2
                 union all
                 select B.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                   from PEND_TRANSACTION A, PEND_TRANSACTION_SET_OF_BOOKS B
                  where A.PEND_TRANS_ID = B.PEND_TRANS_ID
                    and A.POSTING_STATUS = 2))
    order by PEND_TRANS_ID;

   L_MISSING_COUNT := MISSING_PEND_TRANS_IDS.COUNT();

   DBMS_OUTPUT.PUT_LINE('      Post found ' || L_MISSING_COUNT ||
                        ' transactions with at least one missing set_of_books.');

   if L_MISSING_COUNT > 0 then
      begin
         update PEND_TRANSACTION
            set POSTING_STATUS = 3,
                POSTING_ERROR = 'POST 1019: ERROR: This transaction is missing one or more set_of_books from pend_transaction_set_of_books.'
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));
      exception
         when others then
            update PEND_TRANSACTION
               set POSTING_STATUS = 3
             where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));
      end;

      if A_COMMIT_SINGLE_TRANS = 'YES' then
         for COUNTER in 1 .. L_MISSING_COUNT
         loop
            select count(1)
              into L_UNITIZATION_COUNT
              from PEND_TRANSACTION A, PEND_TRANSACTION B
             where A.COMPANY_ID = B.COMPANY_ID
               and A.GL_JE_CODE = B.GL_JE_CODE
               and A.WORK_ORDER_NUMBER = B.WORK_ORDER_NUMBER
               and A.POSTING_STATUS = 2
               and B.PEND_TRANS_ID = MISSING_PEND_TRANS_IDS(COUNTER)
               and A.LDG_DEPR_GROUP_ID <= -1
               and A.LDG_DEPR_GROUP_ID >= -20; --ldg_depr_group_id between -20 and -1 means the transaction is from unitization

            if L_UNITIZATION_COUNT > 0 then
               --we have unitization charges pending on this work order/company/gl_je_code - error out other transactions as well.
               update PEND_TRANSACTION A
                  set POSTING_STATUS = 3
                where POSTING_STATUS = 2
                  and exists (select 1
                         from PEND_TRANSACTION B
                        where A.WORK_ORDER_NUMBER = B.WORK_ORDER_NUMBER
                          and A.COMPANY_ID = B.COMPANY_ID
                          and A.GL_JE_CODE = B.GL_JE_CODE
                          and B.PEND_TRANS_ID = MISSING_PEND_TRANS_IDS(COUNTER));
            end if;
         end loop;
      else
         update PEND_TRANSACTION A
            set POSTING_STATUS = 3
          where POSTING_STATUS = 2
            and exists
          (select 1
                   from PEND_TRANSACTION B
                  where A.WORK_ORDER_NUMBER = B.WORK_ORDER_NUMBER
                    and A.COMPANY_ID = B.COMPANY_ID
                    and A.GL_JE_CODE = B.GL_JE_CODE
                    and B.PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS)));
      end if;

      if A_COMMIT_FLAG = 1 then
         commit; --commit here to make sure that the error gets saved if the next few statements fail.
      end if;

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_transaction_temp');
      insert into PEND_TRANSACTION_TEMP
         (PEND_TRANS_ID, LDG_ASSET_ID, LDG_ACTIVITY_ID, TIME_STAMP, USER_ID, LDG_DEPR_GROUP_ID,
          BOOKS_SCHEMA_ID, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, FUNC_CLASS_ID,
          SUB_ACCOUNT_ID, ASSET_LOCATION_ID, GL_ACCOUNT_ID, COMPANY_ID, GL_POSTING_MO_YR,
          SUBLEDGER_INDICATOR, ACTIVITY_CODE, GL_JE_CODE, WORK_ORDER_NUMBER, POSTING_QUANTITY,
          USER_ID1, POSTING_AMOUNT, USER_ID2, IN_SERVICE_YEAR, DESCRIPTION, LONG_DESCRIPTION,
          PROPERTY_GROUP_ID, RETIRE_METHOD_ID, POSTING_ERROR, POSTING_STATUS, COST_OF_REMOVAL,
          SALVAGE_CASH, SALVAGE_RETURNS, GAIN_LOSS, RESERVE, MISC_DESCRIPTION, FERC_ACTIVITY_CODE,
          SERIAL_NUMBER, RESERVE_CREDITS, REPLACEMENT_AMOUNT, GAIN_LOSS_REVERSAL, USER_ID3,
          DISPOSITION_CODE, MINOR_PEND_TRAN_ID, WIP_COMP_TRANSACTION, WIP_COMPUTATION_ID,
          TAX_ORIG_MONTH_NUMBER, IMPAIRMENT_EXPENSE_AMOUNT, ADJUSTED_COST_OF_REMOVAL,
          ADJUSTED_SALVAGE_CASH, ADJUSTED_SALVAGE_RETURNS, ADJUSTED_RESERVE_CREDITS,
          ADJUSTED_RESERVE)
         select PEND_TRANS_ID,
                LDG_ASSET_ID,
                LDG_ACTIVITY_ID,
                TIME_STAMP,
                USER_ID,
                LDG_DEPR_GROUP_ID,
                BOOKS_SCHEMA_ID,
                RETIREMENT_UNIT_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                FUNC_CLASS_ID,
                SUB_ACCOUNT_ID,
                ASSET_LOCATION_ID,
                GL_ACCOUNT_ID,
                COMPANY_ID,
                GL_POSTING_MO_YR,
                SUBLEDGER_INDICATOR,
                ACTIVITY_CODE,
                GL_JE_CODE,
                WORK_ORDER_NUMBER,
                POSTING_QUANTITY,
                USER_ID1,
                POSTING_AMOUNT,
                USER_ID2,
                IN_SERVICE_YEAR,
                DESCRIPTION,
                LONG_DESCRIPTION,
                PROPERTY_GROUP_ID,
                RETIRE_METHOD_ID,
                POSTING_ERROR,
                POSTING_STATUS,
                COST_OF_REMOVAL,
                SALVAGE_CASH,
                SALVAGE_RETURNS,
                GAIN_LOSS,
                RESERVE,
                MISC_DESCRIPTION,
                FERC_ACTIVITY_CODE,
                SERIAL_NUMBER,
                RESERVE_CREDITS,
                REPLACEMENT_AMOUNT,
                GAIN_LOSS_REVERSAL,
                USER_ID3,
                DISPOSITION_CODE,
                MINOR_PEND_TRAN_ID,
                WIP_COMP_TRANSACTION,
                WIP_COMPUTATION_ID,
                TAX_ORIG_MONTH_NUMBER,
                IMPAIRMENT_EXPENSE_AMOUNT,
                ADJUSTED_COST_OF_REMOVAL,
                ADJUSTED_SALVAGE_CASH,
                ADJUSTED_SALVAGE_RETURNS,
                ADJUSTED_RESERVE_CREDITS,
                ADJUSTED_RESERVE
           from PEND_TRANSACTION
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_basis_temp');
      insert into PEND_BASIS_TEMP
         (PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, GL_ACCT_1, BASIS_2, GL_ACCT_2, BASIS_3,
          GL_ACCT_3, BASIS_4, GL_ACCT_4, BASIS_5, GL_ACCT_5, BASIS_6, GL_ACCT_6, BASIS_7, GL_ACCT_7,
          BASIS_8, GL_ACCT_8, BASIS_9, GL_ACCT_9, BASIS_10, GL_ACCT_10, BASIS_11, GL_ACCT_11,
          BASIS_12, GL_ACCT_12, BASIS_13, GL_ACCT_13, BASIS_14, GL_ACCT_14, BASIS_15, GL_ACCT_15,
          BASIS_16, GL_ACCT_16, BASIS_17, GL_ACCT_17, BASIS_18, GL_ACCT_18, BASIS_19, GL_ACCT_19,
          BASIS_20, GL_ACCT_20, BASIS_21, GL_ACCT_21, BASIS_22, GL_ACCT_22, BASIS_23, GL_ACCT_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70, GL_ACCT_24, GL_ACCT_25, GL_ACCT_26, GL_ACCT_27, GL_ACCT_28, GL_ACCT_29,
          GL_ACCT_30, GL_ACCT_31, GL_ACCT_32, GL_ACCT_33, GL_ACCT_34, GL_ACCT_35, GL_ACCT_36,
          GL_ACCT_37, GL_ACCT_38, GL_ACCT_39, GL_ACCT_40, GL_ACCT_41, GL_ACCT_42, GL_ACCT_43,
          GL_ACCT_44, GL_ACCT_45, GL_ACCT_46, GL_ACCT_47, GL_ACCT_48, GL_ACCT_49, GL_ACCT_50,
          GL_ACCT_51, GL_ACCT_52, GL_ACCT_53, GL_ACCT_54, GL_ACCT_55, GL_ACCT_56, GL_ACCT_57,
          GL_ACCT_58, GL_ACCT_59, GL_ACCT_60, GL_ACCT_61, GL_ACCT_62, GL_ACCT_63, GL_ACCT_64,
          GL_ACCT_65, GL_ACCT_66, GL_ACCT_67, GL_ACCT_68, GL_ACCT_69, GL_ACCT_70)
         select PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                BASIS_1,
                GL_ACCT_1,
                BASIS_2,
                GL_ACCT_2,
                BASIS_3,
                GL_ACCT_3,
                BASIS_4,
                GL_ACCT_4,
                BASIS_5,
                GL_ACCT_5,
                BASIS_6,
                GL_ACCT_6,
                BASIS_7,
                GL_ACCT_7,
                BASIS_8,
                GL_ACCT_8,
                BASIS_9,
                GL_ACCT_9,
                BASIS_10,
                GL_ACCT_10,
                BASIS_11,
                GL_ACCT_11,
                BASIS_12,
                GL_ACCT_12,
                BASIS_13,
                GL_ACCT_13,
                BASIS_14,
                GL_ACCT_14,
                BASIS_15,
                GL_ACCT_15,
                BASIS_16,
                GL_ACCT_16,
                BASIS_17,
                GL_ACCT_17,
                BASIS_18,
                GL_ACCT_18,
                BASIS_19,
                GL_ACCT_19,
                BASIS_20,
                GL_ACCT_20,
                BASIS_21,
                GL_ACCT_21,
                BASIS_22,
                GL_ACCT_22,
                BASIS_23,
                GL_ACCT_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70,
                GL_ACCT_24,
                GL_ACCT_25,
                GL_ACCT_26,
                GL_ACCT_27,
                GL_ACCT_28,
                GL_ACCT_29,
                GL_ACCT_30,
                GL_ACCT_31,
                GL_ACCT_32,
                GL_ACCT_33,
                GL_ACCT_34,
                GL_ACCT_35,
                GL_ACCT_36,
                GL_ACCT_37,
                GL_ACCT_38,
                GL_ACCT_39,
                GL_ACCT_40,
                GL_ACCT_41,
                GL_ACCT_42,
                GL_ACCT_43,
                GL_ACCT_44,
                GL_ACCT_45,
                GL_ACCT_46,
                GL_ACCT_47,
                GL_ACCT_48,
                GL_ACCT_49,
                GL_ACCT_50,
                GL_ACCT_51,
                GL_ACCT_52,
                GL_ACCT_53,
                GL_ACCT_54,
                GL_ACCT_55,
                GL_ACCT_56,
                GL_ACCT_57,
                GL_ACCT_58,
                GL_ACCT_59,
                GL_ACCT_60,
                GL_ACCT_61,
                GL_ACCT_62,
                GL_ACCT_63,
                GL_ACCT_64,
                GL_ACCT_65,
                GL_ACCT_66,
                GL_ACCT_67,
                GL_ACCT_68,
                GL_ACCT_69,
                GL_ACCT_70
           from PEND_BASIS
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Delete from pend_basis');
      delete from PEND_BASIS
       where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_basis');
      insert into PEND_BASIS
         (PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, GL_ACCT_1, BASIS_2, GL_ACCT_2, BASIS_3,
          GL_ACCT_3, BASIS_4, GL_ACCT_4, BASIS_5, GL_ACCT_5, BASIS_6, GL_ACCT_6, BASIS_7, GL_ACCT_7,
          BASIS_8, GL_ACCT_8, BASIS_9, GL_ACCT_9, BASIS_10, GL_ACCT_10, BASIS_11, GL_ACCT_11,
          BASIS_12, GL_ACCT_12, BASIS_13, GL_ACCT_13, BASIS_14, GL_ACCT_14, BASIS_15, GL_ACCT_15,
          BASIS_16, GL_ACCT_16, BASIS_17, GL_ACCT_17, BASIS_18, GL_ACCT_18, BASIS_19, GL_ACCT_19,
          BASIS_20, GL_ACCT_20, BASIS_21, GL_ACCT_21, BASIS_22, GL_ACCT_22, BASIS_23, GL_ACCT_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70, GL_ACCT_24, GL_ACCT_25, GL_ACCT_26, GL_ACCT_27, GL_ACCT_28, GL_ACCT_29,
          GL_ACCT_30, GL_ACCT_31, GL_ACCT_32, GL_ACCT_33, GL_ACCT_34, GL_ACCT_35, GL_ACCT_36,
          GL_ACCT_37, GL_ACCT_38, GL_ACCT_39, GL_ACCT_40, GL_ACCT_41, GL_ACCT_42, GL_ACCT_43,
          GL_ACCT_44, GL_ACCT_45, GL_ACCT_46, GL_ACCT_47, GL_ACCT_48, GL_ACCT_49, GL_ACCT_50,
          GL_ACCT_51, GL_ACCT_52, GL_ACCT_53, GL_ACCT_54, GL_ACCT_55, GL_ACCT_56, GL_ACCT_57,
          GL_ACCT_58, GL_ACCT_59, GL_ACCT_60, GL_ACCT_61, GL_ACCT_62, GL_ACCT_63, GL_ACCT_64,
          GL_ACCT_65, GL_ACCT_66, GL_ACCT_67, GL_ACCT_68, GL_ACCT_69, GL_ACCT_70)
         select PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                BASIS_1,
                GL_ACCT_1,
                BASIS_2,
                GL_ACCT_2,
                BASIS_3,
                GL_ACCT_3,
                BASIS_4,
                GL_ACCT_4,
                BASIS_5,
                GL_ACCT_5,
                BASIS_6,
                GL_ACCT_6,
                BASIS_7,
                GL_ACCT_7,
                BASIS_8,
                GL_ACCT_8,
                BASIS_9,
                GL_ACCT_9,
                BASIS_10,
                GL_ACCT_10,
                BASIS_11,
                GL_ACCT_11,
                BASIS_12,
                GL_ACCT_12,
                BASIS_13,
                GL_ACCT_13,
                BASIS_14,
                GL_ACCT_14,
                BASIS_15,
                GL_ACCT_15,
                BASIS_16,
                GL_ACCT_16,
                BASIS_17,
                GL_ACCT_17,
                BASIS_18,
                GL_ACCT_18,
                BASIS_19,
                GL_ACCT_19,
                BASIS_20,
                GL_ACCT_20,
                BASIS_21,
                GL_ACCT_21,
                BASIS_22,
                GL_ACCT_22,
                BASIS_23,
                GL_ACCT_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70,
                GL_ACCT_24,
                GL_ACCT_25,
                GL_ACCT_26,
                GL_ACCT_27,
                GL_ACCT_28,
                GL_ACCT_29,
                GL_ACCT_30,
                GL_ACCT_31,
                GL_ACCT_32,
                GL_ACCT_33,
                GL_ACCT_34,
                GL_ACCT_35,
                GL_ACCT_36,
                GL_ACCT_37,
                GL_ACCT_38,
                GL_ACCT_39,
                GL_ACCT_40,
                GL_ACCT_41,
                GL_ACCT_42,
                GL_ACCT_43,
                GL_ACCT_44,
                GL_ACCT_45,
                GL_ACCT_46,
                GL_ACCT_47,
                GL_ACCT_48,
                GL_ACCT_49,
                GL_ACCT_50,
                GL_ACCT_51,
                GL_ACCT_52,
                GL_ACCT_53,
                GL_ACCT_54,
                GL_ACCT_55,
                GL_ACCT_56,
                GL_ACCT_57,
                GL_ACCT_58,
                GL_ACCT_59,
                GL_ACCT_60,
                GL_ACCT_61,
                GL_ACCT_62,
                GL_ACCT_63,
                GL_ACCT_64,
                GL_ACCT_65,
                GL_ACCT_66,
                GL_ACCT_67,
                GL_ACCT_68,
                GL_ACCT_69,
                GL_ACCT_70
           from PEND_BASIS_TEMP;

      DBMS_OUTPUT.PUT_LINE('      Update adjusted columns on pend_transaction with saved values.');
      update PEND_TRANSACTION A
         set (ADJUSTED_COST_OF_REMOVAL,
               ADJUSTED_SALVAGE_CASH,
               ADJUSTED_SALVAGE_RETURNS,
               ADJUSTED_RESERVE_CREDITS,
               ADJUSTED_RESERVE) =
              (select ADJUSTED_COST_OF_REMOVAL,
                      ADJUSTED_SALVAGE_CASH,
                      ADJUSTED_SALVAGE_RETURNS,
                      ADJUSTED_RESERVE_CREDITS,
                      ADJUSTED_RESERVE
                 from PEND_TRANSACTION_TEMP B
                where A.PEND_TRANS_ID = B.PEND_TRANS_ID)
       where exists (select 1 from PEND_TRANSACTION_TEMP B where A.PEND_TRANS_ID = B.PEND_TRANS_ID);

      --dont' need to worry about other transactions on work order because that was already taken care of above (after the 1019 error)
      update PEND_TRANSACTION
         set POSTING_ERROR = 'POST 1020: ERROR: Post created new set_of_books data for this transaction. Please review and re-approve to Post.'
       where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      if A_COMMIT_FLAG = 1 then
         commit;
      end if;
   end if;
end POST_CHECK_SET_OF_BOOKS;
/


create or replace function GET_PEND_SOB_ACTV_CODE(P_LDG_ASSET_ID       PEND_TRANSACTION.LDG_ASSET_ID%type,
                                                  P_SET_OF_BOOKS_ID    SET_OF_BOOKS.SET_OF_BOOKS_ID%type,
                                                  P_GL_POSTING_MO_YR   PEND_TRANSACTION.GL_POSTING_MO_YR%type,
                                                  P_ACTIVITY_CODE      PEND_TRANSACTION.ACTIVITY_CODE%type,
                                                  P_POSTING_AMOUNT     PEND_TRANSACTION.POSTING_AMOUNT%type,
                                                  P_GAIN_LOSS_REVERSAL PEND_TRANSACTION.GAIN_LOSS_REVERSAL%type)
   return PEND_TRANSACTION.ACTIVITY_CODE%type is

   V_ACTIVITY_CODE       PEND_TRANSACTION.ACTIVITY_CODE%type;
   V_GL_DEFAULT          DEPR_METHOD_RATES.GAIN_LOSS_DEFAULT%type;
   V_DEPR_GROUP_ID       DEPR_GROUP.DEPR_GROUP_ID%type;
   V_DEPR_INDICATOR      SUBLEDGER_CONTROL.DEPRECIATION_INDICATOR%type;
   V_SUBLEDGER_INDICATOR CPR_LEDGER.SUBLEDGER_INDICATOR%type;
   V_CHECK_COUNT         number(22, 0);

begin
   V_ACTIVITY_CODE := trim(UPPER(P_ACTIVITY_CODE));

   if V_ACTIVITY_CODE = 'URET' or V_ACTIVITY_CODE = 'URGL' or V_ACTIVITY_CODE = 'SALE' or
      V_ACTIVITY_CODE = 'SAGL' then

      -- protect against RWIP Closeout (ldg_asset_id -999) and COR transactions with ldg_asset_id = NULL,
      --   neither of which will never be gain_loss transactions
      if P_LDG_ASSET_ID = -999 or P_LDG_ASSET_ID is null then
         if V_ACTIVITY_CODE = 'URET' or V_ACTIVITY_CODE = 'URGL' then
            return 'URET';
         else
            return 'SALE';
         end if;
      end if;

      --if posting_amount is zero, check if there is a gain_loss_reversal that we need to take into account
      if P_POSTING_AMOUNT = 0 then
         begin
            select count(1)
              into V_CHECK_COUNT
              from WO_ACCRUED_GAIN_LOSS
             where ASSET_ID = P_LDG_ASSET_ID
               and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID;
         exception
            when NO_DATA_FOUND then
               if V_ACTIVITY_CODE = 'URET' or V_ACTIVITY_CODE = 'URGL' then
                  return 'URET';
               else
                  return 'SALE';
               end if;
         end;
      end if;

      --these transactions might be GL or not. get depr_group, subledger_indicator, and depr_indicator
      begin
         select A.DEPR_GROUP_ID, B.DEPRECIATION_INDICATOR, A.SUBLEDGER_INDICATOR
           into V_DEPR_GROUP_ID, V_DEPR_INDICATOR, V_SUBLEDGER_INDICATOR
           from CPR_LEDGER A, SUBLEDGER_CONTROL B
          where A.SUBLEDGER_INDICATOR = B.SUBLEDGER_TYPE_ID
            and A.ASSET_ID = P_LDG_ASSET_ID;
      exception
         when NO_DATA_FOUND then
            RAISE_APPLICATION_ERROR(-20006,
                                    'No CPR asset found for ldg_asset_id ' ||
                                    'on pend_transaction. ldg_asset_id: ' || P_LDG_ASSET_ID);
      end;

      if V_SUBLEDGER_INDICATOR = 0 then
         --group depreciated - check GL default on depr_method_Rates
         select NVL(B.GAIN_LOSS_DEFAULT, 0)
           into V_GL_DEFAULT
           from DEPR_GROUP A, DEPR_METHOD_RATES B
          where A.DEPR_GROUP_ID = V_DEPR_GROUP_ID
            and A.DEPR_METHOD_ID = B.DEPR_METHOD_ID
            and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and B.EFFECTIVE_DATE =
                (select max(EFFECTIVE_DATE)
                   from DEPR_METHOD_RATES M
                  where M.DEPR_METHOD_ID = B.DEPR_METHOD_ID
                    and M.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
                    and M.EFFECTIVE_DATE <= P_GL_POSTING_MO_YR);
      else
         --individually depreciated - check depreciation_indicator to make sure this isn't an old-style subledger
         if V_DEPR_INDICATOR <> 3 then
            V_GL_DEFAULT := 0;
         else
            V_GL_DEFAULT := 1;
         end if;
      end if;

      --set activity_code based on pend_trans activity_code and gain loss default
      if V_ACTIVITY_CODE = 'URET' or V_ACTIVITY_CODE = 'URGL' then
         if V_GL_DEFAULT = 0 then
            return 'URET';
         else
            return 'URGL';
         end if;
      else
         if V_GL_DEFAULT = 0 then
            return 'SALE';
         else
            return 'SAGL';
         end if;
      end if;
   else
      --all other transactions will not change activity code
      return V_ACTIVITY_CODE;
   end if;
end;
/


create or replace function GET_PEND_SOB_GL_REVERSAL(P_LDG_ASSET_ID      PEND_TRANSACTION.LDG_ASSET_ID%type,
                                                    P_SET_OF_BOOKS_ID   SET_OF_BOOKS.SET_OF_BOOKS_ID%type,
                                                    P_ACTIVITY_CODE     PEND_TRANSACTION.ACTIVITY_CODE%type,
                                                    P_WORK_ORDER_NUMBER PEND_TRANSACTION.WORK_ORDER_NUMBER%type,
                                                    P_COMPANY_ID        PEND_TRANSACTION.COMPANY_ID%type)
   return PEND_TRANSACTION.GAIN_LOSS_REVERSAL%type is

   V_ACTIVITY_CODE      PEND_TRANSACTION.ACTIVITY_CODE%type;
   V_GAIN_LOSS_REVERSAL PEND_TRANSACTION.GAIN_LOSS_REVERSAL%type;
   V_WORK_ORDER_ID      WORK_ORDER_CONTROL.WORK_ORDER_ID%type;

begin
   V_ACTIVITY_CODE := trim(UPPER(P_ACTIVITY_CODE));

   if V_ACTIVITY_CODE <> 'URGL' and V_ACTIVITY_CODE <> 'SAGL' then
      return 0;
   end if;

   begin
      select WORK_ORDER_ID
        into V_WORK_ORDER_ID
        from WORK_ORDER_CONTROL
       where WORK_ORDER_NUMBER = P_WORK_ORDER_NUMBER
         and COMPANY_ID = P_COMPANY_ID
         and FUNDING_WO_INDICATOR = 0;

      select NVL(-SUM(ACCR_GAIN_LOSS), 0)
        into V_GAIN_LOSS_REVERSAL
        from WO_ACCRUED_GAIN_LOSS
       where ASSET_ID = P_LDG_ASSET_ID
         and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
         and WORK_ORDER_ID = V_WORK_ORDER_ID;
   exception
      when NO_DATA_FOUND then
         return 0;
   end;

   return V_GAIN_LOSS_REVERSAL;
end;
/


create or replace function GET_PEND_SOB_RESERVE(P_ASSET_ID           number,
                                                P_SET_OF_BOOKS_ID    number,
                                                P_GL_POSTING_MO_YR   date,
                                                P_SOB_ACTIVITY_CODE  varchar2,
                                                P_SOB_POSTING_AMOUNT number,
                                                P_POSTING_QUANTITY   PEND_TRANSACTION.POSTING_QUANTITY%type)
   return number is

   V_ACCUM_QUANTITY      CPR_LEDGER.ACCUM_QUANTITY%type;
   V_RATIO               number(22, 8);
   V_SUBLEDGER_INDICATOR PEND_TRANSACTION.SUBLEDGER_INDICATOR%type;
   V_ACTIVITY_CODE       PEND_TRANSACTION.ACTIVITY_CODE%type;
   V_RESERVE_AMT         PEND_TRANSACTION.RESERVE%type;
   V_DEPR_INDICATOR      SUBLEDGER_CONTROL.DEPRECIATION_INDICATOR%type;
   V_COMPANY_ID          COMPANY_SETUP.COMPANY_ID%type;
   V_DEPR_GROUP_ID       DEPR_GROUP_CONTROL.DEPR_GROUP_ID%type;
   V_VINTAGE             DEPR_RES_ALLO_FACTORS.VINTAGE%type;

   V_CALC_PLANT           number(22, 2);
   V_CALC_RESERVE         number(22, 2);
   V_DEPR_APPROVED_ADJUST number(22, 2);

begin

   V_ACTIVITY_CODE := trim(UPPER(P_SOB_ACTIVITY_CODE));

   -- Our reserve will always be 0 except when processing a gain_loss or transfer
   if V_ACTIVITY_CODE <> 'URGL' and V_ACTIVITY_CODE <> 'SAGL' and V_ACTIVITY_CODE <> 'UTRF' and
      V_ACTIVITY_CODE <> 'UTRT' then
      return 0;
   end if;

   -- check that asset from ldg_asset_id exists and pull asset values in case we need them later
   begin
      select NVL(A.SUBLEDGER_INDICATOR, 0),
             A.ACCUM_QUANTITY,
             C.DEPRECIATION_INDICATOR,
             A.COMPANY_ID,
             A.DEPR_GROUP_ID,
             TO_NUMBER(TO_CHAR(A.ENG_IN_SERVICE_YEAR, 'YYYY'))
        into V_SUBLEDGER_INDICATOR,
             V_ACCUM_QUANTITY,
             V_DEPR_INDICATOR,
             V_COMPANY_ID,
             V_DEPR_GROUP_ID,
             V_VINTAGE
        from CPR_LEDGER A, SUBLEDGER_CONTROL C
       where A.SUBLEDGER_INDICATOR = C.SUBLEDGER_TYPE_ID
         and A.ASSET_ID = P_ASSET_ID;
   exception
      when NO_DATA_FOUND then
         RAISE_APPLICATION_ERROR(-2004,
                                 'No CPR asset found for asset_id passed to ' ||
                                 'GET_PEND_SOB_RESERVE. asset_id: ' || P_ASSET_ID);
   end;

   if V_SUBLEDGER_INDICATOR = 0 then
      --If group depreciation then get the allocation factors * sob amount
      begin
         --abs(p_sob_posting_amount) because an IF statement at the end of this function takes care of applying the correct sign.
         select ROUND(B.FACTOR * ABS(P_SOB_POSTING_AMOUNT), 2)
           into V_RESERVE_AMT
           from DEPR_RES_ALLO_FACTORS B
          where B.VINTAGE = V_VINTAGE
            and B.DEPR_GROUP_ID = V_DEPR_GROUP_ID
            and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and B.MONTH = P_GL_POSTING_MO_YR;
      exception
         when NO_DATA_FOUND then
            RAISE_APPLICATION_ERROR(-20009,
                                    'GET_PEND_SOB_RESERVE function could not find ' ||
                                    'a DEPR_RES_ALLO_FACTORS record. (depr_group_id: ' ||
                                    V_DEPR_GROUP_ID || ', set of book: ' || P_SET_OF_BOOKS_ID ||
                                    ', gl_posting_mo_yr ' || P_GL_POSTING_MO_YR || ', vintage: ' ||
                                    V_VINTAGE || ')');
      end;
   elsif V_DEPR_INDICATOR = 3 then
      --Individually depreciated get from cpr depr by pct.

      --get calc plant from cpr_depr columns
      begin
         select NVL(BEG_ASSET_DOLLARS, 0) + NVL(NET_ADDS_AND_ADJUST, 0) + NVL(RETIREMENTS, 0) +
                NVL(TRANSFERS_IN, 0) + NVL(TRANSFERS_OUT, 0) CALC_PLANT,
                NVL(BEG_RESERVE_MONTH, 0) + NVL(SALVAGE_DOLLARS, 0) + NVL(RESERVE_ADJUSTMENT, 0) +
                NVL(COST_OF_REMOVAL, 0) + NVL(RETIREMENTS, 0) + NVL(RESERVE_TRANS_IN, 0) +
                NVL(RESERVE_TRANS_OUT, 0) + NVL(DEPR_EXP_ADJUST, 0) +
                NVL(OTHER_CREDITS_AND_ADJUST, 0) + NVL(GAIN_LOSS, 0) CALC_RESERVE,
                NVL(CURR_DEPR_EXPENSE, 0) + NVL(DEPR_EXP_ALLOC_ADJUST, 0) DEPR_APPROVED_ADJUST
           into V_CALC_PLANT, V_CALC_RESERVE, V_DEPR_APPROVED_ADJUST
           from CPR_DEPR
          where ASSET_ID = P_ASSET_ID
            and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and GL_POSTING_MO_YR = P_GL_POSTING_MO_YR;
      exception
         when NO_DATA_FOUND then
            return 0;
      end;

      --calculate ratio
      if V_CALC_PLANT = 0 and V_ACCUM_QUANTITY = 0 then
         V_RATIO := 1;
      elsif V_CALC_PLANT = 0 then
         V_RATIO := ABS(P_POSTING_QUANTITY / V_ACCUM_QUANTITY);
      else
         V_RATIO := ABS(P_SOB_POSTING_AMOUNT / V_CALC_PLANT);
      end if;

      --IF depr has been approved, add in the depr_approved_offset (curr_depr_expense and depr_exp_alloc_adjust)
      declare
         V_DEPR_APPROVED date;
      begin
         select DEPR_APPROVED
           into V_DEPR_APPROVED
           from CPR_CONTROL
          where COMPANY_ID = V_COMPANY_ID
            and ACCOUNTING_MONTH = P_GL_POSTING_MO_YR;

         if V_DEPR_APPROVED is not null then
            V_CALC_RESERVE := V_CALC_RESERVE + V_DEPR_APPROVED_ADJUST;
         end if;
      exception
         when NO_DATA_FOUND then
            RAISE_APPLICATION_ERROR(-20009,
                                    'No cpr_control record found for company_id ' || V_COMPANY_ID ||
                                    ' and accounting month ' ||
                                    TO_CHAR(P_GL_POSTING_MO_YR, 'MM-DD-YYYY'));
      end;

      V_RESERVE_AMT := V_CALC_RESERVE * V_RATIO;
   else
      -- old-style subledger - reserve not calculated by set of books
      V_RESERVE_AMT := 0;
   end if;

   -- URGL, SAGL and UTRT need sign flipped
   if V_ACTIVITY_CODE = 'UTRF' then
      V_RESERVE_AMT := -V_RESERVE_AMT;
   end if;

   return V_RESERVE_AMT;
end;
/


create or replace function GET_PEND_SOB_RESERVE_HELPER(P_PEND_TRANS_ID      PEND_TRANSACTION.PEND_TRANS_ID%type,
                                                       P_LDG_ASSET_ID       number,
                                                       P_SET_OF_BOOKS_ID    number,
                                                       P_GL_POSTING_MO_YR   date,
                                                       P_SOB_ACTIVITY_CODE  varchar2,
                                                       P_SOB_POSTING_AMOUNT number,
                                                       P_POSTING_QUANTITY   PEND_TRANSACTION.POSTING_QUANTITY%type)
   return number is

   V_ASSET_ID      CPR_LEDGER.ASSET_ID%type;
   V_ACTIVITY_CODE PEND_TRANSACTION.ACTIVITY_CODE%type;

begin

   V_ACTIVITY_CODE := trim(UPPER(P_SOB_ACTIVITY_CODE));

   -- Our reserve will always be 0 except when processing a gain_loss or transfer
   if V_ACTIVITY_CODE <> 'URGL' and V_ACTIVITY_CODE <> 'SAGL' and V_ACTIVITY_CODE <> 'UTRF' and
      V_ACTIVITY_CODE <> 'UTRT' then
      return 0;
   end if;

   -- get asset id
   if V_ACTIVITY_CODE = 'URGL' or V_ACTIVITY_CODE = 'SAGL' or V_ACTIVITY_CODE = 'UTRF' then
      V_ASSET_ID := P_LDG_ASSET_ID;
   else
      begin
         select UTRF.LDG_ASSET_ID
           into V_ASSET_ID
           from PEND_TRANSACTION UTRT, PEND_TRANSACTION UTRF
          where UTRF.PEND_TRANS_ID = UTRT.LDG_ASSET_ID
            and UTRT.PEND_TRANS_ID = P_PEND_TRANS_ID;
      exception
         when NO_DATA_FOUND then
            RAISE_APPLICATION_ERROR(-2005,
                                    'Could not find ldg_asset_id on UTRF ' ||
                                    'side of transfer for UTRT. UTRT pend_trans_id: ' ||
                                    P_PEND_TRANS_ID);
      end;
   end if;

   return GET_PEND_SOB_RESERVE(V_ASSET_ID,
                               P_SET_OF_BOOKS_ID,
                               P_GL_POSTING_MO_YR,
                               P_SOB_ACTIVITY_CODE,
                               P_SOB_POSTING_AMOUNT,
                               P_POSTING_QUANTITY);
end;
/


create or replace trigger PEND_TRANS_CHECK_UPDATE
   after update on PEND_TRANSACTION
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANS_CHECK_UPDATE
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   ||============================================================================
   */

begin
   if :NEW.RESERVE <> :OLD.RESERVE then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating RESERVE on PEND_TRANSACTION is not permitted. Use ADJUSTED_RESERVE instead.');
   elsif :NEW.COST_OF_REMOVAL <> :OLD.COST_OF_REMOVAL then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating COST_OF_REMOVAL on PEND_TRANSACTION is not permitted. Use ADJUSTED_COST_OF_REMOVAL instead.');
   elsif :NEW.SALVAGE_CASH <> :OLD.SALVAGE_CASH then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating SALVAGE_CASH on PEND_TRANSACTION is not permitted. Use ADJUSTED_SALVAGE_CASH instead.');
   elsif :NEW.SALVAGE_RETURNS <> :OLD.SALVAGE_RETURNS then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating SALVAGE_RETURNS on PEND_TRANSACTION is not permitted. Use ADJUSTED_SALVAGE_RETURNS instead.');
   elsif :NEW.RESERVE_CREDITS <> :OLD.RESERVE_CREDITS then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating RESERVE_CREDITS on PEND_TRANSACTION is not permitted. Use ADJUSTED_RESERVE_CREDITS instead.');
   end if;
end;
/


create or replace trigger PEND_TRANS_SOB_CHECK_UPDATE
   after update on PEND_TRANSACTION_SET_OF_BOOKS
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANS_SOB_CHECK_UPDATE
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   ||============================================================================
   */

begin
   if :NEW.RESERVE <> :OLD.RESERVE then
      RAISE_APPLICATION_ERROR(-20000,
                              'Updating RESERVE on PEND_TRANSACTION_SET_OF_BOOKS is not permitted. Use ADJUSTED_RESERVE instead.');
   end if;
end;
/


create or replace trigger PEND_BASIS_INSERT_PEND_SOB
   after insert on PEND_BASIS
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_BASIS_INSERT_PEND_SOB
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   || 10.4.0.0 03/12/2013 Charlie Shilling Maint-29411
   ||============================================================================
   */

declare
   V_LDG_ASSET_ID      PEND_TRANSACTION.LDG_ASSET_ID%type;
   V_GL_POSTING_MO_YR  PEND_TRANSACTION.GL_POSTING_MO_YR%type;
   V_ACTIVITY_CODE     PEND_TRANSACTION.ACTIVITY_CODE%type;
   V_POSTING_AMOUNT    PEND_TRANSACTION.POSTING_AMOUNT%type;
   V_POSTING_QUANTITY  PEND_TRANSACTION.POSTING_QUANTITY%type;
   V_WORK_ORDER_NUMBER PEND_TRANSACTION.WORK_ORDER_NUMBER%type;
   V_COMPANY_ID        PEND_TRANSACTION.COMPANY_ID%type;

begin
   select LDG_ASSET_ID,
          GL_POSTING_MO_YR,
          ACTIVITY_CODE,
          POSTING_AMOUNT,
          POSTING_QUANTITY,
          WORK_ORDER_NUMBER,
          COMPANY_ID
     into V_LDG_ASSET_ID,
          V_GL_POSTING_MO_YR,
          V_ACTIVITY_CODE,
          V_POSTING_AMOUNT,
          V_POSTING_QUANTITY,
          V_WORK_ORDER_NUMBER,
          V_COMPANY_ID
     from PEND_TRANSACTION
    where PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   if V_ACTIVITY_CODE = 'UTRT' then
      select UTRF.LDG_ASSET_ID
        into V_LDG_ASSET_ID
        from PEND_TRANSACTION UTRF, PEND_TRANSACTION UTRT
       where UTRT.LDG_ASSET_ID = UTRF.PEND_TRANS_ID
         and UTRT.PEND_TRANS_ID = :NEW.PEND_TRANS_ID;
   end if;

   --call functions to calculate gain_loss_reversal and reserve
   update PEND_TRANSACTION
      set RESERVE = GET_PEND_SOB_RESERVE(V_LDG_ASSET_ID,
                                          1,
                                          V_GL_POSTING_MO_YR,
                                          V_ACTIVITY_CODE,
                                          V_POSTING_AMOUNT,
                                          V_POSTING_QUANTITY),
           --HARDCODED TO FIRST SET OF BOOKS
          GAIN_LOSS_REVERSAL = GET_PEND_SOB_GL_REVERSAL(V_LDG_ASSET_ID,
                                                         1,
                                                         V_ACTIVITY_CODE,
                                                         V_WORK_ORDER_NUMBER,
                                                         V_COMPANY_ID)
    where PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   --CBS - This will be done by the table trigger after insert
   /*    --now that we have our gain_loss_reversal and reserve, update gain_loss
   update pend_transaction
   set gain_loss = decode(
            trim(ACTIVITY_CODE),
            'URGL', (nvl(RESERVE,0) + POSTING_AMOUNT + nvl(SALVAGE_CASH,0) + nvl(SALVAGE_RETURNS,0) + nvl(RESERVE_CREDITS,0) + nvl(COST_OF_REMOVAL,0) + nvl(GAIN_LOSS_REVERSAL,0))*(-1),
            'SAGL', (nvl(RESERVE,0) + POSTING_AMOUNT + nvl(SALVAGE_CASH,0) + nvl(SALVAGE_RETURNS,0) + nvl(RESERVE_CREDITS,0) + nvl(COST_OF_REMOVAL,0) + nvl(GAIN_LOSS_REVERSAL,0))*(-1),
            0
         )
   where pend_trans_id = :NEW.pend_trans_id; */

   --update adjusted columns using the original value from pend_transaction
   --This is only done in the insert trigger.
   update PEND_TRANSACTION
      set (ADJUSTED_COST_OF_REMOVAL,
            ADJUSTED_SALVAGE_CASH,
            ADJUSTED_SALVAGE_RETURNS,
            ADJUSTED_RESERVE_CREDITS,
            ADJUSTED_RESERVE) =
           (select A.COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE,
                   A.RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE,
                   A.RESERVE
              from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
             where A.COMPANY_ID = B.COMPANY_ID
               and B.SET_OF_BOOKS_ID = 1
               and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID)
    where PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   --CBS - gain loss calc was removed from here and will now be done by a table trigger (instead of row trigger)
   --       fired after the pend_basis insert
   insert into PEND_TRANSACTION_SET_OF_BOOKS
      (PEND_TRANS_ID, SET_OF_BOOKS_ID, ACTIVITY_CODE, ADJUSTED_COST_OF_REMOVAL,
       ADJUSTED_SALVAGE_CASH, ADJUSTED_SALVAGE_RETURNS, GAIN_LOSS, RESERVE, ADJUSTED_RESERVE,
       ADJUSTED_RESERVE_CREDITS, REPLACEMENT_AMOUNT, GAIN_LOSS_REVERSAL, POSTING_AMOUNT,
       IMPAIRMENT_EXPENSE_AMOUNT)
      select PEND_TRANS_ID,
             SET_OF_BOOKS_ID,
             ACTIVITY_CODE,
             COST_OF_REMOVAL,
             SALVAGE_CASH,
             SALVAGE_RETURNS,
             0 GAIN_LOSS,
             RESERVE,
             RESERVE,
             RESERVE_CREDITS,
             REPLACEMENT_AMOUNT,
             GAIN_LOSS_REVERSAL,
             POSTING_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT
        from (select PEND_TRANS_ID,
                     SET_OF_BOOKS_ID,
                     ACTIVITY_CODE,
                     COST_OF_REMOVAL,
                     SALVAGE_CASH,
                     SALVAGE_RETURNS,
                     GET_PEND_SOB_RESERVE_HELPER(PEND_TRANS_ID,
                                                 LDG_ASSET_ID,
                                                 SET_OF_BOOKS_ID,
                                                 GL_POSTING_MO_YR,
                                                 ACTIVITY_CODE,
                                                 POSTING_AMOUNT,
                                                 POSTING_QUANTITY) RESERVE,
                     RESERVE_CREDITS,
                     REPLACEMENT_AMOUNT,
                     GET_PEND_SOB_GL_REVERSAL(LDG_ASSET_ID,
                                              SET_OF_BOOKS_ID,
                                              ACTIVITY_CODE,
                                              WORK_ORDER_NUMBER,
                                              COMPANY_ID) GAIN_LOSS_REVERSAL,
                     POSTING_AMOUNT,
                     IMPAIRMENT_EXPENSE_AMOUNT
                from (select PEND_TRANS_ID,
                             SET_OF_BOOKS_ID,
                             WORK_ORDER_NUMBER, --need to carry forward to use in get_pend_sob_gl_reversal function
                             COMPANY_ID, --need to carry forward to use in get_pend_sob_gl_reversal function
                             LDG_ASSET_ID,
                             GL_POSTING_MO_YR,
                             GET_PEND_SOB_ACTV_CODE(LDG_ASSET_ID,
                                                    SET_OF_BOOKS_ID,
                                                    GL_POSTING_MO_YR,
                                                    ACTIVITY_CODE,
                                                    POSTING_AMOUNT,
                                                    GAIN_LOSS_REVERSAL) ACTIVITY_CODE,
                             COST_OF_REMOVAL,
                             SALVAGE_CASH,
                             SALVAGE_RETURNS,
                             RESERVE_CREDITS,
                             REPLACEMENT_AMOUNT,
                             GAIN_LOSS_REVERSAL,
                             POSTING_AMOUNT,
                             POSTING_QUANTITY,
                             IMPAIRMENT_EXPENSE_AMOUNT
                        from (select A.PEND_TRANS_ID,
                                     B.SET_OF_BOOKS_ID,
                                     A.WORK_ORDER_NUMBER, --need to carry forward to use in get_pend_sob_gl_reversal function
                                     A.COMPANY_ID, --need to carry forward to use in get_pend_sob_gl_reversal function
                                     A.LDG_ASSET_ID,
                                     A.GL_POSTING_MO_YR,
                                     A.ACTIVITY_CODE,
                                     NVL(COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE, 0) COST_OF_REMOVAL,
                                     NVL(SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_CASH,
                                     NVL(SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_RETURNS,
                                     NVL(RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE, 0) RESERVE_CREDITS,
                                     NVL(REPLACEMENT_AMOUNT, 0) REPLACEMENT_AMOUNT,
                                     NVL(GAIN_LOSS_REVERSAL, 0) GAIN_LOSS_REVERSAL,
                                     (select NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                                             NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                                             NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                                             NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                                             NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                                             NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                                             NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                                             NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                                             NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                                             NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                                             NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                                             NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                                             NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                                             NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                                             NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                                             NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                                             NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                                             NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                                             NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                                             NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                                             NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                                             NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                                             NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                                             NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                                             NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                                             NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                                             NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                                             NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                                             NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                                             NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                                             NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                                             NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                                             NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                                             NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                                             NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                                             NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                                             NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                                             NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                                             NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                                             NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                                             NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                                             NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                                             NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                                             NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                                             NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                                             NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                                             NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                                             NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                                             NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                                             NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                                             NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                                             NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                                             NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                                             NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                                             NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                                             NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                                             NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                                             NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                                             NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                                             NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                                             NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                                             NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                                             NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                                             NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                                             NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                                             NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                                             NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                                             NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                                             NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                                             NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR
                                        from SET_OF_BOOKS
                                       where SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID) POSTING_AMOUNT,
                                     NVL(POSTING_QUANTITY, 0) POSTING_QUANTITY,
                                     DECODE(trim(ACTIVITY_CODE),
                                            'IMPA',
                                            (A.IMPAIRMENT_EXPENSE_AMOUNT *
                                            DECODE(B.SET_OF_BOOKS_ID,
                                                    (select max(SET_OF_BOOKS_ID)
                                                       from CPR_IMPAIRMENT_EVENT
                                                      where IMPAIRMENT_ID =
                                                            (select TO_NUMBER(SUBSTR(WORK_ORDER_NUMBER,
                                                                                     4,
                                                                                     100))
                                                               from PEND_TRANSACTION
                                                              where PEND_TRANS_ID = :NEW.PEND_TRANS_ID)),
                                                    1,
                                                    0)),
                                            0) IMPAIRMENT_EXPENSE_AMOUNT
                                from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
                               where A.COMPANY_ID = B.COMPANY_ID
                                 and B.SET_OF_BOOKS_ID <> 1
                                 and B.INCLUDE_INDICATOR = 1
                                 and LOWER(trim(DECODE(A.FERC_ACTIVITY_CODE,
                                                       2,
                                                       A.DESCRIPTION,
                                                       'not unretire'))) <> 'unretire'
                                 and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                                 and (PEND_TRANS_ID, SET_OF_BOOKS_ID) in
                                     (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                                        from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
                                       where A.COMPANY_ID = B.COMPANY_ID
                                         and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                                      minus
                                      select PEND_TRANS_ID, SET_OF_BOOKS_ID
                                        from PEND_TRANSACTION_SET_OF_BOOKS
                                       where PEND_TRANS_ID = :NEW.PEND_TRANS_ID))));
end;
/


create or replace trigger PEND_BASIS_UPDATE_PEND_SOB
   after update on PEND_BASIS
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_BASIS_UPDATE_PEND_SOB
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   || 10.4.0.0 03/12/2013 Charlie Shilling Maint-29411
   ||============================================================================
   */

begin
   update PEND_TRANSACTION A
      set POSTING_AMOUNT =
           (select sum(NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                       NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                       NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                       NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                       NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                       NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                       NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                       NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                       NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                       NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                       NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                       NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                       NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                       NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                       NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                       NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                       NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                       NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                       NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                       NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                       NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                       NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                       NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                       NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                       NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                       NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                       NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                       NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                       NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                       NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                       NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                       NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                       NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                       NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                       NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                       NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                       NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                       NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                       NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                       NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                       NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                       NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                       NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                       NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                       NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                       NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                       NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                       NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                       NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                       NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                       NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                       NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                       NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                       NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                       NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                       NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                       NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                       NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                       NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                       NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                       NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                       NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                       NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                       NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                       NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                       NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                       NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                       NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                       NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                       NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR)
              from SET_OF_BOOKS B
             where B.SET_OF_BOOKS_ID = 1)
    where A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   update PEND_TRANSACTION_SET_OF_BOOKS A
      set POSTING_AMOUNT =
           (select sum(NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                       NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                       NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                       NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                       NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                       NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                       NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                       NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                       NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                       NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                       NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                       NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                       NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                       NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                       NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                       NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                       NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                       NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                       NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                       NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                       NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                       NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                       NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                       NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                       NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                       NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                       NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                       NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                       NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                       NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                       NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                       NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                       NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                       NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                       NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                       NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                       NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                       NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                       NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                       NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                       NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                       NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                       NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                       NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                       NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                       NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                       NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                       NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                       NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                       NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                       NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                       NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                       NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                       NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                       NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                       NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                       NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                       NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                       NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                       NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                       NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                       NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                       NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                       NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                       NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                       NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                       NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                       NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                       NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                       NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR)
              from SET_OF_BOOKS B
             where A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID)
    where A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
      and exists (select 1
             from PEND_TRANSACTION C, COMPANY_SET_OF_BOOKS D
            where C.COMPANY_ID = D.COMPANY_ID
              and A.PEND_TRANS_ID = C.PEND_TRANS_ID
              and A.SET_OF_BOOKS_ID = D.SET_OF_BOOKS_ID
              and D.INCLUDE_INDICATOR = 1);
end;
/


create or replace trigger PEND_BASIS_INSERT_GNLS_REV
   after insert on PEND_BASIS
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_BASIS_INSERT_GNLS_REV
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 03/27/2013 Brandon Beck  INITIAL Version to spread gain-loss-reversal across pend transactions
   ||============================================================================
   */

begin

   --
   -- Allocate the gain_loss_reversal based on the COR and SALVAGE allocation of charges
   --
   update PEND_TRANSACTION PT2
      set GAIN_LOSS_REVERSAL =
           (with PT_VIEW as (select SP.PEND_TRANS_ID,
                                    ROUND(SP.GAIN_LOSS_REVERSAL * SP.RATE, 2) +
                                    DECODE(ROW_NUMBER() OVER(partition by SP.WORK_ORDER_NUMBER,
                                                SP.COMPANY_ID,
                                                SP.LDG_ASSET_ID order by SP.RATE desc,
                                                SP.PEND_TRANS_ID),
                                           1,
                                           SP.GAIN_LOSS_REVERSAL -
                                           sum(ROUND(SP.GAIN_LOSS_REVERSAL * SP.RATE, 2))
                                           OVER(partition by SP.WORK_ORDER_NUMBER,
                                                SP.COMPANY_ID,
                                                SP.LDG_ASSET_ID),
                                           0) WITH_PLUG
                               from (select PT.PEND_TRANS_ID,
                                            PT.WORK_ORDER_NUMBER,
                                            PT.COMPANY_ID,
                                            PT.LDG_ASSET_ID,
                                            GET_PEND_SOB_GL_REVERSAL(PT.LDG_ASSET_ID,
                                                                     1,
                                                                     PT.ACTIVITY_CODE,
                                                                     PT.WORK_ORDER_NUMBER,
                                                                     PT.COMPANY_ID) as GAIN_LOSS_REVERSAL,
                                            RATIO_TO_REPORT(NVL(ABS(PT.COST_OF_REMOVAL), 0) +
                                                            NVL(ABS(PT.SALVAGE_CASH), 0) +
                                                            NVL(ABS(PT.SALVAGE_RETURNS), 0)) OVER(partition by PT.WORK_ORDER_NUMBER, PT.COMPANY_ID, PT.LDG_ASSET_ID) as RATE
                                       from PEND_TRANSACTION PT
                                      where PT.POSTING_STATUS = 1
                                        and PT.LDG_DEPR_GROUP_ID <= -1
                                        and PT.LDG_DEPR_GROUP_ID >= -20
                                        and UPPER(trim(PT.ACTIVITY_CODE)) in ('URGL', 'SAGL')) SP)
              select PV.WITH_PLUG
                from PT_VIEW PV
               where PV.PEND_TRANS_ID = PT2.PEND_TRANS_ID)
               where PT2.POSTING_STATUS = 1
                 and PT2.LDG_DEPR_GROUP_ID <= -1
                 and PT2.LDG_DEPR_GROUP_ID >= -20
                 and UPPER(trim(PT2.ACTIVITY_CODE)) in ('URGL', 'SAGL');


   -- now update gain_loss
   update PEND_TRANSACTION
      set GAIN_LOSS = -1 * ((NVL(RESERVE, 0) + POSTING_AMOUNT + NVL(SALVAGE_CASH, 0) +
                       NVL(SALVAGE_RETURNS, 0) + NVL(RESERVE_CREDITS, 0) +
                       NVL(COST_OF_REMOVAL, 0) + NVL(GAIN_LOSS_REVERSAL, 0)))
    where UPPER(trim(ACTIVITY_CODE)) in ('URGL', 'SAGL')
      and POSTING_STATUS = 1;

   -- update pend_trans_set_of_books
   update PEND_TRANSACTION_SET_OF_BOOKS PT2
      set GAIN_LOSS_REVERSAL =
           (with PT_VIEW as (select SP.PEND_TRANS_ID,
                                    SP.SET_OF_BOOKS_ID,
                                    ROUND(SP.GAIN_LOSS_REVERSAL * SP.RATE, 2) +
                                    DECODE(ROW_NUMBER() OVER(partition by SP.WORK_ORDER_NUMBER,
                                                SP.COMPANY_ID,
                                                SP.LDG_ASSET_ID,
                                                SP.SET_OF_BOOKS_ID order by SP.RATE desc,
                                                SP.PEND_TRANS_ID),
                                           1,
                                           SP.GAIN_LOSS_REVERSAL -
                                           sum(ROUND(SP.GAIN_LOSS_REVERSAL * SP.RATE, 2))
                                           OVER(partition by SP.WORK_ORDER_NUMBER,
                                                SP.COMPANY_ID,
                                                SP.LDG_ASSET_ID,
                                                SP.SET_OF_BOOKS_ID),
                                           0) WITH_PLUG
                               from (select PT.PEND_TRANS_ID,
                                            PT3.WORK_ORDER_NUMBER,
                                            PT3.COMPANY_ID,
                                            PT3.LDG_ASSET_ID,
                                            PT.SET_OF_BOOKS_ID,
                                            GET_PEND_SOB_GL_REVERSAL(PT3.LDG_ASSET_ID,
                                                                     PT.SET_OF_BOOKS_ID,
                                                                     PT.ACTIVITY_CODE,
                                                                     PT3.WORK_ORDER_NUMBER,
                                                                     PT3.COMPANY_ID) as GAIN_LOSS_REVERSAL,
                                            RATIO_TO_REPORT(NVL(ABS(PT.ADJUSTED_COST_OF_REMOVAL), 0) +
                                                            NVL(ABS(PT.ADJUSTED_SALVAGE_CASH), 0) +
                                                            NVL(ABS(PT.ADJUSTED_SALVAGE_RETURNS), 0)) OVER(partition by PT3.WORK_ORDER_NUMBER, PT3.COMPANY_ID, PT3.LDG_ASSET_ID, PT.SET_OF_BOOKS_ID) as RATE
                                       from PEND_TRANSACTION_SET_OF_BOOKS PT, PEND_TRANSACTION PT3
                                      where PT3.POSTING_STATUS = 1
                                        and PT3.LDG_DEPR_GROUP_ID <= -1
                                        and PT3.LDG_DEPR_GROUP_ID >= -20
                                        and PT.PEND_TRANS_ID = PT3.PEND_TRANS_ID
                                        and UPPER(trim(PT.ACTIVITY_CODE)) in ('URGL', 'SAGL')) SP)
              select PV.WITH_PLUG
                from PT_VIEW PV
               where PV.PEND_TRANS_ID = PT2.PEND_TRANS_ID
                 and PV.SET_OF_BOOKS_ID = PT2.SET_OF_BOOKS_ID)
               where UPPER(trim(PT2.ACTIVITY_CODE)) in ('URGL', 'SAGL')
                 and exists (select 1
                        from PEND_TRANSACTION PT3
                       where PT3.POSTING_STATUS = 1
                         and PT3.LDG_DEPR_GROUP_ID <= -1
                         and PT3.LDG_DEPR_GROUP_ID >= -20
                         and PT2.PEND_TRANS_ID = PT3.PEND_TRANS_ID);


   -- update gain_loss
   update PEND_TRANSACTION_SET_OF_BOOKS PT_SOB
      set GAIN_LOSS = -1 * ((NVL(RESERVE, 0) + POSTING_AMOUNT + NVL(ADJUSTED_SALVAGE_CASH, 0) +
                       NVL(ADJUSTED_SALVAGE_RETURNS, 0) + NVL(ADJUSTED_RESERVE_CREDITS, 0) +
                       NVL(ADJUSTED_COST_OF_REMOVAL, 0) + NVL(GAIN_LOSS_REVERSAL, 0)))
    where UPPER(trim(ACTIVITY_CODE)) in ('URGL', 'SAGL')
      and exists (select 1
             from PEND_TRANSACTION PT
            where PT.POSTING_STATUS = 1
              and PT_SOB.PEND_TRANS_ID = PT.PEND_TRANS_ID);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (342, 0, 10, 4, 0, 0, 29411, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029411_cpr_pend_basis_triggers_and_functions.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;