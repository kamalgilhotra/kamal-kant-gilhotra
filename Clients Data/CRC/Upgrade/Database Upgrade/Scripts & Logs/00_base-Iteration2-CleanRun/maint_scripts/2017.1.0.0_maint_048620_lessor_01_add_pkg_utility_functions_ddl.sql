/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048620_lessor_01_add_pkg_utility_functions_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/12/2017 Andrew Hill      Add utility functions for pkg dev
||============================================================================
*/


CREATE OR REPLACE FUNCTION f_get_call_stack RETURN VARCHAR2 IS
  /*****************************************************************************
  * Function: f_get_call_stack
  * PURPOSE: Returns the call stack at the current time
  * NOTE: This uses a different implementation for versions of Oracle starting with 12,
  *       in order to provide more information (esp. function names inside packages,
  *                                               instead of line numbers
  *
  * RETURNS: Call stack
  ******************************************************************************/
  call_stack VARCHAR2(32767);
  l_depth pls_integer;
BEGIN
  $IF dbms_db_version.VERSION > 11 $THEN
    --https://oracle-base.com/articles/12c/utl-call-stack-12cr1#call-stack
    l_depth := utl_call_stack.dynamic_depth;
    call_stack := '***** Call Stack *****' || CHR(10);
    call_stack := call_stack || 'Depth     Lexical   Line      Owner     Edition   Name' || CHR(10);
    call_stack := call_stack || '.         Depth     Number' || CHR(10);
    call_stack := call_stack || '--------- --------- --------- --------- --------- --------------------';

    FOR I IN 1 .. l_depth LOOP
      call_stack := call_stack || CHR(10);
      call_stack := call_stack || rpad(I, 10) ||
                                  RPAD(UTL_CALL_STACK.lexical_depth(i), 10) ||
                                  RPAD(TO_CHAR(UTL_CALL_STACK.unit_line(i),'99'), 10) ||
                                  RPAD(NVL(UTL_CALL_STACK.owner(i),' '), 10) ||
                                  RPAD(NVL(UTL_CALL_STACK.current_edition(i),' '), 10) ||
                                  UTL_CALL_STACK.concatenate_subprogram(UTL_CALL_STACK.subprogram(i));
    END LOOP;
  $ELSE
    call_stack := dbms_utility.format_call_stack;
  $END
  RETURN call_stack;
END;
/

CREATE OR REPLACE FUNCTION f_get_pp_process_id(a_process_description pp_processes.DESCRIPTION%TYPE) RETURN pp_processes.process_id%TYPE
RESULT_CACHE RELIES_ON (pp_processes)
IS
  /*****************************************************************************
  * Function: f_get_pp_process_id
  * 
  * PARAMETERS: The description of the process in pp_processes
  * RETURNS: The process_id of the process
  ******************************************************************************/
  l_id pp_processes.process_id%type;
BEGIN
  SELECT process_id INTO l_id
  FROM pp_processes
  WHERE UPPER(TRIM(DESCRIPTION)) = UPPER(TRIM(a_process_description));
  
  IF l_id IS NULL THEN
    raise_application_error(-20000, 'Invalid process description passed to f_get_pp_process_id' || chr(10) || f_get_call_stack);
  END IF;
  
  return l_id;
end;
/

CREATE INDEX pp_processes_up_trim_desc_idx ON pp_processes (UPPER(TRIM("DESCRIPTION"))) TABLESPACE pwrplant_idx;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3703, 0, 2017, 1, 0, 0, 48620, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048620_lessor_01_add_pkg_utility_functions_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
