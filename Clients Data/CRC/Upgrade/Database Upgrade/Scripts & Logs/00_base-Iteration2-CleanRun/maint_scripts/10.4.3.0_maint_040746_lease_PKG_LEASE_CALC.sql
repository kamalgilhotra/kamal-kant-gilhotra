/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040746_lease_PKG_LEASE_CALC.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/12/2014 Brandon Beck
||============================================================================
*/

create or replace package PKG_LEASE_CALC as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 07/10/2013 Ryan Oliveria  MLA functions (workflows and revisions)
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   || 10.4.1.0 07/30/2013 Kyle Peterson  Payments and Invoices
   ||============================================================================
   */
   type NUM_ARRAY is table of number(22) index by binary_integer;
   type ASSET_SCHEDULE_LINE_TYPE is table of LS_ASSET_SCHEDULE%rowtype index by pls_integer;
   type ASSET_SCHEDULE_LINE_TYPE2 is record(
      LS_ASSET_ID     LS_ASSET_SCHEDULE.LS_ASSET_ID%type,
      INTEREST_PAID   LS_ASSET_SCHEDULE.INTEREST_PAID%type,
      PRINCIPAL_PAID  LS_ASSET_SCHEDULE.PRINCIPAL_PAID%type,
      LEASE_ID        LS_LEASE.LEASE_ID%type,
      VENDOR_ID       LS_LEASE_VENDOR.VENDOR_ID%type,
      PAYMENT_PCT     LS_LEASE_VENDOR.PAYMENT_PCT%type,
	  EXECUTORY_PAID1 LS_ASSET_SCHEDULE.EXECUTORY_PAID1%type,
	  EXECUTORY_PAID2 LS_ASSET_SCHEDULE.EXECUTORY_PAID2%type,
	  EXECUTORY_PAID3 LS_ASSET_SCHEDULE.EXECUTORY_PAID3%type,
	  EXECUTORY_PAID4 LS_ASSET_SCHEDULE.EXECUTORY_PAID4%type,
	  EXECUTORY_PAID5 LS_ASSET_SCHEDULE.EXECUTORY_PAID5%type,
	  EXECUTORY_PAID6 LS_ASSET_SCHEDULE.EXECUTORY_PAID6%type,
	  EXECUTORY_PAID7 LS_ASSET_SCHEDULE.EXECUTORY_PAID7%type,
	  EXECUTORY_PAID8 LS_ASSET_SCHEDULE.EXECUTORY_PAID8%type,
	  EXECUTORY_PAID9 LS_ASSET_SCHEDULE.EXECUTORY_PAID9%type,
	  EXECUTORY_PAID10 LS_ASSET_SCHEDULE.EXECUTORY_PAID10%type,
	  CONTINGENT_PAID1 LS_ASSET_SCHEDULE.CONTINGENT_PAID1%type,
	  CONTINGENT_PAID2 LS_ASSET_SCHEDULE.CONTINGENT_PAID2%type,
	  CONTINGENT_PAID3 LS_ASSET_SCHEDULE.CONTINGENT_PAID3%type,
	  CONTINGENT_PAID4 LS_ASSET_SCHEDULE.CONTINGENT_PAID4%type,
	  CONTINGENT_PAID5 LS_ASSET_SCHEDULE.CONTINGENT_PAID5%type,
	  CONTINGENT_PAID6 LS_ASSET_SCHEDULE.CONTINGENT_PAID6%type,
	  CONTINGENT_PAID7 LS_ASSET_SCHEDULE.CONTINGENT_PAID7%type,
	  CONTINGENT_PAID8 LS_ASSET_SCHEDULE.CONTINGENT_PAID8%type,
	  CONTINGENT_PAID9 LS_ASSET_SCHEDULE.CONTINGENT_PAID9%type,
	  CONTINGENT_PAID10 LS_ASSET_SCHEDULE.CONTINGENT_PAID10%type,
      ROWNUMBER       number,
      SET_OF_BOOKS_ID LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID%type,
	  COMPANY_ID LS_ASSET.COMPANY_ID%type,
	  MONTH date, ILR_ID LS_ILR.ILR_ID%type, LS_RECONCILE_TYPE_ID LS_RECONCILE_TYPE.ls_reconcile_type_id%type);
   type ASSET_SCHEDULE_LINE_TABLE is table of ASSET_SCHEDULE_LINE_TYPE2 index by pls_integer;

   procedure P_SET_ILR_ID(A_ILR_ID number);

   procedure P_INVOICE_COMPARE(MYINVOICE in LS_INVOICE%rowtype,
                               MYPAYMENT in LS_PAYMENT_HDR%rowtype);

   procedure P_PAYMENT_ROLLUP(A_LEASE_ID in number, A_COMPANY_ID in number, A_MONTH date);

   function F_PAYMENT_MONTH(A_ILR_ID   number,
                            A_REVISION number,
                            A_MONTH    date) return number;

   function F_APPROVE_MLA(A_LEASE_ID number,
                          A_REVISION number) return number;

   function F_REJECT_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_SEND_MLA(A_LEASE_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_MLA(A_LEASE_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID number,
                                  A_REVISION number) return number;

   function F_APPROVE_ILR(A_ILR_ID   number,
                          A_REVISION number) return number;

   function F_REJECT_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_SEND_ILR(A_ILR_ID   number,
                       A_REVISION number) return number;

   function F_UNREJECT_ILR(A_ILR_ID   number,
                           A_REVISION number) return number;

   function F_UNSEND_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   number,
                                  A_REVISION number) return number;

   function F_ACCRUALS_CALC(A_COMPANY_ID  number,
                            A_MONTH       date) return varchar2;

   function F_ACCRUALS_APPROVE(A_COMPANY_ID in number,
                               A_MONTH      in date) return varchar2;

   function F_PAYMENT_CALC(A_COMPANY_ID in number,
                           A_MONTH      in date) return varchar2;

	function F_PAYMENT_CALC(A_LEASE_ID in number, A_COMPANY_ID in number,
                           A_MONTH      in date) return varchar2;

   function F_PAYMENT_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date) return varchar2;

   function F_SEND_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UPDATE_WORKFLOW_PAYMENT(A_PAYMENT_ID number) return number;

   function F_REJECT_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UNREJECT_PAYMENT(A_PAYMENT_ID number) return number;
   --TO APPROVE SINGLE PAYMENT THROUGH WORKFLOW
   function F_APPROVE_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UNSEND_PAYMENT(A_PAYMENT_ID number) return number;

   function F_MATCH_INVOICES(A_MONTH in date) return varchar2;

   function F_GET_ILR_ID return number;

   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   out varchar2) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean) return number;

   function F_LAM_CLOSED(A_COMPANY_ID in number,
                         A_MONTH      in date) return varchar2;

   function F_DEPR_APPROVE(A_COMPANY_ID in number,
                           A_MONTH      in date) return varchar2;

   function F_PROCESS_RESIDUAL(A_LS_ASSET_IDS in NUM_ARRAY) return varchar2;

   function F_PROCESS_RESIDUAL(A_LS_ASSET_IDS in NUM_ARRAY,
                           A_END_LOG in number) return varchar2;

end PKG_LEASE_CALC;
/


create or replace package body PKG_LEASE_CALC as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   ||============================================================================
   */

   L_ILR_ID number;
   type PAYMENT_HDR_TYPE is table of LS_PAYMENT_HDR%rowtype index by pls_integer;
   type INVOICE_HDR_TYPE is table of LS_INVOICE%rowtype index by pls_integer;
   type PAYMENT_LINE_TYPE is table of LS_PAYMENT_LINE%rowtype index by pls_integer;
   type ASSET_TAX_LINE_TYPE is record(
      RATE LS_TAX_STATE_RATES.RATE%type,
      LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
      TAX_LOCAL_ID LS_TAX_LOCAL.TAX_LOCAL_ID%type,
      SET_OF_BOOKS_ID SET_OF_BOOKS.SET_OF_BOOKS_ID%type,
      AMOUNT LS_ASSET_SCHEDULE.INTEREST_ACCRUAL%type,
	  SHIFT LS_ILR_OPTIONS.PAYMENT_SHIFT%type,
	  VENDOR_ID LS_LEASE_VENDOR.VENDOR_ID%type);
   type ASSET_SCHEDULE_HEADER_TYPE is record(
	  LEASE_ID LS_LEASE.LEASE_ID%type,
	  ILR_ID LS_ILR.ILR_ID%type,
	  LS_ASSET_ID LS_ASSET.LS_ASSET_ID %type,
	  VENDOR_ID LS_VENDOR.VENDOR_ID%type);
   type ASSET_TAX_TABLE is table of ASSET_TAX_LINE_TYPE index by pls_integer;
   type ASSET_SCHEDULE_HEADER_TABLE is table of ASSET_SCHEDULE_HEADER_TYPE index by pls_integer;

   --FORWARD DECLARATIONS
   function F_TAX_CALC_ACCRUALS(A_COMPANY_ID in number,
								A_MONTH      in date) return varchar2;
   function F_TAX_CALC_PAYMENTS(A_LEASE_ID in number, A_COMPANY_ID in number,
								A_MONTH      in date) return varchar2;

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_LOGERRORMESSAGE(A_ILR_ID in number,
                               A_MSG    in varchar2) is
      pragma autonomous_transaction;
   begin
      update LS_PEND_TRANSACTION set ERROR_MESSAGE = A_MSG where ILR_ID = A_ILR_ID;

      commit;
   end P_LOGERRORMESSAGE;

   --**************************************************************************
   --                            P_SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            P_AUTO_GENERATE_INVOICES
   --**************************************************************************

   procedure P_AUTO_GENERATE_INVOICES(A_LEASE_ID in number, A_COMPANY_ID number,
                              A_MONTH date) is
   begin

      merge into LS_INVOICE LI
      using (select LPH.*
             from LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
            where LPH.COMPANY_ID = A_COMPANY_ID
              and LPH.GL_POSTING_MO_YR = A_MONTH
              and LPH.LEASE_ID = LL.LEASE_ID
              and LL.LEASE_ID = LLO.LEASE_ID
			  and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
              and LL.CURRENT_REVISION = LLO.REVISION
              and LLO.AUTO_GENERATE_INVOICES = 1
              and LPH.PAYMENT_STATUS_ID < 3) LPH
      on (LPH.VENDOR_ID = LI.VENDOR_ID and LPH.COMPANY_ID = LI.COMPANY_ID and LPH.GL_POSTING_MO_YR = LI.GL_POSTING_MO_YR
		and LPH.LEASE_ID = LI.LEASE_ID and LPH.ILR_ID = LI.ILR_ID and LPH.LS_ASSET_ID = LI.LS_ASSET_ID
		)
      when matched then
         update
            set LI.INVOICE_NUMBER = 'AUTO ' || TO_CHAR(LPH.GL_POSTING_MO_YR) || '-' ||
                              TO_CHAR(LPH.LEASE_ID) || '-' || TO_CHAR(LPH.COMPANY_ID) || '-' ||
                              TO_CHAR(LPH.VENDOR_ID)
      when not matched then
         insert
            (INVOICE_ID, COMPANY_ID, LEASE_ID, GL_POSTING_MO_YR, VENDOR_ID, INVOICE_NUMBER, ILR_ID, LS_ASSET_ID)
         values
            (LS_INVOICE_SEQ.NEXTVAL, LPH.COMPANY_ID, LPH.LEASE_ID, LPH.GL_POSTING_MO_YR, LPH.VENDOR_ID,
             'AUTO ' || TO_CHAR(LPH.GL_POSTING_MO_YR) || '-' || TO_CHAR(LPH.LEASE_ID) || '-' ||
              TO_CHAR(LPH.COMPANY_ID) || '-' || TO_CHAR(LPH.VENDOR_ID), LPH.ILR_ID, LPH.LS_ASSET_ID
			  );

      merge into LS_INVOICE_PAYMENT_MAP LIPM
      using (select LPH.PAYMENT_ID, LI.INVOICE_ID
             from LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL, LS_INVOICE LI
            where LPH.COMPANY_ID = A_COMPANY_ID
              and LPH.GL_POSTING_MO_YR = A_MONTH
              and LPH.LEASE_ID = LL.LEASE_ID
              and LL.LEASE_ID = LLO.LEASE_ID
              and LL.CURRENT_REVISION = LLO.REVISION
              and LLO.AUTO_GENERATE_INVOICES = 1
              and LPH.COMPANY_ID = LI.COMPANY_ID
              and LPH.VENDOR_ID = LI.VENDOR_ID
              and LPH.LEASE_ID = LI.LEASE_ID
			  and LPH.ILR_ID = LI.ILR_ID
			  and LPH.LS_ASSET_ID = LI.LS_ASSET_ID
			  and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
              and LPH.GL_POSTING_MO_YR = LI.GL_POSTING_MO_YR
              and LPH.PAYMENT_STATUS_ID < 3) B
      on (LIPM.INVOICE_ID = B.INVOICE_ID and LIPM.PAYMENT_ID = B.PAYMENT_ID)
      when matched then
         update set LIPM.IN_TOLERANCE = 1
      when not matched then
         insert (INVOICE_ID, PAYMENT_ID, IN_TOLERANCE) values (B.INVOICE_ID, B.PAYMENT_ID, 1);

      --Delete from LS_INVOICE_LINE where the (line, type, asset) is not in LS_PAYMENT_LINE
      delete from LS_INVOICE_LINE
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID < 3)
         and not exists (select 1
              from LS_INVOICE_LINE        LII,
                   LS_INVOICE_PAYMENT_MAP LIPM,
                   LS_PAYMENT_HDR         LPH,
                   LS_LEASE_OPTIONS       LLO,
                   LS_LEASE               LL,
                   LS_PAYMENT_LINE        LPL
             where LII.INVOICE_ID = LIPM.INVOICE_ID
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
               and LPL.PAYMENT_ID = LPH.PAYMENT_ID
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LPL.PAYMENT_LINE_NUMBER = LII.INVOICE_LINE_NUMBER
               and LPL.PAYMENT_TYPE_ID = LII.PAYMENT_TYPE_ID
               and LPL.LS_ASSET_ID = LII.LS_ASSET_ID);

      --merge into LS_INVOICE_LINE from LS_PAYMENT_LINE
      merge into LS_INVOICE_LINE A
      using (select LPL.*, LIPM.INVOICE_ID
             from LS_PAYMENT_LINE        LPL,
                  LS_PAYMENT_HDR         LPH,
                  LS_LEASE               LL,
                  LS_LEASE_OPTIONS       LLO,
                  LS_INVOICE_PAYMENT_MAP LIPM
            where LPL.PAYMENT_ID = LPH.PAYMENT_ID
              and LPH.COMPANY_ID = A_COMPANY_ID
              and LPH.GL_POSTING_MO_YR = A_MONTH
              and LPH.LEASE_ID = LL.LEASE_ID
              and LL.LEASE_ID = LLO.LEASE_ID
              and LL.CURRENT_REVISION = LLO.REVISION
			  and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
              and LLO.AUTO_GENERATE_INVOICES = 1
              and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
              and LPL.SET_OF_BOOKS_ID = (select min(SET_OF_BOOKS_ID) from SET_OF_BOOKS)
              and LPH.PAYMENT_STATUS_ID < 3) B
      on (A.INVOICE_ID = B.INVOICE_ID and A.INVOICE_LINE_NUMBER = B.PAYMENT_LINE_NUMBER and A.PAYMENT_TYPE_ID = B.PAYMENT_TYPE_ID and A.LS_ASSET_ID = B.LS_ASSET_ID)
      when matched then
         update
            set A.AMOUNT = B.AMOUNT, A.DESCRIPTION = '',
               A.NOTES = 'Automatically generated on ' || TO_CHAR(sysdate) || '.',
               A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR
      when not matched then
         insert
            (INVOICE_ID, INVOICE_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, GL_POSTING_MO_YR, AMOUNT,
             NOTES)
         values
            (B.INVOICE_ID, B.PAYMENT_LINE_NUMBER, B.PAYMENT_TYPE_ID, B.LS_ASSET_ID, B.GL_POSTING_MO_YR,
             B.AMOUNT, 'Automatically generated on ' || TO_CHAR(sysdate) || '.');

      --Do the backfills for the amounts.
      update LS_INVOICE
         set (INVOICE_AMOUNT) =
            (select sum(A.AMOUNT)
               from LS_INVOICE_LINE A
              where A.INVOICE_ID = LS_INVOICE.INVOICE_ID
              group by INVOICE_ID)
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID < 3);

      update LS_INVOICE
         set (INVOICE_INTEREST) =
            (select sum(A.AMOUNT)
               from LS_INVOICE_LINE A
              where A.INVOICE_ID = LS_INVOICE.INVOICE_ID
               and A.PAYMENT_TYPE_ID in (2)
              group by INVOICE_ID)
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID < 3);

       update LS_INVOICE set(INVOICE_EXECUTORY) = (select sum(A.AMOUNT)
                                 from LS_INVOICE_LINE A
                                where A.INVOICE_ID = LS_INVOICE.INVOICE_ID
                                 and A.PAYMENT_TYPE_ID between 3 and 12
                                group by INVOICE_ID)
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID < 3);

       update LS_INVOICE set(INVOICE_CONTINGENT) = (select sum(A.AMOUNT)
                                 from LS_INVOICE_LINE A
                                 where A.INVOICE_ID = LS_INVOICE.INVOICE_ID
                                  and A.PAYMENT_TYPE_ID between 13 and 22
                                 group by INVOICE_ID)
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID < 3);

   end P_AUTO_GENERATE_INVOICES;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   function F_PAYMENT_MONTH(A_ILR_ID in number,
                            A_REVISION in number,
                            A_MONTH in date) return number is
      rtn      number;
      i        number;
      loc_date date;
   begin
      for L_PAYMENT_TERMS in (select   LIP.PAYMENT_TERM_ID as PAYMENT_TERM_ID,
                              LIP.PAYMENT_TERM_TYPE_ID as PAYMENT_TERM_TYPE_ID,
                              LIP.PAYMENT_TERM_DATE as PAYMENT_TERM_DATE,
                              LIP.PAYMENT_FREQ_ID as PAYMENT_FREQ_ID,
                              LIP.NUMBER_OF_TERMS as NUMBER_OF_TERMS,
                              LL.PRE_PAYMENT_SW as PRE_PAYMENT_SW
                        from LS_ILR_PAYMENT_TERM LIP, LS_ILR LI, LS_LEASE LL
                        where LIP.ILR_ID = LI.ILR_ID
                        and LI.LEASE_ID = LL.LEASE_ID
                        and LIP.ILR_ID = A_ILR_ID
                        and LIP.REVISION = A_REVISION
                        and LIP.PAYMENT_TERM_TYPE_ID <> 1
                        order by LIP.PAYMENT_TERM_ID)
      loop
         if A_MONTH between L_PAYMENT_TERMS.PAYMENT_TERM_DATE
                  and add_months(L_PAYMENT_TERMS.PAYMENT_TERM_DATE, L_PAYMENT_TERMS.NUMBER_OF_TERMS *
                     case L_PAYMENT_TERMS.PAYMENT_FREQ_ID
                        when 1 then 12
                        when 2 then 6
                        when 3 then 3
                        else 1
                     end) then
            loc_date := case L_PAYMENT_TERMS.PRE_PAYMENT_SW
                        when 1 then L_PAYMENT_TERMS.PAYMENT_TERM_DATE
                        when 0 then add_months(L_PAYMENT_TERMS.PAYMENT_TERM_DATE, case L_PAYMENT_TERMS.PAYMENT_FREQ_ID --Shift our start point
                                          when 1 then 11                                        -- to the end of the term
                                          when 2 then 5                                         --when doing arrears
                                          when 3 then 2
                                          else 0
                                       end)
                        end;
            for i in 1 .. L_PAYMENT_TERMS.NUMBER_OF_TERMS
            loop
               if loc_date = A_MONTH then return 1; end if;
               loc_date := add_months(loc_date, case L_PAYMENT_TERMS.PAYMENT_FREQ_ID
                                          when 1 then 12
                                          when 2 then 6
                                          when 3 then 3
                                          else 1
                                       end);
            end loop;
            return 0;
         end if;
      end loop;
      return 0;
   exception
      when others then
         rollback;
         return -1;
   end F_PAYMENT_MONTH;
   --**************************************************************************
   --                            F_PAYMENT_II
   --**************************************************************************
   function F_PAYMENT_II( A_COMPANY_ID in number,
						  A_MONTH in date) return number is
		L_STATUS varchar2(2000);
		L_GL_JE_CODE varchar2(35);
		L_COUNTER number := 0;
		L_RTN number;
	begin

		select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMPAY'))
		into L_GL_JE_CODE
		from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
		where E.JE_ID = G.JE_ID
		and G.PROCESS_ID = 'LAMPAY';

		for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
									LV.VENDOR_ID as VENDOR_ID,
									LV.PAYMENT_PCT as PAYMENT_PCT,
									LCM.AMOUNT as AMOUNT,
									L.WORK_ORDER_ID as WORK_ORDER_ID,
									L.COMPANY_ID as COMPANY_ID,
									3019 as TRANS_TYPE,
									SB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
									LIA.AP_ACCOUNT_ID as AP_ACCOUNT_ID
							 from LS_ASSET L
							 join LS_ILR LI on L.ILR_ID = LI.ILR_ID
							 join LS_LEASE LL on LL.LEASE_ID = LI.LEASE_ID
							 join LS_LEASE_VENDOR LV on LL.LEASE_ID = LV.LEASE_ID and L.COMPANY_ID = LV.COMPANY_ID
							 join LS_COMPONENT LC on L.LS_ASSET_ID = LC.LS_ASSET_ID
							 join LS_COMPONENT_MONTHLY_II_STG LCM on LCM.COMPONENT_ID = LC.COMPONENT_ID
							 join LS_ILR_ACCOUNT LIA on LI.ILR_ID = LIA.ILR_ID
							 cross join SET_OF_BOOKS SB
							 where L.COMPANY_ID = A_COMPANY_ID
							 and LCM.MONTH = A_MONTH)
		loop
			L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
						TO_CHAR(L_PAYMENTS.TRANS_TYPE);
			L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
												   L_PAYMENTS.TRANS_TYPE,
												   L_PAYMENTS.AMOUNT * L_PAYMENTS.PAYMENT_PCT,
												   0,
												   -1,
												   L_PAYMENTS.WORK_ORDER_ID,
												   -1, --No accrual account
												   0,
												   -1,
												   L_PAYMENTS.COMPANY_ID,
												   A_MONTH,
												   1,
												   L_GL_JE_CODE,
												   L_PAYMENTS.SET_OF_BOOKS_ID,
												   'ii',
												   L_STATUS);
			if L_RTN = -1 then
				return L_STATUS;
			end if;

			-- process the credit.  Payment credits all hit 3022
			L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
						' trans type: 3022';
			L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
												   3022,
												   L_PAYMENTS.AMOUNT * L_PAYMENTS.PAYMENT_PCT,
												   0,
												   -1,
												   L_PAYMENTS.WORK_ORDER_ID,
												   L_PAYMENTS.AP_ACCOUNT_ID,
												   0,
												   -1,
												   L_PAYMENTS.COMPANY_ID,
												   A_MONTH,
												   0,
												   L_GL_JE_CODE,
												   L_PAYMENTS.SET_OF_BOOKS_ID,
												   'ii',
												   L_STATUS);
			if L_RTN = -1 then
				return L_STATUS;
			end if;
			L_COUNTER := L_COUNTER + 1;
		end loop;

		return L_COUNTER;

	end F_PAYMENT_II;
   --**************************************************************************
   --                            F_APPROVE_MLA
   --**************************************************************************

   function F_APPROVE_MLA(A_LEASE_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 3, APPROVAL_DATE = sysdate, CURRENT_REVISION = A_REVISION
       where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving MLA');
         return -1;
   end F_APPROVE_MLA;

   --**************************************************************************
   --                            F_REJECT_MLA
   --**************************************************************************

   function F_REJECT_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 4 where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting MLA');
         return -1;
   end F_REJECT_MLA;

   --**************************************************************************
   --                            F_SEND_MLA
   --**************************************************************************

   function F_SEND_MLA(A_LEASE_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN     number;
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin

      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      --Reject other revisions
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 5, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      --Call F_APPROVE_MLA for auto-approved assets
      commit;

      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LS_LEASE L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.LEASE_ID = A_LEASE_ID;

      if IS_AUTO = 'AUTO' then
         return F_APPROVE_MLA(A_LEASE_ID, A_REVISION);
      end if;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending MLA');
         return -1;
   end F_SEND_MLA;

   --**************************************************************************
   --                            F_UNREJECT_MLA
   --**************************************************************************

   function F_UNREJECT_MLA(A_LEASE_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting MLA');
         return -1;
   end F_UNREJECT_MLA;

   --**************************************************************************
   --                            F_UNSEND_MLA
   --**************************************************************************

   function F_UNSEND_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 1, APPROVAL_DATE = null where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending MLA');
         return -1;
   end F_UNSEND_MLA;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_MLA
   --**************************************************************************

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_LEASE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_LEASE_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'mla_approval'),
                                     0)
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Updating Workflow MLA');
         return -1;
   end F_UPDATE_WORKFLOW_MLA;

   --**************************************************************************
   --                            F_deletePendTrans
   --**************************************************************************
   function F_DELETEPENDTRANS(A_ILR_ID number,
                              A_MSG    out varchar2) return number is
      MY_RTN number;
   begin
      A_MSG := 'Clearing out ls_pend_class_code';
      delete from LS_PEND_CLASS_CODE
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_basis';
      delete from LS_PEND_BASIS
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_set_of_books';
      delete from LS_PEND_SET_OF_BOOKS
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_transaction';
      delete from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID;

      return 1;
   exception
      when others then
         return -1;
   end F_DELETEPENDTRANS;

   --**************************************************************************
   --                            F_archivePendTrans
   --**************************************************************************
   function F_ARCHIVEPENDTRANS(A_ILR_ID   in number,
                               A_REVISION in number,
                               A_MSG      out varchar2) return number is
      MY_RTN number;
   begin
      --archive
      A_MSG := 'Archiving ls_pend_transaction';
      insert into LS_PEND_TRANSACTION_ARC
         (LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, LS_ASSET_ID, POSTING_AMOUNT, POSTING_QUANTITY,
          ACTIVITY_CODE, GL_JE_CODE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID,
          FUNC_CLASS_ID, ILR_ID, COMPANY_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID,
          LEASED_ASSET_NUMBER, SUB_ACCOUNT_ID, WORK_ORDER_ID, SERIAL_NUMBER, REVISION)
         select LS_PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                LS_ASSET_ID,
                POSTING_AMOUNT,
                POSTING_QUANTITY,
                ACTIVITY_CODE,
                GL_JE_CODE,
                RETIREMENT_UNIT_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                FUNC_CLASS_ID,
                ILR_ID,
                COMPANY_ID,
                ASSET_LOCATION_ID,
                PROPERTY_GROUP_ID,
                LEASED_ASSET_NUMBER,
                SUB_ACCOUNT_ID,
                WORK_ORDER_ID,
                SERIAL_NUMBER,
                REVISION
           from LS_PEND_TRANSACTION L
          where L.ILR_ID = A_ILR_ID
            and L.REVISION = A_REVISION;

      A_MSG := 'Archiving ls_pend_basis';
      insert into LS_PEND_BASIS_ARC
         (LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5,
          BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14,
          BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70)
         select B.LS_PEND_TRANS_ID,
                B.TIME_STAMP,
                B.USER_ID,
                BASIS_1,
                BASIS_2,
                BASIS_3,
                BASIS_4,
                BASIS_5,
                BASIS_6,
                BASIS_7,
                BASIS_8,
                BASIS_9,
                BASIS_10,
                BASIS_11,
                BASIS_12,
                BASIS_13,
                BASIS_14,
                BASIS_15,
                BASIS_16,
                BASIS_17,
                BASIS_18,
                BASIS_19,
                BASIS_20,
                BASIS_21,
                BASIS_22,
                BASIS_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70
           from LS_PEND_BASIS B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      A_MSG := 'Archiving ls_pend_set_of_books';
      insert into LS_PEND_SET_OF_BOOKS_ARC
         (LS_PEND_TRANS_ID, SET_OF_BOOKS_ID, POSTING_AMOUNT)
         select B.LS_PEND_TRANS_ID, B.SET_OF_BOOKS_ID, B.POSTING_AMOUNT
           from LS_PEND_SET_OF_BOOKS B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      A_MSG := 'Archiving ls_pend_class_code';
      insert into LS_PEND_CLASS_CODE_ARC
         (CLASS_CODE_ID, LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, value)
         select B.CLASS_CODE_ID, B.LS_PEND_TRANS_ID, B.TIME_STAMP, B.USER_ID, B.VALUE
           from LS_PEND_CLASS_CODE B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      return F_DELETEPENDTRANS(A_ILR_ID, A_MSG);
   exception
      when others then
         return -1;
   end F_ARCHIVEPENDTRANS;

   --**************************************************************************
   --                            F_APPROVE_ILR
   --**************************************************************************
   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      rtn := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, A_STATUS, TRUE);
      return rtn;
   exception
      when others then
         P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_STATUS := 'APPROVE ILR:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR:' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      --Approve this revision
      -- blank out the error message
      --if A_LOG then
         --A_STATUS := 'Clearing out error message';
         --PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
         --P_LOGERRORMESSAGE(A_ILR_ID, '');
      --end if;

      A_STATUS := 'Updating LS_ILR_APPROVAL1';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Updating LS_ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR L set ILR_STATUS_ID = 2, CURRENT_REVISION = A_REVISION where ILR_ID = A_ILR_ID;
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      -- LOAD THE CPR TABLES and create JEs... this is a call to the lease asset package
      A_STATUS := 'Loop over assets';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      for L_ASSET_ID in (select LS_ASSET_ID
                           from LS_PEND_TRANSACTION
                          where ILR_ID = A_ILR_ID
                            and REVISION = A_REVISION)
      loop
         A_STATUS := '   Processing asset_id: ' || TO_CHAR(L_ASSET_ID.LS_ASSET_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
         MY_STR := PKG_LEASE_ASSET_POST.F_ADD_ASSET(L_ASSET_ID.LS_ASSET_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE('   Returned: ' || MY_STR);
         if MY_STR <> 'OK' then
            -- log the error message
            A_STATUS := MY_STR;
            RAISE_APPLICATION_ERROR(-20000, 'Error Adding Leased Asset: ' || A_STATUS);
            return -1;
         end if;
      end loop;

      A_STATUS := 'Archiving';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      RTN := F_ARCHIVEPENDTRANS(A_ILR_ID, A_REVISION, A_STATUS);
      if RTN <> 1 then
         RAISE_APPLICATION_ERROR(-20000, 'Error Archiving Transactions: ' || A_STATUS);
         return -1;
      end if;



      return 1;
   exception
      when others then
      --if A_LOG then
         --P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);
      -- end if;
       PKG_PP_LOG.P_WRITE_MESSAGE(sqlerrm);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

   function F_APPROVE_ILR(A_ILR_ID   in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      MY_STR   varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      -- start by clearing our pend transaction
      RTN := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;
      return 1;
   exception
      when others then
         rollback;
         P_LOGERRORMESSAGE(A_ILR_ID, L_STATUS);
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_STATUS);
         return -1;
   end F_APPROVE_ILR;

   --**************************************************************************
   --                            F_REJECT_ILR
   --**************************************************************************

   function F_REJECT_ILR(A_ILR_ID   in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      L_STATUS varchar2(2000);
   begin
      RTN := F_DELETEPENDTRANS(A_ILR_ID, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Removing Transactions: ' || L_STATUS);
         return -1;
      end if;

      update LS_ILR_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L set ILR_STATUS_ID = 1 where ILR_ID = A_ILR_ID;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 2;

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_REJECT_ILR;

   --**************************************************************************
   --                            F_SEND_ILR
   -- Called when a user clicks send for approval (on an ILR)
   -- Need to mark the approval status to be sent.  And update the ILR to
   -- pending approval.  IN addition need to update the capitalized cost to
   -- be the beginning obligation on the asset schedule for the revision being
   -- sent for approval
   --**************************************************************************
   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   out varchar2) return number is
      RTN     number;
      ILR     number;
      WFS     number;
      SQLS    varchar2(32000);
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
      APPROVED_DATE date;
      START_DATE date;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_STATUS := 'SEND for approval:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR_ID: ' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

     --Check that the lease is valid ie in Open/New Revision status
     select count(1)
     into RTN
     from LS_LEASE LL, LS_ILR LI
     where LI.ILR_ID = A_ILR_ID
     and LI.LEASE_ID = LL.LEASE_ID
     and LL.LEASE_STATUS_ID in (3,7);

     if RTN <> 1 then
      A_STATUS := 'MLA for this ILR is not in Open or New Revision status, so the ILR cannot be posted to the MLA.';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      return -1;
     end if;


      A_STATUS := 'Remove Prior pending transactions for ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      -- start by clearing our pend transaction
      RTN := F_DELETEPENDTRANS(A_ILR_ID, A_STATUS);
      if RTN <> 1 then
         return -1;
      end if;

      A_STATUS := 'Getting start date';
      select min(PAYMENT_TERM_DATE)
        into START_DATE
      from LS_ILR_PAYMENT_TERM
        where ILR_ID = A_ILR_ID
          and REVISION = A_REVISION;

      A_STATUS := 'Getting approved date';
      select min(PAYMENT_TERM_DATE)
        into APPROVED_DATE
      from ls_ilr_payment_term pt, ls_ilr_approval appr
      where pt.ilr_id = appr.ilr_id
      and pt.revision = appr.revision
      and appr.ilr_id = A_ILR_ID
      and appr.revision = (
         select max(revision)
         from ls_ilr_approval
         where ilr_id = appr.ilr_id
           and approval_status_id in (3,6)
           and revision <> A_REVISION
      );

      PKG_PP_LOG.P_WRITE_MESSAGE('   Start Date: ' || to_char(START_DATE, 'YYYY-MM-DD'));
      PKG_PP_LOG.P_WRITE_MESSAGE('   Approved Date: ' || to_char(APPROVED_DATE, 'YYYY-MM-DD'));

      if START_DATE <> APPROVED_DATE and APPROVED_DATE is not null then
        A_STATUS := 'The first payment term date can not be changed once an ILR has been approved';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

        return -1;
      end if;


      A_STATUS := 'updating LS_ILR_APPROVAL';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      A_STATUS := 'updating LS_ASSET';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      -- update the asset status to be pending.  Also update the cap cost here.
      update LS_ASSET L
         set LS_ASSET_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 1;

      -- identify whether or not it is an adjustment or addition
      A_STATUS := 'insert into LS_PEND_TRANSACTION';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_TRANSACTION
         (LS_PEND_TRANS_ID, LS_ASSET_ID, POSTING_AMOUNT, POSTING_QUANTITY, ACTIVITY_CODE,
          GL_JE_CODE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, FUNC_CLASS_ID, ILR_ID,
          COMPANY_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID, LEASED_ASSET_NUMBER, SUB_ACCOUNT_ID,
          WORK_ORDER_ID, SERIAL_NUMBER, REVISION, IN_SERVICE_DATE)
         select LS_PEND_TRANSACTION_SEQ.NEXTVAL,
                L.LS_ASSET_ID,
                NVL(AA.BEG_CAPITAL_COST, 0) - NVL((select min(AA.BEG_CAPITAL_COST)
                                                    from LS_ASSET_SCHEDULE AA
                                                   where AA.LS_ASSET_ID = L.LS_ASSET_ID
                                                     and AA.REVISION = L.APPROVED_REVISION
                                                     and AA.SET_OF_BOOKS_ID = 1),
                                                  0),
                NVL(L.QUANTITY, 1),
                NVL((select case
                              when L.APPROVED_REVISION > 0 then
                               11
                              else
                               2
                           end
                      from LS_CPR_ASSET_MAP M
                     where M.LS_ASSET_ID = L.LS_ASSET_ID),
                    2),
                (select S.GL_JE_CODE
                   from STANDARD_JOURNAL_ENTRIES S, GL_JE_CONTROL G
                  where G.PROCESS_ID = 'LAM ADDS'
                    and G.JE_ID = S.JE_ID),
                L.RETIREMENT_UNIT_ID,
                L.UTILITY_ACCOUNT_ID,
                L.BUS_SEGMENT_ID,
                UA.FUNC_CLASS_ID,
                L.ILR_ID,
                L.COMPANY_ID,
                L.ASSET_LOCATION_ID,
                L.PROPERTY_GROUP_ID,
                L.LEASED_ASSET_NUMBER,
                L.SUB_ACCOUNT_ID,
                L.WORK_ORDER_ID,
                L.SERIAL_NUMBER,
                A_REVISION,
            AA.MONTH
           from LS_ASSET L,
                UTILITY_ACCOUNT UA,
                (select A.*,
                        ROW_NUMBER() OVER(partition by A.LS_ASSET_ID, A.SET_OF_BOOKS_ID, A.REVISION order by month) as THE_ROW
                   from LS_ASSET_SCHEDULE A, LS_ASSET LA
                  where A.LS_ASSET_ID = LA.LS_ASSET_ID
                    and LA.ILR_ID = A_ILR_ID
                    and A.REVISION = A_REVISION
                    and A.SET_OF_BOOKS_ID = 1) AA
          where L.ILR_ID = A_ILR_ID
            and AA.THE_ROW = 1
			and L.LS_ASSET_STATUS_ID <> 4
            and AA.LS_ASSET_ID = L.LS_ASSET_ID
            and UA.UTILITY_ACCOUNT_ID = L.UTILITY_ACCOUNT_ID
            and UA.BUS_SEGMENT_ID = L.BUS_SEGMENT_ID
            and exists (select 1
                   from LS_ASSET_SCHEDULE S
                  where S.REVISION = A_REVISION
                    and S.LS_ASSET_ID = L.LS_ASSET_ID);

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Getting Books Summary';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      select A.BOOK_SUMMARY_ID
        into RTN
        from LS_LEASE_CAP_TYPE A, LS_ILR_OPTIONS B
       where A.LS_LEASE_CAP_TYPE_ID = B.LEASE_CAP_TYPE_ID
         and B.ILR_ID = A_ILR_ID
         and B.REVISION = A_REVISION;

      PKG_PP_LOG.P_WRITE_MESSAGE('   ' || TO_CHAR(RTN));

      A_STATUS := 'insert into LS_PEND_BASIS';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_BASIS
         (LS_PEND_TRANS_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8,
          BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17,
          BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26,
          BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35,
          BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41, BASIS_42, BASIS_43, BASIS_44,
          BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50, BASIS_51, BASIS_52, BASIS_53,
          BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60, BASIS_61, BASIS_62,
          BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70)
         select B.LS_PEND_TRANS_ID,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
           from LS_PEND_TRANSACTION B
          where B.ILR_ID = A_ILR_ID;

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      -- backfill prior cpr_ldg_basis values
      A_STATUS := 'backfill buckets from cpr';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_PEND_BASIS PB
         set (BASIS_1,
               BASIS_2,
               BASIS_3,
               BASIS_4,
               BASIS_5,
               BASIS_6,
               BASIS_7,
               BASIS_8,
               BASIS_9,
               BASIS_10,
               BASIS_11,
               BASIS_12,
               BASIS_13,
               BASIS_14,
               BASIS_15,
               BASIS_16,
               BASIS_17,
               BASIS_18,
               BASIS_19,
               BASIS_20,
               BASIS_21,
               BASIS_22,
               BASIS_23,
               BASIS_24,
               BASIS_25,
               BASIS_26,
               BASIS_27,
               BASIS_28,
               BASIS_29,
               BASIS_30,
               BASIS_31,
               BASIS_32,
               BASIS_33,
               BASIS_34,
               BASIS_35,
               BASIS_36,
               BASIS_37,
               BASIS_38,
               BASIS_39,
               BASIS_40,
               BASIS_41,
               BASIS_42,
               BASIS_43,
               BASIS_44,
               BASIS_45,
               BASIS_46,
               BASIS_47,
               BASIS_48,
               BASIS_49,
               BASIS_50,
               BASIS_51,
               BASIS_52,
               BASIS_53,
               BASIS_54,
               BASIS_55,
               BASIS_56,
               BASIS_57,
               BASIS_58,
               BASIS_59,
               BASIS_60,
               BASIS_61,
               BASIS_62,
               BASIS_63,
               BASIS_64,
               BASIS_65,
               BASIS_66,
               BASIS_67,
               BASIS_68,
               BASIS_69,
               BASIS_70) =
              (select -1 * (C.BASIS_1 + P.BASIS_1),
                      -1 * (C.BASIS_2 + P.BASIS_2),
                      -1 * (C.BASIS_3 + P.BASIS_3),
                      -1 * (C.BASIS_4 + P.BASIS_4),
                      -1 * (C.BASIS_5 + P.BASIS_5),
                      -1 * (C.BASIS_6 + P.BASIS_6),
                      -1 * (C.BASIS_7 + P.BASIS_7),
                      -1 * (C.BASIS_8 + P.BASIS_8),
                      -1 * (C.BASIS_9 + P.BASIS_9),
                      -1 * (C.BASIS_10 + P.BASIS_10),
                      -1 * (C.BASIS_11 + P.BASIS_11),
                      -1 * (C.BASIS_12 + P.BASIS_12),
                      -1 * (C.BASIS_13 + P.BASIS_13),
                      -1 * (C.BASIS_14 + P.BASIS_14),
                      -1 * (C.BASIS_15 + P.BASIS_15),
                      -1 * (C.BASIS_16 + P.BASIS_16),
                      -1 * (C.BASIS_17 + P.BASIS_17),
                      -1 * (C.BASIS_18 + P.BASIS_18),
                      -1 * (C.BASIS_19 + P.BASIS_19),
                      -1 * (C.BASIS_20 + P.BASIS_20),
                      -1 * (C.BASIS_21 + P.BASIS_21),
                      -1 * (C.BASIS_22 + P.BASIS_22),
                      -1 * (C.BASIS_23 + P.BASIS_23),
                      -1 * (C.BASIS_24 + P.BASIS_24),
                      -1 * (C.BASIS_25 + P.BASIS_25),
                      -1 * (C.BASIS_26 + P.BASIS_26),
                      -1 * (C.BASIS_27 + P.BASIS_27),
                      -1 * (C.BASIS_28 + P.BASIS_28),
                      -1 * (C.BASIS_29 + P.BASIS_29),
                      -1 * (C.BASIS_30 + P.BASIS_30),
                      -1 * (C.BASIS_31 + P.BASIS_31),
                      -1 * (C.BASIS_32 + P.BASIS_32),
                      -1 * (C.BASIS_33 + P.BASIS_33),
                      -1 * (C.BASIS_34 + P.BASIS_34),
                      -1 * (C.BASIS_35 + P.BASIS_35),
                      -1 * (C.BASIS_36 + P.BASIS_36),
                      -1 * (C.BASIS_37 + P.BASIS_37),
                      -1 * (C.BASIS_38 + P.BASIS_38),
                      -1 * (C.BASIS_39 + P.BASIS_39),
                      -1 * (C.BASIS_40 + P.BASIS_40),
                      -1 * (C.BASIS_41 + P.BASIS_41),
                      -1 * (C.BASIS_42 + P.BASIS_42),
                      -1 * (C.BASIS_43 + P.BASIS_43),
                      -1 * (C.BASIS_44 + P.BASIS_44),
                      -1 * (C.BASIS_45 + P.BASIS_45),
                      -1 * (C.BASIS_46 + P.BASIS_46),
                      -1 * (C.BASIS_47 + P.BASIS_47),
                      -1 * (C.BASIS_48 + P.BASIS_48),
                      -1 * (C.BASIS_49 + P.BASIS_49),
                      -1 * (C.BASIS_50 + P.BASIS_50),
                      -1 * (C.BASIS_51 + P.BASIS_51),
                      -1 * (C.BASIS_52 + P.BASIS_52),
                      -1 * (C.BASIS_53 + P.BASIS_53),
                      -1 * (C.BASIS_54 + P.BASIS_54),
                      -1 * (C.BASIS_55 + P.BASIS_55),
                      -1 * (C.BASIS_56 + P.BASIS_56),
                      -1 * (C.BASIS_57 + P.BASIS_57),
                      -1 * (C.BASIS_58 + P.BASIS_58),
                      -1 * (C.BASIS_59 + P.BASIS_59),
                      -1 * (C.BASIS_60 + P.BASIS_60),
                      -1 * (C.BASIS_61 + P.BASIS_61),
                      -1 * (C.BASIS_62 + P.BASIS_62),
                      -1 * (C.BASIS_63 + P.BASIS_63),
                      -1 * (C.BASIS_64 + P.BASIS_64),
                      -1 * (C.BASIS_65 + P.BASIS_65),
                      -1 * (C.BASIS_66 + P.BASIS_66),
                      -1 * (C.BASIS_67 + P.BASIS_67),
                      -1 * (C.BASIS_68 + P.BASIS_68),
                      -1 * (C.BASIS_69 + P.BASIS_69),
                      -1 * (C.BASIS_70 + P.BASIS_70)
                 from CPR_LDG_BASIS C,
                      (select M1.ASSET_ID as ASSET_ID,
                              PT.LS_PEND_TRANS_ID as LS_PEND_TRANS_ID,
                              sum(NVL(PB.BASIS_1, 0)) as BASIS_1,
                              sum(NVL(PB.BASIS_2, 0)) as BASIS_2,
                              sum(NVL(PB.BASIS_3, 0)) as BASIS_3,
                              sum(NVL(PB.BASIS_4, 0)) as BASIS_4,
                              sum(NVL(PB.BASIS_5, 0)) as BASIS_5,
                              sum(NVL(PB.BASIS_6, 0)) as BASIS_6,
                              sum(NVL(PB.BASIS_7, 0)) as BASIS_7,
                              sum(NVL(PB.BASIS_8, 0)) as BASIS_8,
                              sum(NVL(PB.BASIS_9, 0)) as BASIS_9,
                              sum(NVL(PB.BASIS_10, 0)) as BASIS_10,
                              sum(NVL(PB.BASIS_11, 0)) as BASIS_11,
                              sum(NVL(PB.BASIS_12, 0)) as BASIS_12,
                              sum(NVL(PB.BASIS_13, 0)) as BASIS_13,
                              sum(NVL(PB.BASIS_14, 0)) as BASIS_14,
                              sum(NVL(PB.BASIS_15, 0)) as BASIS_15,
                              sum(NVL(PB.BASIS_16, 0)) as BASIS_16,
                              sum(NVL(PB.BASIS_17, 0)) as BASIS_17,
                              sum(NVL(PB.BASIS_18, 0)) as BASIS_18,
                              sum(NVL(PB.BASIS_19, 0)) as BASIS_19,
                              sum(NVL(PB.BASIS_20, 0)) as BASIS_20,
                              sum(NVL(PB.BASIS_21, 0)) as BASIS_21,
                              sum(NVL(PB.BASIS_22, 0)) as BASIS_22,
                              sum(NVL(PB.BASIS_23, 0)) as BASIS_23,
                              sum(NVL(PB.BASIS_24, 0)) as BASIS_24,
                              sum(NVL(PB.BASIS_25, 0)) as BASIS_25,
                              sum(NVL(PB.BASIS_26, 0)) as BASIS_26,
                              sum(NVL(PB.BASIS_27, 0)) as BASIS_27,
                              sum(NVL(PB.BASIS_28, 0)) as BASIS_28,
                              sum(NVL(PB.BASIS_29, 0)) as BASIS_29,
                              sum(NVL(PB.BASIS_30, 0)) as BASIS_30,
                              sum(NVL(PB.BASIS_31, 0)) as BASIS_31,
                              sum(NVL(PB.BASIS_32, 0)) as BASIS_32,
                              sum(NVL(PB.BASIS_33, 0)) as BASIS_33,
                              sum(NVL(PB.BASIS_34, 0)) as BASIS_34,
                              sum(NVL(PB.BASIS_35, 0)) as BASIS_35,
                              sum(NVL(PB.BASIS_36, 0)) as BASIS_36,
                              sum(NVL(PB.BASIS_37, 0)) as BASIS_37,
                              sum(NVL(PB.BASIS_38, 0)) as BASIS_38,
                              sum(NVL(PB.BASIS_39, 0)) as BASIS_39,
                              sum(NVL(PB.BASIS_40, 0)) as BASIS_40,
                              sum(NVL(PB.BASIS_41, 0)) as BASIS_41,
                              sum(NVL(PB.BASIS_42, 0)) as BASIS_42,
                              sum(NVL(PB.BASIS_43, 0)) as BASIS_43,
                              sum(NVL(PB.BASIS_44, 0)) as BASIS_44,
                              sum(NVL(PB.BASIS_45, 0)) as BASIS_45,
                              sum(NVL(PB.BASIS_46, 0)) as BASIS_46,
                              sum(NVL(PB.BASIS_47, 0)) as BASIS_47,
                              sum(NVL(PB.BASIS_48, 0)) as BASIS_48,
                              sum(NVL(PB.BASIS_49, 0)) as BASIS_49,
                              sum(NVL(PB.BASIS_50, 0)) as BASIS_50,
                              sum(NVL(PB.BASIS_51, 0)) as BASIS_51,
                              sum(NVL(PB.BASIS_52, 0)) as BASIS_52,
                              sum(NVL(PB.BASIS_53, 0)) as BASIS_53,
                              sum(NVL(PB.BASIS_54, 0)) as BASIS_54,
                              sum(NVL(PB.BASIS_55, 0)) as BASIS_55,
                              sum(NVL(PB.BASIS_56, 0)) as BASIS_56,
                              sum(NVL(PB.BASIS_57, 0)) as BASIS_57,
                              sum(NVL(PB.BASIS_58, 0)) as BASIS_58,
                              sum(NVL(PB.BASIS_59, 0)) as BASIS_59,
                              sum(NVL(PB.BASIS_60, 0)) as BASIS_60,
                              sum(NVL(PB.BASIS_61, 0)) as BASIS_61,
                              sum(NVL(PB.BASIS_62, 0)) as BASIS_62,
                              sum(NVL(PB.BASIS_63, 0)) as BASIS_63,
                              sum(NVL(PB.BASIS_64, 0)) as BASIS_64,
                              sum(NVL(PB.BASIS_65, 0)) as BASIS_65,
                              sum(NVL(PB.BASIS_66, 0)) as BASIS_66,
                              sum(NVL(PB.BASIS_67, 0)) as BASIS_67,
                              sum(NVL(PB.BASIS_68, 0)) as BASIS_68,
                              sum(NVL(PB.BASIS_69, 0)) as BASIS_69,
                              sum(NVL(PB.BASIS_70, 0)) as BASIS_70
                         from PEND_BASIS          PB,
                              PEND_TRANSACTION    PP,
                              LS_CPR_ASSET_MAP    M1,
                              LS_PEND_TRANSACTION PT
                        where PP.LDG_ASSET_ID(+) = M1.ASSET_ID
                          and PP.PEND_TRANS_ID = PB.PEND_TRANS_ID(+)
                          and pp.ferc_activity_code (+) = 2
                          and PT.ILR_ID = A_ILR_ID
                          and M1.LS_ASSET_ID = PT.LS_ASSET_ID
                        group by M1.ASSET_ID, PT.LS_PEND_TRANS_ID) P
                where C.ASSET_ID = P.ASSET_ID
                  and P.LS_PEND_TRANS_ID = PB.LS_PEND_TRANS_ID)
       where exists (select 1
                from LS_CPR_ASSET_MAP M, LS_PEND_TRANSACTION PT
               where M.LS_ASSET_ID = PT.LS_ASSET_ID
                 and PT.ILR_ID = A_ILR_ID
                 and PT.LS_PEND_TRANS_ID = PB.LS_PEND_TRANS_ID);

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      ILR      := A_ILR_ID;
      A_STATUS := 'dynmic update of basis bucket';
      SQLS     := 'UPDATE LS_PEND_BASIS B' || ' SET BASIS_' || TO_CHAR(RTN) || ' = ' || ' (' ||
                  ' SELECT B.BASIS_' || TO_CHAR(RTN) ||
                  '   + case when max(beg_capital_cost) = 0 then max(beg_obligation) else max(beg_capital_cost) end' ||
                  ' from' || ' (' ||
                  '  select pt.ls_pend_trans_id, las.beg_obligation, las.beg_capital_cost,' ||
                  '     row_number() over(partition by las.ls_asset_id, las.revision, las.set_of_books_id order by las.month) as the_row' ||
                  '  from ls_asset_schedule las, ls_pend_transaction pt' ||
                  '  where pt.ls_asset_id = las.ls_asset_id' || '  and las.revision = ' ||
                  TO_CHAR(A_REVISION) || '  and pt.ilr_id = ' || TO_CHAR(A_ILR_ID) || ' ) bbb' ||
                  ' where bbb.the_row = 1' || ' and bbb.ls_pend_trans_id = b.ls_pend_trans_id' ||
                  ' group by bbb.ls_pend_trans_id' || ' )' ||
                  ' where exists (select 1 from ls_pend_transaction d where d.ilr_id = ' ||
                  TO_CHAR(A_ILR_ID) || ' and b.ls_pend_trans_id = d.ls_pend_trans_id)';
      execute immediate SQLS;
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(SQLS);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'insert into ls_pend_class_code';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_CLASS_CODE
         (CLASS_CODE_ID, LS_PEND_TRANS_ID, value)
         select A.CLASS_CODE_ID, B.LS_PEND_TRANS_ID, A.VALUE
           from LS_ASSET_CLASS_CODE A, LS_PEND_TRANSACTION B
          where A.LS_ASSET_ID = B.LS_ASSET_ID
            and B.ILR_ID = A_ILR_ID;

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      -- insert into ls_pend_set_of_books
      A_STATUS := 'insert into ls_pend_set_of_books';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_SET_OF_BOOKS
         (LS_PEND_TRANS_ID, SET_OF_BOOKS_ID, POSTING_AMOUNT)
         select L.LS_PEND_TRANS_ID,
                S.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                S.BASIS_1_INDICATOR * PB.BASIS_1 + S.BASIS_2_INDICATOR * PB.BASIS_2 +
                S.BASIS_3_INDICATOR * PB.BASIS_3 + S.BASIS_4_INDICATOR * PB.BASIS_4 +
                S.BASIS_5_INDICATOR * PB.BASIS_5 + S.BASIS_6_INDICATOR * PB.BASIS_6 +
                S.BASIS_7_INDICATOR * PB.BASIS_7 + S.BASIS_8_INDICATOR * PB.BASIS_8 +
                S.BASIS_9_INDICATOR * PB.BASIS_9 + S.BASIS_10_INDICATOR * PB.BASIS_10 +
                S.BASIS_11_INDICATOR * PB.BASIS_11 + S.BASIS_12_INDICATOR * PB.BASIS_12 +
                S.BASIS_13_INDICATOR * PB.BASIS_13 + S.BASIS_14_INDICATOR * PB.BASIS_14 +
                S.BASIS_15_INDICATOR * PB.BASIS_15 + S.BASIS_16_INDICATOR * PB.BASIS_16 +
                S.BASIS_17_INDICATOR * PB.BASIS_17 + S.BASIS_18_INDICATOR * PB.BASIS_18 +
                S.BASIS_19_INDICATOR * PB.BASIS_19 + S.BASIS_20_INDICATOR * PB.BASIS_20 +
                S.BASIS_21_INDICATOR * PB.BASIS_21 + S.BASIS_22_INDICATOR * PB.BASIS_22 +
                S.BASIS_23_INDICATOR * PB.BASIS_23 + S.BASIS_24_INDICATOR * PB.BASIS_24 +
                S.BASIS_25_INDICATOR * PB.BASIS_25 + S.BASIS_26_INDICATOR * PB.BASIS_26 +
                S.BASIS_27_INDICATOR * PB.BASIS_27 + S.BASIS_28_INDICATOR * PB.BASIS_28 +
                S.BASIS_29_INDICATOR * PB.BASIS_29 + S.BASIS_30_INDICATOR * PB.BASIS_30 +
                S.BASIS_31_INDICATOR * PB.BASIS_31 + S.BASIS_32_INDICATOR * PB.BASIS_32 +
                S.BASIS_33_INDICATOR * PB.BASIS_33 + S.BASIS_34_INDICATOR * PB.BASIS_34 +
                S.BASIS_35_INDICATOR * PB.BASIS_35 + S.BASIS_36_INDICATOR * PB.BASIS_36 +
                S.BASIS_37_INDICATOR * PB.BASIS_37 + S.BASIS_38_INDICATOR * PB.BASIS_38 +
                S.BASIS_39_INDICATOR * PB.BASIS_39 + S.BASIS_40_INDICATOR * PB.BASIS_40 +
                S.BASIS_41_INDICATOR * PB.BASIS_41 + S.BASIS_42_INDICATOR * PB.BASIS_42 +
                S.BASIS_43_INDICATOR * PB.BASIS_43 + S.BASIS_44_INDICATOR * PB.BASIS_44 +
                S.BASIS_45_INDICATOR * PB.BASIS_45 + S.BASIS_46_INDICATOR * PB.BASIS_46 +
                S.BASIS_47_INDICATOR * PB.BASIS_47 + S.BASIS_48_INDICATOR * PB.BASIS_48 +
                S.BASIS_49_INDICATOR * PB.BASIS_49 + S.BASIS_50_INDICATOR * PB.BASIS_50 +
                S.BASIS_51_INDICATOR * PB.BASIS_51 + S.BASIS_52_INDICATOR * PB.BASIS_52 +
                S.BASIS_53_INDICATOR * PB.BASIS_53 + S.BASIS_54_INDICATOR * PB.BASIS_54 +
                S.BASIS_55_INDICATOR * PB.BASIS_55 + S.BASIS_56_INDICATOR * PB.BASIS_56 +
                S.BASIS_57_INDICATOR * PB.BASIS_57 + S.BASIS_58_INDICATOR * PB.BASIS_58 +
                S.BASIS_59_INDICATOR * PB.BASIS_59 + S.BASIS_60_INDICATOR * PB.BASIS_60 +
                S.BASIS_61_INDICATOR * PB.BASIS_61 + S.BASIS_62_INDICATOR * PB.BASIS_62 +
                S.BASIS_63_INDICATOR * PB.BASIS_63 + S.BASIS_64_INDICATOR * PB.BASIS_64 +
                S.BASIS_65_INDICATOR * PB.BASIS_65 + S.BASIS_66_INDICATOR * PB.BASIS_66 +
                S.BASIS_67_INDICATOR * PB.BASIS_67 + S.BASIS_68_INDICATOR * PB.BASIS_68 +
                S.BASIS_69_INDICATOR * PB.BASIS_69 + S.BASIS_70_INDICATOR * PB.BASIS_70 as AMOUNT
           from SET_OF_BOOKS S, COMPANY_SET_OF_BOOKS C, LS_PEND_BASIS PB, LS_PEND_TRANSACTION L
          where PB.LS_PEND_TRANS_ID = L.LS_PEND_TRANS_ID
            and C.COMPANY_ID = L.COMPANY_ID
            and S.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
            and L.ILR_ID = A_ILR_ID;
      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Setting posting amount on ls_pend_transaction';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_PEND_TRANSACTION PT
         set PT.POSTING_AMOUNT =
              (select S.POSTING_AMOUNT
                 from LS_PEND_SET_OF_BOOKS S
                where S.SET_OF_BOOKS_ID = 1
                  and S.LS_PEND_TRANS_ID = PT.LS_PEND_TRANS_ID)
       where ILR_ID = A_ILR_ID;

      --Update status of non-current revisions
      A_STATUS := 'update workflow';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update WORKFLOW
         set APPROVAL_STATUS_ID = 5
       where ID_FIELD1 = A_ILR_ID
         and ID_FIELD2 <> A_REVISION
         and APPROVAL_STATUS_ID = 2
         and SUBSYSTEM = 'ilr_approval';

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'update ls_ilr_approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 5
       where ILR_ID = A_ILR_ID
         and REVISION <> A_REVISION
         and APPROVAL_STATUS_ID = 2;

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

      return 1;
   exception
      when others then
         PKG_PP_LOG.P_WRITE_MESSAGE('Error Sending For Approval: ' || sqlerrm);

         return -1;
   end F_SEND_ILR_NO_COMMIT;

   function F_SEND_ILR(A_ILR_ID   in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      ILR      number;
      WFS      number;
      SQLS     varchar2(32000);
      L_STATUS varchar2(2000);
      IS_AUTO  WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin
      -- start by clearing our pend transaction
      RTN := F_SEND_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      --Call F_APPROVE_ILR for auto-approved assets
      L_STATUS := 'selecting if auto';
      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LS_ILR L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.ILR_ID = A_ILR_ID;

      if IS_AUTO = 'AUTO' then
         L_STATUS := 'sending for approval';
         RTN      := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
         if RTN <> 1 then
            rollback;
            RAISE_APPLICATION_ERROR(-20000, 'Error Approving: ' || L_STATUS);
            return -1;
         end if;
      end if;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending ILR: ' || L_STATUS);
         return -1;
   end F_SEND_ILR;

   --**************************************************************************
   --                            F_UNREJECT_ILR
   --**************************************************************************

   function F_UNREJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNREJECT_ILR;

   --**************************************************************************
   --                            F_UNSEND_ILR
   --**************************************************************************

   function F_UNSEND_ILR(A_ILR_ID   in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      L_STATUS varchar2(2000);
   begin
      RTN := F_DELETEPENDTRANS(A_ILR_ID, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Removing Transactions: ' || L_STATUS);
         return -1;
      end if;

      update LS_ILR_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L set ILR_STATUS_ID = 1 where ILR_ID = A_ILR_ID;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 2;

      --update workflow and ilr status

-- RO: This had to be removed to prevent deadlocks
--     Workflow gets updated in uo_workflow_tools
--
--      update WORKFLOW
--         set APPROVAL_STATUS_ID = 1
--       where ID_FIELD1 = A_ILR_ID
--         and ID_FIELD2 = A_REVISION
--         and APPROVAL_STATUS_ID = 2
--         and SUBSYSTEM = 'ilr_approval';

      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and APPROVAL_STATUS_ID = 2;

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNSEND_ILR;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_ILR
   --**************************************************************************

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_ILR_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_ILR_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'ilr_approval'),
                                     0)
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_ILR;

   function F_FLOATING_RATE_SF_ACCRUAL(A_COMPANY_ID in number,
                               A_MONTH      in date) return varchar2 is
      COUNTER  number;
      BUCKET   LS_RENT_BUCKET_ADMIN%ROWTYPE;
      SQLS     varchar2(10000);
      L_STATUS varchar2(10000);
      L_MAX    varchar2(8);
   begin
      /* Get the floating rates bucket */
      L_STATUS := 'Insert floating rate amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the floating rate bucket';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      /* Get maximum month */
      select to_char(max(month),'yyyymm')
      into L_MAX
      from ls_ilr_schedule
      where ilr_id in
         (select a.ilr_id from ls_ilr a, ls_lease l
         where l.lease_id = a.lease_id and l.lease_type_id = 3);

      /* Do asset accruals */
      sqls :=  'Update LS_ASSET_SCHEDULE LAS '||
               'set LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
                  '(select LAS2.BEG_OBLIGATION * ILR.RATE '||
                  'from LS_ASSET_SCHEDULE LAS2, LS_ILR_GROUP_FLOATING_RATES ILR, LS_ASSET LA, LS_ILR LI '||
                  'where LAS.LS_ASSET_ID = LAS2.LS_ASSET_ID and LAS.REVISION = LAS2.REVISION '||
                  'and LAS.SET_OF_BOOKS_ID = LAS2.SET_OF_BOOKS_ID and LAS.MONTH = LAS2.MONTH '||
                  'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LA.ILR_ID = LI.ILR_ID '||
                  'and LI.ILR_GROUP_ID = ILR.ILR_GROUP_ID '||
                  'and ILR.EFFECTIVE_DATE = '||
                     '(select max(ILR2.EFFECTIVE_DATE) from LS_ILR_GROUP_FLOATING_RATES ILR2 '||
                     'where to_number(to_char(ILR2.EFFECTIVE_DATE,''yyyymm'')) <= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
                     'and ILR2.ILR_GROUP_ID = ILR.ILR_GROUP_ID)) '||
               'where LAS.LS_ASSET_ID in '||
                  '(select LS_ASSET.LS_ASSET_ID from LS_ASSET, LS_ILR, LS_ILR_GROUP, LS_LEASE '||
                  'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                  'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                  'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                  'and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                  'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                  'and LS_LEASE.LEASE_TYPE_ID = 3) '||
               'and to_number(to_char(LAS.MONTH,''yyyymm'')) > to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
               'and to_number(to_char(LAS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

      L_STATUS := SQLS;
      execute immediate SQLS;

      /* Do ILR accruals */
      SQLS :=  'Update LS_ILR_SCHEDULE LIS '||
               'set LIS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
                  '(select sum(LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||') '||
                  'from LS_ASSET_SCHEDULE LAS, LS_ASSET LA '||
                  'where LAS.REVISION = LIS.REVISION and LA.ILR_ID = LIS.ILR_ID '||
                  'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LAS.MONTH = LIS.MONTH '||
                  'and LA.APPROVED_REVISION = LAS.REVISION '||
                  'and LAS.SET_OF_BOOKS_ID = LIS.SET_OF_BOOKS_ID '||
                  'and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LA.RETIREMENT_DATE,''yyyymm'')))) '||
                  'group by LA.ILR_ID, LAS.MONTH, LAS.REVISION, LAS.SET_OF_BOOKS_ID) '||
               'where LIS.ILR_id in '||
                  '(select LS_ILR.ILR_ID from LS_ILR, LS_ILR_GROUP, LS_LEASE '||
                  'where LS_ILR.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                  'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID '||
                  'and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                  'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                  'and LS_LEASE.LEASE_TYPE_ID = 3) '||
               'and to_number(to_char(LIS.MONTH,''yyyymm'')) > to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
               'and to_number(to_char(LIS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

      L_STATUS := SQLS;
      execute immediate SQLS;

      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_FLOATING_RATE_SF_ACCRUAL;

   function F_FLOATING_RATE_SF_PAYMENT(A_LEASE_ID in number, A_COMPANY_ID in number,
                               A_MONTH      in date) return varchar2 is
      COUNTER  number;
      BUCKET   LS_RENT_BUCKET_ADMIN%ROWTYPE;
      SQLS     varchar2(10000);
      L_STATUS varchar2(10000);
      L_MAX    varchar2(8);
   begin
      /* Get the floating rates bucket */
      L_STATUS := 'Insert floating rate amounts on schedule';

      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the floating rate bucket';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      /* Get maximum month */
      select to_char(max(month),'yyyymm')
      into L_MAX
      from ls_ilr_schedule
      where ilr_id in
         (select a.ilr_id from ls_ilr a, ls_lease l
         where l.lease_id = a.lease_id and l.lease_type_id = 3 and a.company_id = a_company_id
		 and case when A_LEASE_ID = -1 then -1 else l.lease_id end = A_LEASE_ID);

      /* Do asset payments */
      SQLS :=  'Update (select LAS.*, LAST.ILR_ID, LAST.APPROVED_REVISION '||
               '  from LS_ASSET_SCHEDULE LAS, LS_ASSET LAST where LAS.LS_ASSET_ID = LAST.LS_ASSET_ID) LAS '||
               'set LAS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
                  '(select nvl(sum(LAS2.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||') '||
               'over(partition by LAS2.LS_ASSET_ID, LAS2.SET_OF_BOOKS_ID, LAS2.REVISION),0) '||
                  'from LS_ASSET_SCHEDULE LAS2 '||
                  'where LAS.LS_ASSET_ID = LAS2.LS_ASSET_ID and LAS.SET_OF_BOOKS_ID = LAS2.SET_OF_BOOKS_ID '||
                  'and LAS2.MONTH = LAS.MONTH and LAS.REVISION = LAS2.REVISION) '||
                  'where LAS.LS_ASSET_ID in (select LS_ASSET.LS_ASSET_ID from LS_ASSET, LS_ILR, LS_ILR_GROUP, LS_LEASE '||
                              'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                              'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                              'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                              ' and case when '||to_char(A_LEASE_ID) ||' = -1 then -1 else ls_ilr.lease_id end = '||to_char(A_LEASE_ID)||
							  ' and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                              'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                              'and LS_LEASE.LEASE_TYPE_ID = 3) '||
                  'and to_number(to_char(LAS.MONTH,''yyyymm'')) > to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
                  'and to_number(to_char(LAS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

      L_STATUS := SQLS;
      execute immediate SQLS;

      /* Do ILR payments */
      SQLS :=  'Update LS_ILR_SCHEDULE LIS '||
               'set LIS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
                  '(select sum(LAS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||') '||
                  'from LS_ASSET_SCHEDULE LAS, LS_ASSET LA '||
                  'where LAS.REVISION = LIS.REVISION and LA.ILR_ID = LIS.ILR_ID '||
                  'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LAS.MONTH = LIS.MONTH '||
                  'and LA.APPROVED_REVISION = LAS.REVISION '||
                  'and LAS.SET_OF_BOOKS_ID = LIS.SET_OF_BOOKS_ID '||
                  'and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LA.RETIREMENT_DATE,''yyyymm'')))) '||
                  'group by LA.ILR_ID, LAS.MONTH, LAS.REVISION, LAS.SET_OF_BOOKS_ID) '||
               'where LIS.ILR_id in '||
                  '(select LS_ILR.ILR_ID from LS_ILR, LS_ILR_GROUP, LS_LEASE '||
                  'where LS_ILR.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                  ' and case when '||to_char(A_LEASE_ID) ||' = -1 then -1 else ls_ilr.lease_id end = '||to_char(A_LEASE_ID)||
				  ' and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID '||
                  'and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                  'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                  'and LS_LEASE.LEASE_TYPE_ID = 3) '||
               'and to_number(to_char(LIS.MONTH,''yyyymm'')) > to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
               'and to_number(to_char(LIS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

      L_STATUS := SQLS;
      execute immediate SQLS;

      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_FLOATING_RATE_SF_PAYMENT;

   function F_FLOATING_RATE_ACCRUAL(A_COMPANY_ID  in number,
                     A_MONTH        in date) return varchar2 is
      counter number;
      bucket   LS_RENT_BUCKET_ADMIN%ROWTYPE;
      sqls  varchar2(10000);
      L_STATUS      varchar2(10000);

   begin
      --get the floating rates bucket
      L_STATUS := 'Insert floating rate amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the floating rate bucket';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      sqls :=  'Update LS_ASSET_SCHEDULE LAS '||
               'set LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
               '(select LAS2.BEG_OBLIGATION * ILR.RATE '||
               'from LS_ASSET_SCHEDULE LAS2, LS_ILR_GROUP_FLOATING_RATES ILR, LS_ASSET LA, LS_ILR LI '||
               'where LAS.LS_ASSET_ID = LAS2.LS_ASSET_ID and LAS.REVISION = LAS2.REVISION '||
               'and LAS.SET_OF_BOOKS_ID = LAS2.SET_OF_BOOKS_ID and LAS.MONTH = LAS2.MONTH '||
               'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LA.ILR_ID = LI.ILR_ID '||
               'and LI.ILR_GROUP_ID = ILR.ILR_GROUP_ID '||
               'and ILR.EFFECTIVE_DATE =  (select max(ILR2.EFFECTIVE_DATE) from LS_ILR_GROUP_FLOATING_RATES ILR2 '||
                                    'where to_number(to_char(ILR2.EFFECTIVE_DATE,''yyyymm'')) <= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
                                    'and ILR2.ILR_GROUP_ID = ILR.ILR_GROUP_ID)) '||
               'where LAS.LS_ASSET_ID in (select LS_ASSET.LS_ASSET_ID from LS_ASSET, LS_ILR, LS_ILR_GROUP '||
                           'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                           'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                           'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                           'and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
               'and to_char(LAS.MONTH,''yyyymm'') = '''||to_char(A_MONTH,'yyyymm')||'''';

      L_STATUS := SQLS;
      execute immediate SQLS;

      SQLS := 'Update LS_ILR_SCHEDULE LIS '||
               'set LIS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
               '(select sum(LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||') '||
               'from LS_ASSET_SCHEDULE LAS, LS_ASSET LA '||
               'where LAS.REVISION = LIS.REVISION and LA.ILR_ID = LIS.ILR_ID '||
               'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LAS.MONTH = LIS.MONTH '||
               'and LA.APPROVED_REVISION = LIS.REVISION '||
               'and to_char(LIS.MONTH,''yyyymm'') = '''||to_char(A_MONTH,'yyyymm')||''' and LAS.SET_OF_BOOKS_ID = LIS.SET_OF_BOOKS_ID '||
               'and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LA.RETIREMENT_DATE,''yyyymm'')))) '||
               'group by LA.ILR_ID, LAS.MONTH, LAS.REVISION, LAS.SET_OF_BOOKS_ID) '||
            'where LIS.ILR_id in (select LS_ILR.ILR_ID from LS_ILR, LS_ILR_GROUP '||
                           'where LS_ILR.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                           'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID '||
                           'and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
            'and to_char(LIS.MONTH,''yyyymm'') = '''||to_char(A_MONTH,'yyyymm')||'''';


      L_STATUS := SQLS;
      execute immediate SQLS;

      return F_FLOATING_RATE_SF_ACCRUAL(A_COMPANY_ID, A_MONTH);
   exception
      when others then
         return L_STATUS;
   end F_FLOATING_RATE_ACCRUAL;

   function F_FLOATING_RATE_PAYMENT(A_LEASE_ID in number, A_COMPANY_ID  in number,
                     A_MONTH        in date) return varchar2 is
      counter number;
      bucket   LS_RENT_BUCKET_ADMIN%ROWTYPE;
      sqls  varchar2(10000);
      L_STATUS      varchar2(10000);

   begin
      L_STATUS := F_FLOATING_RATE_ACCRUAL(A_COMPANY_ID, A_MONTH);

      --get the floating rates bucket
      L_STATUS := 'Insert floating rate amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the floating rate bucket';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      sqls :=  'Update (select LAS.*, LAST.ILR_ID, LAST.APPROVED_REVISION '||
            '  from LS_ASSET_SCHEDULE LAS, LS_ASSET LAST where LAS.LS_ASSET_ID = LAST.LS_ASSET_ID) LAS '||
            'set LAS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
               '(select nvl(sum(LAS2.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||') '||
            'over(partition by LAS2.LS_ASSET_ID, LAS2.SET_OF_BOOKS_ID, LAS2.REVISION),0) '||
               'from LS_ASSET_SCHEDULE LAS2 '||
               'where LAS.LS_ASSET_ID = LAS2.LS_ASSET_ID and LAS.SET_OF_BOOKS_ID = LAS2.SET_OF_BOOKS_ID '||
               'and LAS2.MONTH = LAS.MONTH and LAS.REVISION = LAS2.REVISION '||
               'and to_char(LAS.MONTH,''yyyymm'') = '''||to_char(A_MONTH,'yyyymm')||''') '||
               'where LAS.LS_ASSET_ID in (select LS_ASSET.LS_ASSET_ID from LS_ASSET, LS_ILR, LS_ILR_GROUP '||
                           'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                           'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                           'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                           ' and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||
						   ' and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
               'and to_char(LAS.MONTH,''yyyymm'') = '''||to_char(A_MONTH,'yyyymm')||''''||
               'and PKG_LEASE_CALC.F_PAYMENT_MONTH(LAS.ILR_ID, LAS.APPROVED_REVISION, to_date('''||to_char(A_MONTH,'yyyymm')||''',''yyyymm'')) = 1';

      L_STATUS := SQLS;
      execute immediate SQLS;

      SQLS := 'Update LS_ILR_SCHEDULE LIS '||
               'set LIS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
               '(select sum(LAS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||') '||
               'from LS_ASSET_SCHEDULE LAS, LS_ASSET LA '||
               'where LAS.REVISION = LIS.REVISION and LA.ILR_ID = LIS.ILR_ID '||
               'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LAS.MONTH = LIS.MONTH '||
               'and LA.APPROVED_REVISION = LIS.REVISION '||
               'and to_char(LIS.MONTH) = '''||to_char(A_MONTH)||''' and LAS.SET_OF_BOOKS_ID = LIS.SET_OF_BOOKS_ID '||
               'and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LA.RETIREMENT_DATE,''yyyymm'')))) '||
               'group by LA.ILR_ID, LAS.MONTH, LAS.REVISION, LAS.SET_OF_BOOKS_ID) '||
            'where LIS.ILR_id in (select LS_ILR.ILR_ID from LS_ILR, LS_ILR_GROUP '||
                           'where LS_ILR.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
						   ' and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||
                           ' and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID '||
                           'and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
            'and to_char(LIS.MONTH,''yyyymm'') = '''||to_char(A_MONTH,'yyyymm')||'''';


      L_STATUS := SQLS;
      execute immediate SQLS;

      return F_FLOATING_RATE_SF_PAYMENT(A_LEASE_ID, A_COMPANY_ID, A_MONTH);
   exception
      when others then
         return L_STATUS;
   end F_FLOATING_RATE_PAYMENT;

   --**************************************************************************
   --                            F_ACCRUALS_CALC
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into ls_monthly_accrual_stg
   --  This function uses merge statements, so it can be run multiple times.
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_ACCRUALS_CALC(A_COMPANY_ID in number,
                            A_MONTH      in date) return varchar2 is
      L_STATUS      varchar2(2000);
      L_ASSET_SCH   ASSET_SCHEDULE_LINE_TYPE;
      SCHEDULEINDEX number;
      EXECACCRUAL   number;
      CONTACCRUAL   number;
   begin

	 L_STATUS := F_TAX_CALC_ACCRUALS(A_COMPANY_ID, A_MONTH);
	 if L_STATUS <> 'OK' then
		return L_STATUS;
	 end if;

     L_STATUS := 'Handling floating rates';
     L_STATUS := F_FLOATING_RATE_ACCRUAL(A_COMPANY_ID, A_MONTH);
     if L_STATUS <> 'OK' then
      return L_STATUS;
     end if;

      L_STATUS := 'Bulk collecting';
      select S.* bulk collect
        into L_ASSET_SCH
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and S.MONTH = A_MONTH
         and (A.LS_ASSET_STATUS_ID = 3 or (A.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= A.RETIREMENT_DATE));

      L_STATUS := 'Deleting retired assets from staging table';
      delete from LS_MONTHLY_ACCRUAL_STG STG
       where STG.GL_POSTING_MO_YR = A_MONTH
         and STG.LS_ASSET_ID in (select LS_ASSET_ID from LS_ASSET
                                 where LS_ASSET_STATUS_ID = 4 and A_MONTH > RETIREMENT_DATE);

      L_STATUS := 'Inserting interest accrual';
      forall SCHEDULEINDEX in indices of L_ASSET_SCH
       merge into LS_MONTHLY_ACCRUAL_STG stg
       using (select 0 as accrual_id,
               2 as accrual_type_id,
               L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID as ls_asset_id,
               L_ASSET_SCH(SCHEDULEINDEX).INTEREST_ACCRUAL as amount,
                    A_MONTH as gl_posting_mo_yr,
                    L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
               from DUAL
            where L_ASSET_SCH(SCHEDULEINDEX).INTEREST_ACCRUAL <> 0) b
       on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
            and b.LS_ASSET_ID = stg.LS_ASSET_ID
            and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
            and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
       when matched then update set stg.AMOUNT = b.AMOUNT
       when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
         values (LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.LS_ASSET_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);

      L_STATUS := 'Inserting executory accrual';
      PKG_LEASE_BUCKET.P_CALC_EXEC_ACCRUALS(L_ASSET_SCH);

      L_STATUS := 'Inserting contingent accrual';
      PKG_LEASE_BUCKET.P_CALC_CONT_ACCRUALS(L_ASSET_SCH);

      return 'OK';
   exception
      when others then
         return L_STATUS||sqlerrm;
   end F_ACCRUALS_CALC;

   --**************************************************************************
   --                            F_ACCRUALS_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve and post the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_ACCRUALS_APPROVE(A_COMPANY_ID in number,
                               A_MONTH      in date) return varchar2 is
      L_STATUS varchar2(2000);
      L_RTN    number;
      L_GL_JE_CODE   varchar2(35);
	  BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
	  COUNTER number;
	  TAX_ACCRUAL_TYPE number;
	  L_ACCTS PKG_LEASE_BUCKET.BUCKET_ACCTS;
   begin
	  L_STATUS := 'Getting GL JE Code';
      select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMACC'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAMACC';

      for L_ACCRUALS in (select H.LS_ASSET_ID as LS_ASSET_ID,
                                H.AMOUNT as AMOUNT,
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                3010 TRANS_TYPE,
                                H.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                I.INT_ACCRUAL_ACCOUNT_ID as INT_ACCRUAL_ACCOUNT,
                                I.INT_EXPENSE_ACCOUNT_ID as INT_EXPENSE_ACCOUNT
                           from LS_MONTHLY_ACCRUAL_STG H, LS_ASSET A, LS_ILR_ACCOUNT I
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.AMOUNT <> 0
                            and H.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.ILR_ID = I.ILR_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and H.ACCRUAL_TYPE_ID = 2)
      loop
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_ACCRUALS.TRANS_TYPE);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCRUALS.INT_EXPENSE_ACCOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_ACCRUALS.SET_OF_BOOKS_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_ACCRUALS.TRANS_TYPE + 1);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE + 1,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCRUALS.INT_ACCRUAL_ACCOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_ACCRUALS.SET_OF_BOOKS_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
      end loop;

	  --get the tax bucket
	  L_STATUS := 'Insert tax amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
      end if;

      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET = 1;

	  TAX_ACCRUAL_TYPE := PKG_LEASE_BUCKET.F_GET_TAX_TYPE(BUCKET.RENT_TYPE,BUCKET.BUCKET_NUMBER);

	  for L_ACCRUALS in (select H.LS_ASSET_ID as LS_ASSET_ID,
                                sum(LMT.AMOUNT) as AMOUNT, --Note that this is the amount off the monthly tax table, not the accrual table
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                3040 TRANS_TYPE,
                                H.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                LTL.ACCRUAL_ACCT_ID as ACCRUAL_ACCOUNT,
								LTL.EXPENSE_ACCT_ID as EXPENSE_ACCOUNT
                           from LS_MONTHLY_ACCRUAL_STG H, LS_ASSET A, LS_TAX_LOCAL LTL, LS_MONTHLY_TAX LMT
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.AMOUNT <> 0
                            and H.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and H.ACCRUAL_TYPE_ID = TAX_ACCRUAL_TYPE
							and LTL.TAX_LOCAL_ID = LMT.TAX_LOCAL_ID
							and LMT.ACCRUAL = 1
							and LMT.LS_ASSET_ID = H.LS_ASSET_ID
							and LMT.GL_POSTING_MO_YR = H.GL_POSTING_MO_YR
							and LMT.SET_OF_BOOKS_ID = H.SET_OF_BOOKS_ID
							group by h.ls_asset_id, a.work_order_id, a.company_id, a.in_service_date, 
								h.set_of_books_id, ltl.accrual_acct_id, ltl.expense_acct_id	
							)
      loop
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_ACCRUALS.TRANS_TYPE);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCRUALS.EXPENSE_ACCOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_ACCRUALS.SET_OF_BOOKS_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_ACCRUALS.TRANS_TYPE + 1);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE + 1,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCRUALS.ACCRUAL_ACCOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_ACCRUALS.SET_OF_BOOKS_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
      end loop;

	  L_ACCTS := PKG_LEASE_BUCKET.F_GET_ACCTS;

	  for L_ACCRUALS in (select H.LS_ASSET_ID as LS_ASSET_ID,
                                H.AMOUNT as AMOUNT,
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                case
                                   when H.ACCRUAL_TYPE_ID between 3 and 12 then
                                    3012 --executory
                                   when H.ACCRUAL_TYPE_ID between 13 and 22 then
                                    3014 --contingent
                                end as TRANS_TYPE,
                                H.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
								H.ACCRUAL_TYPE_ID as ACCRUAL_TYPE_ID
                           from LS_MONTHLY_ACCRUAL_STG H, LS_ASSET A
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.AMOUNT <> 0
                            and H.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and H.ACCRUAL_TYPE_ID between 3 and 22)
      loop

		 --we already did the taxes so skip em here
		 if L_ACCRUALS.ACCRUAL_TYPE_ID = TAX_ACCRUAL_TYPE then
			continue;
		end if;

         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_ACCRUALS.TRANS_TYPE);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCTS(L_ACCRUALS.ACCRUAL_TYPE_ID).EXPENSE_ACCT_ID,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_ACCRUALS.SET_OF_BOOKS_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_ACCRUALS.TRANS_TYPE + 1);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE + 1,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCTS(L_ACCRUALS.ACCRUAL_TYPE_ID).ACCRUAL_ACCT_ID,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_ACCRUALS.SET_OF_BOOKS_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
      end loop;

      return 'OK';
   exception
      when others then
         return L_STATUS||sqlerrm;
   end F_ACCRUALS_APPROVE;

   --**************************************************************************
   --                            F_PAYMENT_CALC
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_PAYMENT_CALC(A_LEASE_ID in number, A_COMPANY_ID in number,
                           A_MONTH      in date) return varchar2 is

      L_STATUS                varchar2(2000);
      L_RTN                   number;
      L_ASSET_SCHEDULE_LINE   ASSET_SCHEDULE_LINE_TABLE;
      L_ASSET_SCHEDULE_HEADER ASSET_SCHEDULE_HEADER_TABLE;
      SCHEDULEINDEX           number;

   begin

	  L_STATUS := F_TAX_CALC_PAYMENTS(A_LEASE_ID, A_COMPANY_ID, A_MONTH);
	  if L_STATUS <> 'OK' then
		return L_STATUS;
	  end if;

     L_STATUS := 'Handling floating rates';
     L_STATUS := F_FLOATING_RATE_PAYMENT(A_LEASE_ID, A_COMPANY_ID, A_MONTH);
     if L_STATUS <> 'OK' then
      return L_STATUS;
     end if;

      L_STATUS := 'Deleting';
      delete from LS_PAYMENT_APPROVAL
       where PAYMENT_ID in
	   (
			select PAYMENT_ID
			from LS_PAYMENT_HDR
			where GL_POSTING_MO_YR = A_MONTH
			and COMPANY_ID = A_COMPANY_ID
			and case when a_lease_id = -1 then -1 else LEASE_ID end = A_LEASE_ID
         and PAYMENT_STATUS_ID < 3
		);

      --Cascades to LS_INVOICE_PAYMENT_MAP and LS_PAYMENT_LINE
		delete from LS_PAYMENT_HDR
		where GL_POSTING_MO_YR = A_MONTH
		and COMPANY_ID = A_COMPANY_ID
		and case when a_lease_id = -1 then -1 else LEASE_ID end = A_LEASE_ID
      and PAYMENT_STATUS_ID < 3;

      --delete retired assets from ls_payment_line
      delete from LS_PAYMENT_LINE
      where GL_POSTING_MO_YR = A_MONTH
      and LS_ASSET_ID in (select LS_ASSET_ID from LS_ASSET
                          where LS_ASSET_STATUS_ID = 4 and A_MONTH > RETIREMENT_DATE);

		--insert empty headers. One header for each {lease,vendor} combo for this company
      select distinct L.LEASE_ID,
				case when LO.ls_reconcile_type_id in (1, 2) then i.ilr_id else -1 end,
				case when LO.ls_reconcile_type_id = 1 then a.ls_asset_id else -1 end,
				LV.VENDOR_ID
		bulk collect
        into L_ASSET_SCHEDULE_HEADER
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I, LS_LEASE L, LS_LEASE_VENDOR LV,
            LS_ILR_OPTIONS O, LS_LEASE_OPTIONS LO
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and I.LEASE_ID = L.LEASE_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and LV.LEASE_ID = L.LEASE_ID
         and LV.COMPANY_ID = I.COMPANY_ID
		 and I.ILR_ID = O.ILR_ID
		 and S.REVISION = O.REVISION
		 and L.lease_id = LO.lease_id
		 and L.current_revision = LO.revision
       and (A.LS_ASSET_STATUS_ID = 3 or (A.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= A.RETIREMENT_DATE))
       and not exists
       (
         select 1
         from ls_payment_hdr ph
         where ph.gl_posting_mo_yr = a_month
         and l.lease_id = ph.lease_id
         and i.company_id = ph.company_id
         and lv.vendor_id = ph.vendor_id
         and case when LO.ls_reconcile_type_id in (1, 2) then i.ilr_id else -1 end = ph.ilr_id
			and case when LO.ls_reconcile_type_id = 1 then a.ls_asset_id else -1 end = ph.ls_asset_id
       )
		 and case when a_lease_id = -1 then -1 else l.lease_id end = A_LEASE_ID
         and S.MONTH = add_months(A_MONTH, nvl(O.PAYMENT_SHIFT,0));

      select S.LS_ASSET_ID,
             S.INTEREST_PAID,
             S.PRINCIPAL_PAID,
             L.LEASE_ID,
             LV.VENDOR_ID,
             LV.PAYMENT_PCT,
             EXECUTORY_PAID1,
		     EXECUTORY_PAID2,
		     EXECUTORY_PAID3,
		     EXECUTORY_PAID4,
		     EXECUTORY_PAID5,
		     EXECUTORY_PAID6,
		     EXECUTORY_PAID7,
		     EXECUTORY_PAID8,
		     EXECUTORY_PAID9,
		     EXECUTORY_PAID10,
		     CONTINGENT_PAID1,
		     CONTINGENT_PAID2,
		     CONTINGENT_PAID3,
		     CONTINGENT_PAID4,
		     CONTINGENT_PAID5,
		     CONTINGENT_PAID6,
		     CONTINGENT_PAID7,
		     CONTINGENT_PAID8,
		     CONTINGENT_PAID9,
		     CONTINGENT_PAID10,
             ROW_NUMBER() OVER(partition by L.LEASE_ID, case when LO.ls_reconcile_type_id in (1, 2) then I.ILR_ID else -1 end,
											case when LO.ls_reconcile_type_id = 1 then a.ls_asset_id else -1 end,
											LV.VENDOR_ID, S.SET_OF_BOOKS_ID order by S.LS_ASSET_ID),
             S.SET_OF_BOOKS_ID,
			 A_COMPANY_ID,
			 A_MONTH, case when LO.ls_reconcile_type_id in (1, 2) then I.ILR_ID else -1 end, lo.ls_reconcile_type_id
			 bulk collect
        into L_ASSET_SCHEDULE_LINE
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I, LS_LEASE L, LS_LEASE_VENDOR LV,
         LS_ILR_OPTIONS O, LS_LEASE_OPTIONS LO
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and I.LEASE_ID = L.LEASE_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and LV.LEASE_ID = L.LEASE_ID
         and LV.COMPANY_ID = I.COMPANY_ID
         and O.ILR_ID = I.ILR_ID
         and O.REVISION = I.CURRENT_REVISION
		 and L.lease_id = LO.lease_id
		 and L.current_revision = LO.revision
		 and case when a_lease_id = -1 then -1 else l.lease_id end = A_LEASE_ID
         and S.MONTH = add_months(A_MONTH, nvl(O.PAYMENT_SHIFT,0))
         and (A.LS_ASSET_STATUS_ID = 3 or (A.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= A.RETIREMENT_DATE))
         and not exists
       (
         select 1
         from ls_payment_hdr ph
         where ph.gl_posting_mo_yr = a_month
         and l.lease_id = ph.lease_id
         and i.company_id = ph.company_id
         and lv.vendor_id = ph.vendor_id
         and case when LO.ls_reconcile_type_id in (1, 2) then i.ilr_id else -1 end = ph.ilr_id
			and case when LO.ls_reconcile_type_id = 1 then a.ls_asset_id else -1 end = ph.ls_asset_id
       );

      L_STATUS := 'Inserting dummy headers';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_HEADER
         insert into LS_PAYMENT_HDR
            (PAYMENT_ID, LEASE_ID, VENDOR_ID, COMPANY_ID, GL_POSTING_MO_YR, PAYMENT_STATUS_ID, AP_STATUS_ID, ILR_ID, LS_ASSET_ID)
            select LS_PAYMENT_HDR_SEQ.NEXTVAL,
                   L_ASSET_SCHEDULE_HEADER   (SCHEDULEINDEX).LEASE_ID,
                   L_ASSET_SCHEDULE_HEADER   (SCHEDULEINDEX).VENDOR_ID,
                   A_COMPANY_ID,
                   A_MONTH,
                   1,
               1, L_ASSET_SCHEDULE_HEADER(SCHEDULEINDEX).ILR_ID, L_ASSET_SCHEDULE_HEADER(SCHEDULEINDEX).LS_ASSET_ID
              from DUAL;

      --Insert into approval tables
      L_STATUS := 'Inserting into LS_PAYMENT_APPROVAL';
      insert into LS_PAYMENT_APPROVAL
         (PAYMENT_ID, TIME_STAMP, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID, APPROVER, APPROVAL_DATE,
          REJECTED)
         select PAYMENT_ID,
                sysdate,
                (select distinct DECODE(LG.PYMT_APPROVAL_FLAG, 1, 11, 4)
                   from LS_LEASE_GROUP LG, LS_LEASE L
                  where LG.LEASE_GROUP_ID = L.LEASE_GROUP_ID
                    and L.LEASE_ID = LS_PAYMENT_HDR.LEASE_ID),
                1,
                null,
                null,
                null
           from LS_PAYMENT_HDR
          where GL_POSTING_MO_YR = A_MONTH
            and COMPANY_ID = A_COMPANY_ID
			and case when a_lease_id = -1 then -1 else lease_id end = A_LEASE_ID
         and PAYMENT_STATUS_ID < 3;

      L_STATUS := 'Principal';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    1,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PRINCIPAL_PAID * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT,
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
				    and ILR_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID in (1,2) then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ILR_ID else -1 end
                and LS_ASSET_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID = 1 then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID else -1 end
				    and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PRINCIPAL_PAID <> 0
                and PAYMENT_STATUS_ID < 3);

      L_STATUS := 'Interest';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    2,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    (L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).INTEREST_PAID * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT) +
                        ((select nvl(sum(nvl(II.AMOUNT, 0)), 0) from LS_COMPONENT_MONTHLY_II_STG II, LS_COMPONENT C
                        where II.MONTH = A_MONTH
                        and II.COMPONENT_ID = C.COMPONENT_ID
                        and C.LS_ASSET_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID
                        and C.COMPANY_ID = A_COMPANY_ID) * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT),
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
				and ILR_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID in (1,2)
							then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ILR_ID else -1 end
                and LS_ASSET_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID = 1
							then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID else -1 end
                and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and (L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).INTEREST_PAID * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT) +
                    ((select nvl(sum(nvl(II.AMOUNT, 0)), 0) from LS_COMPONENT_MONTHLY_II_STG II, LS_COMPONENT C
                    where II.MONTH = A_MONTH
                    and II.COMPONENT_ID = C.COMPONENT_ID
                    and C.LS_ASSET_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID
                    and C.COMPANY_ID = A_COMPANY_ID) * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT) <> 0);

      L_STATUS := 'Inserting executory payment lines';
      PKG_LEASE_BUCKET.P_CALC_EXEC_PAYMENTS(L_ASSET_SCHEDULE_LINE);

      L_STATUS := 'Inserting contingent payment lines';
      PKG_LEASE_BUCKET.P_CALC_CONT_PAYMENTS(L_ASSET_SCHEDULE_LINE);

      L_STATUS := 'Looping complete';
      P_PAYMENT_ROLLUP( A_LEASE_ID, A_COMPANY_ID, A_MONTH );
      L_STATUS := 'Roll up Complete';

      L_STATUS := 'Delete the payments with $0';
      delete from LS_PAYMENT_APPROVAL
       where PAYMENT_ID in (select PAYMENT_ID
                              from LS_PAYMENT_HDR
                             where AMOUNT = 0
                               and COMPANY_ID = A_COMPANY_ID
                               and GL_POSTING_MO_YR = A_MONTH
							   and case when a_lease_id = -1 then -1 else lease_id end = A_LEASE_ID
                        and PAYMENT_STATUS_ID < 3);

      delete from LS_PAYMENT_HDR
       where AMOUNT = 0
         and COMPANY_ID = A_COMPANY_ID
         and GL_POSTING_MO_YR = A_MONTH
		 and case when a_lease_id = -1 then -1 else lease_id end = A_LEASE_ID
       and PAYMENT_STATUS_ID < 3;

      L_STATUS := 'Generating invoices for auto MLAs';
      P_AUTO_GENERATE_INVOICES(A_LEASE_ID, A_COMPANY_ID, A_MONTH);

      if A_LEASE_ID = -1 then
		L_STATUS := 'Matching Invoices';
         return F_MATCH_INVOICES(A_MONTH);
      end if;

      return 'OK';
   exception
      when others then
         return L_STATUS || sqlerrm;
   end F_PAYMENT_CALC;


   -- wrapper function.  Pass in -1 as lease id meaning all leases for the company
   function F_PAYMENT_CALC(A_COMPANY_ID in number,
                           A_MONTH      in date) return varchar2 is
   begin
		return F_PAYMENT_CALC( -1, a_company_id, a_month);
   exception
      when others then
         return sqlerrm;
   end F_PAYMENT_CALC;

   --**************************************************************************
   --                            F_PAYMENT_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve and post the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_PAYMENT_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date) return varchar2 is
      L_STATUS varchar2(2000);
      L_RTN    number;
      L_COUNTER number;
      L_GL_JE_CODE   varchar2(35);
	  BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
	  COUNTER number;
	  TAX_PAYMENT_TYPE number;
	  L_ACCTS PKG_LEASE_BUCKET.BUCKET_ACCTS;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      L_STATUS := 'Sending payment JEs.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

	  L_COUNTER := F_PAYMENT_II(A_COMPANY_ID, A_MONTH);

     select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMPAY'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAMPAY';


      for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
                                H.VENDOR_ID as VENDOR_ID,
                                L.AMOUNT as PAYMENT_AMOUNT,
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                M.INVOICE_ID as INVOICE_ID,
                                case
                                   when L.PAYMENT_TYPE_ID = 1 then
                                    3018
                                   when L.PAYMENT_TYPE_ID in (2) then
                                    3019
                                end as TRANS_TYPE,
                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                I.INT_ACCRUAL_ACCOUNT_ID as INT_ACCRUAL_ACCOUNT,
								I.AP_ACCOUNT_ID as AP_ACCOUNT_ID
                           from LS_PAYMENT_HDR H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I, LS_PAYMENT_APPROVAL LPA
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.PAYMENT_ID = L.PAYMENT_ID
                            and H.PAYMENT_ID = M.PAYMENT_ID
                            and L.AMOUNT <> 0
                            and L.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.ILR_ID = I.ILR_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and L.PAYMENT_TYPE_ID in (1,2)
                     and H.PAYMENT_ID = LPA.PAYMENT_ID
                     and nvl(LPA.REJECTED,0) <> 1
                     and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select APPROVAL_TYPE_ID
                                                               from WORKFLOW_TYPE
                                                               where lower(description) = 'auto approve')
                        and LPA.APPROVAL_STATUS_ID  = 1)))
      loop
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_PAYMENTS.TRANS_TYPE);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               case
												   when L_PAYMENTS.TRANS_TYPE in (3018) then
													  -1
												   when L_PAYMENTS.TRANS_TYPE = 3019 then
													  L_PAYMENTS.INT_ACCRUAL_ACCOUNT
												end,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit.  Payment credits all hit 3022
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                     ' trans type: 3022';
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               3022,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_PAYMENTS.AP_ACCOUNT_ID,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
       L_COUNTER := L_COUNTER + 1;
      end loop;

	  --TAXES
	  --get the tax bucket
	  L_STATUS := 'Insert tax amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
      end if;

      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET = 1;

	  TAX_PAYMENT_TYPE := PKG_LEASE_BUCKET.F_GET_TAX_TYPE(BUCKET.RENT_TYPE,BUCKET.BUCKET_NUMBER);

	  for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
                                H.VENDOR_ID as VENDOR_ID,
                                LMT.AMOUNT as PAYMENT_AMOUNT, --Note that this is off the monthly tax table and not payment lines
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                M.INVOICE_ID as INVOICE_ID,
                                3040 as TRANS_TYPE,
                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                LTL.ACCRUAL_ACCT_ID as ACCRUAL_ACCOUNT,
								I.AP_ACCOUNT_ID as AP_ACCOUNT_ID
                           from LS_PAYMENT_HDR H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I,
								LS_PAYMENT_APPROVAL LPA, LS_TAX_LOCAL LTL, LS_MONTHLY_TAX LMT
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.PAYMENT_ID = L.PAYMENT_ID
                            and H.PAYMENT_ID = M.PAYMENT_ID
							and h.vendor_id = lmt.vendor_id
                            and L.AMOUNT <> 0
                            and L.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.ILR_ID = I.ILR_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and L.PAYMENT_TYPE_ID = TAX_PAYMENT_TYPE
							and LTL.TAX_LOCAL_ID = LMT.TAX_LOCAL_ID
							and LMT.ACCRUAL = 0
							and LMT.LS_ASSET_ID = L.LS_ASSET_ID
							and LMT.GL_POSTING_MO_YR = L.GL_POSTING_MO_YR
							and LMT.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
							and H.PAYMENT_ID = LPA.PAYMENT_ID
							and nvl(LPA.REJECTED,0) <> 1
							and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select APPROVAL_TYPE_ID
																	   from WORKFLOW_TYPE
																	   where lower(description) = 'auto approve')
															and LPA.APPROVAL_STATUS_ID  = 1)))
      loop
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_PAYMENTS.TRANS_TYPE);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_PAYMENTS.ACCRUAL_ACCOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit.  Payment credits all hit 3022
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                     ' trans type: 3041';
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE + 1,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_PAYMENTS.AP_ACCOUNT_ID,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
       L_COUNTER := L_COUNTER + 1;
      end loop;

	  L_ACCTS := PKG_LEASE_BUCKET.F_GET_ACCTS;

	  for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
                                H.VENDOR_ID as VENDOR_ID,
                                L.AMOUNT as PAYMENT_AMOUNT,
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                M.INVOICE_ID as INVOICE_ID,
                                case
                                   when L.PAYMENT_TYPE_ID between 3 and 12 then
                                    3020
                                   when L.PAYMENT_TYPE_ID between 13 and 22 then
                                    3021
                                end as TRANS_TYPE,
                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
								I.AP_ACCOUNT_ID as AP_ACCOUNT_ID,
								L.PAYMENT_TYPE_ID as PAYMENT_TYPE_ID
                           from LS_PAYMENT_HDR H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I,
								LS_PAYMENT_APPROVAL LPA
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.PAYMENT_ID = L.PAYMENT_ID
                            and H.PAYMENT_ID = M.PAYMENT_ID
                            and L.AMOUNT <> 0
                            and L.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.ILR_ID = I.ILR_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and L.PAYMENT_TYPE_ID between 3 and 22
                     and H.PAYMENT_ID = LPA.PAYMENT_ID
                     and nvl(LPA.REJECTED,0) <> 1
                     and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select APPROVAL_TYPE_ID
                                                               from WORKFLOW_TYPE
                                                               where lower(description) = 'auto approve')
                        and LPA.APPROVAL_STATUS_ID  = 1)))
      loop

		 --skip the taxes since we already did them
		 if L_PAYMENTS.PAYMENT_TYPE_ID = TAX_PAYMENT_TYPE then
			continue;
		 end if;

         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
                     TO_CHAR(L_PAYMENTS.TRANS_TYPE);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_ACCTS(L_PAYMENTS.PAYMENT_TYPE_ID).ACCRUAL_ACCT_ID,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS||' '||sqlerrm;
         end if;

         -- process the credit.  Payment credits all hit 3022
         L_STATUS := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                     ' trans type: 3022';
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               3022,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_PAYMENTS.AP_ACCOUNT_ID,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
       L_COUNTER := L_COUNTER + 1;
      end loop;

     if L_COUNTER > 0 then
      PKG_PP_LOG.P_WRITE_MESSAGE(L_COUNTER||' JE pairs sent.');
      return 'OK';
     else
      return 'No JEs sent. Please confirm that payments have been approved.';
     end if;

   exception
      when others then
         return L_STATUS;
   end F_PAYMENT_APPROVE;

   --**************************************************************************
   --                            F_SEND_PAYMENT
   --**************************************************************************

   function F_SEND_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LS_PAYMENT_HDR set PAYMENT_STATUS_ID = 2 where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_APPROVAL set APPROVAL_STATUS_ID = 2 where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending Payment');
         return -1;
   end F_SEND_PAYMENT;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_PAYMENT
   --**************************************************************************

   function F_UPDATE_WORKFLOW_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_PAYMENT_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_PAYMENT_ID)
                                        and SUBSYSTEM = 'ls_payment_approval'),
                                     0)
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_PAYMENT;

   --**************************************************************************
   --                            F_REJECT_PAYMENT
   --**************************************************************************

   function F_REJECT_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L set PAYMENT_STATUS_ID = 4 where L.PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting Payment');
         return -1;
   end F_REJECT_PAYMENT;

   --**************************************************************************
   --                            F_UNREJECT_PAYMENT
   --**************************************************************************

   function F_UNREJECT_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L set PAYMENT_STATUS_ID = 2 where L.PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting Payment');
         return -1;
   end F_UNREJECT_PAYMENT;

   --**************************************************************************
   --                            F_APPROVE_PAYMENT
   --**************************************************************************

   function F_APPROVE_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this payment
      update LS_PAYMENT_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L
         set PAYMENT_STATUS_ID = 3
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving Payment');
         return -1;
   end F_APPROVE_PAYMENT;

   --**************************************************************************
   --                            F_UNSEND_PAYMENT
   --**************************************************************************

   function F_UNSEND_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L
         set PAYMENT_STATUS_ID = 1, GL_POSTING_MO_YR = null
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending Payment');
         return -1;
   end F_UNSEND_PAYMENT;

   --**************************************************************************
   --                            F_MATCH_INVOICES
   --**************************************************************************
   function F_MATCH_INVOICES(A_MONTH in date) return varchar2 is
      L_STATUS      varchar2(2000);
      L_RTN         number;
      MYPAYMENTHDRS PAYMENT_HDR_TYPE;
      MYINVOICEHDRS INVOICE_HDR_TYPE;
      INVOICEINDEX  number;
      PAYMENTINDEX  number;
   begin

      select h.* bulk collect
        into MYPAYMENTHDRS
        from LS_PAYMENT_HDR h
       where h.GL_POSTING_MO_YR = A_MONTH
	   and not exists
	   (
			select 1
			from ls_invoice_payment_map m
			where m.payment_id = h.payment_id
	   );

      select h.* bulk collect
	  into MYINVOICEHDRS
	  from LS_INVOICE h
	  where h.GL_POSTING_MO_YR = A_MONTH
	  and not exists
	   (
			select 1
			from ls_invoice_payment_map m
			where m.invoice_id = h.invoice_id
	   );

      for INVOICEINDEX in 1 .. MYINVOICEHDRS.COUNT
      loop
         for PAYMENTINDEX in 1 .. MYPAYMENTHDRS.COUNT
         loop
            P_INVOICE_COMPARE(MYINVOICEHDRS(INVOICEINDEX), MYPAYMENTHDRS(PAYMENTINDEX));
         end loop;
      end loop;
      return 'OK';
   end F_MATCH_INVOICES;

   --**************************************************************************
   --                            P_INVOICE_COMPARE
   --**************************************************************************

   procedure P_INVOICE_COMPARE(MYINVOICE in LS_INVOICE%rowtype,
                               MYPAYMENT in LS_PAYMENT_HDR%rowtype) is
      TOLERANCE_PCT    number;
      LG_TOLERANCE_PCT number;
      IS_IN_TOLERANCE  number;
	  adj_amount number;
	  total_amount number;
   begin
      if MYINVOICE.VENDOR_ID = MYPAYMENT.VENDOR_ID and MYINVOICE.COMPANY_ID = MYPAYMENT.COMPANY_ID and
         MYINVOICE.LEASE_ID = MYPAYMENT.LEASE_ID and MYINVOICE.ILR_ID = MYPAYMENT.ILR_ID and
		 MYINVOICE.LS_ASSET_ID = MYPAYMENT.LS_ASSET_ID		 then
         --Check the tolerance status
         if MYPAYMENT.AMOUNT <> 0 then
            TOLERANCE_PCT := (MYPAYMENT.AMOUNT - MYINVOICE.INVOICE_AMOUNT) / MYPAYMENT.AMOUNT;
         else
            TOLERANCE_PCT := 1;
         end if;

         --
         select min(NVL(TOLERANCE_PCT, 0))
           into LG_TOLERANCE_PCT
           from LS_LEASE_GROUP
          where LEASE_GROUP_ID in
                (select AA.LEASE_GROUP_ID from LS_LEASE AA where AA.LEASE_ID = MYPAYMENT.LEASE_ID);

         --Accept it as being in tolerance if the field is null or zero on LS_LEASE_GROUP
         if ABS(TOLERANCE_PCT) < LG_TOLERANCE_PCT or LG_TOLERANCE_PCT = 0 then
            IS_IN_TOLERANCE := 1;
         else
            IS_IN_TOLERANCE := 0;
         end if;

         --INSERT INTO LS_INVOICE_PAYMENT_MAP (invoice_id, payment_id, in_tolerance) values (myInvoice.Invoice_id, myPayment.Payment_id, is_in_tolerance);
         insert into LS_INVOICE_PAYMENT_MAP LIPM
             (INVOICE_ID, PAYMENT_ID, IN_TOLERANCE)
         values
             (MYINVOICE.INVOICE_ID, MYPAYMENT.PAYMENT_ID, IS_IN_TOLERANCE);

		if is_in_tolerance = 1 and MYPAYMENT.AMOUNT <> MYINVOICE.INVOICE_AMOUNT then
			-- update the payment line and payment hdr
			adj_amount := MYINVOICE.INVOICE_AMOUNT - MYPAYMENT.AMOUNT;

			select sum(l.amount)
			into total_amount
			from ls_payment_line l, ls_invoice_payment_map m
			where l.payment_id = m.payment_id
			and m.invoice_id = MYINVOICE.INVOICE_ID
			and l.payment_type_id = 2
			;

			update ls_payment_line lpl
			set lpl.amount = lpl.amount +
			(
				with rounder as
				(
					select l.payment_line_number, l.ls_asset_id, l.payment_id,
						round(adj_amount * l.amount / total_amount, 2) as amount_wo_rounding,
						adj_amount - case when row_number() over(order by l.amount desc, l.ls_asset_id) = 1
							then sum(round(adj_amount * l.amount / total_amount, 2)) over() else adj_amount end as diff
					from ls_payment_line l, ls_invoice_payment_map m
					where l.payment_id = m.payment_id
					and m.invoice_id = MYINVOICE.INVOICE_ID
					and l.payment_type_id = 2
					and l.set_of_books_id = 1
				)
				select amount_wo_rounding + diff
				from rounder
				where rounder.ls_asset_id = lpl.ls_asset_id
				and rounder.payment_id = lpl.payment_id
				and rounder.payment_line_number = lpl.payment_line_number
			)
			where lpl.payment_type_id = 2
			and set_of_books_id = 1
			and exists
			(
				select 1
				from ls_invoice_payment_map m
				where m.payment_id = lpl.payment_id
				and m.invoice_id = MYINVOICE.INVOICE_ID
			);

			update ls_payment_hdr lpl
			set amount =
			(
				select sum(a.amount)
				from ls_payment_line a
				where a.payment_id = lpl.payment_id
            and a.set_of_books_id = 1
			)
			where exists
			(
				select 1
				from ls_invoice_payment_map m
				where m.payment_id = lpl.payment_id
				and m.invoice_id = MYINVOICE.INVOICE_ID
			)
			;
		end if;

      end if;
   end P_INVOICE_COMPARE;

   --**************************************************************************
   --                            P_PAYMENT_ROLLUP
   --**************************************************************************
   procedure P_PAYMENT_ROLLUP (A_LEASE_ID in number, A_COMPANY_ID in number, A_MONTH in date) is
      MYPAYMENTLINES PAYMENT_LINE_TYPE;
      LINESINDEX     number;
   begin
      select * bulk collect
        into MYPAYMENTLINES
        from LS_PAYMENT_LINE LP1
		where LP1.payment_id in
		(
			select a.payment_id
			from LS_PAYMENT_HDR a
			where a.GL_POSTING_MO_YR = A_MONTH
            and a.COMPANY_ID = A_COMPANY_ID
			and case when a_lease_id = -1 then -1 else a.lease_id end = A_LEASE_ID
		)
		and LP1.SET_OF_BOOKS_ID =
		(
			select min(SET_OF_BOOKS_ID)
			from LS_PAYMENT_LINE LP2
			where LP1.PAYMENT_ID = LP2.PAYMENT_ID
			and LP1.GL_POSTING_MO_YR = LP2.GL_POSTING_MO_YR
		)
      and LP1.PAYMENT_ID not in
      (
         select PAYMENT_ID
         from LS_PAYMENT_HDR
         where PAYMENT_STATUS_ID in (3, 6)
      );

		update LS_PAYMENT_HDR a
		set AMOUNT = 0
		where a.GL_POSTING_MO_YR = A_MONTH
		and a.COMPANY_ID = A_COMPANY_ID
		and case when a_lease_id = -1 then -1 else a.lease_id end = A_LEASE_ID
      and PAYMENT_STATUS_ID < 3;

      for LINESINDEX in 1 .. MYPAYMENTLINES.COUNT
      loop
         update LS_PAYMENT_HDR
            set AMOUNT = AMOUNT + MYPAYMENTLINES(LINESINDEX).AMOUNT
          where PAYMENT_ID = MYPAYMENTLINES(LINESINDEX).PAYMENT_ID;
      end loop;
   end P_PAYMENT_ROLLUP;

   --**************************************************************************
   --                            F_GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

   --**************************************************************************
   --                            F_TAX_CALC_ACCRUALS
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly tax amounts by ls_asset
   -- @@PARAMS
   --    date: a_month
   --       The month to process taxes for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_TAX_CALC_ACCRUALS(A_COMPANY_ID in number,
								A_MONTH      in date) return varchar2 is
      L_STATUS      varchar2(2000);
      TAX_TABLE ASSET_TAX_TABLE;
      L_INDEX number;
      counter number;
      SQLS varchar2(4000);
      BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
   begin
      L_STATUS := 'Deleting old records';
      delete from LS_MONTHLY_TAX
      where GL_POSTING_MO_YR = A_MONTH
      and LS_ASSET_ID in (select LS_ASSET_ID from LS_ASSET where COMPANY_ID = A_COMPANY_ID)
	  and ACCRUAL = 1;

      L_STATUS := 'Bulk collecting';
      select RATES.RATE,
         RATES.LS_ASSET_ID,
         RATES.TAX_LOCAL_ID,
         LAS.SET_OF_BOOKS_ID,
         llv.payment_pct * (LAS.INTEREST_ACCRUAL + LAS.PRINCIPAL_ACCRUAL + NVL(LTBB.CONT1 * LAS.CONTINGENT_ACCRUAL1, 0) +
         NVL(LTBB.CONT2 * LAS.CONTINGENT_ACCRUAL2, 0) + NVL(LTBB.CONT3 * LAS.CONTINGENT_ACCRUAL3, 0) +
         NVL(LTBB.CONT4 * LAS.CONTINGENT_ACCRUAL4, 0) + NVL(LTBB.CONT5 * LAS.CONTINGENT_ACCRUAL5, 0) +
         NVL(LTBB.CONT6 * LAS.CONTINGENT_ACCRUAL6, 0) + NVL(LTBB.CONT7 * LAS.CONTINGENT_ACCRUAL7, 0) +
         NVL(LTBB.CONT8 * LAS.CONTINGENT_ACCRUAL8, 0) + NVL(LTBB.CONT9 * LAS.CONTINGENT_ACCRUAL9, 0) +
         NVL(LTBB.CONT10 * LAS.CONTINGENT_ACCRUAL10, 0) + NVL(LTBB.EXEC1 * LAS.EXECUTORY_ACCRUAL1, 0) +
         NVL(LTBB.EXEC2 * LAS.EXECUTORY_ACCRUAL2, 0) + NVL(LTBB.EXEC3 * LAS.EXECUTORY_ACCRUAL3, 0) +
         NVL(LTBB.EXEC4 * LAS.EXECUTORY_ACCRUAL4, 0) + NVL(LTBB.EXEC5 * LAS.EXECUTORY_ACCRUAL5, 0) +
         NVL(LTBB.EXEC6 * LAS.EXECUTORY_ACCRUAL6, 0) + NVL(LTBB.EXEC7 * LAS.EXECUTORY_ACCRUAL7, 0) +
         NVL(LTBB.EXEC8 * LAS.EXECUTORY_ACCRUAL8, 0) + NVL(LTBB.EXEC9 * LAS.EXECUTORY_ACCRUAL9, 0) +
         NVL(LTBB.EXEC10 * LAS.EXECUTORY_ACCRUAL10, 0)),
		 0, --no shift
		 llv.vendor_id
         bulk collect
         into TAX_TABLE
      from (select sum(TAB1.RATE) RATE,
               TAB1.LS_ASSET_ID LS_ASSET_ID,
               TAB1.TAX_LOCAL_ID TAX_LOCAL_ID,
               TAB1.REVISION REVISION,
			   TAB1.ilr_id
           from (select LTSR.RATE            RATE,
                     LATM.STATUS_CODE_ID  STATUS_CODE_ID,
                     LATM.LS_ASSET_ID     LS_ASSET_ID,
                     LATM.TAX_LOCAL_ID    TAX_LOCAL_ID,
                     LA.APPROVED_REVISION REVISION,
					 la.ilr_id
                 from LS_TAX_STATE_RATES LTSR,
                     ASSET_LOCATION     AL,
                     LS_TAX_LOCAL       LTL,
                     LS_ASSET_TAX_MAP   LATM,
                     LS_ASSET           LA
                where LTL.TAX_LOCAL_ID = LTSR.TAX_LOCAL_ID
                  and AL.STATE_ID = LTSR.STATE_ID
                  and AL.ASSET_LOCATION_ID = LA.TAX_ASSET_LOCATION_ID
                  and LA.LS_ASSET_ID = LATM.LS_ASSET_ID
                  and LTL.TAX_LOCAL_ID = LATM.TAX_LOCAL_ID
                  and LA.COMPANY_ID = A_COMPANY_ID
                  and LA.APPROVED_REVISION is not null
                  and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= LA.RETIREMENT_DATE))
                  and LTSR.EFFECTIVE_DATE =
                     (select max(T2.EFFECTIVE_DATE)
                       from LS_TAX_STATE_RATES T2, LS_LEASE LL, LS_ILR LI, LS_LEASE_OPTIONS LLO
                      where T2.TAX_LOCAL_ID = LTSR.TAX_LOCAL_ID
                        and T2.STATE_ID = LTSR.STATE_ID
                        and LA.ILR_ID = LI.ILR_ID
                        and LI.LEASE_ID = LL.LEASE_ID
                        and LL.CURRENT_REVISION = LLO.REVISION
                        and LLO.LEASE_ID = LL.LEASE_ID
                        and T2.EFFECTIVE_DATE <= case
                             when LLO.TAX_RATE_OPTION_ID = 1 then
                              A_MONTH
                             when LLO.TAX_RATE_OPTION_ID = 2 then
                              LL.MASTER_AGREEMENT_DATE
                           end)
               union
               select LTDR.RATE            RATE,
                     LATM.STATUS_CODE_ID  STATUS_CODE_ID,
                     LATM.LS_ASSET_ID     LS_ASSET_ID,
                     LATM.TAX_LOCAL_ID    TAX_LOCAL_ID,
                     LA.APPROVED_REVISION REVISION,
					 la.ilr_id
                 from LS_TAX_DISTRICT_RATES LTDR,
                     ASSET_LOCATION        AL,
                     LS_TAX_LOCAL          LTL,
                     LS_ASSET_TAX_MAP      LATM,
                     LS_ASSET              LA
                where LTL.TAX_LOCAL_ID = LTDR.TAX_LOCAL_ID
                  and AL.LS_TAX_DISTRICT_ID = LTDR.LS_TAX_DISTRICT_ID
                  and AL.ASSET_LOCATION_ID = LA.TAX_ASSET_LOCATION_ID
                  and LA.LS_ASSET_ID = LATM.LS_ASSET_ID
                  and LTL.TAX_LOCAL_ID = LATM.TAX_LOCAL_ID
                  and LA.COMPANY_ID = A_COMPANY_ID
                  and LA.APPROVED_REVISION is not null
                  and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= LA.RETIREMENT_DATE))
                  and LTDR.EFFECTIVE_DATE =
                     (select max(T2.EFFECTIVE_DATE)
                       from LS_TAX_DISTRICT_RATES T2, LS_LEASE LL, LS_ILR LI, LS_LEASE_OPTIONS LLO
                      where T2.TAX_LOCAL_ID = LTDR.TAX_LOCAL_ID
                        and T2.LS_TAX_DISTRICT_ID = LTDR.LS_TAX_DISTRICT_ID
                        and LA.ILR_ID = LI.ILR_ID
                        and LI.LEASE_ID = LL.LEASE_ID
                        and LL.CURRENT_REVISION = LLO.REVISION
                        and LLO.LEASE_ID = LL.LEASE_ID
                        and T2.EFFECTIVE_DATE <= case
                             when LLO.TAX_RATE_OPTION_ID = 1 then
                              A_MONTH
                             when LLO.TAX_RATE_OPTION_ID = 2 then
                              LL.MASTER_AGREEMENT_DATE
                           end)) TAB1
          where TAB1.STATUS_CODE_ID = 1
          group by TAB1.LS_ASSET_ID, TAB1.TAX_LOCAL_ID, TAB1.REVISION, TAB1.ilr_id) RATES,
         LS_ASSET_SCHEDULE LAS,
         LS_TAX_BASIS_BUCKETS LTBB, ls_ilr ilr, ls_lease_vendor llv
      where LAS.LS_ASSET_ID = RATES.LS_ASSET_ID
	  and rates.ilr_id = ilr.ilr_id and ilr.lease_id = llv.lease_id and ilr.company_id = llv.company_id
      and LAS.REVISION = RATES.REVISION
      and las.month = A_MONTH;

      L_STATUS := 'Calculating Taxes';
      forall L_INDEX in indices of TAX_TABLE
         insert into LS_MONTHLY_TAX (LS_ASSET_ID, TAX_LOCAL_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID, 
						AMOUNT, ACCRUAL, SCHEDULE_MONTH, VENDOR_ID)
         values
            (TAX_TABLE(L_INDEX).LS_ASSET_ID, TAX_TABLE(L_INDEX).TAX_LOCAL_ID, A_MONTH, TAX_TABLE(L_INDEX).SET_OF_BOOKS_ID, 
						TAX_TABLE(L_INDEX).RATE * TAX_TABLE(L_INDEX).AMOUNT, 1, A_MONTH, TAX_TABLE(L_INDEX).VENDOR_ID);

      L_STATUS := 'Insert tax amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET = 1;

      SQLS := 'update LS_ASSET_SCHEDULE '||
               'set LS_ASSET_SCHEDULE.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = ( '||
               'SELECT SUM(LS_MONTHLY_TAX.AMOUNT) '||
               'from LS_MONTHLY_TAX '||
               'where LS_ASSET_SCHEDULE.LS_ASSET_ID = LS_MONTHLY_TAX.LS_ASSET_ID '||
               'and LS_ASSET_SCHEDULE.MONTH = LS_MONTHLY_TAX.GL_POSTING_MO_YR '||
               'and LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID = LS_MONTHLY_TAX.SET_OF_BOOKS_ID '||
			   'and LS_MONTHLY_TAX.ACCRUAL = 1 '|| --ONLY THE ACCRUALS
               ') '||
               'where ls_asset_id in ( '||
               'select ls_asset_id from ls_asset la, ls_ilr li '||
               'where li.ilr_id = la.ilr_id '||
               'and li.company_id = '||to_char(A_COMPANY_ID)||' '||
               ') '||
               'and to_char(month) = '''||to_char(A_MONTH)||''' '||
               'and exists ( '||
               'select 1 '||
               'from LS_MONTHLY_TAX '||
               'where ls_asset_id = ls_asset_schedule.ls_asset_id '||
               'and gl_posting_mo_yr = ls_asset_schedule.month '||
               'and set_of_books_id = ls_asset_schedule.set_of_books_id '||
			   'and accrual = 1 '||
               ') '||
               'and revision = (select approved_revision from ls_asset where ls_asset_id = ls_asset_schedule.ls_asset_id) ';

      L_STATUS := SQLS;
      execute immediate SQLS;

      SQLS := 'update LS_ILR_SCHEDULE '||
               'set LS_ILR_SCHEDULE.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = ('||
               'select sum(nvl(LS_ASSET_SCHEDULE.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||',0) )'||
               'from LS_ASSET_SCHEDULE, LS_ASSET '||
               'where LS_ASSET_SCHEDULE.LS_ASSET_ID = LS_ASSET.LS_ASSET_ID and LS_ASSET.ILR_ID = LS_ILR_SCHEDULE.ILR_ID '||
               'and LS_ASSET_SCHEDULE.REVISION = LS_ILR_SCHEDULE.REVISION '||
               'and LS_ASSET_SCHEDULE.MONTH = LS_ILR_SCHEDULE.MONTH '||
               'and LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID = LS_ILR_SCHEDULE.SET_OF_BOOKS_ID) '||
            'where to_char(month) = '''||to_char(A_MONTH)||''' and ilr_id in (select ILR_ID from LS_ILR where COMPANY_ID = '||to_char(A_COMPANY_ID)||')';

      L_STATUS := SQLS;
      execute immediate SQLS;

      return 'OK';
   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_TAX_CALC_ACCRUALS;

	   --**************************************************************************
   --                            F_TAX_CALC_ACCRUALS
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly tax amounts by ls_asset
   -- @@PARAMS
   --    date: a_month
   --       The month to process taxes for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_TAX_CALC_PAYMENTS(A_LEASE_ID in number, A_COMPANY_ID in number,
								A_MONTH      in date) return varchar2 is
      L_STATUS      varchar2(2000);
      TAX_TABLE ASSET_TAX_TABLE;
      L_INDEX number;
      counter number;
      SQLS varchar2(4000);
      BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
   begin
      L_STATUS := 'Deleting old records';

      delete from LS_MONTHLY_TAX
      where GL_POSTING_MO_YR = A_MONTH
      and LS_ASSET_ID in
	  (
		select la.LS_ASSET_ID
		from LS_ASSET la, ls_ilr li
		where la.COMPANY_ID = A_COMPANY_ID
		and la.ilr_id = li.ilr_id
		and case when a_lease_id = -1 then -1 else li.lease_id end = a_lease_id
	  )
	  and ACCRUAL = 0;

      L_STATUS := 'Bulk collecting';
      select RATES.RATE,
         RATES.LS_ASSET_ID,
         RATES.TAX_LOCAL_ID,
         LAS.SET_OF_BOOKS_ID,
         llv.payment_pct * (LAS.INTEREST_ACCRUAL + LAS.PRINCIPAL_ACCRUAL + NVL(LTBB.CONT1 * LAS.CONTINGENT_ACCRUAL1, 0) +
         NVL(LTBB.CONT2 * LAS.CONTINGENT_ACCRUAL2, 0) + NVL(LTBB.CONT3 * LAS.CONTINGENT_ACCRUAL3, 0) +
         NVL(LTBB.CONT4 * LAS.CONTINGENT_ACCRUAL4, 0) + NVL(LTBB.CONT5 * LAS.CONTINGENT_ACCRUAL5, 0) +
         NVL(LTBB.CONT6 * LAS.CONTINGENT_ACCRUAL6, 0) + NVL(LTBB.CONT7 * LAS.CONTINGENT_ACCRUAL7, 0) +
         NVL(LTBB.CONT8 * LAS.CONTINGENT_ACCRUAL8, 0) + NVL(LTBB.CONT9 * LAS.CONTINGENT_ACCRUAL9, 0) +
         NVL(LTBB.CONT10 * LAS.CONTINGENT_ACCRUAL10, 0) + NVL(LTBB.EXEC1 * LAS.EXECUTORY_ACCRUAL1, 0) +
         NVL(LTBB.EXEC2 * LAS.EXECUTORY_ACCRUAL2, 0) + NVL(LTBB.EXEC3 * LAS.EXECUTORY_ACCRUAL3, 0) +
         NVL(LTBB.EXEC4 * LAS.EXECUTORY_ACCRUAL4, 0) + NVL(LTBB.EXEC5 * LAS.EXECUTORY_ACCRUAL5, 0) +
         NVL(LTBB.EXEC6 * LAS.EXECUTORY_ACCRUAL6, 0) + NVL(LTBB.EXEC7 * LAS.EXECUTORY_ACCRUAL7, 0) +
         NVL(LTBB.EXEC8 * LAS.EXECUTORY_ACCRUAL8, 0) + NVL(LTBB.EXEC9 * LAS.EXECUTORY_ACCRUAL9, 0) +
         NVL(LTBB.EXEC10 * LAS.EXECUTORY_ACCRUAL10, 0)),
		 LIO.PAYMENT_SHIFT,
		 llv.vendor_id
         bulk collect
         into TAX_TABLE
      from (select sum(TAB1.RATE) RATE,
               TAB1.LS_ASSET_ID LS_ASSET_ID,
               TAB1.TAX_LOCAL_ID TAX_LOCAL_ID,
               TAB1.REVISION REVISION
           from (select LTSR.RATE            RATE,
                     LATM.STATUS_CODE_ID  STATUS_CODE_ID,
                     LATM.LS_ASSET_ID     LS_ASSET_ID,
                     LATM.TAX_LOCAL_ID    TAX_LOCAL_ID,
                     LA.APPROVED_REVISION REVISION
                 from LS_TAX_STATE_RATES LTSR,
                     ASSET_LOCATION     AL,
                     LS_TAX_LOCAL       LTL,
                     LS_ASSET_TAX_MAP   LATM,
                     LS_ASSET           LA,
					 LS_ILR_OPTIONS		LIO,
					 LS_ILR LI2
                where LTL.TAX_LOCAL_ID = LTSR.TAX_LOCAL_ID
                  and AL.STATE_ID = LTSR.STATE_ID
                  and AL.ASSET_LOCATION_ID = LA.TAX_ASSET_LOCATION_ID
                  and LA.LS_ASSET_ID = LATM.LS_ASSET_ID
                  and LTL.TAX_LOCAL_ID = LATM.TAX_LOCAL_ID
                  and LA.COMPANY_ID = A_COMPANY_ID
				  and LA.ILR_ID = LI2.ILR_ID
                  and case when a_lease_id = -1 then -1 else li2.lease_id end = a_lease_id
				  and LA.APPROVED_REVISION = LIO.REVISION
				  and LA.ILR_ID = LIO.ILR_ID
              and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= LA.RETIREMENT_DATE))
                  and LTSR.EFFECTIVE_DATE =
                     (select max(T2.EFFECTIVE_DATE)
                       from LS_TAX_STATE_RATES T2, LS_LEASE LL, LS_ILR LI, LS_LEASE_OPTIONS LLO
                      where T2.TAX_LOCAL_ID = LTSR.TAX_LOCAL_ID
                        and T2.STATE_ID = LTSR.STATE_ID
                        and LA.ILR_ID = LI.ILR_ID
                        and LI.LEASE_ID = LL.LEASE_ID
                        and LL.CURRENT_REVISION = LLO.REVISION
                        and LLO.LEASE_ID = LL.LEASE_ID
                        and T2.EFFECTIVE_DATE <= case
                             when LLO.TAX_RATE_OPTION_ID = 1 then
                              add_months(A_MONTH, nvl(LIO.PAYMENT_SHIFT,0))
                             when LLO.TAX_RATE_OPTION_ID = 2 then
                              LL.MASTER_AGREEMENT_DATE
                           end)
               union
               select LTDR.RATE            RATE,
                     LATM.STATUS_CODE_ID  STATUS_CODE_ID,
                     LATM.LS_ASSET_ID     LS_ASSET_ID,
                     LATM.TAX_LOCAL_ID    TAX_LOCAL_ID,
                     LA.APPROVED_REVISION REVISION
                 from LS_TAX_DISTRICT_RATES LTDR,
                     ASSET_LOCATION        AL,
                     LS_TAX_LOCAL          LTL,
                     LS_ASSET_TAX_MAP      LATM,
                     LS_ASSET              LA,
					 LS_ILR_OPTIONS		   LIO,
					 LS_ILR LI2
                where LTL.TAX_LOCAL_ID = LTDR.TAX_LOCAL_ID
                  and AL.LS_TAX_DISTRICT_ID = LTDR.LS_TAX_DISTRICT_ID
                  and AL.ASSET_LOCATION_ID = LA.TAX_ASSET_LOCATION_ID
                  and LA.LS_ASSET_ID = LATM.LS_ASSET_ID
                  and LTL.TAX_LOCAL_ID = LATM.TAX_LOCAL_ID
                  and LA.COMPANY_ID = A_COMPANY_ID
                  and LA.ILR_ID = LI2.ILR_ID
                  and case when a_lease_id = -1 then -1 else li2.lease_id end = a_lease_id
				  and LA.APPROVED_REVISION = LIO.REVISION
				  and LA.ILR_ID = LIO.ILR_ID
              and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= LA.RETIREMENT_DATE))
                  and LTDR.EFFECTIVE_DATE =
                     (select max(T2.EFFECTIVE_DATE)
                       from LS_TAX_DISTRICT_RATES T2, LS_LEASE LL, LS_ILR LI, LS_LEASE_OPTIONS LLO
                      where T2.TAX_LOCAL_ID = LTDR.TAX_LOCAL_ID
                        and T2.LS_TAX_DISTRICT_ID = LTDR.LS_TAX_DISTRICT_ID
                        and LA.ILR_ID = LI.ILR_ID
                        and LI.LEASE_ID = LL.LEASE_ID
                        and LL.CURRENT_REVISION = LLO.REVISION
                        and LLO.LEASE_ID = LL.LEASE_ID
                        and T2.EFFECTIVE_DATE <= case
                             when LLO.TAX_RATE_OPTION_ID = 1 then
                              add_months(A_MONTH,nvl(LIO.PAYMENT_SHIFT,0))
                             when LLO.TAX_RATE_OPTION_ID = 2 then
                              LL.MASTER_AGREEMENT_DATE
                           end)) TAB1
          where TAB1.STATUS_CODE_ID = 1
          group by TAB1.LS_ASSET_ID, TAB1.TAX_LOCAL_ID, TAB1.REVISION) RATES,
         LS_ASSET_SCHEDULE LAS, LS_ILR_OPTIONS LIO,
         LS_TAX_BASIS_BUCKETS LTBB, LS_ASSET LA, LS_ILR LI, ls_lease_vendor llv
      where LAS.LS_ASSET_ID = RATES.LS_ASSET_ID
      and LAS.REVISION = RATES.REVISION
	  and LA.LS_ASSET_ID = LAS.LS_ASSET_ID
	  and RATES.REVISION = LIO.REVISION
	  and LA.ILR_ID = LIO.ILR_ID
	  and LA.ILR_ID = LI.ILR_ID
	  and li.lease_id = llv.lease_id and li.company_id = llv.company_id
      and case when a_lease_id = -1 then -1 else li.lease_id end = a_lease_id
      and month = add_months(A_MONTH, nvl(LIO.PAYMENT_SHIFT,0));

      L_STATUS := 'Calculating Taxes';
      forall L_INDEX in indices of TAX_TABLE
         insert into LS_MONTHLY_TAX (LS_ASSET_ID, TAX_LOCAL_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID,
				AMOUNT, ACCRUAL, SCHEDULE_MONTH, VENDOR_ID)
         values
            (TAX_TABLE(L_INDEX).LS_ASSET_ID, TAX_TABLE(L_INDEX).TAX_LOCAL_ID, A_MONTH, TAX_TABLE(L_INDEX).SET_OF_BOOKS_ID, 
				TAX_TABLE(L_INDEX).RATE * TAX_TABLE(L_INDEX).AMOUNT, 0, add_months(A_MONTH,TAX_TABLE(L_INDEX).SHIFT), TAX_TABLE(L_INDEX).VENDOR_ID);

      L_STATUS := 'Insert tax amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET = 1;


      SQLS := 'update LS_ASSET_SCHEDULE '||
               'set LS_ASSET_SCHEDULE.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = ( '||
               'SELECT SUM(LS_MONTHLY_TAX.AMOUNT) '||
               'from LS_MONTHLY_TAX '||
               'where LS_ASSET_SCHEDULE.LS_ASSET_ID = LS_MONTHLY_TAX.LS_ASSET_ID '||
               'and LS_ASSET_SCHEDULE.MONTH = LS_MONTHLY_TAX.SCHEDULE_MONTH '||
               'and LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID = LS_MONTHLY_TAX.SET_OF_BOOKS_ID '||
			   'and to_char(LS_MONTHLY_TAX.GL_POSTING_MO_YR) = '''||to_char(A_MONTH)||''' '||
			   'and LS_MONTHLY_TAX.ACCRUAL = 0 '||--only payments
               ') '||
               'where ls_asset_id in ( '||
               'select ls_asset_id from ls_asset la, ls_ilr li '||
               'where li.ilr_id = la.ilr_id '||
               'and li.company_id = '||to_char(A_COMPANY_ID)||' '||
			   ' and case when '|| to_char(a_lease_id) ||' = -1 then -1 else li.lease_id end = ' || to_char(a_lease_id) ||
               ' ) '||
               'and exists ( '||
               'select 1 '||
               'from LS_MONTHLY_TAX '||
               'where ls_asset_id = ls_asset_schedule.ls_asset_id '||
               'and schedule_month = ls_asset_schedule.month '||
               'and set_of_books_id = ls_asset_schedule.set_of_books_id '||
			   'and accrual = 0 '||
               ') '||
               'and revision = (select approved_revision from ls_asset where ls_asset_id = ls_asset_schedule.ls_asset_id) ';

      L_STATUS := SQLS;
      execute immediate SQLS;

      SQLS := 'update LS_ILR_SCHEDULE '||
               'set LS_ILR_SCHEDULE.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = ('||
               'select sum(nvl(LS_ASSET_SCHEDULE.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||',0) )'||
               'from LS_ASSET_SCHEDULE, LS_ASSET '||
               'where LS_ASSET_SCHEDULE.LS_ASSET_ID = LS_ASSET.LS_ASSET_ID and LS_ASSET.ILR_ID = LS_ILR_SCHEDULE.ILR_ID '||
               'and LS_ASSET_SCHEDULE.REVISION = LS_ILR_SCHEDULE.REVISION '||
               'and LS_ASSET_SCHEDULE.MONTH = LS_ILR_SCHEDULE.MONTH '||
               'and LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID = LS_ILR_SCHEDULE.SET_OF_BOOKS_ID) '||
            'where month in (select distinct schedule_month from LS_MONTHLY_TAX where ls_asset_id in (select ls_asset_id from ls_Asset where company_id = '||to_char(A_COMPANY_ID)||') and to_char(gl_posting_mo_yr) = '''||to_char(A_MONTH)||''') '||
			'and ilr_id in (select li.ILR_ID from LS_ILR li where li.COMPANY_ID = ' ||
			to_char(A_COMPANY_ID)|| ' and case when '|| to_char(a_lease_id) ||' = -1 then -1 else li.lease_id end = '
			|| to_char(a_lease_id)||' )';

      L_STATUS := SQLS;
      execute immediate SQLS;

      return 'OK';
   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_TAX_CALC_PAYMENTS;

   function F_LAM_CLOSED(  A_COMPANY_ID   in number,
                     A_MONTH     in date) return varchar2 is
      L_STATUS      varchar2(2000);
      L_RTN      number;
      L_GL_JE_CODE   varchar2(35);
   begin
     --verify that the next month is open on the CPR.
     select count(*)
     into L_RTN
     from CPR_CONTROL
     where COMPANY_ID = A_COMPANY_ID
     and ACCOUNTING_MONTH = add_months(A_MONTH, 1);

     if L_RTN = 0 then
      return 'The CPR must be open for the next month in order to close the Lease module.';
     end if;

     --Verify that all our Lease DGs are there for the next month
     select count(1)
     into L_RTN
     from
     (
      select A.DEPR_GROUP_ID
      from DEPR_LEDGER A, DEPR_GROUP B
      where A.GL_POST_MO_YR = A_MONTH
      and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
      and B.SUBLEDGER_TYPE_ID = -100
      and B.COMPANY_ID = A_COMPANY_ID
      minus
      select A.DEPR_GROUP_ID
      from DEPR_LEDGER A, DEPR_GROUP B
      where A.GL_POST_MO_YR = add_months(A_MONTH,1)
      and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
      and B.SUBLEDGER_TYPE_ID = -100
      and B.COMPANY_ID = A_COMPANY_ID
     );

     if L_RTN > 0 then
      return 'There are '||to_char(L_RTN)||' lease depreciation groups for this month that do not have rows in DEPR_LEDGER for next month. Please confirm that the next month is open on the CPR.';
     end if;

    select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMRECLASS'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAMRECLASS';

      for L_RECLASS in (select   LA.LS_ASSET_ID as LS_ASSET_ID,
                           LAS.BEG_LT_OBLIGATION - LAS.END_LT_OBLIGATION as DELTA_LT_OBLIG,
                           (LAS.BEG_OBLIGATION - LAS.BEG_LT_OBLIGATION) - (LAS.END_OBLIGATION - LAS.END_LT_OBLIGATION) as DELTA_ST_OBLIG,
                           LAS.SET_OF_BOOKS_ID as SOB_ID,
                     IA.ST_OBLIG_ACCOUNT_ID as ST_OBLIG_ACCOUNT_ID,
                     IA.LT_OBLIG_ACCOUNT_ID as LT_OBLIG_ACCOUNT_ID
                     from LS_ASSET LA, LS_ASSET_SCHEDULE LAS, LS_ILR_ACCOUNT IA
                     where LA.COMPANY_ID = A_COMPANY_ID
                     and LAS.MONTH = A_MONTH
                     and LAS.REVISION = LA.APPROVED_REVISION
                     and LAS.LS_ASSET_ID = LA.LS_ASSET_ID
                     and LAS.BEG_LT_OBLIGATION - LAS.END_LT_OBLIGATION <> 0
				and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= LA.RETIREMENT_DATE))
                and LA.ILR_ID = IA.ILR_ID)
      loop
         L_STATUS := 'Writing Reclass JEs for ls_asset_id: ' || TO_CHAR(L_RECLASS.LS_ASSET_ID);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_RECLASS.LS_ASSET_ID,
                                               3023,
                                               L_RECLASS.DELTA_LT_OBLIG,
                                               0,
                                               -1,
                                               null,
                                               L_RECLASS.LT_OBLIG_ACCOUNT_ID, --put account here in the future. Should be LT account from LS_ILR_ACCOUNT
                                               0,
                                               -1,
                                               A_COMPANY_ID,
                                               A_month,
                                               1,
                                               L_GL_JE_CODE,
                                               L_RECLASS.SOB_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_STATUS := 'Writing Reclass JEs for ls_asset_id: ' || TO_CHAR(L_RECLASS.LS_ASSET_ID);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_RECLASS.LS_ASSET_ID,
                                               3024,
                                               L_RECLASS.DELTA_LT_OBLIG,
                                               0,
                                               -1,
                                               null,
                                               L_RECLASS.ST_OBLIG_ACCOUNT_ID, --put account here in the future. Should be ST account from LS_ILR_ACCOUNT
                                               0,
                                               -1,
                                               A_COMPANY_ID,
                                               A_month,
                                               0,
                                               L_GL_JE_CODE,
                                               L_RECLASS.SOB_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
      end loop;
   return 'OK';
   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_LAM_CLOSED;

   function F_DEPR_APPROVE( A_COMPANY_ID in number,
                     A_MONTH in date) return varchar2 is
      L_STATUS      varchar2(2000);
      L_RTN      number;
      L_GL_JE_CODE   varchar2(35);
   begin
      select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMDEPR'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAMDEPR';

      for L_DEPR in (select  LCAM.LS_ASSET_ID as LS_ASSET_ID,
                        CD.SET_OF_BOOKS_ID as SOB_ID,
                        CD.CURR_DEPR_EXPENSE + CD.DEPR_EXP_ADJUST + CD.DEPR_EXP_ALLOC_ADJUST as DEPR_EXP,
                        CD.DEPR_GROUP_ID,
                        DG.RESERVE_ACCT_ID as RESERVE_ACCT_ID,
                        DG.EXPENSE_ACCT_ID as EXPENSE_ACCT_ID
                    from CPR_DEPR CD, CPR_LEDGER CL, LS_CPR_ASSET_MAP LCAM, LS_ASSET LA, DEPR_GROUP DG
                   where CD.ASSET_ID = CL.ASSET_ID
                     and CL.SUBLEDGER_INDICATOR = -100
                     and LCAM.ASSET_ID = CL.ASSET_ID
                     and (CD.CURR_DEPR_EXPENSE + CD.DEPR_EXP_ADJUST + CD.DEPR_EXP_ALLOC_ADJUST) <> 0
                     and LA.LS_ASSET_ID = LCAM.LS_ASSET_ID
                     and LA.COMPANY_ID = A_COMPANY_ID
                     and CD.GL_POSTING_MO_YR = A_MONTH
                     AND CL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID)
      loop
         L_STATUS := 'Writing depreciation JEs for ls_asset_id: ' || TO_CHAR(L_DEPR.LS_ASSET_ID);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_DEPR.LS_ASSET_ID,
                                               3032,
                                               L_DEPR.DEPR_EXP,
                                               0,
                                               -1,
                                               null,
                                               L_DEPR.EXPENSE_ACCT_ID, --is this the right account?
                                               0,
                                               -1,
                                               A_COMPANY_ID,
                                               A_month,
                                               1,
                                               L_GL_JE_CODE,
                                               L_DEPR.SOB_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_STATUS := 'Writing depreciation JEs for ls_asset_id: ' || TO_CHAR(L_DEPR.LS_ASSET_ID);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_DEPR.LS_ASSET_ID,
                                               3033,
                                               L_DEPR.DEPR_EXP,
                                               0,
                                               -1,
                                               null,
                                               L_DEPR.RESERVE_ACCT_ID, --what account goes here?
                                               0,
                                               -1,
                                               A_COMPANY_ID,
                                               A_month,
                                               0,
                                               L_GL_JE_CODE,
                                               L_DEPR.SOB_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_STATUS;
         end if;
      end loop;
   return 'OK';
   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_DEPR_APPROVE;



   function F_PROCESS_RESIDUAL( A_LS_ASSET_IDS in NUM_ARRAY, A_END_LOG in number) return varchar2 is
      L_STATUS    varchar2(2000);
      L_RTN       number;
      L_ERR       number;
      L_RETURN    varchar2(2000);
      L_ID_ARRAY     T_NUM_ARRAY;
      L_GL_JE_CODE   varchar2(35);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing residual for ' || A_LS_ASSET_IDS.COUNT || ' assets.');

      L_ERR := 0;

      -- Use this "SQL Type" array so that we can use it in cursor query
      -- Had to use PLSQL type array in argument so we can connect with uo_sqlca
      L_ID_ARRAY := T_NUM_ARRAY();
      for I in 1..A_LS_ASSET_IDS.COUNT
      loop
         L_ID_ARRAY.extend;
         L_ID_ARRAY(I) := A_LS_ASSET_IDS(I);
      end loop;

      select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LEASERESIDUAL'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LEASERESIDUAL';

      for REC in (select LS_ASSET.LS_ASSET_ID, LS_ASSET.ACTUAL_RESIDUAL_AMOUNT,
                     LS_ASSET.COMPANY_ID, SET_OF_BOOKS.SET_OF_BOOKS_ID, MONTH_VIEW.MONTH,
                     LS_ILR_ACCOUNT.RES_DEBIT_ACCOUNT_ID, LS_ILR_ACCOUNT.RES_CREDIT_ACCOUNT_ID,
                     LS_ILR.ILR_NUMBER
                  from LS_ASSET,
                     SET_OF_BOOKS,
                     LS_ILR_ACCOUNT,
                     LS_ILR,
                     (select COMPANY_ID, min(ACCOUNTING_MONTH) as MONTH from CPR_CONTROL
                        where CPR_CLOSED is null
                        group by COMPANY_ID) MONTH_VIEW
                  where LS_ASSET_ID in (select COLUMN_VALUE from table(L_ID_ARRAY))
                     and LS_ILR_ACCOUNT.ILR_ID(+) = LS_ILR.ILR_ID
                     and LS_ILR.ILR_ID = LS_ASSET.ILR_ID
                     and MONTH_VIEW.COMPANY_ID = LS_ASSET.COMPANY_ID
                  order by LS_ASSET.LS_ASSET_ID, SET_OF_BOOKS.SET_OF_BOOKS_ID)
      loop
         PKG_PP_LOG.P_WRITE_MESSAGE('Processing Residual for Asset: ' || to_char(REC.LS_ASSET_ID) || ', Set of Books: ' || to_char(REC.SET_OF_BOOKS_ID));

         -- Validate the account fields are set
         if REC.RES_DEBIT_ACCOUNT_ID is null then
            PKG_PP_LOG.P_WRITE_MESSAGE('ERROR.  ILR '||REC.ILR_NUMBER||' is missing a Residual Debit Account.');
            L_ERR := L_ERR + 1;
         end if;
         if REC.RES_CREDIT_ACCOUNT_ID is null then
            PKG_PP_LOG.P_WRITE_MESSAGE('ERROR.  ILR '||REC.ILR_NUMBER||' is missing a Residual Credit Account.');
            L_ERR := L_ERR + 1;
         end if;

         -- Continue (not break) if there has ever been an error.
         -- This way, we will see all errors at once, instead of fixing one, rerunning, and repeating.
         continue when L_ERR > 0;

         -- process the debit
         L_STATUS := 'Processing residual debit for asset_id: ' || TO_CHAR(REC.LS_ASSET_ID);

            L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(  REC.LS_ASSET_ID,
                                       3038, --A_TRANS_TYPE
                                       REC.ACTUAL_RESIDUAL_AMOUNT, --A_AMT
                                       0, --A_ASSET_ACT_ID
                                       -1, --A_DG_ID
                                       0, --A_WO_ID
                                       REC.RES_DEBIT_ACCOUNT_ID, --A_GL_ACCT_ID
                                       0, --A_GAIN_LOSS
                                       -1, --A_PEND_TRANS_ID
                                       REC.COMPANY_ID, --A_COMPANY_ID
                                       REC.MONTH, --A_MONTH
                                       1, --A_DR_CR
                                       L_GL_JE_CODE, --A_GL_JC
                                       REC.SET_OF_BOOKS_ID, --A_SOB_ID
                                       L_STATUS);
         if L_RTN = -1 then
            if A_END_LOG = 1 then
            PKG_PP_LOG.P_END_LOG();
         end if;
            return L_STATUS;
         end if;

            -- process the credit
            L_STATUS := 'Processing residual credit for asset_id: ' || TO_CHAR(REC.LS_ASSET_ID);

         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(  REC.LS_ASSET_ID,
                                       3039, --A_TRANS_TYPE
                                       REC.ACTUAL_RESIDUAL_AMOUNT, --A_AMT
                                       0, --A_ASSET_ACT_ID
                                       -1, --A_DG_ID
                                       0, --A_WO_ID
                                       REC.RES_CREDIT_ACCOUNT_ID, --A_GL_ACCT_ID
                                       0, --A_GAIN_LOSS
                                       -1, --A_PEND_TRANS_ID
                                       REC.COMPANY_ID, --A_COMPANY_ID
                                       REC.MONTH, --A_MONTH
                                       0, --A_DR_CR
                                       L_GL_JE_CODE, --A_GL_JC
                                       REC.SET_OF_BOOKS_ID, --A_SOB_ID
                                       L_STATUS);
         if L_RTN = -1 then
            if A_END_LOG = 1 then
            PKG_PP_LOG.P_END_LOG();
         end if;
            return L_STATUS;
         end if;

      end loop;

      if L_ERR = 0 then
         L_RETURN := 'OK';
      elsif L_ERR = 1 then
         L_RETURN := 'There was 1 validation error.  No Journal Entries were booked.';
      else
         L_RETURN := 'There were '||L_ERR||' validation errors.  No Journal Entries were booked.';
      end if;

      if A_END_LOG = 1 then
      PKG_PP_LOG.P_END_LOG();
     end if;

      return L_RETURN;

   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_PROCESS_RESIDUAL;



   function F_PROCESS_RESIDUAL( A_LS_ASSET_IDS in NUM_ARRAY) return varchar2 is
   begin
      return F_PROCESS_RESIDUAL(A_LS_ASSET_IDS => A_LS_ASSET_IDS, A_END_LOG => 1);
   end F_PROCESS_RESIDUAL;
--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end PKG_LEASE_CALC;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1562, 0, 10, 4, 3, 0, 40746, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040746_lease_PKG_LEASE_CALC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;