/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034075_taxrpr_co_filter.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/24/2014 Alex P.
||============================================================================
*/

update PP_DYNAMIC_FILTER set DW = 'dw_pp_company_filter' where DW = 'dw_rpr_company_filter';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (986, 0, 10, 4, 2, 0, 34075, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034075_taxrpr_co_filter.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;