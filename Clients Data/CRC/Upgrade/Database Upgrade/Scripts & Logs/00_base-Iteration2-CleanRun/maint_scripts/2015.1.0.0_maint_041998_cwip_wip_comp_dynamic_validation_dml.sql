/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		CWIP
|| File Name:   maint_041998_cwip_wip_comp_dynamic_validation.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1.0.0 01/20/2015 Charlie Shilling	 maint 41998 - add dynamic validation for wip_comp custom functionality
||==========================================================================================
*/

-- Create new SQL Dynamic Validation type for the Work Order Control G/L Reconciliation 
insert into wo_validation_type (
	wo_validation_type_id, description, long_description, "FUNCTION", find_company, col1, col2, hard_edit)
values (
	1059, 'WO Control - f_wip_comp_extension', 'WO Control - Dynamic Validation for WIP Computation Extension', 'f_wip_comp_extension', 'select company_id from company_setup where company_id = <arg2>', 'month_number', 'company_id', 1);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2201, 0, 2015, 1, 0, 0, 041998, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041998_cwip_wip_comp_dynamic_validation_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;