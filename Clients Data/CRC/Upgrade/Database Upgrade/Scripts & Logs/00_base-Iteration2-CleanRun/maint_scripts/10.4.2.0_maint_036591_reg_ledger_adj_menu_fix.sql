/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036591_reg_ledger_adj_menu_fix.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/27/2014 Shane "C" Ward
||============================================================================
*/

update PPBASE_MENU_ITEMS
   set LABEL = 'History Ledger Adjustments', MENU_LEVEL = 2, ITEM_ORDER = 5,
       PARENT_MENU_IDENTIFIER = 'MONTHLY'
 where WORKSPACE_IDENTIFIER = 'uo_reg_ledger_adjust_manage_ws_hist';
update PPBASE_MENU_ITEMS
   set LABEL = 'Forecast Ledger Adjustments', MENU_LEVEL = 2, ITEM_ORDER = 6,
       PARENT_MENU_IDENTIFIER = 'MONTHLY'
 where WORKSPACE_IDENTIFIER = 'uo_reg_ledger_adjust_manage_ws_fore';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (999, 0, 10, 4, 2, 0, 36591, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036591_reg_ledger_adj_menu_fix.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;