/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049215_lessor_01_set_query_workspace_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/11/2017 Josh Sandler   Lessor shares worksapce with Lessee
||============================================================================
*/

UPDATE ppbase_workspace
SET workspace_uo_name = 'uo_ls_querycntr_wksp'
WHERE workspace_uo_name = 'uo_lsr_querycntr_wksp';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3789, 0, 2017, 1, 0, 0, 49215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049215_lessor_01_set_query_workspace_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

