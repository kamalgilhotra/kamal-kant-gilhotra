/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038647_reg_fcst_recon_review.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 10.4.2.7 07/07/2014 Ryan Oliveria        Reg Forecast Recon Review
||========================================================================================
*/

-- Table
create table REG_FORECAST_RECON_LEDGER
(
 FORECAST_VERSION_ID number(22,0) not null,
 RECON_ID            number(22,0) not null,
 GL_MONTH            number(22,1) not null,
 REG_COMPANY_ID      number(22,0) not null,
 USER_ID             varchar2(18),
 TIME_STAMP          date,
 TRIAL_BALANCE_AMT   number(22,2),
 FCST_REG_ACCT_AMT   number(22,2),
 RECON_ADJ_STATUS    number(22,0),
 RECON_ADJ_AMOUNT    number(22,2),
 RECON_COMMENT       varchar2(1000)
);

alter table REG_FORECAST_RECON_LEDGER
   add constraint PK_REG_FORECAST_RECON_LEDGER
       primary key (FORECAST_VERSION_ID, RECON_ID, GL_MONTH, REG_COMPANY_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_FORECAST_RECON_LEDGER
   add constraint FK_REG_FORECAST_RECON_LEDGER_V
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_FORECAST_RECON_LEDGER
   add constraint FK_REG_FORECAST_RECON_LEDGER_R
       foreign key (RECON_ID)
       references REG_FCST_RECON_ITEMS (RECON_ID);

alter table REG_FORECAST_RECON_LEDGER
   add constraint FK_REG_FORECAST_RECON_LEDGER_C
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

-- Comments
comment on table REG_FORECAST_RECON_LEDGER is 'The Reg Forecast Recon Ledger table stores the trial balance and forecast ledger amounts summarized by reconciliation item, reg company, and gl month.';

comment on column REG_FORECAST_RECON_LEDGER.FORECAST_VERSION_ID is 'System assigned identifier for the forecast ledger';
comment on column REG_FORECAST_RECON_LEDGER.RECON_ID is 'System assigned identifier for the reconciliation item';
comment on column REG_FORECAST_RECON_LEDGER.GL_MONTH is 'General Ledger month in YYYYMM format';
comment on column REG_FORECAST_RECON_LEDGER.REG_COMPANY_ID is 'System assigned identifier for the reg company';
comment on column REG_FORECAST_RECON_LEDGER.USER_ID is 'Standard system-assigned user id used for audit purposes';
comment on column REG_FORECAST_RECON_LEDGER.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_FORECAST_RECON_LEDGER.TRIAL_BALANCE_AMT is 'The trial balance amount used to reconcile to the forecast ledger amount';
comment on column REG_FORECAST_RECON_LEDGER.FCST_REG_ACCT_AMT is 'The forecast ledger amount used to reconcile to the trial balance amount';
comment on column REG_FORECAST_RECON_LEDGER.RECON_ADJ_STATUS is 'Indicates whether an adjustment exists for the given recon item and month';
comment on column REG_FORECAST_RECON_LEDGER.RECON_ADJ_AMOUNT is 'Ledger adjustment amount created from the reconciliation process';
comment on column REG_FORECAST_RECON_LEDGER.RECON_COMMENT is 'Comment for a reconciliation adjustment';

-- Config
insert into REG_SOURCE (REG_SOURCE_ID, DESCRIPTION, NON_STANDARD) values (16, 'Fcst Reconciliation', 0);

insert into REG_QUERY_DRILLBACK
   (REG_SOURCE_ID, HIST_FCST_FLAG, REG_QUERY_ID, DESCRIPTION, DEFAULT_FLAG)
   select 16, 2, ID, 'Forecast Ledger Query from Recon', 0
     from CR_DD_SOURCES_CRITERIA
    where LOWER(TABLE_NAME) = 'reg_forecast_ledger_id_sv';


-- Add columns to reg_forecast_ledger
alter table REG_FORECAST_LEDGER add RECON_ADJ_AMOUNT number(22,2);
alter table REG_FORECAST_LEDGER add RECON_ADJ_COMMENT varchar2(1000);

comment on column REG_FORECAST_LEDGER.RECON_ADJ_AMOUNT is 'Ledger adjustment amount created from reconciliation process.';
comment on column REG_FORECAST_LEDGER.RECON_ADJ_COMMENT is 'Comment for a reconciliation ledger adjustment.';


-- Workspace
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_fcst_recon_review', 'Recon Forecast Ledger', 'uo_reg_fcst_recon_review', 'Reconcile Forecast Ledger');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'RECON_FCST_LEDGER', 2, 7, 'Recon Forecast Ledger', 'Reconcile Forecast Ledger', 'MONTHLY',
    'uo_reg_fcst_recon_review', 1);

-- Forecast Ledger Queries
insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, DEFAULT_VALUE, COLUMN_HEADER,
    COLUMN_WIDTH, QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select ID, 'RECON_ADJ_AMOUNT', 10, 1, 1, null, 'Recon Adj Amount', 313, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA
    where LOWER(TABLE_NAME) = 'reg_forecast_ledger_sv';

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, DEFAULT_VALUE, COLUMN_HEADER,
    COLUMN_WIDTH, QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select ID, 'RECON_ADJ_COMMENT', 11, 0, 1, null, 'Recon Adj Comment', 1501, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA
    where LOWER(TABLE_NAME) = 'reg_forecast_ledger_sv';

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, DEFAULT_VALUE, COLUMN_HEADER,
    COLUMN_WIDTH, QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select ID, 'RECON_ADJ_AMOUNT', 16, 1, 1, null, 'Recon Adj Amount', 313, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA
    where LOWER(TABLE_NAME) = 'reg_forecast_ledger_id_sv';

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, DEFAULT_VALUE, COLUMN_HEADER,
    COLUMN_WIDTH, QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select ID, 'RECON_ADJ_COMMENT', 17, 0, 1, null, 'Recon Adj Comment', 1501, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA
    where LOWER(TABLE_NAME) = 'reg_forecast_ledger_id_sv';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1174, 0, 10, 4, 2, 7, 38647, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038647_reg_fcst_recon_review.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;