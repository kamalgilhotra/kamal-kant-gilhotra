/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_25__ilr_import_new_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

alter table ls_import_ilr
add (funding_status_id number(22,0), funding_status_xlate varchar2(254));

alter table ls_import_ilr_archive
add (funding_status_id number(22,0), funding_status_xlate varchar2(254));

COMMENT ON COLUMN ls_import_ilr.funding_status_id IS 'Funding status to determine state of components related to ILR.';
COMMENT ON COLUMN ls_import_ilr.funding_status_xlate IS 'Translation field to determine funding status to determine state of components related to ILR.';

COMMENT ON COLUMN ls_import_ilr_archive.funding_status_id IS 'Funding status to determine state of components related to ILR.';
COMMENT ON COLUMN ls_import_ilr_archive.funding_status_xlate IS 'Translation field to determine funding status to determine state of components related to ILR.';





--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2425, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_25__ilr_import_new_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;