/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048900_lessor_01_invoice_approvals_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/07/2017 Shane "C" Ward    New Lessor Invoice Workflow
||============================================================================
*/

INSERT INTO WORKFLOW_SUBSYSTEM
            (subsystem,
             field1_desc,
             field1_sql,
             pending_notification_type,
             approved_notification_type,
             rejected_notification_type,
             send_sql,
             approve_sql,
             reject_sql,
             unreject_sql,
             unsend_sql,
             update_workflow_type_sql,
             send_emails)
VALUES      ('lsr_invoice_approval',
             'Invoice ID',
             '<<id_field1>>',
             'INVOICE APPROVAL MESSAGE',
             'INVOICE APPROVED NOTICE MESSAGE',
             'INVOICE REJECTION MESSAGE',
             'select PKG_LESSOR_APPROVAL.F_SEND_INVOICE(<<id_field1>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_APPROVE_INVOICE(<<id_field1>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_REJECT_INVOICE(<<id_field1>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_UNREJECT_INVOICE(<<id_field1>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_UNSEND_INVOICE(<<id_field1>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_UPDATE_WORKFLOW_INVOICE(<<id_field1>>) from dual',
             1);

INSERT INTO WORKFLOW_AMOUNT_SQL
            (subsystem,
             base_sql,
             SQL)
VALUES      ('lsr_invoice_approval',
             1,
             'select nvl(sum(nvl(invoice_principal,0)+nvl(invoice_interest,0)+nvl(invoice_executory,0)+nvl(invoice_contingent,0)),0) appr_amount from lsr_invoice where invoice_id = <<id_field1>>');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3971, 0, 2017, 1, 0, 0, 48900, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048900_lessor_01_invoice_approvals_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;