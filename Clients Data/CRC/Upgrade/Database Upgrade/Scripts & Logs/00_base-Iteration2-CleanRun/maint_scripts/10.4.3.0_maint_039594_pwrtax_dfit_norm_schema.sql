/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039594_pwrtax_dfit_norm_schema.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/03/2014 Anand Rajashekar    Normalization Schema Workspace
||========================================================================================
*/

-- Add workspace_identifier

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'deferred_normalization_schema', TO_DATE('03-SEP-14', 'DD-MON-RR'), 'PWRPLANT', 'Normalization Schemas',
    'uo_tax_dfit_wksp_normalization_schema', 'Normalization Schema');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'deferred_normalization_schema'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'deferred_setup_norm_schema';

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (84, TO_DATE('03-SEP-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - Normalization Schema', null,
    'uo_ppbase_tab_filter_dynamic');

-- Add new filters

insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (201,'From Tax Book','dw','norm_from_tax',null,'dw_tax_book_filter','tax_book_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (202,'To Tax Book','dw','norm_to_tax',null,'dw_tax_book_filter','tax_book_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (203,'Normalized Basis Difference','dw','norm_from_tax',null,'dw_tax_book_filter','tax_book_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (204,'Normalization Type','dw','norm_type_id',null,'dddw_norm_type','norm_type_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (205,'Allocate Book Depreciation','dw','book_depr_alloc_ind',null,'dddw_yes_no','yes_no_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (206,'Include Cap Depr in DIT Calc','dw','cap_depr_ind',null,'dddw_yes_no','yes_no_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (207,'Allow Deferreds to Over Reverse','dw','no_zero_check',null,'dddw_yes_no','yes_no_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (208,'Net Basis Difference Activity','dw','basis_diff_activity_split',null,'dddw_yes_no','yes_no_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (209,'Jurisdiction','dw','normalization_schema.jurisdiction_id',null,'dw_jurisdiction_filter','jurisdiction_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (210,'Amortization Type','dw','normalization_schema.amortization_type_id',null,'dw_amortization_type_filter','amortization_type_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);
insert into PP_DYNAMIC_FILTER (FILTER_ID,LABEL,INPUT_TYPE,SQLS_COLUMN_EXPRESSION,SQLS_WHERE_CLAUSE,DW,DW_ID,DW_DESCRIPTION,DW_ID_DATATYPE,REQUIRED,SINGLE_SELECT_ONLY,VALID_OPERATORS,DATA,USER_ID,TIME_STAMP,EXCLUDE_FROM_WHERE,NOT_RETRIEVE_IMMEDIATE,INVISIBLE) values (211,'Deferred Rates','dw','normalization_schema.def_income_tax_rate_id',null,'dw_deferred_rates_filter','def_income_tax_rate_id','description','N',0,0,null,null,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'),0,0,0);



-- Dynamic filter mapping

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,180,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,201,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,202,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,203,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,204,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,205,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,206,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,207,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,208,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,209,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,210,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (84,211,'PWRPLANT',to_date('03-SEP-14','DD-MON-RR'));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1371, 0, 10, 4, 3, 0, 39594, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039594_pwrtax_dfit_norm_schema.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
