/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048943_lessor_01_create_ilr_schedule_sales_direct_table_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.1.0.0 10/02/2017 Johnny Sisouphanh Create final result table for the lessor ILR schedule sales direct calculation
||============================================================================
*/

CREATE TABLE LSR_ILR_SCHEDULE_SALES_DIRECT (
	ilr_id                          NUMBER(22,0) 	NOT NULL,
	revision                        NUMBER(22,0) 	NOT NULL,
	set_of_books_id                 NUMBER(22,0) 	NOT NULL,
	month                           DATE 		 	NOT NULL,
	user_id                         VARCHAR2(18)	NULL,
	time_stamp                      DATE			NULL,
	principal_received              NUMBER(22,0)	NOT NULL,
	principal_accrued               NUMBER(22,0)	NOT NULL,
	beg_long_term_receivable        NUMBER(22,0)	NOT NULL,
	end_long_term_receivable        NUMBER(22,0)	NOT NULL,
	beg_unguaranteed_residual       NUMBER(22,0)	NOT NULL,
	interest_unguaranteed_residual  NUMBER(22,0)	NOT NULL,
	ending_unguaranteed_residual    NUMBER(22,0)	NOT NULL,
	beg_net_investment              NUMBER(22,0)	NOT NULL,
	interest_net_investment         NUMBER(22,0)	NOT NULL,	
	ending_net_investment           NUMBER(22,0)	NOT NULL,
	calculated_initial_rate         NUMBER(22,0)	NOT NULL,
	calculated_rate                 NUMBER(22,0)	NOT NULL
);

ALTER TABLE LSR_ILR_SCHEDULE_SALES_DIRECT
  	ADD CONSTRAINT pk_lsr_ilr_sched_sales_direct PRIMARY KEY (
	    ilr_id,
		revision,
		set_of_books_id,
		month
	)
	USING INDEX
	    TABLESPACE pwrplant_idx
;

ALTER TABLE LSR_ILR_SCHEDULE_SALES_DIRECT
ADD CONSTRAINT r_lsr_ilr_sched_sales_direct1 FOREIGN KEY (
		ilr_id,
		revision
	) REFERENCES lsr_ilr_options (
		ilr_id,
		revision
);

ALTER TABLE LSR_ILR_SCHEDULE_SALES_DIRECT
ADD CONSTRAINT r_lsr_ilr_sched_sales_direct2 FOREIGN KEY (
		set_of_books_id
	) REFERENCES set_of_books (
		set_of_books_id
);	

ALTER TABLE LSR_ILR_SCHEDULE_SALES_DIRECT
ADD CONSTRAINT r_lsr_ilr_sched_sales_direct3 FOREIGN KEY (
	    ilr_id,
		revision,
		set_of_books_id,
		month
	) REFERENCES lsr_ilr_schedule (
	    ilr_id,
		revision,
		set_of_books_id,
		month
);	

COMMENT ON TABLE LSR_ILR_SCHEDULE_SALES_DIRECT IS '(C)  [06] The ILR Schedule Sales Direct table records the ILR''s payment sales type schedule.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.ILR_ID IS 'System-assigned identifier of a particular ILR.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.REVISION IS 'The revision.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.SET_OF_BOOKS_ID IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.MONTH IS 'The month being processed.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.PRINCIPAL_RECEIVED IS 'The Fixed Payment Amount from the payment terms minus the Interest/ Income Received for the payment period.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.PRINCIPAL_ACCRUED IS 'The amount of Principal Received divided by frequency.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.BEG_LONG_TERM_RECEIVABLE IS 'The amount of Beginning Receivable Balance for MONTH +12. For example if in MONTH 1, then the Beginning Receivable Balance for Month 13.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.END_LONG_TERM_RECEIVABLE IS 'The amount of Ending Receivable Balance for MONTH +12. For example if in MONTH 1, then the Ending Receivable Balance for Month 13.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.BEG_UNGUARANTEED_RESIDUAL IS 'For the first month, it is the NPV of the Unguaranteed Residual. Every subsequent month, it is the amount of the Previous month''s Ending Unguaranteed Residual.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.INTEREST_UNGUARANTEED_RESIDUAL IS 'The amount of Beginning Unguaranteed Residual multiplied by the Initial Rate.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.ENDING_UNGUARANTEED_RESIDUAL IS 'The amount of Beginning Unguaranteed Residual plus the Interest on Unguaranteed Residual.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.BEG_NET_INVESTMENT IS 'For the first month, it is the amount of Beginning Receivables plus Beginning Unguaranteed Residual. Every subsequent month of Beginning Net Investment: Previous month''s Ending Net Investment.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.INTEREST_NET_INVESTMENT IS 'The amount of Beginning Net Investment multiplied by the Initial Rate.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.ENDING_NET_INVESTMENT IS 'The amount of Beginning Net Investment - Principal Paid + Interest on Net Investment.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.CALCULATED_INITIAL_RATE IS 'The Calculated Initial Rate.';
COMMENT ON COLUMN LSR_ILR_SCHEDULE_SALES_DIRECT.CALCULATED_RATE IS 'The Calculated Rate.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
  (3752, 0, 2017, 1, 0, 0, 48943, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048943_lessor_01_create_ilr_schedule_sales_direct_table_ddl.sql', 1,
  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;