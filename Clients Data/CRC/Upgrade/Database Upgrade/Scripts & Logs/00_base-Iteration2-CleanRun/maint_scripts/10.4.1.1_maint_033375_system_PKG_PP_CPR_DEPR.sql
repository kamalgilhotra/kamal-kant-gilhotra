/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033375_system_PKG_PP_CPR_DEPR.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/15/2013 Brandon Beck
||============================================================================
*/

--TO DO:
-- finish sch
--
-- we don't need these backfilled for non-106to101:
--    has_nurv isn't used
--    has_nurv_last_month isn't used
--    exists_two_months
--    exists_aro

create or replace package PKG_PP_CPR_DEPR as

   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
                                 A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type);

   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH       date,
                                 A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type);

   procedure P_STAGEMONTHENDDEPR(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
                                 A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type);

   procedure P_STAGEMONTHENDDEPR(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH       date,
                                 A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type);

   function F_PREPDEPRCALC return number;

end PKG_PP_CPR_DEPR;
/


create or replace package body PKG_PP_CPR_DEPR as

   --**************************************************************
   --       VARIABLES
   --**************************************************************

   G_IGNOREINACTIVEYES PKG_PP_COMMON.NUM_TABTYPE;
   G_IGNOREINACTIVENO  PKG_PP_COMMON.NUM_TABTYPE;
   G_CURRMONTH         date;

   --**************************************************************************
   --                            P_CHECKINACTIVEDEPR
   --**************************************************************************
   --
   -- Set the companies that are ignoring inactive status
   --
   procedure P_CHECKINACTIVEDEPR(A_COMPANY_ID number,
                                 A_INDEX      number) is

   begin
      if LOWER(trim(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',
                                                                          A_COMPANY_ID),
                        'no'))) = 'no' then
         G_IGNOREINACTIVENO(A_INDEX) := A_COMPANY_ID;
      else
         G_IGNOREINACTIVEYES(A_INDEX) := A_COMPANY_ID;
      end if;
   end P_CHECKINACTIVEDEPR;

   --**************************************************************************
   --                     P_STARTMONTHENDDEPR
   --**************************************************************************
   --
   -- WRAPPER for the start depr calc to allow single month depr calculations
   --
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH       date,
                                 A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type) is

      MY_MONTHS PKG_PP_COMMON.DATE_TABTYPE;

   begin
      MY_MONTHS(1) := A_MONTH;
      P_STARTMONTHENDDEPR(A_COMPANY_IDS, MY_MONTHS, A_SUBLEDGER);
   end P_STARTMONTHENDDEPR;

   --**************************************************************************
   --                     P_STARTMONTHENDDEPR
   --**************************************************************************
   --
   --  Start a depr calc for an array of companies and an array of months
   -- Start Logging
   -- Load Global Temp Staging table
   -- Calculate Deprecation
   -- Backfill results to depr ledger
   -- End logging
   --
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
                                 A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type) is
      MY_RET number;
      MY_STR varchar2(2000);
   begin
      --
      -- START THE LOGGING and log what companys and months are being processed
      --
      PKG_PP_LOG.P_WRITE_MESSAGE('PROCESSING the following company_ids:');

      --
      -- LOOP over the companies and prep the company for processing
      -- Get company system controls to determine whether to include inactive depr groups
      --
      for I in 1 .. A_COMPANY_IDS.COUNT
      loop
         PKG_PP_LOG.P_WRITE_MESSAGE('   ' || TO_CHAR(A_COMPANY_IDS(I)));
         -- load the arrays for companies ignoring inactive depr groups
         P_CHECKINACTIVEDEPR(A_COMPANY_IDS(I), I);
      end loop;

      P_STAGEMONTHENDDEPR(A_COMPANY_IDS, A_MONTHS, A_SUBLEDGER);

      -- start depreciation calculation
      if F_PREPDEPRCALC() = -1 then
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR During Depreciation Calculation');
      end if;

      --
      --  END LOGGING
      --
      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

/*    exception
      when others then
         RAISE_APPLICATION_ERROR(-20000, 'ERROR: ' || sqlerrm); */
   end P_STARTMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  WRAPPER for the prep function to pass in an array of months.
   --
   procedure P_STAGEMONTHENDDEPR(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
                                 A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type) is

   begin
      for I in A_MONTHS.FIRST .. A_MONTHS.LAST
      loop
         P_STAGEMONTHENDDEPR(A_COMPANY_IDS, A_MONTHS(I), A_SUBLEDGER);
      end loop;
   end P_STAGEMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation.
   -- THE Load is based on depr ledger for a passed in array of company ids.
   -- AND a single month
   --
   procedure P_STAGEMONTHENDDEPR(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH       date,
                                 A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type) is
   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the calculation staging table for month:' ||
                                 TO_CHAR(A_MONTH, 'yyyymm'));

      forall I in indices of G_IGNOREINACTIVENO
         insert into CPR_DEPR_CALC_STG
            (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
             ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS,
             DEPR_CALC_STATUS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH,
             SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN,
             RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
             DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE,
             YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB,
             MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID,
             DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE,
             SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT, END_OF_LIFE, NET_GROSS, OVER_DEPR_CHECK, RATE,
             EFFECTIVE_DATE, SUBLEDGER_TYPE_ID, ENG_IN_SERVICE_YEAR, TRF_WEIGHT, MIN_MPC, TRUEUP_ADJ,
             NET_TRF, ACTIVITY, ACTIVITY_3, BEG_RES_AMT, NET_IMP_AMT, EXISTS_TWO_MONTHS, EXISTS_ARO)
            with CPR_DEPR_VIEW as
             (select *
                from CPR_DEPR
               where GL_POSTING_MO_YR = A_MONTH
                 and COMPANY_ID = G_IGNOREINACTIVENO(I)),
            DEPR_METHOD_RATES_VIEW as
             (select DD.DEPR_METHOD_ID,
                     DD.SET_OF_BOOKS_ID,
                     DD.EFFECTIVE_DATE,
                     DD.RATE,
                     DD.OVER_DEPR_CHECK,
                     DD.NET_GROSS,
                     DD.END_OF_LIFE,
                     ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
                from DEPR_METHOD_RATES DD
               where DD.EFFECTIVE_DATE <= A_MONTH)
            select A.ASSET_ID,
                   A.SET_OF_BOOKS_ID,
                   A.GL_POSTING_MO_YR,
                   nvl(A.INIT_LIFE,0),
                   nvl(A.REMAINING_LIFE,0),
                   nvl(A.ESTIMATED_SALVAGE,0),
                   nvl(A.BEG_ASSET_DOLLARS,0),
                   nvl(A.NET_ADDS_AND_ADJUST,0),
                   nvl(A.RETIREMENTS,0),
                   2,
                   nvl(A.TRANSFERS_IN,0),
                   nvl(A.TRANSFERS_OUT,0),
                   nvl(A.ASSET_DOLLARS,0),
                   A.BEG_RESERVE_MONTH,
                   nvl(A.SALVAGE_DOLLARS,0),
                   nvl(A.RESERVE_ADJUSTMENT,0),
                   nvl(A.COST_OF_REMOVAL,0),
                   nvl(A.RESERVE_TRANS_IN,0),
                   nvl(A.RESERVE_TRANS_OUT,0),
                   nvl(A.DEPR_EXP_ADJUST,0),
                   nvl(A.OTHER_CREDITS_AND_ADJUST,0),
                   nvl(A.GAIN_LOSS,0),
                   0,--A.DEPRECIATION_BASE,
                   0,--CURR_DEPR_EXPENSE
                   nvl(A.DEPR_RESERVE,0),
                   nvl(A.BEG_RESERVE_YEAR,0),
                   nvl(A.YTD_DEPR_EXPENSE,0),
                   nvl(A.YTD_DEPR_EXP_ADJUST,0),
                   nvl(A.PRIOR_YTD_DEPR_EXPENSE,0),
                   nvl(A.PRIOR_YTD_DEPR_EXP_ADJUST,0),
                   A.ACCT_DISTRIB,
                   nvl(A.MONTH_RATE,0),
                   A.COMPANY_ID,
                   UPPER(B.MID_PERIOD_METHOD),
                   DECODE(GL_POSTING_MO_YR, A_MONTH, NVL(B.MID_PERIOD_CONV, 0), A.MID_PERIOD_CONV) MID_PERIOD_CONV,
                   DECODE(GL_POSTING_MO_YR, A_MONTH, B.DEPR_GROUP_ID, A.DEPR_GROUP_ID) DEPR_GROUP_ID,
                   0,--DEPR_EXP_ALLOC_ADJUST,
                   NVL(A.DEPR_METHOD_ID, B.DEPR_METHOD_ID) DEPR_METHOD_ID,
                   DECODE(GL_POSTING_MO_YR, A_MONTH, NVL(B.TRUE_UP_CPR_DEPR, 2), A.TRUE_UP_CPR_DEPR) TRUE_UP_CPR_DEPR,
                   0,--A.SALVAGE_EXPENSE,
                   nvl(A.SALVAGE_EXP_ADJUST,0),
                   0,--SALVAGE_EXP_ALLOC_ADJUST,
                   nvl(A.IMPAIRMENT_ASSET_AMOUNT,0),
                   nvl(A.IMPAIRMENT_EXPENSE_AMOUNT,0),
                   C.END_OF_LIFE,
                   C.NET_GROSS,
                   C.OVER_DEPR_CHECK,
                   nvl(C.RATE,0),
                   C.EFFECTIVE_DATE,
                   B.SUBLEDGER_TYPE_ID,
                   D.ENG_IN_SERVICE_YEAR,
                   DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                                      B.COMPANY_ID),
                                    'no')),
                          'no',
                          1,
                          0),
               DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('CPR DEPR: Allow Exp in Month Added',
                                                                                      B.COMPANY_ID),
                                    'yes')),
                          'no',
                          0,
                          -1
                          ),
                    0,--TRUEUP_ADJ
                    0,0,0,0,0,
                    0,--EXISTS_TWO_MONTHS
                    0--EXISTS_ARO
              from CPR_DEPR_VIEW A, DEPR_GROUP B, DEPR_METHOD_RATES_VIEW C, CPR_LEDGER D
             where B.COMPANY_ID = A.COMPANY_ID
               and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
               and B.SUBLEDGER_TYPE_ID = A_SUBLEDGER
               and A.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
               and NVL(A.DEPR_METHOD_ID, B.DEPR_METHOD_ID) = C.DEPR_METHOD_ID
               and A.ASSET_ID = D.ASSET_ID
               and C.THE_ROW = 1;

      forall I in indices of G_IGNOREINACTIVEYES
         insert into CPR_DEPR_CALC_STG
            (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
             ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS,
             DEPR_CALC_STATUS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH,
             SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN,
             RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
             DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE,
             YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB,
             MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID,
             DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE,
             SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT, END_OF_LIFE, NET_GROSS, OVER_DEPR_CHECK, RATE,
             EFFECTIVE_DATE, SUBLEDGER_TYPE_ID, ENG_IN_SERVICE_YEAR, TRF_WEIGHT, MIN_MPC, TRUEUP_ADJ,
             NET_TRF, ACTIVITY, ACTIVITY_3, BEG_RES_AMT, NET_IMP_AMT, EXISTS_TWO_MONTHS, EXISTS_ARO)
            with CPR_DEPR_VIEW as
             (select *
                from CPR_DEPR
               where GL_POSTING_MO_YR = A_MONTH
                 and COMPANY_ID = G_IGNOREINACTIVEYES(I)),
            DEPR_METHOD_RATES_VIEW as
             (select DD.DEPR_METHOD_ID,
                     DD.SET_OF_BOOKS_ID,
                     DD.EFFECTIVE_DATE,
                     DD.RATE,
                     DD.OVER_DEPR_CHECK,
                     DD.NET_GROSS,
                     DD.END_OF_LIFE,
                     ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
                from DEPR_METHOD_RATES DD
               where DD.EFFECTIVE_DATE <= A_MONTH)
            select A.ASSET_ID,
                   A.SET_OF_BOOKS_ID,
                   A.GL_POSTING_MO_YR,
                   nvl(A.INIT_LIFE,0),
                   nvl(A.REMAINING_LIFE,0),
                   nvl(A.ESTIMATED_SALVAGE,0),
                   nvl(A.BEG_ASSET_DOLLARS,0),
                   nvl(A.NET_ADDS_AND_ADJUST,0),
                   nvl(A.RETIREMENTS,0),
                   2,
                   nvl(A.TRANSFERS_IN,0),
                   nvl(A.TRANSFERS_OUT,0),
                   nvl(A.ASSET_DOLLARS,0),
                   nvl(A.BEG_RESERVE_MONTH,0),
                   nvl(A.SALVAGE_DOLLARS,0),
                   nvl(A.RESERVE_ADJUSTMENT,0),
                   nvl(A.COST_OF_REMOVAL,0),
                   nvl(A.RESERVE_TRANS_IN,0),
                   nvl(A.RESERVE_TRANS_OUT,0),
                   nvl(A.DEPR_EXP_ADJUST,0),
                   nvl(A.OTHER_CREDITS_AND_ADJUST,0),
                   nvl(A.GAIN_LOSS,0),
                   0,--A.DEPRECIATION_BASE,
                   0,--CURR_DEPR_EXPENSE
                   nvl(A.DEPR_RESERVE,0),
                   nvl(A.BEG_RESERVE_YEAR,0),
                   nvl(A.YTD_DEPR_EXPENSE,0),
                   nvl(A.YTD_DEPR_EXP_ADJUST,0),
                   nvl(A.PRIOR_YTD_DEPR_EXPENSE,0),
                   nvl(A.PRIOR_YTD_DEPR_EXP_ADJUST,0),
                   A.ACCT_DISTRIB,
                   nvl(A.MONTH_RATE,0),
                   A.COMPANY_ID,
                   UPPER(B.MID_PERIOD_METHOD),
                   DECODE(GL_POSTING_MO_YR, A_MONTH, NVL(B.MID_PERIOD_CONV, 0), A.MID_PERIOD_CONV) MID_PERIOD_CONV,
                   DECODE(GL_POSTING_MO_YR, A_MONTH, B.DEPR_GROUP_ID, A.DEPR_GROUP_ID) DEPR_GROUP_ID,
                   0,--A.DEPR_EXP_ALLOC_ADJUST,
                   NVL(A.DEPR_METHOD_ID, B.DEPR_METHOD_ID) DEPR_METHOD_ID,
                   DECODE(GL_POSTING_MO_YR, A_MONTH, NVL(B.TRUE_UP_CPR_DEPR, 2), A.TRUE_UP_CPR_DEPR) TRUE_UP_CPR_DEPR,
                   0,--A.SALVAGE_EXPENSE,
                   nvl(A.SALVAGE_EXP_ADJUST,0),
                   0,--A.SALVAGE_EXP_ALLOC_ADJUST,
                   nvl(A.IMPAIRMENT_ASSET_AMOUNT,0),
                   nvl(A.IMPAIRMENT_EXPENSE_AMOUNT,0),
                   C.END_OF_LIFE,
                   C.NET_GROSS,
                   C.OVER_DEPR_CHECK,
                   nvl(C.RATE,0),
                   C.EFFECTIVE_DATE,
                   B.SUBLEDGER_TYPE_ID,
                   D.ENG_IN_SERVICE_YEAR,
                   DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                                      B.COMPANY_ID),
                                    'no')),
                          'no',
                          1,
                          0),
               DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('CPR DEPR: Allow Exp in Month Added',
                                                                                      B.COMPANY_ID),
                                    'yes')),
                          'no',
                          0,
                          -1),
                    0,--TRUEUP_ADJ
                    0,0,0,0,0,
                    0,--EXISTS_TWO_MONTHS
                    0--EXISTS_ARO
              from CPR_DEPR_VIEW A, DEPR_GROUP B, DEPR_METHOD_RATES_VIEW C, CPR_LEDGER D
             where B.COMPANY_ID = A.COMPANY_ID
               and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
               and B.SUBLEDGER_TYPE_ID = A_SUBLEDGER
               and A.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
               and NVL(A.DEPR_METHOD_ID, B.DEPR_METHOD_ID) = C.DEPR_METHOD_ID
               and A.ASSET_ID = D.ASSET_ID
               and C.THE_ROW = 1;

/*   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('ERROR: ' || sqlerrm);
         RAISE_APPLICATION_ERROR(-20000, 'ERROR: ' || sqlerrm); */
   end P_STAGEMONTHENDDEPR;



   --**************************************************************************
   --                            F_CALCBASEANDRATE
   --**************************************************************************
   function F_CALCBASEANDRATE return number is
      L_STATUS varchar2(255);
   begin
      merge into CPR_DEPR_CALC_STG A
      using (select *
               from CPR_DEPR_CALC_STG B
              where B.DEPR_CALC_STATUS = 2 MODEL partition
              by(ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR) DIMENSION by(MID_PERIOD_METHOD)
              MEASURES(MONTH_RATE,
                             DEPRECIATION_BASE,
                             CURR_DEPR_EXPENSE,

                             REMAINING_LIFE,
                             NET_GROSS,
                             TRUE_UP_CPR_DEPR,
                             MID_PERIOD_CONV,
                             ENG_IN_SERVICE_YEAR,
                             PRODUCTION,
                             ESTIMATED_PRODUCTION,
                             RATE,
                             INIT_LIFE,
                             BEG_ASSET_DOLLARS,
                             NET_ADDS_AND_ADJUST,
                             RETIREMENTS,
                             TRANSFERS_IN,
                             TRANSFERS_OUT,
                             IMPAIRMENT_ASSET_AMOUNT,
                             ESTIMATED_SALVAGE,
                             BEG_RESERVE_MONTH,
                             DEPR_EXP_ALLOC_ADJUST,
                             DEPR_EXP_ADJUST,
                             SALVAGE_DOLLARS,
                             COST_OF_REMOVAL,
                             OTHER_CREDITS_AND_ADJUST,
                             GAIN_LOSS,
                             RESERVE_TRANS_IN,
                             RESERVE_TRANS_OUT,
                             RESERVE_ADJUSTMENT,
                             IMPAIRMENT_EXPENSE_AMOUNT,
                             TRF_WEIGHT,
                             EXISTS_LAST_MONTH,
                             HAS_CFNU,
                             DEPR_METHOD_SLE_EXP,
                             NET_TRF,
                             NET_RES_TRF,
                             NET_IMP_AMT,
                             ACTIVITY,
                             BEG_RES_AMT,
                             ACTIVITY_3,
                             BEG_BAL_AMT,
                             SCH_RATE) RULES
              update(NET_TRF [ any ] = TRANSFERS_IN [ CV(MID_PERIOD_METHOD) ] + TRANSFERS_OUT [
                           CV(MID_PERIOD_METHOD) ],
                           NET_RES_TRF [ any ] = RESERVE_TRANS_IN [ CV(MID_PERIOD_METHOD)
                           ] + RESERVE_TRANS_OUT [ CV(MID_PERIOD_METHOD) ],

                           NET_IMP_AMT [ any ] = IMPAIRMENT_EXPENSE_AMOUNT [ CV(MID_PERIOD_METHOD)
                           ] + IMPAIRMENT_ASSET_AMOUNT [ CV(MID_PERIOD_METHOD) ],

                           ACTIVITY [ any ] = NET_ADDS_AND_ADJUST [ CV(MID_PERIOD_METHOD)
                           ] + RETIREMENTS [ CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [
                           CV(MID_PERIOD_METHOD) ],
                           -- check to see if activity includes net trfs
                           ACTIVITY [ any ] = case
                              when TRF_WEIGHT [ CV(MID_PERIOD_METHOD) ] = 0 then
                               ACTIVITY [ CV(MID_PERIOD_METHOD) ] + NET_TRF [ CV(MID_PERIOD_METHOD) ]
                              else
                               ACTIVITY [ CV(MID_PERIOD_METHOD) ]
                           end,

                           BEG_BAL_AMT [ any ] = case
                              when TRF_WEIGHT [ CV(MID_PERIOD_METHOD) ] = 0 then
                               BEG_ASSET_DOLLARS [ CV(MID_PERIOD_METHOD) ]
                              else
                               BEG_ASSET_DOLLARS [ CV(MID_PERIOD_METHOD) ] + NET_TRF [ CV(MID_PERIOD_METHOD) ]
                           end,

                           BEG_RES_AMT [ any ] = case
                              when TRF_WEIGHT [ CV(MID_PERIOD_METHOD) ] = 0 then
                               BEG_RESERVE_MONTH [ CV(MID_PERIOD_METHOD) ] + DEPR_EXP_ALLOC_ADJUST [
                               CV(MID_PERIOD_METHOD) ]
                              else
                               BEG_RESERVE_MONTH [ CV(MID_PERIOD_METHOD) ] + DEPR_EXP_ALLOC_ADJUST [
                               CV(MID_PERIOD_METHOD) ] + NET_RES_TRF [ CV(MID_PERIOD_METHOD) ]
                           end,

                           ACTIVITY_3 [ any ] = RETIREMENTS [ CV(MID_PERIOD_METHOD)
                           ] + DEPR_EXP_ADJUST [ CV(MID_PERIOD_METHOD) ] + SALVAGE_DOLLARS [
                           CV(MID_PERIOD_METHOD) ] + COST_OF_REMOVAL [ CV(MID_PERIOD_METHOD)
                           ] + OTHER_CREDITS_AND_ADJUST [ CV(MID_PERIOD_METHOD) ] + GAIN_LOSS [
                           CV(MID_PERIOD_METHOD) ] + RESERVE_ADJUSTMENT [ CV(MID_PERIOD_METHOD) ],
                           -- check to see if activity includes net trfs
                           ACTIVITY_3 [ any ] = case
                              when TRF_WEIGHT [ CV(MID_PERIOD_METHOD) ] = 0 then
                               ACTIVITY_3 [ CV(MID_PERIOD_METHOD) ] + NET_RES_TRF [ CV(MID_PERIOD_METHOD) ]
                              else
                               ACTIVITY_3 [ CV(MID_PERIOD_METHOD) ]
                           end,

                           --Rate calcs
                           MONTH_RATE [ MID_PERIOD_METHOD in ('SLEOL', 'SL') ] = case
                              when REMAINING_LIFE [ CV(MID_PERIOD_METHOD) ] >= 1 then
                               1 / REMAINING_LIFE [ CV(MID_PERIOD_METHOD) ]
                              else
                               1
                           end,

                           MONTH_RATE [ MID_PERIOD_METHOD in ('SOMD') ] = case
                              when REMAINING_LIFE [ CV(MID_PERIOD_METHOD) ] >= 1 then
                               REMAINING_LIFE [ CV(MID_PERIOD_METHOD)
                               ] / ((INIT_LIFE [ CV(MID_PERIOD_METHOD)
                                ] * (INIT_LIFE [ CV(MID_PERIOD_METHOD) ] + 1)) / 2)
                              else
                               1
                           end,

                           MONTH_RATE [ MID_PERIOD_METHOD in ('UOPR') ] = case
                              when ESTIMATED_PRODUCTION [ CV(MID_PERIOD_METHOD) ] = 0 then
                               0
                              when PRODUCTION [ CV(MID_PERIOD_METHOD) ] > ESTIMATED_PRODUCTION [
                               CV(MID_PERIOD_METHOD) ] then
                               1
                              else
                               PRODUCTION [ CV(MID_PERIOD_METHOD) ] / ESTIMATED_PRODUCTION [
                               CV(MID_PERIOD_METHOD) ]
                           end,

                           MONTH_RATE [ MID_PERIOD_METHOD in ('RATE') ] = RATE [
                           CV(MID_PERIOD_METHOD) ] / 12,

                           MONTH_RATE [ MID_PERIOD_METHOD in ('DB200') ] = case
                              when REMAINING_LIFE [ CV(MID_PERIOD_METHOD) ] > 0 then
                               LEAST(1,
                                     GREATEST(2 / INIT_LIFE [ CV(MID_PERIOD_METHOD) ],
                                              1 / REMAINING_LIFE [ CV(MID_PERIOD_METHOD) ]))
                              else
                               1
                           end,

                           MONTH_RATE [ MID_PERIOD_METHOD in ('DB150') ] = case
                              when REMAINING_LIFE [ CV(MID_PERIOD_METHOD) ] > 0 then
                               LEAST(1,
                                     GREATEST(1.5 / INIT_LIFE [ CV(MID_PERIOD_METHOD) ],
                                              1 / REMAINING_LIFE [ CV(MID_PERIOD_METHOD) ]))
                              else
                               1
                           end,

                           MONTH_RATE [ MID_PERIOD_METHOD in ('SLE') ] = 1,

                           MONTH_RATE [ MID_PERIOD_METHOD in ('SCH') ] = SCH_RATE [
                           CV(MID_PERIOD_METHOD) ],

                           --Base calcs

                           -- SET MID_PERIOD_CONV TO BE A 1 in certain cases
                           MID_PERIOD_CONV [ any ] = case
                              when (REMAINING_LIFE [ CV(MID_PERIOD_METHOD) ] = INIT_LIFE [
                                    CV(MID_PERIOD_METHOD) ]) or
                                   (TRUE_UP_CPR_DEPR [ CV(MID_PERIOD_METHOD) ] != 1 and EXISTS_LAST_MONTH [
                                    CV(MID_PERIOD_METHOD) ] != 1) then
                               MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD) ]
                              else
                               1
                           end,

                           -- certain methods are ALWAYS based on net
                           NET_GROSS [ MID_PERIOD_METHOD in ('SLEOL', 'SL', 'SOMD', 'DB200', 'DB150')
                           ] = 1,

                           DEPRECIATION_BASE [ MID_PERIOD_METHOD not in ('SLE') ] =
                           --if method is sleol, somd, db200, db150 or net-gross = 1 then use this formula
                            case
                               when NET_GROSS [ CV(MID_PERIOD_METHOD) ] = 1 then
                                case
                                   when (BEG_ASSET_DOLLARS [ CV(MID_PERIOD_METHOD) ] + ACTIVITY [
                                         CV(MID_PERIOD_METHOD) ]) *
                                        (1 - ESTIMATED_SALVAGE [ CV(MID_PERIOD_METHOD) ]) -
                                        (BEG_RES_AMT [ CV(MID_PERIOD_METHOD) ] + ACTIVITY_3 [
                                         CV(MID_PERIOD_METHOD) ] + NET_RES_TRF [ CV(MID_PERIOD_METHOD)
                                         ] + NET_IMP_AMT [ CV(MID_PERIOD_METHOD) ]) = 0 then
                                    0
                                   else
                                    (BEG_ASSET_DOLLARS [ CV(MID_PERIOD_METHOD)
                                     ] +
                                     (ACTIVITY [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD) ])) *
                                    (1 - ESTIMATED_SALVAGE [ CV(MID_PERIOD_METHOD) ]) -
                                    (BEG_RES_AMT [ CV(MID_PERIOD_METHOD) ] + NET_IMP_AMT [ CV(MID_PERIOD_METHOD)
                                     ] + ((ACTIVITY_3 [ CV(MID_PERIOD_METHOD) ] + NET_IMP_AMT [
                                      CV(MID_PERIOD_METHOD) ]) * MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD) ]))
                                end
                            --mid-period method is uopr/rate/sch AND net gross = 2, so use this formula
                               else
                                case
                                   when (MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD) ] = 0) and
                                        (EXISTS_LAST_MONTH [ CV(MID_PERIOD_METHOD) ] != 1) and
                                        (HAS_CFNU [ CV(MID_PERIOD_METHOD) ] = 1) and
                                        (ENG_IN_SERVICE_YEAR [ CV(MID_PERIOD_METHOD)
                                         ] <= ADD_MONTHS(CV(GL_POSTING_MO_YR), -1)) then
                                    BEG_ASSET_DOLLARS [ CV(MID_PERIOD_METHOD) ] + ACTIVITY [
                                    CV(MID_PERIOD_METHOD) ]
                                   when (BEG_ASSET_DOLLARS [ CV(MID_PERIOD_METHOD) ] + ACTIVITY [
                                         CV(MID_PERIOD_METHOD) ]) = 0 then
                                    0
                                   else
                                    BEG_ASSET_DOLLARS [ CV(MID_PERIOD_METHOD)
                                    ] +
                                    (ACTIVITY [ CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [ CV(MID_PERIOD_METHOD) ])
                                end
                            end,

                           DEPRECIATION_BASE [ MID_PERIOD_METHOD in ('SLE') ] = DEPR_METHOD_SLE_EXP [
                           CV(MID_PERIOD_METHOD) ],

                           --Expense calc for all methods
                           CURR_DEPR_EXPENSE [ any ] = DEPRECIATION_BASE [ CV(MID_PERIOD_METHOD)
                           ] * MONTH_RATE [ CV(MID_PERIOD_METHOD) ])) B
      on (A.ASSET_ID = B.ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR)
      when matched then
         update
            set DEPRECIATION_BASE = B.DEPRECIATION_BASE, MONTH_RATE = B.MONTH_RATE,
                CURR_DEPR_EXPENSE = B.CURR_DEPR_EXPENSE, NET_GROSS = B.NET_GROSS,
                ACTIVITY = B.ACTIVITY,
                ACTIVITY_3 = B.ACTIVITY_3, BEG_BAL_AMT = B.BEG_BAL_AMT,
                BEG_RES_AMT = B.BEG_RES_AMT, NET_TRF = B.NET_TRF,
                NET_RES_TRF = B.NET_RES_TRF,
                NET_IMP_AMT = B.NET_IMP_AMT,
                DEPR_CALC_MESSAGE = 'Staged';
      return 1;
   exception
      when others then
         L_STATUS := sqlerrm;
         update CPR_DEPR_CALC_STG set DEPR_CALC_MESSAGE = 'Error while calculating base and rate: ' || L_STATUS;
         return -1;
   end F_CALCBASEANDRATE;

   --**************************************************************************
   --                            F_CALCREMLIFE
   --**************************************************************************
   function F_CALCREMLIFE return number is
      L_STATUS varchar2(255);
   begin

    --Set the remaining life on DEPR stg for first months
    update cpr_depr_calc_stg a
    set remaining_life =
         greatest(0,
            least(a.init_life,
               a.init_life + months_between(to_date(to_char(a.eng_in_service_year,'yyyymm'),'yyyymm'), a.gl_posting_mo_yr) + (1 - a.mid_period_conv)
            )
         )
      where EXISTS_LAST_MONTH = 0
      and EXISTS_ARO = 0
      and lower(trim(a.mid_period_method)) not in ( 'custom' )
      and true_up_cpr_depr in (1, 2)
      and mid_period_conv > min_mpc
      and remaining_life = init_life
    ;

    --Set the remaining life on DEPR stg for existing assets with 0 mid period conv
    update cpr_depr_calc_stg a
    set remaining_life =
         greatest(0,
            least(a.init_life,
               a.init_life + months_between(to_date(to_char(a.eng_in_service_year,'yyyymm'),'yyyymm'), a.gl_posting_mo_yr) + (1 - a.mid_period_conv)
            )
         )
    where EXISTS_LAST_MONTH = 1
   and EXISTS_TWO_MONTHS = 0
    and EXISTS_ARO = 0
    and lower(trim(a.mid_period_method)) not in ( 'custom' )
   and true_up_cpr_depr in (1, 2)
    and mid_period_conv = 0
    and remaining_life = init_life
    ;

    return 1;
   exception
      when others then
         L_STATUS := sqlerrm;
         update CPR_DEPR_CALC_STG set DEPR_CALC_MESSAGE = 'Error while calculating remaining life: ' || L_STATUS;
         return -1;
   end F_CALCREMLIFE;
   --**************************************************************************
   --                            F_TRUEUP
   --**************************************************************************
   function F_TRUEUP return number is
      L_STATUS varchar2(255);
   begin
    merge into CPR_DEPR_CALC_STG A
      using (select * from (
                select *
                  from CPR_DEPR_CALC_STG B
                  where B.DEPR_CALC_STATUS = 2
                    and true_up_cpr_depr = 1
                    and upper(mid_period_method) not in ('SCH', 'SLE', 'CUSTOM')
                    and exists_aro = 0
                    and mid_period_conv > min_mpc
                union all
                select *
                  from CPR_DEPR_CALC_STG C
                  where C.DEPR_CALC_STATUS = 2
                    and true_up_cpr_depr = 2
                    and exists_last_month = 0
                    and upper(mid_period_method) not in ('SCH', 'SLE', 'CUSTOM')
                    and exists_aro = 0
                    and mid_period_conv > min_mpc
                union all
                select *
                  from CPR_DEPR_CALC_STG
                  where DEPR_CALC_STATUS = 2
                    and true_up_cpr_depr = 2
                    and exists_last_month = 1
                    and exists_two_months = 0
                    and upper(mid_period_method) not in ('SCH', 'SLE', 'CUSTOM')
                    and exists_aro = 0
                    and mid_period_conv = 0
              )
              MODEL
              PARTITION BY (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR)
              DIMENSION by (MID_PERIOD_METHOD)
              MEASURES (MONTH_RATE,
                             DEPRECIATION_BASE,
                             CURR_DEPR_EXPENSE,

                             REMAINING_LIFE,
                             NET_GROSS,
                             TRUE_UP_CPR_DEPR,
                             MID_PERIOD_CONV,
                             ENG_IN_SERVICE_YEAR,
                             PRODUCTION,
                             ESTIMATED_PRODUCTION,
                             RATE,
                             INIT_LIFE,
                             BEG_ASSET_DOLLARS,
                             NET_ADDS_AND_ADJUST,
                             RETIREMENTS,
                             TRANSFERS_IN,
                             TRANSFERS_OUT,
                             IMPAIRMENT_ASSET_AMOUNT,
                             ESTIMATED_SALVAGE,
                             BEG_RESERVE_MONTH,
                             DEPR_EXP_ALLOC_ADJUST,
                             DEPR_EXP_ADJUST,
                             SALVAGE_DOLLARS,
                             COST_OF_REMOVAL,
                             OTHER_CREDITS_AND_ADJUST,
                             GAIN_LOSS,
                             RESERVE_TRANS_IN,
                             RESERVE_TRANS_OUT,
                             RESERVE_ADJUSTMENT,
                             IMPAIRMENT_EXPENSE_AMOUNT,
                             TRF_WEIGHT,
                             EXISTS_LAST_MONTH,
                             HAS_CFNU,
                             DEPR_METHOD_SLE_EXP,
                             NET_TRF,
                             NET_RES_TRF,
                             NET_IMP_AMT,
                             ACTIVITY,
                             BEG_RES_AMT,
                             ACTIVITY_3,
                             BEG_BAL_AMT,
                             SCH_RATE,
                             TRUEUP_ADJ)
              RULES update(
                            TRUEUP_ADJ [ MID_PERIOD_METHOD in ('DB200') ] =
                              case
                                when sign(REMAINING_LIFE[CV(MID_PERIOD_METHOD)] - (INIT_LIFE[CV(MID_PERIOD_METHOD)]/2)) = 1 then
                                  DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                                  (1 - power((1-(2/INIT_LIFE[CV(MID_PERIOD_METHOD)])), INIT_LIFE[CV(MID_PERIOD_METHOD)] - REMAINING_LIFE[CV(MID_PERIOD_METHOD)]))
                                else
                                  DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                                  (1 - POWER((1 - (2/init_life[CV(MID_PERIOD_METHOD)])),(init_life[CV(MID_PERIOD_METHOD)]-TRUNC(init_life[CV(MID_PERIOD_METHOD)]/2,0)))) +
                                  (
                                    (trunc(INIT_LIFE[CV(MID_PERIOD_METHOD)]/2,0) - REMAINING_LIFE[CV(MID_PERIOD_METHOD)]) *
                                           (DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] -
                                      (
                                        DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                                        (1 - power((1 - (2/init_life[CV(MID_PERIOD_METHOD)])),(init_life[CV(MID_PERIOD_METHOD)]-trunc(init_life[CV(MID_PERIOD_METHOD)]/2,0))))
                                      )
                                    ) /
                                           (TRUNC(init_life[CV(MID_PERIOD_METHOD)]/2,0))
                                  )
                              end,

                            TRUEUP_ADJ [ MID_PERIOD_METHOD in ('DB150') ] =
                              case
                                when sign(REMAINING_LIFE[CV(MID_PERIOD_METHOD)] - (INIT_LIFE[CV(MID_PERIOD_METHOD)]/2)) = 1 then
                                  DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                                  (1 - power((1-(1.5/INIT_LIFE[CV(MID_PERIOD_METHOD)])), INIT_LIFE[CV(MID_PERIOD_METHOD)] - REMAINING_LIFE[CV(MID_PERIOD_METHOD)]))
                                else
                                  DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                                  (1 - POWER((1 - (1.5/init_life[CV(MID_PERIOD_METHOD)])),(init_life[CV(MID_PERIOD_METHOD)]-TRUNC(init_life[CV(MID_PERIOD_METHOD)]/1.5,0)))) +
                                  (
                                    (trunc(INIT_LIFE[CV(MID_PERIOD_METHOD)]/1.5,0) - REMAINING_LIFE[CV(MID_PERIOD_METHOD)]) *
                                           (DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] -
                                      (
                                        DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                                        (1 - power((1 - (1.5/init_life[CV(MID_PERIOD_METHOD)])),(init_life[CV(MID_PERIOD_METHOD)]-trunc(init_life[CV(MID_PERIOD_METHOD)]/1.5,0))))
                                      )
                                    ) /
                                           (TRUNC(init_life[CV(MID_PERIOD_METHOD)]/1.5,0))
                                  )
                              end,

                            TRUEUP_ADJ [ MID_PERIOD_METHOD in ('SOMD') ] =
                              DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                              (1 - (REMAINING_LIFE[CV(MID_PERIOD_METHOD)] * (REMAINING_LIFE[CV(MID_PERIOD_METHOD)]+1) /
                                    (INIT_LIFE[CV(MID_PERIOD_METHOD)] * (INIT_LIFE[CV(MID_PERIOD_METHOD)]+1))
                                   )
                              ),

                            TRUEUP_ADJ [ MID_PERIOD_METHOD in ('RATE', 'UOPR') ] =
                              DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                              (1 - power((1-MONTH_RATE[CV(MID_PERIOD_METHOD)]),
                                        (INIT_LIFE[CV(MID_PERIOD_METHOD)]-REMAINING_LIFE[CV(MID_PERIOD_METHOD)]))
                              ),

                            TRUEUP_ADJ [ MID_PERIOD_METHOD in ('SL', 'SLEOL') ] =
                              DEPRECIATION_BASE[CV(MID_PERIOD_METHOD)] *
                              (INIT_LIFE[CV(MID_PERIOD_METHOD)]-REMAINING_LIFE[CV(MID_PERIOD_METHOD)]) /
                                INIT_LIFE[CV(MID_PERIOD_METHOD)]


                          )
          ) B
      on (A.ASSET_ID = B.ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR)
      when matched then
         update
            set TRUEUP_ADJ = B.TRUEUP_ADJ;

      update CPR_DEPR_CALC_STG
      set TRUEUP_ADJ = DEPRECIATION_BASE
      where DEPR_CALC_STATUS = 2
      and remaining_life = 0;


      update CPR_DEPR_CALC_STG
      set DEPRECIATION_BASE = DEPRECIATION_BASE - TRUEUP_ADJ
      where TRUEUP_ADJ <> 0
      and DEPR_CALC_STATUS = 2
--      and (
--        (
--          true_up_cpr_depr in (1)
--          and upper(mid_period_method) not in ('SCH', 'SLE', 'CUSTOM')
--          and exists_aro = 0
--          and mid_period_conv > min_mpc
--        )
--        or
--        (
--          true_up_cpr_depr = 2
--          and exists_last_month = 0
--          and upper(mid_period_method) not in ('SCH', 'SLE', 'CUSTOM')
--          and exists_aro = 0
--          and mid_period_conv > min_mpc
--        )
--        or
--        (
--          true_up_cpr_depr = 2
--          and exists_last_month = 1
--          and exists_two_months = 0
--          and upper(mid_period_method) not in ('SCH', 'SLE', 'CUSTOM')
--          and exists_aro = 0
--          and mid_period_conv = 0
--        )
--      )
      ;

    return 1;
   exception
      when others then
         L_STATUS := sqlerrm;
         update CPR_DEPR_CALC_STG set DEPR_CALC_MESSAGE = 'Error while calculating true up: ' || L_STATUS;
         return -1;
   end F_TRUEUP;

   --**************************************************************************
   --                            F_SETDEPREXPENSE
   --**************************************************************************
   function F_SETDEPREXPENSE return number is
      L_STATUS varchar2(255);
   begin

    update CPR_DEPR_CALC_STG
    set CURR_DEPR_EXPENSE = DEPRECIATION_BASE * MONTH_RATE,
    DEPR_RESERVE = BEG_RESERVE_MONTH + RETIREMENTS + TRUEUP_ADJ + DEPR_EXP_ADJUST
      + SALVAGE_DOLLARS + COST_OF_REMOVAL + OTHER_CREDITS_AND_ADJUST + GAIN_LOSS
      + RESERVE_TRANS_IN + RESERVE_TRANS_OUT + (DEPRECIATION_BASE*MONTH_RATE)
      + RESERVE_ADJUSTMENT + IMPAIRMENT_ASSET_AMOUNT + IMPAIRMENT_EXPENSE_AMOUNT,
    ASSET_DOLLARS = BEG_ASSET_DOLLARS + NET_ADDS_AND_ADJUST + RETIREMENTS
      + TRANSFERS_IN + TRANSFERS_OUT + IMPAIRMENT_ASSET_AMOUNT
    ;

    return 1;
   exception
      when others then
         L_STATUS := sqlerrm;
         update CPR_DEPR_CALC_STG set DEPR_CALC_MESSAGE = 'Error while setting depr expense: ' || L_STATUS;
         return -1;
   end F_SETDEPREXPENSE;

   --**************************************************************************
   --                            P_DEPRCALC
   --**************************************************************************
   procedure P_DEPRCALC is
      L_STATUS varchar2(255);
   begin

      if F_CALCBASEANDRATE() <> 1 then
        return;
      end if;
      if F_CALCREMLIFE() <> 1 then
        return;
      end if;
      if F_TRUEUP() <> 1 then
        return;
      end if;
      if F_SETDEPREXPENSE() <> 1 then
        return;
      end if;

      delete from cpr_depr_calc_stg_arc a
      where exists (
        select 1
        from cpr_depr_calc_stg b
        where b.asset_id = a.asset_id
        and b.set_of_books_id = a.set_of_books_id
        and b.gl_posting_mo_yr = a.gl_posting_mo_yr
      );

      insert into cpr_depr_calc_stg_arc
            (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, TIME_STAMP, USER_ID, INIT_LIFE, REMAINING_LIFE, ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH, SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS, DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE, YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID, DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, DEPRECIATION_INDICATOR, END_OF_LIFE, ENG_IN_SERVICE_YEAR, ESTIMATED_PRODUCTION, HAS_CFNU, HAS_NURV, HAS_NURV_LAST_MONTH, NET_GROSS, OVER_DEPR_CHECK, PRODUCTION, RATE, EXISTS_LAST_MONTH, EXISTS_TWO_MONTHS, EXISTS_ARO, EFFECTIVE_DATE, TRF_WEIGHT, DEPR_CALC_MESSAGE, SUBLEDGER_TYPE_ID, DEPR_CALC_STATUS, NET_TRF, NET_RES_TRF, NET_IMP_AMT, ACTIVITY, BEG_RES_AMT, ACTIVITY_3, DEPR_METHOD_SLE_EXP, BEG_BAL_AMT, SCH_RATE, SCH_FACTOR, MIN_MPC, TRUEUP_ADJ)
      select ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, TIME_STAMP, USER_ID, INIT_LIFE, REMAINING_LIFE, ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH, SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS, DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE, YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID, DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, DEPRECIATION_INDICATOR, END_OF_LIFE, ENG_IN_SERVICE_YEAR, ESTIMATED_PRODUCTION, HAS_CFNU, HAS_NURV, HAS_NURV_LAST_MONTH, NET_GROSS, OVER_DEPR_CHECK, PRODUCTION, RATE, EXISTS_LAST_MONTH, EXISTS_TWO_MONTHS, EXISTS_ARO, EFFECTIVE_DATE, TRF_WEIGHT, DEPR_CALC_MESSAGE, SUBLEDGER_TYPE_ID, DEPR_CALC_STATUS, NET_TRF, NET_RES_TRF, NET_IMP_AMT, ACTIVITY, BEG_RES_AMT, ACTIVITY_3, DEPR_METHOD_SLE_EXP, BEG_BAL_AMT, SCH_RATE, SCH_FACTOR, MIN_MPC, TRUEUP_ADJ
      from cpr_depr_calc_stg;

   exception
      when others then
         L_STATUS := sqlerrm;
         update CPR_DEPR_CALC_STG set DEPR_CALC_MESSAGE = 'Error while staging: ' || L_STATUS;
   end P_DEPRCALC;

   --**************************************************************************
   --                            F_BACKFILL
   --**************************************************************************
   function F_BACKFILL return number is
      L_STATUS varchar2(254);
   begin
      --has_cfnu
      update CPR_DEPR_CALC_STG Z
         set HAS_CFNU = 1
       where exists (select 1
                from CPR_ACTIVITY Y
               where Y.ASSET_ID = Z.ASSET_ID
                 and Y.GL_POSTING_MO_YR = Z.GL_POSTING_MO_YR
                 and trim(ACTIVITY_CODE) = 'CFNU');

      --has_nurv
      update CPR_DEPR_CALC_STG Z
         set HAS_NURV = 1
       where exists (select 1
                from CPR_ACTIVITY Y
               where Y.ASSET_ID = Z.ASSET_ID
                 and Y.GL_POSTING_MO_YR = Z.GL_POSTING_MO_YR
                 and trim(ACTIVITY_CODE) = 'NURV');

      --has_nurv_last_month
      update CPR_DEPR_CALC_STG Z
         set HAS_NURV_LAST_MONTH = 1
       where exists (select 1
                from CPR_ACTIVITY Y
               where Y.ASSET_ID = Z.ASSET_ID
                 and Y.GL_POSTING_MO_YR = ADD_MONTHS(Z.GL_POSTING_MO_YR, -1)
                 and trim(ACTIVITY_CODE) = 'NURV');

      --estimated_production
      --production
      update CPR_DEPR_CALC_STG A
         set (ESTIMATED_PRODUCTION, PRODUCTION) =
              (select ESTIMATED_PRODUCTION, PRODUCTION
                 from DEPR_METHOD_UOP UOP
                where UOP.DEPR_METHOD_ID = A.DEPR_METHOD_ID
                  and UOP.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
                  and UOP.GL_POST_MO_YR = A.GL_POSTING_MO_YR)
       where exists (select 1
                from DEPR_METHOD_UOP UOP
               where UOP.DEPR_METHOD_ID = A.DEPR_METHOD_ID
                 and UOP.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
                 and UOP.GL_POST_MO_YR = A.GL_POSTING_MO_YR)
         and LOWER(A.MID_PERIOD_METHOD) = 'uopr';

      --exists_last_month
      update CPR_DEPR_CALC_STG Z
         set EXISTS_LAST_MONTH = 1
       where exists (select 1
                from CPR_DEPR Y
               where Y.ASSET_ID = Z.ASSET_ID
                 and Y.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                 and Y.GL_POSTING_MO_YR = ADD_MONTHS(Z.GL_POSTING_MO_YR, -1));

      --exists_two_months
      update CPR_DEPR_CALC_STG Z
         set EXISTS_TWO_MONTHS = 1
       where exists (select 1
                from CPR_DEPR Y
               where Y.ASSET_ID = Z.ASSET_ID
                 and Y.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                 and Y.GL_POSTING_MO_YR = ADD_MONTHS(Z.GL_POSTING_MO_YR, -2));

      --exists_aro
      update CPR_DEPR_CALC_STG Y
         set EXISTS_ARO = 1
       where exists (select 1 from ARO where ARO.ASSET_ID = Y.ASSET_ID);

      --depr_method_sle_exp
      update CPR_DEPR_CALC_STG STG
         set DEPR_METHOD_SLE_EXP =
              (select DMS.DEPR_EXPENSE
                 from DEPR_METHOD_SLE DMS
                where STG.ASSET_ID = DMS.ASSET_ID
                  and STG.SET_OF_BOOKS_ID = DMS.SET_OF_BOOKS_ID
                  and STG.GL_POSTING_MO_YR = DMS.GL_POSTING_MO_YR)
       where exists (select 1
                from DEPR_METHOD_SLE DMS
               where STG.ASSET_ID = DMS.ASSET_ID
                 and STG.SET_OF_BOOKS_ID = DMS.SET_OF_BOOKS_ID
                 and STG.GL_POSTING_MO_YR = DMS.GL_POSTING_MO_YR)
         and LOWER(STG.MID_PERIOD_METHOD) = 'sle';

      --stuff for sch

      --get the factor
      --TODO: figure out how to get the correct factor (factor_1, factor_2, etc.) depending on gl_posting_mo_yr month and factor_id
      update CPR_DEPR_CALC_STG STG
         set STG.SCH_FACTOR =
              (select TO_CHAR(STG.GL_POSTING_MO_YR, 'mm')
                 from (select SF.FACTOR_1  as "01",
                              SF.FACTOR_2  as "02",
                              SF.FACTOR_3  as "03",
                              SF.FACTOR_4  as "04",
                              SF.FACTOR_5  as "05",
                              SF.FACTOR_6  as "06",
                              SF.FACTOR_7  as "07",
                              SF.FACTOR_8  as "08",
                              SF.FACTOR_9  as "09",
                              SF.FACTOR_10 as "10",
                              SF.FACTOR_11 as "11",
                              SF.FACTOR_12 as "12"
                         from SPREAD_FACTOR SF
                        where SF.FACTOR_ID = 1))
       where LOWER(STG.MID_PERIOD_METHOD) = 'sch';

      --get the yearly rate, divide by 12 to make it monthly, and multiply by the factor
      update CPR_DEPR_CALC_STG STG
         set STG.SCH_RATE =
              (select STG.SCH_FACTOR * NVL(RATE / 12, 0)
                 from DEPR_METHOD_SCHEDULE S, CPR_LEDGER L, DEPR_GROUP G
                where NVL(STG.DEPR_METHOD_ID, G.DEPR_METHOD_ID) = S.DEPR_METHOD_ID
                  and STG.ASSET_ID = L.ASSET_ID
                  and STG.DEPR_GROUP_ID = G.DEPR_GROUP_ID
                  and S.YEAR = TO_NUMBER(TO_CHAR(STG.GL_POSTING_MO_YR, 'yyyy')) -
                      TO_NUMBER(TO_CHAR(L.ENG_IN_SERVICE_YEAR, 'yyyy')) + 1)
       where exists (select 1
                from DEPR_METHOD_SCHEDULE S, CPR_LEDGER L, DEPR_GROUP G
               where NVL(STG.DEPR_METHOD_ID, G.DEPR_METHOD_ID) = S.DEPR_METHOD_ID
                 and STG.ASSET_ID = L.ASSET_ID
                 and STG.DEPR_GROUP_ID = G.DEPR_GROUP_ID
                 and S.YEAR = TO_NUMBER(TO_CHAR(STG.GL_POSTING_MO_YR, 'yyyy')) -
                     TO_NUMBER(TO_CHAR(L.ENG_IN_SERVICE_YEAR, 'yyyy')) + 1)
         and LOWER(STG.MID_PERIOD_METHOD) = 'sch';

      return 1;

   exception
      when others then
         L_STATUS := sqlerrm;
         update CPR_DEPR_CALC_STG set DEPR_CALC_STATUS = -25, DEPR_CALC_MESSAGE = L_STATUS;
         return -1;
   end F_BACKFILL;

   --**************************************************************************
   --                            F_PREPDEPRCALC
   --**************************************************************************
   --
   -- This function performs the depreciation calculation
   -- PREREQS: The staging table cpr_depr_calc_stg is already populated by some other means
   -- ALLOWING: this function to calculate depreciation independently of type of calc or what process initiated
   --

   function F_PREPDEPRCALC return number is

   begin
      if F_BACKFILL() = -1 then
         return -1;
      end if;
      P_DEPRCALC();
      return 1;
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return -1;
   end F_PREPDEPRCALC;

end PKG_PP_CPR_DEPR;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (730, 0, 10, 4, 1, 1, 33375, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033375_system_PKG_PP_CPR_DEPR.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
