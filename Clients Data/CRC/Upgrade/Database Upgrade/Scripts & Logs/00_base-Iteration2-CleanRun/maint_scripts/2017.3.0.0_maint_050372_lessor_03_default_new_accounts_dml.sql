/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050372_lessor_03_default_new_accounts_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/12/2018 Jared Watkins  Populate default values for the new accounts
||============================================================================
*/

update lsr_ilr_group a set incurred_costs_account_id =
(select ini_direct_cost_account_id from lsr_ilr_group b where a.ilr_group_id = b.ilr_group_id),
def_costs_account_id =
(select ini_direct_cost_account_id from lsr_ilr_group b where a.ilr_group_id = b.ilr_group_id),
def_selling_profit_account_id =
(select sell_profit_loss_account_id from lsr_ilr_group b where a.ilr_group_id = b.ilr_group_id);


update lsr_ilr_account a set incurred_costs_account_id =
(select ini_direct_cost_account_id from lsr_ilr_account b where a.ilr_id = b.ilr_id),
def_costs_account_id =
(select ini_direct_cost_account_id from lsr_ilr_account b where a.ilr_id = b.ilr_id),
def_selling_profit_account_id =
(select sell_profit_loss_account_id from lsr_ilr_account b where a.ilr_id = b.ilr_id);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4311, 0, 2017, 3, 0, 0, 50372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050372_lessor_03_default_new_accounts_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;