/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050584_lessor_01_add_rate_type_description_unique_index_ddl.sql
|| Description:	Add unique index to lower(trim(description)) of lsr_ilr_rate_types
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/12/2018 Andrew Hill    Add index
||============================================================================
*/
create unique index lsr_ilr_rate_types_desc_unqidx on lsr_ilr_rate_types(lower(trim(description)));

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4183, 0, 2017, 3, 0, 0, 50584, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050584_lessor_01_add_rate_type_description_unique_index_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;