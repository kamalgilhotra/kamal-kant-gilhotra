/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_043476_reg_replace_ifa_with_layer_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/24/2015 Anand R        Reg - replace IFA with Layer for reports
||============================================================================
*/

update pp_reports set description = 'Layer by Reg Account' where upper(trim(report_number)) = 'RL - 102' ;

update pp_reports set description = 'Reg Account by Layer' where upper(trim(report_number)) = 'RL - 103' ;

update pp_reports set description = 'Layer Data' where upper(trim(report_number)) = 'RL - 104' ;

update pp_reports_filter set description = 'Layer Type-Layer-AcctType-SA Type' where upper(trim(description)) = 'IFATYPE-IFA-ACCTTYPE-SA TYPE' ;

update pp_dynamic_filter set label = 'Reg Layer Type' where upper(trim(label)) = 'REG IFA TYPE';

update pp_dynamic_filter set label = 'Reg Layer' where upper(trim(label)) = 'REG IFA';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2647, 0, 2015, 2, 0, 0, 043476, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043476_reg_replace_ifa_with_layer_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;