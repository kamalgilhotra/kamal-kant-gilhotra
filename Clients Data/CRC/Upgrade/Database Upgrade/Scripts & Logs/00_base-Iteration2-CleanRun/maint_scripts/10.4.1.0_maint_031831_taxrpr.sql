/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031831_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.1.0 08/26/2013 Andrew Scott        New Tax Repair Reports
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select MAX_ID + 1,
          'Tax Only Tax Repairs In-Service',
          'Tax Only Tax Repairs Placed-in-Service Analysis grouped by Company',
          'dw_rpr_rpt_asst_taxrpr_taxonly_analysis',
          'REPAIRS',
          'RPR - 0071',
          13,
          100,
          2,
          34,
          1,
          3,
          0
     from DUAL, (select max(REPORT_ID) MAX_ID from PP_REPORTS);

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select MAX_ID + 1,
          'Tax Only Tax Repairs CWIP/InService',
          'Tax Only Tax Repairs CWIP and In-Service Analysis for all work orders grouped by Company for a span of time. Dates correpond to a point of time when charges were incurred.',
          'dw_rpr_rpt_proj_taxrpr_taxonly_analysis',
          'REPAIRS',
          'RPR - 0091',
          13,
          100,
          2,
          34,
          1,
          3,
          0
     from DUAL, (select max(REPORT_ID) MAX_ID from PP_REPORTS);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (545, 0, 10, 4, 1, 0, 31831, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031831_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;