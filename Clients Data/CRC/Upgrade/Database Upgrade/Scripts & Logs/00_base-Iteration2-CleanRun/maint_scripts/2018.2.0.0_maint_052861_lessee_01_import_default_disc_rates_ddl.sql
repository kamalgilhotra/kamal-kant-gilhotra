/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052861_lessee_01_import_default_disc_rates_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 01/21/2019 Shane "C" Ward   New Import table and mods for Default Discount Rate Type
||============================================================================
*/

--Modify ILR Import to include new Default Disc Rate fields
ALTER TABLE ls_import_ilr ADD default_rate_flag NUMBER(1,0);
ALTER TABLE ls_import_ilr ADD default_rate_flag_xlate varchar2(254);
ALTER TABLE ls_import_ilr ADD borrowing_curr_id NUMBER(22,0);
ALTER TABLE ls_import_ilr ADD borrowing_curr_xlate varchar2(254);

COMMENT ON COLUMN ls_import_ilr.default_rate_flag_xlate IS 'Translation field for Default Discount Rate Flag';
COMMENT ON COLUMN ls_import_ilr.default_rate_flag IS 'Flag for whether ILR will leverage Default Discount Rates';
COMMENT ON COLUMN ls_import_ilr.borrowing_curr_xlate IS 'Translation field for Borrowing Currency ID';
COMMENT ON COLUMN ls_import_ilr.borrowing_curr_id IS 'System Assigned ID of a currency marked as the Borrowing Currency for the Rate Type';

ALTER TABLE ls_import_ilr_archive ADD default_rate_flag NUMBER(1,0);
ALTER TABLE ls_import_ilr_archive ADD default_rate_flag_xlate varchar2(254);
ALTER TABLE ls_import_ilr_archive ADD borrowing_curr_id NUMBER(22,0);
ALTER TABLE ls_import_ilr_archive ADD borrowing_curr_xlate varchar2(254);

COMMENT ON COLUMN ls_import_ilr_archive.default_rate_flag_xlate IS 'Translation field for Default Discount Rate Flag';
COMMENT ON COLUMN ls_import_ilr_archive.default_rate_flag IS 'Flag for whether ILR will leverage Default Discount Rates';
COMMENT ON COLUMN ls_import_ilr_archive.borrowing_curr_xlate IS 'Translation field for Borrowing Currency ID';
COMMENT ON COLUMN ls_import_ilr_archive.borrowing_curr_id IS 'System Assigned ID of a currency marked as the Borrowing Currency for the Rate Type';

--Modify ILR Options to include new Default Rate Type field
ALTER TABLE ls_ilr_options ADD default_rate_flag NUMBER(1,0);
ALTER TABLE ls_ilr_options ADD borrowing_curr_id NUMBER(22,0);

COMMENT ON COLUMN ls_ilr_options.default_rate_flag IS 'Flag for whether ILR will leverage Default Discount Rates';
COMMENT ON COLUMN ls_ilr_options.BORROWING_CURR_ID IS 'System Assigned ID of a currency marked as the Borrowing Currency for the Rate Type';

--New Import Tables
CREATE TABLE LS_IMPORT_DEFAULT_RATES (
IMPORT_RUN_ID NUMBER(22,0) NOT NULL,
LINE_ID NUMBER(22,0) NOT NULL,
RATE_TYPE_XLATE VARCHAR2(254),
RATE_TYPE_ID NUMBER(22,0),
TERM_LENGTH NUMBER(22,0),
EFFECTIVE_DATE VARCHAR2(254),
RATE NUMBER(22,8),
BORROWING_CURR_XLATE VARCHAR2(254),
BORROWING_CURR_ID NUMBER(22,0),
ERROR_MESSAGE VARCHAR2(4000),
USER_ID VARCHAR2(18),
TIME_STAMP DATE);

COMMENT ON TABLE LS_IMPORT_DEFAULT_RATES IS '[S] Lessee Import Staging table for Default Discount Rates import';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.IMPORT_RUN_ID IS 'System Assigned Identifier of a PowerPlan Import Run';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.LINE_ID IS 'System Assigned Identifier of a line in spreadhseet';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.RATE_TYPE_XLATE IS 'Translation field for Default Discount Rate Type ID';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.RATE_TYPE_ID IS 'System Assigned Identifier of a Default Discount Rate Type';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.TERM_LENGTH IS 'Number of terms for an ILR to have for the Default Discount Rate to apply to';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.EFFECTIVE_DATE IS 'Effective Date of Default Discount Rate type (MM/DD/YYYY Format)';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.RATE IS 'Rate to be used by the ILR with the permutaion of other attributes for the line';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.BORROWING_CURR_XLATE IS 'Translation field for Borrowing Currency ID';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.BORROWING_CURR_ID IS 'System Assigned ID of a currency marked as the Borrowing Currency for the Rate Type';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.ERROR_MESSAGE IS 'System populated field displaying issues with imported data by row';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.USER_ID IS 'Standard system-assigned user id used for audit purposes';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes';

CREATE TABLE LS_IMPORT_DEFAULT_RATES_ARC (
IMPORT_RUN_ID NUMBER(22,0) NOT NULL,
LINE_ID NUMBER(22,0) NOT NULL,
RATE_TYPE_XLATE VARCHAR2(254),
RATE_TYPE_ID NUMBER(22,0),
TERM_LENGTH NUMBER(22,0),
EFFECTIVE_DATE VARCHAR2(254),
RATE NUMBER(22,8),
BORROWING_CURR_XLATE VARCHAR2(254),
BORROWING_CURR_ID NUMBER(22,0),
ERROR_MESSAGE VARCHAR2(4000),
USER_ID VARCHAR2(18),
TIME_STAMP DATE);

COMMENT ON TABLE LS_IMPORT_DEFAULT_RATES_ARC IS '[S] Lessee Import ARCHIVE table for Default Discount Rates import runs';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.IMPORT_RUN_ID IS 'System Assigned Identifier of a PowerPlan Import Run';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.LINE_ID IS 'System Assigned Identifier of a line in spreadhseet';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.RATE_TYPE_XLATE IS 'Translation field for Default Discount Rate Type ID';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.RATE_TYPE_ID IS 'System Assigned Identifier of a Default Discount Rate Type';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.TERM_LENGTH IS 'Number of terms for an ILR to have for the Default Discount Rate to apply to';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.EFFECTIVE_DATE IS 'Effective Date of Default Discount Rate type (YYYY/MM/DD Format)';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.RATE IS 'Rate to be used by the ILR with the permutaion of other attributes for the line';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.BORROWING_CURR_XLATE IS 'Translation field for Borrowing Currency ID';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.BORROWING_CURR_ID IS 'System Assigned ID of a currency marked as the Borrowing Currency for the Rate Type';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.ERROR_MESSAGE IS 'System populated field displaying issues with imported data by row';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.USER_ID IS 'Standard system-assigned user id used for audit purposes';
COMMENT ON COLUMN LS_IMPORT_DEFAULT_RATES_ARC.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14262, 0, 2018, 2, 0, 0, 52861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052861_lessee_01_import_default_disc_rates_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;