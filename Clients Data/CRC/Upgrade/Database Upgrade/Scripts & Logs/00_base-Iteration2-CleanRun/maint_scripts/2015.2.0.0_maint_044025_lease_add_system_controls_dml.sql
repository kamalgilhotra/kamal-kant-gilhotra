/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044025_lease_add_system_controls_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/15/2015 Will Davis     Add system controls
||============================================================================
*/

insert into pp_system_control_company
(CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID, USER_DISABLED, CONTROL_TYPE)
values
((select max(control_id) from pp_system_control_company) + 1,
'Hide Lease Department in CPR Panel',
'no',
'Hide Lease Department in CPR Panel',
'If yes, hides the department field from the CPR panel in the leased asset window.  This is for clients that use multiple departments for their expensing',
-1,
null,
null);

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
select
   max(control_id)+1 AS control_id,
   'Lease Term Penalty Amount Included' AS control_name,
   'no' AS control_value,
   'dw_yes_no;1' AS description,
   'If yes, then termination penalty amounts will be included in the payment calculation/invoice reconcilation process and be journalized. No will exclude them, and amounts only will be journalized.' AS long_description,
   -1 AS company_id
from pp_system_control_company
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Lease Term Penalty Amount Included')
;

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
select
   max(control_id)+1 AS control_id,
   'Lease Sales Proceed Amount Included' AS control_name,
   'no' AS control_value,
   'dw_yes_no;1' AS description,
   'If yes, then sales proceeds amounts will be included in the payment calculation/invoice reconcilation process and be journalized. No will exclude them, and amounts only will be journalized.' AS long_description,
   -1 AS company_id
from pp_system_control_company
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Lease Sales Proceed Amount Included')
;

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
select
   max(control_id)+1 AS control_id,
   'Lease do not show MLA rates blocker' AS control_name,
   'no' AS control_value,
   'dw_yes_no;1' AS description,
   'If yes, then lease interim rates can be added on all master leases' AS long_description,
   -1 AS company_id
from pp_system_control_company
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Lease do not show MLA rates blocker')
;

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
select
   max(control_id)+1 AS control_id,
   'Lease Specific GL Trans Status' AS control_name,
   ' ' AS control_value,
   'Lease Specific GL Trans Status' AS description,
   'Lease specific gl transaction status. A space will mean lease transactions are put in with a status of 1. Any other number will override this value.' AS long_description,
   -1 AS company_id
from pp_system_control_company
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Lease Specific GL Trans Status');

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
select
   max(control_id)+1 AS control_id,
   'Lease Catch Up Principal' AS control_name,
   'no' AS control_value,
   'Catch Up principal on Lease Backdated Add' AS description,
   'Catch Up principal on Lease Backdated Addition' AS long_description,
   -1 AS company_id
from pp_system_control_company
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Lease Catch Up Principal');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2608, 0, 2015, 2, 0, 0, 044025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044025_lease_add_system_controls_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;