/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039106_reg_fcst_powertax_logic.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.2.7 07/21/2014 Ryan Oliveria       Forecast Powertax Integration Logic
||========================================================================================
*/

create table REG_FCST_TAX_LOAD_TEMP
(
 TAX_LOAD_TEMP_ID     number(22,0) not null,
 VERSION_ID           number(22,0) not null,
 TAX_YEAR             number(22,0) not null,
 COMPANY_ID           number(22,0) not null,
 TAX_ROLLUP_DETAIL_ID number(22,0) not null,
 TAX_CLASS_ID         number(22,0),
 NORMALIZATION_ID     number(22,0) not null,
 REG_COMPANY_ID       number(22,0) not null,
 FORECAST_VERSION_ID  number(22,0) not null,
 REG_ACCT_ID          number(22,0) not null,
 FCST_AMOUNT          number(22,2) not null
);

alter table REG_FCST_TAX_LOAD_TEMP
   add constraint PK_REG_FCST_TAX_LOAD_TEMP
       primary key (TAX_LOAD_TEMP_ID)
       using index tablespace PWRPLANT_IDX;

comment on table REG_FCST_TAX_LOAD_TEMP is 'Calculation table used for loading forecast ledger from PowerTax data';

comment on column REG_FCST_TAX_LOAD_TEMP.TAX_LOAD_TEMP_ID is 'System-assigned identifier for the temporary tax load row';
comment on column REG_FCST_TAX_LOAD_TEMP.VERSION_ID is 'The PowerTax version';
comment on column REG_FCST_TAX_LOAD_TEMP.TAX_YEAR is 'The PowerTax year';
comment on column REG_FCST_TAX_LOAD_TEMP.COMPANY_ID is 'The company';
comment on column REG_FCST_TAX_LOAD_TEMP.TAX_ROLLUP_DETAIL_ID is 'The tax rollup detail';
comment on column REG_FCST_TAX_LOAD_TEMP.TAX_CLASS_ID is 'The tax class';
comment on column REG_FCST_TAX_LOAD_TEMP.NORMALIZATION_ID is 'The normalization';
comment on column REG_FCST_TAX_LOAD_TEMP.REG_COMPANY_ID is 'The Reg Company';
comment on column REG_FCST_TAX_LOAD_TEMP.FORECAST_VERSION_ID is 'The Forecast Version';
comment on column REG_FCST_TAX_LOAD_TEMP.REG_ACCT_ID is 'The Reg Account';
comment on column REG_FCST_TAX_LOAD_TEMP.FCST_AMOUNT is 'The forecasted amount for this year';

update PPBASE_WORKSPACE
   set LABEL = 'Fcst PowerTax'
 where MODULE = 'REG'
   and WORKSPACE_IDENTIFIER = 'uo_reg_fcst_tax_int';

update PPBASE_MENU_ITEMS
   set LABEL = 'Fcst PowerTax', MINIHELP = 'Fcst PowerTax'
 where MODULE = 'REG'
   and MENU_IDENTIFIER = 'FORE_POWERTAX';

delete from PPBASE_MENU_ITEMS
 where MODULE = 'REG'
   and MENU_IDENTIFIER = 'FORE_DEF_TAX';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER - 1
 where PARENT_MENU_IDENTIFIER = 'FORE_INTEGRATION'
   and ITEM_ORDER > 5;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1289, 0, 10, 4, 2, 7, 39106, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039106_reg_fcst_powertax_logic.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;