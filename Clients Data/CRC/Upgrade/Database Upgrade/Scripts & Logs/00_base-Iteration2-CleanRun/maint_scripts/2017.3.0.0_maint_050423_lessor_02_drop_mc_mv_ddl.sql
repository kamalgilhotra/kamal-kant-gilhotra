/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050423_lessor_02_drop_mc_mv_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 02/15/2018 Jared Watkins    Remove the Lessor Multicurrency materialised view and the mview logs
||============================================================================
*/

drop materialized view mv_lsr_ilr_mc_schedule_amounts;

drop materialized view log on lsr_ilr;
drop materialized view log on lsr_ilr_options;
drop materialized view log on lsr_ilr_schedule;
drop materialized view log on lsr_ilr_schedule_sales_direct;
drop materialized view log on lsr_lease;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4140, 0, 2017, 3, 0, 0, 50423, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050423_lessor_02_drop_mc_mv_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	