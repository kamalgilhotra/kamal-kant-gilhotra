/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047036_drop_column_ls_var_component_values_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 03/23/2017 Anand R          drop payment_month_number from ls_variable_component_values table
||                                        ls_variable_component_values        
||============================================================================
*/

alter table ls_variable_component_values
drop column payment_month_number;

	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3410, 0, 2017, 1, 0, 0, 47036, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047036_drop_column_ls_var_component_values_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	