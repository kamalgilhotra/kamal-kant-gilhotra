/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042208_reg_ledger_as_adj_amt_dml.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   01/16/2015 Shane Ward    		    Fix Reg Ledger As Adjusted Amounts
||========================================================================================
*/

--REG_FORECAST_LEDGER_SV
UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 12
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_SV')
	 AND column_order = 11;

UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 11
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_SV')
	 AND column_order = 10;

UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 10
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_SV')
	 AND column_order = 9;

INSERT INTO cr_dd_sources_criteria_fields
	(id,
	 detail_field,
	 column_order,
	 amount_field,
	 include_in_select_criteria,
	 column_header,
	 column_width,
	 quantity_field,
	 column_case,
	 required_filter,
	 table_lookup)
	SELECT id, 'AS_ADJUSTED', 9, 1, 1, 'As Adjusted', 313, 0, 'Any', 0, 0
		FROM cr_dd_sources_criteria
	 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_SV';

-- REG_FORECAST_LEDGER_ID_SV
UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 18
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_ID_SV')
	 AND column_order = 17;

UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 17
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_ID_SV')
	 AND column_order = 16;

UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 16
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_ID_SV')
	 AND column_order = 15;

UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 15
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_ID_SV')
	 AND column_order = 14;

UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 14
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_ID_SV')
	 AND column_order = 13;

UPDATE cr_dd_sources_criteria_fields
	 SET column_order = 13
 WHERE id IN (SELECT id
								FROM cr_dd_sources_criteria
							 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_ID_SV')
	 AND column_order = 12;

INSERT INTO cr_dd_sources_criteria_fields
	(id,
	 detail_field,
	 column_order,
	 amount_field,
	 include_in_select_criteria,
	 column_header,
	 column_width,
	 quantity_field,
	 column_case,
	 required_filter,
	 table_lookup)
	SELECT id, 'AS_ADJUSTED', 12, 1, 1, 'As Adjusted', 313, 0, 'Any', 0, 0
		FROM cr_dd_sources_criteria
	 WHERE Upper(table_name) = 'REG_FORECAST_LEDGER_ID_SV';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2197, 0, 2015, 1, 0, 0, 42208, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042208_reg_ledger_as_adj_amt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;