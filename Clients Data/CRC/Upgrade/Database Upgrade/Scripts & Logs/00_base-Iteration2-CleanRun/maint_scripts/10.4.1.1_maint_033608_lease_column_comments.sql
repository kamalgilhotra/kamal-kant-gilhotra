/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033608_lease_column_comments.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/23/2013 B. Beck
||============================================================================
*/

comment on column LS_LEASE_OPTIONS.TAX_SUMMARY_ID is 'The internal PowerPlan ID for the tax summary for this MLA.';
comment on column LS_LEASE_OPTIONS.TAX_RATE_OPTION_ID is 'The internal PowerPlan ID for the tax rate option for this MLA.';
comment on column LS_ILR_GROUP.PAYMENT_SHIFT is 'The number of months that payments are shifted relative to the schedule months.';
comment on column LS_ILR_OPTIONS.IMPORT_RUN_ID is 'The internal PowerPlan ID for the import run in which the ILR was imported. Null if created via the system.';
comment on column LS_ILR_OPTIONS.PAYMENT_SHIFT is 'The number of months that payments are shifted relative to the schedule months.';

delete from PPBASE_ACTIONS_WINDOWS
 where MODULE = 'LESSEE'
   and ACTION_IDENTIFIER = 'unsend_approval';

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
   (select max(ID) + 1, 'LESSEE', 'unsend_approval', 'Unsend Approval', 6, 'ue_unsendApproval'
      from PPBASE_ACTIONS_WINDOWS);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (732, 0, 10, 4, 1, 1, 33608, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033608_lease_column_comments.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;