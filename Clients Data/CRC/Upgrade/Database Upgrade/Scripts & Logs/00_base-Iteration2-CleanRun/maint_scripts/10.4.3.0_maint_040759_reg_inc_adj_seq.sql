/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040759_reg_inc_adj_seq.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/27/2014 Shane Ward
||============================================================================
*/

create sequence REG_INC_ADJUST_ID
  minvalue 1
  maxvalue 9999999999999999999999999999
  increment by 1
  nocycle
  noorder
  cache 20;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1582, 0, 10, 4, 3, 0, 40759, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040759_reg_inc_adj_seq.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
