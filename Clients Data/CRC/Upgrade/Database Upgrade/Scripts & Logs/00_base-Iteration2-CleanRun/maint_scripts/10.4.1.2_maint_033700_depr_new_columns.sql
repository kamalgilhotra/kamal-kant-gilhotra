/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033700_depr_new_columns.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   11/13/2013 Charlie Shilling
||============================================================================
*/

alter table CPR_DEPR_CALC_STG
   add (IMPAIRMENT_ASSET_ACTIVITY_SALV number(22, 2),
        IMPAIRMENT_ASSET_BEGIN_BALANCE number(22, 2));

comment on column CPR_DEPR_CALC_STG.IMPAIRMENT_ASSET_ACTIVITY_SALV
   is 'This holds any activity against the impairment bucket.  It is used for correctly processing salvage amounts.  It is populated by post.';

comment on column CPR_DEPR_CALC_STG.IMPAIRMENT_ASSET_BEGIN_BALANCE
   is 'This holds the beginning balance for the impairment bucket.  It is used for correctly processing salvage amounts.';

alter table CPR_DEPR_CALC_STG_ARC
   add (IMPAIRMENT_ASSET_ACTIVITY_SALV number(22, 2),
        IMPAIRMENT_ASSET_BEGIN_BALANCE number(22, 2));

comment on column CPR_DEPR_CALC_STG_ARC.IMPAIRMENT_ASSET_ACTIVITY_SALV
   is 'This holds any activity against the impairment bucket.  It is used for correctly processing salvage amounts.  It is populated by post.';

comment on column CPR_DEPR_CALC_STG_ARC.IMPAIRMENT_ASSET_BEGIN_BALANCE
   is 'This holds the beginning balance for the impairment bucket.  It is used for correctly processing salvage amounts.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (751, 0, 10, 4, 1, 2, 33700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_033700_depr_new_columns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;