/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038344_lease_tax_fix.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 06/10/2014 Kyle Peterson
||============================================================================
*/

alter table LS_MONTHLY_TAX_STG add ACCRUAL number(1,0) default -1;

comment on column LS_MONTHLY_TAX_STG.ACCRUAL is 'Determines whether the entry is for an accrual or a payment. Only used in internal calculations';

alter table LS_MONTHLY_TAX_STG add SCHEDULE_MONTH date;

comment on column LS_MONTHLY_TAX_STG.SCHEDULE_MONTH is 'The month on the schedule this tax is for. Different than GL_POSTING_MO_YR for payment shifted ILRs.';

alter table LS_MONTHLY_TAX_STG drop primary key drop index;

alter table LS_MONTHLY_TAX_STG
   add constraint PK_MONTHLY_TAX_STG
       primary key (LS_ASSET_ID, TAX_LOCAL_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID, ACCRUAL)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1182, 0, 10, 4, 3, 0, 38344, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038344_lease_tax_fix.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
