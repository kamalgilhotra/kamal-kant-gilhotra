/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042927_pcm_old_windows_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 02/13/2015 		B.Beck			Open old windows from left hand navigation 
||==========================================================================================
*/

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
('pcm', 'fp_mon_charges', 'Charges (FP)', 'uo_pcm_cntr_wksp_stub', 'Charges (FP)', 1);

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
('pcm', 'fp_mon_commitments', 'Commitments (FP)', 'uo_pcm_cntr_wksp_stub', 'Commitments (FP)', 1);

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
('pcm', 'fp_mon_dashboards', 'Dashboards (FP)', 'uo_pcm_cntr_wksp_stub', 'Dashboards (FP)', 1);

insert into ppbase_menu_items
(module, menu_identifier, menu_level, item_order, 
label, minihelp, parent_menu_identifier,
workspace_identifier, enable_yn)
values
('pcm', 'fp_mon_charges', 4, 1,
'Charges', 'Funding Project Charges', 'fp_monitor',
'fp_mon_charges', 1);

insert into ppbase_menu_items
(module, menu_identifier, menu_level, item_order, 
label, minihelp, parent_menu_identifier,
workspace_identifier, enable_yn)
values
('pcm', 'fp_mon_commitments', 4, 2,
'Commitments', 'Funding Project Commitments', 'fp_monitor',
'fp_mon_commitments', 1);

insert into ppbase_menu_items
(module, menu_identifier, menu_level, item_order, 
label, minihelp, parent_menu_identifier,
workspace_identifier, enable_yn)
values
('pcm', 'fp_mon_dashboards', 4, 3,
'Dashboards', 'Funding Project Dashboards', 'fp_monitor',
'fp_mon_dashboards', 1);

update ppbase_menu_items
set workspace_identifier = ''
where module = 'pcm'
and menu_identifier = 'fp_monitor'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_fp_charge_select'
where module = 'pcm'
and workspace_identifier = 'fp_mon_charges'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_fp_commits_select'
where module = 'pcm'
and workspace_identifier = 'fp_mon_commitments'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_dashboard'
where module = 'pcm'
and workspace_identifier = 'fp_mon_dashboards'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_charge_select'
where module = 'pcm'
and workspace_identifier = 'wo_mon_charges'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_commitments'
where module = 'pcm'
and workspace_identifier = 'wo_mon_commitments'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_dashboard'
where module = 'pcm'
and workspace_identifier = 'wo_mon_dashboards'
;
update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_approval_list'
where module = 'pcm'
and workspace_identifier = 'fp_pend_auth'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_approval_list'
where module = 'pcm'
and workspace_identifier = 'fp_pend_budg_rev'
;


update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_est_build'
where module = 'pcm'
and workspace_identifier = 'wo_est_property'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_est_build'
where module = 'pcm'
and workspace_identifier = 'wo_est_as_built'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_eng_estimates'
where module = 'pcm'
and workspace_identifier = 'wo_est_eng'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_est_monthly_grid_entry_custom'
where module = 'pcm'
and workspace_identifier = 'wo_est_grid'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_matlrec_trans'
where module = 'pcm'
and workspace_identifier = 'wo_est_matl_rec'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_approval_list'
where module = 'pcm'
and workspace_identifier = 'wo_pend_auth'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_task_entry'
where module = 'pcm'
and workspace_identifier = 'jt_create'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_task_select_tabs'
where module = 'pcm'
and workspace_identifier = 'jt_search'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_pp_table_grid', label = 'Work Order Types'
where module = 'pcm'
and workspace_identifier = 'config_maintain'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_dashboard_setup', label = 'Dashboards'
where module = 'pcm'
and workspace_identifier = 'config_monitor'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_approval_group', label = 'Notifications'
where module = 'pcm'
and workspace_identifier = 'config_authorize'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_validation_test'
where module = 'pcm'
and workspace_identifier = 'config_dyn_valid'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_worksheets'
where module = 'pcm'
and workspace_identifier = 'config_fp_worksheets'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_worksheets'
where module = 'pcm'
and workspace_identifier = 'config_wo_worksheets'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_wo_doc_justification_setup', label = 'Justifications'
where module = 'pcm'
and workspace_identifier = 'config_justify'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_matlrec_config'
where module = 'pcm'
and workspace_identifier = 'config_matl_rec'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_powerplant_metrics'
where module = 'pcm'
and workspace_identifier = 'config_metrics'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_rate_edit'
where module = 'pcm'
and workspace_identifier = 'config_rates'
;

update ppbase_workspace
set object_type_id = 2, workspace_uo_name = 'w_workflow_setup'
where module = 'pcm'
and workspace_identifier = 'config_workflow'
;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2292, 0, 2015, 1, 0, 0, 042927, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042927_pcm_old_windows_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;