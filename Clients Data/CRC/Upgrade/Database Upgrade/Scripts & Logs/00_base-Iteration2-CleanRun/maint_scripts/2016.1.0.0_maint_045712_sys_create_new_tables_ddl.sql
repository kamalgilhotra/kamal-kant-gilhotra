/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045712_sys_create_new_tables_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2016.1.0  06/14/2016 Anand R       Create new table to store actions drop down for Alerts
||============================================================================
*/

CREATE TABLE ppbase_alerts_drilldown_list (
  drilldown_id        NUMBER(22,0)  NOT NULL,
  module_name     VARCHAR2(254) NOT NULL,
  key_field       VARCHAR2(254) NOT NULL,
  drill_to_window VARCHAR2(254) NOT NULL,
  user_id         VARCHAR2(18)  NULL,
  time_stamp      DATE          NULL
);

ALTER TABLE ppbase_alerts_drilldown_list
  ADD CONSTRAINT ppbase_alerts_dd_list_pk PRIMARY KEY (
    drilldown_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

COMMENT ON TABLE ppbase_alerts_drilldown_list IS '(S) [10] Table to store Alerts drill down actions dropdowns for Alerts';

COMMENT ON COLUMN ppbase_alerts_drilldown_list.drilldown_id IS 'unique identifier for the record';
COMMENT ON COLUMN ppbase_alerts_drilldown_list.module_name IS 'Module for which the alert is set up';
COMMENT ON COLUMN ppbase_alerts_drilldown_list.key_field IS 'Alert type';
COMMENT ON COLUMN ppbase_alerts_drilldown_list.drill_to_window IS 'This column stored the action performed for each dropdown';
COMMENT ON COLUMN ppbase_alerts_drilldown_list.user_id IS 'Standard system-assigned user id user for audit purposes.';
COMMENT ON COLUMN ppbase_alerts_drilldown_list.time_stamp IS 'Standard system-assigned timestamp user for audit purposes.';

CREATE TABLE ppbase_alerts_drilldown_config (
  drilldown_id             NUMBER(22,0)  NOT NULL,
  module_name          VARCHAR2(254) NOT NULL,
  object_to_open       VARCHAR2(254) NOT NULL,
  workspace_identifier VARCHAR2(254) NOT NULL,
  request_identifier   VARCHAR2(254) NULL,
  request              VARCHAR2(254) NULL,
  object_structure     VARCHAR2(254) NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
);

ALTER TABLE ppbase_alerts_drilldown_config
  ADD CONSTRAINT ppbase_alerts_dd_config_pk PRIMARY KEY (
    drilldown_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx;
    
ALTER TABLE ppbase_alerts_drilldown_config
  ADD CONSTRAINT ppbase_alerts_dd_config_fk1 
  FOREIGN KEY ( drilldown_id)
  REFERENCES ppbase_alerts_drilldown_list(drilldown_id) ;
  
COMMENT ON TABLE ppbase_alerts_drilldown_config IS '(S) [10] Table to store Alerts drill down actions dropdowns for Alerts';

COMMENT ON COLUMN ppbase_alerts_drilldown_config.drilldown_id IS 'unique identifier for the record';
COMMENT ON COLUMN ppbase_alerts_drilldown_config.module_name IS 'Module for which the alert is set up';
COMMENT ON COLUMN ppbase_alerts_drilldown_config.object_to_open IS 'window or user object to open';
COMMENT ON COLUMN ppbase_alerts_drilldown_config.workspace_identifier IS 'The workspace identifier of object to open';
COMMENT ON COLUMN ppbase_alerts_drilldown_config.request_identifier IS 'The request identifier of object to open';
COMMENT ON COLUMN ppbase_alerts_drilldown_config.request IS 'The requset or action for object to open';
COMMENT ON COLUMN ppbase_alerts_drilldown_config.object_structure IS 'The structure associated to object to open';
COMMENT ON COLUMN ppbase_alerts_drilldown_config.user_id IS 'Standard system-assigned user id user for audit purposes.';
COMMENT ON COLUMN ppbase_alerts_drilldown_config.time_stamp IS 'Standard system-assigned timestamp user for audit purposes.';

CREATE GLOBAL TEMPORARY TABLE ppbase_alerts_drilldown_parm (
  drilldown_id   NUMBER(22,0)  NOT NULL,
  parm1_id   NUMBER(22,0)  NULL,
  parm2_desc VARCHAR2(254) NULL
) on commit preserve rows ;

COMMENT ON TABLE ppbase_alerts_drilldown_parm IS '(T) [10] Table to store the parameters values for drill down alerts ';

COMMENT ON COLUMN ppbase_alerts_drilldown_parm.drilldown_id IS 'unique identifier for the record';
COMMENT ON COLUMN ppbase_alerts_drilldown_parm.parm1_id IS 'This column stores the numeric parmeter value';
COMMENT ON COLUMN ppbase_alerts_drilldown_parm.parm2_desc IS 'This column stores string parameter value';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3217, 0, 2016, 1, 0, 0, 045712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045712_sys_create_new_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;