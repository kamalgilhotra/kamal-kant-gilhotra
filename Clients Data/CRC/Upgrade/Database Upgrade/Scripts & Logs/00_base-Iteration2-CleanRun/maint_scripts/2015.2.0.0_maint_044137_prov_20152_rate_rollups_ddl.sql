 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044137_prov_20152_rate_rollups_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- ----------------------------------------
 || 2015.2.0.0  10/29/2015 Jarrett Skov   Script to set up the table and view structure for Provision company rate rollups
 ||============================================================================
 */ 

-- Temporary table to be used by the rollup datawindows
create global temporary table tax_accrual_company_roll_tmp
(
	parent_id		NUMBER(22,0),
	child_id		NUMBER(22,0),
	company_status_ind	NUMBER(22,0)
)
on COMMIT PRESERVE ROWS
;

alter table tax_accrual_company_roll_tmp
add constraint tax_accrual_company_roll_tmp primary key (parent_id, child_id);

comment on table TAX_ACCRUAL_COMPANY_ROLL_TMP is '(T) [08] TAX_ACCRUAL_COMPANY_ROLL_TMP is used to hold the Rates company rollup changes as they are being made, before they are updated to TAX_ACCRUAL_COMPANY_ROLL_RATES. It is also is used to show the company comparison status';

comment on column TAX_ACCRUAL_COMPANY_ROLL_TMP.PARENT_ID is 'Company ID for the parent company in a parent / child company relationship';
comment on column TAX_ACCRUAL_COMPANY_ROLL_TMP.CHILD_ID is 'Company ID for the child company in a parent / child company relationship';
comment on column TAX_ACCRUAL_COMPANY_ROLL_TMP.COMPANY_STATUS_IND is 'Status Indicator that is udpated by the code and used to set the background color of datawindows.';

-- Table that stores the company rollups
CREATE TABLE tax_accrual_company_roll_rates 
(	effective_date	DATE,
	parent_id		NUMBER(22,0),
	child_id		NUMBER(22,0),
	time_stamp		date,
	user_id			varchar2(18)
)
;

alter table tax_accrual_company_roll_rates
add constraint ta_company_rollup_rates primary key (effective_date, parent_id, child_id)
using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_COMPANY_ROLL_RATES is '(S) [08] TAX_ACCRUAL_COMPANY_ROLL_RATES contains the rates company rollup structure for rate comparison and copying between companies. The structure is effective date, parent company and child company';

comment on column TAX_ACCRUAL_COMPANY_ROLL_RATES.EFFECTIVE_DATE is 'The first date that the parent / child rollup is effective.';
comment on column TAX_ACCRUAL_COMPANY_ROLL_RATES.PARENT_ID is 'Company ID for the parent company in a parent / child company relationship';
comment on column TAX_ACCRUAL_COMPANY_ROLL_RATES.CHILD_ID is 'Company ID for the child company in a parent / child company relationship';
comment on column TAX_ACCRUAL_COMPANY_ROLL_RATES.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_ACCRUAL_COMPANY_ROLL_RATES.USER_ID is 'Standard system-assigned user id used for audit purposes.';

-- Apportionment view, shows every entity row and it's current apportionment for each effective date in the table
CREATE OR REPLACE VIEW tax_accrual_apportionment_view as
SELECT 
	inside.company_id,
	inside.entity_id,  
	inside.effective_date,
	comp.description company_description, 
	ent.description entity_description, 
	Nvl(Last_Value(app.apportionment_pct ignore nulls ) OVER (PARTITION BY inside.company_id, inside.entity_id ORDER BY inside.effective_date), 0) AS apportionment_pct
FROM
(
	SELECT app_co.company_id, app_co.entity_id, app_dt.effective_date
	FROM 
	(
		SELECT DISTINCT company_id, entity_id 
		FROM tax_accrual_apportionment 
	) app_co
	INNER JOIN 
	(	
		SELECT DISTINCT company_id, effective_date 
		FROM tax_accrual_apportionment
	) app_dt 
		ON app_co.company_id = app_dt.company_id
) inside
inner join company_setup comp on inside.company_id = comp.company_id
inner join tax_accrual_entity ent on inside.entity_id = ent.entity_id
left OUTER JOIN tax_accrual_apportionment app 
	ON app.company_id = inside.company_id AND app.entity_id = inside.entity_id AND app.effective_date = inside.effective_date
ORDER BY comp.description, inside.effective_date, inside.entity_id
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2951, 0, 2015, 2, 0, 0, 44137, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044137_prov_20152_rate_rollups_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;