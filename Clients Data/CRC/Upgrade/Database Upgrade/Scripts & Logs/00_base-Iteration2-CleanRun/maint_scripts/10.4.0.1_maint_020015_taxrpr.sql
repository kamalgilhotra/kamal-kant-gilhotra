/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_020015_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.1   06/12/2013 Alex P.         Point Release
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, USER_DISABLED,
    COMPANY_ID, CONTROL_TYPE)
   select max(CONTROL_ID) + 1,
          'TAX ADD EXPENSE REVERSE MO',
          '0',
          null,
          'The number of months here indicate how many months in the current year of tax expensing data was picked up during processing of the prior tax year.',
          null,
          -1,
          null
     from PP_SYSTEM_CONTROL_COMPANY;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, USER_DISABLED,
    COMPANY_ID, CONTROL_TYPE)
   select max(CONTROL_ID) + 1,
          'TAX ADD EXPENSE INCLUDE MO',
          '0',
          null,
          'The number of months here indicate how many months in the next year of tax expensing data to process duing the current tax year..',
          null,
          -1,
          null
     from PP_SYSTEM_CONTROL_COMPANY;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, USER_DISABLED,
    COMPANY_ID, CONTROL_TYPE)
   select max(CONTROL_ID) + 1,
          'TAX ADD EARLY IN SERVICE REVERSE MO',
          '0',
          null,
          'The number of months here indicate how many months in the current year of addition data with early in service dates was picked up during processing of the prior tax year',
          null,
          -1,
          null
     from PP_SYSTEM_CONTROL_COMPANY;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, USER_DISABLED,
    COMPANY_ID, CONTROL_TYPE)
   select max(CONTROL_ID) + 1,
          'TAX ADD EARLY IN SERVICE INCLUDE MO',
          '0',
          null,
          'The number of months here indicate how many months in the next year of addition data with early in service dates to process duing the current tax year.',
          null,
          -1,
          null
     from PP_SYSTEM_CONTROL_COMPANY;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, USER_DISABLED,
    COMPANY_ID, CONTROL_TYPE)
   select max(CONTROL_ID) + 1,
          'TAX ADD KEEP WORK ORDER',
          '0',
          '1;0',
          'Yes means to keep the work order number when loading the additions interface. The work order number will be visible through the audit button. No means that the loaded data will not keep work order number.',
          null,
          -1,
          null
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (404, 0, 10, 4, 0, 1, 20015, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_020015_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;