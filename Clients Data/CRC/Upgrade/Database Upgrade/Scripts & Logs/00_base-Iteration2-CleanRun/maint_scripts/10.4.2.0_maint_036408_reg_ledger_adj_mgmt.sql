/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036408_reg_ledger_adj_mgmt.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/24/2014 Shane Ward
||============================================================================
*/

--Add Adjustment Mgmt Workspaces
INSERT INTO ppbase_workspace
   (MODULE, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
VALUES
   ('REG', 'uo_reg_ledger_adjust_manage_ws_hist', NULL, NULL, 'Ledger Adjustments', 'uo_reg_ledger_adjust_manage_ws', 'Historic Ledger Adjustment Management');
INSERT INTO ppbase_workspace
   (MODULE, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp)
VALUES
   ('REG', 'uo_reg_ledger_adjust_manage_ws_fore', NULL, NULL, 'Ledger Adjustments', 'uo_reg_ledger_adjust_manage_ws', 'Forecast Ledger Adjustment Management');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'HIST_ADJUST_MANAGE', null, null, 3, 10, 'Ledger Adjustments',
    'History Ledger Adjustments', 'HIST_INTEGRATION', 'uo_reg_ledger_adjust_manage_ws_hist', 1);

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'FORE_ADJUST_MANAGE', null, null, 3, 8, 'Ledger Adjustments',
    'Forecast Ledger Adjustments', 'FORE_INTEGRATION', 'uo_reg_ledger_adjust_manage_ws_fore', 1);

--Add Parent_adjust_id columns to identify child and parent records
alter table REG_HISTORY_LEDGER_ADJUST  add PARENT_ADJUST_ID number(22, 0);
comment on column reg_history_ledger_adjust.parent_adjust_id is 'Identifier of parent adjustment. If adjustment is parent adjustment, parent_adjust_id is null.';
alter table REG_FORECAST_LEDGER_ADJUST add PARENT_ADJUST_ID number(22, 0);
comment on column reg_forecast_ledger_adjust.parent_adjust_id is 'Identifier of parent adjustment. If adjustment is parent adjustment, parent_adjust_id is null.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (989, 0, 10, 4, 2, 0, 36408, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036408_reg_ledger_adj_mgmt.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;