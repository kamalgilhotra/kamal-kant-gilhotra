/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048620_lessor_02_add_operating_schedule_types_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/12/2017 Andrew Hill      Add types for lessor operating schedule
||============================================================================
*/

CREATE OR REPLACE TYPE lsr_ilr_op_sch_pay_term AS OBJECT( payment_month_frequency NUMBER(22,0),
                                                          payment_term_start_date date,
                                                          number_of_terms NUMBER(22,0),
                                                          payment_amount number(22,2),
                                                          is_prepay number(1,0));
/

CREATE OR REPLACE TYPE lsr_ilr_op_sch_pay_term_tab IS TABLE OF lsr_ilr_op_sch_pay_term;
/

CREATE OR REPLACE TYPE lsr_ilr_op_sch_result IS OBJECT(MONTH DATE,
                                                        interest_income_received NUMBER(22,2),
                                                        interest_income_accrued NUMBER(22,2),
                                                        interest_rental_recvd_spread NUMBER(22,2),
                                                        begin_deferred_rev NUMBER(22,0),
                                                        deferred_rev NUMBER(22,2),
                                                        end_deferred_rev NUMBER(22,2),
                                                        begin_receivable NUMBER(22,2),
                                                        end_receivable NUMBER(22,2));
/

CREATE OR REPLACE TYPE lsr_ilr_op_sch_result_tab IS TABLE OF lsr_ilr_op_sch_result;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3704, 0, 2017, 1, 0, 0, 48620, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048620_lessor_02_add_operating_schedule_types_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
