/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_01_add_columns_ls_pend_trans_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

alter table ls_pend_transaction add gl_posting_mo_yr date;

alter table ls_pend_transaction_arc
add (in_service_date date, gl_posting_mo_yr date);



COMMENT ON COLUMN ls_pend_transaction.gl_posting_mo_yr IS 'Date when additions or adjustments will be posted to the GL';
COMMENT ON COLUMN ls_pend_transaction_arc.gl_posting_mo_yr IS 'Date when additions or adjustments will be posted to the GL';
COMMENT ON COLUMN ls_pend_transaction_arc.in_service_date is 'In service date of the posted leased asset';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2394, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_01_add_columns_ls_pend_trans_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;