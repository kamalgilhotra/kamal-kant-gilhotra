 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041953_sys_filter_values_2_ddl.sql
|| Description: Recreate filter value tables as global temps.
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1   1/19/2015  Alex P.       Recreate filter value tables as global temps.
||============================================================================
*/

drop table PP_DYNAMIC_FILTER_VALUES;
drop table PP_DYNAMIC_FILTER_VALUES_DW;

-- Create PP_DYNAMIC_FILTER_VALUES table
create global temporary table PP_DYNAMIC_FILTER_VALUES
(
  label          VARCHAR2(35) not null,
  users          VARCHAR2(18),
  operator       VARCHAR2(35),
  start_value    VARCHAR2(2000) not null,
  end_value      VARCHAR2(35),
  user_id        VARCHAR2(18),
  time_stamp     DATE,
  search_type    NUMBER(1) not null,
  filter_type_id NUMBER(22) not null
) on commit preserve rows;

-- Add comments to the table 
comment on table PP_DYNAMIC_FILTER_VALUES
  is '(S)  [10]
The PP Dynamic Filter Values table stores filter selections when the dynamic filter is used.';
-- Add comments to the columns 
comment on column PP_DYNAMIC_FILTER_VALUES.label
  is 'Label for a filter entry with selected values.';
comment on column PP_DYNAMIC_FILTER_VALUES.users
  is 'User currently using the filter.';
comment on column PP_DYNAMIC_FILTER_VALUES.operator
  is 'Operator used for a filter entry with selected values.';
comment on column PP_DYNAMIC_FILTER_VALUES.start_value
  is 'First selected values';
comment on column PP_DYNAMIC_FILTER_VALUES.end_value
  is 'Optional second selected value. This is typically used for operations like "between" that have s start and end value.';
comment on column PP_DYNAMIC_FILTER_VALUES.user_id
  is 'Standard System-assigned user id used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_VALUES.time_stamp
  is 'Standard System-assigned timestamp used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_VALUES.search_type
  is 'Numeric identifier defining the type of object that contains values. 1 = datawindow, 2 = single line edit, 3 = daterange, 4 = operation. Note that datawindow values are stored in PP_DYNAMIC_FILTER_VALUES_DW table.';
comment on column PP_DYNAMIC_FILTER_VALUES.filter_type_id
  is 'References the type of filter for this value. Ex: Dynamic Filters, Class Codes, etc.';
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table PP_DYNAMIC_FILTER_VALUES
  add constraint PK_DYNAMIC_FILTER_VALUES primary key (LABEL, FILTER_TYPE_ID);

  
-- Create PP_DYNAMIC_FILTER_VALUES_DW table
create global temporary table PP_DYNAMIC_FILTER_VALUES_DW
(
  label          VARCHAR2(35) not null,
  users          VARCHAR2(18),
  user_id        VARCHAR2(18),
  time_stamp     DATE,
  value_id       VARCHAR2(4000) not null,
  value_desc     VARCHAR2(4000),
  filter_type_id NUMBER(22) not null
) on commit preserve rows;

-- Add comments to the table 
comment on table PP_DYNAMIC_FILTER_VALUES_DW
  is '(S)  [10]
The PP Dynamic Filter Values DW table stores filter selections when the dynamic filter is used for the values selected from datawindows.';
-- Add comments to the columns 
comment on column PP_DYNAMIC_FILTER_VALUES_DW.label
  is 'Label for a filter entry with selected values.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.users
  is 'User currently using the filter.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.user_id
  is 'Standard System-assigned user id used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.time_stamp
  is 'Standard System-assigned timestamp used for audit purposes.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.value_id
  is 'System identifier for a selected value';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.value_desc
  is 'Description for a selected value.';
comment on column PP_DYNAMIC_FILTER_VALUES_DW.filter_type_id
  is 'References the type of filter for this value. Ex: Dynamic Filters, Class Codes, etc.';
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table PP_DYNAMIC_FILTER_VALUES_DW
  add constraint PK_DYNAMIC_FILTER_VALUES_DW primary key (LABEL, VALUE_ID, FILTER_TYPE_ID);
  
  

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2202, 0, 2015, 1, 0, 0, 41953, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041953_sys_filter_values_2_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;