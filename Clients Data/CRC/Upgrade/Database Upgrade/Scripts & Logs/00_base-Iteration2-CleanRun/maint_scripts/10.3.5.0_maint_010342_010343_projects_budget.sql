/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010342_010343_projects_budget.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   06/21/2012 Chris Mardis   Point Release
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'Tax Expense Test WO - New Rev',
          'no',
          'dw_yes_no;1',
          '"Yes" run tax expense test upon creating a new revision on a Work Order. Default is "No".',
          -1
     from PP_SYSTEM_CONTROL_COMPANY
   union all
   select max(CONTROL_ID) + 2,
          'Tax Expense Test FP - New Rev',
          'no',
          'dw_yes_no;1',
          '"Yes" run tax expense test upon creating a new revision on a Funding Project. Default is "No".',
          -1
     from PP_SYSTEM_CONTROL_COMPANY
   union all
   select max(CONTROL_ID) + 3,
          'Tax Expense Test WO - Monthly Ests',
          'no',
          'dw_yes_no;1',
          '"Yes" run tax expense test upon updating Work Order Monthly Estimates. Default is "No".',
          -1
     from PP_SYSTEM_CONTROL_COMPANY
   union all
   select max(CONTROL_ID) + 4,
          'Tax Expense Test FP - Monthly Ests',
          'no',
          'dw_yes_no;1',
          '"Yes" run tax expense test upon updating Funding Project Monthly Estimates. Default is "No".',
          -1
     from PP_SYSTEM_CONTROL_COMPANY
   union all
   select max(CONTROL_ID) + 5,
          'Tax Expense Test FP - Monthly Fcst',
          'no',
          'dw_yes_no;1',
          '"Yes" run tax expense test upon updating Funding Project Monthly Estimates in the Forecast window. Default is "No".',
          -1
     from PP_SYSTEM_CONTROL_COMPANY
   union all
   select max(CONTROL_ID) + 6,
          'Tax Expense Test WO - Unit Ests',
          'no',
          'dw_yes_no;1',
          '"Yes" run tax expense test upon updating Work Order Unit Estimates. Default is "No".',
          -1
     from PP_SYSTEM_CONTROL_COMPANY
   union all
   select max(CONTROL_ID) + 7,
          'Tax Expense Test FP - Unit Ests',
          'no',
          'dw_yes_no;1',
          '"Yes" run tax expense test upon updating Work Order Unit Estimates. Default is "No".',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;


insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    HARD_EDIT)
values
   (47, 'BI Grid Estimates', 'BI Grid Estimates Data Validations',
    'w_wo_est_monthly_grid_entry_custom', 'select company_id from budget where budget_id = <arg1>',
    'budget_id', 'budget_version_id', 1);
commit;

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    HARD_EDIT)
values
   (48, 'FP Accept Budget Review (Pre)', 'FP Accept Budget Review (Pre)', 'f_workflow_base',
    'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id',
    'revision', 1);
commit;

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    HARD_EDIT)
values
   (34, 'Task Detail Update (Post)', 'Task Detail Update (Post)', 'w_task_detail',
    'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id',
    'job_task_id', 0);
commit;

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    COL3, COL4, COL5, COL6, HARD_EDIT)
values
   (43, 'Task Mass Update', 'Task Mass Update', 'w_job_task_mass_update',
    'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id',
    'job_task_id', 'column_name', 'table_name', 'old_value', 'new_value', 1);
commit;

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    COL3, HARD_EDIT)
values
   (44, 'Task Mass Class Code Update', 'Task Mass Class Code Update', 'w_job_task_class_code',
    'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id',
    'job_task_id', 'class_code_id', 1);

delete from WO_VALIDATION_RUN
 where WO_VALIDATION_ID in
       (select WO_VALIDATION_ID from WO_VALIDATION_CONTROL where WO_VALIDATION_TYPE_ID = 1);

delete from WO_VALIDATION_CONTROL where WO_VALIDATION_TYPE_ID = 1;

delete from WO_VALIDATION_TYPE where WO_VALIDATION_TYPE_ID = 1;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Grid Estimates',
       LONG_DESCRIPTION = 'WO/FP Grid Estimates Data Validations',
       "FUNCTION" = 'w_wo_est_monthly_grid_entry_custom', COL3 = null, COL4 = null, COL5 = null,
       COL6 = null, COL7 = null, COL8 = null
 where WO_VALIDATION_TYPE_ID = 2;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Unit Estimates',
       LONG_DESCRIPTION = 'WO/FP Unit Estimates Data Validations', "FUNCTION" = 'w_wo_est_build',
       COL3 = null, COL4 = null, COL5 = null, COL6 = null, COL7 = null, COL8 = null, COL9 = null,
       COL10 = null, COL11 = null, COL12 = null, COL13 = null, COL14 = null, COL15 = null,
       COL16 = null, COL17 = null, COL18 = null, COL19 = null, COL20 = null
 where WO_VALIDATION_TYPE_ID = 3;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP New Revision', LONG_DESCRIPTION = 'WO/FP New Revision',
       "FUNCTION" = 'w_wo_estimates', COL2 = 'new_revision', HARD_EDIT = 0
 where WO_VALIDATION_TYPE_ID = 4;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Send for Approval',
       LONG_DESCRIPTION = 'WO/FP Send for Approval Data Validations', "FUNCTION" = 'f_workflow_base'
 where WO_VALIDATION_TYPE_ID = 5;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Detail Update (Post)', LONG_DESCRIPTION = 'WO/FP Detail Update (Post)',
       "FUNCTION" = 'w_wo_detail'
 where WO_VALIDATION_TYPE_ID = 6;

update WO_VALIDATION_TYPE
   set LONG_DESCRIPTION = 'WO/FP Detail Open', "FUNCTION" = 'w_wo_detail'
 where WO_VALIDATION_TYPE_ID = 7;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'FP Send for Budget Review', LONG_DESCRIPTION = 'FP Send for Budget Review',
       "FUNCTION" = 'f_workflow_base'
 where WO_VALIDATION_TYPE_ID = 8;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Approve a Revision (Post)',
       LONG_DESCRIPTION = 'WO/FP Approve a Revision (Post)', "FUNCTION" = 'f_workflow_base'
 where WO_VALIDATION_TYPE_ID = 9;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'FP Accept Budget Review (Post)',
       LONG_DESCRIPTION = 'FP Accept Budget Review (Post)', "FUNCTION" = 'f_workflow_base',
       HARD_EDIT = 0
 where WO_VALIDATION_TYPE_ID = 10;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Mult Approvers Open', LONG_DESCRIPTION = 'WO/FP Mult Approvers Open',
       "FUNCTION" = 'w_wo_approval_mult'
 where WO_VALIDATION_TYPE_ID = 11;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Mult Approvers Close', LONG_DESCRIPTION = 'WO/FP Mult Approvers Close',
       "FUNCTION" = 'w_wo_approval_mult'
 where WO_VALIDATION_TYPE_ID = 12;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Mult Approvers Update (Pre)',
       LONG_DESCRIPTION = 'WO/FP Mult Approvers Update (Pre)', "FUNCTION" = 'w_wo_approval_mult'
 where WO_VALIDATION_TYPE_ID = 13;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Mult Approvers Update (Post)',
       LONG_DESCRIPTION = 'WO/FP Mult Approvers Update (Post)', "FUNCTION" = 'w_wo_approval_mult'
 where WO_VALIDATION_TYPE_ID = 14;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP/BI Justifications', LONG_DESCRIPTION = 'WO/FP/BI Justifications',
       "FUNCTION" = 'uo_w_wo_doc_just'
 where WO_VALIDATION_TYPE_ID = 18;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Approve a Revision (Pre)',
       LONG_DESCRIPTION = 'WO/FP Approve a Revision (Pre)', "FUNCTION" = 'f_workflow_base'
 where WO_VALIDATION_TYPE_ID = 19;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP/BI Estimate Open', LONG_DESCRIPTION = 'WO/FP/BI Estimate Open',
       "FUNCTION" = 'w_wo_est_monthly_grid_entry_custom',
       FIND_COMPANY = 'select company_id from (select company_id from work_order_control where work_order_id = <arg1> union all select company_id from budget where budget_id = <arg1>) where rownum = 1'
 where WO_VALIDATION_TYPE_ID = 20;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Detail Update (Pre)', LONG_DESCRIPTION = 'WO/FP Detail Update (Pre)',
       "FUNCTION" = 'w_wo_detail'
 where WO_VALIDATION_TYPE_ID = 21;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Number Autogen', LONG_DESCRIPTION = 'WO/FP Number Autogen',
       "FUNCTION" = 'w_wo_entry', HARD_EDIT = 1
 where WO_VALIDATION_TYPE_ID = 22;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Close Update (Post)', LONG_DESCRIPTION = 'WO/FP Close Update (Post)',
       "FUNCTION" = 'w_wo_close'
 where WO_VALIDATION_TYPE_ID = 23;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Redraw Approvals', LONG_DESCRIPTION = 'WO/FP Redraw Approvals',
       "FUNCTION" = 'w_wo_detail'
 where WO_VALIDATION_TYPE_ID = 31;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Close Update (Pre)', LONG_DESCRIPTION = 'WO/FP Close Update (Pre)',
       "FUNCTION" = 'w_wo_close'
 where WO_VALIDATION_TYPE_ID = 32;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Select Tabs Open', LONG_DESCRIPTION = 'WO/FP Select Tabs Open',
       "FUNCTION" = 'w_project_select_tabs'
 where WO_VALIDATION_TYPE_ID = 33;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'Task Detail Update (Pre)', LONG_DESCRIPTION = 'Task Detail Update (Pre)',
       "FUNCTION" = 'w_task_detail', HARD_EDIT = 1
 where WO_VALIDATION_TYPE_ID = 35;

update WO_VALIDATION_TYPE
   set "FUNCTION" = 'w_task_detail', HARD_EDIT = 0
 where WO_VALIDATION_TYPE_ID = 36;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP/BI Justification Update (Pre)',
       LONG_DESCRIPTION = 'WO/FP/BI Justification Update (Pre)', "FUNCTION" = 'uo_w_wo_doc_just'
 where WO_VALIDATION_TYPE_ID = 37;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'FP Subs Send for Review', LONG_DESCRIPTION = 'FP Subs Send for Review',
       "FUNCTION" = 'f_workflow_base'
 where WO_VALIDATION_TYPE_ID = 38;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Mass Update', LONG_DESCRIPTION = 'WO/FP Mass Update',
       "FUNCTION" = 'w_wo_mass_update', HARD_EDIT = 1
 where WO_VALIDATION_TYPE_ID = 39;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'WO/FP Mass Class Code Update',
       LONG_DESCRIPTION = 'WO/FP Mass Class Code Update', "FUNCTION" = 'w_project_class_code',
       HARD_EDIT = 1, COL1 = 'work_order_id', COL2 = 'class_code_id'
 where WO_VALIDATION_TYPE_ID = 40;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'BI Details', LONG_DESCRIPTION = 'BI Details', "FUNCTION" = 'w_budget_detail',
       HARD_EDIT = 1
 where WO_VALIDATION_TYPE_ID = 41;

update WO_VALIDATION_TYPE
   set "FUNCTION" = 'f_workflow_base', HARD_EDIT = 1
 where WO_VALIDATION_TYPE_ID = 42;

update WO_VALIDATION_TYPE
   set DESCRIPTION = 'FP New Revision BV Selection',
       LONG_DESCRIPTION = 'FP New Revision BV Selection (also uses wo_est_processing_temp)',
       "FUNCTION" = 'w_bv_select_response'
 where WO_VALIDATION_TYPE_ID = 45;


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (152, 0, 10, 3, 5, 0, 10343, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010342_010343_projects_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
