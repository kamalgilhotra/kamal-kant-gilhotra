/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042929_pcm_maint_asset_loc_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/03/2015 Alex P.          Asset Location should be required for
||                                        work orders based on sys control.
||============================================================================
*/

insert into pp_required_table_column( id, table_name, column_name, objectpath, description, required_column_expression )
values( 1020, 'work_order_control', 'asset_location_id', 'uo_pcm_maint_info.dw_detail', 'Asset Location', 'funding_wo_indicator=0 and upper(f_pp_system_control_company("WOINIT - REQUIRE LOCATION",company_id))="YES"' );


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2335, 0, 2015, 1, 0, 0, 042929, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042929_pcm_maint_asset_loc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;