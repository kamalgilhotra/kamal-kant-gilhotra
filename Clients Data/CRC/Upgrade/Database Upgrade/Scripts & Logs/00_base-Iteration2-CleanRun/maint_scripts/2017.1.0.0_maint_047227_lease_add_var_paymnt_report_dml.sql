/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047227_lease_add_var_paymnt_report_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.1.0.0  05/04/2017 Anand R          Add new variable payments report to Lease
||============================================================================
*/

declare
filter1 number; 

begin
  select max(pp_report_filter_id) + 1 into filter1 from pp_reports_filter;

  insert into pp_reports_filter
  (pp_report_filter_id, description, table_name, filter_uo_name)
  select
  filter1, 'Lessee - Variable Payments', null, 'uo_ls_selecttabs_var_paymnts'
  from dual;
  
  INSERT INTO pp_reports (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW, TURN_OFF_MULTI_THREAD)
  VALUES (
    (select nvl(max(report_id),  0) + 1 from pp_reports), 'Variable Payment Amounts', 'Variable Payment Amounts by ILR and Contingent Buckets', 'Lessee', 'dw_ls_rpt_variable_payments', NULL, NULL, NULL, 'Lessee - 1053', NULL, NULL, NULL, 11, 309, 2, filter1, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

end;
/		

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3477, 0, 2017, 1, 0, 0, 47227, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047227_lease_add_var_paymnt_report_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;