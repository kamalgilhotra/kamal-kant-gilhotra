/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043270_cap_bud_subs_to_reason_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 3/14/2015  Chris Mardis     Add "to_reason" to budget subs
||============================================================================
*/

update budget_substitutions
set to_reason = reason;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2384, 0, 2015, 1, 0, 0, 43270, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043270_cap_bud_subs_to_reason_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;