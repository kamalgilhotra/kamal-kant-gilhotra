SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036342_pwrtax_intfc_assetid_b.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 06/10/2014 Andrew Scott        additional changes to tax interfaces for
||                                         asset id modifications based on QA enhancement
||                                         requests.
||============================================================================
*/

--Additions
alter table TAX_ADD_AUDIT_TRAIL add ASSET_ID number(22, 0);
comment on column TAX_ADD_AUDIT_TRAIL.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.  Optional and populated depending on the company system option settings.';

--Retirements
alter table TAX_RET_AUDIT_TRAIL_GRP add ASSET_ID number(22, 0);
comment on column TAX_RET_AUDIT_TRAIL_GRP.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.  Optional and populated depending on the company system option settings.';

--Transfers
alter table TAX_TRANS_AUDIT_TRAIL_GRP add ASSET_ID number(22, 0);
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger (for the "from" side of the transfer).  Optional and populated depending on the company system option settings.';

--nvl(asset_id,0) join into batching function of interfaces is slowing things down tremendously.
--change the asset id to be a default of 0 and just do a straight join.

update TAX_BOOK_TRANSACTIONS_GRP set ASSET_ID = 0 where ASSET_ID is null;

alter table TAX_BOOK_TRANSACTIONS_GRP modify ASSET_ID NOT NULL;

alter table TAX_BOOK_TRANSACTIONS_GRP modify ASSET_ID default 0;


update TAX_BOOK_TRANSFERS_GRP set ASSET_ID = 0 where ASSET_ID is null;

alter table TAX_BOOK_TRANSFERS_GRP modify ASSET_ID NOT NULL;

alter table TAX_BOOK_TRANSFERS_GRP modify ASSET_ID default 0;


update TAX_RECORD_CONTROL set ASSET_ID = 0 where ASSET_ID is null;

alter table TAX_RECORD_CONTROL modify ASSET_ID NOT NULL;

alter table TAX_RECORD_CONTROL modify ASSET_ID default 0;


update TAX_BOOK_TRANSACTIONS set ASSET_ID = 0 where ASSET_ID is null;

alter table TAX_BOOK_TRANSACTIONS modify ASSET_ID NOT NULL;

alter table TAX_BOOK_TRANSACTIONS modify ASSET_ID default 0;

--replace the index on tax record control that uses company_id and version_id to include asset id.
DROP INDEX TAX_REC_CO_VER_IDX;

create index TAX_REC_CO_VER_AST_IDX
   on TAX_RECORD_CONTROL (COMPANY_ID, VERSION_ID, ASSET_ID)
      tablespace PWRPLANT_IDX;

--make a comparable index on tax book transactions grp to use company id and asset id (no version id to use)
create index TAX_BOOK_TRANS_GRP_CO_AST_IDX
   on TAX_BOOK_TRANSACTIONS_GRP (COMPANY_ID, ASSET_ID)
      tablespace PWRPLANT_IDX;


declare
   CODE number(22, 0);
begin
   CODE := ANALYZE_TABLE('tax_record_control', -1);
   DBMS_OUTPUT.PUT_LINE('tax_record_control analyze return code : ' || CODE);
end;
/

declare
   CODE number(22, 0);
begin
   CODE := ANALYZE_TABLE('tax_book_transactions_grp', -1);
   DBMS_OUTPUT.PUT_LINE('tax_book_transactions_grp analyze return code : ' || CODE);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1219, 0, 10, 4, 3, 0, 36342, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036342_pwrtax_intfc_assetid_b.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
