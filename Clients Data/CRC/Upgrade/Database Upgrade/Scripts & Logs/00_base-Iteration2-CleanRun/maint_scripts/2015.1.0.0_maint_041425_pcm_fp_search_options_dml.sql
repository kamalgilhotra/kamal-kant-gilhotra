/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041425_pcm_fp_search_options_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	12/01/2014 Ryan Oliveria  Add user options to FP Search Grid
||============================================================================
*/
 
update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_pcm_fp_wksp_search'
 where MODULE = 'pcm'
   and WORKSPACE_IDENTIFIER = 'fp_search';



insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
values
	('Funding Project Search - Grid View', 'The default way to display funding projects in a search grid.', 0, '1 - Standard FP Grid', null, 1, 0);

insert into PPBASE_SYSTEM_OPTIONS_MODULE (SYSTEM_OPTION_ID, MODULE) values ('Funding Project Search - Grid View', 'pcm');

insert into PPBASE_SYSTEM_OPTIONS_WKSP (SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER) values ('Funding Project Search - Grid View', 'fp_search');

insert into PPBASE_SYSTEM_OPTIONS_VALUES (SYSTEM_OPTION_ID, OPTION_VALUE) values ('Funding Project Search - Grid View', '1 - Standard FP Grid');
insert into PPBASE_SYSTEM_OPTIONS_VALUES (SYSTEM_OPTION_ID, OPTION_VALUE) values ('Funding Project Search - Grid View', '2 - FP Main Sets of Books');
insert into PPBASE_SYSTEM_OPTIONS_VALUES (SYSTEM_OPTION_ID, OPTION_VALUE) values ('Funding Project Search - Grid View', '3 - Simple FP Grid');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2086, 0, 2015, 1, 0, 0, 041425, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041425_pcm_fp_search_options_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;