SET DEFINE OFF
SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031484_pwrtax_xfer_intfc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 10/17/2013 Andrew Scott        rewrite/speed up transfers interface
||============================================================================
*/

---------drop new tables, column (to be repeatable)

--drop table tax_book_transfers_grp;
--drop table tax_trans_audit_trail_grp;
--alter table tax_book_transfers drop column tbt_group_id;
--drop index tax_trans_control_combo_idx;
--drop table TAX_TEMP_TRID_DELETES;

------add the group to the tax_book_transfers table
alter table TAX_BOOK_TRANSFERS add TBT_GROUP_ID NUMBER(22,0);

alter table TAX_BOOK_TRANSFERS
   add constraint TBTXFER_GROUP_FK
       foreign key (TBT_GROUP_ID)
       references TAX_BOOK_TRANSLATE_GROUP (TBT_GROUP_ID);

comment on column TAX_BOOK_TRANSFERS.TBT_GROUP_ID is 'System-assigned identifier of a tax book translate group.  Used in the collapsing of transactions.';

create index TAX_TRANS_CONTROL_COMBO_IDX
   on TAX_TRANSFER_CONTROL(FROM_TRID, TAX_YEAR, VERSION_ID, COMPANY_ID)
      tablespace PWRPLANT_IDX;

----new table: tax_book_transfers_grp.  will hopefully replace tax_book_transfers, summarized at the group level
create table TAX_BOOK_TRANSFERS_GRP
(
 TRANS_GRP_ID              number(22,0) not null,
 TAX_YEAR                  number(22,2),
 START_MONTH               number(22,0),
 END_MONTH                 number(22,0),
 TBT_GROUP_ID_TO           number(22,0),
 TAX_ACTIVITY_CODE_ID_TO   number(22,0),
 COMPANY_ID_TO             number(22,0),
 IN_SERVICE_YEAR_TO        date,
 AMOUNT_TO                 number(22,2) default 0,
 BOOK_SALE_AMOUNT_TO       number(22,2) default 0,
 TRANSFER_STATUS_TO        number(22,0),
 TBT_GROUP_ID_FROM         number(22,0),
 TAX_ACTIVITY_CODE_ID_FROM number(22,0),
 COMPANY_ID_FROM           number(22,0),
 IN_SERVICE_YEAR_FROM      date,
 AMOUNT_FROM               number(22,2) default 0,
 BOOK_SALE_AMOUNT_FROM     number(22,2) default 0,
 TRANSFER_STATUS_FROM      number(22,0),
 AMOUNT_UNPROCESSED_FROM   number(22,2),
 TIME_STAMP                date,
 USER_ID                   varchar2(18)
);

alter table TAX_BOOK_TRANSFERS_GRP
   add constraint TAX_BOOK_TRANSFERS_GRP_PK
       primary key (TRANS_GRP_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_BOOK_TRANSFERS_GRP
   add constraint TAX_BOOK_XFER_GRP_CO_TO_FK
       foreign key (COMPANY_ID_TO)
       references COMPANY_SETUP (COMPANY_ID);

alter table TAX_BOOK_TRANSFERS_GRP
   add constraint TAX_BOOK_XFER_GRP_ACTCD_TO_FK
       foreign key (TAX_ACTIVITY_CODE_ID_TO)
       references TAX_ACTIVITY_CODE (TAX_ACTIVITY_CODE_ID);

alter table TAX_BOOK_TRANSFERS_GRP
   add constraint TAX_BOOK_XFER_GRP_TBTGRP_TO_FK
       foreign key (TBT_GROUP_ID_TO)
       references TAX_BOOK_TRANSLATE_GROUP (TBT_GROUP_ID);

alter table TAX_BOOK_TRANSFERS_GRP
   add constraint TAX_BOOK_XFER_GRP_CO_FRM_FK
       foreign key (COMPANY_ID_FROM)
       references COMPANY_SETUP (COMPANY_ID);

alter table TAX_BOOK_TRANSFERS_GRP
   add constraint TAX_BOOK_XFER_GRP_ACTCD_FRM_FK
       foreign key (TAX_ACTIVITY_CODE_ID_FROM)
       references TAX_ACTIVITY_CODE (TAX_ACTIVITY_CODE_ID);

alter table TAX_BOOK_TRANSFERS_GRP
   add constraint TAX_BOOK_XFER_GRP_TBTG_FRM_FK
       foreign key (TBT_GROUP_ID_FROM)
       references TAX_BOOK_TRANSLATE_GROUP (TBT_GROUP_ID);

comment on table TAX_BOOK_TRANSFERS_GRP is
'(C) [09] The Tax Book Transfers Grp holds the summarization of records from Tax Book Transfers for faster transfer processing.';
comment on column TAX_BOOK_TRANSFERS_GRP.TRANS_GRP_ID is 'System-assigned identifier of a transfer grouping on this table.';
comment on column TAX_BOOK_TRANSFERS_GRP.TAX_YEAR is 'PowerTax tax year.';
comment on column TAX_BOOK_TRANSFERS_GRP.START_MONTH is 'Start month to which transfer groupings apply.';
comment on column TAX_BOOK_TRANSFERS_GRP.END_MONTH is 'End month to which transfer groupings apply.';
comment on column TAX_BOOK_TRANSFERS_GRP.TBT_GROUP_ID_FROM is 'System-assigned identifier of a tax book translate group of the "transfer from" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.TAX_ACTIVITY_CODE_ID_FROM is 'System-assigned identifier of a tax activity code (e.g., add, retire, sale, etc.) of the "transfer from" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.COMPANY_ID_FROM is 'System-assigned identifier of a company of the "transfer from" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.IN_SERVICE_YEAR_FROM is 'Book in-service year of the "transfer from" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.AMOUNT_FROM is 'Book dollar amount of the "transfer from" transaction group.';
comment on column TAX_BOOK_TRANSFERS_GRP.BOOK_SALE_AMOUNT_FROM is 'Book sale amount (proceeds) of the "transfer from" transaction group.';
comment on column TAX_BOOK_TRANSFERS_GRP.TRANSFER_STATUS_FROM is 'Used to flag unprocessed records and to tag the iteration of the processing for the "transfer from" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.AMOUNT_UNPROCESSED_FROM is 'Amount left to process for the "transfer from" records.  This is updated with each iteration & batch for the next.';
comment on column TAX_BOOK_TRANSFERS_GRP.TBT_GROUP_ID_TO is 'System-assigned identifier of a tax book translate group of the "transfer to" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.TAX_ACTIVITY_CODE_ID_TO is 'System-assigned identifier of a tax activity code (e.g., add, retire, sale, etc.) of the "transfer to" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.COMPANY_ID_TO is 'System-assigned identifier of a company of the "transfer to" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.IN_SERVICE_YEAR_TO is 'Book in-service year of the "transfer to" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.AMOUNT_TO is 'Book dollar amount of the "transfer to" transaction group.';
comment on column TAX_BOOK_TRANSFERS_GRP.BOOK_SALE_AMOUNT_TO is 'Book sale amount (proceeds) of the "transfer to" transaction group.';
comment on column TAX_BOOK_TRANSFERS_GRP.TRANSFER_STATUS_TO is 'Used to as part of the grouping of transfer records.  Not used for processing status like the "transfer from" records.';
comment on column TAX_BOOK_TRANSFERS_GRP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column TAX_BOOK_TRANSFERS_GRP.USER_ID is 'Standard system-assigned user id used for audit purposes.';

------new table : tax_ret_audit_trail_grp.  will replace the tax_ret_audit_trail table, summarized at the group level
create table TAX_TRANS_AUDIT_TRAIL_GRP
(
 TRANS_GRP_ID           number(22,0) not null,
 TAX_YEAR               number(22,2) not null,
 FROM_TRID              number(22,0) not null,
 TAX_ACTIVITY_CODE_ID   number(22,0),
 IN_SERVICE_YEAR        date,
 START_MONTH            number(22,0),
 END_MONTH              number(22,0),
 TBT_GROUP_ID           number(22,0),
 START_EFF_BOOK_VINTAGE number(22,0),
 END_EFF_BOOK_VINTAGE   number(22,0),
 BOOK_BALANCE           number(22,2),
 HOLD_SUM_BOOK_BALANCE  number(22,2),
 ALLOC_AMOUNT           number(22,2) default 0,
 TRANS_AMOUNT           number(22,2),
 TRANS_SALES_AMOUNT     number(22,2),
 VINTAGE_YEAR           number(22,0),
 TRANSFER_STATUS        number(22,0),
 FROM_TRID_VINTAGE_YEAR number(22,0),
 ALLOC_SALES_AMOUNT     number(22,2),
 CREATE_TO_TRID         number(22,0),
 BATCH                  number(22,0),
 ITERATION              number(22,0),
 TO_TRID                number(22,0) not null,
 COMPANY_ID_TO          number(22,0),
 TBT_GROUP_ID_TO        number(22,0)
);

create index TAX_TRANS_AUDIT_TRAIL_GRP_IDX1
   on TAX_TRANS_AUDIT_TRAIL_GRP (TRANS_GRP_ID, TAX_YEAR, FROM_TRID)
      tablespace PWRPLANT_IDX;

comment on table TAX_TRANS_AUDIT_TRAIL_GRP is
'(C) [09] The Tax Book Audit Trail Group table contains data to link transfer transaction groupings from book to tax for audit purposes.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TRANS_GRP_ID is 'System-assigned identifier of a particular transaction grouping.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TAX_YEAR is 'PowerTax tax year.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.FROM_TRID is 'Corresponding tax record id for the "transfer from" record.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TAX_ACTIVITY_CODE_ID is 'System-assigned identifier of a tax activity code (e.g., add, retire, sale, etc.)';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.IN_SERVICE_YEAR is 'Book in-service year.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.START_MONTH is 'Transactions starting month.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.END_MONTH is 'Transactions ending month.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TBT_GROUP_ID is 'System-assigned identifier of a tax book translate group.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.START_EFF_BOOK_VINTAGE is 'Effecting starting vintage on the tax book translate table.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.END_EFF_BOOK_VINTAGE is 'Effecting ending vintage on the tax book translate table.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.BOOK_BALANCE is 'Beginning book balance (per tax) of this tax target grouping.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.HOLD_SUM_BOOK_BALANCE is 'Sum of the beginning book balance (per tax) of this tax target grouping.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.ALLOC_AMOUNT is 'Amount of the transfer assigned to this "from" tax record.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TRANS_AMOUNT is 'Full transfer grouping book dollar amount.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TRANS_SALES_AMOUNT is 'Full transfer grouping book sales amount.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.VINTAGE_YEAR is 'Book vintage year';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TRANSFER_STATUS is 'Indicates which iteration from the retirement rules processed this transaction grouping.  A negative value indicates that it did not fully process.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.FROM_TRID_VINTAGE_YEAR is 'Tax vintage year on the "from" tax record.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.ALLOC_SALES_AMOUNT is 'Allocated sales amount.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.CREATE_TO_TRID is 'Create To Trid indicates if the to transfer record had to be auto-generated.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.BATCH is 'Processing batch of tax records within an iteration.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.ITERATION is 'Processing iteration.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TO_TRID is 'Corresponding tax record id for the "transfer to" records.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.COMPANY_ID_TO is 'Company id on the transfer to record.';
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.TBT_GROUP_ID_TO is 'Tax Book Translate Group id on the transfer to record.';


create global temporary table TAX_TEMP_TRID_DELETES
(
 TAX_RECORD_ID NUMBER(22,0)
) on commit preserve rows;

create unique index TAX_TEMP_TRID_DELETES_PK
   on TAX_TEMP_TRID_DELETES (TAX_RECORD_ID);

comment on table TAX_TEMP_TRID_DELETES is
'(T) [09] The Tax Temp Trid Deletes temporary table holds a list of tax record ids created previously in the transfers process to aid in the deletion logic.';
comment on column TAX_TEMP_TRID_DELETES.TAX_RECORD_ID is 'Tax record id for any "to" record automatically created previously, that needs to be cleared out.';

----to_trid on tax_transfer_control could be -1 from interface, so drop the foreign key to tax_record_control

declare
   FK_TO_DROP varchar2(50);

begin
   select CONSTRAINT_NAME
     into FK_TO_DROP
     from ALL_CONS_COLUMNS
    where CONSTRAINT_NAME in (select CONSTRAINT_NAME
                                from ALL_CONSTRAINTS
                               where LOWER(TABLE_NAME) = 'tax_transfer_control'
                                 and CONSTRAINT_TYPE = 'R')
      and COLUMN_NAME = 'TO_TRID';

   if FK_TO_DROP is not null then
      execute immediate 'alter table tax_transfer_control drop constraint ' || FK_TO_DROP;
   end if;

exception
   when NO_DATA_FOUND then
      --do nothing.
      DBMS_OUTPUT.PUT_LINE('No fkey on tax_transfer_control.to_trid to drop.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (760, 0, 10, 4, 2, 0, 31484, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_031484_pwrtax_xfer_intfc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON