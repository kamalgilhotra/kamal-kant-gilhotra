/*
||==========================================================================================
|| Application: PowerPlan
|| Module: Property Tax
|| File Name:   maint_044166_proptax_gis_workspace_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2    05/04/2015  Graham Miller    Add returns workspace for GIS Intergration
||==========================================================================================
*/



--Create PT_GIS_STAGING Table
CREATE TABLE PT_GIS_STAGING(
COMPANY_ID NUMBER(22, 0) NOT NULL,
GIS_DISTRICT_CODE VARCHAR2(35) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
PROPERTY_CATEGORY VARCHAR2(35)NOT NULL,
AMOUNT NUMBER(22, 8) NOT NULL,
STATE_ID CHAR(18) NOT NULL,
VINTAGE NUMBER(22,0));



ALTER TABLE PT_GIS_STAGING
  ADD CONSTRAINT PT_GIS_STAGING_COMPANY_FK FOREIGN KEY (
    COMPANY_ID
  ) REFERENCES COMPANY_SETUP (
    COMPANY_ID
  );

ALTER TABLE PT_GIS_STAGING
  ADD CONSTRAINT PT_GIS_STAGING_STATE_FK FOREIGN KEY (
    STATE_ID
  ) REFERENCES STATE (
    STATE_ID
  );


COMMENT ON TABLE PT_GIS_STAGING IS '(S)  [07]
The PT GIS STAGING table holds the raw GIS data that is mapped into PowerPlan via an external GIS system.';

COMMENT ON COLUMN PT_GIS_STAGING.COMPANY_ID IS 'System-assigned identifier of a particular company.';
COMMENT ON COLUMN PT_GIS_STAGING.GIS_DISTRICT_CODE IS 'External GIS locational code denoting tax district.';
COMMENT ON COLUMN PT_GIS_STAGING.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN PT_GIS_STAGING.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN PT_GIS_STAGING.PROPERTY_CATEGORY IS 'Description of property type being tracked in GIS.';
COMMENT ON COLUMN PT_GIS_STAGING.AMOUNT IS 'Count or length for given property category.';
COMMENT ON COLUMN PT_GIS_STAGING.STATE_ID IS 'State ID records the name of a particular state within a given country.';
COMMENT ON COLUMN PT_GIS_STAGING.VINTAGE IS 'In service year associated with the transaction/balance.  For rows "pulled" from the CPR, this value may be either the year from Eng In-Service Date (from CPR Ledger), In Service Date (from CPR Ledger), or GL Posting Mo Yr (from CPR Activity) depending on the settings of the property tax type for this row.  (See Vintage Nonvintage on Property Tax Type Data.)';

--Create PT_GIS_SUMMARY Table
CREATE TABLE PT_GIS_SUMMARY(
TAX_YEAR  NUMBER (22,0) NOT NULL,
PROP_TAX_COMPANY_ID NUMBER (22,0) NOT NULL,
COMPANY_ID NUMBER(22, 0) NOT NULL,
GIS_DISTRICT_CODE VARCHAR2(35) NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
PROPERTY_CATEGORY VARCHAR2(35)NOT NULL,
AMOUNT NUMBER(22, 8) NOT NULL,
STATE_ID CHAR(18) NOT NULL,
VINTAGE NUMBER(22,0));



ALTER TABLE PT_GIS_SUMMARY
  ADD CONSTRAINT PT_GIS_SUMMARY_COMPANY_FK FOREIGN KEY (
    COMPANY_ID
  ) REFERENCES COMPANY_SETUP (
    COMPANY_ID
  );

ALTER TABLE PT_GIS_SUMMARY
  ADD CONSTRAINT PT_GIS_SUMMARY_STATE_FK FOREIGN KEY (
    STATE_ID
  ) REFERENCES STATE (
    STATE_ID
  );

ALTER TABLE PT_GIS_SUMMARY
  ADD CONSTRAINT PT_GIS_SUMMARY_PT_COMPANY_FK FOREIGN KEY (
    PROP_TAX_COMPANY_ID
  ) REFERENCES PT_COMPANY (
    PROP_TAX_COMPANY_ID
  );

ALTER TABLE PT_GIS_SUMMARY
  ADD CONSTRAINT PT_GIS_SUMMARY_TAX_YEAR_FK FOREIGN KEY (
    TAX_YEAR
  ) REFERENCES PROPERTY_TAX_YEAR (
    TAX_YEAR
  );

CREATE UNIQUE INDEX PT_GIS_SUMMARY_UNIQUE_INDEX
  ON PT_GIS_SUMMARY (
    tax_year,
    prop_tax_company_id,
    company_id,
    state_id,
    gis_district_code,
    property_category,
    vintage
  )
  TABLESPACE pwrplant_idx
;


COMMENT ON TABLE PT_GIS_SUMMARY IS'(S)  [07]
The PT GIS SUMMARY table holds the summarized GIS data by tax year that is mapped into PowerPlan via an external GIS system.';

COMMENT ON COLUMN PT_GIS_SUMMARY.TAX_YEAR IS 'System-assigned identifier of a particular property tax year.';
COMMENT ON COLUMN PT_GIS_SUMMARY.PROP_TAX_COMPANY_ID IS 'System-assigned identifier of a property tax company.';
COMMENT ON COLUMN PT_GIS_SUMMARY.COMPANY_ID IS 'System-assigned identifier of a particular company.';
COMMENT ON COLUMN PT_GIS_SUMMARY.GIS_DISTRICT_CODE IS 'External GIS locational code denoting tax district.';
COMMENT ON COLUMN PT_GIS_SUMMARY.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN PT_GIS_SUMMARY.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN PT_GIS_SUMMARY.PROPERTY_CATEGORY IS 'Description of property type being tracked in GIS.';
COMMENT ON COLUMN PT_GIS_SUMMARY.AMOUNT IS 'Count or length for given property category.';
COMMENT ON COLUMN PT_GIS_SUMMARY.STATE_ID IS 'State ID records the name of a particular state within a given country.';
COMMENT ON COLUMN PT_GIS_SUMMARY.VINTAGE IS 'In service year associated with the transaction/balance.  For rows "pulled" from the CPR, this value may be either the year from Eng In-Service Date (from CPR Ledger), In Service Date (from CPR Ledger), or GL Posting Mo Yr (from CPR Activity) depending on the settings of the property tax type for this row.  (See Vintage Nonvintage on Property Tax Type Data.)';



--Create PT_GIS_CATEGORY_MAPPING Table
CREATE TABLE PT_GIS_CATEGORY_MAPPING(
PROPERTY_CATEGORY VARCHAR2(35)NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
PROP_TAX_COMPANY_ID NUMBER (22,0) NOT NULL,
ALLOCATION_ID NUMBER(22,0) NOT NULL);



ALTER TABLE PT_GIS_CATEGORY_MAPPING
  ADD CONSTRAINT PT_GIS_CAT_MAP_PT_COMPANY_FK FOREIGN KEY (
    PROP_TAX_COMPANY_ID
  ) REFERENCES PT_COMPANY (
    PROP_TAX_COMPANY_ID
  );

ALTER TABLE PT_GIS_CATEGORY_MAPPING
  ADD CONSTRAINT PT_GIS_CAT_MAP_ALLOCATION_FK FOREIGN KEY (
    ALLOCATION_ID
  ) REFERENCES PROP_TAX_ALLO_DEFINITION (
    ALLOCATION_ID
  );

ALTER TABLE PT_GIS_CATEGORY_MAPPING
  ADD CONSTRAINT PT_GIS_CATEGORY_MAPPING_PK PRIMARY KEY (
    PROPERTY_CATEGORY,
    PROP_TAX_COMPANY_ID
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

COMMENT ON TABLE PT_GIS_CATEGORY_MAPPING IS'(S)  [07]
The PT GIS CATEGORY MAPPING table holds mappings of external GIS property categories to internal PowerPlan allocation definitions';

COMMENT ON COLUMN PT_GIS_CATEGORY_MAPPING.PROPERTY_CATEGORY IS 'Description of property type being tracked in GIS.';
COMMENT ON COLUMN PT_GIS_CATEGORY_MAPPING.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN PT_GIS_CATEGORY_MAPPING.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN PT_GIS_CATEGORY_MAPPING.PROP_TAX_COMPANY_ID IS 'System-assigned identifier of a property tax company.';
COMMENT ON COLUMN PT_GIS_CATEGORY_MAPPING.ALLOCATION_ID IS 'System-assigned identifier of an allocation statistics definition.';



--Create PT_GIS_PARCEL_MAPPING table
CREATE TABLE PT_GIS_PARCEL_MAPPING(
GIS_DISTRICT_CODE VARCHAR2(35)NOT NULL,
TIME_STAMP DATE,
USER_ID VARCHAR2(18),
PROP_TAX_COMPANY_ID NUMBER (22,0) NOT NULL,
PARCEL_ID NUMBER(22,0) NOT NULL);



ALTER TABLE PT_GIS_PARCEL_MAPPING
  ADD CONSTRAINT PT_GIS_PARCEL_PT_COMPANY_FK FOREIGN KEY (
    PROP_TAX_COMPANY_ID
  ) REFERENCES PT_COMPANY (
    PROP_TAX_COMPANY_ID
  );

ALTER TABLE PT_GIS_PARCEL_MAPPING
  ADD CONSTRAINT PT_GIS_PARCEL_PARCEL_FK FOREIGN KEY (
    PARCEL_ID
  ) REFERENCES PT_PARCEL (
    PARCEL_ID
  );

ALTER TABLE PT_GIS_PARCEL_MAPPING
  ADD CONSTRAINT PT_GIS_PARCEL_MAPPING_PK PRIMARY KEY (
    GIS_DISTRICT_CODE,
    PROP_TAX_COMPANY_ID
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

COMMENT ON TABLE PT_GIS_PARCEL_MAPPING IS'(S)  [07]
The PT GIS PARCEL MAPPING table holds mappings of external GIS district code to internal PowerPlan property tax parcels.';


COMMENT ON COLUMN PT_GIS_PARCEL_MAPPING.GIS_DISTRICT_CODE IS 'External GIS locational code denoting tax district.';
COMMENT ON COLUMN PT_GIS_PARCEL_MAPPING.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN PT_GIS_PARCEL_MAPPING.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN PT_GIS_PARCEL_MAPPING.PROP_TAX_COMPANY_ID IS 'System-assigned identifier of a property tax company.';
COMMENT ON COLUMN PT_GIS_PARCEL_MAPPING.PARCEL_ID IS 'System-assigned identifier of a particular parcel.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2666, 0, 2015, 2, 0, 0, 044166, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044166_proptax_gis_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;