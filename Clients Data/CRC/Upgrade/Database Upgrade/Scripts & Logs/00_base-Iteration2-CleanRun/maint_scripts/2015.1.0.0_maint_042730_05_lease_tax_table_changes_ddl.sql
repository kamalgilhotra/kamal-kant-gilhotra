/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_05_lease_tax_table_changes_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

alter table ls_tax_district_rates
drop primary key drop index;

ALTER TABLE ls_tax_district_rates
  ADD CONSTRAINT pk_ls_tax_district_rates PRIMARY KEY (
    tax_local_id,
    ls_tax_district_id,
	  effective_date
  )
  using index tablespace PWRPLANT_IDX
/

alter table ls_tax_state_rates
drop primary key drop index;

ALTER TABLE ls_tax_state_rates
  ADD CONSTRAINT pk_ls_tax_state_rates PRIMARY KEY (
    tax_local_id,
    state_id,
	  effective_date
  )
  using index tablespace PWRPLANT_IDX
/

alter table ls_monthly_tax
add rate NUMBER(22,8) NULL;

comment on column ls_monthly_tax.rate is 'The tax rate used to calculate the tax amount for the given month';

alter table ls_monthly_tax drop primary key drop index;

DECLARE
	record_count INTEGER;
BEGIN
	SELECT Count(*)
	INTO record_count
	FROM ls_monthly_tax
	WHERE tax_district_id IS NULL;

	IF record_count <> 0 THEN
		Raise_Application_Error(-20001,'The script "maint_042730_05_lease_tax_table_changes_ddl.sql" is attempting to add a primary key to the LS_MONTHLY_TAX table, but the TAX_DISTRICT_ID column contains null values. Please populate this column and then run the SQL to add the primary key. See comments in scripts for details.');
	END IF;

	-- CBS - This script originally came from the field where some improvements to the lease module were made at a client. 
	-- 	The issue is that tax_district_id column is being added to the primary key to the LS_MONTHLY_TAX table, and it
	-- 		was previously a NULLable field. 
	--	However, tax_districts are client specific and there is not a good way to generalize backfilling this column 
	--		in order to guarantee that it will always be populated before adding it to the primary key, which will make
	--		it a NOT NULL field, so the primary key will fail if there are any records in the LS_MONTHLY_TAX table with 
	--		NULL tax_district_ids. 
	--	After speaking with the lease SMEs, we determined that only a few clients will actually have data in this table, 
	--		so it is better to let the script error and have consultants/clients correct the data before continuing rather than 
	--		populate the column with a fake value. 
	--	Once the column has been populated, the ALTER TABLE statement from the following EXECUTE IMMEDIATE needs to be executed
	--		to actually add the primary key. If the data is already good, then the error above will not be thrown and the primary 
	--		key will be added in the standard upgrade process. 
	
	EXECUTE IMMEDIATE 'ALTER TABLE ls_monthly_tax
  		ADD CONSTRAINT pk_ls_monthly_tax PRIMARY KEY (
    		ls_asset_id,
    		tax_local_id,
    		gl_posting_mo_yr,
    		set_of_books_id,
    		accrual,
    		vendor_id,
    		tax_district_Id
  		)
  		using index tablespace PWRPLANT_IDX';
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2414, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_05_lease_tax_table_changes_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;