/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008604_008605_depr_inputs.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/12/2011 Sunjin Cone    Point Release
||============================================================================
*/

--###PATCH(8604, 8605)

-- Input Reserve Transactions Enhancements

-- DEPR_TRANS_ALLO_METHOD
create table DEPR_TRANS_ALLO_METHOD
(
 DEPR_TRANS_ALLO_METHOD_ID number(22,0) not null,
 DESCRIPTION               varchar2(254) not null,
 LONG_DESCRIPTION          varchar2(1000),
 TIME_STAMP                date,
 USER_ID                   varchar2(18)
);

alter table DEPR_TRANS_ALLO_METHOD
   add constraint PK_DEPR_TRANS_ALLO_METHOD
       primary key (DEPR_TRANS_ALLO_METHOD_ID)
       using index tablespace PWRPLANT_IDX;

insert into DEPR_TRANS_ALLO_METHOD
   (DEPR_TRANS_ALLO_METHOD_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID)
values
   (1, 'Manual', '', null, '');

insert into DEPR_TRANS_ALLO_METHOD
   (DEPR_TRANS_ALLO_METHOD_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID)
values
   (2, 'Current Asset NBV', '', null, '');

insert into DEPR_TRANS_ALLO_METHOD
   (DEPR_TRANS_ALLO_METHOD_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID)
values
   (3, 'Current Asset Balance', '', null, '');

insert into DEPR_TRANS_ALLO_METHOD
   (DEPR_TRANS_ALLO_METHOD_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID)
values
   (4, 'Current Reserve Balance', '', null, '');

insert into DEPR_TRANS_ALLO_METHOD
   (DEPR_TRANS_ALLO_METHOD_ID, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP, USER_ID)
values
   (5, 'Current Month Expense', '', null, '');

-- DEPR_TRANS_SET
create table DEPR_TRANS_SET
(
 DEPR_TRANS_SET_ID         number(22,0) not null,
 DESCRIPTION               varchar2(254) not null,
 LONG_DESCRIPTION          varchar2(1000),
 STATUS_CODE_ID            number(22,0) default 1 not null,
 DEPR_TRANS_ALLO_METHOD_ID number(22,0) not null,
 COMPANY_ID                number(22,0),
 USER_ID                   varchar2(18),
 TIME_STAMP                date
);

alter table DEPR_TRANS_SET
   add constraint PK_DEPR_TRANS_SET
       primary key (DEPR_TRANS_SET_ID)
       using index tablespace PWRPLANT_IDX;

alter table DEPR_TRANS_SET
   add constraint FK_DTS_ALLO_METHOD_ID
       foreign key (DEPR_TRANS_ALLO_METHOD_ID)
       references DEPR_TRANS_ALLO_METHOD;

alter table DEPR_TRANS_SET
   add constraint FK_DTS_COID
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

alter table DEPR_TRANS_SET
   add constraint FK_DTS_STATUS_CODE_ID
       foreign key (STATUS_CODE_ID)
       references STATUS_CODE;

create unique index IDX_DEPR_TRANSSET_DESCR
   ON DEPR_TRANS_SET (DESCRIPTION);

-- DEPR_TRANS_SET_DG
create table DEPR_TRANS_SET_DG
(
 DEPR_TRANS_SET_ID number(22,0) not null,
 DEPR_GROUP_ID     number(22,0) not null,
 FACTOR            number(22,8),
 USER_ID           varchar2(18),
 TIME_STAMP        date
);

alter table DEPR_TRANS_SET_DG
   add constraint PK_DEPR_TRANS_SET_DG
       primary key (DEPR_TRANS_SET_ID, DEPR_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

alter table DEPR_TRANS_SET_DG
   add constraint FK_DTSD_DEPR_TRANS_SET_ID
       foreign key (DEPR_TRANS_SET_ID)
       references DEPR_TRANS_SET;

alter table DEPR_TRANS_SET_DG
   add constraint FK_DTSD_DEPR_GROUP_ID
       foreign key (DEPR_GROUP_ID)
       references DEPR_GROUP;

-- DEPR_ACTIVITY
alter table DEPR_ACTIVITY add DEPR_TRANS_SET_ID          number(22,0);
alter table DEPR_ACTIVITY add DEPR_TRANS_SET_ACTIVITY_ID number(22,0);

-- PEND_DEPR_ACTIVITY
alter table PEND_DEPR_ACTIVITY add DEPR_TRANS_SET_ID          number(22,0);
alter table PEND_DEPR_ACTIVITY add DEPR_TRANS_SET_ACTIVITY_ID number(22,0);

--PRIOR PERIOD POSTINGS:  PEND_DEPR_ACTIVITY AND SECURITY
alter table PEND_DEPR_ACTIVITY add CURRENT_MO_YR date;
alter table DEPR_ACTIVITY      add CURRENT_MO_YR DATE;

insert into PP_SECURITY_OBJECTS
   (GROUPS, OBJECTS, DESCRIPTION)
   select GROUPS,
          'w_depr_activity_input.cb_show_prior_periods',
          'Depreciation Activity Input Show Prior Periods'
     from PP_SECURITY_GROUPS
    where GROUPS <> 'system';

insert into PP_SECURITY_OBJECTS
   (GROUPS, OBJECTS, DESCRIPTION)
   select GROUPS,
          'w_depr_activity_approve.cb_prior_periods',
          'Reserve Activity Approval Prior Periods'
     from PP_SECURITY_GROUPS
    where GROUPS <> 'system';

create global temporary table DEPR_POST_ACTIVITY_TEMP
(
 DEPR_ACTIVITY_ID number(22) not null,
 USER_ID          varchar2(18),
 TIME_STAMP       date
) on commit preserve rows;

create global temporary table DEPR_PROCESS_TEMP
(
 DEPR_GROUP_ID   number(22) not null,
 SET_OF_BOOKS_ID number(22) not null,
 GL_POST_MO_YR   date not null,
 USER_ID         varchar2(18),
 TIME_STAMP      date
) on commit preserve rows;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, COMPANY_ID)
   (select max(CONTROL_ID) + 1,
           'Depr Prior Period Months',
           1,
           'Number of prior period months displayed in the Accounting Months selection in Depreciation Group Activity Input screen. (default is 1)',
           -1
      from PP_SYSTEM_CONTROL_COMPANY);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (14, 0, 10, 3, 3, 0, 8604, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008604_008605_depr_inputs.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
