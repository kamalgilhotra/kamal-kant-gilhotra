/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048754_lease_add_columns_ls_lease_options_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.1.0.0 09/19/2017 Johnny Sisouphanh Add column intent_to_purchase
||  									   Add column specialized_asset
||============================================================================*/

alter table ls_lease_options add intent_to_purchase	NUMBER(1);
alter table ls_lease_options add specialized_asset  NUMBER(1);
  
comment on column ls_lease_options.intent_to_purchase  is 'A flag identifying whether there is intent to purchase.';
comment on column ls_lease_options.specialized_asset  is 'A flag identifying if this MLA is for specialized assets.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3721, 0, 2017, 1, 0, 0, 48754, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048754_lease_add_columns_ls_lease_options_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;