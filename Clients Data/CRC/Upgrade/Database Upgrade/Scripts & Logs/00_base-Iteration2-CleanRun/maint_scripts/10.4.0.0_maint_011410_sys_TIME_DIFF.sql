/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011410_sys_TIME_DIFF.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   10/29/2012 Joseph King    Point Release
||============================================================================
*/

create or replace function TIME_DIFF(DATE_1 in date,
                                     DATE_2 in date) return number is

   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: TIME_DIFF
   || Description:
   ||============================================================================
   || Copyright (C) 2012 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- -----------------------------------------
   || 10.4.0.0 10/29/2012 Joseph King    New
   ||============================================================================
   */

   NDATE_1   number;
   NDATE_2   number;
   NSECOND_1 number(5, 0);
   NSECOND_2 number(5, 0);

begin
   -- Get Julian date number from first date (DATE_1)
   NDATE_1 := TO_NUMBER(TO_CHAR(DATE_1, 'J'));

   -- Get Julian date number from second date (DATE_2)
   NDATE_2 := TO_NUMBER(TO_CHAR(DATE_2, 'J'));

   -- Get seconds since midnight from first date (DATE_1)
   NSECOND_1 := TO_NUMBER(TO_CHAR(DATE_1, 'SSSSS'));

   -- Get seconds since midnight from second date (DATE_2)
   NSECOND_2 := TO_NUMBER(TO_CHAR(DATE_2, 'SSSSS'));

   return(((NDATE_2 - NDATE_1) * 86400) + (NSECOND_2 - NSECOND_1));
end TIME_DIFF;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (239, 0, 10, 4, 0, 0, 11410, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011410_sys_TIME_DIFF.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;