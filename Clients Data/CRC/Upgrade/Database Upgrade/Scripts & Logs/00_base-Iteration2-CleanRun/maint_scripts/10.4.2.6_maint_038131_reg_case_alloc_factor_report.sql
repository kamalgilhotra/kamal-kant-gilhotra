/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038131_reg_case_alloc_factor_report.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/29/2014  Kyle Peterson
||========================================================================================
*/

insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1, KEYCOLUMN1, DWNAME2, LABEL2, KEYCOLUMN2,
    DWNAME3, LABEL3, KEYCOLUMN3)
values
   (107, 'Case + eff date + alloc category', 'uo_reg_report_parms_category', 'dw_reg_case_select_dddw', 'Reg Case',
    'reg_case_id', 'dw_reg_alloc_cat_select_dddw', 'Allocation Category', 'reg_alloc_category_id',
    'dw_reg_alloc_date_select_dddw', 'Effective Date', 'effective_date');

insert into PP_REPORT_TYPE (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER) values (408, 'Case Allocations', 8);

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_FILTER_ID)
   select max(REPORT_ID) + 1,
          'Case Allocator Report',
          'This report displays the Statistical Value and Factor value for all Allocators for a given Case, Category, and Effective Date.',
          'Regulatory',
          'dw_reg_report_case_alloc_factor',
          'CA - 101',
          101,
          408,
          107,
          1,
          3,
          1
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1204, 0, 10, 4, 2, 6, 38131, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038131_reg_case_alloc_factor_report.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;