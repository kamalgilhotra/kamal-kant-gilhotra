/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047984_lessor_lsr_lease_group_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ------------------------------------
|| 2017.1.0.0 08/22/2017 Johnny Sisouphanh Create lsr_lease_group table
||============================================================================
*/

create table lsr_lease_group 
  (  
  lease_group_id         number(22,0) not null, 
  time_stamp             date, 
  user_id                varchar2(18), 
  description            varchar2(35), 
  long_description       varchar2(254), 
  workflow_type_id       number(22,0),   
  allow_multiple_vendors number(1,0), 
  send_invoices          number(1,0) default 0, 
  requires_approval      number(1,0)
  );

alter table lsr_lease_group add   
  constraint pk_lsr_lease_group primary key (lease_group_id) using index tablespace pwrplant_idx;
  
comment on column lsr_lease_group.lease_group_id IS 'System generated identifier of a particular lessor lease group';
comment on column lsr_lease_group.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
comment on column lsr_lease_group.user_id IS 'Standard System-assigned user id used for audit purposes.';
comment on column lsr_lease_group.description IS 'Records a brief description of the lease activity type.';
comment on column lsr_lease_group.long_description IS 'a long description field for the table.';
comment on column lsr_lease_group.workflow_type_id IS 'An internal id used for routing records to approval.';
comment on column lsr_lease_group.allow_multiple_vendors IS '1 means multiple vendors/co are allowed';
comment on column lsr_lease_group.send_invoices IS 'Flag to identify whether Invoices are sent to Accounts Receivable.';
comment on column lsr_lease_group.requires_approval IS 'Flag to identify whether Approval is required.';
comment on table lsr_lease_group  IS '(O)  [06] The Lessor Lease Group table contains the primary key and descriptions for Lessor Lease Groups in the PowerPlan Lessee Accounting System.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3671, 0, 2017, 1, 0, 0, 47984, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047984_lessor_lsr_lease_group_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;