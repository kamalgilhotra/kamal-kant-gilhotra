
 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_041870_prov_20151_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/03/2015 	Jarrett Skov   Adding total tax walk through report to PP_REPORTS
 ||============================================================================
 */ 

 delete from pp_reports where report_id = 54540;

insert into pp_reports (report_id, description, long_description, subsystem, datawindow, special_note, report_number, dynamic_dw)
values(
54540, 'Total Tax Walkthrough', 'Total Tax Walkthrough Report - shows current tax expense, deferred tax expense and ETR', 'Tax Accrual', 'dw_tax_accrual_tax_walkthrough', 'CONSOLIDATED-OPER', 'Tax Accrual - 54540', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2472, 0, 2015, 1, 0, 0, 41870, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041870_prov_20151_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;