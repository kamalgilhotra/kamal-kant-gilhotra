/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052717_lessee_02_contract_curr_sys_control_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.1.0.0 11/16/18   C  Yura	  Add new system control for Contract curreny JES
||============================================================================
*/

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
SELECT *
FROM (select
   		max(control_id)+1 AS control_id,
   		'Lease: Contract Currency JEs' AS control_name,
   		'no' AS control_value,
   		'dw_yes_no;1' AS description,
   		'Set to YES to book Lease journal entries in contract currency instead of company currency. Default no.' AS long_description,
   		-1 AS company_id
	from pp_system_control_company
)
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Lease: Contract Currency JEs');

update LS_MC_GL_TRANSACTION_AUDIT set ls_currency_type_id = 2 where ls_currency_type_id  is null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12423, 0, 2018, 1, 0, 0, 52717, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052717_lessee_02_contract_curr_sys_control_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;