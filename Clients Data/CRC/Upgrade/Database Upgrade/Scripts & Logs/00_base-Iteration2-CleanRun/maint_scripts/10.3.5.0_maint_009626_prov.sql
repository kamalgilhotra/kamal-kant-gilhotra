/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009626_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   02/03/2012 Blake Andrews  Point Release
||============================================================================
*/

--###PATCH(9626)
create global temporary table pwrplant.TAX_ACCRUAL_BALANCE_DIFFS_TMP
(  TA_VERSION_ID_FROM      number(22),
   TA_VERSION_ID_TO        number(22),
   RECORD_TYPE_ID          number(22),
   COMPANY_ID              number(22),
   M_ITEM_ID               number(22),
   OPER_IND                number(22),
   TA_NORM_ID              number(22),
   FROM_COMPANY_ID         number(22),
   JE_TYPE_ID              number(22),
   ENTITY_ID               number(22),
   DAMPENING_TYPE_ID       number(22),
   JURISDICTION_ID         number(22),
   GL_ACCOUNT              varchar2(35),
   TAX_YEAR                varchar2(35),
   M_ID                    number(22),
   CURRENT_BEG_BALANCE     number(22,2),
   NEW_BEG_BALANCE         number(22,2),
   AMOUNT_TO_ADD           number(22,2),
   CURRENT_BB_FAS_ACCUM    number(22,2),
   NEW_BB_FAS_ACCUM        number(22,2),
   AMOUNT_TO_ADD_FAS_ACCUM number(22,2),
   CURRENT_BB_FAS_REG      number(22,2),
   NEW_BB_FAS_REG          number(22,2),
   AMOUNT_TO_ADD_FAS_REG   number(22,2),
   CURRENT_BB_FAS_TG       number(22,2),
   NEW_BB_FAS_TG           number(22,2),
   AMOUNT_TO_ADD_FAS_TG    number(22,2),
   CURRENT_BB_FAS_GU       number(22,2),
   NEW_BB_FAS_GU           number(22,2),
   AMOUNT_TO_ADD_FAS_GU    number(22,2),
   CURRENT_BB_FAS_LIAB     number(22,2),
   NEW_BB_FAS_LIAB         number(22,2),
   AMOUNT_TO_ADD_FAS_LIAB  number(22,2),
   ROLL_THIS_ITEM          number(22) default 0,
   constraint TA_BAL_DIFFS_TMP_PK primary key   (  ta_version_id_from, ta_version_id_to, record_type_id, company_id, m_item_id, oper_ind, ta_norm_id, from_company_id, je_type_id,
                                                   entity_id, dampening_type_id, jurisdiction_id, gl_account, tax_year
                                                )
) on commit preserve rows;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (200, 0, 10, 3, 5, 0, 9626, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009626_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
