/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044186_taxrpr_rpr_qty_incl_import_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/10/2015 Anand R        create a new import definition to update Repair Quantity Include
||============================================================================
*/

--import type
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME) 
values ( 121, sysdate, user, 'Update : Repair Quantity Include', 'Import Updates to Repair Quantity Include', 'rpr_import_rpr_qty_incl', 'rpr_import_rpr_qty_incl_arc', null, 1, 'nvo_rpr_logic_import' );

--import type subsystem
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) 
values ( 121, 5, sysdate, user );

-- import column
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 121, 'retirement_unit_id', sysdate, user, 'Retirement Unit', 'retirement_unit_xlate', 1, 1, 'number(22,0)', 'retirement_unit', '', 1, null );

insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) 
values ( 121, 'repair_qty_include', sysdate, user, 'Repair Qty Include', 'repair_qty_include_xlate', 1, 2, 'number(22,0)', 'retirement_unit', '', 1, null );

insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) 
values ( 1074, sysdate, user, 'Retirement Unit.Description', 'The passed in value corresponds to Retirement Unit.Description field.  Translate to Retirement Unit ID using the Description column on the Retirement Unit table.', 'retirement_unit_id', '( select ru.retirement_unit_id from retirement_unit ru where upper( trim( <importfield> ) ) = upper( trim( ru.description ) ) )', 0, 'retirement_unit', 'description', '', '', null );

insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) 
values ( 121, 'retirement_unit_id', 1074, sysdate, user );

insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) 
values ( 121, 'repair_qty_include', 77, sysdate, user );

insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) 
values ( 121, 1074, sysdate, user );

insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) 
values ( 121, 77, sysdate, user );

insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) 
select pp_import_template_seq.nextval, sysdate, user, 121, 'Update Repair Quantity Include', 'Update Repair Quantity Include for existing records', user, sysdate, 1, 1074, null, 0 from dual;

insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) 
select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 121, 'retirement_unit_id', 1074 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 121 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Update Repair Quantity Include';

insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) 
select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 121, 'repair_qty_include', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 121 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Update Repair Quantity Include';    


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2691, 0, 2015, 2, 0, 0, 044186, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044186_taxrpr_rpr_qty_incl_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;