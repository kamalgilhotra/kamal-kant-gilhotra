/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039426_reg_hist_view_as_adj.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/14/2014 Shane Ward       Add As Adjusted column to views
||============================================================================
*/

--History Ledger w/o ID's
update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 14
 where COLUMN_ORDER = 13
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 13
 where COLUMN_ORDER = 12
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 12, COLUMN_WIDTH = 300
 where COLUMN_ORDER = 11
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_WIDTH = 300
 where COLUMN_ORDER = 10
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_sv');

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER, COLUMN_WIDTH,
    QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select ID, 'AS_ADJUSTED', 11, 1, 1, 'As Adjusted', 313, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA
    where TABLE_NAME = 'reg_history_ledger_sv';

create or replace view REG_HISTORY_LEDGER_SV
(
 REG_COMPANY,
 REG_ACCOUNT,
 REG_ACCOUNT_TYPE,
 SUB_ACCOUNT_TYPE,
 REG_SOURCE,
 HISTORIC_LEDGER,
 GL_MONTH,
 ACT_AMOUNT,
 ANNUALIZED_AMT,
 ADJ_AMOUNT,
 ADJ_MONTH,
 AS_ADJUSTED,
 RECON_ADJ_AMOUNT,
 RECON_ADJ_COMMENT
)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       HISTORIC_LEDGER,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       ACT_AMOUNT - ADJ_AMOUNT AS_ADJUSTED,
       RECON_ADJ_AMOUNT,
       RECON_ADJ_COMMENT
  from (select C.DESCRIPTION       REG_COMPANY,
               M.DESCRIPTION       REG_ACCOUNT,
               T.DESCRIPTION       REG_ACCOUNT_TYPE,
               S.DESCRIPTION       SUB_ACCOUNT_TYPE,
               R.DESCRIPTION       REG_SOURCE,
               V.LONG_DESCRIPTION  HISTORIC_LEDGER,
               L.GL_MONTH          GL_MONTH,
               L.ACT_AMOUNT        ACT_AMOUNT,
               L.ANNUALIZED_AMT    ANNUALIZED_AMT,
               L.ADJ_AMOUNT        ADJ_AMOUNT,
               L.ADJ_MONTH         ADJ_MONTH,
               L.RECON_ADJ_AMOUNT  RECON_ADJ_AMOUNT,
               L.RECON_ADJ_COMMENT RECON_ADJ_COMMENT
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER L,
               REG_ACCT_TYPE T,
               REG_SUB_ACCT_TYPE S,
               REG_SOURCE R
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);

--History Ledger with ID's
update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 18
 where COLUMN_ORDER = 17
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_id_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 17
 where COLUMN_ORDER = 16
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_id_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 16
 where COLUMN_ORDER = 15
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_id_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 15
 where COLUMN_ORDER = 14
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_id_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 14
 where COLUMN_ORDER = 13
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_id_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 13
 where COLUMN_ORDER = 12
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_id_sv');

update CR_DD_SOURCES_CRITERIA_FIELDS
   set COLUMN_ORDER = 12, COLUMN_WIDTH = 300
 where COLUMN_ORDER = 11
   and ID = (select ID from CR_DD_SOURCES_CRITERIA where TABLE_NAME = 'reg_history_ledger_id_sv');

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER, COLUMN_WIDTH,
    QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select ID, 'AS_ADJUSTED', 12, 1, 1, 'As Adjusted', 313, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA
    where TABLE_NAME = 'reg_history_ledger_id_sv';

create or replace view REG_HISTORY_LEDGER_ID_SV
(
 REG_COMPANY,
 HISTORIC_LEDGER,
 REG_ACCOUNT,
 REG_ACCOUNT_TYPE,
 SUB_ACCOUNT_TYPE,
 REG_SOURCE,
 GL_MONTH,
 ACT_AMOUNT,
 ANNUALIZED_AMT,
 ADJ_AMOUNT,
 ADJ_MONTH,
 AS_ADJUSTED,
 RECON_ADJ_AMOUNT,
 RECON_ADJ_COMMENT,
 REG_COMPANY_ID,
 REG_ACCT_ID,
 HISTORIC_LEDGER_ID,
 REG_SOURCE_ID
)
as
select REG_COMPANY,
       HISTORIC_LEDGER,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       ACT_AMOUNT - ADJ_AMOUNT AS_ADJUSTED,
       RECON_ADJ_AMOUNT,
       RECON_ADJ_COMMENT,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       HISTORIC_LEDGER_ID,
       REG_SOURCE_ID
  from (select C.DESCRIPTION         REG_COMPANY,
               M.DESCRIPTION         REG_ACCOUNT,
               T.DESCRIPTION         REG_ACCOUNT_TYPE,
               S.DESCRIPTION         SUB_ACCOUNT_TYPE,
               R.DESCRIPTION         REG_SOURCE,
               V.LONG_DESCRIPTION    HISTORIC_LEDGER,
               L.GL_MONTH            GL_MONTH,
               L.ACT_AMOUNT          ACT_AMOUNT,
               L.ANNUALIZED_AMT      ANNUALIZED_AMT,
               L.ADJ_AMOUNT          ADJ_AMOUNT,
               L.ADJ_MONTH           ADJ_MONTH,
               L.RECON_ADJ_AMOUNT    RECON_ADJ_AMOUNT,
               L.RECON_ADJ_COMMENT   RECON_ADJ_COMMENT,
               L.REG_COMPANY_ID      REG_COMPANY_ID,
               L.REG_ACCT_ID         REG_ACCT_ID,
               L.HISTORIC_VERSION_ID HISTORIC_LEDGER_ID,
               L.REG_SOURCE_ID       REG_SOURCE_ID
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER L,
               REG_ACCT_TYPE T,
               REG_SUB_ACCT_TYPE S,
               REG_SOURCE R
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1336, 0, 10, 4, 2, 7, 39426, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039426_reg_hist_view_as_adj.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
