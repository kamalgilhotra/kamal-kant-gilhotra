/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052987_pwrtax_02_norm_schema_tu_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 03/13/2019  K. Powers	   PowerTax Normalization Schema True Up
||============================================================================
*/

INSERT
INTO ppbase_workspace
  (
    module,
    workspace_identifier,
    label,
    workspace_uo_name,
    minihelp,
    object_Type_id
  )
  VALUES
  (
    'powertax',
    'deferred_setup_norm_trueup',
    'Norm Schema Trueup',
    'w_norm_schema_trueup',
    'Normalization Trueup',
    2
  );
  
INSERT
INTO ppbase_menu_items
  (
    module,
    menu_identifier,
    menu_level,
    item_order,
    label,
    minihelp,
    parent_menu_identifier,
    workspace_identifier,
    enable_yn
  )
  VALUES
  (
    'powertax',
    'deferred_setup_norm_trueup',
    3,
    4,
    'Norm Schema Trueup',
    NULL,
    'deferred_setup',
    'deferred_setup_norm_trueup',
    1
  );

UPDATE ppbase_workspace
SET object_Type_id        =1,
  workspace_uo_name       ='uo_tax_dfit_norm_trueup'
WHERE workspace_identifier='deferred_setup_norm_trueup';

-- Insert new filters

INSERT
INTO pp_Reports_filter
  (
    pp_report_filter_id,
    description,
    filter_uo_name
  )
SELECT 105,
  'PowerTax - Norm Schema Trueup',
  'uo_ppbase_tab_filter_dynamic'
FROM dual;

INSERT INTO pp_dynamic_filter
SELECT
  287,
  'Tax Year',
  'dw',
  'tax_year_version.tax_year',
  NULL,
  'dw_tax_year_unlock_by_version_filter',
  'tax_year',
  'tax_year',
  'D',
  1,
  0,
  NULL,
  NULL,
  NULL,
  NULL,
  0,
  0,
  0
FROM dual;

INSERT INTO pp_dynamic_Filter_mapping
  (pp_Report_filter_id, filter_id
  )
SELECT 105,
  filter_id
FROM pp_dynamic_filter_mapping
WHERE pp_Report_filter_id=84
UNION
SELECT 105, 102 FROM dual
UNION
SELECT 105, 13 FROM dual
UNION
SELECT 105,
  (SELECT filter_id
  FROM pp_dynamic_Filter
  WHERE label               ='Tax Year'
  AND dw                    ='dw_tax_year_unlock_by_version_filter'
  AND sqls_column_expression='tax_year_version.tax_year'
  )
FROM dual;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14784, 0, 2018, 2, 0, 0, 52987, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052987_pwrtax_02_norm_schem_tu_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

