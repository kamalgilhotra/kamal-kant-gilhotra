/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009657_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   05/08/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Create a new import type for composite levy rates.
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID ) values ( 26, sysdate, user, 'Add : Levy Rates (Composite)', 'Import New Levy Rates for Composite Tax Authorities', 'pt_import_levy_rates', 'pt_import_levy_rates_archive', null );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 26, sysdate, user, 'case_id', 'Case', 'case_xlate', 1, 1, 'number(22,0)', 'pt_case' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 26, sysdate, user, 'prop_tax_company_id', 'Prop Tax Company', 'prop_tax_company_xlate', 0, 1, 'number(22,0)', 'pt_company' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 26, sysdate, user, 'state_id', 'State', 'state_xlate', 0, 1, 'char(18)', 'state' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 26, sysdate, user, 'county_id', 'County', 'county_xlate', 0, 2, 'char(18)', 'county' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 26, sysdate, user, 'tax_district_id', 'Tax District', 'tax_district_xlate', 0, 3, 'number(22,0)', 'prop_tax_district' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 26, sysdate, user, 'statement_group_id', 'Statement Group', 'statement_group_xlate', 0, 2, 'number(22,0)', 'pt_statement_group' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 26, sysdate, user, 'levy_class_id', 'Levy Class', 'levy_class_xlate', 1, 1, 'number(22,0)', 'pt_levy_class' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE ) values ( 26, sysdate, user, 'mill_levy_rate', 'Levy Rate', null, 0, 1, 'number(22,12)', null );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED ) values ( 151, sysdate, user, 'PT Statement Group.Description', 'The passed in value corresponds to the PT Statement Group: Description field.  Translate to the Statement Group ID using the Description column on the PT Statement Group table.', 'statement_group_id', '( select sg.statement_group_id from pt_statement_group sg where upper( trim( <importfield> ) ) = upper( trim( sg.description ) ) )', 0, 0 );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED ) values ( 152, sysdate, user, 'PT Statement Group.Description (for given State and Prop Tax Company)', 'The passed in value corresponds to the PT Statement Group: Description field (for the given state and prop tax company).  Translate to the Statement Group ID using the Description, State and Prop Tax Company columns on the PT Statement Group table.', 'statement_group_id', '( select sg.statement_group_id from pt_statement_group sg where upper( trim( <importfield> ) ) = upper( trim( sg.description ) ) and <importtable>.state_id = sg.state_id and <importtable>.prop_tax_company_id = sg.prop_tax_company_id )', 0, 0 );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED ) values ( 153, sysdate, user, 'PT Statement Group.Description (for given State)', 'The passed in value corresponds to the PT Statement Group: Description field (for the given state).  Translate to the Statement Group ID using the Description and State columns on the PT Statement Group table.', 'statement_group_id', '( select sg.statement_group_id from pt_statement_group sg where upper( trim( <importfield> ) ) = upper( trim( sg.description ) ) and <importtable>.state_id = sg.state_id )', 0, 0 );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED ) values ( 154, sysdate, user, 'PT Statement Group.Description (for given Prop Tax Company)', 'The passed in value corresponds to the PT Statement Group: Description field (for the given prop tax company).  Translate to the Statement Group ID using the Description and Prop Tax Company columns on the PT Statement Group table.', 'statement_group_id', '( select sg.statement_group_id from pt_statement_group sg where upper( trim( <importfield> ) ) = upper( trim( sg.description ) ) and <importtable>.prop_tax_company_id = sg.prop_tax_company_id )', 0, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'case_id', 65 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'case_id', 66 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'county_id', 43 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'county_id', 61 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'county_id', 62 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'county_id', 63 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'county_id', 64 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'levy_class_id', 50 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'levy_class_id', 51 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'prop_tax_company_id', 3 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'prop_tax_company_id', 4 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'prop_tax_company_id', 5 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'prop_tax_company_id', 6 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'state_id', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'state_id', 2 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'statement_group_id', 151 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'statement_group_id', 152 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'statement_group_id', 153 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'statement_group_id', 154 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 79 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 80 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 81 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 82 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 83 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 84 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 85 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 86 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 87 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 105 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 106 );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 26, sysdate, user, 'tax_district_id', 107 );

alter table PWRPLANT.PT_IMPORT_LEVY_RATES         add PROP_TAX_COMPANY_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEVY_RATES_ARCHIVE add PROP_TAX_COMPANY_XLATE varchar2(254);

alter table PWRPLANT.PT_IMPORT_LEVY_RATES         add PROP_TAX_COMPANY_ID number(22,0);
alter table PWRPLANT.PT_IMPORT_LEVY_RATES_ARCHIVE add PROP_TAX_COMPANY_ID number(22,0);

alter table PWRPLANT.PT_IMPORT_LEVY_RATES         add STATEMENT_GROUP_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEVY_RATES_ARCHIVE add STATEMENT_GROUP_XLATE varchar2(254);

alter table PWRPLANT.PT_IMPORT_LEVY_RATES         add STATEMENT_GROUP_ID number(22,0);
alter table PWRPLANT.PT_IMPORT_LEVY_RATES_ARCHIVE add STATEMENT_GROUP_ID number(22,0);

alter table PWRPLANT.PT_IMPORT_LEVY_RATES         add TAX_DISTRICT_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEVY_RATES_ARCHIVE add TAX_DISTRICT_XLATE varchar2(254);

alter table PWRPLANT.PT_IMPORT_LEVY_RATES         add TAX_DISTRICT_ID number(22,0);
alter table PWRPLANT.PT_IMPORT_LEVY_RATES_ARCHIVE add TAX_DISTRICT_ID number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (135, 0, 10, 3, 5, 0, 9657, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009657_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
