/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045423_projects_backfill_ocr_api_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.1.0 02/12/2016 Anand R        Backfill ocr_api column for existing retirements
||============================================================================
*/

update wo_estimate e
set ocr_api = 2
where nvl(ocr_api,0) = 0
and expenditure_type_id = 2
and est_chg_type_id in
    (select ect.est_chg_type_id from estimate_charge_type ect
    where processing_type_id = 1)
and exists
(select 1 from unitized_work_order uwo
where uwo.unit_estimate_id = e.estimate_id
and uwo.work_order_id = e.work_order_id
)
; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3057, 0, 2015, 2, 1, 0, 045423, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045423_projects_backfill_ocr_api_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;