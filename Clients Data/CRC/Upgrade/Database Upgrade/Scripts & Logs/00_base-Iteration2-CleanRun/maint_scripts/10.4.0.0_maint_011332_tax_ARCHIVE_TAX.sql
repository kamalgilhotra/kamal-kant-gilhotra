/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011332_tax_ARCHIVE_TAX.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   03/05/2013 Roger Roach    Point Release
||============================================================================
*/

create table ARC_TAX_RENUMBER_TIDS
(
 TAX_TRANSFER_ID_OLD number(22,0),
 TAX_TRANSFER_ID_NEW number(22,0)
);

alter table ARC_TAX_RENUMBER_TIDS
   add constraint ARC_TAX_RENUMBER_TIDS_PK
       primary key (TAX_TRANSFER_ID_OLD)
       using index tablespace PWRPLANT_IDX;



create or replace package ARCHIVE_TAX

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: ARCHIVE_TAX
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   ||         01/25/1999
   ||         04/30/2003                Fixed Archive Forecase
   ||         03/05/2013 Roger Roach    Maint 11332
   ||============================================================================
   */

 as
   type CONS_STRUCT_REC is record(
      name      varchar2(40),
      CONS      varchar2(40),
      COLS      varchar2(2000),
      REF_NAME  varchar2(40),
      REF_CONS  varchar2(40),
      REF_COLS  varchar2(2000),
      CONS_TYPE char(1));

   type TRACE_STRUCT_REC is record(
      STR        varchar2(4000),
      TRACE_TYPE integer,
      TRACE_DATE date);

   type CONS_LIST_S is table of CONS_STRUCT_REC index by binary_integer;

   type TABLE_LIST_TYPE is table of varchar2(40) index by binary_integer;

   type TRACE_LIST_TYPE is table of TRACE_STRUCT_REC index by binary_integer;

   type SQL_LIST_TYPE is table of varchar2(2048) index by binary_integer;

   function GET_VERSION return number;

   procedure DELETE_CASE(VERSION_NUMBER number,
                         OWNER_NAME     in varchar2,
                         TABLE2         in varchar2);

   procedure RESTORE(VERSION_NUMBER number);

   procedure ARCHIVE(VERSION_NUMBER number,
                     OWNER_NAME     in varchar2,
                     TABLE2         in varchar2);

   procedure DELETE_ARCHIVE(VERSION_NUMBER number);

   procedure DELETE_FCST_CASE(FCST_VERSION   number,
                              VERSION_NUMBER number);

   procedure RESTORE_FCST(FCST_VERSION   number,
                          VERSION_NUMBER number);

   procedure ARCHIVE_FCST(FCST_VERSION   number,
                          VERSION_NUMBER number);

   procedure DELETE_FCST_ARCHIVE(FCST_VERSION   number,
                                 VERSION_NUMBER number);

   function DELETE_TABLE(TABLE_NAME_STR varchar,
                         OWNER_NAME     varchar,
                         WHERE_CLAUSE   varchar,
                         ERR_MSG        out varchar) return integer;

   function GET_CONSTRAINTS(A_TABLE_NAMES in TABLE_LIST_TYPE,
                            A_COUNT       in integer,
                            A_CONS_LIST   out CONS_LIST_S,
                            OWNER_NAME    in varchar2 := 'PWRPLANT',
                            A_SUBSYSTEM   in varchar2 := 'unknown') return integer;

   function DROP_CONSTRAINTS(A_CONS_LIST  in CONS_LIST_S,
                             A_TRACE_LIST in out TRACE_LIST_TYPE,
                             OWNER_NAME   in varchar2 := 'PWRPLANT') return integer;

   function SET_CONSTRAINTS(A_CONS_LIST  in CONS_LIST_S,
                            A_TRACE_LIST in out TRACE_LIST_TYPE,
                            OWNER_NAME   in varchar2 := 'PWRPLANT') return integer;

   procedure TRACE_SAVE(A_TRACE_LIST in out TRACE_LIST_TYPE,
                        A_JOB_NO     integer,
                        A_LINE_NO    in out integer);

   procedure TRACE_WRITE(A_TRACE_LIST in out TRACE_LIST_TYPE,
                         A_STR        in varchar2,
                         A_TYPE       integer);

   procedure START_JOB(A_VERSION_NUMBER in number,
                       A_FCST_NUMBER    in number,
                       A_ARC_TYPE       in varchar2,
                       A_JOBNO          in out number,
                       A_LOGID          in out number);

   procedure STOP_JOB(A_VERSION_NUMBER in number,
                      A_ARC_TYPE       in varchar2,
                      A_JOBNO          in number,
                      A_LOGID          in number,
                      A_STATUS         in varchar2);

   function GET_COLUMNS(A_TABLE_NAME    in varchar2,
                        A_TABLE_ABBREV  in varchar2,
                        A_OWNER_NAME    in varchar2,
                        A_EX_TABLE_LIST in TABLE_LIST_TYPE,
                        A_COLUMN_STR    in out varchar2,
                        A_ERR_MSG       out varchar2) return integer;

   function CREATE_COLUNN(A_TABLE  in varchar2,
                          A_COLUMN in varchar2) return integer;

   function CREATE_TABLE(A_TABLE in varchar2) return integer;

   function DUPICATE_TABLE(A_FROM_TABLE  in varchar2,
                           A_NEW_TABLE   in varchar2,
                           A_ALTER_EXTRA in varchar2,
                           A_ERR_MSG     in out varchar2) return integer;

   function CHECK_ARC_TABLE(A_TABLE1  in varchar2,
                            A_TABLE2  in varchar2,
                            A_ADD_SQL in varchar2) return integer;

   function CHECK_TABLE_COLUMNS(A_TABLE1  in varchar2,
                                A_TABLE2  in varchar2,
                                A_ERR_MSG in out varchar2) return integer;

   function GET_COLUMN_FORMAT(A_NAME      varchar2,
                              A_FIELD     varchar2,
                              A_PRECISION number,
                              A_SCALE     number,
                              A_WIDTH     number,
                              A_NULLABLE  varchar2,
                              A_DEFAULT   varchar2,
                              A_SQLS      in out varchar2) return integer;

   function CHECK_RECORD_COUNT(A_VERSION_NUMBER in number,
                               A_ARC_TYPE       in varchar2,
                               A_LOGID          number) return integer;

   function GET_TABLE_LIST(A_TABLES               in out TABLE_LIST_TYPE,
                           A_ARC_TABLES           out TABLE_LIST_TYPE,
                           A_FCST_TABLES          in out TABLE_LIST_TYPE,
                           A_ARC_FCST_TABLES      out TABLE_LIST_TYPE,
                           A_FCST_ARC_FCST_TABLES out TABLE_LIST_TYPE) return integer;
end;
/


create or replace package body ARCHIVE_TAX as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: ARCHIVE_TAX
   || Description:
   ||============================================================================
   || Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 6.00                              add short year
   || 6.01    06/16/2000                add ex_gain_loss and ex_retire_res_impact
   || 6.02    07/06/2000                renumbering tax record ids
   || 7.00    04/11/2002                added support transfer tables and dynamic columns
   || 7.17    05/28/2002                fixed a problem with Oracle 8.0 and in and out params
   || 7.23                              job_status ;= status after a label gives an error
   || 7.25                              fixed a problem with copy tax_transfer_control data to the arc_tax_transfer_control table
   || 7.28                              fixed select problems on tax_transfer_control
   || 7.29                              added code to turn off auditing
   || 7.30                              a fixed job_status was 6 now is 0 before incomplete in procedure archive
   || 7.31                              check the record count before deleting any records
   || 7.32                              fixed the order of retrieving tax_transfer_control
   || 7.33                              made the exec_sql functinon a non public function
   || 7.34                              renumber the tax_transfer_ids on restoring the data
   || 7.35                              fixed column miising in tax_transfer_ids on restoring
   || 7.36                              check for null on max tax_transfer_id while restoring tax_transfer_control
   || 7.37    03/16/2011                added arc_tax_renumber_tids table to restore tax_tranfer tables
   || 7.38    08/16/2011                analyze arc_tax_renumber_id table
   ||============================================================================
   */

   --**************************************************************************
   --                   Local Function Definition - Exec SQL
   --**************************************************************************
   function EXEC_SQL(A_SQL     in varchar2,
                     A_ERR_MSG in out varchar2) return integer;

   --**************************************************************************
   --                            Start Body
   --**************************************************************************

   --**************************************************************************
   --                            Get Version
   --**************************************************************************
   function GET_VERSION return number as

   begin
      return 7.38;
   end GET_VERSION;


   --**************************************************************************
   --                               Archive
   --**************************************************************************
   procedure ARCHIVE(VERSION_NUMBER number,
                     OWNER_NAME     in varchar2,
                     TABLE2         in varchar2) as

      STATUS      long(4000);
      J           int;
      I           int;
      WARNINGS    int := 0;
      LINE_NO     int := 0;
      I_JOBNO     int := 0;
      I_LOGID     int := 0;
      STR         varchar2(1024);
      SQL2        varchar2(1024);
      VERSION_KEY number(22, 2);
      CODE        int;

      FCST_STATUS_CODE   int;
      FCST_VERSION       int;
      VERSION_CODE       int;
      INCOMPLETE_ARCHIVE int := 10;
      ARCHIVE_ERROR      int := 6;
      JOB_STATUS         int;
      SQLS               varchar(8048);
      COLUMN_STR         varchar(8048);
      COLUMN_STR2        varchar(8048);
      USER_CURSOR        integer;
      NUM                int;
      NUM2               int;
      --table_names table_list_type := table_list_type('TAX_RECORD_CONTROL','TAX_CONTROL','TAX_DEPRECIATION','TAX_DEPR_ADJUST','TAX_BOOK_RECONCILE',
      --    8.0 stuff                          'BASIS_AMOUNTS','DEFERRED_INCOME_TAX','TAX_ANNOTATION');
      TABLE_NAMES               TABLE_LIST_TYPE;
      TABLE_ARC_NAMES           TABLE_LIST_TYPE;
      TABLE_FCST_NAMES          TABLE_LIST_TYPE;
      TABLE_FCST_ARC_NAMES      TABLE_LIST_TYPE;
      TABLE_FCST_ARC_FCST_NAMES TABLE_LIST_TYPE;
      EX_COLUMN_LIST            TABLE_LIST_TYPE;
      ERR_MSG                   varchar2(8000);
      CONS_LIST                 CONS_LIST_S;
      TABLE_COUNT               integer := 8;
      TRACE_LIST                TRACE_LIST_TYPE;
      STATUS_FLAG               varchar(30);
      RTN_STATUS                int;

      cursor FCST_CUR is
         select TAX_FORECAST_VERSION_ID
           from TAX_FORECAST_VERSION
          where STATUS = FCST_STATUS_CODE
            and VERSION_ID = VERSION_CODE;

   begin
      --update tax_archive_status set description = 'Start Archiving' where version_id = version_number;
      VERSION_CODE := VERSION_NUMBER;
      DBMS_OUTPUT.PUT_LINE('test');
      begin
         CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      exception
         when others then
            CODE := 0;
      end;
      CODE := GET_TABLE_LIST(TABLE_NAMES,
                             TABLE_ARC_NAMES,
                             TABLE_FCST_NAMES,
                             TABLE_FCST_ARC_NAMES,
                             TABLE_FCST_ARC_FCST_NAMES);

      START_JOB(VERSION_NUMBER, 0, 'Tax', I_JOBNO, I_LOGID);
      commit;

      select STATUS into JOB_STATUS from VERSION where VERSION_ID = VERSION_NUMBER;
      if JOB_STATUS = INCOMPLETE_ARCHIVE then
         goto INCOMPLETE_START;
      end if;

      select count(*) into NUM from TAX_RECORD_CONTROL where VERSION_ID = VERSION_NUMBER;
      if NUM = 0 then
         goto INCOMPLETE_COPY;
      end if;
      JOB_STATUS := ARCHIVE_ERROR;

      STR := 'Max Tax Record Control';

      select min(TAX_RECORD_ID)
        into VERSION_KEY
        from PWRPLANT.TAX_RECORD_CONTROL
       where VERSION_ID = VERSION_NUMBER;

      if (NVL(VERSION_KEY, -1) = -1) then
         STR := 'Version ID not found in  TAX_RECORD_CONTROL';
         goto QQUIT;
         return;
      end if;

      TRACE_WRITE(TRACE_LIST, 'Minimum Tax Record ID ' || TO_CHAR(VERSION_KEY), 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      /* set status to archiving */
      STR := 'Updating version table status. ';
      update VERSION set STATUS = 1 where VERSION_ID = VERSION_NUMBER;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Updating version table status. code = ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      /* truncate the table used to renumber tax_record_ids */
      /* truncate table arc_tax_renumber_id; */
      STR  := 'Truncating arc_tax_renumber_id. ';
      SQLS := 'truncate table arc_tax_renumber_id';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      if EXEC_SQL(SQLS, ERR_MSG) = -1 then
         STR := ERR_MSG;
         goto QQUIT;
      end if;

      /* get old  tax_record_ids */
      TRACE_WRITE(TRACE_LIST, 'inserting arc_tax_renumber_id table', 1);
      insert into ARC_TAX_RENUMBER_ID
         (TAX_RECORD_ID_OLD, TAX_RECORD_ID_NEW)
         select TAX_RECORD_ID, ROWNUM from TAX_RECORD_CONTROL where VERSION_ID = VERSION_NUMBER;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'inserting arc_tax_renumber_id table status. code = ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      commit;
      /* analyze  arc_tax_renumber_id; */
      STR := 'analyze arc_tax_renumber_id. ';
      begin
         CODE := ANALYZE_TABLE('arc_tax_renumber_id', -1);
      exception
         when others then
            STR := 'error analyze arc_tax_renumber_id. code=' || TO_CHAR(CODE) || ' ' ||
                   sqlerrm(CODE);
      end;
      TRACE_WRITE(TRACE_LIST, STR, 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);

      for I in 1 .. TABLE_NAMES.COUNT
      loop
         DBMS_OUTPUT.PUT_LINE(TABLE_NAMES(I));
         CODE := CHECK_ARC_TABLE(TABLE_NAMES(I),
                                 TABLE_ARC_NAMES(I),
                                 ' add (version_id number(22,0))');
         if CODE <> 0 then
            STR := 'Error updating columns ' || TABLE_ARC_NAMES(I) || ' code =  ' || CODE;
            goto QQUIT;
         end if;
         EX_COLUMN_LIST.DELETE;
         NUM  := GET_COLUMNS(TABLE_NAMES(I), 'a', OWNER_NAME, EX_COLUMN_LIST, COLUMN_STR, ERR_MSG);
         NUM2 := GET_COLUMNS(TABLE_ARC_NAMES(I),
                             '',
                             OWNER_NAME,
                             EX_COLUMN_LIST,
                             COLUMN_STR2,
                             ERR_MSG);
         if NUM <> NUM2 then
            STR := 'Table Columns are not equal ' || TABLE_ARC_NAMES(I) || ' = ' || NUM2 || ' std=' || NUM;
            goto QQUIT;
         end if;
      end loop;

      /****  tax reconcile *******/

      for I in 1 .. TABLE_NAMES.COUNT
      loop
         STR := 'Insert into Arc ' || TABLE_NAMES(I);
         TRACE_WRITE(TRACE_LIST, STR, 1);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         if LOWER(TABLE_NAMES(I)) <> 'tax_record_control' then
            CODE := CHECK_ARC_TABLE(TABLE_NAMES(I),
                                    TABLE_ARC_NAMES(I),
                                    ' add column(version_id(22,0))');
         else
            --add column(status number(22,0))
            CODE := CHECK_ARC_TABLE(TABLE_NAMES(I), TABLE_ARC_NAMES(I), ' ');
         end if;
         if CODE <> 0 then
            STR := 'Error updating columns ' || TABLE_ARC_NAMES(I) || ' code =  ' || CODE;
            goto QQUIT;
         end if;
         EX_COLUMN_LIST(1) := 'to_trid';
         EX_COLUMN_LIST(2) := 'from_trid';
         NUM := GET_COLUMNS(TABLE_NAMES(I), 'a', OWNER_NAME, EX_COLUMN_LIST, COLUMN_STR, ERR_MSG);
         NUM2 := GET_COLUMNS(TABLE_ARC_NAMES(I),
                             '',
                             OWNER_NAME,
                             EX_COLUMN_LIST,
                             COLUMN_STR2,
                             ERR_MSG);
         EX_COLUMN_LIST.DELETE;
         if NUM <> NUM2 then
            STR := 'Table Columns are not equal ' || TABLE_ARC_NAMES(I) || ' = ' || NUM2 || ' std=' || NUM;
            goto QQUIT;
         end if;

         if LOWER(TABLE_NAMES(I)) = 'tax_transfer_control' then
            SQLS := 'insert into ' || TABLE_ARC_NAMES(I) || '(version_id,to_trid,from_trid,' ||
                    COLUMN_STR2 || ')' || ' Select ' || VERSION_NUMBER ||
                    ', d.tax_record_id_new ,d2.tax_record_id_new, ' || COLUMN_STR || ' FROM ' ||
                    TABLE_NAMES(I) || ' a,tax_record_control c,arc_tax_renumber_id d, ' ||
                    '  arc_tax_renumber_id d2 ' || ' WHERE a.to_trid = d.tax_record_id_old and ' ||
                    'a.from_trid = d2.tax_record_id_old and ' ||
                    'a.from_trid = c.tax_record_id and ' || 'c.version_id = ' || VERSION_NUMBER;
         else
            SQLS := 'insert into ' || TABLE_ARC_NAMES(I) || '(version_id,tax_record_id,' ||
                    COLUMN_STR2 || ')' || ' Select ' || VERSION_NUMBER || ', d.tax_record_id_new ,' ||
                    COLUMN_STR || ' FROM ' || TABLE_NAMES(I) ||
                    ' a,tax_record_control c,arc_tax_renumber_id d ' ||
                    ' WHERE a.tax_record_id = c.tax_record_id and ' ||
                    'c.tax_record_id = d.tax_record_id_old and ' || 'c.version_id = ' ||
                    VERSION_NUMBER;
         end if;
         CODE := EXEC_SQL(SQLS, ERR_MSG);
         if CODE <> -1403 and CODE <> 0 then
            STR := 'Inserting ' || TABLE_ARC_NAMES(I) || ' table status. code = ' || CODE ||
                   ' msg=' || ERR_MSG;
            goto QQUIT;
         end if;
      end loop;

      TRACE_WRITE(TRACE_LIST, STR, 1);
      TRACE_WRITE(TRACE_LIST, 'Start Forecast', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);

      if CHECK_RECORD_COUNT(VERSION_NUMBER, 'Archive', I_LOGID) = -1 then
         CODE := sqlcode;
         STR  := 'Archive and Tax Record are not equal code = ' || sqlerrm(CODE);
         goto QQUIT;
      end if;

      <<INCOMPLETE_COPY>>
   -- Delete Forecast versions
      FCST_STATUS_CODE := 13; -- Deleting
      open FCST_CUR;
      loop
         fetch FCST_CUR
            into FCST_VERSION;
         if (FCST_CUR%notfound) then
            TRACE_WRITE(TRACE_LIST, 'No Forecast', 1);
            exit;
         end if;

         for I in 1 .. TABLE_FCST_NAMES.COUNT
         loop
            TRACE_WRITE(TRACE_LIST, 'delete ' || TABLE_FCST_NAMES(I), 1);
            SQLS := 'delete ' || TABLE_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                    FCST_VERSION;
            if EXEC_SQL(SQLS, ERR_MSG) <> 0 then
               STR := 'deleting tax_forecast_output  code = ' || ERR_MSG || '  fcst_version' ||
                      TO_CHAR(FCST_VERSION);
               goto QQUIT;
            end if;
         end loop;

         delete TAX_FORECAST_VERSION where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'deleting tax_forecast_version  code = ' || sqlerrm(CODE) || '  fcst_version' ||
                   TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      close FCST_CUR;
      -- Delete Archive Forecast versions
      TRACE_WRITE(TRACE_LIST, 'Start Del  Forecast', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      FCST_STATUS_CODE := 14; -- Deleting Forecast Archive
      open FCST_CUR;
      loop
         fetch FCST_CUR
            into FCST_VERSION;
         if (FCST_CUR%notfound) then
            TRACE_WRITE(TRACE_LIST, 'No Del Archive Forecast', 1);
            exit;
         end if;
         TRACE_WRITE(TRACE_LIST, 'delete arc_tax_forecast_output', 1);
         delete ARC_FCST_TAX_FORECAST_OUTPUT where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'deleting arc_fcst_tax_forecast_output  code = ' || sqlerrm(CODE) ||
                   '  fcst_version' || TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
         TRACE_WRITE(TRACE_LIST, 'arc_fcst_tax_forecast_input', 1);
         delete ARC_FCST_TAX_FORECAST_INPUT where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'deleting arc_fcst_tax_forecast_input  code = ' || sqlerrm(CODE) ||
                   '  fcst_version' || TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
         TRACE_WRITE(TRACE_LIST, 'arc_fcst_dfit_forecast_output', 1);
         delete ARC_FCST_DFIT_FORECAST_OUTPUT where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'deleting arc_fcst_dfit_forecast_output  code = ' || sqlerrm(CODE) ||
                   '  fcst_version' || TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
         TRACE_WRITE(TRACE_LIST, 'delete tax_forecast_version', 1);
         delete TAX_FORECAST_VERSION where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'deleting tax_forecast_version  code = ' || sqlerrm(CODE) || '  fcst_version' ||
                   TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      close FCST_CUR;

      -- Archive Tax Forecast versions
      TRACE_WRITE(TRACE_LIST, 'Start Archive  Forecast', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      FCST_STATUS_CODE := 1; -- Archive Tax
      open FCST_CUR;
      loop
         fetch FCST_CUR
            into FCST_VERSION;
         if (FCST_CUR%notfound) then
            TRACE_WRITE(TRACE_LIST, 'No Archive  Forecast', 1);
            exit;
         end if;
         for I in 1 .. TABLE_FCST_NAMES.COUNT
         loop
            NUM  := GET_COLUMNS(TABLE_FCST_NAMES(I),
                                'a',
                                OWNER_NAME,
                                EX_COLUMN_LIST,
                                COLUMN_STR,
                                ERR_MSG);
            NUM2 := GET_COLUMNS(TABLE_FCST_ARC_NAMES(I),
                                '',
                                OWNER_NAME,
                                EX_COLUMN_LIST,
                                COLUMN_STR2,
                                ERR_MSG);
            CODE := CHECK_ARC_TABLE(TABLE_FCST_NAMES(I), TABLE_FCST_ARC_NAMES(I), ' ');
            if CODE <> 0 then
               STR := 'Error updating columns ' || TABLE_FCST_ARC_NAMES(I) || ' code =  ' || CODE;
               goto QQUIT;
            end if;

            SQLS := 'insert into ' || TABLE_FCST_ARC_NAMES(I) || '(tax_record_id,' || COLUMN_STR2 || ')' ||
                    ' Select d.tax_record_id_new ,' || COLUMN_STR || ' from ' ||
                    TABLE_FCST_NAMES(I) || ' a,arc_tax_renumber_id d' ||
                    ' WHERE  tax_forecast_version_id = ' || FCST_VERSION || ' and ' ||
                    ' tax_record_id = tax_record_id_old ';

            if EXEC_SQL(SQLS, ERR_MSG) <> 0 then
               STR := 'Inserting ' || TABLE_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                      '  fcst_version ' || TO_CHAR(FCST_VERSION);
               goto QQUIT;
            end if;
            TRACE_WRITE(TRACE_LIST, 'insert into  ' || TABLE_FCST_NAMES(I), 1);
            SQLS := 'delete ' || TABLE_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                    FCST_VERSION;
            if EXEC_SQL(SQLS, ERR_MSG) <> 0 then
               STR := 'deleting ' || TABLE_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                      '  fcst_version ' || TO_CHAR(FCST_VERSION);
               goto QQUIT;
            end if;
         end loop;

         update TAX_FORECAST_VERSION set STATUS = 2 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'updating tax_forecast_version code = ' || sqlerrm(CODE) || '  fcst_version ' ||
                   TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      close FCST_CUR;

      TRACE_WRITE(TRACE_LIST, 'End Archive  Forecast', 1);
      TRACE_WRITE(TRACE_LIST, 'Start Del  Archive  Forecast', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);

      -- Archive Fcst to  Tax Forecast versions
      FCST_STATUS_CODE := 19; -- Archive Tax

      open FCST_CUR;
      loop
         fetch FCST_CUR
            into FCST_VERSION;
         if (FCST_CUR%notfound) then
            TRACE_WRITE(TRACE_LIST, 'No Del  Archive  Forecast', 1);
            exit;
         end if;

         for I in 1 .. TABLE_FCST_NAMES.COUNT
         loop
            TRACE_WRITE(TRACE_LIST, 'insert into ' || TABLE_FCST_ARC_FCST_NAMES(I), 1);
            NUM  := GET_COLUMNS(TABLE_FCST_ARC_FCST_NAMES(I),
                                'a',
                                OWNER_NAME,
                                EX_COLUMN_LIST,
                                COLUMN_STR,
                                ERR_MSG);
            NUM2 := GET_COLUMNS(TABLE_FCST_ARC_NAMES(I),
                                '',
                                OWNER_NAME,
                                EX_COLUMN_LIST,
                                COLUMN_STR2,
                                ERR_MSG);
            if NUM <> NUM2 then
               STR := 'Table Columns are not equal ' || TABLE_FCST_ARC_FCST_NAMES(I) || ' arc = ' || NUM ||
                      ' std=' || NUM2;
               goto QQUIT;
            end if;

            SQLS := 'insert into ' || TABLE_FCST_ARC_NAMES(I) || '(tax_record_id,' || COLUMN_STR2 || ')' ||
                    ' Select d.tax_record_id_new, ' || COLUMN_STR || ' from ' ||
                    TABLE_FCST_ARC_FCST_NAMES(I) || ' a, arc_tax_renumber_id d' ||
                    ' WHERE  tax_forecast_version_id = ' || FCST_VERSION || ' and ' ||
                    ' tax_record_id = tax_record_id_old ';
            if EXEC_SQL(SQLS, ERR_MSG) = -1 then
               STR := 'Inserting ' || TABLE_FCST_ARC_NAMES(I) || ' status. code = ' || ERR_MSG;
               goto QQUIT;
            end if;

            TRACE_WRITE(TRACE_LIST, 'delete ' || TABLE_FCST_ARC_NAMES(I), 1);
            SQLS := 'delete ' || TABLE_FCST_ARC_NAMES(I) || ' where tax_forecast_version_id = ' ||
                    FCST_VERSION;
            if EXEC_SQL(SQLS, ERR_MSG) = -1 then
               STR := 'deleting ' || TABLE_FCST_ARC_NAMES(I) || ' status. code = ' || ERR_MSG;
               goto QQUIT;
            end if;
         end loop;

         update TAX_FORECAST_VERSION set STATUS = 2 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'updating tax_forecast_version code = ' || sqlerrm(CODE) || '  fcst_version' ||
                   TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      close FCST_CUR;

      TRACE_WRITE(TRACE_LIST, 'End Del  Archive  Forecast', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      --    commit;
      --- The Last thing to do is remove the data from table
      /* Get Table Constraints */
      JOB_STATUS := 1;
      <<INCOMPLETE_START>>
   --     job_status := INCOMPLETE_ARCHIVE;
      TRACE_WRITE(TRACE_LIST, 'Update Version ', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      update VERSION set STATUS = JOB_STATUS where VERSION_ID = VERSION_NUMBER;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      TRACE_WRITE(TRACE_LIST, 'Update Forecast Version ', 1);
      update TAX_FORECAST_VERSION
         set STATUS = 0
       where VERSION_ID = VERSION_NUMBER
         and (STATUS = 1 or STATUS = 13);
      TRACE_WRITE(TRACE_LIST, 'Update Forecast2 Version ', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      JOB_STATUS := INCOMPLETE_ARCHIVE;
      update TAX_FORECAST_VERSION
         set STATUS = 11
       where VERSION_ID = VERSION_NUMBER
         and (STATUS = 19 or STATUS = 14);
      commit;
      TRACE_WRITE(TRACE_LIST, 'Start Getting Constraints ', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      RTN_STATUS := GET_CONSTRAINTS(TABLE_NAMES, TABLE_COUNT, CONS_LIST, OWNER_NAME, 'Tax');
      if CONS_LIST.COUNT > 0 then
         for I in 1 .. CONS_LIST.COUNT
         loop
            TRACE_WRITE(TRACE_LIST,
                        'Cons ' || CONS_LIST(I).NAME || ' ' || CONS_LIST(I).CONS || ' ' || CONS_LIST(I)
                        .REF_NAME || ' ' || CONS_LIST(I).REF_CONS || ' ' || CONS_LIST(I).CONS_TYPE || ' ' || CONS_LIST(I).COLS || ' ' || CONS_LIST(I)
                        .REF_COLS,
                        4);
         end loop;

      end if;
      TRACE_WRITE(TRACE_LIST, 'End Getting Constraints ', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      if RTN_STATUS <> 0 then
         ERR_MSG := 'Error getting Constraints ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      WARNINGS := WARNINGS + DROP_CONSTRAINTS(CONS_LIST, TRACE_LIST, OWNER_NAME);

      for I in 1 .. TABLE_NAMES.COUNT
      loop
         if LOWER(TABLE_NAMES(I)) = 'tax_transfer_control' then
            SQL2 := '  to_trid in (select tax_record_id from tax_record_control where ' ||
                    ' version_id = ' || TO_CHAR(VERSION_NUMBER) || ')';
         else
            SQL2 := '  tax_record_id in (select tax_record_id from tax_record_control where ' ||
                    ' version_id = ' || TO_CHAR(VERSION_NUMBER) || ')';
         end if;
         STR := 'delete from ' || OWNER_NAME || '.' || TABLE_NAMES(I) || ' where ' || SQL2;
         TRACE_WRITE(TRACE_LIST, STR, 1);
         if EXEC_SQL(STR, ERR_MSG) = -1 then
            -- data has copy so mark this as a warning
            STR := ERR_MSG;
            TRACE_WRITE(TRACE_LIST, STR, 3);

            update TAX_FORECAST_VERSION
               set STATUS = 2
             where VERSION_ID = VERSION_NUMBER
               and (STATUS = 1 or STATUS = 13);
            update TAX_FORECAST_VERSION
               set STATUS = 2
             where VERSION_ID = VERSION_NUMBER
               and (STATUS = 19 or STATUS = 14);
            update TAX_ARCHIVE_STATUS set DESCRIPTION = 'Finish' where VERSION_ID = VERSION_NUMBER;
            ERR_MSG := STR;
            goto QQUIT;
         end if;
      end loop;

      WARNINGS := WARNINGS + SET_CONSTRAINTS(CONS_LIST, TRACE_LIST, OWNER_NAME);

      STR := 'Update Version ';
      update VERSION set STATUS = 2 where VERSION_ID = VERSION_NUMBER;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Updating version table status. code = ' || sqlerrm(CODE);
         -- goto qquit;

      end if;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      update TAX_FORECAST_VERSION
         set STATUS = 2
       where VERSION_ID = VERSION_NUMBER
         and (STATUS = 1 or STATUS = 13);
      update TAX_FORECAST_VERSION
         set STATUS = 2
       where VERSION_ID = VERSION_NUMBER
         and (STATUS = 19 or STATUS = 14);
      update TAX_ARCHIVE_STATUS set DESCRIPTION = 'Finish' where VERSION_ID = VERSION_NUMBER;
      commit;
      TRACE_WRITE(TRACE_LIST, 'Finish', 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, 'Finished');
      commit;
      return;

      <<QQUIT>>

      CODE := sqlcode;
      rollback;
      update VERSION set STATUS = JOB_STATUS where VERSION_ID = VERSION_NUMBER;
      update TAX_FORECAST_VERSION
         set STATUS = 0
       where VERSION_ID = VERSION_NUMBER
         and (STATUS = 1 or STATUS = 13);
      update TAX_FORECAST_VERSION
         set STATUS = 11
       where VERSION_ID = VERSION_NUMBER
         and (STATUS = 19 or STATUS = 14);
      update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
      commit;
      TRACE_WRITE(TRACE_LIST, STR, 3);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, TO_CHAR(CODE));
      commit;

   exception
      when others then
         rollback;
         CODE := sqlcode;
         STR  := STR || sqlerrm(CODE);
         update VERSION set STATUS = JOB_STATUS where VERSION_ID = VERSION_NUMBER;
         update TAX_FORECAST_VERSION
            set STATUS = 0
          where VERSION_ID = VERSION_NUMBER
            and (STATUS = 1 or STATUS = 13);
         update TAX_FORECAST_VERSION
            set STATUS = 11
          where VERSION_ID = VERSION_NUMBER
            and (STATUS = 19 or STATUS = 14);
         update TAX_ARCHIVE_STATUS
            set DESCRIPTION = SUBSTR(STR, 1999)
          where VERSION_ID = VERSION_NUMBER;
         TRACE_WRITE(TRACE_LIST, STR, 3);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, TO_CHAR(CODE));
         commit;
   end ARCHIVE;


   --**************************************************************************
   --                               Restore
   --**************************************************************************
  procedure RESTORE(VERSION_NUMBER number) as

      STATUS                    long(4000);
      I                         int;
      J                         int;
      VERSION_KEY               int;
      DIFF                      int;
      DIFF2                     int;
      NEW_KEY                   int;
      STR                       varchar2(1024);
      SQL2                      varchar2(1024);
      CODE                      int;
      NUM                       int;
      NUM2                      int;
      SQLS                      varchar2(8096);
      ERR_MSG                   varchar2(8096);
      COLUMN_STR                varchar2(8096);
      COLUMN_STR2               varchar2(8096);
      OWNER_NAME                varchar2(20);
      FCST_VERSION              int;
      FCST_STATUS_CODE          int;
      VERSION_CODE              int;
      I_JOBNO                   int := 0;
      I_LOGID                   int := 0;
      LINE_NO                   int := 0;
      TRACE_LIST                TRACE_LIST_TYPE;
      TABLE_NAMES               TABLE_LIST_TYPE;
      TABLE_ARC_NAMES           TABLE_LIST_TYPE;
      TABLE_FCST_NAMES          TABLE_LIST_TYPE;
      TABLE_FCST_ARC_NAMES      TABLE_LIST_TYPE;
      TABLE_FCST_ARC_FCST_NAMES TABLE_LIST_TYPE;
      EX_COLUMN_LIST            TABLE_LIST_TYPE;

      cursor FCST_CUR is
         select TAX_FORECAST_VERSION_ID
           from TAX_FORECAST_VERSION F
          where VERSION_ID = VERSION_CODE
            and STATUS = FCST_STATUS_CODE;

   begin
      -- THIS USER EVENT CONTAINS THE SCRIPT TO
      -- RESTORE A TAX CASE
      OWNER_NAME   := 'PWRPLANT';
      VERSION_CODE := VERSION_NUMBER;

      START_JOB(VERSION_NUMBER, 0, 'Tax', I_JOBNO, I_LOGID);
      begin
         CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      exception
         when others then
            CODE := 0;
      end;
      CODE := GET_TABLE_LIST(TABLE_NAMES,
                             TABLE_ARC_NAMES,
                             TABLE_FCST_NAMES,
                             TABLE_FCST_ARC_NAMES,
                             TABLE_FCST_ARC_FCST_NAMES);
      /* check to see if we need to just complete this job */

      STR := 'Max Tax Record Control';
      select max(TAX_RECORD_ID) into NEW_KEY from TAX_RECORD_CONTROL;
      if sqlcode <> 0 then
         CODE := sqlcode;
         STR  := 'Error selecting from tax_record_control ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      TRACE_WRITE(TRACE_LIST, STR, 1);
      DIFF := NEW_KEY;
      STR  := 'Max Tax Transfer ID ';
      select max(TAX_TRANSFER_ID) into NEW_KEY from TAX_TRANSFER_CONTROL;
      if sqlcode <> 0 then
         CODE := sqlcode;
         STR  := 'Error selecting from tax_transfer_control ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      if NEW_KEY is null then
         NEW_KEY := 0;
      end if;
      TRACE_WRITE(TRACE_LIST, STR, 1);
      DIFF2 := NEW_KEY;
      -- Add appropriate records to tax_record_control
      -- test of new archive  procedures
      SQLS := 'truncate table arc_tax_renumber_tids';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      if EXEC_SQL(SQLS, ERR_MSG) = -1 then
         STR := ERR_MSG;
         goto QQUIT;
      end if;
      insert into ARC_TAX_RENUMBER_TIDS
         (TAX_TRANSFER_ID_OLD, TAX_TRANSFER_ID_NEW)
         (select TAX_TRANSFER_ID, ROWNUM + DIFF2
            from TAX_TRANSFER_CONTROL
           where VERSION_ID = VERSION_NUMBER);
      if sqlcode <> 0 then
         CODE := sqlcode;
         STR  := 'Error inserting into arc_tax_renumber_tids ' || sqlerrm(CODE);
         goto QQUIT;
      end if;

      STR := 'Update Version';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      update VERSION set STATUS = 3 where VERSION_ID = VERSION_NUMBER;
      if sqlcode <> 0 then
         --update  tax_archive_status  set description = 'Version ID not found in  version table ' where version_id = version_number;
         CODE := sqlcode;
         STR  := 'Version ID not found in  version table ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      commit;
      /* analyze  arc_tax_renumber_id; */
      STR := 'analyze arc_tax_renumber_id. ';
      begin
         CODE := ANALYZE_TABLE('arc_tax_renumber_id', -1);
      exception
         when others then
            STR := 'error analyze arc_tax_renumber_id. code=' || TO_CHAR(CODE) || ' ' ||
                   sqlerrm(CODE);
      end;
      TRACE_WRITE(TRACE_LIST, STR, 1);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      --      Tax Record Control
      STR := 'Insert into Tax Record Control';
      TRACE_WRITE(TRACE_LIST, STR, 1);

      for I in reverse 1 .. TABLE_NAMES.COUNT
      loop
         STR := 'Insert into Arc ' || TABLE_NAMES(I);
         TRACE_WRITE(TRACE_LIST, STR, 1);

         if LOWER(TABLE_NAMES(I)) <> 'tax_record_control' then
            CODE := CHECK_ARC_TABLE(TABLE_NAMES(I),
                                    TABLE_ARC_NAMES(I),
                                    ' add column(status number(22,0),version_id(22,0))');
         else
            CODE := CHECK_ARC_TABLE(TABLE_NAMES(I),
                                    TABLE_ARC_NAMES(I),
                                    ' add column(status number(22,0))');
         end if;
         if CODE <> 0 then
            STR := 'Error updating columns ' || TABLE_ARC_NAMES(I) || ' code =  ' || CODE;
            goto QQUIT;
         end if;
         if LOWER(TABLE_NAMES(I)) = 'tax_transfer_control' then
            EX_COLUMN_LIST(1) := 'to_trid';
            EX_COLUMN_LIST(2) := 'from_trid';
            EX_COLUMN_LIST(3) := 'tax_transfer_id';
         elsif LOWER(TABLE_NAMES(I)) = 'tax_depreciation_transfer' then
            EX_COLUMN_LIST(1) := 'tax_transfer_id';
         elsif LOWER(TABLE_NAMES(I)) = 'tax_book_reconcile_transfer' then
            EX_COLUMN_LIST(1) := 'tax_transfer_id';
         elsif LOWER(TABLE_NAMES(I)) = 'deferred_income_tax_transfer' then
            EX_COLUMN_LIST(1) := 'tax_transfer_id';
         end if;
         NUM  := GET_COLUMNS(TABLE_NAMES(I), '', OWNER_NAME, EX_COLUMN_LIST, COLUMN_STR, ERR_MSG);
         NUM2 := GET_COLUMNS(TABLE_ARC_NAMES(I),
                             'a',
                             OWNER_NAME,
                             EX_COLUMN_LIST,
                             COLUMN_STR2,
                             ERR_MSG);

         EX_COLUMN_LIST.DELETE;
         if NUM <> NUM2 then
            STR := 'Table Columns are not equal ' || TABLE_ARC_NAMES(I) || ' = ' || NUM2 || ' std=' || NUM;
            goto QQUIT;
         end if;
         if LOWER(TABLE_NAMES(I)) = 'tax_transfer_control' then
            SQLS := 'insert into ' || TABLE_NAMES(I) || '(to_trid,from_trid,tax_transfer_id,' ||
                    COLUMN_STR || ')' || ' Select   a.to_trid + ' || DIFF || ', a.from_trid + ' || DIFF ||
                    ',b.tax_transfer_id_new ' || ',' || COLUMN_STR2 || ' FROM ' ||
                    TABLE_ARC_NAMES(I) || ' a , arc_tax_renumber_tids b' || ' WHERE version_id = ' ||
                    VERSION_NUMBER || ' and a.tax_transfer_id = b.tax_transfer_id_old ';
         elsif TABLE_NAMES(I) = 'TAX_RECORD_CONTROL' then
            SQLS := 'insert into ' || TABLE_NAMES(I) || '(version_id,tax_record_id,' || COLUMN_STR || ')' ||
                    ' Select a.version_id ' || ', a.tax_record_id + ' || DIFF || ',' || COLUMN_STR2 ||
                    ' FROM ' || TABLE_ARC_NAMES(I) || ' a ' || ' WHERE  version_id =' ||
                    VERSION_NUMBER;
         elsif LOWER(TABLE_NAMES(I)) = 'tax_depreciation_transfer' then
            SQLS := 'insert into ' || TABLE_NAMES(I) || '(tax_record_id,tax_transfer_id,' ||
                    COLUMN_STR || ')' || ' Select  ' || ' a.tax_record_id + ' || DIFF ||
                    ', b.tax_transfer_id_new,' || COLUMN_STR2 || ' FROM ' || TABLE_ARC_NAMES(I) ||
                    ' a, arc_tax_renumber_tids b' || ' WHERE a.version_id = ' || VERSION_NUMBER ||
                    ' and a.tax_transfer_id = b.tax_transfer_id_old';
         elsif LOWER(TABLE_NAMES(I)) = 'tax_book_reconcile_transfer' then
            SQLS := 'insert into ' || TABLE_NAMES(I) || '(tax_record_id,tax_transfer_id,' ||
                    COLUMN_STR || ')' || ' Select  ' || ' a.tax_record_id + ' || DIFF ||
                    ', b.tax_transfer_id_new,' || COLUMN_STR2 || ' FROM ' || TABLE_ARC_NAMES(I) ||
                    ' a, arc_tax_renumber_tids b' || ' WHERE a.version_id = ' || VERSION_NUMBER ||
                    ' and a.tax_transfer_id = b.tax_transfer_id_old';
         elsif LOWER(TABLE_NAMES(I)) = 'deferred_income_tax_transfer' then
            SQLS := 'insert into ' || TABLE_NAMES(I) || '(tax_record_id,tax_transfer_id,' ||
                    COLUMN_STR || ')' || ' Select  ' || ' a.tax_record_id + ' || DIFF ||
                    ', b.tax_transfer_id_new,' || COLUMN_STR2 || ' FROM ' || TABLE_ARC_NAMES(I) ||
                    ' a, arc_tax_renumber_tids b' || ' WHERE a.version_id = ' || VERSION_NUMBER ||
                    ' and a.tax_transfer_id = b.tax_transfer_id_old';
         else
            SQLS := 'insert into ' || TABLE_NAMES(I) || '(tax_record_id,' || COLUMN_STR || ')' ||
                    ' Select  ' || ' a.tax_record_id + ' || DIFF || ',' || COLUMN_STR2 || ' FROM ' ||
                    TABLE_ARC_NAMES(I) || ' a ' || ' WHERE  version_id =' || VERSION_NUMBER;
         end if;

         if EXEC_SQL(SQLS, ERR_MSG) = -1 then
            STR := 'Inserting ' || TABLE_NAMES(I) || '  table status. code = ' || SQLS || ERR_MSG;
            goto QQUIT;
         end if;
         SQLS := 'delete ' || TABLE_ARC_NAMES(I) || ' where version_id  =' || VERSION_NUMBER;
         if EXEC_SQL(SQLS, ERR_MSG) = -1 then
            STR := 'Deleting ' || TABLE_ARC_NAMES(I) || '  table status. code =   ' || ERR_MSG;
            goto QQUIT;
         end if;
      end loop;

      if CHECK_RECORD_COUNT(VERSION_NUMBER, 'Restore', I_LOGID) = -1 then
         CODE := sqlcode;
         STR  := 'Archive and Tax Record are not equal code = ' || sqlerrm(CODE);
         goto QQUIT;
      end if;

      -- Restore Tax Archived Forecast versions
      FCST_STATUS_CODE := 3; -- Archive Tax Archived Forecast
      open FCST_CUR;
      loop
         fetch FCST_CUR
            into FCST_VERSION;
         if (FCST_CUR%notfound) then
            exit;
         end if;
         TRACE_WRITE(TRACE_LIST, 'insert into tax_forecast_output', 1);
         for I in 1 .. TABLE_FCST_NAMES.COUNT
         loop

            NUM  := GET_COLUMNS(TABLE_FCST_ARC_NAMES(I),
                                '',
                                OWNER_NAME,
                                EX_COLUMN_LIST,
                                COLUMN_STR,
                                ERR_MSG);
            NUM2 := GET_COLUMNS(TABLE_FCST_NAMES(I),
                                '',
                                OWNER_NAME,
                                EX_COLUMN_LIST,
                                COLUMN_STR2,
                                ERR_MSG);
            if NUM <> NUM2 then
               STR := 'Table Columns are not equal ' || TABLE_FCST_ARC_NAMES(I) || ' arc = ' || NUM ||
                      ' std=' || NUM2;
               goto QQUIT;
            end if;

            SQLS := 'insert into ' || TABLE_FCST_NAMES(I) || '(tax_record_id,' || COLUMN_STR || ')' ||
                    ' Select  tax_record_id + ' || DIFF || ',' || COLUMN_STR2 || ' FROM ' ||
                    TABLE_FCST_ARC_NAMES(I) || ' WHERE  tax_forecast_version_id  =' || FCST_VERSION;
            if EXEC_SQL(SQLS, ERR_MSG) = -1 then
               STR := 'Inserting tax_forecast_output table status. code = ' || ERR_MSG;
               goto QQUIT;
            end if;
            TRACE_WRITE(TRACE_LIST, 'delete ' || TABLE_FCST_ARC_NAMES(I), 1);
            SQLS := 'delete ' || TABLE_FCST_ARC_NAMES(I) || ' where tax_forecast_version_id = ' ||
                    FCST_VERSION;
            if EXEC_SQL(SQLS, ERR_MSG) = -1 then
               STR := 'deleting arc_tax_forecast_output  code = ' || ERR_MSG || '  fcst_version' ||
                      TO_CHAR(FCST_VERSION);
               goto QQUIT;
            end if;
         end loop;

         update TAX_FORECAST_VERSION set STATUS = 0 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'updating tax_forecast_version code = ' || sqlerrm(CODE) || '  fcst_version' ||
                   TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      STR := 'Last Update Version ';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      update VERSION set STATUS = 0 where VERSION_ID = VERSION_NUMBER;
      update TAX_FORECAST_VERSION set STATUS = 0 where VERSION_ID = VERSION_NUMBER;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, 'Finished');
      commit;
      return;

      <<QQUIT>>

      rollback;
      CODE := sqlcode;
      update VERSION set STATUS = 7 where VERSION_ID = VERSION_NUMBER;
      update TAX_FORECAST_VERSION set STATUS = 2 where VERSION_ID = VERSION_NUMBER;
      update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
      TRACE_WRITE(TRACE_LIST, STR, 3);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, TO_CHAR(CODE));
      commit;

   exception
      when others then
         CODE := sqlcode;
         STR  := STR || sqlerrm(CODE);
         rollback;
         update VERSION set STATUS = 7 where VERSION_ID = VERSION_NUMBER;
         update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
         update TAX_FORECAST_VERSION set STATUS = 2 where VERSION_ID = VERSION_NUMBER;
         TRACE_WRITE(TRACE_LIST, STR, 3);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, TO_CHAR(CODE));
         commit;
   end RESTORE;


   --**************************************************************************
   --                             Delete Archive
   --**************************************************************************


   procedure DELETE_ARCHIVE(VERSION_NUMBER number) is

      I                         int;
      J                         int;
      VERSION_KEY               int;
      DIFF                      int;
      NEW_KEY                   int;
      STR                       varchar2(1024);
      SQL2                      varchar2(1024);
      CODE                      int;
      FCST_STATUS_CODE          int;
      FCST_VERSION              int;
      VERSION_CODE              int;
      I_JOBNO                   int := 0;
      I_LOGID                   int := 0;
      LINE_NO                   int := 0;
      ERR_MSG                   varchar2(8000);
      SQLS                      varchar2(8096);
      TABLE_NAMES               TABLE_LIST_TYPE;
      TABLE_ARC_NAMES           TABLE_LIST_TYPE;
      TABLE_FCST_NAMES          TABLE_LIST_TYPE;
      TABLE_FCST_ARC_NAMES      TABLE_LIST_TYPE;
      TABLE_FCST_ARC_FCST_NAMES TABLE_LIST_TYPE;
      TRACE_LIST                TRACE_LIST_TYPE;

      cursor FCST_CUR is
         select TAX_FORECAST_VERSION_ID
           from TAX_FORECAST_VERSION F
          where VERSION_ID = VERSION_CODE
            and STATUS = FCST_STATUS_CODE;

   begin
      -- RESTORE A TAX CASE
      VERSION_CODE := VERSION_NUMBER;
      begin
         CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      exception
         when others then
            CODE := 0;
      end;
      START_JOB(VERSION_NUMBER, 0, 'Tax', I_JOBNO, I_LOGID);

      STR := 'Max Tax Record Control ';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      select max(TAX_RECORD_ID) into NEW_KEY from TAX_RECORD_CONTROL;
      if sqlcode <> 0 then
         CODE := sqlcode;
         STR  := 'Error selecting from tax_archive_status ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      DIFF := NEW_KEY;
      TRACE_WRITE(TRACE_LIST, 'New key ' || TO_CHAR(DIFF), 1);
      -- Add appropriate records to tax_record_control
      -- test of new archive  procedures
      STR := 'Update Version ';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      update VERSION set STATUS = 5 where VERSION_ID = VERSION_NUMBER;
      if sqlcode <> 0 then
         CODE := sqlcode;
         STR  := 'Version ID not found in  version table ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      commit;

      CODE := GET_TABLE_LIST(TABLE_NAMES,
                             TABLE_ARC_NAMES,
                             TABLE_FCST_NAMES,
                             TABLE_FCST_ARC_NAMES,
                             TABLE_FCST_ARC_FCST_NAMES);
      -- Delete Archive Forecast versions
      FCST_STATUS_CODE := 5; -- Deleting Forecast Archive
      open FCST_CUR;
      loop
         fetch FCST_CUR
            into FCST_VERSION;
         if (FCST_CUR%notfound) then
            exit;
         end if;
         for I in 1 .. TABLE_FCST_NAMES.COUNT
         loop
            TRACE_WRITE(TRACE_LIST, 'delete arc_tax_forecast_output', 1);
            SQLS := 'delete ' || TABLE_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                    FCST_VERSION;
            if EXEC_SQL(SQLS, ERR_MSG) = -1 then
               STR := 'deleting ' || TABLE_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                      '  fcst_version' || TO_CHAR(FCST_VERSION);
               goto QQUIT;
            end if;
            TRACE_WRITE(TRACE_LIST, 'delete ' || TABLE_FCST_NAMES(I), 1);
         end loop;

      end loop;

      close FCST_CUR;
      TRACE_WRITE(TRACE_LIST, 'delete tax_year_version', 1);
      delete TAX_YEAR_VERSION where VERSION_ID = VERSION_NUMBER;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Deleting tax_year_version table status. code = ' || sqlerrm(CODE);
         TRACE_WRITE(TRACE_LIST, STR, 2);
      end if;
      TRACE_WRITE(TRACE_LIST, 'delete version', 1);
      delete VERSION where VERSION_ID = VERSION_NUMBER;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Deleting version table status. code = ' || sqlerrm(CODE);
         TRACE_WRITE(TRACE_LIST, STR, 2);
      end if;
      commit;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, 'Finsihed');
      return;

      <<QQUIT>>

      CODE := sqlcode;
      rollback;
      update VERSION set STATUS = 9 where VERSION_ID = VERSION_NUMBER;
      update TAX_FORECAST_VERSION set STATUS = 2 where VERSION_ID = VERSION_NUMBER;
      update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
      TRACE_WRITE(TRACE_LIST, STR, 3);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, TO_CHAR(CODE));
      commit;

   exception
      when others then
         CODE := sqlcode;
         STR  := STR || sqlerrm(CODE);
         DBMS_OUTPUT.PUT_LINE(STR);
         rollback;
         update VERSION set STATUS = 9 where VERSION_ID = VERSION_NUMBER;
         update TAX_FORECAST_VERSION set STATUS = 2 where VERSION_ID = VERSION_NUMBER;
         update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
         TRACE_WRITE(TRACE_LIST, STR, 3);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, TO_CHAR(CODE));
         commit;
   end DELETE_ARCHIVE;


   --**************************************************************************
   --                              Delete Case
   --**************************************************************************
   procedure DELETE_CASE(VERSION_NUMBER number,
                         OWNER_NAME     in varchar2,
                         TABLE2         in varchar2) as

      STATUS                    long(4000);
      J                         int;
      I                         int;
      DIFF                      int;
      STR                       varchar2(1024);
      SQL2                      varchar2(1024);
      VERSION_KEY               number(22, 2);
      CODE                      int;
      I_JOBNO                   int := 0;
      I_LOGID                   int := 0;
      LINE_NO                   int := 0;
      ERR_MSG                   varchar2(8000);
      SQLS                      varchar2(8096);
      TABLE_NAMES               TABLE_LIST_TYPE;
      TABLE_ARC_NAMES           TABLE_LIST_TYPE;
      TABLE_FCST_NAMES          TABLE_LIST_TYPE;
      TABLE_FCST_ARC_NAMES      TABLE_LIST_TYPE;
      TABLE_FCST_ARC_FCST_NAMES TABLE_LIST_TYPE;
      TRACE_LIST                TRACE_LIST_TYPE;
      FCST_STATUS_CODE          int;
      FCST_VERSION              int;
      VERSION_CODE              int;

      cursor FCST_CUR is
         select TAX_FORECAST_VERSION_ID
           from TAX_FORECAST_VERSION F
          where VERSION_ID = VERSION_CODE
            and STATUS = FCST_STATUS_CODE;

   begin
      VERSION_CODE := VERSION_NUMBER;

      START_JOB(VERSION_NUMBER, 0, 'Tax', I_JOBNO, I_LOGID);
      begin
         CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      exception
         when others then
            CODE := 0;
      end;
      STR := 'Tax Archive Status ';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      update TAX_ARCHIVE_STATUS
         set DESCRIPTION = 'Deleting Case'
       where VERSION_ID = VERSION_NUMBER;
      commit;
      /* add appropriate records to tax_record_control test of new archive procedures */
      STR := 'Update Version ';
      DBMS_OUTPUT.PUT_LINE(STR);
      update VERSION set STATUS = 4 where VERSION_ID = VERSION_NUMBER;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Updating version table status. code = ' || sqlerrm(CODE);
         goto QQUIT;
      end if;
      /* tax reconcile */
      commit;

      CODE := GET_TABLE_LIST(TABLE_NAMES,
                             TABLE_ARC_NAMES,
                             TABLE_FCST_NAMES,
                             TABLE_FCST_ARC_NAMES,
                             TABLE_FCST_ARC_FCST_NAMES);

      for I in 1 .. TABLE_NAMES.COUNT
      loop
         if UPPER(TABLE_NAMES(I)) = 'TAX_TRANSFER_CONTROL' then
            SQL2 := '  to_trid in (select tax_record_id from tax_record_control where ' ||
                    ' version_id = ' || TO_CHAR(VERSION_NUMBER) || ')';
         elsif UPPER(TABLE_NAMES(I)) = 'TAX_RECORD_CONTROL' then
            SQL2 := ' version_id = ' || TO_CHAR(VERSION_NUMBER);

         else
            SQL2 := '  tax_record_id in (select tax_record_id from tax_record_control where ' ||
                    ' version_id = ' || TO_CHAR(VERSION_NUMBER) || ')';
         end if;
         CODE := ARCHIVE_TAX.DELETE_TABLE(TABLE_NAMES(I), OWNER_NAME, SQL2, ERR_MSG);
         if CODE <> 0 then
            STR := 'deleting ' || TABLE_NAMES(I) || ' code = ' || sqlerrm(CODE) || ' ' || ERR_MSG;
            goto QQUIT;
         end if;
      end loop;

      -- Delete Forecast versions

      FCST_STATUS_CODE := 4; -- Deleting Forecast Archive
      open FCST_CUR;
      loop
         fetch FCST_CUR
            into FCST_VERSION;
         if (FCST_CUR%notfound) then
            exit;
         end if;

         for I in 1 .. TABLE_FCST_NAMES.COUNT
         loop
            SQLS := 'delete ' || TABLE_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                    FCST_VERSION;
            if EXEC_SQL(SQLS, ERR_MSG) = -1 then
               STR := 'deleting ' || TABLE_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                      '  fcst_version' || TO_CHAR(FCST_VERSION);
               goto QQUIT;
            end if;
            TRACE_WRITE(TRACE_LIST, 'delete ' || TABLE_FCST_NAMES(I), 1);
         end loop;

         TRACE_WRITE(TRACE_LIST, 'delete tax_forecast_version ', 1);

         delete TAX_FORECAST_VERSION where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'deleting tax_forecast_version  code = ' || sqlerrm(CODE) || '  fcst_version' ||
                   TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      close FCST_CUR;

      -- Delete Archived Forecast versions
      FCST_STATUS_CODE := 11; -- Deleting Forecast Archive
      open FCST_CUR;
      loop
         fetch FCST_CUR
            into FCST_VERSION;
         if (FCST_CUR%notfound) then
            exit;
         end if;
         for I in 1 .. TABLE_FCST_ARC_FCST_NAMES.COUNT
         loop
            SQLS := 'delete ' || TABLE_FCST_ARC_FCST_NAMES(I) ||
                    ' where tax_forecast_version_id = ' || FCST_VERSION;
            if EXEC_SQL(SQLS, ERR_MSG) = -1 then
               STR := 'deleting ' || TABLE_FCST_ARC_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                      '  fcst_version' || TO_CHAR(FCST_VERSION);
               goto QQUIT;
            end if;
            TRACE_WRITE(TRACE_LIST, 'delete ' || TABLE_FCST_NAMES(I), 1);
         end loop;

         TRACE_WRITE(TRACE_LIST, 'delete tax_forecast_version ', 1);

         delete TAX_FORECAST_VERSION where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         CODE := sqlcode;
         if CODE <> 0 then
            STR := 'deleting arc_fcst_tax_forecast_version  code = ' || sqlerrm(CODE) ||
                   '  fcst_version' || TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      close FCST_CUR;

      TRACE_WRITE(TRACE_LIST, 'delete  tax_year_version', 1);
      delete TAX_YEAR_VERSION where VERSION_ID = VERSION_NUMBER;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Deleting tax_year_version table status. code = ' || sqlerrm(CODE);
         TRACE_WRITE(TRACE_LIST, 'delete tax_forecast_output', 2);
      end if;
      TRACE_WRITE(TRACE_LIST, 'delete  version', 1);
      delete VERSION where VERSION_ID = VERSION_NUMBER;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Deleting version table status. code = ' || sqlerrm(CODE);
         TRACE_WRITE(TRACE_LIST, 'delete tax_forecast_output', 2);
      end if;

      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, 'Finished');
      commit;
      return;

      <<QQUIT>>

      CODE := sqlcode;
      rollback;
      update VERSION set STATUS = 8 where VERSION_ID = VERSION_NUMBER;
      update TAX_FORECAST_VERSION
         set STATUS = 0
       where VERSION_ID = VERSION_NUMBER
         and STATUS = 4;
      update TAX_FORECAST_VERSION
         set STATUS = 11
       where VERSION_ID = VERSION_NUMBER
         and STATUS = 14;
      update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
      TRACE_WRITE(TRACE_LIST, STR, 3);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, TO_CHAR(CODE));
      commit;

   exception
      when others then
         CODE := sqlcode;
         STR  := STR || sqlerrm(CODE);
         rollback;
         update VERSION set STATUS = 8 where VERSION_ID = VERSION_NUMBER;
         update TAX_FORECAST_VERSION
            set STATUS = 0
          where VERSION_ID = VERSION_NUMBER
            and STATUS = 4;
         update TAX_FORECAST_VERSION
            set STATUS = 11
          where VERSION_ID = VERSION_NUMBER
            and STATUS = 14;
         update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
         TRACE_WRITE(TRACE_LIST, STR, 3);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         STOP_JOB(VERSION_NUMBER, 'Tax', I_JOBNO, I_LOGID, TO_CHAR(CODE));
         commit;
   end DELETE_CASE;


   --**************************************************************************
   --                           Archive Forcast
   --**************************************************************************
   procedure ARCHIVE_FCST(FCST_VERSION   number,
                          VERSION_NUMBER number) is
      STATUS                    long(4000);
      J                         int;
      I                         int;
      DIFF                      int;
      STR                       varchar2(1024);
      SQL2                      varchar2(1024);
      VERSION_KEY               number(22, 2);
      CODE                      int;
      ERR_MSG                   varchar2(1024);
      SQLS                      varchar(8048);
      TABLE_NAMES               TABLE_LIST_TYPE;
      TABLE_ARC_NAMES           TABLE_LIST_TYPE;
      TABLE_FCST_NAMES          TABLE_LIST_TYPE;
      TABLE_FCST_ARC_NAMES      TABLE_LIST_TYPE;
      TABLE_FCST_ARC_FCST_NAMES TABLE_LIST_TYPE;
      I_JOBNO                   int := 0;
      I_LOGID                   int := 0;
      LINE_NO                   int := 0;
      TRACE_LIST                TRACE_LIST_TYPE;

   begin

      CODE := GET_TABLE_LIST(TABLE_NAMES,
                             TABLE_ARC_NAMES,
                             TABLE_FCST_NAMES,
                             TABLE_FCST_ARC_NAMES,
                             TABLE_FCST_ARC_FCST_NAMES);

      begin
         CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      exception
         when others then
            CODE := 0;
      end;
      START_JOB(VERSION_NUMBER, FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID);

      for I in 1 .. TABLE_FCST_NAMES.COUNT
      loop
         SQLS := 'insert into ' || TABLE_FCST_ARC_FCST_NAMES(I) || '  select * from ' ||
                 TABLE_FCST_NAMES(I) || ' where tax_forecast_version_id = ' || FCST_VERSION;
         if EXEC_SQL(SQLS, ERR_MSG) = -1 then
            STR := 'inserting ' || TABLE_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                   '  fcst_version' || TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
         SQLS := 'delete ' || TABLE_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                 FCST_VERSION;
         if EXEC_SQL(SQLS, ERR_MSG) = -1 then
            STR := 'deleting ' || TABLE_FCST_NAMES(I) || '  code = ' || ERR_MSG || '  fcst_version' ||
                   TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      TRACE_WRITE(TRACE_LIST, 'update tax_forecast_version', 1);
      update TAX_FORECAST_VERSION set STATUS = 11 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
      CODE := sqlcode;
      if CODE <> 0 then
         STR := 'updating tax_forecast_version code = ' || sqlerrm(CODE) || '  fcst_version' ||
                TO_CHAR(FCST_VERSION);
         TRACE_WRITE(TRACE_LIST, STR, 2);
      end if;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, 'Finish');
      commit;
      return;

      <<QQUIT>>

      rollback;
      CODE := sqlcode;
      update TAX_FORECAST_VERSION set STATUS = 15 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
      update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
      TRACE_WRITE(TRACE_LIST, STR, 3);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, TO_CHAR(CODE));
      commit;

   exception
      when others then
         rollback;
         CODE := sqlcode;
         STR  := STR || sqlerrm(CODE);
         TRACE_WRITE(TRACE_LIST, STR, 3);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         update TAX_FORECAST_VERSION set STATUS = 15 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
         STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, TO_CHAR(CODE));
         commit;
   end ARCHIVE_FCST;


   --**************************************************************************
   --                             Restore FCST
   --**************************************************************************
   procedure RESTORE_FCST(FCST_VERSION   number,
                          VERSION_NUMBER number) as

      STATUS                    long(4000);
      I                         int;
      J                         int;
      VERSION_KEY               int;
      DIFF                      int;
      ERR_MSG                   varchar2(1024);
      SQLS                      varchar(8048);
      TABLE_NAMES               TABLE_LIST_TYPE;
      TABLE_ARC_NAMES           TABLE_LIST_TYPE;
      TABLE_FCST_NAMES          TABLE_LIST_TYPE;
      TABLE_FCST_ARC_NAMES      TABLE_LIST_TYPE;
      TABLE_FCST_ARC_FCST_NAMES TABLE_LIST_TYPE;
      NEW_KEY                   int;
      STR                       varchar2(1024);
      SQL2                      varchar2(1024);
      CODE                      int;
      I_JOBNO                   int := 0;
      I_LOGID                   int := 0;
      LINE_NO                   int := 0;
      TRACE_LIST                TRACE_LIST_TYPE;

   begin
      -- THIS USER EVENT CONTAINS THE SCRIPT TO
      -- RESTORE A TAX CASE
      START_JOB(VERSION_NUMBER, FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID);

      STR := 'Max Tax Record Control';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      begin
         CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      exception
         when others then
            CODE := 0;
      end;

      CODE := GET_TABLE_LIST(TABLE_NAMES,
                             TABLE_ARC_NAMES,
                             TABLE_FCST_NAMES,
                             TABLE_FCST_ARC_NAMES,
                             TABLE_FCST_ARC_FCST_NAMES);

      for I in 1 .. TABLE_FCST_NAMES.COUNT
      loop
         SQLS := 'insert into ' || TABLE_FCST_NAMES(I) || '  select * from ' ||
                 TABLE_FCST_ARC_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                 FCST_VERSION;
         if EXEC_SQL(SQLS, ERR_MSG) = -1 then
            STR := 'inserting ' || TABLE_FCST_ARC_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                   '  fcst_version' || TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
         SQLS := 'delete ' || TABLE_FCST_ARC_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                 FCST_VERSION;
         if EXEC_SQL(SQLS, ERR_MSG) = -1 then
            STR := 'deleting ' || TABLE_FCST_ARC_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                   '  fcst_version' || TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      TRACE_WRITE(TRACE_LIST, 'update tax_forecast_version ', 1);

      update TAX_FORECAST_VERSION set STATUS = 0 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
      CODE := sqlcode;
      if CODE <> 0 then
         STR := 'updating tax_forecast_version code = ' || sqlerrm(CODE) || '  fcst_version' ||
                TO_CHAR(FCST_VERSION);
         TRACE_WRITE(TRACE_LIST, STR, 2);
      end if;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, 'Finished');
      commit;
      return;

      <<QQUIT>>

      CODE := sqlcode;
      rollback;
      update TAX_FORECAST_VERSION set STATUS = 16 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
      update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
      TRACE_WRITE(TRACE_LIST, STR, 3);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, TO_CHAR(CODE));
      commit;

   exception
      when others then
         CODE := sqlcode;
         STR  := STR || sqlerrm(CODE);
         DBMS_OUTPUT.PUT_LINE(STR);
         rollback;
         update TAX_FORECAST_VERSION set STATUS = 16 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
         TRACE_WRITE(TRACE_LIST, STR, 3);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, TO_CHAR(CODE));
         commit;
   end RESTORE_FCST;


   --**************************************************************************
   --                           Delete Fcst Archive
   --**************************************************************************
   procedure DELETE_FCST_ARCHIVE(FCST_VERSION   number,
                                 VERSION_NUMBER number) is

      I                         int;
      J                         int;
      VERSION_KEY               int;
      DIFF                      int;
      NEW_KEY                   int;
      STR                       varchar2(1024);
      SQL2                      varchar2(1024);
      CODE                      int;
      I_JOBNO                   int := 0;
      I_LOGID                   int := 0;
      LINE_NO                   int := 0;
      ERR_MSG                   varchar2(1024);
      SQLS                      varchar(8048);
      TABLE_NAMES               TABLE_LIST_TYPE;
      TABLE_ARC_NAMES           TABLE_LIST_TYPE;
      TABLE_FCST_NAMES          TABLE_LIST_TYPE;
      TABLE_FCST_ARC_NAMES      TABLE_LIST_TYPE;
      TABLE_FCST_ARC_FCST_NAMES TABLE_LIST_TYPE;
      TRACE_LIST                TRACE_LIST_TYPE;

   begin
      STR := 'Tax Fcst Archive Status ';
      START_JOB(VERSION_NUMBER, FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID);
      begin
         CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      exception
         when others then
            CODE := 0;
      end;
      CODE := GET_TABLE_LIST(TABLE_NAMES,
                             TABLE_ARC_NAMES,
                             TABLE_FCST_NAMES,
                             TABLE_FCST_ARC_NAMES,
                             TABLE_FCST_ARC_FCST_NAMES);

      TRACE_WRITE(TRACE_LIST, STR, 1);
      update TAX_ARCHIVE_STATUS set DESCRIPTION = 'Deleting Case' where VERSION_ID = FCST_VERSION;
      commit;

      /* add appropriate records to tax_record_control test of new archive procedures */
      for I in 1 .. TABLE_FCST_NAMES.COUNT
      loop
         SQLS := 'delete ' || TABLE_FCST_ARC_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                 FCST_VERSION;
         if EXEC_SQL(SQLS, ERR_MSG) = -1 then
            STR := 'deleting ' || TABLE_FCST_ARC_FCST_NAMES(I) || '  code = ' || ERR_MSG ||
                   '  fcst_version' || TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      TRACE_WRITE(TRACE_LIST, 'delete tax_forecast_version', 1);

      delete TAX_FORECAST_VERSION where TAX_FORECAST_VERSION_ID = FCST_VERSION;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Deleting tax forecast version table status. code = ' || sqlerrm(CODE);
         TRACE_WRITE(TRACE_LIST, STR, 2);
      end if;
      commit;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, 'Finished');
      commit;
      return;

      <<QQUIT>>

      CODE := sqlcode;
      rollback;
      update TAX_FORECAST_VERSION set STATUS = 18 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
      update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
      TRACE_WRITE(TRACE_LIST, STR, 3);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, TO_CHAR(CODE));
      commit;

   exception
      when others then
         CODE := sqlcode;
         STR  := STR || sqlerrm(CODE);
         rollback;
         update TAX_FORECAST_VERSION set STATUS = 18 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
         TRACE_WRITE(TRACE_LIST, STR, 3);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, TO_CHAR(CODE));
         commit;
   end DELETE_FCST_ARCHIVE;


   --**************************************************************************
   --                          Delete Forecast Case
   --**************************************************************************
   procedure DELETE_FCST_CASE(FCST_VERSION   number,
                              VERSION_NUMBER number) as

      STATUS                    long(4000);
      J                         int;
      I                         int;
      DIFF                      int;
      STR                       varchar2(1024);
      SQL2                      varchar2(1024);
      VERSION_KEY               number(22, 2);
      SQLS                      varchar(8048);
      TABLE_NAMES               TABLE_LIST_TYPE;
      TABLE_ARC_NAMES           TABLE_LIST_TYPE;
      TABLE_FCST_NAMES          TABLE_LIST_TYPE;
      TABLE_FCST_ARC_NAMES      TABLE_LIST_TYPE;
      TABLE_FCST_ARC_FCST_NAMES TABLE_LIST_TYPE;
      CODE                      int;
      ERR_MSG                   varchar2(1024);
      I_JOBNO                   int := 0;
      I_LOGID                   int := 0;
      LINE_NO                   int := 0;
      TRACE_LIST                TRACE_LIST_TYPE;

   begin
      START_JOB(VERSION_NUMBER, FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID);

      begin
         CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      exception
         when others then
            CODE := 0;
      end;
      STR := 'Tax Fcst Archive Status ';
      TRACE_WRITE(TRACE_LIST, STR, 1);
      update TAX_ARCHIVE_STATUS
         set DESCRIPTION = 'Deleting Case'
       where VERSION_ID = VERSION_NUMBER;
      commit;

      /* remove records from forecast tables */
      CODE := GET_TABLE_LIST(TABLE_NAMES,
                             TABLE_ARC_NAMES,
                             TABLE_FCST_NAMES,
                             TABLE_FCST_ARC_NAMES,
                             TABLE_FCST_ARC_FCST_NAMES);

      for I in 1 .. TABLE_FCST_NAMES.COUNT
      loop
         TRACE_WRITE(TRACE_LIST, 'delete ' || TABLE_FCST_NAMES(I), 1);
         SQLS := ' delete ' || TABLE_FCST_NAMES(I) || ' where tax_forecast_version_id = ' ||
                 FCST_VERSION;
         if EXEC_SQL(SQLS, ERR_MSG) = -1 then
            STR := 'deleting ' || TABLE_FCST_NAMES(I) || '  code = ' || ERR_MSG || '  fcst_version' ||
                   TO_CHAR(FCST_VERSION);
            goto QQUIT;
         end if;
      end loop;

      TRACE_WRITE(TRACE_LIST, 'delete tax_forecast_version', 1);

      delete TAX_FORECAST_VERSION where TAX_FORECAST_VERSION_ID = FCST_VERSION;
      if (sqlcode <> 0) then
         CODE := sqlcode;
         STR  := 'Deleting tax forecast version table status. code = ' || sqlerrm(CODE);
         TRACE_WRITE(TRACE_LIST, STR, 2);
      end if;
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, 'Finished');
      commit;
      return;

      <<QQUIT>>

      rollback;
      update TAX_FORECAST_VERSION set STATUS = 17 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
      update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
      TRACE_WRITE(TRACE_LIST, STR, 3);
      TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
      STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, TO_CHAR(CODE));
      commit;

   exception
      when others then
         CODE := sqlcode;
         STR  := STR || sqlerrm(CODE);
         rollback;
         update TAX_FORECAST_VERSION set STATUS = 17 where TAX_FORECAST_VERSION_ID = FCST_VERSION;
         update TAX_ARCHIVE_STATUS set DESCRIPTION = STR where VERSION_ID = VERSION_NUMBER;
         TRACE_WRITE(TRACE_LIST, STR, 3);
         TRACE_SAVE(TRACE_LIST, I_JOBNO, LINE_NO);
         STOP_JOB(FCST_VERSION, 'Fcst', I_JOBNO, I_LOGID, TO_CHAR(CODE));
         commit;
   end DELETE_FCST_CASE;


   --**************************************************************************
   --                            Delete Table
   --**************************************************************************
   function DELETE_TABLE(TABLE_NAME_STR varchar,
                         OWNER_NAME     varchar,
                         WHERE_CLAUSE   varchar,
                         ERR_MSG        out varchar) return integer is

      REF_TABLES_CONS varchar(2048);
      TABLE_NAME_CONS varchar(2048);

      cursor TAB_CUR is
         select LOWER(C.CONSTRAINT_NAME), LOWER(C2.CONSTRAINT_NAME), C2.TABLE_NAME
           from ALL_CONSTRAINTS C, ALL_CONSTRAINTS C2
          where C.CONSTRAINT_NAME = C2.R_CONSTRAINT_NAME
            and C.OWNER = OWNER_NAME
            and C.TABLE_NAME = UPPER(TABLE_NAME_STR)
            and C.CONSTRAINT_TYPE = 'P';

      COUNT2       int;
      COL_COUNT    int;
      I            int;
      J            int;
      NUM          int;
      REF_TABLE    varchar(2048);
      CODE         int;
      CONS_NAME    varchar(2048);
      TO_CONS_NAME varchar(2048);
      COLUMN_STR   varchar(2048);
      SQLS         varchar(2048);
      USER_CURSOR  integer;

      type COL_LIST is table of varchar(40) index by binary_integer;

      type TABLE_STRUCT_REC is record(
         name varchar2(40),
         CONS varchar2(40));

      type TABLE_LIST_S is table of TABLE_STRUCT_REC index by binary_integer;

      TABLE_LIST TABLE_LIST_S;
      REF_TABLES TABLE_LIST_S;

   begin
      select count(*)
        into COUNT2
        from ALL_TABLES
       where OWNER = UPPER(OWNER_NAME)
         and TABLE_NAME = UPPER(TABLE_NAME);
      COUNT2 := 0;
      open TAB_CUR;
      loop
         fetch TAB_CUR
            into CONS_NAME, TO_CONS_NAME, REF_TABLE;
         if (TAB_CUR%notfound) then
            exit;
         end if;
         COUNT2 := COUNT2 + 1;
         REF_TABLES(COUNT2).CONS := TO_CONS_NAME;
         REF_TABLES(COUNT2).NAME := REF_TABLE;
         TABLE_LIST(COUNT2).CONS := CONS_NAME;
      end loop;

      close TAB_CUR;

      for I in 1 .. COUNT2
      loop
         begin
            SQLS        := 'alter table ' || OWNER_NAME || '.' || REF_TABLES(I).NAME ||
                           ' disable constraint ' || REF_TABLES(I).CONS;
            USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(USER_CURSOR, SQLS, DBMS_SQL.V7);
            NUM := DBMS_SQL.EXECUTE(USER_CURSOR);
            DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);
         exception
            when others then
               SQLS := '';
         end;
      end loop;

      SQLS        := 'delete from ' || OWNER_NAME || '.' || TABLE_NAME_STR || ' where ' ||
                     WHERE_CLAUSE;
      USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(USER_CURSOR, SQLS, DBMS_SQL.V7);
      NUM  := DBMS_SQL.EXECUTE(USER_CURSOR);
      CODE := sqlcode;
      DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);
      if (CODE <> 0) then
         -- code := sqlcode;
         ERR_MSG := 'code = ' || sqlerrm(CODE) || ' ' || SQLS;
         return - 1;
      end if;

      for I in 1 .. COUNT2
      loop
         begin
            SQLS        := 'alter table ' || OWNER_NAME || '.' || REF_TABLES(I).NAME ||
                           ' enable constraint   ' || REF_TABLES(I).CONS;
            USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(USER_CURSOR, SQLS, DBMS_SQL.V7);
            NUM := DBMS_SQL.EXECUTE(USER_CURSOR);
            DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);
         exception
            when others then
               SQLS := '';
         end;
      end loop;

      return 0;

   exception
      when others then
         ERR_MSG := 'code = ' || sqlerrm(CODE) || ' ' || SQLS;
         return - 1;
   end DELETE_TABLE;


   --**************************************************************************
   --                             Get Columns
   --**************************************************************************
   function GET_COLUMNS(A_TABLE_NAME    in varchar2,
                        A_TABLE_ABBREV  in varchar2,
                        A_OWNER_NAME    in varchar2,
                        A_EX_TABLE_LIST in TABLE_LIST_TYPE,
                        A_COLUMN_STR    in out varchar2,
                        A_ERR_MSG       out varchar2) return integer as

      cursor COL_CUR is
         select LOWER(COLUMN_NAME)
           from ALL_TAB_COLUMNS
          where TABLE_NAME = UPPER(A_TABLE_NAME)
            and OWNER = UPPER(A_OWNER_NAME)
          order by COLUMN_NAME;

      COUNT1   int;
      STR      varchar(2000);
      COL_NAME varchar(2000);
      COLS     TABLE_LIST_TYPE;
      CODE     int;
      SKIP     boolean;
      I        int;

   begin
      COUNT1 := 0;
      open COL_CUR;
      loop
         fetch COL_CUR
            into COL_NAME;

         if (COL_CUR%notfound) then
            exit;
         end if;
         if COL_NAME <> 'version_id' and COL_NAME <> 'time_stamp' and COL_NAME <> 'user_id' and
            COL_NAME <> 'tax_record_id' and COL_NAME <> 'status' then
            SKIP := false;
            if A_EX_TABLE_LIST.COUNT > 0 then
               for I in 1 .. A_EX_TABLE_LIST.COUNT
               loop
                  if A_EX_TABLE_LIST(I) = COL_NAME then
                     SKIP := true;
                  end if;
               end loop;

            end if;
            if SKIP = false then
               COUNT1 := COUNT1 + 1;
               COLS(COUNT1) := COL_NAME;
            end if;
         end if;
      end loop;

      close COL_CUR;

      A_COLUMN_STR := '';

      for I in 1 .. COUNT1
      loop
         --dbms_output.put_line('str=' || a_column_str);
         if LENGTH(A_TABLE_ABBREV) > 0 then
            A_COLUMN_STR := A_COLUMN_STR || A_TABLE_ABBREV || '.' || COLS(I);
         else
            A_COLUMN_STR := A_COLUMN_STR || COLS(I);
         end if;
         if I <> COUNT1 then
            A_COLUMN_STR := A_COLUMN_STR || ',';
         end if;
      end loop;

      DBMS_OUTPUT.PUT_LINE('count=' || COUNT1);
      A_ERR_MSG := '';
      return COUNT1;

   exception
      when others then
         CODE      := sqlcode;
         A_ERR_MSG := 'code = ' || sqlerrm(CODE);
         return CODE;
   end GET_COLUMNS;


   --**************************************************************************
   --                            Get Constraints
   --**************************************************************************
   function GET_CONSTRAINTS(A_TABLE_NAMES in TABLE_LIST_TYPE,
                            A_COUNT       in integer,
                            A_CONS_LIST   out CONS_LIST_S,
                            OWNER_NAME    in varchar2 := 'PWRPLANT',
                            A_SUBSYSTEM   in varchar2 := 'unknown') return integer as

      I            int;
      L_TABLE_NAME varchar(40);
      TO_TABLE     varchar(40);
      CONS_NAME    varchar2(40);
      TO_CONS_NAME varchar2(40);
      REF_TABLE    varchar2(40);
      STR          varchar(2000);
      COL_NAME     varchar2(40);
      COUNT1       integer;

      cursor REF_CONS_CUR is
         select LOWER(C.CONSTRAINT_NAME), LOWER(C2.CONSTRAINT_NAME), C2.TABLE_NAME
           from ALL_CONSTRAINTS C, ALL_CONSTRAINTS C2
          where C.CONSTRAINT_NAME = C2.R_CONSTRAINT_NAME
            and C.OWNER = OWNER_NAME
            and C.TABLE_NAME = UPPER(L_TABLE_NAME)
            and (C.CONSTRAINT_TYPE = 'P');

      cursor CONS_CUR is
         select LOWER(C2.CONSTRAINT_NAME), LOWER(C.CONSTRAINT_NAME), C.TABLE_NAME
           from ALL_CONSTRAINTS C, ALL_CONSTRAINTS C2
          where C.CONSTRAINT_NAME = C2.R_CONSTRAINT_NAME
            and C.OWNER = OWNER_NAME
            and C2.TABLE_NAME = UPPER(L_TABLE_NAME)
            and (C.CONSTRAINT_TYPE = 'P' or C.CONSTRAINT_TYPE = 'R');

      cursor COLS_CUR is
         select LOWER(COLUMN_NAME)
           from ALL_CONS_COLUMNS
          where OWNER = UPPER(OWNER_NAME)
            and TABLE_NAME = UPPER(TO_TABLE)
            and CONSTRAINT_NAME = UPPER(CONS_NAME)
          order by POSITION;

   begin
      I := 1;
      /* Tables that reference the Archive Tables */
      COUNT1 := 0;
      for I in 1 .. A_TABLE_NAMES.COUNT
      loop
         begin
            L_TABLE_NAME := A_TABLE_NAMES(I);
            open REF_CONS_CUR;
            loop
               fetch REF_CONS_CUR
                  into CONS_NAME, TO_CONS_NAME, REF_TABLE;
               if (REF_CONS_CUR%notfound) then
                  exit;
               end if;
               COUNT1 := COUNT1 + 1;
               A_CONS_LIST(COUNT1).REF_NAME := L_TABLE_NAME;
               A_CONS_LIST(COUNT1).REF_CONS := CONS_NAME;
               A_CONS_LIST(COUNT1).NAME := REF_TABLE;
               A_CONS_LIST(COUNT1).CONS := TO_CONS_NAME;
               A_CONS_LIST(COUNT1).CONS_TYPE := 'F';
               /* Ref columns */
               TO_TABLE := L_TABLE_NAME;
               open COLS_CUR;
               STR := '';
               fetch COLS_CUR
                  into COL_NAME;
               loop
                  STR := STR || COL_NAME;
                  fetch COLS_CUR
                     into COL_NAME;
                  if (COLS_CUR%notfound) then
                     exit;
                  end if;

                  STR := STR || ',';
               end loop;

               close COLS_CUR;

               A_CONS_LIST(COUNT1).REF_COLS := STR;
               /*  columns */
               TO_TABLE  := REF_TABLE;
               CONS_NAME := TO_CONS_NAME;
               open COLS_CUR;
               STR := '';
               fetch COLS_CUR
                  into COL_NAME;
               loop
                  STR := STR || COL_NAME;
                  fetch COLS_CUR
                     into COL_NAME;
                  if (COLS_CUR%notfound) then
                     exit;
                  end if;

                  STR := STR || ',';
               end loop;

               close COLS_CUR;

               A_CONS_LIST(COUNT1).COLS := STR;

            end loop;

            close REF_CONS_CUR;

         end;
      end loop;

      /* Get Foreign Keys for this Table */
      for I in 1 .. A_TABLE_NAMES.COUNT
      loop
         begin
            L_TABLE_NAME := A_TABLE_NAMES(I);
            open CONS_CUR;
            loop
               fetch CONS_CUR
                  into CONS_NAME, TO_CONS_NAME, REF_TABLE;
               if (CONS_CUR%notfound) then
                  exit;
               end if;
               COUNT1 := COUNT1 + 1;
               A_CONS_LIST(COUNT1).NAME := L_TABLE_NAME;
               A_CONS_LIST(COUNT1).CONS := CONS_NAME;
               A_CONS_LIST(COUNT1).REF_NAME := REF_TABLE;
               A_CONS_LIST(COUNT1).REF_CONS := TO_CONS_NAME;
               A_CONS_LIST(COUNT1).CONS_TYPE := 'R';
               /* Ref columns */
               TO_TABLE := L_TABLE_NAME;
               open COLS_CUR;
               STR := '';
               fetch COLS_CUR
                  into COL_NAME;
               loop
                  STR := STR || COL_NAME;
                  fetch COLS_CUR
                     into COL_NAME;
                  if (COLS_CUR%notfound) then
                     exit;
                  end if;
                  STR := STR || ',';
               end loop;

               close COLS_CUR;

               A_CONS_LIST(COUNT1).COLS := STR;
               /* columns */
               TO_TABLE  := REF_TABLE;
               CONS_NAME := TO_CONS_NAME;
               open COLS_CUR;
               STR := '';
               fetch COLS_CUR
                  into COL_NAME;
               loop
                  STR := STR || COL_NAME;
                  fetch COLS_CUR
                     into COL_NAME;
                  if (COLS_CUR%notfound) then
                     exit;
                  end if;

                  STR := STR || ',';
               end loop;

               close COLS_CUR;

               A_CONS_LIST(COUNT1).REF_COLS := STR;
            end loop;

            close CONS_CUR;

         end;
      end loop;

      return 0;

   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         return - 1;
         /*sql = 'alter table ' + table_ref_list[i]  + ' add  ( foreign key (  ' +
                              col_str + ')' +
                             ' REFERENCES '  +  table_list[i] + ' (' +
                              col_str + '));'
         */
   end GET_CONSTRAINTS;


   --**************************************************************************
   --                           Drop Constraints
   --**************************************************************************
   function DROP_CONSTRAINTS(A_CONS_LIST  in CONS_LIST_S,
                             A_TRACE_LIST in out TRACE_LIST_TYPE,
                             OWNER_NAME   in varchar2 := 'PWRPLANT') return integer as

      I             int;
      J             int;
      ERR_MSG       varchar(2000);
      SQLS          varchar(2000);
      COUNT1        integer;
      TEMP_NAME_NEW varchar2(40);
      name          varchar2(40);
      CODE          int;
      WARNINGS      int;
      USER_CURSOR   int;
      NUM           int;

   begin
      WARNINGS := 0;
      /* Drop constraint */
      COUNT1 := 0;
      for I in 1 .. A_CONS_LIST.COUNT
      loop
         SQLS := 'alter table ' || A_CONS_LIST(I).NAME || ' drop constraint ' || A_CONS_LIST(I).CONS;
         TRACE_WRITE(A_TRACE_LIST, SQLS, 1);
         begin
            --Execute Immediate sqls;
            USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(USER_CURSOR, SQLS, DBMS_SQL.V7);
            NUM := DBMS_SQL.EXECUTE(USER_CURSOR);
            DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);
         exception
            when others then
               CODE := sqlcode;
               if CODE <> -2443 then
                  ERR_MSG := 'code = ' || sqlerrm(CODE);
                  TRACE_WRITE(A_TRACE_LIST, ERR_MSG, 2);
                  WARNINGS := WARNINGS + 1;
               end if;
         end;
      end loop;

      return WARNINGS;
   end DROP_CONSTRAINTS;


   --**************************************************************************
   --                            Set Constraints
   --**************************************************************************
   function SET_CONSTRAINTS(A_CONS_LIST  in CONS_LIST_S,
                            A_TRACE_LIST in out TRACE_LIST_TYPE,
                            OWNER_NAME   in varchar2 := 'PWRPLANT') return integer as

      I             int;
      J             int;
      ERR_MSG       varchar(2000);
      SQLS          varchar(2000);
      COUNT1        integer;
      TEMP_NAME_NEW varchar2(40);
      name          varchar2(40);
      CODE          int;
      WARNINGS      int;
      USER_CURSOR   int;
      NUM           int;

   begin
      WARNINGS := 0;
      /* Get Primary Key and Tables that Refernne it */
      COUNT1 := 0;
      for I in 1 .. A_CONS_LIST.COUNT
      loop
         SQLS := 'alter table ' || A_CONS_LIST(I).NAME || ' add  ( foreign key (  ' || A_CONS_LIST(I).COLS || ')' ||
                 ' REFERENCES ' || A_CONS_LIST(I).REF_NAME || ' (' || A_CONS_LIST(I).REF_COLS || '))';
         TRACE_WRITE(A_TRACE_LIST, SQLS, 4);
         begin
            --Execute Immediate sqls;
            USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(USER_CURSOR, SQLS, DBMS_SQL.V7);
            NUM := DBMS_SQL.EXECUTE(USER_CURSOR);
            DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);
         exception
            when others then
               CODE := sqlcode;
               if CODE <> -2275 then
                  ERR_MSG := 'code = ' || sqlerrm(CODE);
                  TRACE_WRITE(A_TRACE_LIST, ERR_MSG, 2);
                  WARNINGS := WARNINGS + 1;
               end if;
         end;
      end loop;

      return WARNINGS;
   end SET_CONSTRAINTS;


   --**************************************************************************
   --                            Exec SQL
   --**************************************************************************
   function EXEC_SQL(A_SQL     in varchar2,
                     A_ERR_MSG in out varchar2) return integer as

      USER_CURSOR integer;
      CODE        int;
      NUM         int;

   begin
      --Execute Immediate a_sql;
      USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(USER_CURSOR, A_SQL, DBMS_SQL.V7);
      NUM := DBMS_SQL.EXECUTE(USER_CURSOR);
      DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);
      return 0;

   exception
      when NO_DATA_FOUND then
         return 0;

      when others then
         CODE := sqlcode;
         if CODE = 0 then
            return 0;
         end if;
         A_ERR_MSG := 'code = ' || sqlerrm(CODE);
         return - 1;
   end EXEC_SQL;


   --**************************************************************************
   --                            Trace Write
   --**************************************************************************
   --
   --  type 1 info
   --  type 2 warning
   --  type 3 error
   --  type 4 constraint
   --  type 5 index
   --
   --**************************************************************************
   procedure TRACE_WRITE(A_TRACE_LIST in out TRACE_LIST_TYPE,
                         A_STR        in varchar2,
                         A_TYPE       integer) as

      I int;

   begin
      I := A_TRACE_LIST.COUNT + 1;
      A_TRACE_LIST(I).STR := A_STR;
      A_TRACE_LIST(I).TRACE_TYPE := A_TYPE;
      A_TRACE_LIST(I).TRACE_DATE := sysdate;
   exception
      when others then
         return;
   end TRACE_WRITE;


   --**************************************************************************
   --                        Trace Save
   --**************************************************************************
   procedure TRACE_SAVE(A_TRACE_LIST in out TRACE_LIST_TYPE,
                        A_JOB_NO     integer,
                        A_LINE_NO    in out integer) as

      I int;

   begin
      if NVL(A_LINE_NO, -1) = -1 or A_JOB_NO = 0 or A_TRACE_LIST.COUNT = 0 then
         return;
      end if;
      for I in 1 .. A_TRACE_LIST.COUNT
      loop
         insert into ARCHIVE_TRACE
            (JOB_NO, LINE_NO, LINE, TRACE_DATE, TRACE_TYPE)
         values
            (A_JOB_NO, I + A_LINE_NO, A_TRACE_LIST(I).STR, A_TRACE_LIST(I).TRACE_DATE,
             A_TRACE_LIST(I).TRACE_TYPE);
      end loop;

      commit;
      A_LINE_NO := A_LINE_NO + A_TRACE_LIST.COUNT;
      A_TRACE_LIST.DELETE;
      return;

   exception
      when others then
         return;
   end TRACE_SAVE;


   --**************************************************************************
   --                              Start Job
   --**************************************************************************
   procedure START_JOB(A_VERSION_NUMBER in number,
                       A_FCST_NUMBER    in number,
                       A_ARC_TYPE       in varchar2,
                       A_JOBNO          in out number,
                       A_LOGID          in out number) as

      NUM_REC     number;
      NUM_REC_ARC number;

   begin
      /* Get Job No */
      while A_JOBNO = 0
      loop
         select NVL(JOBID, 0)
           into A_JOBNO
           from TAX_ARCHIVE_STATUS
          where VERSION_ID = A_VERSION_NUMBER;
      end loop;

      if A_ARC_TYPE = 'Tax' then
         select count(*) into NUM_REC from TAX_RECORD_CONTROL where VERSION_ID = A_VERSION_NUMBER;
         select count(*)
           into NUM_REC_ARC
           from ARC_TAX_RECORD_CONTROL
          where VERSION_ID = A_VERSION_NUMBER;
      else
         /* must be a forecast type of archive */
         select count(*)
           into NUM_REC
           from TAX_FORECAST_OUTPUT
          where TAX_FORECAST_VERSION_ID = A_FCST_NUMBER;
         select count(*)
           into NUM_REC_ARC
           from ARC_FCST_TAX_FORECAST_OUTPUT
          where TAX_FORECAST_VERSION_ID = A_FCST_NUMBER;
      end if;
      select max(LOG_ID) into A_LOGID from ARCHIVE_SYSTEM_LOG where JOB_NO = A_JOBNO;
      update ARCHIVE_SYSTEM_LOG
         set START_TIME = sysdate, START_ACTIVE_REC = NUM_REC, START_ARCHIVE_REC = NUM_REC_ARC,
             STATUS = 'Start'
       where LOG_ID = A_LOGID;

   exception
      when others then
         return;
   end START_JOB;


   --**************************************************************************
   --                            Stop Job
   --**************************************************************************
   procedure STOP_JOB(A_VERSION_NUMBER in number,
                      A_ARC_TYPE       in varchar2,
                      A_JOBNO          in number,
                      A_LOGID          in number,
                      A_STATUS         in varchar2) as

      NUM_REC     number;
      NUM_REC_ARC number;

   begin

      if A_ARC_TYPE = 'Tax' then
         select count(*) into NUM_REC from TAX_RECORD_CONTROL where VERSION_ID = A_VERSION_NUMBER;
         select count(*)
           into NUM_REC_ARC
           from ARC_TAX_RECORD_CONTROL
          where VERSION_ID = A_VERSION_NUMBER;
      else
         /* must be a forecast type of archive */
         select count(*)
           into NUM_REC
           from TAX_FORECAST_OUTPUT
          where TAX_FORECAST_VERSION_ID = A_VERSION_NUMBER;
         select count(*)
           into NUM_REC_ARC
           from ARC_FCST_TAX_FORECAST_OUTPUT
          where TAX_FORECAST_VERSION_ID = A_VERSION_NUMBER;
      end if;
      update ARCHIVE_SYSTEM_LOG
         set END_TIME = sysdate, END_ACTIVE_REC = NUM_REC, END_ARCHIVE_REC = NUM_REC_ARC,
             STATUS = 'Finish'
       where LOG_ID = A_LOGID;
      commit;

   exception
      when others then
         return;
   end STOP_JOB;


   --**************************************************************************
   --                         Check Record Count
   --**************************************************************************
   function CHECK_RECORD_COUNT(A_VERSION_NUMBER in number,
                               A_ARC_TYPE       in varchar2,
                               A_LOGID          number) return integer as

      NUM_REC     number;
      NUM_REC_ARC number;

   begin

      if A_ARC_TYPE = 'Archive' then
         select count(*) into NUM_REC from TAX_RECORD_CONTROL where VERSION_ID = A_VERSION_NUMBER;
         select count(*)
           into NUM_REC_ARC
           from ARC_TAX_RECORD_CONTROL
          where VERSION_ID = A_VERSION_NUMBER;
      else
         /* Restore */
         select count(*) into NUM_REC from TAX_RECORD_CONTROL where VERSION_ID = A_VERSION_NUMBER;
         select START_ARCHIVE_REC into NUM_REC_ARC from ARCHIVE_SYSTEM_LOG where LOG_ID = A_LOGID;
      end if;
      if NUM_REC <> NUM_REC_ARC then
         return - 1;
      end if;
      return 0;

   exception
      when others then
         return - 1;
   end CHECK_RECORD_COUNT;


   --**************************************************************************
   --                          Create Column
   --**************************************************************************
   function CREATE_COLUNN(A_TABLE  in varchar2,
                          A_COLUMN in varchar2) return integer as

      NUM_REC     number;
      NUM_REC_ARC number;
      CODE        int;
      ASCALE      number;
      AFIELD      varchar2(40);
      APRECISION  number;
      AWIDTH      number;
      ANULLABLE   varchar(4);
      SQLS        varchar(2028);
      ADEFAULT    varchar(100);

   begin

      SQLS := 'alter table ' || A_TABLE || ' add ( ' || A_COLUMN || ' ';
      select DATA_TYPE, DATA_PRECISION, DATA_SCALE, DATA_LENGTH, NULLABLE, DATA_DEFAULT
        into AFIELD, APRECISION, ASCALE, AWIDTH, ANULLABLE, ADEFAULT
        from ALL_TAB_COLUMNS
       where TABLE_NAME = A_TABLE
         and COLUMN_NAME = UPPER(A_COLUMN);

      if LOWER(AFIELD) = 'varchar' then
         SQLS := SQLS || ' varchar(' || TO_CHAR(AWIDTH) || ') ';
      elsif LOWER(AFIELD) = 'varchar2' then
         SQLS := SQLS || ' varchar2(' || TO_CHAR(AWIDTH) || ') ';
      elsif LOWER(AFIELD) = 'char' then
         SQLS := SQLS || ' char(' || TO_CHAR(AWIDTH) || ') ';

      elsif LOWER(AFIELD) = 'number' then
         if not AWIDTH is null and not ASCALE is null then
            SQLS := SQLS || ' number(' || TO_CHAR(AWIDTH) || ',' || TO_CHAR(ASCALE) || ')';
         elsif not AWIDTH is null and ASCALE is null then
            SQLS := SQLS || ' number(' || TO_CHAR(AWIDTH) || ')';
         else
            SQLS := SQLS || ' number ';
         end if;

      elsif LOWER(AFIELD) = 'float' then
         if not ASCALE is null and APRECISION <> 0 then
            SQLS := SQLS || ' float(' || TO_CHAR(APRECISION) || ')';
         else
            SQLS := SQLS || ' float ';
         end if;
      elsif LOWER(AFIELD) = 'long' then
         SQLS := SQLS || ' long ';
      elsif LOWER(AFIELD) = 'long raw' then
         SQLS := SQLS || ' long raw';
      elsif LOWER(AFIELD) = 'rowid' then
         SQLS := SQLS || ' rowid ';
      elsif LOWER(AFIELD) = 'raw' then
         SQLS := SQLS || ' raw ';
      elsif LOWER(AFIELD) = 'date' then
         SQLS := SQLS || ' date ';
      else
         -- messagebox('Error','Unknown type ' + field)
         return 0;
      end if;

      if ADEFAULT is null then
         --if IsNumber ( adefault)  = false then
         SQLS := SQLS || ' default ' || ''' || adefault || ''';
         --else
         --  sqls := sqls || ' default ' || adefault;
         --end if;
      end if;

      if ANULLABLE is null then
         SQLS := SQLS || ' not null ';
      end if;
      return CODE;

   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         CODE := sqlcode;
         return CODE;
   end CREATE_COLUNN;


   --**************************************************************************
   --                           Check Arc Table
   --**************************************************************************
   function CHECK_ARC_TABLE(A_TABLE1  in varchar2,
                            A_TABLE2  in varchar2,
                            A_ADD_SQL in varchar2) return integer as

      CODE    int;
      NUM     int;
      ERR_MSG varchar(2048);

   begin
      select count(*)
        into NUM
        from ALL_TABLES
       where TABLE_NAME = UPPER(A_TABLE2)
         and ROWNUM = 1
         and OWNER = 'PWRPLANT';
      if NUM < 1 then

         CODE := DUPICATE_TABLE(A_TABLE1, A_TABLE2, A_ADD_SQL, ERR_MSG);
      else
         CODE := CHECK_TABLE_COLUMNS(A_TABLE1, A_TABLE2, ERR_MSG);
      end if;
      return CODE;

   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         CODE := sqlcode;
         return CODE;
   end CHECK_ARC_TABLE;


   --**************************************************************************
   --                       Check Table Columns
   --**************************************************************************
   function CHECK_TABLE_COLUMNS(A_TABLE1  in varchar2,
                                A_TABLE2  in varchar2,
                                A_ERR_MSG in out varchar2) return integer as

      cursor TAB_CUR is
         select COLUMN_NAME,
                DATA_TYPE,
                DATA_PRECISION,
                DATA_SCALE,
                DATA_LENGTH,
                NULLABLE,
                DATA_DEFAULT
           from ALL_TAB_COLUMNS
          where TABLE_NAME = A_TABLE1
            and OWNER = 'PWRPLANT'
          order by COLUMN_ID;

      L_NAME          varchar2(100);
      L_FIELD         varchar2(40);
      L_NULLABLE      varchar2(10);
      L_DEFAULT       varchar2(40);
      L_PRECISION     number;
      L_SCALE         number;
      L_WIDTH         number;
      L_NAME_ARC      varchar2(100);
      L_FIELD_ARC     varchar2(40);
      L_NULLABLE_ARC  varchar2(10);
      L_DEFAULT_ARC   varchar2(40);
      L_PRECISION_ARC number;
      L_SCALE_ARC     number;
      L_WIDTH_ARC     number;
      L_WIDTH_NEW     number;
      SQL_LIST        SQL_LIST_TYPE;
      SQL_COL         varchar2(248);
      CODE            int;
      NUM             int;
      K               int;

   begin
      open TAB_CUR;
      NUM := 0;
      loop
         fetch TAB_CUR
            into L_NAME, L_FIELD, L_PRECISION, L_SCALE, L_WIDTH, L_NULLABLE, L_DEFAULT;
         if (TAB_CUR%notfound) then
            exit;
         end if;
         begin
            select COLUMN_NAME,
                   DATA_TYPE,
                   DATA_PRECISION,
                   DATA_SCALE,
                   DATA_LENGTH,
                   NULLABLE,
                   DATA_DEFAULT
              into L_NAME_ARC,
                   L_FIELD_ARC,
                   L_PRECISION_ARC,
                   L_SCALE_ARC,
                   L_WIDTH_ARC,
                   L_NULLABLE_ARC,
                   L_DEFAULT_ARC
              from ALL_TAB_COLUMNS
             where TABLE_NAME = A_TABLE2
               and COLUMN_NAME = UPPER(L_NAME)
               and ROWNUM = 1
               and OWNER = 'PWRPLANT';
         exception
            when NO_DATA_FOUND then
               L_NAME_ARC := '';
            when others then
               begin
                  CODE := sqlcode;
                  DBMS_OUTPUT.PUT_LINE('code= ' || CODE);
                  return CODE;
               end;
         end;
         <<NODATA>>

         if L_NAME_ARC is null then

            DBMS_OUTPUT.PUT_LINE('new col2 ');
            NUM  := NUM + 1;
            CODE := GET_COLUMN_FORMAT(L_NAME,
                                      L_FIELD,
                                      L_PRECISION,
                                      L_SCALE,
                                      L_WIDTH,
                                      L_NULLABLE,
                                      L_DEFAULT,
                                      SQL_COL);
            DBMS_OUTPUT.PUT_LINE(SQL_COL);
            SQL_LIST(NUM) := 'alter table ' || A_TABLE2 || ' add ( ' || SQL_COL || ')';
         else
            --dbms_output.put_line ('exist col ' || l_name);
            if LOWER(L_FIELD) = 'varchar' and LOWER(L_FIELD_ARC) = 'varchar' then
               if L_WIDTH > L_WIDTH_ARC then
                  NUM := NUM + 1;
                  SQL_LIST(NUM) := 'alter table ' || A_TABLE2 || ' modify column (( ' || L_NAME_ARC ||
                                   ' varchar(' || TO_CHAR(L_WIDTH) || '))) ';
               end if;
            elsif LOWER(L_FIELD) = 'varchar2' and LOWER(L_FIELD_ARC) = 'varchar2' then
               if L_WIDTH > L_WIDTH_ARC then
                  NUM := NUM + 1;
                  SQL_LIST(NUM) := 'alter table ' || A_TABLE2 || ' modify column (( ' || L_NAME_ARC ||
                                   ' varchar2(' || TO_CHAR(L_WIDTH) || ')) ';
               end if;
            elsif LOWER(L_FIELD) = 'char' and LOWER(L_FIELD_ARC) = 'char' then
               if L_WIDTH > L_WIDTH_ARC then
                  NUM := NUM + 1;
                  SQL_LIST(NUM) := 'alter table ' || A_TABLE2 || ' modify column ((' || L_NAME_ARC ||
                                   '   char(' || TO_CHAR(L_WIDTH) || ')) ';
               end if;
            elsif LOWER(L_FIELD) = 'number' then
               if LOWER(L_FIELD) = 'number' and LOWER(L_FIELD_ARC) = 'number' then
                  if not L_WIDTH is null and not L_SCALE is null then
                     if L_SCALE > L_SCALE_ARC or L_SCALE_ARC is null then
                        L_WIDTH_NEW := L_WIDTH_ARC + L_SCALE;
                        if L_WIDTH_NEW < L_WIDTH then
                           L_WIDTH_NEW := L_WIDTH;
                        end if;
                        NUM := NUM + 1;
                        SQL_LIST(NUM) := 'alter table ' || A_TABLE2 || ' modify column (( ' ||
                                         L_NAME_ARC || ' number(' || TO_CHAR(L_WIDTH_NEW) || ',' ||
                                         TO_CHAR(L_SCALE) || ')';
                     end if;
                  elsif not L_WIDTH is null and L_SCALE is null then
                     K := 0; --a_sqls := a_sqls || l_name_arc || ' number(' || to_char(a_width) || ')';
                  else
                     K := 0; --a_sqls := a_sqls || l_name_arc || ' number ';
                  end if;
               end if;
            end if; -- number
         end if; -- end elese
      end loop;

      close TAB_CUR;

      DBMS_OUTPUT.PUT_LINE('line col ' || SQL_LIST.COUNT);
      for I in 1 .. SQL_LIST.COUNT
      loop
         CODE := EXEC_SQL(SQL_LIST(I), A_ERR_MSG);
         if CODE <> 0 then
            return CODE;
         end if;
      end loop;

      return CODE;

   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         CODE := sqlcode;
         return CODE;
   end CHECK_TABLE_COLUMNS;


   --**************************************************************************
   --                         Create Table
   --**************************************************************************
   function CREATE_TABLE(A_TABLE in varchar2) return integer as

      cursor TAB_CUR is
         select COLUMN_NAME,
                DATA_TYPE,
                DATA_PRECISION,
                DATA_SCALE,
                DATA_LENGTH,
                NULLABLE,
                DATA_DEFAULT
           from ALL_TAB_COLUMNS
          where TABLE_NAME = A_TABLE
          order by COLUMN_ID;

      SQLS        varchar2(2048);
      STR         varchar2(2048);
      I           int;
      NUM         int;
      name        varchar2(100);
      FIELD       varchar2(40);
      ANULLABLE   varchar2(10);
      DATADEFAULT varchar2(40);
      APRECISION  number;
      ASCALE      number;
      AWIDTH      number;
      SQL_COL     varchar2(248);
      TABLE_OWNER varchar2(100);
      INIT        int;
      CODE        int;

   begin
      select count(*) into NUM from ALL_TABLES where TABLE_NAME = A_TABLE;
      if NUM < 1 then
         return 0;
      end if;

      open TAB_CUR;
      STR := '';

      INIT := 1;
      SQLS := 'create table ' || TABLE_OWNER || '.' || A_TABLE || '(';
      loop
         fetch TAB_CUR
            into name, FIELD, APRECISION, ASCALE, AWIDTH, ANULLABLE, DATADEFAULT;
         if (TAB_CUR%notfound) then
            exit;
         end if;
         CODE := GET_COLUMN_FORMAT(name,
                                   FIELD,
                                   APRECISION,
                                   ASCALE,
                                   AWIDTH,
                                   ANULLABLE,
                                   DATADEFAULT,
                                   SQL_COL);
         if INIT <> 0 then
            SQLS := SQLS + SQL_COL;
         end if;
         INIT := 0;
      end loop;

      close TAB_CUR;

      SQLS := SQLS || ')';

   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         CODE := sqlcode;
         return CODE;
   end CREATE_TABLE;


   --**************************************************************************
   --                       Get Column Format
   --**************************************************************************
   function GET_COLUMN_FORMAT(A_NAME      varchar2,
                              A_FIELD     varchar2,
                              A_PRECISION number,
                              A_SCALE     number,
                              A_WIDTH     number,
                              A_NULLABLE  varchar2,
                              A_DEFAULT   varchar2,
                              A_SQLS      in out varchar2) return integer as

      CODE int;

   begin
      DBMS_OUTPUT.PUT_LINE('field ' || A_FIELD);
      if LOWER(A_FIELD) = 'varchar' then
         A_SQLS := A_SQLS || A_NAME || ' varchar(' || TO_CHAR(A_WIDTH) || ') ';

      elsif LOWER(A_FIELD) = 'varchar2' then
         A_SQLS := A_SQLS || A_NAME || ' varchar2(' || TO_CHAR(A_WIDTH) || ') ';

      elsif LOWER(A_FIELD) = 'char' then
         A_SQLS := A_SQLS || A_NAME || ' char(' || TO_CHAR(A_WIDTH) || ') ';

      elsif LOWER(A_FIELD) = 'number' then
         if not A_WIDTH is null and not A_SCALE is null then
            A_SQLS := A_SQLS || A_NAME || ' number(' || TO_CHAR(A_WIDTH) || ',' || TO_CHAR(A_SCALE) || ')';
         elsif not A_WIDTH is null and A_SCALE is null then
            A_SQLS := A_SQLS || A_NAME || ' number(' || TO_CHAR(A_WIDTH) || ')';
         else
            A_SQLS := A_SQLS || A_NAME || ' number ';
         end if;
         DBMS_OUTPUT.PUT_LINE('field ' || A_SQLS);
      elsif LOWER(A_FIELD) = 'float' then
         if not A_SCALE is null and A_PRECISION <> 0 then
            A_SQLS := A_SQLS || A_NAME || ' float(' || TO_CHAR(A_PRECISION) || ')';
         else
            A_SQLS := A_SQLS || A_NAME || ' float ';
         end if;
      elsif LOWER(A_FIELD) = 'long' then
         A_SQLS := A_SQLS || A_NAME || ' long ';
      elsif LOWER(A_FIELD) = 'long raw' then
         A_SQLS := A_SQLS || A_NAME || ' long raw';
      elsif LOWER(A_FIELD) = 'rowid' then
         A_SQLS := A_SQLS || A_NAME || ' rowid ';
      elsif LOWER(A_FIELD) = 'raw' then
         A_SQLS := A_SQLS || A_NAME || ' raw ';
      elsif LOWER(A_FIELD) = 'date' then
         A_SQLS := A_SQLS || A_NAME || ' date ';
      else
         return - 1;
      end if;

      if not A_DEFAULT is null then
         --if IsNumber ( default)  = false then
         A_SQLS := A_SQLS || ' default ' || '''' || A_DEFAULT || '''';
         --else
         --  sqls := sqls || ' default ' || datadefault
         --end if
      end if;

      if A_NULLABLE = 'N' then
         A_SQLS := A_SQLS || ' not null ';
      end if;

      DBMS_OUTPUT.PUT_LINE('field ' || A_SQLS);

      return 0;

   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         CODE := sqlcode;
         return CODE;
   end GET_COLUMN_FORMAT;


   --**************************************************************************
   --                       Duplicate Table
   --**************************************************************************
   function DUPICATE_TABLE(A_FROM_TABLE  in varchar2,
                           A_NEW_TABLE   in varchar2,
                           A_ALTER_EXTRA in varchar2,
                           A_ERR_MSG     in out varchar2) return integer as

      SQLS        varchar2(2048);
      CODE        int;
      TABLE_OWNER varchar2(100);

   begin
      SQLS := 'create table ' || A_NEW_TABLE || ' as select * from ' || A_FROM_TABLE ||
              ' where rownum = 0';
      DBMS_OUTPUT.PUT_LINE('table2= ' || SQLS);
      CODE := EXEC_SQL(SQLS, A_ERR_MSG);
      SQLS := 'alter table ' || A_NEW_TABLE || ' ' || A_ALTER_EXTRA;
      DBMS_OUTPUT.PUT_LINE('alter= ' || SQLS);
      CODE := EXEC_SQL(SQLS, A_ERR_MSG);

      return CODE;
   end DUPICATE_TABLE;


   --**************************************************************************
   --                       Get Table List
   --**************************************************************************
   function GET_TABLE_LIST(A_TABLES               in out TABLE_LIST_TYPE,
                           A_ARC_TABLES           out TABLE_LIST_TYPE,
                           A_FCST_TABLES          in out TABLE_LIST_TYPE,
                           A_ARC_FCST_TABLES      out TABLE_LIST_TYPE,
                           A_FCST_ARC_FCST_TABLES out TABLE_LIST_TYPE) return integer as

      SQLS        varchar2(2048);
      CODE        int;
      I           int;
      TABLE_OWNER varchar2(100);

   begin
      A_TABLES(1) := 'TAX_DEPRECIATION';
      A_TABLES(2) := 'TAX_BOOK_RECONCILE';
      A_TABLES(3) := 'BASIS_AMOUNTS';
      A_TABLES(4) := 'DEFERRED_INCOME_TAX';
      A_TABLES(5) := 'TAX_ANNOTATION';
      A_TABLES(6) := 'TAX_DEPR_ADJUST';
      A_TABLES(7) := 'TAX_CONTROL';
      A_TABLES(8) := 'TAX_BOOK_RECONCILE_TRANSFER';
      A_TABLES(9) := 'DEFERRED_INCOME_TAX_TRANSFER';
      A_TABLES(10) := 'TAX_DEPRECIATION_TRANSFER';
      A_TABLES(11) := 'TAX_TRANSFER_CONTROL';
      A_TABLES(12) := 'TAX_RECORD_CONTROL';

      for I in 1 .. A_TABLES.COUNT
      loop
         if LENGTH(A_TABLES(I)) > 25 then
            A_ARC_TABLES(I) := 'ARC_' || SUBSTR(A_TABLES(I), 1, 26);
         else
            A_ARC_TABLES(I) := 'ARC_' || A_TABLES(I);
         end if;
      end loop;

      A_FCST_TABLES(1) := 'TAX_FORECAST_OUTPUT';
      A_FCST_TABLES(2) := 'TAX_FORECAST_INPUT';
      A_FCST_TABLES(3) := 'DFIT_FORECAST_OUTPUT';

      for I in 1 .. A_FCST_TABLES.COUNT
      loop
         if LENGTH(A_FCST_TABLES(I)) > 25 then
            A_ARC_FCST_TABLES(I) := 'ARC_' || SUBSTR(A_FCST_TABLES(I), 1, 26);
         else
            A_ARC_FCST_TABLES(I) := 'ARC_' || A_FCST_TABLES(I);
         end if;
         if LENGTH(A_FCST_TABLES(I)) > 20 then
            A_FCST_ARC_FCST_TABLES(I) := 'ARC_FCST_' || SUBSTR(A_FCST_TABLES(I), 1, 21);
         else
            A_FCST_ARC_FCST_TABLES(I) := 'ARC_FCST_' || A_FCST_TABLES(I);
         end if;
      end loop;

      return 0;

   end GET_TABLE_LIST;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (313, 0, 10, 4, 0, 0, 11332, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011332_tax_ARCHIVE_TAX.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
