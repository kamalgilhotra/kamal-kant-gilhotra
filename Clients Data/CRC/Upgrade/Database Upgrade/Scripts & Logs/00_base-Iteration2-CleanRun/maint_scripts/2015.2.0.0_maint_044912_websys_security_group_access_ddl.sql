/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Security
|| File Name:   maint_044912_websys_security_group_access_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2     09/11/2015 Ryan Oliveria    	 Creating new table 'PP_WEB_GROUP_ACCESS'
||==========================================================================================
*/

SET serveroutput ON size 30000;

declare
	doesTableExist number := 0;
begin
	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_GROUP_ACCESS','PWRPLANT');

	if doesTableExist = 0 then
        begin
			dbms_output.put_line('Creating table PP_WEB_GROUP_ACCESS');

			EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_WEB_GROUP_ACCESS"
								(
									"GROUPS" varchar2(35) not null,
									"MODULE_ID" number(22,0) not null,
									"COMPONENT_ID" number(22,0) not null,
									CONSTRAINT FK_PP_WEB_GROUP_ACCESS_G FOREIGN KEY ("GROUPS") REFERENCES PP_WEB_SECURITY_GROUPS ("GROUPS"),
									CONSTRAINT FK_PP_WEB_GROUP_ACCESS_M FOREIGN KEY ("MODULE_ID") REFERENCES PP_WEB_MODULES ("MODULE_ID"),
									CONSTRAINT FK_PP_WEB_GROUP_ACCESS_C FOREIGN KEY ("COMPONENT_ID") REFERENCES PP_WEB_COMPONENT ("COMPONENT_ID")
								) TABLESPACE PWRPLANT';

			EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_WEB_GROUP_ACCESS" IS ''Stores which components a web security group has access to.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_GROUP_ACCESS"."GROUPS" is ''The name of the web security group.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_GROUP_ACCESS"."MODULE_ID" is ''A module this group has access to.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_GROUP_ACCESS"."COMPONENT_ID" is ''A component this group has access to.''';

		end;
	end if;
end;
/

alter table PP_WEB_COMPONENT modify DESCRIPTION varchar2(35);
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2876, 0, 2015, 2, 0, 0, 044912, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044912_websys_security_group_access_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
/