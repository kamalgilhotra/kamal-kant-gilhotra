/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032284_lease_subledger.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   09/09/2013 Ryan Oliveria
||============================================================================
*/

update SUBLEDGER_CONTROL set DEPRECIATION_INDICATOR = 3 where SUBLEDGER_TYPE_ID = -100;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (602, 0, 10, 4, 1, 0, 32284, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032284_lease_subledger.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;