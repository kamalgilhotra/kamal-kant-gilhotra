/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038293_reg_analysis_rev_req.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/22/2014 Ryan Oliveria  Reg Analysis Rev Req Reports
||========================================================================================
*/


insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION,
    REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID,
    PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Revenue Requirement Comparison',
          'Revenue Requirement Comparison',
          null SUBSYSTEM,
          'dw_reg_report_rev_req_compare',
          'already has report time' SPECIAL_NOTE,
          null REPORT_TYPE,
          null TIME_OPTION,
          'Revenue Requirement Comparison' REPORT_NUMBER,
          null INPUT_WINDOW,
          null FILTER_OPTION,
          null STATUS,
          101 PP_REPORT_SUBSYSTEM_ID,
          407 REPORT_TYPE_ID,
          null PP_REPORT_TIME_OPTION_ID,
          null PP_REPORT_FILTER_ID,
          1 PP_REPORT_STATUS_ID,
          7 PP_REPORT_ENVIR_ID, /*All Hide environment, so it will only appear in Reg Analysis Wksp*/
          null DOCUMENTATION,
          null USER_COMMENT,
          null LAST_APPROVED_DATE,
          null PP_REPORT_NUMBER,
          null OLD_REPORT_NUMBER,
          null DYNAMIC_DW
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION,
    REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID,
    PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Deficiency Impact Detail',
          'Deficiency Impact Detail',
          null SUBSYSTEM,
          'dw_reg_report_deficiency_impact_detail',
          'already has report time' SPECIAL_NOTE,
          null REPORT_TYPE,
          null TIME_OPTION,
          'Deficiency Impact Detail' REPORT_NUMBER,
          null INPUT_WINDOW,
          null FILTER_OPTION,
          null STATUS,
          101 PP_REPORT_SUBSYSTEM_ID,
          407 REPORT_TYPE_ID,
          null PP_REPORT_TIME_OPTION_ID,
          null PP_REPORT_FILTER_ID,
          1 PP_REPORT_STATUS_ID,
          7 PP_REPORT_ENVIR_ID, /*All Hide environment, so it will only appear in Reg Analysis Wksp*/
          null DOCUMENTATION,
          null USER_COMMENT,
          null LAST_APPROVED_DATE,
          null PP_REPORT_NUMBER,
          null OLD_REPORT_NUMBER,
          null DYNAMIC_DW
     from PP_REPORTS;

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 1, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_rev_req_compare';

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 2, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_rev_req_compare';

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 1, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_deficiency_impact_detail';

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 2, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_deficiency_impact_detail';

insert into REG_ANALYSIS_OPTION
   (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
values
   (7, 'Case and Return Tag', 'Case and Return Tag', 1, 'uo_reg_analysis_op_case_tag');

insert into REG_ANALYSIS_OPTION
   (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
values
   (8, 'Cases and Return Tags', 'Cases and Return Tags', 1, 'uo_reg_analysis_op_cases_tags');

delete from REG_ANALYSIS_DETAIL_OPTION
 where RA_DETAIL_ID in (1, 2)
   and RA_OPTION_ID in (1, 2);

insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (1, 8);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (2, 7);

update REG_ANALYSIS_DETAIL set DESCRIPTION = 'Revenue Requirement - Case to Case' where RA_DETAIL_ID = 1;
update REG_ANALYSIS_DETAIL set DESCRIPTION = 'Revenue Requirement - Year to Year' where RA_DETAIL_ID = 2;
update REG_ANALYSIS_DETAIL set DESCRIPTION = 'Returns - Case to Case' where RA_DETAIL_ID = 3;
update REG_ANALYSIS_DETAIL set DESCRIPTION = 'Returns - Year to Year' where RA_DETAIL_ID = 4;
update REG_ANALYSIS_DETAIL set DESCRIPTION = 'Cost of Capital - Case to Case' where RA_DETAIL_ID = 8;
update REG_ANALYSIS_DETAIL set DESCRIPTION = 'Cost of Capital - Year to Year' where RA_DETAIL_ID = 9;

/* Years Options */
insert into REG_ANALYSIS_OPTION
   (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
values
   (9, 'Years', 'Years', 1, 'uo_reg_analysis_op_years');

update REG_ANALYSIS_DETAIL_OPTION
   set RA_OPTION_ID = 9
 where RA_OPTION_ID = 4
   and RA_DETAIL_ID in (1, 2, 8, 9);

/* Split Expense Cost Trends into Forecast/Historic */
update REG_ANALYSIS_DETAIL
   set DESCRIPTION = 'Expense Cost Trends - Historic',
       LONG_DESCRIPTION = 'Account Trends - Expense Cost Trends - Historic'
 where RA_DETAIL_ID = 7;

insert into REG_ANALYSIS_PRESENTATION
   (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (10, 'Expense Cost Trends - Forecast', 'Account Trends - Expense Cost Trends - Forecast', 1);

insert into REG_ANALYSIS_DETAIL
   (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
values
   (10, 'Expense Cost Trends - Forecast', 'Account Trends - Expense Cost Trends - Forecast', 1, 3, 10);

insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (10, 4);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (10, 5);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (10, 6);

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION,
    REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID,
    PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Expense Cost - Forecast',
          'Expense Cost - Forecast',
          null SUBSYSTEM,
          'dw_reg_report_expense_cost_fcst',
          'already has report time' SPECIAL_NOTE,
          null REPORT_TYPE,
          null TIME_OPTION,
          'Expense Cost - Forecast' REPORT_NUMBER,
          null INPUT_WINDOW,
          null FILTER_OPTION,
          null STATUS,
          101 PP_REPORT_SUBSYSTEM_ID,
          407 REPORT_TYPE_ID,
          null PP_REPORT_TIME_OPTION_ID,
          null PP_REPORT_FILTER_ID,
          1 PP_REPORT_STATUS_ID,
          7 PP_REPORT_ENVIR_ID, /*All Hide environment, so it will only appear in Reg Analysis Wksp*/
          null DOCUMENTATION,
          null USER_COMMENT,
          null LAST_APPROVED_DATE,
          null PP_REPORT_NUMBER,
          null OLD_REPORT_NUMBER,
          null DYNAMIC_DW
     from PP_REPORTS;

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 10, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_expense_cost_fcst';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1202, 0, 10, 4, 2, 6, 38293, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038293_reg_analysis_rev_req.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;