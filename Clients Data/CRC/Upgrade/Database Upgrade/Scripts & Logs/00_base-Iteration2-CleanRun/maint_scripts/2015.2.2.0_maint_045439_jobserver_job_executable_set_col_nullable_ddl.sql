/*
||==========================================================================================
|| Application: PowerPlan
|| File Name:   maint_045439_jobserver_job_executable_set_col_nullable_ddl.sql
||==========================================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2.2.0 05/09/2016 Anand R		     Make server column nullable
||==========================================================================================
*/

SET DEFINE OFF

declare
  l_nullable varchar2(1);
begin
  select nullable into l_nullable
  from user_tab_columns
  where table_name = 'PP_JOB_HISTORY'
  and   column_name = 'SERVER';

  if l_nullable = 'N' then
    execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_HISTORY" MODIFY ( "SERVER" NULL )';
  end if;
end;
/

SET DEFINE ON

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3183, 0, 2015, 2, 2, 0, 45439, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045439_jobserver_job_executable_set_col_nullable_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;