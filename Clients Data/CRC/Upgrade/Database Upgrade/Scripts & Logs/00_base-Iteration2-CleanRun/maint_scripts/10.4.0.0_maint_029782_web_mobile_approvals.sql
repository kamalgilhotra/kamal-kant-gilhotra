SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029782_web_mobile_approvals.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- --------------   ------------------------------------
|| 10.4.0.0   04/21/2013 Paul Cordero     Point Release
||============================================================================
*/

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   PRIMARY_KEY_EXISTS exception;
   pragma exception_init(PRIMARY_KEY_EXISTS, -02260);

begin
      begin
         execute immediate 'create table PP_WEB_CONFIGURATION
                            (
                             COMPANY_ID   number(22,0) default -1 not null,
                             CONFIG_KEY   varchar2(100) not null,
                             CONFIG_VALUE varchar2(4000) not null,
                             CONFIG_TITLE varchar2(100) not null,
                             CONFIG_DESC  varchar2(254),
                             CONFIG_TYPE  varchar2(35) not null,
                             TIME_STAMP   date
                            )';
      exception
         when OBJECT_EXISTS then
            DBMS_OUTPUT.PUT_LINE('Table already exists.');
      end;

   begin
      execute immediate 'alter table PP_WEB_CONFIGURATION
                            add constraint PP_WEB_CONFIGURATION_PK
                                primary key (COMPANY_ID, CONFIG_KEY)
                                using index tablespace PWRPLANT_IDX';
      DBMS_OUTPUT.PUT_LINE('Primary Key created.');
   exception
      when PRIMARY_KEY_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
   end;

   begin
      execute immediate 'create table PP_WEB_AUTHENTICATION
                        (
                          AUTHENTICATION_NAME          varchar2(50) not null,
                          AUTHENTICATION_TYPE          varchar2(15) not null,
                          IS_ACTIVE                    number(1,0) not null,
                          MAX_FAILED_ATTEMPTS          number(2,0) not null,
                          EXPIRE_LOCKOUT_MINUTES       number(6,0) not null,
                          MUST_CHANGE_PASSWORD_DAYS    number(3,0) not null,
                          PASSWORD_TYPE                varchar2(15) not null,
                          PASSWORD_ALLOWED_CHARACTERS  varchar2(256) not null,
                          PASSWORD_CONTAINS_CHARACTERS varchar2(25) not null,
                          PASSWORD_MINIMUM_LENGTH      number(2,0) not null,
                          PASSWORD_MAXIMUM_LENGTH      number(2,0) not null,
                          PASSWORD_REUSE_COUNT         number(2,0) not null,
                          INTERFACE_DOT_NET_TYPE       varchar2(256) null
                         )';
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Table already exists.');
   end;

   begin
      execute immediate 'alter table PP_WEB_AUTHENTICATION
                            add constraint PP_WEB_AUTHENTICATION_PK
                                primary key (AUTHENTICATION_NAME)
                                using index tablespace PWRPLANT_IDX';
      DBMS_OUTPUT.PUT_LINE('Primary Key created.');
   exception
      when PRIMARY_KEY_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
   end;

   begin
      execute immediate 'create table PP_WEB_AUTHENTICATION_USERS
                         (
                          AUTHENTICATION_NAME        varchar2(50) not null,
                          USERS                      varchar2(18) not null,
                          ACCOUNT                    varchar2(100) not null,
                          PASSWORD                   varchar2(4000) null,
                          IS_ACTIVE                  number(1,0) not null,
                          IS_LOCKED_OUT              number(1,0) not null,
                          FAILED_LOGIN_ATTEMPTS      number(2,0) not null,
                          LOCK_EXPIRATION_TIME_STAMP date null,
                          RESET_TOKEN                varchar2(256) null,
                          TIME_STAMP                 date not null
                         )';
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Table already exists.');
   end;

   begin
      execute immediate 'alter table PP_WEB_AUTHENTICATION_USERS
                            add constraint PP_WEB_AUTHENTICATION_USERS_PK
                                primary key (AUTHENTICATION_NAME, USERS)
                                using index tablespace PWRPLANT_IDX';
      DBMS_OUTPUT.PUT_LINE('Primary Key created.');
   exception
      when PRIMARY_KEY_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
   end;

   begin
      execute immediate 'create table PP_WEB_AUTHENTICATION_HISTORY
                         (
                          AUTHENTICATION_NAME varchar2(50) not null,
                          USERS               varchar2(18) not null,
                          TIME_STAMP          date not null,
                          PASSWORD            varchar2(4000)
                         )';
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Table already exists.');
   end;

   begin
      execute immediate 'alter table PP_WEB_AUTHENTICATION_HISTORY
                            add constraint PP_WEB_AUTHENTICATION_HIST_PK
                                   PRIMARY KEY (AUTHENTICATION_NAME, USERS, TIME_STAMP)
                                   using index tablespace PWRPLANT_IDX';
      DBMS_OUTPUT.PUT_LINE('Primary Key created.');
   exception
      when PRIMARY_KEY_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
   end;

   begin
      execute immediate 'create table PP_WEB_AUTHENTICATION_DEVICE
                         (
                          USERS       varchar2(18) not null,
                          DEVICE_NAME varchar2(25) not null,
                          FINGERPRINT varchar2(50) not null,
                          DETAILS     varchar2(250) not null,
                          STATUS      number(1,0) not null,
                          LOGIN       number(1,0) not null
                         )';
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Table already exists.');
   end;

   begin
      execute immediate 'alter table PP_WEB_AUTHENTICATION_DEVICE
                            add constraint PP_WEB_AUTHENTICATION_DEV_PK
                                primary key (USERS, DEVICE_NAME)
                                using index tablespace PWRPLANT_IDX';
      DBMS_OUTPUT.PUT_LINE('Primary Key created.');
   exception
      when PRIMARY_KEY_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
   end;

   begin
      execute immediate 'create table PP_WEB_AUTHENTICATION_DEVICE
                         (
                          USERS       varchar2(18) not null,
                          DEVICE_NAME varchar2(25) not null,
                          FINGERPRINT varchar2(50) not null,
                          DETAILS     varchar2(250) not null,
                          STATUS      number(1,0) not null,
                          LOGIN       number(1,0) not null
                         )';
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Table already exists.');
   end;

   begin
      execute immediate 'alter table PP_WEB_AUTHENTICATION_DEVICE
                            add constraint PP_WEB_AUTHENTICATION_DEV_PK
                                primary key (USERS, DEVICE_NAME)
                                using index tablespace PWRPLANT_IDX';
      DBMS_OUTPUT.PUT_LINE('Primary Key created.');
   exception
      when PRIMARY_KEY_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
   end;
end;
/

insert into PP_WEB_AUTHENTICATION
(
 AUTHENTICATION_NAME, AUTHENTICATION_TYPE, IS_ACTIVE, MAX_FAILED_ATTEMPTS,
 EXPIRE_LOCKOUT_MINUTES, MUST_CHANGE_PASSWORD_DAYS, PASSWORD_TYPE, PASSWORD_ALLOWED_CHARACTERS,
 PASSWORD_CONTAINS_CHARACTERS, PASSWORD_MINIMUM_LENGTH, PASSWORD_MAXIMUM_LENGTH,
 PASSWORD_REUSE_COUNT, INTERFACE_DOT_NET_TYPE)
   select 'EMAIL-PIN', 'TABLE', 1, 5, 60, 0, 'TEXT', '0123456789', 'N:4', 4, 4, 0, ''
     from DUAL
   union all
   select 'DIRECT-DB', 'DATABASE', 1, 5, 60, 0, 'TEXT', ' ', ' ', 0, 0, 0, ''
     from DUAL;

insert into PP_WEB_CONFIGURATION
   (COMPANY_ID, CONFIG_KEY, CONFIG_VALUE, CONFIG_TITLE, CONFIG_DESC, CONFIG_TYPE, TIME_STAMP)
   select X.COMPANY_ID,
          X.CONFIG_KEY,
          X.CONFIG_VALUE,
          X.CONFIG_TITLE,
          X.CONFIG_DESC,
          X.CONFIG_TYPE,
          X.TIME_STAMP
     from (select -1 COMPANY_ID,
                  'DEVICE_AUTHENTICATION_AUTO_APPROVE' CONFIG_KEY,
                  'Device Registration Auto Approve' CONFIG_TITLE,
                  'Whether or not to automatically approve a device when it is registered.' CONFIG_DESC,
                  'MOBILE' CONFIG_TYPE,
                  sysdate TIME_STAMP,
                  'YES' CONFIG_VALUE
             from DUAL
           union all
           select -1 COMPANY_ID,
                  'DEVICE_AUTHENTICATION_REGISTER_EMAIL' CONFIG_KEY,
                  'Device Registration Email ' CONFIG_TITLE,
                  'The email address to send a notification to when a device is registered.' CONFIG_DESC,
                  'MOBILE' CONFIG_TYPE,
                  sysdate TIME_STAMP,
                  ' ' CONFIG_VALUE
             from DUAL
           union all
           select -1 COMPANY_ID,
                  'MOBILE_CONFIRM_RESET_PIN_EMAIL_TEXT' CONFIG_KEY,
                  'Reset Confirmation Email Text' CONFIG_TITLE,
                  'Email text sent to users for reset confimation.' CONFIG_DESC,
                  'MOBILE' CONFIG_TYPE,
                  sysdate TIME_STAMP,
                  'Your pin number has been reset. If you didn''t reset your pin please' || CHR(13) ||
                  CHR(10) || 'click the following link.' || CHR(13) || CHR(10) || '{TOKEN_LINK}' CONFIG_VALUE
             from DUAL
           union all
           select -1 COMPANY_ID,
                  'MOBILE_RESET_PIN_EMAIL_TEXT' CONFIG_KEY,
                  'Reset Pin Email Text' CONFIG_TITLE,
                  'Email text sent to users for reset pin request' CONFIG_DESC,
                  'MOBILE' CONFIG_TYPE,
                  sysdate TIME_STAMP,
                  'This is an automated email.' || CHR(13) || CHR(10) || CHR(13) || CHR(10) ||
                  'You requested a pin reset.' || CHR(13) || CHR(10) ||
                  'Click the following link to reset your pin.' || CHR(13) || CHR(10) ||
                  '{TOKEN_LINK}' || CHR(13) || CHR(10) || CHR(13) || CHR(10) ||
                  'If you didn''t make this request,' || CHR(13) || CHR(10) ||
                  'click the following link.' || CHR(13) || CHR(10) || '{TOKEN_LINK2}' CONFIG_VALUE
             from DUAL
           union all
           select -1 COMPANY_ID,
                  'WEB_SERVER_URL' CONFIG_KEY,
                  'Web Server URL' CONFIG_TITLE,
                  'URL of Web Server Installation. May or may not be on the same server.' CONFIG_DESC,
                  'ADMIN' CONFIG_TYPE,
                  sysdate TIME_STAMP,
                  'http://0.0.0.0/MobileApprovals/' CONFIG_VALUE
             from DUAL) X
     left join PP_WEB_CONFIGURATION C
       on C.COMPANY_ID = X.COMPANY_ID
      and C.CONFIG_KEY = X.CONFIG_KEY
    where C.CONFIG_KEY is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (352, 0, 10, 4, 0, 0, 29782, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029782_web_mobile_approvals.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
