/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008025_plant.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/28/2011 Lee Quinn      Point Release
||============================================================================
*/

-- Maint 8025

-- Drop the foreign key to company id from clearing_wo_control
begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME = 'CLEARING_WO_CONTROL'
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'COMPANY_SETUP'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
   end loop;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (22, 0, 10, 3, 3, 0, 8025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008025_plant.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
