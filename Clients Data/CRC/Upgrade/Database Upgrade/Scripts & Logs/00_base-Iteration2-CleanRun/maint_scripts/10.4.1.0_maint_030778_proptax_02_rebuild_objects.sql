SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030778_proptax_02_rebuild_objects.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/27/2013 Lee Quinn      Point release
|| 10.4.1.0   09/25/2013 Lee Quinn      Added more skip script inserts
||============================================================================
*/

-- THIS SCRIPT WILL DROP AND RECREATE THE PROPERTY TAX MODULE.
-- THIS SHOULD NOT RUN IF THE PROPERTY TAX MODULE IS BEING USED.

declare
   TABLE_NOT_EXISTS exception;
   pragma exception_init(TABLE_NOT_EXISTS, -00942);

   V_COUNT number;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   begin
      execute immediate 'select count(*) from PROPERTY_TAX_YEAR'
         into V_COUNT;
      if V_COUNT > 0 then
         DBMS_OUTPUT.PUT_LINE('Property Tax is being used.  Do not run this script.');
         RAISE_APPLICATION_ERROR(-20000,
                                 'The Property Tax Module has been implemented. This script should not be run.');
      else
         DBMS_OUTPUT.PUT_LINE('The Property Tax module is NOT being used and will be dropped and recreated.');
      end if;

   exception
      when TABLE_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('PROPERTY_TAX_YEAR table does not exist.');
         DBMS_OUTPUT.PUT_LINE('The Property Tax module is NOT being used and will be dropped and recreated.');
   end;

   -- Check to see if Property Tax is being used
   ----------------------------------------------------
   -- DROP ALL PROPERTY TAX TABLES
   ----------------------------------------------------
   for CSR_CREATE_SQL in (select 'drop public synonym "' || TABLE_NAME || '"' DROP_SYNONYM,
                                 'drop table PWRPLANT."' || TABLE_NAME || '" cascade constraints' DROP_TABLE,
                                 TABLE_NAME
                            from ALL_TABLES
                           where OWNER = 'PWRPLANT'
                             and TABLE_NAME in ('ADJUSTMENT_LEDGER',
                                                'ASSESSED_ALLO_FACTOR_PERCENTS',
                                                'ASSESSED_ALLO_FACTORS',
                                                'ASSESSED_VALUE',
                                                'LEVY_CLASS',
                                                'PLT_ROLLUP',
                                                'PREALLO_ADJUSTMENT_LEDGER',
                                                'PROP_TAX_ALLO_DEFINITION',
                                                'PROP_TAX_ALLOCATIONS',
                                                'PROP_TAX_CALC_CASE',
                                                'PROP_TAX_CONTROL',
                                                'PROP_TAX_CWIP_BALANCE',
                                                'PROP_TAX_INTERFACE',
                                                'PROP_TAX_INTERFACE_DATES',
                                                'PROP_TAX_NET_TAX_RESERVE',
                                                'PROP_TAX_NET_TAX_RESERVE_REP',
                                                'PROP_TAX_PREALLO_PULL',
                                                'PROP_TAX_TAX_RESERVE_PERCENT',
                                                'PROP_TAX_TYPE_ASSIGN_TREE',
                                                'PROP_TAX_TYPE_ASSIGN_TREE_CWIP',
                                                'PROP_TAX_TYPE_AUDIT_CONTROL',
                                                'PROP_TAX_TYPE_CPR_AUDIT',
                                                'PROP_TAX_TYPE_CWIP_AUDIT',
                                                'PROP_TAX_TYPE_FIFTH_LEVEL',
                                                'PROP_TAX_TYPE_FIFTH_LEVEL_CWIP',
                                                'PROP_TAX_TYPE_PROP_TAX_ADJUST',
                                                'PROPERTY_TAX_ACCRUAL',
                                                'PROPERTY_TAX_ADJUST',
                                                'PROPERTY_TAX_ADJUST_TYPE',
                                                'PROPERTY_TAX_AUTHORITY',
                                                'PROPERTY_TAX_AUTHORITY_HISTORY',
                                                'PROPERTY_TAX_AUTHORITY_TYPE',
                                                'PROPERTY_TAX_LEDGER_CONTROL',
                                                'PROPERTY_TAX_LEDGER_NEW',
                                                'PROPERTY_TAX_LEVY',
                                                'PROPERTY_TAX_RATES',
                                                'PROPERTY_TAX_STATEMENT',
                                                'PROPERTY_TAX_STATEMENT_AUTH',
                                                'PROPERTY_TAX_TYPE_ASSIGN',
                                                'PROPERTY_TAX_TYPE_DATA',
                                                'PROPERTY_TAX_TYPE_ROLL2',
                                                'PROPERTY_TAX_YEAR',
                                                'PROPERTY_TAX_YEAR_LOCK',
                                                'PT_ACCRUAL_ALLOWED_AMT',
                                                'PT_ACCRUAL_ASSIGN_TREE',
                                                'PT_ACCRUAL_ASSIGN_TREE_ASSIGN',
                                                'PT_ACCRUAL_ASSIGN_TREE_LEVELS',
                                                'PT_ACCRUAL_CALC',
                                                'PT_ACCRUAL_CHARGE',
                                                'PT_ACCRUAL_CONTROL',
                                                'PT_ACCRUAL_INDIV_ASSESSED_CALC',
                                                'PT_ACCRUAL_MERGE',
                                                'PT_ACCRUAL_MERGE_SAVE',
                                                'PT_ACCRUAL_PARCEL_CALC',
                                                'PT_ACCRUAL_PENDING',
                                                'PT_ACCRUAL_STATUS',
                                                'PT_ACCRUAL_TYPE',
                                                'PT_ACCRUAL_YEAR_CONTROL',
                                                'PT_ACTUALS',
                                                'PT_ACTUALS_STATUS',
                                                'PT_ACTUALS_SUMMARY',
                                                'PT_ADJUSTMENT_LEDGER',
                                                'PT_ADJUSTMENT_TRANSFER_HISTORY',
                                                'PT_ALLOCATE_ASSESSMENT_TYPE',
                                                'PT_ALLOCATION_METHOD',
                                                'PT_ALLOCATION_TYPE',
                                                'PT_APPRAISER',
                                                'PT_ASSESSED_VALUE_ALLOCATION',
                                                'PT_ASSESSED_VALUE_COUNTY',
                                                'PT_ASSESSED_VALUE_TAX_DISTRICT',
                                                'PT_ASSESSMENT_GROUP',
                                                'PT_ASSESSMENT_GROUP_CASE',
                                                'PT_ASSESSMENT_GROUP_TYPES',
                                                'PT_ASSESSMENT_YEAR',
                                                'PT_ASSESSOR',
                                                'PT_ASSET_LOCATION_SNAPSHOT',
                                                'PT_AUTHORITY_RATE_TYPE',
                                                'PT_CASE',
                                                'PT_CASE_CALC',
                                                'PT_CASE_CALC_AUTHORITY',
                                                'PT_CASE_CONTROL',
                                                'PT_CASE_TYPE',
                                                'PT_CLASS_CODE_VALUE_CONTROL',
                                                'PT_COLUMN_HELP',
                                                'PT_COMPANY',
                                                'PT_COMPANY_OVERRIDE',
                                                'PT_COMPANY_TAX_TYPE',
                                                'PT_CONTROL',
                                                'PT_COUNTY_EQUALIZATION',
                                                'PT_COUNTY_EQUILIZATION',
                                                'PT_CPR_PULL_TEMP',
                                                'PT_DATE',
                                                'PT_DATE_FIELD',
                                                'PT_DATE_GENERATOR',
                                                'PT_DATE_LINK',
                                                'PT_DATE_TYPE',
                                                'PT_DATE_TYPE_FIELD',
                                                'PT_DATE_TYPE_FILTER',
                                                'PT_DATE_TYPE_PERSON',
                                                'PT_DATE_WEEKEND_METHOD',
                                                'PT_DEED',
                                                'PT_DEED_HIERARCHY',
                                                'PT_DEPR_FLOOR_AUTOADJ',
                                                'PT_DEPR_FLOOR_AUTOADJ_TYPES',
                                                'PT_DISTRICT_EQUALIZATION',
                                                'PT_DISTRICT_SNAPSHOT',
                                                'PT_EQUALIZATION_FACTOR_TYPE',
                                                'PT_ESCALATED_VALUE_INDEX',
                                                'PT_ESCALATED_VALUE_TYPE',
                                                'PT_IMPORT_ASSESSOR',
                                                'PT_IMPORT_ASSESSOR_ARCHIVE',
                                                'PT_IMPORT_ASSET_LOC',
                                                'PT_IMPORT_ASSET_LOC_ARCHIVE',
                                                'PT_IMPORT_ASSETS',
                                                'PT_IMPORT_ASSETS_ARCHIVE',
                                                'PT_IMPORT_AUTH_DIST',
                                                'PT_IMPORT_AUTH_DIST_ARCHIVE',
                                                'PT_IMPORT_CLASS_CODE',
                                                'PT_IMPORT_CLASS_CODE_ARCHIVE',
                                                'PT_IMPORT_COLUMN',
                                                'PT_IMPORT_COLUMN_LOOKUP',
                                                'PT_IMPORT_CWIP_ASSIGN',
                                                'PT_IMPORT_CWIP_ASSIGN_ARCHIVE',
                                                'PT_IMPORT_DIVISION',
                                                'PT_IMPORT_DIVISION_ARCHIVE',
                                                'PT_IMPORT_ESCVAL_INDEX',
                                                'PT_IMPORT_ESCVAL_INDEX_ARCHIVE',
                                                'PT_IMPORT_LEDGER',
                                                'PT_IMPORT_LEDGER_ADJ_ARCHIVE',
                                                'PT_IMPORT_LEDGER_ARCHIVE',
                                                'PT_IMPORT_LEVY_RATES',
                                                'PT_IMPORT_LEVY_RATES_ARCHIVE',
                                                'PT_IMPORT_LOC_MASTER',
                                                'PT_IMPORT_LOC_MASTER_ARCHIVE',
                                                'PT_IMPORT_LOC_TYPE',
                                                'PT_IMPORT_LOC_TYPE_ARCHIVE',
                                                'PT_IMPORT_LOOKUP',
                                                'PT_IMPORT_MAJOR_LOC',
                                                'PT_IMPORT_MAJOR_LOC_ARCHIVE',
                                                'PT_IMPORT_MINOR_LOC',
                                                'PT_IMPORT_MINOR_LOC_ARCHIVE',
                                                'PT_IMPORT_MUNICIPALITY',
                                                'PT_IMPORT_MUNICIPALITY_ARCHIVE',
                                                'PT_IMPORT_PARCEL',
                                                'PT_IMPORT_PARCEL_ARCHIVE',
                                                'PT_IMPORT_PRCL_ASMT',
                                                'PT_IMPORT_PRCL_ASMT_ARCHIVE',
                                                'PT_IMPORT_PRCL_GEO',
                                                'PT_IMPORT_PRCL_GEO_ARCHIVE',
                                                'PT_IMPORT_PRCL_HISTORY',
                                                'PT_IMPORT_PRCL_HISTORY_ARCHIVE',
                                                'PT_IMPORT_PRCL_LOC',
                                                'PT_IMPORT_PRCL_LOC_ARCHIVE',
                                                'PT_IMPORT_PRCL_RSP',
                                                'PT_IMPORT_PRCL_RSP_ARCHIVE',
                                                'PT_IMPORT_PRCL_RSP_ENT',
                                                'PT_IMPORT_PRCL_RSP_ENT_ARCHIVE',
                                                'PT_IMPORT_PREALLO',
                                                'PT_IMPORT_PREALLO_ADJ_ARCHIVE',
                                                'PT_IMPORT_PREALLO_ARCHIVE',
                                                'PT_IMPORT_PROPTAX_LOC',
                                                'PT_IMPORT_PROPTAX_LOC_ARCHIVE',
                                                'PT_IMPORT_RETIRE_UNIT',
                                                'PT_IMPORT_RETIRE_UNIT_ARCHIVE',
                                                'PT_IMPORT_RSVFCTR_PCTS',
                                                'PT_IMPORT_RSVFCTR_PCTS_ARCHIVE',
                                                'PT_IMPORT_RUN',
                                                'PT_IMPORT_STATEMENT',
                                                'PT_IMPORT_STATEMENT_ARCHIVE',
                                                'PT_IMPORT_STATS_FULL',
                                                'PT_IMPORT_STATS_FULL_ARCHIVE',
                                                'PT_IMPORT_STATS_INCR',
                                                'PT_IMPORT_STATS_INCR_ARCHIVE',
                                                'PT_IMPORT_TAX_DIST',
                                                'PT_IMPORT_TAX_DIST_ARCHIVE',
                                                'PT_IMPORT_TEMPLATE',
                                                'PT_IMPORT_TEMPLATE_EDITS',
                                                'PT_IMPORT_TEMPLATE_FIELDS',
                                                'PT_IMPORT_TYPE',
                                                'PT_IMPORT_TYPE_CODE',
                                                'PT_IMPORT_TYPE_CODE_ARCHIVE',
                                                'PT_IMPORT_TYPE_UPDATES_LOOKUP',
                                                'PT_IMPORT_UTIL_ACCOUNT',
                                                'PT_IMPORT_UTIL_ACCOUNT_ARCHIVE',
                                                'PT_INCREMENTAL_BASE_YEAR',
                                                'PT_INCREMENTAL_INIT_ADJ',
                                                'PT_INCREMENTAL_INITIALIZATION',
                                                'PT_INCREMENTAL_STATISTICS',
                                                'PT_INDIVIDUAL_ASSESSED_DETAIL',
                                                'PT_INDIVIDUAL_ASSESSMENT',
                                                'PT_INSTALLMENT_STATUS',
                                                'PT_INSTALLMENT_TYPE',
                                                'PT_INTEREST_TYPE',
                                                'PT_INTERFACE',
                                                'PT_INTERFACE_TAX_YEAR',
                                                'PT_LEDGER',
                                                'PT_LEDGER_ADJUSTMENT',
                                                'PT_LEDGER_DETAIL',
                                                'PT_LEDGER_DETAIL_COLUMN',
                                                'PT_LEDGER_DETAIL_MAP',
                                                'PT_LEDGER_DETAIL_MAP_FIELDS',
                                                'PT_LEDGER_DETAIL_SOURCE',
                                                'PT_LEDGER_TAX_YEAR',
                                                'PT_LEDGER_TRANSFER',
                                                'PT_LEVY_CLASS',
                                                'PT_LEVY_RATE',
                                                'PT_LOCATION_ROLLUP',
                                                'PT_LOCATION_SNAPSHOT',
                                                'PT_MARKET_VALUE_RATE',
                                                'PT_MARKET_VALUE_RATE_COUNTY',
                                                'PT_METHOD',
                                                'PT_MONTHLY_ACCRUAL',
                                                'PT_MONTHLY_ADMIN',
                                                'PT_NEG_BAL_AUTOTRANS',
                                                'PT_PARCEL',
                                                'PT_PARCEL_APPEAL',
                                                'PT_PARCEL_APPEAL_EVENT',
                                                'PT_PARCEL_APPEAL_TYPE',
                                                'PT_PARCEL_APPRAISAL',
                                                'PT_PARCEL_ASSESSMENT',
                                                'PT_PARCEL_ASSESSMENT_COLUMN',
                                                'PT_PARCEL_ASSET',
                                                'PT_PARCEL_AUTO_ADJUST',
                                                'PT_PARCEL_FLEX_FIELD_CONTROL',
                                                'PT_PARCEL_FLEX_FIELD_USAGE',
                                                'PT_PARCEL_FLEX_FIELD_VALUES',
                                                'PT_PARCEL_FLEX_FIELDS',
                                                'PT_PARCEL_GEOGRAPHY',
                                                'PT_PARCEL_GEOGRAPHY_TYPE',
                                                'PT_PARCEL_GEOGRAPHY_TYPE_STATE',
                                                'PT_PARCEL_HISTORY',
                                                'PT_PARCEL_LOCATION',
                                                'PT_PARCEL_RESPONSIBILITY',
                                                'PT_PARCEL_RESPONSIBLE_ENTITY',
                                                'PT_PARCEL_TAX_YEAR',
                                                'PT_PARCEL_TYPE',
                                                'PT_PAYMENT_STATUS',
                                                'PT_PERIOD_TYPE',
                                                'PT_POWERTAX_COMPANY',
                                                'PT_PREALLO_ADJ_TRANS_HISTORY',
                                                'PT_PREALLO_ADJUSTMENT',
                                                'PT_PREALLO_ADJUSTMENT_LEDGER',
                                                'PT_PREALLO_LEDGER',
                                                'PT_PREALLO_TRANSFER',
                                                'PT_PROCESS',
                                                'PT_PROCESS_COMPANY_STATE',
                                                'PT_PROCESS_COMPANY_STATE_TY',
                                                'PT_PROCESS_DEPENDENCY',
                                                'PT_PROCESS_TYPE',
                                                'PT_QUANTITY_CONVERSION',
                                                'PT_QUERY_COL_SAVE',
                                                'PT_QUERY_COLUMNS',
                                                'PT_QUERY_FILTER_SAVE',
                                                'PT_QUERY_MAIN_SAVE',
                                                'PT_QUERY_TYPE',
                                                'PT_RATE_DEFINITION',
                                                'PT_RATE_TYPE',
                                                'PT_RATES',
                                                'PT_REPORT_COMPANY_FIELD',
                                                'PT_REPORT_COUNTY_FIELD',
                                                'PT_REPORT_FIELD_LABELS',
                                                'PT_REPORT_LOCATION_FIELD',
                                                'PT_REPORT_PARCEL_FIELD',
                                                'PT_REPORT_PT_COMPANY_FIELD',
                                                'PT_REPORT_STATE_FIELD',
                                                'PT_REPORT_TAX_DISTRICT_FIELD',
                                                'PT_REPORT_TYPE_CODE_FIELD',
                                                'PT_RESERVE_FACTOR_COUNTY_PCTS',
                                                'PT_RESERVE_FACTOR_DIST_PCTS',
                                                'PT_RESERVE_FACTOR_PERCENTS',
                                                'PT_RESERVE_FACTOR_TYPE',
                                                'PT_RESERVE_FACTORS',
                                                'PT_RESERVE_METHOD',
                                                'PT_SCHEDULE',
                                                'PT_SCHEDULE_INSTALLMENT',
                                                'PT_SCHEDULE_PERIOD',
                                                'PT_SCHEDULE_TEMPLATE',
                                                'PT_SCHEDULE_TEMPLATE_INSTALL',
                                                'PT_SCHEDULE_TEMPLATE_PERIOD',
                                                'PT_SGROUP_ROLLUP',
                                                'PT_SGROUP_ROLLUP_ASSIGN',
                                                'PT_SGROUP_ROLLUP_VALUES',
                                                'PT_SNAPSHOT_CONTROL',
                                                'PT_STATE_PAYMENT_INSTALLMENT',
                                                'PT_STATE_PAYMENT_PERIOD',
                                                'PT_STATE_PAYMENT_SCHEDULE',
                                                'PT_STATEMENT',
                                                'PT_STATEMENT_APPROVAL',
                                                'PT_STATEMENT_APPROVAL_LEVEL',
                                                'PT_STATEMENT_ASSESSMENT_YEAR',
                                                'PT_STATEMENT_GROUP',
                                                'PT_STATEMENT_GROUP_INSTALLMENT',
                                                'PT_STATEMENT_GROUP_PAYMENT',
                                                'PT_STATEMENT_GROUP_PERIOD',
                                                'PT_STATEMENT_GROUP_SCHEDULE',
                                                'PT_STATEMENT_INDIV_ASSESSED',
                                                'PT_STATEMENT_INSTALLMENT',
                                                'PT_STATEMENT_LINE',
                                                'PT_STATEMENT_LINE_PAYMENT',
                                                'PT_STATEMENT_PARCEL',
                                                'PT_STATEMENT_PAYEE',
                                                'PT_STATEMENT_PAYMENT_APPROVAL',
                                                'PT_STATEMENT_PERIOD',
                                                'PT_STATEMENT_SCHEDULE',
                                                'PT_STATEMENT_STATE_SCHEDULE',
                                                'PT_STATEMENT_STATEMENT_YEAR',
                                                'PT_STATEMENT_STATUS',
                                                'PT_STATEMENT_TAX_AUTHORITY',
                                                'PT_STATEMENT_TAX_DISTRICT',
                                                'PT_STATEMENT_TAX_TYPE',
                                                'PT_STATEMENT_TAX_YEAR',
                                                'PT_STATEMENT_VERIFIED_STATUS',
                                                'PT_STATEMENT_YEAR',
                                                'PT_STATISTICS_AUTOCREATE',
                                                'PT_STATISTICS_FULL',
                                                'PT_STATISTICS_INCREMENTAL',
                                                'PT_STATISTICS_SPREAD_RULES',
                                                'PT_TAXABLE_VALUE_RATE',
                                                'PT_TAXABLE_VALUE_RATE_COUNTY',
                                                'PT_TEMP_ACCRUAL_PRIOR_MONTH',
                                                'PT_TEMP_ACTUALS_PYMT_TOTAL',
                                                'PT_TEMP_ACTUALS_TOTAL',
                                                'PT_TEMP_ALLO_INCR_BASE_PCTS',
                                                'PT_TEMP_ALLO_INCR_TOTAL',
                                                'PT_TEMP_ALLO_PERCENTS',
                                                'PT_TEMP_ALLO_SPREAD',
                                                'PT_TEMP_BILLS_RECALC_TERMS',
                                                'PT_TEMP_CASECALC_ROUNDING',
                                                'PT_TEMP_CASECALC_TOTALS',
                                                'PT_TEMP_CPRPULL_BASIS_ADJS',
                                                'PT_TEMP_CPRPULL_DEPRMONTH',
                                                'PT_TEMP_CPRPULL_LEDGERDETAIL',
                                                'PT_TEMP_IDS',
                                                'PT_TEMP_IDS2',
                                                'PT_TEMP_IDS3',
                                                'PT_TEMP_IDS4',
                                                'PT_TEMP_LEDGER_ADDS',
                                                'PT_TEMP_LEDGER_ADJS',
                                                'PT_TEMP_LEDGER_TRANSFERS',
                                                'PT_TEMP_PARCELS',
                                                'PT_TEMP_PARCELS_ASMT_GP',
                                                'PT_TEMP_PREALLO_TRANSFERS',
                                                'PT_TEMP_STATEMENT_LINES',
                                                'PT_TYPE_ASSIGN_CWIP_PSEUDO',
                                                'PT_TYPE_CWIP_PSEUDO_ASSIGN',
                                                'PT_TYPE_CWIP_PSEUDO_LEVELS',
                                                'PT_TYPE_NATIONAL_MAP',
                                                'PT_TYPE_ROLLUP',
                                                'PT_TYPE_ROLLUP_ASSIGN',
                                                'PT_TYPE_ROLLUP_VALUES',
                                                'PT_UA_ROLLUP',
                                                'PT_UA_ROLLUP_ASSIGN',
                                                'PT_UNIT_COST',
                                                'PT_UNIT_COST_AMOUNT',
                                                'PT_USER_INPUT_LEDGER',
                                                'PT_VINTAGE_OPTION',
                                                'PT_YEAR_OVERRIDE',
                                                'PTC_DOCUMENTS',
                                                'PTC_DOCUMENTS_TYPE',
                                                'PTC_REPORT_PACKAGE',
                                                'PTC_REPORT_PACKAGE_REPORTS',
                                                'PTC_SCRIPT_LOG',
                                                'PTC_SYSTEM_OPTIONS',
                                                'PTC_SYSTEM_OPTIONS_CENTERS',
                                                'PTC_SYSTEM_OPTIONS_VALUES',
                                                'PTC_USER_OPTIONS',
                                                'TAX_AUTH_PROP_TAX_DIST',
                                                'PT_ACCRUAL_ASSIGN_TREE_V10217',
                                                'PT_ESC_VAL_INDEX_V1032',
                                                'PT_IMPORT_EV_IDX_ARC_V1032',
                                                'PT_IMPORT_EV_IDX_V1032',
                                                'PT_IMPORT_STMT',
                                                'PT_IMPORT_STMT_ARCHIVE',
                                                'PT_IMPORT_STMT_LINE',
                                                'PT_IMPORT_STMT_LINE_ARCHIVE',
                                                'PT_PARCEL_RESPONSIBILITY_TYPE',
                                                'PT_RSV_FCTR_COUNTY_PCTS_V1032',
                                                'PT_RSV_FCTR_DIST_PCTS_V1032',
                                                'PT_RSV_FCTR_PCTS_V1032',
                                                'PT_TEMP_ACTUALS_ASMTS',
                                                'PT_TEMP_ACTUALS_LOAD_PCTS',
                                                'PT_TEMP_LEDGER_ADJUSTMENT',
                                                'PT_TEMP_LEDGER_TAX_YEAR')
                           order by TABLE_NAME)
   loop
      -- Drop SYNONYM
      begin
         execute immediate CSR_CREATE_SQL.DROP_SYNONYM;
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.TABLE_NAME || ' synonym dropped');
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.TABLE_NAME || ' synonym did not exist');
      end;
      -- Drop TABLE
      begin
         execute immediate CSR_CREATE_SQL.DROP_TABLE;
         DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.TABLE_NAME || ' table dropped');
      end;
   end loop;

   -- Drop SEQUENCES
   for CSR_CREATE_SQL in (select 'drop sequence "' || SEQUENCE_NAME || '"' DROP_SEQUENCE,
                                 SEQUENCE_NAME
                            from ALL_SEQUENCES
                           where SEQUENCE_OWNER = 'PWRPLANT'
                             and SEQUENCE_NAME in ('PT_PREALLO_SEQ',
                                                   'PT_LEDGER_SEQ',
                                                   'PT_PAYMENT_SEQ',
                                                   'PT_BILLS_SEQ',
                                                   'PT_PARCEL_SEQ',
                                                   'PT_LEDGER_DETAIL_SEQ',
                                                   'PT_PROCESS_OCCURRENCE_SEQ',
                                                   'PT_TABLES_SEQ')
                           order by SEQUENCE_NAME)
   loop
      execute immediate CSR_CREATE_SQL.DROP_SEQUENCE;
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.SEQUENCE_NAME || ' sequence dropped');
   end loop;

/*
-- Generate insert statements to skip old PT Updates
select 'insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select ' || ID || ',1,' || MAJOR_VERSION || ',' || MINOR_VERSION || ',' || POINT_VERSION || ',' ||
       PATCH_VERSION || ',' || MAINT_NUM || ',''' || SCRIPT_PATH || ''',''' || SCRIPT_NAME || ''',' ||
       SCRIPT_REVISION ||
       ', SYSTIMESTAMP, SYS_CONTEXT(''USERENV'', ''OS_USER''), SYS_CONTEXT(''USERENV'', ''TERMINAL''),
     SYS_CONTEXT(''USERENV'', ''SERVICE_NAME'')
     from dual where not exists (select 1 from pp_schema_change_log where id = ' || ID || ');'
  from PP_SCHEMA_CHANGE_LOG
 where SCRIPT_NAME like '%proptax.sql%'
   and MAJOR_VERSION || MINOR_VERSION || POINT_VERSION || PATCH_VERSION <> 10410
 order by MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION;
*/
   -- Insert records into PP_SCHEMA_CHANGE_LOG to keep from rerunning Pre V10.4.1 PropTax scripts
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 36,
          1,
          10,
          3,
          3,
          0,
          8064,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_008064_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 36);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 99,
          1,
          10,
          3,
          3,
          0,
          0,
          'C:\PlasticWks\powerplant\sql',
          'version-updates_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 99);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 83,
          1,
          10,
          3,
          3,
          1,
          0,
          'C:\PlasticWks\powerplant\sql',
          'version-updates_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 83);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 101,
          1,
          10,
          3,
          4,
          0,
          9366,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009366_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 101);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 100,
          1,
          10,
          3,
          4,
          0,
          9371,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009371_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 100);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 90,
          1,
          10,
          3,
          4,
          0,
          9115,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009115_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 90);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 124,
          1,
          10,
          3,
          4,
          0,
          10134,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010134_version-updates_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 124);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 104,
          1,
          10,
          3,
          4,
          0,
          9577,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009577_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 104);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 105,
          1,
          10,
          3,
          4,
          0,
          9578,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009578_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 105);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 106,
          1,
          10,
          3,
          4,
          0,
          9534,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009534_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 106);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 61,
          1,
          10,
          3,
          4,
          0,
          8850,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_008850_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 61);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 211,
          1,
          10,
          3,
          5,
          0,
          10704,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010704_version-updates_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 211);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 205,
          1,
          10,
          3,
          5,
          0,
          10757,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010757_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 205);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 191,
          1,
          10,
          3,
          5,
          0,
          10015,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010015_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 191);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 190,
          1,
          10,
          3,
          5,
          0,
          10016,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010016_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 190);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 188,
          1,
          10,
          3,
          5,
          0,
          10019,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010019_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 188);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 183,
          1,
          10,
          3,
          5,
          0,
          9517,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009517_009518_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 183);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 184,
          1,
          10,
          3,
          5,
          0,
          10012,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010012_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 184);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 178,
          1,
          10,
          3,
          5,
          0,
          9565,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009565_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 178);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 164,
          1,
          10,
          3,
          5,
          0,
          10463,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010463_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 164);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 163,
          1,
          10,
          3,
          5,
          0,
          6335,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_006335_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 163);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 151,
          1,
          10,
          3,
          5,
          0,
          9559,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009559_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 151);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 135,
          1,
          10,
          3,
          5,
          0,
          9657,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009657_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 135);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 134,
          1,
          10,
          3,
          5,
          0,
          9703,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009703_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 134);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 130,
          1,
          10,
          3,
          5,
          0,
          10018,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010018_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 130);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 136,
          1,
          10,
          3,
          5,
          0,
          7236,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_007236_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 136);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 139,
          1,
          10,
          3,
          5,
          0,
          7275,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_007275_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 139);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 144,
          1,
          10,
          3,
          5,
          0,
          10013,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010013_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 144);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 145,
          1,
          10,
          3,
          5,
          0,
          7726,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_007726_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 145);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 150,
          1,
          10,
          3,
          5,
          0,
          9562,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009562_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 150);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 147,
          1,
          10,
          3,
          5,
          0,
          10455,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010455_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 147);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 247,
          1,
          10,
          4,
          0,
          0,
          9751,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009751_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 247);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 246,
          1,
          10,
          4,
          0,
          0,
          11042,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_011042_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 246);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 256,
          1,
          10,
          4,
          0,
          0,
          11573,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_011573_version-updates_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 256);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 248,
          1,
          10,
          4,
          0,
          0,
          11149,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_011149_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 248);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 317,
          1,
          10,
          4,
          0,
          0,
          29467,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_029467_proptax.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 317);
-- Added 09252013
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 20,
          1,
          10,
          3,
          3,
          0,
          7263,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_007263_pt_net_tax_res.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 20);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 49,
          1,
          10,
          3,
          3,
          0,
          8390,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_008390_pt_reports_att.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 49);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 62,
          1,
          10,
          3,
          4,
          0,
          8850,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_008850_proptax_script2.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 62);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 110,
          1,
          10,
          3,
          4,
          0,
          9534,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_009534_proptax_2.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 110);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 206,
          1,
          10,
          3,
          5,
          0,
          10016,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_010016_proptax_rev1.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 206);
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
   select 267,
          1,
          10,
          4,
          0,
          0,
          11085,
          'C:\PlasticWks\powerplant\sql\maint_scripts',
          'maint_011085_proptax_updates.sql',
          1,
          SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'),
          SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME')
     from DUAL
    where not exists (select 1 from PP_SCHEMA_CHANGE_LOG where ID = 267);

commit;

end;
/

----------------------------------------------------
-- CREATE ALL PROPERTY TAX TABLES
----------------------------------------------------

-- pt_company
create table pwrplant.pt_company
   (  prop_tax_company_id  number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      legal_name              varchar2(254),
      address_line1           varchar2(254),
      address_line2           varchar2(254),
      address_line3           varchar2(254),
      city                    varchar2(35),
      state                   varchar2(35),
      zip_code                varchar2(35),
      federal_ein             varchar2(35),
      approval_type_id        number(22,0),
      gl_company_no        varchar2(35),
      notes                   varchar2(4000)
   );

alter table pwrplant.pt_company add ( constraint pt_company_pk primary key ( prop_tax_company_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_company add ( constraint pt_company_app_fk foreign key ( approval_type_id ) references pwrplant.approval );



-- pt_assessment_year
create table pwrplant.pt_assessment_year
   (  assessment_year_id   number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      actual_year          number(22,0)   not null
   );

alter table pwrplant.pt_assessment_year add ( constraint pt_assessment_year_pk primary key ( assessment_year_id ) using index tablespace pwrplant_idx );



-- property_tax_year
create table pwrplant.property_tax_year
   (  tax_year             number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      assessment_year_id   number(22,0)   not null,
      year                 number(22,0)   not null,
      valuation_year       number(22,0)   not null,
      start_month          number(22,0),
      prior_tax_year       number(22,0)
   );

alter table pwrplant.property_tax_year add ( constraint property_tax_year_pk primary key ( tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.property_tax_year add ( constraint pt_year_fk_assmt_yr foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );
alter table pwrplant.property_tax_year add ( constraint pt_year_prior_yr_fk foreign key ( prior_tax_year ) references pwrplant.property_tax_year ( tax_year ) );



-- pt_case_type
create table pwrplant.pt_case_type
   (  case_type_id         number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_case_type add ( constraint pt_case_type_pk primary key ( case_type_id ) using index tablespace pwrplant_idx );



-- pt_case
create table pwrplant.pt_case
   (  case_id              number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)  not null,
      tax_year             number(22,0)   not null,
      case_type_id         number(22,0)   not null,
      creation_date        date           not null,
      parent_case_id       number(22,0),
      filter_string           varchar2(2000),
      locked               number(22,0)   default 0 not null
   );

alter table pwrplant.pt_case add ( constraint pt_case_pk primary key ( case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_case add ( constraint pt_case_fk_ty foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_case add ( constraint pt_case_fk_parent foreign key ( parent_case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_case add ( constraint pt_case_fk_case_type foreign key ( case_type_id ) references pwrplant.pt_case_type );



-- pt_statement_payee
create table pwrplant.pt_statement_payee
   (  statement_payee_id      number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      name_line_1          varchar2(50),
      name_line_2          varchar2(50),
      name_line_3          varchar2(50),
      address_line_1          varchar2(50),
      address_line_2          varchar2(50),
      address_line_3          varchar2(50),
      city                    varchar2(35),
      state                   varchar2(35),
      zip_code                varchar2(35),
      phone_1                 varchar2(35),
      phone_2                 varchar2(35),
      ext_vendor_code         varchar2(35),
      ext_vendor_code2     varchar2(35),
      fax_1                   varchar2(35),
      fax_2                   varchar2(35),
      electronic_payment      varchar2(35),
      notes                   varchar2(254)
   );

alter table pwrplant.pt_statement_payee add ( constraint pt_statement_payee_pk primary key ( statement_payee_id ) using index tablespace pwrplant_idx );



-- pt_assessed_value_allocation
create table pwrplant.pt_assessed_value_allocation
   (  assessed_value_allocation_id  number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(35)   not null,
      long_description              varchar2(254),
      applies_to                    varchar2(35)   not null
   );

alter table pwrplant.pt_assessed_value_allocation add ( constraint pt_assessed_value_allo_pk primary key ( assessed_value_allocation_id ) using index tablespace pwrplant_idx );



-- pt_statement_group
create table pwrplant.pt_statement_group
   (  statement_group_id               number(22,0)   not null,
      time_stamp                       date,
      user_id                          varchar2(18),
      description                      varchar2(100)  not null,
      long_description                 varchar2(254),
      prop_tax_company_id           number(22,0)   not null,
      state_id                         char(18)       not null,
      statement_payee_id               number(22,0)   not null,
      approval_type_id                 number(22,0)   not null,
      assessed_value_allocation_id     number(22,0)   not null,
      external_code1                varchar2(35),
      external_code2                varchar2(35),
      default_first_install_amt           number(22,2),
      separate_checks_yn               number(22,0)
   );

alter table pwrplant.pt_statement_group add ( constraint pt_statement_group_pk primary key ( statement_group_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_group add ( constraint pt_sg_pt_comp_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_statement_group add ( constraint pt_stmt_gp_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_statement_group add ( constraint pt_stmt_gp_payee_fk foreign key ( statement_payee_id ) references pwrplant.pt_statement_payee );
alter table pwrplant.pt_statement_group add ( constraint pt_stmt_gp_approval_type_fk foreign key ( approval_type_id ) references pwrplant.approval );
alter table pwrplant.pt_statement_group add ( constraint pt_stmt_gp_asd_val_allo_fk foreign key ( assessed_value_allocation_id ) references pwrplant.pt_assessed_value_allocation );



-- property_tax_authority_type
create table pwrplant.property_tax_authority_type
   (  tax_authority_type_id      number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      description                varchar2(35)   not null,
      tax_authority_type_code varchar2(35)
   );

alter table pwrplant.property_tax_authority_type add ( constraint property_tax_authority_type_pk primary key ( tax_authority_type_id ) using index tablespace pwrplant_idx );



-- pt_authority_rate_type
create table pwrplant.pt_authority_rate_type
   (  authority_rate_type_id  number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254)
   );

alter table pwrplant.pt_authority_rate_type add ( constraint pt_authority_rate_type_pk primary key ( authority_rate_type_id ) using index tablespace pwrplant_idx );



-- property_tax_authority
create table pwrplant.property_tax_authority
   (  tax_authority_id           number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      description                varchar2(100)  not null,
      long_description           varchar2(254),
      state_id                   char(18)       not null,
      county_id                  char(18),
      authority_rate_type_id     number(22,0)   not null,
      tax_authority_type_id      number(22,0),
      tax_authority_code         varchar2(35),
      tax_district_id               number(22,0),
      statement_group_id         number(22,0)
   );

alter table pwrplant.property_tax_authority add ( constraint property_tax_authority_pk primary key ( tax_authority_id ) using index tablespace pwrplant_idx );
alter table pwrplant.property_tax_authority add ( constraint pt_auth_auth_rate_type_fk foreign key ( authority_rate_type_id ) references pwrplant.pt_authority_rate_type );
alter table pwrplant.property_tax_authority add ( constraint pt_auth_auth_type_fk foreign key ( tax_authority_type_id ) references pwrplant.property_tax_authority_type );
alter table pwrplant.property_tax_authority add ( constraint pt_auth_county_fk foreign key ( county_id, state_id ) references pwrplant.county ( county_id, state_id ) );
alter table pwrplant.property_tax_authority add ( constraint pt_authority_district_fk foreign key ( tax_district_id ) references pwrplant.prop_tax_district );
alter table pwrplant.property_tax_authority add ( constraint pt_authority_stmt_group_fk foreign key ( statement_group_id ) references pwrplant.pt_statement_group );

create index pwrplant.pt_authority_st_co_ndx on pwrplant.property_tax_authority ( state_id, county_id ) tablespace pwrplant_idx;
create index pwrplant.property_tax_auth_comp_ndx on pwrplant.property_tax_authority ( tax_district_id, statement_group_id ) tablespace pwrplant_idx;



--tax_auth_prop_tax_dist
create table pwrplant.tax_auth_prop_tax_dist
   (  tax_authority_id     number(22,0)   not null,
      tax_district_id         number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      tax_year             number(22,0)   not null,
      percentage           number(22,8)   default 1,
      external_code1    varchar2(50),
      external_code2    varchar2(50)
   );

alter table pwrplant.tax_auth_prop_tax_dist add ( constraint tax_auth_prop_tax_dist_pk primary key ( tax_authority_id, tax_district_id, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.tax_auth_prop_tax_dist add ( constraint tatd_authority_fk foreign key ( tax_authority_id ) references pwrplant.property_tax_authority );
alter table pwrplant.tax_auth_prop_tax_dist add ( constraint tatd_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.tax_auth_prop_tax_dist add ( constraint tatd_tax_district_fk foreign key ( tax_district_id ) references pwrplant.prop_tax_district );

create index pwrplant.tatd_ta_ndx on pwrplant.tax_auth_prop_tax_dist ( tax_authority_id ) tablespace pwrplant_idx;



-- pt_parcel_type
create table pwrplant.pt_parcel_type
   (  parcel_type_id    number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null,
      long_description  varchar2(254)
   );

alter table pwrplant.pt_parcel_type add ( constraint pt_parcel_type_pk primary key ( parcel_type_id ) using index tablespace pwrplant_idx );



-- pt_parcel
create table pwrplant.pt_parcel
   (  parcel_id                     number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      description                varchar2(100)  not null,
      long_description           varchar2(254),
      parcel_number              varchar2(100)  not null,
      parcel_type_id             number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      state_id                   char(18)       not null,
      tax_district_id               number(22,0)   not null,
      grantor                    varchar2(254),
      land_acreage               number(22,8),
      improve_sq_ft              number(22,8),
      mineral_acreage            number(22,8),
      company_grid_number     varchar2(35),
      state_grid_number       varchar2(35),
      x_coordinate               varchar2(35),
      y_coordinate               varchar2(35),
      retired_date               date,
      address_1                  varchar2(50),
      address_2                  varchar2(50),
      city                       varchar2(35),
      zip_code                   varchar2(35),
      notes                      varchar2(4000),
      legal_description          varchar2(4000),
      legal_description_b        varchar2(4000),
      legal_description_c        varchar2(4000),
      legal_description_d        varchar2(4000)
   );

alter table pwrplant.pt_parcel add ( constraint pt_parcel_pk primary key ( parcel_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel add ( constraint pt_parcel_ptcomp_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_parcel add ( constraint pt_parcel_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_parcel add ( constraint pt_parcel_tax_dist_fk foreign key ( tax_district_id ) references pwrplant.prop_tax_district );
alter table pwrplant.pt_parcel add ( constraint pt_parcel_prcl_type_fk foreign key ( parcel_type_id ) references pwrplant.pt_parcel_type );

create index pwrplant.pt_parcel_st_ptco_ndx on pwrplant.pt_parcel ( state_id, prop_tax_company_id ) tablespace pwrplant_idx;
create index pwrplant.pt_parcel_td_ndx on pwrplant.pt_parcel ( tax_district_id ) tablespace pwrplant_idx;



-- pt_parcel_asset
create table pwrplant.pt_parcel_asset
   (  asset_id       number(22,0)   not null,
      time_stamp     date,
      user_id        varchar2(18),
      parcel_id         number(22,0)   not null
   );

alter table pwrplant.pt_parcel_asset add ( constraint pt_parcel_asset_pk primary key ( asset_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_asset add ( constraint pt_parcel_asset_parcel_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );

create index pwrplant.pt_prcl_ast_prcl_ndx on pwrplant.pt_parcel_asset ( parcel_id ) tablespace pwrplant_idx;



-- pt_parcel_flex_fields
create table pwrplant.pt_parcel_flex_fields
   (  parcel_id            number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      flex_1            number(22,8),
      flex_2            number(22,8),
      flex_3            number(22,8),
      flex_4            number(22,8),
      flex_5            number(22,8),
      flex_6            date,
      flex_7            date,
      flex_8            date,
      flex_9            date,
      flex_10           date,
      flex_11           varchar2(35),
      flex_12           varchar2(35),
      flex_13           varchar2(35),
      flex_14           varchar2(35),
      flex_15           varchar2(35),
      flex_16           varchar2(35),
      flex_17           varchar2(35),
      flex_18           varchar2(35),
      flex_19           varchar2(35),
      flex_20           varchar2(35),
      flex_21           varchar2(254),
      flex_22           varchar2(254),
      flex_23           varchar2(2000)
   );

alter table pwrplant.pt_parcel_flex_fields add ( constraint pt_parcel_flex_fields_pk primary key ( parcel_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_flex_fields add ( constraint pt_parcel_ff_parcel_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );



-- pt_parcel_flex_field_usage
create table pwrplant.pt_parcel_flex_field_usage
   (  flex_usage_id     number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null
   );

alter table pwrplant.pt_parcel_flex_field_usage add ( constraint pt_parcel_flex_field_usage_pk primary key ( flex_usage_id ) using index tablespace pwrplant_idx );



-- pt_parcel_flex_field_control
create table pwrplant.pt_parcel_flex_field_control
   (  flex_field_id        number(22,0)   not null,
      parcel_type_id    number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      flex_field_label     varchar2(35),
      flex_usage_id     number(22,0)   not null,
      dddw_name         varchar2(35),
      validate_value    number(22,0),
      column_type    varchar2(35)
   );

alter table pwrplant.pt_parcel_flex_field_control add ( constraint pt_parcel_flex_field_cntl_pk primary key ( flex_field_id, parcel_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_flex_field_control add ( constraint pt_prcl_ff_ctrl_usage_fk foreign key ( flex_usage_id ) references pwrplant.pt_parcel_flex_field_usage );
alter table pwrplant.pt_parcel_flex_field_control add ( constraint pt_prcl_ff_ctrl_prcl_type_fk foreign key ( parcel_type_id ) references pwrplant.pt_parcel_type );



-- pt_parcel_flex_field_values
create table pwrplant.pt_parcel_flex_field_values
   (  flex_field_id        number(22,0)   not null,
      parcel_type_id    number(22,0)   not null,
      value             varchar2(35)   not null,
      time_stamp        date,
      user_id           varchar2(18)
   );

alter table pwrplant.pt_parcel_flex_field_values add ( constraint pt_parcel_flex_field_val_pk primary key ( flex_field_id, parcel_type_id, value ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_flex_field_values add ( constraint pt_prcl_ff_values_ff_cntl_fk foreign key ( flex_field_id, parcel_type_id ) references pwrplant.pt_parcel_flex_field_control ( flex_field_id, parcel_type_id ) );
alter table pwrplant.pt_parcel_flex_field_values add ( constraint pt_prcl_ff_values_prcl_tp_fk foreign key ( parcel_type_id ) references pwrplant.pt_parcel_type );



-- pt_parcel_geography_type
create table pwrplant.pt_parcel_geography_type
   (  geography_type_id    number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254)
   );

alter table pwrplant.pt_parcel_geography_type add ( constraint pt_parcel_geography_type_pk primary key ( geography_type_id ) using index tablespace pwrplant_idx );



-- pt_parcel_geography
create table pwrplant.pt_parcel_geography
   (  parcel_id               number(22,0)   not null,
      geography_type_id number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      value                varchar2(35)   not null
   );

alter table pwrplant.pt_parcel_geography add ( constraint pt_parcel_geography_pk primary key ( parcel_id, geography_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_geography add ( constraint pt_prcl_geo_geo_type_fk foreign key ( geography_type_id ) references pwrplant.pt_parcel_geography_type );
alter table pwrplant.pt_parcel_geography add ( constraint pt_prcl_geo_parcel_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );



-- pt_parcel_geography_type_state
create table pwrplant.pt_parcel_geography_type_state
   (  geography_type_id number(22,0)   not null,
      state_id             char(18)       not null,
      time_stamp           date,
      user_id              varchar2(18)
   );

alter table pwrplant.pt_parcel_geography_type_state add ( constraint pt_parcel_geography_type_st_pk primary key ( geography_type_id, state_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_geography_type_state add ( constraint pt_prcl_geo_type_st_geo_typ_fk foreign key ( geography_type_id ) references pwrplant.pt_parcel_geography_type );
alter table pwrplant.pt_parcel_geography_type_state add ( constraint pt_prcl_geo_type_st_state_fk foreign key ( state_id ) references pwrplant.state );



-- pt_parcel_responsible_entity
create table pwrplant.pt_parcel_responsible_entity
   (  responsible_entity_id      number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254),
      address_1               varchar2(100),
      address_2               varchar2(100),
      address_3               varchar2(100),
      city                    varchar2(35),
      state                   varchar2(35),
      zip_code                varchar2(35),
      phone_1                 varchar2(35),
      phone_2                 varchar2(35),
      contact_name            varchar2(35),
      external_code           varchar2(35),
      entity_number           varchar2(35),
      tax_id_code             varchar2(35),
      tax_id_number           varchar2(35),
      prop_tax_company_id  number(22,0),
      notes                      varchar2(4000)
   );

alter table pwrplant.pt_parcel_responsible_entity add ( constraint pt_prcl_responsible_entity_pk primary key ( responsible_entity_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_responsible_entity add ( constraint pt_prcl_resp_ent_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- pt_parcel_responsibility_type
create table pwrplant.pt_parcel_responsibility_type
   (  responsibility_type_id           number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(35),
      long_description              varchar2(254)
   );

alter table pwrplant.pt_parcel_responsibility_type add ( constraint pt_prcl_resp_type_pk primary key ( responsibility_type_id ) using index tablespace pwrplant_idx );



-- pt_parcel_responsibility
create table pwrplant.pt_parcel_responsibility
   (  tax_year                   number(22,0)   not null,
      parcel_id                  number(22,0)   not null,
      responsible_entity_id      number(22,0)   not null,
      responsibility_type_id     number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      percent_responsible     number(22,8)   not null,
      lease_start_date        date,
      lease_end_date       date,
      entity_is_lessor           number(22,0),
      entity_is_lessee        number(22,0),
      reference_number        varchar2(35)
   );

alter table pwrplant.pt_parcel_responsibility add ( constraint pt_parcel_responsibility_pk primary key ( tax_year, parcel_id, responsible_entity_id, responsibility_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_responsibility add ( constraint pt_prcl_respon_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_parcel_responsibility add ( constraint pt_prcl_respon_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_parcel_responsibility add ( constraint pt_prcl_respon_respon_ent_fk foreign key ( responsible_entity_id ) references pwrplant.pt_parcel_responsible_entity );
alter table pwrplant.pt_parcel_responsibility add ( constraint pt_prcl_respon_rt_fk foreign key ( responsibility_type_id ) references pwrplant.pt_parcel_responsibility_type );



-- pt_appraiser
create table pwrplant.pt_appraiser
   (  appraiser_id            number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      name_line_1       varchar2(50),
      name_line_2       varchar2(50),
      name_line_3       varchar2(50),
      company              varchar2(50),
      address_line_1       varchar2(50),
      address_line_2       varchar2(50),
      address_line_3       varchar2(50),
      city                 varchar2(35),
      state                varchar2(35),
      zip_code             varchar2(35),
      phone_1              varchar2(35),
      phone_2              varchar2(35),
      ext_appraiser_code   varchar2(35),
      notes                varchar2(254)
   );

alter table pwrplant.pt_appraiser add ( constraint pt_appraiser_pk primary key ( appraiser_id ) using index tablespace pwrplant_idx );



-- pt_parcel_history
create table pwrplant.pt_parcel_history
   (  parcel_id         number(22,0)      not null,
      event_id       number(22,0)      not null,
      time_stamp     date,
      user_id        varchar2(18),
      event_date     date              not null,
      notes          varchar2(4000) not null
   );

alter table pwrplant.pt_parcel_history add ( constraint pt_parcel_history_pk primary key ( parcel_id, event_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_history add ( constraint pt_parcel_history_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );



-- pt_parcel_appeal_type
create table pwrplant.pt_parcel_appeal_type
   (  appeal_type_id       number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_parcel_appeal_type add ( constraint pt_parcel_appeal_type_pk primary key ( appeal_type_id ) using index tablespace pwrplant_idx );



-- pt_parcel_appeal
create table pwrplant.pt_parcel_appeal
   (  parcel_id                  number(22,0)   not null,
      tax_year                number(22,0)   not null,
      appeal_id               number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      appeal_type_id          number(22,0)   not null,
      appeal_date          date,
      target_assessment    number(22,2),
      assigned_to             varchar2(18),
      resolved_date           date,
      resolved_assessment     number(22,2),
      notes                   varchar2(2000)
   );

alter table pwrplant.pt_parcel_appeal add ( constraint pt_parcel_appeal_pk primary key ( parcel_id, tax_year, appeal_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_appeal add ( constraint pt_prcl_appl_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_parcel_appeal add ( constraint pt_prcl_appl_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_parcel_appeal add ( constraint pt_prcl_appl_appl_ty_fk foreign key ( appeal_type_id ) references pwrplant.pt_parcel_appeal_type );



-- pt_parcel_appeal_event
create table pwrplant.pt_parcel_appeal_event
   (  parcel_id         number(22,0)      not null,
      tax_year       number(22,0)      not null,
      appeal_id      number(22,0)      not null,
      event_id       number(22,0)      not null,
      time_stamp     date,
      user_id        varchar2(18),
      event_date     date              not null,
      notes          varchar2(2000) not null
   );

alter table pwrplant.pt_parcel_appeal_event add ( constraint pt_parcel_appeal_event_pk primary key ( parcel_id, tax_year, appeal_id, event_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_appeal_event add ( constraint pt_prcl_appl_ev_prcl_appl_fk foreign key ( parcel_id, tax_year, appeal_id ) references pwrplant.pt_parcel_appeal ( parcel_id, tax_year, appeal_id ) );



-- pt_assessor
create table pwrplant.pt_assessor
   (  assessor_id          number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      name_line_1       varchar2(50),
      name_line_2       varchar2(50),
      name_line_3       varchar2(50),
      address_line_1       varchar2(50),
      address_line_2       varchar2(50),
      address_line_3       varchar2(50),
      city                 varchar2(35),
      state                varchar2(35),
      zip_code             varchar2(35),
      phone_1              varchar2(35),
      phone_2              varchar2(35),
      ext_assessor_code varchar2(35),
      notes                varchar2(2000)
   );

alter table pwrplant.pt_assessor add ( constraint pt_assessor_pk primary key ( assessor_id ) using index tablespace pwrplant_idx );



-- pt_assessment_group
create table pwrplant.pt_assessment_group
   (  assessment_group_id     number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254)
   );

alter table pwrplant.pt_assessment_group add ( constraint pt_assessment_group_pk primary key ( assessment_group_id ) using index tablespace pwrplant_idx );



-- pt_parcel_appraisal
create table pwrplant.pt_parcel_appraisal
   (  parcel_id                  number(22,0)   not null,
      assessment_group_id     number(22,0)   not null,
      appraisal_id               number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      appraisal_date          date,
      appraiser_id               number(22,0),
      appraisal_value         number(22,2) not null,
      notes                   varchar2(4000)
   );

alter table pwrplant.pt_parcel_appraisal add ( constraint pt_parcel_appraisal_pk primary key ( parcel_id, assessment_group_id, appraisal_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_appraisal add ( constraint pt_prcl_appr_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_parcel_appraisal add ( constraint pt_prcl_appr_ag_fk foreign key ( assessment_group_id ) references pwrplant.pt_assessment_group );
alter table pwrplant.pt_parcel_appraisal add ( constraint pt_prcl_appr_appraiser_fk foreign key ( appraiser_id ) references pwrplant.pt_appraiser );



-- pt_parcel_assessment
create table pwrplant.pt_parcel_assessment
   (  parcel_id                  number(22,0)   not null,
      assessment_group_id     number(22,0)   not null,
      case_id                 number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      assessment              number(22,2),
      equalized_value         number(22,2),
      taxable_value           number(22,2),
      assessment_date         date,
      assessor_id             number(22,0)
   );

alter table pwrplant.pt_parcel_assessment add ( constraint pt_parcel_assessment_pk primary key ( parcel_id, assessment_group_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_assessment add ( constraint pt_prcl_asmt_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_parcel_assessment add ( constraint pt_prcl_asmt_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_parcel_assessment add ( constraint pt_prcl_asmt_asmt_grp_fk foreign key ( assessment_group_id ) references pwrplant.pt_assessment_group );
alter table pwrplant.pt_parcel_assessment add ( constraint pt_prcl_asmt_assessor_fk foreign key ( assessor_id ) references pwrplant.pt_assessor );



-- pt_method
create table pwrplant.pt_method
   (  method_id      number(22,0)   not null,
      time_stamp     date,
      user_id        varchar2(18),
      description    varchar2(35)   not null
   );

alter table pwrplant.pt_method add ( constraint pt_method_pk primary key ( method_id ) using index tablespace pwrplant_idx );



-- pt_reserve_method
create table pwrplant.pt_reserve_method
   (  reserve_method_id number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null
   );

alter table pwrplant.pt_reserve_method add ( constraint pt_reserve_method_pk primary key ( reserve_method_id ) using index tablespace pwrplant_idx );



-- pt_reserve_factor_type
create table pwrplant.pt_reserve_factor_type
   (  reserve_factor_type_id  number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254)
   );

alter table pwrplant.pt_reserve_factor_type add ( constraint pt_reserve_factor_type_pk primary key ( reserve_factor_type_id ) using index tablespace pwrplant_idx );



-- pt_reserve_factors
create table pwrplant.pt_reserve_factors
   (  reserve_factor_id       number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254),
      reserve_factor_type_id  number(22,0)   not null,
      status_code_id             number(22,0)   not null
   );

alter table pwrplant.pt_reserve_factors add ( constraint pt_reserve_factors_pk primary key ( reserve_factor_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_reserve_factors add ( constraint pt_rsv_fctrs_rsv_fctr_type_fk foreign key ( reserve_factor_type_id ) references pwrplant.pt_reserve_factor_type );
alter table pwrplant.pt_reserve_factors add ( constraint pt_rsv_fctr_status_fk foreign key ( status_code_id ) references pwrplant.status_code );



-- pt_reserve_factor_percents
create table pwrplant.pt_reserve_factor_percents
   (  tax_year                   number(22,0)   not null,
      reserve_factor_id          number(22,0)   not null,
      age                        number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      reserve_factor_percent     number(22,8)   not null
   );

--ORIG--alter table pwrplant.pt_reserve_factor_percents add ( constraint pt_reserve_factor_percents_pk primary key ( tax_year, reserve_factor_id, age ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_reserve_factor_percents add ( constraint pt_reserve_factor_percents_pk primary key ( reserve_factor_id, age, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_reserve_factor_percents add ( constraint pt_rsv_fctr_pcts_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_reserve_factor_percents add ( constraint pt_rsv_fctr_pcts_rsv_fctr_fk foreign key ( reserve_factor_id ) references pwrplant.pt_reserve_factors );



-- pt_reserve_factor_county_pcts
create table pwrplant.pt_reserve_factor_county_pcts
   (  tax_year                   number(22,0)   not null,
      reserve_factor_id          number(22,0)   not null,
      state_id                   char(18)       not null,
      county_id                  char(18)       not null,
      age                        number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      reserve_factor_percent     number(22,8)   not null
   );

--ORIG--alter table pwrplant.pt_reserve_factor_county_pcts add ( constraint pt_reserve_factor_cnty_pcts_pk primary key ( tax_year, reserve_factor_id, state_id, county_id, age ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_reserve_factor_county_pcts add ( constraint pt_reserve_factor_cnty_pcts_pk primary key ( reserve_factor_id, state_id, county_id, age, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_reserve_factor_county_pcts add ( constraint pt_rsv_fctr_cnty_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_reserve_factor_county_pcts add ( constraint pt_rsv_fctr_cnty_rsv_fctr_fk foreign key ( reserve_factor_id ) references pwrplant.pt_reserve_factors );
alter table pwrplant.pt_reserve_factor_county_pcts add ( constraint pt_rsv_fctr_cnty_cnty_fk foreign key ( county_id, state_id ) references pwrplant.county ( county_id, state_id ) );



-- pt_reserve_factor_dist_pcts
create table pwrplant.pt_reserve_factor_dist_pcts
   (  tax_year                   number(22,0)   not null,
      reserve_factor_id          number(22,0)   not null,
      tax_district_id               number(22,0)   not null,
      age                        number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      reserve_factor_percent     number(22,8)   not null
   );

--ORIG--alter table pwrplant.pt_reserve_factor_dist_pcts add ( constraint pt_reserve_factor_dist_pcts_pk primary key ( tax_year, reserve_factor_id, tax_district_id, age ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_reserve_factor_dist_pcts add ( constraint pt_reserve_factor_dist_pcts_pk primary key ( reserve_factor_id, tax_district_id, age, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_reserve_factor_dist_pcts add ( constraint pt_rsv_fctr_dist_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_reserve_factor_dist_pcts add ( constraint pt_rsv_fctr_dist_rsv_fctr_fk foreign key ( reserve_factor_id ) references pwrplant.pt_reserve_factors );
alter table pwrplant.pt_reserve_factor_dist_pcts add ( constraint pt_rsv_fctr_dist_tax_dist_fk foreign key ( tax_district_id ) references pwrplant.prop_tax_district );



-- pt_allocation_type
create table pwrplant.pt_allocation_type
   (  allocation_type_id      number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null
   );

alter table pwrplant.pt_allocation_type add ( constraint pt_allocation_type_pk primary key ( allocation_type_id ) using index tablespace pwrplant_idx );



-- pt_allocation_method
create table pwrplant.pt_allocation_method
   (  allocation_method_id    number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254)
   );

alter table pwrplant.pt_allocation_method add ( constraint pt_allocation_method_pk primary key ( allocation_method_id ) using index tablespace pwrplant_idx );



-- prop_tax_allo_definition
create table pwrplant.prop_tax_allo_definition
   (  allocation_id              number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254),
      allocation_method_id    number(22,0)   not null
   );

alter table pwrplant.prop_tax_allo_definition add ( constraint prop_tax_allo_definition_pk primary key ( allocation_id ) using index tablespace pwrplant_idx );
alter table pwrplant.prop_tax_allo_definition add ( constraint pt_allo_def_allo_meth_fk foreign key ( allocation_method_id ) references pwrplant.pt_allocation_method );



-- pt_statistics_full
create table pwrplant.pt_statistics_full
   (  statistics_full_id         number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      tax_year                number(22,0)   not null,
      allocation_id              number(22,0)   not null,
      prop_tax_location_id    number(22,0),
      vintage                 number(22,0),
      parcel_id                  number(22,0)   not null,
      statistic                  number(22,8)   not null
   );

alter table pwrplant.pt_statistics_full add ( constraint pt_statistics_full_pk primary key ( statistics_full_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statistics_full add ( constraint pt_stats_full_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_statistics_full add ( constraint pt_stats_full_allo_fk foreign key ( allocation_id ) references pwrplant.prop_tax_allo_definition );
alter table pwrplant.pt_statistics_full add ( constraint pt_stats_full_ptloc_fk foreign key ( prop_tax_location_id ) references pwrplant.prop_tax_location );
alter table pwrplant.pt_statistics_full add ( constraint pt_stats_full_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );

create unique index pwrplant.pt_stats_full_unique_ndx on pwrplant.pt_statistics_full ( parcel_id, prop_tax_location_id, vintage, allocation_id, tax_year ) tablespace pwrplant_idx;



-- pt_statistics_incremental
create table pwrplant.pt_statistics_incremental
   (  statistics_incr_id         number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      tax_year                number(22,0)   not null,
      allocation_id              number(22,0)   not null,
      prop_tax_location_id    number(22,0),
      parcel_id                  number(22,0)   not null,
      add_statistic           number(22,8)   not null,
      ret_statistic              number(22,8)   not null,
      trans_statistic            number(22,8)   not null
   );

alter table pwrplant.pt_statistics_incremental add ( constraint pt_statistics_incremental_pk primary key ( statistics_incr_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statistics_incremental add ( constraint pt_stats_incr_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_statistics_incremental add ( constraint pt_stats_incr_allo_fk foreign key ( allocation_id ) references pwrplant.prop_tax_allo_definition );
alter table pwrplant.pt_statistics_incremental add ( constraint pt_stats_incr_ptloc_fk foreign key ( prop_tax_location_id ) references pwrplant.prop_tax_location );
alter table pwrplant.pt_statistics_incremental add ( constraint pt_stats_incr_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );

create unique index pwrplant.pt_stats_incr_unique_ndx on pwrplant.pt_statistics_incremental ( parcel_id, prop_tax_location_id, allocation_id, tax_year ) tablespace pwrplant_idx;



-- pt_escalated_value_type
create table pwrplant.pt_escalated_value_type
   (  escalated_value_type_id       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(35)   not null,
      long_description              varchar2(254),
      status_code_id                   number(22,0)   not null
   );

alter table pwrplant.pt_escalated_value_type add ( constraint pt_escalated_value_type_pk primary key ( escalated_value_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_escalated_value_type add ( constraint pt_escval_status_fk foreign key ( status_code_id ) references pwrplant.status_code );



-- pt_escalated_value_index
create table pwrplant.pt_escalated_value_index
   (  tax_year                   number(22,0)   not null,
      escalated_value_type_id    number(22,0)   not null,
      index_year                 number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      index_percent              number(22,8)   not null
   );

--ORIG--alter table pwrplant.pt_escalated_value_index add ( constraint pt_escalated_value_index_pk primary key ( tax_year, escalated_value_type_id, index_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_escalated_value_index add ( constraint pt_escalated_value_index_pk primary key ( escalated_value_type_id, index_year, tax_year  ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_escalated_value_index add ( constraint pt_esc_val_ndx_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_escalated_value_index add ( constraint pt_esc_val_ndx_esc_val_type_fk foreign key ( escalated_value_type_id ) references pwrplant.pt_escalated_value_type );



-- pt_vintage_option
create table pwrplant.pt_vintage_option
   (  vintage_option_id    number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_vintage_option add ( constraint pt_vintage_option_pk primary key ( vintage_option_id ) using index tablespace pwrplant_idx );



-- pt_allocate_assessment_type
create table pwrplant.pt_allocate_assessment_type
   (  allocate_assessment_type_id   number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(35)   not null
   );

alter table pwrplant.pt_allocate_assessment_type add ( constraint pt_allocate_assessment_type_pk primary key ( allocate_assessment_type_id ) using index tablespace pwrplant_idx );



-- pt_rate_type
create table pwrplant.pt_rate_type
   (  rate_type_id      number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null,
      long_description  varchar2(254)
   );

alter table pwrplant.pt_rate_type add ( constraint pt_rate_type_pk primary key ( rate_type_id ) using index tablespace pwrplant_idx );



-- pt_rate_definition
create table pwrplant.pt_rate_definition
   (  pt_rate_id        number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null,
      long_description  varchar2(254),
      rate_type_id      number(22,0)   not null,
      limit_for_rate    number(22,8)
   );

alter table pwrplant.pt_rate_definition add ( constraint pt_rate_definition_pk primary key ( pt_rate_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_rate_definition add ( constraint pt_rate_def_rate_type_fk foreign key ( rate_type_id ) references pwrplant.pt_rate_type );



-- pt_rates
create table pwrplant.pt_rates
   (  pt_rate_id        number(22,0)   not null,
      case_id           number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      assessment_rate   number(22,8)   not null
   );

alter table pwrplant.pt_rates add ( constraint pt_rates_pk primary key ( pt_rate_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_rates add ( constraint pt_rates_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_rates add ( constraint pt_rates_rate_def_fk foreign key ( pt_rate_id ) references pwrplant.pt_rate_definition );



-- pt_market_value_rate
create table pwrplant.pt_market_value_rate
   (  pt_rate_id           number(22,0)   not null,
      tax_year             number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      market_value_rate number(22,8)   not null
   );

alter table pwrplant.pt_market_value_rate add ( constraint pt_market_value_rate_pk primary key ( pt_rate_id, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_market_value_rate add ( constraint pt_mkt_val_rate_rate_def_fk foreign key ( pt_rate_id ) references pwrplant.pt_rate_definition );
alter table pwrplant.pt_market_value_rate add ( constraint pt_mkt_val_rate_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );



-- pt_equalization_factor_type
create table pwrplant.pt_equalization_factor_type
   (  eq_factor_type_id    number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_equalization_factor_type add ( constraint pt_equalization_factor_type_pk primary key ( eq_factor_type_id ) using index tablespace pwrplant_idx );



-- pt_levy_class
create table pwrplant.pt_levy_class
   (  levy_class_id     number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null,
      long_description  varchar2(254)
   );

alter table pwrplant.pt_levy_class add ( constraint pt_levy_class_fk primary key ( levy_class_id ) using index tablespace pwrplant_idx );



-- pt_unit_cost
create table pwrplant.pt_unit_cost
   (  unit_cost_id            number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      round_result_yn      number(22,0)   not null,
      round_to_decimals    number(22,0)
   );

alter table pwrplant.pt_unit_cost add ( constraint pt_unit_cost_pk primary key ( unit_cost_id ) using index tablespace pwrplant_idx );



-- pt_unit_cost_amount
create table pwrplant.pt_unit_cost_amount
   (  unit_cost_id      number(22,0)   not null,
      tax_year       number(22,0)   not null,
      time_stamp     date,
      user_id        varchar2(18),
      unit_amount number(22,2)   not null
   );

alter table pwrplant.pt_unit_cost_amount add ( constraint pt_unit_cost_amount_pk primary key ( unit_cost_id, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_unit_cost_amount add ( constraint pt_unit_cost_amount_uc_fk foreign key ( unit_cost_id ) references pwrplant.pt_unit_cost );
alter table pwrplant.pt_unit_cost_amount add ( constraint pt_unit_cost_amount_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );



-- pt_quantity_conversion
create table pwrplant.pt_quantity_conversion
   (  quantity_conversion_id  number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254),
      formula                 varchar2(254)  not null
   );

alter table pwrplant.pt_quantity_conversion add ( constraint pt_quantity_conversion_pk primary key ( quantity_conversion_id ) using index tablespace pwrplant_idx );



-- pt_ledger_detail_column
create table pwrplant.pt_ledger_detail_column
   (  column_name       varchar2(35)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      column_type          varchar2(35)   not null
   );

alter table pwrplant.pt_ledger_detail_column add ( constraint pt_ldg_det_column_pk primary key ( column_name ) using index tablespace pwrplant_idx );



-- pt_ledger_detail_source
create table pwrplant.pt_ledger_detail_source
   (  detail_source_id     number(22,0)      not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(100)     not null,
      long_description     varchar2(500)     not null,
      table_name           varchar2(35)      not null,
      column_name       varchar2(35)      not null,
      column_type       varchar2(35)      not null,
      lookup_sql           varchar2(4000)
   );

alter table pwrplant.pt_ledger_detail_source add ( constraint pt_ldg_det_source_pk primary key ( detail_source_id ) using index tablespace pwrplant_idx );



-- pt_ledger_detail_map
create table pwrplant.pt_ledger_detail_map
   (  detail_map_id        number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      created_by           varchar2(18),
      created_date         date
   );

alter table pwrplant.pt_ledger_detail_map add ( constraint pt_ldg_det_map_pk primary key ( detail_map_id ) using index tablespace pwrplant_idx );



-- pt_ledger_detail_map_fields
create table pwrplant.pt_ledger_detail_map_fields
   (  detail_map_id           number(22,0)   not null,
      column_name          varchar2(35)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      is_class_code_yn        number(22,0)   not null,
      class_code_id           number(22,0),
      detail_source_id        number(22,0)
   );

alter table pwrplant.pt_ledger_detail_map_fields add ( constraint pt_ldg_det_map_flds_pk primary key ( detail_map_id, column_name ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_ledger_detail_map_fields add ( constraint pt_ldg_det_map_flds_map_fk foreign key ( detail_map_id ) references pwrplant.pt_ledger_detail_map );
alter table pwrplant.pt_ledger_detail_map_fields add ( constraint pt_ldg_det_map_flds_col_fk foreign key ( column_name ) references pwrplant.pt_ledger_detail_column );
alter table pwrplant.pt_ledger_detail_map_fields add ( constraint pt_ldg_det_map_flds_cc_fk foreign key ( class_code_id ) references pwrplant.class_code );
alter table pwrplant.pt_ledger_detail_map_fields add ( constraint pt_ldg_det_map_flds_src_fk foreign key ( detail_source_id ) references pwrplant.pt_ledger_detail_source );



-- property_tax_type_data
create table pwrplant.property_tax_type_data
   (  property_tax_type_id          number(22,0)   not null,
      time_stamp                       date,
      user_id                          varchar2(18),
      description                      varchar2(65)   not null,
      state_id                         char(18)       not null,
      valuation_date                   date           not null,
      status                           char(35)       not null,
      method                           number(22,0)   not null,
      reserve_method                number(22,0)   not null,
      reserve_factor_id                number(22,0),
      depreciation_floor                  number(22,8),
      depreciation_floor_recovery      number(22,8),
      allocation_type                     number(22,0)   not null,
      allocation_id                       number(22,0),
      incremental_method               number(22,0),
      vintage_nonvintage               number(22,0),
      vintage_month_yn              number(22,0),
      quantity_indicator                  number(22,0),
      escalated_values                 number(22,0),
      escalate_reserve_yn              number(22,0),
      market_value_rate_id          number(22,0),
      levy_class_id                    number(22,0),
      external_code                    varchar2(35),
      unit_cost_id                     number(22,0),
      quantity_conversion_id           number(22,0),
      quantity_factor                  number(22,8),
      parcel_type_id                   number(22,0) not null,
      set_of_books_id                  number(22,0) not null,
      keep_vintage_on_ledger           number(22,0),
      detail_map_id                    number(22,0)
   );

alter table pwrplant.property_tax_type_data add ( constraint property_tax_type_data_pk primary key ( property_tax_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.property_tax_type_data add ( constraint pttd_vint_opt_fk foreign key ( vintage_nonvintage ) references pwrplant.pt_vintage_option );
alter table pwrplant.property_tax_type_data add ( constraint pttd_mkt_val_rate_fk foreign key ( market_value_rate_id ) references pwrplant.pt_rate_definition );
alter table pwrplant.property_tax_type_data add ( constraint ptt_esc_val_fk foreign key ( escalated_values ) references pwrplant.pt_escalated_value_type );
alter table pwrplant.property_tax_type_data add ( constraint pttd_allo_type_fk foreign key ( allocation_type ) references pwrplant.pt_allocation_type );
alter table pwrplant.property_tax_type_data add ( constraint pttd_allocations_fk foreign key ( allocation_id ) references pwrplant.prop_tax_allo_definition );
alter table pwrplant.property_tax_type_data add ( constraint pttd_method_fk foreign key ( method ) references pwrplant.pt_method );
alter table pwrplant.property_tax_type_data add ( constraint pttd_rsv_fctr_fk foreign key ( reserve_factor_id ) references pwrplant.pt_reserve_factors );
alter table pwrplant.property_tax_type_data add ( constraint pttd_rsv_method_fk foreign key ( reserve_method ) references pwrplant.pt_reserve_method );
alter table pwrplant.property_tax_type_data add ( constraint pttd_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.property_tax_type_data add ( constraint pttd_levy_class_fk foreign key ( levy_class_id ) references pwrplant.pt_levy_class );
alter table pwrplant.property_tax_type_data add ( constraint pttd_unit_cost_fk foreign key ( unit_cost_id ) references pwrplant.pt_unit_cost );
alter table pwrplant.property_tax_type_data add ( constraint pttd_qty_conv_fk foreign key ( quantity_conversion_id ) references pwrplant.pt_quantity_conversion );
alter table pwrplant.property_tax_type_data add ( constraint pttd_set_of_books_fk foreign key ( set_of_books_id ) references pwrplant.set_of_books );
alter table pwrplant.property_tax_type_data add ( constraint pttd_det_map_fk foreign key ( detail_map_id ) references pwrplant.pt_ledger_detail_map );

create index pwrplant.pttd_state_ndx on pwrplant.property_tax_type_data ( state_id ) tablespace pwrplant_idx;



-- pt_type_rollup
create table pwrplant.pt_type_rollup
   (  type_rollup_id    number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null,
      long_description  varchar2(254),
      status_code_id       number(22,0)   not null
   );

alter table pwrplant.pt_type_rollup add ( constraint pt_type_rollup_pk primary key ( type_rollup_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_type_rollup add ( constraint pt_rollup_status_fk foreign key ( status_code_id ) references pwrplant.status_code );



-- pt_type_rollup_values
create table pwrplant.pt_type_rollup_values
   (  type_rollup_id          number(22,0)   not null,
      type_rollup_value_id    number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254),
      external_code           varchar2(35)
   );

alter table pwrplant.pt_type_rollup_values add ( constraint pt_type_rollup_values_pk primary key ( type_rollup_id, type_rollup_value_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_type_rollup_values add ( constraint pt_rollup_val_rollup_fk foreign key ( type_rollup_id ) references pwrplant.pt_type_rollup );



-- pt_type_rollup_assign
create table pwrplant.pt_type_rollup_assign
   (  property_tax_type_id number(22,0)   not null,
      type_rollup_id          number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      type_rollup_value_id    number(22,0)   not null
   );

alter table pwrplant.pt_type_rollup_assign add ( constraint pt_type_rollup_assign_pk primary key ( property_tax_type_id, type_rollup_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_type_rollup_assign add ( constraint pt_rollup_asgn_pttd_fk foreign key ( property_tax_type_id ) references pwrplant.property_tax_type_data );
alter table pwrplant.pt_type_rollup_assign add ( constraint pt_rollup_asgn_rollup_val_fk foreign key ( type_rollup_id, type_rollup_value_id ) references pwrplant.pt_type_rollup_values ( type_rollup_id, type_rollup_value_id ) );


-- pt_company_tax_type
create table pwrplant.pt_company_tax_type
   (  prop_tax_company_id     number(22,0)   not null,
      property_tax_type_id    number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18)
   );

alter table pwrplant.pt_company_tax_type add ( constraint ptco_tax_type_pk primary key ( prop_tax_company_id, property_tax_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_company_tax_type add ( constraint ptco_tax_type_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_company_tax_type add ( constraint ptco_tax_type_ptt_fk foreign key ( property_tax_type_id ) references pwrplant.property_tax_type_data );


-- pt_ledger_detail
create table pwrplant.pt_ledger_detail
   (  ledger_detail_id        number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description_1           varchar2(254),
      description_2           varchar2(254),
      description_3           varchar2(254),
      unit_number          varchar2(50),
      serial_number           varchar2(50),
      make                    varchar2(50),
      model                   varchar2(50),
      model_year              varchar2(50),
      body_type               varchar2(50),
      fuel_type               varchar2(50),
      gross_weight            number(22,2),
      license_number          varchar2(50),
      license_weight          number(22,2),
      license_expiration         date,
      key_number              varchar2(50),
      primary_driver          varchar2(50),
      group_code              varchar2(50),
      length                  varchar2(50),
      width                   varchar2(50),
      height                  varchar2(50),
      construction_type       varchar2(50),
      stories                 varchar2(50),
      lease_number            varchar2(50),
      lease_start             date,
      lease_end               date,
      source_system           varchar2(50),
      external_code           varchar2(50),
      notes                   varchar2(4000)
   );

alter table pwrplant.pt_ledger_detail add ( constraint pt_ledger_detail_pk primary key ( ledger_detail_id ) using index tablespace pwrplant_idx );



-- pt_user_input_ledger
create table pwrplant.pt_user_input_ledger
   (  user_input        number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35),
      state_id          char(18)
   );

alter table pwrplant.pt_user_input_ledger add ( constraint pt_user_input_ledger_pk primary key ( user_input ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_user_input_ledger add ( constraint pt_ui_ldg_state_fk foreign key ( state_id ) references pwrplant.state );


-- pt_ledger
create table pwrplant.pt_ledger
   (  property_tax_ledger_id     number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      state_id                   char(18)       not null,
      prop_tax_location_id       number(22,0),
      parcel_id                     number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      company_id                 number(22,0)   not null,
      gl_account_id              number(22,0)   not null,
      bus_segment_id          number(22,0)   not null,
      utility_account_id            number(22,0),
      property_tax_type_id    number(22,0)   not null,
      vintage                    number(22,0),
      vintage_month              number(22,0),
      class_code_value1       varchar2(254),
      class_code_value2       varchar2(254),
      user_input                 number(22,0)   not null,
      ledger_detail_id           number(22,0),
      notes                      varchar2(4000)
   );

alter table pwrplant.pt_ledger add ( constraint pt_ledger_pk primary key ( property_tax_ledger_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_ptloc_fk foreign key ( prop_tax_location_id ) references pwrplant.prop_tax_location );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_co_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_gla_fk foreign key ( gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_bs_fk foreign key ( bus_segment_id ) references pwrplant.business_segment );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_ua_fk foreign key ( bus_segment_id, utility_account_id ) references pwrplant.utility_account ( bus_segment_id, utility_account_id ) );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_pttd_fk foreign key ( property_tax_type_id ) references pwrplant.property_tax_type_data );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_ldgdtl_fk foreign key ( ledger_detail_id ) references pwrplant.pt_ledger_detail );
alter table pwrplant.pt_ledger add ( constraint pt_ledger_ui_fk foreign key ( user_input ) references pwrplant.pt_user_input_ledger );

create unique index pwrplant.pt_ledger_unique_index on pwrplant.pt_ledger ( parcel_id, property_tax_type_id, gl_account_id, bus_segment_id, state_id, company_id, prop_tax_company_id, user_input, prop_tax_location_id, utility_account_id, vintage, vintage_month, class_code_value1, class_code_value2, ledger_detail_id ) tablespace pwrplant_idx;
create index pwrplant.ptl_prcl_ptt_co_gl_st_bs_ndx on pwrplant.pt_ledger ( prop_tax_location_id, parcel_id, property_tax_type_id, company_id, gl_account_id, state_id, bus_segment_id ) tablespace pwrplant_idx;
create index pwrplant.ptl_prcl_ndx on pwrplant.pt_ledger ( parcel_id ) tablespace pwrplant_idx;
create index pwrplant.pt_ledger_ldgdtl_ndx on pwrplant.pt_ledger ( ledger_detail_id ) tablespace pwrplant_idx;
create index pwrplant.pt_ledger_vint_ndx on pwrplant.pt_ledger ( vintage ) tablespace pwrplant_idx;



-- pt_ledger_tax_year
create table pwrplant.pt_ledger_tax_year
   (  property_tax_ledger_id           number(22,0)   not null,
      tax_year                         number(22,0)   not null,
      time_stamp                       date,
      user_id                          varchar2(18),
      beginning_book_balance           number(22,2)   default 0,
      book_additions                   number(22,2)   default 0,
      book_retirements                 number(22,2)   default 0,
      book_transfers_adj               number(22,2)   default 0,
      ending_book_balance              number(22,2)   default 0,
      cwip_balance                     number(22,2)   default 0,
      unit_cost_amount                 number(22,2)   default 0,
      beg_bal_adjustment               number(22,2)   default 0,
      additions_adjustment             number(22,2)   default 0,
      retirements_adjustment           number(22,2)   default 0,
      transfers_adjustment             number(22,2)   default 0,
      end_bal_adjustment               number(22,2)   default 0,
      cwip_adjustment                  number(22,2)   default 0,
      beginning_tax_basis              number(22,2)   default 0,
      tax_basis_additions              number(22,2)   default 0,
      tax_basis_retirements            number(22,2)   default 0,
      tax_basis_trans_adj              number(22,2)   default 0,
      allocated_tax_basis              number(22,2)   default 0,
      allocated_accum_ore              number(22,2)   default 0,
      beginning_tax_reserve            number(22,2)   default 0,
      tax_reserve_additions            number(22,2)   default 0,
      tax_reserve_retirements          number(22,2)   default 0,
      tax_reserve_trans_adj            number(22,2)   default 0,
      allocated_tax_reserve               number(22,2)   default 0,
      beg_tax_basis_adjustment         number(22,2)   default 0,
      tax_basis_adds_adjustment     number(22,2)   default 0,
      tax_basis_rets_adjustment        number(22,2)   default 0,
      tax_basis_trans_adjustment       number(22,2)   default 0,
      tax_basis_adjustment             number(22,2)   default 0,
      prop_tax_balance                 number(22,2)   default 0,
      associated_reserve               number(22,2)   default 0,
      calculated_reserve                  number(22,2)   default 0,
      reserve_adjustment               number(22,2)   default 0,
      final_reserve                    number(22,2)   default 0,
      escalated_prop_tax_balance    number(22,2)   default 0,
      escalated_prop_tax_reserve       number(22,2)   default 0,
      cost_approach_value              number(22,2)   default 0,
      market_value                     number(22,2)   default 0,
      quantity                         number(22,8),
      quantity_factored                number(22,8),
      allocation_statistic                number(22,8),
      notes                            varchar2(4000)
   );

alter table pwrplant.pt_ledger_tax_year add ( constraint pt_ledger_tax_year_pk primary key ( property_tax_ledger_id, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_ledger_tax_year add ( constraint pt_ledger_ty_ledger_fk foreign key ( property_tax_ledger_id ) references pwrplant.pt_ledger );
alter table pwrplant.pt_ledger_tax_year add ( constraint pt_ledger_ty_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );



-- property_tax_adjust_type
create table pwrplant.property_tax_adjust_type
   (  adjust_type_id    number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null,
      long_description  varchar2(254)
   );

alter table pwrplant.property_tax_adjust_type add ( constraint property_tax_adjust_type_pk primary key ( adjust_type_id ) using index tablespace pwrplant_idx );



-- property_tax_adjust
create table pwrplant.property_tax_adjust
   (  property_tax_adjust_id     number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      description                varchar2(35)   not null,
      adjust_type_id             number(22,0)   not null,
      duration_years             number(22,0)
   );

alter table pwrplant.property_tax_adjust add ( constraint property_tax_adjust_pk primary key ( property_tax_adjust_id ) using index tablespace pwrplant_idx );
alter table pwrplant.property_tax_adjust add ( constraint pt_adjust_adjust_type_fk foreign key ( adjust_type_id ) references pwrplant.property_tax_adjust_type );



-- pt_ledger_adjustment
create table pwrplant.pt_ledger_adjustment
   (  property_tax_ledger_id     number(22,0)   not null,
      property_tax_adjust_id     number(22,0)   not null,
      user_input                 number(22,0)   not null,
      tax_year                   number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      beg_bal_adjustment         number(22,2)   default 0,
      additions_adjustment       number(22,2)   default 0,
      retirements_adjustment     number(22,2)   default 0,
      transfers_adjustment       number(22,2)   default 0,
      end_bal_adjustment         number(22,2)   default 0,
      cwip_adjustment            number(22,2)   default 0,
      beg_tax_basis_adjustment   number(22,2)   default 0,
      tax_basis_adds_adjustment  number(22,2)   default 0,
      tax_basis_rets_adjustment  number(22,2)   default 0,
      tax_basis_trans_adjustment number(22,2)   default 0,
      tax_basis_adjustment       number(22,2)   default 0,
      reserve_adjustment         number(22,2)   default 0,
      notes                      varchar2(4000)
   );

alter table pwrplant.pt_ledger_adjustment add ( constraint pt_ledger_adjustment_pk primary key ( property_tax_ledger_id, property_tax_adjust_id, user_input, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_ledger_adjustment add ( constraint pt_ledger_adj_ledger_ty_fk foreign key ( property_tax_ledger_id, tax_year ) references pwrplant.pt_ledger_tax_year ( property_tax_ledger_id, tax_year ) );
alter table pwrplant.pt_ledger_adjustment add ( constraint pt_ledger_adj_ptadj_fk foreign key ( property_tax_adjust_id ) references pwrplant.property_tax_adjust );
alter table pwrplant.pt_ledger_adjustment add ( constraint pt_ledger_adjustment_ui_fk foreign key ( user_input ) references pwrplant.pt_user_input_ledger );

create index pwrplant.pt_ledger_adj_ptlty on pwrplant.pt_ledger_adjustment ( property_tax_ledger_id, tax_year ) tablespace pwrplant_idx;



-- pt_ledger_transfer
create table pwrplant.pt_ledger_transfer
   (  transfer_id             number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      from_ledger_id          number(22,0)   not null,
      to_ledger_id               number(22,0)   not null,
      tax_year                number(22,0)   not null,
      beg_bal_amount       number(22,2)   default 0,
      additions_amount        number(22,2)   default 0,
      retirements_amount      number(22,2)   default 0,
      transfers_amount        number(22,2)   default 0,
      end_bal_amount       number(22,2)   default 0,
      cwip_amount          number(22,2)   default 0,
      tax_basis_amount        number(22,2)   default 0,
      reserve_amount       number(22,2)   default 0,
      beg_bal_pct             number(22,8)   default 0,
      additions_pct           number(22,8)   default 0,
      retirements_pct         number(22,8)   default 0,
      transfers_pct           number(22,8)   default 0,
      end_bal_pct             number(22,8)   default 0,
      cwip_pct                number(22,8)   default 0,
      tax_basis_pct           number(22,8)   default 0,
      reserve_pct                number(22,8)   default 0,
      notes                   varchar2(4000)
   );

alter table pwrplant.pt_ledger_transfer add ( constraint pt_ledger_transfer_pk primary key ( transfer_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_ledger_transfer add ( constraint pt_ledger_xfer_from_fk foreign key ( from_ledger_id, tax_year ) references pwrplant.pt_ledger_tax_year ( property_tax_ledger_id, tax_year ) );
alter table pwrplant.pt_ledger_transfer add ( constraint pt_ledger_xfer_to_fk foreign key ( to_ledger_id, tax_year ) references pwrplant.pt_ledger_tax_year ( property_tax_ledger_id, tax_year ) );

create index pwrplant.pt_ledger_xfer_from_ndx on pwrplant.pt_ledger_transfer ( from_ledger_id, tax_year ) tablespace pwrplant_idx;
create index pwrplant.pt_ledger_xfer_to_ndx on pwrplant.pt_ledger_transfer ( to_ledger_id, tax_year ) tablespace pwrplant_idx;



-- pt_preallo_ledger
create table pwrplant.pt_preallo_ledger
   (  preallo_ledger_id          number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year                   number(22,0)   not null,
      state_id                   char(18)       not null,
      prop_tax_location_id       number(22,0),
      parcel_id                     number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      company_id                 number(22,0)   not null,
      gl_account_id              number(22,0)   not null,
      bus_segment_id          number(22,0)   not null,
      utility_account_id            number(22,0),
      property_tax_type_id    number(22,0)   not null,
      depr_group_id              number(22,0),
      vintage                    number(22,0),
      vintage_month              number(22,0),
      class_code_value1       varchar2(254),
      class_code_value2       varchar2(254),
      ledger_detail_id           number(22,0),
      user_input                 number(22,0)   not null,
      beginning_book_balance     number(22,2)   default 0,
      book_additions             number(22,2)   default 0,
      book_retirements           number(22,2)   default 0,
      book_transfers_adj         number(22,2)   default 0,
      ending_book_balance        number(22,2)   default 0,
      cwip_balance               number(22,2)   default 0,
      quantity                   number(22,8),
      depr_factor                number(22,8),
      notes                      varchar2(4000)
   );

alter table pwrplant.pt_preallo_ledger add ( constraint pt_preallo_ledger_pk primary key ( preallo_ledger_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_ptloc_fk foreign key ( prop_tax_location_id ) references pwrplant.prop_tax_location );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_co_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_gla_fk foreign key ( gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_bs_fk foreign key ( bus_segment_id ) references pwrplant.business_segment );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_ua_fk foreign key ( bus_segment_id, utility_account_id ) references pwrplant.utility_account ( bus_segment_id, utility_account_id ) );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_pttd_fk foreign key ( property_tax_type_id ) references pwrplant.property_tax_type_data );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_dg_fk foreign key ( depr_group_id ) references pwrplant.depr_group );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_pre_ldg_ldgdtl_fk foreign key ( ledger_detail_id ) references pwrplant.pt_ledger_detail );
alter table pwrplant.pt_preallo_ledger add ( constraint pt_preallo_ledger_ui_fk foreign key ( user_input ) references pwrplant.pt_user_input_ledger );

create index pwrplant.pt_pre_ldg_co_st_ty_ptt_ndx on pwrplant.pt_preallo_ledger ( prop_tax_location_id, property_tax_type_id, company_id, state_id, tax_year ) tablespace pwrplant_idx;
create index pwrplant.pt_preallo_ledger_prcl_ndx on pwrplant.pt_preallo_ledger ( parcel_id ) tablespace pwrplant_idx;
create index pwrplant.pt_pre_ldg_ptt_co_ty_st_ndx on pwrplant.pt_preallo_ledger ( property_tax_type_id, company_id, tax_year, state_id ) tablespace pwrplant_idx;
create index pwrplant.pt_preallo_ledger_vint_ndx on pwrplant.pt_preallo_ledger ( vintage ) tablespace pwrplant_idx;
create index pwrplant.pt_pre_ldg_ldg_dtl_ndx on pwrplant.pt_preallo_ledger ( ledger_detail_id ) tablespace pwrplant_idx;



-- pt_preallo_adjustment
create table pwrplant.pt_preallo_adjustment
   (  preallo_ledger_id          number(22,0)   not null,
      property_tax_adjust_id     number(22,0)   not null,
      user_input                 number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      beg_bal_adjustment         number(22,2)   default 0,
      additions_adjustment       number(22,2)   default 0,
      retirements_adjustment     number(22,2)   default 0,
      transfers_adjustment       number(22,2)   default 0,
      end_bal_adjustment         number(22,2)   default 0,
      cwip_adjustment            number(22,2)   default 0,
      notes                      varchar2(4000)
   );

alter table pwrplant.pt_preallo_adjustment add ( constraint pt_preallo_adjustment_pk primary key ( preallo_ledger_id, property_tax_adjust_id, user_input ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_preallo_adjustment add ( constraint pt_preallo_adj_preallo_fk foreign key ( preallo_ledger_id ) references pwrplant.pt_preallo_ledger );
alter table pwrplant.pt_preallo_adjustment add ( constraint pt_preallo_adj_ptadj_fk foreign key ( property_tax_adjust_id ) references pwrplant.property_tax_adjust );
alter table pwrplant.pt_preallo_adjustment add ( constraint pt_preallo_adjustment_ui_fk foreign key ( user_input ) references pwrplant.pt_user_input_ledger );

create index pwrplant.pt_preallo_adj_preallo_ndx on pwrplant.pt_preallo_adjustment ( preallo_ledger_id ) tablespace pwrplant_idx;



-- pt_preallo_transfer
create table pwrplant.pt_preallo_transfer
   (  transfer_id          number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      from_preallo_id      number(22,0)   not null,
      to_preallo_id        number(22,0)   not null,
      beg_bal_amount    number(22,2)   default 0,
      additions_amount     number(22,2)   default 0,
      retirements_amount   number(22,2)   default 0,
      transfers_amount     number(22,2)   default 0,
      end_bal_amount    number(22,2)   default 0,
      cwip_amount       number(22,2)   default 0,
      beg_bal_pct          number(22,8)   default 0,
      additions_pct        number(22,8)   default 0,
      retirements_pct      number(22,8)   default 0,
      transfers_pct        number(22,8)   default 0,
      end_bal_pct          number(22,8)   default 0,
      cwip_pct             number(22,8)   default 0,
      notes                varchar2(4000)
   );

alter table pwrplant.pt_preallo_transfer add ( constraint pt_preallo_transfer_pk primary key ( transfer_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_preallo_transfer add ( constraint pt_preallo_xfer_from_fk foreign key ( from_preallo_id ) references pwrplant.pt_preallo_ledger );
alter table pwrplant.pt_preallo_transfer add ( constraint pt_preallo_xfer_to_fk foreign key ( to_preallo_id ) references pwrplant.pt_preallo_ledger );

create index pwrplant.pt_preallo_xfer_from_ndx on pwrplant.pt_preallo_transfer ( from_preallo_id ) tablespace pwrplant_idx;
create index pwrplant.pt_preallo_xfer_to_ndx on pwrplant.pt_preallo_transfer ( to_preallo_id ) tablespace pwrplant_idx;



-- pt_case_control
create table pwrplant.pt_case_control
   (  prop_tax_company_id        number(22,0)   not null,
      state_id                      char(18)       not null,
      case_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      calc_assessment               date,
      calc_liability                   date,
      calc_statements               date,
      lock_case                     date,
      calc_tax_from_prior_yr_bills  number(22,0)   not null,
      liability_inflation_pct          number(22,8),
      assessment_year_id            number(22,0)
   );

alter table pwrplant.pt_case_control add ( constraint pt_case_control_pk primary key ( prop_tax_company_id, state_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_case_control add ( constraint pt_case_cntl_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_case_control add ( constraint pt_cs_cntl_pt_comp_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_case_control add ( constraint pt_case_cntl_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_case_control add ( constraint pt_case_cntl_ay_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );



-- pt_district_equalization
create table pwrplant.pt_district_equalization
   (  tax_district_id         number(22,0)   not null,
      case_id              number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      equalization_factor  number(22,8)   not null
   );

alter table pwrplant.pt_district_equalization add ( constraint pt_district_equalization_pk primary key ( tax_district_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_district_equalization add ( constraint pt_dist_eq_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_district_equalization add ( constraint pt_dist_eq_tax_dist_fk foreign key ( tax_district_id ) references pwrplant.prop_tax_district );



-- pt_case_calc
create table pwrplant.pt_case_calc
   (  property_tax_ledger_id  number(22,0)   not null,
      case_id                 number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      prop_tax_balance        number(22,2),
      final_reserve           number(22,2),
      cost_approach_value     number(22,2),
      market_value            number(22,2),
      assessment              number(22,2),
      equalized_value         number(22,2),
      taxable_value           number(22,2),
      tax_liability              number(22,2),
      quantity_factored       number(22,8)
   );

alter table pwrplant.pt_case_calc add ( constraint pt_case_calc_pk primary key ( property_tax_ledger_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_case_calc add ( constraint pt_case_calc_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_case_calc add ( constraint pt_case_calc_ledger_fk foreign key ( property_tax_ledger_id ) references pwrplant.pt_ledger );



-- pt_case_calc_authority
create table pwrplant.pt_case_calc_authority
   (  property_tax_ledger_id  number(22,0)   not null,
      tax_authority_id        number(22,0)   not null,
      levy_class_id           number(22,0)   not null,
      case_id                 number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      prop_tax_balance        number(22,2),
      final_reserve           number(22,2),
      cost_approach_value     number(22,2),
      market_value            number(22,2),
      assessment              number(22,2),
      equalized_value         number(22,2),
      taxable_value           number(22,2),
      tax_liability              number(22,2),
      quantity_factored       number(22,8)
   );

alter table pwrplant.pt_case_calc_authority add ( constraint pt_case_calc_authority_pk primary key ( property_tax_ledger_id, tax_authority_id, levy_class_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_case_calc_authority add ( constraint pt_case_calc_auth_auth_fk foreign key ( tax_authority_id ) references pwrplant.property_tax_authority );
alter table pwrplant.pt_case_calc_authority add ( constraint pt_case_calc_auth_calc_fk foreign key ( property_tax_ledger_id, case_id ) references pwrplant.pt_case_calc ( property_tax_ledger_id, case_id ) );
alter table pwrplant.pt_case_calc_authority add ( constraint pt_case_calc_auth_levy_clss_fk foreign key ( levy_class_id ) references pwrplant.pt_levy_class );

create index pwrplant.pt_case_cal_au_cid_lid_aid_idx on pwrplant.pt_case_calc_authority ( property_tax_ledger_id, tax_authority_id, case_id ) tablespace pwrplant_idx;
create index pwrplant.pt_cca_ta_ndx on pwrplant.pt_case_calc_authority ( tax_authority_id ) tablespace pwrplant_idx;
create index pwrplant.pt_cca_cc_ndx on pwrplant.pt_case_calc_authority ( property_tax_ledger_id, case_id ) tablespace pwrplant_idx;



-- pt_taxable_value_rate
create table pwrplant.pt_taxable_value_rate
   (  pt_rate_id           number(22,0)   not null,
      case_id              number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      taxable_value_rate   number(22,8)   not null
   );

alter table pwrplant.pt_taxable_value_rate add ( constraint pt_tax_val_rate_pk primary key ( pt_rate_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_taxable_value_rate add ( constraint pt_tv_rate_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_taxable_value_rate add ( constraint pt_tv_rate_rate_fk foreign key ( pt_rate_id ) references pwrplant.pt_rate_definition );



-- pt_assessment_group_case
create table pwrplant.pt_assessment_group_case
   (  assessment_group_id              number(22,0)   not null,
      prop_tax_company_id           number(22,0)   not null,
      state_id                         char(18)       not null,
      case_id                          number(22,0)   not null,
      time_stamp                       date,
      user_id                          varchar2(18),
      allocate_assessment_type_id      number(22,0)   not null,
      assessed_value_allocation_id     number(22,0)   not null,
      assessment_rate_id               number(22,0),
      eq_factor_type_id                number(22,0),
      taxable_value_rate_id            number(22,0),
      unit_assessment                  number(22,2)
   );

alter table pwrplant.pt_assessment_group_case add ( constraint pt_assessment_group_case_pk primary key ( assessment_group_id, prop_tax_company_id, state_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_ag_fk foreign key ( assessment_group_id ) references pwrplant.pt_assessment_group );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_allo_asmt_ty_fk foreign key ( allocate_assessment_type_id ) references pwrplant.pt_allocate_assessment_type );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_asd_val_allo_fk foreign key ( assessed_value_allocation_id ) references pwrplant.pt_assessed_value_allocation );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_asmt_rate_fk foreign key ( assessment_rate_id ) references pwrplant.pt_rate_definition );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_eq_fctr_fk foreign key ( eq_factor_type_id ) references pwrplant.pt_equalization_factor_type );
alter table pwrplant.pt_assessment_group_case add ( constraint pt_asmt_grp_cs_taxval_rate_fk foreign key ( taxable_value_rate_id ) references pwrplant.pt_rate_definition );



-- pt_assessed_value_tax_district
create table pwrplant.pt_assessed_value_tax_district
   (  assessment_group_id        number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      state_id                   char(18)       not null,
      tax_district_id               number(22,0)   not null,
      case_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      unit_assessment            number(22,2)   not null
   );

alter table pwrplant.pt_assessed_value_tax_district add ( constraint pt_assessed_value_tax_dist_pk primary key ( assessment_group_id, prop_tax_company_id, state_id, tax_district_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_assessed_value_tax_district add ( constraint pt_asd_val_td_asmt_gp_cs_fk foreign key ( assessment_group_id, prop_tax_company_id, state_id, case_id ) references pwrplant.pt_assessment_group_case ( assessment_group_id, prop_tax_company_id, state_id, case_id ) );
alter table pwrplant.pt_assessed_value_tax_district add ( constraint pt_asd_val_td_td_fk foreign key ( tax_district_id ) references pwrplant.prop_tax_district );



-- pt_assessed_value_county
create table pwrplant.pt_assessed_value_county
   (  assessment_group_id        number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      state_id                   char(18)       not null,
      county_id                  char(18)       not null,
      case_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      unit_assessment            number(22,2)   not null
   );

alter table pwrplant.pt_assessed_value_county add ( constraint pt_assessed_value_county_pk primary key ( assessment_group_id, prop_tax_company_id, state_id, county_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_assessed_value_county add ( constraint pt_asd_val_cnty_asmt_gp_cs_fk foreign key ( assessment_group_id, prop_tax_company_id, state_id, case_id ) references pwrplant.pt_assessment_group_case ( assessment_group_id, prop_tax_company_id, state_id, case_id ) );
alter table pwrplant.pt_assessed_value_county add ( constraint pt_asd_val_cnty_cnty_fk foreign key ( state_id, county_id ) references pwrplant.county ( state_id, county_id ) );



-- pt_assessment_group_types
create table pwrplant.pt_assessment_group_types
   (  property_tax_type_id    number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      state_id                   char(18)       not null,
      case_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      assessment_group_id        number(22,0)
   );

alter table pwrplant.pt_assessment_group_types add ( constraint pt_assessment_group_types_pk primary key ( property_tax_type_id, prop_tax_company_id, state_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_assessment_group_types add ( constraint pt_asmt_grp_type_pttd_fk foreign key ( property_tax_type_id ) references pwrplant.property_tax_type_data );
alter table pwrplant.pt_assessment_group_types add ( constraint pt_asmt_grp_type_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_assessment_group_types add ( constraint pt_asmt_grp_type_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_assessment_group_types add ( constraint pt_asmt_grp_type_cs_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_assessment_group_types add ( constraint pt_asmt_grp_type_ag_fk foreign key ( assessment_group_id ) references pwrplant.pt_assessment_group );



-- pt_levy_rate
create table pwrplant.pt_levy_rate
   (  tax_authority_id           number(22,0)   not null,
      levy_class_id              number(22,0)   not null,
      case_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      mill_levy_rate             number(22,12),
      full_mill_levy_rate           number(22,12),
      unit_rate                     number(22,8),
      threshold_amount           number(22,2),
      mill_levy_rate_over_thold  number(22,12)
   );

alter table pwrplant.pt_levy_rate add ( constraint pt_levy_rate_pk primary key ( tax_authority_id, levy_class_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_levy_rate add ( constraint pt_levy_rate_tax_auth_fk foreign key ( tax_authority_id ) references pwrplant.property_tax_authority );
alter table pwrplant.pt_levy_rate add ( constraint pt_levy_rate_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_levy_rate add ( constraint pt_levy_rate_levy_class_fk foreign key ( levy_class_id ) references pwrplant.pt_levy_class );

create index pwrplant.pt_levy_rate_ta_ndx on pwrplant.pt_levy_rate ( tax_authority_id ) tablespace pwrplant_idx;



-- pt_county_equalization
create table pwrplant.pt_county_equalization
   (  state_id             char(18)       not null,
      county_id            char(18)       not null,
      case_id              number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      equalization_factor  number(22,8)   not null
   );

alter table pwrplant.pt_county_equalization add ( constraint pt_county_equalization_pk primary key ( state_id, county_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_county_equalization add ( constraint pt_cnty_equal_case_fk foreign key ( case_id ) references pwrplant.pt_case );
alter table pwrplant.pt_county_equalization add ( constraint pt_cnty_equal_cnty_fk foreign key ( county_id, state_id ) references pwrplant.county ( county_id, state_id ) );



-- pt_accrual_charge
create table pwrplant.pt_accrual_charge
   (  property_tax_ledger_id        number(22,0) not null,
      tax_authority_id              number(22,0) not null,
      assessment_year_id            number(22,0) not null,
      accrual_type_id               number(22,0) not null,
      gl_monthnum                number(22,0) not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      calculated_tax_liability         number(22,2),
      billed_tax_liability             number(22,2),
      calculated_monthly_accrual    number(22,2),
      billed_monthly_accrual        number(22,2),
      trueup_amount                 number(22,2),
      total_monthly_charge       number(22,2),
      cumulative_charge          number(22,2),
      deferred_amount               number(22,2),
      debit_gl_account_id           number(22,0),
      credit_gl_account_id          number(22,0),
      deferred_gl_account_id        number(22,0)
   );

alter table pwrplant.pt_accrual_charge add ( constraint pt_accrual_charge_pk primary key ( property_tax_ledger_id, tax_authority_id, assessment_year_id, accrual_type_id, gl_monthnum ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_charge add ( constraint pt_accr_chg_asmt_yr_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );
alter table pwrplant.pt_accrual_charge add ( constraint pt_accr_chg_auth_fk foreign key ( tax_authority_id ) references pwrplant.property_tax_authority );
alter table pwrplant.pt_accrual_charge add ( constraint pt_accr_chg_ledger_fk foreign key ( property_tax_ledger_id ) references pwrplant.pt_ledger );
alter table pwrplant.pt_accrual_charge add ( constraint pt_accr_chg_debitgl_fk foreign key ( debit_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_charge add ( constraint pt_accr_chg_creditgl_fk foreign key ( credit_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_charge add ( constraint pt_accr_chg_defergl_fk foreign key ( deferred_gl_account_id ) references pwrplant.gl_account );

create index pwrplant.pt_accrual_charge_ta_ndx on pwrplant.pt_accrual_charge ( tax_authority_id ) tablespace pwrplant_idx;



-- pt_accrual_year_control
create table pwrplant.pt_accrual_year_control
   (  assessment_year_id         number(22,0)   not null,
      gl_monthnum             number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      current_accrual_case_id    number(22,0)   not null
   );

alter table pwrplant.pt_accrual_year_control add ( constraint pt_accrual_year_control_pk primary key ( assessment_year_id, gl_monthnum ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_year_control add ( constraint pt_accr_yr_cntl_asmtyr_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );
alter table pwrplant.pt_accrual_year_control add ( constraint pt_accr_yr_cntl_case_fk foreign key ( current_accrual_case_id ) references pwrplant.pt_case );



-- pt_statement
create table pwrplant.pt_statement
   (  statement_id      number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(100)  not null,
      long_description  varchar2(254),
      account_number varchar2(35),
      state_id          char(18)       not null,
      county_id         char(18),
      notes             varchar2(254)
   );

alter table pwrplant.pt_statement add ( constraint pt_statement_pk primary key ( statement_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement add ( constraint pt_statement_county_fk foreign key ( county_id, state_id ) references pwrplant.county ( county_id, state_id ) );



-- pt_statement_year
create table pwrplant.pt_statement_year
   (  statement_year_id number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      year                 number(22,0)   not null
   );

alter table pwrplant.pt_statement_year add ( constraint pt_statement_year_pk primary key ( statement_year_id ) using index tablespace pwrplant_idx );



-- pt_statement_status
create table pwrplant.pt_statement_status
   (  statement_status_id  number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_statement_status add ( constraint pt_statement_status_pk primary key ( statement_status_id ) using index tablespace pwrplant_idx );



-- pt_statement_verified_status
create table pwrplant.pt_statement_verified_status
   (  verified_status_id      number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_statement_verified_status add ( constraint pt_statement_verified_stat_fk primary key ( verified_status_id ) using index tablespace pwrplant_idx );



-- pt_payment_status
create table pwrplant.pt_payment_status
   (  payment_status_id number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      include_amount    number(22,0) not null
   );

alter table pwrplant.pt_payment_status add ( constraint pt_payment_status_pk primary key ( payment_status_id ) using index tablespace pwrplant_idx );



-- pt_schedule
create table pwrplant.pt_schedule
   (  schedule_id       number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null,
      long_description  varchar2(254),
      state_id          char(18) not null
   );

alter table pwrplant.pt_schedule add ( constraint pt_schedule_pk primary key ( schedule_id ) using index tablespace pwrplant_idx );



-- pt_installment_type
create table pwrplant.pt_installment_type
   (  installment_type_id     number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null
   );

alter table pwrplant.pt_installment_type add ( constraint pt_installment_type_pk primary key ( installment_type_id ) using index tablespace pwrplant_idx );



-- pt_schedule_installment
create table pwrplant.pt_schedule_installment
   (  schedule_id             number(22,0)   not null,
      statement_year_id    number(22,0)   not null,
      installment_id          number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      due_date             date           not null,
      percent_due          number(22,8)   not null,
      installment_type_id     number(22, 0)  not null,
      installment_number      number(22,0)
   );

alter table pwrplant.pt_schedule_installment add ( constraint pt_schedule_installment_pk primary key ( schedule_id, statement_year_id, installment_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_schedule_installment add ( constraint pt_sched_install_stmt_yr_fk foreign key ( statement_year_id ) references pwrplant.pt_statement_year );
alter table pwrplant.pt_schedule_installment add ( constraint pt_sched_install_sched_fk foreign key ( schedule_id ) references pwrplant.pt_schedule );
alter table pwrplant.pt_schedule_installment add ( constraint pt_sched_install_inst_type_fk foreign key ( installment_type_id ) references pwrplant.pt_installment_type );



-- pt_period_type
create table pwrplant.pt_period_type
   (  period_type_id    number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null
   );

alter table pwrplant.pt_period_type add ( constraint pt_period_type_pk primary key ( period_type_id ) using index tablespace pwrplant_idx );



-- pt_schedule_period
create table pwrplant.pt_schedule_period
   (  schedule_id          number(22,0)   not null,
      statement_year_id number(22,0)   not null,
      installment_id       number(22,0)   not null,
      period_id               number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      period_type_id       number(22,0)   not null,
      start_date           date,
      end_date          date,
      percent_due       number(22,8)   not null
   );

alter table pwrplant.pt_schedule_period add ( constraint pt_schedule_period_pk primary key ( schedule_id, statement_year_id, installment_id, period_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_schedule_period add ( constraint pt_sched_period_stmt_yr_fk foreign key ( statement_year_id ) references pwrplant.pt_statement_year );
alter table pwrplant.pt_schedule_period add ( constraint pt_sched_period_install_fk foreign key ( schedule_id, statement_year_id, installment_id ) references pwrplant.pt_schedule_installment ( schedule_id, statement_year_id, installment_id ) );
alter table pwrplant.pt_schedule_period add ( constraint pt_sched_period_prd_type_fk foreign key ( period_type_id ) references pwrplant.pt_period_type );



-- pt_statement_group_schedule
create table pwrplant.pt_statement_group_schedule
   (  statement_group_id      number(22,0)   not null,
      statement_year_id    number(22,0)   not null,
      schedule_id             number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18)
   );

alter table pwrplant.pt_statement_group_schedule add ( constraint pt_statement_group_schedule_pk primary key ( statement_group_id, statement_year_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_group_schedule add ( constraint pt_stmt_grp_sched_stmt_grp_fk foreign key ( statement_group_id ) references pwrplant.pt_statement_group );
alter table pwrplant.pt_statement_group_schedule add ( constraint pt_stmt_grp_sched_stmt_yr_fk foreign key ( statement_year_id ) references pwrplant.pt_statement_year );
alter table pwrplant.pt_statement_group_schedule add ( constraint pt_stmt_grp_sched_schedule_fk foreign key ( schedule_id ) references pwrplant.pt_schedule );



-- pt_statement_group_payment
create table pwrplant.pt_statement_group_payment
   (  payment_id                       number(22,0)   not null,
      time_stamp                       date,
      user_id                          varchar2(18),
      statement_group_id               number(22,0)   not null,
      statement_year_id                number(22,0)   not null,
      schedule_id                      number(22,0)   not null,
      installment_id                   number(22,0)   not null,
      period_id                           number(22,0)   not null,
      statement_payee_id               number(22,0)   not null,
      assessed_value_allocation_id     number(22,0)   not null,
      payment_status_id                number(22,0)   not null,
      amount_paid                   number(22,2)   not null,
      tax_amount                       number(22,2),
      credit_amount                    number(22,2),
      penalty_amount                number(22,2),
      interest_amount                  number(22,2),
      protest_amount                number(22,2),
      payment_enterer                  varchar2(18)   not null,
      payment_entry_date               date not null,
      payment_approval_date            date,
      payment_releaser                 varchar2(18),
      payment_release_date          date,
      paid_date                        date,
      certified_mail_date              date,
      certified_mail_number            varchar2(35),
      check_number                     varchar2(35),
      voucher_number                varchar2(35),
      scheduled_pay_date               date,
      sent_to_ap_indicator                number(22,0),
      ap_reference_number           varchar2(35),
      override_release_date            date,
      separate_checks_payment_id    number(22,0),
      voider                              varchar2(18),
      voided_date                      date,
      special_instructions             varchar2(254),
      notes                            varchar2(2000)
   );

alter table pwrplant.pt_statement_group_payment add ( constraint pt_statement_group_payment_pk primary key ( payment_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_group_payment add ( constraint pt_stmt_grp_pymt_stmt_grp_fk foreign key ( statement_group_id ) references pt_statement_group );
alter table pwrplant.pt_statement_group_payment add ( constraint pt_stmt_grp_pymt_stmt_yr_fk foreign key ( statement_year_id ) references pt_statement_year );
alter table pwrplant.pt_statement_group_payment add ( constraint pt_stmt_grp_pymt_sched_prd_fk foreign key ( schedule_id, statement_year_id, installment_id, period_id ) references pwrplant.pt_schedule_period ( schedule_id, statement_year_id, installment_id, period_id ) );
alter table pwrplant.pt_statement_group_payment add ( constraint pt_stmt_grp_pymt_payee_fk foreign key ( statement_payee_id ) references pwrplant.pt_statement_payee );
alter table pwrplant.pt_statement_group_payment add ( constraint pt_sgp_ava_fk foreign key ( assessed_value_allocation_id ) references pwrplant.pt_assessed_value_allocation );
alter table pwrplant.pt_statement_group_payment add ( constraint pt_stmt_grp_pymt_pymt_stat_fk foreign key ( payment_status_id ) references pwrplant.pt_payment_status );
alter table pwrplant.pt_statement_group_payment add ( constraint pt_sg_pymt_sep_chk_pymt_fk foreign key ( separate_checks_payment_id ) references pwrplant.pt_statement_group_payment );



-- pt_statement_payment_approval
create table pwrplant.pt_statement_payment_approval
   (  payment_id           number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      authorizer1          varchar2(18),
      authorized_date1     date,
      authorizer2          varchar2(18),
      authorized_date2     date,
      authorizer3          varchar2(18),
      authorized_date3     date,
      authorizer4          varchar2(18),
      authorized_date4     date,
      authorizer5          varchar2(18),
      authorized_date5     date,
      authorizer6          varchar2(18),
      authorized_date6     date,
      authorizer7          varchar2(18),
      authorized_date7     date,
      authorizer8          varchar2(18),
      authorized_date8     date,
      authorizer9          varchar2(18),
      authorized_date9     date,
      authorizer10         varchar2(18),
      authorized_date10 date,
      rejector             varchar2(18),
      rejected_date        date,
      notes                varchar2(2000)
   );

alter table pwrplant.pt_statement_payment_approval add ( constraint pt_statement_payment_approv_fk primary key ( payment_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_payment_approval add ( constraint pt_stmt_pymt_appv_pymt_fk foreign key ( payment_id ) references pwrplant.pt_statement_group_payment );



-- pt_statement_statement_year
create table pwrplant.pt_statement_statement_year
   (  statement_id            number(22,0)   not null,
      statement_year_id    number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      assessment_year_id      number(22,0)   not null,
      statement_group_id      number(22,0)   not null,
      statement_status_id     number(22,0)   not null,
      statement_number     varchar2(35),
      received_date           date,
      tax_amount              number(22,2) default 0,
      credit_amount           number(22,2) default 0,
      penalty_amount       number(22,2) default 0,
      interest_amount         number(22,2) default 0,
      estimated_switch_yn     number(22,0),
      notes                   varchar2(2000)
   );

alter table pwrplant.pt_statement_statement_year add ( constraint pt_statement_statement_year_pk primary key ( statement_id, statement_year_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_statement_year add ( constraint pt_stmt_stmt_yr_stmt_fk foreign key ( statement_id ) references pwrplant.pt_statement );
alter table pwrplant.pt_statement_statement_year add ( constraint pt_stmt_stmt_yr_asmt_yr_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );
alter table pwrplant.pt_statement_statement_year add ( constraint pt_stmt_stmt_yr_stmt_yr_fk foreign key ( statement_year_id ) references pwrplant.pt_statement_year );
alter table pwrplant.pt_statement_statement_year add ( constraint pt_stmt_stmt_yr_stmt_grp_fk foreign key ( statement_group_id ) references pwrplant.pt_statement_group );
alter table pwrplant.pt_statement_statement_year add ( constraint pt_stmt_stmt_yr_stmt_status_fk foreign key ( statement_status_id ) references pwrplant.pt_statement_status );

create index pwrplant.pt_ssy_statement_ndx on pwrplant.pt_statement_statement_year ( statement_id ) tablespace pwrplant_idx;



-- pt_statement_installment
create table pwrplant.pt_statement_installment
   (  statement_id         number(22,0)   not null,
      statement_year_id number(22,0)   not null,
      schedule_id          number(22,0)   not null,
      installment_id       number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      amount_due        number(22,2)   not null,
      tax_amount           number(22,2),
      credit_amount        number(22,2),
      penalty_amount    number(22,2),
      interest_amount      number(22,2),
      protest_amount    number(22,2),
      due_date          date
   );

alter table pwrplant.pt_statement_installment add ( constraint pt_statement_installment_pk primary key ( statement_id, statement_year_id, schedule_id, installment_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_installment add ( constraint pt_stmt_nstl_stmt_stmt_yr_fk foreign key ( statement_id, statement_year_id ) references pwrplant.pt_statement_statement_year ( statement_id, statement_year_id ) );
alter table pwrplant.pt_statement_installment add ( constraint pt_stmt_nstl_stmt_install_fk foreign key ( schedule_id, statement_year_id, installment_id ) references pwrplant.pt_schedule_installment ( schedule_id, statement_year_id, installment_id ) );

create index pwrplant.pt_si_stmt_stmt_yr_ndx on pwrplant.pt_statement_installment ( statement_id, statement_year_id ) tablespace pwrplant_idx;



-- pt_statement_period
create table pwrplant.pt_statement_period
   (  statement_id         number(22,0)   not null,
      statement_year_id    number(22,0)   not null,
      schedule_id          number(22,0)   not null,
      installment_id       number(22,0)   not null,
      period_id               number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      amount_due        number(22,2)   not null,
      tax_amount           number(22,2),
      credit_amount        number(22,2),
      penalty_amount    number(22,2),
      interest_amount      number(22,2),
      protest_amount    number(22,2)
   );

alter table pwrplant.pt_statement_period add ( constraint pt_statement_period_pk primary key ( statement_id, statement_year_id, schedule_id, installment_id, period_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_period add ( constraint pt_stmt_prd_stmt_stmt_yr_fk foreign key ( statement_id, statement_year_id ) references pwrplant.pt_statement_statement_year ( statement_id, statement_year_id ) );
alter table pwrplant.pt_statement_period add ( constraint pt_stmt_prd_sched_prd_fk foreign key ( schedule_id, statement_year_id, installment_id, period_id ) references pwrplant.pt_schedule_period ( schedule_id, statement_year_id, installment_id, period_id ) );

create index pwrplant.pt_sp_stmt_stmt_yr_ndx on pwrplant.pt_statement_period ( statement_id, statement_year_id ) tablespace pwrplant_idx;



-- pt_statement_line
create table pwrplant.pt_statement_line
   (  statement_id            number(22,0)   not null,
      statement_year_id       number(22,0)   not null,
      line_id                 number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      tax_authority_id        number(22,0)   not null,
      levy_class_id           number(22,0)   not null,
      parcel_id                  number(22,0) not null,
      verified_status_id         number(22,0)   not null,
      assessment              number(22,2),
      taxable_value           number(22,2),
      quantity                number(22,8),
      tax_amount              number(22,2),
      credit_amount           number(22,2),
      penalty_amount       number(22,2),
      interest_amount         number(22,2),
      calculated_amount    number(22,2),
      first_install_yn           number(22,0),
      notes                   varchar2(2000)
   );

alter table pwrplant.pt_statement_line add ( constraint pt_statement_line_pk primary key ( statement_id, statement_year_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_line add ( constraint pt_stmt_line_stmt_stmt_yr_fk foreign key ( statement_id, statement_year_id ) references pwrplant.pt_statement_statement_year ( statement_id, statement_year_id ) );
alter table pwrplant.pt_statement_line add ( constraint pt_stmt_line_tax_authority_fk foreign key ( tax_authority_id ) references pwrplant.property_tax_authority );
alter table pwrplant.pt_statement_line add ( constraint pt_stmt_line_levy_class_fk foreign key ( levy_class_id ) references pwrplant.pt_levy_class );
alter table pwrplant.pt_statement_line add ( constraint pt_stmt_line_parcel_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_statement_line add ( constraint pt_stmt_line_verify_status_fk foreign key ( verified_status_id ) references pwrplant.pt_statement_verified_status );

create index pwrplant.pt_sl_stmt_stmt_yr_ndx on pwrplant.pt_statement_line ( statement_id, statement_year_id ) tablespace pwrplant_idx;
create index pwrplant.pt_statement_line_ta_ndx on pwrplant.pt_statement_line ( tax_authority_id ) tablespace pwrplant_idx;



-- pt_statement_line_payment
create table pwrplant.pt_statement_line_payment
   (  statement_id         number(22,0)   not null,
      statement_year_id    number(22,0)   not null,
      line_id              number(22,0)   not null,
      payment_id           number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      paid_amount       number(22,2)   not null,
      tax_amount           number(22,2),
      credit_amount        number(22,2),
      penalty_amount    number(22,2),
      interest_amount      number(22,2),
      protest_amount    number(22,2),
      notes                varchar2(2000)
   );

alter table pwrplant.pt_statement_line_payment add ( constraint pt_statement_line_payment_pk primary key ( statement_id, statement_year_id, line_id, payment_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statement_line_payment add ( constraint pt_stmt_line_pymt_line_fk foreign key ( statement_id, statement_year_id, line_id ) references pwrplant.pt_statement_line ( statement_id, statement_year_id, line_id ) );
alter table pwrplant.pt_statement_line_payment add ( constraint pt_stmt_line_pymt_pymt_fk foreign key ( payment_id ) references pwrplant.pt_statement_group_payment );

create index pwrplant.pt_slp_payment_ndx on pwrplant.pt_statement_line_payment ( payment_id ) tablespace pwrplant_idx;
create index pwrplant.pt_slp_stmt_stmt_yr_ndx on pwrplant.pt_statement_line_payment ( statement_id, statement_year_id ) tablespace pwrplant_idx;
create index pwrplant.pt_slp_stmt_stmt_yr_ln_ndx on pwrplant.pt_statement_line_payment ( statement_id, statement_year_id, line_id ) tablespace pwrplant_idx;



-- pt_accrual_type
create table pwrplant.pt_accrual_type
   (  accrual_type_id               number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(35)   not null,
      long_description              varchar2(254),
      debit_gl_account_id           number(22,0)   not null,
      accrue_credit_gl_account_id   number(22,0)   not null,
      amort_credit_gl_account_id    number(22,0)   not null,
      deferred_gl_account_id        number(22,0),
      interest_gl_account_id        number(22,0),
      protest_gl_account_id         number(22,0),
      penalty_gl_account_id         number(22,0),
      factor_id                        number(22,0)   not null,
      offset_months                 number(22,0)   not null,
      exclude_from_accruals         number(22,0)   not null,
      external_code                 varchar2(35)
   );

alter table pwrplant.pt_accrual_type add ( constraint pt_accrual_type_pk primary key ( accrual_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_type add ( constraint pt_accr_type_deb_gla_fk foreign key ( debit_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_type add ( constraint pt_accr_type_accr_cr_gla_fk foreign key ( accrue_credit_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_type add ( constraint pt_accr_type_amort_cr_gla_fk foreign key ( amort_credit_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_type add ( constraint pt_accr_type_defer_gla_fk foreign key ( deferred_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_type add ( constraint pt_accr_type_intrst_gla_fk foreign key ( interest_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_type add ( constraint pt_accr_type_prtst_gla_fk foreign key ( protest_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_type add ( constraint pt_accr_type_pnlty_gla_fk foreign key ( penalty_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_accrual_type add ( constraint pt_accr_type_spread_fctr_fk foreign key ( factor_id ) references pwrplant.spread_factor );



-- pt_actuals
create table pwrplant.pt_actuals
   (  payment_id                 number(22,0)   not null,
      statement_id               number(22,0)   not null,
      statement_year_id       number(22,0)   not null,
      assessment_year_id         number(22,0)   not null,
      property_tax_ledger_id     number(22,0)   not null,
      tax_authority_id           number(22,0)   not null,
      accrual_type_id            number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      prop_tax_balance           number(22,2)   not null,
      final_reserve              number(22,2)   not null,
      cost_approach_value        number(22,2)   not null,
      market_value               number(22,2)   not null,
      assessment                 number(22,2)   not null,
      equalized_value            number(22,2)   not null,
      taxable_value              number(22,2)   not null,
      quantity                   number(22,8),
      quantity_factored          number(22,8),
      tax_gl_account_id          number(22,0),
      tax_amount                 number(22,2)   not null,
      credit_amount              number(22,2)   not null,
      interest_gl_account_id     number(22,0),
      interest_amount            number(22,2)   not null,
      penalty_gl_account_id      number(22,0),
      penalty_amount          number(22,2)   not null,
      protest_gl_account_id      number(22,0),
      protest_amount          number(22,2)
   );

alter table pwrplant.pt_actuals add ( constraint pt_actuals_pk primary key ( payment_id, statement_id, statement_year_id, assessment_year_id, property_tax_ledger_id, tax_authority_id, accrual_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_pymt_fk foreign key ( payment_id ) references pwrplant.pt_statement_group_payment );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_stmt_stmt_yr_fk foreign key ( statement_id, statement_year_id ) references pwrplant.pt_statement_statement_year );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_asmt_yr_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_ledger_fk foreign key ( property_tax_ledger_id ) references pwrplant.pt_ledger );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_ta_fk foreign key ( tax_authority_id ) references pwrplant.property_tax_authority );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_accr_type_fk foreign key ( accrual_type_id ) references pwrplant.pt_accrual_type );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_tax_gl_acct_fk foreign key ( tax_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_intrst_gl_acct_fk foreign key ( interest_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_pnlty_gl_acct_fk foreign key ( penalty_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_actuals add ( constraint pt_actuals_ptst_gl_acct_fk foreign key ( protest_gl_account_id ) references pwrplant.gl_account );

create index pwrplant.pt_actuals_led_auth_yr_idx on pwrplant.pt_actuals ( property_tax_ledger_id, tax_authority_id, assessment_year_id ) tablespace pwrplant_idx;
create index pwrplant.pt_actuals_stmt_stmt_yr_ndx on pwrplant.pt_actuals ( statement_id, statement_year_id ) tablespace pwrplant_idx;
create index pwrplant.pt_actuals_payment_ndx on pwrplant.pt_actuals ( payment_id ) tablespace pwrplant_idx;
create index pwrplant.pt_actuals_ta_ndx on pwrplant.pt_actuals ( tax_authority_id ) tablespace pwrplant_idx;



-- pt_date_generator
create table pwrplant.pt_date_generator
   (  date_generator_id number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)  not null,
      table_name           varchar2(35),
      pk1_name          varchar2(35),
      pk2_name          varchar2(35),
      pk3_name          varchar2(35),
      pk4_name          varchar2(35),
      pk5_name          varchar2(35),
      pk6_name          varchar2(35)
   );

alter table pwrplant.pt_date_generator add ( constraint pt_date_generator_pk primary key ( date_generator_id ) using index tablespace pwrplant_idx );



-- pt_date_field
create table pwrplant.pt_date_field
   (  date_generator_id number(22,0)   not null,
      date_field_id        number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)  not null,
      column_name       varchar2(35)
   );

alter table pwrplant.pt_date_field add ( constraint pt_date_field_pk primary key ( date_generator_id, date_field_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_date_field add ( constraint pt_date_field_date_gen_fk foreign key ( date_generator_id ) references pwrplant.pt_date_generator );



-- pt_date_weekend_method
create table pwrplant.pt_date_weekend_method
   (  weekend_method_id number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_date_weekend_method add ( constraint pt_date_weekend_method_pk primary key ( weekend_method_id ) using index tablespace pwrplant_idx );



-- pt_date_type
create table pwrplant.pt_date_type
   (  date_type_id               number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      description                varchar2(35)   not null,
      long_description           varchar2(254),
      date_generator_id       number(22,0)   not null,
      date_field_id              number(22,0)   not null,
      date_description           varchar2(35),
      date_long_description      varchar2(254),
      add_days                number(22,0),
      notification_days_before   number(22,0),
      weekend_method_id       number(22,0),
      active_switch              number(22,0)   not null
   );

alter table pwrplant.pt_date_type add ( constraint pt_date_type_pk primary key ( date_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_date_type add ( constraint pt_date_type_field_fk foreign key ( date_generator_id, date_field_id ) references pwrplant.pt_date_field ( date_generator_id, date_field_id ) );
alter table pwrplant.pt_date_type add ( constraint pt_date_type_wknd_mth_fk foreign key ( weekend_method_id ) references pwrplant.pt_date_weekend_method );



-- pt_date_type_person
create table pwrplant.pt_date_type_person
   (  date_type_id         number(22,0)   not null,
      company_id           number(22,0)   not null,
      state_id             char(18)       not null,
      time_stamp           date,
      user_id              varchar2(18),
      responsible_person   varchar2(18)   not null
   );

alter table pwrplant.pt_date_type_person add ( constraint pt_date_type_person_pk primary key ( date_type_id, company_id, state_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_date_type_person add ( constraint pt_dt_type_psn_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_date_type_person add ( constraint pt_dt_type_psn_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_date_type_person add ( constraint pt_dt_type_psn_dt_type_fk foreign key ( date_type_id ) references pwrplant.pt_date_type );



-- pt_date_type_filter
create table pwrplant.pt_date_type_filter
   (  date_type_id         number(22,0)   not null,
      dumb_key          number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      company_id           number(22,0),
      tax_year             number(22,0),
      statement_year_id number(22,0),
      state_id             char(18),
      county_id            char(18)
   );

alter table pwrplant.pt_date_type_filter add ( constraint pt_date_type_filter_pk primary key ( date_type_id, dumb_key ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_date_type_filter add ( constraint pt_dt_type_fltr_county_fk foreign key ( county_id, state_id ) references pwrplant.county ( county_id, state_id ) );
alter table pwrplant.pt_date_type_filter add ( constraint pt_dt_type_fltr_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_date_type_filter add ( constraint pt_dt_type_fltr_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_date_type_filter add ( constraint pt_dt_type_fltr_dt_type_fk foreign key ( date_type_id ) references pwrplant.pt_date_type );
alter table pwrplant.pt_date_type_filter add ( constraint pt_dt_type_fltr_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_date_type_filter add ( constraint pt_dt_type_fltr_stmt_yr_fk foreign key ( statement_year_id ) references pwrplant.pt_statement_year );



-- pt_date
create table pwrplant.pt_date
   (  date_id              number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      event_date           date           not null,
      date_type_id         number(22,0)   not null,
      responsible_person   varchar2(18),
      tax_year             number(22,0),
      statement_year_id number(22,0),
      company_id           number(22,0),
      state_id             char(18),
      county_id            char(18),
      notes                varchar2(2000)
   );

alter table pwrplant.pt_date add ( constraint pt_date_pk primary key ( date_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_date add ( constraint pt_date_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_date add ( constraint pt_date_county_fk foreign key ( county_id, state_id ) references pwrplant.county ( county_id, state_id ) );
alter table pwrplant.pt_date add ( constraint pt_date_date_type_fk foreign key ( date_type_id ) references pwrplant.pt_date_type );
alter table pwrplant.pt_date add ( constraint pt_date_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_date add ( constraint pt_date_statement_year_fk foreign key ( statement_year_id ) references pwrplant.pt_statement_year );
alter table pwrplant.pt_date add ( constraint pt_date_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );



-- pt_date_link
create table pwrplant.pt_date_link
   (  date_id              number(22,0)   not null,
      date_generator_id number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      pk1                  number(22,0),
      pk2                  number(22,0),
      pk3                  number(22,0),
      pk4                  number(22,0),
      pk5                  number(22,0),
      pk6                  number(22,0)
   );

alter table pwrplant.pt_date_link add ( constraint pt_date_link_pk primary key ( date_id, date_generator_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_date_link add ( constraint pt_date_link_date_fk foreign key ( date_id ) references pwrplant.pt_date );
alter table pwrplant.pt_date_link add ( constraint pt_date_link_dtype_fk foreign key ( date_generator_id ) references pwrplant.pt_date_generator );



-- prop_tax_net_tax_reserve
create table pwrplant.prop_tax_net_tax_reserve
   (  net_tax_reserve_id         number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year                   number(22,0)   not null,
      company_id                 number(22,0)   not null,
      state_id                   char(18)       not null,
      depr_group_id              number(22,0),
      book_alloc_group_id        number(22,0)   not null,
      vintage                    number(22,0)   not null,
      book_basis                 number(22,2),
      tax_basis                  number(22,2),
      tax_reserve                number(22,2),
      book_basis_from_tax        number(22,2),
      accum_ordin_retires_end number(22,2)
   );

alter table pwrplant.prop_tax_net_tax_reserve add ( constraint prop_tax_net_tax_reserve_pk primary key ( net_tax_reserve_id ) using index tablespace pwrplant_idx );
alter table pwrplant.prop_tax_net_tax_reserve add ( constraint pt_nt_rvs_book_alloc_gp_fk foreign key ( book_alloc_group_id ) references pwrplant.book_alloc_group );
alter table pwrplant.prop_tax_net_tax_reserve add ( constraint pt_nt_rsv_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.prop_tax_net_tax_reserve add ( constraint pt_nt_rsv_depr_group_fk foreign key ( depr_group_id ) references pwrplant.depr_group );
alter table pwrplant.prop_tax_net_tax_reserve add ( constraint pt_nt_rsv_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.prop_tax_net_tax_reserve add ( constraint pt_nt_rsv_state_fk foreign key ( state_id ) references pwrplant.state );



-- prop_tax_net_tax_reserve_rep
create table pwrplant.prop_tax_net_tax_reserve_rep
   (  net_tax_reserve_id         number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year                   number(22,0)   not null,
      company_id                 number(22,0)   not null,
      state_id                   char(18)       not null,
      depr_group_id              number(22,0),
      book_alloc_group_id        number(22,0)   not null,
      vintage                    number(22,0)   not null,
      book_basis                 number(22,2),
      tax_basis                  number(22,2),
      tax_reserve                number(22,2),
      book_basis_from_tax        number(22,2),
      accum_ordin_retires_end number(22,2)
   );



-- prop_tax_tax_reserve_percent
create table pwrplant.prop_tax_tax_reserve_percent
   (  company_id                 number(22,0)   not null,
      state_id                   char(18)       not null,
      tax_year                   number(22,0)   not null,
      book_alloc_group_id        number(22,0)   not null,
      vintage                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      book_basis                 number(22,2),
      tax_basis                  number(22,2),
      tax_reserve                number(22,2),
      accum_ordin_retires_end number(22,2)
   );

alter table pwrplant.prop_tax_tax_reserve_percent add ( constraint prop_tax_tax_reserve_pct_pk primary key ( company_id, state_id, tax_year, book_alloc_group_id, vintage ) using index tablespace pwrplant_idx );
alter table pwrplant.prop_tax_tax_reserve_percent add ( constraint pt_tt_rsv_pct_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.prop_tax_tax_reserve_percent add ( constraint pt_tt_rsv_pct_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.prop_tax_tax_reserve_percent add ( constraint pt_tt_rsv_pct_book_alloc_fk foreign key ( book_alloc_group_id ) references pwrplant.book_alloc_group );
alter table pwrplant.prop_tax_tax_reserve_percent add ( constraint pt_tt_rsv_pct_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );



-- prop_tax_type_assign_tree
create table pwrplant.prop_tax_type_assign_tree
   (  state_id                   char(18)       not null,
      tax_year                   number(22,0)   not null,
      prop_tax_company_id        number(22,0)   not null,
      method                     number(22,0)   not null,
      company_id                 number(22,0)   not null,
      gl_account_id              number(22,0)   not null,
      bus_segment_id          number(22,0)   not null,
      utility_account_id            number(22,0)   not null,
      asset_location_id          number(22,0)   default 0 not null,
      class_code_id              number(22,0)   default 0 not null,
      class_code_value           varchar2(254)  not null,
      major_location_id          number(22,0)   default 0 not null,
      minor_location_id          number(22,0)   default 0 not null,
      retirement_unit_id            number(22,0)   default 0 not null,
      sub_account_id          number(22,0)   default 0 not null,
      situs_allocated_indicator     number(22,0)   default 0 not null,
      state_id2                  char(18)       not null,
      county_id                  char(18)          not null,
      vintage_start              number(22,0)   default 0 not null,
      vintage_end             number(22,0)   default 0 not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      show_active_only           number(22,0)   default 0 not null,
      tree_level                 number(22,0)   not null,
      asset_location_level       number(22,0)   default 0 not null,
      class_code_level           number(22,0)   default 0 not null,
      major_location_level       number(22,0)   default 0 not null,
      minor_location_level       number(22,0)   default 0 not null,
      retirement_unit_level         number(22,0)   default 0 not null,
      sub_account_level          number(22,0)   default 0 not null,
      situs_allocated_level         number(22,0)   default 0 not null,
      state_level                number(22,0)   default 0 not null,
      county_level               number(22,0)   default 0 not null,
      vintage_level              number(22,0)   default 0 not null,
      priority                   number(22,0)   default 0,
      activity_cost              number(22,2)   default 0 not null,
      property_tax_type_id    number(22,0)
   );

alter table pwrplant.prop_tax_type_assign_tree add ( constraint prop_tax_type_assign_tree_pk primary key ( state_id, tax_year, prop_tax_company_id, method, company_id, gl_account_id, bus_segment_id, utility_account_id, asset_location_id, class_code_id, class_code_value, major_location_id, minor_location_id, retirement_unit_id, sub_account_id, situs_allocated_indicator, state_id2, county_id, vintage_start, vintage_end ) using index tablespace pwrplant_idx );
alter table pwrplant.prop_tax_type_assign_tree add ( constraint ptt_asgn_tree_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.prop_tax_type_assign_tree add ( constraint ptt_asgn_tree_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.prop_tax_type_assign_tree add ( constraint ptt_asgn_tree_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.prop_tax_type_assign_tree add ( constraint ptt_asgn_tree_state_fk foreign key ( state_id ) references pwrplant.state );



-- prop_tax_type_assign_tree_cwip
create table pwrplant.prop_tax_type_assign_tree_cwip
   (  state_id                      char(18)       not null,
      tax_year                      number(22,0)   not null,
      method                        number(22,0)   not null,
      rwip_indicator                number(22,0)   not null,
      prop_tax_company_id        number(22,0)   not null,
      company_id                    number(22,0)   not null,
      gl_account_id                 number(22,0)   not null,
      bus_segment_id             number(22,0)   not null,
      work_order_type_id            number(22,0)   not null,
      asset_location_id             number(22,0)   default 0 not null,
      class_code_id                 number(22,0)   default 0 not null,
      class_code_value              varchar2(254)  not null,
      funding_wo_id                 number(22,0)   default 0 not null,
      major_location_id             number(22,0)   default 0 not null,
      reason_cd_id                  number(22,0)   default 0 not null,
      situs_allocated_indicator        number(22,0)   default 0 not null,
      county_id                     char(18)          not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tree_level                    number(22,0)   not null,
      asset_location_level          number(22,0)   default 0 not null,
      class_code_level              number(22,0)   default 0 not null,
      funding_wo_level              number(22,0)   default 0 not null,
      major_location_level          number(22,0)   default 0 not null,
      reason_code_level          number(22,0)   default 0 not null,
      situs_allocated_level            number(22,0)   default 0 not null,
      county_level                  number(22,0)   default 0 not null,
      priority                      number(22,0)   default 0,
      activity_cost                 number(22,2)   default 0 not null,
      property_tax_type_id       number(22,0),
      handle                        number(22,0),
      calc_property_tax_type_id     number(22,0)
   );

alter table pwrplant.prop_tax_type_assign_tree_cwip add ( constraint pt_type_assign_tree_cwip_pk primary key ( state_id, tax_year, prop_tax_company_id, method, company_id, gl_account_id, bus_segment_id, work_order_type_id, asset_location_id, class_code_id, class_code_value, funding_wo_id, major_location_id, reason_cd_id, situs_allocated_indicator, county_id, rwip_indicator ) using index tablespace pwrplant_idx );
alter table pwrplant.prop_tax_type_assign_tree_cwip add ( constraint ptt_asgn_tree_cwip_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.prop_tax_type_assign_tree_cwip add ( constraint ptt_asgn_tree_cwip_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.prop_tax_type_assign_tree_cwip add ( constraint ptt_asgn_tree_cwip_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );



-- pt_type_assign_cwip_pseudo
create table pwrplant.pt_type_assign_cwip_pseudo
   (  state_id                   char(18)       not null,
      tax_year                   number(22,0)   not null,
      prop_tax_company_id        number(22,0)   not null,
      method                     number(22,0)   not null,
      company_id                 number(22,0)   not null,
      gl_account_id              number(22,0)   not null,
      bus_segment_id          number(22,0)   not null,
      utility_account_id            number(22,0)   not null,
      asset_location_id          number(22,0)   default 0 not null,
      class_code_id              number(22,0)   default 0 not null,
      class_code_value           varchar2(35)   not null,
      county_id                  char(18)       not null,
      major_location_id          number(22,0)   default 0 not null,
      ps_class_code_id           number(22,0)   default 0 not null,
      ps_class_code_value        varchar2(35)   not null,
      sub_account_id             number(22,0)   default 0 not null,
      situs_allocated_indicator     number(22,0)   default 0 not null,
      work_order_type_id         number(22,0)   default 0 not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tree_level                 number(22,0)   not null,
      asset_location_level       number(22,0)   default 0 not null,
      class_code_level           number(22,0)   default 0 not null,
      county_level                  number(22,0)   default 0 not null,
      major_location_level       number(22,0)   default 0 not null,
      ps_class_code_level        number(22,0)   default 0 not null,
      sub_account_level          number(22,0)   default 0 not null,
      situs_allocated_level         number(22,0)   default 0 not null,
      work_order_type_level      number(22,0)   default 0 not null,
      priority                   number(22,0)   default 0,
      activity_cost                 number(22,2)   default 0 not null,
      property_tax_type_id       number(22,0)
   );

alter table pwrplant.pt_type_assign_cwip_pseudo add ( constraint pt_type_cwip_pseudo_pk primary key ( state_id, tax_year, prop_tax_company_id, method, company_id, gl_account_id, bus_segment_id, utility_account_id, asset_location_id, class_code_id, class_code_value, county_id, major_location_id, ps_class_code_id, ps_class_code_value, sub_account_id, situs_allocated_indicator, work_order_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_type_assign_cwip_pseudo add ( constraint pt_type_cwip_pseudo_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_type_assign_cwip_pseudo add ( constraint pt_type_cwip_pseudo_co_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_type_assign_cwip_pseudo add ( constraint pt_type_cwip_pseudo_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_type_assign_cwip_pseudo add ( constraint pt_type_cwip_pseudo_st_fk foreign key ( state_id ) references pwrplant.state );



-- prop_tax_type_prop_tax_adjust
create table pwrplant.prop_tax_type_prop_tax_adjust
   (  prop_tax_type_id        number(22,0)   not null,
      prop_tax_adjust_id      number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      auto_adjust_percent     number(22,8)
   );

alter table pwrplant.prop_tax_type_prop_tax_adjust add ( constraint prop_tax_type_prop_tax_adj_pk primary key ( prop_tax_type_id, prop_tax_adjust_id ) using index tablespace pwrplant_idx );
alter table pwrplant.prop_tax_type_prop_tax_adjust add ( constraint ptt_pt_adj_adjust_fk foreign key ( prop_tax_adjust_id ) references pwrplant.property_tax_adjust );
alter table pwrplant.prop_tax_type_prop_tax_adjust add ( constraint ptt_pt_adj_pttd_fk foreign key ( prop_tax_type_id ) references pwrplant.property_tax_type_data );



-- property_tax_year_lock
create table pwrplant.property_tax_year_lock
   (  company_id     number(22,0)   not null,
      state_id       char(18)       not null,
      tax_year       number(22,0)   not null,
      time_stamp     date,
      user_id        varchar2(18),
      locked         number(22,0)   default 0 not null
   );

alter table pwrplant.property_tax_year_lock add ( constraint property_tax_year_lock_pk primary key ( company_id, state_id, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.property_tax_year_lock add ( constraint pt_year_lock_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.property_tax_year_lock add ( constraint pt_year_lock_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.property_tax_year_lock add ( constraint pt_year_lock_state_fk foreign key ( state_id ) references pwrplant.state );



-- pt_accrual_assign_tree
create table pwrplant.pt_accrual_assign_tree
   (  assessment_year_id            number(22,0)   not null,
      prop_tax_company_id        number(22,0)   not null,
      tree_level                    number(22,0)   not null,
      dumb_key                   number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      gl_account_id                 number(22,0),
      gl_account_level              number(22,0),
      bus_segment_id             number(22,0),
      bus_segment_level          number(22,0),
      utility_account_id               number(22,0),
      utility_account_level            number(22,0),
      property_tax_type_id       number(22,0),
      property_tax_type_level    number(22,0),
      state_id                      char(18),
      state_level                   number(22,0),
      county_id                     char(18),
      county_level                  number(22,0),
      tax_district_id                  number(22,0),
      tax_district_level               number(22,0),
      prop_tax_location_id          number(22,0),
      prop_tax_location_level       number(22,0),
      tax_authority_type_id         number(22,0),
      tax_authority_type_level      number(22,0),
      parcel_id                        number(22,0),
      parcel_level                     number(22,0),
      vintage                       number(22,0),
      vintage_level                 number(22,0),
      class_code_value1          varchar2(254),
      class_code_value1_level       number(22,0),
      class_code_value2          varchar2(254),
      class_code_value2_level       number(22,0),
      user_input                    number(22,0),
      user_input_level              number(22,0),
      company_id                 number(22,0),
      company_level                 number(22,0)
   );

alter table pwrplant.pt_accrual_assign_tree add ( constraint pt_accrual_assign_tree_pk primary key ( assessment_year_id, prop_tax_company_id, tree_level, dumb_key ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_assign_tree add ( constraint pt_accr_asgn_tree_asmt_yr_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );
alter table pwrplant.pt_accrual_assign_tree add ( constraint pt_accr_tree_pt_comp_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );

create unique index pwrplant.pt_accrual_assign_tree_unq_ndx on pwrplant.pt_accrual_assign_tree ( assessment_year_id, prop_tax_company_id, tree_level, company_id, gl_account_id, bus_segment_id, utility_account_id, property_tax_type_id, state_id, county_id, tax_district_id, prop_tax_location_id, tax_authority_type_id, parcel_id, vintage, class_code_value1, class_code_value2, user_input ) tablespace pwrplant_idx;



-- pt_accrual_assign_tree_levels
create table pwrplant.pt_accrual_assign_tree_levels
   (  assessment_year_id         number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      gl_account_level           number(22,0),
      bus_segment_level       number(22,0),
      utility_account_level         number(22,0),
      property_tax_type_level number(22,0),
      state_level                number(22,0),
      county_level               number(22,0),
      tax_district_level            number(22,0),
      prop_tax_location_level    number(22,0),
      tax_authority_type_level   number(22,0),
      parcel_level                  number(22,0),
      vintage_level              number(22,0),
      class_code_value1_level    number(22,0),
      class_code_value2_level    number(22,0),
      user_input_level           number(22,0),
      company_level              number(22,0)
   );

alter table pwrplant.pt_accrual_assign_tree_levels add ( constraint pt_accrual_assign_tree_lvls_pk primary key ( assessment_year_id, prop_tax_company_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_assign_tree_levels add ( constraint pt_accrl_tree_lvls_asmt_yr_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );
alter table pwrplant.pt_accrual_assign_tree_levels add ( constraint pt_accrl_tree_lvls_pt_comp_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- pt_accrual_assign_tree_assign
create table pwrplant.pt_accrual_assign_tree_assign
   (  assessment_year_id         number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      tree_level                 number(22,0)   not null,
      dumb_key                number(22,0)   not null,
      accrual_type_id            number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      percentage                 number(22,8)   not null,
      temp_month              number(22,0),
      temp_factor_pct            number(33,22)
   );

alter table pwrplant.pt_accrual_assign_tree_assign add ( constraint pt_accrual_assign_tree_asgn_pk primary key ( assessment_year_id, prop_tax_company_id, tree_level, dumb_key, accrual_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_assign_tree_assign add ( constraint pt_accr_tree_asgn_tree_fk foreign key ( assessment_year_id, prop_tax_company_id, tree_level, dumb_key ) references pwrplant.pt_accrual_assign_tree ( assessment_year_id, prop_tax_company_id, tree_level, dumb_key ) );
alter table pwrplant.pt_accrual_assign_tree_assign add ( constraint pt_accr_tree_asgn_atype_fk foreign key ( accrual_type_id ) references pwrplant.pt_accrual_type );



-- pt_control
create table pwrplant.pt_control
   (  company_id              number(22,0)   not null,
      state_id                char(18)       not null,
      tax_year                number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      powertax_version_id     number(22,0),
      prior_tax_year          number(22,0),
      cpr_extraction          date,
      cwip_extraction         date,
      nettax_setup            date,
      allocation              date,
      apply_escalations       date,
      lock_tax_year           date,
      cwip_pseudo_yn          number(22,0)
   );

alter table pwrplant.pt_control add ( constraint pt_control_pk primary key ( company_id, state_id, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_control add ( constraint pt_control_prior_ty_fk foreign key ( prior_tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_control add ( constraint pt_control_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_control add ( constraint pt_control_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_control add ( constraint pt_control_state_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_control add ( constraint pt_control_ptax_version_fk foreign key ( powertax_version_id ) references pwrplant.version );



-- pt_incremental_initialization
create table pwrplant.pt_incremental_initialization
   (  dumb_key             number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      tax_year                number(22,0)   not null,
      state_id                char(18)       not null,
      prop_tax_location_id    number(22,0),
      parcel_id                  number(22,0)   not null,
      prop_tax_company_id  number(22,0)   not null,
      company_id              number(22,0),
      gl_account_id           number(22,0),
      bus_segment_id       number(22,0),
      utility_account_id         number(22,0),
      property_tax_type_id number(22,0),
      vintage                 number(22,0),
      vintage_month           number(22,0),
      class_code_value1    varchar2(254),
      class_code_value2    varchar2(254),
      ledger_detail_id        number(22,0),
      statistic                  number(22,8)   not null,
      statistic_pct              number(22,12)
   );

alter table pwrplant.pt_incremental_initialization add ( constraint pt_incremental_initialize_pk primary key ( dumb_key ) using index tablespace pwrplant_idx );

create unique index pwrplant.pt_incr_init_unique_ndx on pwrplant.pt_incremental_initialization ( parcel_id, property_tax_type_id, gl_account_id, bus_segment_id, state_id, company_id, prop_tax_company_id, prop_tax_location_id, utility_account_id,  vintage, vintage_month, class_code_value1, class_code_value2, ledger_detail_id, tax_year ) tablespace pwrplant_idx;



-- pt_incremental_base_year
create table pwrplant.pt_incremental_base_year
   (  dumb_key             number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      state_id                char(18)       not null,
      prop_tax_location_id    number(22,0),
      parcel_id                  number(22,0)   not null,
      prop_tax_company_id  number(22,0)   not null,
      company_id              number(22,0),
      gl_account_id           number(22,0),
      bus_segment_id       number(22,0),
      utility_account_id         number(22,0),
      property_tax_type_id number(22,0),
      vintage                 number(22,0),
      vintage_month           number(22,0),
      class_code_value1    varchar2(254),
      class_code_value2    varchar2(254),
      ledger_detail_id        number(22,0),
      vintage_prior_yn        number(22,0) default 0 not null,
      balance                 number(22,2)   not null
   );

alter table pwrplant.pt_incremental_base_year add ( constraint pt_incremental_base_year_pk primary key ( dumb_key ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_ptloc_fk foreign key ( prop_tax_location_id ) references pwrplant.prop_tax_location );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_co_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_gla_fk foreign key ( gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_bs_fk foreign key ( bus_segment_id ) references pwrplant.business_segment );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_ua_fk foreign key ( bus_segment_id, utility_account_id ) references pwrplant.utility_account ( bus_segment_id, utility_account_id ) );
alter table pwrplant.pt_incremental_base_year add ( constraint pt_incr_by_pttd_fk foreign key ( property_tax_type_id ) references pwrplant.property_tax_type_data );

create unique index pwrplant.pt_incremental_base_yr_index on pwrplant.pt_incremental_base_year ( parcel_id, property_tax_type_id, gl_account_id, bus_segment_id, state_id, company_id, prop_tax_company_id, prop_tax_location_id, utility_account_id, vintage, vintage_month, class_code_value1, class_code_value2, ledger_detail_id );



-- pt_incremental_init_adj
create table pwrplant.pt_incremental_init_adj
   (  dumb_key             number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      tax_year                number(22,0)   not null,
      prop_tax_company_id  number(22,0)   not null,
      state_id                char(18)       not null,
      parcel_id                  number(22,0)   not null,
      prop_tax_location_id    number(22,0),
      company_id              number(22,0),
      gl_account_id           number(22,0),
      bus_segment_id       number(22,0),
      utility_account_id         number(22,0),
      property_tax_type_id number(22,0),
      vintage                 number(22,0),
      vintage_month           number(22,0),
      class_code_value1    varchar2(254),
      class_code_value2    varchar2(254),
      ledger_detail_id        number(22,0),
      property_tax_adjust_id  number(22,0)   not null,
      adjustment              number(22,2)   not null
   );

alter table pwrplant.pt_incremental_init_adj add ( constraint pt_incremental_init_adj_pk primary key ( dumb_key ) using index tablespace pwrplant_idx );

create unique index pwrplant.pt_incr_init_adj_unique_ndx on pwrplant.pt_incremental_init_adj ( parcel_id, property_tax_type_id, gl_account_id, bus_segment_id, state_id, company_id, prop_tax_company_id, prop_tax_location_id, utility_account_id,  vintage, vintage_month, class_code_value1, class_code_value2, ledger_detail_id, tax_year, property_tax_adjust_id ) tablespace pwrplant_idx;



-- pt_interface
create table pwrplant.pt_interface
   (  interface_id            number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      window               varchar2(35)   not null,
      needed_for_closing   number(22,0)   not null
   );

alter table pwrplant.pt_interface add ( constraint pt_interface_pk primary key ( interface_id ) using index tablespace pwrplant_idx );



-- pt_interface_tax_year
create table pwrplant.pt_interface_tax_year
   (  company_id        number(22,0)   not null,
      state_id          char(18)       not null,
      tax_year          number(22,0)   not null,
      interface_id         number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      last_run          date
   );

alter table pwrplant.pt_interface_tax_year add ( constraint pt_interface_tax_year_pk primary key ( company_id, state_id, tax_year, interface_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_interface_tax_year add ( constraint pt_interface_ty_company_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_interface_tax_year add ( constraint pt_interface_ty_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_interface_tax_year add ( constraint pt_interface_ty_interface_fk foreign key ( interface_id ) references pwrplant.pt_interface );
alter table pwrplant.pt_interface_tax_year add ( constraint pt_interface_ty_state_fk foreign key ( state_id ) references pwrplant.state );



-- pt_report_field_labels
create table pwrplant.pt_report_field_labels
   (  report_id         number(22,0)   not null,
      table_name     varchar2(35)   not null,
      column_name varchar2(35)   not null,
      time_stamp     date,
      user_id        varchar2(18),
      label          varchar2(35)   not null
   );

alter table pwrplant.pt_report_field_labels add ( constraint pt_report_field_labels_pk primary key ( report_id, table_name, column_name ) using index tablespace pwrplant_idx );



-- pt_report_company_field
create table pwrplant.pt_report_company_field
   (  report_id         number(22,0)   not null,
      company_id     number(22,0)   not null,
      time_stamp     date,
      user_id        varchar2(18),
      field1            varchar2(254),
      field2            varchar2(254),
      field3            varchar2(254),
      field4            varchar2(254),
      field5            varchar2(254),
      field6            varchar2(254),
      field7            varchar2(254),
      field8            varchar2(254),
      field9            varchar2(254),
      field10        varchar2(254),
      field11        varchar2(254),
      field12        varchar2(254),
      field13        varchar2(254),
      field14        varchar2(254),
      field15        varchar2(254),
      field16        varchar2(254),
      field17        varchar2(254),
      field18        varchar2(254),
      field19        varchar2(254),
      field20        varchar2(254),
      field21        varchar2(254),
      field22        varchar2(254),
      field23        varchar2(254),
      field24        varchar2(254),
      field25        varchar2(254),
      field26        varchar2(2000),
      field27        varchar2(2000),
      field28        varchar2(2000),
      field29        varchar2(2000),
      field30        varchar2(2000),
      field31        varchar2(254),
      field32        varchar2(254),
      field33        varchar2(254),
      field34        varchar2(254),
      field35        varchar2(254),
      field36        varchar2(254),
      field37        varchar2(254),
      field38        varchar2(254),
      field39        varchar2(254),
      field40        varchar2(254),
      field41        varchar2(254),
      field42        varchar2(254),
      field43        varchar2(254),
      field44        varchar2(254),
      field45        varchar2(254),
      field46        varchar2(254),
      field47        varchar2(254),
      field48        varchar2(254),
      field49        varchar2(254),
      field50        varchar2(254)
   );

alter table pwrplant.pt_report_company_field add ( constraint pt_report_company_field_pk primary key ( report_id, company_id ) using index tablespace pwrplant_idx );



-- pt_report_pt_company_field
create table pwrplant.pt_report_pt_company_field
   (  report_id                  number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      field1                     varchar2(254),
      field2                     varchar2(254),
      field3                     varchar2(254),
      field4                     varchar2(254),
      field5                     varchar2(254),
      field6                     varchar2(254),
      field7                     varchar2(254),
      field8                     varchar2(254),
      field9                     varchar2(254),
      field10                 varchar2(254),
      field11                 varchar2(254),
      field12                 varchar2(254),
      field13                 varchar2(254),
      field14                 varchar2(254),
      field15                 varchar2(254),
      field16                 varchar2(254),
      field17                 varchar2(254),
      field18                 varchar2(254),
      field19                 varchar2(254),
      field20                 varchar2(254),
      field21                 varchar2(254),
      field22                 varchar2(254),
      field23                 varchar2(254),
      field24                 varchar2(254),
      field25                 varchar2(254),
      field26                 varchar2(2000),
      field27                 varchar2(2000),
      field28                 varchar2(2000),
      field29                 varchar2(2000),
      field30                 varchar2(2000),
      field31                 varchar2(254),
      field32                 varchar2(254),
      field33                 varchar2(254),
      field34                 varchar2(254),
      field35                 varchar2(254),
      field36                 varchar2(254),
      field37                 varchar2(254),
      field38                 varchar2(254),
      field39                 varchar2(254),
      field40                 varchar2(254),
      field41                 varchar2(254),
      field42                 varchar2(254),
      field43                 varchar2(254),
      field44                 varchar2(254),
      field45                 varchar2(254),
      field46                 varchar2(254),
      field47                 varchar2(254),
      field48                 varchar2(254),
      field49                 varchar2(254),
      field50                 varchar2(254)
   );

alter table pwrplant.pt_report_pt_company_field add ( constraint pt_report_pt_company_field_pk primary key ( report_id, prop_tax_company_id ) using index tablespace pwrplant_idx );



-- pt_report_tax_district_field
create table pwrplant.pt_report_tax_district_field
   (  report_id                  number(22,0)   not null,
      prop_tax_company_id     number(22,0) not null,
      tax_district_id            number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      field1                     varchar2(254),
      field2                     varchar2(254),
      field3                     varchar2(254),
      field4                     varchar2(254),
      field5                     varchar2(254),
      field6                     varchar2(254),
      field7                     varchar2(254),
      field8                     varchar2(254),
      field9                     varchar2(254),
      field10                 varchar2(254),
      field11                 varchar2(254),
      field12                 varchar2(254),
      field13                 varchar2(254),
      field14                 varchar2(254),
      field15                 varchar2(254),
      field16                 varchar2(254),
      field17                 varchar2(254),
      field18                 varchar2(254),
      field19                 varchar2(254),
      field20                 varchar2(254),
      field21                 varchar2(254),
      field22                 varchar2(254),
      field23                 varchar2(254),
      field24                 varchar2(254),
      field25                 varchar2(254),
      field26                 varchar2(2000),
      field27                 varchar2(2000),
      field28                 varchar2(2000),
      field29                 varchar2(2000),
      field30                 varchar2(2000),
      field31                 varchar2(254),
      field32                 varchar2(254),
      field33                 varchar2(254),
      field34                 varchar2(254),
      field35                 varchar2(254),
      field36                 varchar2(254),
      field37                 varchar2(254),
      field38                 varchar2(254),
      field39                 varchar2(254),
      field40                 varchar2(254),
      field41                 varchar2(254),
      field42                 varchar2(254),
      field43                 varchar2(254),
      field44                 varchar2(254),
      field45                 varchar2(254),
      field46                 varchar2(254),
      field47                 varchar2(254),
      field48                 varchar2(254),
      field49                 varchar2(254),
      field50                 varchar2(254)
   );

alter table pwrplant.pt_report_tax_district_field add ( constraint pt_report_tax_dist_field_pk primary key ( report_id, prop_tax_company_id, tax_district_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_report_tax_district_field add ( constraint pt_rpt_td_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- pt_report_type_code_field
create table pwrplant.pt_report_type_code_field
   (  report_id                  number(22,0)   not null,
      prop_tax_company_id     number(22,0) not null,
      type_code_id            number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      field1                     varchar2(254),
      field2                     varchar2(254),
      field3                     varchar2(254),
      field4                     varchar2(254),
      field5                     varchar2(254),
      field6                     varchar2(254),
      field7                     varchar2(254),
      field8                     varchar2(254),
      field9                     varchar2(254),
      field10                 varchar2(254),
      field11                 varchar2(254),
      field12                 varchar2(254),
      field13                 varchar2(254),
      field14                 varchar2(254),
      field15                 varchar2(254),
      field16                 varchar2(254),
      field17                 varchar2(254),
      field18                 varchar2(254),
      field19                 varchar2(254),
      field20                 varchar2(254),
      field21                 varchar2(254),
      field22                 varchar2(254),
      field23                 varchar2(254),
      field24                 varchar2(254),
      field25                 varchar2(254),
      field26                 varchar2(2000),
      field27                 varchar2(2000),
      field28                 varchar2(2000),
      field29                 varchar2(2000),
      field30                 varchar2(2000),
      field31                 varchar2(254),
      field32                 varchar2(254),
      field33                 varchar2(254),
      field34                 varchar2(254),
      field35                 varchar2(254),
      field36                 varchar2(254),
      field37                 varchar2(254),
      field38                 varchar2(254),
      field39                 varchar2(254),
      field40                 varchar2(254),
      field41                 varchar2(254),
      field42                 varchar2(254),
      field43                 varchar2(254),
      field44                 varchar2(254),
      field45                 varchar2(254),
      field46                 varchar2(254),
      field47                 varchar2(254),
      field48                 varchar2(254),
      field49                 varchar2(254),
      field50                 varchar2(254)
   );

alter table pwrplant.pt_report_type_code_field add ( constraint pt_report_type_code_field_pk primary key ( report_id, prop_tax_company_id, type_code_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_report_type_code_field add ( constraint pt_rpt_tc_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- pt_report_county_field
create table pwrplant.pt_report_county_field
   (  report_id                  number(22,0)   not null,
      prop_tax_company_id  number(22,0) not null,
      state_id                char(18)       not null,
      county_id               char(18)       not null,
      time_stamp              date,
      user_id                 varchar2(18),
      field1                     varchar2(254),
      field2                     varchar2(254),
      field3                     varchar2(254),
      field4                     varchar2(254),
      field5                     varchar2(254),
      field6                     varchar2(254),
      field7                     varchar2(254),
      field8                     varchar2(254),
      field9                     varchar2(254),
      field10                 varchar2(254),
      field11                 varchar2(254),
      field12                 varchar2(254),
      field13                 varchar2(254),
      field14                 varchar2(254),
      field15                 varchar2(254),
      field16                 varchar2(254),
      field17                 varchar2(254),
      field18                 varchar2(254),
      field19                 varchar2(254),
      field20                 varchar2(254),
      field21                 varchar2(254),
      field22                 varchar2(254),
      field23                 varchar2(254),
      field24                 varchar2(254),
      field25                 varchar2(254),
      field26                 varchar2(2000),
      field27                 varchar2(2000),
      field28                 varchar2(2000),
      field29                 varchar2(2000),
      field30                 varchar2(2000),
      field31                 varchar2(254),
      field32                 varchar2(254),
      field33                 varchar2(254),
      field34                 varchar2(254),
      field35                 varchar2(254),
      field36                 varchar2(254),
      field37                 varchar2(254),
      field38                 varchar2(254),
      field39                 varchar2(254),
      field40                 varchar2(254),
      field41                 varchar2(254),
      field42                 varchar2(254),
      field43                 varchar2(254),
      field44                 varchar2(254),
      field45                 varchar2(254),
      field46                 varchar2(254),
      field47                 varchar2(254),
      field48                 varchar2(254),
      field49                 varchar2(254),
      field50                 varchar2(254)
   );

alter table pwrplant.pt_report_county_field add ( constraint pt_report_county_field_pk primary key ( report_id, prop_tax_company_id, state_id, county_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_report_county_field add ( constraint pt_rpt_cnty_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- pt_report_parcel_field
create table pwrplant.pt_report_parcel_field
   (  report_id               number(22,0),
      parcel_id               number(22,0),
      time_stamp           date,
      user_id              varchar2(18),
      field1                  varchar2(254),
      field2                  varchar2(254),
      field3                  varchar2(254),
      field4                  varchar2(254),
      field5                  varchar2(254),
      field6                  varchar2(254),
      field7                  varchar2(254),
      field8                  varchar2(254),
      field9                  varchar2(254),
      field10              varchar2(254),
      field11              varchar2(254),
      field12              varchar2(254),
      field13              varchar2(254),
      field14              varchar2(254),
      field15              varchar2(254),
      field16              varchar2(254),
      field17              varchar2(254),
      field18              varchar2(254),
      field19              varchar2(254),
      field20              varchar2(254),
      field21              varchar2(254),
      field22              varchar2(254),
      field23              varchar2(254),
      field24              varchar2(254),
      field25              varchar2(254),
      field26              varchar2(2000),
      field27              varchar2(2000),
      field28              varchar2(2000),
      field29              varchar2(2000),
      field30              varchar2(2000),
      field31              varchar2(254),
      field32              varchar2(254),
      field33              varchar2(254),
      field34              varchar2(254),
      field35              varchar2(254),
      field36              varchar2(254),
      field37              varchar2(254),
      field38              varchar2(254),
      field39              varchar2(254),
      field40              varchar2(254),
      field41              varchar2(254),
      field42              varchar2(254),
      field43              varchar2(254),
      field44              varchar2(254),
      field45              varchar2(254),
      field46              varchar2(254),
      field47              varchar2(254),
      field48              varchar2(254),
      field49              varchar2(254),
      field50              varchar2(254)
   );

alter table pwrplant.pt_report_parcel_field add ( constraint pt_report_parcel_field_pk primary key ( report_id, parcel_id ) using index tablespace pwrplant_idx );



-- pt_report_location_field
create table pwrplant.pt_report_location_field
   (  report_id                  number(22,0)   not null,
      prop_tax_company_id  number(22,0) not null,
      prop_tax_location_id    number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      field1                     varchar2(254),
      field2                     varchar2(254),
      field3                     varchar2(254),
      field4                     varchar2(254),
      field5                     varchar2(254),
      field6                     varchar2(254),
      field7                     varchar2(254),
      field8                     varchar2(254),
      field9                     varchar2(254),
      field10                 varchar2(254),
      field11                 varchar2(254),
      field12                 varchar2(254),
      field13                 varchar2(254),
      field14                 varchar2(254),
      field15                 varchar2(254),
      field16                 varchar2(254),
      field17                 varchar2(254),
      field18                 varchar2(254),
      field19                 varchar2(254),
      field20                 varchar2(254),
      field21                 varchar2(254),
      field22                 varchar2(254),
      field23                 varchar2(254),
      field24                 varchar2(254),
      field25                 varchar2(254),
      field26                 varchar2(2000),
      field27                 varchar2(2000),
      field28                 varchar2(2000),
      field29                 varchar2(2000),
      field30                 varchar2(2000),
      field31                 varchar2(254),
      field32                 varchar2(254),
      field33                 varchar2(254),
      field34                 varchar2(254),
      field35                 varchar2(254),
      field36                 varchar2(254),
      field37                 varchar2(254),
      field38                 varchar2(254),
      field39                 varchar2(254),
      field40                 varchar2(254),
      field41                 varchar2(254),
      field42                 varchar2(254),
      field43                 varchar2(254),
      field44                 varchar2(254),
      field45                 varchar2(254),
      field46                 varchar2(254),
      field47                 varchar2(254),
      field48                 varchar2(254),
      field49                 varchar2(254),
      field50                 varchar2(254)
   );

alter table pwrplant.pt_report_location_field add ( constraint pt_report_location_field_pk primary key ( report_id, prop_tax_company_id, prop_tax_location_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_report_location_field add ( constraint pt_rpt_loc_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- pt_report_state_field
create table pwrplant.pt_report_state_field
   (  report_id                  number(22,0)   not null,
      prop_tax_company_id  number(22,0)   not null,
      state_id                char(18)       not null,
      time_stamp              date,
      user_id                 varchar2(18),
      field1                     varchar2(254),
      field2                     varchar2(254),
      field3                     varchar2(254),
      field4                     varchar2(254),
      field5                     varchar2(254),
      field6                     varchar2(254),
      field7                     varchar2(254),
      field8                     varchar2(254),
      field9                     varchar2(254),
      field10                 varchar2(254),
      field11                 varchar2(254),
      field12                 varchar2(254),
      field13                 varchar2(254),
      field14                 varchar2(254),
      field15                 varchar2(254),
      field16                 varchar2(254),
      field17                 varchar2(254),
      field18                 varchar2(254),
      field19                 varchar2(254),
      field20                 varchar2(254),
      field21                 varchar2(254),
      field22                 varchar2(254),
      field23                 varchar2(254),
      field24                 varchar2(254),
      field25                 varchar2(254),
      field26                 varchar2(2000),
      field27                 varchar2(2000),
      field28                 varchar2(2000),
      field29                 varchar2(2000),
      field30                 varchar2(2000),
      field31                 varchar2(254),
      field32                 varchar2(254),
      field33                 varchar2(254),
      field34                 varchar2(254),
      field35                 varchar2(254),
      field36                 varchar2(254),
      field37                 varchar2(254),
      field38                 varchar2(254),
      field39                 varchar2(254),
      field40                 varchar2(254),
      field41                 varchar2(254),
      field42                 varchar2(254),
      field43                 varchar2(254),
      field44                 varchar2(254),
      field45                 varchar2(254),
      field46                 varchar2(254),
      field47                 varchar2(254),
      field48                 varchar2(254),
      field49                 varchar2(254),
      field50                 varchar2(254)
   );

alter table pwrplant.pt_report_state_field add ( constraint pt_report_state_field_pk primary key ( report_id, prop_tax_company_id, state_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_report_state_field add ( constraint pt_rpt_st_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- pt_actuals_summary
create table pwrplant.pt_actuals_summary
   (  payment_id              number(22,0)   not null,
      statement_id            number(22,0)   not null,
      statement_year_id    number(22,0)   not null,
      accrual_type_id         number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      tax_amount              number(22,2),
      interest_amount         number(22,2),
      penalty_amount       number(22,2),
      protest_amount       number(22,2),
      total_amount            number(22,2),
      credit_amount           number(22,2),
      tax_gl_account_id       number(22,0),
      interest_gl_account_id  number(22,0),
      penalty_gl_account_id   number(22,0),
      protest_gl_account_id   number(22,0)
   );

alter table pwrplant.pt_actuals_summary add ( constraint pt_actuals_summary_pk primary key ( payment_id, statement_id, statement_year_id, accrual_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_actuals_summary add ( constraint pt_actuals_summary_pymt_fk foreign key ( payment_id ) references pwrplant.pt_statement_group_payment );
alter table pwrplant.pt_actuals_summary add ( constraint pt_actuals_summary_ssy_fk foreign key ( statement_id, statement_year_id ) references pwrplant.pt_statement_statement_year ( statement_id, statement_year_id ) );
alter table pwrplant.pt_actuals_summary add ( constraint pt_actuals_summary_acr_ty_fk foreign key ( accrual_type_id ) references pwrplant.pt_accrual_type );
alter table pwrplant.pt_actuals_summary add ( constraint pt_act_sum_tax_gl_acct_fk foreign key ( tax_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_actuals_summary add ( constraint pt_act_sum_intrst_gl_acct_fk foreign key ( interest_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_actuals_summary add ( constraint pt_act_sum_pnlty_gl_acct_fk foreign key ( penalty_gl_account_id ) references pwrplant.gl_account );
alter table pwrplant.pt_actuals_summary add ( constraint pt_act_sum_ptst_gl_acct_fk foreign key ( protest_gl_account_id ) references pwrplant.gl_account );

create index pwrplant.pt_actsum_stmt_stmt_yr_ndx on pwrplant.pt_actuals_summary ( statement_id, statement_year_id ) tablespace pwrplant_idx;
create index pwrplant.pt_actsum_payment_ndx on pwrplant.pt_actuals_summary ( payment_id ) tablespace pwrplant_idx;



-- pt_location_rollup
create table pwrplant.pt_location_rollup
   (  location_rollup_id      number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_location_rollup add ( constraint pt_location_rollup_pk primary key ( location_rollup_id ) using index tablespace pwrplant_idx );



-- pt_accrual_control
create table pwrplant.pt_accrual_control
   (  prop_tax_company_id  number(22,0) not null,
      gl_monthnum          number(22,0) not null,
      time_stamp              date,
      user_id                 varchar2(18),
      calc_accrual               date,
      calc_trueup             date,
      review_je               date,
      release_je              date,
      close_month          date,
      released_je             number(22,0),
      closed_month            number(22,0),
      calc_deferral           date
   );

alter table pwrplant.pt_accrual_control add ( constraint pt_accrual_control_pk primary key ( prop_tax_company_id, gl_monthnum ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_control add ( constraint pt_accr_cntl_pt_comp_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- pt_accrual_allowed_amt
create table pwrplant.pt_accrual_allowed_amt
   (  company_id           number(22,0) not null,
      state_id             char(18) not null,
      assessment_year_id   number(22,0) not null,
      gl_monthnum       number(22,0) not null,
      time_stamp           date,
      user_id              varchar2(18),
      allowed_amount    number(22,2) not null,
      deferral_pct            number(22,8) not null,
      offset_months        number(22,0)   not null
   );

alter table pwrplant.pt_accrual_allowed_amt add ( constraint pt_accrual_allowed_amt_pk primary key ( company_id, state_id, assessment_year_id, gl_monthnum ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_allowed_amt add ( constraint pt_accrual_allowed_amt_co_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_accrual_allowed_amt add ( constraint pt_accrual_allowed_amt_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_accrual_allowed_amt add ( constraint pt_accrual_allowed_amt_ay_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );



-- pt_accrual_status
create table pwrplant.pt_accrual_status
   (  accrual_status_id       number(22,0) not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)
   );

alter table pwrplant.pt_accrual_status add ( constraint pt_accrual_status_pk primary key ( accrual_status_id ) using index tablespace pwrplant_idx );



-- pt_accrual_pending
create table pwrplant.pt_accrual_pending
   (  pending_id              number(22,0) not null,
      time_stamp              date,
      user_id                 varchar2(18),
      accrual_type_id         number(22,0),
      assessment_year_id      number(22,0),
      approval_type_id        number(22,0),
      company_id              number(22,0),
      accrual_status_id       number(22,0),
      gl_trans_id             number(22,0),
      month                date,
      company_number       char(10),
      gl_account              varchar2(2000),
      debit_credit_indicator     number(22,0),
      amount                  number(22,2),
      gl_je_code              char(18),
      description             varchar2(254),
      source                  varchar2(35),
      authorizer1             varchar2(18),
      authorized_date1        date,
      authorizer2             varchar2(18),
      authorized_date2        date,
      authorizer3             varchar2(18),
      authorized_date3        date,
      authorizer4             varchar2(18),
      authorized_date4        date,
      authorizer5             varchar2(18),
      authorized_date5        date,
      authorizer6             varchar2(18),
      authorized_date6        date,
      authorizer7             varchar2(18),
      authorized_date7        date,
      authorizer8             varchar2(18),
      authorized_date8        date,
      authorizer9             varchar2(18),
      authorized_date9        date,
      authorizer10            varchar2(18),
      authorized_date10       date,
      rejector                varchar2(18),
      rejected_date           date,
      final_approval_date     date
   );

alter table pwrplant.pt_accrual_pending add ( constraint pt_accrual_pending_pk primary key ( pending_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_accrual_pending add ( constraint pt_accrual_pending_co_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_accrual_pending add ( constraint pt_accrual_pending_at_fk foreign key ( accrual_type_id ) references pwrplant.pt_accrual_type );
alter table pwrplant.pt_accrual_pending add ( constraint pt_accrual_pending_ay_fk foreign key ( assessment_year_id ) references pwrplant.pt_assessment_year );
alter table pwrplant.pt_accrual_pending add ( constraint pt_accrual_pending_as_fk foreign key ( accrual_status_id ) references pwrplant.pt_accrual_status );
alter table pwrplant.pt_accrual_pending add ( constraint pt_accrual_pending_app_fk foreign key ( approval_type_id ) references pwrplant.approval );



-- pt_parcel_location
create table pwrplant.pt_parcel_location
   (  asset_location_id       number(22,0)   not null,
      prop_tax_company_id  number(22,0)   not null,
      parcel_type_id          number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      allocated_yn            number(22,0)   not null,
      parcel_id                  number(22,0)
   );

alter table pwrplant.pt_parcel_location add ( constraint pt_parcel_location_pk primary key ( asset_location_id, prop_tax_company_id, parcel_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_location add ( constraint pt_prcl_loc_al_fk foreign key ( asset_location_id ) references pwrplant.asset_location );
alter table pwrplant.pt_parcel_location add ( constraint pt_prcl_loc_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_parcel_location add ( constraint pt_prcl_loc_prcl_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );

create index pwrplant.pt_prcl_loc_parcel_ndx on pwrplant.pt_parcel_location ( parcel_id ) tablespace pwrplant_idx;



-- pt_statistics_spread_rules
create table pwrplant.pt_statistics_spread_rules
   (  property_tax_type_id       number(22,0) not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      spread_on_book             number(22,0) default 0,
      spread_on_adjusted            number(22,0) default 0,
      state_id                      number(22.0) default 0,
      company_id                    number(22,0) default 0,
      gl_account_id                 number(22,0) default 0,
      bus_segment_id             number(22,0) default 0,
      utility_account_id               number(22,0) default 0,
      parcel_id                        number(22,0) default 0,
      prop_tax_location_id          number(22,0) default 0,
      vintage                       number(22,0) default 0,
      user_input                    number(22,0) default 0,
      property_tax_types            number(22,0) default 0,
      property_tax_types_string     varchar2(4000)
   );

alter table pwrplant.pt_statistics_spread_rules add ( constraint pt_statistics_spread_pk primary key ( property_tax_type_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statistics_spread_rules add ( constraint pt_statistics_spread_ptt_fk foreign key ( property_tax_type_id ) references pwrplant.property_tax_type_data );



-- pt_company_override
create table pwrplant.pt_company_override
   (  company_id              number(22,0)   not null,
      state_id                char(18)       not null,
      time_stamp              date,
      user_id                 varchar2(18),
      prop_tax_company_id  number(22,0)   not null
   );

alter table pwrplant.pt_company_override add ( constraint pt_company_override_pk primary key ( company_id, state_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_company_override add ( constraint pt_co_override_co_fk foreign key ( company_id ) references pwrplant.company_setup );
alter table pwrplant.pt_company_override add ( constraint pt_co_override_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_company_override add ( constraint pt_co_override_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );



-- ptc_user_options
create table pwrplant.ptc_user_options
   (  object               varchar2(1000) not null,
      option_identifier    varchar2(254)     not null,
      users                varchar2(18)      not null,
      time_stamp           date,
      user_id              varchar2(18),
      user_option          varchar2(4000),
      user_option_b        varchar2(4000),
      user_option_c        varchar2(4000),
      user_option_d        varchar2(4000)
   );

alter table pwrplant.ptc_user_options add ( constraint ptc_user_options_pk primary key ( object, option_identifier, users ) using index tablespace pwrplant_idx );



-- pt_process_type
create table pwrplant.pt_process_type
   (  pt_process_type_id   number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254)
   );

alter table pwrplant.pt_process_type add ( constraint pt_process_type_pk primary key ( pt_process_type_id ) using index tablespace pwrplant_idx );



-- pt_process
create table pwrplant.pt_process
   (  pt_process_id           number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      long_description        varchar2(254)  not null,
      short_description       varchar2(22)   not null,
      identifier                 varchar2(35)   not null,
      pt_process_type_id      number(22,0)   not null,
      is_base_process         number(22,0)   not null,
      default_step_number     number(22,0),
      object_name          varchar2(254),
      documentation           varchar2(4000)
   );

alter table pwrplant.pt_process add ( constraint pt_process_pk primary key ( pt_process_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_process add ( constraint pt_process_proc_type_fk foreign key ( pt_process_type_id ) references pwrplant.pt_process_type );

create unique index pwrplant.pt_process_id_index on pwrplant.pt_process ( identifier ) tablespace pwrplant_idx;



-- pt_process_company_state
create table pwrplant.pt_process_company_state
   (  prop_tax_company_id  number(22,0)   not null,
      state_id                char(18)       not null,
      pt_process_id           number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      specific_documentation  varchar2(4000)
   );

alter table pwrplant.pt_process_company_state add ( constraint pt_process_company_state_pk primary key ( prop_tax_company_id, state_id, pt_process_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_process_company_state add ( constraint pt_process_co_st_co_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_process_company_state add ( constraint pt_process_co_st_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_process_company_state add ( constraint pt_proc_co_st_process_fk foreign key ( pt_process_id ) references pwrplant.pt_process );



-- pt_process_company_state_ty
create table pwrplant.pt_process_company_state_ty
   (  tax_year                   number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      state_id                   char(18)       not null,
      pt_process_id              number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      step_number             number(22,0)   not null,
      last_run_time              date,
      last_run_by                varchar2(18),
      elapsed_time               number(22,0),
      is_completed_successful    number(22,0)   not null
   );

alter table pwrplant.pt_process_company_state_ty add ( constraint pt_process_company_state_ty_pk primary key ( tax_year, prop_tax_company_id, state_id, pt_process_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_process_company_state_ty add ( constraint pt_proc_co_st_ty_proc_co_st_fk foreign key ( prop_tax_company_id, state_id, pt_process_id ) references pwrplant.pt_process_company_state ( prop_tax_company_id, state_id, pt_process_id ) );
alter table pwrplant.pt_process_company_state_ty add ( constraint pt_proc_co_st_ty_tax_year_fk foreign key ( tax_year ) references pwrplant.property_tax_year );



-- pt_process_dependency
create table pwrplant.pt_process_dependency
   (  tax_year                   number(22,0)   not null,
      prop_tax_company_id     number(22,0)   not null,
      state_id                   char(18)       not null,
      pt_process_id              number(22,0)   not null,
      dependent_process_id    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18)
   );

alter table pwrplant.pt_process_dependency add ( constraint pt_process_dependency_pk primary key ( tax_year, prop_tax_company_id, state_id, pt_process_id, dependent_process_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_process_dependency add ( constraint pt_proc_dep_proc_co_st_ty_fk foreign key ( tax_year, prop_tax_company_id, state_id, pt_process_id ) references pwrplant.pt_process_company_state_ty ( tax_year, prop_tax_company_id, state_id, pt_process_id ) );
alter table pwrplant.pt_process_dependency add ( constraint pt_proc_dep_dep_process_fk foreign key ( dependent_process_id ) references pwrplant.pt_process );
alter table pwrplant.pt_process_dependency add ( constraint pt_proc_dep_dep_co_st_ty_fk foreign key ( tax_year, prop_tax_company_id, state_id, dependent_process_id ) references pwrplant.pt_process_company_state_ty ( tax_year, prop_tax_company_id, state_id, pt_process_id ) );



-- pt_query_type
create table pwrplant.pt_query_type
   (  pt_query_type_id     number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(50)   not null
   );

alter table pwrplant.pt_query_type add ( constraint pt_query_type_pk primary key ( pt_query_type_id ) using index tablespace pwrplant_idx );



-- pt_query_columns
create table pwrplant.pt_query_columns
   (  pt_query_column_id      number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      pt_query_type_id        number(22,0)   not null,
      display_description     varchar2(30)   not null,
      src_column              varchar2(50)   not null,
      src_table                  varchar2(50)   not null,
      src_join_column_from    varchar2(50),
      join_column_to          varchar2(50),
      join_table_to           varchar2(50),
      outer_join_flag            varchar2(1)    not null,
      nullable                varchar2(1)    not null,
      summed_column        varchar2(1)    not null,
      iteration_level            varchar2(1)    not null,
      filter_select_or_joinonly  varchar2(1)    not null,
      numeric_flag            varchar2(1)    not null,
      src_join_column_from2   varchar2(50),
      join_column_to2         varchar2(50),
      src_join_column_from3   varchar2(50),
      join_column_to3         varchar2(50)
   );

alter table pwrplant.pt_query_columns add ( constraint pt_query_columns_pk primary key ( pt_query_column_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_query_columns add ( constraint pt_query_columns_qry_type_fk foreign key ( pt_query_type_id ) references pwrplant.pt_query_type );

create unique index pwrplant.pt_qry_cols_type_desc_unq_ndx on pwrplant.pt_query_columns ( pt_query_type_id, display_description );



-- pt_query_main_save
create table pwrplant.pt_query_main_save
   (  pt_query_id          number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      pt_query_type_id     number(22,0),
      description          varchar2(254)  not null,
      private_flag            number(22,0)   not null,
      query_owner       varchar2(18)
   );

alter table pwrplant.pt_query_main_save add ( constraint pt_query_main_save_pk primary key ( pt_query_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_query_main_save add ( constraint pt_query_main_save_type_fk foreign key ( pt_query_type_id ) references pwrplant.pt_query_type );



-- pt_query_filter_save
create table pwrplant.pt_query_filter_save
   (  pt_query_id          number(22,0)   not null,
      pt_filter_row_id     number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      pt_query_type_id     number(22,0),
      columns              varchar2(254),
      filter_string           varchar2(2000),
      operator             varchar2(10),
      and_or               varchar2(3),
      left_paren           varchar2(3),
      right_paren          varchar2(3)
   );

alter table pwrplant.pt_query_filter_save add ( constraint pt_query_filter_save_pk primary key ( pt_query_id, pt_filter_row_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_query_filter_save add ( constraint pt_query_filter_save_type_fk foreign key ( pt_query_type_id ) references pwrplant.pt_query_type );
alter table pwrplant.pt_query_filter_save add ( constraint pt_query_filter_save_query_fk foreign key ( pt_query_id ) references pwrplant.pt_query_main_save );



-- pt_query_col_save
create table pwrplant.pt_query_col_save
   (  pt_query_id          number(22,0)   not null,
      pt_query_column_id   number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      pt_query_type_id     number(22,0),
      display_order        number(22,0),
      subtotal_flag        varchar2(1)
   );

alter table pwrplant.pt_query_col_save add ( constraint pt_query_col_save_pk primary key ( pt_query_id, pt_query_column_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_query_col_save add ( constraint pt_query_col_save_type_fk foreign key ( pt_query_type_id ) references pwrplant.pt_query_type );
alter table pwrplant.pt_query_col_save add ( constraint pt_query_col_save_query_fk foreign key ( pt_query_id ) references pwrplant.pt_query_main_save );
alter table pwrplant.pt_query_col_save add ( constraint pt_query_col_save_col_fk foreign key ( pt_query_column_id ) references pwrplant.pt_query_columns );



-- ptc_system_options
create table pwrplant.ptc_system_options
   (  system_option_id     varchar2(254)     not null,
      time_stamp           date,
      user_id              varchar2(18),
      long_description     varchar2(4000) not null,
      system_only       number(22,0)      not null,
      pp_default_value     varchar2(254)     not null,
      option_value         varchar2(254),
      is_base_option       number(22,0)      not null
   );

alter table pwrplant.ptc_system_options add ( constraint ptc_system_options_pk primary key ( system_option_id ) using index tablespace pwrplant_idx );



-- ptc_system_options_values
create table pwrplant.ptc_system_options_values
   (  system_option_id     varchar2(254)     not null,
      option_value         varchar2(254)     not null,
      time_stamp           date,
      user_id              varchar2(18)
   );

alter table pwrplant.ptc_system_options_values add ( constraint ptc_system_options_values_pk primary key ( system_option_id, option_value ) using index tablespace pwrplant_idx );
alter table pwrplant.ptc_system_options_values add ( constraint pt_sys_opt_values_sys_opt_fk foreign key ( system_option_id ) references pwrplant.ptc_system_options );



-- ptc_system_options_centers
create table pwrplant.ptc_system_options_centers
   (  system_option_id     varchar2(254)     not null,
      center_name       varchar2(35)      not null,
      time_stamp           date,
      user_id              varchar2(18)
   );

alter table pwrplant.ptc_system_options_centers add ( constraint ptc_system_options_centers_pk primary key ( system_option_id, center_name ) using index tablespace pwrplant_idx );
alter table pwrplant.ptc_system_options_centers add ( constraint pt_sys_opt_centers_sys_opt_fk foreign key ( system_option_id ) references pwrplant.ptc_system_options );



-- pt_statistics_autocreate
create table pwrplant.pt_statistics_autocreate
   (  tax_year                number(22,0)   not null,
      prop_tax_company_id  number(22,0)   not null,
      state_id                char(18)       not null,
      allocation_id              number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      statistics_table           varchar2(35),
      ledger_table            varchar2(35),
      year_used               varchar2(35),
      field                   varchar2(35),
      property_tax_types      varchar2(4000)
   );

alter table pwrplant.pt_statistics_autocreate add ( constraint pt_statistics_auto_pk primary key ( tax_year, prop_tax_company_id, state_id, allocation_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_statistics_autocreate add ( constraint pt_statistics_auto_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_statistics_autocreate add ( constraint pt_statistics_auto_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_statistics_autocreate add ( constraint pt_statistics_auto_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_statistics_autocreate add ( constraint pt_statistics_auto_allo_fk foreign key ( allocation_id ) references pwrplant.prop_tax_allo_definition );



-- pt_parcel_auto_adjust
create table pwrplant.pt_parcel_auto_adjust
   (  parcel_id                     number(22,0)      not null,
      property_tax_adjust_id     number(22,0)      not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      auto_adjust_percent        number(22,8)      not null
   );

alter table pwrplant.pt_parcel_auto_adjust add ( constraint pt_parcel_auto_adjust_pk primary key ( parcel_id, property_tax_adjust_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_parcel_auto_adjust add ( constraint pt_parcel_auto_adjust_pr_fk foreign key ( parcel_id ) references pwrplant.pt_parcel );
alter table pwrplant.pt_parcel_auto_adjust add ( constraint pt_parcel_auto_adjust_pta_fk foreign key ( property_tax_adjust_id ) references pwrplant.property_tax_adjust );


-- pt_import_ledger
create table pwrplant.pt_import_ledger
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      prop_tax_location_xlate       varchar2(254),
      prop_tax_location_id          number(22,0),
      county_xlate                  varchar2(254),
      county_id                     char(18),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel2_xlate                 varchar2(254),
      parcel_id                        number(22,0),
      company_xlate                 varchar2(254),
      company_id                    number(22,0),
      gl_account_xlate              varchar2(254),
      gl_account_id                 number(22,0),
      bus_segment_xlate          varchar2(254),
      bus_segment_id             number(22,0),
      utility_account_xlate            varchar2(254),
      utility_account_id               number(22,0),
      property_tax_type_xlate    varchar2(254),
      property_tax_type_id       number(22,0),
      vintage                       varchar2(35),
      vintage_equalizer             varchar2(35),
      vintage_month                 varchar2(35),
      vintage_month_equalizer    varchar2(35),
      class_code_value1_xlate    varchar2(254),
      class_code_value1          varchar2(254),
      class_code_value2_xlate    varchar2(254),
      class_code_value2          varchar2(254),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      property_tax_adjust_xlate     varchar2(254),
      property_tax_adjust_id        number(22,0),
      beginning_book_balance        varchar2(35),
      book_additions                varchar2(35),
      book_retirements              varchar2(35),
      book_transfers_adj            varchar2(35),
      end_bal_adjustment            varchar2(35),
      cwip_balance                  varchar2(35),
      allocated_tax_basis           varchar2(35),
      allocated_accum_ore           varchar2(35),
      associated_reserve            varchar2(35),
      allocated_tax_reserve         varchar2(35),
      quantity                      varchar2(35),
      beg_bal_adjustment            varchar2(35),
      additions_adjustment          varchar2(35),
      retirements_adjustment        varchar2(35),
      transfers_adjustment          varchar2(35),
      cwip_adjustment               varchar2(35),
      tax_basis_adjustment       varchar2(35),
      reserve_adjustment            varchar2(35),
      adjust_is_percent_xlate       varchar2(254),
      adjust_is_percent_yn          number(22,0),
      spread_on_book_xlate       varchar2(254),
      spread_on_book_yn          number(22,0),
      autocalc_rsv_xlate               varchar2(254),
      autocalc_rsv_yn               number(22,0),
      ledger_notes                  varchar2(4000),
      ledger_tax_year_notes         varchar2(4000),
      adjustment_notes              varchar2(4000),
      ld_description_1              varchar2(254),
      ld_description_2              varchar2(254),
      ld_description_3              varchar2(254),
      ld_unit_number                varchar2(50),
      ld_serial_number              varchar2(50),
      ld_make                       varchar2(50),
      ld_model                      varchar2(50),
      ld_model_year                 varchar2(50),
      ld_body_type                  varchar2(50),
      ld_fuel_type                  varchar2(50),
      ld_gross_weight               varchar2(50),
      ld_license_number             varchar2(50),
      ld_license_weight                varchar2(50),
      ld_license_expiration            varchar2(35),
      ld_key_number              varchar2(50),
      ld_primary_driver                varchar2(50),
      ld_group_code                 varchar2(50),
      ld_length                     varchar2(50),
      ld_width                         varchar2(50),
      ld_height                     varchar2(50),
      ld_construction_type          varchar2(50),
      ld_stories                    varchar2(50),
      ld_source_system              varchar2(50),
      ld_external_code              varchar2(50),
      ld_lease_number               varchar2(50),
      ld_lease_start                   varchar2(35),
      ld_lease_end                  varchar2(35),
      ld_notes                         varchar2(4000),
      ledger_detail_xlate           varchar2(254),
      ledger_detail_id              number(22,0),
      property_tax_ledger_xlate     varchar2(254),
      property_tax_ledger_id        number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_ledger add ( constraint pt_import_ledger_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_ledger add ( constraint pt_impt_ldg_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_ledger_archive
create table pwrplant.pt_import_ledger_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      prop_tax_location_xlate       varchar2(254),
      prop_tax_location_id          number(22,0),
      county_xlate                  varchar2(254),
      county_id                     char(18),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel2_xlate                 varchar2(254),
      parcel_id                        number(22,0),
      company_xlate                 varchar2(254),
      company_id                    number(22,0),
      gl_account_xlate              varchar2(254),
      gl_account_id                 number(22,0),
      bus_segment_xlate          varchar2(254),
      bus_segment_id             number(22,0),
      utility_account_xlate            varchar2(254),
      utility_account_id               number(22,0),
      property_tax_type_xlate    varchar2(254),
      property_tax_type_id       number(22,0),
      vintage                       varchar2(35),
      vintage_equalizer             varchar2(35),
      vintage_month                 varchar2(35),
      vintage_month_equalizer    varchar2(35),
      class_code_value1_xlate    varchar2(254),
      class_code_value1          varchar2(254),
      class_code_value2_xlate    varchar2(254),
      class_code_value2          varchar2(254),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      property_tax_adjust_xlate     varchar2(254),
      property_tax_adjust_id        number(22,0),
      beginning_book_balance        varchar2(35),
      book_additions                varchar2(35),
      book_retirements              varchar2(35),
      book_transfers_adj            varchar2(35),
      cwip_balance                  varchar2(35),
      allocated_tax_basis           varchar2(35),
      allocated_accum_ore           varchar2(35),
      associated_reserve            varchar2(35),
      allocated_tax_reserve         varchar2(35),
      quantity                      varchar2(35),
      beg_bal_adjustment            varchar2(35),
      additions_adjustment          varchar2(35),
      retirements_adjustment        varchar2(35),
      transfers_adjustment          varchar2(35),
      end_bal_adjustment            varchar2(35),
      cwip_adjustment               varchar2(35),
      tax_basis_adjustment       varchar2(35),
      reserve_adjustment            varchar2(35),
      adjust_is_percent_xlate       varchar2(254),
      adjust_is_percent_yn          number(22,0),
      spread_on_book_xlate       varchar2(254),
      spread_on_book_yn          number(22,0),
      autocalc_rsv_xlate               varchar2(254),
      autocalc_rsv_yn               number(22,0),
      ledger_notes                  varchar2(4000),
      ledger_tax_year_notes         varchar2(4000),
      adjustment_notes              varchar2(4000),
      ld_description_1              varchar2(254),
      ld_description_2              varchar2(254),
      ld_description_3              varchar2(254),
      ld_unit_number                varchar2(50),
      ld_serial_number              varchar2(50),
      ld_make                       varchar2(50),
      ld_model                      varchar2(50),
      ld_model_year                 varchar2(50),
      ld_body_type                  varchar2(50),
      ld_fuel_type                  varchar2(50),
      ld_gross_weight               varchar2(50),
      ld_license_number             varchar2(50),
      ld_license_weight                varchar2(50),
      ld_license_expiration            varchar2(35),
      ld_key_number              varchar2(50),
      ld_primary_driver                varchar2(50),
      ld_group_code                 varchar2(50),
      ld_length                     varchar2(50),
      ld_width                         varchar2(50),
      ld_height                     varchar2(50),
      ld_construction_type             varchar2(50),
      ld_stories                    varchar2(50),
      ld_source_system              varchar2(50),
      ld_external_code              varchar2(50),
      ld_lease_number               varchar2(50),
      ld_lease_start                   varchar2(35),
      ld_lease_end                  varchar2(35),
      ld_notes                         varchar2(4000),
      ledger_detail_xlate           varchar2(254),
      ledger_detail_id              number(22,0),
      property_tax_ledger_xlate     varchar2(254),
      property_tax_ledger_id        number(22,0)
   );

alter table pwrplant.pt_import_ledger_archive add ( constraint pt_import_ledger_archive_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_ledger_archive add ( constraint pt_impt_ldg_arch_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_preallo
create table pwrplant.pt_import_preallo
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      prop_tax_location_xlate       varchar2(254),
      prop_tax_location_id          number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      company_xlate                 varchar2(254),
      company_id                    number(22,0),
      gl_account_xlate              varchar2(254),
      gl_account_id                 number(22,0),
      bus_segment_xlate          varchar2(254),
      bus_segment_id             number(22,0),
      utility_account_xlate            varchar2(254),
      utility_account_id               number(22,0),
      property_tax_type_xlate    varchar2(254),
      property_tax_type_id       number(22,0),
      depr_group_xlate              varchar2(254),
      depr_group_id                 number(22,0),
      vintage                       varchar2(35),
      vintage_equalizer             varchar2(35),
      vintage_month                 varchar2(35),
      vintage_month_equalizer    varchar2(35),
      class_code_value1_xlate    varchar2(254),
      class_code_value1          varchar2(254),
      class_code_value2_xlate    varchar2(254),
      class_code_value2          varchar2(254),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      beginning_book_balance        varchar2(35),
      book_additions                varchar2(35),
      book_retirements              varchar2(35),
      book_transfers_adj            varchar2(35),
      cwip_balance                  varchar2(35),
      quantity                      varchar2(35),
      notes                         varchar2(4000),
      preallo_ledger_xlate          varchar2(254),
      preallo_ledger_id             number(22,0),
      county_xlate                  varchar2(254),
      county_id                     char(18),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      property_tax_adjust_xlate     varchar2(254),
      property_tax_adjust_id        number(22,0),
      beg_bal_adjustment            varchar2(35),
      additions_adjustment          varchar2(35),
      retirements_adjustment        varchar2(35),
      transfers_adjustment          varchar2(35),
      end_bal_adjustment            varchar2(35),
      cwip_adjustment               varchar2(35),
      adjustment_notes              varchar2(4000),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_preallo add ( constraint pt_import_preallo_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_preallo add ( constraint pt_impt_pre_impt_run_fk foreign key ( import_run_id ) references  pwrplant.pp_import_run );



-- pt_import_preallo_archive
create table pwrplant.pt_import_preallo_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      prop_tax_location_xlate       varchar2(254),
      prop_tax_location_id          number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      company_xlate                 varchar2(254),
      company_id                    number(22,0),
      gl_account_xlate              varchar2(254),
      gl_account_id                 number(22,0),
      bus_segment_xlate          varchar2(254),
      bus_segment_id             number(22,0),
      utility_account_xlate            varchar2(254),
      utility_account_id               number(22,0),
      property_tax_type_xlate       varchar2(254),
      property_tax_type_id          number(22,0),
      depr_group_xlate              varchar2(254),
      depr_group_id                 number(22,0),
      vintage                       varchar2(35),
      vintage_equalizer             varchar2(35),
      vintage_month                 varchar2(35),
      vintage_month_equalizer    varchar2(35),
      class_code_value1_xlate       varchar2(254),
      class_code_value1             varchar2(254),
      class_code_value2_xlate       varchar2(254),
      class_code_value2             varchar2(254),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      beginning_book_balance        varchar2(35),
      book_additions                varchar2(35),
      book_retirements              varchar2(35),
      book_transfers_adj            varchar2(35),
      cwip_balance                  varchar2(35),
      quantity                      varchar2(35),
      notes                         varchar2(4000),
      preallo_ledger_xlate          varchar2(254),
      preallo_ledger_id             number(22,0),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      property_tax_adjust_xlate     varchar2(254),
      property_tax_adjust_id        number(22,0),
      beg_bal_adjustment            varchar2(35),
      additions_adjustment          varchar2(35),
      retirements_adjustment        varchar2(35),
      transfers_adjustment          varchar2(35),
      end_bal_adjustment            varchar2(35),
      cwip_adjustment               varchar2(35),
      adjustment_notes              varchar2(4000)
   );

alter table pwrplant.pt_import_preallo_archive add ( constraint pt_import_preallo_archive_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_preallo_archive add ( constraint pt_impt_pre_arch_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_ledger_adj_archive
create table pwrplant.pt_import_ledger_adj_archive
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      property_tax_ledger_id     number(22,0)   not null,
      property_tax_adjust_id     number(22,0)   not null,
      user_input                 number(22,0)   not null,
      tax_year                   number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      beg_bal_adjustment         number(22,2),
      additions_adjustment       number(22,2),
      retirements_adjustment     number(22,2),
      transfers_adjustment       number(22,2),
      end_bal_adjustment         number(22,2),
      cwip_adjustment            number(22,2),
      tax_basis_adjustment    number(22,2),
      reserve_adjustment         number(22,2),
      notes                      varchar2(4000)
   );

alter table pwrplant.pt_import_ledger_adj_archive add ( constraint pt_import_ledger_adj_archiv_pk primary key ( import_run_id, line_id, property_tax_ledger_id, property_tax_adjust_id, user_input, tax_year ) using index tablespace pwrplant_idx );



-- pt_import_preallo_adj_archive
create table pwrplant.pt_import_preallo_adj_archive
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      preallo_ledger_id          number(22,0)   not null,
      property_tax_adjust_id     number(22,0)   not null,
      user_input                 number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      beg_bal_adjustment         number(22,2),
      additions_adjustment       number(22,2),
      retirements_adjustment     number(22,2),
      transfers_adjustment       number(22,2),
      end_bal_adjustment         number(22,2),
      cwip_adjustment            number(22,2),
      notes                      varchar2(4000)
   );

alter table pwrplant.pt_import_preallo_adj_archive add ( constraint pt_import_preallo_adj_archi_pk primary key ( import_run_id, line_id, preallo_ledger_id, property_tax_adjust_id, user_input ) using index tablespace pwrplant_idx );



-- pt_import_stats_full
create table pwrplant.pt_import_stats_full
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year_xlate             varchar2(254),
      tax_year                   number(22,0),
      allocation_xlate           varchar2(254),
      allocation_id                 number(22,0),
      prop_tax_location_xlate    varchar2(254),
      prop_tax_location_id       number(22,0),
      vintage                    varchar2(35),
      state_xlate                varchar2(254),
      state_id                   char(18),
      prop_tax_company_xlate  varchar2(254),
      prop_tax_company_id     number(22,0),
      county_xlate               varchar2(254),
      county_id                  char(18),
      assessor_xlate             varchar2(254),
      assessor_id                number(22,0),
      parcel_xlate                  varchar2(254),
      parcel_id                     number(22,0),
      parcel_type_xlate          varchar2(254),
      parcel_type_id             number(22,0),
      tax_district_xlate            varchar2(254),
      tax_district_id               number(22,0),
      asset_location_xlate       varchar2(254),
      asset_location_id          number(22,0),
      statistic                     varchar2(35),
      statistics_full_id            number(22,0),
      error_message              varchar2(4000)
   );

alter table pwrplant.pt_import_stats_full add ( constraint pt_import_stats_full_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_stats_full add ( constraint pt_impt_statsfull_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_stats_full_archive
create table pwrplant.pt_import_stats_full_archive
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year_xlate             varchar2(254),
      tax_year                   number(22,0),
      allocation_xlate           varchar2(254),
      allocation_id                 number(22,0),
      prop_tax_location_xlate    varchar2(254),
      prop_tax_location_id       number(22,0),
      vintage                    varchar2(35),
      state_xlate                varchar2(254),
      state_id                   char(18),
      prop_tax_company_xlate  varchar2(254),
      prop_tax_company_id     number(22,0),
      county_xlate               varchar2(254),
      county_id                  char(18),
      assessor_xlate             varchar2(254),
      assessor_id                number(22,0),
      parcel_xlate                  varchar2(254),
      parcel_id                     number(22,0),
      parcel_type_xlate          varchar2(254),
      parcel_type_id             number(22,0),
      tax_district_xlate            varchar2(254),
      tax_district_id               number(22,0),
      asset_location_xlate       varchar2(254),
      asset_location_id          number(22,0),
      statistic                     varchar2(35),
      statistics_full_id            number(22,0)
   );

alter table pwrplant.pt_import_stats_full_archive add ( constraint pt_import_stats_full_archiv_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_stats_full_archive add ( constraint pt_impt_sfull_arch_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_stats_incr
create table pwrplant.pt_import_stats_incr
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year_xlate             varchar2(254),
      tax_year                   number(22,0),
      allocation_xlate           varchar2(254),
      allocation_id                 number(22,0),
      prop_tax_location_xlate    varchar2(254),
      prop_tax_location_id       number(22,0),
      state_xlate                varchar2(254),
      state_id                   char(18),
      prop_tax_company_xlate  varchar2(254),
      prop_tax_company_id     number(22,0),
      county_xlate               varchar2(254),
      county_id                  char(18),
      assessor_xlate             varchar2(254),
      assessor_id                number(22,0),
      parcel_xlate                  varchar2(254),
      parcel_id                     number(22,0),
      parcel_type_xlate          varchar2(254),
      parcel_type_id             number(22,0),
      tax_district_xlate            varchar2(254),
      tax_district_id               number(22,0),
      asset_location_xlate       varchar2(254),
      asset_location_id          number(22,0),
      add_statistic              varchar2(35),
      ret_statistic                 varchar2(35),
      trans_statistic               varchar2(35),
      statistics_incr_id            number(22,0),
      error_message              varchar2(4000)
   );

alter table pwrplant.pt_import_stats_incr add ( constraint pt_import_stats_incr_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_stats_incr add ( constraint pt_impt_statsincr_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_stats_incr_archive
create table pwrplant.pt_import_stats_incr_archive
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year_xlate             varchar2(254),
      tax_year                   number(22,0),
      allocation_xlate           varchar2(254),
      allocation_id                 number(22,0),
      prop_tax_location_xlate    varchar2(254),
      prop_tax_location_id       number(22,0),
      state_xlate                varchar2(254),
      state_id                   char(18),
      prop_tax_company_xlate  varchar2(254),
      prop_tax_company_id     number(22,0),
      county_xlate               varchar2(254),
      county_id                  char(18),
      assessor_xlate             varchar2(254),
      assessor_id                number(22,0),
      parcel_xlate                  varchar2(254),
      parcel_id                     number(22,0),
      parcel_type_xlate          varchar2(254),
      parcel_type_id             number(22,0),
      tax_district_xlate            varchar2(254),
      tax_district_id               number(22,0),
      asset_location_xlate       varchar2(254),
      asset_location_id          number(22,0),
      add_statistic              varchar2(35),
      ret_statistic                 varchar2(35),
      trans_statistic               varchar2(35),
      statistics_incr_id            number(22,0)
   );

alter table pwrplant.pt_import_stats_incr_archive add ( constraint pt_import_stats_incr_archiv_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_stats_incr_archive add ( constraint pt_impt_sincr_arch_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_escval_index
create table pwrplant.pt_import_escval_index
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      escalated_value_type_xlate    varchar2(254),
      escalated_value_type_id       number(22,0),
      index_year                    varchar2(35),
      index_percent                 varchar2(35),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_escval_index add ( constraint pt_import_escval_index_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_escval_index add ( constraint pt_impt_escval_ndx_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_escval_index_archive
create table pwrplant.pt_import_escval_index_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      escalated_value_type_xlate    varchar2(254),
      escalated_value_type_id       number(22,0),
      index_year                    varchar2(35),
      index_percent                 varchar2(35)
   );

alter table pwrplant.pt_import_escval_index_archive add ( constraint pt_import_escval_ndx_archiv_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_escval_index_archive add ( constraint pt_impt_escvalndx_arch_irun_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );


-- pt_import_levy_rates
create table pwrplant.pt_import_levy_rates
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      state_xlate                varchar2(254),
      state_id                   char(18),
      county_xlate               varchar2(254),
      county_id                  char(18),
      tax_district_xlate            varchar2(254),
      tax_district_id               number(22,0),
      statement_group_xlate      varchar2(254),
      statement_group_id         number(22,0),
      tax_authority_xlate        varchar2(254),
      tax_authority_id           number(22,0),
      levy_class_xlate           varchar2(254),
      levy_class_id              number(22,0),
      case_xlate                 varchar2(254),
      case_id                    number(22,0),
      mill_levy_rate             varchar2(35),
      unit_rate                     varchar2(35),
      threshold_amount           varchar2(35),
      mill_levy_rate_over_thold  varchar2(35),
      error_message              varchar2(4000)
   );

alter table pwrplant.pt_import_levy_rates add ( constraint pt_import_levy_rates_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_levy_rates add ( constraint pt_impt_levy_rates_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_levy_rates_archive
create table pwrplant.pt_import_levy_rates_archive
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      state_xlate                varchar2(254),
      state_id                   char(18),
      county_xlate               varchar2(254),
      county_id                  char(18),
      tax_district_xlate            varchar2(254),
      tax_district_id               number(22,0),
      statement_group_xlate      varchar2(254),
      statement_group_id         number(22,0),
      tax_authority_xlate        varchar2(254),
      tax_authority_id           number(22,0),
      levy_class_xlate           varchar2(254),
      levy_class_id              number(22,0),
      case_xlate                 varchar2(254),
      case_id                    number(22,0),
      mill_levy_rate             varchar2(35),
      unit_rate                     varchar2(35),
      threshold_amount           varchar2(35),
      mill_levy_rate_over_thold  varchar2(35)
   );

alter table pwrplant.pt_import_levy_rates_archive add ( constraint pt_import_levy_rates_archiv_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_levy_rates_archive add ( constraint pt_impt_levyrates_arch_irun_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );


-- pt_import_parcel
create table pwrplant.pt_import_parcel
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                  varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      description                   varchar2(254),
      long_description              varchar2(254),
      parcel_number                 varchar2(254),
      grantor                       varchar2(254),
      land_acreage                  varchar2(35),
      improve_sq_ft                 varchar2(35),
      mineral_acreage               varchar2(35),
      company_grid_number        varchar2(254),
      state_grid_number          varchar2(254),
      x_coordinate                  varchar2(254),
      y_coordinate                  varchar2(254),
      retired_date                  varchar2(35),
      address_1                     varchar2(254),
      address_2                     varchar2(254),
      city                          varchar2(254),
      zip_code                      varchar2(254),
      notes                         varchar2(4000),
      legal_description             varchar2(4000),
      legal_description_b           varchar2(4000),
      legal_description_c           varchar2(4000),
      legal_description_d           varchar2(4000),
      type_code_xlate               varchar2(254),
      type_code_id                  number(22,0),
      tax_district_code             varchar2(254),
      grid_coordinate               varchar2(254),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      use_composite_auth_xlate      varchar2(254),
      use_composite_authority_yn number(22,0),
      is_parcel_one_to_one_xlate varchar2(254),
      is_parcel_one_to_one       number(22,0),
      assignment_indicator          varchar2(254),
      flex_1                        varchar2(35),
      flex_2                        varchar2(35),
      flex_3                        varchar2(35),
      flex_4                        varchar2(35),
      flex_5                        varchar2(35),
      flex_6                        varchar2(35),
      flex_7                        varchar2(35),
      flex_8                        varchar2(35),
      flex_9                        varchar2(35),
      flex_10                       varchar2(35),
      flex_11                       varchar2(254),
      flex_12                       varchar2(254),
      flex_13                       varchar2(254),
      flex_14                       varchar2(254),
      flex_15                       varchar2(254),
      flex_16                       varchar2(254),
      flex_17                       varchar2(254),
      flex_18                       varchar2(254),
      flex_19                       varchar2(254),
      flex_20                       varchar2(254),
      flex_21                       varchar2(254),
      flex_22                       varchar2(254),
      flex_23                       varchar2(2000),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_parcel add ( constraint pt_import_parcel_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_parcel add ( constraint pt_import_parcel_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_parcel_archive
create table pwrplant.pt_import_parcel_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                  varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      description                   varchar2(254),
      long_description              varchar2(254),
      parcel_number                 varchar2(254),
      grantor                       varchar2(254),
      land_acreage                  varchar2(35),
      improve_sq_ft                 varchar2(35),
      mineral_acreage               varchar2(35),
      company_grid_number        varchar2(254),
      state_grid_number          varchar2(254),
      x_coordinate                  varchar2(254),
      y_coordinate                  varchar2(254),
      retired_date                  varchar2(35),
      address_1                     varchar2(254),
      address_2                     varchar2(254),
      city                          varchar2(254),
      zip_code                      varchar2(254),
      notes                         varchar2(4000),
      legal_description             varchar2(4000),
      legal_description_b           varchar2(4000),
      legal_description_c           varchar2(4000),
      legal_description_d           varchar2(4000),
      type_code_xlate               varchar2(254),
      type_code_id                  number(22,0),
      tax_district_code             varchar2(254),
      grid_coordinate               varchar2(254),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      use_composite_auth_xlate      varchar2(254),
      use_composite_authority_yn number(22,0),
      is_parcel_one_to_one_xlate varchar2(254),
      is_parcel_one_to_one       number(22,0),
      assignment_indicator          varchar2(254),
      flex_1                        varchar2(35),
      flex_2                        varchar2(35),
      flex_3                        varchar2(35),
      flex_4                        varchar2(35),
      flex_5                        varchar2(35),
      flex_6                        varchar2(35),
      flex_7                        varchar2(35),
      flex_8                        varchar2(35),
      flex_9                        varchar2(35),
      flex_10                       varchar2(35),
      flex_11                       varchar2(254),
      flex_12                       varchar2(254),
      flex_13                       varchar2(254),
      flex_14                       varchar2(254),
      flex_15                       varchar2(254),
      flex_16                       varchar2(254),
      flex_17                       varchar2(254),
      flex_18                       varchar2(254),
      flex_19                       varchar2(254),
      flex_20                       varchar2(254),
      flex_21                       varchar2(254),
      flex_22                       varchar2(254),
      flex_23                       varchar2(2000),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0)
   );

alter table pwrplant.pt_import_parcel_archive add ( constraint pt_import_parcel_archive_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_parcel_archive add ( constraint pt_impt_prcl_arch_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_asmt
create table pwrplant.pt_import_prcl_asmt
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                  varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      assessment_group_xlate     varchar2(254),
      assessment_group_id           number(22,0),
      case_xlate                    varchar2(254),
      case_id                       number(22,0),
      assessment                    varchar2(35),
      equalized_value               varchar2(35),
      taxable_value                 varchar2(35),
      assessment_date               varchar2(35),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_prcl_asmt add ( constraint pt_import_prcl_asmt_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_asmt add ( constraint pt_import_pasmt_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run);



-- pt_import_prcl_asmt_archive
create table pwrplant.pt_import_prcl_asmt_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                  varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      assessment_group_xlate     varchar2(254),
      assessment_group_id           number(22,0),
      case_xlate                    varchar2(254),
      case_id                       number(22,0),
      assessment                    varchar2(35),
      equalized_value               varchar2(35),
      taxable_value                 varchar2(35),
      assessment_date               varchar2(35)
   );

alter table pwrplant.pt_import_prcl_asmt_archive add ( constraint pt_import_prcl_asmt_archive_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_asmt_archive add ( constraint pt_impt_pasmt_arch_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_cwip_assign
create table pwrplant.pt_import_cwip_assign
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      work_order_xlate              varchar2(254),
      work_order_id                 number(22,0),
      company_xlate                 varchar2(254),
      company_id                    number(22,0),
      property_tax_type_xlate    varchar2(254),
      property_tax_type_id       number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_cwip_assign add ( constraint pt_import_cwip_assign_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_cwip_assign add ( constraint pt_import_cwipasgn_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_cwip_assign_archive
create table pwrplant.pt_import_cwip_assign_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      work_order_xlate              varchar2(254),
      work_order_id                 number(22,0),
      company_xlate                 varchar2(254),
      company_id                    number(22,0),
      property_tax_type_xlate    varchar2(254),
      property_tax_type_id       number(22,0)
   );

alter table pwrplant.pt_import_cwip_assign_archive add ( constraint pt_import_cwip_assign_arch_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_cwip_assign_archive add ( constraint pt_impt_casn_arch_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_auth_dist
create table pwrplant.pt_import_auth_dist
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year_xlate             varchar2(254),
      tax_year                   number(22,0),
      tax_authority_xlate        varchar2(254),
      tax_authority_id           number(22,0),
      tax_district_xlate            varchar2(254),
      tax_district_id               number(22,0),
      percentage                 varchar2(35),
      external_code1          varchar2(35),
      external_code2          varchar2(35),
      state_xlate                varchar2(254),
      state_id                   char(18),
      county_xlate               varchar2(254),
      county_id                  char(18),
      error_message              varchar2(4000)
   );

alter table pwrplant.pt_import_auth_dist add ( constraint pt_import_auth_dist_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_auth_dist add ( constraint pt_impt_authdist_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_auth_dist_archive
create table pwrplant.pt_import_auth_dist_archive
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      tax_year_xlate             varchar2(254),
      tax_year                   number(22,0),
      tax_authority_xlate        varchar2(254),
      tax_authority_id           number(22,0),
      tax_district_xlate            varchar2(254),
      tax_district_id               number(22,0),
      percentage                 varchar2(35),
      external_code1          varchar2(35),
      external_code2          varchar2(35),
      state_xlate                varchar2(254),
      state_id                   char(18),
      county_xlate               varchar2(254),
      county_id                  char(18)
   );

alter table pwrplant.pt_import_auth_dist_archive add ( constraint pt_import_auth_dist_archive_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_auth_dist_archive add ( constraint pt_impt_adist_arch_impt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_rsp_ent
create table pwrplant.pt_import_prcl_rsp_ent
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      address_1                     varchar2(254),
      address_2                     varchar2(254),
      address_3                     varchar2(254),
      city                          varchar2(254),
      state                         varchar2(254),
      zip_code                      varchar2(254),
      phone_1                       varchar2(254),
      phone_2                       varchar2(254),
      contact_name                  varchar2(254),
      entity_number                 varchar2(254),
      external_code                 varchar2(254),
      tax_id_code                   varchar2(254),
      tax_id_number                 varchar2(254),
      responsible_entity_xlate         varchar2(254),
      responsible_entity_id            number(22,0),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      notes                            varchar2(4000),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_prcl_rsp_ent add ( constraint pt_import_pr_rsp_ent_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_rsp_ent add ( constraint pt_import_pr_rsp_ent_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_rsp_ent_archive
create table pwrplant.pt_import_prcl_rsp_ent_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      address_1                     varchar2(254),
      address_2                     varchar2(254),
      address_3                     varchar2(254),
      city                          varchar2(254),
      state                         varchar2(254),
      zip_code                      varchar2(254),
      phone_1                       varchar2(254),
      phone_2                       varchar2(254),
      contact_name                  varchar2(254),
      entity_number                 varchar2(254),
      external_code                 varchar2(254),
      tax_id_code                   varchar2(254),
      tax_id_number                 varchar2(254),
      responsible_entity_xlate         varchar2(254),
      responsible_entity_id            number(22,0),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      notes                            varchar2(4000)
   );

alter table pwrplant.pt_import_prcl_rsp_ent_archive add ( constraint pt_import_pr_rsp_ent_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_rsp_ent_archive add ( constraint pt_import_pr_rsp_ent_arc_rn_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_rsp
create table pwrplant.pt_import_prcl_rsp
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      responsible_entity_xlate         varchar2(254),
      responsible_entity_id            number(22,0),
      responsibility_type_xlate        varchar2(254),
      responsibility_type_id           number(22,0),
      percent_responsible           varchar2(35),
      lease_start_date              varchar2(35),
      lease_end_date             varchar2(35),
      entity_is_lessor_xlate           varchar2(254),
      entity_is_lessor                 number(22,0),
      entity_is_lessee_xlate           varchar2(254),
      entity_is_lessee              number(22,0),
      reference_number              varchar2(254),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_prcl_rsp add ( constraint pt_import_pr_rsp_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_rsp add ( constraint pt_import_pr_rsp_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_rsp_archive
create table pwrplant.pt_import_prcl_rsp_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      responsible_entity_xlate         varchar2(254),
      responsible_entity_id            number(22,0),
      responsibility_type_xlate        varchar2(254),
      responsibility_type_id           number(22,0),
      percent_responsible           varchar2(35),
      lease_start_date              varchar2(35),
      lease_end_date             varchar2(35),
      entity_is_lessor_xlate           varchar2(254),
      entity_is_lessor                 number(22,0),
      entity_is_lessee_xlate           varchar2(254),
      entity_is_lessee              number(22,0),
      reference_number              varchar2(254)
   );

alter table pwrplant.pt_import_prcl_rsp_archive add ( constraint pt_import_pr_rsp_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_rsp_archive add ( constraint pt_import_pr_rsp_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_history
create table pwrplant.pt_import_prcl_history
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      event_id                      number(22,0),
      event_date                    varchar2(35),
      notes                         varchar2(4000),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_prcl_history add ( constraint pt_import_pr_hist_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_history add ( constraint pt_import_pr_hist_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_history_archive
create table pwrplant.pt_import_prcl_history_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      event_id                      number(22,0),
      event_date                    varchar2(35),
      notes                         varchar2(4000)
   );

alter table pwrplant.pt_import_prcl_history_archive add ( constraint pt_import_pr_hist_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_history_archive add ( constraint pt_import_pr_hist_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_geo
create table pwrplant.pt_import_prcl_geo
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      geography_type_xlate          varchar2(254),
      geography_type_id          number(22,0),
      value                         varchar2(254),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_prcl_geo add ( constraint pt_import_pr_geo_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_geo add ( constraint pt_import_pr_geo_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_geo_archive
create table pwrplant.pt_import_prcl_geo_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      geography_type_xlate          varchar2(254),
      geography_type_id          number(22,0),
      value                         varchar2(254)
   );

alter table pwrplant.pt_import_prcl_geo_archive add ( constraint pt_import_pr_geo_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_geo_archive add ( constraint pt_import_pr_geo_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_rsvfctr_pcts
create table pwrplant.pt_import_rsvfctr_pcts
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      reserve_factor_xlate          varchar2(254),
      reserve_factor_id             number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      age                           varchar2(35),
      reserve_factor_percent        varchar2(35),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_rsvfctr_pcts add ( constraint pt_import_rf_pcts_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_rsvfctr_pcts add ( constraint pt_import_rf_pcts_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_rsvfctr_pcts_archive
create table pwrplant.pt_import_rsvfctr_pcts_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tax_year_xlate                varchar2(254),
      tax_year                      number(22,0),
      reserve_factor_xlate          varchar2(254),
      reserve_factor_id             number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      age                           varchar2(35),
      reserve_factor_percent        varchar2(35)
   );

alter table pwrplant.pt_import_rsvfctr_pcts_archive add ( constraint pt_import_rf_pcts_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_rsvfctr_pcts_archive add ( constraint pt_import_rf_pcts_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_assessor
create table pwrplant.pt_import_assessor
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      name_line_1                varchar2(254),
      name_line_2                varchar2(254),
      name_line_3                varchar2(254),
      address_line_1                varchar2(254),
      address_line_2                varchar2(254),
      address_line_3                varchar2(254),
      city                          varchar2(254),
      state                         varchar2(254),
      zip_code                      varchar2(254),
      phone_1                       varchar2(254),
      phone_2                       varchar2(254),
      ext_assessor_code          varchar2(254),
      notes                         varchar2(2000),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_assessor add ( constraint pt_import_assr_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_assessor add ( constraint pt_import_assr_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_assessor_archive
create table pwrplant.pt_import_assessor_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      name_line_1                varchar2(254),
      name_line_2                varchar2(254),
      name_line_3                varchar2(254),
      address_line_1                varchar2(254),
      address_line_2                varchar2(254),
      address_line_3                varchar2(254),
      city                          varchar2(254),
      state                         varchar2(254),
      zip_code                      varchar2(254),
      phone_1                       varchar2(254),
      phone_2                       varchar2(254),
      ext_assessor_code          varchar2(254),
      notes                         varchar2(2000),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0)
   );

alter table pwrplant.pt_import_assessor_archive add ( constraint pt_import_assr_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_assessor_archive add ( constraint pt_import_assr_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_type_code
create table pwrplant.pt_import_type_code
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      external_code                 varchar2(254),
      parent_type_code_xlate        varchar2(254),
      parent_type_code_id           number(22,0),
      type_code_xlate               varchar2(254),
      type_code_id                  number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_type_code add ( constraint pt_import_tc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_type_code add ( constraint pt_import_tc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_type_code_archive
create table pwrplant.pt_import_type_code_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      external_code                 varchar2(254),
      parent_type_code_xlate        varchar2(254),
      parent_type_code_id           number(22,0),
      type_code_xlate               varchar2(254),
      type_code_id                  number(22,0)
   );

alter table pwrplant.pt_import_type_code_archive add ( constraint pt_import_tc_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_type_code_archive add ( constraint pt_import_tc_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_tax_dist
create table pwrplant.pt_import_tax_dist
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      assignment_indicator          varchar2(254),
      type_code_xlate               varchar2(254),
      type_code_id                  number(22,0),
      description                   varchar2(254),
      long_description              varchar2(254),
      grid_coordinate                  varchar2(254),
      tax_district_code             varchar2(254),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      is_parcel_one_to_one_xlate    varchar2(254),
      is_parcel_one_to_one          number(22,0),
      use_composite_auth_xlate      varchar2(254),
      use_composite_authority_yn number(22,0),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_tax_dist add ( constraint pt_import_td_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_tax_dist add ( constraint pt_import_td_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_tax_dist_archive
create table pwrplant.pt_import_tax_dist_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      assignment_indicator          varchar2(254),
      type_code_xlate               varchar2(254),
      type_code_id                  number(22,0),
      description                   varchar2(254),
      long_description              varchar2(254),
      grid_coordinate                  varchar2(254),
      tax_district_code             varchar2(254),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      is_parcel_one_to_one_xlate    varchar2(254),
      is_parcel_one_to_one          number(22,0),
      use_composite_auth_xlate      varchar2(254),
      use_composite_authority_yn number(22,0),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0)
   );

alter table pwrplant.pt_import_tax_dist_archive add ( constraint pt_import_td_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_tax_dist_archive add ( constraint pt_import_td_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_minor_loc
create table pwrplant.pt_import_minor_loc
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      minor_location_xlate          varchar2(254),
      minor_location_id             number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_minor_loc add ( constraint pt_import_minloc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_minor_loc add ( constraint pt_import_minloc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_minor_loc_archive
create table pwrplant.pt_import_minor_loc_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      minor_location_xlate          varchar2(254),
      minor_location_id             number(22,0)
   );

alter table pwrplant.pt_import_minor_loc_archive add ( constraint pt_import_minloc_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_minor_loc_archive add ( constraint pt_import_minloc_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_loc_type
create table pwrplant.pt_import_loc_type
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      location_type_xlate           varchar2(254),
      location_type_id              number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_loc_type add ( constraint pt_import_lt_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_loc_type add ( constraint pt_import_lt_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_loc_type_archive
create table pwrplant.pt_import_loc_type_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      location_type_xlate           varchar2(254),
      location_type_id              number(22,0)
   );

alter table pwrplant.pt_import_loc_type_archive add ( constraint pt_import_lt_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_loc_type_archive add ( constraint pt_import_lt_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_division
create table pwrplant.pt_import_division
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      company_xlate                 varchar2(254),
      company_id                    number(22,0),
      bus_segment_xlate          varchar2(254),
      bus_segment_id             number(22,0),
      description                   varchar2(254),
      long_description              varchar2(254),
      division_xlate                varchar2(254),
      division_id                   number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_division add ( constraint pt_import_div_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_division add ( constraint pt_import_div_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_division_archive
create table pwrplant.pt_import_division_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      company_xlate                 varchar2(254),
      company_id                    number(22,0),
      bus_segment_xlate          varchar2(254),
      bus_segment_id             number(22,0),
      description                   varchar2(254),
      long_description              varchar2(254),
      division_xlate                varchar2(254),
      division_id                   number(22,0)
   );

alter table pwrplant.pt_import_division_archive add ( constraint pt_import_div_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_division_archive add ( constraint pt_import_div_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_municipality
create table pwrplant.pt_import_municipality
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      municipality_id                  varchar2(254),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_municipality add ( constraint pt_import_muni_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_municipality add ( constraint pt_import_muni_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );


-- pt_import_municipality_archive
create table pwrplant.pt_import_municipality_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      municipality_id                  varchar2(254),
      state_xlate                   varchar2(254),
      state_id                      char(18)
   );

alter table pwrplant.pt_import_municipality_archive add ( constraint pt_import_muni_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_municipality_archive add ( constraint pt_import_muni_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_major_loc
create table pwrplant.pt_import_major_loc
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      municipality_xlate               varchar2(254),
      municipality_id                  char(18),
      division_xlate                varchar2(254),
      division_id                   number(22,0),
      location_type_xlate           varchar2(254),
      location_type_id              number(22,0),
      external_location_id          varchar2(254),
      grid_coordinate                  varchar2(254),
      description                   varchar2(254),
      long_description              varchar2(254),
      address                       varchar2(254),
      zip_code                      varchar2(254),
      location_report                  varchar2(254),
      rate_area_xlate               varchar2(254),
      rate_area_id                  number(22,0),
      status_code_xlate             varchar2(254),
      status_code_id                number(22,0),
      major_location_xlate          varchar2(254),
      major_location_id             number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_major_loc add ( constraint pt_import_majloc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_major_loc add ( constraint pt_import_majloc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_major_loc_archive
create table pwrplant.pt_import_major_loc_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      municipality_xlate               varchar2(254),
      municipality_id                  char(18),
      division_xlate                varchar2(254),
      division_id                   number(22,0),
      location_type_xlate           varchar2(254),
      location_type_id              number(22,0),
      external_location_id          varchar2(254),
      grid_coordinate                  varchar2(254),
      description                   varchar2(254),
      long_description              varchar2(254),
      address                       varchar2(254),
      zip_code                      varchar2(254),
      location_report                  varchar2(254),
      rate_area_xlate               varchar2(254),
      rate_area_id                  number(22,0),
      status_code_xlate             varchar2(254),
      status_code_id                number(22,0),
      major_location_xlate          varchar2(254),
      major_location_id             number(22,0)
   );

alter table pwrplant.pt_import_major_loc_archive add ( constraint pt_import_majloc_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_major_loc_archive add ( constraint pt_import_majloc_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_proptax_loc
create table pwrplant.pt_import_proptax_loc
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      description                   varchar2(254),
      location_code                 varchar2(254),
      voltage                       varchar2(254),
      address_1                     varchar2(254),
      address_2                     varchar2(254),
      city                          varchar2(254),
      zip_code                      varchar2(254),
      location_rollup_xlate            varchar2(254),
      location_rollup_id               number(22,0),
      prop_tax_location_xlate          varchar2(254),
      prop_tax_location_id             number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_proptax_loc add ( constraint pt_import_ptloc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_proptax_loc add ( constraint pt_import_ptloc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_proptax_loc_archive
create table pwrplant.pt_import_proptax_loc_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      description                   varchar2(254),
      location_code                 varchar2(254),
      voltage                       varchar2(254),
      address_1                     varchar2(254),
      address_2                     varchar2(254),
      city                          varchar2(254),
      zip_code                      varchar2(254),
      location_rollup_xlate            varchar2(254),
      location_rollup_id               number(22,0),
      prop_tax_location_xlate          varchar2(254),
      prop_tax_location_id             number(22,0)
   );

alter table pwrplant.pt_import_proptax_loc_archive add ( constraint pt_import_ptloc_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_proptax_loc_archive add ( constraint pt_import_ptloc_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_asset_loc
create table pwrplant.pt_import_asset_loc
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tax_location_xlate               varchar2(254),
      tax_location_id                  number(22,0),
      minor_location_xlate          varchar2(254),
      minor_location_id             number(22,0),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      major_location_xlate          varchar2(254),
      major_location_id             number(22,0),
      minor_location2_xlate            varchar2(254),
      minor_location_id2            number(22,0),
      line_number_xlate             varchar2(254),
      line_number_id                number(22,0),
      ext_asset_location               varchar2(254),
      long_description              varchar2(254),
      status_code_xlate             varchar2(254),
      status_code_id                number(22,0),
      grid_coordinate                  varchar2(254),
      prop_tax_location_xlate       varchar2(254),
      prop_tax_location_id          number(22,0),
      address                       varchar2(254),
      zip_code                      varchar2(254),
      town_xlate                    varchar2(254),
      town_id                       number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      charge_location_xlate            varchar2(254),
      charge_location_id               number(22,0),
      grid_coordinate1              varchar2(254),
      grid_coordinate2              varchar2(254),
      repair_location_xlate            varchar2(254),
      repair_location_id               number(22,0),
      asset_location_xlate          varchar2(254),
      asset_location_id             number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_asset_loc add ( constraint pt_import_astloc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_asset_loc add ( constraint pt_import_astloc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_asset_loc_archive
create table pwrplant.pt_import_asset_loc_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      tax_location_xlate               varchar2(254),
      tax_location_id                  number(22,0),
      minor_location_xlate          varchar2(254),
      minor_location_id             number(22,0),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      major_location_xlate          varchar2(254),
      major_location_id             number(22,0),
      minor_location2_xlate            varchar2(254),
      minor_location_id2            number(22,0),
      line_number_xlate             varchar2(254),
      line_number_id                number(22,0),
      ext_asset_location               varchar2(254),
      long_description              varchar2(254),
      status_code_xlate             varchar2(254),
      status_code_id                number(22,0),
      grid_coordinate                  varchar2(254),
      prop_tax_location_xlate       varchar2(254),
      prop_tax_location_id          number(22,0),
      address                       varchar2(254),
      zip_code                      varchar2(254),
      town_xlate                    varchar2(254),
      town_id                       number(22,0),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      charge_location_xlate            varchar2(254),
      charge_location_id               number(22,0),
      grid_coordinate1              varchar2(254),
      grid_coordinate2              varchar2(254),
      repair_location_xlate            varchar2(254),
      repair_location_id               number(22,0),
      asset_location_xlate          varchar2(254),
      asset_location_id             number(22,0)
   );

alter table pwrplant.pt_import_asset_loc_archive add ( constraint pt_import_astloc_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_asset_loc_archive add ( constraint pt_import_astloc_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_loc
create table pwrplant.pt_import_prcl_loc
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      asset_location_xlate          varchar2(254),
      asset_location_id             number(22,0),
      allocated_xlate                  varchar2(254),
      allocated_yn                     number(22,0),
      parcel_type_xlate_map         varchar2(254),
      parcel_type_id_map            number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_prcl_loc add ( constraint pt_import_prclloc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_loc add ( constraint pt_import_prclloc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_prcl_loc_archive
create table pwrplant.pt_import_prcl_loc_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      state_xlate                   varchar2(254),
      state_id                      char(18),
      county_xlate                     varchar2(254),
      county_id                     char(18),
      assessor_xlate                varchar2(254),
      assessor_id                   number(22,0),
      tax_district_xlate               varchar2(254),
      tax_district_id                  number(22,0),
      parcel_type_xlate             varchar2(254),
      parcel_type_id                number(22,0),
      prop_tax_company_xlate        varchar2(254),
      prop_tax_company_id           number(22,0),
      parcel_xlate                     varchar2(254),
      parcel_id                        number(22,0),
      asset_location_xlate          varchar2(254),
      asset_location_id             number(22,0),
      allocated_xlate                  varchar2(254),
      allocated_yn                     number(22,0),
      parcel_type_xlate_map         varchar2(254),
      parcel_type_id_map            number(22,0)
   );

alter table pwrplant.pt_import_prcl_loc_archive add ( constraint pt_import_prclloc_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_prcl_loc_archive add ( constraint pt_import_prclloc_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );


-- pt_import_stmt
create table pwrplant.pt_import_stmt
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      description                varchar2(254),
      long_description           varchar2(254),
      account_number          varchar2(254),
      state_xlate                varchar2(254),
      state_id                   char(18),
      county_xlate                  varchar2(254),
      county_id                  char(18),
      statement_xlate            varchar2(254),
      statement_id               number(22,0),
      statement_notes            varchar2(254),
      statement_year_xlate       varchar2(254),
      statement_year_id          number(22,0),
      assessment_year_xlate      varchar2(254),
      assessment_year_id         number(22,0),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      statement_group_xlate      varchar2(254),
      statement_group_id         number(22,0),
      statement_number        varchar2(254),
      received_date              varchar2(35),
      estimated_switch_xlate     varchar2(254),
      estimated_switch_yn        number(22,0),
      year_notes                 varchar2(2000),
      tax_amount                 varchar2(35),
      credit_amount              varchar2(35),
      penalty_amount          varchar2(35),
      interest_amount            varchar2(35),
      assessment                 varchar2(35),
      taxable_value              varchar2(35),
      error_message              varchar2(4000)
   );

alter table pwrplant.pt_import_stmt add ( constraint pt_import_stmt_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_stmt add ( constraint pt_impt_stmt_imp_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );


-- pt_import_stmt_archive
create table pwrplant.pt_import_stmt_archive
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      description                varchar2(254),
      long_description           varchar2(254),
      account_number          varchar2(254),
      state_xlate                varchar2(254),
      state_id                   char(18),
      county_xlate                  varchar2(254),
      county_id                  char(18),
      statement_xlate            varchar2(254),
      statement_id               number(22,0),
      statement_notes            varchar2(254),
      statement_year_xlate       varchar2(254),
      statement_year_id          number(22,0),
      assessment_year_xlate      varchar2(254),
      assessment_year_id         number(22,0),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      statement_group_xlate      varchar2(254),
      statement_group_id         number(22,0),
      statement_number        varchar2(254),
      received_date              varchar2(35),
      estimated_switch_xlate     varchar2(254),
      estimated_switch_yn        number(22,0),
      year_notes                 varchar2(2000),
      tax_amount                 varchar2(35),
      credit_amount              varchar2(35),
      penalty_amount          varchar2(35),
      interest_amount            varchar2(35),
      assessment                 varchar2(35),
      taxable_value              varchar2(35)
   );

alter table pwrplant.pt_import_stmt_archive add ( constraint pt_import_stmt_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_stmt_archive add ( constraint pt_impt_stmt_arc_imp_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );


-- pt_import_stmt_line
create table pwrplant.pt_import_stmt_line
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      statement_year_xlate       varchar2(254),
      statement_year_id          number(22,0),
      state_xlate                varchar2(254),
      state_id                   char(18),
      county_xlate                  varchar2(254),
      county_id                  char(18),
      statement_xlate            varchar2(254),
      statement_id               number(22,0),
      tax_authority_xlate        varchar2(254),
      tax_authority_id           number(22,0),
      levy_class_xlate           varchar2(254),
      levy_class_id              number(22,0),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      assessor_xlate             varchar2(254),
      assessor_id                number(22,0),
      parcel_xlate                  varchar2(254),
      parcel_id                     number(22,0),
      verified_status_xlate         varchar2(254),
      verified_status_id            number(22,0),
      first_install_xlate           varchar2(254),
      first_install_yn              number(22,0),
      tax_amount                 varchar2(35),
      credit_amount              varchar2(35),
      penalty_amount          varchar2(35),
      interest_amount            varchar2(35),
      assessment                 varchar2(35),
      taxable_value              varchar2(35),
      notes                      varchar2(2000),
      error_message              varchar2(4000)
   );

alter table pwrplant.pt_import_stmt_line add ( constraint pt_import_stmt_ln_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_stmt_line add ( constraint pt_impt_stmt_ln_imp_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );


-- pt_import_stmt_line_archive
create table pwrplant.pt_import_stmt_line_archive
   (  import_run_id              number(22,0)   not null,
      line_id                    number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      statement_year_xlate       varchar2(254),
      statement_year_id          number(22,0),
      state_xlate                varchar2(254),
      state_id                   char(18),
      county_xlate                  varchar2(254),
      county_id                  char(18),
      statement_xlate            varchar2(254),
      statement_id               number(22,0),
      tax_authority_xlate        varchar2(254),
      tax_authority_id           number(22,0),
      levy_class_xlate           varchar2(254),
      levy_class_id              number(22,0),
      prop_tax_company_xlate     varchar2(254),
      prop_tax_company_id        number(22,0),
      assessor_xlate             varchar2(254),
      assessor_id                number(22,0),
      parcel_xlate                  varchar2(254),
      parcel_id                     number(22,0),
      verified_status_xlate         varchar2(254),
      verified_status_id            number(22,0),
      first_install_xlate           varchar2(254),
      first_install_yn              number(22,0),
      tax_amount                 varchar2(35),
      credit_amount              varchar2(35),
      penalty_amount          varchar2(35),
      interest_amount            varchar2(35),
      assessment                 varchar2(35),
      taxable_value              varchar2(35),
      notes                      varchar2(2000)
   );

alter table pwrplant.pt_import_stmt_line_archive add ( constraint pt_import_stmt_ln_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_stmt_line_archive add ( constraint pt_impt_stmt_ln_arc_imp_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );


-- pt_import_util_account
create table pwrplant.pt_import_util_account
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      bus_segment_xlate          varchar2(254),
      bus_segment_id             number(22,0),
      ferc_plt_acct_xlate              varchar2(254),
      ferc_plt_acct_id              number(22,0),
      func_class_xlate              varchar2(254),
      func_class_id                 number(22,0),
      description                   varchar2(254),
      status_code_xlate             varchar2(254),
      status_code_id                number(22,0),
      external_account_code         varchar2(254),
      utility_account_xlate            varchar2(254),
      utility_account_id               number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_util_account add ( constraint pt_import_ua_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_util_account add ( constraint pt_import_ua_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_util_account_archive
create table pwrplant.pt_import_util_account_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      bus_segment_xlate          varchar2(254),
      bus_segment_id             number(22,0),
      ferc_plt_acct_xlate              varchar2(254),
      ferc_plt_acct_id              number(22,0),
      func_class_xlate              varchar2(254),
      func_class_id                 number(22,0),
      description                   varchar2(254),
      status_code_xlate             varchar2(254),
      status_code_id                number(22,0),
      external_account_code         varchar2(254),
      utility_account_xlate            varchar2(254),
      utility_account_id               number(22,0)
   );

alter table pwrplant.pt_import_util_account_archive add ( constraint pt_import_ua_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_util_account_archive add ( constraint pt_import_ua_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_retire_unit
create table pwrplant.pt_import_retire_unit
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      property_unit_xlate           varchar2(254),
      property_unit_id              number(22,0),
      status_code_xlate             varchar2(254),
      status_code_id                number(22,0),
      external_retire_unit          varchar2(254),
      retirement_unit_xlate            varchar2(254),
      retirement_unit_id               number(22,0),
      is_modified                   number(22,0),
      error_message                 varchar2(4000)
   );

alter table pwrplant.pt_import_retire_unit add ( constraint pt_import_ru_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_retire_unit add ( constraint pt_import_ru_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_retire_unit_archive
create table pwrplant.pt_import_retire_unit_archive
   (  import_run_id                 number(22,0)   not null,
      line_id                       number(22,0)   not null,
      time_stamp                    date,
      user_id                       varchar2(18),
      description                   varchar2(254),
      long_description              varchar2(254),
      property_unit_xlate           varchar2(254),
      property_unit_id              number(22,0),
      status_code_xlate             varchar2(254),
      status_code_id                number(22,0),
      external_retire_unit          varchar2(254),
      retirement_unit_xlate            varchar2(254),
      retirement_unit_id               number(22,0)
   );

alter table pwrplant.pt_import_retire_unit_archive add ( constraint pt_import_ru_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_retire_unit_archive add ( constraint pt_import_ru_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_assets
create table pwrplant.pt_import_assets
   (  import_run_id                    number(22,0)   not null,
      line_id                          number(22,0)   not null,
      time_stamp                       date,
      user_id                          varchar2(18),
      company_xlate                    varchar2(254),
      company_id                       number(22,0),
      gl_account_xlate                 varchar2(254),
      gl_account_id                    number(22,0),
      bus_segment_xlate             varchar2(254),
      bus_segment_id                   number(22,0),
      utility_account_xlate               varchar2(254),
      utility_account_id                  number(22,0),
      retirement_unit_xlate               varchar2(254),
      retirement_unit_id               number(22,0),
      state_xlate                      varchar2(254),
      state_id                         char(18),
      asset_location_xlate             varchar2(254),
      asset_location_id                number(22,0),
      description                      varchar2(254),
      long_description                 varchar2(254),
      serial_number                    varchar2(254),
      work_order_number             varchar2(254),
      in_service_year                  varchar2(254),
      eng_in_service_year              varchar2(254),
      quantity                         varchar2(254),
      cost                             varchar2(254),
      disposal_date                    varchar2(254),
      posting_date                     varchar2(254),
      asset_id                         number(22,0),
      property_group_id                number(22,0),
      depr_group_id                    number(22,0),
      books_schema_id                  number(22,0),
      sub_account_id                   number(22,0),
      ledger_status                    number(22,0),
      subledger_indicator              number(22,0),
      act_asset_activity_id               number(22,0),
      act_gl_posting_mo_yr             date,
      act_cpr_posting_mo_yr            date,
      act_gl_je_code                   char(18),
      act_description                     varchar2(35),
      act_long_description             varchar2(254),
      act_activity_code                char(8),
      act_activity_status                 number(22,0),
      act_activity_quantity               number(22,2),
      act_activity_cost                number(22,2),
      act_ferc_activity_code           number(22,0),
      act_month_number              number(22,0),
      trans_from_asset_id              number(22,0),
      trans_to_asset_id                number(22,0),
      trans_from_asset_activity_id     number(22,0),
      trans_to_asset_activity_id1         number(22,0),
      trans_to_asset_activity_id2         number(22,0),
      trans_gl_posting_mo_yr           date,
      trans_cpr_posting_mo_yr       date,
      trans_gl_je_code                 char(18),
      trans_description                varchar2(35),
      trans_long_description           varchar2(254),
      trans_to_activity_code1          char(8),
      trans_to_activity_code2          char(8),
      trans_from_activity_code            char(8),
      trans_activity_status               number(22,0),
      trans_to_activity_quantity1         number(22,2),
      trans_to_activity_cost1          number(22,2),
      trans_to_activity_quantity2         number(22,2),
      trans_to_activity_cost2          number(22,2),
      trans_from_activity_quantity        number(22,2),
      trans_from_activity_cost            number(22,2),
      trans_to_ferc_activity_code1     number(22,0),
      trans_to_ferc_activity_code2     number(22,0),
      trans_from_ferc_activity_code    number(22,0),
      trans_month_number               number(22,0),
      disp_asset_activity_id              number(22,0),
      disp_gl_posting_mo_yr            date,
      disp_cpr_posting_mo_yr           date,
      disp_out_of_service_mo_yr        date,
      disp_gl_je_code                  char(18),
      disp_description                 varchar2(35),
      disp_long_description               varchar2(254),
      disp_disposition_code               number(22,0),
      disp_activity_code                  char(8),
      disp_activity_status             number(22,0),
      disp_activity_quantity              number(22,2),
      disp_activity_cost                  number(22,2),
      disp_ferc_activity_code          number(22,0),
      disp_month_number             number(22,0),
      compute_act_status               number(22,0),
      ferc_plt_acct_xlate                 varchar2(254),
      ferc_plt_acct_id                 number(22,0),
      func_class_xlate                 varchar2(254),
      func_class_id                    number(22,0),
      loaded                           number(22,0),
      is_modified                      number(22,0),
      error_message                    varchar2(4000)
   );

alter table pwrplant.pt_import_assets add ( constraint pt_import_assets_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_assets add ( constraint pt_import_assets_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_assets_archive
create table pwrplant.pt_import_assets_archive
   (  import_run_id                    number(22,0)   not null,
      line_id                          number(22,0)   not null,
      time_stamp                       date,
      user_id                          varchar2(18),
      company_xlate                    varchar2(254),
      company_id                       number(22,0),
      gl_account_xlate                 varchar2(254),
      gl_account_id                    number(22,0),
      bus_segment_xlate             varchar2(254),
      bus_segment_id                   number(22,0),
      utility_account_xlate               varchar2(254),
      utility_account_id                  number(22,0),
      retirement_unit_xlate               varchar2(254),
      retirement_unit_id               number(22,0),
      state_xlate                      varchar2(254),
      state_id                         char(18),
      asset_location_xlate             varchar2(254),
      asset_location_id                number(22,0),
      description                      varchar2(254),
      long_description                 varchar2(254),
      serial_number                    varchar2(254),
      work_order_number             varchar2(254),
      in_service_year                  varchar2(254),
      eng_in_service_year              varchar2(254),
      quantity                         varchar2(254),
      cost                             varchar2(254),
      disposal_date                    varchar2(254),
      posting_date                     varchar2(254),
      asset_id                         number(22,0),
      property_group_id                number(22,0),
      depr_group_id                    number(22,0),
      books_schema_id                  number(22,0),
      sub_account_id                   number(22,0),
      ledger_status                    number(22,0),
      subledger_indicator              number(22,0),
      act_asset_activity_id               number(22,0),
      act_gl_posting_mo_yr             date,
      act_cpr_posting_mo_yr            date,
      act_gl_je_code                   char(18),
      act_description                     varchar2(35),
      act_long_description             varchar2(254),
      act_activity_code                char(8),
      act_activity_status                 number(22,0),
      act_activity_quantity               number(22,2),
      act_activity_cost                number(22,2),
      act_ferc_activity_code           number(22,0),
      act_month_number              number(22,0),
      trans_from_asset_id              number(22,0),
      trans_to_asset_id                number(22,0),
      trans_from_asset_activity_id     number(22,0),
      trans_to_asset_activity_id1         number(22,0),
      trans_to_asset_activity_id2         number(22,0),
      trans_gl_posting_mo_yr           date,
      trans_cpr_posting_mo_yr       date,
      trans_gl_je_code                 char(18),
      trans_description                varchar2(35),
      trans_long_description           varchar2(254),
      trans_to_activity_code1          char(8),
      trans_to_activity_code2          char(8),
      trans_from_activity_code            char(8),
      trans_activity_status               number(22,0),
      trans_to_activity_quantity1         number(22,2),
      trans_to_activity_cost1          number(22,2),
      trans_to_activity_quantity2         number(22,2),
      trans_to_activity_cost2          number(22,2),
      trans_from_activity_quantity        number(22,2),
      trans_from_activity_cost            number(22,2),
      trans_to_ferc_activity_code1     number(22,0),
      trans_to_ferc_activity_code2     number(22,0),
      trans_from_ferc_activity_code    number(22,0),
      trans_month_number               number(22,0),
      disp_asset_activity_id              number(22,0),
      disp_gl_posting_mo_yr            date,
      disp_cpr_posting_mo_yr           date,
      disp_out_of_service_mo_yr        date,
      disp_gl_je_code                  char(18),
      disp_description                 varchar2(35),
      disp_long_description               varchar2(254),
      disp_disposition_code               number(22,0),
      disp_activity_code                  char(8),
      disp_activity_status             number(22,0),
      disp_activity_quantity              number(22,2),
      disp_activity_cost                  number(22,2),
      disp_ferc_activity_code          number(22,0),
      disp_month_number             number(22,0),
      compute_act_status               number(22,0),
      ferc_plt_acct_xlate                 varchar2(254),
      ferc_plt_acct_id                 number(22,0),
      func_class_xlate                 varchar2(254),
      func_class_id                    number(22,0),
      loaded                           number(22,0)
   );

alter table pwrplant.pt_import_assets_archive add ( constraint pt_import_assets_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_assets_archive add ( constraint pt_import_assets_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_class_code
create table pwrplant.pt_import_class_code
   (  import_run_id        number(22,0)   not null,
      line_id              number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      class_code_xlate     varchar2(254),
      class_code_id        number(22,0),
      asset_xlate          varchar2(254),
      asset_id             number(22,0),
      value                varchar2(254),
      is_modified          number(22,0),
      error_message        varchar2(4000)
   );

alter table pwrplant.pt_import_class_code add ( constraint pt_import_cc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_class_code add ( constraint pt_import_cc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_import_class_code_archive
create table pwrplant.pt_import_class_code_archive
   (  import_run_id        number(22,0)   not null,
      line_id              number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      class_code_xlate     varchar2(254),
      class_code_id        number(22,0),
      asset_xlate          varchar2(254),
      asset_id             number(22,0),
      value                varchar2(254)
   );

alter table pwrplant.pt_import_class_code_archive add ( constraint pt_import_cc_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_import_class_code_archive add ( constraint pt_import_cc_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run );



-- pt_powertax_company
create table pwrplant.pt_powertax_company
   (  company_id              number(22,0) not null,
      powertax_company_id  number(22,0) not null,
      time_stamp              date,
      user_id                 varchar2(18)
   );

alter table pwrplant.pt_powertax_company add ( constraint pt_ptax_co_pk primary key ( company_id, powertax_company_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_powertax_company add ( constraint pt_ptax_co_co_fk foreign key ( company_id ) references pwrplant.company_setup );



-- pt_sgroup_rollup
create table pwrplant.pt_sgroup_rollup
   (  sg_rollup_id         number(22,0)   not null,
      time_stamp        date,
      user_id           varchar2(18),
      description       varchar2(35)   not null,
      long_description  varchar2(254)
   );

alter table pwrplant.pt_sgroup_rollup add ( constraint pt_sgroup_rollup_pk primary key ( sg_rollup_id ) using index tablespace pwrplant_idx );



-- pt_sgroup_rollup_values
create table pwrplant.pt_sgroup_rollup_values
   (  sg_rollup_id            number(22,0)   not null,
      sg_rollup_value_id      number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      external_code        varchar2(35)
   );

alter table pwrplant.pt_sgroup_rollup_values add ( constraint pt_sgroup_rollup_values_pk primary key ( sg_rollup_id, sg_rollup_value_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_sgroup_rollup_values add ( constraint pt_sg_values_sg_rollup_fk foreign key ( sg_rollup_id ) references pwrplant.pt_sgroup_rollup );



-- pt_sgroup_rollup_assign
create table pwrplant.pt_sgroup_rollup_assign
   (  sg_rollup_id            number(22,0)   not null,
      statement_group_id   number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      sg_rollup_value_id      number(22,0)   not null
   );

alter table pwrplant.pt_sgroup_rollup_assign add ( constraint pt_sgroup_rollup_assign_pk primary key ( sg_rollup_id, statement_group_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_sgroup_rollup_assign add ( constraint pt_sg_assign_sg_values_fk foreign key ( sg_rollup_id, sg_rollup_value_id ) references pwrplant.pt_sgroup_rollup_values ( sg_rollup_id, sg_rollup_value_id ) );
alter table pwrplant.pt_sgroup_rollup_assign add ( constraint pt_sg_assign_sg_fk foreign key ( statement_group_id ) references pwrplant.pt_statement_group );



-- pt_depr_floor_autoadj
create table pwrplant.pt_depr_floor_autoadj
   (  tax_year                   number(22,0) not null,
      prop_tax_company_id     number(22,0) not null,
      state_id                   char(18) not null,
      floor_id                   number(22,0) not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      bus_segment_id_yn       number(22,0) default 0,
      class_code_value1_yn    number(22,0) default 0,
      class_code_value2_yn    number(22,0) default 0,
      county_id_yn               number(22,0) default 0,
      gl_account_id_yn           number(22,0) default 0,
      parcel_id_yn               number(22,0) default 0,
      prop_tax_location_id_yn    number(22,0) default 0,
      property_tax_type_id_yn number(22,0) default 0,
      tax_district_id_yn            number(22,0) default 0,
      type_code_id_yn            number(22,0) default 0,
      utility_account_id_yn         number(22,0) default 0,
      vintage_yn                 number(22,0) default 0,
      vintage_month_yn        number(22,0) default 0,
      spread_on_book          number(22,0) default 0,
      spread_on_adjusted         number(22,0) default 0,
      depr_floor                 number(22,8)
   );

alter table pwrplant.pt_depr_floor_autoadj add ( constraint pt_depr_floor_auto_pk primary key ( tax_year, prop_tax_company_id, state_id, floor_id ) using index tablespace pwrplant_idx );

alter table pwrplant.pt_depr_floor_autoadj add ( constraint pt_depr_floor_auto_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_depr_floor_autoadj add ( constraint pt_depr_floor_auto_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_depr_floor_autoadj add ( constraint pt_depr_floor_auto_st_fk foreign key ( state_id ) references pwrplant.state );



-- pt_depr_floor_autoadj_types
create table pwrplant.pt_depr_floor_autoadj_types
   (  tax_year                number(22,0) not null,
      prop_tax_company_id  number(22,0) not null,
      state_id                char(18) not null,
      floor_id                number(22,0) not null,
      property_tax_type_id number(22,0) not null,
      time_stamp              date,
      user_id                 varchar2(18)
   );

alter table pwrplant.pt_depr_floor_autoadj_types add ( constraint pt_depr_floor_auto_type_pk primary key ( tax_year, prop_tax_company_id, state_id, floor_id, property_tax_type_id ) using index tablespace pwrplant_idx );

alter table pwrplant.pt_depr_floor_autoadj_types add ( constraint pt_depr_floor_auto_type_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_depr_floor_autoadj_types add ( constraint pt_depr_floor_auto_type_ptc_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_depr_floor_autoadj_types add ( constraint pt_depr_floor_auto_type_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_depr_floor_autoadj_types add ( constraint pt_depr_floor_auto_type_ptt_fk foreign key ( property_tax_type_id ) references pwrplant.property_tax_type_data );
alter table pwrplant.pt_depr_floor_autoadj_types add ( constraint pt_depr_floor_auto_type_df_fk foreign key ( tax_year, prop_tax_company_id, state_id, floor_id ) references pwrplant.pt_depr_floor_autoadj ( tax_year, prop_tax_company_id, state_id, floor_id ) );



-- pt_neg_bal_autotrans
create table pwrplant.pt_neg_bal_autotrans
   (  tax_year                   number(22,0) not null,
      prop_tax_company_id     number(22,0) not null,
      state_id                   char(18) not null,
      negtrans_id                number(22,0) not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      bus_segment_id_yn       number(22,0) default 0,
      class_code_value1_yn    number(22,0) default 0,
      class_code_value2_yn    number(22,0) default 0,
      county_id_yn               number(22,0) default 0,
      gl_account_id_yn           number(22,0) default 0,
      parcel_id_yn               number(22,0) default 0,
      prop_tax_location_id_yn    number(22,0) default 0,
      property_tax_type_id_yn number(22,0) default 0,
      tax_district_id_yn            number(22,0) default 0,
      type_rollup_yn             number(22,0) default 0,
      type_rollup_id             number(22,0),
      type_code_id_yn            number(22,0) default 0,
      utility_account_id_yn         number(22,0) default 0,
      vintage_yn                 number(22,0) default 0,
      vintage_month_yn        number(22,0) default 0,
      change_field               varchar2(35),
      logic                      varchar2(254)
   );

alter table pwrplant.pt_neg_bal_autotrans add ( constraint pt_neg_bal_autotrans_pk primary key ( tax_year, prop_tax_company_id, state_id, negtrans_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_neg_bal_autotrans add ( constraint pt_neg_bal_autotrans_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_neg_bal_autotrans add ( constraint pt_neg_bal_autotrans_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_neg_bal_autotrans add ( constraint pt_neg_bal_autotrans_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_neg_bal_autotrans add ( constraint pt_neg_bal_autotrans_tr_fk foreign key ( type_rollup_id ) references pwrplant.pt_type_rollup );



-- pt_type_national_map
create table pwrplant.pt_type_national_map
   (  tax_year                number(22,0) not null,
      from_tax_type_id        number(22,0) not null,
      state_id                char(18) not null,
      time_stamp              date,
      user_id                 varchar2(18),
      to_tax_type_id          number(22,0) not null
   );

alter table pwrplant.pt_type_national_map add ( constraint pt_type_national_map_pk primary key ( tax_year, from_tax_type_id, state_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_type_national_map add ( constraint pt_type_national_map_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );
alter table pwrplant.pt_type_national_map add ( constraint pt_type_national_map_fptt_fk foreign key ( from_tax_type_id ) references pwrplant.property_tax_type_data (property_tax_type_id) );
alter table pwrplant.pt_type_national_map add ( constraint pt_type_national_map_st_fk foreign key ( state_id ) references pwrplant.state );
alter table pwrplant.pt_type_national_map add ( constraint pt_type_national_map_tptt_fk foreign key ( to_tax_type_id ) references pwrplant.property_tax_type_data (property_tax_type_id) );



-- pt_market_value_rate_county
create table pwrplant.pt_market_value_rate_county
   (  pt_rate_id           number(22,0)   not null,
      state_id             char(18)       not null,
      county_id            char(18)       not null,
      tax_year             number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      market_value_rate number(22,8)   not null
   );

alter table pwrplant.pt_market_value_rate_county add ( constraint pt_market_value_rate_county_pk primary key ( pt_rate_id, state_id, county_id, tax_year ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_market_value_rate_county add ( constraint pt_mktval_rt_cnty_rate_fk foreign key ( pt_rate_id ) references pwrplant.pt_rate_definition );
alter table pwrplant.pt_market_value_rate_county add ( constraint pt_mktval_rt_cnty_cnty_fk foreign key ( state_id, county_id ) references pwrplant.county ( state_id, county_id ) );
alter table pwrplant.pt_market_value_rate_county add ( constraint pt_mktval_rt_cnty_ty_fk foreign key ( tax_year ) references pwrplant.property_tax_year );



-- pt_taxable_value_rate_county
create table pwrplant.pt_taxable_value_rate_county
   (  pt_rate_id           number(22,0)   not null,
      state_id             char(18)       not null,
      county_id            char(18)       not null,
      case_id              number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      taxable_value_rate   number(22,8)   not null
   );

alter table pwrplant.pt_taxable_value_rate_county add ( constraint pt_taxable_value_rate_cnty_pk primary key ( pt_rate_id, state_id, county_id, case_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_taxable_value_rate_county add ( constraint pt_tval_rt_cnty_rt_fk foreign key ( pt_rate_id ) references pwrplant.pt_rate_definition );
alter table pwrplant.pt_taxable_value_rate_county add ( constraint pt_tval_rt_cnty_cnty_fk foreign key ( state_id, county_id ) references pwrplant.county ( state_id, county_id ) );
alter table pwrplant.pt_taxable_value_rate_county add ( constraint pt_tval_rt_cnty_case_fk foreign key ( case_id ) references pwrplant.pt_case );



-- pt_year_override
create table pwrplant.pt_year_override
   (  prop_tax_company_id  number(22,0)   not null,
      state_id                char(18)       not null,
      time_stamp              date,
      user_id                 varchar2(18),
      add_activity_year       number(22,0)   not null,
      add_valuation_year      number(22,0)   not null
   );

alter table pwrplant.pt_year_override add ( constraint pt_year_override_pk primary key ( prop_tax_company_id, state_id ) using index tablespace pwrplant_idx );
alter table pwrplant.pt_year_override add ( constraint pt_year_override_ptco_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );
alter table pwrplant.pt_year_override add ( constraint pt_year_override_st_fk foreign key ( state_id ) references pwrplant.state );



-- ptc_report_package
create table pwrplant.ptc_report_package
   (  package_id           number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      package_number    varchar2(35)   not null,
      description          varchar2(35)   not null,
      long_description     varchar2(254),
      separate_jobs_on     varchar2(254)  not null,
      name_jobs_on         varchar2(254)  not null
   );

alter table pwrplant.ptc_report_package add ( constraint ptc_report_package_pk primary key ( package_id ) using index tablespace pwrplant_idx );



-- ptc_report_package_reports
create table pwrplant.ptc_report_package_reports
   (  package_id           number(22,0)   not null,
      report_id               number(22,0)   not null,
      time_stamp           date,
      user_id              varchar2(18),
      report_order         number(22,0)   not null,
      where_clause         varchar2(2000)
   );

alter table pwrplant.ptc_report_package_reports add ( constraint ptc_report_package_reports_pk primary key ( package_id, report_order ) using index tablespace pwrplant_idx );
alter table pwrplant.ptc_report_package_reports add ( constraint ptc_rptpackrtp_rptpack_fk foreign key ( package_id ) references pwrplant.ptc_report_package );
alter table pwrplant.ptc_report_package_reports add ( constraint ptc_rptpackrtp_rpt_fk foreign key ( report_id ) references pwrplant.pp_reports );

create unique index pwrplant.ptc_rpt_pckg_rpt_pack_rpt_ndx on pwrplant.ptc_report_package_reports ( report_id, package_id ) tablespace pwrplant_idx;



-- ptc_documents_type
create table pwrplant.ptc_documents_type
   (  ptc_documents_type_id      number(22,0)   not null,
      time_stamp                 date,
      user_id                    varchar2(18),
      description                varchar2(35)   not null,
      long_description           varchar2(254),
      pk_fields                     varchar2(254)  not null
   );

alter table pwrplant.ptc_documents_type add ( constraint ptc_documents_type_pk primary key ( ptc_documents_type_id ) using index tablespace pwrplant_idx );



-- ptc_documents
create table pwrplant.ptc_documents
   (  ptc_document_id         number(22,0)   not null,
      time_stamp              date,
      user_id                 varchar2(18),
      description             varchar2(35)   not null,
      ptc_documents_type_id   number(22,0)   not null,
      file_name               varchar2(100)  not null,
      file_extension          varchar2(35)   not null,
      filesize                number(22,0),
      attacher                varchar2(18),
      attach_date             date,
      document             blob,
      notes                   varchar2(4000),
      parcel_id                  number(22,0),
      payment_id              number(22,0),
      pt_process_id           number(22,0),
      prop_tax_company_id  number(22,0),
      state_id                char(18),
      statement_id            number(22,0),
      statement_year_id    number(22,0)
   );

alter table pwrplant.ptc_documents add ( constraint ptc_documents_pk primary key ( ptc_document_id ) using index tablespace pwrplant_idx );
alter table pwrplant.ptc_documents add ( constraint ptc_docs_docs_type_fk foreign key ( ptc_documents_type_id ) references pwrplant.ptc_documents_type );

create index pwrplant.ptc_docs_prcl_ndx on pwrplant.ptc_documents ( parcel_id ) tablespace pwrplant_idx;
create index pwrplant.ptc_docs_pymt_ndx on pwrplant.ptc_documents ( payment_id ) tablespace pwrplant_idx;
create index pwrplant.ptc_docs_proc_ndx on pwrplant.ptc_documents ( pt_process_id, prop_tax_company_id, state_id ) tablespace pwrplant_idx;
create index pwrplant.ptc_docs_stmt_ndx on pwrplant.ptc_documents ( statement_id, statement_year_id ) tablespace pwrplant_idx;



-- ptc_script_log
create table pwrplant.ptc_script_log
   (  version           varchar2(35)   not null,
      last_run_script      varchar2(35)   not null
   );

alter table pwrplant.ptc_script_log add ( constraint ptc_script_log_pk primary key ( version, last_run_script ) using index tablespace pwrplant_idx );



----------------------------------------------------
-- CREATE GLOBAL TEMP TABLES
----------------------------------------------------

-- pt_temp_accrual_prior_month
create global temporary table pwrplant.pt_temp_accrual_prior_month
   (  property_tax_ledger_id  number(22,0),
      tax_authority_id        number(22,0),
      accrual_type_id         number(22,0),
      cumulative_charge       number(22,2)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_accrual_prior_month_pk on pwrplant.pt_temp_accrual_prior_month ( property_tax_ledger_id, tax_authority_id, accrual_type_id );



-- pt_temp_actuals_total
create global temporary table pwrplant.pt_temp_actuals_total
   (  statement_id            number(22,0),
      statement_year_id    number(22,0),
      tax_authority_id        number(22,0),
      parcel_id                  number(22,0),
      prop_tax_balance        number(22,2),
      cost_approach_value     number(22,2),
      quantity_factored       number(22,8),
      assessment              number(22,2),
      numrows                 number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_actuals_total_pk on pwrplant.pt_temp_actuals_total ( statement_id, statement_year_id, tax_authority_id, parcel_id );



-- pt_temp_allo_incr_base_pcts
create global temporary table pwrplant.pt_temp_allo_incr_base_pcts
   (  company_id              number(22,0),
      gl_account_id           number(22,0),
      bus_segment_id       number(22,0),
      utility_account_id         number(22,0),
      property_tax_type_id number(22,0),
      prop_tax_location_id    number(22,0),
      vintage                 number(22,0),
      vintage_prior_yn        number(22,0),
      vintage_month           number(22,0),
      class_code_value1    varchar2(254),
      class_code_value2    varchar2(254),
      ledger_detail_id        number(22,0),
      parcel_id                  number(22,0),
      statistic                  number(22,8),
      statistic_pct              number(22,12)
   ) on commit preserve rows;



-- pt_temp_allo_incr_total
create global temporary table pwrplant.pt_temp_allo_incr_total
   (  prop_tax_company_id  number(22,0),
      company_id              number(22,0),
      prop_tax_location_id    number(22,0),
      gl_account_id           number(22,0),
      bus_segment_id       number(22,0),
      utility_account_id         number(22,0),
      property_tax_type_id number(22,0),
      vintage                 number(22,0),
      vintage_month           number(22,0),
      class_code_value1    varchar2(254),
      class_code_value2    varchar2(254),
      ledger_detail_id        number(22,0),
      sum_statistic           number(22,8)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_allo_incr_total_pk on pwrplant.pt_temp_allo_incr_total ( property_tax_type_id, gl_account_id, bus_segment_id, company_id, prop_tax_company_id, utility_account_id, prop_tax_location_id, vintage, vintage_month, class_code_value1, class_code_value2, ledger_detail_id );



-- pt_temp_allo_percents
create global temporary table pwrplant.pt_temp_allo_percents
   (  parcel_id                  number(22,0),
      prop_tax_location_id    number(22,0),
      state_id                char(18),
      county_id               char(18),
      vintage                 number(22,0),
      statistic                  number(22,8),
      statistic_pct              number(22,12),
      add_statistic           number(22,8),
      add_statistic_pct       number(22,12),
      ret_statistic              number(22,8),
      ret_statistic_pct       number(22,12),
      trans_statistic            number(22,8),
      trans_statistic_pct        number(22,12)
   ) on commit preserve rows;



-- pt_temp_allo_spread
create global temporary table pwrplant.pt_temp_allo_spread
   (  property_tax_type_id    number(22,0),
      sequence                number(22,0),
      property_tax_ledger_id     number(22,0),
      amount                     number(22,2),
      reserve_amount             number(22,2)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_allo_spread_pk on pwrplant.pt_temp_allo_spread ( property_tax_type_id, sequence, property_tax_ledger_id );



-- pt_temp_bills_recalc_terms
create global temporary table pwrplant.pt_temp_bills_recalc_terms
   (  statement_id                  number(22,0),
      total_tax                        number(22,2),
      total_credit                     number(22,2),
      total_penalty                 number(22,2),
      total_interest                number(22,2),
      total_protest                 number(22,2),
      first_install_tax                number(22,2),
      first_install_credit             number(22,2),
      first_install_penalty            number(22,2),
      first_install_interest           number(22,2),
      first_install_protest            number(22,2),
      paid_tax                      number(22,2),
      paid_credit                   number(22,2),
      paid_penalty                  number(22,2),
      paid_interest                 number(22,2),
      paid_protest                  number(22,2),
      unpaid_nonreg_tax          number(22,2),
      unpaid_nonreg_credit       number(22,2),
      unpaid_nonreg_penalty         number(22,2),
      unpaid_nonreg_interest        number(22,2),
      unpaid_nonreg_protest         number(22,2),
      first_install_is_paid            number(22,0),
      earliest_unpaid_installment_id   number(22,0),
      latest_unpaid_installment_id  number(22,0),
      num_unpaid_reg_installs       number(22,0),
      tax_difference                number(22,2),
      credit_difference             number(22,2),
      penalty_difference            number(22,2),
      interest_difference           number(22,2),
      protest_difference            number(22,2),
      earliest_unpaid_reg_num    number(22,0),
      latest_unpaid_reg_num         number(22,0),
      earliest_unpaid_nonreg_id     number(22,0),
      latest_unpaid_nonreg_id       number(22,0)
   ) on commit delete rows;

create unique index pwrplant.pt_temp_bills_recalc_terms_pk on pwrplant.pt_temp_bills_recalc_terms ( statement_id );



-- pt_temp_casecalc_rounding
create global temporary table pwrplant.pt_temp_casecalc_rounding
   (  parcel_id                  number(22,0),
      assessment_group_id     number(22,0),
      max_assessment       number(22,2),
      assessment_diff         number(22,2),
      equalized_value_diff    number(22,2),
      taxable_value_diff      number(22,2)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_casecalc_rounding_pk on pwrplant.pt_temp_casecalc_rounding ( parcel_id, assessment_group_id );



-- pt_temp_casecalc_totals
create global temporary table pwrplant.pt_temp_casecalc_totals
   (  parcel_id                  number(22,0),
      assessment_group_id     number(22,0),
      assessment              number(22,2),
      equalized_value         number(22,2),
      taxable_value           number(22,2),
      prop_tax_balance        number(22,2),
      market_value            number(22,2),
      quantity_factored       number(22,8),
      num_rows             number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_casecalc_totals_pk on pwrplant.pt_temp_casecalc_totals ( parcel_id, assessment_group_id );



-- pt_temp_cprpull_basis_adjs
create global temporary table pwrplant.pt_temp_cprpull_basis_adjs
   (  preallo_ledger_id          number(22,0),
      state_id                   char(18),
      company_id                 number(22,0),
      prop_tax_company_id        number(22,0),
      gl_account_id              number(22,0),
      bus_segment_id          number(22,0),
      utility_account_id            number(22,0),
      property_tax_type_id       number(22,0),
      prop_tax_location_id       number(22,0),
      parcel_id                     number(22,0),
      depr_group_id              number(22,0),
      vintage                    number(22,0),
      vintage_month              number(22,0),
      class_code_value1          varchar2(254),
      class_code_value2          varchar2(254),
      ledger_detail_id           number(22,0),
      tax_year                   number(22,0),
      beginning_book_balance     number(22,2),
      book_additions             number(22,2),
      book_retirements           number(22,2),
      book_transfers_adj         number(22,2),
      quantity                   number(22,8),
      property_tax_adjust_id1    number(22,2),
      beg_bal_adjustment1        number(22,2),
      additions_adjustment1      number(22,2),
      retirements_adjustment1 number(22,2),
      transfers_adjustment1      number(22,2),
      property_tax_adjust_id2    number(22,2),
      beg_bal_adjustment2        number(22,2),
      additions_adjustment2      number(22,2),
      retirements_adjustment2 number(22,2),
      transfers_adjustment2      number(22,2),
      property_tax_adjust_id3    number(22,2),
      beg_bal_adjustment3        number(22,2),
      additions_adjustment3      number(22,2),
      retirements_adjustment3 number(22,2),
      transfers_adjustment3      number(22,2),
      property_tax_adjust_id4    number(22,2),
      beg_bal_adjustment4        number(22,2),
      additions_adjustment4      number(22,2),
      retirements_adjustment4 number(22,2),
      transfers_adjustment4      number(22,2),
      property_tax_adjust_id5    number(22,2),
      beg_bal_adjustment5        number(22,2),
      additions_adjustment5      number(22,2),
      retirements_adjustment5 number(22,2),
      transfers_adjustment5      number(22,2),
      property_tax_adjust_id6    number(22,2),
      beg_bal_adjustment6        number(22,2),
      additions_adjustment6      number(22,2),
      retirements_adjustment6 number(22,2),
      transfers_adjustment6      number(22,2),
      property_tax_adjust_id7    number(22,2),
      beg_bal_adjustment7        number(22,2),
      additions_adjustment7      number(22,2),
      retirements_adjustment7 number(22,2),
      transfers_adjustment7      number(22,2),
      property_tax_adjust_id8    number(22,2),
      beg_bal_adjustment8        number(22,2),
      additions_adjustment8      number(22,2),
      retirements_adjustment8 number(22,2),
      transfers_adjustment8      number(22,2),
      property_tax_adjust_id9    number(22,2),
      beg_bal_adjustment9        number(22,2),
      additions_adjustment9      number(22,2),
      retirements_adjustment9 number(22,2),
      transfers_adjustment9      number(22,2),
      property_tax_adjust_id10   number(22,2),
      beg_bal_adjustment10    number(22,2),
      additions_adjustment10     number(22,2),
      retirements_adjustment10   number(22,2),
      transfers_adjustment10     number(22,2)
   ) on commit preserve rows;

create index pwrplant.pt_temp_cprpull_basis_ptt_ndx on pwrplant.pt_temp_cprpull_basis_adjs ( property_tax_type_id );



-- pt_temp_cprpull_deprmonth
create global temporary table pwrplant.pt_temp_cprpull_deprmonth
   (  asset_id       number(22,0),
      depr_month     date
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_cprpull_deprmonth_pk on pwrplant.pt_temp_cprpull_deprmonth ( asset_id );



-- pt_temp_cprpull_ledgerdetail
create global temporary table pwrplant.pt_temp_cprpull_ledgerdetail
   (  asset_id                number(22,0),
      description_1           varchar2(254),
      description_2           varchar2(254),
      description_3           varchar2(254),
      unit_number          varchar2(50),
      serial_number           varchar2(50),
      make                    varchar2(50),
      model                   varchar2(50),
      model_year              varchar2(50),
      body_type               varchar2(50),
      fuel_type                  varchar2(50),
      gross_weight            number(22,2),
      license_number       varchar2(50),
      license_weight          number(22,2),
      license_expiration         date,
      key_number              varchar2(50),
      primary_driver          varchar2(50),
      group_code              varchar2(50),
      length                     varchar2(50),
      width                   varchar2(50),
      height                     varchar2(50),
      construction_type       varchar2(50),
      stories                 varchar2(50),
      lease_number            varchar2(50),
      lease_start             date,
      lease_end               date,
      source_system           varchar2(50),
      external_code           varchar2(50),
      notes                   varchar2(4000),
      ledger_detail_id        number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_cprpull_ledgerdetail on pwrplant.pt_temp_cprpull_ledgerdetail ( asset_id );



-- pt_temp_ids
create global temporary table pwrplant.pt_temp_ids
   (  temp_id     number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ids_pk on pwrplant.pt_temp_ids ( temp_id );



-- pt_temp_ids2
create global temporary table pwrplant.pt_temp_ids2
   (  temp_id     number(22,0),
      temp_id2    number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ids2_pk on pwrplant.pt_temp_ids2 ( temp_id, temp_id2 );



-- pt_temp_ids3
create global temporary table pwrplant.pt_temp_ids3
   (  temp_id     number(22,0),
      temp_id2    number(22,0),
      temp_id3    number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ids3_pk on pwrplant.pt_temp_ids3 ( temp_id, temp_id2, temp_id3 );



-- pt_temp_ids4
create global temporary table pwrplant.pt_temp_ids4
   (  temp_id     number(22,0),
      temp_id2    number(22,0),
      temp_id3    number(22,0),
      temp_id4    number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ids4_pk on pwrplant.pt_temp_ids4 ( temp_id, temp_id2, temp_id3, temp_id4 );


-- pt_temp_ledger_adds
create global temporary table pwrplant.pt_temp_ledger_adds
   (  property_tax_ledger_id        number(22,0) not null,
      tax_year                      number(22,0) not null,
      beginning_book_balance        number(22,2) default 0 not null,
      book_additions                number(22,2) default 0 not null,
      book_retirements              number(22,2) default 0 not null,
      book_transfers_adj            number(22,2) default 0 not null,
      ending_book_balance           number(22,2) default 0 not null,
      cwip_balance                  number(22,2) default 0 not null,
      allocated_tax_basis           number(22,2) default 0 not null,
      allocated_accum_ore           number(22,2) default 0 not null,
      unit_cost_amount              number(22,2) default 0 not null,
      prop_tax_balance              number(22,8) default 0 not null,
      associated_reserve            number(22,8) default 0 not null,
      calculated_reserve               number(22,8) default 0 not null,
      allocated_tax_reserve            number(22,8) default 0 not null,
      final_reserve                 number(22,8) default 0 not null,
      escalated_prop_tax_balance number(22,8) default 0 not null,
      escalated_prop_tax_reserve    number(22,8) default 0 not null,
      cost_approach_value           number(22,8) default 0 not null,
      market_value                  number(22,8) default 0 not null,
      quantity                      number(22,8),
      quantity_factored             number(22,8)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ledger_adds_pk on pwrplant.pt_temp_ledger_adds ( property_tax_ledger_id, tax_year );


-- pt_temp_ledger_adjs
create global temporary table pwrplant.pt_temp_ledger_adjs
   (  property_tax_ledger_id     number(22,0) not null,
      property_tax_adjust_id     number(22,0) not null,
      user_input                 number(22,0) not null,
      tax_year                   number(22,0) not null,
      beg_bal_adjustment         number(22,2) default 0 not null,
      additions_adjustment       number(22,2) default 0 not null,
      retirements_adjustment     number(22,2) default 0 not null,
      transfers_adjustment       number(22,2) default 0 not null,
      end_bal_adjustment         number(22,2) default 0 not null,
      cwip_adjustment            number(22,2) default 0 not null,
      tax_basis_adjustment       number(22,2) default 0 not null,
      reserve_adjustment         number(22,2) default 0 not null,
      notes                      varchar2(4000)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ledger_adjs_pk on pwrplant.pt_temp_ledger_adjs ( property_tax_ledger_id );


-- pt_temp_ledger_adjustment
create global temporary table pwrplant.pt_temp_ledger_adjustment
(     property_tax_ledger_id     number(22,0)   not null,
      property_tax_adjust_id     number(22,0)   not null,
      user_input                 number(22,0)   not null,
      tax_year                   number(22,0)   not null,
      from_tax_type_id           number(22,0)   not null,
      beg_bal_adjustment         number(22,2)   default 0,
      additions_adjustment       number(22,2)   default 0,
      retirements_adjustment     number(22,2)   default 0,
      transfers_adjustment       number(22,2)   default 0,
      end_bal_adjustment         number(22,2)   default 0,
      cwip_adjustment            number(22,2)   default 0,
      tax_basis_adjustment       number(22,2)   default 0,
      reserve_adjustment         number(22,2)   default 0
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ledger_adjustment on pwrplant.pt_temp_ledger_adjustment ( property_tax_ledger_id, property_tax_adjust_id, user_input, tax_year, from_tax_type_id );


-- pt_temp_ledger_tax_year
create global temporary table pwrplant.pt_temp_ledger_tax_year
(     property_tax_ledger_id           number(22,0)   not null,
      tax_year                         number(22,0)   not null,
      from_tax_type_id                 number(22,0)   not null,
      beginning_book_balance           number(22,2)   default 0,
      book_additions                   number(22,2)   default 0,
      book_retirements                 number(22,2)   default 0,
      book_transfers_adj               number(22,2)   default 0,
      ending_book_balance              number(22,2)   default 0,
      cwip_balance                     number(22,2)   default 0,
      beg_bal_adjustment               number(22,2)   default 0,
      additions_adjustment             number(22,2)   default 0,
      retirements_adjustment           number(22,2)   default 0,
      transfers_adjustment             number(22,2)   default 0,
      end_bal_adjustment               number(22,2)   default 0,
      cwip_adjustment                  number(22,2)   default 0,
      allocated_tax_basis              number(22,2)   default 0,
      allocated_accum_ore              number(22,2)   default 0,
      allocated_tax_reserve               number(22,2)   default 0,
      tax_basis_adjustment             number(22,2)   default 0,
      prop_tax_balance                 number(22,2)   default 0,
      associated_reserve               number(22,2)   default 0,
      calculated_reserve                  number(22,2)   default 0,
      reserve_adjustment               number(22,2)   default 0,
      final_reserve                    number(22,2)   default 0,
      escalated_prop_tax_balance    number(22,2)   default 0,
      escalated_prop_tax_reserve       number(22,2)   default 0,
      cost_approach_value              number(22,2)   default 0,
      market_value                     number(22,2)   default 0,
      quantity                         number(22,8),
      allocation_statistic                number(22,8)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ledger_tax_year on pwrplant.pt_temp_ledger_tax_year ( property_tax_ledger_id, tax_year, from_tax_type_id );


-- pt_temp_ledger_transfers
create global temporary table pwrplant.pt_temp_ledger_transfers
   (  from_ledger_id             number(22,0) not null,
      to_ledger_id                  number(22,0),
      tax_year                   number(22,0) not null,
      beg_bal_amount          number(22,2) default 0 not null,
      additions_amount           number(22,2) default 0 not null,
      retirements_amount         number(22,2) default 0 not null,
      transfers_amount           number(22,2) default 0 not null,
      end_bal_amount          number(22,2) default 0 not null,
      cwip_amount             number(22,2) default 0 not null,
      tax_basis_amount           number(22,2) default 0 not null,
      reserve_amount          number(22,2) default 0 not null,
      from_reserve_amount     number(22,2) default 0 not null,
      beg_bal_pct                number(22,8)   default 0,
      additions_pct              number(22,8)   default 0,
      retirements_pct            number(22,8)   default 0,
      transfers_pct              number(22,8)   default 0,
      end_bal_pct                number(22,8)   default 0,
      cwip_pct                   number(22,8)   default 0,
      tax_basis_pct              number(22,8)   default 0,
      reserve_pct                   number(22,8)   default 0,
      notes                      varchar2(4000),
      state_id                   char(18) not null,
      prop_tax_location_id       number(22,0),
      parcel_id                     number(22,0) not null,
      prop_tax_company_id        number(22,0) not null,
      company_id                 number(22,0) not null,
      gl_account_id              number(22,0) not null,
      bus_segment_id          number(22,0) not null,
      utility_account_id            number(22,0),
      property_tax_type_id       number(22,0) not null,
      vintage                    number(22,0),
      vintage_month              number(22,0),
      class_code_value1          varchar2(254),
      class_code_value2          varchar2(254),
      user_input                 number(22,0) not null,
      ledger_detail_id           number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_ledger_transfers_pk on pwrplant.pt_temp_ledger_transfers ( from_ledger_id );



-- pt_temp_preallo_transfers
create global temporary table pwrplant.pt_temp_preallo_transfers
   (  from_preallo_id            number(22,0) not null,
      to_preallo_id              number(22,0),
      tax_year                   number(22,0) not null,
      beg_bal_amount          number(22,2) default 0 not null,
      additions_amount           number(22,2) default 0 not null,
      retirements_amount         number(22,2) default 0 not null,
      transfers_amount           number(22,2) default 0 not null,
      end_bal_amount          number(22,2) default 0 not null,
      cwip_amount             number(22,2) default 0 not null,
      beg_bal_pct                number(22,8)   default 0,
      additions_pct              number(22,8)   default 0,
      retirements_pct            number(22,8)   default 0,
      transfers_pct              number(22,8)   default 0,
      end_bal_pct                number(22,8)   default 0,
      cwip_pct                   number(22,8)   default 0,
      notes                      varchar2(4000),
      state_id                   char(18) not null,
      prop_tax_location_id       number(22,0),
      parcel_id                     number(22,0) not null,
      prop_tax_company_id        number(22,0) not null,
      company_id                 number(22,0) not null,
      gl_account_id              number(22,0) not null,
      bus_segment_id          number(22,0) not null,
      utility_account_id            number(22,0),
      property_tax_type_id       number(22,0) not null,
      depr_group_id              number(22,0),
      vintage                    number(22,0),
      vintage_month              number(22,0),
      class_code_value1          varchar2(254),
      class_code_value2          varchar2(254),
      user_input                 number(22,0) not null,
      ledger_detail_id           number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_preallo_transfers_pk on pwrplant.pt_temp_preallo_transfers ( from_preallo_id );



-- pt_temp_parcels
create global temporary table pwrplant.pt_temp_parcels
   (  parcel_id         number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_parcels_pk on pwrplant.pt_temp_parcels ( parcel_id );



-- pt_temp_parcels_asmt_gp
create global temporary table pwrplant.pt_temp_parcels_asmt_gp
   (  parcel_id                  number(22,0),
      assessment_group_id     number(22,0)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_parcels_asmt_gp_pk on pwrplant.pt_temp_parcels_asmt_gp ( parcel_id, assessment_group_id );



-- pt_temp_actuals_load_pcts
create global temporary table pwrplant.pt_temp_actuals_load_pcts
   (  statement_id               number(22,0),
      statement_year_id       number(22,0),
      line_id                    number(22,0),
      property_tax_ledger_id     number(22,0),
      tax_authority_id           number(22,0),
      accrual_type_id            number(22,0),
      payment_id                 number(22,0),
      assessment_year_id         number(22,0),
      tax_year                   number(22,0),
      case_id                    number(22,0),
      parcel_id                     number(22,0),
      auth_dist_pct              number(33,22),
      accrual_type_pct           number(33,22),
      prop_tax_balance           number(22,2),
      final_reserve              number(22,2),
      cost_approach_value        number(22,2),
      market_value               number(22,2),
      quantity                   number(22,8),
      quantity_factored          number(22,8),
      assessment                 number(22,2),
      equalized_value            number(22,2),
      taxable_value              number(22,2)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_actuals_load_pcts_pk on pwrplant.pt_temp_actuals_load_pcts ( statement_id, statement_year_id, line_id, property_tax_ledger_id, tax_authority_id, accrual_type_id );



-- pt_temp_actuals_asmts
create global temporary table pwrplant.pt_temp_actuals_asmts
   (  statement_id               number(22,0),
      statement_year_id       number(22,0),
      line_id                    number(22,0),
      property_tax_ledger_id     number(22,0),
      tax_authority_id           number(22,0),
      accrual_type_id            number(22,0),
      assessment                 number(22,2),
      equalized_value            number(22,2),
      taxable_value              number(22,2)
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_actuals_asmts_pk on pwrplant.pt_temp_actuals_asmts ( statement_id, statement_year_id, line_id, property_tax_ledger_id, tax_authority_id, accrual_type_id );



-- pt_temp_statement_lines
create global temporary table pwrplant.pt_temp_statement_lines
   (  statement_id               number(22,0) not null,
      statement_year_id          number(22,0) not null,
      line_id                    number(22,0) not null,
      assessment                 number(22,2),
      taxable_value              number(22,2),
      tax_amount                 number(22,2),
      credit_amount              number(22,2),
      penalty_amount          number(22,2),
      interest_amount            number(22,2),
      calculated_amount          number(22,2) default 0 not null,
      billed_taxable_value       number(22,2) default 0 not null,
      parcel_taxable_value       number(22,2) default 0 not null,
      billed_assessment          number(22,2) default 0 not null,
      parcel_assessment       number(22,2) default 0 not null,
      total_calculated_amount    number(22,2) default 0 not null,
      total_assessment           number(22,2) default 0 not null,
      total_taxable_value        number(22,2) default 0 not null
   ) on commit preserve rows;

create unique index pwrplant.pt_temp_statement_lines_pk on pwrplant.pt_temp_statement_lines ( statement_id, statement_year_id, line_id );



----------------------------------------------------
-- CREATE VIEWS
----------------------------------------------------

-- ptv_co_view
create or replace view PTV_CO_VIEW as
   select      co_st.company_id company_id,
            co_st.state_id state_id,
            nvl( pt_company_override.prop_tax_company_id, co_st.prop_tax_company_id ) prop_tax_company_id
   from     pt_company_override,
            (  select      company.company_id company_id,
                        state.state_id state_id,
                        company.prop_tax_company_id prop_tax_company_id
               from     company,
                        state
            ) co_st
   where co_st.company_id = pt_company_override.company_id (+)
      and   co_st.state_id = pt_company_override.state_id (+);


-- ptv_payment_term
create or replace view PTV_PAYMENT_TERM as
select      pt_schedule_period.schedule_id schedule_id,
            pt_schedule_period.statement_year_id statement_year_id,
            pt_schedule_period.installment_id installment_id,
            pt_schedule_period.period_id period_id,
            decode( pt_schedule_installment.installment_type_id,
                        1, to_char( pt_schedule_installment.installment_number ) || ' - ' || pt_period_type.description,
                        pt_installment_type.description
                     ) description,
            pt_schedule_installment.installment_type_id,
            pt_schedule_period.period_type_id,
            pt_schedule_period.start_date start_date,
            pt_schedule_period.end_date end_date
   from     pt_schedule_installment,
            pt_installment_type,
            pt_schedule_period,
            pt_period_type
   where pt_schedule_installment.installment_type_id = pt_installment_type.installment_type_id
      and   pt_schedule_installment.schedule_id = pt_schedule_period.schedule_id
      and   pt_schedule_installment.statement_year_id = pt_schedule_period.statement_year_id
      and   pt_schedule_installment.installment_id = pt_schedule_period.installment_id
      and   pt_schedule_period.period_type_id = pt_period_type.period_type_id;

-- ptv_final_cases
create or replace view PTV_FINAL_CASES as
select      pt_assessment_year.assessment_year_id assessment_year_id,
            tax_year_case_view.tax_year tax_year,
            tax_year_case_view.case_id case_id
   from     pt_assessment_year,
            (  select      property_tax_year.assessment_year_id assessment_year_id,
                        property_tax_year.tax_year tax_year,
                        pt_case.case_id
               from     property_tax_year,
                        pt_case
               where property_tax_year.tax_year = pt_case.tax_year
                  and   pt_case.case_type_id = 1
            ) tax_year_case_view
   where pt_assessment_year.assessment_year_id = tax_year_case_view.assessment_year_id (+);

-- ptv_year_view
create or replace view PTV_YEAR_VIEW as
select      property_tax_year.tax_year tax_year,
            all_data.prop_tax_company_id prop_tax_company_id,
            all_data.state_id state_id,
            nvl( pt_year_override.add_activity_year, 0 ) + property_tax_year.year year,
            nvl( pt_year_override.add_valuation_year, 0 ) + property_tax_year.valuation_year valuation_year
   from     pt_year_override,
            property_tax_year,
            (  select      pt_company.prop_tax_company_id prop_tax_company_id,
                        state.state_id state_id
               from     pt_company,
                        state
            ) all_data
   where all_data.prop_tax_company_id = pt_year_override.prop_tax_company_id (+)
   and      all_data.state_id = pt_year_override.state_id (+);


----------------------------------------------------
-- CREATE ALL PROPERTY TAX SEQUENCES  - ADJUST THE "START WITH" VALUE AS NECESSARY
----------------------------------------------------

-- pt_preallo_seq
create sequence pwrplant.pt_preallo_seq start with 100;

-- pt_ledger_seq
create sequence pwrplant.pt_ledger_seq start with 100;

-- payment sequence
create sequence pwrplant.pt_payment_seq start with 1;

-- bills sequence
create sequence pwrplant.pt_bills_seq start with 1;

-- parcel sequence
create sequence pwrplant.pt_parcel_seq start with 1;

-- ledger detail sequence
create sequence pwrplant.pt_ledger_detail_seq start with 1;

-- process occurrence sequence
create sequence pwrplant.pt_process_occurrence_seq start with 1;

-- sequence for property tax types, rollups, reserve factors, escalated value type
create sequence pwrplant.pt_tables_seq start with 100000;

----------------------------------------------------
-- FIX BASE TABLE CONSTRAINTS THAT WENT AWAY WHEN CASCADING DELETES
----------------------------------------------------

-- prop_tax_location
alter table pwrplant.prop_tax_location add ( constraint pt_loc_loc_rollup_fk foreign key ( location_rollup_id ) references pwrplant.pt_location_rollup );


-- prop tax district
alter table pwrplant.prop_tax_district add ( constraint prop_tax_dist_assessor_fk foreign key ( assessor_id ) references pwrplant.pt_assessor );


-- book_summary
alter table pwrplant.book_summary add ( constraint book_summary_pt_adjust_fk foreign key ( property_tax_adjust_id ) references pwrplant.property_tax_adjust );


--company_setup
alter table pwrplant.company_setup add ( constraint company_setup_pt_comp_fk foreign key ( prop_tax_company_id ) references pwrplant.pt_company );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (538, 0, 10, 4, 1, 0, 30778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030778_proptax_02_rebuild_objects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
