/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029297_cwip_1.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/20/2013 Joseph King    Point Release
||============================================================================
*/

update PP_INTEGRATION_COMPONENTS
   set ARGUMENT1_TITLE = 'Batch / Instance', ARGUMENT2_TITLE = null
 where UPPER(COMPONENT) = UPPER('OCR Process');


--update PP_INTEGRATION_COMPONENTS
--   set ARGUMENT1_TITLE = 'Work Order ID', ARGUMENT2_TITLE = null, ARGUMENT3_TITLE = null
-- where UPPER(COMPONENT) = UPPER('OCR Process Est');

insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE)
   select 'OCR Process', 'Batch / Instance'
     from DUAL
    where not exists
    (select 1 from PP_INTEGRATION_COMPONENTS where UPPER(COMPONENT) = UPPER('OCR Process'));

-- insert into PP_INTEGRATION_COMPONENTS
--    (COMPONENT, ARGUMENT1_TITLE)
--    select 'OCR Process Est', 'Work Order ID'
--      from DUAL
--     where not exists (select 1
--              from PP_INTEGRATION_COMPONENTS
--             where UPPER(COMPONENT) = UPPER('OCR Process Est'));

delete from PP_INTEGRATION_COMPONENTS where UPPER(COMPONENT) = UPPER('OCR Process Est');
delete from PP_INTEGRATION where UPPER(COMPONENT) = UPPER('OCR Process Est');


begin
   execute immediate ('create table WO_INTERFACE_OCR_STG
                       (
                        COMPANY_ID          number(22,0),
                        EXT_COMPANY         varchar2(35),
                        WORK_ORDER_ID       number(22,0),
                        WORK_ORDER_NUMBER   varchar2(35),
                        ASSET_ID            number(22,0),
                        BUS_SEGMENT_ID      number(22,0),
                        EXT_BUS_SEGMENT     varchar2(35),
                        UTILITY_ACCOUNT_ID  number(22,0),
                        EXT_UTILITY_ACCOUNT varchar2(35),
                        SUB_ACCOUNT_ID      number(22,0),
                        EXT_SUB_ACCOUNT     varchar2(35),
                        PROPERTY_GROUP_ID   number(22,0),
                        EXT_PROPERTY_GROUP  varchar2(35),
                        RETIREMENT_UNIT_ID  number(22,0),
                        EXT_RETIREMENT_UNIT varchar2(35),
                        ASSET_LOCATION_ID   number(22,0),
                        EXT_ASSET_LOCATION  varchar2(35),
                        GL_ACCOUNT_ID       number(22,0),
                        EXT_GL_ACCOUNT      varchar2(35),
                        SERIAL_NUMBER       varchar2(35),
                        VINTAGE             number(4,0),
                        QUANTITY            number(22,2) not null,
                        AMOUNT              number(22,2) not null,
                        UPLOAD_INDICATOR    varchar2(1) default ''N'',
                        VALIDATION_MESSAGE  varchar2(2000),
                        BATCH_ID            varchar2(60) not null,
                        ESTIMATE_ID         number(22,0),
                        ROW_ID              number(22,0) not null,
                        USER_ID             varchar2(18),
                        TIME_STAMP          date
                       )');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('Table WO_INTERFACE_OCR_STG already exists.');
      else
         raise;
      end if;
end;
/

-- Add table doc comments to WO_INTERFACE_OCR_STG
Comment on table WO_INTERFACE_OCR_STG 
	is 'This table is used to stage data for the Original Cost Retirements API.';

Comment on column WO_INTERFACE_OCR_STG.COMPANY_ID
	is 'System-assigned identifier of a particular company.';

Comment on column WO_INTERFACE_OCR_STG.EXT_COMPANY
	is 'The external system-assigned identifier of a particular company.';

Comment on column WO_INTERFACE_OCR_STG.WORK_ORDER_ID
	is 'The system-assigned identifier of a work order.';

Comment on column WO_INTERFACE_OCR_STG.WORK_ORDER_NUMBER
	is 'User assigned work order number.';

Comment on column WO_INTERFACE_OCR_STG.ASSET_ID
	is 'System-assigned identifier of a particular asset recorded on the CPR Ledger. Not used for mass retirements.';

Comment on column WO_INTERFACE_OCR_STG.BUS_SEGMENT_ID
	is 'System-assigned identifier of a particular business Segment.';

Comment on column WO_INTERFACE_OCR_STG.EXT_BUS_SEGMENT
	is 'The external system-assigned identifier of a particular business Segment.';

Comment on column WO_INTERFACE_OCR_STG.UTILITY_ACCOUNT_ID
	is 'System-assigned identifier of a particular utility account.';

Comment on column WO_INTERFACE_OCR_STG.EXT_UTILITY_ACCOUNT
	is 'The external system-assigned identifier of a particular utility account.';

Comment on column WO_INTERFACE_OCR_STG.SUB_ACCOUNT_ID
	is 'User-designated values to further detail a particular utility account.';

Comment on column WO_INTERFACE_OCR_STG.EXT_SUB_ACCOUNT
	is 'The external systems user-designated values to further detail a particular utility account.';

Comment on column WO_INTERFACE_OCR_STG.PROPERTY_GROUP_ID
	is 'System-assigned identifier of a particular property group for the associated asset.';

Comment on column WO_INTERFACE_OCR_STG.EXT_PROPERTY_GROUP
	is 'The external system-assigned identifier of a particular property group for the associated asset.';

Comment on column WO_INTERFACE_OCR_STG.RETIREMENT_UNIT_ID 
	is 'System-assigned identifier of a particular retirement unit.';

Comment on column WO_INTERFACE_OCR_STG.EXT_RETIREMENT_UNIT
	is 'The external system-assigned identifier of a particular retirement unit.';

Comment on column WO_INTERFACE_OCR_STG.ASSET_LOCATION_ID
	is 'System-assigned identifier of a particular detailed asset location.';

Comment on column WO_INTERFACE_OCR_STG.EXT_ASSET_LOCATION 
	is 'The external system-assigned identifier of a particular detailed asset location.';

Comment on column WO_INTERFACE_OCR_STG.GL_ACCOUNT_ID
	is 'System-assigned identifier of a particular GL Account.';

Comment on column WO_INTERFACE_OCR_STG.EXT_GL_ACCOUNT 
	is 'The external system-assigned identifier of a particular GL Account.';

Comment on column WO_INTERFACE_OCR_STG.SERIAL_NUMBER
	is 'Serial number of the asset.';

Comment on column WO_INTERFACE_OCR_STG.VINTAGE  
	is 'Initial year in service (Engineering in service date). Null for Mass Ret. Require for specific Ret.';

Comment on column WO_INTERFACE_OCR_STG.QUANTITY 
	is 'Records the quantity associated with the retirement. Required to be non-zero for mass retirements.';

Comment on column WO_INTERFACE_OCR_STG.AMOUNT
	is 'Records the amount associated with the retirement.';

Comment on column WO_INTERFACE_OCR_STG.UPLOAD_INDICATOR
	is 'Indicates rows current status of upload: I In-Progress, E Error, D Done.';

Comment on column WO_INTERFACE_OCR_STG.VALIDATION_MESSAGE
	is 'Concatenates validations messages if it cannot derive accounting key fields.';

Comment on column WO_INTERFACE_OCR_STG.BATCH_ID 
	is 'System-assigned identifier of a particular batch.';

Comment on column WO_INTERFACE_OCR_STG.ESTIMATE_ID
	is 'System-assigned identifier of a particular wo_estimate record.';

Comment on column WO_INTERFACE_OCR_STG.ROW_ID 
	is 'Unique id number for each row.';

Comment on column WO_INTERFACE_OCR_STG.USER_ID  
	is 'Standard system-assigned user id used for audit purposes.';

Comment on column WO_INTERFACE_OCR_STG.TIME_STAMP
	is 'Standard system-assigned timestamp used for audit purposes.';

begin
   execute immediate ('create sequence WO_INTERFACE_OCR_STG_SEQ start with 1 increment by 1 nocache');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('sequence WO_INTERFACE_OCR_STG_SEQ already exists.');
      else
         raise;
      end if;
end;
/


create or replace trigger WO_INTERFACE_OCR_STG_PK
   before insert on WO_INTERFACE_OCR_STG
   for each row
declare
   L_NEW_SEQUENCE number;
begin
   select WO_INTERFACE_OCR_STG_SEQ.NEXTVAL into L_NEW_SEQUENCE from DUAL;
   :NEW.ROW_ID := L_NEW_SEQUENCE;
end;
/


begin
   execute immediate ('alter table WO_INTERFACE_OCR_STG
                          add constraint WO_INTERFACE_OCR_STG_PK
                              primary key (ROW_ID)
                              using index tablespace PWRPLANT_IDX');
exception
   when others then
      if (sqlcode = -2260) then
         DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
      else
         raise;
      end if;
end;
/


begin
   execute immediate ('create table WO_INTERFACE_OCR_RESULTS
                       (
                        ROW_ID          number(22,0) not null,
                        UNIT_ITEM_ID    number(22,0),
                        WO_ESTIMATE_ID  number(22,0),
                        CHARGE_GROUP_ID number(22,0),
                        CHARGE_ID       number(22,0),
                        USER_ID         varchar2(18),
                        TIME_STAMP      date
                       )');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('Table already exists.');
      else
         raise;
      end if;
end;
/

-- Add table doc comments to WO_INTERFACE_OCR_RESULTS

Comment on table WO_INTERFACE_OCR_RESULTS
	is 'This table only contains the records generated by this API.';

Comment on column WO_INTERFACE_OCR_RESULTS.ROW_ID   
	is 'Unique id number for each row.';

Comment on column WO_INTERFACE_OCR_RESULTS.UNIT_ITEM_ID 
	is 'Set by interface – the retirement unit.';

Comment on column WO_INTERFACE_OCR_RESULTS.WO_ESTIMATE_ID 
	is 'Set by interface – th system-assigned identifier of a particular wo_estimate record. Not populated if provided by external system.';

Comment on column WO_INTERFACE_OCR_RESULTS.CHARGE_GROUP_ID 
	is 'Set by interface – Charge group in unitization.';

Comment on column WO_INTERFACE_OCR_RESULTS.CHARGE_ID
	is 'Set by interface – CWIP charge id.';

Comment on column WO_INTERFACE_OCR_RESULTS.USER_ID  
	is 'Standard system-assigned user id used for audit purposes.';

Comment on column WO_INTERFACE_OCR_RESULTS.TIME_STAMP
	is 'Standard system-assigned timestamp used for audit purposes.';

begin
   execute immediate ('create table WO_INTERFACE_OCR_ARC
                          as select A.*, sysdate ARCHIVE_TIME_STAMP from WO_INTERFACE_OCR_STG A');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('Table already exists.');
      else
         raise;
      end if;
end;
/

-- Adds table doc comments to WO_INTERFACE_OCR_ARC
Comment on table WO_INTERFACE_OCR_ARC
	is 'This table is used to archive the staging data for the Original Cost Retirements API from the WO_INTERFACE_OCR_STG table.';

Comment on column WO_INTERFACE_OCR_ARC.ARCHIVE_TIME_STAMP
	is 'The time at which this row was added to the archive table.';

Comment on column WO_INTERFACE_OCR_ARC.COMPANY_ID
	is 'System-assigned identifier of a particular company.';

Comment on column WO_INTERFACE_OCR_ARC.EXT_COMPANY
	is 'The external system-assigned identifier of a particular company.';

Comment on column WO_INTERFACE_OCR_ARC.WORK_ORDER_ID
	is 'The system-assigned identifier of a work order.';

Comment on column WO_INTERFACE_OCR_ARC.WORK_ORDER_NUMBER
	is 'User assigned work order number.';

Comment on column WO_INTERFACE_OCR_ARC.ASSET_ID
	is 'System-assigned identifier of a particular asset recorded on the CPR Ledger. Not used for mass retirements.';

Comment on column WO_INTERFACE_OCR_ARC.BUS_SEGMENT_ID
	is 'System-assigned identifier of a particular business Segment.';

Comment on column WO_INTERFACE_OCR_ARC.EXT_BUS_SEGMENT
	is 'The external system-assigned identifier of a particular business Segment.';

Comment on column WO_INTERFACE_OCR_ARC.UTILITY_ACCOUNT_ID
	is 'System-assigned identifier of a particular utility account.';

Comment on column WO_INTERFACE_OCR_ARC.EXT_UTILITY_ACCOUNT
	is 'The external system-assigned identifier of a particular utility account.';

Comment on column WO_INTERFACE_OCR_ARC.SUB_ACCOUNT_ID
	is 'User-designated values to further detail a particular utility account.';

Comment on column WO_INTERFACE_OCR_ARC.EXT_SUB_ACCOUNT
	is 'The external systems user-designated values to further detail a particular utility account.';

Comment on column WO_INTERFACE_OCR_ARC.PROPERTY_GROUP_ID
	is 'System-assigned identifier of a particular property group for the associated asset.';

Comment on column WO_INTERFACE_OCR_ARC.EXT_PROPERTY_GROUP
	is 'The external system-assigned identifier of a particular property group for the associated asset.';

Comment on column WO_INTERFACE_OCR_ARC.RETIREMENT_UNIT_ID 
	is 'System-assigned identifier of a particular retirement unit.';

Comment on column WO_INTERFACE_OCR_ARC.EXT_RETIREMENT_UNIT
	is 'The external system-assigned identifier of a particular retirement unit.';

Comment on column WO_INTERFACE_OCR_ARC.ASSET_LOCATION_ID
	is 'System-assigned identifier of a particular detailed asset location.';

Comment on column WO_INTERFACE_OCR_ARC.EXT_ASSET_LOCATION 
	is 'The external system-assigned identifier of a particular detailed asset location.';

Comment on column WO_INTERFACE_OCR_ARC.GL_ACCOUNT_ID
	is 'System-assigned identifier of a particular GL Account.';

Comment on column WO_INTERFACE_OCR_ARC.EXT_GL_ACCOUNT 
	is 'The external system-assigned identifier of a particular GL Account.';

Comment on column WO_INTERFACE_OCR_ARC.SERIAL_NUMBER
	is 'Serial number of the asset.';

Comment on column WO_INTERFACE_OCR_ARC.VINTAGE  
	is 'Initial year in service (Engineering in service date). Null for Mass Ret. Require for specific Ret.';

Comment on column WO_INTERFACE_OCR_ARC.QUANTITY 
	is 'Records the quantity associated with the retirement. Required to be non-zero for mass retirements.';

Comment on column WO_INTERFACE_OCR_ARC.AMOUNT
	is 'Records the amount associated with the retirement.';

Comment on column WO_INTERFACE_OCR_ARC.UPLOAD_INDICATOR
	is 'Indicates rows current status of upload: I In-Progress, E Error, D Done.';

Comment on column WO_INTERFACE_OCR_ARC.VALIDATION_MESSAGE
	is 'Concatenates validations messages if it cannot derive accounting key fields.';

Comment on column WO_INTERFACE_OCR_ARC.BATCH_ID 
	is 'System-assigned identifier of a particular batch.';

Comment on column WO_INTERFACE_OCR_ARC.ESTIMATE_ID
	is 'System-assigned identifier of a particular wo_estimate record.';

Comment on column WO_INTERFACE_OCR_ARC.ROW_ID 
	is 'Unique id number for each row.';

Comment on column WO_INTERFACE_OCR_ARC.USER_ID  
	is 'Standard system-assigned user id used for audit purposes.';

Comment on column WO_INTERFACE_OCR_ARC.TIME_STAMP
	is 'Standard system-assigned timestamp used for audit purposes.';

begin
   execute immediate ('alter table WO_INTERFACE_OCR_ARC
                          add constraint WO_INTERFACE_OCR_ARC_PK
                              primary key (ROW_ID)
                              using index tablespace PWRPLANT_IDX');
exception
   when others then
      if (sqlcode = -2260) then
         DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
      else
         raise;
      end if;
end;
/


begin
   execute immediate ('create index WO_INTERFACE_OCR_ARC_WO_ID
                          on WO_INTERFACE_OCR_ARC (WORK_ORDER_ID)
                             tablespace PWRPLANT_IDX');
exception
   when others then
      if (sqlcode = -955) then
         DBMS_OUTPUT.PUT_LINE('Index already exists.');
      else
         raise;
      end if;
end;
/


insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, USER_DISABLED,
    COMPANY_ID, CONTROL_TYPE)
   select *
     from (select max(CONTROL_ID) + 1 CONTROL_ID,
                  'Maximum Vintage Search Window' CONTROL_NAME,
                  2 CONTROL_VALUE,
                  null DESCRIPTION,
                  'The maximum number of years up and down from the selected vintage to search when looking for an asset in the OCR API.' LONG_DESCRIPTION,
                  null USER_DISABLED,
                  -1 COMPANY_ID,
                  null CONTROL_TYPE
             from PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PP_SYSTEM_CONTROL_COMPANY
            where UPPER(CONTROL_NAME) = 'MAXIMUM VINTAGE SEARCH WINDOW');


-- View CPR_LEDGER_SPEC_AVAIL_TO_RET_V
create or replace view CPR_LEDGER_SPEC_AVAIL_TO_RET_V as
select A.ASSET_ID,
       A.PROPERTY_GROUP_ID,
       A.DEPR_GROUP_ID,
       A.BOOKS_SCHEMA_ID,
       A.RETIREMENT_UNIT_ID,
       A.BUS_SEGMENT_ID,
       A.COMPANY_ID,
       A.FUNC_CLASS_ID,
       A.UTILITY_ACCOUNT_ID,
       A.GL_ACCOUNT_ID,
       A.ASSET_LOCATION_ID,
       A.SUB_ACCOUNT_ID,
       A.WORK_ORDER_NUMBER,
       A.LEDGER_STATUS,
       A.IN_SERVICE_YEAR,
       sum(A.ACCUM_QUANTITY) ACCUM_QUANTITY,
       sum(A.ACCUM_COST) ACCUM_COST,
       A.SUBLEDGER_INDICATOR,
       A.DESCRIPTION,
       A.LONG_DESCRIPTION,
       A.ENG_IN_SERVICE_YEAR,
       A.SERIAL_NUMBER,
       A.ACCUM_COST_2,
       A.SECOND_FINANCIAL_COST,
       A.WIP_COMPUTATION_ID
  from (select A.ASSET_ID,
               A.PROPERTY_GROUP_ID,
               A.DEPR_GROUP_ID,
               A.BOOKS_SCHEMA_ID,
               A.RETIREMENT_UNIT_ID,
               A.BUS_SEGMENT_ID,
               A.COMPANY_ID,
               A.FUNC_CLASS_ID,
               A.UTILITY_ACCOUNT_ID,
               A.GL_ACCOUNT_ID,
               A.ASSET_LOCATION_ID,
               A.SUB_ACCOUNT_ID,
               A.WORK_ORDER_NUMBER,
               A.LEDGER_STATUS,
               A.IN_SERVICE_YEAR,
               A.ACCUM_QUANTITY,
               A.ACCUM_COST,
               A.SUBLEDGER_INDICATOR,
               A.DESCRIPTION,
               A.LONG_DESCRIPTION,
               A.ENG_IN_SERVICE_YEAR,
               A.SERIAL_NUMBER,
               A.ACCUM_COST_2,
               A.SECOND_FINANCIAL_COST,
               A.WIP_COMPUTATION_ID
          from CPR_LEDGER A
        union all
        select A.ASSET_ID,
               A.PROPERTY_GROUP_ID,
               A.DEPR_GROUP_ID,
               A.BOOKS_SCHEMA_ID,
               A.RETIREMENT_UNIT_ID,
               A.BUS_SEGMENT_ID,
               A.COMPANY_ID,
               A.FUNC_CLASS_ID,
               A.UTILITY_ACCOUNT_ID,
               A.GL_ACCOUNT_ID,
               A.ASSET_LOCATION_ID,
               A.SUB_ACCOUNT_ID,
               A.WORK_ORDER_NUMBER,
               A.LEDGER_STATUS,
               A.IN_SERVICE_YEAR,
               -1 * CGC.QUANTITY,
               -1 * CGC.AMOUNT,
               A.SUBLEDGER_INDICATOR,
               A.DESCRIPTION,
               A.LONG_DESCRIPTION,
               A.ENG_IN_SERVICE_YEAR,
               A.SERIAL_NUMBER,
               A.ACCUM_COST_2,
               A.SECOND_FINANCIAL_COST,
               A.WIP_COMPUTATION_ID
          from CHARGE_GROUP_CONTROL CGC, UNITIZED_WORK_ORDER UWO, CPR_LEDGER A, CHARGE_TYPE CT
         where NVL(CGC.PEND_TRANSACTION, 0) = 0
           and CGC.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
           and CT.PROCESSING_TYPE_ID = 1
           and CGC.EXPENDITURE_TYPE_ID = 2
           and UWO.UNIT_ITEM_ID = CGC.UNIT_ITEM_ID
           and UWO.WORK_ORDER_ID = CGC.WORK_ORDER_ID
           and A.ASSET_ID = UWO.LDG_ASSET_ID
        union all
        select A.ASSET_ID,
               A.PROPERTY_GROUP_ID,
               A.DEPR_GROUP_ID,
               A.BOOKS_SCHEMA_ID,
               A.RETIREMENT_UNIT_ID,
               A.BUS_SEGMENT_ID,
               A.COMPANY_ID,
               A.FUNC_CLASS_ID,
               A.UTILITY_ACCOUNT_ID,
               A.GL_ACCOUNT_ID,
               A.ASSET_LOCATION_ID,
               A.SUB_ACCOUNT_ID,
               A.WORK_ORDER_NUMBER,
               A.LEDGER_STATUS,
               A.IN_SERVICE_YEAR,
               B.POSTING_QUANTITY,
               B.POSTING_AMOUNT,
               A.SUBLEDGER_INDICATOR,
               A.DESCRIPTION,
               A.LONG_DESCRIPTION,
               A.ENG_IN_SERVICE_YEAR,
               A.SERIAL_NUMBER,
               A.ACCUM_COST_2,
               A.SECOND_FINANCIAL_COST,
               A.WIP_COMPUTATION_ID
          from CPR_LEDGER A, PEND_TRANSACTION B
         where A.ASSET_ID = B.LDG_ASSET_ID
           and FERC_ACTIVITY_CODE = 2
           and B.LDG_ASSET_ID is not null) A
 where RETIREMENT_UNIT_ID not between 1 and 5
   and LEDGER_STATUS < 100
 group by A.ASSET_ID,
          A.PROPERTY_GROUP_ID,
          A.DEPR_GROUP_ID,
          A.BOOKS_SCHEMA_ID,
          A.RETIREMENT_UNIT_ID,
          A.BUS_SEGMENT_ID,
          A.COMPANY_ID,
          A.FUNC_CLASS_ID,
          A.UTILITY_ACCOUNT_ID,
          A.GL_ACCOUNT_ID,
          A.ASSET_LOCATION_ID,
          A.SUB_ACCOUNT_ID,
          A.WORK_ORDER_NUMBER,
          A.LEDGER_STATUS,
          A.IN_SERVICE_YEAR,
          A.SUBLEDGER_INDICATOR,
          A.DESCRIPTION,
          A.LONG_DESCRIPTION,
          A.ENG_IN_SERVICE_YEAR,
          A.SERIAL_NUMBER,
          A.ACCUM_COST_2,
          A.SECOND_FINANCIAL_COST,
          A.WIP_COMPUTATION_ID;

-- View CPR_LEDGER_MASS_AVAIL_TO_RET_V
create or replace view CPR_LEDGER_MASS_AVAIL_TO_RET_V as
select A.PROPERTY_GROUP_ID,
       A.RETIREMENT_UNIT_ID,
       A.BUS_SEGMENT_ID,
       A.COMPANY_ID,
       A.UTILITY_ACCOUNT_ID,
       A.GL_ACCOUNT_ID,
       A.ASSET_LOCATION_ID,
       A.SUB_ACCOUNT_ID,
       sum(A.ACCUM_QUANTITY) ACCUM_QUANTITY
  from (select A.PROPERTY_GROUP_ID,
               A.RETIREMENT_UNIT_ID,
               A.BUS_SEGMENT_ID,
               A.COMPANY_ID,
               A.UTILITY_ACCOUNT_ID,
               A.GL_ACCOUNT_ID,
               A.ASSET_LOCATION_ID,
               A.SUB_ACCOUNT_ID,
               A.ACCUM_QUANTITY
          from CPR_LEDGER A
         where LEDGER_STATUS < 100
        union all
        select UWO.PROPERTY_GROUP_ID,
               UWO.RETIREMENT_UNIT_ID,
               UWO.BUS_SEGMENT_ID,
               UWO.COMPANY_ID,
               UWO.UTILITY_ACCOUNT_ID,
               UWO.GL_ACCOUNT_ID,
               UWO.ASSET_LOCATION_ID,
               UWO.SUB_ACCOUNT_ID,
               -1 * CGC.QUANTITY
          from CHARGE_GROUP_CONTROL CGC, UNITIZED_WORK_ORDER UWO, CHARGE_TYPE CT
         where NVL(CGC.PEND_TRANSACTION, 0) = 0
           and CGC.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
           and CT.PROCESSING_TYPE_ID = 1
           and CGC.EXPENDITURE_TYPE_ID = 2
           and UWO.UNIT_ITEM_ID = CGC.UNIT_ITEM_ID
           and UWO.WORK_ORDER_ID = CGC.WORK_ORDER_ID
        union all
        select B.PROPERTY_GROUP_ID,
               B.RETIREMENT_UNIT_ID,
               B.BUS_SEGMENT_ID,
               B.COMPANY_ID,
               B.UTILITY_ACCOUNT_ID,
               B.GL_ACCOUNT_ID,
               B.ASSET_LOCATION_ID,
               B.SUB_ACCOUNT_ID,
               B.POSTING_QUANTITY
          from PEND_TRANSACTION B
         where B.FERC_ACTIVITY_CODE = 2) A
 where RETIREMENT_UNIT_ID not between 1 and 5
 group by A.PROPERTY_GROUP_ID,
          A.RETIREMENT_UNIT_ID,
          A.BUS_SEGMENT_ID,
          A.COMPANY_ID,
          A.UTILITY_ACCOUNT_ID,
          A.GL_ACCOUNT_ID,
          A.ASSET_LOCATION_ID,
          A.SUB_ACCOUNT_ID;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (329, 0, 10, 4, 1, 0, 29297, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029297_cwip_1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
