/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045346_version-updates_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.1.3.0 9/22/2015  Luke Warren      Patch Release
|| 2015.1.4.0 1/21/2016  Anand R          Patch Release
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 2015.1.0.0',
       PP_PATCH   = 'Patch: 2015.1.4.0',
       POST_VERSION = 'Version 2015.1.e.0, Jan. 21, 2016',
       POWERTAX_VERSION = '2015.1.4.0',
       PROVISION_VERSION = '2015.1.4.0',
       PROP_TAX_VERSION = '2015.1.4.0',
       LEASE_VERSION = '2015.1.4.0',
       BUDGET_VERSION = '2015.1.4.0',
       DATE_INSTALLED = sysdate
where PP_VERSION_ID = 1;

update PP_PROCESSES
   set VERSION = '2015.1.4.0'
 where trim(LOWER(EXECUTABLE_FILE)) in ('automatedreview.exe',
                                        'batreporter.exe',
                                        'budget_allocations.exe',
                                        'cpm.exe',
                                        'cr_allocations.exe',
                                        'cr_balances.exe',
                                        'cr_balances_bdg.exe',
                                        'cr_batch_derivation.exe',
                                        'cr_batch_derivation_bdg.exe',
                                        'cr_budget_entry_calc_all.exe',
                                        'cr_build_combos.exe',
                                        'cr_build_deriver.exe',
                                        'cr_delete_budget_allocations.exe',
                                        'cr_financial_reports_build.exe',
                                        'cr_flatten_structures.exe',
                                        'cr_man_jrnl_reversals.exe',
                                        'cr_posting.exe',
                                        'cr_posting_bdg.exe',
                                        'cr_sap_trueup.exe', -- Next version cr_sap_trueup.exe will be renamed to cr_derivation_trueup.exe
                                        'cr_sum.exe',
                                        'cr_sum_ab.exe',
                                        'cr_to_commitments.exe',
                                        'cwip_charge.exe',
                                        'populate_matlrec.exe',
                                        'ppmetricbatch.exe',
                                        'pptocr.exe',
                                        'ppverify.exe',
                                        'pp_integration.exe',
                                        'ssp_aro_approve.exe',
                                        'ssp_aro_calc.exe',
                                        'ssp_budget.exe',
                                        'ssp_budget.exe afudc',
                                        'ssp_budget.exe cr',
                                        'ssp_budget.exe esc',
                                        'ssp_budget.exe load',
                                        'ssp_budget.exe oh',
                                        'ssp_budget.exe uwa',
                                        'ssp_budget.exe wip',
                                        'ssp_cpr_close_month.exe',
                                        'ssp_cpr_new_month.exe',
                                        'ssp_close_powerplant.exe',
                                        'ssp_cpr_balance_pp.exe',
                                        'ssp_depr_approval.exe',
                                        'ssp_depr_calc.exe',
                                        'ssp_depr_calc.exe fcst:grp',
                                        'ssp_depr_calc.exe fcst:incr',
                                        'ssp_depr_calc.exe fcst:ind',
                                        'ssp_depr_calc.exe group',
                                        'ssp_depr_calc.exe ind:-100',
                                        'ssp_depr_calc.exe ind:100',
                                        'ssp_depr_calc.exe ind:99',
                                        'ssp_gl_recon.exe',
                                        'ssp_release_je.exe',
                                        'ssp_release_je_wo.exe',
                                        'ssp_wo_appr_oh_afudc_wip.exe',
                                        'ssp_wo_approve_accruals.exe',
                                        'ssp_wo_auto_nonunitize.exe failed',
                                        'ssp_wo_auto_nonunitize.exe non',
                                        'ssp_wo_auto_unitization.exe',
                                        'ssp_wo_calc_oh_afudc_wip.exe',
                                        'ssp_wo_calculate_accruals.exe',
                                        'ssp_wo_close_charge_collection.exe',
                                        'ssp_wo_close_month.exe',
                                        'ssp_wo_gl_reconciliation.exe',
                                        'ssp_wo_new_month.exe',
                                        'ssp_wo_retirements.exe'
                                        );

update PP_CUSTOM_PBD_VERSIONS
set VERSION = '2015.1.4.0'
where trim(lower(PBD_NAME )) in (
    'exe_budget_allocations_custom.pbd', 
    'exe_budallo_custom_dw.pbd', 
    'exe_cr_allocations_custom.pbd',   
    'exe_crallo_custom_dw.pbd', 
    'cr_balances_custom.pbd', 
    'cr_balances_bdg_custom.pbd', 
    'cr_batch_derivation_custom.pbd', 
    'cr_batch_derivation_bdg_custom.pbd', 
    'cr_budget_entry_calc_all_custom.pbd',
    'cr_build_deriver_custom.pbd',
    'cr_delete_budget_allocations_custom.pbd',
    'cr_financial_reports_build_custom.pbd',     
    'cr_flatten_structures_custom.pbd', 
    'cr_man_jrnl_reversals_custom.pbd', 
    'cr_posting_custom.pbd',     
    'cr_posting_bdg_custom.pbd',
    'cr_sap_trueup_custom.pbd',     
    'cr_sum_custom.pbd',
    'cr_sum_ab_custom.pbd',
    'cr_to_commitments_custom.pbd', 
    'cwip_charge_custom.pbd',    
    'pp_integration_custom.pbd',     
    'pptocr_custom.pbd',
    'ssp_depr_approval_custom.pbd', 
    'ssp_release_je_custom.pbd', 
    'ssp_gl_recon_custom.pbd', 
    'ssp_close_powerplant_custom.pbd', 
    'ppdepr_interface_custom.pbd', 
    'ssp_wo_gl_reconciliation_custom.pbd', 
    'ppprojct_custom.pbd', 
    'ssp_preview_je_custom.pbd',
    'ssp_release_je_wo_custom.pbd', 
    'ssp_wo_calculate_accruals_custom.pbd', 
    'ssp_wo_close_month_custom.pbd',
    'ssp_report_gen_custom.pbd',
    'ppsystem_interface_custom.pbd' );    
                                        
update PP_MODULES
   set VERSION = '2015.1.4.0'
 where trim(LOWER(MODULE_ID)) in (1, -- Depreciation Studies
                          2, -- Cost Repository
                          3, -- Property Tax
                          4, -- Asset Management
                          5, -- Project Management
                          6, -- Budget
                          7, -- Property Unit
                          8, -- Administration
                          9, -- Depreciation
                          10, -- PowerTax
                          11, -- Tables
                          12, -- PowerTax Provision
                          13, -- OM Budget
                          14, -- Tax Repairs
                          15 -- Regulatory
                                  );


insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
          select '2015.1.4.0', 'automatedreview.exe',                '2015.1.4.0' from dual
union all select '2015.1.4.0', 'batreporter.exe',                    '2015.1.4.0' from dual
union all select '2015.1.4.0', 'budget_allocations.exe',             '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cpm.exe',                            '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_allocations.exe',                 '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_balances.exe',                    '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_balances_bdg.exe',                '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_batch_derivation.exe',            '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_batch_derivation_bdg.exe',        '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_budget_entry_calc_all.exe',       '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_build_combos.exe',                '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_build_deriver.exe',               '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_delete_budget_allocations.exe',   '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_financial_reports_build.exe',     '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_flatten_structures.exe',          '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_man_jrnl_reversals.exe',          '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_posting.exe',                     '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_posting_bdg.exe',                 '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_sap_trueup.exe',                  '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_sum.exe',                         '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_sum_ab.exe',                      '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cr_to_commitments.exe',              '2015.1.4.0' from dual
union all select '2015.1.4.0', 'cwip_charge.exe',                    '2015.1.4.0' from dual
union all select '2015.1.4.0', 'populate_matlrec.exe',               '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ppmetricbatch.exe',                  '2015.1.4.0' from dual
union all select '2015.1.4.0', 'pptocr.exe',                         '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ppverify.exe',                       '2015.1.4.0' from dual
union all select '2015.1.4.0', 'pp_integration.exe',                 '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_aro_approve.exe',                '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_aro_calc.exe',                   '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_budget.exe',                     '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_cpr_close_month.exe',            '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_cpr_new_month.exe',              '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_close_powerplant.exe',           '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_cpr_balance_pp.exe',             '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_depr_approval.exe',              '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_depr_calc.exe',                  '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_gl_recon.exe',                   '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_release_je.exe',                 '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_release_je_wo.exe',              '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_appr_oh_afudc_wip.exe',       '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_approve_accruals.exe',        '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_auto_nonunitize.exe',         '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_auto_unitization.exe',        '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_calc_oh_afudc_wip.exe',       '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_calculate_accruals.exe',      '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_close_charge_collection.exe', '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_close_month.exe',             '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_gl_reconciliation.exe',       '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_new_month.exe',               '2015.1.4.0' from dual
union all select '2015.1.4.0', 'ssp_wo_retirements.exe',             '2015.1.4.0' from dual
minus select PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION from PP_VERSION_EXES;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3031, 0, 2015, 1, 4, 0, 045346, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.4.0_maint_045346_version-updates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;