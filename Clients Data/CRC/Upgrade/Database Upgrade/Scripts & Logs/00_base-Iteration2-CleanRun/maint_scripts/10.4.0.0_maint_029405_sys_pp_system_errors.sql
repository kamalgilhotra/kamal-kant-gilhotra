/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029405_sys_pp_system_errors.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   03/06/2013 Charlie Shilling Point Release
||============================================================================
*/

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1023',
    'POST 1023: ERROR: There is not enough projected mortality to process the retirement transaction. Total projected mortality: ### Number to retire: ###. ',
    'POST 1023: ERROR: There is not enough projected mortality to process the retirement transaction. Total projected mortality: ### Number to retire: ###. ',
    'POST 1023: The transaction may need a specific retirement. ');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (315, 0, 10, 4, 0, 0, 29405, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029405_sys_pp_system_errors.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;