/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039268_reg_fcst_depr_inc_create.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/11/2014 Shane Ward
||============================================================================
*/

alter table REG_INCREMENTAL_ADJUSTMENT modify ORIG_BUDGET_VERSION_ID       number(22,0) null;
alter table REG_INCREMENTAL_ADJUSTMENT modify ORIG_VERSION_ID              number(22,0) null;
alter table REG_INCREMENTAL_ADJUSTMENT modify ORIG_SET_OF_BOOKS_ID         number(22,0) null;
alter table REG_INCREMENTAL_ADJUSTMENT modify ORIG_FCST_DEPR_VERSION_ID    number(22,0) null;
alter table REG_INCREMENTAL_ADJUSTMENT modify UPDATED_BUDGET_VERSION_ID    number(22,0) null;
alter table REG_INCREMENTAL_ADJUSTMENT modify UPDATED_VERSION_ID           number(22,0) null;
alter table REG_INCREMENTAL_ADJUSTMENT modify UPDATED_SET_OF_BOOKS_ID      number(22,0) null;
alter table REG_INCREMENTAL_ADJUSTMENT modify UPDATED_FCST_DEPR_VERSION_ID number(22,0) null;


create table REG_INCREMENTAL_DEPR_ADJ_TRANS
(
 FORECAST_VERSION_ID  number(22,0) not null,
 INCREMENTAL_ADJ_ID   number(22,0) not null,
 LABEL_ID             number(22,0) not null,
 ITEM_ID              number(22,0) not null,
 FCST_DEPR_VERSION_ID number(22,0) not null,
 SET_OF_BOOKS_ID      number(22,0) not null,
 VERSION_ID           number(22,0) null,
 REG_COMPANY_ID       number(22,0) null,
 REG_ACCT_ID          number(22,2) null,
 OVERRIDE_TRANSLATE   number(22,0) null,
 OVERRIDE_REG_ACCT_ID number(22,0) null,
 FCST_DEPR_GROUP_ID   number(22,0) not null,
 TAX_CLASS_ID         number(22,0) null,
 NORMALIZATION_ID     number(22,0) null,
 JURISDICTION_ID      number(22,0) null,
 USER_ID              varchar2(18) null,
 TIMESTAMP            date         null
);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint PK_REG_INCREMENTAL_DEPR_ADJ_TR
       primary key (FORECAST_VERSION_ID,
                    INCREMENTAL_ADJ_ID,
                    LABEL_ID,
                    ITEM_ID,
                    FCST_DEPR_VERSION_ID,
                    SET_OF_BOOKS_ID,
                    FCST_DEPR_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T1
       foreign key (forecast_version_id)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T2
       foreign key (INCREMENTAL_ADJ_ID)
       references REG_INCREMENTAL_ADJUSTMENT (INCREMENTAL_ADJ_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T3
       foreign key (LABEL_ID, ITEM_ID)
       references INCREMENTAL_FP_ADJ_ITEM (LABEL_ID, ITEM_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T4
       foreign key (FCST_DEPR_VERSION_ID)
       references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T5
       foreign key (SET_OF_BOOKS_ID)
       references SET_OF_BOOKS (SET_OF_BOOKS_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T6
       foreign key (VERSION_ID)
       references VERSION (VERSION_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T7
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T8
       foreign key (REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_ADJ_T9
       foreign key (OVERRIDE_REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_T_10
       foreign key (FCST_DEPR_GROUP_ID)
       references FCST_DEPR_GROUP (FCST_DEPR_GROUP_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_T_T11
       foreign key (TAX_CLASS_ID)
       references TAX_CLASS (TAX_CLASS_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_T12
       foreign key (NORMALIZATION_ID)
       references NORMALIZATION_SCHEMA (NORMALIZATION_ID);

alter table REG_INCREMENTAL_DEPR_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_DEPR_T13
       foreign key (JURISDICTION_ID)
       references JURISDICTION (JURISDICTION_ID);

comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.FORECAST_VERSION_ID IS 'System assigned identifier for regulatory forecast ledger';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.INCREMENTAL_ADJ_ID IS 'System assigned identifier for the incremental adjustment';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.LABEL_ID IS 'System assigned identifier for the incremental data label';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.ITEM_ID IS 'System assigned identifier for the incremental data item';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.FCST_DEPR_VERSION_ID IS 'System assigned identifier for the depreciation forecast version';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.VERSION_ID IS 'System assigned identifier for the PowerTax case';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.REG_COMPANY_ID IS 'System assigned identifier for the Regulatory Company';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.REG_ACCT_ID IS 'System assigned identifier for the Regulatory Account assigned by the system';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.OVERRIDE_TRANSLATE IS '1 = User selects account to override system translated account, 0 = use system translated account';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.OVERRIDE_REG_ACCT_ID IS 'System assigned identifier for the Regulatory Account selected by the user';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.FCST_DEPR_GROUP_ID IS 'System assigned identifier for the forecast depreciation group for labels/items that involve depreciation';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.SET_OF_BOOKS_ID IS 'System assigned identifier for the Set of Books for labels/items that involve depreciation';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.TAX_CLASS_ID IS 'System assigned identifier for the tax class for labels/items that involve deferred taxes';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.NORMALIZATION_ID IS 'System assigned identifier for the normalization schema for labels/items that involve deferred taxes';
comment on column REG_INCREMENTAL_DEPR_ADJ_TRANS.JURISDICTION_ID IS 'System assigned identifier for the jurisdiction for labels/items that involve deferred taxes';

alter table INCREMENTAL_DEPR_ADJ_DATA add COMPANY_ID number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1322, 0, 10, 4, 2, 7, 39268, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039268_reg_fcst_depr_inc_create.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;