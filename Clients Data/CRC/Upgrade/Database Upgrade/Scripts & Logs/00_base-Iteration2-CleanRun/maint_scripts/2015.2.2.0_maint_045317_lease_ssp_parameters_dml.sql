/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045317_lease_ssp_parameters_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 02/08/2016 Will Davis			 SSP Job server parameters
|| 2015.2.2.0 04/13/2016 David Haupt		 Backporting to 2015.2.2
||============================================================================
*/ 

insert into pp_job_executable
(JOB_EXECUTABLE_ID, JOB_TYPE, JOB_NAME, EXECUTABLE, CATEGORY, ARGUMENTS)
select
(select nvl(max(job_executable_id),0) from pp_job_executable) + rownum,
'SSP' as job_type,
a.job_name,
'ssp_lease_control.exe',
'Month End Processes',
a.params
from
	(select 'Lock Lease' as job_name,
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}, {"type":"string","value":"LAM Closed","label":"Process","required":"true"}]}'
  as params
	from dual union
	select 'Approve Lease Depr',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}, {"type":"string","value":"Approve Depr","label":"Process","required":"true"}]}'
	from dual union
	select 'Lease Auto Retirements',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}, {"type":"string","value":"Auto Retirements","label":"Process","required":"true"}]}'
	from dual union
	select 'Lease Calc Accruals',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}, {"type":"string","value":"Calc Accruals","label":"Process","required":"true"}]}'
	from dual union
	select 'Lease Approve Accruals',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}, {"type":"string","value":"Approve Accruals","label":"Process","required":"true"}]}'
	from dual union
	select 'Lease Calc Payments',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}, {"type":"string","value":"Calc Payments","label":"Process","required":"true"}]}'
	from dual union
	select 'Lease Approve Payments',
  '{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}, {"type":"string","value":"Approve Payments","label":"Process","required":"true"}]}'
	from dual union
  select 'Lease Open Next',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}, {"type":"string","value":"Open Next","label":"Process","required":"true"}]}'
	from dual ) a;




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3052, 0, 2015, 2, 2, 0, 045317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045317_lease_ssp_parameters_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;