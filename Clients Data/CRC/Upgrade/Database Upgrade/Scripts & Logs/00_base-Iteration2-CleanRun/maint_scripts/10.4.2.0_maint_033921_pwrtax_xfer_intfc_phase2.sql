/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033921_pwrtax_xfer_intfc_phase2.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 10.4.2.0 12/4/2013  Andrew Scott        changes to transfer interface needed
||                                         new tables for traceback logic, faster
||                                         load activity.
||============================================================================
*/

---------drop new tables, but don't error if they're not already there
begin
   execute immediate 'drop table TAX_XFER_TRACEBACK_STG';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_XFER_TRACEBACK_STG does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table TAX_XFER_TRACEBACK_FINAL';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_XFER_TRACEBACK_STG_FINAL does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table TAX_TEMP_BOOK_TRANSFERS_STG';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TEMP_BOOK_TRANSFERS_STG does not exist or drop table priv not granted.');
end;
/

----create the traceback staging table
create table TAX_XFER_TRACEBACK_STG
(
   TAX_YEAR             number(22,2),
   FROM_ASSET_ID        number(22,0),
   TO_ASSET_ID          number(22,0),
   GL_POSTING_MO_YR     date,
   TO_ASSET_ACTIVITY_ID number(22,0),
   FROM_TRANSFER_AMOUNT number(22,2),
   TO_TRANSFER_AMOUNT   number(22,2),
   MIN_FROM_STATUS      number(22,0),
   TIME_STAMP           date,
   USER_ID              varchar2(18)
);

alter table TAX_XFER_TRACEBACK_STG
   add constraint TAX_XFER_TRACEBACK_STGPK
       primary key (TAX_YEAR, FROM_ASSET_ID, TO_ASSET_ID, GL_POSTING_MO_YR, TO_ASSET_ACTIVITY_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_XFER_TRACEBACK_STG is
'(C) [09] The Tax Book Xfer Stg table holds the original book transfer transactions pulled in from PowerPlant for a given year. Used in the Load Activity step of the PowerTax Transfers interface.';
comment on column TAX_XFER_TRACEBACK_STG.TAX_YEAR is 'The PowerTax Tax Year.';
comment on column TAX_XFER_TRACEBACK_STG.FROM_ASSET_ID is 'The asset id on the "from" side of the transfer.';
comment on column TAX_XFER_TRACEBACK_STG.TO_ASSET_ID is 'The asset id on the "to" side of the transfer.';
comment on column TAX_XFER_TRACEBACK_STG.GL_POSTING_MO_YR is 'The posting month in which the transfer occurred.';
comment on column TAX_XFER_TRACEBACK_STG.TO_ASSET_ACTIVITY_ID is 'The asset activity id on the transfer "to".  This aids in the sorting of the records for processing.';
comment on column TAX_XFER_TRACEBACK_STG.FROM_TRANSFER_AMOUNT is 'The activity cost of the transfer "from".';
comment on column TAX_XFER_TRACEBACK_STG.TO_TRANSFER_AMOUNT is 'The activity cost of the transfer "to".';
comment on column TAX_XFER_TRACEBACK_STG.MIN_FROM_STATUS is 'The minimum activity status on the "from" transfer activity.  This aids in the sorting of the records for processing.';
comment on column TAX_XFER_TRACEBACK_STG.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column TAX_XFER_TRACEBACK_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';

----create the traceback final table
create table TAX_XFER_TRACEBACK_FINAL
(
   TAX_YEAR                   number(22,2),
   FROM_ASSET_ID              number(22,0),
   TO_ASSET_ID                number(22,0),
   GL_POSTING_MO_NUM          number(22,0),
   RUNNING_TO_TRANSFER_AMOUNT number(22,2),
   FIRST_PROCESSING_ORDER     number(22,0),
   LAST_PROCESSING_ORDER      number(22,0),
   TIME_STAMP                 date,
   USER_ID                    varchar2(18)
);

alter table TAX_XFER_TRACEBACK_FINAL
   add constraint TAX_XFER_TRACEBACK_FINAL_PK
       primary key (TAX_YEAR, FROM_ASSET_ID, TO_ASSET_ID)
       using index tablespace PWRPLANT_IDX;

create index TAX_XFER_TRACEBACK_FINAL_IDX1
   on TAX_XFER_TRACEBACK_FINAL (TO_ASSET_ID)
      tablespace PWRPLANT_IDX;

comment on table TAX_XFER_TRACEBACK_FINAL is
'(C) [09] The Tax Book Xfer Final table holds the book transfer history for a given year, after the transfers have been traced back and consolidated as much as possible. Used in the Load Activity step of the PowerTax Transfers interface.';
comment on column TAX_XFER_TRACEBACK_STG.TAX_YEAR is 'The PowerTax Tax Year.';
comment on column TAX_XFER_TRACEBACK_FINAL.FROM_ASSET_ID is 'The asset id on the "from" side of the transfer.';
comment on column TAX_XFER_TRACEBACK_FINAL.TO_ASSET_ID is 'The asset id on the "to" side of the transfer.';
comment on column TAX_XFER_TRACEBACK_FINAL.GL_POSTING_MO_NUM is 'The posting month number in which the transfer occurred.';
comment on column TAX_XFER_TRACEBACK_FINAL.RUNNING_TO_TRANSFER_AMOUNT is 'The traced back cost of the transfer "to".';
comment on column TAX_XFER_TRACEBACK_FINAL.FIRST_PROCESSING_ORDER is 'The processing order id when the row was first inserted into the table.  This aids in the event that multiple record sets need to be used in the trace back logic.';
comment on column TAX_XFER_TRACEBACK_FINAL.LAST_PROCESSING_ORDER is 'The most recent processing order id when the row was first inserted into the table.';
comment on column TAX_XFER_TRACEBACK_FINAL.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column TAX_XFER_TRACEBACK_FINAL.USER_ID is 'Standard system-assigned user id used for audit purposes.';


----create the temporary table used to populate tax book transfers
create global temporary table TAX_TEMP_BOOK_TRANSFERS_STG
(
	TRANSFER_GROUP            number(22,0),
	TAX_ACTIVITY_CODE_ID_FROM number(22,0),
	UTILITY_ACCOUNT_ID_FROM   number(22,0),
	SUB_ACCOUNT_ID_FROM       number(22,0),
	COMPANY_ID_FROM           number(22,0),
	BUS_SEGMENT_ID_FROM       number(22,0),
	IN_SERVICE_YEAR_FROM      date,
	GL_POSTING_MO_YR_FROM     date,
	TAX_LOCATION_ID_FROM      number(22,0),
	GL_ACCOUNT_ID_FROM        number(22,0),
	TAX_DISTINCTION_ID_FROM   number(22,0),
	CLASS_CODE_ID_FROM        number(22,0),
	CC_TAX_INDICATOR_FROM     number(22,0),
	CC_VALUE_FROM             varchar2(35),
	TAX_ACTIVITY_CODE_ID_TO   number(22,0),
	UTILITY_ACCOUNT_ID_TO     number(22,0),
	SUB_ACCOUNT_ID_TO         number(22,0),
	COMPANY_ID_TO             number(22,0),
	BUS_SEGMENT_ID_TO         number(22,0),
	IN_SERVICE_YEAR_TO        date,
	GL_POSTING_MO_YR_TO       date,
	TAX_LOCATION_ID_TO        number(22,0),
	GL_ACCOUNT_ID_TO          number(22,0),
	TAX_DISTINCTION_ID_TO     number(22,0),
	CLASS_CODE_ID_TO          number(22,0),
	CC_TAX_INDICATOR_TO       number(22,0),
	CC_VALUE_TO               varchar2(35),
	BOOK_SALE_AMOUNT          number(22,2),
	AMOUNT                    number(22,2)
) on commit preserve rows;

create unique index TAX_TEMP_BOOK_TRANSFERS_STG_PK
   on TAX_TEMP_BOOK_TRANSFERS_STG (TRANSFER_GROUP);

comment on table TAX_TEMP_BOOK_TRANSFERS_STG is
'(T) [09] The Tax Temp Book Transfers Stg temporary table holds the traced back transfer history, with the from and to transfer in separate rows, linked with a dumb transfer group key.  This is used as a step in the population of the tax book transfers table.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.TRANSFER_GROUP is 'System-assigned identifier that groups the transfer in with the transfer out.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.TAX_ACTIVITY_CODE_ID_FROM is 'System-assigned identifier of a tax activity code (e.g. add, retire, sale, etc.) of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.UTILITY_ACCOUNT_ID_FROM is 'System-assigned identifier of a utility account of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.SUB_ACCOUNT_ID_FROM is 'System-assigned identifier of a sub-account of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.COMPANY_ID_FROM is 'System-assigned identifier of a company of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.BUS_SEGMENT_ID_FROM is 'System-assigned identifier of a business segment of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.IN_SERVICE_YEAR_FROM is 'Book in-service year of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.GL_POSTING_MO_YR_FROM is 'Book posting month and year of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.TAX_LOCATION_ID_FROM is 'System-assigned identifier of a location being used to distinguish income tax classes on the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.GL_ACCOUNT_ID_FROM is 'System-assigned identifier of a particular general ledger account of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.TAX_DISTINCTION_ID_FROM is 'System-assigned distinction for a group of retirement units used in the book to tax translate for the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.CLASS_CODE_ID_FROM is 'System-assigned identifier of a classification code of the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.CC_TAX_INDICATOR_FROM is 'Yes/No Indicator designating whether or not the class code is applicable for tax for the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.CC_VALUE_FROM is 'Value of the classification code for these assets for the from side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.TAX_ACTIVITY_CODE_ID_FROM is 'System-assigned identifier of a tax activity code (e.g. add, retire, sale, etc.) of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.UTILITY_ACCOUNT_ID_FROM is 'System-assigned identifier of a utility account of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.SUB_ACCOUNT_ID_FROM is 'System-assigned identifier of a sub-account of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.COMPANY_ID_FROM is 'System-assigned identifier of a company of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.BUS_SEGMENT_ID_FROM is 'System-assigned identifier of a business segment of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.IN_SERVICE_YEAR_FROM is 'Book in-service year of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.GL_POSTING_MO_YR_FROM is 'Book posting month and year of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.TAX_LOCATION_ID_FROM is 'System-assigned identifier of a location being used to distinguish income tax classes on the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.GL_ACCOUNT_ID_FROM is 'System-assigned identifier of a particular general ledger account of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.TAX_DISTINCTION_ID_FROM is 'System-assigned distinction for a group of retirement units used in the book to tax translate for the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.CLASS_CODE_ID_FROM is 'System-assigned identifier of a classification code of the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.CC_TAX_INDICATOR_FROM is 'Yes/No Indicator designating whether or not the class code is applicable for tax for the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.CC_VALUE_FROM is 'Value of the classification code for these assets for the to side of the transfer.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.BOOK_SALE_AMOUNT is 'Amount of transfer allocated to the tax records.';
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.AMOUNT is 'Book dollar amount of the transfer transaction.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (786, 0, 10, 4, 2, 0, 33921, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033921_pwrtax_xfer_intfc_phase2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;