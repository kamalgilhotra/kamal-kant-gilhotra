 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048883_lessor_02_update_ilr_group_accounts_notnull_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 11/09/2017 J Sisouphanh   Add accounts to Lessor ILR Groups
 ||============================================================================
 */ 

alter table LSR_ILR_GROUP
   MODIFY (	INT_ACCRUAL_ACCOUNT_ID        number(22,0) NOT NULL,
			INT_EXPENSE_ACCOUNT_ID        number(22,0) NOT NULL,
			EXEC_ACCRUAL_ACCOUNT_ID       number(22,0) NOT NULL,
			EXEC_EXPENSE_ACCOUNT_ID       number(22,0) NOT NULL,
			CONT_ACCRUAL_ACCOUNT_ID       number(22,0) NOT NULL,
			CONT_EXPENSE_ACCOUNT_ID       number(22,0) NOT NULL,
			ST_RECEIVABLE_ACCOUNT_ID      number(22,0) NOT NULL,
			LT_RECEIVABLE_ACCOUNT_ID      number(22,0) NOT NULL,
			AR_ACCOUNT_ID                 number(22,0) NOT NULL,
			UNGUARAN_RES_ACCOUNT_ID       number(22,0) NOT NULL,
			INT_UNGUARAN_RES_ACCOUNT_ID   number(22,0) NOT NULL,
			SELL_PROFIT_LOSS_ACCOUNT_ID   number(22,0) NOT NULL,
			INI_DIRECT_COST_ACCOUNT_ID    number(22,0) NOT NULL,
			PROP_PLANT_ACCOUNT_ID         number(22,0) NOT NULL,
			ST_DEFERRED_ACCOUNT_ID        number(22,0) NOT NULL,
			LT_DEFERRED_ACCOUNT_ID        number(22,0) NOT NULL,
			CURR_GAIN_LOSS_ACCT_ID        number(22,0) NOT NULL,
			CURR_GAIN_LOSS_OFFSET_ACCT_ID number(22,0) NOT NULL
		);
		
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3946, 0, 2017, 1, 0, 0, 48883, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048883_lessor_02_update_ilr_group_accounts_notnull_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;