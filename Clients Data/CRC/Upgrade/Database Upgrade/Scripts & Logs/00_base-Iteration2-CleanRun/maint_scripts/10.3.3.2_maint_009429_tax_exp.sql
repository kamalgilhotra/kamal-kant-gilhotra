/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009429_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.2   02/03/2012 Sunjin Cone    Point Release
||============================================================================
*/

create index CCWOID_EXP_COST_IX
   on CWIP_CHARGE (WORK_ORDER_ID, COST_ELEMENT_ID, EXPENDITURE_TYPE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (85, 0, 10, 3, 3, 2, 9429, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.2_maint_009429_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
