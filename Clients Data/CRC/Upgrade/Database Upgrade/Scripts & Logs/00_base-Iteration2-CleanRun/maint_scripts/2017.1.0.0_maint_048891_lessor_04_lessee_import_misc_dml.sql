/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048891_lessor_04_lessee_import_misc_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/29/2017 Shane "C" Ward  Set up Lessor Lessee Import Tables
||============================================================================
*/

--Point Lessor Lease group lookup to Lessor table
INSERT INTO pp_import_lookup (import_lookup_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name)
VALUES (1075, 'Lsr Lease Group.Description', 'Lessor Lease Group Id', 'lease_group_id', '( select b.lease_group_id from lsr_lease_group b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )',
0, 'lsr_lease_group', 'description');

UPDATE pp_import_column_lookup SET import_lookup_id = 1075
WHERE column_name = 'lease_group_id' AND import_type_id = '500';

--Recreate Default Template
DELETE FROM pp_import_template_fields WHERE import_type_id = 500 AND import_template_id IN (SELECT import_template_id FROM pp_import_template WHERE description = 'Lessor Lessee Add' AND import_type_id = 500);

INSERT INTO pp_import_template_fields (import_template_id, field_id, import_type_id, column_name)
SELECT t.import_template_id, rownum, t.import_type_id, column_name  FROM pp_import_template t, pp_import_column c WHERE
t.description = 'Lessor Lessee Add' AND t.import_type_id = 500
AND c.import_type_id = t.import_type_id;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3751, 0, 2017, 1, 0, 0, 48891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048891_lessor_04_lessee_import_misc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
