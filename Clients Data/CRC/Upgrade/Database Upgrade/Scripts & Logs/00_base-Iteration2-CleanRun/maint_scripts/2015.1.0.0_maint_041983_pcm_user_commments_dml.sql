/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041983_pcm_user_commments_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 01/19/2015 Sarah Byers      Porting the User Comments and Attachments functionality
||============================================================================
*/

-- Set up new System Options based on System Controls
-- FP - Secure Approved Docs
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'FP - Secure Approved Docs', long_description, 0, 'No', control_value, 1, 0
  from pp_system_control
 where lower(control_name) = 'fp - secure approved docs';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FP - Secure Approved Docs', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FP - Secure Approved Docs', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'FP - Secure Approved Docs', 'pcm');

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'FP - Secure Approved Docs', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'fp - secure approved docs'
	and company_id <> -1;

-- WO - Secure Approved Docs
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WO - Secure Approved Docs', long_description, 0, 'No', control_value, 1, 0
  from pp_system_control
 where lower(control_name) = 'wo - secure approved docs';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WO - Secure Approved Docs', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WO - Secure Approved Docs', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WO - Secure Approved Docs', 'pcm');

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WO - Secure Approved Docs', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'wo - secure approved docs'
	and company_id <> -1;

-- Attach Document to Email Default
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Attach Document to Email Default', 
		 'Controls whether attached documents are defaulted as being included in email attachments. "Yes" includes them by default. "No" does not include them by default.', 
		 0, 'No', control_value, 1, 0
  from pp_system_control
 where lower(control_name) = 'attach document to email default';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Attach Document to Email Default', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Attach Document to Email Default', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Attach Document to Email Default', 'pcm');

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Attach Document to Email Default', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'attach document to email default'
	and company_id <> -1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2193, 0, 2015, 1, 0, 0, 041983, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041983_pcm_user_commments_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;