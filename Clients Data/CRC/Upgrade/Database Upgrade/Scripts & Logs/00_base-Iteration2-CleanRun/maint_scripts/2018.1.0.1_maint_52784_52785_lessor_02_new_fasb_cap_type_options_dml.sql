/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_52784_52785_lessor_02_new_fasb_cap_type_options_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.1 12/03/2018  Josh Sandler     Add new options for Lessor cap types
||============================================================================
*/

UPDATE lsr_fasb_type_sob
SET separate_cost_of_goods_sw = 0,
include_idc_sw = 0
WHERE fasb_cap_type_id = 2;

insert into je_trans_type (trans_type, description)
select 4074, '4074 - Cost of Goods Sold Debit - PPE'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4074);

insert into je_trans_type (trans_type, description)
select 4075, '4075 - Revenue Credit - Receivable'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4075);

insert into je_trans_type (trans_type, description)
select 4076, '4076 - Revenue Credit - Unguaranteed Residual'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4076);

insert into je_trans_type (trans_type, description)
select 4077, '4077 - Cost of Goods Sold Debit - Initial Direct Costs'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4077);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
where trans_type IN (4074, 4075, 4076, 4077)
and not exists (select 1 from je_method_trans_type where trans_type IN (4074, 4075, 4076, 4077) and je_method_id = 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13043, 0, 2018, 1, 0, 1, 52785, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_52784_52785_lessor_02_new_fasb_cap_type_options_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;