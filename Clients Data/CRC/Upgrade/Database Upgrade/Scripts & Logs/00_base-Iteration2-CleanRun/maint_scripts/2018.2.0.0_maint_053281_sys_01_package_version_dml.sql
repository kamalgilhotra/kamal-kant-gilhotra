 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_053281_sys_01_package_version_dml.sql
 ||============================================================================
 || Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2018.2.0.0  03/25/2019 Anand R        Update version info for packages. No new packages for 2018.2
 ||============================================================================
 */

update pp_package_versions
set version = '2018.2.0.0';

insert into pp_package_versions (package_name, version)
select 'RPR_WO_STATUS_PKG' package_name, '2018.2.0.0' version 
from dual 
where not exists
     (select 1 from pp_package_versions where package_name = 'RPR_WO_STATUS_PKG');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16242, 0, 2018, 2, 0, 0, 53281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053281_sys_01_package_version_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;