/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_011457_pwrtax_add_actuals_month.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/08/2014 Anand Rajashekar    Add actuals_month column to version table
||========================================================================================
*/

-- Add actuals_month column to version table

alter table VERSION add ACTUALS_MONTH number(22,2);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1375, 0, 10, 4, 3, 0, 11457, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_011457_pwrtax_add_actuals_month.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
