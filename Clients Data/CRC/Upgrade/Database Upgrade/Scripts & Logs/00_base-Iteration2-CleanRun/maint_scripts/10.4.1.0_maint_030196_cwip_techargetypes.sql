/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030196_cwip_techargetypes.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/30/2013 Sunjin Cone
||============================================================================
*/

update CHARGE_TYPE_DATA
   set PERCENT_BOOK_BASIS = 0, AFUDC_ELIG_INDICATOR = 0, CPI_ELIG_INDICATOR = 0,
       ACCRUAL_ELIG_INDICATOR = 0
 where CHARGE_TYPE_ID in
       (select CHARGE_TYPE_ID
          from CHARGE_TYPE
         where BOOK_SUMMARY_ID in (select BOOK_SUMMARY_ID
                                     from BOOK_SUMMARY
                                    where LOWER(SUMMARY_NAME) like '%tax repair%'
                                   union
                                   select BOOK_SUMMARY_ID
                                     from BOOK_SUMMARY
                                    where LOWER(SUMMARY_NAME) like '%tax expense%'
                                   union
                                   select REVERSAL_BOOK_SUMMARY_ID
                                     from TAX_REVERSAL_BOOK_SUMMARY
                                    where TAX_REVERSAL_TYPE = 'Repairs'));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (491, 0, 10, 4, 1, 0, 30196, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030196_cwip_techargetypes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

commit;