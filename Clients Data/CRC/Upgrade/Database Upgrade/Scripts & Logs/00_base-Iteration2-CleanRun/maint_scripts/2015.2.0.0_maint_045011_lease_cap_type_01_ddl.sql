/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_045011_lease_cap_type_01_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 09/30/2015 Andrew Scott   new cap type window, new fasb cap type table
||============================================================================
*/

create table ls_fasb_cap_type (
   fasb_cap_type_id number(22,0),
   description varchar2(35),
   user_id varchar2(18),
   time_stamp date
);

alter table ls_fasb_cap_type
   add constraint ls_fasb_cap_type_pk
       primary key (fasb_cap_type_id)
       using index tablespace PWRPLANT_IDX; 

COMMENT ON TABLE ls_fasb_cap_type IS '(S) [06] The lease FASB cap type.';
COMMENT ON COLUMN ls_fasb_cap_type.fasb_cap_type_id IS 'System-assigned identifier that specifies the lease FASB cap type.';
COMMENT ON COLUMN ls_fasb_cap_type.description IS 'Description of the FASB cap type.';
COMMENT ON COLUMN ls_fasb_cap_type.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_fasb_cap_type.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

alter table ls_lease_cap_type
add ( fasb_cap_type_id number(22,0), active number(22,0) );

COMMENT ON COLUMN ls_lease_cap_type.fasb_cap_type_id IS 'System-assigned identifier that specifies the lease FASB cap type.';
COMMENT ON COLUMN ls_lease_cap_type.active IS 'Active = 1, Inactive = 0.';

alter table ls_lease_cap_type
   add constraint ls_lease_cap_type_fasb_fk
       foreign key (fasb_cap_type_id)
       references ls_fasb_cap_type;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2891, 0, 2015, 2, 0, 0, 45011, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045011_lease_cap_type_01_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
