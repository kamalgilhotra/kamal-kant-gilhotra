/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009695_ocr_interface.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/12/2012 Chris Mardis   Point Release
||============================================================================
*/

create table ORIGINAL_COST_RETIREMENT
(
 ACTIVITY_CODE       varchar2(8),
 ALLOC_ID            number(22,0),
 AMOUNT              number(22,2),
 ASSET_ID            number(22,0),
 ASSET_LOCATION_ID   number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 CHARGE_AUDIT_ID     number(22,0),
 CHARGE_GROUP_ID     number(22,0),
 CHARGE_MO_YR        date,
 CHARGE_ID           number(22,0),
 CHARGE_TYPE_ID      number(22,0),
 COMPANY_ID          number(22,0),
 DEPARTMENT_ID       number(22,0),
 DESCRIPTION         varchar2(35),
 ESTIMATE_ID         number(22,0),
 EXPENDITURE_TYPE_ID number(22,0),
 EXTERNAL_GL_ACCOUNT varchar2(254),
 GL_ACCOUNT_ID       number(22,0),
 JOB_TASK_ID         number(22,0),
 NOTES               varchar2(2000),
 PROPERTY_GROUP_ID   number(22,0),
 QUANTITY            number(22,0),
 RETIREMENT_UNIT_ID  number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 UNIT_ITEM_ID        number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 WORK_ORDER_ID       number(22,0),
 SERIAL_NUMBER       varchar2(35),
 BATCH_ID            varchar2(35),
 LINE_NO             number(22,0),
 STATUS              number(22,0),
 USER_ID             varchar2(18),
 TIME_STAMP          date
);


--alter table ORIGINAL_COST_RETIREMENT add constraint ORIGINAL_COST_RETIREMENT_PK primary key (LINE_NO, BATCH_ID) using index tablespace PWRPLANT_IDX;

--** original_cost_retirement_stage

CREATE TABLE ORIGINAL_COST_RETIREMENT_STG
(
 ACTIVITY_CODE       varchar2(8),
 ALLOC_ID            number(22,2),
 AMOUNT              number(22,2),
 ASSET_ID            number(22,0),
 ASSET_LOCATION_ID   number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 CHARGE_AUDIT_ID     number(22,0),
 CHARGE_GROUP_ID     number(22,0),
 CHARGE_MO_YR        date,
 CHARGE_ID           number(22,0),
 CHARGE_TYPE_ID      number(22,0),
 COMPANY_ID          number(22,0),
 DEPARTMENT_ID       number(22,0),
 DESCRIPTION         varchar2(35),
 ESTIMATE_ID         number(22,0),
 EXPENDITURE_TYPE_ID number(22,0),
 EXTERNAL_GL_ACCOUNT varchar2(254),
 GL_ACCOUNT_ID       number(22,0),
 JOB_TASK_ID         varchar2(35),
 NOTES               varchar2(2000),
 PROPERTY_GROUP_ID   number(22,0),
 QUANTITY            number(22,0),
 RETIREMENT_UNIT_ID  number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 UNIT_ITEM_ID        number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 WORK_ORDER_ID       number(22,0),
 SERIAL_NUMBER       varchar2(35),
 BATCH_ID            varchar2(35) not null,
 LINE_NO             number(22,0) not null,
 STATUS              number(22,0),
 USER_ID             varchar2(18),
 TIME_STAMP          date
 );

--alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_PK primary key (BATCH_ID, LINE_NO) using index tablespace PWRPLANT_IDX;

alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_ASSET_FK            foreign key (ASSET_ID)            references CPR_LEDGER;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_ASSET_LOCATION_FK   foreign key (ASSET_LOCATION_ID)   references ASSET_LOCATION;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_BUS_SEGMENT_FK      foreign key (BUS_SEGMENT_ID)      references BUSINESS_SEGMENT;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_COMPANY_FK          foreign key (COMPANY_ID)          references COMPANY_SETUP;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_DEPARTMENT_FK       foreign key (DEPARTMENT_ID)       references DEPARTMENT;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_EXPENDITURE_TYPE_FK foreign key (EXPENDITURE_TYPE_ID) references EXPENDITURE_TYPE;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_GL_ACCOUNT_FK       foreign key (GL_ACCOUNT_ID)       references GL_ACCOUNT;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_JOB_TASK_FK         foreign key (JOB_TASK_ID)         references JOB_TASK_LIST;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_PROPERTY_GROUP_FK   foreign key (PROPERTY_GROUP_ID)   references PROPERTY_GROUP;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_RETIREMENT_UNIT_FK  foreign key (RETIREMENT_UNIT_ID)  references RETIREMENT_UNIT;
alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_WORK_ORDER_FK       foreign key (WORK_ORDER_ID)       references WORK_ORDER_CONTROL;

alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_SUB_ACCOUNT_FK      foreign key (UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID) references SUB_ACCOUNT;

--alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_SUB_ACCOUNT_LIST_FK foreign key (SUB_ACCOUNT_ID) references SUB_ACCOUNT_LIST;
--alter table ORIGINAL_COST_RETIREMENT_STG add constraint OCR_UTILITY_ACCOUNT_FK  foreign key (UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID) references UTILITY_ACCOUNT;

insert into PP_WORKSHEETS
   (WORKSHEET_ID, DATAWINDOW, TITLE, DESCRIPTION, ADD_BUTTON, DELETE_BUTTON, EDIT_FIELDS)
   select max(WORKSHEET_ID) + 1,
          'dw_ocr_kickout',
          'OCR Error Rows',
          'Original Cost Retirement - Kickouts',
          'No ',
          'No ',
          1
     from PP_WORKSHEETS;

alter table UNITIZED_WORK_ORDER          add RETIRE_VINTAGE number(22,0);
alter table WO_ESTIMATE                  add RETIRE_VINTAGE number(22,0);
alter table ORIGINAL_COST_RETIREMENT     add RETIRE_VINTAGE number(22,0);
alter table ORIGINAL_COST_RETIREMENT_STG add RETIRE_VINTAGE number(22,0);
alter table WO_INTERFACE_UNIT            add RETIRE_VINTAGE number(22,0);
alter table WO_INTERFACE_UNIT_ARC        add RETIRE_VINTAGE number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (116, 0, 10, 3, 4, 0, 9695, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009695_ocr_interface.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
