/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033683_pwrtax_mex.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/15/2014 Nathan Hollis
||============================================================================
*/

create table TAX_MX_INFLATION_INDEX
(
 ADDITION_INDEX   number(22),
 BASE_INDEX       number(22),
 IN_SERVICE_MONTH date,
 RETIREMENT_INDEX number(22)
);

alter table TAX_MX_INFLATION_INDEX
   add constraint TAX_MX_INFLATION_INDEX_PK
       primary key (IN_SERVICE_MONTH)
       using index tablespace PWRPLANT_IDX;

comment on column TAX_MX_INFLATION_INDEX.ADDITION_INDEX is 'Used to store the monthly INPC. The effective INPC for an asset added that month. It is based upon the Base Index and its relationship to the Base Index should be consistent from year to year.';
comment on column TAX_MX_INFLATION_INDEX.BASE_INDEX is 'Used to store the monthly INPC. The native INPC for that month as published by the government.';
comment on column TAX_MX_INFLATION_INDEX.IN_SERVICE_MONTH is 'Used to store the monthly INPC. In service month for the asset.';
comment on column TAX_MX_INFLATION_INDEX.RETIREMENT_INDEX is 'Used to store the monthly INPC. The Retirement Index is the effective INPC for an asset retired that month. It is based upon the Base Index and its relationship to the Base Index should be consistent from year to year.';

create table TAX_MX_INFLATION_MIDPOINT
(
 MIDPOINT_INDEX number(22),
 TAX_YEAR       number(22)
);

alter table TAX_MX_INFLATION_MIDPOINT
   add constraint TAX_MX_INFLATION_MIDPOINT_PK
       primary key (TAX_YEAR)
       using index tablespace PWRPLANT_IDX;

comment on column TAX_MX_INFLATION_MIDPOINT.MIDPOINT_INDEX is 'Used to store the midpoint INPC for the year (typically June’s INPC). This value is repeated for each monthly tax year for ease of use when joining to the PowerTax records.';
comment on column TAX_MX_INFLATION_MIDPOINT.TAX_YEAR is 'Used to store the midpoint INPC for the year (typically June’s INPC). This value is repeated for each monthly tax year for ease of use when joining to the PowerTax records.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (859, 0, 10, 4, 2, 0, 33683, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033683_pwrtax_mex.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

