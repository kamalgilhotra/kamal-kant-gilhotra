SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029827_lease_create_sequences_8.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/23/2013 Lee Quinn      Point Release
||============================================================================
*/

declare
   TABLE_DOES_NOT_EXIST exception;
   pragma exception_init(TABLE_DOES_NOT_EXIST, -00942);

   LOOPING_CHAIN_OF_SYNONYMS exception;
   pragma exception_init(LOOPING_CHAIN_OF_SYNONYMS, -01775);

   type CV_TYP is ref cursor;
   CV CV_TYP;

   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   L_COUNT             number := 1;
   LB_LEASE_BEING_USED boolean := true;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   begin
      open CV for 'select count(*) from LS_ASSET where ROWNUM < 10';
      fetch CV
         into L_COUNT;
      close CV;

   if L_COUNT = 0 then
      LB_LEASE_BEING_USED := false;
   end if;

   exception
      when TABLE_DOES_NOT_EXIST then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'LS_ASSET table Doesn''t exist so Lease Module can be refreshed.');
      when LOOPING_CHAIN_OF_SYNONYMS then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Looping Chain of Synonyms - LS_ASSET table Doesn''t exist so the Lease Module can be refreshed.');
   end;

   if LB_LEASE_BEING_USED then
      DBMS_OUTPUT.PUT_LINE('Lease is being used so no sequences will be dropped.');
   else
      for CSR_SQL in (select 'drop sequence ' || SEQUENCE_NAME SQL_LINE, SEQUENCE_NAME
                        from ALL_SEQUENCES
                       where SEQUENCE_OWNER = 'PWRPLANT'
                         and SEQUENCE_NAME like 'PWRLEASE%'
                       order by SEQUENCE_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_SQL.SQL_LINE || ';');
         begin
            execute immediate CSR_SQL.SQL_LINE;
            DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Sequence ' || CSR_SQL.SEQUENCE_NAME || ' dropped.');
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;

      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Creating Sequences.');

      execute immediate 'create sequence PWRLEASE1 start with 100';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'PWRLEASE1 sequence created');
      execute immediate 'create sequence PWRLEASE2 start with 100';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'PWRLEASE2 sequence created');
      execute immediate 'create sequence PWRLEASE3 start with 100';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'PWRLEASE3 sequence created');
      execute immediate 'create sequence PWRLEASE4 start with 100';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'PWRLEASE4 sequence created');
      execute immediate 'create sequence PWRLEASE5 start with 100';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'PWRLEASE5 sequence created');
      execute immediate 'create sequence PWRLEASE6 start with 100';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'PWRLEASE6 sequence created');
      execute immediate 'create sequence PWRLEASE7 start with 100';
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'PWRLEASE7 sequence created');
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (364, 0, 10, 4, 0, 0, 29827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029827_lease_create_sequences_8.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
