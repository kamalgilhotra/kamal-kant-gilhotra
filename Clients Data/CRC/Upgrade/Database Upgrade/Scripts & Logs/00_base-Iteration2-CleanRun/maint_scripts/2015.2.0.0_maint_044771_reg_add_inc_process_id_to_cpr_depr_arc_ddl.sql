/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044771_reg_add_inc_process_id_to_cpr_depr_arc_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   09/01/2015 Sarah Byers    Add incremental_process_id to cpr_depr_calc_stg_arc
||============================================================================
*/

alter table cpr_depr_calc_stg_arc add (incremental_process_id number(22,0));

comment on column cpr_depr_calc_stg_arc.incremental_process_id is 'System assigned identifier for an incremental process.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2836, 0, 2015, 2, 0, 0, 044771, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044771_reg_add_inc_process_id_to_cpr_depr_arc_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;