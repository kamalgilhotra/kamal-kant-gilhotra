/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048782_lessor_01_receivable_bucket_admin_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/11/2017 Shane Ward       Create Receivable Bucket Maint workspace for Lessor
||============================================================================
*/

CREATE TABLE lsr_receivable_bucket_admin (
  receivable_type            VARCHAR2(35) NOT NULL,
  bucket_number        NUMBER(2,0)  NOT NULL,
  bucket_name          VARCHAR2(35) NULL,
  status_code_id       NUMBER(1,0)  NULL,
  time_stamp           DATE         NULL,
  user_id              VARCHAR2(18) NULL,
  accrual_acct_id      NUMBER(22,0) NOT NULL,
  receivable_acct_id      NUMBER(22,0) NOT NULL
);

ALTER TABLE lsr_receivable_bucket_admin
  ADD CONSTRAINT lsr_receivable_bucket_admin_pk PRIMARY KEY (
    receivable_type,
    bucket_number
  )
;

ALTER TABLE lsr_receivable_bucket_admin
  ADD CONSTRAINT fk_lsr_rec_bucket_admin1 FOREIGN KEY (
    accrual_acct_id
  ) REFERENCES gl_account (
    gl_account_id
  );

ALTER TABLE lsr_receivable_bucket_admin
  ADD CONSTRAINT fk_lsr_rec_bucket_admin2 FOREIGN KEY (
    receivable_acct_id
  ) REFERENCES gl_account (
    gl_account_id
  );


COMMENT ON TABLE lsr_receivable_bucket_admin IS '(S)  [06]
The Lessor Receivables Bucket Admin table is a configuration table to configure up to 10 Executory and Contingent buckets.';

COMMENT ON COLUMN lsr_receivable_bucket_admin.receivable_type IS 'Contingent or Executory.';
COMMENT ON COLUMN lsr_receivable_bucket_admin.bucket_number IS '1 - 10.';
COMMENT ON COLUMN lsr_receivable_bucket_admin.bucket_name IS 'The name of the bucket.';
COMMENT ON COLUMN lsr_receivable_bucket_admin.status_code_id IS '1 means the bucket is active.';
COMMENT ON COLUMN lsr_receivable_bucket_admin.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_receivable_bucket_admin.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_receivable_bucket_admin.accrual_acct_id IS 'The GL Account for accruals for this particular bucket.';
COMMENT ON COLUMN lsr_receivable_bucket_admin.receivable_acct_id IS 'The GL Account for receivables for this particular bucket.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3694, 0, 2017, 1, 0, 0, 48782, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048782_lessor_01_receivable_bucket_admin_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;