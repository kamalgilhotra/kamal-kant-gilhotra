/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029969_lease_transfers.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/05/2013 Kyle Peterson  Point Release
||============================================================================
*/

update SUBLEDGER_CONTROL set DEPRECIATION_INDICATOR = 3 where SUBLEDGER_TYPE_ID = -100;

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   ((select max(PROCESS_ID) + 1 from PP_PROCESSES), 'Lessee Calculations', 'Lessee Calculations');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (586, 0, 10, 4, 1, 0, 29969, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029969_lease_transfers.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
