set serveroutput on
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_006985_sys_add_post_bal_108_control.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 10.4.1.0   08/08/2013 Charlie Shilling add system control
||============================================================================
*/

declare
   V_SYS_CONTROL_COUNT number(22, 0) := 0;

begin
   select count(1)
     into V_SYS_CONTROL_COUNT
     from PP_SYSTEM_CONTROL_COMPANY
    where trim(UPPER(CONTROL_NAME)) = 'POST BALANCE CWIP 108';

   if V_SYS_CONTROL_COUNT = 0 then
      insert into PP_SYSTEM_CONTROL_COMPANY
         (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
      values
         ((select NVL(max(CONTROL_ID), 0) + 1 from PP_SYSTEM_CONTROL_COMPANY),
          'POST BALANCE CWIP 108', 'YES', 'dw_yes_no;1',
          '"Yes" will cause Post to balance cost of removal, salvage cash, salvage returns, and reserve credits between CWIP and pend transaction',
          -1);

      DBMS_OUTPUT.PUT_LINE('POST BALANCE CWIP 108 system control successfully added to database with a default value of YES');
   else
      DBMS_OUTPUT.PUT_LINE('POST BALANCE CWIP 108 system control already exists in this database.');
   end if;
end; --*no exception block - let all exception fall through
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (501, 0, 10, 4, 1, 0, 6985, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_006985_sys_add_post_bal_108_control.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
