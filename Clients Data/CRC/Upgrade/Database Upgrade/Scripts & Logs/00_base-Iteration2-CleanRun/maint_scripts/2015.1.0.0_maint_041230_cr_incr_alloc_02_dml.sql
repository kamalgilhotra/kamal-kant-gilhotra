/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041230_cr_incr_alloc_dml_2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   11/20/2015 M Allen, A Scott Incremental Allocations Setup DML 2
||============================================================================
*/ 

insert into CR_ALLOC_INCREMENTAL_STATUS
   (INCREMENTAL_STATUS_ID, DESCRIPTION)
values
   (1, 'Processed');

insert into CR_ALLOC_INCREMENTAL_STATUS
   (INCREMENTAL_STATUS_ID, DESCRIPTION)
values
   (2, 'Undone/Deleted');

insert into CR_ALLOC_INCREMENTAL_STATUS
   (INCREMENTAL_STATUS_ID, DESCRIPTION)
values
   (3, 'Reversed');

insert into CR_ALLOC_INCREMENTAL_STATUS
   (INCREMENTAL_STATUS_ID, DESCRIPTION)
values
   (4, 'Reprocessed');




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2057, 0, 2015, 1, 0, 0, 41230, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041230_cr_incr_alloc_02_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;