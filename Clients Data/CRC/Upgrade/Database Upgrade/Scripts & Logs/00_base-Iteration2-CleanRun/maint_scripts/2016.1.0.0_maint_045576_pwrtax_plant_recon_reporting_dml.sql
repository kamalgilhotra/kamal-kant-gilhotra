/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045576_pwrtax_plant_recon_reporting_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 04/11/2016 Charlie Shilling add new report to reporting workspace
||============================================================================
*/
INSERT INTO pp_report_type (report_type_id, description, sort_order)
VALUES(410, 'PowerTax - Reconciliation', 5);

INSERT INTO pp_reports_filter (pp_report_filter_id, description, filter_uo_name)
VALUES (100, 'PowerTax - Plant Recon Form Report', 'uo_ppbase_tab_filter_dynamic');

DECLARE
	l_next_filter_id 			pp_dynamic_filter.filter_id%TYPE;
BEGIN
	SELECT Nvl(Max(filter_id),0) + 1
	INTO l_next_filter_id
	FROM pp_dynamic_filter;

	INSERT INTO pp_dynamic_filter (filter_id, label, sqls_column_expression, input_type, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only)
	VALUES (l_next_filter_id, 'Plant Reconciliation Form', 'pr.tax_pr_form_id', 'dw', 'dw_tax_plant_recon_form_filter', 'tax_plant_recon_form_tax_pr_form_id', 'description', 'n', 1, 1);

	INSERT INTO pp_dynamic_filter_mapping (pp_report_filter_id, filter_id)
	VALUES (100, l_next_filter_id);
END;
/

INSERT INTO pp_reports (report_id, description, long_description, subsystem, datawindow,
	pp_report_subsystem_id, report_number, report_type_id, pp_report_status_id,
	pp_report_time_option_id, pp_report_filter_id, pp_report_envir_id, turn_off_multi_thread)
SELECT Nvl(Max(report_id),0) + 1, 'Tax Plant Reconciliation', 'Tax Plant Reconciliation', 'PowerTax', 'dw_tax_plant_recon_rpt_header',
	1, 'PowerTax - 302', 410, 1,
	0, 100, 3, 1
FROM pp_reports;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3134, 0, 2016, 1, 0, 0, 045576, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045576_pwrtax_plant_recon_reporting_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;