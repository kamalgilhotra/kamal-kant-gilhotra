/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_044209_budgetcap_overheads_eligibility_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/09/2015 Ryan Oliveria  New Eligibility Tables for Overheads
||============================================================================
*/

create table WO_TYPE_CLEAR_DFLT_BDG
(
	WORK_ORDER_TYPE_ID number(22,0) not null,
	CLEARING_ID number(22,0) not null,
	TIME_STAMP date,
	USER_ID varchar2(18)
);

alter table WO_TYPE_CLEAR_DFLT_BDG
	add constraint PK_WO_TYPE_CLEAR_DFLT_BDG
		primary key (WORK_ORDER_TYPE_ID, CLEARING_ID)
		using index tablespace PWRPLANT_IDX;

alter table WO_TYPE_CLEAR_DFLT_BDG
   add constraint R_WO_TYPE_CLEAR_DFLT_BDG1
       foreign key (WORK_ORDER_TYPE_ID)
       references WORK_ORDER_TYPE;

alter table WO_TYPE_CLEAR_DFLT_BDG
   add constraint R_WO_TYPE_CLEAR_DFLT_BDG2
       foreign key (CLEARING_ID)
       references CLEARING_WO_CONTROL_BDG;

comment on table WO_TYPE_CLEAR_DFLT_BDG is 'The Work Order Type Clearing Default Budget data table associates each Budget Overhead with the Work Order Types to which the overheads should be allocated. Individual Work Orders within a particular Work Order Type may be overridden using the WO FP Clearing Override Budget data table. The Work Order Type Clearing Defaults Budget table is maintained on the Overhead maintenance Screens.';
comment on column WO_TYPE_CLEAR_DFLT_BDG.WORK_ORDER_TYPE_ID is 'System-assigned identifier of the particular Work Order Type.';
comment on column WO_TYPE_CLEAR_DFLT_BDG.CLEARING_ID is 'System-assigned identifier of a particular Clearing Work Order.';
comment on column WO_TYPE_CLEAR_DFLT_BDG.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column WO_TYPE_CLEAR_DFLT_BDG.USER_ID is 'Standard System-assigned user id used for audit purposes';



create table WO_FP_CLEAR_OVER_BDG
(
	WORK_ORDER_ID number(22,0) not null,
	CLEARING_ID number(22,0) not null,
	TIME_STAMP date,
	USER_ID varchar2(18),
	INCLUDE_EXCLUDE number(22,0) not null
);

alter table WO_FP_CLEAR_OVER_BDG
	add constraint PK_WO_FP_CLEAR_OVER_BDG
		primary key (WORK_ORDER_ID, CLEARING_ID)
		using index tablespace PWRPLANT_IDX;

alter table WO_FP_CLEAR_OVER_BDG
   add constraint R_WO_FP_CLEAR_OVER_BDG1
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL;

alter table WO_FP_CLEAR_OVER_BDG
   add constraint R_WO_FP_CLEAR_OVER_BDG2
       foreign key (CLEARING_ID)
       references CLEARING_WO_CONTROL_BDG;

comment on table WO_FP_CLEAR_OVER_BDG is 'The Work Order/Funding Project Clearing Overrides Budget data table allows the user to specify that this Work Order should or should not be included for a particular overhead (contained in a particular clearing Work Order). Normally, participation in overheads is determined by Work Order Type (via the Work Order Type Clearing Default Budget data table). The Work Order Clearing Overrides table is maintained through the Overhead Maintenance screen.';
comment on column WO_FP_CLEAR_OVER_BDG.WORK_ORDER_ID is 'System-assigned identifier of a particular work order.';
comment on column WO_FP_CLEAR_OVER_BDG.CLEARING_ID is 'System-assigned identifier of a particular clearing work order.';
comment on column WO_FP_CLEAR_OVER_BDG.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column WO_FP_CLEAR_OVER_BDG.USER_ID is 'Standard System-assigned user id used for audit purposes.';
comment on column WO_FP_CLEAR_OVER_BDG.INCLUDE_EXCLUDE is 'Include_exclude is a yes/no indicator indicating whether or not the Work Order participates in the overhead clearing process. A ''yes'' (1) indicates the Work Order is included, a ''No'' (0) indicates the Work Order is excluded.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2689, 0, 2015, 2, 0, 0, 044209, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044209_budgetcap_overheads_eligibility_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;