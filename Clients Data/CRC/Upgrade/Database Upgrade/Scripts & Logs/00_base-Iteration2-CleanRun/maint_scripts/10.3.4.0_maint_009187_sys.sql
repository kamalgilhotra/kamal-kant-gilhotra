/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009187_sys.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   01/18/2011 Paul Bull      Table Maintenance
||============================================================================
*/

-- Inserts for cr_dd_sources_criteria

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, COLUMN_RANK)
values
   ('cr_dd_sources_criteria', 'SQL', 'sql', 'e', 9);


-- Inserts for cr_dd_sources_criteria_fields

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, COLUMN_RANK)
values
   ('cr_dd_sources_criteria_fields', 'DDDW_SQL', 'DDDW SQL', 'e', 17);

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, DROPDOWN_NAME, COLUMN_RANK)
values
   ('cr_dd_sources_criteria_fields', 'ELEMENT_ID', 'Element Id', 'p', 'cr_element', 13);

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, COLUMN_RANK)
values
   ('cr_dd_sources_criteria_fields', 'FORMAT_OVERRIDE', 'Format Override', 'e', 14);

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, DROPDOWN_NAME, COLUMN_RANK)
values
   ('cr_dd_sources_criteria_fields', 'QUANTITY_FIELD', 'Quantity Field', 'p', 'qf_yes_no', 11);

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, DROPDOWN_NAME, COLUMN_RANK)
values
   ('cr_dd_sources_criteria_fields', 'REQUIRED_FILTER', 'Required Filter', 'p', 'rf_yes_no', 15);

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, DROPDOWN_NAME, COLUMN_RANK)
values
   ('cr_dd_sources_criteria_fields', 'REQUIRED_ONE_MULTI', 'Required One Multiple', 'p',
    'rom_yes_no', 16);

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, DROPDOWN_NAME, COLUMN_RANK)
values
   ('cr_dd_sources_criteria_fields', 'TABLE_LOOKUP', 'Table Lookup', 'p', 'tl_yes_no', 18);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (92, 0, 10, 3, 4, 0, 9187, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009187_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
