/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029215_03_POST_CHECK_SET_OF_BOOKS.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- --------------   ------------------------------------
|| 10.4.0.0   02/05/2013 Charlie Shilling Point Release
||============================================================================
*/

create or replace procedure POST_CHECK_SET_OF_BOOKS(A_COMMIT_FLAG in integer) is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: POST_CHECK_SET_OF_BOOKS
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   ||============================================================================
   */

   MISSING_PEND_TRANS_IDS PEND_TRANS_IDS_T;
   L_MISSING_COUNT        integer;

begin
   DBMS_OUTPUT.PUT_LINE('    Begin post_check_set_of_books Oracle procedure');

   select distinct PEND_TRANS_ID bulk collect
     into MISSING_PEND_TRANS_IDS
     from (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
             from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B, PEND_BASIS C
            where A.COMPANY_ID = B.COMPANY_ID
              and A.PEND_TRANS_ID = C.PEND_TRANS_ID
              and B.INCLUDE_INDICATOR = 1
              and A.POSTING_STATUS = 2
           minus (select A.PEND_TRANS_ID, 1 SET_OF_BOOKS_ID
                   from PEND_TRANSACTION A
                  where A.POSTING_STATUS = 2
                 union all
                 select B.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                   from PEND_TRANSACTION A, PEND_TRANSACTION_SET_OF_BOOKS B
                  where A.PEND_TRANS_ID = B.PEND_TRANS_ID
                    and A.POSTING_STATUS = 2))
    order by PEND_TRANS_ID;

   L_MISSING_COUNT := MISSING_PEND_TRANS_IDS.COUNT();

   DBMS_OUTPUT.PUT_LINE('      Post found ' || L_MISSING_COUNT ||
                        ' transactions with at least one missing set_of_books.');

   if L_MISSING_COUNT > 0 then
      begin
         update PEND_TRANSACTION
            set POSTING_STATUS = 3,
                POSTING_ERROR = 'POST 1019: ERROR: This transaction is missing one or more set_of_books from pend_transaction_set_of_books.'
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));
      exception
         when others then
            update PEND_TRANSACTION
               set POSTING_STATUS = 3
             where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));
      end;

      if A_COMMIT_FLAG = 1 then
         commit; --commit here to make sure that the eror gets saved if the next few statements error.
      end if;

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_transaction_temp');
      insert into PEND_TRANSACTION_TEMP
         select *
           from PEND_TRANSACTION
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_basis_temp');
      insert into PEND_BASIS_TEMP
         select *
           from PEND_BASIS
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Delete from pend_basis');
      delete from PEND_BASIS
       where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_basis');
      insert into PEND_BASIS
         select * from PEND_BASIS_TEMP;

      DBMS_OUTPUT.PUT_LINE('      Update adjusted columns on pend_transaction');
      update PEND_TRANSACTION A
         set (ADJUSTED_COST_OF_REMOVAL,
               ADJUSTED_SALVAGE_CASH,
               ADJUSTED_SALVAGE_RETURNS,
               ADJUSTED_RESERVE_CREDITS) =
              (select ADJUSTED_COST_OF_REMOVAL,
                      ADJUSTED_SALVAGE_CASH,
                      ADJUSTED_SALVAGE_RETURNS,
                      ADJUSTED_RESERVE_CREDITS
                 from PEND_TRANSACTION_TEMP B
                where A.PEND_TRANS_ID = B.PEND_TRANS_ID)
       where exists (select 1 from PEND_TRANSACTION_TEMP B where A.PEND_TRANS_ID = B.PEND_TRANS_ID);

      update PEND_TRANSACTION
         set POSTING_ERROR = 'POST 1020: ERROR: Post created new set_of_books data for this transaction. Please review and re-approve to Post.'
       where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      if A_COMMIT_FLAG = 1 then
         commit;
      end if;
   end if;
end POST_CHECK_SET_OF_BOOKS;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (285, 0, 10, 4, 0, 0, 29215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029215_03_POST_CHECK_SET_OF_BOOKS.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;