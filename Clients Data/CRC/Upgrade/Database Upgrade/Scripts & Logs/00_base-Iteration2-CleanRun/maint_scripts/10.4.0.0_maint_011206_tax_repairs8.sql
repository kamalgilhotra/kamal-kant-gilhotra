/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs8.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   04/16/2013 Alex P.         Point Release
||============================================================================
*/

create global temporary table REPAIR_POST_ACTIVITY_STG
(
 ASSET_ID                 number(22,0) not null,
 ASSET_ACTIVITY_ID        number(22,0) not null,
 BATCH_ID                 number(22,0) not null,
 BOOK_SUMMARY_ID          number(22,0) not null,
 WORK_REQUEST             varchar2(35) not null,
 INSERT_ASSET_ACTIVITY_ID number(22,0) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
) on commit preserve rows;

alter table REPAIR_POST_ACTIVITY_STG
   add constraint PK_RPA_TEMP
       primary key (ASSET_ID, ASSET_ACTIVITY_ID, BATCH_ID, BOOK_SUMMARY_ID,
                    WORK_REQUEST, INSERT_ASSET_ACTIVITY_ID);


update PP_DATAWINDOW_HINTS
   set SELECT_NUMBER = 1, HINT = '/*+ no_merge(cpr_max) */',
       REASON = 'Improves Row_Number() function performance'
 where DATAWINDOW = 'TAXREP UPDATE INSERT_ACTIVITY_ID';

update PPBASE_MENU_ITEMS
   set LABEL = 'Testing Parameters'
 where LABEL = 'Assign Tax Test'
   and MODULE = 'REPAIRS';

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'PWRPLANT',
          TO_TIMESTAMP('22-03-2013 16:41:30', 'DD-MM-YYYY HH24:MI:SS'),
          'Tax Repairs CWIP Analysis',
          'Tax Repairs CWIP Analysis grouped by Company',
          null,
          'dw_rpr_rpt_proj_taxrpr_analysis',
          'REPAIRS',
          null,
          null,
          'RPR - 0090',
          null,
          null,
          null,
          13,
          100,
          43,
          34,
          1,
          3,
          null,
          null,
          TO_TIMESTAMP('22-03-2013 16:41:30', 'DD-MM-YYYY HH24:MI:SS'),
          null,
          null,
          0
     from PP_REPORTS;


alter table TE_AGGREGATION
   add (TIME_STAMP date,
        USER_ID    varchar2(18));

update PP_REPORTS
   set DESCRIPTION = 'Tax Repairs CWIP Roll Forward',
       LONG_DESCRIPTION = 'Tax Repairs CWIP Roll Forward grouped by Company',
       DATAWINDOW = 'dw_rpr_rpt_cwip_rollfwd'
 where DATAWINDOW = 'dw_rpr_rpt_cwip_repairs';

update PP_REPORTS
   set DESCRIPTION = 'Tax Repairs In-Service Analysis',
       LONG_DESCRIPTION = 'Tax Repairs Placed-in-Service Analysis grouped by Company',
       DATAWINDOW = 'dw_rpr_rpt_asst_taxrpr_analysis'
 where DATAWINDOW = 'dw_rpr_rpt_inservice_details';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (351, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs8.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;