/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047023_lease_add_payment_term_stg_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2017.1.0.0 03/16/2017 David Haupt		 Add staging table for initial measure calc
||============================================================================
*/ 

CREATE GLOBAL TEMPORARY TABLE ls_ilr_payment_term_stg (
  ilr_id                      NUMBER(22,0) NOT NULL,
  payment_term_id             NUMBER(22,0) NOT NULL,
  payment_term_type_id        NUMBER(22,0) NOT NULL,
  payment_term_date           DATE         NOT NULL,
  payment_freq_id             NUMBER(22,0) NOT NULL,
  number_of_terms             NUMBER(22,0) NULL,
  est_executory_cost          NUMBER(22,2) NULL,
  paid_amount                 NUMBER(22,2) NULL,
  contingent_amount           NUMBER(22,2) DEFAULT 0 NULL,
  currency_type_id            NUMBER(22,0) NULL,
  c_bucket_1                  NUMBER(22,2) NULL,
  c_bucket_2                  NUMBER(22,2) NULL,
  c_bucket_3                  NUMBER(22,2) NULL,
  c_bucket_4                  NUMBER(22,2) NULL,
  c_bucket_5                  NUMBER(22,2) NULL,
  c_bucket_6                  NUMBER(22,2) NULL,
  c_bucket_7                  NUMBER(22,2) NULL,
  c_bucket_8                  NUMBER(22,2) NULL,
  c_bucket_9                  NUMBER(22,2) NULL,
  c_bucket_10                 NUMBER(22,2) NULL,
  e_bucket_1                  NUMBER(22,2) NULL,
  e_bucket_2                  NUMBER(22,2) NULL,
  e_bucket_3                  NUMBER(22,2) NULL,
  e_bucket_4                  NUMBER(22,2) NULL,
  e_bucket_5                  NUMBER(22,2) NULL,
  e_bucket_6                  NUMBER(22,2) NULL,
  e_bucket_7                  NUMBER(22,2) NULL,
  e_bucket_8                  NUMBER(22,2) NULL,
  e_bucket_9                  NUMBER(22,2) NULL,
  e_bucket_10                 NUMBER(22,2) NULL,
  revision                    NUMBER(22,0) NOT NULL,
  make_ii_payment             NUMBER(1,0)  DEFAULT 0 NULL,
  interim_interest_begin_date DATE         NULL,
  term_air                    NUMBER(22,8) NULL
)
  ON COMMIT PRESERVE ROWS
/

ALTER TABLE ls_ilr_payment_term_stg
  ADD CONSTRAINT pk_ls_ilr_payment_term_stg PRIMARY KEY (
    ilr_id,
    revision,
    payment_term_id
  )
/

COMMENT ON TABLE ls_ilr_payment_term_stg IS '(O)  [06]
The ILR payment term staging table contains the payment parameters for the associated ILR.  An ILR will typically contain several records in the payment term table spanning all interim, normal, and extended payment periods.';

COMMENT ON COLUMN ls_ilr_payment_term_stg.ilr_id IS 'System-assigned identifier of a particular Individual Lease Record.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.payment_term_id IS 'Sequence number used to maintain uniqueness among the ILR payment term records.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.payment_term_type_id IS 'References a payment term type from the fixed table, ls_payment_term_type.  Payment term types include the following values:  Annual, Semi-annual, Monthly, Quarterly, or Actual.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.payment_term_date IS 'Records date related to the payment term.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.payment_freq_id IS 'System-assigned identifier for one of the following values:  Annual, Semi-annual, Monthly, Quarterly, or Actual.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.number_of_terms IS 'Indicates a number of successive months that possess this record''s [type - frequency - date] combination.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.est_executory_cost IS 'Records estimated payments to be made for maintenance, taxes, and insurance that is included in the lease payment.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.paid_amount IS 'Represents the payment amount for the particular term.  For variable leases, the Paid Amount is not used due to dynamic interest rates.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.contingent_amount IS 'Records estimated payments to be made for contingent rent that is included in the lease payment.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.currency_type_id IS 'Multiple currency support.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_1 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_2 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_3 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_4 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_5 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_6 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_7 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_8 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_9 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.c_bucket_10 IS 'Payment Amount for Contingent Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_1 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_2 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_3 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_4 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_5 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_6 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_7 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_8 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_9 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.e_bucket_10 IS 'Payment Amount for Executory Bucket.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.revision IS 'The revision.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.make_ii_payment IS 'Indicates whether an interim interest term makes a payment in the same month. Defaults to 0';
COMMENT ON COLUMN ls_ilr_payment_term_stg.interim_interest_begin_date IS 'The date on which interim interest begins for an interim interest payment term.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.term_air IS 'Indicates the interest rate for the given term for a fixed principal lease';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3388, 0, 2017, 1, 0, 0, 47023, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047023_lease_add_payment_term_stg_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;