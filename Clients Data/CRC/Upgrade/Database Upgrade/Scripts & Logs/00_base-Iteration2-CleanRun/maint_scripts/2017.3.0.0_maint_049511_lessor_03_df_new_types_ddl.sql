/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049511_lessor_03_df_new_types_ddl.sql
|| Description:	Prepare types for DF Schedule
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.2.0.0 01/30/2018 Andrew Hill    Add new types to support df schedule calcs
||============================================================================
*/

CREATE OR REPLACE TYPE t_number_22_2_tab as table of number(22,2);
/

CREATE OR REPLACE TYPE lsr_ilr_df_schedule_result IS OBJECT(MONTH DATE,
                                                            principal_received NUMBER(22,2),
                                                            interest_income_received 		 NUMBER(22,2),
                                                            interest_income_accrued 		 NUMBER(22,2),
                                                            principal_accrued            NUMBER(22,2),
                                                            begin_receivable 				     NUMBER(22,2),
                                                            end_receivable 					     NUMBER(22,2),
                                                            begin_lt_receivable          NUMBER(22,2),
                                                            end_lt_receivable            NUMBER(22,2),
                                                            initial_direct_cost          NUMBER(22,2),
                                                            executory_accrual1           NUMBER(22,2),
                                                            executory_accrual2           NUMBER(22,2),
                                                            executory_accrual3           NUMBER(22,2),
                                                            executory_accrual4           NUMBER(22,2),
                                                            executory_accrual5           NUMBER(22,2),
                                                            executory_accrual6           NUMBER(22,2),
                                                            executory_accrual7           NUMBER(22,2),
                                                            executory_accrual8           NUMBER(22,2),
                                                            executory_accrual9           NUMBER(22,2),
                                                            executory_accrual10          NUMBER(22,2),
                                                            executory_paid1              NUMBER(22,2),
                                                            executory_paid2              NUMBER(22,2),
                                                            executory_paid3              NUMBER(22,2),
                                                            executory_paid4              NUMBER(22,2),
                                                            executory_paid5              NUMBER(22,2),
                                                            executory_paid6              NUMBER(22,2),
                                                            executory_paid7              NUMBER(22,2),
                                                            executory_paid8              NUMBER(22,2),
                                                            executory_paid9              NUMBER(22,2),
                                                            executory_paid10             NUMBER(22,2),
                                                            contingent_accrual1          NUMBER(22,2),
                                                            contingent_accrual2          NUMBER(22,2),
                                                            contingent_accrual3          NUMBER(22,2),
                                                            contingent_accrual4          NUMBER(22,2),
                                                            contingent_accrual5          NUMBER(22,2),
                                                            contingent_accrual6          NUMBER(22,2),
                                                            contingent_accrual7          NUMBER(22,2),
                                                            contingent_accrual8          NUMBER(22,2),
                                                            contingent_accrual9          NUMBER(22,2),
                                                            contingent_accrual10         NUMBER(22,2),
                                                            contingent_paid1             NUMBER(22,2),
                                                            contingent_paid2             NUMBER(22,2),
                                                            contingent_paid3             NUMBER(22,2),
                                                            contingent_paid4             NUMBER(22,2),
                                                            contingent_paid5             NUMBER(22,2),
                                                            contingent_paid6             NUMBER(22,2),
                                                            contingent_paid7             NUMBER(22,2),
                                                            contingent_paid8             NUMBER(22,2),
                                                            contingent_paid9             NUMBER(22,2),
                                                            contingent_paid10            NUMBER(22,2),
                                                            begin_unguaranteed_residual NUMBER(22,2),
                                                            int_on_unguaranteed_residual NUMBER(22,2),
                                                            end_unguaranteed_residual NUMBER(22,2),
                                                            begin_net_investment NUMBER(22,2),
                                                            int_on_net_investment NUMBER(22,2),
                                                            end_net_investment NUMBER(22,2),
                                                            begin_deferred_profit number(22,2),
                                                            recognized_profit number(22,2),
                                                            end_deferred_profit number(22,2),
                                                            rate_implicit NUMBER(22,8),
                                                            compounded_rate NUMBER(22,8),
                                                            discount_rate number(22,8),
                                                            rate_implicit_ni number(22,8),
                                                            compounded_rate_ni number(22,8),
                                                            discount_rate_ni number(22,8),
                                                            begin_lease_receivable number(22,2),
                                                            npv_lease_payments NUMBER(22,2),
                                                            npv_guaranteed_residual NUMBER(22,2),
                                                            npv_unguaranteed_residual NUMBER(22,2),
                                                            selling_profit_loss NUMBER(22,2),
                                                            cost_of_goods_sold NUMBER(22,2));
/

create or replace type lsr_ilr_df_schedule_result_tab as table of lsr_ilr_df_schedule_result;
/

CREATE OR REPLACE TYPE t_lsr_rates_implicit_in_lease AS OBJECT (rate_implicit NUMBER(22,8),
                                                                rate_implicit_ni NUMBER(22,8));
/

DROP TYPE lsr_ilr_sales_sch_result_tab;

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER(22,2),
                                                          interest_income_received 		 NUMBER(22,2),
                                                          interest_income_accrued 		 NUMBER(22,2),
                                                          principal_accrued            NUMBER(22,2),
                                                          begin_receivable 				     NUMBER(22,2),
                                                          end_receivable 					     NUMBER(22,2),
                                                          begin_lt_receivable          NUMBER(22,2),
                                                          end_lt_receivable            NUMBER(22,2),
                                                          initial_direct_cost          NUMBER(22,2),
                                                          executory_accrual1           NUMBER(22,2),
                                                          executory_accrual2           NUMBER(22,2),
                                                          executory_accrual3           NUMBER(22,2),
                                                          executory_accrual4           NUMBER(22,2),
                                                          executory_accrual5           NUMBER(22,2),
                                                          executory_accrual6           NUMBER(22,2),
                                                          executory_accrual7           NUMBER(22,2),
                                                          executory_accrual8           NUMBER(22,2),
                                                          executory_accrual9           NUMBER(22,2),
                                                          executory_accrual10          NUMBER(22,2),
                                                          executory_paid1              NUMBER(22,2),
                                                          executory_paid2              NUMBER(22,2),
                                                          executory_paid3              NUMBER(22,2),
                                                          executory_paid4              NUMBER(22,2),
                                                          executory_paid5              NUMBER(22,2),
                                                          executory_paid6              NUMBER(22,2),
                                                          executory_paid7              NUMBER(22,2),
                                                          executory_paid8              NUMBER(22,2),
                                                          executory_paid9              NUMBER(22,2),
                                                          executory_paid10             NUMBER(22,2),
                                                          contingent_accrual1          NUMBER(22,2),
                                                          contingent_accrual2          NUMBER(22,2),
                                                          contingent_accrual3          NUMBER(22,2),
                                                          contingent_accrual4          NUMBER(22,2),
                                                          contingent_accrual5          NUMBER(22,2),
                                                          contingent_accrual6          NUMBER(22,2),
                                                          contingent_accrual7          NUMBER(22,2),
                                                          contingent_accrual8          NUMBER(22,2),
                                                          contingent_accrual9          NUMBER(22,2),
                                                          contingent_accrual10         NUMBER(22,2),
                                                          contingent_paid1             NUMBER(22,2),
                                                          contingent_paid2             NUMBER(22,2),
                                                          contingent_paid3             NUMBER(22,2),
                                                          contingent_paid4             NUMBER(22,2),
                                                          contingent_paid5             NUMBER(22,2),
                                                          contingent_paid6             NUMBER(22,2),
                                                          contingent_paid7             NUMBER(22,2),
                                                          contingent_paid8             NUMBER(22,2),
                                                          contingent_paid9             NUMBER(22,2),
                                                          contingent_paid10            NUMBER(22,2),
                                                          begin_unguaranteed_residual NUMBER(22,2),
                                                          int_on_unguaranteed_residual NUMBER(22,2),
                                                          end_unguaranteed_residual NUMBER(22,2),
                                                          begin_net_investment NUMBER(22,2),
                                                          int_on_net_investment NUMBER(22,2),
                                                          end_net_investment NUMBER(22,2),
                                                          rate_implicit NUMBER(22,8),
                                                          compounded_rate NUMBER(22,8),
                                                          discount_rate number(22,8),
                                                          rate_implicit_ni number(22,8),
                                                          compounded_rate_ni number(22,8),
                                                          discount_rate_ni number(22,8),
                                                          begin_lease_receivable number(22,2),
                                                          npv_lease_payments NUMBER(22,2),
                                                          npv_guaranteed_residual NUMBER(22,2),
                                                          npv_unguaranteed_residual NUMBER(22,2),
                                                          selling_profit_loss NUMBER(22,2),
                                                          cost_of_goods_sold NUMBER(22,2));
/

create type lsr_ilr_sales_sch_result_tab as table of lsr_ilr_sales_sch_result;
/

drop type lsr_ilr_sales_sch_info_tab;

CREATE OR REPLACE TYPE "PWRPLANT"."LSR_ILR_SALES_SCH_INFO" AS OBJECT (carrying_cost NUMBER(22,2),
                                                                      fair_market_value NUMBER(22,2),
                                                                      guaranteed_residual NUMBER(22,2),
                                                                      estimated_residual NUMBER(22,2),
                                                                      days_in_year NUMBER(22,2),
                                                                      purchase_option_amount NUMBER(22,2),
                                                                      termination_amount NUMBER(22,2));
/

create type lsr_ilr_sales_sch_info_tab as table of lsr_ilr_sales_sch_info;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4096, 0, 2017, 3, 0, 0, 49511, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_049511_lessor_03_df_new_types_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 
