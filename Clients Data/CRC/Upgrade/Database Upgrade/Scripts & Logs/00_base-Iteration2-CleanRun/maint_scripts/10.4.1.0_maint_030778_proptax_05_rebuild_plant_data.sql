/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030778_proptax_05_rebuild_plant_data.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/27/2013 Lee Quinn      Point release
||============================================================================
*/

----------------------------------------------------
-- FIXED VALUES FOR POWERPLANT TABLES
----------------------------------------------------

-- insert property tax type and cwip property tax type class codes
insert into PWRPLANT.CLASS_CODE
   (CLASS_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, CWIP_INDICATOR, TAX_INDICATOR, CPR_INDICATOR,
    BUDGET_INDICATOR, LONG_DESCRIPTION, CLASS_CODE_TYPE, CLASS_CODE_CATEGORY, REQUIRED, FP_INDICATOR,
    INHERIT_FROM_FP)
   select MAX_ID + 1,
          sysdate,
          user,
          'Property Tax Type',
          0,
          0,
          1,
          0,
          'Stores the Property Tax Type for property tax processing.',
          'freeform',
          null,
          0,
          0,
          1
     from DUAL, (select NVL(max(CLASS_CODE_ID), 0) MAX_ID from PWRPLANT.CLASS_CODE)
    where not exists
    (select 1 from PWRPLANT.CLASS_CODE where UPPER(trim(DESCRIPTION)) = 'PROPERTY TAX TYPE');

insert into PWRPLANT.CLASS_CODE
   (CLASS_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, CWIP_INDICATOR, TAX_INDICATOR, CPR_INDICATOR,
    BUDGET_INDICATOR, LONG_DESCRIPTION, CLASS_CODE_TYPE, CLASS_CODE_CATEGORY, REQUIRED, FP_INDICATOR,
    INHERIT_FROM_FP)
   select MAX_ID + 1,
          sysdate,
          user,
          'CWIP Property Tax Type',
          1,
          0,
          0,
          0,
          'Stores the CWIP Property Tax Type for property tax processing.',
          'freeform',
          null,
          0,
          1,
          1
     from DUAL, (select NVL(max(CLASS_CODE_ID), 0) MAX_ID from PWRPLANT.CLASS_CODE)
    where not exists (select 1
             from PWRPLANT.CLASS_CODE
            where UPPER(trim(DESCRIPTION)) = 'CWIP PROPERTY TAX TYPE');

insert into PWRPLANT.CLASS_CODE
   (CLASS_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, CWIP_INDICATOR, TAX_INDICATOR, CPR_INDICATOR,
    BUDGET_INDICATOR, LONG_DESCRIPTION, CLASS_CODE_TYPE, CLASS_CODE_CATEGORY, REQUIRED, FP_INDICATOR,
    INHERIT_FROM_FP)
   select MAX_ID + 1,
          sysdate,
          user,
          'RWIP Property Tax Type',
          1,
          0,
          0,
          0,
          'Stores the RWIP Property Tax Type for property tax processing.',
          'freeform',
          null,
          0,
          1,
          1
     from DUAL, (select NVL(max(CLASS_CODE_ID), 0) MAX_ID from PWRPLANT.CLASS_CODE)
    where not exists (select 1
             from PWRPLANT.CLASS_CODE
            where UPPER(trim(DESCRIPTION)) = 'RWIP PROPERTY TAX TYPE');

-- insert property tax accrual value into standard_journal_entries
insert into PWRPLANT.STANDARD_JOURNAL_ENTRIES
   (JE_ID, TIME_STAMP, USER_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select 500000,
          sysdate,
          user,
          'PROPTAXACCRUAL',
          'PROPTAXACCRUAL',
          'PowerPlant Property Tax Accrual',
          'PowerPlant Property Tax Accrual'
     from DUAL
    where not exists (select 1 from PWRPLANT.STANDARD_JOURNAL_ENTRIES where UPPER(JE_ID) = 500000);

--insert property tax accrual value into gl_je_control
insert into PWRPLANT.GL_JE_CONTROL
   (PROCESS_ID, TIME_STAMP, USER_ID, JE_ID, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN)
   select 'Property Tax Accrual', sysdate, user, 500000, 'None', 'None', 'None', 'None'
     from DUAL
    where not exists (select 1
             from PWRPLANT.GL_JE_CONTROL
            where UPPER(trim(PROCESS_ID)) = 'PROPERTY TAX ACCRUAL');

-- insert property tax account into account type
insert into PWRPLANT.ACCOUNT_TYPE
   (ACCOUNT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION)
   select 50, sysdate, user, 'Property Tax'
     from DUAL
    where not exists (select 1 from PWRPLANT.ACCOUNT_TYPE where ACCOUNT_TYPE_ID = 50);

-- insert assessment year time options in pp_reports_time_option
insert into PWRPLANT.PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, TIME_STAMP, USER_ID, DESCRIPTION)
   select 33, sysdate, user, 'PT - Single Asmt Yr - None'
     from DUAL
    where not exists
    (select 1 from PWRPLANT.PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 33)
   union
   select 34, sysdate, user, 'PT - Single Asmt Yr - Single Month'
     from DUAL
    where not exists
    (select 1 from PWRPLANT.PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 34)
   union
   select 35, sysdate, user, 'PT - Single Asmt Yr - Span'
     from DUAL
    where not exists
    (select 1 from PWRPLANT.PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 35);



----------------------------------------------------
-- PP SYSTEM CONTROL
----------------------------------------------------

insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Allow Paid Bill Modify',
          'No',
          sysdate,
          user,
          'YES;NO;ESTIMATED',
          'Default is "NO".  This switch controls whether or not partially paid bills may be modified.  "YES" means any partially paid bill may be modified.  "NO" means no partially paid bill may be modified.  "ESTIMATED" means only estimated bills may be modified.',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Allow Paid Bill Modify');
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Allow Payment Changes',
          'No',
          sysdate,
          user,
          'dw_yes_no;1',
          'Default is "NO".  This will allow payments to be changed after they have been made using the ''Payment Maintenance'' window.',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Allow Payment Changes');
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Bill Calculation Method',
          'ON BILL BY LINE',
          sysdate,
          user,
          'ON BILL BY LINE;ON BILL BY AUTHORITY;FROM CASE',
          'Default is "ON BILL BY LINE".  This switch controls the way taxes are calculated on the bill modify and verify windows.  "ON BILL BY LINE" calculates tax by multiplying the assessment on each line by the current levy rate.  "ON BILL BY AUTHORITY" calculates tax by multiplying the total assessment for an authority by the levy rate.  "FROM CASE" copies the liability from the current accrual case.',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Bill Calculation Method');
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Default Interest Acct',
          'No',
          sysdate,
          user,
          'dw_yes_no;1',
          'If the interest_gl_account_id field is null on the pt_accrual_type table, then a control value of Yes indicates that the accrue_credit_gl_account_id will be used for any interest dollars.',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Default Interest Acct');
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Default Penalty Acct',
          'No',
          sysdate,
          user,
          'dw_yes_no;1',
          'If the penalty_gl_account_id field is null on the pt_accrual_type table, then a control value of Yes indicates that the accrue_credit_gl_account_id will be used for any penalty dollars.',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Default Penalty Acct');
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Default Protest Acct',
          'No',
          sysdate,
          user,
          'dw_yes_no;1',
          'If the protest_gl_account_id field is null on the pt_accrual_type table, then a control value of Yes indicates that the accrue_credit_gl_account_id will be used for any protest dollars.',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Default Protest Acct');
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Interfaced Payments DW',
          'not in use',
          sysdate,
          user,
          null,
          'PropTax - Custom Datawindow - Interfaced Payments',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Interfaced Payments DW');
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Unique Ledger Items',
          'Yes',
          sysdate,
          user,
          'dw_yes_no;1',
          '"Yes" means add likes in the property tax ledger will force uniqueness (i.e., an identical record may not be entered twice).  "No" means two identical records can be entered into the property tax ledger.',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Unique Ledger Items');
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    COMPANY_ID)
   select MAX_ID + 1,
          'PropTax - Use Company GL Comp No',
          'Yes',
          sysdate,
          user,
          'dw_yes_no;1',
          'This control indicates whether or not you want to use the Company GL Company No on accruals.  If you select Yes, the system will use the GL Company No on the Company table.  If you select No, the system will use the GL Company No on the PT Company table.',
          -1
     from DUAL, (select NVL(max(CONTROL_ID), 0) MAX_ID from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
            where CONTROL_NAME = 'PropTax - Use Company GL Comp No');

----------------------------------------------------
-- REPORT TYPES AND FILTERS
----------------------------------------------------

-- get rid of report type 40
delete from PP_REPORT_TYPE where REPORT_TYPE_ID = 40;

-- update the report type description for main property tax reports
update PP_REPORT_TYPE set DESCRIPTION = 'Property Tax - Standard Reports' where REPORT_TYPE_ID = 9;

-- create the new property tax report types we'll need

insert into PWRPLANT.PP_REPORT_TYPE
   (REPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION)
   select 200, sysdate, user, 'Property Tax - Returns'
     from DUAL
    where not exists (select 1 from PWRPLANT.PP_REPORT_TYPE where REPORT_TYPE_ID = 200);
insert into PWRPLANT.PP_REPORT_TYPE
   (REPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION)
   select 201, sysdate, user, 'Property Tax - Electronic Filings'
     from DUAL
    where not exists (select 1 from PWRPLANT.PP_REPORT_TYPE where REPORT_TYPE_ID = 201);

-- update the descriptions and table names of the existing property tax filters
update PP_REPORTS_FILTER
   set DESCRIPTION = DECODE(PP_REPORT_FILTER_ID,
                             7,
                             'Property Tax - Ledger',
                             8,
                             'Property Tax - Bills',
                             9,
                             'Property Tax - Parcels'),
       TABLE_NAME = DECODE(PP_REPORT_FILTER_ID,
                            7,
                            'pt_ledger',
                            8,
                            'pt_statement_statement_year',
                            9,
                            'pt_parcel')
 where PP_REPORT_FILTER_ID in (7, 8, 9);

-- add new property tax filters
insert into PWRPLANT.PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME)
   select 15, sysdate, user, 'Property Tax - Payments', 'pt_statement_group_payment'
     from DUAL
    where not exists (select 1 from PWRPLANT.PP_REPORTS_FILTER where PP_REPORT_FILTER_ID = 15);
insert into PWRPLANT.PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME)
   select 16, sysdate, user, 'Property Tax - Preallo Ledger', 'pt_preallo_ledger'
     from DUAL
    where not exists (select 1 from PWRPLANT.PP_REPORTS_FILTER where PP_REPORT_FILTER_ID = 16);
insert into PWRPLANT.PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME)
   select 20, sysdate, user, 'Property Tax - Statistics (Full)', 'pt_statistics_full'
     from DUAL
    where not exists (select 1 from PWRPLANT.PP_REPORTS_FILTER where PP_REPORT_FILTER_ID = 20);
insert into PWRPLANT.PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME)
   select 21, sysdate, user, 'Property Tax - Statistics (Incr.)', 'pt_statistics_incremental'
     from DUAL
    where not exists (select 1 from PWRPLANT.PP_REPORTS_FILTER where PP_REPORT_FILTER_ID = 21);

----------------------------------------------------
-- COLUMNS FOR POWERPLANT TABLES
----------------------------------------------------

-- insert the columns for powerplant tables into powerplant_columns
insert into PWRPLANT.POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION)
   select 'prop_tax_company_id',
          'company_setup',
          'pt_prop_tax_company_filter',
          'p',
          null,
          'Prop Tax Company',
          'Prop Tax Company',
          80,
          'Prop Tax Company'
     from DUAL
    where not exists (select 1
             from PWRPLANT.POWERPLANT_COLUMNS
            where UPPER(trim(TABLE_NAME)) = 'COMPANY_SETUP'
              and UPPER(trim(COLUMN_NAME)) = 'PROP_TAX_COMPANY_ID');

insert into PWRPLANT.POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK)
   select 'is_proptax', 'approval', 'yes_no', 'p', 'Is Property Tax', 99
     from DUAL
    where not exists (select 1
             from PWRPLANT.POWERPLANT_COLUMNS
            where UPPER(trim(TABLE_NAME)) = 'APPROVAL'
              and UPPER(trim(COLUMN_NAME)) = 'IS_PROPTAX');

-- Make sure the dropdown is used on book_summary
update PWRPLANT.POWERPLANT_COLUMNS
   set DROPDOWN_NAME = 'pt_adjust_filter', PP_EDIT_TYPE_ID = 'p'
 where LOWER(trim(TABLE_NAME)) = 'book_summary'
   and LOWER(trim(COLUMN_NAME)) = 'property_tax_adjust_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (541, 0, 10, 4, 1, 0, 30778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030778_proptax_05_rebuild_plant_data.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
