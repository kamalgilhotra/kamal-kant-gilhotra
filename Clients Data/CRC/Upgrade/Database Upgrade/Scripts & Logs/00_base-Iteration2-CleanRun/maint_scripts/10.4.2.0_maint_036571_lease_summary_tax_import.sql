/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036571_lease_summary_tax_import.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/26/2014 Kyle Peterson
||============================================================================
*/

alter table LS_IMPORT_LEASE
   add (TAX_SUMMARY_XLATE            varchar2(254),
        TAX_SUMMARY_ID               number(22,0),
        TAX_RATE_OPTION_XLATE        varchar(254),
        TAX_RATE_OPTION_ID           number(22,0),
        AUTO_GENERATE_INVOICES_XLATE varchar2(254),
        AUTO_GENERATE_INVOICES       number(22,0));

alter table LS_IMPORT_LEASE_ARCHIVE
   add (TAX_SUMMARY_XLATE            varchar2(254),
        TAX_SUMMARY_ID               number(22,0),
        TAX_RATE_OPTION_XLATE        varchar(254),
        TAX_RATE_OPTION_ID           number(22,0),
        AUTO_GENERATE_INVOICES_XLATE varchar2(254),
        AUTO_GENERATE_INVOICES       number(22,0));

comment on column LS_IMPORT_LEASE.TAX_SUMMARY_XLATE is 'Translation field for determining Tax Summary';
comment on column LS_IMPORT_LEASE.TAX_SUMMARY_ID is 'The Tax Summary for the MLA.';
comment on column LS_IMPORT_LEASE.TAX_RATE_OPTION_XLATE is 'Translation field for determining Tax Rate Option';
comment on column LS_IMPORT_LEASE.TAX_RATE_OPTION_ID is 'The Tax Rate Option for the MLA.';
comment on column LS_IMPORT_LEASE.AUTO_GENERATE_INVOICES_XLATE is 'Translation field for determining Auto Generate Invoices';
comment on column LS_IMPORT_LEASE.AUTO_GENERATE_INVOICES is 'Whether or not to auto generate invoices for the MLA.';

comment on column LS_IMPORT_LEASE_ARCHIVE.TAX_SUMMARY_XLATE is 'Translation field for determining Tax Summary';
comment on column LS_IMPORT_LEASE_ARCHIVE.TAX_SUMMARY_ID is 'The Tax Summary for the MLA.';
comment on column LS_IMPORT_LEASE_ARCHIVE.TAX_RATE_OPTION_XLATE is 'Translation field for determining Tax Rate Option';
comment on column LS_IMPORT_LEASE_ARCHIVE.TAX_RATE_OPTION_ID is 'The Tax Rate Option for the MLA.';
comment on column LS_IMPORT_LEASE_ARCHIVE.AUTO_GENERATE_INVOICES_XLATE is 'Translation field for determining Auto Generate Invoices';
comment on column LS_IMPORT_LEASE_ARCHIVE.AUTO_GENERATE_INVOICES is 'Whether or not to auto generate invoices for the MLA.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'tax_summary_id',
           'Tax Summary',
           'tax_summary_xlate',
           0,
           1,
           'number(22,0)',
           'ls_tax_summary',
           1,
           'tax_summary_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: MLA%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'tax_rate_option_id',
           'Tax Rate Option',
           'tax_rate_option_xlate',
           0,
           1,
           'number(22,0)',
           'LS_TAX_RATE_OPTION',
           1,
           'tax_rate_option_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: MLA%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'auto_generate_invoices',
           'Automatically Generate Invoices?',
           'auto_generate_invoices_xlate',
           0,
           1,
           'number(22,0)',
           'YES_NO',
           1,
           'yes_no_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: MLA%');

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   (select 2501,
           'LS Tax Summary.Description',
           'The passed in value corresponds to the LS Tax Summary: Description field.  Translate to the Tax Summary ID using the Description column on the LS Tax Summary table.',
           'tax_summary_id',
           '( select t.tax_summary_id from ls_tax_summary t where upper( trim( <importfield> ) ) = upper( trim( t.description ) ) )',
           0,
           'ls_tax_summary',
           'description'
      from DUAL);

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'tax_summary_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: MLA%'
    and (PPIL.DESCRIPTION like 'LS Tax Summary.Description'));

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   (select 2502,
           'LS Tax Rate Option.Description',
           'The passed in value corresponds to the LS Tax Rate Option: Description field.  Translate to the Tax Rate Option ID using the Description column on the LS Tax Rate Option table.',
           'tax_rate_option_id',
           '( select t.tax_rate_option_id from ls_tax_rate_option t where upper( trim( <importfield> ) ) = upper( trim( t.description ) ) )',
           0,
           'ls_tax_rate_option',
           'description'
      from DUAL);

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'tax_rate_option_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: MLA%'
       and (PPIL.DESCRIPTION like 'LS Tax Rate Option.Description'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'auto_generate_invoices', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: MLA%'
       and (PPIL.DESCRIPTION like 'Yes No.Description (Yes or No)'));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (992, 0, 10, 4, 2, 0, 36571, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036571_lease_summary_tax_import.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;