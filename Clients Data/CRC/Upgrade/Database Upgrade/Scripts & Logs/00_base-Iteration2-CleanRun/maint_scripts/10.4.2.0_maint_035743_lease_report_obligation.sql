/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035743_lease_report_obligation.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   02/07/2014 Ryan Oliveria  Changing report filter to fit new sql
||============================================================================
*/

update PP_REPORTS set PP_REPORT_FILTER_ID = 42 where DATAWINDOW = 'dw_ls_rpt_short_long_term_ob';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (950, 0, 10, 4, 2, 0, 35743, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035743_lease_report_obligation.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;