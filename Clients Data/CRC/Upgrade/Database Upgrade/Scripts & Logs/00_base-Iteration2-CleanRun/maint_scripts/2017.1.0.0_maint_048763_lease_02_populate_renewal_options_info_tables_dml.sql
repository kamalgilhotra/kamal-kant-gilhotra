/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048763_lease_02_populate_renewal_options_info_tables_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/14/2017 Andrew Hill      Populate default data in renewal options tables
||============================================================================
*/

INSERT INTO ls_ilr_renewal_probability(ilr_renewal_probability_id, DESCRIPTION)
SELECT 1, 'Likely'
from dual
UNION
SELECT 2, 'Unlikely'
FROM dual
UNION
SELECT 3, 'None'
from dual;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3710, 0, 2017, 1, 0, 0, 48763, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048763_lease_02_populate_renewal_options_info_tables_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;