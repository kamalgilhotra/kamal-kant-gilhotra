/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043000_pcm_other_workspace.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/18/2015 Mardis           Install the "Other" workspace
||============================================================================
*/

delete from ppbase_workspace_links where workspace_identifier in ('fp_maint_other','wo_maint_other');
delete from ppbase_menu_items where workspace_identifier in ('fp_maint_other','wo_maint_other');
delete from ppbase_workspace where workspace_identifier in ('fp_maint_other','wo_maint_other');

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('pcm', 'fp_maint_other', 'Other (FP)', 'uo_pcm_maint_wksp_other', 'Other (FP)', 1);

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('pcm', 'wo_maint_other', 'Other (WO)', 'uo_pcm_maint_wksp_other', 'Other (WO)', 1);

insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('pcm', 'fp_maint_other', 4, 8, 'Other', 'Funding Project Other', 'fp_maintain', 'fp_maint_other', 1);

insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('pcm', 'wo_maint_other', 4, 8, 'Other', 'Work Order Other', 'wo_maintain', 'wo_maint_other', 1);

insert into ppbase_workspace_links (module, workspace_identifier, link_order, linked_workspace_identifier, linked_window, linked_window_label, linked_window_opensheet)
select module, replace(workspace_identifier,'_info','_other'), link_order, linked_workspace_identifier, linked_window, linked_window_label, linked_window_opensheet
from ppbase_workspace_links
where module = 'pcm'
and workspace_identifier in ('fp_maint_info','wo_maint_info');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2308, 0, 2015, 1, 0, 0, 043000, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043000_pcm_other_workspace_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;