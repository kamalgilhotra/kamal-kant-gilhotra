/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035220_aro_je_trans_types.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/09/2014 Ryan Oliveria
||============================================================================
*/

update JE_TRANS_TYPE set DESCRIPTION = '39 - Depr Expense Debit JE by Asset' where TRANS_TYPE = 39;

update JE_TRANS_TYPE
   set DESCRIPTION = '40 - Depr Expense Credit JE by Asset'
 where TRANS_TYPE = 40;

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (41, '41 - Regulatory ARO DR');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (42, '42 - Regulatory ARO CR');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (849, 0, 10, 4, 2, 0, 35220, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035220_aro_je_trans_types.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
