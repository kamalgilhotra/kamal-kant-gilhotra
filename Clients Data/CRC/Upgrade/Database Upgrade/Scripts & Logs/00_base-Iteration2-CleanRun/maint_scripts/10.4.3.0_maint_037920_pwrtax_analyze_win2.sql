/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_037920_pwrtax_analyze_win2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 06/16/2014 Andrew Scott        QA of audit window encountered some errors
||                                           around sub tax class being removed.
||============================================================================
*/

delete from PP_TABLE_AUDITS_PK_LOOKUP where LOWER(TABLE_NAME) = 'sub_tax_class';

delete from POWERPLANT_DDDW where LOWER(TABLE_NAME) = 'sub_tax_class';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1214, 0, 10, 4, 3, 0, 37920, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037920_pwrtax_analyze_win2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;