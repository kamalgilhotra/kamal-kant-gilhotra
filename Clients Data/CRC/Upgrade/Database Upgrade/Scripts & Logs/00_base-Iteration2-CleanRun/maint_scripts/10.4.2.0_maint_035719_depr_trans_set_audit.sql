/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035719_depr_trans_set_audit.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/17/2014 Ryan Oliveria  Need to audit changes on depr trans sets
||============================================================================
*/

CREATE OR REPLACE TRIGGER
DEPR_TRANS_SET_AUDIT AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF COMPANY_ID, DEPR_TRANS_ALLO_METHOD_ID, DEPR_TRANS_SET_ID, DEPR_TRANS_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, USED_IN_ARO_CALC
 ON DEPR_TRANS_SET FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING THEN
   IF :old.COMPANY_ID <> :new.COMPANY_ID OR
     (:old.COMPANY_ID is null AND :new.COMPANY_ID is not null) OR
     (:new.COMPANY_ID is null AND :old.COMPANY_ID is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'COMPANY_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.COMPANY_ID, :new.COMPANY_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.DEPR_TRANS_ALLO_METHOD_ID <> :new.DEPR_TRANS_ALLO_METHOD_ID OR
     (:old.DEPR_TRANS_ALLO_METHOD_ID is null AND :new.DEPR_TRANS_ALLO_METHOD_ID is not null) OR
     (:new.DEPR_TRANS_ALLO_METHOD_ID is null AND :old.DEPR_TRANS_ALLO_METHOD_ID is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_ALLO_METHOD_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.DEPR_TRANS_ALLO_METHOD_ID, :new.DEPR_TRANS_ALLO_METHOD_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.DEPR_TRANS_SET_ID <> :new.DEPR_TRANS_SET_ID OR
     (:old.DEPR_TRANS_SET_ID is null AND :new.DEPR_TRANS_SET_ID is not null) OR
     (:new.DEPR_TRANS_SET_ID is null AND :old.DEPR_TRANS_SET_ID is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_SET_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.DEPR_TRANS_SET_ID, :new.DEPR_TRANS_SET_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.DEPR_TRANS_TYPE_ID <> :new.DEPR_TRANS_TYPE_ID OR
     (:old.DEPR_TRANS_TYPE_ID is null AND :new.DEPR_TRANS_TYPE_ID is not null) OR
     (:new.DEPR_TRANS_TYPE_ID is null AND :old.DEPR_TRANS_TYPE_ID is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_TYPE_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.DEPR_TRANS_TYPE_ID, :new.DEPR_TRANS_TYPE_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.DESCRIPTION <> :new.DESCRIPTION OR
     (:old.DESCRIPTION is null AND :new.DESCRIPTION is not null) OR
     (:new.DESCRIPTION is null AND :old.DESCRIPTION is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DESCRIPTION',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.DESCRIPTION, :new.DESCRIPTION,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.LONG_DESCRIPTION <> :new.LONG_DESCRIPTION OR
     (:old.LONG_DESCRIPTION is null AND :new.LONG_DESCRIPTION is not null) OR
     (:new.LONG_DESCRIPTION is null AND :old.LONG_DESCRIPTION is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'LONG_DESCRIPTION',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.LONG_DESCRIPTION, :new.LONG_DESCRIPTION,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.STATUS_CODE_ID <> :new.STATUS_CODE_ID OR
     (:old.STATUS_CODE_ID is null AND :new.STATUS_CODE_ID is not null) OR
     (:new.STATUS_CODE_ID is null AND :old.STATUS_CODE_ID is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'STATUS_CODE_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.STATUS_CODE_ID, :new.STATUS_CODE_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.USED_IN_ARO_CALC <> :new.USED_IN_ARO_CALC OR
     (:old.USED_IN_ARO_CALC is null AND :new.USED_IN_ARO_CALC is not null) OR
     (:new.USED_IN_ARO_CALC is null AND :old.USED_IN_ARO_CALC is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'USED_IN_ARO_CALC',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.USED_IN_ARO_CALC, :new.USED_IN_ARO_CALC,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
END IF;
IF INSERTING THEN
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'COMPANY_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :new.COMPANY_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_ALLO_METHOD_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :new.DEPR_TRANS_ALLO_METHOD_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_SET_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :new.DEPR_TRANS_SET_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_TYPE_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :new.DEPR_TRANS_TYPE_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DESCRIPTION',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :new.DESCRIPTION,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'LONG_DESCRIPTION',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :new.LONG_DESCRIPTION,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'STATUS_CODE_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :new.STATUS_CODE_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'USED_IN_ARO_CALC',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; ', pk_lookup, :new.USED_IN_ARO_CALC,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
IF DELETING THEN
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'COMPANY_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.COMPANY_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_ALLO_METHOD_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.DEPR_TRANS_ALLO_METHOD_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_SET_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.DEPR_TRANS_SET_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DEPR_TRANS_TYPE_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.DEPR_TRANS_TYPE_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'DESCRIPTION',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.DESCRIPTION,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'LONG_DESCRIPTION',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.LONG_DESCRIPTION,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'STATUS_CODE_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.STATUS_CODE_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET', 'USED_IN_ARO_CALC',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; ', pk_lookup, :old.USED_IN_ARO_CALC,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
END;
/

DELETE FROM PP_TABLE_AUDITS WHERE TABLE_NAME = 'DEPR_TRANS_SET';
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET', 'COMPANY_ID', 'PWRPLANT', to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_AUDIT', 'IUD', 'pp_company_filter', 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET', 'DEPR_TRANS_ALLO_METHOD_ID', 'PWRPLANT', to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_AUDIT', 'IUD', null, 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET', 'DEPR_TRANS_SET_ID', 'PWRPLANT', to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_AUDIT', 'IUD', null, 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET', 'DEPR_TRANS_TYPE_ID', 'PWRPLANT', to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_AUDIT', 'IUD', null, 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET', 'DESCRIPTION', 'PWRPLANT', to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_AUDIT', 'IUD', null, 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET', 'LONG_DESCRIPTION', 'PWRPLANT', to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_AUDIT', 'IUD', null, 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET', 'STATUS_CODE_ID', 'PWRPLANT', to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_AUDIT', 'IUD', 'status_code', 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET', 'USED_IN_ARO_CALC', 'PWRPLANT', to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_AUDIT', 'IUD', null, 'DEPR', null, 0) ;
DELETE FROM PP_TABLE_AUDITS_OBJ_ACTIONS WHERE TRIGGER_NAME = 'DEPR_TRANS_SET_AUDIT';
DELETE FROM PP_TABLE_AUDITS_PK_LOOKUP WHERE TABLE_NAME = 'DEPR_TRANS_SET';
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('DEPR_TRANS_SET', 'DEPR_TRANS_SET_ID', null, null, null, to_date('2014-01-17 15:21:55', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
COMMIT;


CREATE OR REPLACE TRIGGER
DEPR_TRANS_SET_DG_AUDIT AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF DEPR_GROUP_ID, DEPR_TRANS_SET_ID, FACTOR
 ON DEPR_TRANS_SET_DG FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING THEN
   IF :old.DEPR_GROUP_ID <> :new.DEPR_GROUP_ID OR
     (:old.DEPR_GROUP_ID is null AND :new.DEPR_GROUP_ID is not null) OR
     (:new.DEPR_GROUP_ID is null AND :old.DEPR_GROUP_ID is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'DEPR_GROUP_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :new.DEPR_GROUP_ID||'; ', pk_lookup, :old.DEPR_GROUP_ID, :new.DEPR_GROUP_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.DEPR_TRANS_SET_ID <> :new.DEPR_TRANS_SET_ID OR
     (:old.DEPR_TRANS_SET_ID is null AND :new.DEPR_TRANS_SET_ID is not null) OR
     (:new.DEPR_TRANS_SET_ID is null AND :old.DEPR_TRANS_SET_ID is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'DEPR_TRANS_SET_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :new.DEPR_GROUP_ID||'; ', pk_lookup, :old.DEPR_TRANS_SET_ID, :new.DEPR_TRANS_SET_ID,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
   IF :old.FACTOR <> :new.FACTOR OR
     (:old.FACTOR is null AND :new.FACTOR is not null) OR
     (:new.FACTOR is null AND :old.FACTOR is not null) THEN


      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'FACTOR',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :new.DEPR_GROUP_ID||'; ', pk_lookup, :old.FACTOR, :new.FACTOR,
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
   END IF;
END IF;
IF INSERTING THEN
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'DEPR_GROUP_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :new.DEPR_GROUP_ID||'; ', pk_lookup, :new.DEPR_GROUP_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'DEPR_TRANS_SET_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :new.DEPR_GROUP_ID||'; ', pk_lookup, :new.DEPR_TRANS_SET_ID,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'FACTOR',
        ''|| 'DEPR_TRANS_SET_ID=' || :new.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :new.DEPR_GROUP_ID||'; ', pk_lookup, :new.FACTOR,
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
IF DELETING THEN
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'DEPR_GROUP_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :old.DEPR_GROUP_ID||'; ', pk_lookup, :old.DEPR_GROUP_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'DEPR_TRANS_SET_ID',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :old.DEPR_GROUP_ID||'; ', pk_lookup, :old.DEPR_TRANS_SET_ID,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
      INSERT INTO PP_AUDITS_DEPR_TRAIL
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP,
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'DEPR_TRANS_SET_DG', 'FACTOR',
        ''|| 'DEPR_TRANS_SET_ID=' || :old.DEPR_TRANS_SET_ID||'; '|| 'DEPR_GROUP_ID=' || :old.DEPR_GROUP_ID||'; ', pk_lookup, :old.FACTOR,
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans);
END IF;
END;
/

DELETE FROM PP_TABLE_AUDITS WHERE TABLE_NAME = 'DEPR_TRANS_SET_DG';
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET_DG', 'DEPR_GROUP_ID', 'PWRPLANT', to_date('2014-01-17 15:26:32', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_DG_AUDIT', 'IUD', 'pt_depr_group_filter', 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET_DG', 'DEPR_TRANS_SET_ID', 'PWRPLANT', to_date('2014-01-17 15:26:32', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_DG_AUDIT', 'IUD', null, 'DEPR', null, 0) ;
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('DEPR_TRANS_SET_DG', 'FACTOR', 'PWRPLANT', to_date('2014-01-17 15:26:32', 'yyyy-mm-dd hh24:mi:ss'), 'DEPR_TRANS_SET_DG_AUDIT', 'IUD', null, 'DEPR', null, 0) ;
DELETE FROM PP_TABLE_AUDITS_OBJ_ACTIONS WHERE TRIGGER_NAME = 'DEPR_TRANS_SET_DG_AUDIT';
DELETE FROM PP_TABLE_AUDITS_PK_LOOKUP WHERE TABLE_NAME = 'DEPR_TRANS_SET_DG';
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('DEPR_TRANS_SET_DG', 'DEPR_GROUP_ID', 'depr_group', 'description', 'depr_group_id', to_date('2014-01-17 15:26:32', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('DEPR_TRANS_SET_DG', 'DEPR_TRANS_SET_ID', null, null, null, to_date('2014-01-17 15:26:32', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
COMMIT;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (879, 0, 10, 4, 2, 0, 35719, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035719_depr_trans_set_audit.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
