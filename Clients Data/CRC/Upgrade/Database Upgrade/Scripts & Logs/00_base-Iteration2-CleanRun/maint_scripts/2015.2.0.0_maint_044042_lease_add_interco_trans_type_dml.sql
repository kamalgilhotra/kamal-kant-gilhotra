/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044042_lease_add_interco_trans_type_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 06/29/2015 Will Davis        New trans types for lease reserve transfers 
||============================================================================
*/

insert into je_trans_type
(trans_type, description)
select
3049,
'3049 - Lease Intercompany Debit'
from dual
where not exists (select 1 from je_trans_type where trans_type = 3049);

update je_trans_type
set description = replace(description, 'Transfercompany', 'Intercompany')
where trans_type in (3043, 3044);

update je_trans_type
set description = replace(description, 'Addition', 'Addition/Transfer')
where trans_type in (3002, 3003);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2659, 0, 2015, 2, 0, 0, 044042, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044042_lease_add_interco_trans_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;