/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035218_aro_reg_acct.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/08/2014 Brandon Beck   Point Release
||============================================================================
*/

alter table ARO add REG_ARO_ACCT number(22,0);

alter table ARO
   add constraint ARO_REG_ARO_ACCT_FK
       foreign key (REG_ARO_ACCT)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
comment on column aro.reg_aro_acct is 'The regulatory aro asset account to use for the offset to the Cost of Removal.  This field is only relevant if the regulatory obligation field is flagged yes.';


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (839, 0, 10, 4, 2, 0, 35218, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035218_aro_reg_acct.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;