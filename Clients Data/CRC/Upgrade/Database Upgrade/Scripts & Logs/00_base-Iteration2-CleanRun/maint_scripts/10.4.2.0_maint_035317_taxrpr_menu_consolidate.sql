/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035317_taxrpr_menu_consolidate.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/08/2014 Alex P.        Point Release
||============================================================================
*/

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 100 + ROWNUM
 where MENU_IDENTIFIER in ('menu_wksp_work_order', 'menu_wksp_asset')
   and WORKSPACE_IDENTIFIER is null
   and MODULE = 'REPAIRS';

-- ******* Tax Units of Property workspace *******

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REPAIRS', 'menu_wksp_tax_units', null, null, 'Tax Units of Property',
    'uo_rpr_config_wksp_unit_code', 'Tax Units of Property');

update PPBASE_MENU_ITEMS
   set MENU_IDENTIFIER = 'menu_wksp_tax_units', MENU_LEVEL = 2, ITEM_ORDER = 5,
       LABEL = 'Tax Units of Property', PARENT_MENU_IDENTIFIER = 'menu_wksp_config',
       WORKSPACE_IDENTIFIER = 'menu_wksp_tax_units'
 where WORKSPACE_IDENTIFIER in ('menu_wksp_tax_units_work_order')
   and MODULE = 'REPAIRS';

delete PPBASE_MENU_ITEMS
 where WORKSPACE_IDENTIFIER in ('menu_wksp_tax_units_asset')
   and MODULE = 'REPAIRS';

delete PPBASE_WORKSPACE
 where WORKSPACE_IDENTIFIER in ('menu_wksp_tax_units_work_order', 'menu_wksp_tax_units_asset')
   and MODULE = 'REPAIRS';

-- ********* Thresholds workspace ************

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REPAIRS', 'menu_wksp_tax_thresholds', null, null, 'Tax Thresholds',
    'uo_rpr_config_wksp_threshold', 'Tax Thresholds');

update PPBASE_MENU_ITEMS
   set MENU_IDENTIFIER = 'menu_wksp_tax_thresholds', MENU_LEVEL = 2, ITEM_ORDER = 6,
       LABEL = 'Tax Thresholds', PARENT_MENU_IDENTIFIER = 'menu_wksp_config',
       WORKSPACE_IDENTIFIER = 'menu_wksp_tax_thresholds'
 where WORKSPACE_IDENTIFIER in ('menu_wksp_tax_thresholds_work_order')
   and MODULE = 'REPAIRS';

delete PPBASE_MENU_ITEMS
 where WORKSPACE_IDENTIFIER in ('menu_wksp_tax_thresholds_asset')
   and MODULE = 'REPAIRS';

delete PPBASE_WORKSPACE
 where WORKSPACE_IDENTIFIER in
       ('menu_wksp_tax_thresholds_work_order', 'menu_wksp_tax_thresholds_asset')
   and MODULE = 'REPAIRS';

-- ********** Shift down menu items **********

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 3
 where MENU_IDENTIFIER in ('menu_wksp_import', 'menu_wksp_system_options')
   and MODULE = 'REPAIRS';

update PPBASE_MENU_ITEMS
   set MENU_LEVEL = 2, ITEM_ORDER = 7, LABEL = 'Tax Status (Gen)',
       PARENT_MENU_IDENTIFIER = 'menu_wksp_config'
 where MENU_IDENTIFIER in ('menu_wksp_tax_status')
   and MODULE = 'REPAIRS';

update PPBASE_WORKSPACE
   set LABEL = 'Tax Status (Gen)'
 where WORKSPACE_IDENTIFIER in ('menu_wksp_tax_status')
   and MODULE = 'REPAIRS';

update PPBASE_MENU_ITEMS
   set MENU_LEVEL = 2, ITEM_ORDER = 8, LABEL = 'Range Test (Gen)',
       PARENT_MENU_IDENTIFIER = 'menu_wksp_config'
 where MENU_IDENTIFIER in ('menu_wksp_range_test')
   and MODULE = 'REPAIRS';

update PPBASE_WORKSPACE
   set LABEL = 'Range Test (Gen)'
 where WORKSPACE_IDENTIFIER in ('menu_wksp_range_test')
   and MODULE = 'REPAIRS';

update PPBASE_MENU_ITEMS
   set MENU_LEVEL = 2, ITEM_ORDER = 9, LABEL = 'Repair Loc Rollups (Alloc)',
       PARENT_MENU_IDENTIFIER = 'menu_wksp_config'
 where MENU_IDENTIFIER in ('menu_wksp_tax_loc_rollups')
   and MODULE = 'REPAIRS';

update PPBASE_WORKSPACE
   set LABEL = 'Repair Loc Rollups (Alloc)'
 where WORKSPACE_IDENTIFIER in ('menu_wksp_tax_loc_rollups')
   and MODULE = 'REPAIRS';

delete PPBASE_MENU_ITEMS
 where MENU_IDENTIFIER in ('menu_wksp_work_order', 'menu_wksp_asset')
   and WORKSPACE_IDENTIFIER is null
   and MODULE = 'REPAIRS';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (851, 0, 10, 4, 2, 0, 35317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035317_taxrpr_menu_consolidate.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;