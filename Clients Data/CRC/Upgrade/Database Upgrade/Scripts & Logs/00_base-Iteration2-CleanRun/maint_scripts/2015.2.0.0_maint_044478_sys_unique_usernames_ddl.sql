/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_044478_sys_unique_usernames_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/22/2015 Andrew Hill    Added constraint to ensure unique usernames
||============================================================================
*/
DECLARE
  num_duplicates NUMBER(22,0);
BEGIN
  SELECT NVL(Max(count(lower(users))),0) INTO num_duplicates FROM pp_security_users GROUP BY users HAVING (count(lower(users)) > 1);
  IF num_duplicates = 0
  THEN
    EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX pp_idx_uk_users ON pp_security_users (lower(users))';
  ELSE
    dbms_output.put_line('Error: Duplicate Usernames Exist. Please remove users with
                              duplicate usernames and rerun maint_044478_sys_unique_usernames_ddl.sql');
  END IF;
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2712, 0, 2015, 2, 0, 0, 044478, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044478_sys_unique_usernames_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;