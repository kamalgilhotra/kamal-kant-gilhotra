/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049352_lease_01_field_interco_credit_trans_type_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

insert into je_trans_type(trans_type, description)
select 3050, '3050 - Lease Intercompany Credit'
from dual;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4022, 0, 2017, 1, 0, 0, 49352, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049352_lease_01_field_interco_credit_trans_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;