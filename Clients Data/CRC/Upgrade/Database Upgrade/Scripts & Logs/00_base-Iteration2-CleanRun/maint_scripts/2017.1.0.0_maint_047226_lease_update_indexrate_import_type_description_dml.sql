/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047226_lease_update_indexrate_import_type_description_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/12/2017 Jared Watkins    change the title for the Index/Rate Component import type
||============================================================================
*/

UPDATE pp_import_type 
SET description = 'Add: Var Payment Index/Rate Amounts', 
    long_description = 'Add: Variable Payment Index/Rate Component Amounts' 
WHERE import_type_id = 263;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3495, 0, 2017, 1, 0, 0, 47226, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047226_lease_update_indexrate_import_type_description_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
