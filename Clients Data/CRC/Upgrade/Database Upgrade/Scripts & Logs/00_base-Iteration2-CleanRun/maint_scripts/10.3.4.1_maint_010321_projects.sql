/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010321_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.1   05/17/2012 Chris Mardis   Point Release
||============================================================================
*/

alter table WO_INTERFACE_STAGING
   add (USER_ID    varchar2(18),
        TIME_STAMP date);

alter table WO_INTERFACE_STAGING_ARC
   add (USER_ID    varchar2(18),
        TIME_STAMP date);

alter table WO_INTERFACE_STAGING       modify SEQ_ID number(22) default 0;
alter table JOB_TASK_INTERFACE_STAGING modify SEQ_ID number(22) default 0;

alter table WO_INTERFACE_UNIT     add SEQ_ID number(22) default 0;
alter table WO_INTERFACE_UNIT_ARC add SEQ_ID number(22);

alter table WO_INTERFACE_UNIT_ARC modify EXPENDITURE_TYPE_ID number(22) null;
alter table WO_INTERFACE_UNIT_ARC modify EST_CHG_TYPE_ID     number(22) null;
alter table WO_INTERFACE_UNIT_ARC modify HOURS               number(22,2) null;
alter table WO_INTERFACE_UNIT_ARC modify AMOUNT              number(22,2) null;
alter table WO_INTERFACE_UNIT_ARC modify WORK_ORDER_NUMBER   varchar2(35) not null;

alter table WO_INTERFACE_UNIT     add ROW_ID number(22);
alter table WO_INTERFACE_UNIT_ARC add ROW_ID number(22);

insert into POWERPLANT_DDDW
   (DROPDOWN_NAME, TABLE_NAME, CODE_COL, DISPLAY_COL)
values
   ('workflow_type', 'workflow_type', 'workflow_type_id', 'description');

insert into POWERPLANT_DDDW
   (DROPDOWN_NAME, TABLE_NAME, CODE_COL, DISPLAY_COL)
values
   ('wo_approval_grp', 'wo_approval_group', 'wo_approval_group_id', 'description');

insert into POWERPLANT_DDDW
   (DROPDOWN_NAME, TABLE_NAME, CODE_COL, DISPLAY_COL)
values
   ('wo_est_trans_type', 'wo_est_trans_type', 'wo_est_trans_type_id', 'description');

update WO_INTERFACE_STAGING set ROW_ID = PWRPLANT1.NEXTVAL where ROW_ID is null;

alter table WO_INTERFACE_STAGING
   add constraint WO_INT_STAGING_PK
       primary key (ROW_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger WO_INTERFACE_STAGING_PK
   before insert on WO_INTERFACE_STAGING
   for each row
declare
   L_NEW_SEQUENCE number;
begin
   select PWRPLANT1.NEXTVAL into L_NEW_SEQUENCE from DUAL;
   :NEW.ROW_ID := L_NEW_SEQUENCE;
end;
/

update JOB_TASK_INTERFACE_STAGING set ROW_ID = PWRPLANT1.NEXTVAL where ROW_ID is null;


alter table JOB_TASK_INTERFACE_STAGING
   add constraint JOB_TASK_INT_STAGING_PK
       primary key (ROW_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger JOB_TASK_INTERFACE_UNIT_PK
   before insert on JOB_TASK_INTERFACE_STAGING
   for each row
declare
   L_NEW_SEQUENCE number;
begin
   select PWRPLANT1.NEXTVAL into L_NEW_SEQUENCE from DUAL;
   :NEW.ROW_ID := L_NEW_SEQUENCE;
end;
/

update WO_INTERFACE_UNIT set ROW_ID = PWRPLANT1.NEXTVAL where ROW_ID is null;

alter table WO_INTERFACE_UNIT
   add constraint WO_INTERFACE_UNIT_PK
       primary key (ROW_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger WO_INTERFACE_UNIT_PK
   before insert on WO_INTERFACE_UNIT
   for each row
declare
   L_NEW_SEQUENCE number;
begin
   select PWRPLANT1.NEXTVAL into L_NEW_SEQUENCE from DUAL;
   :NEW.ROW_ID := L_NEW_SEQUENCE;
end;
/

create table PP_INTEGRATION_COMPONENTS
(
 component varchar2(100),
 ARGUMENT1_TITLE  varchar2(1000),
 ARGUMENT2_TITLE  varchar2(1000),
 ARGUMENT3_TITLE  varchar2(1000),
 ARGUMENT4_TITLE  varchar2(1000),
 ARGUMENT5_TITLE  varchar2(1000),
 ARGUMENT6_TITLE  varchar2(1000),
 ARGUMENT7_TITLE  varchar2(1000),
 ARGUMENT8_TITLE  varchar2(1000),
 ARGUMENT9_TITLE  varchar2(1000),
 ARGUMENT10_TITLE varchar2(1000)
);

alter table PP_INTEGRATION_COMPONENTS
   add constraint PP_INTEGRATION_COMPONENTS_PK
       primary key (COMPONENT)
       using index tablespace PWRPLANT_IDX;

alter table PP_INTEGRATION_COMPONENTS
   add (USER_ID    varchar2(18),
        TIME_STAMP date);

alter table PP_INTEGRATION add DESCRIPTION varchar2(100);

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('wo_interface_staging', TO_DATE('2012-05-07 09:14:55', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    's', 'Wo Interface Staging', 'always', null, 'work,budget', null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_work_order_group', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:50:48', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext work order group', 430, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_work_order_type', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:27:35', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext work order type', 90, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_wo_number', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:49:32', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'external wo number', 20, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('financial_analysis', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:45:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'financial analysis', 400, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('funding_proj_number', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:28:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'funding proj number', 100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('funding_wo_indicator', 'wo_interface_staging',
    TO_DATE('2012-05-08 10:52:24', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'yes_no', 'p', null,
    'funding wo indicator', 190, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('future_projects', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:45:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'future projects', 380, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('in_service_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:40:12', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'in service date', 240, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('initiation_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:44:18', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'initiation date', 300, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('initiator', 'wo_interface_staging', TO_DATE('2012-05-07 10:25:14', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'pp_security_users', 'p', null, 'initiator', 290, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:49:32', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'long description', 40, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('major_location_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:23:35', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'major_location', 'p', null,
    'major location id', 130, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('notes', 'wo_interface_staging', TO_DATE('2012-05-07 10:49:32', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'notes', 45, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('other_contact', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:25:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'other contact', 350, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('out_of_service_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:40:12', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'out of service date', 260, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('plant_accountant', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:25:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'plant accountant', 320, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('project_manager', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:25:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'project manager', 330, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('reason_cd_id', 'wo_interface_staging', TO_DATE('2012-05-07 10:26:23', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'reason_code', 'p', null, 'reason cd id', 410, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('reason_for_work', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:45:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'reason for work', 360, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('reimbursable_type_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:34:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'reimb_type', 'p', null,
    'reimbursable type id', 460, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('row_id', 'wo_interface_staging', TO_DATE('2012-05-07 09:24:27', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 's', null, 'row id', 0, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('seq_id', 'wo_interface_staging', TO_DATE('2012-05-07 10:09:16', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process seq id', 2002, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('status', 'wo_interface_staging', TO_DATE('2012-05-07 10:08:56', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process status', 2001, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('suspended_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:40:12', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'suspended date', 280, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'wo_interface_staging', TO_DATE('2012-05-07 09:27:35', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'time stamp', 10000, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'wo_interface_staging', TO_DATE('2012-05-07 10:01:07', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'user id', 10001, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('wo_approval_group_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 11:11:44', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'wo_approval_grp', 'p',
    null, 'wo approval group id', 450, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('wo_status_id', 'wo_interface_staging', TO_DATE('2012-05-07 10:24:17', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'work_order_status', 'p', null, 'wo status id', 230, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('work_order_grp_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:26:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'work_order_group', 'p',
    null, 'work order grp id', 420, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('work_order_number', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:49:32', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'work order number', 10, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('work_order_type_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:23:05', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'work_order_type', 'p',
    null, 'work order type id', 80, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('action', 'wo_interface_staging', TO_DATE('2012-05-07 10:11:30', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process action', 2004, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('afudc_start_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:47:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'afudc start date', 281, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('afudc_stop_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:47:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'afudc stop date', 282, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('agreement_id', 'wo_interface_staging', TO_DATE('2012-05-07 10:36:27', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'co_tenancy_agreement', 'p', null, 'agreement id', 470, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('alternatives', 'wo_interface_staging', TO_DATE('2012-05-07 09:45:34', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'alternatives', 390, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('approval_type_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:28:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'workflow_type', 'p', null,
    'approval type id', 440, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('asset_location_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:23:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'asset_location', 'p', null,
    'asset location id', 150, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('background', 'wo_interface_staging', TO_DATE('2012-05-07 09:45:34', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'background', 370, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('base_year', 'wo_interface_staging', TO_DATE('2012-05-07 09:52:00', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'base year', 490, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('batch_id', 'wo_interface_staging', TO_DATE('2012-05-07 10:11:01', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process batch id', 2003, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('budget_company_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:23:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'company', 'p', null,
    'budget company id', 120, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('budget_number', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:28:24', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'budget number', 110, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('bus_segment_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:14:06', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'business_segment', 'p',
    null, 'bus segment id', 70, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code1', 'wo_interface_staging', TO_DATE('2012-05-07 09:53:27', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code1', 1001, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code10', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code10', 1019, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code11', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code11', 1021, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code12', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code12', 1023, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code13', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code13', 1025, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code14', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code14', 1027, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code15', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code15', 1029, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code16', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code16', 1031, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code17', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code17', 1033, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code18', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code18', 1035, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code19', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code19', 1037, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code2', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code2', 1003, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code20', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code20', 1039, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code21', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code21', 1041, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code22', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code22', 1043, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code23', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code23', 1045, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code24', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code24', 1047, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code25', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code25', 1049, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code26', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code26', 1051, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code27', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code27', 1053, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code28', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code28', 1055, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code29', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code29', 1057, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code3', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code3', 1005, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code30', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code30', 1059, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code4', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code4', 1007, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code5', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code5', 1009, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code6', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code6', 1011, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code7', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code7', 1013, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code8', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code8', 1015, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code9', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class code9', 1017, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value1', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value1', 1002, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value10', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value10', 1020, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value11', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value11', 1022, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value12', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value12', 1024, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value13', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value13', 1026, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value14', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value14', 1028, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value15', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value15', 1030, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value16', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value16', 1032, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value17', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value17', 1034, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value18', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value18', 1036, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value19', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value19', 1038, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value2', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value2', 1004, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value20', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value20', 1040, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value21', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value21', 1042, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value22', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value22', 1044, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value23', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value23', 1046, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value24', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value24', 1048, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value25', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value25', 1050, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value26', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value26', 1052, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value27', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value27', 1054, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value28', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value28', 1056, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value29', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value29', 1058, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value3', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value3', 1006, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value30', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value30', 1060, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value4', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value4', 1008, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value5', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value5', 1010, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value6', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value6', 1012, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value7', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value7', 1014, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value8', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value8', 1016, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value9', 'wo_interface_staging', TO_DATE('2012-05-07 09:59:40', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'class value9', 1018, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('close_date', 'wo_interface_staging', TO_DATE('2012-05-07 09:40:12', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'close date', 270, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('company_id', 'wo_interface_staging', TO_DATE('2012-05-07 10:13:50', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'company', 'p', null, 'company id', 50, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('completion_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:40:12', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'completion date', 250, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('contract_admin', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:25:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'contract admin', 340, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('department_id', 'wo_interface_staging',
    TO_DATE('2012-05-07 10:24:01', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'department', 'p', null,
    'department id', 170, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'wo_interface_staging', TO_DATE('2012-05-07 10:49:32', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'description', 30, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('engineer', 'wo_interface_staging', TO_DATE('2012-05-07 10:25:53', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'pp_security_users', 'p', null, 'engineer', 310, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('est_annual_rev', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:52:00', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'est annual rev', 480, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('est_complete_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:38:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'est complete date', 210, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('est_in_service_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:38:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'est in service date', 220, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('est_start_date', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:38:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'est start date', 200, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_asset_location', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:36:57', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext asset location', 160, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_company', 'wo_interface_staging', TO_DATE('2012-05-07 09:27:35', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'ext company', 60, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_department', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:36:57', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext department', 180, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_major_location', 'wo_interface_staging',
    TO_DATE('2012-05-07 09:36:57', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext major location', 140, null, null, null, null, null, null, null, null);


insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('job_task_interface_staging', TO_DATE('2012-05-08 10:09:39', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 's', 'Job Task Interface Staging', 'always', null, 'work,budget', null, null, null,
    null, null, null, null, null, null, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('action', 'job_task_interface_staging', TO_DATE('2012-05-08 10:13:37', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process action', 2004, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('alternatives', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'alternatives', 430, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('background', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'background', 410, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('batch_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:13:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'process batch id', 2003, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('budget_analyst', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:55:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'budget analyst', 310, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('chargeable', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:53:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'yes_no', 'p', null,
    'chargeable', 170, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code1', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code1', 1001, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code10', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code10', 1019, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code11', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code11', 1021, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code12', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code12', 1023, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code13', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code13', 1025, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code14', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code14', 1027, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code15', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code15', 1029, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code16', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code16', 1031, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code17', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code17', 1033, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code18', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code18', 1035, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code19', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code19', 1037, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code2', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code2', 1003, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code20', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:05', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code20', 1039, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code3', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code3', 1005, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code4', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code4', 1007, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code5', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code5', 1009, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code6', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code6', 1011, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code7', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code7', 1013, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code8', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code8', 1015, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_code9', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class code9', 1017, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value1', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value1', 1002, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value10', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value10', 1020, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value11', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value11', 1022, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value12', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value12', 1024, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value13', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value13', 1026, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value14', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value14', 1028, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value15', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value15', 1030, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value16', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value16', 1032, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value17', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value17', 1034, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value18', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value18', 1036, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value19', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value19', 1038, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value2', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value2', 1004, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value20', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:05', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value20', 1040, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value3', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value3', 1006, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value4', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value4', 1008, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value5', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value5', 1010, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value6', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value6', 1012, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value7', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value7', 1014, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value8', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value8', 1016, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('class_value9', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:19:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'class value9', 1018, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('company_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:51:29', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'company', 'p', null,
    'company id', 70, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('completion_date', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'completion date', 160, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'description', 40, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('engineer', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:55:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'engineer', 330, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('est_complete_date', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'est complete date', 130, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('est_start_date', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'est start date', 120, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('estimate', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:53:15', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'yes_no', 'p', null,
    'estimate', 180, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_company', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext company', 80, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_job_task', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'external job task', 20, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field1', 'job_task_interface_staging', TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field1', 350, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field2', 'job_task_interface_staging', TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field2', 360, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field3', 'job_task_interface_staging', TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field3', 370, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field4', 'job_task_interface_staging', TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field4', 380, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field5', 'job_task_interface_staging', TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field5', 390, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('financial_analysis', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'financial analysis', 440, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('forecast', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:53:20', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'yes_no', 'p', null,
    'forecast', 190, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('funding_task_indicator', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:52:35', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'yes_no', 'p', null,
    'funding task indicator', 115, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('future_projects', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'future projects', 420, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('group_descr', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext group', 230, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('group_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:53:44', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'job_task_group', 'p', null,
    'group id', 220, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('initiator', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:54:54', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'initiator', 290, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('job_task_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'job task id', 10, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('job_task_status_descr', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext job task status', 150, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('job_task_status_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:52:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'job_task_status', 'p',
    null, 'job task status id', 140, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('level_number', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'level number', 450, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'long description', 50, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('notes', 'job_task_interface_staging', TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'notes', 60, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('other_contact', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:55:47', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'other contact', 340, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('parent_job_task', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'parent job task', 110, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('percent_complete', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'percent complete', 250, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('priority_code_descr', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext priority code', 280, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('priority_code_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'job_task_priority_code',
    'p', null, 'priority code id', 270, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('project_manager', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:55:30', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'project manager', 320, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('reason_for_work', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'reason for work', 400, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('row_id', 'job_task_interface_staging', TO_DATE('2012-05-08 10:50:36', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 's', null, 'row id', 0, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('seq_id', 'job_task_interface_staging', TO_DATE('2012-05-08 10:13:37', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process seq id', 2002, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('setup_template', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext setup template', 100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('setup_template_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:51:48', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'job_task_setup_template',
    'p', null, 'setup template id', 90, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('status', 'job_task_interface_staging', TO_DATE('2012-05-08 10:13:37', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process status', 2001, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('status_code_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:53:58', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'status_code', 'p', null,
    'status code id', 240, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('system_descr', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext system', 210, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('system_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:53:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'job_task_system', 'p',
    null, 'system id', 200, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('task_owner', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:55:11', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'pp_security_users', 'p',
    null, 'task owner', 300, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:14:28', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'time stamp', 10000, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:14:28', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null, 'user id',
    10001, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('work_effort', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'work effort', 260, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('work_order_number', 'job_task_interface_staging',
    TO_DATE('2012-05-08 10:49:53', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'work order number', 30, null, null, null, null, null, null, null, null);


insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('wo_interface_unit', TO_DATE('2012-05-08 10:58:24', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 's',
    'Wo Interface Unit', 'always', null, 'work,budget', null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('amount', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'amount', 320, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('asset_location_id', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:52:12', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'asset_location', 'p', null,
    'asset location id', 240, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('batch_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:01:43', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process batch id', 2003, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('bus_segment_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:50:50', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'business_segment', 'p', null, 'bus segment id', 120, null, null, null, null, null,
    null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('company_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:49:34', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'company', 'p', null, 'company id', 50, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('department_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:52:27', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'department', 'p', null, 'department id', 260, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('est_chg_type_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:50:34', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'estimate_charge_type_wo', 'p', null, 'est chg type id', 100, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('expenditure_type_id', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:50:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'expenditure_type', 'p',
    null, 'expenditure type id', 80, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_asset_location', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext asset location', 250, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_bus_segment', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'ext bus segment', 130, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_company', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'ext company', 60, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_department', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'ext department', 270, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_est_chg_type', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext est chg type', 110, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_expenditure_type', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext expenditure type', 90, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_job_task', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'ext job task', 30, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_property_group', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext property group', 210, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_retirement_unit', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext retirement unit', 190, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_stck_keep_unit', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext stck keep unit', 230, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_sub_account', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'ext sub account', 170, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_utility_account', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext utility account', 150, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ext_wo_est_trans_type', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'ext wo est trans type', 290, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field_1', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field 1', 350, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field_2', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field 2', 360, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field_3', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field 3', 370, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field_4', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field 4', 380, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('field_5', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'field 5', 390, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('funding_wo_indicator', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:49:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'yes_no', 'p', null,
    'funding wo indicator', 70, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('hours', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'hours', 300, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('job_task_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'job task id', 20, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('notes', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'notes', 340, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('process_level', 'wo_interface_unit', TO_DATE('2012-05-08 11:48:03', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'l', null, 'process level', 40, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('property_group_id', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:51:45', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'property_group', 'p', null,
    'property group id', 200, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('quantity', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'quantity', 310, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('replacement_amount', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'replacement amount', 330, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('retire_vintage', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'retire vintage', 410, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('retirement_unit_id', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:51:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'retirement_unit', 'p',
    null, 'retirement unit id', 180, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('row_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:00:32', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'row id', 0, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('seq_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:01:43', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process seq id', 2002, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('serial_number', 'wo_interface_unit', TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'serial number', 400, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('status', 'wo_interface_unit', TO_DATE('2012-05-08 11:01:43', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'process status', 2001, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('stck_keep_unit_id', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:51:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'stck_keep_unit', 'p', null,
    'stck keep unit id', 220, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('sub_account_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:51:17', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', 'sub_account', 'p', null, 'sub account id', 160, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'wo_interface_unit', TO_DATE('2012-05-08 11:01:43', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'time stamp', 10000, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'wo_interface_unit', TO_DATE('2012-05-08 11:01:43', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'user id', 10001, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('utility_account_id', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:51:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'utility_account', 'p',
    null, 'utility account id', 140, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('wo_est_trans_type_id', 'wo_interface_unit',
    TO_DATE('2012-05-08 12:02:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'wo_est_trans_type', 'p',
    null, 'wo est trans type id', 280, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('work_order_number', 'wo_interface_unit',
    TO_DATE('2012-05-08 11:45:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'work order number', 10, null, null, null, null, null, null, null, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (142, 0, 10, 3, 4, 1, 10321, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.1_maint_010321_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
