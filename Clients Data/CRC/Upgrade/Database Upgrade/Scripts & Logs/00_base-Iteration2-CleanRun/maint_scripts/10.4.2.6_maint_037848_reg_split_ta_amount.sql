/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037848_reg_split_ta_amount.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/15/2014 Sarah Byers
||========================================================================================
*/

-- REG_HIST_TA_LOAD_STG
create table REG_HIST_TA_LOAD_STG
(
 REG_COMPANY_ID      number(22,0) not null,
 REG_ACCT_ID         number(22,0) not null,
 HISTORIC_VERSION_ID number(22,0) not null,
 GL_MONTH            number(22,1) not null,
 ACT_AMOUNT          number(22,2) not null,
 REG_SOURCE_ID       number(22,0) not null,
 SPLIT_TA_AMOUNT     number(22,0),
 USER_ID             varchar2(18),
 TIME_STAMP          date
);

alter table REG_HIST_TA_LOAD_STG
   add constraint PK_REG_HIST_TA_LOAD_STG
       primary key (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH, REG_SOURCE_ID)
       using index tablespace PWRPLANT_IDX;

-- FK
alter table REG_HIST_TA_LOAD_STG
   add constraint R_REG_HIST_TA_LOAD_STG1
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

alter table REG_HIST_TA_LOAD_STG
   add constraint R_REG_HIST_TA_LOAD_STG2
       foreign key (REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

alter table REG_HIST_TA_LOAD_STG
   add constraint R_REG_HIST_TA_LOAD_STG3
       foreign key (HISTORIC_VERSION_ID)
       references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

alter table REG_HIST_TA_LOAD_STG
   add constraint R_REG_HIST_TA_LOAD_STG4
       foreign key (REG_SOURCE_ID)
       references REG_SOURCE (REG_SOURCE_ID);

-- COMMENTS
comment on table REG_HIST_TA_LOAD_STG is '(O) [19] The Reg Hist TA Load Stg table is the staging table used by the Tax Provision interface that stages data for insert into the Reg History Ledger.';
comment on column REG_HIST_TA_LOAD_STG.REG_COMPANY_ID is 'System assigned identifier of a Regulatory Company.';
comment on column REG_HIST_TA_LOAD_STG.REG_ACCT_ID is 'System assigned identifier of a Regulatory Account.';
comment on column REG_HIST_TA_LOAD_STG.HISTORIC_VERSION_ID is 'System assigned identifier of a Historic Ledger (version).';
comment on column REG_HIST_TA_LOAD_STG.GL_MONTH is 'General Ledger posting account month in the YYYYMM format.';
comment on column REG_HIST_TA_LOAD_STG.ACT_AMOUNT is 'The actual month end account balance or monthly amount.';
comment on column REG_HIST_TA_LOAD_STG.REG_SOURCE_ID is 'System assigned identifier of a regulatory source.';
comment on column REG_HIST_TA_LOAD_STG.SPLIT_TA_AMOUNT is '1 indicates that the amount will be allocated to PowerTax amounts; 0 indicates the amount should be loaded to the history ledger.';
comment on column REG_HIST_TA_LOAD_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_HIST_TA_LOAD_STG.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';

-- REG_TA_SPLIT_PROV_AMOUNT
create table REG_TA_SPLIT_PROV_AMOUNT
(
 REG_COMPANY_ID      number(22,0) not null,
 HISTORIC_VERSION_ID number(22,0) not null,
 REG_ACCT_ID         number(22,0) not null,
 GL_MONTH            number(22,0) not null,
 AMOUNT              number(22,2) not null,
 USER_ID             varchar2(18),
 TIME_STAMP          date
);

alter table REG_TA_SPLIT_PROV_AMOUNT
   add constraint PK_REG_TA_SPLIT_PROV_AMOUNT
       primary key (REG_COMPANY_ID, HISTORIC_VERSION_ID, REG_ACCT_ID, GL_MONTH)
       using index tablespace PWRPLANT_IDX;

-- FK
alter table REG_TA_SPLIT_PROV_AMOUNT
   add constraint R_REG_TA_SPLIT_PROV_AMOUNT1
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

alter table REG_TA_SPLIT_PROV_AMOUNT
   add constraint R_REG_TA_SPLIT_PROV_AMOUNT2
       foreign key (REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

alter table REG_TA_SPLIT_PROV_AMOUNT
   add constraint R_REG_TA_SPLIT_PROV_AMOUNT3
       foreign key (HISTORIC_VERSION_ID)
       references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

-- COMMENTS
comment on table REG_TA_SPLIT_PROV_AMOUNT is '(O) [19] The Reg TA Split Prov Amount table is used by the Provision interface to store the amount by Provision Reg Account, historic ledger, and gl month that needs to be allocated to PowerTax Reg Accounts.';
comment on column REG_TA_SPLIT_PROV_AMOUNT.REG_COMPANY_ID is 'System assigned identifier of a Regulatory Company.';
comment on column REG_TA_SPLIT_PROV_AMOUNT.REG_ACCT_ID is 'System assigned identifier of a Regulatory Account.';
comment on column REG_TA_SPLIT_PROV_AMOUNT.HISTORIC_VERSION_ID is 'System assigned identifier of a Historic Ledger (version).';
comment on column REG_TA_SPLIT_PROV_AMOUNT.GL_MONTH is 'General Ledger posting account month in the YYYYMM format.';
comment on column REG_TA_SPLIT_PROV_AMOUNT.AMOUNT is 'The actual month end account balance or monthly amount.';
comment on column REG_HIST_TA_LOAD_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_HIST_TA_LOAD_STG.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';

-- Add Reg Company to REG_TA_SPLIT_RULE_AMOUNT
drop table REG_TA_SPLIT_RULE_AMOUNT;

create table REG_TA_SPLIT_RULE_AMOUNT
(
 REG_TA_SPLIT_RULE_ID number(22,0) not null,
 REG_COMPANY_ID       number(22,0) not null,
 MONTH                number(6,0) not null,
 AMOUNT               number(22,2) not null,
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

comment on table REG_TA_SPLIT_RULE_AMOUNT
  is '(  )  [  ]~r~nThe Reg TA Split Rule Amount table is used to store the amounts used split a single provision based reg account to multiple powertax based reg accounts per effective month.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.REG_TA_SPLIT_RULE_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A TAX PROVISION SPLIT RULE.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.REG_COMPANY_ID is 'System assigned identifier of a Regulatory Company.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.MONTH is 'Effective Month (YYYYMM) that the PowerTax amounts should be used when splitting provision reg accounts.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.AMOUNT is 'THE POWERTAX AMOUNT LOADED PER REG ACCOUNT TO USE AS PRO-RATA FACTOR IN SPLITTING PROVISION AMOUNT.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

alter table REG_TA_SPLIT_RULE_AMOUNT
   add constraint PK_REG_TA_SPLIT_RULE_AMOUNT
       primary key (REG_TA_SPLIT_RULE_ID, REG_COMPANY_ID, MONTH)
       using index tablespace PWRPLANT_IDX;

alter table REG_TA_SPLIT_RULE_AMOUNT
   add constraint R_REG_TA_SPLIT_RULE_AMOUNT1
       foreign key (REG_TA_SPLIT_RULE_ID)
       references REG_TA_SPLIT_RULES (REG_TA_SPLIT_RULE_ID);

alter table REG_TA_SPLIT_RULE_AMOUNT
   add constraint R_REG_TA_SPLIT_RULE_AMOUNT2
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1143, 0, 10, 4, 2, 6, 37848, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_037848_reg_split_ta_amount.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;