/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051527_lessee_02_recreate_pk_ls_lease_calculated_date_rates_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.4.0 06/12/2018 Sisouphanh       Recreate the primary key for ls_lease_calculated_date_rates
||============================================================================
*/

/* Recrate the primary key */
alter table ls_lease_calculated_date_rates 
      drop primary key 
      drop index
;

ALTER TABLE ls_lease_calculated_date_rates 
      ADD CONSTRAINT ls_lease_calc_date_rates_pk 
      PRIMARY KEY (company_id, contract_currency_id, accounting_month, exchange_rate_type_id)
      USING INDEX TABLESPACE pwrplant_idx
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7223, 0, 2017, 4, 0, 0, 51527, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051527_lessee_02_recreate_pk_ls_lease_calculated_date_rates_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;