/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039454_reg_time_stamp_fields.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/07/2014 Ryan Oliveria    Fix Time Stamp Fields
||============================================================================
*/

--* Rename TIMESTAMP to TIME_STAMP
alter table REG_INCREMENTAL_ADJUSTMENT     rename column TIMESTAMP to TIME_STAMP;
alter table REG_INCREMENTAL_ADJUST_TYPE    rename column TIMESTAMP to TIME_STAMP;
alter table REG_INCREMENTAL_ADJ_VERSION    rename column TIMESTAMP to TIME_STAMP;
alter table REG_INCREMENTAL_FP_ADJ_TRANS   rename column TIMESTAMP to TIME_STAMP;
alter table REG_INCREMENTAL_ITEM_MAP       rename column TIMESTAMP to TIME_STAMP;
alter table REG_INCREMENTAL_LABEL_MAP      rename column TIMESTAMP to TIME_STAMP;

alter table INCREMENTAL_DEPR_ADJ_DATA      rename column TIMESTAMP to TIME_STAMP;
alter table INCREMENTAL_FP_ADJ_DATA        rename column TIMESTAMP to TIME_STAMP;
alter table INCREMENTAL_FP_RUN_LOG         rename column TIMESTAMP to TIME_STAMP;

alter table REG_EXT_COMPANY_MAP            rename column TIMESTAMP to TIME_STAMP;
alter table REG_FCST_INC_ADJ_LEDGER        rename column TIMESTAMP to TIME_STAMP;

--* Add missing USER_ID fields
alter table REG_ALLOCATOR_DYN_EXT          add USER_ID varchar2(18);
alter table REG_CO_INPUT_SCALE             add USER_ID varchar2(18);
alter table REG_DEPR_LEDGER_COLUMNS        add USER_ID varchar2(18);
alter table REG_EXT_STG_FIELD              add USER_ID varchar2(18);
alter table REG_HIST_RECON_GROUP           add USER_ID varchar2(18);
alter table REG_IMPORT_EXT_STAGE_ARC       add USER_ID varchar2(18);
alter table REG_INPUT_SCALE                add USER_ID varchar2(18);
alter table REG_JUR_INPUT_SCALE            add USER_ID varchar2(18);
alter table REG_QUERY_LIST                 add USER_ID varchar2(18);

comment on column INCREMENTAL_FP_ADJ_ITEM.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column INCREMENTAL_FP_ADJ_LABEL.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_ALLOCATOR_DYN_EXT.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_CO_INPUT_SCALE.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_DEPR_LEDGER_COLUMNS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_EXT_STG_FIELD.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_HIST_RECON_GROUP.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_IMPORT_EXT_STAGE_ARC.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_INPUT_SCALE.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_JUR_INPUT_SCALE.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_QUERY_LIST.USER_ID is 'Standard system-assigned user id used for audit purposes.';


--* Add missing TIME_STAMP fields
alter table INCREMENTAL_FP_ADJ_ITEM        add TIME_STAMP date;
alter table INCREMENTAL_FP_ADJ_LABEL       add TIME_STAMP date;
alter table REG_ALLOCATOR_DYN_EXT          add TIME_STAMP date;
alter table REG_CO_INPUT_SCALE             add TIME_STAMP date;
alter table REG_DEPR_LEDGER_COLUMNS        add TIME_STAMP date;
alter table REG_EXT_STG_FIELD              add TIME_STAMP date;
alter table REG_HIST_RECON_GROUP           add TIME_STAMP date;
alter table REG_IMPORT_EXT_STAGE_ARC       add TIME_STAMP date;
alter table REG_INPUT_SCALE                add TIME_STAMP date;
alter table REG_JUR_INPUT_SCALE            add TIME_STAMP date;
alter table REG_QUERY_LIST                 add TIME_STAMP date;

comment on column INCREMENTAL_FP_ADJ_ITEM.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column INCREMENTAL_FP_ADJ_LABEL.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_ALLOCATOR_DYN_EXT.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_CO_INPUT_SCALE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_DEPR_LEDGER_COLUMNS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_EXT_STG_FIELD.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_HIST_RECON_GROUP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_IMPORT_EXT_STAGE_ARC.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_INPUT_SCALE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_JUR_INPUT_SCALE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column REG_QUERY_LIST.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1333, 0, 10, 4, 2, 7, 39454, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039454_reg_time_stamp_fields.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
