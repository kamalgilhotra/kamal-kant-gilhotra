/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033610_depr_group_xlate.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/24/2013 Ryan Oliveria
||============================================================================
*/

--Depr group translation was grabbing set of books id
update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select dg.depr_group_id from depr_group dg where upper( trim( <importfield> ) ) = upper( trim( dg.description ) ) and <importtable>.company_id = dg.company_id )'
 where COLUMN_NAME = 'depr_group_id'
   and LOOKUP_SQL = '( select set_of_books_id from depr_group dg where upper( trim( <importfield> ) ) = upper( trim( dg.description ) ) and <importtable>.company_id = dg.company_id )';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (707, 0, 10, 4, 2, 0, 33610, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033610_depr_group_xlate.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;