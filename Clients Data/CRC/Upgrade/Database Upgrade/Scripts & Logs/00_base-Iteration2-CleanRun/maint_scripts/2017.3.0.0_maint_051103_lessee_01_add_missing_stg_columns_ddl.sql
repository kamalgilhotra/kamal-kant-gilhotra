/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051103_lessee_01_add_missing_stg_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 05/11/2018 Josh Sandler     Add missing columns to stg table
||============================================================================
*/

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD interest_convention_id NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.interest_convention_id IS 'The interest convention used for calculating interest.';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD deferred_rent_sw NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.deferred_rent_sw IS 'Switch to allow or disallow prepaid/deferred_rent.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5342, 0, 2017, 3, 0, 0, 51103, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051103_lessee_01_add_missing_stg_columns_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;