/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052688_lessee_02_ls_depr_fcst_changes_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/21/2018 Sarah Byers    Populate new fields on ls_depr_forecast
||============================================================================
*/

declare
  rtn varchar2(2000);
  b_error boolean;
  L_BOOK_SUMMARY_ID          NUMBER;
  L_CPR_AMOUNT               NUMBER;
  L_ASSET_AMOUNT             NUMBER;
  L_DEPR_METHOD_SLE_AMOUNT   NUMBER;
  L_RESIDUAL_AMOUNT          NUMBER;
  L_IN_SERVICE_EXCHANGE_RATE LS_ILR_OPTIONS.IN_SERVICE_EXCHANGE_RATE%type;
  L_SQLS                     VARCHAR2(4000);
begin
  DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
  b_error := FALSE;
  
  for L_ASSETS in (select distinct a.ls_asset_id, i.current_revision, m.asset_id, f.set_of_books_id, c.open_month
                     from ls_asset a
                     join ls_cpr_asset_map m on m.ls_asset_id = a.ls_asset_id
                     join ls_ilr i on i.ilr_id = a.ilr_id
                     join ls_depr_forecast f on f.ls_asset_id = a.ls_asset_id and f.revision = i.current_revision
                     join (select company_id, min(gl_posting_mo_yr) open_month
                             from ls_process_control
                            where lam_closed is null
                            group by company_id) c on c.company_id = a.company_id
                    where f.month >= c.open_month
                      and f.depr_exp_comp_curr is null
                      and f.depr_expense is not null)
  loop
    -- Calc the company currency depr expense
    DBMS_OUTPUT.PUT_LINE('Calculating for LS_ASSET_ID: '||to_char(L_ASSETS.LS_ASSET_ID));
    
    begin
      merge into ls_depr_forecast a
      using (
        with war as
         (select ilr_id, L_ASSETS.LS_ASSET_ID ls_asset_id, revision, set_of_books_id,
                 net_weighted_avg_rate, effective_month
            from (select war.ilr_id, war.revision, war.set_of_books_id,
                          war.net_weighted_avg_rate,
                          trunc(nvl(lio.remeasurement_date, ilr.est_in_svc_date),
                                 'month') effective_month,
                          row_number() over(partition by lia.ilr_id, trunc(nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') order by lia.approval_date DESC) rnum
                     from ls_ilr_weighted_avg_rates war
                     join ls_ilr ilr on war.ilr_id = ilr.ilr_id
                     join ls_ilr_options lio on lio.ilr_id = war.ilr_id
                                            and lio.revision = war.revision
                     join ls_ilr_approval lia on lia.ilr_id = lio.ilr_id
                                             and lia.revision = lio.revision
                     join ls_asset la on la.ilr_id = ilr.ilr_id
                    where war.set_of_books_id = L_ASSETS.SET_OF_BOOKS_ID
                      and lia.approval_status_id = 3
                      and la.ls_asset_id = L_ASSETS.LS_ASSET_ID
                      and lia.approval_date <=
                          (select approval_date
                             from ls_ilr_approval
                            where ilr_id = ilr.ilr_id
                              and revision = L_ASSETS.CURRENT_REVISION))
           where rnum = 1)
        select f.ls_asset_id, f.set_of_books_id, f.revision, f.month,
               f.depr_expense * war.net_weighted_avg_rate depr_exp_comp_curr,
               war.net_weighted_avg_rate exchange_rate,
               f.depr_exp_alloc_adjust * war.net_weighted_avg_rate depr_exp_aa_comp_curr
          from war
          join ls_depr_forecast f on f.ls_asset_id = war.ls_asset_id
                                 and f.set_of_books_id = war.set_of_books_id
                                 and f.revision = L_ASSETS.CURRENT_REVISION
         where f.month >= war.effective_month
           and war.effective_month =
               (select max(effective_month)
                  from war eff_war
                 where eff_war.ilr_id = war.ilr_id
                   and eff_war.effective_month <= f.month)) b 
      on (    a.ls_asset_id = b.ls_asset_id 
          and a.set_of_books_id = b.set_of_books_id 
          and a.revision = b.revision 
          and a.month = b.month) 
      when matched then
        update set a.depr_exp_comp_curr    = b.depr_exp_comp_curr,
                   a.exchange_rate         = b.exchange_rate,
                   a.depr_exp_aa_comp_curr = b.depr_exp_aa_comp_curr;  
   exception
     when others then
        b_error := TRUE;
        DBMS_OUTPUT.PUT_LINE('ERROR: Calculating for LS_ASSET_ID: '||to_char(L_ASSETS.LS_ASSET_ID)||': '||sqlerrm);
        rollback;
   end;  
   
   SELECT fasb.book_summary_id
    INTO L_BOOK_SUMMARY_ID
    FROM ls_asset la
      JOIN ls_ilr_options lio ON lio.ilr_id = la.ilr_id
      JOIN ls_lease_cap_type lct ON lct.ls_lease_cap_type_id =
                                    lio.lease_cap_type_id
      JOIN ls_fasb_cap_type_sob_map fasb ON fasb.lease_cap_type_id =
                                            lio.lease_cap_type_id
    WHERE la.ls_asset_id = L_ASSETS.LS_ASSET_ID
    AND lio.revision = L_ASSETS.CURRENT_REVISION
    AND fasb.set_of_books_id = L_ASSETS.SET_OF_BOOKS_ID;

    L_SQLS := 'SELECT clb.basis_' || L_BOOK_SUMMARY_ID || ' ' ||
    'FROM cpr_ldg_basis clb ' ||
    ' JOIN ls_cpr_asset_map map ON clb.asset_id = map.asset_id ' ||
    'WHERE map.ls_asset_id = ' || L_ASSETS.LS_ASSET_ID;

    BEGIN
      EXECUTE IMMEDIATE L_SQLS
      INTO L_CPR_AMOUNT;
    EXCEPTION
      WHEN No_Data_Found THEN
        L_CPR_AMOUNT := 0;
    END;

    DBMS_OUTPUT.PUT_LINE('Existing CPR Asset Amount: ' || To_Char(L_CPR_AMOUNT));

    begin 
      SELECT o.in_service_exchange_rate
        INTO L_IN_SERVICE_EXCHANGE_RATE
        FROM ls_asset a
        join ls_ilr_options o on o.ilr_id = a.ilr_id
       WHERE a.ls_asset_id = L_ASSETS.LS_ASSET_ID
         and o.revision = L_ASSETS.CURRENT_REVISION;
    exception
      when no_data_found then
        L_IN_SERVICE_EXCHANGE_RATE := 1;
    end;

    SELECT Nvl(Round(CASE
                       WHEN la.estimated_residual = 0 and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual',
                                                                                                                       la.COMPANY_ID))) =
                            'yes' THEN
      LA.GUARANTEED_RESIDUAL_AMOUNT
    ELSE
                        Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual',
                                                                                            la.COMPANY_ID))),
                               'no',
                               0,
                               round(la.ESTIMATED_RESIDUAL * la.FMV, 2))
                     END * L_IN_SERVICE_EXCHANGE_RATE,
                     2),
               0)
    INTO L_RESIDUAL_AMOUNT
    FROM ls_asset la
    WHERE ls_asset_id = L_ASSETS.LS_ASSET_ID;

    DBMS_OUTPUT.PUT_LINE('Asset Residual Amount: ' ||To_Char(L_RESIDUAL_AMOUNT));

    SELECT Nvl(Sum(depr_exp_comp_curr), 0)
    INTO L_DEPR_METHOD_SLE_AMOUNT
    FROM ls_depr_forecast
    WHERE ls_asset_id = L_ASSETS.LS_ASSET_ID
    AND set_of_books_id = L_ASSETS.SET_OF_BOOKS_ID
    and revision = L_ASSETS.CURRENT_REVISION;

    DBMS_OUTPUT.PUT_LINE('Total Depr Expense Amount: ' ||To_Char(L_DEPR_METHOD_SLE_AMOUNT));

    IF L_CPR_AMOUNT - L_RESIDUAL_AMOUNT =
       L_DEPR_METHOD_SLE_AMOUNT THEN
       DBMS_OUTPUT.PUT_LINE('Asset and total depr amounts match. No rounding needed.');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Updating last month with rounding plug: ' ||
                                 To_Char(L_CPR_AMOUNT -
                                         L_RESIDUAL_AMOUNT -
                                         L_DEPR_METHOD_SLE_AMOUNT));
      begin
        UPDATE ls_depr_forecast
           SET depr_exp_comp_curr = depr_exp_comp_curr +
                                    (L_CPR_AMOUNT -
                                     L_RESIDUAL_AMOUNT - L_DEPR_METHOD_SLE_AMOUNT)
        WHERE ls_asset_id = L_ASSETS.LS_ASSET_ID
          AND set_of_books_id = L_ASSETS.SET_OF_BOOKS_ID
          and revision = L_ASSETS.CURRENT_REVISION
          AND month =
               (SELECT Max(month)
                  FROM ls_depr_forecast
                 WHERE ls_asset_id = L_ASSETS.LS_ASSET_ID
                   AND set_of_books_id = L_ASSETS.SET_OF_BOOKS_ID
                   and revision = L_ASSETS.CURRENT_REVISION);
      exception
        when others then
          b_error := TRUE;
          DBMS_OUTPUT.PUT_LINE('ERROR: Plugging last month with rounding amount for LS_ASSET_ID: '||to_char(L_ASSETS.LS_ASSET_ID)||': '||sqlerrm);
          rollback;
      end;  
    END if;     
  end loop;
  
  if b_error then
    RAISE_APPLICATION_ERROR(-20000, 'Errors were encountered in the script.  Please review the errors and contact product for support in resolving.'); 
  end if;
end;
/
                   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12202, 0, 2018, 1, 0, 0, 52688, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052688_lessee_02_ls_depr_fcst_changes_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
