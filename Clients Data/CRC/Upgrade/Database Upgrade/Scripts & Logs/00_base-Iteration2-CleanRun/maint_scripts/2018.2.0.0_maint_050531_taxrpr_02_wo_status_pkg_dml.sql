/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050531_taxrpr_02_wo_status_pkg_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 08/01/2017 Eric Berger    Create the wo_status processing tables
||										and pl/sql package.
||============================================================================
*/

--create the system option for bulk tax status evaluation
INSERT INTO ppbase_system_options (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
SELECT 'Repairs - Bulk Evaluate Tax Status', 'Whether or not to bulk calculate work order tax status.', 0, 'No', null, 1, 0
FROM dual
WHERE NOT EXISTS ( 
    SELECT 1
    FROM ppbase_system_options
    WHERE Upper(system_option_id) = 'REPAIRS - BULK EVALUATE TAX STATUS'
);

INSERT INTO ppbase_system_options_module (SYSTEM_OPTION_ID, MODULE)
SELECT 'Repairs - Bulk Evaluate Tax Status', 'REPAIRS'
FROM dual
WHERE NOT EXISTS (
    SELECT 1
    FROM ppbase_system_options_module
    WHERE Upper(system_option_id) = 'REPAIRS - BULK EVALUATE TAX STATUS'
    AND Upper(MODULE) = 'REPAIRS'
);

--turn on the system option
UPDATE ppbase_system_options
SET option_value = 'Yes'
WHERE Upper(system_option_id) = 'REPAIRS - BULK EVALUATE TAX STATUS'
AND (Upper(option_value) <> 'YES' OR option_value IS NULL)
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13740, 0, 2018, 2, 0, 0, 50531, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_050531_taxrpr_02_wo_status_pkg_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;