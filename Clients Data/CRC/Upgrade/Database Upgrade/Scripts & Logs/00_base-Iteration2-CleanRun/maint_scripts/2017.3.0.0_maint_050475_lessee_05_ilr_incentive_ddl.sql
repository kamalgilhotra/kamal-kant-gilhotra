/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_05_ilr_incentive_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/25/2018 Shane "C" Ward    Create Lessee ILR Incentives Table Structure
||============================================================================
*/
--Lessee Incentive Group DDL
CREATE TABLE ls_incentive_group (
  incentive_group_id     NUMBER(22,0)  NOT NULL,
  description      VARCHAR2(35)  NOT NULL,
  long_description VARCHAR2(254) NULL,
  user_id          VARCHAR2(18)  NULL,
  time_stamp       DATE          NULL
)
;

ALTER TABLE ls_incentive_group
  ADD CONSTRAINT ls_incentive_group_pk PRIMARY KEY (
    incentive_group_id
  );

ALTER TABLE ls_incentive_group
  ADD CONSTRAINT ls_incentive_group_uk UNIQUE (
    description
  );

COMMENT ON TABLE ls_incentive_group IS '(S) []LESSEE The table stores the Incentive Group Information.';

COMMENT ON COLUMN ls_incentive_group.incentive_group_id IS 'System-assigned identifier of an Incentive group.';
COMMENT ON COLUMN ls_incentive_group.description IS 'Description of the Incentive group.';
COMMENT ON COLUMN ls_incentive_group.long_description IS 'Long description of the Incentive group.';
COMMENT ON COLUMN ls_incentive_group.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_incentive_group.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--Lessee ILR Incentive Table DDL
create table ls_ilr_incentive (
  ilr_incentive_id     NUMBER(22,0),
  ilr_id       NUMBER(22,0) not null,
  revision          NUMBER(22,0) not null,
  incentive_group_id      NUMBER(22,0) not null,
  date_incurred     DATE,
  amount            NUMBER(22,2) default 0.0,
  description       VARCHAR2(254),
  user_id           VARCHAR2(18),
  time_stamp        DATE);

alter table ls_ilr_incentive
  add constraint ls_ilr_incentive_pk primary key (ilr_incentive_id);

alter table ls_ilr_incentive
  add constraint ls_ilr_incentive_fk1 foreign key (ilr_id, revision)
  references ls_ilr_approval(ilr_id, revision) ;

alter table ls_ilr_incentive
  add constraint ls_ilr_incentive_fk2 foreign key (incentive_group_id)
  references ls_incentive_group(incentive_group_id) ;

-- Comments
comment on table ls_ilr_incentive is '(S) [] The table stores the Incentives Amounts for ILRs.';
comment on column ls_ilr_incentive.ilr_incentive_id is 'System-assigned identifier of a Lessee Incentive.';
comment on column ls_ilr_incentive.ilr_id is 'System-assigned identifier of a Lessee ILR.';
comment on column ls_ilr_incentive.revision is 'System-assigned identifier of a Lessee ILR revision.';
comment on column ls_ilr_incentive.incentive_group_id is 'System-assigned identifier of an Incentive group.';
comment on column ls_ilr_incentive.date_incurred is 'The date on which the cost incurred.';
comment on column ls_ilr_incentive.amount is 'Cost incurred';
comment on column ls_ilr_incentive.description is 'Description of the ILR indirect cost group assoiation.';
comment on column ls_ilr_incentive.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_ilr_incentive.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--ID sequence
CREATE SEQUENCE ls_ilr_incentive_seq
  MINVALUE 1
  START WITH 1
  INCREMENT BY 1
  CACHE 20 ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4238, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_05_ilr_incentive_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;