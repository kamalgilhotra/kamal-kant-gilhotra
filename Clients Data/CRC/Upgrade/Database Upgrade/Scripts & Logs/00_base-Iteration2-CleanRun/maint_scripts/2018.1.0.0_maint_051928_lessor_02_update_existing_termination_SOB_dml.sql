/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051928_lessor_02_update_existing_termination_SOB_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/26/2018  Alex Healey    set initial set of books for all existing records to be 1, since we based it off the first set of books
||============================================================================
*/
UPDATE lsr_ilr_termination_st_df
SET set_of_books_id = 1;

UPDATE lsr_ilr_termination_op
SET set_of_books_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11023, 0, 2018, 1, 0, 0, 51928, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051928_lessor_02_update_existing_termination_SOB_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		   