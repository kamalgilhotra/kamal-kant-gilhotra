/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043037_sys_add_os_return_value_column_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/11/2015 Charlie Shilling need os_return_value column on pp_job_request for JobServer
||============================================================================
*/

ALTER TABLE pp_job_request 
ADD os_return_value NUMBER(22,0) NULL; 

COMMENT ON COLUMN pp_job_request.os_return_value IS 'This is the value that the executable returned to the operating system using the Windows API function ExitProcess().';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2364, 0, 2015, 1, 0, 0, 043229, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043037_sys_add_os_return_value_column_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;