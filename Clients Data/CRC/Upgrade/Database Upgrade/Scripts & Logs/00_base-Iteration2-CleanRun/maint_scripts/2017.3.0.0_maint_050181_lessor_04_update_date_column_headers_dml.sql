/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050181_lessor_04_update_date_column_headers_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/05/2018 Andrew Hill       Update date column headers to specify format
||============================================================================
*/

UPDATE pp_import_column
SET DESCRIPTION = 'Est In Svc Date (yyyymm)'
WHERE column_name = 'est_in_svc_date'
AND import_type_id = 502;

UPDATE pp_import_column
SET DESCRIPTION = 'Payment Term Date (yyyymm)'
WHERE column_name = 'payment_term_date'
and import_type_id = 502;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4279, 0, 2017, 3, 0, 0, 50181, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050181_lessor_04_update_date_column_headers_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 