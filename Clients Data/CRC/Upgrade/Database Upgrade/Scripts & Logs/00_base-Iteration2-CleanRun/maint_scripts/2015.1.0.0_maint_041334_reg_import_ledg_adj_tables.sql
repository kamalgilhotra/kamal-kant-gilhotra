/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041334_reg_import_ledg_adj_tables.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   11/18/2014 Shane Ward    		Add Import for Ledger Adjustments
||========================================================================================
*/

/****************************
**  Stage and Arch Table
****************************/
create table REG_IMPORT_LEDGER_ADJ_STG(IMPORT_RUN_ID      number(22, 0) not null,
                                       LINE_ID            number(22, 0) not null,
                                       ERROR_MESSAGE      varchar2(4000) null,
                                       VERSION_ID         number(22, 0) null,
                                       VERSION_ID_XLATE   varchar2(254) null,
                                       REG_COMPANY_ID     number(22, 0) null,
                                       COMPANY_ID_XLATE   varchar2(254) null,
                                       REG_ACCT_ID        number(22, 0) null,
                                       REG_ACCT_ID_XLATE  varchar2(254) null,
                                       GL_MONTH           varchar2(254) null,
                                       ADJUST_MONTH       varchar2(254) null,
                                       REVERSE_MONTH      varchar2(254) null,
                                       "comment"          varchar2(2000) null,
                                       ADJUST_AMOUNT      varchar2(254) null,
                                       gl_source_id          VARCHAR2(254) NULL,
                                       adj_source_id          VARCHAR2(254) NULL,
                                       USER_ID            varchar2(35) null,
                                       TIME_STAMP         varchar2(35) null);

alter table REG_IMPORT_LEDGER_ADJ_STG ADD constraint REG_IMPORT_LEDGER_ADJ_STG_PK PRIMARY KEY(IMPORT_RUN_ID,
                                                                                              LINE_ID);
																							  
comment on table reg_import_ledger_adj_Stg is 'Staging table for importing Regulatory Ledger Adjustments (Historic and Forecast).';
comment on column reg_import_ledger_adj_stg.import_run_id is 'Identifier of Import Run.';
comment on column reg_import_ledger_adj_stg.LINE_ID is 'Line Number of Import Run.';
comment on column reg_import_ledger_adj_stg.ERROR_MESSAGE is 'Error Message from Import Validation.';
comment on column reg_import_ledger_adj_stg.VERSION_ID is 'Version ID (Historic or Forecast)';
comment on column reg_import_ledger_adj_stg.VERSION_ID_XLATE is 'Translate column for Version Id';
comment on column reg_import_ledger_adj_stg.REG_COMPANY_ID is 'Reg Company Id.';
comment on column reg_import_ledger_adj_stg.COMPANY_ID_XLATE is 'Translate column for Reg Company Id.';
comment on column reg_import_ledger_adj_stg.REG_ACCT_ID is 'Reg Account Id.';
comment on column reg_import_ledger_adj_stg.REG_ACCT_ID_XLATE is 'Translate column for Reg Account.';
comment on column reg_import_ledger_adj_stg.GL_MONTH is 'General Ledger Month (From Month).';
comment on column reg_import_ledger_adj_stg.ADJUST_MONTH is 'Adjustment Month (To Month).';
comment on column reg_import_ledger_adj_stg.REVERSE_MONTH is 'Month of Adjustment Reversal.';
comment on column reg_import_ledger_adj_stg."comment"  is 'Note created by user to describe why adjustment.';
comment on column reg_import_ledger_adj_stg.ADJUST_AMOUNT is 'Adjustment Amount.';
comment on column reg_import_ledger_adj_stg.gl_source_id is 'Reg Source for GL Month.';
comment on column reg_import_ledger_adj_stg.adj_source_id is 'Reg Source for Adjust Month.';


create table REG_IMPORT_LEDGER_ADJ_STG_ARC(IMPORT_RUN_ID      number(22, 0) not null,
                                       LINE_ID            number(22, 0) not null,
                                       VERSION_ID         number(22, 0) null,
                                       VERSION_ID_XLATE   varchar2(254) null,
                                       REG_COMPANY_ID     number(22, 0) null,
                                       COMPANY_ID_XLATE   varchar2(254) null,
                                       REG_ACCT_ID        number(22, 0) null,
                                       REG_ACCT_ID_XLATE  varchar2(254) null,
                                       GL_MONTH           varchar2(254) null,
                                       ADJUST_MONTH       varchar2(254) null,
                                       REVERSE_MONTH      varchar2(254) null,
                                       "comment"      varchar2(2000) null,
                                       ADJUST_AMOUNT      varchar2(254) null,
                                       gl_source_id          VARCHAR2(254) NULL,
                                       adj_source_id          VARCHAR2(254) NULL,
                                       USER_ID            varchar2(35) null,
                                       TIME_STAMP         varchar2(35) null);
									   
comment on table reg_import_ledger_adj_stg_arc is 'Archive table for importing Regulatory Ledger Adjustments (Historic and Forecast).';
comment on column reg_import_ledger_adj_stg_arc.import_run_id is 'Identifier of Import Run.';
comment on column reg_import_ledger_adj_stg_arc.LINE_ID is 'Line Number of Import Run.';
comment on column reg_import_ledger_adj_stg_arc.VERSION_ID is 'Version ID (Historic or Forecast)';
comment on column reg_import_ledger_adj_stg_arc.VERSION_ID_XLATE is 'Translate column for Version Id';
comment on column reg_import_ledger_adj_stg_arc.REG_COMPANY_ID is 'Reg Company Id.';
comment on column reg_import_ledger_adj_stg_arc.COMPANY_ID_XLATE is 'Translate column for Reg Company Id.';
comment on column reg_import_ledger_adj_stg_arc.REG_ACCT_ID is 'Reg Account Id.';
comment on column reg_import_ledger_adj_stg_arc.REG_ACCT_ID_XLATE is 'Translate column for Reg Account.';
comment on column reg_import_ledger_adj_stg_arc.GL_MONTH is 'General Ledger Month (From Month).';
comment on column reg_import_ledger_adj_stg_arc.ADJUST_MONTH is 'Adjustment Month (To Month).';
comment on column reg_import_ledger_adj_stg_arc.REVERSE_MONTH is 'Month of Adjustment Reversal.';
comment on column reg_import_ledger_adj_stg_arc."comment"  is 'Note created by user to describe why adjustment.';
comment on column reg_import_ledger_adj_stg_arc.ADJUST_AMOUNT is 'Adjustment Amount.';
comment on column reg_import_ledger_adj_stg_arc.gl_source_id is 'Reg Source for GL Month.';
comment on column reg_import_ledger_adj_stg_arc.adj_source_id is 'Reg Source for Adjust Month.';


/****************************
**  Translation Views
****************************/
CREATE OR REPLACE VIEW reg_fcst_version_lookup AS SELECT forecast_version_id version_id, description, long_description FROM reg_forecast_version;
CREATE OR REPLACE VIEW reg_hist_version_lookup AS SELECT historic_version_id version_id, long_description FROM reg_historic_version;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2051, 0, 2015, 1, 0, 0, 41334, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041334_reg_import_ledg_adj_tables.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;