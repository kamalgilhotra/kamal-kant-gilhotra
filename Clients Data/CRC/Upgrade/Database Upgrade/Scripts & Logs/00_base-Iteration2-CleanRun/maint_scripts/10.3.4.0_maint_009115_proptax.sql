/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009115_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   01/12/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Maint 9115 - Create a temp table to store the calculated balances of ledger items being added.
--
create global temporary table PWRPLANT.PT_TEMP_LEDGER_ADDS
(
 property_tax_ledger_id     number(22,0) not null,
 tax_year                   number(22,0) not null,
 beginning_book_balance     number(22,2) default 0 not null,
 book_additions             number(22,2) default 0 not null,
 book_retirements           number(22,2) default 0 not null,
 book_transfers_adj         number(22,2) default 0 not null,
 ending_book_balance        number(22,2) default 0 not null,
 cwip_balance               number(22,2) default 0 not null,
 allocated_tax_basis        number(22,2) default 0 not null,
 allocated_accum_ore        number(22,2) default 0 not null,
 unit_cost_amount           number(22,2) default 0 not null,
 prop_tax_balance           number(22,8) default 0 not null,
 associated_reserve         number(22,8) default 0 not null,
 calculated_reserve         number(22,8) default 0 not null,
 allocated_tax_reserve      number(22,8) default 0 not null,
 final_reserve              number(22,8) default 0 not null,
 escalated_prop_tax_balance number(22,8) default 0 not null,
 escalated_prop_tax_reserve number(22,8) default 0 not null,
 cost_approach_value        number(22,8) default 0 not null,
 market_value               number(22,8) default 0 not null,
 quantity                   number(22,8),
 quantity_factored          number(22,8)
) on commit preserve rows;

create unique index PWRPLANT.PT_TEMP_LEDGER_ADDS_PK
   on PWRPLANT.PT_TEMP_LEDGER_ADDS (PROPERTY_TAX_LEDGER_ID, TAX_YEAR);

--grant all on pwrplant.pt_temp_ledger_adds to pwrplant_role_dev;
--create or replace public synonym pt_temp_ledger_adds for pwrplant.pt_temp_ledger_adds;

begin
   DBMS_STATS.SET_TABLE_STATS('PWRPLANT', 'PT_TEMP_LEDGER_ADDS', '', null, null, 1, 1, 36, null);
   DBMS_STATS.LOCK_TABLE_STATS('PWRPLANT', 'PT_TEMP_LEDGER_ADDS');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (90, 0, 10, 3, 4, 0, 9115, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009115_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
