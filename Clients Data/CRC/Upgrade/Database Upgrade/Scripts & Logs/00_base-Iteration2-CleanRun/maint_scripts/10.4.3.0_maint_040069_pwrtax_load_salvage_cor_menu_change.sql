/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040069_pwrtax_load_salvage_cor_menu_change.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 10/07/2014 Anand Rajashekar    Menu changes for Load Salvage/COR
||========================================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID)
values
   ('powertax', 'load_book_depreciation', TO_DATE('07-OCT-14', 'DD-MON-RR'), 'PWRPLANT', 'Load Book Depreciation',
    'uo_tax_depr_act_wksp_load_salvage_cor', 'Load Book Depreciation', 1);

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'load_book_depreciation'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'deferred_input_load_depr';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1498, 0, 10, 4, 3, 0, 40069, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040069_pwrtax_load_salvage_cor_menu_change.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
