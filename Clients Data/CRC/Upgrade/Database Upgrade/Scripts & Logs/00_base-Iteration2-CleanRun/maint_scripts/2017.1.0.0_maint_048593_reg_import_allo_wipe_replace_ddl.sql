 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048593_reg_import_allo_wipe_replace_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 07/18/2017 Shane "C" Ward	Add column and ability to Wipe and Replace Allo Factors RMS
 ||============================================================================
 */


ALTER TABLE reg_import_allo_factor_stg ADD wipe_and_replace VARCHAR2(30);
COMMENT ON COLUMN reg_import_allo_factor_stg.wipe_and_replace IS '"Yes" or "No" value specifies whether to wipe and replace allocation factors on import';
ALTER TABLE reg_import_allo_factor_stg_arc ADD wipe_and_replace VARCHAR2(30);
COMMENT ON COLUMN reg_import_allo_factor_stg_arc.wipe_and_replace IS '"Yes" or "No" value specifies whether to wipe and replace allocation factors on import';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3591, 0, 2017, 1, 0, 0, 48593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048593_reg_import_allo_wipe_replace_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;