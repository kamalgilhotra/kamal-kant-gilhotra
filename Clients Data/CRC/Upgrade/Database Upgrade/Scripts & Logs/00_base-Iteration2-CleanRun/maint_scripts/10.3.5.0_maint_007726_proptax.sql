/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007726_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   05/09/2012 Julia Breuer   Point Release
||============================================================================
*/
--
-- Create a temp table to store the ledger items being transferred.
--
create global temporary table PT_TEMP_LEDGER_TRANSFERS
(
 FROM_LEDGER_ID       number(22,0) not null,
 TO_LEDGER_ID         number(22,0),
 TAX_YEAR             number(22,0) not null,
 BEG_BAL_AMOUNT       number(22,2) default 0 not null,
 ADDITIONS_AMOUNT     number(22,2) default 0 not null,
 RETIREMENTS_AMOUNT   number(22,2) default 0 not null,
 TRANSFERS_AMOUNT     number(22,2) default 0 not null,
 END_BAL_AMOUNT       number(22,2) default 0 not null,
 CWIP_AMOUNT          number(22,2) default 0 not null,
 TAX_BASIS_AMOUNT     number(22,2) default 0 not null,
 RESERVE_AMOUNT       number(22,2) default 0 not null,
 FROM_RESERVE_AMOUNT  number(22,2) default 0 not null,
 NOTES                varchar2(4000),
 STATE_ID             char(18) not null,
 PROP_TAX_LOCATION_ID number(22,0),
 PARCEL_ID            number(22,0) not null,
 PROP_TAX_COMPANY_ID  number(22,0) not null,
 COMPANY_ID           number(22,0) not null,
 GL_ACCOUNT_ID        number(22,0) not null,
 BUS_SEGMENT_ID       number(22,0) not null,
 UTILITY_ACCOUNT_ID   number(22,0),
 PROPERTY_TAX_TYPE_ID number(22,0) not null,
 VINTAGE              number(22,0),
 VINTAGE_MONTH        number(22,0),
 CLASS_CODE_VALUE1    varchar2(254),
 CLASS_CODE_VALUE2    varchar2(254),
 USER_INPUT           number(22,0) not null,
 LEDGER_DETAIL_ID     number(22,0)
) on commit preserve rows;

create unique index PT_TEMP_LEDGER_TRANSFERS_PK
   on PT_TEMP_LEDGER_TRANSFERS (FROM_LEDGER_ID);

--
-- Create a temp table to store the preallo ledger items being transferred.
--
create global temporary table PT_TEMP_PREALLO_TRANSFERS
(
 FROM_PREALLO_ID      number(22,0) not null,
 TO_PREALLO_ID        number(22,0),
 TAX_YEAR             number(22,0) not null,
 BEG_BAL_AMOUNT       number(22,2) default 0 not null,
 ADDITIONS_AMOUNT     number(22,2) default 0 not null,
 RETIREMENTS_AMOUNT   number(22,2) default 0 not null,
 TRANSFERS_AMOUNT     number(22,2) default 0 not null,
 END_BAL_AMOUNT       number(22,2) default 0 not null,
 CWIP_AMOUNT          number(22,2) default 0 not null,
 NOTES                varchar2(4000),
 STATE_ID             char(18) not null,
 PROP_TAX_LOCATION_ID number(22,0),
 PARCEL_ID            number(22,0) not null,
 PROP_TAX_COMPANY_ID  number(22,0) not null,
 COMPANY_ID           number(22,0) not null,
 GL_ACCOUNT_ID        number(22,0) not null,
 BUS_SEGMENT_ID       number(22,0) not null,
 UTILITY_ACCOUNT_ID   number(22,0),
 PROPERTY_TAX_TYPE_ID number(22,0) not null,
 DEPR_GROUP_ID        number(22,0),
 VINTAGE              number(22,0),
 VINTAGE_MONTH        number(22,0),
 CLASS_CODE_VALUE1    varchar2(254),
 CLASS_CODE_VALUE2    varchar2(254),
 USER_INPUT           number(22,0) not null,
 LEDGER_DETAIL_ID     number(22,0)
) on commit preserve rows;

create unique index PT_TEMP_PREALLO_TRANSFERS_PK
   on PT_TEMP_PREALLO_TRANSFERS (FROM_PREALLO_ID);

begin
DBMS_STATS.SET_TABLE_STATS ('PWRPLANT','PT_TEMP_LEDGER_TRANSFERS','',NULL,NULL,1,1,36,NULL);
end;
/

begin
DBMS_STATS.SET_TABLE_STATS ('PWRPLANT','PT_TEMP_PREALLO_TRANSFERS','',NULL,NULL,1,1,36,NULL);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (145, 0, 10, 3, 5, 0, 7726, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_007726_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
