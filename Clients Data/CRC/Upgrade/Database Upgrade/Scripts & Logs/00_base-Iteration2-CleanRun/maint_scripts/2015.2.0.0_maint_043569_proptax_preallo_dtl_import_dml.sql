/*
||==========================================================================================
|| Application: PowerPlan
|| Module: Property Tax
|| File Name:   maint_043569_proptax_preallo_dtl_import_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2    04/10/2015  Graham Miller    Add ability to import ledger detail on preallo impt
||==========================================================================================
*/


--Insert into PP_IMPORT_COLUMN for new ledger detail field options
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_body_type', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Body Type (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_construction_type', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Construction Type (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null,
null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_description_1', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Description 1 (Detail)', null, 0, 1, 'varchar2(254)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_description_2', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Description 2 (Detail)', null, 0, 1, 'varchar2(254)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_description_3', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Description 3 (Detail)', null, 0, 1, 'varchar2(254)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_external_code', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'External Code (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_fuel_type', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Fuel Type (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_gross_weight', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Gross Weight (Detail)', null, 0, 1, 'number(22,2)', null, null, 1, null, null, null, null)
;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_group_code', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Group Code (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9, 'ld_height',
 to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Height (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_key_number', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Key Number (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_lease_end', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Lease End Date (Detail)', null, 0, 1, 'date', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_lease_number', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Lease Number (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null)
;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_lease_start', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Lease Start Date (Detail)', null, 0, 1, 'date', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9, 'ld_length',
 to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Length (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_license_expiration', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'License Expiration (Detail)', null, 0, 1, 'date', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_license_number', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'License Number (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_license_weight', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'License Weight (Detail)', null, 0, 1, 'number(22,2)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9, 'ld_make',
to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Make (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9, 'ld_model',
to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Model (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_model_year', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Model Year (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9, 'ld_notes',
to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Notes (Detail)', null, 0, 1, 'varchar2(4000)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_primary_driver', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Primary Driver (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_serial_number', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Serial Number (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_source_system', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Source System (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null,
null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_stories', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Stories (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ld_unit_number', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Unit Number (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9, 'ld_width',
to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Width (Detail)', null, 0, 1, 'varchar2(50)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (9,
'ledger_detail_id', to_date('2015-04-09 10:49:50', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Ledger Detail ID', 'ledger_detail_xlate', 0, 1, 'number(22,0)', 'pt_ledger_detail', null,
1, null, null, null, null) ;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2575, 0, 2015, 2, 0, 0, 043569, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043569_proptax_preallo_dtl_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;