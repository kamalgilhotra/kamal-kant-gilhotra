/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_08_deferred_rent_stg_cols_ddl.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding trans types for deferred rent
||========================================================================================
*/

alter table ls_ilr_stg add deferred_rent_balance number(22,2);
alter table ls_ilr_asset_stg add alloc_deferred_rent number(22,2);

COMMENT ON column LS_ILR_STG.DEFERRED_RENT_BALANCE is 'Total Deferred Rent Amount from prior revision used in calculation of asset cost.';
COMMENT ON column LS_ILR_ASSET_STG.ALLOC_DEFERRED_RENT is 'Deferred rent amount allocated to each asset record. Used in capital cost calculation.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3574, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_08_deferred_rent_stg_cols_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;