/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047037_lease_import_vp_var_formula_comp_amount_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/11/2017 Anand R          create the staging table(s) needed for CF amounts import
||============================================================================
*/

--create the staging table to hold the imported values
create table ls_import_vp_var_comp_amt(
  import_run_id number(22,0) not null,
  line_id number(22,0) not null,
  error_message varchar2(4000),
  time_stamp date,
  user_id varchar2(18),
  formula_component_id number(22,0),
  formula_component_xlate varchar2(254),
  ilr_id number(22,0),
  ilr_xlate varchar2(254),
  ls_asset_id number(22,0),
  ls_asset_xlate varchar2(254),
  incurred_month_number number(6,0),
  amount varchar2(254)
);

alter table ls_import_vp_var_comp_amt
add constraint ls_import_vp_var_comp_amt_pk
primary key (import_run_id, line_id)
using index tablespace PWRPLANT_IDX;

alter table ls_import_vp_var_comp_amt
add constraint ls_import_vp_var_comp_amt_fk1
foreign key (import_run_id)
references pp_import_run (import_run_id);

comment on table ls_import_vp_var_comp_amt is '(S) [06] The Lease Import VP Var Comp Amt table is an API table used to import Variable Formula Component Amounts.';
comment on column ls_import_vp_var_comp_amt.import_run_id is 'System-assigned ID that specifies the import run that this record was imported in.';
comment on column ls_import_vp_var_comp_amt.line_id is 'System-assigned line number for this import run.';
comment on column ls_import_vp_var_comp_amt.error_message is 'Error messages resulting from data validation in the import process.';
comment on column ls_import_vp_var_comp_amt.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_import_vp_var_comp_amt.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_import_vp_var_comp_amt.formula_component_id is 'The internal Formula Component id within PowerPlant.';
comment on column ls_import_vp_var_comp_amt.formula_component_xlate is 'Translation field for determining the Formula Component.';
comment on column ls_import_vp_var_comp_amt.ilr_id is 'System assigned identifier for an ILR';
comment on column ls_import_vp_var_comp_amt.ilr_xlate is 'Translation field for determining the ILR.';
comment on column ls_import_vp_var_comp_amt.ls_asset_id is 'System assigned identifier for a Leased asset';
comment on column ls_import_vp_var_comp_amt.ls_asset_xlate is 'Translation field for determining the Leased asset';
comment on column ls_import_vp_var_comp_amt.incurred_month_number is 'The date on which the amount for the variable component formula was incurred';
comment on column ls_import_vp_var_comp_amt.amount is 'The amount we are loading for the specific Formula Component.';

--create the archive staging table
create table ls_import_vp_var_comp_amt_arch(
  import_run_id number(22,0) not null,
  line_id number(22,0) not null,
  time_stamp date,
  user_id varchar2(18),
  formula_component_id number(22,0),
  formula_component_xlate varchar2(254),
  ilr_id number(22,0),
  ilr_xlate varchar2(254),
  ls_asset_id number(22,0),
  ls_asset_xlate varchar2(254),
  incurred_month_number number(6,0),
  amount varchar2(254)
);

comment on table ls_import_vp_var_comp_amt_arch is '(S) [06] The Lease Import VP Var Comp Amt table is an API table used to import Variable Formula Component Amounts.';
comment on column ls_import_vp_var_comp_amt_arch.import_run_id is 'System-assigned ID that specifies the import run that this record was imported in.';
comment on column ls_import_vp_var_comp_amt_arch.line_id is 'System-assigned line number for this import run.';
comment on column ls_import_vp_var_comp_amt_arch.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_import_vp_var_comp_amt_arch.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_import_vp_var_comp_amt_arch.formula_component_id is 'The internal Formula Component id within PowerPlant.';
comment on column ls_import_vp_var_comp_amt_arch.formula_component_xlate is 'Translation field for determining the Formula Component.';
comment on column ls_import_vp_var_comp_amt_arch.ilr_id is 'System assigned identifier for an ILR';
comment on column ls_import_vp_var_comp_amt_arch.ilr_xlate is 'Translation field for determining the ILR.';
comment on column ls_import_vp_var_comp_amt_arch.ls_asset_id is 'System assigned identifier for a Leased asset';
comment on column ls_import_vp_var_comp_amt_arch.ls_asset_xlate is 'Translation field for determining the Leased asset';
comment on column ls_import_vp_var_comp_amt_arch.incurred_month_number is 'The date on which the amount for the variable component formula was incurred';
comment on column ls_import_vp_var_comp_amt_arch.amount is 'The amount we are loading for the specific Formula Component.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3494, 0, 2017, 1, 0, 0, 47037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047037_lease_import_vp_var_formula_comp_amount_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;