/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041319_pcm_fp_search_dml.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	11/25/2014 Ryan Oliveria		Altering dynamic filters for FP Search
||========================================================================================
*/

update PP_DYNAMIC_FILTER
   set NOT_RETRIEVE_IMMEDIATE = 1,
   	   DW = 'dw_pp_funding_project_filter',
	   DW_ID = 'work_order_id',
	   DW_DESCRIPTION = 'work_order_number',
	   DW_ID_DATATYPE = 'N',
	   INPUT_TYPE = 'dw',
	   SQLS_COLUMN_EXPRESSION = 'work_order_control.work_order_id'
 where FILTER_ID = 213;

insert into PP_DYNAMIC_FILTER_TYPE (FILTER_TYPE_ID, DESCRIPTION) values (1, 'Dynamic Filters');
insert into PP_DYNAMIC_FILTER_TYPE (FILTER_TYPE_ID, DESCRIPTION) values (2, 'Class Codes');
insert into PP_DYNAMIC_FILTER_TYPE (FILTER_TYPE_ID, DESCRIPTION) values (3, 'Justifications');

update PP_DYNAMIC_FILTER_SAVED_VALUES
   set FILTER_TYPE_ID = 1
 where FILTER_TYPE_ID is null;

update PP_DYN_FILTER_SAVED_VALUES_DW
   set FILTER_TYPE_ID = 1
 where FILTER_TYPE_ID is null;

update PP_DYNAMIC_FILTER_VALUES
   set FILTER_TYPE_ID = 1
 where FILTER_TYPE_ID is null;

 update PP_DYNAMIC_FILTER_VALUES_DW
   set FILTER_TYPE_ID = 1
 where FILTER_TYPE_ID is null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2069, 0, 2015, 1, 0, 0, 041319, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041319_pcm_fp_search_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;