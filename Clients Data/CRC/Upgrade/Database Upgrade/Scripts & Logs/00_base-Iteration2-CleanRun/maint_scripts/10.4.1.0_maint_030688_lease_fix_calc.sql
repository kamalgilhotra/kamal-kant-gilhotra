/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030688_lease_fix_calc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/10/2013 Brandon Beck   Point Release
||============================================================================
*/

drop table LS_ILR_SCHEDULE_STG;
drop table LS_ILR_ASSET_SCHEDULE_STG;
drop table LS_ILR_ASSET_SCHEDULE_CALC_STG;

create global temporary table LS_ILR_SCHEDULE_STG
(
 ID               number(22,0),
 ILR_ID           number(22,0) not null,
 REVISION         number(4,0),
 MONTH            date not null,
 AMOUNT           number(22,2),
 RESIDUAL_AMOUNT  number(22,2),
 TERM_PENALTY     number(22,2),
 BPO_PRICE        number(22,2),
 PREPAY_SWITCH    number(1,0),
 RATE             number(22,12),
 NPV              number(22,12),
 PROCESS_NPV      number(1,0),
 PAYMENT_MONTH    number(1,0),
 MONTHS_TO_ACCRUE number(2,0)
) on commit preserve rows;

alter table LS_ILR_SCHEDULE_STG
   add constraint LS_ILR_SCHEDULE_STG_PK
       primary key (ILR_ID, MONTH);

create global temporary table LS_ILR_ASSET_SCHEDULE_STG
(
 ID                 number(22,0),
 ILR_ID             number(22,0) not null,
 REVISION           number(4,0),
 LS_ASSET_ID        number(22,0) not null,
 MONTH              date not null,
 AMOUNT             number(22,2),
 RESIDUAL_AMOUNT    number(22,2),
 TERM_PENALTY       number(22,2),
 BPO_PRICE          number(22,2),
 PREPAY_SWITCH      number(1,0),
 PAYMENT_MONTH      number(1,0),
 MONTHS_TO_ACCRUE   number(2,0),
 RATE               number(22,12),
 NPV                number(22,12),
 CURRENT_LEASE_COST number(22,2),
 BEG_CAPITAL_COST   number(22,2),
 END_CAPITAL_COST   number(22,2),
 BEG_OBLIGATION     number(22,2),
 END_OBLIGATION     number(22,2),
 BEG_LT_OBLIGATION  number(22,2),
 END_LT_OBLIGATION  number(22,2),
 UNPAID_BALANCE     number(22,2),
 INTEREST_ACCRUAL   number(22,2),
 PRINCIPAL_ACCRUAL  number(22,2),
 INTEREST_PAID      number(22,2),
 PRINCIPAL_PAID     number(22,2)
) on commit preserve rows;

alter table LS_ILR_ASSET_SCHEDULE_STG
   add constraint LS_ILR_ASSET_SCHEDULE_STG_PK
       primary key (ILR_ID, LS_ASSET_ID, MONTH);

create table LS_ILR_ASSET_SCHEDULE_CALC_STG
(
 ID                 number(22,0),
 ILR_ID             number(22,0),
 REVISION           number(4,0),
 LS_ASSET_ID        number(22,0),
 MONTH              date,
 AMOUNT             number(22,2),
 RESIDUAL_AMOUNT    number(22,2),
 TERM_PENALTY       number(22,2),
 BPO_PRICE          number(22,2),
 PREPAY_SWITCH      number(1,0),
 PAYMENT_MONTH      number(1,0),
 MONTHS_TO_ACCRUE   number(2,0),
 RATE               number(22,12),
 NPV                number(22,12),
 CURRENT_LEASE_COST number(22,8),
 BEG_CAPITAL_COST   number(22,8),
 END_CAPITAL_COST   number(22,8),
 BEG_OBLIGATION     number(22,8),
 END_OBLIGATION     number(22,8),
 BEG_LT_OBLIGATION  number(22,8),
 END_LT_OBLIGATION  number(22,8),
 UNPAID_BALANCE     number(22,8),
 INTEREST_ACCRUAL   number(22,8),
 PRINCIPAL_ACCRUAL  number(22,8),
 INTEREST_PAID      number(22,8),
 PRINCIPAL_PAID     number(22,8),
 PENNY_ROUNDER      number(22,2),
 PENNY_PRIN_ROUNDER number(22,2),
 PENNY_INT_ROUNDER  number(22,2),
 PENNY_END_ROUNDER  number(22,2),
 PRINCIPAL_ACCRUED  number(22,8),
 INTEREST_ACCRUED   number(22,8),
 MONTHS_ACCRUED     number(4,0),
 PRIN_ROUND_ACCRUED number(22,2),
 INT_ROUND_ACCRUED  number(22,2)
);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (470, 0, 10, 4, 1, 0, 30688, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030688_lease_fix_calc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
