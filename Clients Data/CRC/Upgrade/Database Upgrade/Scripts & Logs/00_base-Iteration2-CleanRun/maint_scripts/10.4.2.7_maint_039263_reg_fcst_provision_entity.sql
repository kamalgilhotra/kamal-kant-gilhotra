/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039263_reg_fcst_provision_entity.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.2.7 07/29/2014 Ryan Oliveria       Need entity on family member map
||========================================================================================
*/

alter table REG_TA_FAMILY_MEMBER_MAP add ENTITY_ID number(22,0);

alter table REG_TA_FAMILY_MEMBER_MAP
   add constraint R_REG_TA_FAMILY_MEMBER_MAP7
      foreign key (ENTITY_ID)
      references TAX_ACCRUAL_ENTITY (ENTITY_ID);

update REG_TA_FAMILY_MEMBER_MAP set ENTITY_ID = 10 where ENTITY_ID is null;

comment on column REG_TA_FAMILY_MEMBER_MAP.ENTITY_ID is 'System-assigned identifier of the taxing entity (e.g. Federal, Illinois, D.C., etc) underlying the deferred.';

update PPBASE_MENU_ITEMS
   set LABEL = replace(LABEL, 'Tax Provision', 'Fcst Tax Prov')
 where MODULE = 'REG'
   and MENU_IDENTIFIER in ('FCST_TAX_PROV_SETUP', 'FCST_DEF_TAX', 'FCST_TAX_PROV');

update PPBASE_WORKSPACE
   set LABEL = replace(LABEL, 'Tax Provision', 'Fcst Tax Prov')
 where MODULE = 'REG'
   and WORKSPACE_IDENTIFIER in ('uo_reg_fcst_prov_int', 'uo_reg_fcst_ta_def_int', 'uo_reg_fcst_ta_setup');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1293, 0, 10, 4, 2, 7, 39263, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039263_reg_fcst_provision_entity.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;