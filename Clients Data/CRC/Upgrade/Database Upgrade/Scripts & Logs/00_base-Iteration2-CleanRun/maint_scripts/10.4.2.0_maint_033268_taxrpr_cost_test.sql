/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033268_taxrpr_cost_test.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   12/17/2013 Alex P.
||============================================================================
*/

alter table REPAIR_UNIT_CODE add QUANTITY_VS_COST number(2, 0);
comment on column REPAIR_UNIT_CODE.QUANTITY_VS_COST is 'A flag indicating whether quantity or cost is used in a Blended method during Unit Of Property testing. This feature is typically used for general property.';

alter table REPAIR_WORK_ORDER_SEGMENTS add QUANTITY_VS_COST number(2, 0);
comment on column REPAIR_WORK_ORDER_SEGMENTS.QUANTITY_VS_COST is 'A flag indicating whether quantity or cost is used in a Blended method during Unit Of Property testing. This feature is typically used for general property.';

alter table REPAIR_WO_SEG_REPORTING add QUANTITY_VS_COST number(2, 0);
comment on column REPAIR_WO_SEG_REPORTING.QUANTITY_VS_COST is 'A flag indicating whether quantity or cost is used in a Blended method during Unit Of Property testing. This feature is typically used for general property.';

create table REPAIR_LOOKUP_QTY_COST
(
 ID          number(22,0) not null,
 DESCRIPTION varchar2(35) not null,
 TIME_STAMP  date,
 USER_ID     varchar2(18)
);

alter table REPAIR_LOOKUP_QTY_COST
  add constraint PK_RLQC_ID
      primary key (ID)
      using index tablespace PWRPLANT_IDX;

comment on table REPAIR_LOOKUP_QTY_COST is '(F) [18] The Repair Lookup Qty Cost is a lookup table for quantity vs cost flags.';
comment on column REPAIR_LOOKUP_QTY_COST.ID is 'System-assigned identifier of Quantity or Cost values.';
comment on column REPAIR_LOOKUP_QTY_COST.DESCRIPTION is 'Description of Quantity or Cost values.';
comment on column REPAIR_LOOKUP_QTY_COST.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column REPAIR_LOOKUP_QTY_COST.USER_ID is 'Standard System-assigned user id used for audit purposes.';

insert into REPAIR_LOOKUP_QTY_COST
   (ID, DESCRIPTION, TIME_STAMP, USER_ID)
values
   (1, 'Quantity', null, null);

insert into REPAIR_LOOKUP_QTY_COST
   (ID, DESCRIPTION, TIME_STAMP, USER_ID)
values
   (2, 'Cost', null, null);

insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION, REQUIRED_COLUMN_EXPRESSION)
   select max(ID) + 1,
          'repair_unit_code',
          'quantity_vs_cost',
          'uo_rpr_config_wksp_unit_code.dw_details',
          'Test UOPs on Qty vs Cost',
          'describe("quantity_vs_cost.visible")=''1'''
     from PP_REQUIRED_TABLE_COLUMN;

alter table REPAIR_UNIT_CODE
   add constraint FK_RUC_QTY_VS_COST
       foreign key (QUANTITY_VS_COST)
       references REPAIR_LOOKUP_QTY_COST;

alter table REPAIR_WO_SEG_REPORTING
   add constraint FK_RWSR_QTY_VS_COST
       foreign key (QUANTITY_VS_COST)
       references REPAIR_LOOKUP_QTY_COST;

alter table WO_TAX_EXPENSE_TEST add COST_EXPENSE_PERCENT NUMBER(28,8);
comment on column WO_TAX_EXPENSE_TEST.COST_EXPENSE_PERCENT is 'Threshold expense percent used when Blended method is used for General Property and testing is performed on cost instead of quantity.';

insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'cost_expense_percent',
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Cost Expense Percent'
     from PP_REQUIRED_TABLE_COLUMN;

create table REPAIR_WO_RUC_EXCEPTION
(
 WORK_ORDER_ID       number(22, 0) not null,
 REPAIR_UNIT_CODE_ID number(22, 0) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table REPAIR_WO_RUC_EXCEPTION
   add constraint PK_REPAIR_WO_RUC_EXCEPTION
       primary key (WORK_ORDER_ID, REPAIR_UNIT_CODE_ID)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_WO_RUC_EXCEPTION
   add constraint FK_RWRE_WORK_ORDER_ID
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL;

alter table REPAIR_WO_RUC_EXCEPTION
   add constraint FK_RWRE_REPAIR_UNIT_CODE_ID
       foreign key (REPAIR_UNIT_CODE_ID)
       references REPAIR_UNIT_CODE;

comment on table REPAIR_WO_RUC_EXCEPTION is '(F) [18] REPAIR WO RUC EXCEPTION is a mapping table of work orders and repair unit codes that need to be excluded from Tax Repairs testing in the Blended method of general property.';
comment on column REPAIR_WO_RUC_EXCEPTION.WORK_ORDER_ID is 'System-assigned identifier of a particular work order.';
comment on column REPAIR_WO_RUC_EXCEPTION.REPAIR_UNIT_CODE_ID is 'System-assigned identifier of a particular repair unit code.';
comment on column REPAIR_WO_RUC_EXCEPTION.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column REPAIR_WO_RUC_EXCEPTION.USER_ID is 'Standard System-assigned user id used for audit purposes.';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 4
 where MODULE = 'REPAIRS'
   and MENU_IDENTIFIER = 'menu_wksp_wo_balance'
   and ITEM_ORDER = 3;

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REPAIRS', 'menu_wksp_wo_ruc_exception', null, null, 'WO-RUC Exceptions',
    'uo_rpr_wo_wksp_ruc_exception', 'Work Order - Repair Unit Code Exclusions');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REPAIRS', 'menu_wksp_wo_ruc_exception', null, null, 2, 3, 'WO-RUC Exeptions',
    'Work Order - Repair Unit Code Exclusions', null, 'menu_wksp_wo_ruc_exception', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (814, 0, 10, 4, 2, 0, 33268, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033268_taxrpr_cost_test.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
