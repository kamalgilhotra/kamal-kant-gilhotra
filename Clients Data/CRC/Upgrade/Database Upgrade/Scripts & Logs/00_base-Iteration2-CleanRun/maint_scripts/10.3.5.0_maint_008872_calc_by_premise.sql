/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008872_calc_by_premise.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   03/15/2012 Brandon Beck   Point Release
||============================================================================
*/

alter table REIMB_PREMISES           add PREMISE_GROUP number(22,0) default 1;

alter table REIMB_BILL_REVIEW_DETAIL add PREMISE_GROUP number(22,0) default 1;

alter table REIMB_REFUND_TYPE
   add CALC_BY_PREMISE_GROUP number(1, 0) default 0;

alter table REIMB_BILL_REVIEW_DETAIL drop primary key drop index;

alter table REIMB_BILL_REVIEW_DETAIL
   add constraint REIMB_BILL_REVIEW_DETAIL_PK
       primary key (REVIEW_ID, STATISTIC_TYPE_ID, REIMB_RATE_SCH_ID,
                    REFUND_TITLE, PREMISE_GROUP)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (125, 0, 10, 3, 5, 0, 8872, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_008872_calc_by_premise.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
