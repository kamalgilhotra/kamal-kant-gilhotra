/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048888_lessor_03_ilr_import_renewal_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/18/2017 Shane "C" Ward		Set up Lessor ILR Renewal Import Tables
||============================================================================
*/
CREATE TABLE LSR_IMPORT_ILR_RENEWAL
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     time_stamp                 DATE NULL,
	 error_message				varchar2(4000) null,
     user_id                    VARCHAR2(18) NULL,
     ilr_id                     NUMBER(22, 0) NULL,
     ilr_id_xlate               VARCHAR2(254) NULL,
	 revision					number(22,0) null,
     ilr_renewal_prob_id        NUMBER(22, 0) NULL,
     ilr_renewal_prob_id_xlate  VARCHAR2(35) NULL,
     renewal_notice             NUMBER(22, 0) NULL,
     renewal_frequency_id       NUMBER(22, 0) NULL,
     renewal_frequency_id_xlate VARCHAR2(35) NULL,
     renewal_number_of_terms    NUMBER(22, 0) NULL,
     renewal_amount_per_term    NUMBER(22, 2) NULL,
     ilr_renewal_id             NUMBER(22, 0) NULL,
     ilr_renewal_option_id      NUMBER(22, 0) NULL
  );

COMMENT ON TABLE LSR_IMPORT_ILR_RENEWAL IS '(S)  [06]
The Lessor Import ILR Renewal table is an API table used to import Lessor ILRs Renewal Options.';

COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.ilr_id_xlate IS 'Translation field for internal ILR id within PowerPlant .';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.revision IS 'Revision of ILR.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.ilr_renewal_prob_id IS 'ID of Renewal probability of ILR';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.ilr_renewal_prob_id_xlate IS 'Translation of ID of Renewal probability of ILR';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.renewal_notice IS 'Number of Months needed for notice prior to end of regular lease term for renewal option';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.renewal_frequency_id IS 'Frequency of renewal payments, e.g., Monthly, Quarterly, Semi-Annually, Annually';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.renewal_frequency_id_xlate IS 'Translation for Frequency of renewal payments, e.g., Monthly, Quarterly, Semi-Annually, Annually';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.renewal_number_of_terms IS 'Number of terms associated with renewal option';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.renewal_amount_per_term IS 'Amount associated with Renewal Option'; 


CREATE TABLE LSR_IMPORT_ILR_RENEWAL_ARCHIVE
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     time_stamp                 DATE NULL,
	 error_message varchar2(4000) NULL,
     user_id                    VARCHAR2(18) NULL,
     ilr_id                     NUMBER(22, 0) NULL,
     ilr_id_xlate               VARCHAR2(254) NULL,
	 revision					number(22,0) null,
     ilr_renewal_prob_id        NUMBER(22, 0) NULL,
     ilr_renewal_prob_id_xlate  VARCHAR2(35) NULL,
     renewal_notice             NUMBER(22, 0) NULL,
     renewal_frequency_id       NUMBER(22, 0) NULL,
     renewal_frequency_id_xlate VARCHAR2(35) NULL,
     renewal_number_of_terms    NUMBER(22, 0) NULL,
     renewal_amount_per_term    NUMBER(22, 2) NULL,
     ilr_renewal_id             NUMBER(22, 0) NULL,
     ilr_renewal_option_id      NUMBER(22, 0) NULL
  );
  
COMMENT ON TABLE LSR_IMPORT_ILR_RENEWAL_ARCHIVE IS '(S)  [06]
The Lessor Import ILR Renewal Archive table is an API table used to Archive imported Lessor ILRs Renewal Options.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_id_xlate IS 'Translation field for internal ILR id within PowerPlant .';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.revision IS 'Revision of ILR.';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_renewal_prob_id IS 'ID of Renewal probability of ILR';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_renewal_prob_id_xlate IS 'Translation of ID of Renewal probability of ILR';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_notice IS 'Number of Months needed for notice prior to end of regular lease term for renewal option';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_frequency_id IS 'Frequency of renewal payments, e.g., Monthly, Quarterly, Semi-Annually, Annually';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_frequency_id_xlate IS 'Translation for Frequency of renewal payments, e.g., Monthly, Quarterly, Semi-Annually, Annually';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_number_of_terms IS 'Number of terms associated with renewal option';
COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_amount_per_term IS 'Amount associated with Renewal Option'; 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3800, 0, 2017, 1, 0, 0, 48888, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048888_lessor_03_ilr_import_renewal_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;