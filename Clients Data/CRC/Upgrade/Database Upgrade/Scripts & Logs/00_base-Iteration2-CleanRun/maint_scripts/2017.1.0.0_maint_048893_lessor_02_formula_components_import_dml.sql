/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048893_lessor_02_formula_components_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/31/2017 Alex Healey    populate all of the rows needed for the new lessor import
||============================================================================
*/
--this will insert the new import type we need
INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name)
VALUES      ( 510,
              'Add: Var Payment Index/Rate Amounts',
              'Add: Variable Payment Index/Rate Component Amounts',
              'LSR_IMPORT_VP_INDEX_COMP_AMTS',
              'LSR_IMPRT_VP_INDX_CMP_AMTS_ARC',
              1,
              'nvo_lsr_logic_import' );

--relate the import type to the lease subsystem
INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_type_id,
             import_subsystem_id)
VALUES      ( 510,
              14 );

--insert the columns we will need to use in the import
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      ( 510,
              'formula_component_id',
              'Formula Component Identifier',
              'formula_component_xlate',
              1,
              1,
              'number(22,0)',
              'lsr_formula_component',
              'formula_component_id' );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      ( 510,
              'effective_date',
              'Effective Date',
              NULL,
              1,
              1,
              'varchar2(254)',
              NULL,
              NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      ( 510,
              'amount',
              'Amount',
              NULL,
              0,
              1,
              'varchar2(254)',
              NULL,
              NULL );

--add the new lookup for the Formula Component ID into the database
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name,
			 lookup_values_alternate_sql)
VALUES      ( 1100,
              'Index/Rate Component.Description',
              'The passed in value corresponds to the LSR Formula Component: Description field for Index/Rate Formula Components. Translate to the Formula Component ID using the Description column on the LSR Formula Component table.', 
              'formula_component_id',
              '( select f.formula_component_id from lsr_formula_component f where upper( trim( <importfield> ) ) = upper( trim( f.description ) ) )',
              0,
              'lsr_formula_component',
              'description' ,
			  'select description from lsr_formula_component where component_type_id = 1');

--associate the column in the import with the lookup translation
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 510,
              'formula_component_id',
              1100 );

--add the import template (based on max ID + 1) and the fields for the template

--add the specific template into the DB
INSERT INTO PP_IMPORT_TEMPLATE
            (import_template_id,
             import_type_id,
             description,
             long_description,
             created_by,
             created_date,
             do_update_with_add)
VALUES      ( ( SELECT Max(import_template_id) + 1
                FROM   PP_IMPORT_TEMPLATE ),
              510,
              'Add: Var Payment Index/Rate Amounts',
              'Index/Rates Formula Component Amounts Add',
              'PWRPLANT',
              sysdate,
              1 );

--add the fields for the template into the DB
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
                FROM   PP_IMPORT_TEMPLATE
                WHERE  import_type_id = 510 AND
                       description = 'Add: Var Payment Index/Rate Amounts' ),
              1,
              510,
              'formula_component_id',
              1100 );

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
                FROM   PP_IMPORT_TEMPLATE
                WHERE  import_type_id = 510 AND
                       description = 'Add: Var Payment Index/Rate Amounts' ),
              2,
              510,
              'effective_date',
              NULL );

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
                FROM   PP_IMPORT_TEMPLATE
                WHERE  import_type_id = 510 AND
                       description = 'Add: Var Payment Index/Rate Amounts' ),
              3,
              510,
              'amount',
              NULL ); 

			  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3965, 0, 2017, 1, 0, 0, 48893, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048893_lessor_02_formula_components_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
