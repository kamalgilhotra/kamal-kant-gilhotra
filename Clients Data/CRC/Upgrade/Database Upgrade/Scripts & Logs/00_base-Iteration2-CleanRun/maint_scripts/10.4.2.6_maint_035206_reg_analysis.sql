/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_035206_reg_analysis.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/19/2014 Ryan Oliveria  Reworking the Reg Analysis workspace
||========================================================================================
*/

--*********** REG_ANALYSIS_GROUP *************
create table REG_ANALYSIS_GROUP
(
 RA_GROUP_ID      number(22,0) not null,
 USER_ID          varchar2(18),
 TIME_STAMP       date,
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254) not null,
 STATUS_CODE_ID   number(22,0) not null
);

comment on table REG_ANALYSIS_GROUP is 'This table defines an analysis grouping of detail analyses the user can run, e.g. Rate Base Analysis, Revenues Analysis.';
comment on column REG_ANALYSIS_GROUP.RA_GROUP_ID is 'System assigned identifier of the regulatory analysis group.';
comment on column REG_ANALYSIS_GROUP.DESCRIPTION is 'Description of the regulatory analysis group.';
comment on column REG_ANALYSIS_GROUP.LONG_DESCRIPTION is 'Long description of the regulatory analysis group.';
comment on column REG_ANALYSIS_GROUP.STATUS_CODE_ID is 'Active/Inactive indicator.  Active = 1: this RA group can be used. Inactive = 2: this RA group cannot be used.  Default is Active';
comment on column REG_ANALYSIS_GROUP.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ANALYSIS_GROUP.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

alter table REG_ANALYSIS_GROUP add constraint PK_REG_ANALYSIS_GROUP primary key (RA_GROUP_ID) using index tablespace pwrplant_idx;

PROMPT alter table REG_ANALYSIS_GROUP add constraint R_REG_ANALYSIS_GROUP1 foreign key
alter table REG_ANALYSIS_GROUP
   add constraint R_REG_ANALYSIS_GROUP1
       foreign key (STATUS_CODE_ID)
       references STATUS_CODE (STATUS_CODE_ID);

insert into REG_ANALYSIS_GROUP ("RA_GROUP_ID", "DESCRIPTION", "LONG_DESCRIPTION", "STATUS_CODE_ID", "USER_ID", TIME_STAMP) values (1, 'Revenue Requirements', 'Revenue Requirements Analysis', 1, null, null) ;
insert into REG_ANALYSIS_GROUP ("RA_GROUP_ID", "DESCRIPTION", "LONG_DESCRIPTION", "STATUS_CODE_ID", "USER_ID", TIME_STAMP) values (2, 'Rate Base/Investment', 'Rate Base/Investment Analysis', 2, null, null) ;
insert into REG_ANALYSIS_GROUP ("RA_GROUP_ID", "DESCRIPTION", "LONG_DESCRIPTION", "STATUS_CODE_ID", "USER_ID", TIME_STAMP) values (3, 'Account Trends', 'Account Trends Analysis', 1, null, null) ;
insert into REG_ANALYSIS_GROUP ("RA_GROUP_ID", "DESCRIPTION", "LONG_DESCRIPTION", "STATUS_CODE_ID", "USER_ID", TIME_STAMP) values (4, 'Cost of Capital', 'Cost of Capital Analysis', 1, null, null) ;
insert into REG_ANALYSIS_GROUP ("RA_GROUP_ID", "DESCRIPTION", "LONG_DESCRIPTION", "STATUS_CODE_ID", "USER_ID", TIME_STAMP) values (5, 'Returns', 'Returns Analysis', 1, null, null) ;


--*********** REG_ANALYSIS_PRESENTATION *************
create table REG_ANALYSIS_PRESENTATION
(
 RA_PRESENT_ID    number(22,0) not null,
 USER_ID          varchar2(18),
 TIME_STAMP       date,
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254) not null,
 STATUS_CODE_ID   number(22,0) not null
);

comment on table REG_ANALYSIS_PRESENTATION is 'This table controls which charts, reports, and data is presented and what interaction is allowed.';
comment on column REG_ANALYSIS_PRESENTATION.RA_PRESENT_ID is 'System assigned identifier of the regulatory analysis presentation.';
comment on column REG_ANALYSIS_PRESENTATION.DESCRIPTION is 'Description of the regulatory analysis presentation.';
comment on column REG_ANALYSIS_PRESENTATION.LONG_DESCRIPTION is 'Long description of the regulatory analysis presentation.';
comment on column REG_ANALYSIS_PRESENTATION.STATUS_CODE_ID is 'Active/Inactive indicator.  Active = 1: this RA group can be used. Inactive = 2: this RA group cannot be used.  Default is Active';
comment on column REG_ANALYSIS_PRESENTATION.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ANALYSIS_PRESENTATION.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';


PROMPT alter table REG_ANALYSIS_PRESENTATION add constraint PK_REG_ANALYSIS_PRESENTATION primary key
alter table REG_ANALYSIS_PRESENTATION
   add constraint PK_REG_ANALYSIS_PRESENTATION
       primary key (RA_PRESENT_ID)
       using index tablespace PWRPLANT_IDX;

PROMPT alter table REG_ANALYSIS_PRESENTATION add constraint R_REG_PRESENTATION1 foreign key
alter table REG_ANALYSIS_PRESENTATION
   add constraint R_REG_ANALYSIS_PRESENTATION1
       foreign key (STATUS_CODE_ID)
       references STATUS_CODE (STATUS_CODE_ID);

--* Insert default presentations (copies of the detail table)
insert into REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
   values (1, 'Case to Case', 'Revenue Requirements - Case to Case', 1);

insert into REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
   values (2, 'Year to Year', 'Revenue Requirements - Year to Year', 1);

insert into REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
   values (3, 'Case to Case', 'Returns - Case to Case', 1);

insert into REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
   values (4, 'Year to Year', 'Returns - Year to Year', 1);

insert into REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
   values (5, 'Account Trends Over Time', 'Account Trends - Account Trends Over Time', 1);

insert into REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
   values (6, 'Account Disallowances', 'Account Trends - Account Disallowances', 1);

insert into REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
   values (7, 'Expense Cost Trends Analysis', 'Account Trends - Expense Cost Trends Analysis', 1);


--*********** REG_ANALYSIS_DETAIL *************
create table REG_ANALYSIS_DETAIL
(
 RA_DETAIL_ID     number(22,0) not null,
 USER_ID          varchar2(18),
 TIME_STAMP       date,
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254) not null,
 STATUS_CODE_ID   number(22,0) not null,
 RA_GROUP_ID      number(22,0) not null,
 RA_PRESENT_ID    number(22,0) not null
);

comment on table REG_ANALYSIS_DETAIL is 'This table defines the detailed analysis.  The definition includes the analysis group, analysis option and parameters, and the types of analysis results�charts, reports and data.';
comment on column REG_ANALYSIS_DETAIL.RA_DETAIL_ID is 'System assigned identifier of the regulatory analysis detail item.';
comment on column REG_ANALYSIS_DETAIL.DESCRIPTION is 'Description of the regulatory analysis group.';
comment on column REG_ANALYSIS_DETAIL.LONG_DESCRIPTION is 'Long description of the regulatory analysis detail.';
comment on column REG_ANALYSIS_DETAIL.STATUS_CODE_ID is 'Active/Inactive indicator.  Active = 1: this RA detail item can be used. Inactive = 2: this RA detail item cannot be used.  Default is Active';
comment on column REG_ANALYSIS_DETAIL.RA_GROUP_ID is 'System assigned identifier of the regulatory analysis group that this detail analysis item is related to.';
comment on column REG_ANALYSIS_DETAIL.RA_PRESENT_ID is 'System assigned identifier of the regulatory analysis presentation this detail analysis item uses.';
comment on column REG_ANALYSIS_DETAIL.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ANALYSIS_DETAIL.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';


PROMPT alter table REG_ANALYSIS_DETAIL add constraint pk_REG_ANALYSIS_DETAIL primary key
alter table REG_ANALYSIS_DETAIL
   add constraint PK_REG_ANALYSIS_DETAIL
       primary key (RA_DETAIL_ID)
       using index tablespace PWRPLANT_IDX;

PROMPT alter table REG_ANALYSIS_DETAIL add constraint R_REG_ANALYSIS_DETAIL1 foreign key
alter table REG_ANALYSIS_DETAIL
   add constraint R_REG_ANALYSIS_DETAIL1
       foreign key (STATUS_CODE_ID)
       references STATUS_CODE (STATUS_CODE_ID);

PROMPT alter table REG_ANALYSIS_DETAIL add constraint R_REG_ANALYSIS_DETAIL2 foreign key
alter table REG_ANALYSIS_DETAIL
   add constraint R_REG_ANALYSIS_DETAIL2
       foreign key (RA_GROUP_ID)
       references REG_ANALYSIS_GROUP (RA_GROUP_ID);

PROMPT alter table REG_ANALYSIS_DETAIL add constraint R_REG_ANALYSIS_DETAIL3 foreign key
alter table REG_ANALYSIS_DETAIL
   add constraint R_REG_ANALYSIS_DETAIL3
       foreign key (RA_PRESENT_ID)
       references REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID);

insert into REG_ANALYSIS_DETAIL (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
   values (1, 'Case to Case', 'Revenue Requirements - Case to Case', 1, 1, 1);

insert into REG_ANALYSIS_DETAIL (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
   values (2, 'Year to Year', 'Revenue Requirements - Year to Year', 1, 1, 2);

insert into REG_ANALYSIS_DETAIL (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
   values (3, 'Case to Case', 'Returns - Case to Case', 1, 5, 3);

insert into REG_ANALYSIS_DETAIL (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
   values (4, 'Year to Year', 'Returns - Year to Year', 1, 5, 4);

insert into REG_ANALYSIS_DETAIL (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
   values (5, 'Account Trends Over Time', 'Account Trends - Account Trends Over Time', 1, 3, 5);

insert into REG_ANALYSIS_DETAIL (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
   values (6, 'Account Disallowances', 'Account Trends - Account Disallowances', 1, 3, 6);

insert into REG_ANALYSIS_DETAIL (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
   values (7, 'Expense Cost Trends Analysis', 'Account Trends - Expense Cost Trends Analysis', 1, 3, 7);


--*********** REG_ANALYSIS_OPTION *************
create table REG_ANALYSIS_OPTION
(
 RA_OPTION_ID     number(22,0) not null,
 USER_ID          varchar2(18),
 TIME_STAMP       date,
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254) not null,
 STATUS_CODE_ID   number(22,0) not null,
 OPTION_UO_NAME   varchar2(35)
);

comment on table REG_ANALYSIS_OPTION is 'This table defines the analysis options used by a detail analysis.';
comment on column REG_ANALYSIS_OPTION.RA_OPTION_ID is 'System assigned identifier of the regulatory analysis option.';
comment on column REG_ANALYSIS_OPTION.DESCRIPTION is 'Description of the regulatory analysis option.';
comment on column REG_ANALYSIS_OPTION.LONG_DESCRIPTION is 'Long description of the regulatory analysis option.';
comment on column REG_ANALYSIS_OPTION.STATUS_CODE_ID is 'Active/Inactive indicator.  Active = 1: this RA group can be used. Inactive = 2: this RA group cannot be used.  Default is Active';
comment on column REG_ANALYSIS_OPTION.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ANALYSIS_OPTION.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_ANALYSIS_OPTION.OPTION_UO_NAME is 'The user object used to set the value of this option.';


PROMPT alter table REG_ANALYSIS_OPTION add constraint PK_REG_ANALYSIS_OPTION
alter table REG_ANALYSIS_OPTION
   add constraint PK_REG_ANALYSIS_OPTION
       primary key (RA_OPTION_ID)
       using index tablespace PWRPLANT_IDX;

PROMPT alter table REG_ANALYSIS_OPTION add constraint R_REG_ANALYSIS_OPTION1 foreign key
alter table REG_ANALYSIS_OPTION
   add constraint R_REG_ANALYSIS_OPTION1
       foreign key (STATUS_CODE_ID)
       references STATUS_CODE (STATUS_CODE_ID);

insert into REG_ANALYSIS_OPTION (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
   values (1, 'Case', 'Case', 1, 'uo_reg_analysis_op_case');

insert into REG_ANALYSIS_OPTION (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
   values (2, 'Cases', 'Cases', 1, 'uo_reg_analysis_op_cases');

insert into REG_ANALYSIS_OPTION (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
   values (3, 'Year', 'Year', 1, 'uo_reg_analysis_op_year');

insert into REG_ANALYSIS_OPTION (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
   values (4, 'Time Span', 'Time Span', 1, 'uo_reg_analysis_op_time_span');

insert into REG_ANALYSIS_OPTION (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
   values (5, 'Regulatory Ledger', 'Regulatory Ledger', 1, 'uo_reg_analysis_op_ledger');

insert into REG_ANALYSIS_OPTION (RA_OPTION_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, OPTION_UO_NAME)
   values (6, 'Account Scope', 'Account Scope', 1, 'uo_reg_analysis_op_account_scope');



--*********** REG_ANALYSIS_DETAIL_OPTION *************
create table REG_ANALYSIS_DETAIL_OPTION
(
 RA_DETAIL_ID number(22,0) not null,
 RA_OPTION_ID number(22,0) not null,
 USER_ID      varchar2(18),
 TIME_STAMP   date
);

comment on table REG_ANALYSIS_DETAIL_OPTION is 'This tables saves the last five analyses performed by each user.  The oldest one is automatically rolled off every time an analysis is performed.';
comment on column REG_ANALYSIS_DETAIL_OPTION.RA_OPTION_ID is 'System assigned identifier of the regulatory analysis option.';
comment on column REG_ANALYSIS_DETAIL_OPTION.RA_DETAIL_ID is 'System assigned identifier of the regulatory analysis detail item.';
comment on column REG_ANALYSIS_DETAIL_OPTION.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ANALYSIS_DETAIL_OPTION.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

PROMPT alter table REG_ANALYSIS_DETAIL_OPTION add constraint PK_REG_ANALYSIS_DETAIL_OPTION primary key
alter table REG_ANALYSIS_DETAIL_OPTION
   add constraint PK_REG_ANALYSIS_DETAIL_OPTION
      primary key (RA_OPTION_ID, RA_DETAIL_ID)
      using index tablespace PWRPLANT_IDX;

PROMPT alter table REG_ANALYSIS_DETAIL_OPTION add constraint R_REG_ANALYSIS_DETAIL_OPTION1 foreign key
alter table REG_ANALYSIS_DETAIL_OPTION
   add constraint R_REG_ANALYSIS_DETAIL_OPTION1
      foreign key (RA_DETAIL_ID)
      references REG_ANALYSIS_DETAIL (RA_DETAIL_ID);

PROMPT alter table REG_ANALYSIS_DETAIL_OPTION add constraint R_REG_ANALYSIS_DETAIL_OPTION2 foreign key
alter table REG_ANALYSIS_DETAIL_OPTION
   add constraint R_REG_ANALYSIS_DETAIL_OPTION2
       foreign key (RA_OPTION_ID)
       references REG_ANALYSIS_OPTION (RA_OPTION_ID);


insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (1, 2);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (1, 3);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (2, 1);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (2, 4);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (3, 2);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (3, 3);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (4, 1);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (4, 4);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (5, 4);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (5, 5);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (7, 4);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (7, 5);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (7, 6);



--*********** REG_ANALYSIS_CHARTS *************
create table REG_ANALYSIS_CHARTS
(
 RA_PRESENT_ID  number (22,0) not null,
 CHART_ID       number (22,0) not null,
 USER_ID        varchar2(18),
 TIME_STAMP     date,
 SHOW_DATA_FLAG number(22,0) not null,
 DEFAULT_FLAG   number(22, 0) not null
);

comment on table REG_ANALYSIS_CHARTS is 'This table relates, or controls, which charts (REG_CHARTS) are available for an analysis presentation.';
comment on column REG_ANALYSIS_CHARTS.RA_PRESENT_ID is 'System assigned identifier of the regulatory analysis presentation.';
comment on column REG_ANALYSIS_CHARTS.CHART_ID is 'System assigned identifier of the chart definition.';
comment on column REG_ANALYSIS_CHARTS.SHOW_DATA_FLAG is 'Controls if the underlying data from the chart SQL can be displayed by user in a grid for exporting, etc. 0 = No; 1 = Yes. Default value is No.';
comment on column REG_ANALYSIS_CHARTS.DEFAULT_FLAG is 'Controls if this chart be displayed by default when the analysis is run. 0 = No; 1 = Yes. Default value is No.';
comment on column REG_ANALYSIS_CHARTS.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ANALYSIS_CHARTS.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';


PROMPT alter table REG_ANALYSIS_CHARTS add constraint PK_REG_ANALYSIS_CHARTS primary key
alter table REG_ANALYSIS_CHARTS
   add constraint PK_REG_ANALYSIS_CHARTS
      primary key (RA_PRESENT_ID, CHART_ID)
      using index tablespace PWRPLANT_IDX;

PROMPT alter table REG_ANALYSIS_CHARTS add constraint R_REG_ANALYSIS_CHARTS1 foreign key
alter table REG_ANALYSIS_CHARTS
   add constraint R_REG_ANALYSIS_CHARTS1
       foreign key (RA_PRESENT_ID)
       references REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID);


insert into REG_ANALYSIS_CHARTS (RA_PRESENT_ID, CHART_ID, SHOW_DATA_FLAG, DEFAULT_FLAG) values (1, 4, 0, 1);
insert into REG_ANALYSIS_CHARTS (RA_PRESENT_ID, CHART_ID, SHOW_DATA_FLAG, DEFAULT_FLAG) values (2, 4, 0, 1);
insert into REG_ANALYSIS_CHARTS (RA_PRESENT_ID, CHART_ID, SHOW_DATA_FLAG, DEFAULT_FLAG) values (3, 3, 0, 1);
insert into REG_ANALYSIS_CHARTS (RA_PRESENT_ID, CHART_ID, SHOW_DATA_FLAG, DEFAULT_FLAG) values (4, 3, 0, 1);



--* Account Trend Charts
insert into REG_CHART (CHART_ID, DESCRIPTION, LONG_DESCRIPTION, URL, SQL_COMMAND)
   values (8, 'Annualized Account Type Trend', 'Annualized Account Type Trend', null, null);

insert into REG_CHART (CHART_ID, DESCRIPTION, LONG_DESCRIPTION, URL, SQL_COMMAND)
   values (9, 'Annualized Sub Account Type Trend', 'Annualized Sub Account Type Trend', null, null);

insert into REG_CHART (CHART_ID, DESCRIPTION, LONG_DESCRIPTION, URL, SQL_COMMAND)
   values (10, 'Adjustment Amount by Account Type', 'Adjustment Amount by Account Type', null, null);

insert into REG_ANALYSIS_CHARTS (RA_PRESENT_ID, CHART_ID, SHOW_DATA_FLAG, DEFAULT_FLAG) values (5, 8, 0, 1);
insert into REG_ANALYSIS_CHARTS (RA_PRESENT_ID, CHART_ID, SHOW_DATA_FLAG, DEFAULT_FLAG) values (5, 9, 0, 1);
insert into REG_ANALYSIS_CHARTS (RA_PRESENT_ID, CHART_ID, SHOW_DATA_FLAG, DEFAULT_FLAG) values (5, 10, 0, 1);

insert into REG_CHART (CHART_ID, DESCRIPTION, LONG_DESCRIPTION, URL, SQL_COMMAND)
   values (11, 'Expense Cost Trends', 'Expense Cost Trends', null, null);

insert into REG_ANALYSIS_CHARTS (RA_PRESENT_ID, CHART_ID, SHOW_DATA_FLAG, DEFAULT_FLAG) values (7, 11, 0, 1);




--*********** REG_ANALYSIS_REPORTS *************
create table REG_ANALYSIS_REPORTS
(
 RA_PRESENT_ID  number(22,0) not null,
 REPORT_ID      number(22,0) not null,
 USER_ID        varchar2(18),
 TIME_STAMP     date,
 SHOW_DATA_FLAG number(22,0),
 DEFAULT_FLAG   number(22,0)
);

comment on table REG_ANALYSIS_REPORTS is 'This table relates, or controls, which charts (REG_CHARTS) are available for an analysis presentation.';
comment on column REG_ANALYSIS_REPORTS.ra_present_id is 'System assigned identifier of the regulatory analysis presentation.';
comment on column REG_ANALYSIS_REPORTS.report_id is 'System assigned identifier of the report definition.';
comment on column REG_ANALYSIS_REPORTS.show_data_flag is 'Controls if the underlying data from the chart SQL can be displayed by user in a grid for exporting, etc. 0 = No; 1 = Yes. Default value is No.';
comment on column REG_ANALYSIS_REPORTS.default_flag is 'Controls if this chart be displayed by default when the analysis is run. 0 = No; 1 = Yes. Default value is No.';
comment on column REG_ANALYSIS_REPORTS.user_id is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ANALYSIS_REPORTS.time_stamp is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';


PROMPT alter table REG_ANALYSIS_REPORTS add constraint PK_REG_ANALYSIS_REPORTS primary key
alter table REG_ANALYSIS_REPORTS
   add constraint PK_REG_ANALYSIS_REPORTS
       primary key (RA_PRESENT_ID, REPORT_ID)
       using index tablespace PWRPLANT_IDX;

PROMPT alter table REG_ANALYSIS_REPORTS add constraint r_reg_ANALYSIS_REPORTS1 foreign key
alter table REG_ANALYSIS_REPORTS
   add constraint R_REG_ANALYSIS_REPORTS1
       foreign key (RA_PRESENT_ID)
       references REG_ANALYSIS_PRESENTATION (RA_PRESENT_ID);

PROMPT alter table REG_ANALYSIS_REPORTS add constraint R_REG_ANALYSIS_REPORTS2 foreign key
alter table REG_ANALYSIS_REPORTS
   add constraint R_REG_ANALYSIS_REPORTS2
       foreign key (REPORT_ID)
       references PP_REPORTS (REPORT_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1145, 0, 10, 4, 2, 6, 35206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_035206_reg_analysis.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
