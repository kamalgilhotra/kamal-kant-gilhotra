/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044084_mobile_add_alerts_tables_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   06/12/2015 Daniel Motter  Creation
||============================================================================
*/

/* Drop tables for re-runability */
begin
	execute immediate 'drop table mobile_disp_inpts_mapping';
	execute immediate 'drop table mobile_alerts';
	execute immediate 'drop table mobile_display';
	execute immediate 'drop table mobile_inputs';
exception
	when others then
        dbms_output.put_line('Error: '||sqlerrm);
end;
/

/* Create mobile_display table */
create table mobile_display (
	display_id	number(22,0)	not null,
	description	varchar2(35)	not null,
	time_stamp	date			null,
	user_id		varchar2(15)	null
);

alter table mobile_display
    add constraint pk_mobile_display primary key (
        display_id
    )
    using index
        tablespace pwrplant_idx;

comment on table mobile_display is 'Contains the different details screens that may appear when a user takes action on an alert';
comment on column mobile_display.display_id is 'The number used by the system to uniquely identify each details screen to display';
comment on column mobile_display.description is 'The description of the details screen that the user sees';
comment on column mobile_display.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column mobile_display.user_id is 'Standard System-assigned user id used for audit purposes.';

/* Create mobile_inputs table */
create table mobile_inputs (
	input_id	number(22,0)	not null,
	description	varchar2(35)	not null,
	time_stamp	date			null,
	user_id		varchar2(15)	null
);

alter table mobile_inputs
    add constraint pk_mobile_inputs primary key (
        input_id
    )
    using index
        tablespace pwrplant_idx;
		
comment on table mobile_inputs is 'Contains the different input actions users can take on an alert';
comment on column mobile_inputs.input_id is 'The number used by the system to uniquely identify each action';
comment on column mobile_inputs.description is 'The description of the action that the user sees';
comment on column mobile_inputs.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column mobile_inputs.user_id is 'Standard System-assigned user id used for audit purposes.';

/* Create mobile_disp_inpts_mapping table */
create table mobile_disp_inpts_mapping (
	display_id	number(22,0)	not null,
	input_id	number(22,0)	not null,
	time_stamp	date			null,
	user_id		varchar2(15)	null
);
alter table mobile_disp_inpts_mapping
    add constraint pk_mobile_disp_inpts_mapping primary key (
        display_id, input_id
    )
    using index
        tablespace pwrplant_idx;
		
alter table mobile_disp_inpts_mapping
    add constraint fk_mobile_disp_mapping foreign key (
        display_id
    ) references mobile_display (
        display_id
    );
	
alter table mobile_disp_inpts_mapping
    add constraint fk_mobile_inpts_mapping foreign key (
        input_id
    ) references mobile_inputs (
        input_id
    );
	
comment on table mobile_disp_inpts_mapping is 'Contains the mappings for which mobile input actions are associated with each mobile details screen';
comment on column mobile_disp_inpts_mapping.display_id is 'The number used by the system to uniquely identify each details screen to display';
comment on column mobile_disp_inpts_mapping.input_id is 'The number used by the system to uniquely identify each action';
comment on column mobile_disp_inpts_mapping.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column mobile_disp_inpts_mapping.user_id is 'Standard System-assigned user id used for audit purposes.';

/* Create mobile_alerts table */
create table mobile_alerts (
	verify_id	number(22,0)	not null,
	display_id	number(22,0)	not null,
	input_id	number(22,0)	not null,
	time_stamp	date			null,
	user_id		varchar2(15)	null
);
alter table mobile_alerts
    add constraint pk_mobile_alerts primary key (
        verify_id, display_id, input_id
    )
    using index
        tablespace pwrplant_idx;
		
alter table mobile_alerts
    add constraint fk_mobile_alerts_verify foreign key (
        verify_id
    ) references pp_verify (
        verify_id
    );
	
alter table mobile_alerts
    add constraint fk_mobile_alerts_display foreign key (
        display_id
    ) references mobile_display (
        display_id
    );
	
alter table mobile_alerts
    add constraint fk_mobile_alerts_input foreign key (
        input_id
    ) references mobile_inputs (
        input_id
    );
	
comment on table mobile_alerts is 'Contains which details screen and input options should display for each alert on mobile';
comment on column mobile_alerts.verify_id is 'The id of the pp alert';
comment on column mobile_alerts.display_id is 'The id of the details screen to display for this alert';
comment on column mobile_alerts.input_id is 'The id of the input to display for this alert';
comment on column mobile_alerts.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column mobile_alerts.user_id is 'Standard System-assigned user id used for audit purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2617, 0, 2015, 2, 0, 0, 044084, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044084_mobile_add_alerts_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;