/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010600_cr_PP_SEQUENCE_CLEAR_CACHE.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    07/05/2012 Joseph King    Point Release
||============================================================================
*/

create or replace function PP_SEQUENCE_CLEAR_CACHE(A_SEQUENCE varchar2) return number as
   type NUMBERS is table of number;

   INC  number;
   CS   number;
   RAC  varchar2(5);
   BURN NUMBERS;

begin
   select INCREMENT_BY, CACHE_SIZE
     into INC, CS
     from ALL_SEQUENCES
    where SEQUENCE_NAME = UPPER(trim(A_SEQUENCE));
   select "VALUE" into RAC from V$PARAMETER where "NAME" = 'cluster_database';

   if UPPER(RAC) = 'FALSE' then
      /* On non-RAC databases, no need to clear the cache as only 1 cache is being used */
      return 0;
   else
      /* RAC database -- could potentially have multiple caches -- purge the current instance to make sure we have
      fresh values */

      if INC <> 1 then
         /* Expect sequences with increment_by = 1 only. */
         return - 1;
      else
         /* If they are using a cache, burn that many numbers */
         if CS > 0 then
            select CRDETAIL.NEXTVAL bulk collect into BURN from DUAL connect by level <= CS;
            return BURN(CS);
         else
            /* No need to clear */
            return 0;
         end if;
      end if;
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (166, 0, 10, 3, 5, 0, 10600, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010600_cr_PP_SEQUENCE_CLEAR_CACHE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
