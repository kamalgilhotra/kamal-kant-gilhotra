/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044812_reg_to_excel_case_status_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 09/10/2015 Sarah Byers    Logging for RMS Export
||============================================================================
*/

insert into reg_case_process (
	reg_case_process_id, description, order_num)
values (
	11, 'Case Export', 11);

insert into reg_case_process_steps (
	reg_case_process_id, reg_process_step_id, description, order_num)
values (
	11, 1, 'Export Case Working Model', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2859, 0, 2015, 2, 0, 0, 044812, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044812_reg_to_excel_case_status_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;