/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047566_lease_01_mla_import_add_contract_currency_ddl.sql
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2017.1.0.0 06/08/2017 Jared Watkins  Add the contract currency columns to the ls_import_lease table
||============================================================================
*/

alter table ls_import_lease
add contract_currency_id number(22,0);
alter table ls_import_lease
add contract_currency_xlate varchar2(254);

comment on column ls_import_lease.contract_currency_id is 'The Contract Currency for the MLA.';
comment on column ls_import_lease.contract_currency_xlate is 'Translation field for determining Contract Currency.';

alter table ls_import_lease_archive
add contract_currency_id number(22,0);
alter table ls_import_lease_archive
add contract_currency_xlate varchar2(254);

comment on column ls_import_lease_archive.contract_currency_id is 'The Contract Currency for the MLA.';
comment on column ls_import_lease_archive.contract_currency_xlate is 'Translation field for determining Contract Currency.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3520, 0, 2017, 1, 0, 0, 47566, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047566_lease_01_mla_import_add_contract_currency_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;