/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030237_version-updates.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.1   06/15/2013 Lee Quinn      10.4.0.1 version
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 10.4.0.0',
       PP_PATCH   = 'Patch: 10.4.0.1',
       POWERTAX_VERSION = '10.4.0.1',
       DATE_INSTALLED = sysdate
where PP_VERSION_ID = 1;

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
          select '10.4.0.1', 'automatedreview.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'batreporter.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'budget_allocations.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_allocations.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_balances.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_balances_bdg.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_batch_derivation.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_batch_derivation_bdg.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_budget_entry_calc_all.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_build_combos.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_build_deriver.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_delete_budget_allocations.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_financial_reports_build.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_flatten_structures.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_man_jrnl_reversals.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_posting.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_posting_bdg.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_sap_trueup.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_sum.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_sum_ab.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_to_commitments.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cwip_charge.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'populate_matlrec.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'ppmetricbatch.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'pptocr.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'ppverify.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'pp_integration.exe', '10.4.0.0' from dual
union all select '10.4.0.1', 'cr_derivation_trueup.exe', '10.4.0.0' from dual
minus select PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION from PP_VERSION_EXES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (412, 0, 10, 4, 0, 1, 30237, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_030237_version-updates.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
   SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
