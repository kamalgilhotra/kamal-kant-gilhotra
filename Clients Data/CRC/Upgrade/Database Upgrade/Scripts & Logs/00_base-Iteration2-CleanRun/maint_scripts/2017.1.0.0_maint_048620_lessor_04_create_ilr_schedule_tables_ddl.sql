/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048620_lessor_04_create_ilr_schedule_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/18/2017 Charlie Shilling Create final result table for the lessor ILR schedule calculation
||============================================================================
*/
CREATE TABLE LSR_ILR_SCHEDULE (
	ilr_id							NUMBER(22,0)	NOT NULL,
	revision						NUMBER(22,0)	NOT NULL,
	set_of_books_id					NUMBER(22,0)	NOT NULL,
	month							DATE			NOT NULL,
	user_id							VARCHAR2(18) 	NULL,
	time_stamp						DATE			NULL,
	interest_income_received		NUMBER(22,2)	NOT NULL,
	interest_income_accrued			NUMBER(22,2)	NOT NULL,
	interest_rental_recvd_spread	NUMBER(22,2)	NOT NULL,
	beg_deferred_rev				NUMBER(22,2)	NOT NULL,
	deferred_rev_activity 			NUMBER(22,2)	NOT NULL,
	end_deferred_rev				NUMBER(22,2)	NOT NULL,
	beg_receivable					NUMBER(22,2)	NOT NULL,
	end_receivable					NUMBER(22,2)	NOT NULL,
  	executory_accrual1				NUMBER(22,2)	NOT NULL,
  	executory_accrual2   			NUMBER(22,2)	NOT NULL,
  	executory_accrual3   			NUMBER(22,2)	NOT NULL,
  	executory_accrual4   			NUMBER(22,2)	NOT NULL,
  	executory_accrual5   			NUMBER(22,2)	NOT NULL,
	executory_accrual6 			  	NUMBER(22,2)	NOT NULL,
  	executory_accrual7  			NUMBER(22,2)	NOT NULL,
  	executory_accrual8  			NUMBER(22,2)	NOT NULL,
  	executory_accrual9  	 		NUMBER(22,2)	NOT NULL,
  	executory_accrual10  			NUMBER(22,2)	NOT NULL,
  	executory_paid1      			NUMBER(22,2)	NOT NULL,
  	executory_paid2      			NUMBER(22,2)	NOT NULL,
  	executory_paid3      			NUMBER(22,2)	NOT NULL,
  	executory_paid4      			NUMBER(22,2)	NOT NULL,
  	executory_paid5      			NUMBER(22,2)	NOT NULL,
  	executory_paid6      			NUMBER(22,2)	NOT NULL,
  	executory_paid7      			NUMBER(22,2)	NOT NULL,
  	executory_paid8      			NUMBER(22,2)	NOT NULL,
  	executory_paid9      			NUMBER(22,2)	NOT NULL,
  	executory_paid10     			NUMBER(22,2)	NOT NULL,
  	contingent_accrual1  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual2  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual3  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual4  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual5  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual6  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual7  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual8  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual9  			NUMBER(22,2)	NOT NULL,
  	contingent_accrual10 			NUMBER(22,2)	NOT NULL,
  	contingent_paid1     			NUMBER(22,2)	NOT NULL,
  	contingent_paid2     			NUMBER(22,2)	NOT NULL,
  	contingent_paid3     			NUMBER(22,2)	NOT NULL,
  	contingent_paid4     			NUMBER(22,2)	NOT NULL,
  	contingent_paid5     			NUMBER(22,2)	NOT NULL,
  	contingent_paid6     			NUMBER(22,2)	NOT NULL,
  	contingent_paid7     			NUMBER(22,2)	NOT NULL,
  	contingent_paid8     			NUMBER(22,2)	NOT NULL,
  	contingent_paid9     			NUMBER(22,2)	NOT NULL,
  	contingent_paid10    			NUMBER(22,2)	NOT NULL,
	current_lease_cost				NUMBER(22,2) 	NOT NULL
);

ALTER TABLE LSR_ILR_SCHEDULE
  	ADD CONSTRAINT pk_lsr_ilr_schedule PRIMARY KEY (
    	ilr_id,
    	revision,
    	set_of_books_id,
    	month
	)
  	USING INDEX
    	TABLESPACE pwrplant_idx
;

ALTER TABLE LSR_ILR_SCHEDULE
ADD CONSTRAINT r_lsr_ilr_schedule1 FOREIGN KEY (
		ilr_id,
		revision
	) REFERENCES lsr_ilr_options (
		ilr_id,
		revision
);

ALTER TABLE LSR_ILR_SCHEDULE
ADD CONSTRAINT r_lsr_ilr_schedule2 FOREIGN KEY (
		set_of_books_id
	) REFERENCES set_of_books (
		set_of_books_id
);


COMMENT ON COLUMN LSR_ILR_SCHEDULE.ILR_ID IS 'System-assigned identifier of a particular ILR.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.REVISION IS 'The revision.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.SET_OF_BOOKS_ID IS 'The internal set of books id within PowerPlant.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.MONTH IS 'The month being processed.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.interest_income_received IS 'The interest/income received in this period';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.interest_income_accrued IS 'The interest/income accrued as of this period';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.interest_rental_recvd_spread	IS 'The interest/rental received spread of this period';

comment on column LSR_ILR_SCHEDULE.beg_deferred_rev is 'The deferred revenue as of the start of the period';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.deferred_rev_activity IS 'The deferred revenue activity in this period';

comment on column LSR_ILR_SCHEDULE.end_deferred_rev is 'The total deferred revenue as of the end of this period';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.beg_receivable IS 'The receivable amount as of the beginning of this period';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.end_receivable IS 'The receivable amount as of the end of this period';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_ACCRUAL1 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_ACCRUAL2 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_ACCRUAL3 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_ACCRUAL4 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.executory_accrual5 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.executory_accrual6 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_ACCRUAL7 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_ACCRUAL8 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_ACCRUAL9 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.executory_accrual10 IS 'The amount of executory costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_PAID1 IS 'The executory paid amount associated with the executory bucket number 1.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_PAID2 IS 'The executory paid amount associated with the executory bucket number 2.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.executory_paid3 IS 'The executory paid amount associated with the executory bucket number 3.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_PAID4 IS 'The executory paid amount associated with the executory bucket number 4.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_PAID5 IS 'The executory paid amount associated with the executory bucket number 5.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.executory_paid6 IS 'The executory paid amount associated with the executory bucket number 6.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_PAID7 IS 'The executory paid amount associated with the executory bucket number 7.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_PAID8 IS 'The executory paid amount associated with the executory bucket number 8.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_PAID9 IS 'The executory paid amount associated with the executory bucket number 9.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.EXECUTORY_PAID10 IS 'The executory paid amount associated with the executory bucket number 10.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_ACCRUAL1 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_ACCRUAL2 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_ACCRUAL3 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.contingent_accrual4 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.contingent_accrual5 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_ACCRUAL6 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.contingent_accrual7 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.contingent_accrual8 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.contingent_accrual9 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_ACCRUAL10 IS 'The amount of contingent costs accrued in this period.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID1 IS 'The contingent paid amount associated with the contingent bucket number 1.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID2 IS 'The contingent paid amount associated with the contingent bucket number 2.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID3 IS 'The contingent paid amount associated with the contingent bucket number 3.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID4 IS 'The contingent paid amount associated with the contingent bucket number 4.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID5 IS 'The contingent paid amount associated with the contingent bucket number 5.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID6 IS 'The contingent paid amount associated with the contingent bucket number 6.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID7 IS 'The contingent paid amount associated with the contingent bucket number 7.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID8 IS 'The contingent paid amount associated with the contingent bucket number 8.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID9 IS 'The contingent paid amount associated with the contingent bucket number 9.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CONTINGENT_PAID10 IS 'The contingent paid amount associated with the contingent bucket number 10.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.USER_ID IS 'Standard System-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.TIME_STAMP IS 'Standard System-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_ILR_SCHEDULE.CURRENT_LEASE_COST IS 'The total amount funded on the ILR in the given month';

COMMENT ON TABLE LSR_ILR_SCHEDULE IS '(C)  [06] The ILR Schedule table records the ILR''s payment schedule.  The ILR''s schedule is allocated to its leased assets as part of the monthly lease expense calc process.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3725, 0, 2017, 1, 0, 0, 48620, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048620_lessor_04_create_ilr_schedule_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;