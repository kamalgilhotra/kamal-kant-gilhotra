/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010859_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/20/2012 Sunjin Cone    Point Release
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: WMIS Run Results I',
          'WMIS Network Repairs:  Part 1 of 2 Process Run Results for Non-Related Work Orders for the In Process Batch.  Displays quantity values tested for each work order number, work request, and repair unit.',
          'dw_repair_wmis_uop1_report',
          '',
          'WMIS - 0001',
          'dw_repair_batch_control_wmis',
          13,
          13,
          0,
          1,
          1,
          'WMIS - 0001',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Repairs: WMIS Run Results II',
          'WMIS Network Repairs:  Part 2 of 2 Process Run Results for Non-Related Work Orders for the In Process Batch.  Display dollar values for each work order and repair unit.',
          'dw_repair_wmis_uop2_report',
          '',
          'WMIS - 0002',
          'dw_repair_batch_control_wmis',
          13,
          13,
          0,
          1,
          1,
          'WMIS - 0002',
          3,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (199, 0, 10, 3, 5, 0, 10859, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010859_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;