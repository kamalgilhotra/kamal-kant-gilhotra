/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053078_taxrpr_reset_always_capital_flag_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 02/08/2019 David Conway   change always_capital from 1 to 0, since 1 is no longer valid.
||============================================================================
*/

update wo_tax_expense_test
set always_capital = 0
where always_capital = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14886, 0, 2018, 2, 0, 0, 53078, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053078_taxrpr_reset_always_capital_flag_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;


