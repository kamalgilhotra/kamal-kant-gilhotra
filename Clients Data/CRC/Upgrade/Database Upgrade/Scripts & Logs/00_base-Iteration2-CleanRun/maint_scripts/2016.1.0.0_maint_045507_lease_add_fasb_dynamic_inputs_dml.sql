/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045507_lease_add_fasb_dynamic_inputs_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 02/29/2016 Will Davis       Make FASB lease test dynamic
||============================================================================
*/

insert into ls_fasb_test_criteria a
(company_id, time_stamp, user_id, set_of_books_id, econ_life_percent, npv_percent)
select
 co.company_id, sysdate, user, set_of_books_id, .75, .9
from company_setup co, company_set_of_books cosb
where co.is_lease_company = 1
  and cosb.company_id = co.company_id
  and not exists (select 1 from ls_fasb_test_criteria where company_id = co.company_id and set_of_books_id = cosb.set_of_books_id);

insert into ls_fasb_test_criteria
(company_id, time_stamp, user_id, set_of_books_id, econ_life_percent, npv_percent)
select
 -1, sysdate, user, sob.set_of_books_id, .75, .9
from set_of_books sob
where not exists (select 1 from ls_fasb_test_criteria where company_id = -1 and set_of_books_id = sob.set_of_books_id);

update ppbase_actions_windows
set action_text = 'Capital Test'
where module = 'LESSEE'
and action_text ='Brightline Test';

INSERT INTO "POWERPLANT_TABLES" ("TABLE_NAME", "TIME_STAMP", "USER_ID", "PP_TABLE_TYPE_ID", "DESCRIPTION", "SUBSYSTEM_SCREEN", "SELECT_WINDOW", "SUBSYSTEM_DISPLAY", "SUBSYSTEM", 
"DELIVERY", "NOTES", "ASSET_MANAGEMENT", "BUDGET", "CHARGE_REPOSITORY", "CWIP_ACCOUNTING", "DEPR_STUDIES", "LEASE", "PROPERTY_TAX", "POWERTAX_PROVISION", "POWERTAX", "SYSTEM", 
"UNITIZATION", "WORK_ORDER_MANAGEMENT", "CLIENT", "WHERE_CLAUSE") VALUES ('ls_fasb_test_criteria', to_date('2016-02-29 16:00:10', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 's', 
'Lease Capitalization Test Criteria', 'always', null, 'lessee', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null) ; 
INSERT INTO "POWERPLANT_COLUMNS" ("COLUMN_NAME", "TABLE_NAME", "TIME_STAMP", "USER_ID", "DROPDOWN_NAME", "PP_EDIT_TYPE_ID", "HELP_INDEX", "DESCRIPTION", "COLUMN_RANK", 
"SHORT_LONG_DESCRIPTION", "EDIT_MASK", "DROPDOWN_PERCENT", "DROPDOWN_RESTRICT", "RELATED_TYPE", "RELATED_TABLE", "READ_ONLY", "DEFAULT_VALUE") VALUES ('company_id', 
'ls_fasb_test_criteria', to_date('2016-02-29 16:01:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'company', 'p', null, 'company id', 1, null, null, null, null, null, null, null, 
null) ; 
INSERT INTO "POWERPLANT_COLUMNS" ("COLUMN_NAME", "TABLE_NAME", "TIME_STAMP", "USER_ID", "DROPDOWN_NAME", "PP_EDIT_TYPE_ID", "HELP_INDEX", "DESCRIPTION", "COLUMN_RANK", 
"SHORT_LONG_DESCRIPTION", "EDIT_MASK", "DROPDOWN_PERCENT", "DROPDOWN_RESTRICT", "RELATED_TYPE", "RELATED_TABLE", "READ_ONLY", "DEFAULT_VALUE") VALUES ('econ_life_percent', 
'ls_fasb_test_criteria', to_date('2016-02-29 16:02:17', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null, 'econ life percent', 4, null, null, null, null, null, null, null, 
null) ; 
INSERT INTO "POWERPLANT_COLUMNS" ("COLUMN_NAME", "TABLE_NAME", "TIME_STAMP", "USER_ID", "DROPDOWN_NAME", "PP_EDIT_TYPE_ID", "HELP_INDEX", "DESCRIPTION", "COLUMN_RANK", 
"SHORT_LONG_DESCRIPTION", "EDIT_MASK", "DROPDOWN_PERCENT", "DROPDOWN_RESTRICT", "RELATED_TYPE", "RELATED_TABLE", "READ_ONLY", "DEFAULT_VALUE") VALUES ('npv_percent', 
'ls_fasb_test_criteria', to_date('2016-02-29 16:00:12', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null, 'npv percent', 3, null, null, null, null, null, null, null, null) ; 
INSERT INTO "POWERPLANT_COLUMNS" ("COLUMN_NAME", "TABLE_NAME", "TIME_STAMP", "USER_ID", "DROPDOWN_NAME", "PP_EDIT_TYPE_ID", "HELP_INDEX", "DESCRIPTION", "COLUMN_RANK", 
"SHORT_LONG_DESCRIPTION", "EDIT_MASK", "DROPDOWN_PERCENT", "DROPDOWN_RESTRICT", "RELATED_TYPE", "RELATED_TABLE", "READ_ONLY", "DEFAULT_VALUE") VALUES ('set_of_books_id', 
'ls_fasb_test_criteria', to_date('2016-02-29 16:02:07', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'set_of_books', 'p', null, 'set of books id', 2, null, null, null, null, null, null, 
null, null) ; 
INSERT INTO "POWERPLANT_COLUMNS" ("COLUMN_NAME", "TABLE_NAME", "TIME_STAMP", "USER_ID", "DROPDOWN_NAME", "PP_EDIT_TYPE_ID", "HELP_INDEX", "DESCRIPTION", "COLUMN_RANK", 
"SHORT_LONG_DESCRIPTION", "EDIT_MASK", "DROPDOWN_PERCENT", "DROPDOWN_RESTRICT", "RELATED_TYPE", "RELATED_TABLE", "READ_ONLY", "DEFAULT_VALUE") VALUES ('time_stamp', 
'ls_fasb_test_criteria', to_date('2016-02-29 16:00:12', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null, 'time stamp', 100, null, null, null, null, null, null, null, null) ; 
INSERT INTO "POWERPLANT_COLUMNS" ("COLUMN_NAME", "TABLE_NAME", "TIME_STAMP", "USER_ID", "DROPDOWN_NAME", "PP_EDIT_TYPE_ID", "HELP_INDEX", "DESCRIPTION", "COLUMN_RANK", 
"SHORT_LONG_DESCRIPTION", "EDIT_MASK", "DROPDOWN_PERCENT", "DROPDOWN_RESTRICT", "RELATED_TYPE", "RELATED_TABLE", "READ_ONLY", "DEFAULT_VALUE") VALUES ('user_id', 
'ls_fasb_test_criteria', to_date('2016-02-29 16:00:12', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null, 'user id', 101, null, null, null, null, null, null, null, null) ; 

declare 
v_count number;
begin
select count(1) into v_count 
from pp_table_groups;
if v_count =0 then 
  return;
else 
insert into pp_table_groups
(groups, table_name)
values 
('system','ls_fasb_test_criteria');
end if;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3083, 0, 2016, 1, 0, 0, 045507, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045507_lease_add_fasb_dynamic_inputs_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;