/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029853_cwip_retro_validates.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   08/20/2013 Sunjin Cone     Point Release
||============================================================================
*/

alter table CPI_RETRO_COMPARE add CPR_DATA_PROBLEM varchar2(254);

comment on column CPI_RETRO_COMPARE.CPR_DATA_PROBLEM is 'Provides additional details for why the CPI Retro calc  activity has GL Account changed to CPI in CWIP (1).  No additional details indicates no problems found.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (523, 0, 10, 4, 1, 0, 29853, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029853_cwip_retro_validates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
