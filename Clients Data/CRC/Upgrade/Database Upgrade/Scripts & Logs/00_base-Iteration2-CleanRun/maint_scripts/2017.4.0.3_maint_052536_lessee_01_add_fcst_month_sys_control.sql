/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052536_lessee_01_add_fcst_month_sys_control.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.3 10/18/2018 C.Yura           Add system control to disallow fcst in service where open month <> conversion month
||============================================================================
*/

insert into pp_system_control_company
(control_id, control_name, control_value, description, long_description, company_id)
select * From (
select
      max(control_id)+1 AS control_id,
      'Lessee Fcst In-Svc Non-Curr Month' AS control_name,
      'no' AS control_value,
      'dw_yes_no;1' AS description,
      'If No, do not allow Lessee forecast in service where conversion month is not equal to current open month' AS long_description,
      -1 AS company_id
      from pp_system_control_company )
  where not exists(
  select 1
  from pp_system_control_company
  where control_name = 'Lessee Fcst In-Svc Non-Curr Month');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10522, 0, 2017, 4, 0, 3, 52536, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052536_lessee_01_add_fcst_month_sys_control.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
