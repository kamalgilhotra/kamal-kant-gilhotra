/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051039_lessee_02_populate_new_sob_column_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/09/2018 Charlie Shilling need to populate SOB column on LS_ILR_PAYMENT_TERM_VAR_PAYMNT before we add it to the PK (which makes it NOT NULL)
||============================================================================
*/
--First, we need to default all existing records to SOB 1
UPDATE ls_ilr_payment_term_var_paymnt
SET set_of_books_id = 1
WHERE set_of_books_id IS NULL;

--Then we need to insert new rows for the additional sets of books, copying the rest of the values from the original row, which is now SOB 1
INSERT INTO ls_ilr_payment_term_var_paymnt (ilr_id, revision, payment_term_id, rent_type, bucket_number, variable_payment_id, incl_in_initial_measure, run_order, initial_var_payment_value, set_of_books_id)
SELECT vp.ilr_id, vp.revision, vp.payment_term_id, vp.rent_type, vp.bucket_number, vp.variable_payment_id, vp.incl_in_initial_measure, vp.run_order, vp.initial_var_payment_value, csob.set_of_books_id
FROM ls_ilr_payment_term_var_paymnt vp
INNER JOIN ls_ilr ilr
	ON vp.ilr_id = ilr.ilr_id
INNER JOIN company_set_of_books csob
	ON ilr.company_id = csob.company_id
WHERE csob.include_indicator = 1
AND vp.set_of_books_id = 1
AND NOT EXISTS (
	SELECT 1
	FROM ls_ilr_payment_term_var_paymnt vp2
	WHERE vp.ilr_id = vp2.ilr_id
	AND vp.revision = vp2.revision
	AND vp.payment_term_id = vp2.payment_term_id
	AND vp.bucket_number = vp2.bucket_number
	AND csob.set_of_books_id = vp2.set_of_books_id
)
ORDER BY ilr_id, revision, set_of_books_id, payment_term_id, rent_type, bucket_number
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5285, 0, 2017, 4, 0, 0, 51039, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051039_lessee_02_populate_new_sob_column_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;