/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029827_lease_powerplant_columns_inserts_6.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/15/2013 Lee Quinn      Point Release
||============================================================================
*/

-- If Lease exists then comment out these inserts
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_5_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 5 Exp',
    '<Long>', 20, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_4_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 4 Payable', '<Long>', 19, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_4_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 4 Exp',
    '<Long>', 18, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_3_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 3 Payable', '<Long>', 17, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_3_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 3 Exp',
    '<Long>', 16, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('company_id', 'ls_accounting_defaults', sysdate, user, 'company', 'p', null, 'Company',
    '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('current_lease_oblig_acct', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Cur. Lease Oblig. Acct.', '<Long>', 50, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('func_class_id', 'ls_accounting_defaults', sysdate, user, 'func_class', 'p', null,
    'Functional Class', '<Long>', 5, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('gl_account_id', 'ls_accounting_defaults', sysdate, user, 'gl_account', 'p', null, 'GL Account',
    '<Long>', 6, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('interest_expense_acct', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Interest Expense Account', '<Long>', 7, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('je_method_id', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Journal Entry Method ID', '<Long>', 52, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lessor_payable_acct', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Lessor Payable Account', '<Long>', 9, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_10_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 10 Exp', '<Long>', 30, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_10_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 10 Payable', '<Long>', 31, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_1_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 1 Exp',
    '<Long>', 10, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_1_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 1 Payable', '<Long>', 11, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_2_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 2 Exp',
    '<Long>', 14, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_2_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 2 Payable', '<Long>', 15, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('dddw_name', 'ls_asset_flex_control', sysdate, user, null, 'e', null, 'Drop-Down DW Name',
    '<Long>', 4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('column_usage_id', 'ls_asset_flex_control', sysdate, user, 'ls_column_usage', 'p', null,
    'Column Usage', '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_asset_flex_control', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_asset_flex_control', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('asset_flex_id', 'ls_asset_flex_control', sysdate, user, null, 's', null, 'Asset Flex ID',
    '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('asset_flex_label', 'ls_asset_flex_control', sysdate, user, null, 'e', null, 'Asset Flex Label',
    '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id',
    'ls_asset_flex_values',
    sysdate, user, null, 'e', null, 'User', '<Long>', 101, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('flex_value',
    'ls_asset_flex_values',
    sysdate, user, null, 'e', null, 'Flex Value', '<Long>', 2, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('asset_flex_id',
    'ls_asset_flex_values',
    sysdate, user, null, 'e', null, 'Asset Flex ID', '<Long>', 1, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp',
    'ls_asset_flex_values',
    sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100, null, null, null, null, null, null,
    null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_asset_loc_local_tax_distri', sysdate, user, null, 'e', null, 'user id', '<Long>',
    101, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_asset_loc_local_tax_distri', sysdate, user, null, 'e', null, 'time stamp',
    '<Long>', 100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_district_id', 'ls_asset_loc_local_tax_distri', sysdate, user,
    'ls_local_tax_district', 'p', null, 'local tax district id', '<Long>', 2, null, null, null, null,
    null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('asset_location_id', 'ls_asset_loc_local_tax_distri', sysdate, user, 'asset_location', 'p',
    null, 'asset location id', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_id', 'ls_asset_local_tax', sysdate, user, 'ls_local_tax_dddw', 'p', null,
    'LS Local Tax ', '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ls_asset_id', 'ls_asset_local_tax', sysdate, user, null, 'e', null, 'Ls Asset ID', '<Long>', 1,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_asset_local_tax', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 101,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_asset_local_tax', sysdate, user, null, 'e', null, 'User Id', '<Long>', 100, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('cost_element_id', 'ls_cost_element_valid', sysdate, user, 'cost_element', 'p', null,
    'Cost Element', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_cost_element_valid', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_cost_element_valid', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('element_label', 'ls_dist_def_control', sysdate, user, null, 'e', null, 'Element Label',
    '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('element_id', 'ls_dist_def_control', sysdate, user, null, 'e', null, 'Element ID', '<Long>', 2,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_dist_def_control', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('column_usage_id', 'ls_dist_def_control', sysdate, user, 'ls_column_usage', 'p', null,
    'Column Usage', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_dist_def_control', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('parm_value', 'ls_dist_def_parms', sysdate, user, null, 'e', null, 'Validate (0-No, 1-Yes)',
    '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_dist_def_parms', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_dist_def_parms', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('parm_descr', 'ls_dist_def_parms', sysdate, user, null, 'e', null, 'Validation Parameter',
    '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('department_id', 'ls_dist_department', sysdate, user, null, 's', null, 'Department ID',
    '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_department_code', 'ls_dist_department', sysdate, user, null, 'e', null,
    'Ext. Department Code', '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'ls_dist_department', sysdate, user, null, 'e', null, 'Long Description',
    '<Long>', 4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('status_code_id', 'ls_dist_department', sysdate, user, 'status_code', 'p', null, 'Status Code',
    '<Long>', 5, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_dist_department', sysdate, user, null, 'e', null, 'Description', '<Long>', 0,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_dist_department', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_dist_department', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_dist_gl_account', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_dist_gl_account', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('status_code_id', 'ls_dist_gl_account', sysdate, user, null, 'e', null, 'Status Code ID',
    '<Long>', 7, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'ls_dist_gl_account', sysdate, user, null, 'e', null, 'Long Description',
    '<Long>', 6, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('ferc_sys_of_accts_id', 'ls_dist_gl_account', sysdate, user, 'ferc_sys_of_accts', 'p', null,
    'FERC System of Accts.', '<Long>', 5, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('accounting_default_id', 'ls_accounting_defaults', sysdate, user, null, 's', null,
    'Accounting Default ID', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('bus_segment_id', 'ls_accounting_defaults', sysdate, user, 'business_segment', 'p', null,
    'Business Segment', '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'User', '<Long>', 101,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('non_current_lease_oblig_acct', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Non Cur. Lse. Oblig. Acct.', '<Long>', 51, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_9_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 9 Payable', '<Long>', 29, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_9_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 9 Exp',
    '<Long>', 28, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_8_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 8 Payable', '<Long>', 27, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_8_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 8 Exp',
    '<Long>', 26, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_7_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 7 Payable', '<Long>', 25, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_7_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 7 Exp',
    '<Long>', 24, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_6_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 6 Payable', '<Long>', 23, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_6_exp', 'ls_accounting_defaults', sysdate, user, null, 'e', null, 'Local Tax 6 Exp',
    '<Long>', 22, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_5_payable', 'ls_accounting_defaults', sysdate, user, null, 'e', null,
    'Local Tax 5 Payable', '<Long>', 21, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('account_type_id', 'ls_dist_gl_account', sysdate, user, 'account_type', 'p', null,
    'Account Type', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_set_of_books', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('sob_type_id', 'ls_set_of_books', sysdate, user, 'ls_sob_type', 'p', null, 'Set Of Books Type',
    '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('sales_tax_var_pct', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Sales Tax Variance Pct.', '<Long>', 7, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('address1', 'ls_lessor', sysdate, user, null, 'e', null, 'Address 1', '<Long>', 1, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('address2', 'ls_lessor', sysdate, user, null, 'e', null, 'Address 2', '<Long>', 2, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('state_id', 'ls_lessor', sysdate, user, 'state', 'p', null, 'State', '<Long>', 7, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_lessor', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_lessor', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null, null, null,
    null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('address3', 'ls_lessor', sysdate, user, null, 'e', null, 'Address 3', '<Long>', 3, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('address4', 'ls_lessor', sysdate, user, null, 'e', null, 'Address 4', '<Long>', 4, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('city', 'ls_lessor', sysdate, user, null, 'e', null, 'City', '<Long>', 5, null, null, null,
    null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('country_id', 'ls_lessor', sysdate, user, 'country', 'p', null, 'Country', '<Long>', 9, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('county_id', 'ls_lessor', sysdate, user, 'county', 'p', null, 'County', '<Long>', 6, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_lessor', sysdate, user, null, 'e', null, 'Description', '<Long>', 0, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('extension', 'ls_lessor', sysdate, user, null, 'e', null, 'Extension', '<Long>', 12, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('fax_number', 'ls_lessor', sysdate, user, null, 'e', null, 'Fax Number', '<Long>', 13, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lessor_id', 'ls_lessor', sysdate, user, null, 's', null, 'Lessor ID', '<Long>', 15, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'ls_lessor', sysdate, user, null, 'e', null, 'Long Description', '<Long>',
    20, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('phone_number', 'ls_lessor', sysdate, user, null, 'e', null, 'Phone Number', '<Long>', 11, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('postal', 'ls_lessor', sysdate, user, null, 'e', null, 'Postal', '<Long>', 10, null, null, null,
    null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('zip', 'ls_lessor', sysdate, user, null, 'e', null, 'Zip Code', '<Long>', 8, null, null, null,
    null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lessor_id', 'ls_lessor_state', sysdate, user, 'ls_lessor', 'p', null, 'Lessor', '<Long>', 1,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('state_id', 'ls_lessor_state', sysdate, user, 'state', 'p', null, 'State', '<Long>', 2, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_lessor_state', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_lessor_state', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_local_tax', sysdate, user, null, 'e', null, 'user id', '<Long>', 101, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_local_tax', sysdate, user, null, 'e', null, 'description', '<Long>', 0, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lessor_tax_yn_sw', 'ls_local_tax', sysdate, user, null, 'e', null, 'Lessor Tax Yn Sw',
    '<Long>', 4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_id', 'ls_local_tax', sysdate, user, null, 'e', null, 'local tax id', '<Long>', 2,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'ls_local_tax', sysdate, user, null, 'e', null, 'long description',
    '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('summary_local_tax_id', 'ls_local_tax', sysdate, user, null, 'e', null, 'summary local tax id',
    '<Long>', 4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_local_tax', sysdate, user, null, 'e', null, 'time stamp', '<Long>', 100, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_local_tax_district', sysdate, user, null, 'e', null, 'user id', '<Long>', 101,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_local_tax_district', sysdate, user, null, 'e', null, 'time stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'ls_local_tax_district', sysdate, user, null, 'e', null, 'long description',
    '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_district_id', 'ls_local_tax_district', sysdate, user, null, 'e', null,
    'local tax district id', '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_local_tax_district', sysdate, user, null, 'e', null, 'description', '<Long>',
    0, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_local_tax_elig', sysdate, user, null, 'e', null, 'time stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('state_id', 'ls_local_tax_elig', sysdate, user, null, 'e', null, 'state id', '<Long>', 4, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_id', 'ls_local_tax_elig', sysdate, user, null, 'e', null, 'local tax id', '<Long>',
    3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_default_id', 'ls_local_tax_elig', sysdate, user, null, 'e', null,
    'local tax default id', '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_local_tax_elig', sysdate, user, null, 'e', null, 'user id', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('company_id', 'ls_local_tax_elig', sysdate, user, null, 'e', null, 'company id', '<Long>', 1,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('grandfather_date', 'ls_state_grandfather_date', sysdate, user, null, 'e', null,
    'Grandfather Date', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('grandfather_level_id', 'ls_state_grandfather_date', sysdate, user, null, 'e', null,
    'Grandfather Level Id', '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('local_tax_id', 'ls_state_grandfather_date', sysdate, user, null, 'e', null, 'Local Tax Id',
    '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('state_id', 'ls_state_grandfather_date', sysdate, user, 'state', 'e', null, 'State', '<Long>',
    4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_state_grandfather_date', sysdate, user, null, 'e', null, 'time stamp',
    '<Long>', 100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_state_grandfather_date', sysdate, user, null, 'e', null, 'user id', '<Long>',
    101, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_summary_local_tax', sysdate, user, null, 'e', null, 'description', '<Long>',
    0, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'ls_summary_local_tax', sysdate, user, null, 'e', null, 'long description',
    '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('summary_local_tax_id', 'ls_summary_local_tax', sysdate, user, null, 'e', null,
    'summary local tax id', '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_summary_local_tax', sysdate, user, null, 'e', null, 'time stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_summary_local_tax', sysdate, user, null, 'e', null, 'user id', '<Long>', 101,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_vendor_id', 'ls_lessor', sysdate, user, null, 'e', null, 'External Vendor ID',
    '<Long>', 14, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('work_order_id', 'ls_valid_funding_projects', sysdate, user, 'ls_valid_funding_projects', 'p',
    null, 'Funding Project Number', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_valid_funding_projects', sysdate, user, null, 'e', null, 'Time Stamp',
    '<Long>', 100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_valid_funding_projects', sysdate, user, null, 'e', null, 'User', '<Long>', 101,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_dist_def_type', sysdate, user, 'ls_dist_def_type', 'p', null,
    'Dist Def Type Description', '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_dist_def_type', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_dist_def_type', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('dist_def_type_id', 'ls_dist_def_type', sysdate, user, null, 's', null, 'Dist Def Type ID',
    '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('column_usage_id', 'ls_dist_def_type', sysdate, user, 'yes_no', 'p', null, 'Column Usage ID',
    '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('set_of_books_id', 'ls_set_of_books', sysdate, user, 'set_of_book_filter', 'p', null,
    'Set of Books ID', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_set_of_books', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('rent_holiday_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null, 'Rent Holiday Switch',
    '<Long>', 14, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('reset_extended_rental_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null,
    'Reset Extend. Rental Sw.', '<Long>', 15, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('reset_interest_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null,
    'Reset Interest Switch', '<Long>', 16, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('reset_pretax_payment_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null,
    'Reset Pretax Pymt. Sw.', '<Long>', 17, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_lease_rule', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_lease_rule', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null, null,
    null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('factor_round', 'ls_lease_rule', sysdate, user, null, 'e', null, 'Factor Round', '<Long>', 6,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_lease_rule', sysdate, user, null, 'e', null, 'Description', '<Long>', 0,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('curr_mo_int_override_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null,
    'Cur. Mo. Int Override Sw.', '<Long>', 4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('allow_neg_pretax_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null,
    'Allow Neg. Pretax Sw.', '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lease_rule_id', 'ls_lease_rule', sysdate, user, null, 's', null, 'Lease Rule ID', '<Long>', 9,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('principal_var_pct', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Principal Variance Pct.', '<Long>', 5, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('principal_var_limit', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Principal Variance Limit', '<Long>', 4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lease_type_id', 'ls_lease_type_tolerance', sysdate, user, 'ls_lease_type', 'p', null,
    'Lease Type', '<Long>', 15, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('interest_var_pct', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Interest Variance Pct.', '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('interest_var_limit', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Interest Variance Limit', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('sales_tax_var_limit', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Sales Tax Variance Limit', '<Long>', 6, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null, 'User', '<Long>', 101,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('unpaid_bal_var_pct', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Unpaid Balance Var Pct.', '<Long>', 12, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('unpaid_bal_var_limit', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Unpaid Balance Var. Limit', '<Long>', 11, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('total_var_pct', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Total Variance Percent', '<Long>', 10, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('total_var_limit', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null,
    'Total Variance Limit', '<Long>', 9, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_lease_type_tolerance', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_dist_gl_account', sysdate, user, null, 'e', null, 'Description', '<Long>', 0,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('dist_gl_account_id', 'ls_dist_gl_account', sysdate, user, null, 's', null,
    'Dist. GL Account ID', '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_account_code', 'ls_dist_gl_account', sysdate, user, null, 'e', null,
    'External Account Code', '<Long>', 4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'ls_distribution_type', sysdate, user, null, 'e', null, 'Description', '<Long>',
    0, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('distribution_type_id', 'ls_distribution_type', sysdate, user, null, 's', null,
    'Distribution Type ID', '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'ls_distribution_type', sysdate, user, null, 'e', null, 'Long Description',
    '<Long>', 3, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_distribution_type', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_distribution_type', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lease_calc_role_id', 'ls_lease_calc_role_user', sysdate, user, null, 'e', null,
    'lease calc role id', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_lease_calc_role_user', sysdate, user, null, 'e', null, 'time stamp', '<Long>',
    100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('users', 'ls_lease_calc_role_user', sysdate, user, null, 'e', null, 'users', '<Long>', 4, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_lease_calc_role_user', sysdate, user, null, 'e', null, 'user id', '<Long>', 101,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('approve_user_id', 'ls_lease_calc_user_approver', sysdate, user, null, 'e', null,
    'approve user id', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('submit_user_id', 'ls_lease_calc_user_approver', sysdate, user, null, 'e', null,
    'submit user id', '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_lease_calc_user_approver', sysdate, user, null, 'e', null, 'time stamp',
    '<Long>', 100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_lease_calc_user_approver', sysdate, user, null, 'e', null, 'user id', '<Long>',
    101, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('company_id', 'ls_lease_company', sysdate, user, 'company', 'p', null, 'Company', '<Long>', 1,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('deposit_amount', 'ls_lease_company', sysdate, user, null, 'e', null, 'Deposit Amount',
    '<Long>', 2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lease_id', 'ls_lease_company', sysdate, user, 'lease', 'p', null, 'Lease', '<Long>', 3, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('max_lease_line', 'ls_lease_company', sysdate, user, null, 'e', null, 'Max Lease Line',
    '<Long>', 4, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_lease_company', sysdate, user, null, 'e', null, 'Time Stamp', '<Long>', 100,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_lease_company', sysdate, user, null, 'e', null, 'User', '<Long>', 101, null,
    null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'ls_lease_disposition_code', sysdate, user, null, 'e', null, 'User', '<Long>', 101,
    null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('disposition_code', 'ls_lease_disposition_code', sysdate, user, 'disp_code', 'p', null,
    'Disposition Code', '<Long>', 1, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('lease_id', 'ls_lease_disposition_code', sysdate, user, 'lease', 'p', null, 'Lease', '<Long>',
    2, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'ls_lease_disposition_code', sysdate, user, null, 'e', null, 'Time Stamp',
    '<Long>', 100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('factor_round_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null, 'Factor Round Switch',
    '<Long>', 5, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('interest_round_position', 'ls_lease_rule', sysdate, user, null, 'e', null,
    'Interest Round Position', '<Long>', 8, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('interest_round_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null,
    'Interest Round Switch', '<Long>', 7, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('use_age_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null, 'Use Age Switch', '<Long>',
    18, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'ls_lease_rule', sysdate, user, null, 'e', null, 'Long Description',
    '<Long>', 10, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('period_override', 'ls_lease_rule', sysdate, user, null, 'e', null, 'Period Override ',
    '<Long>', 19, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('principle_round', 'ls_lease_rule', sysdate, user, null, 'e', null, 'Principle Round', '<Long>',
    13, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('principle_round_sw', 'ls_lease_rule', sysdate, user, 'yes_no', 'p', null,
    'Principle Round Switch', '<Long>', 12, null, null, null, null, null, null, null, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (362, 0, 10, 4, 0, 0, 29827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029827_lease_powerplant_columns_inserts_6.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
