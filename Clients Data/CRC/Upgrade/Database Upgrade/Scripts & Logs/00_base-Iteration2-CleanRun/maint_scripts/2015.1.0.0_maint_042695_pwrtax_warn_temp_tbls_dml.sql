--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042695_pwrtax_warn_temp_tbls_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     01/26/2015 Andrew Scott     Prior to 10.4.3, tax code allowed users to 
--||                                        create and drop temp tables.  Revised code
--||                                        truncates a temp table owned by pwrplant.
--||                                        need to drop any older copies of these tables
--||                                        not owned by PWRPLANT.  pwrplant id is used
--||                                        during upgrade script run, but most likely will
--||                                        not have permissions to drop tables owned by others.
--||                                        generate an error and warning so that consultants can
--||                                        work with DBAs to drop these tables.
--||============================================================================
--*/

SET SERVEROUTPUT ON
SET LINESIZE 200

declare
   PPCMSG  varchar2(10) := 'PPC' || '-MSG> ';
   PPCERR  varchar2(10) := 'PPC' || '-ERR> ';
   NEWLINE varchar2(10) := CHR(10);

   L_SKIP_CHECK boolean := false;
   L_COUNT      number;
   L_ERROR      boolean := false;
   L_MSG        varchar2(256);

begin

  --find duplicate tax temp tables

   begin
     select counter
      into L_COUNT
      from (
         SELECT lower(table_name), Count(*) counter
         FROM DBA_TABLES
         where lower(table_name) in ('temp_renumber_trids')
         GROUP BY lower(table_name)
         HAVING count(*) > 1
         )
      ;
   exception
      when no_data_found then
        --no issues.  this is good!
        L_MSG   := 'No multiple instances of temp_renumber_trids.  Continuing...';
        DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
   end;

  if L_COUNT > 0 then
    L_ERROR := true;
    DBMS_OUTPUT.PUT_LINE(NEWLINE);
    L_MSG   := 'There are '||L_COUNT||' instances of temp_renumber_trids.';
    DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
    L_MSG   := 'Have a DBA drop the copies of this table not owned by PWRPLANT.';
    DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
  end if;

   begin
     select counter
      into L_COUNT
      from (
         SELECT lower(table_name), Count(*) counter
         FROM DBA_TABLES
         where lower(table_name) in ('temp_renumber_transfers')
         GROUP BY lower(table_name)
         HAVING count(*) > 1
         )
      ;
   exception
      when no_data_found then
        --no issues.  this is good!
        L_MSG   := 'No multiple instances of temp_renumber_transfers.  Continuing...';
        DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
   end;

  if L_COUNT > 0 then
    L_ERROR := true;
    DBMS_OUTPUT.PUT_LINE(NEWLINE);
    L_MSG   := 'There are '||L_COUNT||' instances of temp_renumber_transfers.';
    DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
    L_MSG   := 'Have a DBA drop the copies of this table not owned by PWRPLANT.';
    DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
  end if;

  if L_ERROR = true then
    -- Raise application error
    DBMS_OUTPUT.PUT_LINE(NEWLINE);
    RAISE_APPLICATION_ERROR(-20000, 'Read the above comments and work with the DBA to have the non-PWRPLANT tables dropped as needed.');
  end if;
end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2279, 0, 2015, 1, 0, 0, 42695, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042695_pwrtax_warn_temp_tbls_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;