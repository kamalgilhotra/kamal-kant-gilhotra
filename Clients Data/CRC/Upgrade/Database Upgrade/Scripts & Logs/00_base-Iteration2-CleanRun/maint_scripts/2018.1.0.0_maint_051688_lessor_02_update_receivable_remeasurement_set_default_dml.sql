/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051688_lessor_02_update_receivable_remeasurement_set_default_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.1.0.0  09/13/2018 Jared Watkins    Backfill the receivable remeasurement field with 0's
||============================================================================
*/

update lsr_ilr_schedule 
set receivable_remeasurement = 0 
where receivable_remeasurement is null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9803, 0, 2018, 1, 0, 0, 51668, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051688_lessor_02_update_receivable_remeasurement_set_default_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;