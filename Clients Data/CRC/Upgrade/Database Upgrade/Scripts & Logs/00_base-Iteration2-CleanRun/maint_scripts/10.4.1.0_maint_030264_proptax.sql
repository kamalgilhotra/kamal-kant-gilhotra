/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030264_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/27/2013 Julia Breuer   Point release
||============================================================================
*/

--
-- Create backups of the accrual charge table (since we're dropping columns) and the allowed amount table (since we're dropping it).
--
create table D1_PT_ACCRUAL_CHARGE_V104 as select * from PT_ACCRUAL_CHARGE;
create table D1_PT_ACCRUAL_ALLOWED_AMT_V104 as select * from PT_ACCRUAL_ALLOWED_AMT;

--
-- Create new tables for storing accrual deferral amounts.
--
create table PT_ACCRUAL_DEFERRAL_GROUP
(
 DEFERRAL_GROUP_ID number(22,0)  not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(35),
 LONG_DESCRIPTION  varchar2(254),
 OFFSET_MONTHS     number(22,0)
);

alter table PT_ACCRUAL_DEFERRAL_GROUP
   add constraint PT_ACCRUAL_DEF_GRP_PK
       primary key (DEFERRAL_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SUBSYSTEM_DISPLAY, SUBSYSTEM, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT)
values
   ('pt_accrual_deferral_group', sysdate, user, 's', 'PT Accrual Deferral Group',
    'PT Accrual Deferral Group', 'proptax', 'proptax', 'proptax', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
    0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('deferral_group_id', 'pt_accrual_deferral_group', sysdate, user, null, 's', 'Deferral Group ID',
    'Deferral Group ID', 'Deferral Group ID', 1, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('description', 'pt_accrual_deferral_group', sysdate, user, null, 'e', 'Description',
    'Description', 'Description', 2, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('long_description', 'pt_accrual_deferral_group', sysdate, user, null, 'e', 'Long Description',
    'Long Description', 'Long Description', 3, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('offset_months', 'pt_accrual_deferral_group', sysdate, user, null, 'e', 'Offset Months',
    'Offset Months', 'Offset Months', 4, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('time_stamp', 'pt_accrual_deferral_group', sysdate, user, null, 'e', 'Time Stamp', 'Time Stamp',
    'Time Stamp', 100, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('user_id', 'pt_accrual_deferral_group', sysdate, user, null, 'e', 'User ID', 'User ID',
    'User ID', 101, 0);

create table PT_ACCRUAL_DEFERRAL_AMOUNT
(
 DEFERRAL_GROUP_ID  number(22,0) not null,
 ASSESSMENT_YEAR_ID number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 ALLOWED_AMOUNT     number(22,2),
 DEFERRAL_PCT       number(22,8)
);

alter table PT_ACCRUAL_DEFERRAL_AMOUNT
   add constraint PT_ACCRUAL_DEF_AMT_PK
       primary key (DEFERRAL_GROUP_ID, ASSESSMENT_YEAR_ID)
       using index tablespace PWRPLANT_IDX;

alter table PT_ACCRUAL_DEFERRAL_AMOUNT
   add constraint PT_ACCRUAL_DEF_AMT_DG_FK
       foreign key (DEFERRAL_GROUP_ID)
       references PT_ACCRUAL_DEFERRAL_GROUP;

alter table PT_ACCRUAL_DEFERRAL_AMOUNT
   add constraint PT_ACCRUAL_DEF_AMT_AY_FK
       foreign key (ASSESSMENT_YEAR_ID)
       references PT_ASSESSMENT_YEAR;

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SUBSYSTEM_DISPLAY, SUBSYSTEM, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT)
values
   ('pt_accrual_deferral_amount', sysdate, user, 's', 'PT Accrual Deferral Amount',
    'PT Accrual Deferral Amount', 'proptax', 'proptax', 'proptax', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
    0, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('deferral_group_id', 'pt_accrual_deferral_amount', sysdate, user,
    'pt_accrual_deferral_group_filter', 'p', 'Deferral Group', 'Deferral Group', 'Deferral Group', 1,
    0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('assessment_year_id', 'pt_accrual_deferral_amount', sysdate, user, 'pt_assessment_year_filter',
    'p', 'Assessment Year', 'Assessment Year', 'Assessment Year', 2, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('allowed_amount', 'pt_accrual_deferral_amount', sysdate, user, null, 'e', 'Allowed Amount',
    'Allowed Amount', 'Allowed Amount', 3, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('deferral_pct', 'pt_accrual_deferral_amount', sysdate, user, null, 'e', 'Deferral Percent',
    'Deferral Percent', 'Deferral Percent', 4, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('time_stamp', 'pt_accrual_deferral_amount', sysdate, user, null, 'e', 'Time Stamp',
    'Time Stamp', 'Time Stamp', 100, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('user_id', 'pt_accrual_deferral_amount', sysdate, user, null, 'e', 'User ID', 'User ID',
    'User ID', 101, 0);

--
-- Update PT Accrual Type accordingly.
--
alter table PT_ACCRUAL_TYPE add DEFERRAL_GROUP_ID number(22,0);

alter table PT_ACCRUAL_TYPE
   add constraint PT_ACCR_TYPE_DEF_GRP_FK
       foreign key (DEFERRAL_GROUP_ID)
       references PT_ACCRUAL_DEFERRAL_GROUP;

alter table PT_ACCRUAL_TYPE add DEFERRAL_CREDIT_GL_ACCOUNT_ID number(22,0);

alter table PT_ACCRUAL_TYPE
   add constraint PT_ACCR_TYPE_DEF_CRED_GLA_FK
       foreign key (DEFERRAL_CREDIT_GL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table PT_ACCRUAL_TYPE add DEFERRAL_DEBIT_GL_ACCOUNT_ID number(22,0);

alter table PT_ACCRUAL_TYPE
   add constraint PT_ACCR_TYPE_DEF_DEB_GLA_FK
       foreign key (DEFERRAL_DEBIT_GL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

update PT_ACCRUAL_TYPE set DEFERRAL_DEBIT_GL_ACCOUNT_ID = DEFERRED_GL_ACCOUNT_ID;

alter table PT_ACCRUAL_TYPE drop column DEFERRED_GL_ACCOUNT_ID;

delete from POWERPLANT_COLUMNS
 where TABLE_NAME = 'pt_accrual_type'
   and COLUMN_NAME = 'deferred_gl_account_id';

update POWERPLANT_COLUMNS
   set COLUMN_RANK = COLUMN_RANK + 2
 where TABLE_NAME = 'pt_accrual_type'
   and COLUMN_RANK between 7 and 99;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('deferral_group_id', 'pt_accrual_type', sysdate, user, 'pt_accrual_deferral_group_filter', 'p',
    'Deferral Group', 'Deferral Group', 'Deferral Group', 7, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('deferral_credit_gl_account_id', 'pt_accrual_type', sysdate, user,
    'pt_gl_account_accrual_filter', 'p', 'Deferral Credit GL Account', 'Deferral Credit GL Account',
    'Deferral Credit GL Account', 8, 0);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION,
    LONG_DESCRIPTION, SHORT_LONG_DESCRIPTION, COLUMN_RANK, READ_ONLY)
values
   ('deferral_debit_gl_account_id', 'pt_accrual_type', sysdate, user,
    'pt_gl_account_accrual_filter', 'p', 'Deferral Debit GL Account', 'Deferral Debit GL Account',
    'Deferral Debit GL Account', 9, 0);

insert into POWERPLANT_DDDW
   (DROPDOWN_NAME, TIME_STAMP, USER_ID, TABLE_NAME, CODE_COL, DISPLAY_COL)
values
   ('pt_accrual_deferral_group_filter', sysdate, user, 'pt_accrual_deferral_group',
    'deferral_group_id', 'description');

--
-- Update PT Accrual Charge accordingly.  We will now have separate rows for the deferral charges.
--
alter table PT_ACCRUAL_CHARGE drop column DEFERRED_GL_ACCOUNT_ID;
alter table PT_ACCRUAL_CHARGE drop column DEFERRED_AMOUNT;

alter table PT_ACCRUAL_CHARGE add IS_DEFERRAL number(22,0);

update PT_ACCRUAL_CHARGE set IS_DEFERRAL = 0 where IS_DEFERRAL is null;

alter table PT_ACCRUAL_CHARGE modify IS_DEFERRAL not null;

alter table PT_ACCRUAL_CHARGE drop primary key drop index;

alter table PT_ACCRUAL_CHARGE
   add constraint PT_ACCRUAL_CHARGE_PK
       primary key (PROPERTY_TAX_LEDGER_ID, TAX_AUTHORITY_ID, ASSESSMENT_YEAR_ID, ACCRUAL_TYPE_ID, GL_MONTHNUM, IS_DEFERRAL)
       using index tablespace PWRPLANT_IDX;

alter table PT_ACCRUAL_CHARGE add DEFERRAL_TRUEUP_AMOUNT number(22,2);

--
-- Update PT Accrual Control accordingly.
--
alter table PT_ACCRUAL_CONTROL add CALC_DEFERRAL_TRUEUP date;

--
-- Update PT Accrual Pending to store prop_tax_company_id.
-- Also, make the assessment_year_id and month fields not nullable.
--
alter table PT_ACCRUAL_PENDING add PROP_TAX_COMPANY_ID number(22,0);

alter table PT_ACCRUAL_PENDING
   add constraint PT_ACCRUAL_PENDING_PTCO_FK
       foreign key (PROP_TAX_COMPANY_ID)
       references PT_COMPANY;

update PT_ACCRUAL_PENDING PEND
   set PEND.PROP_TAX_COMPANY_ID =
        (select CO.PROP_TAX_COMPANY_ID from COMPANY CO where CO.COMPANY_ID = PEND.COMPANY_ID);

alter table PT_ACCRUAL_PENDING modify PROP_TAX_COMPANY_ID not null;
alter table PT_ACCRUAL_PENDING modify ASSESSMENT_YEAR_ID not null;
alter table PT_ACCRUAL_PENDING modify MONTH not null;

--
-- Drop pt_accrual_allowed_amt, as it's been replaced by pt_accrual_deferral_amount.
-- Remove it from table maintenance.
--
drop table PT_ACCRUAL_ALLOWED_AMT;

delete from PP_TABLE_GROUPS where TABLE_NAME = 'pt_accrual_allowed_amt';
delete from POWERPLANT_COLUMNS where TABLE_NAME = 'pt_accrual_allowed_amt';
delete from POWERPLANT_TABLES where TABLE_NAME = 'pt_accrual_allowed_amt';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (419, 0, 10, 4, 1, 0, 30264, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030264_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
