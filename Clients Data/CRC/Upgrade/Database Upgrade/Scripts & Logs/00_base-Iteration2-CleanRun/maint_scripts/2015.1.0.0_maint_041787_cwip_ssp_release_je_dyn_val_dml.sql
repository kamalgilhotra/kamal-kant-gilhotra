/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041787_cwip_ssp_release_je_dyn_val_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2015.1.0.0 12/1/2014  Johnny Sisouphanh Dynamic Validation Type for WO - Release JE process
||============================================================================
*/

insert into wo_validation_type (
	wo_validation_type_id, description, long_description, "FUNCTION", find_company, col1, col2, col3, hard_edit)
values (
	1057, 'WO - Release JE', 'WO - Release Journal Entries', 'f_release_je_wo', 'select company_id from company_setup where company_id = <arg2>', 'month', 'company_id', 'dw_sqls', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2125, 0, 2015, 1, 0, 0, 041787, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041787_cwip_ssp_release_je_dyn_val_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;