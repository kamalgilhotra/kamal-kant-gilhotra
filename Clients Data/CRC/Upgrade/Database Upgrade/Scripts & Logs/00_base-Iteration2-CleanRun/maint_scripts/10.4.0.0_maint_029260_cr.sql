SET ECHO ON
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029260_cr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.0.0 02/08/2013 Joseph King    Point Release
||============================================================================
*/

-- ** Make all CR ACK Fields in cr_deriver_wo_link_control 30 characters to allow for keywords
declare
   V_RCOUNT number default 0;
   PPCSQL   varchar2(10) := 'PPC-SQL> ';
   PPCMSG   varchar2(10) := 'PPC-MSG> ';
   PPCERR   varchar2(10) := 'PPC-ERR> ';

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   for CSR_ALTER_SQL in (select 'alter table cr_deriver_wo_link_control modify ' || COLUMN_NAME ||
                                ' varchar2(35)' ALTER_SQL
                           from ALL_TAB_COLUMNS
                          where TABLE_NAME = 'CR_DERIVER_WO_LINK_CONTROL'
                            and OWNER = 'PWRPLANT'
                            and COLUMN_NAME not in ('CR_DERIVATION_TYPE',
                                                    'PROJECT_BASIS',
                                                    'STRING_CO',
                                                    'STRING_WO',
                                                    'STRING_TASK',
                                                    'STRING_ECT',
                                                    'OFFSET_TYPE',
                                                    'USER_ID',
                                                    'TIME_STAMP')
                            and DATA_TYPE = 'VARCHAR2'
                            and DATA_LENGTH < 35
                          order by COLUMN_ID)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_ALTER_SQL.ALTER_SQL || ';');
      begin
         execute immediate CSR_ALTER_SQL.ALTER_SQL;
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_ALTER_SQL.ALTER_SQL);
            DBMS_OUTPUT.PUT_LINE(SUBSTR('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm,
                                        1,
                                        255));
            return;
      end;
   end loop;

   if V_RCOUNT < 1 then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'No changes needed.');
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (295, 0, 10, 4, 0, 0, 29260, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029260_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;