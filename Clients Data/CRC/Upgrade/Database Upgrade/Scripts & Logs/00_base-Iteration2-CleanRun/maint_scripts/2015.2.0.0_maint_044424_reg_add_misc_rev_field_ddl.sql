/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044424_reg_add_misc_rev_field_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 07/29/2015 Sarah Byers 		 	Add new misc revenues field to reg_case_rev_req_results
||========================================================================================
*/

alter table reg_case_rev_req_results add (misc_revenue number(22,2));

comment on column reg_case_rev_req_results.misc_revenue is 'Miscellaneous Revenues in dollars';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2727, 0, 2015, 2, 0, 0, 044424, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044424_reg_add_misc_rev_field_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;