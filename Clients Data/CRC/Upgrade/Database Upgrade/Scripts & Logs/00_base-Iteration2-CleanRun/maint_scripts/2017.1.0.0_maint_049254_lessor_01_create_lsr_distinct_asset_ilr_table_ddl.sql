/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049254_lessor_01_create_lsr_distinct_asset_ilr_table_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/19/2017 Jared Watkins    Add the new table and trigger to maintain the distinct list of assets assigned to any revision of an ILR
||============================================================================
*/

create table lsr_distinct_asset_ilr(
	asset_id number(22,0) not null,
	ilr_id number(22,0) not null,
	user_id varchar2(18),
	time_stamp date,
  constraint lsr_distinct_asset_ilr_pk primary key(asset_id, ilr_id)
) organization index;

create or replace trigger insert_distinct_asset_ilrs
after insert or update on lsr_asset
declare
begin
  insert into lsr_distinct_asset_ilr(asset_id, ilr_id)
  select distinct lsr_asset_id, ilr_id
  from lsr_asset a
  where not exists (select 1 from lsr_distinct_asset_ilr b where a.lsr_asset_id = b.asset_id and a.ilr_id = b.ilr_id);
end;
/

create or replace trigger delete_distinct_asset_ilrs
after delete or update on lsr_asset
declare
begin
  delete from lsr_distinct_asset_ilr a
  where not exists (select 1 from lsr_asset b where a.asset_id = b.lsr_asset_id and a.ilr_id = b.ilr_id);
end;
/

comment on table lsr_distinct_asset_ilr is '(C)[06] The Lessor Distinct Asset/ILR table is a system-maintained table that tracks the distinct set of all Lessor ILR/asset combinations in order to have a table we can FK to from the Variable Component values table that is revision-agnostic.';

comment on column lsr_distinct_asset_ilr.asset_id is 'The Lessor Asset ID for this row';
comment on column lsr_distinct_asset_ilr.ilr_id is 'The ILR this Lessor asset is related to (on some revision, not necessarily the first or the current one)';
comment on column lsr_distinct_asset_ilr.user_id is 'Standard system-assigned identifier';
comment on column lsr_distinct_asset_ilr.time_stamp is 'Standard system-assigned timestamp';

--also add missing FK on the Variable Component Amounts table
alter table lsr_variable_component_values
add constraint lsr_vc_values_ilr_asset_fk
foreign key (ilr_id, asset_id)
references lsr_distinct_asset_ilr (ilr_id, asset_id);

--also remove the revision column which we do not actually want
alter table lsr_variable_component_values
drop column revision;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3835, 0, 2017, 1, 0, 0, 49254, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049254_lessor_01_create_lsr_distinct_asset_ilr_table_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
