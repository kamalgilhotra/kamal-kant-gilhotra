/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_052036_lessor_02_add_je_trans_types_dml.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  07/24/2018 Sarah Byers      Add new JE Trans Types for LT Curr Gain/Loss
||============================================================================
*/

-- update the existing Currency Gain/Loss Credit to be Short Term
update je_trans_type
   set description = '4052 - Lessor Currency Gain/Loss LT Credit'
 where trans_type = 4052;

-- add 4069 - Lessor Currency Gain/Loss ST Credit
insert into je_trans_type ( trans_type, description)
select 4069, '4069 - Lessor Currency Gain/Loss ST Credit'
from dual
where not exists( select 1 from je_trans_type where trans_type = 4069) ;

-- insert the above type into je_method_trans_type, if it does not already exist
insert into je_method_trans_type(je_method_id, trans_type)
select 1 AS je_method_id, t.trans_type
from je_trans_type t
  left join je_method_trans_type m on m.trans_type = t.trans_type
where t.trans_type in (4069)
  and m.trans_type is null ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8587, 0, 2017, 4, 0, 0, 52036, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052036_lessor_02_add_je_trans_types_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;