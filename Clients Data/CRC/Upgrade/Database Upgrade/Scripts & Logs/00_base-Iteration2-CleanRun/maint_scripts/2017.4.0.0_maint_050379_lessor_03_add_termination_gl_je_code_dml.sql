/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050379_lessor_03_add_termination_gl_je_code_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/31/2018 Jared Watkins    Add new GL JE Code
||============================================================================
*/

insert into standard_journal_entries (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
values ( (select nvl((max(je_id) + 1),1) from standard_journal_entries), 'Lessor Termination', 'Lessor Termination', 'Lessor Termination', 'Lessor Termination');

insert into gl_je_control (PROCESS_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN, CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
values ('Lessor Termination', (select max(je_id) from standard_journal_entries ), null, null, 'NONE', 'NONE', 'NONE', 'NONE', null, null, null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6223, 0, 2017, 4, 0, 0, 50379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050379_lessor_03_add_termination_gl_je_code_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;