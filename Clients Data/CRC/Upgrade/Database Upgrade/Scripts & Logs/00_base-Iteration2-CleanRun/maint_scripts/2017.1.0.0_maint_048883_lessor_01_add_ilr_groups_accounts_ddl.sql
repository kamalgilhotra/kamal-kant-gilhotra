 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048883_lessor_01_add_ilr_groups_accounts_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 11/09/2017 J Sisouphanh   Add accounts to Lessor ILR Groups
 ||============================================================================
 */ 

alter table LSR_ILR_GROUP
   add (INT_ACCRUAL_ACCOUNT_ID        number(22,0),
        INT_EXPENSE_ACCOUNT_ID        number(22,0),
        EXEC_ACCRUAL_ACCOUNT_ID       number(22,0),
        EXEC_EXPENSE_ACCOUNT_ID       number(22,0),
        CONT_ACCRUAL_ACCOUNT_ID       number(22,0),
        CONT_EXPENSE_ACCOUNT_ID       number(22,0),
        ST_RECEIVABLE_ACCOUNT_ID      number(22,0),
        LT_RECEIVABLE_ACCOUNT_ID      number(22,0),
        AR_ACCOUNT_ID                 number(22,0),
        UNGUARAN_RES_ACCOUNT_ID       number(22,0),
        INT_UNGUARAN_RES_ACCOUNT_ID   number(22,0),
        SELL_PROFIT_LOSS_ACCOUNT_ID  number(22,0),
        INI_DIRECT_COST_ACCOUNT_ID    number(22,0),
        PROP_PLANT_ACCOUNT_ID         number(22,0),
        ST_DEFERRED_ACCOUNT_ID        number(22,0),
        LT_DEFERRED_ACCOUNT_ID        number(22,0),
        CURR_GAIN_LOSS_ACCT_ID        number(22,0),
        CURR_GAIN_LOSS_OFFSET_ACCT_ID number(22,0)
		);
		
comment on column LSR_ILR_GROUP.INT_ACCRUAL_ACCOUNT_ID        is 'The interest accrual account';
comment on column LSR_ILR_GROUP.INT_EXPENSE_ACCOUNT_ID        is 'The interest expense account';
comment on column LSR_ILR_GROUP.EXEC_ACCRUAL_ACCOUNT_ID       is 'The executory accrual account';
comment on column LSR_ILR_GROUP.EXEC_EXPENSE_ACCOUNT_ID       is 'The executory expense account';
comment on column LSR_ILR_GROUP.CONT_ACCRUAL_ACCOUNT_ID       is 'The contingent accrual account';
comment on column LSR_ILR_GROUP.CONT_EXPENSE_ACCOUNT_ID       is 'The contingent expense account';
comment on column LSR_ILR_GROUP.ST_RECEIVABLE_ACCOUNT_ID      is 'The short term receivables account';
comment on column LSR_ILR_GROUP.LT_RECEIVABLE_ACCOUNT_ID      is 'The long term receivables account';
comment on column LSR_ILR_GROUP.AR_ACCOUNT_ID                 is 'The accounts receivable account';
comment on column LSR_ILR_GROUP.UNGUARAN_RES_ACCOUNT_ID       is 'The unguaranteed residual account';
comment on column LSR_ILR_GROUP.INT_UNGUARAN_RES_ACCOUNT_ID   is 'The interest on unguaranteed residual account';
comment on column LSR_ILR_GROUP.SELL_PROFIT_LOSS_ACCOUNT_ID  is 'The selling profit/loss account';
comment on column LSR_ILR_GROUP.INI_DIRECT_COST_ACCOUNT_ID    is 'The initial direct cost account';
comment on column LSR_ILR_GROUP.PROP_PLANT_ACCOUNT_ID         is 'The property plant and equipment account';
comment on column LSR_ILR_GROUP.ST_DEFERRED_ACCOUNT_ID        is 'The short term deferred rent account';
comment on column LSR_ILR_GROUP.LT_DEFERRED_ACCOUNT_ID        is 'The long term deferred rent account';
comment on column LSR_ILR_GROUP.CURR_GAIN_LOSS_ACCT_ID        is 'The currency gain/loss account';
comment on column LSR_ILR_GROUP.CURR_GAIN_LOSS_OFFSET_ACCT_ID is 'The currency gain/loss offset account';

alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK2
       foreign key (INT_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK3
       foreign key (INT_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK4
       foreign key (EXEC_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK5
       foreign key (EXEC_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK6
       foreign key (CONT_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK7
       foreign key (CONT_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK8
       foreign key (ST_RECEIVABLE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK9
       foreign key (LT_RECEIVABLE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK10
       foreign key (AR_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK11
       foreign key (UNGUARAN_RES_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK12
       foreign key (INT_UNGUARAN_RES_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK13
       foreign key (SELL_PROFIT_LOSS_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK14
       foreign key (INI_DIRECT_COST_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK15
       foreign key (PROP_PLANT_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK16
       foreign key (ST_DEFERRED_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK17
       foreign key (LT_DEFERRED_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK18
       foreign key (CURR_GAIN_LOSS_ACCT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
	   
alter table LSR_ILR_GROUP
   add constraint LSR_ILR_GROUP_FK19
       foreign key (CURR_GAIN_LOSS_OFFSET_ACCT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3930, 0, 2017, 1, 0, 0, 48883, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048883_lessor_01_add_ilr_groups_accounts_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
