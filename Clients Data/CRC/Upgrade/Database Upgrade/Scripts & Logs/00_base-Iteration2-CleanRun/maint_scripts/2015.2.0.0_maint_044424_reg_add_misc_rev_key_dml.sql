/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044424_reg_add_misc_rev_key_dml.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 07/29/2015 Sarah Byers 		 	Add new misc revenues key
||========================================================================================
*/

-- Add Misc Revenue Key
insert into reg_fin_monitor_key (
	reg_fin_monitor_key_id, description, process_flag, parent_key, key_level, sort_order)
values (
	215, 'Misc Revenues', 1, 210, 2, 215);

-- Change the Key Level to 1 for Operating Revenues
update reg_fin_monitor_key
	set key_level = 1
 where reg_fin_monitor_key_id = 210;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2728, 0, 2015, 2, 0, 0, 044424, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044424_reg_add_misc_rev_key_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;