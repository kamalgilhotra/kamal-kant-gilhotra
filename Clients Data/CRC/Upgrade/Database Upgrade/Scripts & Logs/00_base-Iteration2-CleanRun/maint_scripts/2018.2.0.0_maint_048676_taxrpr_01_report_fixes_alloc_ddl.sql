/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048676_taxrpr_01_report_fixes_alloc_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 10/13/2017 Eric Berger    Adding the new columns referenced by
||                                      the allocation method reports.
||============================================================================
*/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blkt_results_reporting ADD repair_qty_include NUMBER(22,0)';
EXCEPTION
    WHEN OTHERS THEN
         Dbms_Output.put_line('Column repair_qty_include already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blanket_process_report ADD repair_qty_include NUMBER(22,0)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column repair_qty_include already exists.');
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13728, 0, 2018, 2, 0, 0, 48676, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_048676_taxrpr_01_report_fixes_alloc_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;