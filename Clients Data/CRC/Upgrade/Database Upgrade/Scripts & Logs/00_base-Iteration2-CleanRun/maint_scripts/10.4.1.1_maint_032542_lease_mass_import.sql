/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032542_lease_mass_import.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Kyle Peterson  Patch Release
||============================================================================
*/

alter table LS_ILR_OPTIONS add IMPORT_RUN_ID number(22,0);

alter table LS_ILR_OPTIONS
   add constraint FK_ILR_OPT_IMP_RUN
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);

alter table LS_ASSET add IMPORT_RUN_ID number(22,0);

alter table LS_ASSET
   add constraint FK_LS_ASSET_IMP_RUN
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'import_tool', 'Mass Import Tool', 'uo_ls_importcntr_mass_wksp');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = null
 where MENU_IDENTIFIER = 'menu_wksp_import'
 and MODULE = 'LESSEE';

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_import_new', 2, 1, 'New Import', 'menu_wksp_import', 'menu_wksp_import', 1);

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_import_tool', 2, 2, 'Mass Import Tool', 'menu_wksp_import', 'import_tool', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (639, 0, 10, 4, 1, 1, 32542, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032542_lease_mass_import.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
