/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051707_lessee_01_add_depr_calc_method_import_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/19/2018 Sarah Byers      Add depr calc method to ILR import
||============================================================================
*/

BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_ilr add depr_calc_method NUMBER(22,0) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/


BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_ilr_archive add depr_calc_method NUMBER(22,0) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/


COMMENT ON COLUMN ls_import_ilr.depr_calc_method IS '0: Straight Line/FERC, 1: Units of Production.';
COMMENT ON COLUMN ls_import_ilr_archive.depr_calc_method IS '0: Straight Line/FERC, 1: Units of Production.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6823, 0, 2017, 4, 0, 0, 51707, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051707_lessee_01_add_depr_calc_method_import_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;