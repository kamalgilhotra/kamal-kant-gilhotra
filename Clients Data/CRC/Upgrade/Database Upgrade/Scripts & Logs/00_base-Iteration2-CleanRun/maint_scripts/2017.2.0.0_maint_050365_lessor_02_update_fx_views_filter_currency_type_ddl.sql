/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050365_lessor_02_update_fx_views_filter_currency_type_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.2.0.0 03/05/2018 Andrew Hill   	  Filter FX Views on Currency Type
||============================================================================
*/

CREATE OR REPLACE VIEW V_LSR_INVOICE_FX_VW AS
  WITH
cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
 invoice_id,
 invoice_number,
 inv.ilr_id,
 gl_posting_Mo_yr,
 invoice_principal * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_principal,
 invoice_interest * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_interest,
 invoice_executory * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_executory,
 invoice_contingent * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_contingent,
 lease.lease_id,
 lease.lease_number,
 cur.ls_cur_type ls_cur_type,
 cr.exchange_date,
 lease.contract_currency_id,
 cur.currency_id display_currency_id,
 cs.currency_id company_currency_id,
 cur.iso_code,
 cur.currency_display_symbol
 FROM lsr_invoice inv
 INNER JOIN lsr_ilr ilr
   on inv.ilr_id = ilr.ilr_id
 INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
 INNER JOIN lsr_lease lease
    ON ilr.lease_id = lease.lease_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 LEFT OUTER JOIN ls_lease_calculated_date_rates cr
    ON lease.contract_currency_id = cr.contract_currency_id
   AND cur.currency_id = cr.company_currency_id
   AND ilr.company_id = cr.company_id
   AND inv.gl_posting_mo_yr = cr.accounting_month
 WHERE Nvl(cr.exchange_rate_type_id, 1) = 1
 AND cs.currency_type_id = 1;
/
 
 CREATE OR REPLACE VIEW V_LSR_INVOICE_LINE_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code,
                1                                    historic_rate
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code,
                0
         FROM   currency company_cur)
SELECT lil.invoice_id,
       lil.invoice_line_number,
       lil.invoice_type_id,
       lil.amount * Decode(ls_cur_type, 2, Nvl(rate, historic_rate), 1) amount,
       lil.adjustment_amount,
       lil.description,
       lil.notes,
       lease.contract_currency_id,
       cs.currency_id                       company_currency_id,
       cur.ls_cur_type                      AS ls_cur_type,
       Nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
       Decode(ls_cur_type, 2, Nvl(rate, historic_rate),1) rate,
       cur.iso_code,
       cur.currency_display_symbol,
   lil.set_of_books_id
FROM   lsr_invoice_line lil
       inner join lsr_invoice li
               ON ( li.invoice_id = lil.invoice_id )
       inner join lsr_ilr ilr
           ON li.ilr_id = ilr.ilr_id
       inner join lsr_lease lease
               ON ilr.lease_id = lease.lease_id
       inner join currency_schema cs
               ON ilr.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = lease.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       left outer join ls_lease_calculated_date_rates cr
                    ON ( cr.company_id = ilr.company_id
                         AND cr.contract_currency_id =
                             lease.contract_currency_id
                         AND cr.company_currency_id = cs.currency_id
                         AND cr.accounting_month = li.gl_posting_mo_yr )
WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1
AND cs.currency_type_id = 1;
/

CREATE OR REPLACE VIEW V_LSR_MONTHLY_ACCR_STG_FX_VW AS
WITH
cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
acr.accrual_id,
acr.accrual_type_id,
acr.ilr_id,
gl_posting_mo_yr,
acr.set_of_books_id,
acr.amount * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) amount,
cur.ls_cur_type ls_cur_type,
cr.exchange_date,
lease.contract_currency_id,
cur.currency_id display_currency_id,
cs.currency_id company_currency_id,
cur.iso_code,
cur.currency_display_symbol
FROM lsr_monthly_accrual_stg acr
INNER JOIN lsr_ilr ilr
  on acr.ilr_id = ilr.ilr_id
INNER JOIN currency_schema cs
  ON ilr.company_id = cs.company_id
INNER JOIN lsr_lease lease
  ON ilr.lease_id = lease.lease_id
INNER JOIN cur
  ON (cur.ls_cur_type = 1 AND
      cur.currency_id = lease.contract_currency_id)
  OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
LEFT OUTER JOIN ls_lease_calculated_date_rates cr
  ON lease.contract_currency_id = cr.contract_currency_id
  AND cur.currency_id = cr.company_currency_id
  AND ilr.company_id = cr.company_id
  AND acr.gl_posting_mo_yr = cr.accounting_month
WHERE Nvl(cr.exchange_rate_type_id, 1) = 1
AND cs.currency_type_id = 1;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4163, 0, 2017, 2, 0, 0, 50365, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050365_lessor_02_update_fx_views_filter_currency_type_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;