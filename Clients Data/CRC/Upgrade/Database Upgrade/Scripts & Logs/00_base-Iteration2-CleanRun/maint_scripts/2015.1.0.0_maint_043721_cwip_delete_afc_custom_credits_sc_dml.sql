/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043721_cwip_delete_afc_custom_credits_sc_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 4/28/2015  Sarah Byers      Remove the 'AFUDC - CUSTOMIZED CREDITS' system control
||============================================================================
*/

delete from pp_system_control_company
 where upper(control_name) = 'AFUDC - CUSTOMIZED CREDITS';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2533, 0, 2015, 1, 0, 0, 043721, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043721_cwip_delete_afc_custom_credits_sc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;