/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048623_lease_07_update_outer_fx_views_fix_cr_now.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/14/2017 Andrew Hill      Update outer fx views to correct bug introduced into cr_now calcuation
||============================================================================
*/


create or replace view v_ls_asset_schedule_fx_vw as
with
cur as (select  ls_currency_type_id as ls_cur_type, 
                currency_id, 
                currency_display_symbol, 
                iso_code, 
                case ls_currency_type_id
                when 1 then 1
                else null
              END as contract_approval_rate
        from currency
        cross join ls_lease_currency_type),
open_month AS (
  SELECT company_id, Min(gl_posting_mo_yr) open_month
  FROM ls_process_control
  WHERE open_next IS NULL
  GROUP BY company_id
),
calc_rate AS (
  SELECT a.company_id, a.contract_currency_id, a.company_currency_id,
    a.accounting_month, a.exchange_date, a.rate, b.rate prev_rate
  FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b
  ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = Add_Months(b.accounting_month, 1)
),
cr_now as (
  select currency_from, currency_to, rate
  from (select currency_from, currency_to, rate, row_number() over(partition by currency_from, currency_to order by exchange_date) as rn
        from mv_currency_rates
        where trunc(exchange_date, 'MONTH') <= trunc(sysdate, 'MONTH'))
  WHERE rn = 1
)
SELECT
  las.ilr_id ilr_id,
  las.ls_asset_id ls_asset_id,
  las.revision revision,
  las.set_of_books_id set_of_books_id,
  las.MONTH MONTH,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  cr.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  las.contract_currency_id,
  cur.currency_id,
  cr.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  las.residual_amount * nvl(calc_rate.rate, cr.rate) residual_amount,
  las.term_penalty * nvl(calc_rate.rate, cr.rate) term_penalty,
  las.bpo_price * nvl(calc_rate.rate, cr.rate) bpo_price,
  las.beg_capital_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) beg_capital_cost,
  las.end_capital_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) end_capital_cost,
  las.beg_obligation * nvl(calc_rate.rate, cr.rate) beg_obligation,
  las.end_obligation * nvl(calc_rate.rate, cr.rate) end_obligation,
  las.beg_lt_obligation * nvl(calc_rate.rate, cr.rate) beg_lt_obligation,
  las.end_lt_obligation * nvl(calc_rate.rate, cr.rate) end_lt_obligation,
  las.interest_accrual * nvl(calc_rate.rate, cr.rate) interest_accrual,
  las.principal_accrual * nvl(calc_rate.rate, cr.rate) principal_accrual,
  las.interest_paid * nvl(calc_rate.rate, cr.rate) interest_paid,
  las.principal_paid * nvl(calc_rate.rate, cr.rate) principal_paid,
  las.executory_accrual1 * nvl(calc_rate.rate, cr.rate) executory_accrual1,
  las.executory_accrual2 * nvl(calc_rate.rate, cr.rate) executory_accrual2,
  las.executory_accrual3 * nvl(calc_rate.rate, cr.rate) executory_accrual3,
  las.executory_accrual4 * nvl(calc_rate.rate, cr.rate) executory_accrual4,
  las.executory_accrual5 * nvl(calc_rate.rate, cr.rate) executory_accrual5,
  las.executory_accrual6 * nvl(calc_rate.rate, cr.rate) executory_accrual6,
  las.executory_accrual7 * nvl(calc_rate.rate, cr.rate) executory_accrual7,
  las.executory_accrual8 * nvl(calc_rate.rate, cr.rate) executory_accrual8,
  las.executory_accrual9 * nvl(calc_rate.rate, cr.rate) executory_accrual9,
  las.executory_accrual10 * nvl(calc_rate.rate, cr.rate) executory_accrual10,
  las.executory_paid1 * nvl(calc_rate.rate, cr.rate) executory_paid1,
  las.executory_paid2 * nvl(calc_rate.rate, cr.rate) executory_paid2,
  las.executory_paid3 * nvl(calc_rate.rate, cr.rate) executory_paid3,
  las.executory_paid4 * nvl(calc_rate.rate, cr.rate) executory_paid4,
  las.executory_paid5 * nvl(calc_rate.rate, cr.rate) executory_paid5,
  las.executory_paid6 * nvl(calc_rate.rate, cr.rate) executory_paid6,
  las.executory_paid7 * nvl(calc_rate.rate, cr.rate) executory_paid7,
  las.executory_paid8 * nvl(calc_rate.rate, cr.rate) executory_paid8,
  las.executory_paid9 * nvl(calc_rate.rate, cr.rate) executory_paid9,
  las.executory_paid10 * nvl(calc_rate.rate, cr.rate) executory_paid10,
  las.contingent_accrual1 * nvl(calc_rate.rate, cr.rate) contingent_accrual1,
  las.contingent_accrual2 * nvl(calc_rate.rate, cr.rate) contingent_accrual2,
  las.contingent_accrual3 * nvl(calc_rate.rate, cr.rate) contingent_accrual3,
  las.contingent_accrual4 * nvl(calc_rate.rate, cr.rate) contingent_accrual4,
  las.contingent_accrual5 * nvl(calc_rate.rate, cr.rate) contingent_accrual5,
  las.contingent_accrual6 * nvl(calc_rate.rate, cr.rate) contingent_accrual6,
  las.contingent_accrual7 * nvl(calc_rate.rate, cr.rate) contingent_accrual7,
  las.contingent_accrual8 * nvl(calc_rate.rate, cr.rate) contingent_accrual8,
  las.contingent_accrual9 * nvl(calc_rate.rate, cr.rate) contingent_accrual9,
  las.contingent_accrual10 * nvl(calc_rate.rate, cr.rate) contingent_accrual10,
  las.contingent_paid1 * nvl(calc_rate.rate, cr.rate) contingent_paid1,
  las.contingent_paid2 * nvl(calc_rate.rate, cr.rate) contingent_paid2,
  las.contingent_paid3 * nvl(calc_rate.rate, cr.rate) contingent_paid3,
  las.contingent_paid4 * nvl(calc_rate.rate, cr.rate) contingent_paid4,
  las.contingent_paid5 * nvl(calc_rate.rate, cr.rate) contingent_paid5,
  las.contingent_paid6 * nvl(calc_rate.rate, cr.rate) contingent_paid6,
  las.contingent_paid7 * nvl(calc_rate.rate, cr.rate) contingent_paid7,
  las.contingent_paid8 * nvl(calc_rate.rate, cr.rate) contingent_paid8,
  las.contingent_paid9 * nvl(calc_rate.rate, cr.rate) contingent_paid9,
  las.contingent_paid10 * nvl(calc_rate.rate, cr.rate) contingent_paid10,
  las.current_lease_cost * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) current_lease_cost,
  decode(calc_rate.rate, NULL, 0, ( las.beg_obligation * ( calc_rate.rate - coalesce(calc_rate.prev_rate, las.in_service_exchange_rate, calc_rate.rate, 0 ) ) ) ) gain_loss_fx,
  las.depr_expense * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) depr_expense,
  las.begin_reserve * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) begin_reserve,
  las.end_reserve * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) end_reserve,
  las.depr_exp_alloc_adjust * nvl(nvl(cur.contract_approval_rate, las.in_service_exchange_rate), cr_now.rate) depr_exp_alloc_adjust,
  las.asset_description,
  las.leased_asset_number,
  las.fmv * nvl(calc_rate.rate, cr.rate) fmv,
  las.is_om,
  las.approved_revision,
  las.lease_cap_type_id,
  las.ls_asset_status_id,
  las.retirement_date
from mv_multicurr_ls_asset_inner las
INNER JOIN currency_schema cs
  on las.company_id = cs.company_id
inner join cur on cur.currency_id = case cur.ls_cur_type
                                      when 1 then las.contract_currency_id
                                      when 2 then cs.currency_id
                                      else null
                                    END
INNER JOIN open_month
  on las.company_id = open_month.company_id
INNER JOIN mv_currency_rates cr
  ON cur.currency_id = cr.currency_to
  and las.contract_currency_id = cr.currency_from
  AND trunc(cr.exchange_date, 'MONTH') = trunc(las.month, 'MONTH')
INNER JOIN cr_now
  ON cur.currency_id = cr_now.currency_to
  AND las.contract_currency_id = cr_now.currency_from
LEFT OUTER JOIN calc_rate
  ON las.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND las.company_id = calc_rate.company_id
  and las.month = calc_rate.accounting_month
WHERE cs.currency_type_id = 1;


CREATE OR REPLACE VIEW V_LS_ILR_SCHEDULE_FX_VW as
WITH
cur as (select  ls_currency_type_id as ls_cur_type, 
                currency_id, 
                currency_display_symbol, 
                iso_code, 
                case ls_currency_type_id
                when 1 then 1
                else null
              END as contract_approval_rate
        from currency
        cross join ls_lease_currency_type),
open_month AS (
  SELECT company_id, Min(gl_posting_mo_yr) open_month
  FROM ls_process_control
  WHERE open_next IS NULL
  GROUP BY company_id
),
calc_rate AS (
  SELECT a.company_id, a.contract_currency_id, a.company_currency_id, a.accounting_month, a.exchange_date, a.rate, b.rate prev_rate
  FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b
  ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = Add_Months(b.accounting_month, 1)
),
cr_now as (
  select currency_from, currency_to, rate
  from (select currency_from, currency_to, rate, row_number() over(partition by currency_from, currency_to order by exchange_date) as rn
        from mv_currency_rates
        where trunc(exchange_date, 'MONTH') <= trunc(sysdate, 'MONTH'))
  WHERE rn = 1
)
SELECT
  lis.ilr_id ilr_id,
  lis.ilr_number,
  lease.lease_id,
  lease.lease_number,
  lis.current_revision,
  lis.revision revision,
  lis.set_of_books_id set_of_books_id,
  lis.MONTH MONTH,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  cr.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  lease.contract_currency_id,
  cur.currency_id display_currency_id,
  cr.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  lis.is_om,
  lis.purchase_option_amt * nvl(calc_rate.rate, cr.rate) purchase_option_amt,
  lis.termination_amt * nvl(calc_rate.rate, cr.rate) termination_amt,
  lis.net_present_value * nvl(calc_rate.rate, cr.rate) net_present_value,
  lis.capital_cost * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) capital_cost,
  lis.beg_capital_cost * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) beg_capital_cost,
  lis.end_capital_cost * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) end_capital_cost,
  lis.beg_obligation * nvl(calc_rate.rate, cr.rate) beg_obligation,
  lis.end_obligation * nvl(calc_rate.rate, cr.rate) end_obligation,
  lis.beg_lt_obligation * nvl(calc_rate.rate, cr.rate) beg_lt_obligation,
  lis.end_lt_obligation * nvl(calc_rate.rate, cr.rate) end_lt_obligation,
  lis.interest_accrual * nvl(calc_rate.rate, cr.rate) interest_accrual,
  lis.principal_accrual * nvl(calc_rate.rate, cr.rate) principal_accrual,
  lis.interest_paid * nvl(calc_rate.rate, cr.rate) interest_paid,
  lis.principal_paid * nvl(calc_rate.rate, cr.rate) principal_paid,
  lis.executory_accrual1 * nvl(calc_rate.rate, cr.rate) executory_accrual1,
  lis.executory_accrual2 * nvl(calc_rate.rate, cr.rate) executory_accrual2,
  lis.executory_accrual3 * nvl(calc_rate.rate, cr.rate) executory_accrual3,
  lis.executory_accrual4 * nvl(calc_rate.rate, cr.rate) executory_accrual4,
  lis.executory_accrual5 * nvl(calc_rate.rate, cr.rate) executory_accrual5,
  lis.executory_accrual6 * nvl(calc_rate.rate, cr.rate) executory_accrual6,
  lis.executory_accrual7 * nvl(calc_rate.rate, cr.rate) executory_accrual7,
  lis.executory_accrual8 * nvl(calc_rate.rate, cr.rate) executory_accrual8,
  lis.executory_accrual9 * nvl(calc_rate.rate, cr.rate) executory_accrual9,
  lis.executory_accrual10 * nvl(calc_rate.rate, cr.rate) executory_accrual10,
  lis.executory_paid1 * nvl(calc_rate.rate, cr.rate) executory_paid1,
  lis.executory_paid2 * nvl(calc_rate.rate, cr.rate) executory_paid2,
  lis.executory_paid3 * nvl(calc_rate.rate, cr.rate) executory_paid3,
  lis.executory_paid4 * nvl(calc_rate.rate, cr.rate) executory_paid4,
  lis.executory_paid5 * nvl(calc_rate.rate, cr.rate) executory_paid5,
  lis.executory_paid6 * nvl(calc_rate.rate, cr.rate) executory_paid6,
  lis.executory_paid7 * nvl(calc_rate.rate, cr.rate) executory_paid7,
  lis.executory_paid8 * nvl(calc_rate.rate, cr.rate) executory_paid8,
  lis.executory_paid9 * nvl(calc_rate.rate, cr.rate) executory_paid9,
  lis.executory_paid10 * nvl(calc_rate.rate, cr.rate) executory_paid10,
  lis.contingent_accrual1 * nvl(calc_rate.rate, cr.rate) contingent_accrual1,
  lis.contingent_accrual2 * nvl(calc_rate.rate, cr.rate) contingent_accrual2,
  lis.contingent_accrual3 * nvl(calc_rate.rate, cr.rate) contingent_accrual3,
  lis.contingent_accrual4 * nvl(calc_rate.rate, cr.rate) contingent_accrual4,
  lis.contingent_accrual5 * nvl(calc_rate.rate, cr.rate) contingent_accrual5,
  lis.contingent_accrual6 * nvl(calc_rate.rate, cr.rate) contingent_accrual6,
  lis.contingent_accrual7 * nvl(calc_rate.rate, cr.rate) contingent_accrual7,
  lis.contingent_accrual8 * nvl(calc_rate.rate, cr.rate) contingent_accrual8,
  lis.contingent_accrual9 * nvl(calc_rate.rate, cr.rate) contingent_accrual9,
  lis.contingent_accrual10 * nvl(calc_rate.rate, cr.rate) contingent_accrual10,
  lis.contingent_paid1 * nvl(calc_rate.rate, cr.rate) contingent_paid1,
  lis.contingent_paid2 * nvl(calc_rate.rate, cr.rate) contingent_paid2,
  lis.contingent_paid3 * nvl(calc_rate.rate, cr.rate) contingent_paid3,
  lis.contingent_paid4 * nvl(calc_rate.rate, cr.rate) contingent_paid4,
  lis.contingent_paid5 * nvl(calc_rate.rate, cr.rate) contingent_paid5,
  lis.contingent_paid6 * nvl(calc_rate.rate, cr.rate) contingent_paid6,
  lis.contingent_paid7 * nvl(calc_rate.rate, cr.rate) contingent_paid7,
  lis.contingent_paid8 * nvl(calc_rate.rate, cr.rate) contingent_paid8,
  lis.contingent_paid9 * nvl(calc_rate.rate, cr.rate) contingent_paid9,
  lis.contingent_paid10 * nvl(calc_rate.rate, cr.rate) contingent_paid10,
  lis.current_lease_cost * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) current_lease_cost,
  lis.beg_obligation * (nvl(calc_rate.rate, 0) - nvl(calc_rate.prev_rate, 0)) gain_loss_fx,
  lis.depr_expense * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) depr_expense,
  lis.begin_reserve * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) begin_reserve,
  lis.end_reserve * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) end_reserve,
  lis.depr_exp_alloc_adjust * nvl(nvl(cur.contract_approval_rate, lis.in_service_exchange_rate), cr_now.rate) depr_exp_alloc_adjust
FROM v_multicurrency_lis_inner lis
INNER JOIN ls_lease lease
  ON lis.lease_id = lease.lease_id
INNER JOIN currency_schema cs
  ON lis.company_id = cs.company_id
inner join cur on cur.currency_id = case cur.ls_cur_type
                                      when 1 then lease.contract_currency_id
                                      when 2 then cs.currency_id
                                      else null
                                    END
INNER JOIN open_month
  on lis.company_id = open_month.company_id
INNER JOIN mv_currency_rates cr
  ON cur.currency_id = cr.currency_to
  and lease.contract_currency_id = cr.currency_from
  AND trunc(cr.exchange_date, 'MONTH') = trunc(lis.month, 'MONTH')
INNER JOIN cr_now
  ON cur.currency_id = cr_now.currency_to
  AND lease.contract_currency_id = cr_now.currency_from
LEFT OUTER JOIN calc_rate
  ON lease.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND lis.company_id = calc_rate.company_id
  and lis.month = calc_rate.accounting_month
WHERE cs.currency_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3648, 0, 2017, 1, 0, 0, 48623, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048623_lease_07_update_outer_fx_views_fix_cr_now.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;