/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036900_reg_misc_new_db_fixes.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/03/2014 Shane "C" Ward
||============================================================================
*/

--Maint Script found from testing AssetsDev

--Missing Not Mapped Component Asset Depr Interface
insert into REG_DEPR_FAMILY_COMPONENTS
   (REG_COMPONENT_ID, REG_FAMILY_ID, DESCRIPTION)
values
   (0, 1, 'Not Mapped');

--Missing Not Mapped and Not Used in Recon Items
insert into REG_HIST_RECON_ITEMS (RECON_ID, DESCRIPTION, STATUS) values (-1, 'Not Mapped', 1);
insert into REG_HIST_RECON_ITEMS (RECON_ID, DESCRIPTION, STATUS) values (0, 'Not Used', 1);

--Fix names of ledger adjustments
update PPBASE_WORKSPACE
   set LABEL = 'History Ledger Adjustments'
 where WORKSPACE_IDENTIFIER = 'uo_reg_ledger_adjust_manage_ws_hist';

update PPBASE_WORKSPACE
   set LABEL = 'Forecast Ledger Adjustments'
 where WORKSPACE_IDENTIFIER = 'uo_reg_ledger_adjust_manage_ws_fore';

--Fix to status checks
update REG_STATUS_CHECK
   set CHECK_SQL = 'SELECT DISTINCT ''Adjustment '' || a.description || '' parameters are not complete in case '' || c.case_name || '' and test year '' || ac.test_yr_ended || ''.'', ac.adjustment_id FROM reg_case c, reg_case_adjustment a, reg_case_adjust_acct ac WHERE c.reg_case_id = a.reg_case_id AND a.reg_case_id = ac.reg_case_id AND a.adjustment_id = ac.adjustment_id AND a.adjustment_id <> 0 AND ac.input_percentage IS NULL AND ac.input_amount IS NULL AND c.reg_case_id = <case_id> AND ac.test_yr_ended = <case_year>'
 where REG_STATUS_CHECK_ID = 16;

update REG_STATUS_CHECK
   set CHECK_SQL = 'SELECT DISTINCT ''Adjustment '' || a.description || '' monthly parameters are not complete in case '' || c.case_name || '' and test year '' || ac.test_yr_ended || ''.'', ac.adjustment_id FROM reg_case c, reg_case_adjustment a, reg_case_adjust_acct_monthly ac WHERE c.reg_case_id = a.reg_case_id AND a.reg_case_id = ac.reg_case_id AND a.adjustment_id = ac.adjustment_id AND a.adjustment_id <> 0 AND ac.input_amount IS NULL AND c.reg_case_id = <case_id>'
 where REG_STATUS_CHECK_ID = 17;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1007, 0, 10, 4, 2, 0, 36900, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036900_reg_misc_new_db_fixes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;