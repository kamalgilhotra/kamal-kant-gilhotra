/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052790_lessee_01_mat_analysis_updates_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.1 12/5/2018 C.Yura   Maturity Analysis needs discounted term penalty,residual,bpo
||============================================================================
*/

alter table ls_ilr_npv_fx_temp add disc_residual_amount number(22,2);
alter table ls_ilr_npv_fx_temp add disc_term_penalty number(22,2);
alter table ls_ilr_npv_fx_temp add disc_bpo_price number(22,2);

comment on column ls_ilr_npv_fx_temp.disc_residual_amount is 'The discounted guaranteed residual amount.';
comment on column ls_ilr_npv_fx_temp.disc_term_penalty is 'The discounted penalty amount for terminating the lease.';
comment on column ls_ilr_npv_fx_temp.disc_bpo_price is 'The discounted bargain purchase amount.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12764, 0, 2018, 1, 0, 1, 52790, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_052790_lessee_01_mat_analysis_updates_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;