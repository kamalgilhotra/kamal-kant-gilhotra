SET SERVEROUTPUT ON
SET LINESIZE 200

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038574_depr_data_check.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By      Reason for Change
|| -------- ---------- --------------- ---------------------------------------
|| 10.4.2.2 07/14/2014 Lee Quinn
||============================================================================
*/

declare
   PPCMSG  varchar2(10) := 'PPC' || '-MSG> ';
   PPCERR  varchar2(10) := 'PPC' || '-ERR> ';
   NEWLINE varchar2(10) := CHR(10);

   L_SKIP_CHECK boolean := false;
   L_COUNT      number;
   L_ERROR      boolean := false;
   L_MSG        varchar2(256);

begin
   if L_SKIP_CHECK = false then
      --find groups with negative cor balance
      select count(*)
        into L_COUNT
        from (select distinct G.DEPR_GROUP_ID,
                              G.COMPANY_ID,
                              G.DESCRIPTION,
                              GL_POST_MO_YR,
                              L.SET_OF_BOOKS_ID,
                              COST_OF_REMOVAL_PCT,
                              NET_SALVAGE_PCT,
                              L.END_BALANCE,
                              L.COR_END_RESERVE,
                              L.END_RESERVE,
                              L.COR_BEG_RESERVE,
                              F.FACTOR,
                              F.VINTAGE
                from DEPR_LEDGER L, DEPR_GROUP G, DEPR_METHOD_RATES R, DEPR_RES_ALLO_FACTORS F
               where L.DEPR_GROUP_ID = G.DEPR_GROUP_ID
                 and SIGN(COR_END_RESERVE) <> SIGN(END_BALANCE)
                 and COR_END_RESERVE <> 0
                 and END_BALANCE <> 0
                 and R.DEPR_METHOD_ID = G.DEPR_METHOD_ID
                 and R.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
                 and R.COST_OF_REMOVAL_PCT <> 0
                 and R.EFFECTIVE_DATE = (select max(Z.EFFECTIVE_DATE)
                                           from DEPR_METHOD_RATES Z
                                          where R.DEPR_METHOD_ID = Z.DEPR_METHOD_ID
                                            and R.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                                            and Z.EFFECTIVE_DATE <= L.GL_POST_MO_YR)
                 and (COMPANY_ID, GL_POST_MO_YR) in (select COMPANY_ID, max(ACCOUNTING_MONTH)
                                                       from CPR_CONTROL
                                                      where POWERPLANT_CLOSED is not null
                                                      group by COMPANY_ID)
                 and F.DEPR_GROUP_ID = L.DEPR_GROUP_ID
                 and F.MONTH = L.GL_POST_MO_YR
                 and F.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
                 and F.FACTOR = 1 - R.NET_SALVAGE_PCT
               order by COMPANY_ID, DEPR_GROUP_ID, SET_OF_BOOKS_ID, VINTAGE);

      if L_COUNT > 0 then
         L_ERROR := true;
         L_MSG   := L_COUNT || ' allocation factors affectd by negative cor balance were found';
         DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
      else
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Data check for negative cor balances passed.');
         DBMS_OUTPUT.PUT_LINE(NEWLINE);
      end if;

      -- find groups with 0% cor pct
      select count(*)
        into L_COUNT
        from (select distinct G.DEPR_GROUP_ID,
                              G.COMPANY_ID,
                              G.DESCRIPTION,
                              GL_POST_MO_YR,
                              L.SET_OF_BOOKS_ID,
                              COST_OF_REMOVAL_PCT,
                              NET_SALVAGE_PCT,
                              L.END_BALANCE,
                              L.COR_END_RESERVE,
                              L.END_RESERVE,
                              L.COR_BEG_RESERVE,
                              F.FACTOR,
                              F.VINTAGE
                from DEPR_LEDGER L, DEPR_GROUP G, DEPR_METHOD_RATES R, DEPR_RES_ALLO_FACTORS F
               where L.DEPR_GROUP_ID = G.DEPR_GROUP_ID
                 and COR_END_RESERVE <> 0
                 and END_BALANCE <> 0
                 and R.DEPR_METHOD_ID = G.DEPR_METHOD_ID
                 and R.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
                 and R.COST_OF_REMOVAL_PCT = 0
                 and R.EFFECTIVE_DATE = (select max(Z.EFFECTIVE_DATE)
                                           from DEPR_METHOD_RATES Z
                                          where R.DEPR_METHOD_ID = Z.DEPR_METHOD_ID
                                            and R.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                                            and Z.EFFECTIVE_DATE <= L.GL_POST_MO_YR)
                 and (COMPANY_ID, GL_POST_MO_YR) in (select COMPANY_ID, max(ACCOUNTING_MONTH)
                                                       from CPR_CONTROL
                                                      where POWERPLANT_CLOSED is not null
                                                      group by COMPANY_ID)
                 and F.DEPR_GROUP_ID = L.DEPR_GROUP_ID
                 and F.MONTH = L.GL_POST_MO_YR
                 and F.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
                 and F.FACTOR = 1 - R.NET_SALVAGE_PCT
               order by 2, 1, 3);

      if L_COUNT > 0 then
         L_ERROR := true;
         L_MSG   := L_COUNT || ' allocation factors affectd by COR reserve with 0% cor pct were found';
         DBMS_OUTPUT.PUT_LINE(PPCMSG || L_MSG);
      else
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Data check for 0% cor pct passed.');
         DBMS_OUTPUT.PUT_LINE(NEWLINE);
      end if;

      if L_ERROR = true then
         -- Raise application error
         DBMS_OUTPUT.PUT_LINE(NEWLINE);
         DBMS_OUTPUT.PUT_LINE('     Please edit the script and read the comments at the bottom ');
         DBMS_OUTPUT.PUT_LINE('     for further instructions how to evaluate the data issues.');
         DBMS_OUTPUT.PUT_LINE(NEWLINE);
         DBMS_OUTPUT.PUT_LINE('Once the system has been configured, edit this script and set the L_SKIP_CHECK variable to "true"');
         DBMS_OUTPUT.PUT_LINE('to turn off data validation checking.');
         DBMS_OUTPUT.PUT_LINE(NEWLINE);
         RAISE_APPLICATION_ERROR(-20000, 'Read the above comments and make the necessary changes to the system.');
      end if;
   else
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Data check was skipped.');
   end if;
end;
/

/*
Maintenance 38574 fixed a problem with depr_res_allo_factors when the
system must allocate life reserve + COR reserve and the pro-rata the
total factor between life_factor and cor_factor.

When this is triggered the maximum factor should be 1 - net_salvage_pct + cor_pct
but prior to v10.4.2.2 the maximum factor was not including cor_pct and was
limited to 1 - net_salvage_pct.

Depending on the change in allocation factors, this could cause a significant
shift in allocated reserve which is used for net book value (NBV) reporting,
retirement gain/loss calculation, reserve amount for asset transfer and possibly
property tax processing.

Run the queries that are found on the Confluence page to diagnose the data issue. The data is not necessarily
wrong but communication with the client may be needed to explain changes in allocation results that may occur.

Please check PowerPlan confluence page for further updates:  https://powerme.pwrplan.com/x/uoKzAQ
*/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1285, 0, 10, 4, 2, 2, 38574, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038574_depr_data_check.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
