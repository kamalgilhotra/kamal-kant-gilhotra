/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053052_pwrtax_01_norm_schema_trueup_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.2 03/04/2019  David Conway     Merged to base
|| 2018.1.0.2 03/04/2019  David Conway     Moved new norm trueup to Admin menu.
||============================================================================
*/

-----------------------------
--set up lefthand menu access
-----------------------------
DELETE FROM ppbase_menu_items WHERE workspace_identifier = 'def_input_norm_schema_trueup_beg';
DELETE FROM ppbase_workspace WHERE workspace_identifier = 'def_input_norm_schema_trueup_beg';

INSERT INTO ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, object_type_id)
SELECT 'powertax', 'def_input_norm_schema_trueup_beg', 'Norm Schema Trueup Beg', 'uo_tax_dfit_wksp_norm_trueup_beg',1 FROM dual;

INSERT INTO ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
SELECT 'powertax', 'norm_schema_trueup_beg', 2, 3, 'Norm Schema Trueup Beg', 'admin', 'def_input_norm_schema_trueup_beg', 1 FROM dual;

---------------------------------
--set up filter options in window
---------------------------------
DELETE FROM pp_dynamic_filter_mapping
WHERE pp_report_filter_id IN (SELECT pp_report_filter_id FROM pp_reports_filter WHERE description='PowerTax - Norm Schema Trueup Beg');

DELETE FROM pp_reports_filter
WHERE description = 'PowerTax - Norm Schema Trueup Beg';

DELETE FROM pp_dynamic_filter
WHERE sqls_column_expression='deferred_income_tax.tax_year'
AND single_select_only = 1
AND filter_id <> 156;

INSERT INTO pp_reports_filter (pp_report_filter_id, description, table_name, filter_uo_name)
SELECT  108 /* max filter_id + 1 from MasterDB */
      , 'PowerTax - Norm Schema Trueup Beg'
      , NULL
      , 'uo_ppbase_tab_filter_dynamic'
FROM dual
;

INSERT INTO pp_dynamic_filter (filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, exclude_from_where, not_retrieve_immediate, invisible)
SELECT  291 /* from MasterDB: SELECT NVL(Max(filter_id),0)+1 FROM pp_dynamic_filter */
      , label
      , input_type
      , 'deferred_income_tax.tax_year' sqls_column_expression
      , sqls_where_clause
      , dw
      , dw_id
      , dw_description
      , dw_id_datatype
      , required
      , single_select_only
      , valid_operators
      , data
      , exclude_from_where
      , not_retrieve_immediate
      , invisible
FROM pp_dynamic_filter
WHERE filter_id=156   --tax_deprecaition.tax_year single select only filter
;

INSERT INTO pp_dynamic_filter_mapping (pp_report_filter_id, filter_id)
SELECT  pp_report_filter_id, filter_id
FROM    pp_reports_filter
CROSS JOIN (SELECT df.filter_id
            FROM pp_reports_filter rf
            JOIN pp_dynamic_filter_mapping dfm ON dfm.pp_report_filter_id=rf.pp_report_filter_id
            JOIN pp_dynamic_filter df ON df.filter_id=dfm.filter_id
            WHERE rf.description = 'PowerTax - Normalization Schema'
           )
WHERE   pp_reports_filter.description='PowerTax - Norm Schema Trueup Beg'
UNION
SELECT  (SELECT pp_report_filter_id FROM pp_reports_filter WHERE pp_reports_filter.description='PowerTax - Norm Schema Trueup Beg')
      , (SELECT Max(filter_id)
          FROM pp_dynamic_filter
          WHERE sqls_column_expression='deferred_income_tax.tax_year'
          AND single_select_only=1) --dit.tax_year required filter
FROM    dual
UNION
SELECT  pp_report_filter_id, 101 FROM pp_reports_filter WHERE pp_reports_filter.description='PowerTax - Norm Schema Trueup Beg' --company
UNION
SELECT  pp_report_filter_id, 102 FROM pp_reports_filter WHERE pp_reports_filter.description='PowerTax - Norm Schema Trueup Beg' --tax_class
UNION
SELECT  pp_report_filter_id, 103 FROM pp_reports_filter WHERE pp_reports_filter.description='PowerTax - Norm Schema Trueup Beg' --vintage
;

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15702, 0, 2018, 2, 0, 0, 53052, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053052_pwrtax_01_norm_schema_trueup_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;