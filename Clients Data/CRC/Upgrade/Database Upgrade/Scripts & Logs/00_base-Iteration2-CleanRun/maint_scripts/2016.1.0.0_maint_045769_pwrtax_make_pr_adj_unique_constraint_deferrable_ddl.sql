/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045769_pwrtax_make_pr_adj_unique_constraint_deferrable_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 06/21/2016 Charlie Shilling make unique constraint on tax_plant_recon_form_adj deferrable
||============================================================================
*/
DROP INDEX tax_plant_recon_form_adj_ui;

ALTER TABLE tax_plant_recon_form_adj
ADD CONSTRAINT tax_plant_recon_form_adj_ui
	UNIQUE (tax_pr_form_id, jurisdiction_id, tax_pr_adj_id)
	INITIALLY IMMEDIATE DEFERRABLE;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3227, 0, 2016, 1, 0, 0, 045769, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045769_pwrtax_make_pr_adj_unique_constraint_deferrable_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;