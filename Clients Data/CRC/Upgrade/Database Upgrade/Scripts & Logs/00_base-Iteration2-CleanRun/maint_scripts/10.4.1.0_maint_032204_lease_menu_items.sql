/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032204_lease_menu_items.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/05/2013 Ryan Oliveria  Point Release
||============================================================================
*/

delete from PPBASE_MENU_ITEMS
 where MODULE = 'LESSEE'
   and MENU_IDENTIFIER in ('admin_vendors', 'approval_trans_ret', 'control_invoices');

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER - 1
 where MODULE = 'LESSEE'
   and PARENT_MENU_IDENTIFIER = 'menu_wksp_admin'
   and ITEM_ORDER > 4;

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER - 1
 where MODULE = 'LESSEE'
   and PARENT_MENU_IDENTIFIER = 'menu_wksp_approval'
   and ITEM_ORDER > 3;

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER - 1
 where MODULE = 'LESSEE'
   and PARENT_MENU_IDENTIFIER = 'menu_wksp_control'
   and ITEM_ORDER > 2;

update PPBASE_MENU_ITEMS
   set LABEL = 'Payments and Invoices'
 where MODULE = 'LESSEE'
   and MENU_IDENTIFIER = 'control_payments';

update PPBASE_WORKSPACE
   set LABEL = 'Payments and Invoices'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'control_payments';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (582, 0, 10, 4, 1, 0, 32204, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032204_lease_menu_items.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
