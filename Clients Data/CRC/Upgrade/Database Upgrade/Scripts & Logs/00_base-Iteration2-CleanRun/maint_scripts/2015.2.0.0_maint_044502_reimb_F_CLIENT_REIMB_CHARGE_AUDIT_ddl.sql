/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044502_reimb_F_CLIENT_REIMB_CHARGE_AUDIT_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2   07/29/2015    Andrew Hill     Adding pre_pull argument to function
||============================================================================
*/
create or replace FUNCTION f_client_reimb_charge_audit(a_billing_group_id NUMBER,
                                            a_status           NUMBER,
                                            a_pre_pull BOOLEAN) return NUMBER is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_CHARGE_PULL_AUDIT
   RETURN 1;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2720, 0, 2015, 2, 0, 0, 044502, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044502_reimb_F_CLIENT_REIMB_CHARGE_AUDIT_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;