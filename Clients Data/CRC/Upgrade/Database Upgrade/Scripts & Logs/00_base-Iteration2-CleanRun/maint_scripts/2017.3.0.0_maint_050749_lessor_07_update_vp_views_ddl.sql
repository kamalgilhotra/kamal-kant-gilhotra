/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050749_lessor_07_update_vp_views_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/04/2018 Andrew Hill       Update psuedo asset schedule views
||============================================================================
*/

CREATE OR REPLACE VIEW V_LSR_PSEUDO_ASSET_SCH_SALES AS
SELECT lsr_asset_id,
      ilr_id,
     revision,
     set_of_books_id,
     month,
     principal_received * allocation AS principal_received,
     principal_accrued * allocation AS principal_accrued,
     beg_unguaranteed_residual * allocation AS beg_unguaranteed_residual,
     interest_unguaranteed_residual * allocation AS interest_unguaranteed_residual,
     ending_unguaranteed_residual * allocation AS ending_unguaranteed_residual,
     beg_net_investment * allocation AS beg_net_investment,
     interest_net_investment * allocation AS interest_net_investment,
     ending_net_investment * allocation AS ending_net_investment
FROM ( SELECT asset.lsr_asset_id,
        sch.ilr_id,
       sch.revision,
       sch.set_of_books_id,
       sch.month,
       sch.principal_received,
       sch.principal_accrued,
       sch.beg_unguaranteed_residual,
       sch.interest_unguaranteed_residual,
       sch.ending_unguaranteed_residual,
       sch.beg_net_investment,
       sch.interest_net_investment,
       sch.ending_net_investment,
       coalesce(
    RATIO_TO_REPORT(
      asset.fair_market_value
    ) OVER(PARTITION BY
      sch.ilr_id,
      sch.revision,
      sch.month,
      sch.set_of_books_id
    ),
    0
  ) AS allocation
FROM lsr_ilr_schedule_sales_direct sch
  JOIN lsr_asset asset ON sch.ilr_id = asset.ilr_id
  AND sch.revision = asset.revision
);
/

CREATE OR REPLACE VIEW V_LSR_PSEUDO_ASSET_SCHEDULE
AS
SELECT  lsr_asset_id,
      ilr_id,
     revision,
     set_of_books_id,
     month,
     interest_income_received * allocation AS interest_income_received,
     interest_income_accrued * allocation AS interest_income_accrued,
     interest_rental_recvd_spread * allocation AS interest_rental_recvd_spread,
     beg_deferred_rev * allocation AS beg_deferred_rev,
     deferred_rev_activity * allocation AS deferred_rev_activity,
     end_deferred_rev * allocation AS end_deferred_rev,
     beg_receivable * allocation AS beg_receivable,
     end_receivable * allocation AS end_receivable,
     beg_lt_receivable * allocation AS beg_lt_receivable,
     end_lt_receivable * allocation AS end_lt_receivable,
     initial_direct_cost * allocation as initial_direct_cost,
     executory_accrual1 * allocation AS executory_accrual1,
     executory_accrual2 * allocation AS executory_accrual2,
     executory_accrual3 * allocation AS executory_accrual3,
     executory_accrual4 * allocation AS executory_accrual4,
     executory_accrual5 * allocation AS executory_accrual5,
     executory_accrual6 * allocation AS executory_accrual6,
     executory_accrual7 * allocation AS executory_accrual7,
     executory_accrual8 * allocation AS executory_accrual8,
     executory_accrual9 * allocation AS executory_accrual9,
     executory_accrual10 * allocation AS executory_accrual10,
     executory_paid1 * allocation AS executory_paid1,
     executory_paid2 * allocation AS executory_paid2,
     executory_paid3 * allocation AS executory_paid3,
     executory_paid4 * allocation AS executory_paid4,
     executory_paid5 * allocation AS executory_paid5,
     executory_paid6 * allocation AS executory_paid6,
     executory_paid7 * allocation AS executory_paid7,
     executory_paid8 * allocation AS executory_paid8,
     executory_paid9 * allocation AS executory_paid9,
     executory_paid10 * allocation AS executory_paid10,
     contingent_accrual1 * allocation AS contingent_accrual1,
     contingent_accrual2 * allocation AS contingent_accrual2,
     contingent_accrual3 * allocation AS contingent_accrual3,
     contingent_accrual4 * allocation AS contingent_accrual4,
     contingent_accrual5 * allocation AS contingent_accrual5,
     contingent_accrual6 * allocation AS contingent_accrual6,
     contingent_accrual7 * allocation AS contingent_accrual7,
     contingent_accrual8 * allocation AS contingent_accrual8,
     contingent_accrual9 * allocation AS contingent_accrual9,
     contingent_accrual10 * allocation AS contingent_accrual10,
     contingent_paid1 * allocation AS contingent_paid1,
     contingent_paid2 * allocation AS contingent_paid2,
     contingent_paid3 * allocation AS contingent_paid3,
     contingent_paid4 * allocation AS contingent_paid4,
     contingent_paid5 * allocation AS contingent_paid5,
     contingent_paid6 * allocation AS contingent_paid6,
     contingent_paid7 * allocation AS contingent_paid7,
     contingent_paid8 * allocation AS contingent_paid8,
     contingent_paid9 * allocation AS contingent_paid9,
     contingent_paid10 * allocation AS contingent_paid10
FROM ( SELECT asset.lsr_asset_id,
              sch.ilr_id,
              sch.revision,
       sch.set_of_books_id,
       sch.month,
       sch.interest_income_received,
       sch.interest_income_accrued,
       sch.interest_rental_recvd_spread,
       sch.beg_deferred_rev,
       sch.deferred_rev_activity,
       sch.end_deferred_rev,
       sch.beg_receivable,
       sch.end_receivable,
       sch.beg_lt_receivable,
       sch.end_lt_receivable,
       sch.initial_direct_cost,
       sch.executory_accrual1,
       sch.executory_accrual2,
       sch.executory_accrual3,
       sch.executory_accrual4,
       sch.executory_accrual5,
       sch.executory_accrual6,
       sch.executory_accrual7,
       sch.executory_accrual8,
       sch.executory_accrual9,
       sch.executory_accrual10,
       sch.executory_paid1,
       sch.executory_paid2,
       sch.executory_paid3,
       sch.executory_paid4,
       sch.executory_paid5,
       sch.executory_paid6,
       sch.executory_paid7,
       sch.executory_paid8,
       sch.executory_paid9,
       sch.executory_paid10,
       sch.contingent_accrual1,
       sch.contingent_accrual2,
       sch.contingent_accrual3,
       sch.contingent_accrual4,
       sch.contingent_accrual5,
       sch.contingent_accrual6,
       sch.contingent_accrual7,
       sch.contingent_accrual8,
       sch.contingent_accrual9,
       sch.contingent_accrual10,
       sch.contingent_paid1,
       sch.contingent_paid2,
       sch.contingent_paid3,
       sch.contingent_paid4,
       sch.contingent_paid5,
       sch.contingent_paid6,
       sch.contingent_paid7,
       sch.contingent_paid8,
       sch.contingent_paid9,
       sch.contingent_paid10,
       coalesce(
    RATIO_TO_REPORT(
      asset.fair_market_value
    ) OVER(PARTITION BY
      sch.ilr_id,
      sch.revision,
      sch.month,
      sch.set_of_books_id
    ),
    0
  ) AS allocation
FROM lsr_ilr_schedule sch
JOIN lsr_asset asset ON sch.ilr_id = asset.ilr_id
AND sch.revision = asset.revision);
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4270, 0, 2017, 3, 0, 0, 50749, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050749_lessor_07_update_vp_views_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;