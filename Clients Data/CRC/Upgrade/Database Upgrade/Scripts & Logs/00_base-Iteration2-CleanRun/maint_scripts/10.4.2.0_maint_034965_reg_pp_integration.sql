/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034965_reg_pp_integration.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/05/2014 Shane "C" Ward
||============================================================================
*/

--Update reg interface table to new names
update REG_INTERFACE
   set DESCRIPTION = 'Project CWIP', LONG_DESCRIPTION = 'Project CWIP',
       BUTTON_TEXT = 'Run Proj. CWIP'
 where REG_INTERFACE_ID = 2;

update REG_INTERFACE
   set DESCRIPTION = 'Tax Provision', LONG_DESCRIPTION = 'Tax Provision',
       BUTTON_TEXT = 'Run Tax Prov.'
 where REG_INTERFACE_ID = 6;

update REG_INTERFACE
   set DESCRIPTION = 'PowerTax', LONG_DESCRIPTION = 'PowerTax', BUTTON_TEXT = 'Run PowerTax'
 where REG_INTERFACE_ID = 4;

update REG_INTERFACE
   set DESCRIPTION = 'CR Integration', LONG_DESCRIPTION = 'CR Integration',
       BUTTON_TEXT = 'Run CR Int.'
 where REG_INTERFACE_ID = 3;

update REG_INTERFACE
   set DESCRIPTION = 'Assets/Depr Integration', LONG_DESCRIPTION = 'Assets/Depr Integration',
       BUTTON_TEXT = 'Run Assets'
 where REG_INTERFACE_ID = 1;

delete from REG_INTERFACE_CONTROL where REG_INTERFACE_ID in (5, 16);
delete from REG_INTERFACE where REG_INTERFACE_ID in (5, 16);

--Fixed lookup sql for import tool
update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select j.reg_jurisdiction_id from reg_jurisdiction j where upper(trim(<importfield>)) = upper(trim(j.description)))'
 where IMPORT_LOOKUP_ID = 1506;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select j.reg_case_id from reg_case j where upper(trim(<importfield>)) = upper(trim(j.long_description)))'
 where IMPORT_LOOKUP_ID = 1516;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select c.reg_alloc_category_id from reg_alloc_category c where upper(trim(<importfield>)) = upper(trim(c.description)) )'
 where IMPORT_LOOKUP_ID = 1505;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select j.reg_jur_template_id from reg_jur_template j where upper(trim(<importfield>)) = upper(trim(j.description)) )'
 where IMPORT_LOOKUP_ID = 1515;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select j.reg_case_id from reg_case j where upper(trim(<importfield>)) = upper(trim(j.case_name)) )'
 where IMPORT_LOOKUP_ID = 1517;

 --Add new PP Integration Function for External Source Integration
insert into PP_INTEGRATION_COMPONENTS
   (COMPONENT, ARGUMENT1_TITLE, ARGUMENT2_TITLE, ARGUMENT3_TITLE, ARGUMENT4_TITLE, ARGUMENT5_TITLE,
    ARGUMENT6_TITLE)
values
   ('Reg External Source', 'Company ID', 'Historic Version', 'Forecast Version', 'Source ID',
    'Start Month', 'End Month');

create index REG_ALLOC_RESULT_CASE_YEAR_IX
   on REG_ALLOC_RESULT (REG_CASE_ID, CASE_YEAR)
      tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1012, 0, 10, 4, 2, 0, 34965, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034965_reg_pp_integration.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;