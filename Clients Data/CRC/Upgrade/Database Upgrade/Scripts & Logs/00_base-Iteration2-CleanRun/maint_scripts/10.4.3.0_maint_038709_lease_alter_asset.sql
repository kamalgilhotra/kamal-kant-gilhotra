/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038709_lease_alter_asset.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 10.4.3.0 07/02/2014 Daniel Motter        Add column for tax summary to ls_asset
||========================================================================================
*/

alter table LS_ASSET add TAX_SUMMARY_ID number(22,0) null;
comment on column LS_ASSET.TAX_SUMMARY_ID is 'Contains which tax summary is associated with this asset in case it is different from the one associated with this asset''s MLA.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1245, 0, 10, 4, 3, 0, 38709, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038709_lease_alter_asset.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
