SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011230_proptax_lock_stats.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   01/07/2013 Lee Quinn      Point Release
||============================================================================
*/

declare
   STATISTICS_ALREADY_LOCKED exception;
   pragma exception_init(STATISTICS_ALREADY_LOCKED, -20005);

   LS_SQL          varchar2(256);
   LS_STATS_LOCKED boolean;

begin
   DBMS_OUTPUT.ENABLE(buffer_size => NULL);

   for CSR_SQL in (select 'DBMS_STATS.SET_TABLE_STATS (''PWRPLANT'', ''' || UT.TABLE_NAME ||
                          ''', '''', NULL, NULL, 1, 1, 36, NULL);' SQL_LINE1,
                          'DBMS_STATS.LOCK_TABLE_STATS(''PWRPLANT'', ''' || UT.TABLE_NAME || ''');' SQL_LINE2
                     from USER_TABLES UT, USER_TAB_STATISTICS UTS
                    where UT.TEMPORARY = 'Y'
                      and UT.TABLE_NAME = UTS.TABLE_NAME
                      and UT.TABLE_NAME like 'PT%TEMP%')
   loop
      LS_STATS_LOCKED := false;
      LS_SQL          := 'begin ' || CSR_SQL.SQL_LINE1 || ' end;';
      DBMS_OUTPUT.PUT_LINE(LS_SQL);

      begin
         execute immediate LS_SQL;
      exception
         when STATISTICS_ALREADY_LOCKED then
            DBMS_OUTPUT.PUT_LINE('Statistics are locked and therefore cannot be altered.');
            LS_STATS_LOCKED := true;
      end;

      if not LS_STATS_LOCKED then
         LS_SQL := 'begin ' || CSR_SQL.SQL_LINE2 || ' end;';
         DBMS_OUTPUT.PUT_LINE(LS_SQL);
         execute immediate LS_SQL;
      end if;
   end loop;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (270, 0, 10, 4, 1, 0, 11230, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011230_proptax_lock_stats.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
