/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041319_pcm_fp_search_ddl.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	11/25/2014 Ryan Oliveria		Altering dynamic filters for FP Search
|| 2015.1	02/02/2015 Alex Pivoshenko		Remove foreign keys, since tables are now global temp
||========================================================================================
*/

create table PP_DYNAMIC_FILTER_TYPE (
	FILTER_TYPE_ID number(22,0),
	USER_ID varchar2(18),
	TIME_STAMP date,
	DESCRIPTION varchar2(35)
);

alter table PP_DYNAMIC_FILTER_TYPE
	add constraint PK_DYNAMIC_FILTER_TYPE
		primary key (FILTER_TYPE_ID)
		using index tablespace PWRPLANT_IDX;



alter table PP_DYNAMIC_FILTER_SAVED_VALUES add FILTER_TYPE_ID number(22,0);

alter table PP_DYNAMIC_FILTER_SAVED_VALUES
	add constraint FK_DFS_FILTER_TYPE_ID
		foreign key (FILTER_TYPE_ID)
		references PP_DYNAMIC_FILTER_TYPE (FILTER_TYPE_ID);





alter table PP_DYN_FILTER_SAVED_VALUES_DW add FILTER_TYPE_ID number(22,0);

alter table PP_DYN_FILTER_SAVED_VALUES_DW
	add constraint FK_PDFSVD_FILTER_TYPE_ID
		foreign key (FILTER_TYPE_ID)
		references PP_DYNAMIC_FILTER_TYPE (FILTER_TYPE_ID);






alter table PP_DYNAMIC_FILTER_VALUES add FILTER_TYPE_ID number(22,0);
alter table PP_DYNAMIC_FILTER_VALUES_DW add FILTER_TYPE_ID number(22,0);


-- COMMENTS
comment on table PP_DYNAMIC_FILTER_TYPE is 'Holds the different types of dynamic filters such as standard dynamic filters, class codes, etc.  This is used primarily for the save/restore functionality on filters.';

comment on column PP_DYNAMIC_FILTER_TYPE.FILTER_TYPE_ID is 'The ID of this filter type.';
comment on column PP_DYNAMIC_FILTER_TYPE.USER_ID is 'Standard system-assigned user id for audit purposes.';
comment on column PP_DYNAMIC_FILTER_TYPE.TIME_STAMP is 'Standard system-assigned time stamp for audit purposes.';
comment on column PP_DYNAMIC_FILTER_TYPE.DESCRIPTION is 'A short description of the filter type.';

comment on column PP_DYNAMIC_FILTER_SAVED_VALUES.FILTER_TYPE_ID is 'References the type of filter for this saved value. Ex: Dynamic Filters, Class Codes, etc.';

comment on column PP_DYN_FILTER_SAVED_VALUES_DW.FILTER_TYPE_ID is 'References the type of filter for this saved value. Ex: Dynamic Filters, Class Codes, etc.';

comment on column PP_DYNAMIC_FILTER_VALUES.FILTER_TYPE_ID is 'References the type of filter for this value. Ex: Dynamic Filters, Class Codes, etc.';

comment on column PP_DYNAMIC_FILTER_VALUES_DW.FILTER_TYPE_ID is 'References the type of filter for this value. Ex: Dynamic Filters, Class Codes, etc.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2068, 0, 2015, 1, 0, 0, 041319, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041319_pcm_fp_search_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;