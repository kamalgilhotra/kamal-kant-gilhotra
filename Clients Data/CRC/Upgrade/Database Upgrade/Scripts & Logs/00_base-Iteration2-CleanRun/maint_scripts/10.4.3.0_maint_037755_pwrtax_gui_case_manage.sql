SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037755_pwrtax_gui_case_manage.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 05/28/2014  Nathan Hollis      Base GUI workspaces for Case Management
||============================================================================
*/

----
----  Delete the prior attempt
----
delete from PPBASE_WORKSPACE
 where MODULE = 'powertax'
   and WORKSPACE_IDENTIFIER = 'admin_case';

----
----  set up the new workspace to be accessible in powertax base gui
----

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'case_manage_cases', 'Case Management', 'uo_tax_case_wksp_manage', 'Case Management');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'case_manage_cases'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'case_manage_cases';

----
----  make the tax year lock column a default of 0 and not-nullable
----

update TAX_YEAR_VERSION set TAX_YEAR_LOCK = 0 where TAX_YEAR_LOCK is null;

alter table TAX_YEAR_VERSION MODIFY TAX_YEAR_LOCK not null;

alter table TAX_YEAR_VERSION MODIFY TAX_YEAR_LOCK default 0;

----
----  make the "official" column on version default of 0 and not-nullable
----

update VERSION set OFFICIAL = 0 where OFFICIAL is null;

alter table VERSION MODIFY OFFICIAL not null;

alter table VERSION MODIFY OFFICIAL default 0;

----
----  set up the dynamic filters for the copy and vintage copy logic
----
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (65, 'Powertax - Case - Copy', 'uo_ppbase_tab_filter_dynamic');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (66, 'Powertax - Case - Vintage', 'uo_ppbase_tab_filter_dynamic');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (157, 'Company', 'dw', '', 'dw_tax_company_by_version_filter', 'company_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (158, 'Tax Book', 'dw', '', 'dw_tax_book_filter', 'tax_book_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (159, 'Tax Class', 'dw', '', 'dw_tax_class_by_version_filter', 'tax_class_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (160, 'Vintage', 'dw', '', 'dw_tax_vintage_by_version_filter', 'vintage_id', 'description', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (161, 'Tax Year', 'dw', '', 'dw_tax_year_by_version_filter', 'tax_year_version_id', 'tax_year', 'N', 0, 0, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (162, 'Vintage', 'dw', '', 'dw_tax_vintage_by_version_filter', 'vintage_id', 'description', 'N', 1, 1, 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (163, 'Tax Year', 'dw', '', 'dw_tax_year_by_version_filter', 'tax_year_version_id', 'tax_year', 'N', 1, 1, 1, 0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   select 65, FILTER_ID from PP_DYNAMIC_FILTER where FILTER_ID in (157, 158, 159, 160, 161);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   select 66, FILTER_ID from PP_DYNAMIC_FILTER where FILTER_ID in (157, 158, 159, 162, 163);

---- create the renumbering tax record control table used by the logic.  older code dropped and
---- recreated it every time during the run.  Instead of doing this, let's
---- create it in this script and the logic will now truncate and repopulate.
declare
   SQLS varchar2(1000);
begin

   DBMS_OUTPUT.PUT_LINE('Creating temp renumber trids table.');
   SQLS := 'create table temp_renumber_trids ( rnum number(22,0), tax_record_id number(22,0) )';
   execute immediate SQLS;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('temp_renumber_trids table already exists.  Continuing...');
end;
/


declare
   SQLS varchar2(1000);
begin

   DBMS_OUTPUT.PUT_LINE('Creating temp renumber trids pkey.');
   SQLS := 'alter table temp_renumber_trids add constraint temp_renumber_tridspk primary key (tax_record_id)';
   execute immediate SQLS;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('temp_renumber_tridspk pkey already exists.  Continuing...');
end;
/


---- create the renumbering tax transfer table used by the logic.  older code dropped and
---- recreated it every time during the run.  Instead of doing this, let's
---- create it in this script and the logic will now truncate and repopulate.
declare
   SQLS varchar2(1000);
begin

   DBMS_OUTPUT.PUT_LINE('Creating temp renumber trid xfer table.');
   SQLS := 'create table temp_renumber_transfers ( rnum number(22,0), tax_transfer_id number(22,0) )';
   execute immediate SQLS;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('temp_renumber_transfers table already exists.  Continuing...');
end;
/


declare
   SQLS varchar2(1000);
begin

   DBMS_OUTPUT.PUT_LINE('Creating temp renumber trids pkey.');
   SQLS := 'alter table temp_renumber_transfers add constraint temp_renumber_transferspk primary key (tax_transfer_id)';
   execute immediate SQLS;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('temp_renumber_transferspk pkey already exists.  Continuing...');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1162, 0, 10, 4, 3, 0, 37755, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037755_pwrtax_gui_case_manage.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;