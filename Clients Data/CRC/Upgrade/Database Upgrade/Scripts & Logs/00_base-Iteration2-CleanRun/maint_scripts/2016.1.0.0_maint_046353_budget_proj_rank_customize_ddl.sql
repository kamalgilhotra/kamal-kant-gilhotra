 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046353_budget_proj_rank_customize_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- -------------------------------------
 || 2016.1.0.0  09/06/2016 Daniel Mendel  Increase column size for proj rank customize
 ||============================================================================
 */ 

alter table pp_user_profile_detail modify name varchar2(254);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3279, 0, 2016, 1, 0, 0, 046353, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046353_budget_proj_rank_customize_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;