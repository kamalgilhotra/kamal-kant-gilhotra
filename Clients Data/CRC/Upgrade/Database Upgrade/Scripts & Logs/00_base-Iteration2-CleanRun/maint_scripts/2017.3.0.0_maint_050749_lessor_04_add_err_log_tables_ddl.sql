/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050749_lessor_04_add_err_log_tables_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/04/2018 Andrew Hill       Add error logging tables
||============================================================================
*/

CREATE TABLE PWRPLANT.ERR$_LSR_ILR_SCHEDULE ( ORA_ERR_NUMBER$ NUMBER, 
                                              ORA_ERR_MESG$ VARCHAR2(2000 BYTE), 
                                              ORA_ERR_ROWID$ UROWID (4000), 
                                              ORA_ERR_OPTYP$ VARCHAR2(2 BYTE), 
                                              ORA_ERR_TAG$ VARCHAR2(2000 BYTE), 
                                              ILR_ID VARCHAR2(4000 BYTE), 
                                              REVISION VARCHAR2(4000 BYTE), 
                                              SET_OF_BOOKS_ID VARCHAR2(4000 BYTE), 
                                              MONTH VARCHAR2(4000 BYTE), 
                                              USER_ID VARCHAR2(4000 BYTE), 
                                              TIME_STAMP VARCHAR2(4000 BYTE), 
                                              INTEREST_INCOME_RECEIVED VARCHAR2(4000 BYTE), 
                                              INTEREST_INCOME_ACCRUED VARCHAR2(4000 BYTE), 
                                              INTEREST_RENTAL_RECVD_SPREAD VARCHAR2(4000 BYTE), 
                                              BEG_DEFERRED_REV VARCHAR2(4000 BYTE), 
                                              DEFERRED_REV_ACTIVITY VARCHAR2(4000 BYTE), 
                                              END_DEFERRED_REV VARCHAR2(4000 BYTE), 
                                              BEG_RECEIVABLE VARCHAR2(4000 BYTE), 
                                              END_RECEIVABLE VARCHAR2(4000 BYTE), 
                                              BEG_LT_RECEIVABLE VARCHAR2(4000 BYTE), 
                                              END_LT_RECEIVABLE VARCHAR2(4000 BYTE), 
                                              INITIAL_DIRECT_COST VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL1 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL2 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL3 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL4 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL5 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL6 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL7 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL8 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL9 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_ACCRUAL10 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID1 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID2 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID3 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID4 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID5 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID6 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID7 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID8 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID9 VARCHAR2(4000 BYTE), 
                                              EXECUTORY_PAID10 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL1 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL2 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL3 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL4 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL5 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL6 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL7 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL8 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL9 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_ACCRUAL10 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID1 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID2 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID3 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID4 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID5 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID6 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID7 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID8 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID9 VARCHAR2(4000 BYTE), 
                                              CONTINGENT_PAID10 VARCHAR2(4000 BYTE));
COMMENT ON TABLE PWRPLANT.ERR$_LSR_ILR_SCHEDULE  IS 'DML Error Logging table for LSR_ILR_SCHEDULE';
   

CREATE TABLE ERR$_LSR_ILR_SCH_SALES_DIRECT (ORA_ERR_NUMBER$ NUMBER, 
                                            ORA_ERR_MESG$ VARCHAR2(2000 BYTE), 
                                            ORA_ERR_ROWID$ UROWID (4000), 
                                            ORA_ERR_OPTYP$ VARCHAR2(2 BYTE), 
                                            ORA_ERR_TAG$ VARCHAR2(2000 BYTE), 
                                            ILR_ID VARCHAR2(4000 BYTE), 
                                            REVISION VARCHAR2(4000 BYTE), 
                                            SET_OF_BOOKS_ID VARCHAR2(4000 BYTE), 
                                            MONTH VARCHAR2(4000 BYTE), 
                                            USER_ID VARCHAR2(4000 BYTE), 
                                            TIME_STAMP VARCHAR2(4000 BYTE), 
                                            PRINCIPAL_RECEIVED VARCHAR2(4000 BYTE), 
                                            PRINCIPAL_ACCRUED VARCHAR2(4000 BYTE), 
                                            BEG_UNGUARANTEED_RESIDUAL VARCHAR2(4000 BYTE), 
                                            INTEREST_UNGUARANTEED_RESIDUAL VARCHAR2(4000 BYTE), 
                                            ENDING_UNGUARANTEED_RESIDUAL VARCHAR2(4000 BYTE), 
                                            BEG_NET_INVESTMENT VARCHAR2(4000 BYTE), 
                                            INTEREST_NET_INVESTMENT VARCHAR2(4000 BYTE), 
                                            ENDING_NET_INVESTMENT VARCHAR2(4000 BYTE));
COMMENT ON TABLE PWRPLANT.ERR$_LSR_ILR_SCH_SALES_DIRECT  IS 'DML Error Logging table for LSR_ILR_SCHEDULE_SALES_DIRECT';
   

  CREATE TABLE PWRPLANT.ERR$_LSR_ILR_SCH_DIRECT_FIN 
   (	ORA_ERR_NUMBER$ NUMBER, 
	ORA_ERR_MESG$ VARCHAR2(2000 BYTE), 
	ORA_ERR_ROWID$ UROWID (4000), 
	ORA_ERR_OPTYP$ VARCHAR2(2 BYTE), 
	ORA_ERR_TAG$ VARCHAR2(2000 BYTE), 
	ILR_ID VARCHAR2(4000 BYTE), 
	REVISION VARCHAR2(4000 BYTE), 
	SET_OF_BOOKS_ID VARCHAR2(4000 BYTE), 
	MONTH VARCHAR2(4000 BYTE), 
	USER_ID VARCHAR2(4000 BYTE), 
	TIME_STAMP VARCHAR2(4000 BYTE), 
	BEGIN_DEFERRED_PROFIT VARCHAR2(4000 BYTE), 
	RECOGNIZED_PROFIT VARCHAR2(4000 BYTE), 
	END_DEFERRED_PROFIT VARCHAR2(4000 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE PWRPLANT ;

   COMMENT ON TABLE PWRPLANT.ERR$_LSR_ILR_SCH_DIRECT_FIN  IS 'DML Error Logging table for LSR_ILR_SCHEDULE_DIRECT_FIN'; 
   
CREATE TABLE PWRPLANT.ERR$_LSR_ILR_AMOUNTS (ORA_ERR_NUMBER$ NUMBER, 
                                            ORA_ERR_MESG$ VARCHAR2(2000 BYTE), 
                                            ORA_ERR_ROWID$ UROWID (4000), 
                                            ORA_ERR_OPTYP$ VARCHAR2(2 BYTE), 
                                            ORA_ERR_TAG$ VARCHAR2(2000 BYTE), 
                                            ILR_ID VARCHAR2(4000 BYTE), 
                                            REVISION VARCHAR2(4000 BYTE), 
                                            SET_OF_BOOKS_ID VARCHAR2(4000 BYTE), 
                                            TIME_STAMP VARCHAR2(4000 BYTE), 
                                            USER_ID VARCHAR2(4000 BYTE), 
                                            NPV_LEASE_PAYMENTS VARCHAR2(4000 BYTE), 
                                            NPV_GUARANTEED_RESIDUAL VARCHAR2(4000 BYTE), 
                                            NPV_UNGUARANTEED_RESIDUAL VARCHAR2(4000 BYTE), 
                                            SELLING_PROFIT_LOSS VARCHAR2(4000 BYTE), 
                                            BEGINNING_LEASE_RECEIVABLE VARCHAR2(4000 BYTE), 
                                            BEGINNING_NET_INVESTMENT VARCHAR2(4000 BYTE), 
                                            COST_OF_GOODS_SOLD VARCHAR2(4000 BYTE));
COMMENT ON TABLE PWRPLANT.ERR$_LSR_ILR_AMOUNTS  IS 'DML Error Logging table for LSR_ILR_AMOUNTS';

CREATE TABLE PWRPLANT.ERR$_LSR_ILR_RATES (ORA_ERR_NUMBER$ NUMBER, 
                                          ORA_ERR_MESG$ VARCHAR2(2000 BYTE), 
                                          ORA_ERR_ROWID$ UROWID (4000), 
                                          ORA_ERR_OPTYP$ VARCHAR2(2 BYTE), 
                                          ORA_ERR_TAG$ VARCHAR2(2000 BYTE), 
                                          ILR_ID VARCHAR2(4000 BYTE), 
                                          REVISION VARCHAR2(4000 BYTE), 
                                          RATE_TYPE_ID VARCHAR2(4000 BYTE), 
                                          RATE VARCHAR2(4000 BYTE), 
                                          TIME_STAMP VARCHAR2(4000 BYTE), 
                                          USER_ID VARCHAR2(4000 BYTE));
COMMENT ON TABLE PWRPLANT.ERR$_LSR_ILR_RATES  IS 'DML Error Logging table for LSR_ILR_RATES';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4264, 0, 2017, 3, 0, 0, 50749, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050749_lessor_04_add_err_log_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;