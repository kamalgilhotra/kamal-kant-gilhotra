 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041897_reg_cap_bdg_Drillback_fix_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/02/2015 Shane Ward     Fix Drillback Lookup Error
||============================================================================
*/

UPDATE cr_dd_sources_criteria_fields
SET element_id = NULL
WHERE id in (
   select reg_query_id
   from reg_query_drillback
   where description = 'Capital Budget Activity Drillback'
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2135, 0, 2015, 1, 0, 0, 41897, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041897_reg_cap_bdg_drillback_fix_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;