/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_043084_cwip_ocr_01_ddl.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   3/3/2015   Sunjin Cone      New tool for deleting WO OCR
||============================================================================
*/ 


create global temporary table WO_OCR_TOOL_STAGE
(WORK_ORDER_ID NUMBER(22,0) NOT NULL, 
CHARGE_ID NUMBER(22,0) NOT NULL, 
COST_ELEMENT_ID NUMBER(22,0) NOT NULL, 
CHARGE_TYPE_ID NUMBER(22,0) NOT NULL, 
MONTH_NUMBER NUMBER(22,0) NOT NULL, 
JOURNAL_CODE VARCHAR2(35), 
JOB_TASK_ID VARCHAR2(35), 
DEPARTMENT_ID NUMBER(22,0), 
VENDOR_INFORMATION VARCHAR2(254), 
CHARGE_GROUP_ID NUMBER(22,0) NOT NULL, 
PEND_TRANSACTION NUMBER(22,0), 
BATCH_UNIT_ITEM_ID NUMBER(22,0), 
UNIT_ITEM_ID NUMBER(22,0) NOT NULL, 
COMPANY_ID NUMBER(22,0), 
BUS_SEGMENT_ID NUMBER(22,0), 
UTILITY_ACCOUNT_ID NUMBER(22,0), 
SUB_ACCOUNT_ID NUMBER(22,0), 
RETIREMENT_UNIT_ID NUMBER(22,0), 
RETIRE_METHOD_ID NUMBER(22,0), 
AMOUNT NUMBER(22,2), 
QUANTITY NUMBER(22,2), 
ACTIVITY_CODE VARCHAR2(8), 
DESCRIPTION VARCHAR2(254), 
SHORT_DESCRIPTION VARCHAR2(35), 
PROPERTY_GROUP_ID NUMBER(22,0), 
ASSET_LOCATION_ID NUMBER(22,0), 
GL_ACCOUNT_ID NUMBER(22,0), 
SERIAL_NUMBER VARCHAR2(35), 
LDG_ASSET_ID NUMBER(22,0), 
RETIRE_VINTAGE NUMBER(22,0), 
UNIT_ESTIMATE_ID NUMBER(22,0),
REVISION NUMBER(22,0),
EST_CHG_TYPE_ID NUMBER(22,0)
) on commit preserve rows;

comment on table  WO_OCR_TOOL_STAGE is '(C) [04] A global temp table of the original cost retirement records staged to be deleted using the admin tool to delete in mass by looping through a list of work orders. No primary key.';
comment on column WO_OCR_TOOL_STAGE.WORK_ORDER_ID is 'Identifier of the work order number.';
comment on column WO_OCR_TOOL_STAGE.CHARGE_ID is 'Identifier of a particular ocr charge.';
comment on column WO_OCR_TOOL_STAGE.COST_ELEMENT_ID is 'Identifier of the ocr cost element.';
comment on column WO_OCR_TOOL_STAGE.CHARGE_TYPE_ID is 'Identifier of the ocr charge type.';
comment on column WO_OCR_TOOL_STAGE.MONTH_NUMBER is 'Identifier of the gl month in yyyymm format.';
comment on column WO_OCR_TOOL_STAGE.JOURNAL_CODE is 'The journal code on the ocr record.';
comment on column WO_OCR_TOOL_STAGE.JOB_TASK_ID is 'Identifier job task on the ocr record.';
comment on column WO_OCR_TOOL_STAGE.DEPARTMENT_ID is 'Identifier of the department.';
comment on column WO_OCR_TOOL_STAGE.VENDOR_INFORMATION is 'The vendor information on the ocr record.';
comment on column WO_OCR_TOOL_STAGE.CHARGE_GROUP_ID is 'Identifier of the ocr charge group.';
comment on column WO_OCR_TOOL_STAGE.PEND_TRANSACTION is '0/null = not yet retired in CPR; 100 = pending; 1 = posted to CPR. should never be 1 or 100 for this process.';
comment on column WO_OCR_TOOL_STAGE.BATCH_UNIT_ITEM_ID is 'null = no rwip unitized; negative = fake unitized rwip; positive = unitized rwip.';
comment on column WO_OCR_TOOL_STAGE.UNIT_ITEM_ID is 'Identifier of the ocr unit item.';
comment on column WO_OCR_TOOL_STAGE.COMPANY_ID is 'Identifier of a particular company.';
comment on column WO_OCR_TOOL_STAGE.BUS_SEGMENT_ID is 'Identifier of the business segment.';
comment on column WO_OCR_TOOL_STAGE.UTILITY_ACCOUNT_ID is 'Identifer of the utility plant account.';
comment on column WO_OCR_TOOL_STAGE.SUB_ACCOUNT_ID is 'Identifier of the sub account.';
comment on column WO_OCR_TOOL_STAGE.RETIREMENT_UNIT_ID is 'Identifier retirement unit.';
comment on column WO_OCR_TOOL_STAGE.RETIRE_METHOD_ID is 'Identifier retirement method.';
comment on column WO_OCR_TOOL_STAGE.PROPERTY_GROUP_ID is 'Identifier property group.';
comment on column WO_OCR_TOOL_STAGE.ASSET_LOCATION_ID is 'Identifier asset location.';   
comment on column WO_OCR_TOOL_STAGE.SERIAL_NUMBER is 'The serial number on the ocr record.';
comment on column WO_OCR_TOOL_STAGE.GL_ACCOUNT_ID is 'Identifier of the gl account.'; 
comment on column WO_OCR_TOOL_STAGE.ACTIVITY_CODE is 'An activity code from the Activity Code table.';
comment on column WO_OCR_TOOL_STAGE.DESCRIPTION is 'Description of the ocr record.';
comment on column WO_OCR_TOOL_STAGE.SHORT_DESCRIPTION is 'Short description of the ocr record.';
comment on column WO_OCR_TOOL_STAGE.AMOUNT is 'Amount of the ocr record.';
comment on column WO_OCR_TOOL_STAGE.QUANTITY is 'Quantity of the ocr record.';
comment on column WO_OCR_TOOL_STAGE.LDG_ASSET_ID is 'Identifier of the CPR asset.'; 
comment on column WO_OCR_TOOL_STAGE.RETIRE_VINTAGE is 'The yyyy format of the vintage used to find the ldg_asset_id, if any.'; 
comment on column WO_OCR_TOOL_STAGE.UNIT_ESTIMATE_ID is 'Identifier of the wo_estimate ocr record.'; 
comment on column WO_OCR_TOOL_STAGE.REVISION is 'The revision number of the wo_estimate ocr record';
comment on column WO_OCR_TOOL_STAGE.EST_CHG_TYPE_ID is 'Identifier of estimate charge type.';  


CREATE TABLE WO_OCR_TOOL_DELETED 
(WORK_ORDER_ID NUMBER(22,0) NOT NULL, 
CHARGE_ID NUMBER(22,0) NOT NULL, 
COST_ELEMENT_ID NUMBER(22,0) NOT NULL, 
CHARGE_TYPE_ID NUMBER(22,0) NOT NULL, 
MONTH_NUMBER NUMBER(22,0) NOT NULL, 
JOURNAL_CODE VARCHAR2(35), 
JOB_TASK_ID VARCHAR2(35), 
DEPARTMENT_ID NUMBER(22,0), 
VENDOR_INFORMATION VARCHAR2(254), 
CHARGE_GROUP_ID NUMBER(22,0) NOT NULL, 
PEND_TRANSACTION NUMBER(22,0), 
BATCH_UNIT_ITEM_ID NUMBER(22,0), 
UNIT_ITEM_ID NUMBER(22,0) NOT NULL, 
COMPANY_ID NUMBER(22,0), 
BUS_SEGMENT_ID NUMBER(22,0), 
UTILITY_ACCOUNT_ID NUMBER(22,0), 
SUB_ACCOUNT_ID NUMBER(22,0), 
RETIREMENT_UNIT_ID NUMBER(22,0), 
RETIRE_METHOD_ID NUMBER(22,0), 
AMOUNT NUMBER(22,2), 
QUANTITY NUMBER(22,2), 
ACTIVITY_CODE VARCHAR2(8), 
DESCRIPTION VARCHAR2(254), 
SHORT_DESCRIPTION VARCHAR2(35), 
PROPERTY_GROUP_ID NUMBER(22,0), 
ASSET_LOCATION_ID NUMBER(22,0), 
GL_ACCOUNT_ID NUMBER(22,0), 
SERIAL_NUMBER VARCHAR2(35), 
LDG_ASSET_ID NUMBER(22,0), 
RETIRE_VINTAGE NUMBER(22,0), 
UNIT_ESTIMATE_ID NUMBER(22,0),
REVISION NUMBER(22,0),
EST_CHG_TYPE_ID NUMBER(22,0),
TIME_STAMP DATE, 
USER_ID varchar2(18)
) ; 

comment on table  WO_OCR_TOOL_DELETED is '(C) [04] A table of the deleted original cost retirement records using the admin tool to delete in mass by looping through a list of work orders. No primary key.';
comment on column WO_OCR_TOOL_DELETED.WORK_ORDER_ID is 'Identifier of the work order number.';
comment on column WO_OCR_TOOL_DELETED.CHARGE_ID is 'Identifier of a particular ocr charge.';
comment on column WO_OCR_TOOL_DELETED.COST_ELEMENT_ID is 'Identifier of the ocr cost element.';
comment on column WO_OCR_TOOL_DELETED.CHARGE_TYPE_ID is 'Identifier of the ocr charge type.';
comment on column WO_OCR_TOOL_DELETED.MONTH_NUMBER is 'Identifier of the gl month in yyyymm format.';
comment on column WO_OCR_TOOL_DELETED.JOURNAL_CODE is 'The journal code on the ocr record.';
comment on column WO_OCR_TOOL_DELETED.JOB_TASK_ID is 'Identifier job task on the ocr record.';
comment on column WO_OCR_TOOL_DELETED.DEPARTMENT_ID is 'Identifier of the department.';
comment on column WO_OCR_TOOL_DELETED.VENDOR_INFORMATION is 'The vendor information on the ocr record.';
comment on column WO_OCR_TOOL_DELETED.CHARGE_GROUP_ID is 'Identifier of the ocr charge group.';
comment on column WO_OCR_TOOL_DELETED.PEND_TRANSACTION is '0/null = not yet retired in CPR; 100 = pending; 1 = posted to CPR. should never be 1 or 100 for this process.';
comment on column WO_OCR_TOOL_DELETED.BATCH_UNIT_ITEM_ID is 'null = no rwip unitized; negative = fake unitized rwip; positive = unitized rwip.';
comment on column WO_OCR_TOOL_DELETED.UNIT_ITEM_ID is 'Identifier of the ocr unit item.';
comment on column WO_OCR_TOOL_DELETED.COMPANY_ID is 'Identifier of a particular company.';
comment on column WO_OCR_TOOL_DELETED.BUS_SEGMENT_ID is 'Identifier of the business segment.';
comment on column WO_OCR_TOOL_DELETED.UTILITY_ACCOUNT_ID is 'Identifer of the utility plant account.';
comment on column WO_OCR_TOOL_DELETED.SUB_ACCOUNT_ID is 'Identifier of the sub account.';
comment on column WO_OCR_TOOL_DELETED.RETIREMENT_UNIT_ID is 'Identifier retirement unit.';
comment on column WO_OCR_TOOL_DELETED.RETIRE_METHOD_ID is 'Identifier retirement method.';
comment on column WO_OCR_TOOL_DELETED.PROPERTY_GROUP_ID is 'Identifier property group.';
comment on column WO_OCR_TOOL_DELETED.ASSET_LOCATION_ID is 'Identifier asset location.';   
comment on column WO_OCR_TOOL_DELETED.SERIAL_NUMBER is 'The serial number on the ocr record.';
comment on column WO_OCR_TOOL_DELETED.GL_ACCOUNT_ID is 'Identifier of the gl account.'; 
comment on column WO_OCR_TOOL_DELETED.ACTIVITY_CODE is 'An activity code from the Activity Code table.';
comment on column WO_OCR_TOOL_DELETED.DESCRIPTION is 'Description of the ocr record.';
comment on column WO_OCR_TOOL_DELETED.SHORT_DESCRIPTION is 'Short description of the ocr record.';
comment on column WO_OCR_TOOL_DELETED.AMOUNT is 'Amount of the ocr record.';
comment on column WO_OCR_TOOL_DELETED.QUANTITY is 'Quantity of the ocr record.';
comment on column WO_OCR_TOOL_DELETED.LDG_ASSET_ID is 'Identifier of the CPR asset.'; 
comment on column WO_OCR_TOOL_DELETED.RETIRE_VINTAGE is 'The yyyy format of the vintage used to find the ldg_asset_id, if any.'; 
comment on column WO_OCR_TOOL_DELETED.UNIT_ESTIMATE_ID is 'Identifier of the wo_estimate ocr record.'; 
comment on column WO_OCR_TOOL_DELETED.REVISION is 'The revision number of the wo_estimate ocr record';
comment on column WO_OCR_TOOL_DELETED.EST_CHG_TYPE_ID is 'Identifier of estimate charge type.'; 
comment on column WO_OCR_TOOL_DELETED.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column WO_OCR_TOOL_DELETED.USER_ID is 'Standard system-assigned user id used for audit purposes.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2338, 0, 2015, 1, 0, 0, 43084, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043084_cwip_ocr_01_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;