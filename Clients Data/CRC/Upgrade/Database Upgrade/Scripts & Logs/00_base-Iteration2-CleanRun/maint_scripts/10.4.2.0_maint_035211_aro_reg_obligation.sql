/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035211_aro_reg_obligation.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/08/2014 Brandon Beck   Point Release
||============================================================================
*/

alter table ARO add REGULATORY_OBLIGATION number(1,0) default 0;

alter table ARO
   add constraint ARO_REG_OBL_YES_NO_FK
       foreign key (REGULATORY_OBLIGATION)
       references YES_NO (YES_NO_ID);

comment on column aro.regulatory_obligation is 'A yes / no field identifying whetherr or not this ARO has a regulatory obligation and should be flowing through cost of removal on the depreciation ledger';alter table aro

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (840, 0, 10, 4, 2, 0, 35211, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035211_aro_reg_obligation.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
