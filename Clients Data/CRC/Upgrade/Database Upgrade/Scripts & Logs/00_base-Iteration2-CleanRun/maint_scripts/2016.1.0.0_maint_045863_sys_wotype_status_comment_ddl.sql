 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045863_sys_wotype_status_comment_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 07/08/2016 Daniel Mendel  WO Type Status ID comment was incorrect
 ||============================================================================
 */
 
 comment on column work_order_type.status_id is 'Null or ''1'' = Active; ''2'' = Inactive. Inactive work order types will not show in certain pulldowns (e.g. when initiating new work orders).';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3243, 0, 2016, 1, 0, 0, 045863, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045863_sys_wotype_status_comment_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;