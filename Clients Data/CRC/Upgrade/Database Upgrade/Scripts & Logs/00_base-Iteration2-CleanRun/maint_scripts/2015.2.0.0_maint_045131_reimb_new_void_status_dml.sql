/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045131_reimb_new_void_status_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/27/2015 Jared Watkins  Add new statuses for voided reimb bills
||============================================================================
*/

insert into reimb_bill_status
(bill_status_id, description)
Values
(11, 'Voided');

insert into reimb_bill_status
(bill_status_id, description)
VALUES
(12, 'Void Reversal');

insert into reimb_je_timing(reimb_je_timing_id, description)
values('13', 'Void');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2944, 0, 2015, 2, 0, 0, 45131, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045131_reimb_new_void_status_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;