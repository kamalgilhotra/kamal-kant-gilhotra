/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044024_lease_tax_import_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/11/2015 William Davis  Template creation for lease tax 
||                                      imports	
||============================================================================
*/

insert into pp_import_type(import_type_id, description, long_description, import_table_name, archive_table_name,
   allow_updates_on_add, delegate_object_name)
select
   262 AS import_type_id,
   'Add: Leased Asset Taxes' AS description,
   'Leased Asset Tax Mappings' AS long_description,
   'ls_import_asset_taxes' AS import_table_name,
   'ls_import_asset_taxes_archive' AS archive_table_name,
   1 AS allow_updates_on_add,
   'nvo_ls_logic_import' AS delegate_object_name
from dual;

insert into pp_import_type_subsystem(import_type_id, import_subsystem_id)
select 262, 8 from dual;

insert into pp_import_template(import_template_id, import_type_id, description, long_description, created_by,
   created_date, do_update_with_add, is_autocreate_template)
select
   pp_import_template_seq.nextval AS import_Template_id,
   262 AS import_type_id,
   'Leased Asset Taxes Add' AS description,
   'Leased Asset Taxes Add' AS long_description,
   'PWRPLANT' AS created_by,
   sysdate AS created_date,
   0 AS do_update_with_add,
   0 AS is_autocreate_template
from dual;

insert into pp_import_column(import_type_id, column_name, description, import_column_name, is_required, processing_order,
   column_type, parent_table, is_on_table, parent_table_pk_column)
select 262, 'ls_asset_id', 'LS Asset ID', 'ls_asset_xlate', 1, 2, 'number(22,0)', 'ls_asset', 1, 'ls_asset_id' from dual union
select 262, 'tax_local_id', 'Tax Local ID', 'tax_local_xlate', 1, 1, 'number(22,0)', 'ls_tax_local', 1, 'tax_local_id' from dual union
select 262, 'company_id', 'Company ID', 'company_xlate', 0, 1, 'number(22,0)', 'company_setup', 1, 'company_id' from dual union
select 262, 'status_code_id', 'Status Code ID', null, 1, 1, 'number(22,0)', null, 1, null from dual;

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
   is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql)
select
   (select nvl(max(import_lookup_id),0) from pp_import_lookup) + rownum AS import_lookup_id,
   'Lease '||description,
   long_description,
   column_name,
   replace(lookup_sql, ') ) )',') ) and is_lease_company = 1 )'),
   is_derived,
   lookup_table_name,
   lookup_column_name,
   'is_lease_company' AS lookup_constraining_columns,
   'select description from company where is_lease_company = 1' AS lookup_values_alternate_sql
/* CJS 2/16/15 We'll see if company view works for users; doesn't with pwrplant */
from pp_import_lookup
where column_name = 'company_id'
and not exists(
   select 1
   from pp_import_lookup
   where column_name = 'company_id'
   and description like 'Lease%');

declare
	import_temp_id number;
	asset_lookup   number;
	tax_lookup     number;
	company_lookup number;
begin
	select import_template_id into import_temp_id from pp_import_template where import_type_id = 262;
	select import_lookup_id into asset_lookup from pp_import_lookup where column_name = 'ls_asset_id' and lookup_column_name = 'leased_asset_number' and lookup_constraining_columns = 'company_id';
	select import_lookup_id into tax_lookup from pp_import_lookup where lookup_table_name = 'ls_tax_local';
	select import_lookup_id
	into company_lookup
	from pp_import_lookup where lookup_table_name = 'company' and lookup_column_name = 'gl_company_no' and lower(description) like '%lease%';
	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select import_temp_id, 1, 262, 'ls_asset_id', asset_lookup from dual union
	select import_temp_id, 2, 262, 'tax_local_id', tax_lookup from dual union
	select import_temp_id, 3, 262, 'status_code_id', null from dual union
	select import_temp_id, 4, 262, 'company_id', null from dual
	;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2594, 0, 2015, 2, 0, 0, 044024, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044024_lease_tax_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;