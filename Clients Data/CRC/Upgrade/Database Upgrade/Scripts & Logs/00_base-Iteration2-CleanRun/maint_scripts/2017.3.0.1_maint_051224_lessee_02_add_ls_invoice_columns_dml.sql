/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051224_lessee_02_add_ls_invoice_columns_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.1 5/10/2018 Crystal Yura       Add columns to use for payment recon interfaces
||============================================================================
*/

insert into pp_import_column
(import_Type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
select import_Type_id, 'paid_date', 'Paid Date',0,1,'date',1 
from pp_import_type a where lower(import_table_name) = 'ls_import_invoice'
and not exists (select 1 from pp_import_column b where a.import_type_id = b.import_type_id and lower(b.column_name) = 'paid_date');


insert into pp_import_column
(import_Type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
select import_Type_id, 'external_value1', 'External Value 1',0,1,'varchar2(254)',1 
from pp_import_type a where lower(import_table_name) = 'ls_import_invoice'
and not exists (select 1 from pp_import_column b where a.import_type_id = b.import_type_id and lower(b.column_name) = 'external_value1');


insert into pp_import_column
(import_Type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
select import_Type_id, 'external_value2', 'External Value 2',0,1,'varchar2(254)',1 
from pp_import_type a where lower(import_table_name) = 'ls_import_invoice'
and not exists (select 1 from pp_import_column b where a.import_type_id = b.import_type_id and lower(b.column_name) = 'external_value2');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5303, 0, 2017, 3, 0, 1, 51224, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051224_lessee_02_add_ls_invoice_columns_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;