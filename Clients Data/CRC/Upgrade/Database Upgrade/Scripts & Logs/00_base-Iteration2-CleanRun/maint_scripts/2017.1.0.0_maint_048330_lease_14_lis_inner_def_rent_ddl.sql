/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_048330_lease_14_lis_inner_def_rent_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.1.0.0  08/25/2017 build script   2017.1.0.0 Release
||============================================================================
*/

CREATE OR REPLACE VIEW v_multicurrency_lis_inner (
  ilr_id,
  ilr_number,
  current_revision,
  revision,
  set_of_books_id,
  month,
  beg_capital_cost,
  end_capital_cost,
  beg_obligation,
  end_obligation,
  beg_lt_obligation,
  end_lt_obligation,
  interest_accrual,
  principal_accrual,
  interest_paid,
  principal_paid,
  executory_accrual1,
  executory_accrual2,
  executory_accrual3,
  executory_accrual4,
  executory_accrual5,
  executory_accrual6,
  executory_accrual7,
  executory_accrual8,
  executory_accrual9,
  executory_accrual10,
  executory_paid1,
  executory_paid2,
  executory_paid3,
  executory_paid4,
  executory_paid5,
  executory_paid6,
  executory_paid7,
  executory_paid8,
  executory_paid9,
  executory_paid10,
  contingent_accrual1,
  contingent_accrual2,
  contingent_accrual3,
  contingent_accrual4,
  contingent_accrual5,
  contingent_accrual6,
  contingent_accrual7,
  contingent_accrual8,
  contingent_accrual9,
  contingent_accrual10,
  contingent_paid1,
  contingent_paid2,
  contingent_paid3,
  contingent_paid4,
  contingent_paid5,
  contingent_paid6,
  contingent_paid7,
  contingent_paid8,
  contingent_paid9,
  contingent_paid10,
  current_lease_cost,
  beg_deferred_rent,
  deferred_rent,
  end_deferred_rent,
  beg_st_deferred_rent,
  end_st_deferred_rent,
  depr_expense,
  begin_reserve,
  end_reserve,
  depr_exp_alloc_adjust,
  lease_id,
  company_id,
  in_service_exchange_rate,
  purchase_option_amt,
  termination_amt,
  net_present_value,
  capital_cost,
  is_om
) AS
SELECT
  A.ilr_id,
  A.ilr_number,
  A.current_revision,
  a.revision,
  a.set_of_books_id,
  a.MONTH,
  A.beg_capital_cost,
  a.end_capital_cost,
  a.beg_obligation,
  a.end_obligation,
  a.beg_lt_obligation,
  a.end_lt_obligation,
  a.interest_accrual,
  a.principal_accrual,
  a.interest_paid,
  a.principal_paid,
  a.executory_accrual1,
  a.executory_accrual2,
  a.executory_accrual3,
  a.executory_accrual4,
  a.executory_accrual5,
  a.executory_accrual6,
  a.executory_accrual7,
  a.executory_accrual8,
  a.executory_accrual9,
  a.executory_accrual10,
  a.executory_paid1,
  a.executory_paid2,
  a.executory_paid3,
  a.executory_paid4,
  a.executory_paid5,
  a.executory_paid6,
  a.executory_paid7,
  a.executory_paid8,
  a.executory_paid9,
  a.executory_paid10,
  a.contingent_accrual1,
  a.contingent_accrual2,
  a.contingent_accrual3,
  a.contingent_accrual4,
  a.contingent_accrual5,
  a.contingent_accrual6,
  a.contingent_accrual7,
  a.contingent_accrual8,
  a.contingent_accrual9,
  a.contingent_accrual10,
  a.contingent_paid1,
  a.contingent_paid2,
  a.contingent_paid3,
  a.contingent_paid4,
  a.contingent_paid5,
  a.contingent_paid6,
  a.contingent_paid7,
  a.contingent_paid8,
  a.contingent_paid9,
  a.contingent_paid10,
  a.current_lease_cost,
  a.beg_deferred_rent,
  a.deferred_rent,
  a.end_deferred_rent,
  a.beg_st_deferred_rent,
  a.end_st_deferred_rent,
  depr.depr_expense,
  depr.begin_reserve,
  depr.end_reserve,
  depr.depr_exp_alloc_adjust,
  a.lease_id,
  a.company_id,
  A.in_service_exchange_rate,
  a.purchase_option_amt,
  a.termination_amt,
  a.net_present_value,
  a.capital_cost,
  A.is_om
FROM mv_multicurr_lis_inner_amounts A
LEFT OUTER JOIN mv_multicurr_lis_inner_depr depr
  ON a.ilr_id = depr.ilr_id
  AND a.revision = depr.revision
  AND A.set_of_books_id = depr.set_of_books_id
  AND a.month = depr.month
/

GRANT DELETE,INSERT,SELECT,UPDATE ON v_multicurrency_lis_inner TO pwrplant_role_dev;
GRANT SELECT ON v_multicurrency_lis_inner TO pwrplant_role_rdonly;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4030, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_14_lis_inner_def_rent_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;