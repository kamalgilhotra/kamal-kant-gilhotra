/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052005_lessee_01_ilr_import_pay_term_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  07/17/2018 Shane "C" Ward    Update import format for Payment Term Date on ILR Import
||============================================================================
*/

UPDATE PP_IMPORT_COLUMN
   SET DESCRIPTION = 'Payment Term Date (YYYYMMDD)'
 WHERE LOWER(COLUMN_NAME) IN ('payment_term_date')
   AND IMPORT_TYPE_ID = 252;
   
UPDATE PP_IMPORT_COLUMN
   SET DESCRIPTION = 'Depr Units of Production'
 WHERE LOWER(COLUMN_NAME) IN ('depr_calc_method')
   AND IMPORT_TYPE_ID = 252;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8142, 0, 2017, 4, 0, 0, 52005, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052005_lessee_01_ilr_import_pay_term_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;