/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045537_projects_wofp_estimates_dynamic_validation_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 06/10/2016 Josh Sandler   
||============================================================================
*/
 
insert into wo_validation_type (wo_validation_type_id, description, long_description, function, find_company, col1, col2, hard_edit)
values
(49,'WO/FP Revision Update','WO/FP Revision Update','w_wo_estimates.cb_update','select company_id from work_order_control where work_order_id = <arg1>','work_order_id','revision',1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3215, 0, 2016, 1, 0, 0, 045537, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045537_projects_wofp_estimates_dynamic_validation_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;