/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038388_lease_tax_reports.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 06/16/2014 Ryan Oliveria      Adding Lease Tax Reports
||============================================================================
*/

/**************************************************
*
*  Did you get an ERROR running this script?
*
*  On the teeny tiny chance that there are rows in LS_RENT_BUCKET_ADMIN with null account ID's
*  AND there are no GL_ACCOUNT's with a lease account_type_id (11),
*  then this script will fail.
*  Try creating some lease accounts (gl_account.account_type_id = 11) and running this script again.
*
*  Have a nice day! : )
*
***************************************************/
update LS_RENT_BUCKET_ADMIN
   set ACCRUAL_ACCT_ID =
        (select EXEC_ACCRUAL_ACCOUNT_ID
           from (select EXEC_ACCRUAL_ACCOUNT_ID, count(1) CNT, 1 THE_SORT
                   from LS_ILR_GROUP
                  group by EXEC_ACCRUAL_ACCOUNT_ID
                 union all
                 select GL_ACCOUNT_ID, 0 CNT, 2 THE_SORT
                   from GL_ACCOUNT
                  where ACCOUNT_TYPE_ID = 11
                  order by THE_SORT asc, CNT desc)
          where ROWNUM = 1)
 where ACCRUAL_ACCT_ID is null;

update LS_RENT_BUCKET_ADMIN
   set EXPENSE_ACCT_ID =
        (select EXEC_EXPENSE_ACCOUNT_ID
           from (select EXEC_EXPENSE_ACCOUNT_ID, count(1) CNT, 1 THE_SORT
                   from LS_ILR_GROUP
                  group by EXEC_EXPENSE_ACCOUNT_ID
                 union all
                 select GL_ACCOUNT_ID, 0 CNT, 2 THE_SORT
                   from GL_ACCOUNT
                  where ACCOUNT_TYPE_ID = 11
                  order by THE_SORT asc, CNT desc)
          where ROWNUM = 1)
 where EXPENSE_ACCT_ID is null;


alter table LS_RENT_BUCKET_ADMIN
   modify (ACCRUAL_ACCT_ID number(22,0) not null,
           EXPENSE_ACCT_ID number(22,0) not null);

--delete from pp_reports where report_type_id = 310 or datawindow like 'dw_ls_rpt_bucket_balance%';
--delete from pp_report_type where report_type_id = 310;
--delete from pp_reports_time_option where pp_report_time_option_id = 201;

insert into PP_REPORT_TYPE (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER) values (310, 'Lessee - Tax', 3);

insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1, KEYCOLUMN1, DWNAME2, LABEL2, KEYCOLUMN2,
    DWNAME3, LABEL3, KEYCOLUMN3)
values
   (201, 'Lease Span + SOB', 'uo_ppbase_report_parms_dddw_three', 'dw_ls_months', 'Start (yyyymm)', 'month_number',
    'dw_ls_months', 'End (yyyymm)', 'month_number', 'dw_ls_set_of_books', 'Set of Books', 'set_of_books_id');

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_FILTER_ID)
   select max(REPORT_ID) + 1,
          'Local Taxes (Tree View)',
          'This report displays taxes, grouped by local tax summary.',
          'Lessee' SUBSYSTEM,
          'dw_ls_rpt_tax_local' DATAWINDOW,
          'Lessee - 1019a' REPORT_NUMBER,
          11 PP_REPORT_SUBSYSTEM_ID,
          310 REPORT_TYPE_ID,
          201 PP_REPORT_TIME_OPTION_ID,
          1 PP_REPORT_STATUS_ID,
          3 PP_REPORT_ENVIR_ID,
          1 PP_REPORT_FILTER_ID
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_FILTER_ID)
   select max(REPORT_ID) + 1,
          'Local Taxes (Static)',
          'This report displays taxes, grouped by local tax summary.  ',
          'Lessee' SUBSYSTEM,
          'dw_ls_rpt_tax_local_static' DATAWINDOW,
          'Lessee - 1019b' REPORT_NUMBER,
          11 PP_REPORT_SUBSYSTEM_ID,
          310 REPORT_TYPE_ID,
          201 PP_REPORT_TIME_OPTION_ID,
          1 PP_REPORT_STATUS_ID,
          3 PP_REPORT_ENVIR_ID,
          1 PP_REPORT_FILTER_ID
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_FILTER_ID)
   select max(REPORT_ID) + 1,
          'Balances by Bucket (Tree View)',
          'This report displays amounts by bucket.',
          'Lessee' SUBSYSTEM,
          'dw_ls_rpt_bucket_balance' DATAWINDOW,
          'Lessee - 1020a' REPORT_NUMBER,
          11 PP_REPORT_SUBSYSTEM_ID,
          309 REPORT_TYPE_ID,
          201 PP_REPORT_TIME_OPTION_ID,
          1 PP_REPORT_STATUS_ID,
          3 PP_REPORT_ENVIR_ID,
          1 PP_REPORT_FILTER_ID
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_FILTER_ID)
   select max(REPORT_ID) + 1,
          'Balances by Bucket (Static)',
          'This report displays amounts by bucket.',
          'Lessee' SUBSYSTEM,
          'dw_ls_rpt_bucket_balance_static' DATAWINDOW,
          'Lessee - 1020b' REPORT_NUMBER,
          11 PP_REPORT_SUBSYSTEM_ID,
          309 REPORT_TYPE_ID,
          201 PP_REPORT_TIME_OPTION_ID,
          1 PP_REPORT_STATUS_ID,
          3 PP_REPORT_ENVIR_ID,
          1 PP_REPORT_FILTER_ID
     from PP_REPORTS;

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (44, 'Lessee - Tax', 'uo_ls_selecttabs_tax');

update PP_REPORTS
   set PP_REPORT_FILTER_ID = 44, PP_REPORT_TIME_OPTION_ID = 2
 where REPORT_NUMBER like 'Lessee - 1019%'
    or REPORT_NUMBER like 'Lessee - 1020%';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1210, 0, 10, 4, 3, 0, 38388, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038388_lease_tax_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
