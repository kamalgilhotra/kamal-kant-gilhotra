 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045289_pwrtax_import_xlate_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 04/28/2016 Rob Burns      Initial script for import translations
 ||============================================================================
 */ 

UPDATE pp_import_lookup 
SET lookup_values_alternate_sql = 'select business_segment.description, company.<company_id> from business_segment, company_bus_segment_control, company where business_segment.bus_segment_id = company_bus_segment_control.bus_segment_id and company_bus_segment_control.company_id = company.company_id and 5=5'
WHERE import_lookup_id = 605;

UPDATE pp_import_lookup 
SET lookup_values_alternate_sql = 'select sub_account.description, business_segment.<bus_segment_id>, utility_account.<utility_account_id> from sub_account, business_segment, utility_account where sub_account.utility_account_id = utility_account.utility_account_id and sub_account.bus_segment_id = utility_account.bus_segment_id and utility_account.bus_segment_id = business_segment.bus_segment_id and 5=5'
WHERE import_lookup_id = 612;

UPDATE pp_import_lookup 
SET lookup_values_alternate_sql = 'select sub_account.long_description, business_segment.<bus_segment_id>, utility_account.<utility_account_id> from sub_account, business_segment, utility_account where sub_account.utility_account_id = utility_account.utility_account_id and sub_account.bus_segment_id = utility_account.bus_segment_id and utility_account.bus_segment_id = business_segment.bus_segment_id and 5=5'
WHERE import_lookup_id = 613;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3176, 0, 2016, 1, 0, 0, 045289, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045289_pwrtax_import_xlate_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;