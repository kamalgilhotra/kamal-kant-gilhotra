/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_051996_lessee_02_retire_dynamic_validation_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2018.1.0.0 07/16/2018 Crystal Yura     Add new dynamic validation for Lessee Retirements
||============================================================================
*/


INSERT INTO WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, FUNCTION, FIND_COMPANY, COL1, HARD_EDIT)
  SELECT 5006,
         'Lessee Retirements Open',
         'Lessee Module Retirements Open',
         'uo_ls_assetcntr_retire_payment_terms',
         'select company_id from ls_asset where ls_asset_id = <arg1>',
         'ls_asset_id',
         '0'
    FROM DUAL
   WHERE NOT EXISTS (SELECT 1
            FROM WO_VALIDATION_TYPE
           WHERE WO_VALIDATION_TYPE_ID = 5006
             AND LOWER(FUNCTION) = 'uo_ls_assetcntr_retire_payment_terms');

UPDATE WO_VALIDATION_TYPE SET HARD_EDIT = 1 WHERE WO_VALIDATION_TYPE_ID = 25;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8183, 0, 2018, 1, 0, 0, 51996, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051996_lessee_02_retire_dynamic_validation_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
