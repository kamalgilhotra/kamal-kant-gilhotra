/*
||================================================================================================
|| Application: PowerPlan
|| Module: 		Security
|| File Name:   maint_044915_websys_security_permissions_int_man_dml.sql
||================================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||================================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------  ----------------------------------------------------
|| 2015.2     09/14/2015 Khamchanh Sisouphanh Updating data for web security - Integration Manager
||================================================================================================
*/

--Job Server Permissions
DELETE FROM PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL
 WHERE MODULE_ID = 9;
 
--File Watcher Component 
INSERT INTO PWRPLANT.PP_WEB_COMPONENT (COMPONENT_ID, DESCRIPTION, LONG_DESCRIPTION) values (8, 'File Watcher', 'The File Watcher component.'); 

--Permissions Inserts
----Schedule > AddClone
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.AddCloneBudgetProcesses', 9, null,'Schedule','Add/Clone','Budget Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.AddCloneMonthEndProcesses', 9, null,'Schedule','Add/Clone','Month End Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.AddCloneCRSystemProcesses', 9, null,'Schedule','Add/Clone','CR/System Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.AddCloneSQLJobs', 9, null,'Schedule','Add/Clone','SQL Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.AddCloneJobFlowJobs', 9, null,'Schedule','Add/Clone','Job Flow Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.AddCloneInputJobs', 9, null,'Schedule','Add/Clone','Input Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.AddCloneNotification', 9, null,'Schedule','Add/Clone','Notification');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.AddCloneCustomProcesses', 9, null,'Schedule','Add/Clone','Custom Processes');

----Schedule > Edit
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.EditBudgetProcesses', 9, null,'Schedule','Edit','Budget Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.EditMonthEndProcesses', 9, null,'Schedule','Edit','Month End Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.EditCRSystemProcesses', 9, null,'Schedule','Edit','CR/System Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.EditSQLJobs', 9, null,'Schedule','Edit','SQL Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.EditJobFlowJobs', 9, null,'Schedule','Edit','Job Flow Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.EditInputJobs', 9, null,'Schedule','Edit','Input Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.EditNotification', 9, null,'Schedule','Edit','Notification');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.EditCustomProcesses', 9, null,'Schedule','Edit','Custom Processes');

----Schedule > Delete
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.DeleteBudgetProcesses', 9, null,'Schedule','Delete','Budget Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.DeleteMonthEndProcesses', 9, null,'Schedule','Delete','Month End Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.DeleteCRSystemProcesses', 9, null,'Schedule','Delete','CR/System Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.DeleteSQLJobs', 9, null,'Schedule','Delete','SQL Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.DeleteJobFlowJobs', 9, null,'Schedule','Delete','Job Flow Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.DeleteInputJobs', 9, null,'Schedule','Delete','Input Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.DeleteNotification', 9, null,'Schedule','Delete','Notification');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Schedule.DeleteCustomProcesses', 9, null,'Schedule','Delete','Custom Processes');

----Queue > Kill
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.KillBudgetProcesses', 9, null,'Queue','Kill','Budget Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.KillMonthEndProcesses', 9, null,'Queue','Kill','Month End Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.KillCRSystemProcesses', 9, null,'Queue','Kill','CR/System Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.KillSQLJobs', 9, null,'Queue','Kill','SQL Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.KillJobFlowJobs', 9, null,'Queue','Kill','Job Flow Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.KillInputJobs', 9, null,'Queue','Kill','Input Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.KillNotification', 9, null,'Queue','Kill','Notification');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.KillCustomProcesses', 9, null,'Queue','Kill','Custom Processes');

----Job Flows > Add/Clone
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.AddCloneBudgetProcesses', 9,7,null,'Add/Clone','Budget Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.AddCloneMonthEndProcesses', 9,7,null,'Add/Clone','Month End Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.AddCloneCRSystemProcesses', 9,7,null,'Add/Clone','CR/System Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.AddCloneSQLJobs', 9,7,null,'Add/Clone','SQL Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.AddCloneJobFlowJobs', 9,7,null,'Add/Clone','Job Flow Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.AddCloneInputJobs', 9,7,null,'Add/Clone','Input Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.AddCloneNotification', 9,7,null,'Add/Clone','Notification');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.AddCloneCustomProcesses', 9,7,null,'Add/Clone','Custom Processes');

----Job Flows > Edit
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.EditBudgetProcesses', 9,7,null,'Edit','Budget Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.EditMonthEndProcesses', 9,7,null,'Edit','Month End Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.EditCRSystemProcesses', 9,7,null,'Edit','CR/System Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.EditSQLJobs', 9,7,null,'Edit','SQL Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.EditJobFlowJobs', 9,7,null,'Edit','Job Flow Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.EditInputJobs', 9,7,null,'Edit','Input Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.EditNotification', 9,7,null,'Edit','Notification');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.EditCustomProcesses', 9,7,null,'Edit','Custom Processes');

----Job Flows > Delete
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.DeleteBudgetProcesses', 9,7,null,'Delete','Budget Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.DeleteMonthEndProcesses', 9,7,null,'Delete','Month End Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.DeleteCRSystemProcesses', 9,7,null,'Delete','CR/System Processes');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.DeleteSQLJobs', 9,7,null,'Delete','SQL Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.DeleteJobFlowJobs', 9,7,null,'Delete','Job Flow Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.DeleteInputJobs', 9,7,null,'Delete','Input Jobs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.DeleteNotification', 9,7,null,'Delete','Notification');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.JobFlows.DeleteCustomProcesses', 9,7,null,'Delete','Custom Processes');

----File Watcher 
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.FileWatcher.AddClone', 9,8,null,'All','Add/Clone');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.FileWatcher.Edit', 9,8,null,'All','Edit');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.FileWatcher.Delete', 9,8,null,'All','Delete');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2872, 0, 2015, 2, 0, 0, 044915, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044915_websys_security_permissions_int_man_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;