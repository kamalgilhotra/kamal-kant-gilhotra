 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041569_pcm_PPBase_WS_Class_Codes_dml.sql
|| Description: Create Relationship record for Class Codes
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	12/05/2014 Louis Alston   New Class Codes Page
||============================================================================
*/

DECLARE 
  alreadyThere NUMBER;
BEGIN

  SELECT COUNT(*)
  INTO alreadyThere
    FROM PPBASE_WORKSPACE
  WHERE MODULE = 'pcm' 
		AND WORKSPACE_IDENTIFIER = 'wo_maint_class_codes';

  IF( alreadyThere = 0 ) THEN
	INSERT INTO PPBASE_WORKSPACE 
	   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID)
	   VALUES ( 'pcm'
				,'wo_maint_class_codes'
				,'Class Codes (WO)'
				,'uo_pcm_maint_wksp_class_codes'
				,'Class Codes (WO)'
				,1
				);
				
  ELSE --alreadyThere should = 1 since Module and WORKSPACE_IDENTIFIER are the primary key.
    UPDATE PPBASE_WORKSPACE
		SET WORKSPACE_UO_NAME = 'uo_pcm_maint_wksp_class_codes'
			, LABEL = 'Class Codes (WO)'
			, MINIHELP = 'Class Codes (WO)'
			, OBJECT_TYPE_ID = 1
	WHERE MODULE = 'pcm' AND WORKSPACE_IDENTIFIER = 'wo_maint_class_codes';
  END IF;

END;
/ 
			
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2091, 0, 2015, 1, 0, 0, 040800, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040800_pcm_PPBase_WS_Class_Codes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;