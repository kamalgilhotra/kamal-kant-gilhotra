/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052072_lessee_01_add_PO_gain_loss_je_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  07/24/2018 Josh Sandler     Add new trans types for Purchase option gain/loss
||============================================================================
*/

UPDATE je_trans_type
SET description = '3071 - Lease PO Gain/Loss (Asset) Debit'
WHERE trans_type = 3071;

UPDATE je_trans_type
SET description = '3072 - Lease PO Gain/Loss (Asset) Credit'
WHERE trans_type = 3072;

INSERT INTO je_trans_type (trans_type, description)
SELECT 3074, '3074 - Lease PO Gain/Loss (Liability) Debit'
FROM dual
WHERE NOT EXISTS (SELECT 1 FROM je_trans_type WHERE trans_type = 3074);

INSERT INTO je_trans_type (trans_type, description)
SELECT 3075, '3075 - Lease PO Gain/Loss (Liability) Credit'
FROM dual
WHERE NOT EXISTS (SELECT 1 FROM je_trans_type WHERE trans_type = 3075);

INSERT INTO je_method_trans_type (je_method_id, trans_type)
SELECT 1, trans_type FROM je_trans_type
WHERE trans_type = 3074
AND NOT EXISTS (SELECT 1 FROM je_method_trans_type WHERE trans_type = 3074 AND je_method_id = 1);

INSERT INTO je_method_trans_type (je_method_id, trans_type)
SELECT 1, trans_type FROM je_trans_type
WHERE trans_type = 3075
AND NOT EXISTS (SELECT 1 FROM je_method_trans_type WHERE trans_type = 3075 AND je_method_id = 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8622, 0, 2017, 4, 0, 0, 52072, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052072_lessee_01_add_PO_gain_loss_je_trans_types_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
