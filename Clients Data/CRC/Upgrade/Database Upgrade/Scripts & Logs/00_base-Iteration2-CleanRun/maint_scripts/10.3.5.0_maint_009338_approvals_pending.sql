/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009338_approvals_pending.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/06/2012 Chris Mardis Point Release
||============================================================================
*/

--** Repair the existing bad data
delete from APPROVALS_PENDING A
 where not exists (select 1
          from WORK_ORDER_APPROVAL B
         where A.WORK_ORDER_ID = B.WORK_ORDER_ID
           and A.REVISION = B.REVISION
           and B.APPROVAL_STATUS_ID = 2);

insert into APPROVALS_PENDING
   (WORK_ORDER_ID, REVISION)
   select WORK_ORDER_ID, REVISION
     from WORK_ORDER_APPROVAL B
    where B.APPROVAL_STATUS_ID = 2
      and not exists (select 1
             from APPROVALS_PENDING A
            where A.WORK_ORDER_ID = B.WORK_ORDER_ID
              and A.REVISION = B.REVISION);

--** Replace the 3 old triggers with 1 new trigger
drop trigger TRIG_WO_APPROVALS_NOT_PENDING;
drop trigger TRIG_WO_APPROVALS_DELETE;
--drop trigger TRIG_WO_APPROVALS_PENDING; --Just changing this to cover all 3

create or replace trigger TRIG_WO_APPROVALS_PENDING
   after insert or update of WORK_ORDER_ID, REVISION, APPROVAL_STATUS_ID, AUTHORIZED_DATE1, AUTHORIZED_DATE2, AUTHORIZED_DATE3, AUTHORIZED_DATE4, AUTHORIZED_DATE5, AUTHORIZED_DATE6, AUTHORIZED_DATE7, AUTHORIZED_DATE8, AUTHORIZED_DATE9, AUTHORIZED_DATE10, AUTHORIZER1, AUTHORIZER2, AUTHORIZER3, AUTHORIZER4, AUTHORIZER5, AUTHORIZER6, AUTHORIZER7, AUTHORIZER8, AUTHORIZER9, AUTHORIZER10 or delete on WORK_ORDER_APPROVAL
   referencing new as new old as old
   for each row

declare
   AUTH_LEVEL number(2, 0);
   AUTH_USER  varchar2(18);

begin

   if INSERTING or UPDATING then
      if UPDATING then
         delete from APPROVALS_PENDING
          where WORK_ORDER_ID = :NEW.WORK_ORDER_ID
            and REVISION = :NEW.REVISION;
      end if;

      if :NEW.APPROVAL_STATUS_ID = 2 then
         if :NEW.AUTHORIZER1 is not null and :NEW.AUTHORIZED_DATE1 is null then
            AUTH_LEVEL := 1;
            AUTH_USER  := :NEW.AUTHORIZER1;
         elsif :NEW.AUTHORIZER2 is not null and :NEW.AUTHORIZED_DATE2 is null then
            AUTH_LEVEL := 2;
            AUTH_USER  := :NEW.AUTHORIZER2;
         elsif :NEW.AUTHORIZER3 is not null and :NEW.AUTHORIZED_DATE3 is null then
            AUTH_LEVEL := 3;
            AUTH_USER  := :NEW.AUTHORIZER3;
         elsif :NEW.AUTHORIZER4 is not null and :NEW.AUTHORIZED_DATE4 is null then
            AUTH_LEVEL := 4;
            AUTH_USER  := :NEW.AUTHORIZER4;
         elsif :NEW.AUTHORIZER5 is not null and :NEW.AUTHORIZED_DATE5 is null then
            AUTH_LEVEL := 5;
            AUTH_USER  := :NEW.AUTHORIZER5;
         elsif :NEW.AUTHORIZER6 is not null and :NEW.AUTHORIZED_DATE6 is null then
            AUTH_LEVEL := 6;
            AUTH_USER  := :NEW.AUTHORIZER6;
         elsif :NEW.AUTHORIZER7 is not null and :NEW.AUTHORIZED_DATE7 is null then
            AUTH_LEVEL := 7;
            AUTH_USER  := :NEW.AUTHORIZER7;
         elsif :NEW.AUTHORIZER8 is not null and :NEW.AUTHORIZED_DATE8 is null then
            AUTH_LEVEL := 8;
            AUTH_USER  := :NEW.AUTHORIZER8;
         elsif :NEW.AUTHORIZER9 is not null and :NEW.AUTHORIZED_DATE9 is null then
            AUTH_LEVEL := 9;
            AUTH_USER  := :NEW.AUTHORIZER9;
         elsif :NEW.AUTHORIZER10 is not null and :NEW.AUTHORIZED_DATE10 is null then
            AUTH_LEVEL := 10;
            AUTH_USER  := :NEW.AUTHORIZER10;
         end if;

         insert into APPROVALS_PENDING
            (WORK_ORDER_ID, REVISION, PENDING_USER, PENDING_LEVEL)
         values
            (:NEW.WORK_ORDER_ID, :NEW.REVISION, AUTH_USER, AUTH_LEVEL);
      end if;
   elsif DELETING then
      delete from APPROVALS_PENDING
       where WORK_ORDER_ID = :OLD.WORK_ORDER_ID
         and REVISION = :OLD.REVISION;
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (173, 0, 10, 3, 5, 0, 9338, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009338_approvals_pending.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
