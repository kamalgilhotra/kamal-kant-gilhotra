/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049512_lessor_01_add_termination_action_ilr_details_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/22/2018  Andrew Hill    Add new termination action to ILR Details Actions DropDown
||============================================================================
*/

INSERT INTO ppbase_actions_windows(ID, module, action_identifier, action_text, action_order, action_event)
SELECT nvl(MAX(ID), 0) + 1, 'LESSEE', 'terminate_ilr', 'Terminate ILR', 11, 'ue_terminate'
from ppbase_actions_windows;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5807, 0, 2017, 4, 0, 0, 49512, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_049512_lessor_01_add_termination_action_ilr_details_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;