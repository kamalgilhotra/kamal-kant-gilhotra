/*
||==========================================================================================
|| Application: PowerPlan
|| Module: Property Tax
|| File Name:   maint_043569_proptax_preallo_dtl_import_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2    04/10/2015  Graham Miller    Add ability to import ledger detail on preallo impt
||==========================================================================================
*/


--Alter preallo import tables to include ledger detail fields

alter table pt_import_preallo add
(  ld_description_1          VARCHAR2(254)  NULL,
  ld_description_2          VARCHAR2(254)  NULL,
  ld_description_3          VARCHAR2(254)  NULL,
  ld_unit_number            VARCHAR2(50)   NULL,
  ld_serial_number          VARCHAR2(50)   NULL,
  ld_make                   VARCHAR2(50)   NULL,
  ld_model                  VARCHAR2(50)   NULL,
  ld_model_year             VARCHAR2(50)   NULL,
  ld_body_type              VARCHAR2(50)   NULL,
  ld_fuel_type              VARCHAR2(50)   NULL,
  ld_gross_weight           VARCHAR2(50)   NULL,
  ld_license_number         VARCHAR2(50)   NULL,
  ld_license_weight         VARCHAR2(50)   NULL,
  ld_license_expiration     VARCHAR2(35)   NULL,
  ld_key_number             VARCHAR2(50)   NULL,
  ld_primary_driver         VARCHAR2(50)   NULL,
  ld_group_code             VARCHAR2(50)   NULL,
  ld_length                 VARCHAR2(50)   NULL,
  ld_width                  VARCHAR2(50)   NULL,
  ld_height                 VARCHAR2(50)   NULL,
  ld_construction_type      VARCHAR2(50)   NULL,
  ld_stories                VARCHAR2(50)   NULL,
  ld_source_system          VARCHAR2(50)   NULL,
  ld_external_code          VARCHAR2(50)   NULL,
  ld_lease_number           VARCHAR2(50)   NULL,
  ld_lease_start            VARCHAR2(35)   NULL,
  ld_lease_end              VARCHAR2(35)   NULL,
  ld_notes                  VARCHAR2(4000) NULL,
  ledger_detail_id          NUMBER(22,0)   NULL,
  ledger_detail_xlate       VARCHAR2(254)  NULL);


alter table pt_import_preallo_archive add
(  ld_description_1          VARCHAR2(254)  NULL,
  ld_description_2          VARCHAR2(254)  NULL,
  ld_description_3          VARCHAR2(254)  NULL,
  ld_unit_number            VARCHAR2(50)   NULL,
  ld_serial_number          VARCHAR2(50)   NULL,
  ld_make                   VARCHAR2(50)   NULL,
  ld_model                  VARCHAR2(50)   NULL,
  ld_model_year             VARCHAR2(50)   NULL,
  ld_body_type              VARCHAR2(50)   NULL,
  ld_fuel_type              VARCHAR2(50)   NULL,
  ld_gross_weight           VARCHAR2(50)   NULL,
  ld_license_number         VARCHAR2(50)   NULL,
  ld_license_weight         VARCHAR2(50)   NULL,
  ld_license_expiration     VARCHAR2(35)   NULL,
  ld_key_number             VARCHAR2(50)   NULL,
  ld_primary_driver         VARCHAR2(50)   NULL,
  ld_group_code             VARCHAR2(50)   NULL,
  ld_length                 VARCHAR2(50)   NULL,
  ld_width                  VARCHAR2(50)   NULL,
  ld_height                 VARCHAR2(50)   NULL,
  ld_construction_type      VARCHAR2(50)   NULL,
  ld_stories                VARCHAR2(50)   NULL,
  ld_source_system          VARCHAR2(50)   NULL,
  ld_external_code          VARCHAR2(50)   NULL,
  ld_lease_number           VARCHAR2(50)   NULL,
  ld_lease_start            VARCHAR2(35)   NULL,
  ld_lease_end              VARCHAR2(35)   NULL,
  ld_notes                  VARCHAR2(4000) NULL,
  ledger_detail_id          NUMBER(22,0)   NULL,
  ledger_detail_xlate       VARCHAR2(254)  NULL);





  
COMMENT ON COLUMN pt_import_preallo_archive.ld_description_1 IS 'Description 1 on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_description_2 IS 'Description 2 on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_description_3 IS 'Description 3 on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_unit_number IS 'Unit number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_serial_number IS 'Serial number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_make IS 'Make on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_model IS 'Model on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_model_year IS 'Model year on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_body_type IS 'Body type on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_fuel_type IS 'Fuel type on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_gross_weight IS 'Gross weight on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_license_number IS 'License number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_license_weight IS 'License weight on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_license_expiration IS 'License expiration on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_key_number IS 'Key number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_primary_driver IS 'Primary driver on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_group_code IS 'Group code on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_length IS 'Length on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_width IS 'Width on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_height IS 'Height on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_construction_type IS 'Construction type on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_stories IS 'Stories on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_source_system IS 'Source system on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_external_code IS 'External code on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_lease_number IS 'Lease number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_lease_start IS 'Lease start on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_lease_end IS 'Lease end on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_notes IS 'User input freeform notes for the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ledger_detail_id IS 'System-assigned identifier of a particular ledger detail item.';
COMMENT ON COLUMN pt_import_preallo_archive.ledger_detail_xlate IS 'Value from the import file translated to obtain the ledger_detail_id.';


COMMENT ON COLUMN pt_import_preallo_archive.ld_description_1 IS 'Description 1 on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_description_2 IS 'Description 2 on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_description_3 IS 'Description 3 on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_unit_number IS 'Unit number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_serial_number IS 'Serial number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_make IS 'Make on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_model IS 'Model on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_model_year IS 'Model year on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_body_type IS 'Body type on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_fuel_type IS 'Fuel type on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_gross_weight IS 'Gross weight on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_license_number IS 'License number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_license_weight IS 'License weight on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_license_expiration IS 'License expiration on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_key_number IS 'Key number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_primary_driver IS 'Primary driver on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_group_code IS 'Group code on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_length IS 'Length on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_width IS 'Width on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_height IS 'Height on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_construction_type IS 'Construction type on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_stories IS 'Stories on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_source_system IS 'Source system on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_external_code IS 'External code on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_lease_number IS 'Lease number on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_lease_start IS 'Lease start on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_lease_end IS 'Lease end on the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ld_notes IS 'User input freeform notes for the ledger detail record.';
COMMENT ON COLUMN pt_import_preallo_archive.ledger_detail_id IS 'System-assigned identifier of a particular ledger detail item.';
COMMENT ON COLUMN pt_import_preallo_archive.ledger_detail_xlate IS 'Value from the import file translated to obtain the ledger_detail_id.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2574, 0, 2015, 2, 0, 0, 043569, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043569_proptax_preallo_dtl_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;