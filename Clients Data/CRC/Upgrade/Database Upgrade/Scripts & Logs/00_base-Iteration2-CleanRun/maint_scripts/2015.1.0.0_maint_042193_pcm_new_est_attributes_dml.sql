/*
||==========================================================================================
|| Application: PowerPlan
|| Module:      PCM
|| File Name:   maint_042193_pcm_new_est_attributes_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     1/15/15    MARDIS              Added 10 new estimate attributes
||==========================================================================================
*/

update workflow_subsystem
set send_sql = replace(send_sql,'work_order_id,','work_order_id, attribute01_id, attribute02_id, attribute03_id, attribute04_id, attribute05_id, attribute06_id, attribute07_id, attribute08_id, attribute09_id, attribute10_id,')
where subsystem = 'bi_approval';
commit;

insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 01', 'wem.attribute01_id', 'wem.attribute01_id', 'wem.attribute01_id', 'Attribute 01', 'wem.attribute01_id', 'wem.attribute01_id', 'wem.attribute01_id', 'Attribute 01', null, 'attribute01_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 02', 'wem.attribute02_id', 'wem.attribute02_id', 'wem.attribute02_id', 'Attribute 02', 'wem.attribute02_id', 'wem.attribute02_id', 'wem.attribute02_id', 'Attribute 02', null, 'attribute02_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 03', 'wem.attribute03_id', 'wem.attribute03_id', 'wem.attribute03_id', 'Attribute 03', 'wem.attribute03_id', 'wem.attribute03_id', 'wem.attribute03_id', 'Attribute 03', null, 'attribute03_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 04', 'wem.attribute04_id', 'wem.attribute04_id', 'wem.attribute04_id', 'Attribute 04', 'wem.attribute04_id', 'wem.attribute04_id', 'wem.attribute04_id', 'Attribute 04', null, 'attribute04_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 05', 'wem.attribute05_id', 'wem.attribute05_id', 'wem.attribute05_id', 'Attribute 05', 'wem.attribute05_id', 'wem.attribute05_id', 'wem.attribute05_id', 'Attribute 05', null, 'attribute05_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 06', 'wem.attribute06_id', 'wem.attribute06_id', 'wem.attribute06_id', 'Attribute 06', 'wem.attribute06_id', 'wem.attribute06_id', 'wem.attribute06_id', 'Attribute 06', null, 'attribute06_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 07', 'wem.attribute07_id', 'wem.attribute07_id', 'wem.attribute07_id', 'Attribute 07', 'wem.attribute07_id', 'wem.attribute07_id', 'wem.attribute07_id', 'Attribute 07', null, 'attribute07_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 08', 'wem.attribute08_id', 'wem.attribute08_id', 'wem.attribute08_id', 'Attribute 08', 'wem.attribute08_id', 'wem.attribute08_id', 'wem.attribute08_id', 'Attribute 08', null, 'attribute08_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 09', 'wem.attribute09_id', 'wem.attribute09_id', 'wem.attribute09_id', 'Attribute 09', 'wem.attribute09_id', 'wem.attribute09_id', 'wem.attribute09_id', 'Attribute 09', null, 'attribute09_id', 0);
insert into wo_est_forecast_attributes (ATTRIBUTE_NAME, ID_COLUMN, DESC_COLUMN, GROUP_BY_COLUMN, COLUMN_NAME, ID_COLUMN_ACT, DESC_COLUMN_ACT, GROUP_BY_COLUMN_ACT, COLUMN_HEADER, PROJECT_WHERE_CLAUSE, ESTIMATE_WHERE_CLAUSE, ACTIVE)
values ('Attribute 10', 'wem.attribute10_id', 'wem.attribute10_id', 'wem.attribute10_id', 'Attribute 10', 'wem.attribute10_id', 'wem.attribute10_id', 'wem.attribute10_id', 'Attribute 10', null, 'attribute10_id', 0);

update wo_est_forecast_attributes
set column_name = lower(replace(column_name,' ','_'))
where attribute_name like 'Attribute %';

insert into pp_system_control_company (control_id, control_name, control_value,
   description, long_description, company_id)
select nvl(max(control_id),0)+1 control_id, 'WOEST - Show Attributes' control_name, '0,0,0,0,0,0,0,0,0,0' control_value,
   null description, 'Comma-delimited list of binary flags (0/1) indicating whether each of the 10 "Attribute" fields will be displayed for Work Order estimates (i.e., Attribute 01, Attribute 02, ..., Attribute 10). Default is NOT to display any "Attribute" fields (i.e., "0,0,0,0,0,0,0,0,0,0").', -1
from pp_system_control_company
union all
select nvl(max(control_id),0)+2 control_id, 'FPEST - Show Attributes' control_name, '0,0,0,0,0,0,0,0,0,0' control_value,
   null description, 'Comma-delimited list of binary flags (0/1) indicating whether each of the 10 "Attribute" fields will be displayed for Funding Project estimates (i.e., Attribute 01, Attribute 02, ..., Attribute 10). Default is NOT to display any "Attribute" fields (i.e., "0,0,0,0,0,0,0,0,0,0").', -1
from pp_system_control_company
union all
select nvl(max(control_id),0)+3 control_id, 'BDGEST - Show Attributes' control_name, '0,0,0,0,0,0,0,0,0,0' control_value,
   null description, 'Comma-delimited list of binary flags (0/1) indicating whether each of the 10 "Attribute" fields will be displayed for Budget Item estimates (i.e., Attribute 01, Attribute 02, ..., Attribute 10). Default is NOT to display any "Attribute" fields (i.e., "0,0,0,0,0,0,0,0,0,0").', -1
from pp_system_control_company;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2180, 0, 2015, 1, 0, 0, 42193, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042193_pcm_new_est_attributes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;