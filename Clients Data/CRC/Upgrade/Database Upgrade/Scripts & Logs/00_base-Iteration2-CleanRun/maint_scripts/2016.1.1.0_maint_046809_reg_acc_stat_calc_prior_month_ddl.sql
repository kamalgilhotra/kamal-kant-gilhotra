/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_046809_reg_acc_stat_calc_prior_month_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2016.1.1.0 01/01/2017 Johnny Sisouphanh Add flag for prior month calculation for Account Statistics
||============================================================================
*/

alter table REG_ACCT_STAT_FORMULA add PRIOR_MONTH number(22,0);

comment on column REG_ACCT_STAT_FORMULA.PRIOR_MONTH is 'Flag to indicate if prior month will be used in account statistic calculation. 0/null = no 1 = yes';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3359, 0, 2016, 1, 1, 0, 046809, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.1.0_maint_046809_reg_acc_stat_calc_prior_month_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;