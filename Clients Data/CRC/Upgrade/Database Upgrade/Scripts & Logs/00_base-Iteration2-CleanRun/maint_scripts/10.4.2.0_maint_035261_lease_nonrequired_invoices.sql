/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035261_lease_nonrequired_invoices.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 01/27/2014 Kyle Peterson
||============================================================================
*/

alter table LS_LEASE_GROUP add AUTO_GENERATE_INVOICES number(1,0) default 0;

alter table LS_LEASE_GROUP
   add constraint FK_LS_LEASE_GROUP1
       foreign key (AUTO_GENERATE_INVOICES)
       references YES_NO(YES_NO_ID);

comment on column LS_LEASE_GROUP.AUTO_GENERATE_INVOICES is 'A switch that indicates whether or not invoices are automatically created during Month End.';

alter table LS_LEASE_OPTIONS add AUTO_GENERATE_INVOICES number(1,0) default 0;

alter table LS_LEASE_OPTIONS
   add constraint FK_LS_LEASE_OPTIONS2
       foreign key (AUTO_GENERATE_INVOICES)
       references YES_NO(YES_NO_ID);

comment on column LS_LEASE_OPTIONS.AUTO_GENERATE_INVOICES is 'A switch that indicates whether or not invoices are automatically created during Month End.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (913, 0, 10, 4, 2, 0, 35261, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035261_lease_nonrequired_invoices.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;