/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037568_depr_update_pp_processes.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/04/2014 Charlie Shilling changed name of exe
||============================================================================
*/

update PP_PROCESSES
   set EXECUTABLE_FILE = 'ssp_depr_calc.exe'
 where UPPER(trim(DESCRIPTION)) = 'DEPRECIATION CALCULATION';

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
   select '10.4.2.0', 'ssp_depr_calc.exe', '10.4.2.0' from DUAL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1099, 0, 10, 4, 2, 0, 37568, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037568_depr_update_pp_processes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
