/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047226_lease_update_formula_component_lookups_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/25/2017 Jared Watkins    update the import lookups for the new formula component imports
||                                        to filter the valid values (and add a new one for the VC component description)
||============================================================================
*/

--update the IR formula component lookup to only pull valid values for the Index/Rate components
update pp_import_lookup 
  set description = 'Index/Rate Component.Description', 
    long_description = 'The passed in value corresponds to the LS Formula Component: Description field for Index/Rate Formula Components. Translate to the Formula Component ID using the Description column on the LS Formula Component table.', 
    lookup_values_alternate_sql = 'select description from ls_formula_component where ls_component_type_id = 1'
where import_lookup_id = 2504
;

--add new lookup for VC formula components
insert into pp_import_lookup(import_lookup_id, description, 
  long_description, 
  column_name, lookup_sql, 
  is_derived, lookup_table_name, lookup_column_name, lookup_values_alternate_sql)
select 2508, 'Variable Component.Description', 
  'The passed in value corresponds to the LS Formula Component: Description field for Variable Formula Components. Translate to the Formula Component ID using the Description column on the LS Formula Component table.', 
  'formula_component_id', '( select f.formula_component_id from ls_formula_component f where upper( trim( <importfield> ) ) = upper( trim( f.description ) ) )', 
  0, 'ls_formula_component', 'description', 'select description from ls_formula_component where ls_component_type_id = 2'
from dual
where not exists (select 1 
                  from pp_import_lookup 
                  where import_lookup_id = 2508)
;
  
--change the VC import type to use the new VC import lookup for the formula component ID
update pp_import_column_lookup 
  set import_lookup_id = 2508 
where import_type_id = 264 
  and column_name = 'formula_component_id'
;

--update the ILR lookup to only pull valid values for ILRs related to a VC component
update pp_import_lookup 
  set lookup_values_alternate_sql = 
    'select ilr_number from ls_ilr where ilr_id in 
      (select distinct ilr_id from ls_variable_payment_components a
         inner join ls_ilr_payment_term_var_paymnt b
         on a.variable_payment_id = b.variable_payment_id
         where a.formula_component_id in (select formula_component_id from ls_formula_component where ls_component_type_id = 2)
       union
       select distinct ilr_id from ls_variable_component_values)'
where import_lookup_id = 2505
;

--update the LS Asset lookup to only pull valid values for assets related to an ILR related to a VC component
update pp_import_lookup 
  set lookup_values_alternate_sql = 
    'select leased_asset_number from ls_asset where ilr_id in 
      (select distinct ilr_id from ls_variable_payment_components a
         inner join ls_ilr_payment_term_var_paymnt b
         on a.variable_payment_id = b.variable_payment_id
         where a.formula_component_id in (select formula_component_id from ls_formula_component where ls_component_type_id = 2)
       union
       select distinct ilr_id from ls_variable_component_values)'
where import_lookup_id = 2506
;

--update the Lease Company lookup to only pull back lease companies
update pp_import_lookup 
  set lookup_values_alternate_sql =
    'select description from company where is_lease_company = 1'
where import_lookup_id = 2507
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3512, 0, 2017, 1, 0, 0, 47226, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047226_lease_update_formula_component_lookups_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
