/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052933_pwrtax_03_import_config_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 03/28/19   K Powers   Update PARENT_TABLE column setting
||============================================================================
*/

update pp_import_column 
set parent_table = 'cpr_ledger' 
where import_type_id in (212,213,214,224)
and column_name = 'asset_id';

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16422, 0, 2018, 2, 0, 0, 52933, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052933_pwrtax_03_import_config_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

