/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045650_reg_case_alloc_dyn_target_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2016.1.0.0 04/20/2016 Sarah Byers	   Create reg_case_alloc_dyn_target
||============================================================================
*/

create table reg_case_alloc_dyn_target (
reg_case_id number(22,0) not null,
reg_allocator_id number(22,0) not null,
reg_alloc_category_id number(22,0) not null,
reg_alloc_target_id number(22,0) not null,
target_used number(1,0) not null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_case_alloc_dyn_target add (
constraint pk_reg_case_alloc_dyn_target primary key (reg_case_id, reg_allocator_id, reg_alloc_category_id, reg_alloc_target_id)
using index tablespace pwrplant_idx);

alter table reg_case_alloc_dyn_target
add constraint fk_reg_case_alloc_dyn_target1
foreign key (reg_case_id)
references reg_case (reg_case_id);

alter table reg_case_alloc_dyn_target
add constraint fk_reg_case_alloc_dyn_target2
foreign key (reg_allocator_id)
references reg_allocator (reg_allocator_id);

alter table reg_case_alloc_dyn_target
add constraint fk_reg_case_alloc_dyn_target3
foreign key (reg_alloc_category_id)
references reg_alloc_category (reg_alloc_category_id);

alter table reg_case_alloc_dyn_target
add constraint fk_reg_case_alloc_dyn_target4
foreign key (reg_alloc_target_id)
references reg_alloc_target (reg_alloc_target_id);

comment on table reg_case_alloc_dyn_target is 'The Reg Case Alloc Dyn Target Basis Table stores the targets included as statistical values in the calculation of the dynamic allocator factor.';
comment on column reg_case_alloc_dyn_target.reg_case_id is 'System assigned identifier of the Regulatory Case.';
comment on column reg_case_alloc_dyn_target.reg_allocator_id is 'System assigned identifier of the Regulatory Allocator.';
comment on column reg_case_alloc_dyn_target.reg_alloc_category_id is 'System assigned identifier of the Regulatory Allocation Category.';
comment on column reg_case_alloc_dyn_target.reg_alloc_target_id is 'System assigned identifier of the Regulatory Allocation Target.';
comment on column reg_case_alloc_dyn_target.target_used is '1: Target is used as a statistical value for the allocator; 0: Target is not used.';
comment on column reg_case_alloc_dyn_target.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_case_alloc_dyn_target.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

create table reg_jur_alloc_dyn_target (
reg_jur_template_id number(22,0) not null,
reg_allocator_id number(22,0) not null,
reg_alloc_category_id number(22,0) not null,
reg_alloc_target_id number(22,0) not null,
target_used number(1,0) not null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_jur_alloc_dyn_target add (
constraint pk_reg_jur_alloc_dyn_target primary key (reg_jur_template_id, reg_allocator_id, reg_alloc_category_id, reg_alloc_target_id)
using index tablespace pwrplant_idx);

alter table reg_jur_alloc_dyn_target
add constraint fk_reg_jur_alloc_dyn_target1
foreign key (reg_jur_template_id)
references reg_jur_template (reg_jur_template_id);

alter table reg_jur_alloc_dyn_target
add constraint fk_reg_jur_alloc_dyn_target2
foreign key (reg_allocator_id)
references reg_allocator (reg_allocator_id);

alter table reg_jur_alloc_dyn_target
add constraint fk_reg_jur_alloc_dyn_target3
foreign key (reg_alloc_category_id)
references reg_alloc_category (reg_alloc_category_id);

alter table reg_jur_alloc_dyn_target
add constraint fk_reg_jur_alloc_dyn_target4
foreign key (reg_alloc_target_id)
references reg_alloc_target (reg_alloc_target_id);

comment on table reg_jur_alloc_dyn_target is 'The Reg Case Alloc Dyn Target Basis Table stores the targets included as statistical values in the calculation of the dynamic allocator factor.';
comment on column reg_jur_alloc_dyn_target.reg_jur_template_id is 'System assigned identifier of the Regulatory Jurisdictional Template.';
comment on column reg_jur_alloc_dyn_target.reg_allocator_id is 'System assigned identifier of the Regulatory Allocator.';
comment on column reg_jur_alloc_dyn_target.reg_alloc_category_id is 'System assigned identifier of the Regulatory Allocation Category.';
comment on column reg_jur_alloc_dyn_target.reg_alloc_target_id is 'System assigned identifier of the Regulatory Allocation Target.';
comment on column reg_jur_alloc_dyn_target.target_used is '1: Target is used as a statistical value for the allocator; 0: Target is not used.';
comment on column reg_jur_alloc_dyn_target.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column reg_jur_alloc_dyn_target.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- Add target to reg allocator
alter table reg_allocator add (reg_alloc_target_id number(22,0) null);

alter table reg_allocator
add constraint fk_reg_allocator2
foreign key (reg_alloc_target_id)
references reg_alloc_target (reg_alloc_target_id);

comment on column reg_allocator.reg_alloc_target_id is 'System assigned identifier of a Regulatory Allocation Target.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3174, 0, 2016, 1, 0, 0, 45650, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045650_reg_case_alloc_dyn_target_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;