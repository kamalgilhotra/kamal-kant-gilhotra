/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047990_lessor_01_add_column_current_revision_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/14/2017 Anand R          Add column current_revsion to lsr_lease
||============================================================================
*/

alter table lsr_lease
 add 
 ( current_revision number(22,0)
 );
 
comment on column lsr_lease.current_revision is 'The most recent approved revision for this MLA';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3711, 0, 2017, 1, 0, 0, 47990, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047990_lessor_01_add_column_current_revision_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
