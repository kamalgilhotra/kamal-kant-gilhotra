 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_041928_sys_wo_intfc_stg_tbl_mt_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 01/13/2014 D. Mendel     Remove column for table maint that was previously removed from table
 ||============================================================================
 */ 

delete from powerplant_columns where lower(table_name) = 'wo_interface_staging' and lower(column_name) = 'approval_type_id';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2157, 0, 2015, 1, 0, 0, 41928, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041928_sys_wo_intfc_stg_tbl_mt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;