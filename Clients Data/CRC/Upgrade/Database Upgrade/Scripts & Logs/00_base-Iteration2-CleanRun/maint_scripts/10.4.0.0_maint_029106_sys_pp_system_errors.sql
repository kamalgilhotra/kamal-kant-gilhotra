/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029106_sys_pp_system_errors.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- --------------   ------------------------------------
|| 10.4.0.0   01/22/2013 Charlie Shilling Point Release
||============================================================================
*/

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1015',
    'POST 1015: ERROR: Could not find matching and approved UTRF for this UTRT.',
    'POST 1015: ERROR: Could not find matching and approved UTRF for this UTRT.',
    'POST 1015: Make sure that the associated UTRF transaction exists and has been approved.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1016',
    'POST 1016: ERROR: Could not find a basis to apply rounding error to.',
    'POST 1016: ERROR: Could not find a basis to apply rounding error to. ',
    'POST 1016: A rounding error that could not be handled was introduced when calculating the basis amounts. Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1017',
    'POST 1017: ERROR: There were not enough bases to apply the rounding error to.',
    'POST 1017: ERROR: There were not enough bases to apply the rounding error to.',
    'POST 1017: A rounding error that could not be handled was introduced when calculating the basis amounts. Call PPC');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1018',
    'POST 1018: ERROR: The difference between the original total and the rounded total is greater than the accepted tolerance.',
    'POST 1018: ERROR: The difference between the original total and the rounded total is greater than the accepted tolerance.',
    'POST 1018: A rounding error that could not be handled was introduced when calculating the basis amounts. Call PPC');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (277, 0, 10, 4, 0, 0, 29106, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029106_sys_pp_system_errors.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
