/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_041635_lease_component_charge.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 01/09/2015 	B.Beck    	 		Adding a component_charge_table
||==========================================================================================
*/

--Drop interim interest start date from ls_component
alter table ls_component drop column interim_interest_start_date;

--Create table
drop table ls_component_charge;

create table ls_component_charge
(
	id number(22,0),
	component_id number(22, 0), 
	interim_interest_start_date date, 
	invoice_date date, 
	invoice_number varchar2(35), 
	amount number(22, 2), 
	user_id varchar2(18), 
	time_stamp date
);

alter table ls_component_charge
add CONSTRAINT pk_ls_component_charge
PRIMARY KEY (id);

alter table ls_component_charge
add CONSTRAINT fk_ls_component
FOREIGN KEY (component_id)
REFERENCES ls_component (component_id);

comment on table ls_component_charge is 'This table holds components that have received invoices for payment';

comment on column ls_component_charge.id is 'An internal id';
comment on column ls_component_charge.component_id is 'The component that received the invoice';
comment on column ls_component_charge.interim_interest_start_date is 'The month the component can start receiving interim interest';
comment on column ls_component_charge.invoice_date is 'The date the invoice is received';
comment on column ls_component_charge.invoice_number is 'The invoice number';
comment on column ls_component_charge.amount is 'The amount being invoiced';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2154, 0, 10, 4, 3, 2, 041635, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041635_lease_component_charge.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;