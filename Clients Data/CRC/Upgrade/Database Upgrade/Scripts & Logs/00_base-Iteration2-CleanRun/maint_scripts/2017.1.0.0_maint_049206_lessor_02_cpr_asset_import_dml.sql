/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049206_lessor_02_cpr_asset_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/26/2017 Shane "C" Ward		Set up Lessor CPR Asset Import Tables
||============================================================================
*/

--New Import Type
INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name,
             archive_additional_columns)
VALUES      (503,
             'Add: Lessor CPR Assets',
             'Lessor CPR Assets',
             'lsr_import_cpr_asset',
             'lsr_import_cpr_asset_archive',
             1,
             'nvo_lsr_logic_import',
             null);

--Associate new Import Type
INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_type_id,
             import_subsystem_id)
VALUES      (503,
             14);

--Insert all the columns for the import type
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'ilr_id',
             NULL,
             NULL,
             'ILR ID',
             'ilr_id_xlate',
             1,
             1,
             'number(22,0)',
             'lsr_ilr',
             NULL,
             1,
             NULL,
             'ilr_id',
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'revision',
             NULL,
             NULL,
             'Revision',
             null,
             0,
             1,
             'number(22,0)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'cpr_asset_id',
             NULL,
             NULL,
             'CPR Asset ID',
             'cpr_asset_id_xlate',
             1,
             1,
             'number(22,0)',
             null,
             NULL,
             1,
             NULL,
             null,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'estimated_residual_pct',
             NULL,
             NULL,
             'Estimated Residual Percent',
             null,
             1,
             1,
             'number(22,8)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'estimated_residual',
             NULL,
             NULL,
             'Estimated Residual Amount',
             null,
             1,
             1,
             'number(22,2)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'fair_market_value',
             NULL,
             NULL,
             'Fair Market Value',
             null,
             1,
             1,
             'number(22,2)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'guaranteed_residual',
             NULL,
             NULL,
             'Guaranteed Residual Amount',
             null,
             1,
             1,
             'number(22,2)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'expected_life',
             NULL,
             NULL,
             'Expected Life',
             null,
             1,
             1,
             'number(22,0)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 503,
             'economic_life',
             NULL,
             NULL,
             'Economic Life',
             null,
             1,
             1,
             'number(22,0)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

--New Lookups
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1091,
             'Lessor ILR Description',
             'Lessor ILR Description',
             'ilr_id',
             '( select b.ilr_id from lsr_ilr b where upper(trim( <importfield>)) = upper(trim( b.description )))',
             0,
             'lsr_ilr',
             'description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1092,
             'Lessor ILR Long Description',
             'Lessor ILR Long Description',
             'ilr_id',
             '( select b.ilr_id from lsr_ilr b where upper(trim( <importfield>)) = upper(trim( b.long_description )))',
             0,
             'lsr_ilr',
             'long_description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1093,
             'CPR Ledger.Description',
             'NO VALID VALUES LIST The passed in value corresponds to the CPR Ledger: Description field.  Translate to the Asset ID using the Description column on the CPR Ledger table.',
             'asset_id',
             '( select cpr.asset_id from cpr_ledger cpr where upper( trim( <importfield> ) ) = upper( trim( cpr.description ) ) )',
             0,
             null,
             null);

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1094,
             'CPR Ledger.Long Description',
             'NO VALID VALUES LIST The passed in value corresponds to the CPR Ledger: Long Description field.  Translate to the Asset ID using the Long Description column on the CPR Ledger table.',
             'asset_id',
             '( select cpr.asset_id from cpr_ledger cpr where upper( trim( <importfield> ) ) = upper( trim( cpr.long_description ) ) )',
             0,
             null,
             null);

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1095,
             'CPR Ledger.Serial Number',
             'NO VALID VALUES LIST The passed in value corresponds to the CPR Ledger: Serial Number field.  Translate to the Asset ID using the Serial Number column on the CPR Ledger table.',
             'asset_id',
             '( select cpr.asset_id from cpr_ledger cpr where upper( trim( <importfield> ) ) = upper( trim( cpr.serial_number ) ) )',
             0,
             null,
             null);

--Column Lookups
INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (503, 1093, 'cpr_asset_id');

INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (503, 1094, 'cpr_asset_id');

INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (503, 1095, 'cpr_asset_id');

INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (503, 1090, 'ilr_id');

INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (503, 1091, 'ilr_id');

INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (503, 1092, 'ilr_id');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3846, 0, 2017, 1, 0, 0, 49206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049206_lessor_02_cpr_asset_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;