/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045505_reg_acct_rollup_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 03/03/2016 Shane Ward  	  Make Account Rollup WS available
||============================================================================
*/

INSERT INTO ppbase_menu_items (MODULE, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier,workspace_identifier, enable_yn)
VALUES
('REG', 'ACCT_ROLLUP', 3, 5, 'Account Rollups', 'Maintain Account Rollups', 'ACCT_SETUP', 'uo_reg_acct_rollup_ws', 1);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3088, 0, 2016, 1, 0, 0, 045505, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045505_reg_acct_rollup_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;