/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034532_depr_PP_DEPR_PKG.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   10/09/2013 Brandon Beck   Point Release
||============================================================================
*/

create or replace package PP_DEPR_PKG
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PP_DEPR_PKG
|| Description: Depr functions and procedures for PowerPlant application.
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0     07/18/2011 David Liss     Create
|| 2.0     04/03/2013 Brandon Beck   Adding Depreciation Calculation Functions
|| 3.0     10/08/13   Brandon Beck   Add Over Depr Check
||============================================================================
*/
 as


   procedure P_LEASEDEPRCALC(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                             A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE);

   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS         PKG_PP_COMMON.DATE_TABTYPE,
                                 A_LOAD_RECURRING number);

   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH          date,
                                 A_LOAD_RECURRING number);

   procedure P_STAGEMONTHENDDEPR(A_MONTHS PKG_PP_COMMON.DATE_TABTYPE);

   procedure P_STAGEMONTHENDDEPR(A_MONTH date);

/*
   procedure P_FCSTDEPRSTAGE(A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                      A_MONTH date);

   procedure P_FCSTDEPRCALC(A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                      A_MONTH      date);

   procedure P_FCSTDEPRCALC(A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                      A_MONTHS     PKG_PP_COMMON.DATE_TABTYPE);
*/
   function F_PREPDEPRCALC return number;

   function F_OVERDEPR_SINGLE_ROW ( A_END_BALANCE     DEPR_LEDGER.END_BALANCE%TYPE,
                         A_END_RESERVE       DEPR_LEDGER.END_RESERVE%TYPE,
                         A_EXPENSE        DEPR_LEDGER.DEPRECIATION_EXPENSE%TYPE,
                         A_NET_SALVAGE_PCT   DEPR_METHOD_RATES.NET_SALVAGE_PCT%TYPE)
      return NUMBER;

   /*
   *  A Wrapper for the f_overdepr(params) function.  This executed over depr adjustment for all rows in
   *  depr_calc_stg
   */
   function f_overdepr return number;

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_find_depr_group
   || Description: Finds the depr group under the depr group control lookup.
   || None of the parameters can be null before entering the function.
   || Make sure you check sqlca.sqlcode when calling this function.
   || Return Values: -1 - No data found
   ||             dg_id - Valid depr group ID
   ||                -9 - Invalid depr group
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     07/18/2011 David Liss     Create
   || 2.0     04/03/2013 Brandon Beck   Adding Depreciation Calculation Functions
   ||============================================================================
   */
   function F_FIND_DEPR_GROUP(A_COMPANY_ID         number,
                              A_GL_ACCOUNT_ID      number,
                              A_MAJOR_LOCATION_ID  number,
                              A_UTILITY_ACCOUNT_ID number,
                              A_BUS_SEGMENT_ID     number,
                              A_SUB_ACCOUNT_ID     number,
                              A_SUBLEDGER_TYPE_ID  number,
                              A_BOOK_VINTAGE       number,
                              A_ASSET_LOCATION_ID  number,
                              A_LOCATION_TYPE_ID   number,
                              A_RETIREMENT_UNIT_ID number,
                              A_PROPERTY_UNIT_ID   number,
                              A_CLASS_CODE_ID      number,
                              A_CC_VALUE           varchar2)

    return number;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_get_dg_cc_id
   || Description: Finds the depr group class code.
   || Make sure you check sqlca.sqlcode when calling this function.
   || Return Values: 0 - No data found
   ||                dg_cc_id - DEPR GROUP CONTROL class code id
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     08/12/2011 David Liss     Create
   ||============================================================================
   */
   function F_GET_DG_CC_ID return number;

   --Check to validate depr group status
   procedure P_DG_STATUS_CHECK(A_ENABLE boolean);

   --Check to validate depr group business segment
   procedure P_CHECK_DG_BS;

   G_DG_STATUS_CHECK boolean := true;
   G_CHECK_DG_BS     varchar2(10);

end PP_DEPR_PKG;
/


create or replace package body PP_DEPR_PKG as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --       VARIABLES
   --**************************************************************
   G_IGNOREINACTIVEYES PKG_PP_COMMON.NUM_TABTYPE;
   G_IGNOREINACTIVENO  PKG_PP_COMMON.NUM_TABTYPE;

   G_VERSION_ID         FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%type;
   G_MONTHS          PKG_PP_COMMON.DATE_TABTYPE;

   --**************************************************************
   --       PROCEDURES
   --**************************************************************



   --**************************************************************************
   --                            P_LEASEDEPRCALC
   --**************************************************************************
   -- Depr Calc for Lease
   procedure P_LEASEDEPRCALC(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
                             A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE) is
   begin
      PKG_PP_CPR_DEPR.P_STARTMONTHENDDEPR(A_COMPANY_IDS, A_MONTHS, -100);
   end P_LEASEDEPRCALC;

   --**************************************************************************
   --                            P_SET_FISCALYEARSTART
   --**************************************************************************
   -- Sets the beginning of the fiscal year
   procedure P_SET_FISCALYEARSTART is

   begin
      update DEPR_CALC_STG STG
         set FISCALYEARSTART =
              (select FY.MONTH
                 from PP_CALENDAR C, PP_CALENDAR FY
                where C.MONTH = STG.GL_POST_MO_YR
                  and C.FISCAL_YEAR = FY.FISCAL_YEAR
                  and FY.FISCAL_MONTH = 1)
       where STG.DEPR_CALC_STATUS = 1;

   end P_SET_FISCALYEARSTART;

   --**************************************************************************
   --                            P_CHECKINACTP_CORSALVAMORTVEDEPR
   --**************************************************************************
   --
   -- Get COR and SALV amort
   -- PREREQ: Staging table must be filled in
   --
   -- this function is not currently being used.
   procedure P_CORSALVAMORT is

   begin
      update DEPR_CALC_STG Z
         set (DNSA_COR_BAL, DNSA_SALV_BAL) =
              (select sum(COST_OF_REMOVAL_BAL - COST_OF_REMOVAL_RESERVE),
                      sum(SALVAGE_BAL - SALVAGE_RESERVE)
                 from DEPR_NET_SALVAGE_AMORT D
                where D.DEPR_GROUP_ID = Z.DEPR_GROUP_ID
                  and D.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                  and D.GL_POST_MO_YR = ADD_MONTHS(Z.GL_POST_MO_YR, -1))
       where COR_TREATMENT + SALVAGE_TREATMENT > 0
         and DEPR_CALC_STATUS = 1;

      update DEPR_CALC_STG Z
         set (COR_YTD, SALV_YTD) =
              (select sum(L.COST_OF_REMOVAL), sum(L.SALVAGE_CASH - L.SALVAGE_RETURNS)
                 from DEPR_LEDGER L
                where L.DEPR_GROUP_ID = Z.DEPR_GROUP_ID
                  and L.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                  and L.GL_POST_MO_YR between Z.FISCALYEARSTART and ADD_MONTHS(Z.GL_POST_MO_YR, -1))
       where COR_TREATMENT + SALVAGE_TREATMENT > 0
         and DEPR_CALC_STATUS = 1;

   end P_CORSALVAMORT;

   --**************************************************************************
   --                            P_CHECKINACTIVEDEPR
   --**************************************************************************
   --
   -- Set the companies that are ignoring inactive status
   --
   procedure P_CHECKINACTIVEDEPR(A_COMPANY_ID number,
                                 A_INDEX      number) is

   begin
      if LOWER(trim(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',
                                                                          A_COMPANY_ID),
                        'no'))) = 'no' then
         G_IGNOREINACTIVENO(A_INDEX) := A_COMPANY_ID;
      else
         G_IGNOREINACTIVEYES(A_INDEX) := A_COMPANY_ID;
      end if;
   end P_CHECKINACTIVEDEPR;

   --**************************************************************************
   --                            P_MISSINGSETSOFBOOKS
   --**************************************************************************
   --
   -- Checks for missing sets of books.
   -- Set the depr calc status to be -1 if a set of books is missing for the company
   -- PREREQ: Staging Table must be filled in
   --
   procedure P_MISSINGSETSOFBOOKS is

   begin
      update DEPR_CALC_STG Z
         set DEPR_CALC_STATUS = -1, DEPR_CALC_MESSAGE = 'Missing Sets of Books in Depr Ledger'
       where COMPANY_ID in (select YY.COMPANY_ID
                              from (select CS.COMPANY_ID, CS.SET_OF_BOOKS_ID
                                      from COMPANY_SET_OF_BOOKS CS
                                    minus
                                    select ZZ.COMPANY_ID, ZZ.SET_OF_BOOKS_ID
                                      from DEPR_CALC_STG ZZ) YY);


   end P_MISSINGSETSOFBOOKS;

   procedure P_AMORT_STAGE (A_MONTH date) is
   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the amortization staging table for month:' ||
                           TO_CHAR(A_MONTH, 'YYYYMM'));

      forall I in indices of G_IGNOREINACTIVENO
         insert into DEPR_CALC_AMORT_STG (
            DEPR_GROUP_ID,
            SET_OF_BOOKS_ID,
            GL_POST_MO_YR,
            VINTAGE,
            COR_TREATMENT,
            COR_LIFE,
            PRIOR_COR_BALANCE,
            PRIOR_COR_RESERVE,
            COR_BALANCE,
            COR_AMORT,
            COR_RESERVE,
            SALV_TREATMENT,
            SALV_LIFE,
            PRIOR_SALV_BALANCE,
            PRIOR_SALV_RESERVE,
            SALV_BALANCE,
            SALV_AMORT,
            SALV_RESERVE)
         select DNSA.DEPR_GROUP_ID,
            DNSA.SET_OF_BOOKS_ID,
            DNSA.GL_POST_MO_YR,
            DNSA.VINTAGE,
            DMR.COR_TREATMENT,
            DMR.NET_SALVAGE_AMORT_LIFE,
            NVL(DNSB.COST_OF_REMOVAL_BAL, 0),
            NVL(DNSB.COST_OF_REMOVAL_RESERVE, 0),
            DNSA.COST_OF_REMOVAL_BAL,
            0 AS COR_AMORT,
            DNSA.COST_OF_REMOVAL_RESERVE,
            DMR.SALVAGE_TREATMENT,
            DMR.NET_SALVAGE_AMORT_LIFE,
            NVL(DNSB.SALVAGE_BAL, 0),
            NVL(DNSB.SALVAGE_RESERVE, 0),
            DNSA.SALVAGE_BAL,
            0 AS SALV_AMORT,
            DNSA.SALVAGE_RESERVE
         from DEPR_NET_SALVAGE_AMORT DNSA, DEPR_METHOD_RATES DMR, DEPR_GROUP DG, DEPR_NET_SALVAGE_AMORT DNSB
         where DG.DEPR_GROUP_ID = DNSA.DEPR_GROUP_ID
         and DNSA.DEPR_GROUP_ID = DNSB.DEPR_GROUP_ID (+)
         and DNSA.SET_OF_BOOKS_ID = DNSB.SET_OF_BOOKS_ID (+)
         and DNSA.VINTAGE = DNSB.VINTAGE (+)
         and DNSB.GL_POST_MO_YR (+) = ADD_MONTHS(A_MONTH, -1)
         and DG.DEPR_METHOD_ID = DMR.DEPR_METHOD_ID
         and DNSA.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
         and DMR.EFFECTIVE_DATE = (
            select MAX(EFFECTIVE_DATE)
            from DEPR_METHOD_RATES DMR1
            where DMR1.DEPR_METHOD_ID = DMR.DEPR_METHOD_ID
            and DMR1.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
            and DMR1.EFFECTIVE_DATE <= A_MONTH)
         and DNSA.GL_POST_MO_YR = A_MONTH
         and (LOWER(TRIM(DMR.COR_TREATMENT)) <> 'no' or LOWER(TRIM(DMR.SALVAGE_TREATMENT)) <> 'no')
         and DG.COMPANY_ID = G_IGNOREINACTIVENO(I);

      forall I in indices of G_IGNOREINACTIVEYES
         insert into DEPR_CALC_AMORT_STG (
            DEPR_GROUP_ID,
            SET_OF_BOOKS_ID,
            GL_POST_MO_YR,
            VINTAGE,
            COR_TREATMENT,
            COR_LIFE,
            PRIOR_COR_BALANCE,
            PRIOR_COR_RESERVE,
            COR_BALANCE,
            COR_AMORT,
            COR_RESERVE,
            SALV_TREATMENT,
            SALV_LIFE,
            PRIOR_SALV_BALANCE,
            PRIOR_SALV_RESERVE,
            SALV_BALANCE,
            SALV_AMORT,
            SALV_RESERVE)
         select DNSA.DEPR_GROUP_ID,
            DNSA.SET_OF_BOOKS_ID,
            DNSA.GL_POST_MO_YR,
            DNSA.VINTAGE,
            DMR.COR_TREATMENT,
            DMR.NET_SALVAGE_AMORT_LIFE,
            NVL(DNSB.COST_OF_REMOVAL_BAL, 0),
            NVL(DNSB.COST_OF_REMOVAL_RESERVE, 0),
            DNSA.COST_OF_REMOVAL_BAL,
            0 AS COR_AMORT,
            DNSA.COST_OF_REMOVAL_RESERVE,
            DMR.SALVAGE_TREATMENT,
            DMR.NET_SALVAGE_AMORT_LIFE,
            NVL(DNSB.SALVAGE_BAL, 0),
            NVL(DNSB.SALVAGE_RESERVE, 0),
            DNSA.SALVAGE_BAL,
            0 AS SALV_AMORT,
            DNSA.SALVAGE_RESERVE
         from DEPR_NET_SALVAGE_AMORT DNSA, DEPR_METHOD_RATES DMR, DEPR_GROUP DG, DEPR_NET_SALVAGE_AMORT DNSB, DEPR_LEDGER DL
         where DG.DEPR_GROUP_ID = DNSA.DEPR_GROUP_ID
         AND DNSA.DEPR_GROUP_ID = DL.DEPR_GROUP_ID
         AND DNSA.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
         AND DNSA.GL_POST_MO_YR = DL.GL_POST_MO_YR
         and DNSA.DEPR_GROUP_ID = DNSB.DEPR_GROUP_ID (+)
         and DNSA.SET_OF_BOOKS_ID = DNSB.SET_OF_BOOKS_ID (+)
         and DNSA.VINTAGE = DNSB.VINTAGE (+)
         and DNSB.GL_POST_MO_YR (+) = ADD_MONTHS(A_MONTH, -1)
         and DG.DEPR_METHOD_ID = DMR.DEPR_METHOD_ID
         and DNSA.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
         and DMR.EFFECTIVE_DATE = (
            select MAX(EFFECTIVE_DATE)
            from DEPR_METHOD_RATES DMR1
            where DMR1.DEPR_METHOD_ID = DMR.DEPR_METHOD_ID
            and DMR1.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
            and DMR1.EFFECTIVE_DATE <= A_MONTH)
         and DNSA.GL_POST_MO_YR = A_MONTH
         and (LOWER(TRIM(DMR.COR_TREATMENT)) <> 'no' or LOWER(TRIM(DMR.SALVAGE_TREATMENT)) <> 'no')
         and DG.COMPANY_ID = G_IGNOREINACTIVEYES(I)
         and (NVL(DG.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
            DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
            DL.COR_BEG_RESERVE <> 0);
   end P_AMORT_STAGE;

   --**************************************************************************
   --                            P_STARTMONTHENDDEPR
   --**************************************************************************
   --
   -- WRAPPER for the start depr calc to allow single month depr calculations
   --
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH          date,
                                 A_LOAD_RECURRING number) is

      MY_MONTHS PKG_PP_COMMON.DATE_TABTYPE;

   begin
      MY_MONTHS(1) := A_MONTH;
      P_STARTMONTHENDDEPR(A_COMPANY_IDS, MY_MONTHS, A_LOAD_RECURRING);
   end P_STARTMONTHENDDEPR;

   --**************************************************************************
   --                            P_STARTMONTHENDDEPR
   --**************************************************************************
   --
   --  Start a depr calc for an array of companies and an array of months
   -- Start Logging
   -- Load Global Temp Staging table
   -- Calculate Depreciation
   -- Backfill results to depr ledger
   -- End logging
   --
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS         PKG_PP_COMMON.DATE_TABTYPE,
                                 A_LOAD_RECURRING number) is
      MY_RET number;
      MY_STR varchar2(2000);
   begin
      --
      -- START THE LOGGING and log what companies and months are being processed
      --
      --      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_PP_DEPR_COMMON.F_GETDEPRPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('PROCESSING the following company_ids:');

      --
      -- LOOP over the companies and prep the company for processing
      -- Get company system controls to determine whether to include inactive depr groups
      --
      for I in 1 .. A_COMPANY_IDS.COUNT
      loop
         PKG_PP_LOG.P_WRITE_MESSAGE('   ' || TO_CHAR(A_COMPANY_IDS(I)));
         -- load the arrays for companies ignoring inactive depr groups
         P_CHECKINACTIVEDEPR(A_COMPANY_IDS(I), I);
      end loop;

      if A_LOAD_RECURRING = 1 then
         if PKG_PP_DEPR_ACTIVITY.F_RECURRINGACTIVITY(A_COMPANY_IDS, A_MONTHS, MY_STR) = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(MY_STR);
            return;
         end if;
      end if;

      P_STAGEMONTHENDDEPR(A_MONTHS);

      -- start depreciation calculation
      if F_PREPDEPRCALC() = -1 then
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR During Depreciation Calculation');
      end if;

      --
      --  END LOGGING
      --
      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');
      --      PKG_PP_LOG.P_END_LOG();


   end P_STARTMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  WRAPPER for the prep function to pass in an array of months.
   --
   procedure P_STAGEMONTHENDDEPR(A_MONTHS PKG_PP_COMMON.DATE_TABTYPE) is

   begin
      for I in A_MONTHS.FIRST .. A_MONTHS.LAST
      loop
         P_STAGEMONTHENDDEPR(A_MONTHS(I));
       P_AMORT_STAGE(A_MONTHS(I));
      end loop;
   end P_STAGEMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation.
   -- THE Load is based on depr ledger for a passed in array of company ids.
   -- AND a single month
   --
   procedure P_STAGEMONTHENDDEPR(A_MONTH date) is

   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the calculation staging table for month:' ||
                                 TO_CHAR(A_MONTH, 'yyyymm'));

      forall I in indices of G_IGNOREINACTIVENO
         insert into DEPR_CALC_STG
            (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
             DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, FISCALYEAROFFSET, SMOOTH_CURVE,
             DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
             DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS,
             TRUE_UP_CPR_DEPR, RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
             END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
             COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
             SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
             BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
             RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
             RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
             ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
             END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
             DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
             SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
             GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
             CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
             IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
             COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
             COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
             COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
             RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
             SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
             RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
             IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
             ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
             ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
          ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE, impairment_asset_activity_salv, impairment_asset_begin_balance)
            select DL.DEPR_GROUP_ID, --  depr_group_id                   NULL,
                   DL.SET_OF_BOOKS_ID, --  set_of_books_id              NUMBER(22,0)   NULL,
                   DL.GL_POST_MO_YR, --  gl_post_mo_yr                DATE           NULL,
                   A_MONTH,   --  calc_month                   DATE           NULL,
                   1, --  depr_calc_status             NUMBER(2,0)    NULL,
                   '', --  depr_calc_message            VARCHAR2(2000) NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                               DG.COMPANY_ID),
                             'no')), --  trf_in_est_adds              VARCHAR2(35)   NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                                               DG.COMPANY_ID),
                             'no')), --  include_rwip_in_net          VARCHAR2(35)   NULL,
                   TO_NUMBER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('FISCAL YEAR OFFSET',
                                                                                   DG.COMPANY_ID),
                                 '0')), --  fiscalyearoffset             NUMBER(2,0)    NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
                                                                               DG.COMPANY_ID),
                             '0')), -- smooth_curve
                   0, --  dnsa_cor_bal                 NUMBER(22,2)   NULL,
                   0, --  dnsa_salv_bal                NUMBER(22,2)   NULL,
                   0, --  cor_ytd                      NUMBER(22,2)   NULL,
                   0, --  salv_ytd                     NUMBER(22,2)   NULL,
                   DG.COMPANY_ID, --  company_id                   NUMBER(22,0)   NULL,
                   DG.SUBLEDGER_TYPE_ID, --  subledger_type_id            NUMBER(22,0)   NULL,
                   DG.DESCRIPTION, --  description                  VARCHAR2(254)  NULL,
                   DG.DEPR_METHOD_ID, --  depr_method_id               NUMBER(22,0)   NULL,
                   DG.MID_PERIOD_CONV, --  mid_period_conv              NUMBER(22,2)   NULL,
                   DG.MID_PERIOD_METHOD, --  mid_period_method            VARCHAR2(35)   NULL,
                   DG.EST_ANN_NET_ADDS, --  dg_est_ann_net_adds          NUMBER(22,8)   NULL,
                   DG.TRUE_UP_CPR_DEPR, --  true_up_cpr_depr             NUMBER(22,0)   NULL,
                   DMR.RATE, --  rate                         NUMBER(22,8)   NULL,
                   DMR.NET_GROSS, --  net_gross                    NUMBER(22,0)   NULL,
                   NVL(DMR.OVER_DEPR_CHECK, 0), --  over_depr_check              NUMBER(22,0)   NULL,
                   DMR.NET_SALVAGE_PCT, --  net_salvage_pct              NUMBER(22,8)   NULL,
                   NVL(DMR.RATE_USED_CODE, 0), --  rate_used_code               NUMBER(22,0)   NULL,
                   DMR.END_OF_LIFE, --  end_of_life                  NUMBER(22,0)   NULL,
                   NVL(DMR.EXPECTED_AVERAGE_LIFE, 0), --  expected_average_life        NUMBER(22,0)   NULL,
                   NVL(DMR.RESERVE_RATIO_ID, 0), --  reserve_ratio_id             NUMBER(22,0)   NULL,
                   DMR.COST_OF_REMOVAL_RATE, --  dmr_cost_of_removal_rate     NUMBER(22,8)   NULL,
                   DMR.COST_OF_REMOVAL_PCT, --  cost_of_removal_pct          NUMBER(22,8)   NULL,
                   DMR.SALVAGE_RATE, --  dmr_salvage_rate             NUMBER(22,8)   NULL,
                   DMR.INTEREST_RATE, --  interest_rate                NUMBER(22,8)   NULL,
                   NVL(DMR.AMORTIZABLE_LIFE, 0), --  amortizable_life             NUMBER(22,0)   NULL,
                   DECODE(TRIM(LOWER(NVL(DMR.COR_TREATMENT, 'no'))), 'no', 0, 1), --  cor_treatment                NUMBER(1,0)    NULL,
                   DECODE(TRIM(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no'))), 'no', 0, 1), --  salvage_treatment            NUMBER(1,0)    NULL,
                   NVL(DMR.NET_SALVAGE_AMORT_LIFE, 0), --  net_salvage_amort_life       NUMBER(22,0)   NULL,
                   DMR.ALLOCATION_PROCEDURE, --  allocation_procedure         VARCHAR2(5)    NULL,
                   DL.DEPR_LEDGER_STATUS, --  depr_ledger_status           NUMBER(22,0)   NULL,
                   DL.BEGIN_RESERVE, --  begin_reserve                NUMBER(22,2)   NULL,
                   0 AS END_RESERVE, --  end_reserve                  NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_PROVISION, --  reserve_bal_provision        NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_COR, --  reserve_bal_cor              NUMBER(22,2)   NULL,
                   DL.SALVAGE_BALANCE, --  salvage_balance              NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_ADJUST, --  reserve_bal_adjust           NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements      NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in          NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out         NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_OTHER_CREDITS, --  reserve_bal_other_credits    NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss        NUMBER(22,2)   NULL,
                   DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor         NUMBER(22,2)   NULL,
                   DL.BEGIN_BALANCE, --  begin_balance                NUMBER(22,2)   NULL,
                   DL.ADDITIONS, --  additions
                   DL.RETIREMENTS, --  retirements                  NUMBER(22,2)   NULL,
                   DL.TRANSFERS_IN, --  transfers_in                 NUMBER(22,2)   NULL,
                   DL.TRANSFERS_OUT, --  transfers_out                NUMBER(22,2)   NULL,
                   DL.ADJUSTMENTS, --  adjustments                  NUMBER(22,2)   NULL,
                   0 AS DEPRECIATION_BASE, --  depreciation_base            NUMBER(22,2)
                   0 AS END_BALANCE, --  end_balance                  NUMBER(22,2)   NULL
                   0 AS DEPRECIATION_RATE, --  depreciation_rate            NUMBER(22,8)   NULL,
                   0 AS DEPRECIATION_EXPENSE, --  depreciation_expense         NUMBER(22,2)   NULL,
                   DL.DEPR_EXP_ADJUST, --  depr_exp_adjust              NUMBER(22,2)   NULL,
                   0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust        NUMBER(22,2)   NULL,
                   DL.COST_OF_REMOVAL, --  cost_of_removal              NUMBER(22,2)   NULL,
                   DL.RESERVE_RETIREMENTS, --  reserve_retirements          NUMBER(22,2)   NULL,
                   DL.SALVAGE_RETURNS, --  salvage_returns              NUMBER(22,2)   NULL,
                   DL.SALVAGE_CASH, --  salvage_cash                 NUMBER(22,2)   NULL,
                   DL.RESERVE_CREDITS, --  reserve_credits              NUMBER(22,2)   NULL,
                   DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments          NUMBER(22,2)   NULL,
                   DL.RESERVE_TRAN_IN, --  reserve_tran_in              NUMBER(22,2)   NULL,
                   DL.RESERVE_TRAN_OUT, --  reserve_tran_out             NUMBER(22,2)   NULL,
                   DL.GAIN_LOSS, --  gain_loss                    NUMBER(22,2)   NULL,
                   DL.VINTAGE_NET_SALVAGE_AMORT, --  vintage_net_salvage_amort    NUMBER(22,2)   NULL,
                   DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
                   DL.CURRENT_NET_SALVAGE_AMORT, --  current_net_salvage_amort    NUMBER(22,2)   NULL,
                   DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg       NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act       NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end       NUMBER(22,2)   NULL,
                   DL.EST_ANN_NET_ADDS, --  est_ann_net_adds             NUMBER(22,2)   NULL,
                   DL.RWIP_ALLOCATION, --  rwip_allocation              NUMBER(22,2)   NULL,
                   DL.COR_BEG_RESERVE, --  cor_beg_reserve              NUMBER(22,2)   NULL,
                   0 AS COR_EXPENSE, --  cor_expense                  NUMBER(22,2)   NULL,
                   NVL(DL.COR_EXP_ADJUST,0), --  cor_exp_adjust               NUMBER(22,2)   NULL,
                   0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust         NUMBER(22,2)   NULL,
                   DL.COR_RES_TRAN_IN, --  cor_res_tran_in              NUMBER(22,2)   NULL,
                   DL.COR_RES_TRAN_OUT, --  cor_res_tran_out             NUMBER(22,2)   NULL,
                   DL.COR_RES_ADJUST, --  cor_res_adjust               NUMBER(22,2)   NULL,
                   0 AS COR_END_RESERVE, --  cor_end_reserve              NUMBER(22,2)   NULL,
                   DL.COST_OF_REMOVAL_RATE, --  cost_of_removal_rate         NUMBER(22,8)   NULL,
                   DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base         NUMBER(22,2)   NULL,
                   DL.RWIP_COST_OF_REMOVAL, --  rwip_cost_of_removal         NUMBER(22,2)   NULL,
                   DL.RWIP_SALVAGE_CASH, --  rwip_salvage_cash            NUMBER(22,2)   NULL,
                   DL.RWIP_SALVAGE_RETURNS, --  rwip_salvage_returns         NUMBER(22,2)   NULL,
                   DL.RWIP_RESERVE_CREDITS, --  rwip_reserve_credits         NUMBER(22,2)   NULL,
                   DL.SALVAGE_RATE, --  salvage_rate                 NUMBER(22,8)   NULL,
                   0 AS SALVAGE_BASE, --  salvage_base                 NUMBER(22,2)   NULL,
                   0 AS SALVAGE_EXPENSE, --  salvage_expense              NUMBER(22,2)   NULL,
                   DL.SALVAGE_EXP_ADJUST, --  salvage_exp_adjust           NUMBER(22,2)   NULL,
                   0 AS SALVAGE_EXP_ALLOC_ADJUST, --  salvage_exp_alloc_adjust     NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_SALVAGE_EXP, --  reserve_bal_salvage_exp      NUMBER(22,2)   NULL,
                   DL.RESERVE_BLENDING_ADJUSTMENT, --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
                   DL.RESERVE_BLENDING_TRANSFER, --  reserve_blending_transfer    NUMBER(22,2)   NULL,
                   DL.COR_BLENDING_ADJUSTMENT, --  cor_blending_adjustment      NUMBER(22,2)   NULL,
                   DL.COR_BLENDING_TRANSFER, --  cor_blending_transfer        NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_ASSET_AMOUNT, --  impairment_asset_amount      NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_EXPENSE_AMOUNT, --  impairment_expense_amount    NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_IMPAIRMENT, --  reserve_bal_impairment       NUMBER(22,2)   NUll,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPRECIATION_EXPENSE,0)), --ORIG_DEPR_EXPENSE
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPR_EXP_ALLOC_ADJUST,0)), --  orig_depr_exp_alloc_adjust   NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXPENSE,0)), --  orig_salv_expense            NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXP_ALLOC_ADJUST,0)), --  orig_salv_exp_alloc_adjust   NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXP_ALLOC_ADJUST,0)), --  orig_cor_exp_alloc_adjust    NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXPENSE,0)), --  orig_cor_expense             NUMBER(22,2)   NULL
               DL.BEGIN_RESERVE, --orig_begin_reserve          NUMBER(22,2) DEFAULT 0  NULL,
               DL.COR_BEG_RESERVE, --  orig_cor_begin_reserve     NUMBER(22,2) DEFAULT 0 NULL
               dl.impairment_asset_activity_salv, dl.impairment_asset_begin_balance
              from DEPR_METHOD_RATES DMR, DEPR_GROUP DG, DEPR_LEDGER DL
             where DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
               and DMR.EFFECTIVE_DATE =
                   (select max(DMR1.EFFECTIVE_DATE)
                      from DEPR_METHOD_RATES DMR1
                     where DMR.DEPR_METHOD_ID = DMR1.DEPR_METHOD_ID
                       and DMR.SET_OF_BOOKS_ID = DMR1.SET_OF_BOOKS_ID
                       and DMR1.EFFECTIVE_DATE <= A_MONTH)
               and DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
               and DL.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
               and DL.GL_POST_MO_YR <= A_MONTH
               and DL.GL_POST_MO_YR >= DECODE(DMR.RATE_USED_CODE, 2, DMR.EFFECTIVE_DATE, A_MONTH)
               and DG.COMPANY_ID = G_IGNOREINACTIVENO(I);

      -- statement to load the groups that are ignoring inactive
      forall I in indices of G_IGNOREINACTIVEYES
         insert into DEPR_CALC_STG
            (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
             DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, FISCALYEAROFFSET, SMOOTH_CURVE,
             DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
             DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS,
             TRUE_UP_CPR_DEPR, RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
             END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
             COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
             SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
             BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
             RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
             RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
             ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
             END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
             DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
             SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
             GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
             CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
             IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
             COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
             COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
             COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
             RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
             SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
             RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
             IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
             ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
             ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
          ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE,
          impairment_asset_activity_salv, impairment_asset_begin_balance)
            select DL.DEPR_GROUP_ID, --  depr_group_id                   NULL,
                   DL.SET_OF_BOOKS_ID, --  set_of_books_id              NUMBER(22,0)   NULL,
                   DL.GL_POST_MO_YR, --  gl_post_mo_yr                DATE           NULL,
                   A_MONTH,   --  calc_month                   DATE           NULL,
                   1, --  depr_calc_status             NUMBER(2,0)    NULL,
                   '', --  depr_calc_message            VARCHAR2(2000) NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                               DG.COMPANY_ID),
                             'no')), --  trf_in_est_adds              VARCHAR2(35)   NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                                               DG.COMPANY_ID),
                             'no')), --  include_rwip_in_net          VARCHAR2(35)   NULL,
                   TO_NUMBER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('FISCAL YEAR OFFSET',
                                                                                   DG.COMPANY_ID),
                                 '0')), --  fiscalyearoffset             NUMBER(2,0)    NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
                                                                               DG.COMPANY_ID),
                             '0')), -- smooth_curve
                   0, --  dnsa_cor_bal                 NUMBER(22,2)   NULL,
                   0, --  dnsa_salv_bal                NUMBER(22,2)   NULL,
                   0, --  cor_ytd                      NUMBER(22,2)   NULL,
                   0, --  salv_ytd                     NUMBER(22,2)   NULL,
                   DG.COMPANY_ID, --  company_id                   NUMBER(22,0)   NULL,
                   DG.SUBLEDGER_TYPE_ID, --  subledger_type_id            NUMBER(22,0)   NULL,
                   DG.DESCRIPTION, --  description                  VARCHAR2(254)  NULL,
                   DG.DEPR_METHOD_ID, --  depr_method_id               NUMBER(22,0)   NULL,
                   DG.MID_PERIOD_CONV, --  mid_period_conv              NUMBER(22,2)   NULL,
                   DG.MID_PERIOD_METHOD, --  mid_period_method            VARCHAR2(35)   NULL,
                   DG.EST_ANN_NET_ADDS, --  dg_est_ann_net_adds          NUMBER(22,8)   NULL,
                   DG.TRUE_UP_CPR_DEPR, --  true_up_cpr_depr             NUMBER(22,0)   NULL,
                   DMR.RATE, --  rate                         NUMBER(22,8)   NULL,
                   DMR.NET_GROSS, --  net_gross                    NUMBER(22,0)   NULL,
                   NVL(DMR.OVER_DEPR_CHECK, 0), --  over_depr_check              NUMBER(22,0)   NULL,
                   DMR.NET_SALVAGE_PCT, --  net_salvage_pct              NUMBER(22,8)   NULL,
                   NVL(DMR.RATE_USED_CODE, 0), --  rate_used_code               NUMBER(22,0)   NULL,
                   DMR.END_OF_LIFE, --  end_of_life                  NUMBER(22,0)   NULL,
                   NVL(DMR.EXPECTED_AVERAGE_LIFE, 0), --  expected_average_life        NUMBER(22,0)   NULL,
                   NVL(DMR.RESERVE_RATIO_ID, 0), --  reserve_ratio_id             NUMBER(22,0)   NULL,
                   DMR.COST_OF_REMOVAL_RATE, --  dmr_cost_of_removal_rate     NUMBER(22,8)   NULL,
                   DMR.COST_OF_REMOVAL_PCT, --  cost_of_removal_pct          NUMBER(22,8)   NULL,
                   DMR.SALVAGE_RATE, --  dmr_salvage_rate             NUMBER(22,8)   NULL,
                   DMR.INTEREST_RATE, --  interest_rate                NUMBER(22,8)   NULL,
                   NVL(DMR.AMORTIZABLE_LIFE, 0), --  amortizable_life             NUMBER(22,0)   NULL,
                   DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1), --  cor_treatment                NUMBER(1,0)    NULL,
                   DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1), --  salvage_treatment            NUMBER(1,0)    NULL,
                   NVL(DMR.NET_SALVAGE_AMORT_LIFE, 0), --  net_salvage_amort_life       NUMBER(22,0)   NULL,
                   DMR.ALLOCATION_PROCEDURE, --  allocation_procedure         VARCHAR2(5)    NULL,
                   DL.DEPR_LEDGER_STATUS, --  depr_ledger_status           NUMBER(22,0)   NULL,
                   DL.BEGIN_RESERVE, --  begin_reserve                NUMBER(22,2)   NULL,
                   0 AS END_RESERVE, --  end_reserve                  NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_PROVISION, --  reserve_bal_provision        NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_COR, --  reserve_bal_cor              NUMBER(22,2)   NULL,
                   DL.SALVAGE_BALANCE, --  salvage_balance              NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_ADJUST, --  reserve_bal_adjust           NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements      NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in          NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out         NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_OTHER_CREDITS, --  reserve_bal_other_credits    NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss        NUMBER(22,2)   NULL,
                   DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor         NUMBER(22,2)   NULL,
                   DL.BEGIN_BALANCE, --  begin_balance                NUMBER(22,2)   NULL,
                   DL.ADDITIONS, --  additions
                   DL.RETIREMENTS, --  retirements                  NUMBER(22,2)   NULL,
                   DL.TRANSFERS_IN, --  transfers_in                 NUMBER(22,2)   NULL,
                   DL.TRANSFERS_OUT, --  transfers_out                NUMBER(22,2)   NULL,
                   DL.ADJUSTMENTS, --  adjustments                  NUMBER(22,2)   NULL,
                   0 AS DEPRECIATION_BASE, --  depreciation_base            NUMBER(22,2)
                   0 AS END_BALANCE, --  end_balance                  NUMBER(22,2)   NULL
                   0 AS DEPRECIATION_RATE, --  depreciation_rate            NUMBER(22,8)   NULL,
                   0 AS DEPRECIATION_EXPENSE, --  depreciation_expense         NUMBER(22,2)   NULL,
                   DL.DEPR_EXP_ADJUST, --  depr_exp_adjust              NUMBER(22,2)   NULL,
                   0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust        NUMBER(22,2)   NULL,
                   DL.COST_OF_REMOVAL, --  cost_of_removal              NUMBER(22,2)   NULL,
                   DL.RESERVE_RETIREMENTS, --  reserve_retirements          NUMBER(22,2)   NULL,
                   DL.SALVAGE_RETURNS, --  salvage_returns              NUMBER(22,2)   NULL,
                   DL.SALVAGE_CASH, --  salvage_cash                 NUMBER(22,2)   NULL,
                   DL.RESERVE_CREDITS, --  reserve_credits              NUMBER(22,2)   NULL,
                   DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments          NUMBER(22,2)   NULL,
                   DL.RESERVE_TRAN_IN, --  reserve_tran_in              NUMBER(22,2)   NULL,
                   DL.RESERVE_TRAN_OUT, --  reserve_tran_out             NUMBER(22,2)   NULL,
                   DL.GAIN_LOSS, --  gain_loss                    NUMBER(22,2)   NULL,
                   DL.VINTAGE_NET_SALVAGE_AMORT, --  vintage_net_salvage_amort    NUMBER(22,2)   NULL,
                   DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
                   DL.CURRENT_NET_SALVAGE_AMORT, --  current_net_salvage_amort    NUMBER(22,2)   NULL,
                   DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg       NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act       NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end       NUMBER(22,2)   NULL,
                   DL.EST_ANN_NET_ADDS, --  est_ann_net_adds             NUMBER(22,2)   NULL,
                   DL.RWIP_ALLOCATION, --  rwip_allocation              NUMBER(22,2)   NULL,
                   DL.COR_BEG_RESERVE, --  cor_beg_reserve              NUMBER(22,2)   NULL,
                   0 AS COR_EXPENSE, --  cor_expense                  NUMBER(22,2)   NULL,
                   NVL(DL.COR_EXP_ADJUST,0), --  cor_exp_adjust               NUMBER(22,2)   NULL,
                   0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust         NUMBER(22,2)   NULL,
                   DL.COR_RES_TRAN_IN, --  cor_res_tran_in              NUMBER(22,2)   NULL,
                   DL.COR_RES_TRAN_OUT, --  cor_res_tran_out             NUMBER(22,2)   NULL,
                   DL.COR_RES_ADJUST, --  cor_res_adjust               NUMBER(22,2)   NULL,
                   0 AS COR_END_RESERVE, --  cor_end_reserve              NUMBER(22,2)   NULL,
                   DL.COST_OF_REMOVAL_RATE, --  cost_of_removal_rate         NUMBER(22,8)   NULL,
                   DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base         NUMBER(22,2)   NULL,
                   DL.RWIP_COST_OF_REMOVAL, --  rwip_cost_of_removal         NUMBER(22,2)   NULL,
                   DL.RWIP_SALVAGE_CASH, --  rwip_salvage_cash            NUMBER(22,2)   NULL,
                   DL.RWIP_SALVAGE_RETURNS, --  rwip_salvage_returns         NUMBER(22,2)   NULL,
                   DL.RWIP_RESERVE_CREDITS, --  rwip_reserve_credits         NUMBER(22,2)   NULL,
                   DL.SALVAGE_RATE, --  salvage_rate                 NUMBER(22,8)   NULL,
                   0 AS SALVAGE_BASE, --  salvage_base                 NUMBER(22,2)   NULL,
                   0 AS SALVAGE_EXPENSE, --  salvage_expense              NUMBER(22,2)   NULL,
                   DL.SALVAGE_EXP_ADJUST, --  salvage_exp_adjust           NUMBER(22,2)   NULL,
                   0 AS SALVAGE_EXP_ALLOC_ADJUST, --  salvage_exp_alloc_adjust     NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_SALVAGE_EXP, --  reserve_bal_salvage_exp      NUMBER(22,2)   NULL,
                   DL.RESERVE_BLENDING_ADJUSTMENT, --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
                   DL.RESERVE_BLENDING_TRANSFER, --  reserve_blending_transfer    NUMBER(22,2)   NULL,
                   DL.COR_BLENDING_ADJUSTMENT, --  cor_blending_adjustment      NUMBER(22,2)   NULL,
                   DL.COR_BLENDING_TRANSFER, --  cor_blending_transfer        NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_ASSET_AMOUNT, --  impairment_asset_amount      NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_EXPENSE_AMOUNT, --  impairment_expense_amount    NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_IMPAIRMENT, --  reserve_bal_impairment       NUMBER(22,2)   NUll,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPRECIATION_EXPENSE,0)), --ORIG_DEPR_EXPENSE
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPR_EXP_ALLOC_ADJUST,0)), --  orig_depr_exp_alloc_adjust   NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXPENSE,0)), --  orig_salv_expense            NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXP_ALLOC_ADJUST,0)), --  orig_salv_exp_alloc_adjust   NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXP_ALLOC_ADJUST,0)), --  orig_cor_exp_alloc_adjust    NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXPENSE,0)), --  orig_cor_expense             NUMBER(22,2)   NULL
               DL.BEGIN_RESERVE, --orig_begin_reserve          NUMBER(22,2) DEFAULT 0  NULL,
               DL.COR_BEG_RESERVE, --  orig_cor_begin_reserve     NUMBER(22,2) DEFAULT 0 NULL
               dl.impairment_asset_activity_salv, dl.impairment_asset_begin_balance
              from DEPR_METHOD_RATES DMR, DEPR_GROUP DG, DEPR_LEDGER DL
             where DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
               and DMR.EFFECTIVE_DATE =
                   (select max(DMR1.EFFECTIVE_DATE)
                      from DEPR_METHOD_RATES DMR1
                     where DMR.DEPR_METHOD_ID = DMR1.DEPR_METHOD_ID
                       and DMR.SET_OF_BOOKS_ID = DMR1.SET_OF_BOOKS_ID
                       and DMR1.EFFECTIVE_DATE <= A_MONTH)
               and DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
               and DL.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
               and DL.GL_POST_MO_YR <= A_MONTH
               and DL.GL_POST_MO_YR >= DECODE(DMR.RATE_USED_CODE, 2, DMR.EFFECTIVE_DATE, A_MONTH)
               and DG.COMPANY_ID = G_IGNOREINACTIVEYES(I)
               and (NVL(DG.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
                   DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
                   DL.COR_BEG_RESERVE <> 0);

   end P_STAGEMONTHENDDEPR;

/*

   --**************************************************************************
   --                            P_FCSTDEPRSTAGE
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation forecast.
   -- THE Load is based on fcst depr ledger for a version id
   -- AND a single month
   --
   procedure P_FCSTDEPRSTAGE(A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                      A_MONTH date) is

   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the forecast calculation staging table for month:' ||
                                 TO_CHAR(A_MONTH, 'yyyymm'));

     insert into FCST_DEPR_LEDGER
      (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, FCST_DEPR_VERSION_ID,
      DEPR_LEDGER_STATUS, BEGIN_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR,
      RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
      RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR,
      BEGIN_BALANCE, ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
      DEPR_EXP_ADJUST, RESERVE_RETIREMENTS, SALVAGE_RETURNS, SALVAGE_CASH, RESERVE_CREDITS,
      RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT, GAIN_LOSS,
      VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
      CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE,
      IMPAIRMENT_RESERVE_BEG, IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END,
      EST_ANN_NET_ADDS, RWIP_ALLOCATION, COR_BEG_RESERVE, COR_EXP_ADJUST,
      COR_RES_TRAN_IN, COR_RES_TRAN_OUT, COR_RES_ADJUST,
      COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_BASE,
      RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS, RWIP_RESERVE_CREDITS,
      SALVAGE_RATE, SALVAGE_EXP_ADJUST, RESERVE_BAL_SALVAGE_EXP,
      RESERVE_BLENDING_ADJUSTMENT, RESERVE_BLENDING_TRANSFER,
      COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
      IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT)
     select gv.FCST_DEPR_GROUP_ID, vsob.SET_OF_BOOKS_ID, A_MONTH, A_VERSION_ID,
      9 DEPR_LEDGER_STATUS, 0 BEGIN_RESERVE, 0 RESERVE_BAL_PROVISION, 0 RESERVE_BAL_COR,
      0 RESERVE_BAL_ADJUST, 0 RESERVE_BAL_RETIREMENTS, 0 RESERVE_BAL_TRAN_IN, 0 RESERVE_BAL_TRAN_OUT,
      0 RESERVE_BAL_OTHER_CREDITS, 0 RESERVE_BAL_GAIN_LOSS, 0 RESERVE_ALLOC_FACTOR,
      0 BEGIN_BALANCE, 0 ADDITIONS, 0 RETIREMENTS, 0 TRANSFERS_IN, 0 TRANSFERS_OUT, 0 ADJUSTMENTS,
      0 DEPR_EXP_ADJUST, 0 RESERVE_RETIREMENTS, 0 SALVAGE_RETURNS, 0 SALVAGE_CASH, 0 RESERVE_CREDITS,
      0 RESERVE_ADJUSTMENTS, 0 RESERVE_TRAN_IN, 0 RESERVE_TRAN_OUT, 0 GAIN_LOSS,
      0 VINTAGE_NET_SALVAGE_AMORT, 0 VINTAGE_NET_SALVAGE_RESERVE,
      0 CURRENT_NET_SALVAGE_AMORT, 0 CURRENT_NET_SALVAGE_RESERVE,
      0 IMPAIRMENT_RESERVE_BEG, 0 IMPAIRMENT_RESERVE_ACT, 0 IMPAIRMENT_RESERVE_END,
      0 EST_ANN_NET_ADDS, 0 RWIP_ALLOCATION, 0 COR_BEG_RESERVE, 0 COR_EXP_ADJUST,
      0 COR_RES_TRAN_IN, 0 COR_RES_TRAN_OUT, 0 COR_RES_ADJUST,
      0 COST_OF_REMOVAL_RATE, 0 COST_OF_REMOVAL_BASE,
      0 RWIP_COST_OF_REMOVAL, 0 RWIP_SALVAGE_CASH, 0 RWIP_SALVAGE_RETURNS, 0 RWIP_RESERVE_CREDITS,
      0 SALVAGE_RATE, 0 SALVAGE_EXP_ADJUST, 0 RESERVE_BAL_SALVAGE_EXP,
      0 RESERVE_BLENDING_ADJUSTMENT, 0 RESERVE_BLENDING_TRANSFER,
      0 COR_BLENDING_ADJUSTMENT, 0 COR_BLENDING_TRANSFER,
      0 IMPAIRMENT_ASSET_AMOUNT, 0 IMPAIRMENT_EXPENSE_AMOUNT, 0 RESERVE_BAL_IMPAIRMENT
     from FCST_DEPR_GROUP_VERSION gv, FCST_VERSION_SET_OF_BOOKS vsob
     where gv.FCST_DEPR_VERSION_ID = A_VERSION_ID
      and vsob.FCST_DEPR_VERSION_ID = gv.FCST_DEPR_VERSION_ID
      and not exists (
         select 1 from FCST_DEPR_LEDGER b
         where b.FCST_DEPR_GROUP_ID = gv.FCST_DEPR_GROUP_ID
         and b.SET_OF_BOOKS_ID = vsob.SET_OF_BOOKS_ID
         and b.GL_POST_MO_YR = A_MONTH
         and b.FCST_DEPR_VERSION_ID = A_VERSION_ID
      )
     group by gv.FCST_DEPR_GROUP_ID, vsob.SET_OF_BOOKS_ID;

    insert into DEPR_CALC_STG
      (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
       DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, FISCALYEAROFFSET, SMOOTH_CURVE,
       DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
       DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS,
       TRUE_UP_CPR_DEPR, RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
       END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
       COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
       SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
       BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
       RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
       RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
       ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
       END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
       DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
       SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
       GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
       CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
       IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
       COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
       COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
       COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
       RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
       SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
       RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
       IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
       ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
       ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
       ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE, impairment_asset_activity_salv, impairment_asset_begin_balance)
      select DL.FCST_DEPR_GROUP_ID, --  depr_group_id                   NULL,
            DL.SET_OF_BOOKS_ID, --  set_of_books_id              NUMBER(22,0)   NULL,
            DL.GL_POST_MO_YR, --  gl_post_mo_yr                DATE           NULL,
            A_MONTH,   --  calc_month                   DATE           NULL,
            1, --  depr_calc_status             NUMBER(2,0)    NULL,
            '', --  depr_calc_message            VARCHAR2(2000) NULL,
            LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                         DGV.COMPANY_ID),
                   'no')), --  trf_in_est_adds              VARCHAR2(35)   NULL,
            LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                         DGV.COMPANY_ID),
                   'no')), --  include_rwip_in_net          VARCHAR2(35)   NULL,
            TO_NUMBER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('FISCAL YEAR OFFSET',
                                                            DGV.COMPANY_ID),
                      '0')), --  fiscalyearoffset             NUMBER(2,0)    NULL,
            LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
                                                         DGV.COMPANY_ID),
                   '0')), -- smooth_curve
            0, --  dnsa_cor_bal                 NUMBER(22,2)   NULL,
            0, --  dnsa_salv_bal                NUMBER(22,2)   NULL,
            0, --  cor_ytd                      NUMBER(22,2)   NULL,
            0, --  salv_ytd                     NUMBER(22,2)   NULL,
            DGV.COMPANY_ID, --  company_id                   NUMBER(22,0)   NULL,
            DGV.SUBLEDGER_TYPE_ID, --  subledger_type_id            NUMBER(22,0)   NULL,
            DGV.DESCRIPTION, --  description                  VARCHAR2(254)  NULL,
            DGV.FCST_DEPR_METHOD_ID, --  depr_method_id               NUMBER(22,0)   NULL,
            DGV.MID_PERIOD_CONV, --  mid_period_conv              NUMBER(22,2)   NULL,
            DGV.MID_PERIOD_METHOD, --  mid_period_method            VARCHAR2(35)   NULL,
            DGV.EST_ANN_NET_ADDS, --  dg_est_ann_net_adds          NUMBER(22,8)   NULL,
            DGV.TRUE_UP_CPR_DEPR, --  true_up_cpr_depr             NUMBER(22,0)   NULL,
            DMR.RATE, --  rate                         NUMBER(22,8)   NULL,
            DMR.NET_GROSS, --  net_gross                    NUMBER(22,0)   NULL,
            NVL(DMR.OVER_DEPR_CHECK, 0), --  over_depr_check              NUMBER(22,0)   NULL,
            DMR.NET_SALVAGE_PCT, --  net_salvage_pct              NUMBER(22,8)   NULL,
            NVL(DMR.RATE_USED_CODE, 0), --  rate_used_code               NUMBER(22,0)   NULL,
            DMR.END_OF_LIFE, --  end_of_life                  NUMBER(22,0)   NULL,
            NVL(DMR.EXPECTED_AVERAGE_LIFE, 0), --  expected_average_life        NUMBER(22,0)   NULL,
            NVL(DMR.RESERVE_RATIO_ID, 0), --  reserve_ratio_id             NUMBER(22,0)   NULL,
            DMR.COST_OF_REMOVAL_RATE, --  dmr_cost_of_removal_rate     NUMBER(22,8)   NULL,
            DMR.COST_OF_REMOVAL_PCT, --  cost_of_removal_pct          NUMBER(22,8)   NULL,
            DMR.SALVAGE_RATE, --  dmr_salvage_rate             NUMBER(22,8)   NULL,
            DMR.INTEREST_RATE, --  interest_rate                NUMBER(22,8)   NULL,
            NVL(DMR.AMORTIZABLE_LIFE, 0), --  amortizable_life             NUMBER(22,0)   NULL,
            DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1), --  cor_treatment                NUMBER(1,0)    NULL,
            DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1), --  salvage_treatment            NUMBER(1,0)    NULL,
            NVL(DMR.NET_SALVAGE_AMORT_LIFE, 0), --  net_salvage_amort_life       NUMBER(22,0)   NULL,
            DMR.ALLOCATION_PROCEDURE, --  allocation_procedure         VARCHAR2(5)    NULL,
            DL.DEPR_LEDGER_STATUS, --  depr_ledger_status           NUMBER(22,0)   NULL,
            DL.BEGIN_RESERVE, --  begin_reserve                NUMBER(22,2)   NULL,
            0 AS END_RESERVE, --  end_reserve                  NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_PROVISION, --  reserve_bal_provision        NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_COR, --  reserve_bal_cor              NUMBER(22,2)   NULL,
            DL.SALVAGE_BALANCE, --  salvage_balance              NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_ADJUST, --  reserve_bal_adjust           NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements      NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in          NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out         NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_OTHER_CREDITS, --  reserve_bal_other_credits    NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss        NUMBER(22,2)   NULL,
            DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor         NUMBER(22,2)   NULL,
            DL.BEGIN_BALANCE, --  begin_balance                NUMBER(22,2)   NULL,
            DL.ADDITIONS, --  additions
            DL.RETIREMENTS, --  retirements                  NUMBER(22,2)   NULL,
            DL.TRANSFERS_IN, --  transfers_in                 NUMBER(22,2)   NULL,
            DL.TRANSFERS_OUT, --  transfers_out                NUMBER(22,2)   NULL,
            DL.ADJUSTMENTS, --  adjustments                  NUMBER(22,2)   NULL,
            0 AS DEPRECIATION_BASE, --  depreciation_base            NUMBER(22,2)
            0 AS END_BALANCE, --  end_balance                  NUMBER(22,2)   NULL
            0 AS DEPRECIATION_RATE, --  depreciation_rate            NUMBER(22,8)   NULL,
            0 AS DEPRECIATION_EXPENSE, --  depreciation_expense         NUMBER(22,2)   NULL,
            DL.DEPR_EXP_ADJUST, --  depr_exp_adjust              NUMBER(22,2)   NULL,
            0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust        NUMBER(22,2)   NULL,
            DL.COST_OF_REMOVAL, --  cost_of_removal              NUMBER(22,2)   NULL,
            DL.RESERVE_RETIREMENTS, --  reserve_retirements          NUMBER(22,2)   NULL,
            DL.SALVAGE_RETURNS, --  salvage_returns              NUMBER(22,2)   NULL,
            DL.SALVAGE_CASH, --  salvage_cash                 NUMBER(22,2)   NULL,
            DL.RESERVE_CREDITS, --  reserve_credits              NUMBER(22,2)   NULL,
            DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments          NUMBER(22,2)   NULL,
            DL.RESERVE_TRAN_IN, --  reserve_tran_in              NUMBER(22,2)   NULL,
            DL.RESERVE_TRAN_OUT, --  reserve_tran_out             NUMBER(22,2)   NULL,
            DL.GAIN_LOSS, --  gain_loss                    NUMBER(22,2)   NULL,
            DL.VINTAGE_NET_SALVAGE_AMORT, --  vintage_net_salvage_amort    NUMBER(22,2)   NULL,
            DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
            DL.CURRENT_NET_SALVAGE_AMORT, --  current_net_salvage_amort    NUMBER(22,2)   NULL,
            DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
            DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg       NUMBER(22,2)   NULL,
            DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act       NUMBER(22,2)   NULL,
            DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end       NUMBER(22,2)   NULL,
            DL.EST_ANN_NET_ADDS, --  est_ann_net_adds             NUMBER(22,2)   NULL,
            DL.RWIP_ALLOCATION, --  rwip_allocation              NUMBER(22,2)   NULL,
            DL.COR_BEG_RESERVE, --  cor_beg_reserve              NUMBER(22,2)   NULL,
            0 AS COR_EXPENSE, --  cor_expense                  NUMBER(22,2)   NULL,
            NVL(DL.COR_EXP_ADJUST,0), --  cor_exp_adjust               NUMBER(22,2)   NULL,
            0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust         NUMBER(22,2)   NULL,
            DL.COR_RES_TRAN_IN, --  cor_res_tran_in              NUMBER(22,2)   NULL,
            DL.COR_RES_TRAN_OUT, --  cor_res_tran_out             NUMBER(22,2)   NULL,
            DL.COR_RES_ADJUST, --  cor_res_adjust               NUMBER(22,2)   NULL,
            0 AS COR_END_RESERVE, --  cor_end_reserve              NUMBER(22,2)   NULL,
            DL.COST_OF_REMOVAL_RATE, --  cost_of_removal_rate         NUMBER(22,8)   NULL,
            DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base         NUMBER(22,2)   NULL,
            DL.RWIP_COST_OF_REMOVAL, --  rwip_cost_of_removal         NUMBER(22,2)   NULL,
            DL.RWIP_SALVAGE_CASH, --  rwip_salvage_cash            NUMBER(22,2)   NULL,
            DL.RWIP_SALVAGE_RETURNS, --  rwip_salvage_returns         NUMBER(22,2)   NULL,
            DL.RWIP_RESERVE_CREDITS, --  rwip_reserve_credits         NUMBER(22,2)   NULL,
            DL.SALVAGE_RATE, --  salvage_rate                 NUMBER(22,8)   NULL,
            0 AS SALVAGE_BASE, --  salvage_base                 NUMBER(22,2)   NULL,
            0 AS SALVAGE_EXPENSE, --  salvage_expense              NUMBER(22,2)   NULL,
            DL.SALVAGE_EXP_ADJUST, --  salvage_exp_adjust           NUMBER(22,2)   NULL,
            0 AS SALVAGE_EXP_ALLOC_ADJUST, --  salvage_exp_alloc_adjust     NUMBER(22,2)   NULL,
            DL.RESERVE_BAL_SALVAGE_EXP, --  reserve_bal_salvage_exp      NUMBER(22,2)   NULL,
            DL.RESERVE_BLENDING_ADJUSTMENT, --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
            DL.RESERVE_BLENDING_TRANSFER, --  reserve_blending_transfer    NUMBER(22,2)   NULL,
            DL.COR_BLENDING_ADJUSTMENT, --  cor_blending_adjustment      NUMBER(22,2)   NULL,
            DL.COR_BLENDING_TRANSFER, --  cor_blending_transfer        NUMBER(22,2)   NULL,
            DL.IMPAIRMENT_ASSET_AMOUNT, --  impairment_asset_amount      NUMBER(22,2)   NULL,
            DL.IMPAIRMENT_EXPENSE_AMOUNT, --  impairment_expense_amount    NUMBER(22,2)   NULL,
            0, --  reserve_bal_impairment       NUMBER(22,2)   NUll,
            DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPRECIATION_EXPENSE,0)), --ORIG_DEPR_EXPENSE
            DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPR_EXP_ALLOC_ADJUST,0)), --  orig_depr_exp_alloc_adjust   NUMBER(22,2)   NULL,
            DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXPENSE,0)), --  orig_salv_expense            NUMBER(22,2)   NULL,
            DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, 0), --  orig_salv_exp_alloc_adjust   NUMBER(22,2)   NULL,
            DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXP_ALLOC_ADJUST,0)), --  orig_cor_exp_alloc_adjust    NUMBER(22,2)   NULL,
            DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXPENSE,0)), --  orig_cor_expense             NUMBER(22,2)   NULL
            DL.BEGIN_RESERVE, --orig_begin_reserve          NUMBER(22,2) DEFAULT 0  NULL,
            DL.COR_BEG_RESERVE, --  orig_cor_begin_reserve     NUMBER(22,2) DEFAULT 0 NULL
            dl.impairment_asset_activity_salv, dl.impairment_asset_begin_balance
        from FCST_DEPR_METHOD_RATES DMR, FCST_DEPR_LEDGER DL, FCST_DEPR_GROUP_VERSION DGV
       where DMR.FCST_DEPR_METHOD_ID = DGV.FCST_DEPR_METHOD_ID
         and DMR.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
         and DMR.EFFECTIVE_DATE =
            (select max(DMR1.EFFECTIVE_DATE)
              from FCST_DEPR_METHOD_RATES DMR1
             where DMR.FCST_DEPR_METHOD_ID = DMR1.FCST_DEPR_METHOD_ID
               and DMR.SET_OF_BOOKS_ID = DMR1.SET_OF_BOOKS_ID
               and DMR1.EFFECTIVE_DATE <= A_MONTH)
         and DL.FCST_DEPR_GROUP_ID = DGV.FCST_DEPR_GROUP_ID
         and DL.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
         and DL.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
         and DL.GL_POST_MO_YR <= A_MONTH
         and DL.GL_POST_MO_YR >= DECODE(DMR.RATE_USED_CODE, 2, DMR.EFFECTIVE_DATE, A_MONTH)
         and DGV.FCST_DEPR_VERSION_ID = A_VERSION_ID
         --Either we don't ignore inactive depr groups, or the depr group is active
         and (
            LOWER(trim(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',DGV.COMPANY_ID),'no'))) = 'no'
            or NVL(DGV.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
            DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
            DL.COR_BEG_RESERVE <> 0
         )
         ;

   end P_FCSTDEPRSTAGE;

   --**************************************************************************
   --                            P_FCSTDEPRSTAGE
   --**************************************************************************
   --
   --  WRAPPER for the prep function to pass in an array of months.
   --
   procedure P_FCSTDEPRSTAGE(A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                      A_MONTHS PKG_PP_COMMON.DATE_TABTYPE) is

   begin
      for I in A_MONTHS.FIRST .. A_MONTHS.LAST
      loop
         P_FCSTDEPRSTAGE(A_VERSION_ID, A_MONTHS(I));
      end loop;
   end P_FCSTDEPRSTAGE;

   --**************************************************************************
   --                            P_FCSTDEPRCALC
   --**************************************************************************
   --
   -- WRAPPER for the fcst depr calc to allow single month depr calculations
   --
   procedure P_FCSTDEPRCALC(A_VERSION_ID     FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                            A_MONTH          date) is

      MY_MONTHS PKG_PP_COMMON.DATE_TABTYPE;

   begin
      MY_MONTHS(1) := A_MONTH;
      P_FCSTDEPRCALC(A_VERSION_ID, MY_MONTHS);
   end P_FCSTDEPRCALC;

   --**************************************************************************
   --                            P_FCSTDEPRCALC
   --**************************************************************************
   --
   --  Start a depr calc for an array of companies and an array of months
   -- Start Logging
   -- Load Global Temp Staging table
   -- Calculate Depreciation Forecast
   -- Backfill results to fcst depr ledger
   -- End logging
   --
   procedure P_FCSTDEPRCALC(A_VERSION_ID     FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                            A_MONTHS         PKG_PP_COMMON.DATE_TABTYPE) is
      MY_RET number;
      MY_STR varchar2(2000);
     MY_I number;
   begin
     -- set global variables
     G_VERSION_ID := A_VERSION_ID;
     G_MONTHS := A_MONTHS;

     -- stage the data into DEPR_CALC_STG
      P_FCSTDEPRSTAGE(A_VERSION_ID, A_MONTHS);

      -- start depreciation calculation
      if F_PREPDEPRCALC() = -1 then
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR During Forecast Depreciation Calculation');
      end if;

     -- clear global variables
     G_VERSION_ID := null;

      --
      --  END LOGGING
      --
      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');


   end P_FCSTDEPRCALC;

*/


   --**************************************************************************
   --                            P_DEPRCALC
   --**************************************************************************
   procedure P_DEPRCALC is
      L_STATUS varchar2(254);
     L_COUNT number;
   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING Depreciation Calculation');

      update DEPR_CALC_STG set DEPR_CALC_STATUS = 2, DEPR_CALC_MESSAGE = 'Starting to Calc';

     select nvl(count(1),0)
     into L_COUNT
     from DEPR_CALC_STG;
     PKG_PP_LOG.P_WRITE_MESSAGE(to_char(L_COUNT) || ' records in staging table...');

     select nvl(count(1),0)
     into L_COUNT
     from DEPR_CALC_STG
     where DEPR_CALC_STATUS = 2
     and SUBLEDGER_TYPE_ID = 0;
     PKG_PP_LOG.P_WRITE_MESSAGE(to_char(L_COUNT) || ' valid records in staging table...');
      --
      -- IF SMD and end of life provided, the net_gross has to be 1
      -- IF mid_period_method is 'End of Life', then end_of_life has to be provided and net_gross has to be 1
      -- IF mid_period_method is Monthly then end_of_life does not get used
      --
      merge into DEPR_CALC_STG A
      using (select *
               from DEPR_CALC_STG B
              where B.DEPR_CALC_STATUS = 2
                and B.SUBLEDGER_TYPE_ID = 0 MODEL partition
              by(DEPR_GROUP_ID, SET_OF_BOOKS_ID) DIMENSION
              by(row_number() over(partition by DEPR_GROUP_ID, SET_OF_BOOKS_ID order by GL_POST_MO_YR) - 1 as ID, MID_PERIOD_METHOD) MEASURES(
                      ADDITIONS,
                             RETIREMENTS,
                             TRANSFERS_IN,
                             TRANSFERS_OUT,
                             ADJUSTMENTS,
                             IMPAIRMENT_ASSET_AMOUNT,
                             SALVAGE_CASH,
                             SALVAGE_RETURNS,
                             CURRENT_NET_SALVAGE_AMORT,
                             RESERVE_CREDITS,
                             RESERVE_TRAN_IN,
                             RESERVE_TRAN_OUT,
                             RESERVE_ADJUSTMENTS,
                             IMPAIRMENT_EXPENSE_AMOUNT,
                             COST_OF_REMOVAL,
                             COR_RES_TRAN_IN,
                             COR_RES_TRAN_OUT,
                             COR_RES_ADJUST,
                             MID_PERIOD_CONV,
                             RATE,
                             NET_SALVAGE_PCT,
                             COST_OF_REMOVAL_PCT,
                             RWIP_SALVAGE_CASH,
                             RWIP_SALVAGE_RETURNS,
                             RWIP_RESERVE_CREDITS,
                             RWIP_COST_OF_REMOVAL,
                             COR_BEG_RESERVE,
                             BEGIN_BALANCE,
                             BEGIN_RESERVE,
                             COR_TREATMENT,
                             SALVAGE_TREATMENT,
                             COR_YTD,
                             SALV_YTD,
                             DNSA_COR_BAL,
                             DNSA_SALV_BAL,
                             END_BALANCE,
                             DEPRECIATION_RATE,
                             DEPRECIATION_BASE,
                             DEPRECIATION_EXPENSE,
                             CALC_MONTH,
                             END_OF_LIFE,
                             NET_GROSS,
                      OVER_DEPR_CHECK,
                             TRF_IN_EST_ADDS,
                             INCLUDE_RWIP_IN_NET,
                             SALVAGE_BASE,
                             SALVAGE_RATE,
                             SALVAGE_EXPENSE,
                             DMR_SALVAGE_RATE,
                             COST_OF_REMOVAL_BASE,
                             COST_OF_REMOVAL_RATE,
                             COR_EXPENSE,
                             DMR_COST_OF_REMOVAL_RATE,
                             0 as PLANT_ACTIVITY,
                             0 as RESERVE_ACTIVITY,
                             0 as RWIP_SALVAGE,
                             0 as COR_ACTIVITY,
                             0 as REMAINING_MONTHS,
                             0 as RWIP_COR,
                             -- new vars
                             BEGIN_BALANCE_QTR,
                             BEGIN_RESERVE_QTR,
                             FISCALQTRSTART,
                             BEGIN_BALANCE_YEAR,
                             BEGIN_RESERVE_YEAR,
                             FISCALYEARSTART,
                             SMOOTH_CURVE,
                             GAIN_LOSS,
                             PLANT_ACTIVITY_2,
                             RESERVE_ACTIVITY_2,
                             COR_ACTIVITY_2,
                             EST_NET_ADDS,
                             0 as CUMULATIVE_TRANSFERS,
                             0 as CUM_UOP_DEPR,
                             0 as CUM_UOP_DEPR_2,
                             TYPE_2_EXIST,
                             RESERVE_DIFF,
                             PRODUCTION,
                             ESTIMATED_PRODUCTION,
                             PRODUCTION_2,
                             ESTIMATED_PRODUCTION_2,
                             CURR_UOP_EXP,
                             CURR_UOP_EXP_2,
                             MIN_CALC,
                             MIN_UOP_EXP,
                             YTD_UOP_DEPR,
                             LESS_YEAR_UOP_DEPR,
                             DEPRECIATION_UOP_RATE,
                             DEPRECIATION_UOP_RATE_2,
                             DEPRECIATION_BASE_UOP,
                             DEPRECIATION_BASE_UOP_2,
                             0 as COR_END_RESERVE,
                             0 as END_RESERVE,
                             GL_POST_MO_YR,
                             months_between(max(gl_post_mo_yr) over(partition by depr_group_id, set_of_Books_id), gl_post_mo_yr) as MONTH_ID,
                      DEPR_EXP_ALLOC_ADJUST,
                      SALVAGE_EXP_ALLOC_ADJUST,
                      COR_EXP_ALLOC_ADJUST,
                      ORIG_DEPR_EXPENSE,
                      ORIG_DEPR_EXP_ALLOC_ADJUST,
                      ORIG_SALV_EXPENSE,
                      ORIG_SALV_EXP_ALLOC_ADJUST,
                      ORIG_COR_EXP_ALLOC_ADJUST,
                      ORIG_COR_EXPENSE,
                      RETRO_DEPR_ADJ,
                      RETRO_COR_ADJ,
                      RETRO_SALV_ADJ,
                      RATE_USED_CODE,
                      BEGIN_COR_YEAR,
                      BEGIN_COR_QTR,
                      DEPR_EXP_ADJUST,
                      SALVAGE_EXP_ADJUST,
                      0 AS OVER_DEPR_ADJ,
                      0 AS OVER_DEPR_ADJ_COR,
                      0 AS OVER_DEPR_ADJ_SALV,
                      ORIG_BEGIN_RESERVE,
                      ORIG_COR_BEG_RESERVE,
                      impairment_asset_activity_salv, impairment_asset_begin_balance)
                             RULES update
ITERATE(240)
              (
                           -- prep activity and begin balances based on flags and settings
                           -- applies to ALL mid period methods.
                           PLANT_ACTIVITY [ ITERATION_NUMBER, any ] = case
                              when TRF_IN_EST_ADDS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 'no' then
                               ADDITIONS [ CV(ID), CV(MID_PERIOD_METHOD) ] + RETIREMENTS [  CV(ID), CV(MID_PERIOD_METHOD)]
                        + ADJUSTMENTS [ CV(ID), CV(MID_PERIOD_METHOD) ] + IMPAIRMENT_ASSET_AMOUNT [CV(ID), CV(MID_PERIOD_METHOD) ]
                              else
                               ADDITIONS [ CV(ID), CV(MID_PERIOD_METHOD) ] + RETIREMENTS [ CV(ID), CV(MID_PERIOD_METHOD)]
                        + TRANSFERS_IN [ CV(ID), CV(MID_PERIOD_METHOD) ] + TRANSFERS_OUT [CV(ID), CV(MID_PERIOD_METHOD) ]
                        + ADJUSTMENTS [ CV(ID), CV(MID_PERIOD_METHOD)] + IMPAIRMENT_ASSET_AMOUNT [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           END
                     ,
                           -- reserve activity needs to take into account salvage amort if it has any (multiply salvage by the salvage treatment
                           --  So if salvage treatment is yes (1) the cash and returns are included.  If salvage treatment is no (0 they are not).
                           --  Since 1 times anything is anything.  And 0 times anything is zero
                           RESERVE_ACTIVITY [ ITERATION_NUMBER, any ] = case
                              when TRF_IN_EST_ADDS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 'no' then
                           RETIREMENTS [ CV(ID), CV(MID_PERIOD_METHOD) ] + SALVAGE_CASH [ CV(ID), CV(MID_PERIOD_METHOD)]
                           + SALVAGE_RETURNS [ CV(ID), CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [CV(ID), CV(MID_PERIOD_METHOD) ] +
                           RESERVE_ADJUSTMENTS [ CV(ID), CV(MID_PERIOD_METHOD)] - CURRENT_NET_SALVAGE_AMORT [ CV(ID), CV(MID_PERIOD_METHOD)]
                           + IMPAIRMENT_ASSET_AMOUNT [ CV(ID), CV(MID_PERIOD_METHOD)] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(ID), CV(MID_PERIOD_METHOD)]
                           - (SALVAGE_TREATMENT [ CV(ID), CV(MID_PERIOD_METHOD)] * (SALVAGE_CASH [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           + SALVAGE_RETURNS [CV(ID), CV(MID_PERIOD_METHOD) ]))
                              else
                               RETIREMENTS [ CV(ID), CV(MID_PERIOD_METHOD) ]  + GAIN_LOSS [ CV(ID), CV(MID_PERIOD_METHOD) ]
                        + SALVAGE_CASH [ CV(ID), CV(MID_PERIOD_METHOD)]
                        + SALVAGE_RETURNS [ CV(ID), CV(MID_PERIOD_METHOD) ] + RESERVE_CREDITS [CV(ID), CV(MID_PERIOD_METHOD) ]
                        + RESERVE_TRAN_IN [ CV(ID), CV(MID_PERIOD_METHOD)] + RESERVE_TRAN_OUT [ CV(ID), CV(MID_PERIOD_METHOD) ]
                        + RESERVE_ADJUSTMENTS [CV(ID), CV(MID_PERIOD_METHOD) ] - CURRENT_NET_SALVAGE_AMORT [ CV(ID), CV(MID_PERIOD_METHOD)]
                        + IMPAIRMENT_ASSET_AMOUNT [ CV(ID), CV(MID_PERIOD_METHOD)] + IMPAIRMENT_EXPENSE_AMOUNT [ CV(ID), CV(MID_PERIOD_METHOD)]
                        - (SALVAGE_TREATMENT [ CV(ID), CV(MID_PERIOD_METHOD)] * (SALVAGE_CASH [ CV(ID), CV(MID_PERIOD_METHOD) ]
                        + SALVAGE_RETURNS [CV(ID), CV(MID_PERIOD_METHOD) ]))
                           END
                     ,
                           COR_ACTIVITY [ ITERATION_NUMBER,any ] = case
                              when TRF_IN_EST_ADDS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 'no' then
                               COST_OF_REMOVAL [ CV(ID), CV(MID_PERIOD_METHOD) ] + COR_RES_ADJUST [CV(ID), CV(MID_PERIOD_METHOD) ]
                              else
                               COST_OF_REMOVAL [ CV(ID), CV(MID_PERIOD_METHOD) ] + COR_RES_TRAN_IN [CV(ID), CV(MID_PERIOD_METHOD) ]
                        + COR_RES_TRAN_OUT [ CV(ID), CV(MID_PERIOD_METHOD)] + COR_RES_ADJUST [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           END
                     ,

                           -- begin reserve needs to take into account cor and salvage treatment.
                           BEGIN_RESERVE [ ITERATION_NUMBER,any ] = case
                              when TRF_IN_EST_ADDS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 'no' then
                               BEGIN_RESERVE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                        + RESERVE_TRAN_IN [CV(ID), CV(MID_PERIOD_METHOD) ]
                        + RESERVE_TRAN_OUT [ CV(ID), CV(MID_PERIOD_METHOD)]
                        + (COR_TREATMENT [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           * (COR_BEG_RESERVE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           - DNSA_COR_BAL [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           - COR_YTD [ CV(ID), CV(MID_PERIOD_METHOD) ]))
                        - (SALVAGE_TREATMENT [ CV(ID), CV(MID_PERIOD_METHOD)]
                           * (DNSA_SALV_BAL [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           + SALV_YTD [ CV(ID), CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_RESERVE [ CV(ID), CV(MID_PERIOD_METHOD)]
                        + (COR_TREATMENT [ CV(ID), CV(MID_PERIOD_METHOD)]
                           * (COR_BEG_RESERVE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           - DNSA_COR_BAL [CV(ID), CV(MID_PERIOD_METHOD) ]
                           - COR_YTD [ CV(ID), CV(MID_PERIOD_METHOD) ]))
                        - (SALVAGE_TREATMENT [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           * (DNSA_SALV_BAL [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           + SALV_YTD [ CV(ID), CV(MID_PERIOD_METHOD) ]))
                           END
                     ,


                           --
                           --  Certain methods require net_gross = 1 if end of life is given
                           --
                           NET_GROSS [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('Monthly', 'End of Life', 'SMD') ] = case
                              when END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ] is not null then
                               1
                              else
                               NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           END
                     ,

                           -- if including RWIP and net_gross is 1
                           RWIP_SALVAGE [ITERATION_NUMBER, any ] = case
                              when INCLUDE_RWIP_IN_NET [ CV(ID), CV(MID_PERIOD_METHOD) ] = 'yes' and NET_GROSS [CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               RWIP_SALVAGE_CASH [ CV(ID), CV(MID_PERIOD_METHOD) ] + RWIP_SALVAGE_RETURNS [CV(ID), CV(MID_PERIOD_METHOD) ]
                        + RWIP_RESERVE_CREDITS [ CV(ID), CV(MID_PERIOD_METHOD) ]
                              else
                               0
                           end,
                           RWIP_COR [ ITERATION_NUMBER,any ] = case
                              when INCLUDE_RWIP_IN_NET [ CV(ID), CV(MID_PERIOD_METHOD) ] = 'yes' and NET_GROSS [
                               CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               RWIP_COST_OF_REMOVAL [ CV(ID), CV(MID_PERIOD_METHOD) ]
                              else
                               0
                           END
                     ,

                           -- remaining months if end of life provided (min value of 1)
                           -- ELSE use a 0
                           REMAINING_MONTHS [ITERATION_NUMBER, any ] = case
                              when END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ] is not null then
                               GREATEST(1,
                                        1 +
                                        MONTHS_BETWEEN(TO_DATE(TO_CHAR(END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ]),
                                                               'yyyymm'),
                                                       CALC_MONTH [ CV(ID), CV(MID_PERIOD_METHOD) ]))
                              else
                               0
                           END
                     ,

                           -- Start mid_period_method rate calcs

                           -- GET The depreciation rates for MONTHLY.
                           DEPRECIATION_RATE [ITERATION_NUMBER, 'Monthly' ] = RATE [ CV(ID),'Monthly' ],
                           SALVAGE_RATE [ ITERATION_NUMBER,'Monthly' ] = DMR_SALVAGE_RATE [ CV(ID),'Monthly' ],
                           COST_OF_REMOVAL_RATE [ ITERATION_NUMBER,'Monthly' ] = DMR_COST_OF_REMOVAL_RATE [ CV(ID),'Monthly' ],

                           -- GET The depreciation rates for END OF LIFE.  Salv rate and cor rate are same
                           DEPRECIATION_RATE [ ITERATION_NUMBER,'End of Life'
                           ] = 12 / GREATEST(1, REMAINING_MONTHS [ CV(ID),'End of Life' ]),
                           SALVAGE_RATE [ ITERATION_NUMBER, 'End of Life' ] = DEPRECIATION_RATE [ CV(ID),'End of Life' ],
                           COST_OF_REMOVAL_RATE [ ITERATION_NUMBER, 'End of Life' ] = DEPRECIATION_RATE [ CV(ID),'End of Life' ],

                           -- GET the depreciation rates for SMD
                           DEPRECIATION_RATE [ITERATION_NUMBER, 'SMD' ] = case
                              when END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ] is null then
                               RATE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                              else
                               12 * (REMAINING_MONTHS [ CV(ID),'SMD' ] / ((REMAINING_MONTHS [ CV(ID),'SMD'
                                 ] * (REMAINING_MONTHS [ CV(ID),'SMD' ] + 1)) / 2))
                           END
                     ,
                           SALVAGE_RATE [ ITERATION_NUMBER,'SMD' ] = case
                              when END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ] is null then
                               DMR_SALVAGE_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                              else -- same as depreciation rate
                               DEPRECIATION_RATE [ CV(ID),'SMD' ]
                           end,
                           COST_OF_REMOVAL_RATE [ITERATION_NUMBER, 'SMD' ] = case
                              when END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ] is null then
                               DMR_COST_OF_REMOVAL_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                              else -- same as depreciation rate
                               DEPRECIATION_RATE [ CV(ID),'SMD' ]
                           END
                     ,

                           -- take into account net salvage now for end of life provided
                           DEPRECIATION_RATE [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('End of Life', 'SMD') ] = case
                              when END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ] is not null and NET_SALVAGE_PCT [
                               CV(ID), CV(MID_PERIOD_METHOD) ] <> 1 then
                               DEPRECIATION_RATE [ CV(ID), CV(MID_PERIOD_METHOD)] / (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                              else
                               DEPRECIATION_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           end,
                           SALVAGE_RATE [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('End of Life', 'SMD') ] = case
                              when END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ] is not null then
                               case
                                  when NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] <> 1 then
                                   SALVAGE_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [CV(ID), CV(MID_PERIOD_METHOD)]
                           * -1 / (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                                  else
                                   0
                               end
                              else
                               SALVAGE_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                           END
                     ,

                           --GET the depreciation rates for Quarterly, QTR
                           DEPRECIATION_RATE [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = RATE [CV(ID), CV(MID_PERIOD_METHOD) ],
                           SALVAGE_RATE [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('Quarterly', 'QTR')]
                        = DMR_SALVAGE_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ],
                           COST_OF_REMOVAL_RATE [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('Quarterly', 'QTR')]
                        = DMR_COST_OF_REMOVAL_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ],

                           --GET the depreciation rates for Yearly
                           DEPRECIATION_RATE [ ITERATION_NUMBER,'Yearly' ] = RATE [ CV(ID),'Yearly' ],
                           SALVAGE_RATE [ ITERATION_NUMBER,'Yearly' ] = DMR_SALVAGE_RATE [ CV(ID),'Yearly' ],
                           COST_OF_REMOVAL_RATE [ ITERATION_NUMBER,'Yearly' ] = DMR_COST_OF_REMOVAL_RATE [ CV(ID),'Yearly' ]
                     ,

                           --GET the depreciation rates for SFY
                           DEPRECIATION_RATE [ ITERATION_NUMBER,'SFY' ] = RATE [ CV(ID),'SFY' ],
                           SALVAGE_RATE [ ITERATION_NUMBER,'SFY' ] = DMR_SALVAGE_RATE [ CV(ID),'SFY' ],
                           COST_OF_REMOVAL_RATE [ ITERATION_NUMBER,'SFY' ] = DMR_COST_OF_REMOVAL_RATE [ CV(ID),'SFY' ],

                           --GET the depreciation rates for curve
                           DEPRECIATION_RATE [ ITERATION_NUMBER,'curve' ] = RATE [ CV(ID),'curve' ],
                           SALVAGE_RATE [ ITERATION_NUMBER,'curve' ] = DMR_SALVAGE_RATE [ CV(ID),'curve' ],
                           COST_OF_REMOVAL_RATE [ ITERATION_NUMBER,'curve' ] = DMR_COST_OF_REMOVAL_RATE [CV(ID), 'curve' ]
                     ,

                           --GET the depreciation rates for curve_no_true_up
                           DEPRECIATION_RATE [ ITERATION_NUMBER,'curve_no_true_up' ] = RATE [ CV(ID),'curve_no_true_up' ],
                           SALVAGE_RATE [ ITERATION_NUMBER,'curve_no_true_up' ] = DMR_SALVAGE_RATE [CV(ID),'curve_no_true_up' ],
                           COST_OF_REMOVAL_RATE [ ITERATION_NUMBER,'curve_no_true_up' ] = DMR_COST_OF_REMOVAL_RATE [CV(ID),'curve_no_true_up' ],

                           --GET the depreciation rates for UOP
                           DEPRECIATION_RATE [ITERATION_NUMBER, 'UOP' ] = case
                              when END_OF_LIFE [ CV(ID), CV(MID_PERIOD_METHOD) ] is not null and NET_GROSS [CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                              --if net/gross is net then end-of-life will be used to derive rate
                               12 / REMAINING_MONTHS [ CV(ID),'UOP' ]
                              else
                              --if net/gross is gross then end-of-life is not used
                               RATE [ CV(ID),'UOP' ]
                           end,

                           DEPRECIATION_UOP_RATE [ITERATION_NUMBER, 'UOP' ] = case
                              when ESTIMATED_PRODUCTION [ CV(ID), CV(MID_PERIOD_METHOD) ] <> 0 then
                               (PRODUCTION [ CV(ID), CV(MID_PERIOD_METHOD) ] / ESTIMATED_PRODUCTION [CV(ID), CV(MID_PERIOD_METHOD) ]) * 12
                              else
                               12
                           END
                     ,

                           DEPRECIATION_UOP_RATE_2 [ITERATION_NUMBER, 'UOP' ] = case
                              when TYPE_2_EXIST [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1
                       and ESTIMATED_PRODUCTION_2 [CV(ID), CV(MID_PERIOD_METHOD) ] <> 0 then
                        (PRODUCTION_2 [ CV(ID), CV(MID_PERIOD_METHOD) ] / ESTIMATED_PRODUCTION_2 [CV(ID), CV(MID_PERIOD_METHOD) ]) * 12
                              when TYPE_2_EXIST [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 and ESTIMATED_PRODUCTION_2 [CV(ID), CV(MID_PERIOD_METHOD) ] = 0 then
                        12
                              else
                        null
                           end,
                           -- unsure of salvage_rate and cor_rate for UOP
                           SALVAGE_RATE [ ITERATION_NUMBER,'UOP' ] = DMR_SALVAGE_RATE [ CV(ID),'UOP' ],
                           COST_OF_REMOVAL_RATE [ITERATION_NUMBER, 'UOP' ] = DMR_COST_OF_REMOVAL_RATE [ CV(ID),'UOP' ],

                           -- Start mid_period_method base calcs

                           --GET the depreciation bases for Monthly, End of Life, and SMD
                           DEPRECIATION_BASE [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('Monthly', 'End of Life', 'SMD')]
                     = case
                        when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                           (BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD)] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                           + ( impairment_asset_begin_balance[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                           - BEGIN_RESERVE [CV(ID), CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(ID), CV(MID_PERIOD_METHOD) ])
                           + (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)] * (PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD)]
                           * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                           + ( impairment_asset_activity_salv[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                           - RESERVE_ACTIVITY [CV(ID), CV(MID_PERIOD_METHOD) ]))
                        else
                           BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD)]
                           + (PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [CV(ID), CV(MID_PERIOD_METHOD) ])
                        END
                        ,

                           -- salvage base is the same as depreciation base
                           SALVAGE_BASE [ITERATION_NUMBER, MID_PERIOD_METHOD in ('Monthly', 'End of Life', 'SMD')]
                        = DEPRECIATION_BASE [ CV(ID), CV(MID_PERIOD_METHOD) ],

                           COST_OF_REMOVAL_BASE [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('Monthly', 'End of Life', 'SMD') ]
                     = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                CV(ID), CV(MID_PERIOD_METHOD) ] - COR_BEG_RESERVE [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] - RWIP_COR [ CV(ID), CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                 CV(ID), CV(MID_PERIOD_METHOD) ] - COR_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD)] + (PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD) ]
                        * MID_PERIOD_CONV [CV(ID), CV(MID_PERIOD_METHOD) ])
                           END
                     ,


                           --GET the depreciation bases for Quarterly, QTR
                           DEPRECIATION_BASE [ITERATION_NUMBER, MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE_QTR [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                        + ( impairment_asset_begin_balance[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                        - BEGIN_RESERVE_QTR [
                                CV(ID), CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(ID), CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD)
                                 ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                           + ( impairment_asset_activity_salv[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                           - round(RESERVE_ACTIVITY_2 [CV(ID), CV(MID_PERIOD_METHOD) ],2)))
                              else
                               BEGIN_BALANCE_QTR [ CV(ID), CV(MID_PERIOD_METHOD)
                               ] + (round(PLANT_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD) ],2) * MID_PERIOD_CONV [
                                CV(ID), CV(MID_PERIOD_METHOD) ])
                           end,


                           SALVAGE_BASE [ITERATION_NUMBER, MID_PERIOD_METHOD in ('Quarterly', 'QTR')]
                        = DEPRECIATION_BASE [ CV(ID), CV(MID_PERIOD_METHOD) ],

                           COST_OF_REMOVAL_BASE [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('Quarterly', 'QTR') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE_QTR [ CV(ID), CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                CV(ID), CV(MID_PERIOD_METHOD) ] - BEGIN_COR_QTR [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] - RWIP_COR [ CV(ID), CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (round(PLANT_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD) ],2) * COST_OF_REMOVAL_PCT [
                                 CV(ID), CV(MID_PERIOD_METHOD) ] - round(COR_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD) ],2)))
                              else
                               BEGIN_BALANCE_QTR [ CV(ID), CV(MID_PERIOD_METHOD)] +
                        (round(PLANT_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD) ],2) * MID_PERIOD_CONV [CV(ID), CV(MID_PERIOD_METHOD) ])
                           end,

                           --GET the depreciation bases for Yearly, SFY
                           DEPRECIATION_BASE [ITERATION_NUMBER, MID_PERIOD_METHOD in ('Yearly', 'SFY') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE_YEAR [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                        + ( impairment_asset_begin_balance[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                        - BEGIN_RESERVE_YEAR [
                                CV(ID), CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(ID), CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (round(PLANT_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD)],2)
                        * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                           + ( impairment_asset_activity_salv[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                           - round(RESERVE_ACTIVITY_2 [CV(ID), CV(MID_PERIOD_METHOD) ],2)))
                              else
                               BEGIN_BALANCE_YEAR [ CV(ID), CV(MID_PERIOD_METHOD)
                               ] + (round(PLANT_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD) ],2) * MID_PERIOD_CONV [CV(ID), CV(MID_PERIOD_METHOD) ])
                           end,


                           SALVAGE_BASE [ITERATION_NUMBER, MID_PERIOD_METHOD in ('Yearly', 'SFY')]
                        = DEPRECIATION_BASE [ CV(ID), CV(MID_PERIOD_METHOD) ],

                           COST_OF_REMOVAL_BASE [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('Yearly', 'SFY') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE_YEAR [ CV(ID), CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                CV(ID), CV(MID_PERIOD_METHOD) ] -
                               --not sure if cor_beg_reserve is right. Might need some sort of "year" equivalent
                                BEGIN_COR_YEAR [ CV(ID), CV(MID_PERIOD_METHOD) ] - RWIP_COR [ CV(ID), CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD) ] * COST_OF_REMOVAL_PCT [
                                 CV(ID), CV(MID_PERIOD_METHOD) ] - COR_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE_YEAR [ CV(ID), CV(MID_PERIOD_METHOD)
                               ] + (PLANT_ACTIVITY_2 [ CV(ID), CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [CV(ID), CV(MID_PERIOD_METHOD) ])
                           end,


                           --GET the depreciation bases for curve, curve_no_true_up
                           DEPRECIATION_BASE [ITERATION_NUMBER, MID_PERIOD_METHOD in ('curve', 'curve_no_true_up') ] = case
                              when TRF_IN_EST_ADDS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 'no' then
                               BEGIN_BALANCE_YEAR [ CV(ID), CV(MID_PERIOD_METHOD) ] + CUMULATIVE_TRANSFERS [
                               CV(ID), CV(MID_PERIOD_METHOD) ] + (EST_NET_ADDS [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD) ])
                              else
                               BEGIN_BALANCE_YEAR [ CV(ID), CV(MID_PERIOD_METHOD)
                               ] + (EST_NET_ADDS [ CV(ID), CV(MID_PERIOD_METHOD) ] * MID_PERIOD_CONV [
                                CV(ID), CV(MID_PERIOD_METHOD) ])
                           end,


                           SALVAGE_BASE [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('curve', 'curve_no_true_up')]
                        = DEPRECIATION_BASE [ CV(ID), CV(MID_PERIOD_METHOD) ],

                           --not sure of curve, curve_no_true_up COR base so using depr base for now.
                           COST_OF_REMOVAL_BASE [ ITERATION_NUMBER,MID_PERIOD_METHOD in ('curve', 'curve_no_true_up')]
                        = DEPRECIATION_BASE [ CV(ID), CV(MID_PERIOD_METHOD) ],

                           --GET the depreciation bases for UOP
                           DEPRECIATION_BASE [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                        + ( impairment_asset_begin_balance[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                        - BEGIN_RESERVE [
                                CV(ID), CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(ID), CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD)
                                 ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                           + ( impairment_asset_activity_salv[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                           - RESERVE_ACTIVITY [CV(ID), CV(MID_PERIOD_METHOD) ]))
                              else
                               BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD)] + (PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD) ]
                        * MID_PERIOD_CONV [CV(ID), CV(MID_PERIOD_METHOD) ])
                           end,

                           DEPRECIATION_BASE_UOP [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               (BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                        + ( impairment_asset_begin_balance[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                        - RESERVE_DIFF [
                                CV(ID), CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] - CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD) ]) +
                               (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)
                                ] * (PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD)
                                 ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                           + ( impairment_asset_activity_salv[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                           - RESERVE_ACTIVITY [CV(ID), CV(MID_PERIOD_METHOD) ]))
                           end,

                           DEPRECIATION_BASE_UOP_2 [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               case
                                  when TYPE_2_EXIST [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                                   (BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                           + ( impairment_asset_begin_balance[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                           - RESERVE_DIFF [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] - RWIP_SALVAGE [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] - CUM_UOP_DEPR_2 [ CV(ID), CV(MID_PERIOD_METHOD) ]) +
                                   (MID_PERIOD_CONV [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] * (PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD)
                                     ] * (1 - NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ])
                            + ( impairment_asset_activity_salv[ CV(ID), CV(MID_PERIOD_METHOD) ] * NET_SALVAGE_PCT [ CV(ID), CV(MID_PERIOD_METHOD) ] )
                           - RESERVE_ACTIVITY [
                                     CV(ID), CV(MID_PERIOD_METHOD) ]))
                               end
                           end,

                           SALVAGE_BASE [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ]
                        = DEPRECIATION_BASE [CV(ID), CV(MID_PERIOD_METHOD) ],
                           --not sure of UOP COR base so using depr base for now.
                           COST_OF_REMOVAL_BASE [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ]
                        = DEPRECIATION_BASE [CV(ID), CV(MID_PERIOD_METHOD) ],

                           --BEGIN THE CALCS TO DETERMINE WHICH UOP BASE AND RATE TO USE
                           -- uop rate determining calc is here instead of up with rates because curr_uop_exp (_2) need all the bases and rates calculated before they can be calculated
                           CURR_UOP_EXP [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               DEPRECIATION_BASE_UOP [ CV(ID),'UOP' ] * (DEPRECIATION_UOP_RATE [CV(ID), 'UOP' ] / 12)
                              else
                               DEPRECIATION_BASE [ CV(ID),'UOP' ] * (DEPRECIATION_UOP_RATE [ CV(ID),'UOP' ] / 12)
                           end,

                           CURR_UOP_EXP_2 [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ] = case
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 and TYPE_2_EXIST [CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               DEPRECIATION_BASE_UOP_2 [ CV(ID),'UOP' ] * (DEPRECIATION_UOP_RATE_2 [ CV(ID),'UOP' ] / 12)
                              when NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] <> 1 and TYPE_2_EXIST [CV(ID), CV(MID_PERIOD_METHOD) ] = 1 then
                               DEPRECIATION_BASE [ CV(ID),'UOP' ] * (DEPRECIATION_UOP_RATE_2 [ CV(ID),'UOP' ] / 12)
                           end,

                           DEPRECIATION_RATE [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ] = case
                              when ((NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1) and
                                   ((MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] = YTD_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 2 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] = CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 3 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] = CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ] - LESS_YEAR_UOP_DEPR [
                                    CV(ID), CV(MID_PERIOD_METHOD) ]))) then
                               DEPRECIATION_UOP_RATE [ CV(ID),'UOP' ]
                              when ((NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 and TYPE_2_EXIST [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] = 1) and
                                   ((MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] <> YTD_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 2 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] <> CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 3 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] <> CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ] - LESS_YEAR_UOP_DEPR [
                                    CV(ID), CV(MID_PERIOD_METHOD) ]))) then
                               DEPRECIATION_UOP_RATE_2 [ CV(ID),'UOP' ]
                              else
                               DEPRECIATION_RATE [CV(ID), 'UOP' ]
                           end,

                           DEPRECIATION_BASE [ ITERATION_NUMBER, MID_PERIOD_METHOD in ('UOP') ] = case
                              when ((NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1) and
                                   ((MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] = YTD_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 2 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] = CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 3 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] = CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ] - LESS_YEAR_UOP_DEPR [
                                    CV(ID), CV(MID_PERIOD_METHOD) ]))) then
                               DEPRECIATION_BASE_UOP [ CV(ID),'UOP' ]
                              when ((NET_GROSS [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 and TYPE_2_EXIST [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] = 1) and
                                   ((MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 1 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] <> YTD_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 2 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] <> CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ]) or
                                   (MIN_CALC [ CV(ID), CV(MID_PERIOD_METHOD) ] = 3 and MIN_UOP_EXP [
                                    CV(ID), CV(MID_PERIOD_METHOD) ] <> CUM_UOP_DEPR [ CV(ID), CV(MID_PERIOD_METHOD)
                                    ] + CURR_UOP_EXP [ CV(ID), CV(MID_PERIOD_METHOD) ] - LESS_YEAR_UOP_DEPR [
                                    CV(ID), CV(MID_PERIOD_METHOD) ]))) then
                               DEPRECIATION_BASE_UOP_2 [ CV(ID),'UOP' ]
                              else
                               DEPRECIATION_BASE [ CV(ID),'UOP' ]
                           END
                     ,

                     --calculate expenses
                           DEPRECIATION_EXPENSE [ ITERATION_NUMBER,any ]
                        = round(DEPRECIATION_BASE [ CV(ID), CV(MID_PERIOD_METHOD)] * DEPRECIATION_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ] / 12,2),

                           SALVAGE_EXPENSE [ ITERATION_NUMBER,any ] = case SALVAGE_TREATMENT [CV(ID),CV(MID_PERIOD_METHOD)]
                        when 0 then
                           round(SALVAGE_BASE [ CV(ID), CV(MID_PERIOD_METHOD)] * SALVAGE_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ] / 12,2)
                        else -- SALVAGE_TREATMENT is non-zero - expense already calculated in amortization logic.
                           SALVAGE_EXPENSE [ CV(ID), CV(MID_PERIOD_METHOD)]
                        end
                  ,

                           COR_EXPENSE [ ITERATION_NUMBER,any ] = case COR_TREATMENT [CV(ID),CV(MID_PERIOD_METHOD)]
                        when 0 then
                           round(COST_OF_REMOVAL_BASE [ CV(ID), CV(MID_PERIOD_METHOD)] * COST_OF_REMOVAL_RATE [ CV(ID), CV(MID_PERIOD_METHOD) ] / 12,2)
                        else -- COR_TREATMENT is non-zero - expense already calculated in amortization logic.
                           COR_EXPENSE [ CV(ID), CV(MID_PERIOD_METHOD)]
                        end
                     ,

                     --figure out retro columns
                     RETRO_DEPR_ADJ[ITERATION_NUMBER, ANY] =
                        CASE WHEN rate_used_code [cv(),cv()] = 2 AND GL_POST_MO_YR [cv(),cv()] <> CALC_MONTH [cv(),cv()] THEN
                          nvl(RETRO_DEPR_ADJ[CV() - 1, CV()],0) + nvl(DEPRECIATION_EXPENSE[CV(),CV()],0)
                           + nvl(DEPR_EXP_ALLOC_ADJUST[CV(),CV()],0) - (nvl(ORIG_DEPR_EXPENSE[CV(),CV()],0) + nvl(ORIG_DEPR_EXP_ALLOC_ADJUST[CV(),CV()],0))
                       WHEN rate_used_code [cv(),cv()] = 2 AND GL_POST_MO_YR [cv(),cv()] = CALC_MONTH [cv(),cv()] THEN
                          nvl(RETRO_DEPR_ADJ[CV() - 1, CV()],0)
                       ELSE 0
                       END,
                     RETRO_COR_ADJ[ITERATION_NUMBER, ANY] =
                        CASE WHEN rate_used_code [cv(),cv()] = 2 AND GL_POST_MO_YR [cv(),cv()] <> CALC_MONTH [cv(),cv()] THEN
                          nvl(RETRO_COR_ADJ[CV() - 1, CV()],0) + nvl(COR_EXPENSE[CV(),CV()],0)
                           + nvl(COR_EXP_ALLOC_ADJUST[CV(),CV()],0) - (nvl(ORIG_COR_EXPENSE[CV(),CV()],0) + nvl(ORIG_COR_EXP_ALLOC_ADJUST[CV(),CV()],0))
                       WHEN rate_used_code [cv(),cv()] = 2 AND GL_POST_MO_YR [cv(),cv()] = CALC_MONTH [cv(),cv()] THEN
                          nvl(RETRO_COR_ADJ[CV() - 1, CV()],0)
                       ELSE 0
                       END,
                     RETRO_SALV_ADJ[ITERATION_NUMBER, ANY] =
                        CASE WHEN rate_used_code [cv(),cv()] = 2 AND GL_POST_MO_YR [cv(),cv()] <> CALC_MONTH [cv(),cv()] THEN
                          nvl(RETRO_SALV_ADJ[CV() - 1, CV()],0) + nvl(SALVAGE_EXPENSE[CV(),CV()],0)
                           + nvl(SALVAGE_EXP_ALLOC_ADJUST[CV(),CV()],0) - (nvl(ORIG_SALV_EXPENSE[CV(),CV()],0) + nvl(ORIG_SALV_EXP_ALLOC_ADJUST[CV(),CV()],0))
                       WHEN rate_used_code [cv(),cv()] = 2 AND GL_POST_MO_YR [cv(),cv()] = CALC_MONTH [cv(),cv()] THEN
                          nvl(RETRO_SALV_ADJ[CV() - 1, CV()],0)
                       ELSE 0
                       END
                       ,

                           -- ALL Mid Period Methods - calc end balances
                           END_BALANCE [ ITERATION_NUMBER, any ]
                        = BEGIN_BALANCE [ CV(ID), CV(MID_PERIOD_METHOD)] + PLANT_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD) ]
                     ,
                           END_RESERVE [ ITERATION_NUMBER,any ]
                        = decode(GL_POST_MO_YR[CV(),CV()], CALC_MONTH[CV(),CV()], ORIG_BEGIN_RESERVE [ CV(ID), CV(MID_PERIOD_METHOD)], BEGIN_RESERVE[CV(),CV()]) + RESERVE_ACTIVITY[CV(ID), CV(MID_PERIOD_METHOD) ]
                         + DEPRECIATION_EXPENSE [ CV(ID), CV(MID_PERIOD_METHOD) ] + SALVAGE_EXPENSE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                         + DEPR_EXP_ADJUST [ CV(ID), CV(MID_PERIOD_METHOD) ] + decode(GL_POST_MO_YR[CV(),CV()], CALC_MONTH[CV(),CV()], RETRO_DEPR_ADJ [ CV(ID), CV(MID_PERIOD_METHOD) ], 0)
                         + SALVAGE_EXP_ADJUST [ CV(ID), CV(MID_PERIOD_METHOD) ] + decode(GL_POST_MO_YR[CV(),CV()], CALC_MONTH[CV(),CV()], RETRO_SALV_ADJ [ CV(ID), CV(MID_PERIOD_METHOD) ], 0)
                         ,
                           COR_END_RESERVE [ ITERATION_NUMBER,any ]
                        = decode(GL_POST_MO_YR[CV(),CV()], CALC_MONTH[CV(),CV()], ORIG_COR_BEG_RESERVE [ CV(ID), CV(MID_PERIOD_METHOD)], COR_BEG_RESERVE[CV(),CV()]) + COR_EXPENSE [ CV(ID), CV(MID_PERIOD_METHOD) ]
                         + COR_ACTIVITY [ CV(ID), CV(MID_PERIOD_METHOD) ] + decode(GL_POST_MO_YR[CV(),CV()], CALC_MONTH[CV(),CV()], RETRO_COR_ADJ [ CV(ID), CV(MID_PERIOD_METHOD) ], 0)
                     ,

                     --over depr check - compare end balances to end_reserves where necessary.
                     OVER_DEPR_ADJ [ ITERATION_NUMBER, any ] =
                           case
                           when OVER_DEPR_CHECK [ CV(), CV() ] in (1,2) then
                              --life over_deprs
                              F_OVERDEPR_SINGLE_ROW(
                                 END_BALANCE[CV(),CV()],
                                 END_RESERVE[CV(),CV()],
                                 DEPRECIATION_EXPENSE[CV(),CV()]
                                    + SALVAGE_EXPENSE[CV(),CV()]
                                    + decode(GL_POST_MO_YR[CV(),CV()], CALC_MONTH[CV(),CV()], RETRO_DEPR_ADJ[CV(),CV()] + RETRO_SALV_ADJ[CV(),CV()], 0),
                                 NET_SALVAGE_PCT[CV(),CV()]
                                 )
                           when OVER_DEPR_CHECK [ CV(), CV() ] = 3 then
                              --combined life and cor limit
                              --Do not prorate yet - we will use this value to help calculate our COR over depr, then refoot this column
                              F_OVERDEPR_SINGLE_ROW(
                                 END_BALANCE[CV(),CV()],
                                 END_RESERVE[CV(),CV()] + COR_END_RESERVE[CV(),CV()],
                                 DEPRECIATION_EXPENSE[CV(),CV()]
                                    + SALVAGE_EXPENSE[CV(),CV()]
                                    + COR_EXPENSE[CV(),CV()]
                                    + decode(GL_POST_MO_YR[CV(),CV()],
                                       CALC_MONTH[CV(),CV()],
                                       RETRO_DEPR_ADJ[CV(),CV()]
                                          + RETRO_SALV_ADJ[CV(),CV()]
                                          + RETRO_COR_ADJ[CV(),CV()], 0),
                                 NET_SALVAGE_PCT[CV(),CV()] + COST_OF_REMOVAL_PCT[CV(),CV()])
                           when OVER_DEPR_CHECK [ CV(), CV() ] = 4 then -9999999999
                              --combined depr_groups TODO
                           else 0 --no over depr check
                           end
                        ,
                     OVER_DEPR_ADJ_COR [ ITERATION_NUMBER, any ] =
                           case
                           when OVER_DEPR_CHECK [ CV(), CV() ] = 1 then
                              --separate life and cor over depr - do COR
                              F_OVERDEPR_SINGLE_ROW(
                                 END_BALANCE[CV(),CV()],
                                 COR_END_RESERVE[CV(),CV()],
                                 COR_EXPENSE[CV(),CV()]
                                    + decode(GL_POST_MO_YR[CV(),CV()],
                                       CALC_MONTH[CV(),CV()],
                                       RETRO_COR_ADJ[CV(),CV()], 0),
                                 COST_OF_REMOVAL_PCT[CV(),CV()])
                           when OVER_DEPR_CHECK [ CV(), CV() ] = 3 then
                              --combined life and cor over_depr
                              -- the value calculated for life has not been prorated yet,
                              --    so we will prorate here and then adjust subtract this value
                              -- from the current life value to get the actual life value
                              OVER_DEPR_ADJ[CV(),CV()]
                              * (COR_EXPENSE[CV(),CV()] + RETRO_COR_ADJ[CV(),CV()])
                              / (DEPRECIATION_EXPENSE[CV(),CV()]
                                    + SALVAGE_EXPENSE[CV(),CV()]
                                    + COR_EXPENSE[CV(),CV()]
                                    + decode(GL_POST_MO_YR[CV(),CV()],
                                       CALC_MONTH[CV(),CV()],
                                       RETRO_DEPR_ADJ[CV(),CV()]
                                          + RETRO_SALV_ADJ[CV(),CV()]
                                          + RETRO_COR_ADJ[CV(),CV()], 0))
                           when OVER_DEPR_CHECK [ CV(), CV() ] = 4 then -9999999999
                              --combined depr_groups TODO
                           else 0 --over_depr = 0 or 2, either no over_depr or life-only over_depr
                           end
                        ,
                     --refoot life OVER_DEPR based on prorated COR overdepr
                     OVER_DEPR_ADJ[ITERATION_NUMBER,any] = case
                        when OVER_DEPR_CHECK[CV(),CV()] = 3 and OVER_DEPR_ADJ_COR[CV(),CV()] <> 0 then
                           OVER_DEPR_ADJ[CV(),CV()] - OVER_DEPR_ADJ_COR[CV(),CV()]
                        else OVER_DEPR_ADJ[CV(),CV()]  -- else leave the same.
                        end,
                     --prorate over_depr_salv
                     OVER_DEPR_ADJ_SALV[ITERATION_NUMBER,any] = case
                        when OVER_DEPR_CHECK[CV(),CV()] in (1,2,3)
                          and OVER_DEPR_ADJ[CV(),CV()] <> 0 and SALVAGE_EXPENSE[CV(),CV()] <> 0 then
                           OVER_DEPR_ADJ[CV(),CV()]
                           * SALVAGE_EXPENSE[CV(),CV()]
                           / (DEPRECIATION_EXPENSE[CV(),CV()]
                              + SALVAGE_EXPENSE[CV(),CV()]
                              + decode(GL_POST_MO_YR[CV(),CV()],
                                    CALC_MONTH[CV(),CV()],
                                    RETRO_DEPR_ADJ[CV(),CV()] + RETRO_SALV_ADJ[CV(),CV()], 0))
                        else 0
                        end,
                     --refoot life OVER_DEPR based on prorated SALV overdepr
                     OVER_DEPR_ADJ[ITERATION_NUMBER,any] = case
                        when OVER_DEPR_ADJ_SALV[CV(),CV()] <> 0 then
                           OVER_DEPR_ADJ[CV(),CV()] - OVER_DEPR_ADJ_SALV[CV(),CV()]
                        else OVER_DEPR_ADJ[CV(),CV()]  -- else leave the same.
                        end,

                     --refoot end balances after over depr check
                     END_RESERVE[ITERATION_NUMBER,any] = END_RESERVE[CV(),CV()] + OVER_DEPR_ADJ[CV(),CV()] + OVER_DEPR_ADJ_SALV[CV(),CV()],
                     COR_END_RESERVE[ITERATION_NUMBER,any] = COR_END_RESERVE[CV(),CV()] + OVER_DEPR_ADJ_COR[CV(),CV()],


                           --Set Next month balances
                           BEGIN_BALANCE[ITERATION_NUMBER + 1, ANY] = END_BALANCE[CV(ID) - 1, CV(MID_PERIOD_METHOD)],
                           BEGIN_RESERVE[ITERATION_NUMBER + 1, ANY] = END_RESERVE[CV(ID) - 1, CV(MID_PERIOD_METHOD)],
                     BEGIN_RESERVE_QTR[ITERATION_NUMBER + 1, MID_PERIOD_METHOD in ('QTR', 'Quarterly')]
                     = case --Add the difference for the last three months if this is the start of a new quarter
                        when GL_POST_MO_YR[CV(), CV()] = FISCALQTRSTART[CV(),CV()] then
                           END_RESERVE[CV() - 1,CV()]
                        else
                           BEGIN_RESERVE_QTR[CV() - 1, CV()]
                        end,

                     BEGIN_RESERVE_YEAR[ITERATION_NUMBER + 1, MID_PERIOD_METHOD in ('Yearly','SFY')]
                     = case --Add the difference for the last 12 months if this is the start of a new YEAR
                        when GL_POST_MO_YR[CV(),CV()] = FISCALYEARSTART[CV(),CV()] then
                           END_RESERVE[CV() - 1, CV()]
                        else
                           BEGIN_RESERVE_YEAR[CV() - 1, CV()]
                        end,
                     --TODO we probably need to update the COR_BEG_RES_YEAR AND COR_BEG_RES_QTR columns as we do reserve above.
                           COR_BEG_RESERVE[ITERATION_NUMBER + 1, ANY] = COR_END_RESERVE[CV(ID) - 1, CV(MID_PERIOD_METHOD)]
            )) B
      on (A.DEPR_GROUP_ID = B.DEPR_GROUP_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POST_MO_YR = B.GL_POST_MO_YR)
      when matched then
         update
            set END_BALANCE = B.END_BALANCE,
            DEPRECIATION_BASE = B.DEPRECIATION_BASE,
                END_RESERVE = B.END_RESERVE,
                COR_END_RESERVE = B.COR_END_RESERVE,
                DEPRECIATION_RATE = B.DEPRECIATION_RATE,
                DEPRECIATION_EXPENSE = B.DEPRECIATION_EXPENSE,
            SALVAGE_BASE = B.SALVAGE_BASE,
                SALVAGE_RATE = B.SALVAGE_RATE,
            SALVAGE_EXPENSE = B.SALVAGE_EXPENSE,
                COST_OF_REMOVAL_BASE = B.COST_OF_REMOVAL_BASE,
                COST_OF_REMOVAL_RATE = B.COST_OF_REMOVAL_RATE,
            COR_EXPENSE = B.COR_EXPENSE,
                DEPR_CALC_STATUS = 5,
            DEPR_CALC_MESSAGE = 'Calculated',
            RETRO_DEPR_ADJ = B.RETRO_DEPR_ADJ,
            RETRO_COR_ADJ = B.RETRO_COR_ADJ,
            RETRO_SALV_ADJ = B.RETRO_SALV_ADJ,
            PLANT_ACTIVITY = B.PLANT_ACTIVITY,
            RESERVE_ACTIVITY = B.RESERVE_ACTIVITY,
            COR_ACTIVITY = B.COR_ACTIVITY,
            OVER_DEPR_ADJ = B.OVER_DEPR_ADJ,
            OVER_DEPR_ADJ_COR = B.OVER_DEPR_ADJ_COR,
            OVER_DEPR_ADJ_SALV = B.OVER_DEPR_ADJ_SALV,
            BEGIN_BALANCE = B.BEGIN_BALANCE,
            BEGIN_RESERVE = B.BEGIN_RESERVE,
            COR_BEG_RESERVE = B.COR_BEG_RESERVE
         ;
      --'depr_exp_alloc_adjust', depr_exp_adj + retro_depr_exp_ad
      --'salvage_exp_alloc_adjust', salv_exp_adj + retro_salv_exp_adj
      --'cor_end_reserve', (begin_cor_month + cor_act + cor_prov + retro_cor_exp_adj + cor_exp_adj + cor_exp_adjust_in)
      --'cor_exp_alloc_adjust', retro_cor_exp_adj + cor_exp_adj

     PKG_PP_LOG.P_WRITE_MESSAGE('ENDING Depreciation Calculation');

      update DEPR_CALC_STG set DEPR_CALC_STATUS = 9, DEPR_CALC_MESSAGE = 'End of Calc';
   exception
      when others then
         L_STATUS := sqlerrm;
         update DEPR_CALC_STG
            set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Calc: ' || L_STATUS;
   end P_DEPRCALC;

   function F_OVERDEPR_SINGLE_ROW ( A_END_BALANCE     DEPR_LEDGER.END_BALANCE%TYPE,
                              A_END_RESERVE     DEPR_LEDGER.END_RESERVE%TYPE,
                              A_EXPENSE         DEPR_LEDGER.DEPRECIATION_EXPENSE%TYPE,
                              A_NET_SALVAGE_PCT DEPR_METHOD_RATES.NET_SALVAGE_PCT%TYPE)
      return number
   is
      V_SIGN_BALANCE       NUMBER(22,0);
      V_SIGN_RESERVE       NUMBER(22,0);
      V_SIGN_EXPENSE       NUMBER(22,0);

      V_OVERDEPR_EXPENSE      DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST%TYPE;
      /* V_OVER_DEPR_MAXIMUM */
   begin
      --make sure we have a reason to calculate
      if A_EXPENSE = 0 or (A_END_BALANCE = 0 and A_END_RESERVE = 0) then
         return 0;
      end if;

      --get signs
      V_SIGN_BALANCE := sign(A_END_BALANCE);
      V_SIGN_RESERVE := sign(A_END_RESERVE);
      V_SIGN_EXPENSE := sign(A_EXPENSE);

      --handle special cases (these mimic the updates and deletes at the end of f_overdepr_stage... see "--This eliminates a lot of conditions...")
      if V_SIGN_BALANCE = 0 then
         V_SIGN_BALANCE := V_SIGN_RESERVE;
      elsif V_SIGN_RESERVE = 0 then
         V_SIGN_RESERVE := V_SIGN_BALANCE;
      end if;
      if (V_SIGN_BALANCE = V_SIGN_RESERVE and V_SIGN_BALANCE <> V_SIGN_EXPENSE) or (V_SIGN_BALANCE <> V_SIGN_RESERVE and V_SIGN_BALANCE = V_SIGN_EXPENSE) then
         return 0;
      end if;

      --calculate over depr amount
      if V_SIGN_BALANCE + V_SIGN_RESERVE + V_SIGN_EXPENSE in (3, -3) then
         if abs(A_END_RESERVE) > abs(A_END_BALANCE * (1 - A_NET_SALVAGE_PCT)) then
            V_OVERDEPR_EXPENSE := A_END_BALANCE * (1 - A_NET_SALVAGE_PCT) - A_END_RESERVE;
         else
            return 0;
         end if;
      else
         V_OVERDEPR_EXPENSE := -1 * V_SIGN_EXPENSE * least(abs(A_EXPENSE), abs(A_END_RESERVE));
      end if;

      --check to make sure we aren't adjusting more than we've accrued this month
      if abs(V_OVERDEPR_EXPENSE) > abs(A_EXPENSE) then
         V_OVERDEPR_EXPENSE := -A_EXPENSE;
      end if;

      return V_OVERDEPR_EXPENSE;
   end F_OVERDEPR_SINGLE_ROW;

  function F_OVERDEPR_STAGE return number
   is
      L_STATUS varchar2(254);
   begin

    l_status := 'Starting Over Depr Check (LIFE)';
    insert into depr_calc_over_stg
      (DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, TYPE,
      END_BALANCE, END_RESERVE, NET_SALVAGE_PCT, EXPENSE,
      RETRO_EXPENSE, SIGN_BALANCE, SIGN_RESERVE, SIGN_EXPENSE)
    select DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, 'LIFE',
          END_BALANCE, END_RESERVE, NET_SALVAGE_PCT, DEPRECIATION_EXPENSE+SALVAGE_EXPENSE,
          RETRO_DEPR_ADJ, sign(END_BALANCE), sign(END_RESERVE), sign(DEPRECIATION_EXPENSE+SALVAGE_EXPENSE)
      from DEPR_CALC_STG
      where OVER_DEPR_CHECK in (1,2)
        and DEPRECIATION_EXPENSE + SALVAGE_EXPENSE <> 0;

      l_status := 'Starting Over Depr Check (COR)';
    insert into depr_calc_over_stg
      (DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, TYPE,
      END_BALANCE, END_RESERVE, NET_SALVAGE_PCT, EXPENSE,
      RETRO_EXPENSE, SIGN_BALANCE, SIGN_RESERVE, SIGN_EXPENSE)
    select DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, 'COR',
          END_BALANCE, COR_END_RESERVE, 1-COST_OF_REMOVAL_PCT, COR_EXPENSE,
          RETRO_DEPR_ADJ, sign(END_BALANCE), sign(COR_END_RESERVE), sign(COR_EXPENSE)
      from DEPR_CALC_STG
      where OVER_DEPR_CHECK in (1,4)
        and COR_EXPENSE <> 0;

      l_status := 'Starting Over Depr Check (COMBINED)';
      insert into depr_calc_over_stg
      (DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, TYPE,
      END_BALANCE, END_RESERVE, NET_SALVAGE_PCT, EXPENSE,
      RETRO_EXPENSE, SIGN_BALANCE, SIGN_RESERVE, SIGN_EXPENSE)
    select DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, 'COMB',
          END_BALANCE, END_RESERVE+COR_END_RESERVE, NET_SALVAGE_PCT-COST_OF_REMOVAL_PCT, DEPRECIATION_EXPENSE+SALVAGE_EXPENSE+COR_EXPENSE,
          RETRO_DEPR_ADJ, sign(END_BALANCE), sign(END_RESERVE+COR_END_RESERVE), sign(DEPRECIATION_EXPENSE+SALVAGE_EXPENSE+COR_EXPENSE)
      from DEPR_CALC_STG
      where OVER_DEPR_CHECK in (3)
        and DEPRECIATION_EXPENSE + SALVAGE_EXPENSE + COR_EXPENSE <> 0;

    --This eliminates a lot of conditions...
    l_status := 'Updating balance sign';
    update DEPR_CALC_OVER_STG
      set SIGN_BALANCE = SIGN_RESERVE
      where SIGN_BALANCE = 0;

    l_status := 'Updating reserve sign';
    update DEPR_CALC_OVER_STG
      set SIGN_RESERVE = SIGN_BALANCE
      where SIGN_RESERVE = 0;

    l_status := 'First delete';
    delete from DEPR_CALC_OVER_STG
      where SIGN_BALANCE = SIGN_RESERVE
      and SIGN_BALANCE <> SIGN_EXPENSE;

    l_status := 'Second delete';
    delete from DEPR_CALC_OVER_STG
      where SIGN_BALANCE <> SIGN_RESERVE
      and SIGN_BALANCE = SIGN_EXPENSE;

    return 1;

  exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr Stage: ' || L_STATUS;
      return -1;
   end F_OVERDEPR_STAGE;


  function F_OVERDEPR_CALC return number
   is
      L_STATUS varchar2(254);
   begin

    L_STATUS := 'Starting Over Depr Calc';
    update DEPR_CALC_OVER_STG
    set OVER_DEPR_AMOUNT =
      case
        when SIGN_BALANCE + SIGN_RESERVE + SIGN_EXPENSE = 3 then
          least(0, END_BALANCE*(1-NET_SALVAGE_PCT) - END_RESERVE)
        when SIGN_BALANCE + SIGN_RESERVE + SIGN_EXPENSE = -3 then
          greatest(0, END_BALANCE*(1-NET_SALVAGE_PCT) - END_RESERVE)
        else
          -1 * sign(EXPENSE) * least(abs(EXPENSE+RETRO_EXPENSE), abs(END_RESERVE))
      end
    ;

    L_STATUS := 'Updating over depr check amount';
    update DEPR_CALC_OVER_STG
    set OVER_DEPR_CHECK_AMOUNT = greatest(abs(EXPENSE), abs(EXPENSE+RETRO_EXPENSE))
    where abs(SIGN_BALANCE + SIGN_RESERVE + SIGN_EXPENSE) = 3
    and OVER_DEPR_AMOUNT <> 0
    ;

    L_STATUS := 'Updating over depr amount';
    update DEPR_CALC_OVER_STG
    set OVER_DEPR_AMOUNT = -1 * SIGN_EXPENSE * OVER_DEPR_CHECK_AMOUNT
    where abs(OVER_DEPR_AMOUNT) > abs(OVER_DEPR_CHECK_AMOUNT)
    and OVER_DEPR_AMOUNT <> 0
    ;

    return 1;

  exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr Calc: ' || L_STATUS;
      return -1;
   end F_OVERDEPR_CALC;


  function F_OVERDEPR_BACKFILL return number
  is
    L_STATUS varchar2(254);
   begin

    L_STATUS := 'Starting over depr backfill';
    update DEPR_CALC_STG s1
    set (OVER_DEPR_ADJ, OVER_DEPR_ADJ_COR) =
    (
      select
        sum(case when "TYPE" = 'COR' then 0 else nvl(OVER_DEPR_AMOUNT,0) end),
        sum(case when "TYPE" = 'COR' then nvl(OVER_DEPR_AMOUNT,0) else 0 end)
       from DEPR_CALC_OVER_STG s2
      where s2.DEPR_GROUP_ID = s1.DEPR_GROUP_ID
        and s2.GL_POST_MO_YR = s1.GL_POST_MO_YR
        and s2.SET_OF_BOOKS_ID = s1.SET_OF_BOOKS_ID
    )
    where s1.OVER_DEPR_CHECK in (1,2,3,4)
    ;

   L_STATUS := 'Refooting END_RESERVE';
   update DEPR_CALC_STG
      set END_RESERVE = END_RESERVE + OVER_DEPR_ADJ
    where NVL(OVER_DEPR_ADJ,0) <> 0;

   L_STATUS := 'Refooting END_RESERVE';
   update DEPR_CALC_STG
      set COR_END_RESERVE = COR_END_RESERVE + OVER_DEPR_ADJ_COR
    where NVL(OVER_DEPR_ADJ_COR,0) <> 0;

    return 1;

  exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr Backfill: ' || L_STATUS;
      return -1;
   end F_OVERDEPR_BACKFILL;


  function F_OVERDEPR_PRORATE return number
   is
      L_STATUS varchar2(254);
   begin

      -- prorate between COR and LIFE when combined check (over_depr = 3)
      l_status := 'PRORATE COR for combined';
      update depr_calc_stg
      set over_depr_adj_cor = over_depr_adj * ( cor_expense / (cor_expense + salvage_expense + depreciation_expense) )
      where over_depr_adj <> 0
      and cor_expense <> 0
      and over_depr_check = 3
      ;

      l_status := 'PRORATE COR for combined (2)';
      update depr_calc_stg
      set over_depr_adj = over_depr_adj - over_depr_adj_cor
      where over_depr_adj_cor <> 0
      and over_depr_check = 3
      ;

      -- PRORATE THE SALVAGE when trueup LIFE if a salvage expense calculated (1, 2, 3)
      l_status := 'PRORATE SALVAGE';
      update depr_calc_stg
      set over_depr_adj_salv = over_depr_adj * ( salvage_expense / (salvage_expense + depreciation_expense) )
      where over_depr_adj <> 0
      and salvage_expense <> 0
      and over_depr_check in (1, 2, 3)
      ;

      l_status := 'PRORATE SALVAGE (2)';
      update depr_calc_stg
      set over_depr_adj = over_depr_adj - over_depr_adj_salv
      where over_depr_adj_salv <> 0
      and over_depr_check in (1, 2, 3)
      ;

    return 1;

  exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr Prorate: ' || L_STATUS;
      return -1;
   end F_OVERDEPR_PRORATE;


  /*
  PLANT  RESERVE  EXPENSE  COMMENT                          CASE

  -1       -1      -1      Valid Combination                Case 2
  -1       -1       0      Removed Because Expense is 0
  -1       -1       1      Removed in first delete
  -1        0      -1      Removed in second delete
  -1        0       0      Removed Because Expense is 0
  -1        0       1      Valid Combination                Case Else
  -1        1      -1      Removed in second delete
  -1        1       0      Removed Because Expense is 0
  -1        1       1      Valid Combination                Case Else
   0       -1      -1      Valid Combination                Case Else
   0       -1       0      Removed Because Expense is 0
   0       -1       1      Valid Combination                Case Else
   0        0      -1      Removed in first delete
   0        0       0      Removed Because Expense is 0
   0        0       1      Removed in first delete
   0        1      -1      Valid Combination                Case Else
   0        1       0      Removed Because Expense is 0
   0        1       1      Valid Combination                Case Else
   1       -1      -1      Valid Combination                Case Else
   1       -1       0      Removed Because Expense is 0
   1       -1       1      Removed in second delete
   1        0      -1      Valid Combination                Case Else
   1        0       0      Removed Because Expense is 0
   1        0       1      Removed in second delete
   1        1      -1      Removed in first delete
   1        1       0      Removed Because Expense is 0
   1        1       1      Valid Combination                Case 1
  */
  function F_OVERDEPR return number
   is
      L_STATUS varchar2(254);
   begin
    if F_OVERDEPR_STAGE <> 1 then
      return -1;
    end if;

    if F_OVERDEPR_CALC <> 1 then
      return -1;
    end if;

    if F_OVERDEPR_BACKFILL <> 1 then
      return -1;
    end if;

    if F_OVERDEPR_PRORATE <> 1 then
      return -1;
    end if;

   delete from DEPR_CALC_OVER_STG_ARC arc
   where exists (
      select 1
      from DEPR_CALC_OVER_STG STG
      where ARC.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
      and ARC.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
      and ARC.GL_POST_MO_YR = STG.GL_POST_MO_YR
   );

   insert into DEPR_CALC_OVER_STG_ARC (
      DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, TYPE, END_BALANCE, END_RESERVE, EXPENSE, RETRO_EXPENSE, NET_SALVAGE_PCT, SIGN_BALANCE, SIGN_RESERVE, SIGN_EXPENSE, OVER_DEPR_AMOUNT, OVER_DEPR_CHECK_AMOUNT)
   select
      DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID, TYPE, END_BALANCE, END_RESERVE, EXPENSE, RETRO_EXPENSE, NET_SALVAGE_PCT, SIGN_BALANCE, SIGN_RESERVE, SIGN_EXPENSE, OVER_DEPR_AMOUNT, OVER_DEPR_CHECK_AMOUNT
   from DEPR_CALC_OVER_STG;

    return 1;

   exception
   when others then
      L_STATUS := sqlerrm;
      update DEPR_CALC_STG
         set DEPR_CALC_STATUS = -2, DEPR_CALC_MESSAGE = 'Error on Over Depr: ' || L_STATUS;
      return -1;
   end F_OVERDEPR;

   procedure P_AMORT_CALC is
   begin
      --adjust "bal" when it crosses 0 so that we are evaluating the activity not the balance
      update DEPR_CALC_AMORT_STG
      set COR_BALANCE = COR_BALANCE - PRIOR_COR_BALANCE
      where SIGN(COR_BALANCE) <> SIGN(PRIOR_COR_BALANCE)
      and COR_BALANCE <> 0
      and PRIOR_COR_BALANCE <> 0;

      update DEPR_CALC_AMORT_STG
      set SALV_BALANCE = SALV_BALANCE - PRIOR_SALV_BALANCE
      where SIGN(SALV_BALANCE) <> SIGN(PRIOR_SALV_BALANCE)
      and SALV_BALANCE <> 0
      and PRIOR_SALV_BALANCE <> 0;

      --TODO what to do when PRIOR_COR_BALANCE <> 0 and COR_BALANCE = 0, or COR_BALANCE = 0 and COR_RESERVE <> 0

      --calculate amortization amounts
      update DEPR_CALC_AMORT_STG
      set COR_AMORT = COR_BALANCE / COR_LIFE
      where COR_TREATMENT <> 'no'
      and COR_BALANCE <> COR_RESERVE;

      update DEPR_CALC_AMORT_STG
      set SALV_AMORT = SALV_BALANCE / SALV_LIFE
      where SALV_TREATMENT <> 'no'
      and SALV_BALANCE <> SALV_RESERVE;

      --check for over-amortization
      update DEPR_CALC_AMORT_STG
      set COR_AMORT = COR_BALANCE - PRIOR_COR_RESERVE
      where COR_TREATMENT <> 'no'
      and abs(COR_AMORT + COR_BALANCE) > abs(COR_RESERVE);

      update DEPR_CALC_AMORT_STG
      set SALV_AMORT = SALV_BALANCE - PRIOR_SALV_RESERVE
      where SALV_TREATMENT <> 'no'
      and abs(SALV_AMORT + SALV_BALANCE) > abs(SALV_RESERVE);

      --adjust reserve
      update DEPR_CALC_AMORT_STG
      set COR_RESERVE = COR_BALANCE + COR_AMORT
      where COR_AMORT <> 0;

      update DEPR_CALC_AMORT_STG
      set SALV_RESERVE = SALV_BALANCE + SALV_AMORT
      where SALV_AMORT <> 0;

      --copy results from depr_calc_amort_stg to depr_calc_stg
         --no where clause necessary on update since we only stage depr_groups and SOBs
         -- that get amortized, and the model statement will overwrite this result anyway
         -- in case of an error where we update something we shouldn't
      update DEPR_CALC_STG A
      set (COR_EXPENSE, SALVAGE_EXPENSE) = (
         select SUM(COR_AMORT), SUM(SALV_AMORT)
         from DEPR_CALC_AMORT_STG B
         where A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
         and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
         and A.GL_POST_MO_YR = B.GL_POST_MO_YR);

      --archive amort staging table
      delete from DEPR_CALC_AMORT_ARC ARC
      where exists (
         select 1
         from DEPR_CALC_AMORT_STG STG
         where STG.DEPR_GROUP_ID = ARC.DEPR_GROUP_ID
         and STG.SET_OF_BOOKS_ID = ARC.SET_OF_BOOKS_ID
         and STG.GL_POST_MO_YR = ARC.GL_POST_MO_YR
      );

      insert into DEPR_CALC_AMORT_ARC (
         DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, VINTAGE,
         COR_TREATMENT, COR_LIFE, PRIOR_COR_BALANCE, PRIOR_COR_RESERVE, COR_BALANCE,
         COR_AMORT, COR_RESERVE, SALV_TREATMENT, SALV_LIFE, PRIOR_SALV_BALANCE,
         PRIOR_SALV_RESERVE, SALV_BALANCE, SALV_AMORT, SALV_RESERVE)
      select DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, VINTAGE,
         COR_TREATMENT, COR_LIFE, PRIOR_COR_BALANCE, PRIOR_COR_RESERVE, COR_BALANCE,
         COR_AMORT, COR_RESERVE, SALV_TREATMENT, SALV_LIFE, PRIOR_SALV_BALANCE,
         PRIOR_SALV_RESERVE, SALV_BALANCE, SALV_AMORT, SALV_RESERVE
      from DEPR_CALC_AMORT_STG;

   end P_AMORT_CALC;

   function F_BACKFILLDATA return number is
      L_STATUS varchar2(254);
   begin
      --fiscal qtrs based on pp_calendars instead of pp_table_months
      update DEPR_CALC_STG STG
         set (FISCALQTRSTART, FISCAL_MONTH) =
              (select ADD_MONTHS(STG.FISCALYEARSTART, (CAL.FISCAL_QUARTER - 1) * 3), CAL.FISCAL_MONTH
                 from PP_CALENDAR CAL
                where CAL.MONTH = STG.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('QTR', 'Quarterly');

      -- begin_balance_qtr and begin_reserve_qtr for Quarterly
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_QTR, BEGIN_RESERVE_QTR, BEGIN_COR_QTR) =
              (select NVL(DL.BEGIN_BALANCE, 0), NVL(DL.BEGIN_RESERVE, 0), NVL(DL.COR_BEG_RESERVE,0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALQTRSTART)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('QTR', 'Quarterly');

     -- calculate plant activity, reserve_activity, and cor_activity for Yearly, SFY
     update DEPR_CALC_STG STG
        set (STG.PLANT_ACTIVITY_2, STG.RESERVE_ACTIVITY_2, STG.COR_ACTIVITY_2) =
              (select round(sum((dl.ADDITIONS + dl.RETIREMENTS + decode(STG.trf_in_est_adds, 'no', 0, dl.TRANSFERS_IN + dl.TRANSFERS_OUT)
                     + dl.ADJUSTMENTS + dl.IMPAIRMENT_ASSET_AMOUNT)
                     / (decode(STG.smooth_curve, 'no', 1, (3-months_between(dl.gl_post_mo_yr, STG.FISCALQTRSTART))))
                  ),2)
                  * decode(STG.smooth_curve, 'no', 1, 3),
               round(sum((dl.RETIREMENTS + DL.GAIN_LOSS + DL.SALVAGE_CASH + DL.SALVAGE_RETURNS +
                     + DL.RESERVE_CREDITS + decode(STG.trf_in_est_adds, 'no', 0, DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT)
                     + DL.RESERVE_ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT + IMPAIRMENT_EXPENSE_AMOUNT)
                     / (decode(stg.smooth_curve, 'no', 1, (3-months_between(dl.gl_post_mo_yr, stg.FISCALQTRSTART))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 3),
               round(sum((DL.COST_OF_REMOVAL + DL.COR_RES_ADJUST + DECODE(STG.TRF_IN_EST_ADDS, 'no', 0, DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT))
                     / (decode(stg.smooth_curve, 'no', 1, (3-months_between(dl.gl_post_mo_yr, stg.FISCALQTRSTART))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 3)
             from depr_ledger dl
            where dl.depr_group_id = stg.depr_group_id
              and dl.set_of_books_id = stg.set_of_books_id
              and dl.gl_post_mo_yr between stg.FISCALQTRSTART and stg.gl_post_mo_yr)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('QTR', 'Quarterly');


      -- begin_balance_year and begin_reserve_year for Yearly, SFY
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR, BEGIN_COR_YEAR) =
              (select NVL(DL.BEGIN_BALANCE, 0), NVL(DL.BEGIN_RESERVE, 0), NVL(DL.COR_BEG_RESERVE,0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALYEARSTART)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('Yearly', 'SFY');

     -- calculate plant activity, reserve_activity, and cor_activity for Yearly, SFY
     update DEPR_CALC_STG STG
        set (STG.PLANT_ACTIVITY_2, STG.RESERVE_ACTIVITY_2, STG.COR_ACTIVITY_2) =
              (select round(sum((dl.ADDITIONS + dl.RETIREMENTS + decode(STG.trf_in_est_adds, 'no', 0, dl.TRANSFERS_IN + dl.TRANSFERS_OUT)
                     + dl.ADJUSTMENTS + dl.IMPAIRMENT_ASSET_AMOUNT)
                     / (decode(STG.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, STG.fiscalyearstart))))
                  ),2)
                  * decode(STG.smooth_curve, 'no', 1, 12),
               round(sum((dl.RETIREMENTS + DL.GAIN_LOSS + DL.SALVAGE_CASH + DL.SALVAGE_RETURNS +
                     + DL.RESERVE_CREDITS + decode(STG.trf_in_est_adds, 'no', 0, DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT)
                     + DL.RESERVE_ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT + IMPAIRMENT_EXPENSE_AMOUNT)
                     / (decode(stg.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, stg.fiscalyearstart))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 12),
               round(sum((DL.COST_OF_REMOVAL + DL.COR_RES_ADJUST + DECODE(STG.TRF_IN_EST_ADDS, 'no', 0, DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT))
                     / (decode(stg.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, stg.fiscalyearstart))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 12)
             from depr_ledger dl
            where dl.depr_group_id = stg.depr_group_id
              and dl.set_of_books_id = stg.set_of_books_id
              and dl.gl_post_mo_yr between stg.fiscalyearstart and stg.gl_post_mo_yr)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('Yearly', 'SFY');


      -- begin_balance_year and begin_reserve_year for curve, curve_no_true_up
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_YEAR) =
              (select NVL(DL.BEGIN_BALANCE, 0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALYEARSTART)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('curve', 'curve_no_true_up');

      -- est_net_adds for curve_no_true_up and curve
      update DEPR_CALC_STG STG
         set EST_NET_ADDS =
              (select NVL(EST_ANN_NET_ADDS, 0)
                 from DEPR_GROUP DG
                where DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('curve_no_true_up', 'curve');

      -- overwrite est_net_adds for curve if in fiscal month = 12
      update DEPR_CALC_STG STG
         set EST_NET_ADDS =
              (select case
                         when STG.TRF_IN_EST_ADDS = 'no' then
                          sum(ADDITIONS + RETIREMENTS + ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT)
                         else
                          sum(ADDITIONS + RETIREMENTS + TRANSFERS_IN + TRANSFERS_OUT + ADJUSTMENTS +
                              IMPAIRMENT_ASSET_AMOUNT)
                      end
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and STG.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and FISCAL_MONTH = 12
         and MID_PERIOD_METHOD in ('curve');

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --cumulative_transfers for curve, curve_no_true_up
      update DEPR_CALC_STG STG
         set CUMULATIVE_TRANSFERS =
              (select sum(DL.TRANSFERS_IN + DL.TRANSFERS_OUT)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and
                      ADD_MONTHS(STG.GL_POST_MO_YR, -1))
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('curve', 'curve_no_true_up')
         and TRF_IN_EST_ADDS = 'no';

      --if sqlca.sqlCode <> 0 then
      --error
      --end if;

      --type_2_exist for UOP
      update DEPR_CALC_STG STG
         set TYPE_2_EXIST = NVL((select case
                                           when UOP_TYPE_ID2 > 0 then
                                            1
                                           else
                                            0
                                        end
                                   from DEPR_METHOD_UOP M, DEPR_GROUP_UOP G
                                  where G.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                                    and G.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                                    and G.GL_POST_MO_YR = STG.GL_POST_MO_YR
                                    and M.DEPR_METHOD_ID = STG.DEPR_METHOD_ID
                                    and M.SET_OF_BOOKS_ID = G.SET_OF_BOOKS_ID
                                    and M.GL_POST_MO_YR = G.GL_POST_MO_YR),
                                 0)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');


      --cum_uop_depr, cum_uop_depr_2 for UOP
      update DEPR_CALC_STG STG
         set (CUM_UOP_DEPR, CUM_UOP_DEPR_2) =
              (select NVL(sum(UOP_DEPR_EXPENSE), 0), NVL(sum(UOP_DEPR_EXPENSE_2), 0)
                 from DEPR_GROUP_UOP DGO
                where DGO.GL_POST_MO_YR < STG.GL_POST_MO_YR
                  and DGO.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DGO.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');


      --reserve_diff for UOP
      update DEPR_CALC_STG STG
         set STG.RESERVE_DIFF =
              (select STG.BEGIN_RESERVE -
                      (select sum(DGO.LEDGER_DEPR_EXPENSE)
                         from DEPR_GROUP_UOP DGO
                        where DGO.GL_POST_MO_YR < STG.GL_POST_MO_YR
                          and DGO.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                          and DGO.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
                 from DUAL)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');

      --production, estimated_production, production_2, estimated_production_2, min_calc for UOP
      update DEPR_CALC_STG STG
         set (PRODUCTION, ESTIMATED_PRODUCTION, PRODUCTION_2, ESTIMATED_PRODUCTION_2, MIN_CALC) =
              (select PRODUCTION,
                      ESTIMATED_PRODUCTION,
                      PRODUCTION_2,
                      ESTIMATED_PRODUCTION_2,
                      MIN_CALC_OPTION
                 from DEPR_METHOD_UOP M, DEPR_GROUP_UOP G
                where G.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and G.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and G.GL_POST_MO_YR = STG.GL_POST_MO_YR
                  and M.DEPR_METHOD_ID = STG.DEPR_METHOD_ID
                  and M.SET_OF_BOOKS_ID = G.SET_OF_BOOKS_ID
                  and M.GL_POST_MO_YR = G.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');

      --ytd_uop_depr, ytd_uop_depr_2 for UOP
      update DEPR_CALC_STG STG
         set (YTD_UOP_DEPR, YTD_UOP_DEPR_2) =
              (select NVL(sum(UOP_DEPR_EXPENSE), 0), NVL(sum(UOP_DEPR_EXPENSE_2), 0)
                 from DEPR_GROUP_UOP DG
                where DG.GL_POST_MO_YR < STG.GL_POST_MO_YR
                  and DG.GL_POST_MO_YR >= STG.FISCALYEARSTART
                  and DG.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP');

      --less_year_uop_depr for UOP
      update DEPR_CALC_STG STG
         set LESS_YEAR_UOP_DEPR =
              (select NVL(sum(LEDGER_DEPR_EXPENSE), 0)
                 from DEPR_GROUP_UOP DG
                where DG.GL_POST_MO_YR < STG.FISCALYEARSTART
                  and DG.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP')
         and MIN_CALC in (3);


      -- have the information for min_uop_exp. get min_uop_exp for UOP for mincalc 1 or 2
      update DEPR_CALC_STG
         set MIN_UOP_EXP = GREATEST(YTD_UOP_DEPR + CURR_UOP_EXP, YTD_UOP_DEPR_2 + CURR_UOP_EXP_2)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP')
         and MIN_CALC in (1, 2);


      -- have the information for min_uop_exp. get min_uop_exp for UOP for mincalc 3
      update DEPR_CALC_STG
         set MIN_UOP_EXP = GREATEST(CUM_UOP_DEPR + CURR_UOP_EXP - LESS_YEAR_UOP_DEPR,
                                     CUM_UOP_DEPR_2 + CURR_UOP_EXP_2 - LESS_YEAR_UOP_DEPR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('UOP')
         and MIN_CALC in (3);

      return 1;
   exception
      when others then
         L_STATUS := sqlerrm;
         update DEPR_CALC_STG set DEPR_CALC_STATUS = -25, DEPR_CALC_MESSAGE = L_STATUS;

         return -1;
   end F_BACKFILLDATA;

   --**************************************************************************
   --                            F_PREPDEPRCALC
   --**************************************************************************
   --
   -- This function performs the depreciation calculation
   -- PREREQS: The staging table depr_calc_stg is already populated by some other means
   -- ALLOWING: this function to calculate depreciation independently of type of calc or what process initiated
   --

   function F_PREPDEPRCALC return number is
      MY_FIRST_MONTH date;
      MY_LAST_MONTH date;
   begin
     PKG_PP_LOG.P_WRITE_MESSAGE('STARTING Missing Sets of Books');
      P_MISSINGSETSOFBOOKS();

     PKG_PP_LOG.P_WRITE_MESSAGE('STARTING Set Fiscal Year Start');
      P_SET_FISCALYEARSTART();

     PKG_PP_LOG.P_WRITE_MESSAGE('STARTING COR Salvage Amortization');
     P_AMORT_CALC();

     PKG_PP_LOG.P_WRITE_MESSAGE('STARTING Backfill Data');
      if F_BACKFILLDATA() = -1 then
         return -1;
      end if;

      P_DEPRCALC();
/*       if F_OVERDEPR <> 1 then
        return -1;
      end if; */

     --remove rows from archive table
     PKG_PP_LOG.P_WRITE_MESSAGE('Removing rows from archive table');
--     if nvl(G_VERSION_ID,0) = 0 then
        delete from depr_calc_stg_arc a
        where exists (
         select 1
         from depr_calc_stg b
         where b.depr_group_id = a.depr_group_id
         and b.set_of_books_id = a.set_of_books_id
         and b.gl_post_mo_yr = a.gl_post_mo_yr
        );

        --archive rows
        PKG_PP_LOG.P_WRITE_MESSAGE('Archiving Results');
        insert into DEPR_CALC_STG_ARC
         (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS, DEPR_CALC_MESSAGE,
         TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD,
         FISCALYEAROFFSET, FISCALYEARSTART, COMPANY_ID, SUBLEDGER_TYPE_ID, DESCRIPTION, DEPR_METHOD_ID,
         MID_PERIOD_CONV, MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS, TRUE_UP_CPR_DEPR, RATE, NET_GROSS,
         OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE, END_OF_LIFE, EXPECTED_AVERAGE_LIFE,
         RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE,
         AMORTIZABLE_LIFE, COR_TREATMENT, SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE,
         DEPR_LEDGER_STATUS, BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR,
         SALVAGE_BALANCE, RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN,
         RESERVE_BAL_TRAN_OUT, RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR,
         BEGIN_BALANCE, ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
         DEPRECIATION_BASE, END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
         DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS, SALVAGE_CASH,
         RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT, GAIN_LOSS,
         VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE, CURRENT_NET_SALVAGE_AMORT,
         CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG, IMPAIRMENT_RESERVE_ACT,
         IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION, COR_BEG_RESERVE, COR_EXPENSE,
         COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN, COR_RES_TRAN_OUT, COR_RES_ADJUST,
         COR_END_RESERVE, COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL,
         RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS, RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE,
         SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP,
         RESERVE_BLENDING_ADJUSTMENT, RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT,
         COR_BLENDING_TRANSFER, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT,
         RESERVE_BAL_IMPAIRMENT, BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR, BEGIN_BALANCE_QTR,
         BEGIN_RESERVE_QTR, PLANT_ACTIVITY_2, RESERVE_ACTIVITY_2, EST_NET_ADDS, CUMULATIVE_TRANSFERS,
         RESERVE_DIFF, FISCAL_MONTH, FISCALQTRSTART, TYPE_2_EXIST, CUM_UOP_DEPR, CUM_UOP_DEPR_2,
         PRODUCTION, ESTIMATED_PRODUCTION, PRODUCTION_2, ESTIMATED_PRODUCTION_2, SMOOTH_CURVE, MIN_CALC,
         MIN_UOP_EXP, YTD_UOP_DEPR, YTD_UOP_DEPR_2, CURR_UOP_EXP, CURR_UOP_EXP_2, LESS_YEAR_UOP_DEPR,
         DEPRECIATION_UOP_RATE, DEPRECIATION_UOP_RATE_2, DEPRECIATION_BASE_UOP, DEPRECIATION_BASE_UOP_2,
         COR_ACTIVITY_2, OVER_DEPR_ADJ, OVER_DEPR_ADJ_COR, OVER_DEPR_ADJ_SALV,
         ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
         ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE, RETRO_DEPR_ADJ,
         RETRO_COR_ADJ, RETRO_SALV_ADJ, BEGIN_COR_YEAR, BEGIN_COR_QTR, PLANT_ACTIVITY, RESERVE_ACTIVITY,
         COR_ACTIVITY, ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE,
         impairment_asset_activity_salv, impairment_asset_begin_balance)
         select DEPR_GROUP_ID,
              SET_OF_BOOKS_ID,
              GL_POST_MO_YR,
              CALC_MONTH,
              DEPR_CALC_STATUS,
              DEPR_CALC_MESSAGE,
              TRF_IN_EST_ADDS,
              INCLUDE_RWIP_IN_NET,
              DNSA_COR_BAL,
              DNSA_SALV_BAL,
              COR_YTD,
              SALV_YTD,
              FISCALYEAROFFSET,
              FISCALYEARSTART,
              COMPANY_ID,
              SUBLEDGER_TYPE_ID,
              DESCRIPTION,
              DEPR_METHOD_ID,
              MID_PERIOD_CONV,
              MID_PERIOD_METHOD,
              DG_EST_ANN_NET_ADDS,
              TRUE_UP_CPR_DEPR,
              RATE,
              NET_GROSS,
              OVER_DEPR_CHECK,
              NET_SALVAGE_PCT,
              RATE_USED_CODE,
              END_OF_LIFE,
              EXPECTED_AVERAGE_LIFE,
              RESERVE_RATIO_ID,
              DMR_COST_OF_REMOVAL_RATE,
              COST_OF_REMOVAL_PCT,
              DMR_SALVAGE_RATE,
              INTEREST_RATE,
              AMORTIZABLE_LIFE,
              COR_TREATMENT,
              SALVAGE_TREATMENT,
              NET_SALVAGE_AMORT_LIFE,
              ALLOCATION_PROCEDURE,
              DEPR_LEDGER_STATUS,
              BEGIN_RESERVE,
              END_RESERVE,
              RESERVE_BAL_PROVISION,
              RESERVE_BAL_COR,
              SALVAGE_BALANCE,
              RESERVE_BAL_ADJUST,
              RESERVE_BAL_RETIREMENTS,
              RESERVE_BAL_TRAN_IN,
              RESERVE_BAL_TRAN_OUT,
              RESERVE_BAL_OTHER_CREDITS,
              RESERVE_BAL_GAIN_LOSS,
              RESERVE_ALLOC_FACTOR,
              BEGIN_BALANCE,
              ADDITIONS,
              RETIREMENTS,
              TRANSFERS_IN,
              TRANSFERS_OUT,
              ADJUSTMENTS,
              DEPRECIATION_BASE,
              END_BALANCE,
              DEPRECIATION_RATE,
              DEPRECIATION_EXPENSE,
              DEPR_EXP_ADJUST,
              DEPR_EXP_ALLOC_ADJUST,
              COST_OF_REMOVAL,
              RESERVE_RETIREMENTS,
              SALVAGE_RETURNS,
              SALVAGE_CASH,
              RESERVE_CREDITS,
              RESERVE_ADJUSTMENTS,
              RESERVE_TRAN_IN,
              RESERVE_TRAN_OUT,
              GAIN_LOSS,
              VINTAGE_NET_SALVAGE_AMORT,
              VINTAGE_NET_SALVAGE_RESERVE,
              CURRENT_NET_SALVAGE_AMORT,
              CURRENT_NET_SALVAGE_RESERVE,
              IMPAIRMENT_RESERVE_BEG,
              IMPAIRMENT_RESERVE_ACT,
              IMPAIRMENT_RESERVE_END,
              EST_ANN_NET_ADDS,
              RWIP_ALLOCATION,
              COR_BEG_RESERVE,
              COR_EXPENSE,
              COR_EXP_ADJUST,
              COR_EXP_ALLOC_ADJUST,
              COR_RES_TRAN_IN,
              COR_RES_TRAN_OUT,
              COR_RES_ADJUST,
              COR_END_RESERVE,
              COST_OF_REMOVAL_RATE,
              COST_OF_REMOVAL_BASE,
              RWIP_COST_OF_REMOVAL,
              RWIP_SALVAGE_CASH,
              RWIP_SALVAGE_RETURNS,
              RWIP_RESERVE_CREDITS,
              SALVAGE_RATE,
              SALVAGE_BASE,
              SALVAGE_EXPENSE,
              SALVAGE_EXP_ADJUST,
              SALVAGE_EXP_ALLOC_ADJUST,
              RESERVE_BAL_SALVAGE_EXP,
              RESERVE_BLENDING_ADJUSTMENT,
              RESERVE_BLENDING_TRANSFER,
              COR_BLENDING_ADJUSTMENT,
              COR_BLENDING_TRANSFER,
              IMPAIRMENT_ASSET_AMOUNT,
              IMPAIRMENT_EXPENSE_AMOUNT,
              RESERVE_BAL_IMPAIRMENT,
              BEGIN_BALANCE_YEAR,
              BEGIN_RESERVE_YEAR,
              BEGIN_BALANCE_QTR,
              BEGIN_RESERVE_QTR,
              PLANT_ACTIVITY_2,
              RESERVE_ACTIVITY_2,
              EST_NET_ADDS,
              CUMULATIVE_TRANSFERS,
              RESERVE_DIFF,
              FISCAL_MONTH,
              FISCALQTRSTART,
              TYPE_2_EXIST,
              CUM_UOP_DEPR,
              CUM_UOP_DEPR_2,
              PRODUCTION,
              ESTIMATED_PRODUCTION,
              PRODUCTION_2,
              ESTIMATED_PRODUCTION_2,
              SMOOTH_CURVE,
              MIN_CALC,
              MIN_UOP_EXP,
              YTD_UOP_DEPR,
              YTD_UOP_DEPR_2,
              CURR_UOP_EXP,
              CURR_UOP_EXP_2,
              LESS_YEAR_UOP_DEPR,
              DEPRECIATION_UOP_RATE,
              DEPRECIATION_UOP_RATE_2,
              DEPRECIATION_BASE_UOP,
              DEPRECIATION_BASE_UOP_2,
              COR_ACTIVITY_2,
              OVER_DEPR_ADJ,
              OVER_DEPR_ADJ_COR,
              OVER_DEPR_ADJ_SALV,
              ORIG_DEPR_EXPENSE,
              ORIG_DEPR_EXP_ALLOC_ADJUST,
              ORIG_SALV_EXPENSE,
              ORIG_SALV_EXP_ALLOC_ADJUST,
              ORIG_COR_EXP_ALLOC_ADJUST,
              ORIG_COR_EXPENSE,
              RETRO_DEPR_ADJ,
              RETRO_COR_ADJ,
              RETRO_SALV_ADJ,
              BEGIN_COR_YEAR,
              BEGIN_COR_QTR,
              PLANT_ACTIVITY,
              RESERVE_ACTIVITY,
              COR_ACTIVITY,
              ORIG_BEGIN_RESERVE,
              ORIG_COR_BEG_RESERVE,
              impairment_asset_activity_salv, impairment_asset_begin_balance
          from DEPR_CALC_STG;
/*     else
        MY_FIRST_MONTH := G_MONTHS(G_MONTHS.first);
        MY_LAST_MONTH := G_MONTHS(G_MONTHS.last);

        delete from fcst_depr_calc_stg_arc a
        where a.fcst_depr_version_id = G_VERSION_ID
         and a.gl_post_mo_yr >= MY_FIRST_MONTH
         and a.gl_post_mo_yr <= MY_LAST_MONTH;


        --archive rows
        PKG_PP_LOG.P_WRITE_MESSAGE('Archiving Results');
        insert into FCST_DEPR_CALC_STG_ARC
         (FCST_DEPR_GROUP_ID, FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS, DEPR_CALC_MESSAGE,
         TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD,
         FISCALYEAROFFSET, FISCALYEARSTART, COMPANY_ID, SUBLEDGER_TYPE_ID, DESCRIPTION, FCST_DEPR_METHOD_ID,
         MID_PERIOD_CONV, MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS, TRUE_UP_CPR_DEPR, RATE, NET_GROSS,
         OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE, END_OF_LIFE, EXPECTED_AVERAGE_LIFE,
         RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE,
         AMORTIZABLE_LIFE, COR_TREATMENT, SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE,
         DEPR_LEDGER_STATUS, BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR,
         SALVAGE_BALANCE, RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN,
         RESERVE_BAL_TRAN_OUT, RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR,
         BEGIN_BALANCE, ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
         DEPRECIATION_BASE, END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
         DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS, SALVAGE_CASH,
         RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT, GAIN_LOSS,
         VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE, CURRENT_NET_SALVAGE_AMORT,
         CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG, IMPAIRMENT_RESERVE_ACT,
         IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION, COR_BEG_RESERVE, COR_EXPENSE,
         COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN, COR_RES_TRAN_OUT, COR_RES_ADJUST,
         COR_END_RESERVE, COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL,
         RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS, RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE,
         SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP,
         RESERVE_BLENDING_ADJUSTMENT, RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT,
         COR_BLENDING_TRANSFER, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT,
         RESERVE_BAL_IMPAIRMENT, BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR, BEGIN_BALANCE_QTR,
         BEGIN_RESERVE_QTR, PLANT_ACTIVITY_2, RESERVE_ACTIVITY_2, EST_NET_ADDS, CUMULATIVE_TRANSFERS,
         RESERVE_DIFF, FISCAL_MONTH, FISCALQTRSTART, TYPE_2_EXIST, CUM_UOP_DEPR, CUM_UOP_DEPR_2,
         PRODUCTION, ESTIMATED_PRODUCTION, PRODUCTION_2, ESTIMATED_PRODUCTION_2, SMOOTH_CURVE, MIN_CALC,
         MIN_UOP_EXP, YTD_UOP_DEPR, YTD_UOP_DEPR_2, CURR_UOP_EXP, CURR_UOP_EXP_2, LESS_YEAR_UOP_DEPR,
         DEPRECIATION_UOP_RATE, DEPRECIATION_UOP_RATE_2, DEPRECIATION_BASE_UOP, DEPRECIATION_BASE_UOP_2,
         COR_ACTIVITY_2, OVER_DEPR_ADJ, OVER_DEPR_ADJ_COR, OVER_DEPR_ADJ_SALV, ORIG_DEPR_EXPENSE,
         ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
         ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE, RETRO_DEPR_ADJ,
         RETRO_COR_ADJ, RETRO_SALV_ADJ, BEGIN_COR_YEAR, BEGIN_COR_QTR, PLANT_ACTIVITY, RESERVE_ACTIVITY,
         COR_ACTIVITY, ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE,
         impairment_asset_activity_salv, impairment_asset_begin_balance)
        select
          DEPR_GROUP_ID, G_VERSION_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS, DEPR_CALC_MESSAGE,
         TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD,
         FISCALYEAROFFSET, FISCALYEARSTART, COMPANY_ID, SUBLEDGER_TYPE_ID, DESCRIPTION, DEPR_METHOD_ID,
         MID_PERIOD_CONV, MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS, TRUE_UP_CPR_DEPR, RATE, NET_GROSS,
         OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE, END_OF_LIFE, EXPECTED_AVERAGE_LIFE,
         RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE,
         AMORTIZABLE_LIFE, COR_TREATMENT, SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE,
         DEPR_LEDGER_STATUS, BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR,
         SALVAGE_BALANCE, RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN,
         RESERVE_BAL_TRAN_OUT, RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR,
         BEGIN_BALANCE, ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
         DEPRECIATION_BASE, END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
         DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS, SALVAGE_CASH,
         RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT, GAIN_LOSS,
         VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE, CURRENT_NET_SALVAGE_AMORT,
         CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG, IMPAIRMENT_RESERVE_ACT,
         IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION, COR_BEG_RESERVE, COR_EXPENSE,
         COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN, COR_RES_TRAN_OUT, COR_RES_ADJUST,
         COR_END_RESERVE, COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL,
         RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS, RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE,
         SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP,
         RESERVE_BLENDING_ADJUSTMENT, RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT,
         COR_BLENDING_TRANSFER, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT,
         RESERVE_BAL_IMPAIRMENT, BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR, BEGIN_BALANCE_QTR,
         BEGIN_RESERVE_QTR, PLANT_ACTIVITY_2, RESERVE_ACTIVITY_2, EST_NET_ADDS, CUMULATIVE_TRANSFERS,
         RESERVE_DIFF, FISCAL_MONTH, FISCALQTRSTART, TYPE_2_EXIST, CUM_UOP_DEPR, CUM_UOP_DEPR_2,
         PRODUCTION, ESTIMATED_PRODUCTION, PRODUCTION_2, ESTIMATED_PRODUCTION_2, SMOOTH_CURVE, MIN_CALC,
         MIN_UOP_EXP, YTD_UOP_DEPR, YTD_UOP_DEPR_2, CURR_UOP_EXP, CURR_UOP_EXP_2, LESS_YEAR_UOP_DEPR,
         DEPRECIATION_UOP_RATE, DEPRECIATION_UOP_RATE_2, DEPRECIATION_BASE_UOP, DEPRECIATION_BASE_UOP_2,
         COR_ACTIVITY_2, OVER_DEPR_ADJ, OVER_DEPR_ADJ_COR, OVER_DEPR_ADJ_SALV, ORIG_DEPR_EXPENSE,
         ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
         ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE, RETRO_DEPR_ADJ,
         RETRO_COR_ADJ, RETRO_SALV_ADJ, BEGIN_COR_YEAR, BEGIN_COR_QTR, PLANT_ACTIVITY, RESERVE_ACTIVITY,
         COR_ACTIVITY, ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE,
         impairment_asset_activity_salv, impairment_asset_begin_balance
        from DEPR_CALC_STG;
     end if;
*/

      return 1;
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return - 1;
   end F_PREPDEPRCALC;

   --**************************************************************************
   --                            F_FIND_DEPR_GROUP
   --**************************************************************************
   function F_FIND_DEPR_GROUP(A_COMPANY_ID         number,
                              A_GL_ACCOUNT_ID      number,
                              A_MAJOR_LOCATION_ID  number,
                              A_UTILITY_ACCOUNT_ID number,
                              A_BUS_SEGMENT_ID     number,
                              A_SUB_ACCOUNT_ID     number,
                              A_SUBLEDGER_TYPE_ID  number,
                              A_BOOK_VINTAGE       number,
                              A_ASSET_LOCATION_ID  number,
                              A_LOCATION_TYPE_ID   number,
                              A_RETIREMENT_UNIT_ID number,
                              A_PROPERTY_UNIT_ID   number,
                              A_CLASS_CODE_ID      number,
                              A_CC_VALUE           varchar2) return number is
      DG_ID  number;
      STATUS number;
      CC_VAL varchar2(35);

   begin

      if A_CC_VALUE is null then
         CC_VAL := 'NO CLASS CODE';
      else
         CC_VAL := A_CC_VALUE;
      end if;

      select DEPR_GROUP_ID, STATUS_ID
        into DG_ID, STATUS
        from (select DEPR_GROUP_CONTROL.DEPR_GROUP_ID DEPR_GROUP_ID, NVL(STATUS_ID, 1) STATUS_ID
                from DEPR_GROUP_CONTROL, DEPR_GROUP
               where DEPR_GROUP.COMPANY_ID = A_COMPANY_ID
                 and DEPR_GROUP_CONTROL.BUS_SEGMENT_ID = A_BUS_SEGMENT_ID
                 and DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID = A_UTILITY_ACCOUNT_ID
                 and NVL(DEPR_GROUP_CONTROL.GL_ACCOUNT_ID, A_GL_ACCOUNT_ID) = A_GL_ACCOUNT_ID
                 and NVL(DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID, A_MAJOR_LOCATION_ID) =
                     A_MAJOR_LOCATION_ID
                 and NVL(DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID, A_SUB_ACCOUNT_ID) = A_SUB_ACCOUNT_ID
                 and NVL(TO_NUMBER(TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY')), A_BOOK_VINTAGE) =
                     A_BOOK_VINTAGE
                 and NVL(DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID, A_SUBLEDGER_TYPE_ID) =
                     A_SUBLEDGER_TYPE_ID
                 and NVL(DEPR_GROUP_CONTROL.ASSET_LOCATION_ID, A_ASSET_LOCATION_ID) =
                     A_ASSET_LOCATION_ID
                 and NVL(DEPR_GROUP_CONTROL.LOCATION_TYPE_ID, A_LOCATION_TYPE_ID) =
                     A_LOCATION_TYPE_ID
                 and NVL(DEPR_GROUP_CONTROL.PROPERTY_UNIT_ID, A_PROPERTY_UNIT_ID) =
                     A_PROPERTY_UNIT_ID
                 and NVL(DEPR_GROUP_CONTROL.RETIREMENT_UNIT_ID, A_RETIREMENT_UNIT_ID) =
                     A_RETIREMENT_UNIT_ID
                 and NVL(DEPR_GROUP_CONTROL.CLASS_CODE_ID, A_CLASS_CODE_ID) = A_CLASS_CODE_ID
                 and NVL(DEPR_GROUP_CONTROL.CC_VALUE, CC_VAL) = CC_VAL
                 and DEPR_GROUP_CONTROL.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID
                 and DECODE(G_CHECK_DG_BS,
                            'YES',
                            TO_CHAR(DEPR_GROUP_CONTROL.BUS_SEGMENT_ID),
                            'NO_BUS_SEG_CHECK') =
                     DECODE(G_CHECK_DG_BS,
                            'YES',
                            TO_CHAR(DEPR_GROUP.BUS_SEGMENT_ID),
                            'NO_BUS_SEG_CHECK')
               order by DEPR_GROUP.COMPANY_ID,
                        DEPR_GROUP_CONTROL.BUS_SEGMENT_ID,
                        DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.GL_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID,
                        DEPR_GROUP_CONTROL.ASSET_LOCATION_ID,
                        DEPR_GROUP_CONTROL.LOCATION_TYPE_ID,
                        DEPR_GROUP_CONTROL.PROPERTY_UNIT_ID,
                        DEPR_GROUP_CONTROL.RETIREMENT_UNIT_ID,
                        TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY'),
                        DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID,
                        DEPR_GROUP_CONTROL.CLASS_CODE_ID,
                        DEPR_GROUP_CONTROL.CC_VALUE)
       where ROWNUM = 1;

      if STATUS = 1 then
         return DG_ID;
      else
         /* invalid depr group */
         if G_DG_STATUS_CHECK then
            return - 9;
         else
            return DG_ID;
         end if;
      end if;
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return - 1;
   end;

   --**************************************************************************
   --                            F_GET_DG_CC_ID
   --**************************************************************************
   function F_GET_DG_CC_ID return number is
      DG_CC_DESC varchar2(35);
      DG_CC_ID   number;

   begin

      select CONTROL_VALUE
        into DG_CC_DESC
        from PP_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = 'DEPR GROUP CLASS CODE';

      select CLASS_CODE_ID
        into DG_CC_ID
        from CLASS_CODE
       where UPPER(trim(DESCRIPTION)) = UPPER(trim(DG_CC_DESC));

      return DG_CC_ID;

   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return 0;
   end;

   --**************************************************************************
   --                            P_DG_STATUS_CHECK
   --**************************************************************************
   procedure P_DG_STATUS_CHECK(A_ENABLE boolean) is

   begin
      G_DG_STATUS_CHECK := A_ENABLE;
   end P_DG_STATUS_CHECK;

   --**************************************************************************
   --                            P_CHECK_DG_BS
   --**************************************************************************
   procedure P_CHECK_DG_BS is
      PP_CONTROL_VALUE varchar2(35);

   begin
      select UPPER(trim(CONTROL_VALUE))
        into PP_CONTROL_VALUE
        from PP_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = 'POST VALIDATE DEPR BUSINESS SEGMENT';

      if PP_CONTROL_VALUE = 'YES' then
         G_CHECK_DG_BS := 'YES';
      else
         G_CHECK_DG_BS := 'NO';
      end if;
   end P_CHECK_DG_BS;



--**************************************************************************************
-- Initialization procedure (Called the first time this package is opened in a session)
-- Initialize the VALIDATE DEPR BUSINESS SEGMENT variable
--**************************************************************************************
begin
   P_CHECK_DG_BS;

end PP_DEPR_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (794, 0, 10, 4, 1, 2, 34532, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_034532_depr_PP_DEPR_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
