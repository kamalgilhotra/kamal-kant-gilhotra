/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050918_sys_02_IntHub_component_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/05/2018 TechProdMgmt	New Integration Component to be used by Integration Hub APIs
||============================================================================
*/
insert into pp_integration_components (COMPONENT, USER_ID, TIME_STAMP)
select new_component, user, sysdate
from (select 'Integration Hub' new_component from dual
	minus
	select component from pp_integration_components
	);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6403, 0, 2017, 4, 0, 0, 50918, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050918_sys_02_IntHub_component_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;