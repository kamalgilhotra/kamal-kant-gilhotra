/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045616_4_reg_stat_ledger_import_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2016.1.0.0 04/05/2016 Sarah Byers		Create RMS Statistic Ledger Imports
||============================================================================
*/

-- PP_IMPORT_TYPE
insert into pp_import_type (
	import_type_id, time_stamp, user_id, description, long_description, import_table_name, 
	archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, 
	archive_additional_columns, autocreate_description, autocreate_restrict_sql ) 
values (
	 155, sysdate, user, 'Regulatory Statistic', 'Import Regulatory Statistics', 'reg_import_statistic_stg', 
	 'reg_import_statistic_arc', null, 0, 'nvo_reg_logic_import', '', '', '' );

insert into pp_import_type (
	import_type_id, time_stamp, user_id, description, long_description, import_table_name, 
	archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, 
	archive_additional_columns, autocreate_description, autocreate_restrict_sql ) 
 values ( 
	156, sysdate, user, 'Regulatory Statistic Ledger', 'Import Regulatory Statistics Ledger Values', 'reg_import_stat_ledger_stg', 
	'reg_import_stat_ledger_arc', null, 1, 'nvo_reg_logic_import', '', '', '' );

-- PP_IMPORT_TYPE_SUBSYSTEM
insert into pp_import_type_subsystem (import_type_id, import_subsystem_id, time_stamp, user_id)
values ( 155, 6, sysdate, user );

insert into pp_import_type_subsystem (import_type_id, import_subsystem_id, time_stamp, user_id)
values ( 156, 6, sysdate, user );

-- PP_IMPORT_COLUMN
insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 155, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(100)', '', '', 1, null, '', '' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 155, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(2000)', '', '', 1, null, '', '' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 155, 'unit_of_measure', sysdate, user, 'Unit of Measure', '', 0, 1, 'varchar2(70)', '', '', 1, null, '', '' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 155, 'stat_used_for', sysdate, user, 'Statistic Used For', '', 0, 1, 'number(1,0)', '', '', 1, null, '', '3' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 156, 'historic_version_id', sysdate, user, 'Historic Ledger', 'historic_version_id_xlate', 1, 1, 'number(22,0)', 'reg_historic_version', '', 1, null, 'historic_version_id', '0' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 156, 'forecast_version_id', sysdate, user, 'Forecast Ledger', 'forecast_version_id_xlate', 1, 1, 'number(22,0)', 'reg_forecast_version', '', 1, null, 'forecast_version_id', '0' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 156, 'reg_company_id', sysdate, user, 'Regulatory Company', 'reg_company_id_xlate', 1, 1, 'number(22,0)', 'reg_company', '', 1, null, 'reg_company_id', '0' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 156, 'reg_stat_id', sysdate, user, 'Regulatory Statistic', 'reg_stat_id_xlate', 1, 1, 'number(22,0)', 'reg_statistic', '', 1, null, 'reg_stat_id', '' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 156, 'month_number', sysdate, user, 'Month Number', '', 1, 1, 'number(6,0)', '', '', 1, null, '', '' );

insert into pp_import_column (
	import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, 
	column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, 
	default_value ) 
values ( 156, 'stat_value', sysdate, user, 'Statistic Value', '', 1, 1, 'number(22,16)', '', '', 1, null, '', '' );

-- PP_IMPORT_LOOKUP
insert into pp_import_lookup (
	import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, 
	lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn) 
values ( 
	1527, sysdate, user, 'Regulatory Forecast Version.Description', 
	'The passed in value corresponds to the Regulatory Forecast Version: Description field.  Translate to the Forecast Version ID using the Description column on the Regulatory Forecast Version table.', 
	'forecast_version_id', 
	'(select forecast_version_id from reg_forecast_version where upper(trim(description)) = upper(trim(<importfield>)))', 
	0, 'reg_forecast_version', 'description', '', '', null);

insert into pp_import_lookup (
	import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, 
	lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn) 
values ( 
	1528, sysdate, user, 'Regulatory Statistic.Description', 
	'The passed in value corresponds to the Regulatory Statistic: Description field.  Translate to the Reg Stat ID using the Description column on the Regulatory Statistic table.', 
	'reg_stat_id', 
	'( select rs.reg_stat_id from reg_statistic rs where upper( trim( <importfield> ) ) = upper( trim( rs.description ) ) )', 
	0, 'reg_statistic', 'description', '', '',  null);

-- PP_IMPORT_COLUMN_LOOKUP
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 156, 'historic_version_id', 1514, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 156, 'forecast_version_id', 1527, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 156, 'reg_company_id', 1511, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 156, 'reg_stat_id', 1528, sysdate, user );


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3158, 0, 2016, 1, 0, 0, 045616, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045616_4_reg_stat_ledger_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;