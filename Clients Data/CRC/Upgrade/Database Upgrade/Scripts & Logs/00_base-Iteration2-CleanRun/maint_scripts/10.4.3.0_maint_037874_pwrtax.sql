/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037874_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 05/07/2014 Andrew Scott
||============================================================================
*/

--
-- Add the Tax Book Translate workspace.
--
insert into pwrplant.ppbase_workspace ( module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp ) values ( 'powertax', 'depr_input_iface_tax_book_xlate', sysdate, user, 'Tax Book Translate', 'uo_tax_depr_act_wksp_translate', 'Tax Book Translate' );
update pwrplant.ppbase_menu_items set workspace_identifier = 'depr_input_iface_tax_book_xlate' where module = 'powertax' and menu_identifier = 'depr_input_iface_tax_book_xlate';

--
-- Create a system option for the default value for Tax Class Filter on the Tax Book Translate workspace.
--
insert into pwrplant.ppbase_system_options ( system_option_id, time_stamp, user_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override ) values ( 'Tax Book Translate - Default Tax Class Filter', sysdate, user, 'The default selection for the Tax Class Filter.', 0, 'None', null, 1, 0 );
insert into pwrplant.ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Tax Book Translate - Default Tax Class Filter', 'Case', sysdate, user );
insert into pwrplant.ppbase_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Tax Book Translate - Default Tax Class Filter', 'None', sysdate, user );
insert into pwrplant.ppbase_system_options_wksp ( system_option_id, workspace_identifier, time_stamp, user_id ) values ( 'Tax Book Translate - Default Tax Class Filter', 'depr_input_iface_tax_book_xlate', sysdate, user );
insert into pwrplant.ppbase_system_options_module ( system_option_id, module, time_stamp, user_id ) values ( 'Tax Book Translate - Default Tax Class Filter', 'powertax', sysdate, user );

--
-- Add a column to tax_book_translate to keep track of validation errors.
--
alter table TAX_BOOK_TRANSLATE add ERROR_MESSAGE varchar2(4000);

comment on column TAX_BOOK_TRANSLATE.ERROR_MESSAGE
  is 'Error message explaining the reasons why tax-book translation failed validation.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1158, 0, 10, 4, 3, 0, 37874, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037874_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;