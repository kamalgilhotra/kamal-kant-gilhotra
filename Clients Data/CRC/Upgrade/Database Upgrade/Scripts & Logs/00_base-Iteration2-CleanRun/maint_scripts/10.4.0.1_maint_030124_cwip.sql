/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030124_cwip.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.1   06/13/2013 Sunjin Cone    Patch Release
||============================================================================
*/

insert into PP_CONVERSION_WINDOWS
   (WINDOW_ID, DESCRIPTION, LONG_DESCRIPTION, WINDOW_NAME)
   select max(WINDOW_ID) + 1,
          'Fix 106 Bal WIP Comp',
          'Fix 106 Out of Balance WIP Computations',
          'w_fix_oob_106_wipcomp'
     from PP_CONVERSION_WINDOWS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (411, 0, 10, 4, 0, 1, 30124, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_030124_cwip.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

commit;