/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010266_sys.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/22/2013 Lee Quinn
||============================================================================
*/

create index PWR_TAB_IDX99
   on POWERPLANT_TABLES (PP_TABLE_TYPE_ID, TABLE_NAME, DESCRIPTION)
      tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (531, 0, 10, 4, 1, 0, 10266, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010266_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
