/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037789_sys_add_trans_type_report.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 06/19/2014 Charlie Shilling maint 37789
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_STATUS_ID, REPORT_NUMBER, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID)
   select NVL(max(REPORT_ID), 0) + 1,
          'GL Transactions By Transaction Type',
          'A report that summarizes GL Transactions by their transaction type, grouped by company, for a given month',
          'dw_trans_type_summary',
          null,
          13, /*All/Table/Security/Admin*/
          13, /*Main - Admin*/
          1, /*single month*/
          3, /*Not reviewed*/
          'GL TRANS SUMMARY',
          'GL TRANS SUMMARY',
          3
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1222, 0, 10, 4, 3, 0, 37789, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037789_sys_add_trans_type_report.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;