/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_046868_reg_modify_menu_labels_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2017.1.0.0 01/10/2017 Anand R           Modify menu labels in RMS
||============================================================================
*/

UPDATE ppbase_menu_items
SET label = 'Forecast Ledger Adj',
minihelp = 'Forecast Ledger Adj'
WHERE MODULE = 'REG'
AND menu_identifier = 'FORE_ADJUST_MANAGE'
;

UPDATE ppbase_menu_items
SET label = 'Historic Ledger Adj',
minihelp = 'Historic Ledger Adj'
WHERE MODULE = 'REG'
AND menu_identifier = 'HIST_ADJUST_MANAGE'
;

UPDATE ppbase_menu_items
SET label = 'Query Workspace',
minihelp = 'Query Workspace'
WHERE MODULE = 'REG'
AND menu_identifier = 'QUERY_DASHBOARD'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3356, 0, 2017, 1, 0, 0, 046868, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_046868_reg_modify_menu_labels_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;