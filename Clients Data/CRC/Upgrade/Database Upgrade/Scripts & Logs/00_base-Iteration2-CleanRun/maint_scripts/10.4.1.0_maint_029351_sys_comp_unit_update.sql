SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029351_sys_comp_unit_update.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   08/12/2013 Stephen Motter   Point Release
||============================================================================
*/

--Script to add CU specific data tables

--compatible_unit--
alter table COMPATIBLE_UNIT
   add (MAJOR_UNIT_INDICATOR       number(22,0),
        PRECAPITALIZED_INDICATOR   number(22,0),
        STATUS_CODE_ID             number(22,0),
        UNIT_OF_MEASURE_ID         number(22,0),
        NOTES                      varchar2(254),
        CATALOG_NUMBER             varchar2(35),
        CREW_TYPE_ID               number(22,0),
        NONSTOCK_UNIT_PRICE        number(22,8),
        PRECAP_NONSTOCK_UNIT_PRICE number(22,8),
        HAS_CHILDREN               number(22,0),
        EXPENSE_OVERRIDE           number(22,0),
        INVOLVES_DIGGING           number(22,0));

--comp_unit_hierarchy--
create table COMP_UNIT_HIERARCHY
(
 PARENT_COMP_UNIT_ID number(22,0) not null,
 COMP_UNIT_ID        number(22,0) not null,
 ACTION_INDICATOR    varchar2(1)  not null,
 QUANTITY            number(22,8) not null,
 QUANTITY_FIXED      number(22,8) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table COMP_UNIT_HIERARCHY
   add constraint COMP_UNIT_HIERARCHY_PK
       primary key (PARENT_COMP_UNIT_ID, COMP_UNIT_ID, ACTION_INDICATOR)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PP_AU_COMP_UNIT_HIERARCHY
   after insert or update on COMP_UNIT_HIERARCHY
   for each row
declare
   HAS_CHILDREN number(22, 0);
begin
   select HAS_CHILDREN
     into HAS_CHILDREN
     from COMPATIBLE_UNIT
    where COMP_UNIT_ID = :NEW.PARENT_COMP_UNIT_ID;
   if HAS_CHILDREN = 0 then
      update COMPATIBLE_UNIT set HAS_CHILDREN = 1 where COMP_UNIT_ID = :NEW.PARENT_COMP_UNIT_ID;
   end if;
end;
/

create or replace trigger PP_AU_COMP_UNIT_HIERARCHY_DEL
   after delete or update on COMP_UNIT_HIERARCHY
   for each row
declare
   NUMBER_OF_CHILDREN number(22, 0);
   pragma autonomous_transaction;
begin
   select count(1)
     into NUMBER_OF_CHILDREN
     from COMP_UNIT_HIERARCHY
    where COMP_UNIT_ID <> :OLD.COMP_UNIT_ID
      and PARENT_COMP_UNIT_ID = :OLD.PARENT_COMP_UNIT_ID;
   if NUMBER_OF_CHILDREN = 0 then
      update COMPATIBLE_UNIT set HAS_CHILDREN = 0 where COMP_UNIT_ID = :OLD.PARENT_COMP_UNIT_ID;
   end if;
   commit;
end;
/

--comp_unit_action_code_std--
create table COMP_UNIT_ACTION_CODE_STD
(
 COMP_UNIT_STD_ID       number(22,0) not null,
 EFFECTIVE_DATE         date         not null,
 COMP_UNIT_ID           number(22,0) not null,
 ACTION_INDICATOR       varchar2(1)  not null,
 WORK_TYPE_ID           number(22,0),
 TIME_STAMP             date         not null,
 USER_ID                varchar2(18) not null,
 STANDARD_LABOR_HRS_VAR number(22,8) not null,
 STANDARD_LABOR_HRS_FIX number(22,8)
);

alter table COMP_UNIT_ACTION_CODE_STD
   add constraint COMP_UNIT_ACTION_CODE_STD_PK
       primary key (COMP_UNIT_STD_ID)
       using index tablespace PWRPLANT_IDX;

--comp_unit_stock_keeping_unit--
alter table COMP_UNIT_STOCK_KEEPING_UNIT
   add (QUANTITY_FIXED_ADD   number(22,8),
        QUANTITY_LOAD_PCT    number(22,8),
        UNIT_OF_MEASURE_CONV number(22,8));

--comp_unit_work_type--
create table COMP_UNIT_WORK_TYPE
(
 COMP_UNIT_ID          number(22,0) not null,
 WORK_TYPE_ID          number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 UTILITY_ACCOUNT_ID    number(22,0) not null,
 BUS_SEGMENT_ID        number(22,0) not null,
 SUB_ACCOUNT_ID        number(22,0) not null,
 RETIREMENT_UNIT_ID    number(22,0),
 PROPERTY_GROUP_ID     number(22,0),
 EXPENSE_GL_ACCOUNT_ID number(22,0)
);

alter table COMP_UNIT_WORK_TYPE
   add constraint COMP_UNIT_WORK_TYPE_PK
       primary key (COMP_UNIT_ID, WORK_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

--stck_keep_unit--
alter table STCK_KEEP_UNIT
   add (UNIT_OF_MEASURE_ID       number(22,0),
        STOCK_TYPE_ID            number(22,0),
        SERIALIZED               number(22,0),
        PRECAPITALIZED_INDICATOR number(22,0),
        STATUS_CODE_ID           number(22,0),
        TOTAL_QTY_IN_STOCK       number(22,8),
        TOTAL_QTY_ON_ORDER       number(22,8),
        BUILDING_CD              varchar2(35),
        FLOOR_CD                 varchar2(35),
        SECTION_CD               varchar2(35),
        AISLE_CD                 varchar2(35),
        ROW_CD                   varchar2(35),
        BIN_CD                   varchar2(35));

create table STCK_KEEP_UNIT_PRICE_HIST
(
 ID                 number(22,0),
 ARCHIVED_DATE      date,
 STCK_KEEP_UNIT_ID  number(22,0),
 time_stamp         DATE,
 user_id            VARCHAR2(18),
 UNIT_PRICE         number(22,2)
);

alter table STCK_KEEP_UNIT_PRICE_HIST
   add constraint STCK_KEEP_UNIT_PRICE_HIST_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger STCK_KEEP_UNIT_HIST_AU
   after update or delete on STCK_KEEP_UNIT
   for each row
begin

   insert into STCK_KEEP_UNIT_PRICE_HIST
      (ID, ARCHIVED_DATE, STCK_KEEP_UNIT_ID, TIME_STAMP, USER_ID, UNIT_PRICE)
   values
      (PWRPLANT4.NEXTVAL, sysdate, :OLD.STCK_KEEP_UNIT_ID, :OLD.TIME_STAMP, :OLD.USER_ID,
       :OLD.UNIT_PRICE);
end;
/


--stock_type--
create table STOCK_TYPE
(
 STOCK_TYPE_ID    number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254)
);

alter table STOCK_TYPE
   add constraint STOCK_TYPE_PK
       primary key (STOCK_TYPE_ID)
       using index tablespace PWRPLANT_IDX;


--crew type--
create table CREW_TYPE
(
 CREW_TYPE_ID     number(22,0)  not null,
 TIME_STAMP       date,
 USER_ID          varchar2(35),
 DESCRIPTION      varchar2(35)  not null,
 LONG_DESCRIPTION varchar2(254),
 EXTERNAL_VALUE   varchar2(35),
 HOURLY_RATE      number(22,8)  not null,
 WO_ENG_EST_TYPE  varchar2(254)
);

alter table CREW_TYPE
   add constraint CREW_TYPE_PK
       primary key (CREW_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

create table CREW_TYPE_RATE_HIST
(
 ID             number(22,0) not null,
 ARCHIVED_DATE  date,
 CREW_TYPE_ID   number(22,0),
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 HOURLY_RATE    number(22,8)
);

alter table CREW_TYPE_RATE_HIST
   add constraint CREW_TYPE_RATE_HIST_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger CREW_TYPE_RATE_HIST_AU
   after update or delete on CREW_TYPE
   for each row
begin
   insert into CREW_TYPE_RATE_HIST
      (ID, ARCHIVED_DATE, CREW_TYPE_ID, TIME_STAMP, USER_ID, HOURLY_RATE)
   values
      (PWRPLANT4.NEXTVAL, sysdate, :OLD.CREW_TYPE_ID, :OLD.TIME_STAMP, :OLD.USER_ID,
       :OLD.HOURLY_RATE);
end;
/

alter table ESTIMATE_CHARGE_TYPE add WO_ENG_EST_TYPE varchar2(254);


--action code--
create table ACTION_CODE
(
 ACTION_CODE_ID       number(22,0)  not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(35)  not null,
 LONG_DESCRIPTION     varchar2(254),
 EXTERNAL_ACTION_CODE varchar2(35),
 ACTION_INDICATOR     varchar2(1)   not null
);

alter table ACTION_CODE
   add constraint ACTION_CODE_PK
       primary key (ACTION_CODE_ID)
       using index tablespace PWRPLANT_IDX;

insert into ACTION_CODE
   (ACTION_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_ACTION_CODE,
    ACTION_INDICATOR)
values
   (1, null, null, 'Install', 'Install', null, 'I');
insert into ACTION_CODE
   (ACTION_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_ACTION_CODE,
    ACTION_INDICATOR)
values
   (2, null, null, 'Remove', 'Remove', null, 'R');
insert into ACTION_CODE
   (ACTION_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_ACTION_CODE,
    ACTION_INDICATOR)
values
   (3, null, null, 'Replace', 'Replace (remove and install)', null, 'B');
insert into ACTION_CODE
   (ACTION_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_ACTION_CODE,
    ACTION_INDICATOR)
values
   (4, null, null, 'Abandon', 'Abandon', null, 'A');
insert into ACTION_CODE
   (ACTION_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_ACTION_CODE,
    ACTION_INDICATOR)
values
   (5, null, null, 'Salvage', 'Salvage', null, 'S');
insert into ACTION_CODE
   (ACTION_CODE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_ACTION_CODE,
    ACTION_INDICATOR)
values
   (6, null, null, 'Transfer', 'Transfer', null, 'T');

create table ACTION_INDICATOR_XLAT
(
 ACTION_INDICATOR      varchar2(1) not null,
 ACTION_INDICATOR_XLAT varchar2(1) not null,
 EXPENDITURE_TYPE_ID   number(22),
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 DESCRIPTION           varchar2(35)
);

alter table ACTION_INDICATOR_XLAT
   add constraint ACTION_INDICATOR_PK
       primary key (ACTION_INDICATOR, ACTION_INDICATOR_XLAT)
       using index tablespace PWRPLANT_IDX;

insert into PWRPLANT.ACTION_INDICATOR_XLAT
   (ACTION_INDICATOR, ACTION_INDICATOR_XLAT, EXPENDITURE_TYPE_ID, DESCRIPTION)
   select 'I', 'I', 1, 'Install'         from DUAL union all
   select 'R', 'R', 2, 'Remove'          from DUAL union all
   select 'B', 'I', 1, 'Replace-Install' from DUAL union all
   select 'B', 'R', 2, 'Replace-Remove'  from DUAL union all
   select 'A', 'A', 2, 'Abandon'         from DUAL union all
   select 'S', 'S', 2, 'Salvage'         from DUAL union all
   select 'T', 'T', 3, 'Transfer'        from DUAL;

--work situation--
create table WORK_SITUATION
(
 WORK_SITUATION_ID  number(22) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(35) not null,
 LABOR_HRS_LOAD_PCT number(22,8)
);

alter table WORK_SITUATION
   add constraint WORK_SITUATION_PK
       primary key (WORK_SITUATION_ID)
       using index tablespace PWRPLANT_IDX;

insert into WORK_SITUATION
   (WORK_SITUATION_ID, TIME_STAMP, USER_ID, DESCRIPTION, LABOR_HRS_LOAD_PCT)
values
   (1, null, null, 'Frozen Ground', 0.1);

--* Wo Eng Estimate Tables

--wo_eng_estimate--
alter table WO_ENG_ESTIMATE
   add (ACTION_CODE_ID    number(22,0),
        WORK_SITUATION_ID number(22,0),
        ASSET_LOCATION_ID number(22,0));

--wo_eng_est_child--
create table WO_ENG_EST_CHILD
(
 WO_ENG_EST_CHILD_ID number(22,0) not null,
 ENG_ESTIMATE_ID     number(22,0) not null,
 ENG_REVISION        number(22,0) not null,
 WORK_ORDER_ID       number(22,0) not null,
 ACTION_CODE_ID      number(22,0),
 WORK_TYPE_ID        number(22,0),
 COMP_UNIT_ID        number(22,0) not null,
 CREW_TYPE_ID        number(22,0),
 BUS_SEGMENT_ID      number(22,0) not null,
 UTILITY_ACCOUNT_ID  number(22,0) not null,
 SUB_ACCOUNT_ID      number(22,0),
 RETIREMENT_UNIT_ID  number(22,0),
 CU_QUANTITY         number(22,2) not null,
 MATERIAL_COST       number(22,2) not null,
 LABOR_HOURS         number(22,4) not null,
 LABOR_AMOUNT        number(22,2) not null,
 OTHER_AMOUNT        number(22,2) not null,
 NOTES               varchar2(254),
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 ASSET_LOCATION_ID   number(22,0)
);

create index WOENGC_CUID_IX
   on WO_ENG_EST_CHILD (COMP_UNIT_ID)
      tablespace PWRPLANT_IDX;

create index WOENGC_WOID_IX
   on WO_ENG_EST_CHILD (WORK_ORDER_ID)
      tablespace PWRPLANT_IDX;

alter table WO_ENG_EST_CHILD
   add constraint WO_ENG_EST_CHILD_PK
       primary key (WO_ENG_EST_CHILD_ID)
       using index tablespace PWRPLANT_IDX;


--wo_eng_est_detail--
alter table WO_ENG_EST_DETAIL
   drop (QUANTITY,
         MATERIAL_COST,
         LABOR_COST,
         OTHER_COST,
         HOURS);

alter table WO_ENG_EST_DETAIL
   add (ACTION_CODE_ID    number(22,0),
        WORK_TYPE_ID      number(22,0),
        COMP_UNIT_ID      number(22,0),
        STCK_KEEP_UNIT_ID number(22,0),
        MATERIAL_QUANTITY number(22,2),
        MATERIAL_AMOUNT   number(22,2),
        OTHER_AMOUNT      number(22,2),
        NOTES             varchar2(254),
        ASSET_LOCATION_ID number(22,0));


--foreign keys--
alter table COMPATIBLE_UNIT
   add constraint comp_unit_uom_fk
       foreign key (UNIT_OF_MEASURE_ID)
       references UNIT_OF_MEASURE (UNIT_OF_MEASURE_ID);

alter table COMPATIBLE_UNIT
   add constraint COMP_UNIT_CREW_FK
       foreign key (CREW_TYPE_ID)
       references CREW_TYPE (CREW_TYPE_ID);

alter table COMP_UNIT_HIERARCHY
   add constraint COMP_UNIT_HIER_PCU_FK
       foreign key (PARENT_COMP_UNIT_ID)
       references COMPATIBLE_UNIT (COMP_UNIT_ID);

alter table COMP_UNIT_HIERARCHY
   add constraint COMP_UNIT_HIER_CU_FK
       foreign key (COMP_UNIT_ID)
       references COMPATIBLE_UNIT (COMP_UNIT_ID);

alter table COMP_UNIT_ACTION_CODE_STD
   add constraint CU_ACTCD_CU_FK
       foreign key (COMP_UNIT_ID)
       references COMPATIBLE_UNIT (COMP_UNIT_ID);

alter table COMP_UNIT_ACTION_CODE_STD
   add constraint CU_ACTCD_WT_FK
       foreign key (WORK_TYPE_ID)
       references WORK_TYPE (WORK_TYPE_ID);

alter table COMP_UNIT_WORK_TYPE
   add constraint CU_WKTYPE_CU_FK
       foreign key (COMP_UNIT_ID)
       references COMPATIBLE_UNIT (COMP_UNIT_ID);

alter table COMP_UNIT_WORK_TYPE
   add constraint CU_WKTYPE_WT_FK
       foreign key (WORK_TYPE_ID)
       references WORK_TYPE (WORK_TYPE_ID);

alter table COMP_UNIT_WORK_TYPE
   add constraint CU_WKTYPE_SA_FK
       foreign key (UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID)
       references SUB_ACCOUNT (UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID);

alter table COMP_UNIT_WORK_TYPE
   add constraint CU_WKTYPE_RU_FK
       foreign key (RETIREMENT_UNIT_ID)
       references RETIREMENT_UNIT (RETIREMENT_UNIT_ID);

alter table COMP_UNIT_WORK_TYPE
   add constraint CU_WKTYPE_PG_FK
       foreign key (PROPERTY_GROUP_ID)
       references PROPERTY_GROUP (PROPERTY_GROUP_ID);

alter table COMP_UNIT_WORK_TYPE
   add constraint CU_WKTYPE_EXPGL_FK
       foreign key (EXPENSE_GL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table STCK_KEEP_UNIT
   add constraint SKU_UOM_FK
       foreign key (UNIT_OF_MEASURE_ID)
       references UNIT_OF_MEASURE (UNIT_OF_MEASURE_ID);

alter table STCK_KEEP_UNIT
   add constraint SKU_STCKTYPE_FK
       foreign key (STOCK_TYPE_ID)
       references STOCK_TYPE (STOCK_TYPE_ID);

alter table STCK_KEEP_UNIT
   add constraint SKU_STATUSCODE_FK
       foreign key (STATUS_CODE_ID)
       references STATUS_CODE (STATUS_CODE_ID);

alter table ACTION_INDICATOR_XLAT
   add constraint actindxlat_et_fk
       foreign key (EXPENDITURE_TYPE_ID)
       references EXPENDITURE_TYPE (EXPENDITURE_TYPE_ID);

alter table WO_ENG_ESTIMATE
   add constraint WOENGEST_ACTCD_FK
       foreign key (ACTION_CODE_ID)
       references ACTION_CODE (ACTION_CODE_ID);

alter table WO_ENG_ESTIMATE
   add constraint WOENGEST_WRKSIT_FK
       foreign key (WORK_SITUATION_ID)
       references WORK_SITUATION (WORK_SITUATION_ID);

alter table WO_ENG_ESTIMATE
   add constraint WOENGEST_AL_FK
       foreign key (ASSET_LOCATION_ID)
       references ASSET_LOCATION (ASSET_LOCATION_ID);

--table doc--
comment on table COMPATIBLE_UNIT is '(O) [04] [13] The Compatible Unit data table lists all compatible units.';
comment on column COMPATIBLE_UNIT.MAJOR_UNIT_INDICATOR is 'Yes/No (1/0) flag indicating whether this compatible unit corresponds to a retirement unit.';
comment on column COMPATIBLE_UNIT.PRECAPITALIZED_INDICATOR is 'Yes/No (1/0) flag indicating whether this compatible unit has previously been capitalized.';
comment on column COMPATIBLE_UNIT.STATUS_CODE_ID is 'System-assigned identifier of a compatible unit status.';
comment on column COMPATIBLE_UNIT.UNIT_OF_MEASURE_ID is 'System-assigned identifier of a particular unit of measure, such as "each", "feet", etc.';
comment on column COMPATIBLE_UNIT.NOTES is 'Optional user notes.';
comment on column COMPATIBLE_UNIT.CATALOG_NUMBER is 'Optional freeform field for referencing a catalog number.';
comment on column COMPATIBLE_UNIT.CREW_TYPE_ID is 'System-assigned identifier of a particular crew type that is responsible for doing the work associated with this compatible unit.';
comment on column COMPATIBLE_UNIT.NONSTOCK_UNIT_PRICE is 'Price of the compatible unit not kept in stock (not related to any stock keeping units). (See the table COMP_UNIT_STOCK_KEEPING_UNIT.)';
comment on column COMPATIBLE_UNIT.PRECAP_NONSTOCK_UNIT_PRICE is 'Price of the pre-capitalized compatible unit not kept in stock (not related to any stock keeping units).';
comment on column COMPATIBLE_UNIT.HAS_CHILDREN is 'Yes/No (1/0) flag corresponding to whether this compatible unit is a parent compatible unit for other compatible units. (See the table COMP_UNIT_HIERARCHY.)';
comment on column COMPATIBLE_UNIT.EXPENSE_OVERRIDE is 'Yes/No (1/0) flag indicating whether the accounting for this compatible unit should be treated as expense when it might normally be treated as capital.';
comment on column COMPATIBLE_UNIT.INVOLVES_DIGGING is 'Yes/No (1/0) flag indicating whether work for this compatible unit involves digging.';

comment on table COMP_UNIT_HIERARCHY is '(O) [04] [13] The Comp Unit Hierarchy table maintains the relationship between detailed compatible units (which may relate to stock keeping units) and their parent compatible units (used for grouping compatible units for convenience of estimating).';
comment on column COMP_UNIT_HIERARCHY.PARENT_COMP_UNIT_ID is 'System-assigned identifier of the parent compatible unit for the applicable compatible unit. Parent compatible units cannot be related to stock keeping units; they are strictly used to aggregate one or more detailed compatible units.';
comment on column COMP_UNIT_HIERARCHY.COMP_UNIT_ID is 'System-assigned identifier of the child compatible unit. Only compatible units that do not have any children can be related to stock keeping units.';
comment on column COMP_UNIT_HIERARCHY.ACTION_INDICATOR is 'System-assigned identifier indicating whether the child compatible unit is being used within the parent compatible unit in an install or remove capacity.';
comment on column COMP_UNIT_HIERARCHY.QUANTITY is 'Number of child compatible units that apply for each parent compatible unit being used.';
comment on column COMP_UNIT_HIERARCHY.QUANTITY_FIXED is 'Additional number of child compatible units that are being used regardless of the number of compatible units being used.';
comment on column COMP_UNIT_HIERARCHY.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column COMP_UNIT_HIERARCHY.USER_ID is 'Standard system-assigned user_id used for audit purposes.';

comment on table COMP_UNIT_ACTION_CODE_STD is '(O) [04] [13] The Comp Unit Action Code Std table maintains the labor standards for each compatible unit according to the action (install, remove, replace).';
comment on column COMP_UNIT_ACTION_CODE_STD.COMP_UNIT_STD_ID is 'System-assigned identifier of the labor standards.';
comment on column COMP_UNIT_ACTION_CODE_STD.EFFECTIVE_DATE is 'Date at which this standard went into effect.';
comment on column COMP_UNIT_ACTION_CODE_STD.COMP_UNIT_ID is 'System-assigned identifier of the compatible unit.';
comment on column COMP_UNIT_ACTION_CODE_STD.ACTION_INDICATOR is 'System-assigned identifier indicating whether the compatible unit is being used in an install, remove, or replace capacity.';
comment on column COMP_UNIT_ACTION_CODE_STD.WORK_TYPE_ID is 'System-assigned identifier for the Work Type.';
comment on column COMP_UNIT_ACTION_CODE_STD.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column COMP_UNIT_ACTION_CODE_STD.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column COMP_UNIT_ACTION_CODE_STD.STANDARD_LABOR_HRS_VAR is 'Number of hours that apply to the action for each compatible unit being used.';
comment on column COMP_UNIT_ACTION_CODE_STD.STANDARD_LABOR_HRS_FIX is 'Additional mumber of hous that apply to the action regardless of the number of compatible units being used.';

comment on table COMP_UNIT_STOCK_KEEPING_UNIT is '(O) [04] [13] The Compatible Unit Stock Keeping Unit data table maintains the "bill of material" for each compatible unit in terms of stock keeping units or bin locations (items and quantities).';
comment on column COMP_UNIT_STOCK_KEEPING_UNIT.QUANTITY_FIXED_ADD is 'Additional number of SKUs regardless of the number of compatible units being used.';
comment on column COMP_UNIT_STOCK_KEEPING_UNIT.QUANTITY_LOAD_PCT is 'Additional multiplier to the number of SKUs. (E.g., multiplier to account for swag in wire between poles.)';
comment on column COMP_UNIT_STOCK_KEEPING_UNIT.UNIT_OF_MEASURE_CONV is 'Conversion multiplier used when SKUs have a different Unit_Of_Measure_Id than the compatible unit.';

comment on table COMP_UNIT_WORK_TYPE is '(O) [04] [13] The Comp Unit / Work Type data table maintains the relationships between compatible units and work types set up by the user. Each combination will drive the proper accounting based on the applicable mappings.';
comment on column COMP_UNIT_WORK_TYPE.COMP_UNIT_ID is 'System-assigned identifier of the compatible unit.';
comment on column COMP_UNIT_WORK_TYPE.WORK_TYPE_ID is 'System-assigned identifier for the Work Type Attribute.';
comment on column COMP_UNIT_WORK_TYPE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column COMP_UNIT_WORK_TYPE.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column COMP_UNIT_WORK_TYPE.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.';
comment on column COMP_UNIT_WORK_TYPE.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column COMP_UNIT_WORK_TYPE.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column COMP_UNIT_WORK_TYPE.RETIREMENT_UNIT_ID is 'System-assigned identifier of a particular retirement unit.';
comment on column COMP_UNIT_WORK_TYPE.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column COMP_UNIT_WORK_TYPE.EXPENSE_GL_ACCOUNT_ID is 'System-assigned identifier of a unique Expense General Ledger account.';

comment on table STCK_KEEP_UNIT is '(O) [04] [13] The Stock Keeping Unit data table maintains a list of all stock keeping units maintained by a utility.';
comment on column STCK_KEEP_UNIT.unit_of_measure_id is 'System-assigned identifier of a particular unit of measure, such as "each", "feet", etc.';
comment on column STCK_KEEP_UNIT.stock_type_id is 'System-assigned identifier of the stock type.';
comment on column STCK_KEEP_UNIT.serialized is 'Flag indicating whether this particular stock keeping unit is individually tracked.';
comment on column STCK_KEEP_UNIT.precapitalized_indicator is 'Yes/No (1/0) flag indicating whether this stock keeping unit has previously been capitalized.';
comment on column STCK_KEEP_UNIT.status_code_id is 'System-assigned identifier of a stock keeping unit status.';
comment on column STCK_KEEP_UNIT.total_qty_in_stock is 'Total number of items that are currently in stock.';
comment on column STCK_KEEP_UNIT.total_qty_on_order is 'Total number of items that are on order.';
comment on column STCK_KEEP_UNIT.building_cd is 'The building in which this stock keeping item resides.';
comment on column STCK_KEEP_UNIT.floor_cd is 'The floor in the building in which this stock keeping item resides.';
comment on column STCK_KEEP_UNIT.section_cd is 'The section of the floor or building in which this stock keeping item resides.';
comment on column STCK_KEEP_UNIT.aisle_cd is 'The aisle in which this stock keeping item resides.';
comment on column STCK_KEEP_UNIT.row_cd is 'The row in which this stock keeping item resides.';
comment on column STCK_KEEP_UNIT.bin_cd is 'The bin in which this stock keeping item resides.';

comment on table STCK_KEEP_UNIT_PRICE_HIST is '(O) [04] [13] The Stock Keeping Unit Price History data table maintains the historical changes to the stock keeping unit prices.';
comment on column STCK_KEEP_UNIT_PRICE_HIST.ID is 'System-assigned identifier of a historical record of changes to sku prices.';
comment on column STCK_KEEP_UNIT_PRICE_HIST.ARCHIVED_DATE is 'Date this record was archived.';
comment on column STCK_KEEP_UNIT_PRICE_HIST.STCK_KEEP_UNIT_ID is 'System-assigned identifier of a particular stock keeping unit.';
comment on column STCK_KEEP_UNIT_PRICE_HIST.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column STCK_KEEP_UNIT_PRICE_HIST.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column STCK_KEEP_UNIT_PRICE_HIST.UNIT_PRICE is 'Price per unit of this particular stock keeping unit.';

comment on table STOCK_TYPE is '(O) [04] [13] The Stock Type data table maintains the list of types of stock; for example, items that are either truck stocked or stored in a warehouse. (See the STCK_KEEP_UNIT table.)';
comment on column STOCK_TYPE.STOCK_TYPE_ID is 'System-assigned identifier of the stock type.';
comment on column STOCK_TYPE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column STOCK_TYPE.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column STOCK_TYPE.DESCRIPTION is 'The description of the stock type.';
comment on column STOCK_TYPE.LONG_DESCRIPTION is 'The long description of the stock type.';

comment on table CREW_TYPE is '(O) [04] [13] The Crew Type data table maintains the list of types of crews that typically do work; for example, one or more line crews may have specific hourly rates and only work on certain types of Compatible Units.';
comment on column CREW_TYPE.CREW_TYPE_ID is 'System-assigned identifier of the crew type.';
comment on column CREW_TYPE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column CREW_TYPE.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column CREW_TYPE.DESCRIPTION is 'The description of the crew type.';
comment on column CREW_TYPE.LONG_DESCRIPTION is 'The long description of the crew type.';
comment on column CREW_TYPE.EXTERNAL_VALUE is 'External value that might identify this crew type for interface purposes.';
comment on column CREW_TYPE.HOURLY_RATE is 'The hourly rate for this particular crew type.';
comment on column CREW_TYPE.WO_ENG_EST_TYPE is 'A freeform field that, when populated, will identify which Estimate Charge Type to which the costs for this particular crew type will be mapped when populating the unit estimates. (Note: a corresponding value must be entered in the WO_ENG_EST_TYPE field in the ESTIMATE_CHARGE_TYPE table.)';

comment on table CREW_TYPE_RATE_HIST is '(O) [04] [13] The Crew Type Rate History data table maintains the historical changes to the crew type hourly rates.';
comment on column CREW_TYPE_RATE_HIST.ID is 'System-assigned identifier of a historical record of changes to sku prices.';
comment on column CREW_TYPE_RATE_HIST.ARCHIVED_DATE is 'Date this record was archived.';
comment on column CREW_TYPE_RATE_HIST.CREW_TYPE_ID is 'System-assigned identifier of a particular crew type.';
comment on column CREW_TYPE_RATE_HIST.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column CREW_TYPE_RATE_HIST.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column CREW_TYPE_RATE_HIST.HOURLY_RATE is 'Rate per hour for this particular crew type.';

comment on table ESTIMATE_CHARGE_TYPE is '(S) [01] [04] [12] [13] The Estimate Charge Type table maintains the manner in which the detailed charge types used by a utility are summarized for the purposes of estimates for work orders, funded projects, and budgets. For example, if the company has multiple charge types for labor, they could be summarized to one labor charge type for estimates (see the CHARGE TYPE table). Note that there can be two types of estimated charge types – one for work orders and one for funded (projects) and budgets – as denoted by the funding charge indicator.';
comment on column ESTIMATE_CHARGE_TYPE.WO_ENG_EST_TYPE is 'A freeform field that, when populated, will identify which Estimate Charge Type to which the costs from the Compatible Unit estimates will be mapped when populating the Unit Estimates. (See the CREW_TYPE table for labor costs. "STOCK" should be used for stock material costs.)';

comment on table ACTION_CODE is '(O) [04] [13] The Action Code data table maintains the configurable list of action codes used in the Compatible Unit estimating functionality; for example, Install, Remove, Replace, Abandon, Salvage, and Transfer.';
comment on column ACTION_CODE.ACTION_CODE_ID is 'System-assigned identifier of the action code.';
comment on column ACTION_CODE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ACTION_CODE.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column ACTION_CODE.DESCRIPTION is 'The description of the action code.';
comment on column ACTION_CODE.LONG_DESCRIPTION is 'The long description of the action code.';
comment on column ACTION_CODE.EXTERNAL_ACTION_CODE is 'External value that might identify this action code for interface purposes.';
comment on column ACTION_CODE.ACTION_INDICATOR is 'Fixed list of values that are used to programmatically identify the intended purpose of the action code: I=Install; R=Remve; B=Replace (Both Install & Remove); A=Abandon; S=Salvage; T=Transfer. (See the ACTION_INDICATOR_XLAT table.)';

comment on table ACTION_INDICATOR_XLAT is '(F) [04] [13] The Action Indicator Translate data table translates the Action Indicator for purposes of interfacing to the Unit Estimates.';
comment on column ACTION_INDICATOR_XLAT.ACTION_INDICATOR is 'Fixed list of values that are used to programmatically identify the intended purpose of the action code: I=Install; R=Remve; B=Replace (Both Install & Remove); A=Abandon; S=Salvage; T=Transfer.';
comment on column ACTION_INDICATOR_XLAT.ACTION_INDICATOR_XLAT is 'Translated action indicator. Same as the action indicator in all cases except for Replace (B) in which case it is mapped to both Install (I) as well as Remove (R).';
comment on column ACTION_INDICATOR_XLAT.EXPENDITURE_TYPE_ID is 'Translated expenditure type.';
comment on column ACTION_INDICATOR_XLAT.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ACTION_INDICATOR_XLAT.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column ACTION_INDICATOR_XLAT.DESCRIPTION is 'The description of the action indicator.';

comment on table WORK_SITUATION is '(O) [04] [13] The Work Situation data table maintains the various conditions that crews may face that would increase or perhaps decrease the labor hours involved in a set of work.';
comment on column WORK_SITUATION.WORK_SITUATION_ID is 'System-assigned identifier of the work situation.';
comment on column WORK_SITUATION.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column WORK_SITUATION.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column WORK_SITUATION.DESCRIPTION is 'The description of the work situation.';
comment on column WORK_SITUATION.LABOR_HRS_LOAD_PCT is 'The percentage multiplier by which to scale the number of hours worked. (E.g., 0.10 will increase the amount of hours by 10%.)';

comment on table WO_ENG_ESTIMATE is '(O) [04] [13] The WO Eng Estimate table holds compatible unit estimates for a work order by engineering revisions.';
comment on column WO_ENG_ESTIMATE.ACTION_CODE_ID is 'System-assigned identifier of the action code applying to the particular compatible unit estimate.';
comment on column WO_ENG_ESTIMATE.WORK_SITUATION_ID is 'System-assigned identifier of the work situation applying to the particular compatible unit estimate.';
comment on column WO_ENG_ESTIMATE.ASSET_LOCATION_ID is 'System-assigned identifier of the asset location applying to the particular compatible unit estimate.';

comment on table WO_ENG_EST_CHILD is '(C) [04] [13] The WO Eng Est Child table records the asset related data based on the entry of the compatible unit estimate. This table is computed based on the compatible unit estimates (broken out to the child compatible units) and the supporting configuration.';
comment on column WO_ENG_EST_CHILD.WO_ENG_EST_CHILD_ID is 'System identifier for a engineering estimate child record.';
comment on column WO_ENG_EST_CHILD.ENG_ESTIMATE_ID is 'System identifier for a compatible unit estimate record.';
comment on column WO_ENG_EST_CHILD.ENG_REVISION is 'System identifier for a revision of engineering estimate.';
comment on column WO_ENG_EST_CHILD.WORK_ORDER_ID is 'System identifier for a work order.';
comment on column WO_ENG_EST_CHILD.ACTION_CODE_ID is 'System-assigned identifier of the action code applying to the particular compatible unit estimate.';
comment on column WO_ENG_EST_CHILD.WORK_TYPE_ID is 'System-assigned identifier for the Work Type Attribute.';
comment on column WO_ENG_EST_CHILD.COMP_UNIT_ID is 'System-assigned identifier of the compatible unit.';
comment on column WO_ENG_EST_CHILD.CREW_TYPE_ID is 'System-assigned identifier of a particular crew type that is responsible for doing the work associated with this compatible unit.';
comment on column WO_ENG_EST_CHILD.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column WO_ENG_EST_CHILD.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.';
comment on column WO_ENG_EST_CHILD.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column WO_ENG_EST_CHILD.RETIREMENT_UNIT_ID is 'System-assigned identifier of a particular retirement unit.';
comment on column WO_ENG_EST_CHILD.CU_QUANTITY is 'Total quantity of the applicable compatible unit for this particular estimate record.';
comment on column WO_ENG_EST_CHILD.MATERIAL_COST is 'Total calculated material cost for the compatible unit estimate record.';
comment on column WO_ENG_EST_CHILD.LABOR_HOURS is 'Total calculated labor hours for the compatible unit estimate record.';
comment on column WO_ENG_EST_CHILD.LABOR_AMOUNT is 'Total calculated labor cost for the compatible unit estimate record.';
comment on column WO_ENG_EST_CHILD.OTHER_AMOUNT is 'Additional costs not provided through the compatible unit catalog standards.';
comment on column WO_ENG_EST_CHILD.NOTES is 'Optional user notes.';
comment on column WO_ENG_EST_CHILD.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column WO_ENG_EST_CHILD.USER_ID is 'Standard system-assigned user_id used for audit purposes.';
comment on column WO_ENG_EST_CHILD.ASSET_LOCATION_ID is 'System-assigned identifier of the asset location applying to the particular compatible unit estimate.';

comment on table WO_ENG_EST_DETAIL is '(C) [04] [13] The WO Eng Est Detail table records the related stock-level asset data based on the entry of the compatible unit estimate, broken out by the individual stock keeping units. This table is computed based on the compatible unit estimates and the supporting configuration.';
comment on column WO_ENG_EST_DETAIL.ACTION_CODE_ID is 'System-assigned identifier of the action code applying to the particular compatible unit estimate.';
comment on column WO_ENG_EST_DETAIL.WORK_TYPE_ID is 'System-assigned identifier for the Work Type Attribute.';
comment on column WO_ENG_EST_DETAIL.COMP_UNIT_ID is 'System-assigned identifier of the compatible unit.';
comment on column WO_ENG_EST_DETAIL.STCK_KEEP_UNIT_ID is 'System-assigned identifier of a particular stock keeping unit.';
comment on column WO_ENG_EST_DETAIL.MATERIAL_QUANTITY is 'Total calculated material quantity for the stock keeping unit estimate detail.';
comment on column WO_ENG_EST_DETAIL.MATERIAL_AMOUNT is 'Total calculated material cost for the stock keeping unit estimate detail.';
comment on column WO_ENG_EST_DETAIL.OTHER_AMOUNT is 'Additional costs not provided through the compatible unit catalog standards.';
comment on column WO_ENG_EST_DETAIL.NOTES is 'Optional user notes.';
comment on column WO_ENG_EST_DETAIL.ASSET_LOCATION_ID is 'System-assigned identifier of the asset location applying to the particular stock keeping unit estimate detail.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (555, 0, 10, 4, 1, 0, 29351, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029351_sys_comp_unit_update.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON