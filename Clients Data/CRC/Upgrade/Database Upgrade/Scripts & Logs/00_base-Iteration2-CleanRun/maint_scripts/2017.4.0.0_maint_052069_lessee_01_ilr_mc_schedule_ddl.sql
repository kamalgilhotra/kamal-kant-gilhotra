/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052069_lessee_01_ilr_mc_schedule_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.4.0.0 07/24/2018 Sarah Byers       Restrict ILR views to Actual Exchange Rate Type
||============================================================================
*/

CREATE OR REPLACE FORCE VIEW V_LS_ILR_CLASS_TEST_FX_VW
 AS
WITH cur AS (
  SELECT ls_currency_type_id AS ls_cur_type,
    currency_id,
    currency_display_symbol,
    iso_code,
    CASE ls_currency_type_id
      WHEN 1
      THEN 1
      ELSE NULL
    END AS contract_approval_rate
  FROM currency
  CROSS JOIN ls_lease_currency_type
)
SELECT ls_ilr.ilr_id,
  ls_ilr_classification_test.revision,
  ls_ilr_classification_test.user_id,
  ls_ilr_classification_test.time_stamp,
  ls_ilr_classification_test.finance_tests,
  ls_ilr_classification_test.msg_finance_tests,
  ls_ilr_classification_test.economic_life,
  ls_ilr_classification_test.msg_economic_life,
  ls_ilr_classification_test.net_present_value,
  ls_ilr_classification_test.msg_net_present_value,
  ls_ilr_classification_test.transfer_of_ownership,
  ls_ilr_classification_test.msg_transfer_of_ownership,
  ls_ilr_classification_test.intent_to_purchase,
  ls_ilr_classification_test.msg_intent_to_purchase,
  ls_ilr_classification_test.specialized_asset,
  ls_ilr_classification_test.msg_specialized_asset,
  ls_ilr_classification_test.economic_life_percent,
  ls_ilr_classification_test.min_economic_life,
  ls_ilr_classification_test.max_economic_life,
  ls_ilr_classification_test.lease_term,
  ls_ilr_classification_test.fmv * NVL(cur.contract_approval_rate, cur_rate.rate) AS fmv,
  ls_ilr_classification_test.npv * NVL(cur.contract_approval_rate, cur_rate.rate) AS npv,
  ls_ilr_classification_test.npv_percent,
  NVL(cur.contract_approval_rate, cur_rate.rate) AS rate,
  cur.ls_cur_type,
  cur.currency_id,
  cur.currency_display_symbol,
  cur.iso_code
FROM cur,
  ls_ilr,
  ls_lease,
  ls_ilr_classification_test,
  currency_schema cs,
  currency_rate_default_dense cur_rate
WHERE ls_ilr.company_id               = cs.company_id
AND ls_ilr.lease_id                   = ls_lease.lease_id
AND ls_ilr.current_revision           = ls_ilr_classification_test.revision
AND ls_ilr_classification_test.ilr_id = ls_ilr.ilr_id
AND cur.currency_id                   =
  CASE cur.ls_cur_type
    WHEN 1
    THEN ls_lease.contract_currency_id
    WHEN 2
    THEN cs.currency_id
    ELSE NULL
  END
AND cur_rate.exchange_date        = to_date(TO_CHAR(sysdate,'YYYYMM'),'YYYYMM')
AND cs.currency_id                = cur_rate.currency_to
AND ls_lease.contract_currency_id = cur_rate.currency_from
AND cs.currency_type_id           = 1
and cur_rate.exchange_rate_type_id = 1;

CREATE OR REPLACE VIEW v_ls_ilr_mc_options (
     company,
       set_of_books,
       lease_group,
       ilr_id,
       Cancelable_Type,
       Termination_amt,
       Bargain_Purchase,
       purchase_opt_amt,
       Renew_opt,
       renewal_period_start,
       renewal_period_end,
       terms_per_renewal,
       amount_per_term,
     renew_prob_id,
       Renew_notice_req,
       lease_cap_type,
     rate,
     ls_cur_type,
     currency_id,
       currency_display_symbol,
       iso_code
)  AS 
WITH cur AS (
  SELECT ls_currency_type_id AS ls_cur_type,
    currency_id,
    currency_display_symbol,
    iso_code,
    CASE ls_currency_type_id
      WHEN 1 THEN 1
      ELSE NULL
    END AS contract_approval_rate
  FROM currency
  CROSS JOIN ls_lease_currency_type
)
SELECT   company.company_id as company,
       set_of_books.set_of_books_id as set_of_books,
       ls_lease_group.lease_group_id as lease_group,
       ls_ilr.ilr_id as ilr_id,
       ls_ilr_options.cancelable_type_id AS Cancelable_Type,
       Nvl(ls_ilr_options.termination_amt,0) * Nvl(cur.contract_approval_rate, cur_rate.rate) AS Termination_amt,
       ls_ilr_options.purchase_option_type_id AS Bargain_Purchase,
       Nvl(ls_ilr_options.purchase_option_amt,0) * Nvl(cur.contract_approval_rate, cur_rate.rate) AS purchase_opt_amt,
       ls_ilr_options.renewal_option_type_id AS Renew_opt,
       ls_ilr_renewal_options.renewal_start_date AS renewal_period_start,
       Add_Months(ls_ilr_renewal_options.renewal_start_date, (ls_ilr_renewal_options.number_of_terms * decode(ls_ilr_renewal_options.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1))) AS renewal_period_end,
       Round((ls_ilr_renewal_options.number_of_terms/ls_ilr_renewal_options.payment_freq_id),2) AS terms_per_renewal,
       Nvl(ls_ilr_renewal_options.amount_per_term, 0 ) * Nvl(cur.contract_approval_rate, cur_rate.rate) AS amount_per_term,
       ls_ilr_renewal_options.ilr_renewal_probability_id as renew_prob_id,
       ls_ilr_renewal_options.notice_requirement_months AS Renew_notice_req,
       ls_lease_cap_type.ls_lease_cap_type_id as lease_cap_type,
	   Nvl(cur.contract_approval_rate, ls_ilr_options.in_service_exchange_rate) rate,
	   cur.ls_cur_type as ls_cur_type,
	   cur.currency_id as currency_id,
       cur.currency_display_symbol as currency_display_symbol,
       cur.iso_code as iso_code
       from  cur, ls_ilr, ls_lease, ls_fasb_cap_type_sob_map,
       ls_lease_cap_type, ls_ilr_options, ls_lease_group, 
       company, ls_ilr_group, set_of_books, ls_ilr_renewal_options,  
       ls_ilr_renewal_probability, ls_ilr_renewal, currency_schema cs, currency_rate_default_dense cur_rate
 where  ls_ilr.company_id = company.company_id
   AND company.company_id = cs.company_id
   AND  ls_ilr.lease_id = ls_lease.lease_id
   AND ls_ilr.current_revision = ls_ilr_options.revision
   and ls_lease.lease_group_id = ls_lease_group.lease_group_id
   and ls_ilr_group.ilr_group_id = ls_ilr.ilr_group_id                         
   and ls_lease_cap_type.ls_lease_cap_type_id = ls_fasb_cap_type_sob_map.lease_cap_type_id
   and ls_ilr_options.lease_cap_type_id = ls_lease_cap_type.ls_lease_cap_type_id
   AND set_of_books.set_of_books_id = ls_fasb_cap_type_sob_map.set_of_books_id
   and ls_ilr_options.ilr_id = ls_ilr.ilr_id
   AND ls_ilr.ilr_id =  ls_ilr_renewal.ilr_id
   AND ls_ilr_renewal_probability.ilr_renewal_probability_id = ls_ilr_renewal_options.ilr_renewal_probability_id
   AND ls_ilr_renewal.ilr_renewal_id = ls_ilr_renewal_options.ilr_renewal_id
   AND ls_ilr_renewal.revision = ls_ilr.current_revision         
   and ls_fasb_cap_type_sob_map.fasb_cap_type_id in (1, 2, 4, 5)
   AND cur.currency_id =
       CASE cur.ls_cur_type
        WHEN 1 THEN ls_lease.contract_currency_id
        WHEN 2 THEN cs.currency_id
        ELSE NULL
      END
    AND cur_rate.exchange_date = ls_ilr_renewal_options.renewal_start_date
    AND cs.currency_id = cur_rate.currency_to
    AND ls_lease.contract_currency_id = cur_rate.currency_from
    AND cs.currency_type_id = 1
    and cur_rate.exchange_rate_type_id = 1;


  CREATE OR REPLACE VIEW "PWRPLANT"."V_LS_MONTHLY_TAX_FX" ("LS_ASSET_ID", "COMPANY_ID", "GL_POSTING_MO_YR", "SET_OF_BOOKS_ID", "TAX_LOCAL_ID", "ACCRUAL", "SCHEDULE_MONTH", "TAX_DISTRICT_ID", "TAX_RATE", "AMOUNT", "ADJUSTMENT_AMOUNT", "TAX_BASE", "CONTRACT_CURRENCY_ID", "COMPANY_CURRENCY_ID", "LS_CUR_TYPE", "EXCHANGE_DATE", "RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL") AS 
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code
           FROM   currency company_cur),
       cr
       AS (SELECT /*+ materialize */ exchange_date,
                                     currency_from,
                                     currency_to,
                                     rate
           FROM   currency_rate_default a
           WHERE  exchange_date = (SELECT Max(exchange_date)
                                   FROM   currency_rate_default b
                                   WHERE  To_char(a.exchange_date, 'yyyymm') =
                                          To_char(b.exchange_date, 'yyyymm')
                                          AND a.currency_from = b.currency_from
                                          AND a.currency_to = b.currency_to
                                          AND a.exchange_rate_type_id = b.exchange_rate_type_id)
             AND  exchange_rate_type_id = 1),
       calc_rate
       AS (SELECT /*+ materialize */ a.company_id,
                                     a.contract_currency_id,
                                     a.company_currency_id,
                                     a.accounting_month,
                                     a.exchange_date,
                                     a.rate,
                                     b.rate prev_rate
           FROM   ls_lease_calculated_date_rates a
                  left outer join ls_lease_calculated_date_rates b
                               ON a.company_id = b.company_id
                                  AND a.contract_currency_id =
                                      b.contract_currency_id
                                  AND a.accounting_month =
                                      Add_months(b.accounting_month,
                                      1)
                                  AND b.exchange_rate_type_id = 1
           where a.exchange_rate_type_id = 1)
  SELECT lmt.ls_asset_id,
         ass.company_id,
         lmt.gl_posting_mo_yr,
         lmt.set_of_books_id,
         lmt.tax_local_id,
         lmt.accrual,
         lmt.schedule_month,
         lmt.tax_district_id,
         lmt.rate                                       tax_rate,
         lmt.amount * Decode(ls_cur_type, 2, cr.rate,
                                          1)            amount,
         lmt.adjustment_amount * Decode(ls_cur_type, 2, cr.rate,
                                                     1) adjustment_amount,
         lmt.tax_base * Decode(ls_cur_type, 2, cr.rate,
                                            1)          tax_base,
         lease.contract_currency_id,
         cs.currency_id                                 company_currency_id,
         cur.ls_cur_type                                AS ls_cur_type,
         cr.exchange_date,
         Decode(ls_cur_type, 2, cr.rate,
                             1)                         rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_monthly_tax lmt
         inner join ls_asset ass
                 ON ass.ls_asset_id = lmt.ls_asset_id
         inner join currency_schema cs
                 ON ass.company_id = cs.company_id
         inner join ls_ilr ilr
                 ON ( ilr.ilr_id = ass.ilr_id
                      AND ilr.company_id = ass.company_id )
         inner join ls_lease lease
                 ON ( ilr.lease_id = lease.lease_id )
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = lease.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         inner join cr
                 ON cur.currency_id = cr.currency_to
                    AND lease.contract_currency_id = cr.currency_from
                    AND cr.exchange_date < Add_months(lmt.gl_posting_mo_yr, 1)
         left outer join calc_rate
                      ON lease.contract_currency_id =
                         calc_rate.contract_currency_id
                         AND cur.currency_id = calc_rate.company_currency_id
                         AND ass.company_id = calc_rate.company_id
                         AND lmt.gl_posting_mo_yr = calc_rate.accounting_month
  WHERE  cr.exchange_date = (SELECT Max(exchange_date)
                             FROM   cr cr2
                             WHERE  cr.currency_from = cr2.currency_from
                                    AND cr.currency_to = cr2.currency_to
                                    AND cr2.exchange_date <
                                        Add_months(lmt.gl_posting_mo_yr, 1))
         AND cs.currency_type_id = 1;


CREATE OR REPLACE VIEW "PWRPLANT"."V_LS_MONTHLY_TAX_FX_VW" ("LS_ASSET_ID", "TAX_LOCAL_ID", "GL_POSTING_MO_YR", "SET_OF_BOOKS_ID", "AMOUNT", "ACCRUAL", "SCHEDULE_MONTH", "VENDOR_ID", "TAX_DISTRICT_ID", "TAX_RATE", "ADJUSTMENT_AMOUNT", "TAX_BASE", "ILR_ID", "ILR_NUMBER", "LEASE_ID", "LEASE_NUMBER", "MONTH", "COMPANY_ID", "OPEN_MONTH", "LS_CUR_TYPE", "EXCHANGE_DATE", "PREV_EXCHANGE_DATE", "CONTRACT_CURRENCY_ID", "DISPLAY_CURRENCY_ID", "COMPANY_CURRENCY_ID", "RATE", "CALCULATED_RATE", "PREVIOUS_CALCULATED_RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL") AS 
  WITH
cur AS (
  select /*+ materialize */ ls_cur_type, currency_id, currency_display_symbol, iso_code, contract_approval_rate
  from (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
      contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 contract_approval_rate
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id,
      company_cur.currency_display_symbol, company_cur.iso_code, NULL
    FROM currency company_cur
  )
),
open_month AS (
  SELECT /*+ materialize */ company_id, Min(gl_posting_mo_yr) open_month
  FROM ls_process_control
  WHERE open_next IS NULL
  GROUP BY company_id
),
cr AS (
  SELECT /*+ materialize */ exchange_date, currency_from, currency_to, rate
  FROM currency_rate_default a
  WHERE exchange_date = (select max(exchange_date)
                         from currency_rate_default b
                         where to_char(a.exchange_date, 'yyyymm') = to_char(b.exchange_date, 'yyyymm')
                         and a.currency_from = b.currency_from and a.currency_to = b.currency_to
                         and a.exchange_rate_type_id = b.exchange_rate_type_id)
    AND exchange_rate_type_id = 1
),
calc_rate AS (
  SELECT /*+ materialize */ a.company_id, a.contract_currency_id, a.company_currency_id, a.accounting_month, a.exchange_date, a.rate, b.rate prev_rate
  FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b
  ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = Add_Months(b.accounting_month, 1)
  AND b.exchange_rate_type_id = 1
  WHERE a.exchange_rate_type_id = 1
)
SELECT /*+ no_merge */
 lmt.ls_asset_id,
 tax_local_id,
 gl_posting_Mo_yr,
 set_of_books_id,
 amount* Nvl(calc_rate.rate, cr.rate) amount,
 accrual* Nvl(calc_rate.rate, cr.rate) accrual,
 schedule_month,
 vendor_id,
 tax_district_id,
 lmt.rate                    tax_rate,
 adjustment_amount* Nvl(calc_rate.rate, cr.rate) adjustment_amount,
 tax_base* Nvl(calc_rate.rate, cr.rate) tax_base,
 ilr.ilr_id                  ilr_id,
 ilr.ilr_number,
 lease.lease_id,
 lease.lease_number,
 lmt.gl_posting_mo_yr        MONTH,
 open_month.company_id,
 open_month.open_month,
 cur.ls_cur_type             AS ls_cur_type,
 cr.exchange_date,
 calc_rate.exchange_date     prev_exchange_date,
 lease.contract_currency_id,
 cur.currency_id             display_currency_id,
 cs.currency_id company_currency_id,
 cr.rate,
 calc_rate.rate              calculated_rate,
 calc_rate.prev_rate         previous_calculated_rate,
 cur.iso_code,
 cur.currency_display_symbol
  FROM ls_monthly_tax lmt
 INNER JOIN ls_asset asst
    ON asst.ls_asset_id = lmt.ls_asset_id
 INNER JOIN ls_ilr ilr
    ON ilr.ilr_id = asst.ilr_id
   AND ilr.company_id = asst.company_id
 INNER JOIN ls_lease lease
    ON ilr.lease_id = lease.lease_id
 INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 INNER JOIN open_month
    ON ilr.company_id = open_month.company_id
 INNER JOIN cr
    ON cur.currency_id = cr.currency_to
   AND lease.contract_currency_id = cr.currency_from
   AND cr.exchange_date < Add_Months(lmt.schedule_month, 1)
  LEFT OUTER JOIN calc_rate
    ON lease.contract_currency_id = calc_rate.contract_currency_id
   AND cur.currency_id = calc_rate.company_currency_id
   AND ilr.company_id = calc_rate.company_id
   AND lmt.schedule_month = calc_rate.accounting_month
 WHERE cr.exchange_date =
       (SELECT Max(exchange_date)
          FROM cr cr2
         WHERE cr.currency_from = cr2.currency_from
           AND cr.currency_to = cr2.currency_to
           AND cr2.exchange_date < Add_Months(lmt.schedule_month, 1))
   AND cs.currency_type_id = 1
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8682, 0, 2017, 4, 0, 0, 52069, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052069_lessee_01_ilr_mc_schedule_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;