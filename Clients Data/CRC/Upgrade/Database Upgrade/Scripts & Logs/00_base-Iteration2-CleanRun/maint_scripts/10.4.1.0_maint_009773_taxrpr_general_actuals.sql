/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_009773_taxrpr_general_actuals.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- -------------------------------------
|| 10.4.1.0    08/19/2013 ALex P.        Point Release
||============================================================================
*/

alter table WO_EST_PROCESSING_TEMP
   add (WORK_ORDER_NUMBER varchar2(35),
        COMPANY_ID        number(22, 0) not null);

comment on column WO_EST_PROCESSING_TEMP.WORK_ORDER_NUMBER     is 'Work Order Number that corresponds to work order id in this table. Used when testing actuals.';
comment on column WO_EST_PROCESSING_TEMP.COMPANY_ID    is 'Standard system-assigned company identifier that corresponds to work order id in this table. Used when testing actuals.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (518, 0, 10, 4, 1, 0, 9773, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009773_taxrpr_general_actuals.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
