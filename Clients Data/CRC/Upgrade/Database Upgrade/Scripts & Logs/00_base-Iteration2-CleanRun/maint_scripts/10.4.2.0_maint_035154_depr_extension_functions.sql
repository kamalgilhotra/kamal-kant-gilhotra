/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035154_depr_extension_functions.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/15/2014 Brandon Beck
||============================================================================
*/

/* Uncomment if you want to reload all functions.  This could overwrite custom code.
drop function F_CLIENT_CPR_DEPR_ACCT_DISTRIB;
drop function F_CLIENT_CALC_CPR_DEPR_CUSTOM;
drop function F_CLIENT_DEPR_CALC_AND_APPROVE;
drop function F_CLIENT_PRE_APPROVE_DEPR_CO;
*/


create function F_CLIENT_CPR_DEPR_ACCT_DISTRIB(A_COMPANY_ID number,
                                               A_MONTH      date) return number is
-- *********************************************
-- In order to utilize an extension function.
-- The following table MUST be populated
-- pp_client_extensions:
--    id = A unique number
--    function_name = the name of the oracle function being implemented
--    is_active = 1 (Can be inactivated later by setting to 0)
-- *********************************************

begin
   --maps to PB function F_CPR_DEPR_ACCT_DISTRIB
   --RETURN 0 IF SUCCESS, -1 OTHERWISE
   return 0;
end F_CLIENT_CPR_DEPR_ACCT_DISTRIB;
/


create function F_CLIENT_CALC_CPR_DEPR_CUSTOM(A_COMPANY_ID number,
                                              A_MONTH      date) return number is
-- *********************************************
-- In order to utilize an extension function.
-- The following table MUST be populated
-- pp_client_extensions:
--    id = A unique number
--    function_name = the name of the oracle function being implemented
--    is_active = 1 (Can be inactivated later by setting to 0)
-- *********************************************
begin
   --maps to PB function F_CALC_CPR_DEPR_CUSTOM
   return 1;
end F_CLIENT_CALC_CPR_DEPR_CUSTOM;
/


   /*
     create function F_CLIENT_CALCULATE_DEPR_CUSTOM(A_CURR_MO number,
                                                    A_SET_OF_BOOKS number,
                                                    A_DEPR_GROUP number,
                                                    A_METHOD varchar2,
                                                    A_CONVENTION number,
                                                    A_SPREAD number,
                                                    A_EST_NET_ADDS number,
                                                    A_DEPR_METHOD number,
                                                    A_RATE number,
                                                    A_NET_GROSS number,
                                                    A_NET_SALVAGE number,
                                                    A_END_OF_LIFE number,
                                                    A_BEGIN_BAL_MONTH number,
                                                    A_BEGIN_RES_MONTH number,
    DO THIS FUNCTION LAST OR NEVER. IT IS GROSS
   */

create function F_CLIENT_DEPR_CALC_AND_APPROVE(A_COMPANY_ID  number,
                                               A_MONTH       date,
                                               A_CALLED_FROM varchar2) return number is
-- *********************************************
-- In order to utilize an extension function.
-- The following table MUST be populated
-- pp_client_extensions:
--    id = A unique number
--    function_name = the name of the oracle function being implemented
--    is_active = 1 (Can be inactivated later by setting to 0)
-- *********************************************
begin
   --maps to PB function F_DEPR_CALC_AND_APPROVE_CUSTOM
   --RETURN 0 IF SUCCESS, -1 OTHERWISE
   return 0;
end F_CLIENT_DEPR_CALC_AND_APPROVE;
/


create function F_CLIENT_PRE_APPROVE_DEPR_CO(A_COMPANY_ID number,
                                             A_MONTH      date) return varchar2 is
-- *********************************************
-- In order to utilize an extension function.
-- The following table MUST be populated
-- pp_client_extensions:
--    id = A unique number
--    function_name = the name of the oracle function being implemented
--    is_active = 1 (Can be inactivated later by setting to 0)
-- *********************************************
begin
   --maps to PB function F_PRE_APPROVE_DEPR_CO_CUSTOM
   return '';
end F_CLIENT_PRE_APPROVE_DEPR_CO;
/


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (864, 0, 10, 4, 2, 0, 35154, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035154_depr_extension_functions.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;