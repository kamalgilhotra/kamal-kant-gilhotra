/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036859_aro_calc_ssp.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/03/2014 Ryan Oliveria   ARO Calc Process
||============================================================================
*/

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED,
    SYSTEM_LOCK, ASYNC_EMAIL_ON_COMPLETE)
   select max(PROCESS_ID) + 1, 'ARO Processing', 'ARO Processing', 'aro_calc.exe', '10.4.3.0', 0, 0, 0, 0
     from PP_PROCESSES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1374, 0, 10, 4, 3, 0, 36859, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036859_aro_calc_ssp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
