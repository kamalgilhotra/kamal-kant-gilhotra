/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039103_reg_fcst_provision_logic.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.2.7 07/24/2014 Ryan Oliveria       Forecast Provision Integration Logic
||========================================================================================
*/

create table REG_FCST_TA_LOAD_STG
(
 REG_COMPANY_ID      number(22,0) not null,
 REG_ACCT_ID         number(22,0) not null,
 FORECAST_VERSION_ID number(22,0) not null,
 GL_MONTH            number(22,1) not null,
 FCST_AMOUNT         number(22,2) not null,
 REG_SOURCE_ID       number(22,0) not null,
 SPLIT_TA_AMOUNT     number(22,0),
 USER_ID             varchar2(18),
 TIME_STAMP          date
);

alter table REG_FCST_TA_LOAD_STG
   add constraint PK_REG_FCST_TA_LOAD_STG
       primary key (REG_COMPANY_ID, REG_ACCT_ID, FORECAST_VERSION_ID, GL_MONTH, REG_SOURCE_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_FCST_TA_LOAD_STG
   add constraint R_REG_FCST_TA_LOAD_STG1
       foreign key (REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

alter table REG_FCST_TA_LOAD_STG
   add constraint R_REG_FCST_TA_LOAD_STG2
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

alter table REG_FCST_TA_LOAD_STG
   add constraint R_REG_FCST_TA_LOAD_STG3
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_FCST_TA_LOAD_STG
   add constraint R_REG_FCST_TA_LOAD_STG4
       foreign key (REG_SOURCE_ID)
       references REG_SOURCE (REG_SOURCE_ID);

comment on table REG_FCST_TA_LOAD_STG is 'Staging table used to load data from Provision to the Reg Forecast Ledger';

comment on column REG_FCST_TA_LOAD_STG.REG_COMPANY_ID is 'System assigned identifier of a Regulatory Company';
comment on column REG_FCST_TA_LOAD_STG.REG_ACCT_ID is 'System assigned identifier of a Regulatory Account';
comment on column REG_FCST_TA_LOAD_STG.FORECAST_VERSION_ID is 'System assigned identifier of a Forecast Ledger (version).';
comment on column REG_FCST_TA_LOAD_STG.GL_MONTH is 'General Ledger posting account month in the YYYYMM format.';
comment on column REG_FCST_TA_LOAD_STG.FCST_AMOUNT is 'The actual month end account balance or monthly amount';
comment on column REG_FCST_TA_LOAD_STG.REG_SOURCE_ID is 'System assigned identifier of a regulatory source.';
comment on column REG_FCST_TA_LOAD_STG.SPLIT_TA_AMOUNT is '1 indicates that the amount will be allocated to PowerTax amounts; 0 indicates the amount should be loaded to the forecast ledger.';
comment on column REG_FCST_TA_LOAD_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_FCST_TA_LOAD_STG.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1292, 0, 10, 4, 2, 7, 39103, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039103_reg_fcst_provision_logic.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;