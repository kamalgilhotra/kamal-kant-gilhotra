/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030104_lease.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/11/2013 Brandon Beck   Point Release
||============================================================================
*/

alter table LS_ILR ADD CAPITAL_YN_SW               number(22,0);
alter table LS_ILR ADD LEASE_GROUP_ID              number(22,0) not null;
alter table LS_ILR ADD NPV_METHOD_ID               number(22,0);
alter table LS_ILR ADD OPERATING_EXPENSE_METHOD_ID number(22,0);
alter table LS_ILR ADD PYMNT_MONTH_INDICATOR       number(22,0);
alter table LS_ILR ADD TAX_CAPITAL_YN_SW           number(22,0);

alter table LS_ILR_GROUP_AIR ADD EFFDT DATE;

alter table LS_ILR_PAYMENT_TERM ADD CURRENCY_TYPE_ID number(22,0);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_1       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_2       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_3       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_4       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_5       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_6       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_7       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_8       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_9       number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_10      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_11      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_12      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_13      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_14      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_15      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_16      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_17      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_18      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_19      number(22,2);
alter table LS_ILR_PAYMENT_TERM ADD C_BUCKET_20      number(22,2);

alter table LS_ILR drop column MODIFY_SCHEDULE_SW;

alter table LS_ILR_PAYMENT_TERM drop column SNAPSHOT_PAID_AMOUNT;

create table ls_ilr_schedule_bak
as select *
from ls_ilr_schedule;

create table ls_asset_activity_bak
as select *
from ls_asset_activity;


drop table LS_TEMP_ASSET;
drop table LS_TEMP_CALC_ALLMONTHS;
drop table LS_TEMP_COMPONENT;
drop table LS_TEMP_ILR;
drop table LS_TEMP_LEASE;
drop table LS_VALID_COMPANY_PO;
drop table LS_VALID_FUNDING_PROJECTS;
drop table LS_ILR_SCHEDULE;
drop table LS_API_ASSET;
drop table LS_API_ASSET_TMP;
drop table LS_API_BATCH;
drop table LS_API_BATCH_STATUS;
drop table LS_API_COMPONENT;
drop table LS_API_DIST_DEF;
drop table LS_API_DIST_DEF_TMP;
drop table LS_API_ILR;
drop table LS_API_ILR_TERMS;
drop table LS_API_ILR_TMP;
drop table LS_API_INVOICE;
drop table LS_API_PICK_PROPGRP;
drop table LS_API_UNLOAD_ASSET;
drop table LS_API_UNLOAD_ILR;
drop table LS_ASSET_ACTIVITY;
drop table LS_ASSET_ACTIVITY_LT;
drop table LS_ASSET_CAPITAL_VS_OPERATING;
drop table LS_ASSET_CPR_FIELDS;
drop table LS_ASSET_DELETE_LT_ACTIVITY;
drop table LS_ASSET_FLEX_CONTROL;
drop table LS_ASSET_FLEX_FIELDS;
drop table LS_ASSET_FLEX_VALUES;
drop table LS_ASSET_RETIREMENT_HISTORY;
drop table LS_ASSET_TRANSFER_HISTORY;
drop table LS_CALC_APPROVAL_RULE;
drop table LS_COST_ELEMENT_VALID;
drop table LS_DIST_DEF_CONTROL;
drop table LS_DIST_DEF_PARMS;
drop table LS_DIST_DEF_VALIDATE;
drop table LS_DIST_LINE;
drop table LS_EOL_DEFAULTS;
drop table LS_FCST_ALLOCATION;
drop table LS_FCST_ALLOCATION_CREDITS;
drop table LS_FCST_ASSETS;
drop table LS_FCST_ASSET_ACTIVITY;
drop table LS_FCST_CONTROL;
drop table LS_FCST_DIST_DEF;
drop table LS_FCST_EXCHANGE;
drop table LS_FCST_ROUNDING;
drop table LS_FCST_ROUNDING2;
drop table LS_FCST_STAGING;
drop table LS_GL_TRANSACTION;
drop table LS_ILR_EARLY_RET;
drop table LS_JE_ALLOCATION;
drop table LS_JE_ALLOCATION_CREDITS;
drop table LS_JE_ASSET_ACTIVITY;
drop table LS_JE_DIST_DEF;
drop table LS_JE_EXCHANGE;
drop table LS_JE_EXCHANGE_STLT_RECAT;
drop table LS_JE_ROUNDING;
drop table LS_JE_ROUNDING2;
drop table LS_JE_STAGING;
drop table LS_LEASE_CALC_BALANCES;
drop table LS_LEASE_CALC_ROLE;
drop table LS_LEASE_CALC_ROLE_USER;
drop table LS_LEASE_CALC_STAGING;
drop table LS_LEASE_CALC_USER_APPROVER;
drop table LS_LEASE_EXPENSE;
drop table LS_SET_OF_BOOKS;
drop table LS_SOB_TYPE;
drop table LS_ACTIVITY_TYPE;

alter table LS_ASSET drop column ACCOUNTING_DEFAULT_ID;

drop table LS_ACCOUNTING_DEFAULTS;
drop table LS_DIST_DEF;
drop table LS_DIST_DEF_TYPE;
drop table LS_DIST_DEPARTMENT;
drop table LS_DIST_GL_ACCOUNT;
drop table LS_JE_METHOD;

alter table LS_LEASE drop column LEASE_APPROVAL_ID;

drop table LS_LEASE_APPROVAL;
drop table LS_LOCAL_TAX_SNAPSHOT;
drop table LS_LEASE_CALC_CONTROL;
drop table LS_LEASE_CALC_TYPE;
drop table LS_LEASE_CALC_STATUS;


create table LS_NPV_METHOD
(
 NPV_METHOD_ID    number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_NPV_METHOD
   add constraint LS_NPV_METHOD_PK
       primary key (NPV_METHOD_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_VENDOR_INVOICE_STATUS
(
 INVOICE_STATUS_ID number(22,0) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(35),
 LONG_DESCRIPTION  varchar2(254)
);

alter table LS_VENDOR_INVOICE_STATUS
   add constraint PK_LS_VENDOR_INVOICE_STATUS
       primary key (INVOICE_STATUS_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_LEASE_VENDOR
(
 LEASE_ID    number(22,0) not null,
 COMPANY_ID  number(22,0) not null,
 VENDOR_ID   number(22,0) not null,
 TIME_STAMP  date,
 USER_ID     varchar2(18),
 PAYMENT_PCT number(22,12) not null,
 SHC_1       varchar2(255)
);

alter table LS_LEASE_VENDOR
   add constraint PK_LS_LEASE_VENDOR
       primary key (LEASE_ID, COMPANY_ID, VENDOR_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_LEASE_VENDOR
   add constraint FK_LS_LEASE_VENDOR_1
       foreign key (LEASE_ID, COMPANY_ID)
       references LS_LEASE_COMPANY (LEASE_ID, COMPANY_ID);


create table LS_OPERATING_EXPENSE_METHOD
(
 OPERATING_EXPENSE_METHOD_ID number(22,0) not null,
 TIME_STAMP                  date,
 USER_ID                     varchar2(18),
 DESCRIPTION                 varchar2(35) not null,
 LONG_DESCRIPTION            varchar2(255)
);

alter table LS_OPERATING_EXPENSE_METHOD
   add constraint LS_OPERATING_EXPENSE_METHOD_PK
       primary key (OPERATING_EXPENSE_METHOD_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_SUBLET_SW
(
 SUBLET_SW        number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_SUBLET_SW
   add constraint LS_SUBLET_SW_PK
       primary key (SUBLET_SW)
       using index tablespace PWRPLANT_IDX;


create table LS_MUNI_BO_SW
(
 MUNI_BO_SW       number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_MUNI_BO_SW
   add constraint LS_MUNI_BO_SW_PK
       primary key (MUNI_BO_SW)
       using index tablespace PWRPLANT_IDX;


create table LS_PARTIAL_RETIRE_SW
(
 PARTIAL_RETIRE_SW number(22,0) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(35) not null,
 LONG_DESCRIPTION  varchar2(255)
);

alter table LS_PARTIAL_RETIRE_SW
   add constraint LS_PARTIAL_RETIRE_SW_PK
       primary key (PARTIAL_RETIRE_SW)
       using index tablespace PWRPLANT_IDX;


create table LS_PAD_SIDE
(
 PAD_SIDE_ID      number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254)
);

alter table LS_PAD_SIDE
   add constraint PK_LS_PAD_SIDE
       primary key (PAD_SIDE_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_LEASE_VENDOR_REL
(
 LEASE_VENDOR_REL number(22,0) not null,
 TIME_STAMP date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_LEASE_VENDOR_REL
   add constraint LS_LEASE_VENDOR_REL_PK
       primary key (LEASE_VENDOR_REL)
       using index tablespace PWRPLANT_IDX;


create table LS_PAYMENT_MONTH_INDICATOR
(
 PAYMENT_MONTH_INDICATOR number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DESCRIPTION             varchar2(35) not null,
 LONG_DESCRIPTION        varchar2(254)
);

alter table LS_PAYMENT_MONTH_INDICATOR
   add constraint PK_LS_PAYMENT_MONTH_INDICATOR
       primary key (PAYMENT_MONTH_INDICATOR)
       using index tablespace PWRPLANT_IDX;


create table LS_OPERATING_JE_FLG
(
 OPERATING_JE_FLG number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_OPERATING_JE_FLG
   add constraint LS_OPERATING_JE_FLG_PK
       primary key (OPERATING_JE_FLG)
       using index tablespace PWRPLANT_IDX;


create table LS_VENDOR_INVOICE_LINE_MAP
(
 INVOICE_ID        number(22,0) not null,
 INVOICE_LINE_ID   number(22,0) not null,
 LS_ASSET_ID       number(22,0) not null,
 ORIG_MONTH_NUMBER number(22,0) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18)
);

alter table LS_VENDOR_INVOICE_LINE_MAP
   add constraint PK_LS_VENDOR_INVOICE_LINE_MAP
       primary key (INVOICE_ID, INVOICE_LINE_ID, LS_ASSET_ID, ORIG_MONTH_NUMBER)
       using index tablespace PWRPLANT_IDX;


create table LS_LEASE_ILR_REL
(
 LEASE_ILR_REL    number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_LEASE_ILR_REL
   add constraint LS_LEASE_ILR_REL_PK
       primary key (LEASE_ILR_REL)
       using index tablespace PWRPLANT_IDX;


create table LS_ILR_ASSET_REL
(
 ILR_ASSET_REL    number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_ILR_ASSET_REL
   add constraint LS_ILR_ASSET_REL_PK
       primary key (ILR_ASSET_REL)
       using index tablespace PWRPLANT_IDX;


create table LS_ASSET_COMP_REL
(
 ASSET_COMP_REL   number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_ASSET_COMP_REL
   add constraint LS_ASSET_COMP_REL_PK
       primary key (ASSET_COMP_REL)
       using index tablespace PWRPLANT_IDX;


create table LS_COMP_INV_REL
(
 COMP_INV_REL     number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_COMP_INV_REL
   add constraint LS_COMP_INV_REL_PK
       primary key (COMP_INV_REL)
       using index tablespace PWRPLANT_IDX;


create table LS_CONT_RENT_FLG
(
 CONT_RENT_FLG    number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_CONT_RENT_FLG
   add constraint LS_CONT_RENT_FLG_PK
       primary key (CONT_RENT_FLG)
       using index tablespace PWRPLANT_IDX;


create table LS_INVOICE_LINE_LEVEL
(
 INVOICE_LINE_LEVEL_ID number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 DESCRIPTION           varchar2(35),
 LONG_DESCRIPTION      varchar2(254)
);

alter table LS_INVOICE_LINE_LEVEL
   add constraint PK_LS_INVOICE_LINE_LEVEL
       primary key (INVOICE_LINE_LEVEL_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_LESSOR_TAX_YN_SW
(
 LESSOR_TAX_YN_SW number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(354)
);

alter table LS_LESSOR_TAX_YN_SW
   add constraint PK_LS_LESSOR_TAX_YN_SW
       primary key (LESSOR_TAX_YN_SW)
       using index tablespace PWRPLANT_IDX;


create table LS_CONT_RENT_ILR_MAP
(
 ILR_ID     number(22,0) not null,
 EFFDT      date not null,
 TIME_STAMP date,
 USER_ID    varchar2(18),
 FORMULA_ID number(22,0) not null
);

alter table LS_CONT_RENT_ILR_MAP
   add constraint PK_LS_CONT_RENT_ILR_MAP
       primary key (ILR_ID, EFFDT)
       using index tablespace PWRPLANT_IDX;

alter table LS_CONT_RENT_ILR_MAP
   add constraint FK_LS_CONT_RENT_ILR_MAP_1
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);


create table LS_VENDOR
(
 VENDOR_ID          number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 LESSOR_ID          number(22,0) not null,
 DESCRIPTION        varchar2(35) not null,
 LONG_DESCRIPTION   varchar2(255),
 ACTIVE_START       date,
 ACTIVE_END         date,
 EXTERNAL_VENDOR_ID varchar2(35),
 REMIT_ADDR         varchar2(35)
);

alter table LS_VENDOR
   add constraint LS_VENDOR_PK
       primary key (VENDOR_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_VENDOR
   add constraint FK_LS_VENDOR_1
       foreign key (LESSOR_ID)
       references LS_LESSOR (LESSOR_ID);


create table LS_ILR_GROUP
(
 ILR_GROUP_ID     number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254),
 LEASE_ID         number(22,0) not null
);

alter table LS_ILR_GROUP
   add constraint PK_LS_ILR_GROUP
       primary key (ILR_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_ILR_GROUP
   add constraint FK_LS_ILR_GROUP_1
       foreign key (LEASE_ID)
       references LS_LEASE (LEASE_ID);


create table LS_IBR
(
 COMPANY_ID number(22,0) not null,
 EFFDT      date not null,
 TIME_STAMP date,
 USER_ID    varchar2(18),
 IBR        number(22,12)
);

alter table LS_IBR
   add constraint PK_LS_IBR
       primary key (COMPANY_ID, EFFDT)
       using index tablespace PWRPLANT_IDX;

alter table LS_IBR
   add constraint FK_LS_IBR_1
       foreign key (COMPANY_ID)
       references COMPANY_SETUP (COMPANY_ID);


create table LS_ILR_PAYMENTS
(
 ILR_ID         number(22,0),
 MONTHNUM       number(22,0),
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 PAYMENT_AMOUNT number(22,3) not null
);

alter table LS_ILR_PAYMENTS
   add constraint FK_LS_ILR_PAYMENTS_1
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);


create table LS_CONT_RENT_ILR_AMOUNT
(
 ILR_ID                   number(22,0) not null,
 GL_MONTH_NUMBER          number(22,0) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 CONTINGENT_RENT_ACCRUAL  number(22,3),
 CONTINGENT_RENTAL_AMOUNT number(22,3)
);

alter table LS_CONT_RENT_ILR_AMOUNT
   add constraint PK_LS_CONT_RENT_ILR_AMOUNT
       primary key (ILR_ID, GL_MONTH_NUMBER)
       using index tablespace PWRPLANT_IDX;

alter table LS_CONT_RENT_ILR_AMOUNT
   add constraint FK_LS_CONT_RENT_ILR_AMOUNT_1
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);


create table LS_VOUCHER_APPROVAL_RECORDS
(
 VOUCHER_ID         number(22,0) not null,
 APPROVAL_LEVEL_ID  number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 APPROVE_USER       varchar2(18),
 APPROVE_TIME_STAMP date
);

alter table LS_VOUCHER_APPROVAL_RECORDS
   add constraint PK_LS_VOUCHER_APPROVAL_RECORDS
       primary key (VOUCHER_ID, APPROVAL_LEVEL_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_CONT_RENT_VAR_TYPE
(
 VARIABLE_TYPE_ID number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(255)
);

alter table LS_CONT_RENT_VAR_TYPE
   add constraint PK_LS_CONT_RENT_VAR_TYPE
       primary key (VARIABLE_TYPE_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_CONT_RENT_VAR_USAGE
(
 VARIABLE_ID     number(22,0) not null,
 ILR_ID          number(22,0) not null,
 USAGE_MONTH     number(22,0) not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 USAGE           number(36,12) not null,
 GL_MONTH_NUMBER number(22,0) not null
);

alter table LS_CONT_RENT_VAR_USAGE
   add constraint PK_LS_CONT_RENT_VAR_USAGE
       primary key (VARIABLE_ID, ILR_ID, USAGE_MONTH)
       using index tablespace PWRPLANT_IDX;

alter table LS_CONT_RENT_VAR_USAGE
   add constraint FK_LS_CONT_RENT_VAR_USAGE_1
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);


create table LS_EXECUTORY_BUCKETS
(
 BUCKET_ID        number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35),
 LONG_DESCRIPTION varchar2(254)
);

alter table LS_EXECUTORY_BUCKETS
   add constraint PK_LS_EXECUTORY_BUCKETS
       primary key (BUCKET_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_CONT_RENT_VAR_EXT
(
 VARIABLE_ID      number(22,0) not null,
 VARIABLE_TYPE_ID number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(100)
);

alter table LS_CONT_RENT_VAR_EXT
   add constraint PK_LS_CONT_RENT_VAR_EXT
       primary key (VARIABLE_ID, VARIABLE_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_CONT_RENT_VAR_EXT
   add constraint FK_LS_CONT_RENT_VAR_EXT_1
       foreign key (VARIABLE_TYPE_ID)
       references LS_CONT_RENT_VAR_TYPE (VARIABLE_TYPE_ID);


create table LS_VENDOR_INVOICE
(
 INVOICE_ID          number(22,0) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 VENDOR_ID           number(22,0) not null,
 GL_POSTING_MONTH    number(22,0) not null,
 INVOICE_STATUS      number(22,0) not null,
 INVOICE_NUMBER      varchar2(35),
 INVOICE_DATE        date,
 INVOICE_DESCRIPTION varchar2(35),
 INVOICE_NOTES       varchar2(255),
 INVOICE_AMOUNT      number(22,3),
 INVOICE_PRINCIPAL   number(22,3),
 INVOICE_INTEREST    number(22,3),
 INVOICE_USE_TAX     number(22,3),
 INVOICE_EXECUTORY   number(22,3),
 INVOICE_CONTINGENT  number(22,3),
 FLEX_1              varchar2(35),
 FLEX_2              varchar2(35),
 FLEX_3              varchar2(35),
 FLEX_4              varchar2(35),
 FLEX_5              varchar2(35),
 FLEX_6              varchar2(35),
 FLEX_7              varchar2(35),
 FLEX_8              varchar2(35),
 FLEX_9              varchar2(254),
 FLEX_10             varchar2(2000)
);

alter table LS_VENDOR_INVOICE
   add constraint PK_LS_VENDOR_INVOICE
       primary key (INVOICE_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_VENDOR_INVOICE_MAP
(
 INVOICE_ID       number(22,0) not null,
 LEASE_PAYMENT_ID number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18)
);

alter table LS_VENDOR_INVOICE_MAP
   add constraint PK_LS_VENDOR_INVOICE_MAP
       primary key (INVOICE_ID, LEASE_PAYMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_VENDOR_INVOICE_MAP
   add constraint FK_LS_VENDOR_INVOICE_MAP_1
       foreign key (INVOICE_ID)
       references LS_VENDOR_INVOICE (INVOICE_ID);


create table LS_VENDOR_INVOICE_LINE
(
 INVOICE_ID               number(22,0) not null,
 INVOICE_LINE_ID          number(22,0) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 INVOICE_LINE_NUMBER      varchar2(35),
 INVOICE_LINE_DESCRIPTION varchar2(35),
 ILR_ASSET_ID             number(22,0) not null,
 INVOICE_LINE_AMOUNT      number(22,3),
 INVOICE_LINE_PRINCIPAL   number(22,3),
 INVOICE_LINE_INTEREST    number(22,3),
 INVOICE_LINE_USE_TAX     number(22,3),
 INVOICE_LINE_EXECUTORY   number(22,3),
 INVOICE_LINE_CONTINGENT  number(22,3)
);

alter table LS_VENDOR_INVOICE_LINE
   add constraint PK_LS_VENDOR_INVOICE_LINE
       primary key (INVOICE_ID, INVOICE_LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_VENDOR_INVOICE_LINE
   add constraint FK_LS_VENDOR_INVOICE_LINE_1
       foreign key (INVOICE_ID)
       references LS_VENDOR_INVOICE (INVOICE_ID);


create table LS_VENDOR_INVOICE_LINE_LEVEL
(
 INVOICE_ID            number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 INVOICE_LINE_LEVEL_ID number(22,0) not null
);

alter table LS_VENDOR_INVOICE_LINE_LEVEL
   add constraint PK_LS_VENDOR_INVOICE_LINE_LVL
       primary key (INVOICE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_VENDOR_INVOICE_LINE_LEVEL
   add constraint FK_LS_VENDOR_INVOICE_LINE_LVL2
       foreign key (INVOICE_LINE_LEVEL_ID)
       references LS_INVOICE_LINE_LEVEL (INVOICE_LINE_LEVEL_ID);

alter table LS_VENDOR_INVOICE_LINE_LEVEL
   add constraint FK_LS_VENDOR_INVOICE_LINE_LVL1
       foreign key (INVOICE_ID)
       references LS_VENDOR_INVOICE (INVOICE_ID);


create table LS_LEASE_GROUP_USER
(
 LEASE_GROUP_ID       number(22,0) not null,
 LEASE_USER_ID        varchar2(18) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 LEASE_APPROVER_FLG   number(22,0) not null,
 PAYMENT_APPROVER_FLG number(22,0) not null,
 JE_APPROVER_FLG      number(22,0) not null
);

alter table LS_LEASE_GROUP_USER
   add constraint PK_LS_LEASE_GROUP_USER
       primary key (LEASE_GROUP_ID, LEASE_USER_ID)
       using index tablespace PWRPLANT_IDX;


create table LS_LEASE_DOCUMENT
(
 LEASE_ID      number(22,0) not null,
 DOCUMENT_DATA blob,
 DOCUMENT_ID   number(22,0) not null,
 NOTES         varchar2(2000),
 DESCRIPTION   varchar2(35),
 FILE_NAME     varchar2(100) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 FILESIZE      number(22,0),
 FILE_LINK     varchar2(2000)
);

alter table LS_LEASE_DOCUMENT
   add constraint PK_LS_LEASE_DOCUMENT
       primary key (LEASE_ID, DOCUMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_LEASE_DOCUMENT
   add constraint R_LS_LEASE_DOCUMENT1
       foreign key (LEASE_ID)
       references LS_LEASE;


create table LS_ILR_DOCUMENT
(
 ILR_ID        number(22,0) not null,
 DOCUMENT_DATA blob,
 DOCUMENT_ID   number(22,0) not null,
 NOTES         varchar2(2000),
 DESCRIPTION   varchar2(35),
 FILE_NAME     varchar2(100) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 FILESIZE      number(22,0),
 FILE_LINK     varchar2(2000)
);

alter table LS_ILR_DOCUMENT
   add constraint PK_LS_ILR_DOCUMENT
       primary key (ILR_ID, DOCUMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_ILR_DOCUMENT
   add constraint R_LS_ILR_DOCUMENT1
       foreign key (ILR_ID)
       references LS_LEASE;


create table LS_CONTINGENT_BUCKETS
(
 BUCKET_ID        number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35),
 LONG_DESCRIPTION varchar2(254),
 CAPITAL_YES_NO   number(22,0)
);

alter table LS_CONTINGENT_BUCKETS
   add constraint PK_LS_CONTINGENT_BUCKETS
       primary key (BUCKET_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_CONTINGENT_BUCKETS
   add constraint FK_LS_CONTINGENT_BUCKETS_CAP
       foreign key (CAPITAL_YES_NO)
       references YES_NO (YES_NO_ID);


create table LS_CONT_RENT_VARIABLE
(
 VARIABLE_ID      number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254),
 STATUS_CODE_ID   number(22,0) not null
);

alter table LS_CONT_RENT_VARIABLE
   add constraint PK_LS_CONT_RENT_VARIABLE
       primary key (VARIABLE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_CONT_RENT_VARIABLE
   add constraint FK_LS_CONT_RENT_VARIABLE_1
       foreign key (STATUS_CODE_ID)
       references STATUS_CODE (STATUS_CODE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (393, 0, 10, 4, 1, 0, 30104, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030104_lease.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
