/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049557_lessee_02_ilr_import_renewal_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/10/2017 Shane "C" Ward		Set up Lessee ILR Renewal Import Tables
||============================================================================
*/

INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name,
             archive_additional_columns)
VALUES      (265,
             'Add: ILR Renewal Options',
             'Lessee ILR Renewal Options',
             'ls_import_ilr_renewal',
             'ls_import_ilr_renewal_archive',
             1,
             'nvo_ls_logic_import',
             'ilr_renewal_id');

INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_type_id,
             import_subsystem_id)
VALUES      (265,
             8);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 265,
             'ilr_id',
             'ILR ID',
             'ilr_id_xlate',
             1,
             1,
             'number(22,0)',
             'ls_ilr',
             1,
             'ilr_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 265,
             'revision',
             'Revision',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 265,
             'ilr_renewal_prob_id',
             'Probability of Exercising Renewal',
             'ilr_renewal_prob_id_xlate',
             0,
             1,
             'number(22,0)',
             'ls_ilr_renewal_probability',
             1,
             'ilr_renewal_probability_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 265,
             'renewal_notice',
             'Renewal Notice (in Months)',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 265,
             'renewal_frequency_id',
             'Renewal Frequency',
             'renewal_frequency_id_xlate',
             0,
             1,
             'number(22,0)',
             'ls_payment_freq',
             1,
             'payment_freq_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 265,
             'renewal_number_of_terms',
             'Renewal Number of Terms',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 265,
             'renewal_amount_per_term',
             'Renewal Amount Per Term',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             is_required,
             processing_order,
             column_type,
             is_on_table)
VALUES      (265,
             'renewal_start_date',
             'Renewal Start Date',
             0,
             1,
             'varchar(35)',
             1);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             is_required,
             processing_order,
             column_type,
             is_on_table)
VALUES      (265,
             'ilr_renewal_option_id',
             'Renewal Option',
             1,
             1,
             'Number(22,0)',
             1);

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1096,
             'Ls Ilr.Ilr Number',
             'Lessee ILR ID',
             'ilr_id',
             '(( select b.ilr_id from ls_ilr b where upper( trim( <importfield> ) ) = upper( trim( b.ilr_number ) )))',
             0,
             'ls_ilr',
             'ilr_number');

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             import_lookup_id,
             column_name)
VALUES      (265,
             1096,
             'ilr_id');

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 265,
             'renewal_frequency_id',
             1047);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 265,
             'renewal_frequency_id',
             1064);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 265,
             'ilr_renewal_prob_id',
             1086);

--Default Template
INSERT INTO PP_IMPORT_TEMPLATE
            (import_template_id,
             import_type_id,
             description,
             long_description,
             do_update_with_add,
             is_autocreate_template)
VALUES      ( ( SELECT Max(import_template_id) + 1
               FROM   PP_IMPORT_TEMPLATE ),
             265,
             'ILR Renewal Options Add',
             'Default Lessee ILR Renewal Options Template',
             1,
             0);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             1,
             265,
             'ilr_id',
             1096);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             2,
             265,
             'revision',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             3,
             265,
             'ilr_renewal_option_id',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             4,
             265,
             'ilr_renewal_prob_id',
             1086);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             5,
             265,
             'renewal_notice',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             6,
             265,
             'renewal_frequency_id',
             1047);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             7,
             265,
             'renewal_number_of_terms',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             8,
             265,
             'renewal_amount_per_term',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 265 AND
                      description = 'ILR Renewal Options Add' ),
             9,
             265,
             'renewal_start_date',
             NULL); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3924, 0, 2017, 1, 0, 0, 49557, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049557_lessee_02_ilr_import_renewal_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;