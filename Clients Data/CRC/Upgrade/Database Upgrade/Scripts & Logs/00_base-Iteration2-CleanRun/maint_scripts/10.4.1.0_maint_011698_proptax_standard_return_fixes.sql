SET DEFINE OFF
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011698_proptax_standard_return_fixes.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   01/15/2013 Andrew Scott   Point Release
|| 10.4.1.2   12/11/2013 Julia Breuer   Script was removed
||============================================================================
*/

/*
----
---- Maint 11698
----
----  Description : Standardized returns fixes, enhancements, and additions were made during the
----      TWC install. Remove TWC specific portions and submit latest standardized reports
----      & data to the base.
----

----
----new returns: AL  ADV:U1/ADV:U5-10
----
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (512104, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'),
    'AL ADV U5-U10 Standardized', 'AL ADV U5-U10 Standardized', null, 'dw_ptr_rtn_al_adv_u5_10',
    null, null, null, 'AL ADV U5-U10', null, null, null, 10, 200, 12, 7, 4, 3, null, null, null,
    null, null, 0);
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (512105, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'), 'AL-ADV U1',
    'Alabama ADV U1 Statement of Property', null, 'dw_ptr_rtn_al_adv_u1', null, null, null,
    'AL-ADV U1', null, null, null, 10, 200, 12, 7, 4, 3, null, null, null, null, null, 0);

insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512104, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Officer');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512104, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Telephone Number');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512104, 'pt_report_pt_company_field', 'field3',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Fax Number');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512104, 'pt_report_pt_company_field', 'field4',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Place of Business');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512104, 'pt_report_pt_company_field', 'field5',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Revision Date');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512104, 'pt_report_pt_company_field', 'field6',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Month/Year Ending');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512105, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'address_1');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512105, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'county');

insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (12002, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'AL-ADV:U5-U10',
    'AL-ADV:U5-U10', 2);
insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (12003, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'AL-ADV-U1',
    'AL-ADV-U1', 2);

insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (12002, 1, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'AL-ADV:U5-U10 ',
    'AL-ADV:U5-U10 ', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (12003, 1, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'AL-ADV-U1',
    'AL-ADV-U1', null);

insert into PWRPLANT.PROPERTY_TAX_TYPE_DATA
   (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS,
    METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY,
    ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN,
    QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID,
    EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID,
    SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID)
values
   (12024, sysdate, user, 'AL-ADV:U5-U10', 'AL', TO_DATE('01/01/2000', 'mm/dd/yyyy'),
    'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null,
    null, 1, '', null, null, null, 0, 1, null, null);

insert into PWRPLANT.PROPERTY_TAX_TYPE_DATA
   (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS,
    METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY,
    ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN,
    QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID,
    EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID,
    SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID)
values
   (12025, sysdate, user, 'AL-ADV-U1', 'AL', TO_DATE('01/01/2000', 'mm/dd/yyyy'),
    'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null,
    null, 1, '', null, null, null, 0, 1, null, null);

insert into PWRPLANT.PT_TYPE_ROLLUP_ASSIGN
   (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID)
values
   (12024, 12002, 1, sysdate, user);

insert into PWRPLANT.PT_TYPE_ROLLUP_ASSIGN
   (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID)
values
   (12025, 12003, 1, sysdate, user);

----
----CO 15 AS Form delivered was the lease form--which would still be good to have.
----change the 512501 id to be the lease form, then make the 512502 as the original, intended form.
----clear out the report rows, report field label rows, then re-insert
----
delete from PP_REPORTS where REPORT_ID between 512501 and 512502;
delete from PT_REPORT_FIELD_LABELS where REPORT_ID between 512501 and 512502;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (512501, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'),
    'CO-15 DPT-AS Form-Lease', 'Colorado 15 DPT-AS Form DS 056 61 Lease', null,
    'dw_ptr_rtn_co_15_dpt_as_lessor', null, null, null, 'CO-15 DPT-AS Form-Lease', null, null, null,
    10, 200, 12, 7, 4, 3, null, null, null, null, null, 0);

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (512502, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'),
    'CO-15 DPT-AS Form DS 056 61',
    'Colorado 15 DPT-AS Form DS 056 61 Personal Property Declaration Schedule', null,
    'dw_ptr_rtn_co_15_dpt_as', null, null, null, 'CO-15 DPT-AS Form DS 056 61', null, null, null, 10,
    200, 12, 7, 4, 3, null, null, null, null, null, 0);

insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field1', 'Business Status' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field2', 'Out of Business (Y/N)?' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field3', 'Signer Name' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field4', 'Signer Phone' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field5', 'Signer Email' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field6', 'Signer Fax' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field7', 'Question H1 (Y/N)?' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field8', 'Question H2 (Y/N)?' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field9', 'Question H3 (Y/N)?' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field10', 'Renewable Energy Production (Y/N)?' );

insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512501, 'pt_report_pt_company_field', 'field11', 'Address from Parcel or Location?' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512502, 'pt_report_pt_company_field', 'field1', 'Business Status' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512502, 'pt_report_pt_company_field', 'field2', 'Out of Business (Y/N)?' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512502, 'pt_report_pt_company_field', 'field3', 'Signer Name' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512502, 'pt_report_pt_company_field', 'field4', 'Signer Phone' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512502, 'pt_report_pt_company_field', 'field5', 'Signer Email' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512502, 'pt_report_pt_company_field', 'field6', 'Signer Fax' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values (512502, 'pt_report_pt_company_field', 'field7', 'Address from Parcel or Location?' );

update PT_TYPE_ROLLUP
   set DESCRIPTION = 'CO-15 DPT-AS Form-Lease', LONG_DESCRIPTION = 'CO-15 DPT-AS Form-Lease'
 where TYPE_ROLLUP_ID = 16001;

insert into PWRPLANT.PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (16002, sysdate, user, 'CO-15 DPT-AS Form DS 056 61', 'CO-15 DPT-AS Form DS 056 61', 2);

delete from PT_TYPE_ROLLUP_ASSIGN
 where TYPE_ROLLUP_ID = 16001
   and TYPE_ROLLUP_VALUE_ID between 1 and 2;

delete from PT_TYPE_ROLLUP_VALUES
 where TYPE_ROLLUP_ID = 16001
   and TYPE_ROLLUP_VALUE_ID between 1 and 2;

insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (16002, 1, sysdate, user, 'Itemized Listing', 'Itemized Listing', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (16002, 2, sysdate, user, 'Mobile Equipment', 'Mobile Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (16002, 3, sysdate, user, 'General Ledger', 'General Ledger', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (16002, 4, sysdate, user, 'Fully Depreciated', 'Fully Depreciated', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (16002, 5, sysdate, user, 'Leased, Loaned, Rented', 'Leased, Loaned, Rented', '' );

insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID ) values ( 16005, sysdate, user, 'CO-Itemized Listing', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID ) values ( 16006, sysdate, user, 'CO-Mobile Equipment', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID ) values ( 16007, sysdate, user, 'CO-General Ledger', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID ) values ( 16008, sysdate, user, 'CO-Fully Depreciated', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID ) values ( 16009, sysdate, user, 'CO-Leased, Loaned, Rented', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );

insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16005, 16002, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16006, 16002, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16007, 16002, 3, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16008, 16002, 4, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16009, 16002, 5, sysdate, user);

----
----new return: CO-ASOP Telephone Company
----
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (512500, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'),
    'CO-ASOP Telephone Company', 'CO-ASOP Telephone Company', null, 'dw_ptr_rtn_co_asop', null, null,
    null, 'CO-ASOP Telephone Company', null, null, null, 10, 200, 12, 7, 4, 3, null, null, null,
    null, null, 0);

insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Department');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field10',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Agent Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field11',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Agent Title');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field12',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Agent Phone');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field13',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Agent Fax');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field14',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Agent Email');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field15',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field16',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Email');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'New Info: Yes or No?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field3',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Contact');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field4',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Contact Title');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field5',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Contact Phone');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field6',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Contact Fax');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field7',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Contact Email');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field8',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Property Ratio');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512500, 'pt_report_pt_company_field', 'field9',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Year Operations Began');

insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (16000, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'CO-ASOP Telephone',
    'CO-ASOP Telephone', 2);

insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (16000, 1, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Locally Assessed Property', 'Locally Assessed Property', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (16000, 2, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Licensed Vehicles', 'Licensed Vehicles', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (16000, 3, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Licensed Special Mobile Machinery', 'Licensed Special Mobile Machinery', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (16000, 4, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Inventories, Materials and Supplies', 'Inventories, Materials and Supplies', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (16000, 5, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'CWIP allowed in Rate Base', 'CWIP allowed in Rate Base', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (16000, 6, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'CWIP - personal property only', 'CWIP - personal property only', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (16000, 7, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Dark Fiber',
    'Dark Fiber', null);

insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (16010, sysdate, user, 'CO-ASOP-Tele-Locally Assessed Property', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (16011, sysdate, user, 'CO-ASOP-Tele-Licensed Vehicles', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (16012, sysdate, user, 'CO-ASOP-Tele-Licensed Special Mobile Machinery', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (16013, sysdate, user, 'CO-ASOP-Tele-Inventories, Materials and Supplies', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (16014, sysdate, user, 'CO-ASOP-Tele-CWIP allowed in Rate Base', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (16015, sysdate, user, 'CO-ASOP-Tele-CWIP - personal property only', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (16016, sysdate, user, 'CO-ASOP-Tele-Dark Fiber', 'CO', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, 2900, 1, '', null, null, null, 0, 1, null, null );

insert into PT_TYPE_ROLLUP_ASSIGN ( PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16010, 16000, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN ( PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16011, 16000, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN ( PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16012, 16000, 3, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN ( PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16013, 16000, 4, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN ( PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16014, 16000, 5, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN ( PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16015, 16000, 6, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN ( PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (16016, 16000, 7, sysdate, user);

----
----new return: DC FP 31
----
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (512702, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'), 'DC-FP 31 P1-2',
    'DC-FP 31 Pages 1-2', null, 'dw_ptr_rtn_dc_fp_31_p1-2', null, null, null, 'DC-FP 31 P1-2', null,
    null, null, 10, 200, 12, 7, 4, 3, null, null, null, null, null, 0);
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (512703, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'), 'DC-FP 31 P3',
    'DC-FP 31 Page 3', null, 'dw_ptr_rtn_dc_fp_31_p3', null, null, null, 'DC-FP 31 P3', null, null,
    null, 10, 200, 12, 7, 4, 3, null, null, null, null, null, 0);

insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'FEIN or SSN?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field10',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Question D');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field11',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Question E');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field12',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Outside US?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field13',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Officer Title');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field14',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Contact Phone');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field15',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Firm Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field16',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Firm Address');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field17',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Phone');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field18',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Payer Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field19',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Paid');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'FEIN Number');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field20',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Penalties');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field21',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Interest');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field22',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Amount Paid');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field3',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Amended?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field4',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Final?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field5',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Certified QHTC?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field6',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Under 225,000?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field7',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Question A');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field8',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Question B');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (512702, 'pt_report_pt_company_field', 'field9',
    TO_DATE('2012-10-15 12:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Question C');

insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (18001, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'DC-FP 31',
    'DC-FP 31', 2);

insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (18001, 1, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Books, DVDs, Reference', 'Books, DVDs, Reference', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (18001, 2, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Furniture and Fixtures', 'Furniture and Fixtures', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (18001, 3, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Machinery and Equipment', 'Machinery and Equipment', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (18001, 4, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Unreg Motor Vehicles', 'Unreg Motor Vehicles', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (18001, 5, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Other', 'Other',
    null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (18001, 6, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Office Supplies', 'Office Supplies', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (18001, 7, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Leased Personal Prop (D-1)', 'Leased Personal Prop (D-1)', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (18001, 8, TO_DATE('2012-09-18 11:05:16', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Leased Personal Prop (D-2)', 'Leased Personal Prop (D-2)', null);

insert into STATE (STATE_ID, COUNTRY_ID, LONG_DESCRIPTION, ACTIVE_YN)
select 'DC', 'USA', 'DC', 1 from dual
where not exists (
	select 1 from state
	where upper(trim(state_id)) = 'DC'
);

insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18001, sysdate, user, 'DC-Not Reportable', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18002, sysdate, user, 'DC-Real', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18003, sysdate, user, 'DC-Books, DVDs, Reference', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18004, sysdate, user, 'DC-Furniture and Fixtures', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18005, sysdate, user, 'DC-Machinery and Equipment', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18006, sysdate, user, 'DC-Unreg Motor Vehicles', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18007, sysdate, user, 'DC-Other', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18008, sysdate, user, 'DC-Office Supplies', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18009, sysdate, user, 'DC-Leased Personal Prop (D-1)', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (18010, sysdate, user, 'DC-Leased Personal Prop (D-2)', 'DC', to_date('07/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );

insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (18003, 18001, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (18004, 18001, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (18005, 18001, 3, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (18006, 18001, 4, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (18007, 18001, 5, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (18008, 18001, 6, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (18009, 18001, 7, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (18010, 18001, 8, sysdate, user);

----
----new return: IN 106 (uses the rollup and will usually package with the 103)
----
insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
values
   (513517, sysdate, user, 'IN-Form 106', 'IN-Form 106', 'dw_ptr_rtn_in_form_106', 'IN-Form 106',
    10, 200, 12, 7, 4, 3, 0);

insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL)
values
   (513517, 'pt_report_pt_company_field', 'field1', 'Signer Title');

----
----return enhancements: MD Form 1
----
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL)
values
   (514000, 'pt_report_pt_company_field', 'field35', 'Preparer Firm Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL)
values
   (514000, 'pt_report_pt_company_field', 'field36', 'Preparer Phone');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL)
values
   (514000, 'pt_report_pt_company_field', 'field37', 'Preparer Email');

----
----new returns: ME bpp & ME telecom pp
----
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (514100, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'),
    'ME-Personal Property Declaration', 'ME-Personal Property Declaration (36 MRSA Section 706)',
    null, 'dw_ptr_rtn_me_local_bpp', null, null, null, 'ME-Personal Prop. Declare', null, null, null,
    10, 200, 12, 7, 4, 3, null, null, null, null, null, 0);
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (514101, 'PWRPLANT', TO_DATE('2012-10-16 01:31:43', 'yyyy-mm-dd hh24:mi:ss'),
    'ME-Telecom PP Return', 'ME-Telecom PP Return', null, 'dw_ptr_rtn_me_telecom_ppr', null, null,
    null, 'ME-Telecom PP Return', null, null, null, 10, 200, 12, 7, 4, 3, null, null, null, null,
    null, 0);

insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514100, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514100, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Phone');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514100, 'pt_report_pt_company_field', 'field3',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Fax');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514100, 'pt_report_pt_company_field', 'field4',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Title');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514100, 'pt_report_pt_company_field', 'field5',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Email');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514100, 'pt_report_pt_company_field', 'field6',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Address from Parcel or Location?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514101, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514101, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Title');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514101, 'pt_report_pt_company_field', 'field3',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Owner Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514101, 'pt_report_pt_company_field', 'field4',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Telephone Number');

insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (32000, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop. Declaration', 'ME-Personal Prop. Declaration', 2);
insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (32001, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Telecom PP Return', 'ME-Telecom PP Return', 2);

insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32000, 1, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop Furn & Fix', 'ME-Personal Prop Furn & Fix', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32000, 2, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop Mech & Equip', 'ME-Personal Prop Mech & Equip', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32000, 3, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop Comp Equip', 'ME-Personal Prop Comp Equip', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32000, 4, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop Misc', 'ME-Personal Prop Misc', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32000, 5, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop Additions', 'ME-Personal Prop Additions', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32000, 6, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop Deletions', 'ME-Personal Prop Deletions', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32000, 7, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop Leased Equip', 'ME-Personal Prop Leased Equip', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32000, 8, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'ME-Personal Prop Owned Others', 'ME-Personal Prop Owned Others', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32001, 1, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Network Computer Equip & Software', 'Network Computer Equip & Software', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32001, 2, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Central Office Equipment & Antennas', 'Central Office Equipment & Antennas', null);
insert into "PT_TYPE_ROLLUP_VALUES"
   ("TYPE_ROLLUP_ID", "TYPE_ROLLUP_VALUE_ID", "TIME_STAMP", "USER_ID", "DESCRIPTION",
    "LONG_DESCRIPTION", "EXTERNAL_CODE")
values
   (32001, 3, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Dist: Metal Cables, Conduit, Poles', 'Dist: Metal Cables, Conduit, Poles', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32001, 4, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Dist: Non-Metal (Fiber Optic)', 'Dist: Non-Metal (Fiber Optic)', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32001, 5, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Wireless Telephone Electronic Equip', 'Wireless Telephone Electronic Equip', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (32001, 6, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Other- FF&E and Test Tools', 'Other- FF&E and Test Tools', null);

insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (32000, sysdate, user, 'ME-Network Computer Equip, Software', 'ME- Network Computer Equipment & Software', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (32001, sysdate, user, 'ME-Central Office Equip & Antennas', 'ME- Central Office Equipment & Antennas', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (32002, sysdate, user, 'ME-Distributions: Metallic Cables', 'ME- Distributions: Metallic Cables, Conduit, Cable and Poles', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (32003, sysdate, user, 'ME-Distribution: Non-Metallic', 'ME-Distribution: Non-Metallic (Fiber Optic)', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (32005, sysdate, user, 'ME-FF&E and Test Tools', 'ME-FF&E and Test Tools', 1, 2 );

insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32001, sysdate, user, 'ME-Non Reportable', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32002, sysdate, user, 'ME-Real', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32003, sysdate, user, 'ME-Personal Prop Furn & Fix', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32004, sysdate, user, 'ME-Personal Prop Mech & Equip', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32005, sysdate, user, 'ME-Personal Prop Comp Equip', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32006, sysdate, user, 'ME-Personal Prop Misc', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32007, sysdate, user, 'ME-Personal Prop Additions', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32008, sysdate, user, 'ME-Personal Prop Deletions', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32009, sysdate, user, 'ME-Personal Prop Leased Equip', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32010, sysdate, user, 'ME-Personal Prop Owned Others', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32011, sysdate, user, 'ME-Tele-Network Computer Equip & Software', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 3, 32000, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32012, sysdate, user, 'ME-Tele-Central Office Equipment & Antennas', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 3, 32001, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32013, sysdate, user, 'ME-Tele-Dist: Metal Cables, Conduit, Poles', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 3, 32002, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32014, sysdate, user, 'ME-Tele-Dist: Non-Metal (Fiber Optic)', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 3, 32003, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32015, sysdate, user, 'ME-Tele-Wireless Telephone Electronic Equip', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (32016, sysdate, user, 'ME-Tele-Other- FF&E and Test Tools', 'ME', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive                           ', 1, 3, 32005, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );

insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32003, 32000, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32004, 32000, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32005, 32000, 3, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32006, 32000, 4, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32007, 32000, 5, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32008, 32000, 6, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32009, 32000, 7, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32010, 32000, 8, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32011, 32001, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32012, 32001, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32013, 32001, 3, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32014, 32001, 4, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32015, 32001, 5, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (32016, 32001, 6, sysdate, user);

----
----new returns: MI 3589   Cable Television and Utility Assets from Form 3589, which flows to line 15 A on the L4175
----
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (33017, sysdate, user, 'MI-3589-A-G1 Assets', 'MI-3589-A-G1 Assets', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (33018, sysdate, user, 'MI-3589-A-G2 Assets', 'MI-3589-A-G2 Assets', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (33019, sysdate, user, 'MI-3589-A-G3 Assets', 'MI-3589-A-G3 Assets', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (33020, sysdate, user, 'MI-3589-B-H Assets', 'MI-3589-B-H Assets', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (33021, sysdate, user, 'MI-3589-B-I Assets', 'MI-3589-B-I Assets', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (33022, sysdate, user, 'MI-3589-B-J Assets', 'MI-3589-B-J Assets', 1, 2 );
insert into PT_RESERVE_FACTORS (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID) values (33023, sysdate, user, 'MI-3589-B-K Assets', 'MI-3589-B-K Assets', 1, 2 );

insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (33001, 16, sysdate, user, '3589-A-G1 Assets', '3589-A-G1 Assets', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (33001, 17, sysdate, user, '3589-A-G2 Assets', '3589-A-G2 Assets', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (33001, 18, sysdate, user, '3589-A-G3 Assets', '3589-A-G3 Assets', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (33001, 19, sysdate, user, '3589-B-H Assets', '3589-B-H Assets', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (33001, 20, sysdate, user, '3589-B-I Assets', '3589-B-I Assets', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (33001, 21, sysdate, user, '3589-B-J Assets', '3589-B-J Assets', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values (33001, 22, sysdate, user, '3589-B-K Assets', '3589-B-K Assets', '' );

insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (33029, sysdate, user, 'MI-3589-A-G1 Assets', 'MI                ', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 33017, null, null, 0, null, null, 1, 0, 0, 0, null, 5000, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (33030, sysdate, user, 'MI-3589-A-G2 Assets', 'MI                ', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 33018, null, null, 0, null, null, 1, 0, 0, 0, null, 5000, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (33031, sysdate, user, 'MI-3589-A-G3 Assets', 'MI                ', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 33019, null, null, 0, null, null, 1, 0, 0, 0, null, 5000, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (33032, sysdate, user, 'MI-3589-B-H Assets', 'MI                ', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 33020, null, null, 0, null, null, 1, 0, 0, 0, null, 5000, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (33033, sysdate, user, 'MI-3589-B-I Assets', 'MI                ', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 33021, null, null, 0, null, null, 1, 0, 0, 0, null, 5000, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (33034, sysdate, user, 'MI-3589-B-J Assets', 'MI                ', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 33022, null, null, 0, null, null, 1, 0, 0, 0, null, 5000, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (33035, sysdate, user, 'MI-3589-B-K Assets', 'MI                ', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 33023, null, null, 0, null, null, 1, 0, 0, 0, null, 5000, 1, '', null, null, null, 0, 1, null, null );

insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (33029, 33001, 16, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (33030, 33001, 17, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (33031, 33001, 18, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (33032, 33001, 19, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (33033, 33001, 20, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (33034, 33001, 21, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (33035, 33001, 22, sysdate, user);

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
values
   (514203, sysdate, user, 'MI-Pers Prop Cable, Utility-3589',
    'Michigan Cable Television and Utility Personal Property Return 3589', 'dw_ptr_rtn_mi_3589',
    'MI-Form 3589', 10, 200, 9, 7, 4, 3, 0);

insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL) values (514203, 'pt_report_pt_company_field', 'field1', sysdate, user, 'Doing Business As' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL) values (514203, 'pt_report_pt_company_field', 'field2', sysdate, user, 'Contact Name' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL) values (514203, 'pt_report_pt_company_field', 'field3', sysdate, user, 'Contact Phone' );

----
----new returns: NE 96 173 99 (local personal property)
----
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (514900, 'PWRPLANT', TO_DATE('2012-10-16 11:41:23', 'yyyy-mm-dd hh24:mi:ss'),
    'NE-Personal Prop. Return Standard', 'NE-Personal Prop. Return Standard (96-173-99 Rev)', null,
    'dw_ptr_rtn_ne_96-173-99_rev_dw1', null, null, null, 'NE-Personal Prop. Return Standard', null,
    null, null, 10, 200, 12, 7, 4, 3, null, null, null, null, null, 0);
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (514901, 'PWRPLANT', TO_DATE('2013-01-11 16:34:08', 'yyyy-mm-dd hh24:mi:ss'),
    'NE-Personal Prop. Schedule Standard', 'NE-Personal Prop. Schedule Standard (96-173-99 Rev)',
    null, 'dw_ptr_rtn_ne_96-173-99_rev_dw2', null, null, null, 'NE-Personal Prop. Schedule Standard',
    null, null, null, 10, 200, 12, 7, 4, 3, null, null, null, null, null, 0);

insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514900, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Owner Phone');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514900, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514900, 'pt_report_pt_company_field', 'field3',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Property Type');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514900, 'pt_report_pt_company_field', 'field4',
    TO_DATE('2012-11-08 14:49:46', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Address from Parcel or Loc');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514900, 'pt_report_pt_company_field', 'field5',
    TO_DATE('2012-11-08 14:47:43', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (514900, 'pt_report_pt_company_field', 'field6',
    TO_DATE('2012-11-08 14:49:26', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Description of Property');

insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (40000, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Personal Prop. Return Standard', 'NE-Personal Prop. Return', 2);
insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (40001, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Personal Prop. Schecule Standard', 'NE-Personal Prop. Filing Type', 2);

insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40000, 1, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Commercial & Industrial', 'NE-Commercial & Industrial', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40000, 2, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Agricultural Mech & Equip', 'NE-Agricultural Mech & Equip', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 1, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Commercial & Industrial Depr3', 'NE-Commercial & Industrial Depr3', '3');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 2, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Commercial & Industrial Depr5', 'NE-Commercial & Industrial Depr5', '5');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 3, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Commercial & Industrial Depr7', 'NE-Commercial & Industrial Depr7', '7');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 4, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Commercial & Industrial Depr10', 'NE-Commercial & Industrial Depr10', '10');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 5, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Commercial & Industrial Depr15', 'NE-Commercial & Industrial Depr15', '15');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 6, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Commercial & Industrial Depr20', 'NE-Commercial & Industrial Depr20', '20');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 7, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Agricultural Mech & Equip Depr3', 'NE-Agricultural Mech & Equip Depr3', '3');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 8, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Agricultural Mech & Equip Depr5', 'NE-Agricultural Mech & Equip Depr5', '5');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 9, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Agricultural Mech & Equip Depr7', 'NE-Agricultural Mech & Equip Depr7', '7');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 10, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Agricultural Mech & Equip Depr10', 'NE-Agricultural Mech & Equip Depr10', '10');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 11, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Agricultural Mech & Equip Depr15', 'NE-Agricultural Mech & Equip Depr15', '15');
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (40001, 12, TO_DATE('2012-11-08 15:04:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'NE-Agricultural Mech & Equip Depr20', 'NE-Agricultural Mech & Equip Depr20', '20');

insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40001, sysdate, user, 'NE-Not Reportable', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40002, sysdate, user, 'NE-Real', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40003, sysdate, user, 'NE-Commercial & Industrial Depr3', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40004, sysdate, user, 'NE-Commercial & Industrial Depr5', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40005, sysdate, user, 'NE-Commercial & Industrial Depr7', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40006, sysdate, user, 'NE-Commercial & Industrial Depr10', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40007, sysdate, user, 'NE-Commercial & Industrial Depr15', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40008, sysdate, user, 'NE-Commercial & Industrial Depr20', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40009, sysdate, user, 'NE-Agricultural Mech & Equip Depr3', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40010, sysdate, user, 'NE-Agricultural Mech & Equip Depr5', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40011, sysdate, user, 'NE-Agricultural Mech & Equip Depr7', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40012, sysdate, user, 'NE-Agricultural Mech & Equip Depr10', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40013, sysdate, user, 'NE-Agricultural Mech & Equip Depr15', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (40014, sysdate, user, 'NE-Agricultural Mech & Equip Depr20', 'NE', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 0, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );

insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40003, 40001, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40004, 40001, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40005, 40001, 3, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40006, 40001, 4, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40007, 40001, 5, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40008, 40001, 6, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40009, 40001, 7, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40010, 40001, 8, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40011, 40001, 9, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40012, 40001, 10, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40013, 40001, 11, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40014, 40001, 12, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40003, 40000, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40004, 40000, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40005, 40000, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40006, 40000, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40007, 40000, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40008, 40000, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40009, 40000, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40010, 40000, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40011, 40000, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40012, 40000, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40013, 40000, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (40014, 40000, 2, sysdate, user);

----
----new returns: SC 420 (utility, rail filings)
----
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (516000, 'PWRPLANT', TO_DATE('2012-10-16 11:41:23', 'yyyy-mm-dd hh24:mi:ss'),
    'SC-PT-420 Standardized', 'SC-PT-420 Standardized (2010 revision)', null, 'dw_ptr_rtn_sc_pt420',
    null, null, null, 'SC-PT420 Standardized', null, null, null, 10, 200, 12, 7, 4, 3, null, null,
    null, null, null, 0);

insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'File Number');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field10',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'SC Gross Income');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field11',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Total Net Income');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field12',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'SC Net Income');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field13',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'SC Net Book Value');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field14',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Signer Title');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Contact Person');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field3',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Phone Number');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field4',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Fax Number');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field5',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Total Revenue');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field6',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'SC Revenue');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field7',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Total Net Operating Income');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field8',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'SC Net Operating Income');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516000, 'pt_report_pt_company_field', 'field9',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Total Gross Income');

insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (51010, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'SC-PT-420',
    'SC-PT-420', 2);

insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (51010, 1, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'SC-Personal Property', 'SC-Personal Property', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (51010, 2, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'SC-Real Property', 'SC-Real Property', null);

insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (51060, sysdate, user, 'SC-PT-420-Personal', 'SC', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (51061, sysdate, user, 'SC-PT-420-Real', 'SC', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );

insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (51060, 51010, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (51061, 51010, 2, sysdate, user);

----
----tx bpp was out of date--fixed
----
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL)
values
   (516301, 'pt_report_pt_company_field', 'field10', 'Authorized Agent (Y/N)?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL)
values
   (516301, 'pt_report_pt_company_field', 'field11', 'In Fiduciary Capacity (Y/N)?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL)
values
   (516301, 'pt_report_pt_company_field', 'field12', 'Affirm Check Box (Y/N)?');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL)
values
   (516301, 'pt_report_pt_company_field', 'field13', 'Affirm Check Box Prior Year');

----
----new returns: WI 003
----
insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (516800, 'PWRPLANT', TO_DATE('2013-01-11 18:04:23', 'yyyy-mm-dd hh24:mi:ss'), 'WI-PA-003',
    'WI-PA-003', null, 'dw_ptr_rtn_wi_pa_003', null, null, null, 'WI-PA-003', null, null, null, 10,
    200, 12, 7, 1, 3, null, null, null, null, null, 0);

insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field1',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Type of Business');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field10',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Fax');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field11',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Email');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field12',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Revision Date');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field2',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Owner Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field3',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Owner Address');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field4',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Owner Phone');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field5',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Owner Fax');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field6',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Owner Email');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field7',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Name');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field8',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Address');
insert into PT_REPORT_FIELD_LABELS
   (REPORT_ID, TABLE_NAME, COLUMN_NAME, TIME_STAMP, USER_ID, LABEL)
values
   (516800, 'pt_report_pt_company_field', 'field9',
    TO_DATE('2012-09-06 22:52:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Preparer Phone');

insert into PT_TYPE_ROLLUP
   (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (59003, TO_DATE('2012-09-10 20:54:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'WI-PA-003',
    'WI-PA-003', 2);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 1, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Boats & Other Watercraft', 'Boats & Other Watercraft', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 2, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Machinery, Tools, Patterns', 'Machinery, Tools, Patterns', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 3, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Furniture, Fixtures & Office Equip', 'Furniture, Fixtures & Office Equip', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 4, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Fax, Postage, Copiers, Phone', 'Fax, Postage, Copiers, Phone', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 5, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Leased Equipment', 'Leased Equipment', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 6, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Supplies',
    'Supplies', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 7, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'All Other Personal Property', 'All Other Personal Property', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 8, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'Building on Leased Land', 'Building on Leased Land', null);
insert into PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
    EXTERNAL_CODE)
values
   (59003, 9, TO_DATE('2012-09-06 22:52:34', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'EXEMPT Computer Equipment & Soft', 'EXEMPT Computer Equipment & Soft', null);

insert into PT_RESERVE_FACTORS
   (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID,
    STATUS_CODE_ID)
values
   (59011, TO_DATE('2012-11-01 17:39:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'WI-BPP-Sch C-Mach Tools', 'WI-BPP-Sch C-Mach Tools', 1, 2);
insert into PT_RESERVE_FACTORS
   (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID,
    STATUS_CODE_ID)
values
   (59012, TO_DATE('2012-11-01 17:39:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'WI-BPP-Sch D-Furn Fix', 'WI-BPP-Sch D-Furn Fix', 1, 2);
insert into PT_RESERVE_FACTORS
   (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID,
    STATUS_CODE_ID)
values
   (59013, TO_DATE('2012-11-01 17:39:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'WI-BPP-Sch D1-Exempt Comp', 'WI-BPP-Sch D1-Exempt Comp', 1, 2);
insert into PT_RESERVE_FACTORS
   (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID,
    STATUS_CODE_ID)
values
   (59014, TO_DATE('2012-11-01 17:39:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    'WI-BPP-Sch D2-Faxes Copiers', 'WI-BPP-Sch D2-Faxes Copiers', 1, 2);

insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59036, sysdate, user, 'WI-PA-003-Boats & Other Watercraft', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59037, sysdate, user, 'WI-PA-003-Machinery, Tools, Patterns', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 59011, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59038, sysdate, user, 'WI-PA-003-Furniture, Fixtures & Office Equip', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 59012, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59039, sysdate, user, 'WI-PA-003-Fax, Postage, Copiers, Phone', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 59014, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59040, sysdate, user, 'WI-PA-003-Leased Equipment', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59041, sysdate, user, 'WI-PA-003-Supplies', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59042, sysdate, user, 'WI-PA-003-All Other Personal Property', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59043, sysdate, user, 'WI-PA-003-Building on Leased Land', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 1, null, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );
insert into PROPERTY_TAX_TYPE_DATA (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD, RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID, INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, ESCALATE_RESERVE_YN, MARKET_VALUE_RATE_ID, LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID, KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID) values (59044, sysdate, user, 'WI-PA-003-EXEMPT Computer, Soft', 'WI', to_date('01/01/2000', 'mm/dd/yyyy'), 'Inactive', 1, 3, 59013, null, null, 0, null, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null );

insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59036, 59003, 1, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59037, 59003, 2, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59038, 59003, 3, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59039, 59003, 4, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59040, 59003, 5, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59041, 59003, 6, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59042, 59003, 7, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59043, 59003, 8, sysdate, user);
insert into PT_TYPE_ROLLUP_ASSIGN (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID) values (59044, 59003, 9, sysdate, user);
*/
--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (273, 0, 10, 4, 1, 0, 11698, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011698_proptax_standard_return_fixes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
SET DEFINE ON