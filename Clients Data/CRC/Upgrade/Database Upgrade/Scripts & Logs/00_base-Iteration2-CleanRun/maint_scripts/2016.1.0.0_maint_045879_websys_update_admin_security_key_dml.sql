/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045879_websys_update_admin_security_key_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 08/09/2016 Jared Watkins			 Update default Admin key for easier security access through the web app
||============================================================================
*/

update pp_web_security_perm_control
set module_id = 1,
component_id = NULL,
pseudo_component = 'Security'
where objects = 'Security.Admin.All'
and exists (
  select 1 from pp_web_security_perm_control
  where objects = 'Security.Admin.All'
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3252, 0, 2016, 1, 0, 0, 45879, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045879_websys_update_admin_security_key_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;