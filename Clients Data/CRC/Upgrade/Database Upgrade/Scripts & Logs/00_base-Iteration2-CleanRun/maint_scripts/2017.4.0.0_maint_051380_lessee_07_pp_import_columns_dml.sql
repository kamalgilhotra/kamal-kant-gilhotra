/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051380_lessee_07_pp_import_columns_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/30/2018  Alex Healey    Create new import columns setup for purchase options
||============================================================================
*/

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                         ,
                'ilr_id'                    ,
                SYSDATE                     ,
                USER                        ,
                'ILR ID'                    ,
                'ilr_id_xlate'              ,
                1                           ,
                1                           ,
                'number(22,0)'              ,
                'ls_ilr',
                ' '                          ,
                1                           ,
                NULL                        ,
                ' '                         ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268           ,
                'revision'    ,
                SYSDATE       ,
                USER          ,
                'Revision'    ,
                ' '            ,
                0             ,
                1             ,
                'number(22,0)',
                ' '            ,
                ' '            ,
                1             ,
                NULL          ,
                ' '           ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                     ,
                'ilr_purchase_option_id',
                SYSDATE                 ,
                USER                    ,
                'Purchase Option ID'       ,
                ' '                      ,
                1                       ,
                1                       ,
                'number(22,0)'          ,
                ' '                      ,
                ' '                      ,
                1                       ,
                NULL                    ,
                ' '                     ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                          ,
                'ilr_purchase_probability_id',
                SYSDATE                      ,
                USER                         ,
                'Probability of Purchase'    ,
                'ilr_purchase_prob_id_xlate' ,
                1                            ,
                1                            ,
                'number(22,0)'               ,
                'ls_ilr_renewal_probability' ,
                ' '                           ,
                1                            ,
                NULL                         ,
                ' '                          ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                          ,
                'decision_notice'            ,
                SYSDATE                      ,
                USER                         ,
                'Decision Notice (in Months)',
                ' '                           ,
                0                            ,
                1                            ,
                'number(22,0)'               ,
                ' '                           ,
                ' '                           ,
                1                            ,
                NULL                         ,
                ' '                          ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                    ,
                'decision_date'        ,
                SYSDATE                ,
                USER                   ,
                'Actual Date to Decide',
                ' '                     ,
                0                      ,
                1                      ,
                'date mmddyyyy format' ,
                ' '                     ,
                ' '                     ,
                1                      ,
                NULL                   ,
                ' '                    ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                   ,
                'purchase_date'       ,
                SYSDATE               ,
                USER                  ,
                'Date Purchase Occurs',
                ' '                    ,
                0                     ,
                1                     ,
                'date mmddyyyy format',
                ' '                    ,
                ' '                    ,
                1                     ,
                NULL                  ,
                ' '                   ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                         ,
                'purchase_option_type'      ,
                SYSDATE                     ,
                USER                        ,
                'Type of Purchase Option'   ,
                'purchase_option_type_xlate',
                1                           ,
                1                           ,
                'number(22,0)'              ,
                'ls_purchase_option_type'   ,
                ' '                          ,
                1                           ,
                NULL                        ,
                ' '                         ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                 ,
                'purchase_amt'      ,
                SYSDATE             ,
                USER                ,
                'Amount to Purchase',
                ' '                  ,
                1                   ,
                1                   ,
                'number(22,2)'      ,
                ' '                  ,
                ' '                  ,
                1                   ,
                NULL                ,
                ' '                 ,
                '0'
        );
INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                268                 ,
                'is_modified'      ,
                SYSDATE             ,
                USER                ,
                'Is modified',
                ' '                  ,
                1                   ,
                1                   ,
                'number(22,0)'      ,
                ' '                  ,
                ' '                  ,
                1                   ,
                NULL                ,
                ' '                 ,
                '0'
        );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6083, 0, 2017, 4, 0, 0, 51380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051380_lessee_07_pp_import_columns_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;