/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_036380_proptax_mi3589_mods2.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/30/2014 Anand Rajashekar    changes to MI-3589 returns
||========================================================================================
*/

-- Change description for property tax types for MI-3589 report

update PROPERTY_TAX_TYPE_DATA set DESCRIPTION = 'MI-3589-1-G1 Assets' where DESCRIPTION = 'MI-3589-A-G1 Assets' ;

update PROPERTY_TAX_TYPE_DATA set DESCRIPTION = 'MI-3589-1-G2 Assets' where DESCRIPTION = 'MI-3589-A-G2 Assets' ;

update PROPERTY_TAX_TYPE_DATA set DESCRIPTION = 'MI-3589-1-G3 Assets' where DESCRIPTION = 'MI-3589-A-G3 Assets' ;

update PROPERTY_TAX_TYPE_DATA set DESCRIPTION = 'MI-3589-2-H Assets' where DESCRIPTION = 'MI-3589-B-H Assets' ;

update PROPERTY_TAX_TYPE_DATA set DESCRIPTION = 'MI-3589-2-I Assets' where DESCRIPTION = 'MI-3589-B-I Assets' ;

update PROPERTY_TAX_TYPE_DATA set DESCRIPTION = 'MI-3589-2-J Assets' where DESCRIPTION = 'MI-3589-B-J Assets' ;

update PROPERTY_TAX_TYPE_DATA set DESCRIPTION = 'MI-3589-2-K Assets' where DESCRIPTION = 'MI-3589-B-K Assets' ;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1465, 0, 10, 4, 3, 0, 36380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036380_proptax_mi3589_mods2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;