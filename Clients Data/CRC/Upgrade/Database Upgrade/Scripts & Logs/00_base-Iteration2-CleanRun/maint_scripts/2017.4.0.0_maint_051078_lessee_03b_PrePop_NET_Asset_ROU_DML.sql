/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051078_lessee_03b_PrePop_NET_Asset_ROU_DML.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0   05/18/2018   K. Powers   PP-51078 Prepopulate the Schedule tables with 
 ||                                     beg_net_rou_asset and end_net_rou_asset
 ||============================================================================
 */ 
-- Seed data into new columns in LS_ASSET_SCHEDULE

merge INTO LS_ASSET_SCHEDULE z
    USING ( SELECT distinct ls_asset_id,revision,set_of_books_id,begin_reserve,end_reserve,month
        FROM   LS_DEPR_FORECAST ldf) a
    ON ( z.ls_asset_id = a.ls_asset_id AND z.revision = a.revision AND z.set_of_books_id = a.set_of_books_id AND z.month=a.month)
  WHEN matched THEN
      UPDATE SET z.beg_net_ROU_asset = nvl(z.beg_capital_cost,0) - nvl(a.begin_reserve,0),
                 z.end_net_ROU_asset = nvl(z.end_capital_cost,0) - nvl(a.end_reserve,0);
                

-- Seed new columns in LS_ILR_SCHEDULE
 
  merge INTO LS_ILR_SCHEDULE z
    USING ( SELECT la.ilr_id as ilr_id,ldf.revision as revision,ldf.set_of_books_id as set_of_books_id,ldf.month as month,
            sum(ldf.begin_reserve) as beg_res,sum(ldf.end_reserve) as end_res
        FROM   LS_DEPR_FORECAST ldf,ls_asset la 
        where  ldf.ls_asset_id = la.ls_asset_id 
        group by la.ilr_id,ldf.revision,ldf.set_of_books_id,month) a
    ON ( z.ilr_id = a.ilr_id AND z.revision = a.revision AND z.set_of_books_id = a.set_of_books_id AND z.month=a.month)
    WHEN matched THEN
      UPDATE SET z.beg_net_ROU_asset = nvl(z.beg_capital_cost,0) - nvl(a.beg_res,0),
                 z.end_net_ROU_asset = nvl(z.end_capital_cost,0) - nvl(a.end_res,0);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5624, 0, 2017, 4, 0, 0, 51078, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051078_lessee_03b_PrePop_NET_Asset_ROU_DML.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;