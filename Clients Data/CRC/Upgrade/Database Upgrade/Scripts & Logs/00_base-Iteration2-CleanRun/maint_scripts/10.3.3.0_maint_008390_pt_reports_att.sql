SET DEFINE OFF
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008390_pt_reports_att.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/11/2011 Julia Breuer   Point Release
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DYNAMIC_DW)
values
   (503080, 'Asmt-MV-Without Pymt by Dist & Prcl',
    'Assessment values (market value only) by tax district and parcel for which there is no corresponding payment. Includes prop tax balance, market value, assessment, equalized value, and taxable value.  Grouped by prop tax company, state, and county.',
    'dw_ptr_asmt_by_st_cty_td_pr_wo_pymt_mv', 'PropTax - 3080', 10, 9, 18, 7, 1, 3, 0);

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DYNAMIC_DW)
values
   (506531, 'Compare: Taxes Paid by StGrp & Stmt',
    'Comparison of taxes paid by statement.  Includes prior value, current value, difference, and percent difference.  Grouped by prop tax company, state, and statement group.',
    'dw_ptr_bill_tax_compare_by_sgroup_stmt', 'PropTax - 6531', 10, 9, 27, 8, 1, 3, 0);

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DYNAMIC_DW)
values
   (507110, 'Payments by Asmt Year, Co, & State',
    'Payment amounts by assessment year, prop tax company, and state.  Includes paid amount.',
    'dw_ptr_bill_paid_by_ay_ptco_st', 'PropTax - 7110', 10, 9, 33, 15, 1, 3, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (49, 0, 10, 3, 3, 0, 8390, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008390_pt_reports_att.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON