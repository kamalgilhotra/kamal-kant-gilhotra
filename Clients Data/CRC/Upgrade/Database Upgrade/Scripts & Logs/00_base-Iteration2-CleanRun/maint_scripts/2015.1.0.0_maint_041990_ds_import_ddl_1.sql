/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041990_ds_import_ddl_1.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   01/12/2015 A Scott          Import tool setup for depr studies
||============================================================================
*/


SET SERVEROUTPUT ON

begin
   execute immediate 'drop table ds_import_plant';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ds_import_plant does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table ds_import_plant_arc';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ds_import_plant_arc does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table ds_import_reserve';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ds_import_reserve does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table ds_import_reserve_arc';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ds_import_reserve_arc does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table ds_import_data_account';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ds_import_data_account does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table ds_import_data_account_arc';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ds_import_data_account_arc does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table ds_import_depr_group';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ds_import_depr_group does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 'drop table ds_import_depr_group_arc';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ds_import_depr_group_arc does not exist or drop table priv not granted.');
end;
/

----ds_import_plant
create table ds_import_plant
(
  import_run_id             NUMBER(22,0) not null,
  line_id                   NUMBER(22,0) not null,
  time_stamp                DATE,
  user_id                   VARCHAR2(18),
  ds_trans_id               NUMBER(22,0),
  ds_data_account_xlate     VARCHAR2(254),
  ds_data_account_id        NUMBER(22,0),
  company_xlate             VARCHAR2(254),
  company_id                NUMBER(22,0),
  analysis_trans_xlate      VARCHAR2(254),
  analysis_trans_id         NUMBER(22,0),
  vintage                   VARCHAR2(35),
  activity_year             VARCHAR2(35),
  adjustment_year           VARCHAR2(35),
  effective_date            DATE,
  amount                    VARCHAR2(35),
  sum_factor                NUMBER(22,8),
  final_amount              NUMBER(22,2),
  quantity                  VARCHAR2(35),
  transaction_input_type_id NUMBER(22,0),
  description               VARCHAR2(254),
  set_of_books_id           NUMBER(22,0),
  set_of_books_id_xlate     VARCHAR2(254),
  is_modified               NUMBER(22,0),
  error_message             VARCHAR2(4000)
);

comment on table ds_import_plant is '(O and C) [05] The DS Import Plant table is a staging table into which data from a run of the DS Import Plant process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the DS Import Plant Archive table.';
comment on column ds_import_plant.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column ds_import_plant.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column ds_import_plant.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ds_import_plant.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ds_import_plant.ds_trans_id is 'Unique identifier for final table DS data transaction.  Populated with the pwrplant1.nextval sequence during import processing.';
comment on column ds_import_plant.ds_data_account_xlate is 'Field provided by the user on the import file used to translate ds data account id.';
comment on column ds_import_plant.ds_data_account_id is 'System-assigned identifier of the ds data account.';
comment on column ds_import_plant.company_xlate is 'Field provided by the user on the import file used to translate company id.';
comment on column ds_import_plant.company_id is 'System-assigned identifier of a company.';
comment on column ds_import_plant.analysis_trans_xlate is 'Field provided by the user on the import file used to translate analysis trans id';
comment on column ds_import_plant.analysis_trans_id is 'System-assigned identifier of an analysis transaction code.';
comment on column ds_import_plant.vintage is 'Vintage of the plant transaction';
comment on column ds_import_plant.activity_year is 'Activity year of the plant transaction';
comment on column ds_import_plant.adjustment_year is 'Adjustment year of the plant transaction';
comment on column ds_import_plant.effective_date is 'Effective date of the plant transaction.  Calculated as June of the activity year.';
comment on column ds_import_plant.amount is 'Amount imported from file.';
comment on column ds_import_plant.sum_factor is 'Sum factor populated from the analysis transaction code.';
comment on column ds_import_plant.final_amount is 'Amount imported from file times the sum factor field from the corresponding analysis transaction code.';
comment on column ds_import_plant.quantity is 'Quantity imported from file.';
comment on column ds_import_plant.transaction_input_type_id is 'System-assigned identifier of the transaction input type.  Set to 2/"External" during import processing.';
comment on column ds_import_plant.description is 'Description of the transaction being imported.';
comment on column ds_import_plant.set_of_books_id is 'System-assigned identifier of a set of books.';
comment on column ds_import_plant.set_of_books_id_xlate is 'Field provided by the user on the import file used to translate set of books id';
comment on column ds_import_plant.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column ds_import_plant.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';

alter table ds_import_plant
  add constraint ds_import_plant_pk primary key (IMPORT_RUN_ID, LINE_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table ds_import_plant
  add constraint ds_import_plant_run_fk foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);

----ds_import_plant_arc
create table ds_import_plant_arc
(
  import_run_id             NUMBER(22,0) not null,
  line_id                   NUMBER(22,0) not null,
  time_stamp                DATE,
  user_id                   VARCHAR2(18),
  ds_trans_id               NUMBER(22,0),
  ds_data_account_xlate     VARCHAR2(254),
  ds_data_account_id        NUMBER(22,0),
  company_xlate             VARCHAR2(254),
  company_id                NUMBER(22,0),
  analysis_trans_xlate      VARCHAR2(254),
  analysis_trans_id         NUMBER(22,0),
  vintage                   VARCHAR2(35),
  activity_year             VARCHAR2(35),
  adjustment_year           VARCHAR2(35),
  effective_date            DATE,
  amount                    VARCHAR2(35),
  sum_factor                NUMBER(22,8),
  final_amount              NUMBER(22,2),
  quantity                  VARCHAR2(35),
  transaction_input_type_id NUMBER(22,0),
  description               VARCHAR2(254),
  set_of_books_id           NUMBER(22,0),
  set_of_books_id_xlate     VARCHAR2(254),
  is_modified               NUMBER(22,0)
);

comment on table ds_import_plant_arc is '(O and C) [05] The DS Import Plant Archive is where run data of the DS Import Plant process is archived.';
comment on column ds_import_plant_arc.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column ds_import_plant_arc.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column ds_import_plant_arc.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ds_import_plant_arc.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ds_import_plant_arc.ds_trans_id is 'Unique identifier for final table DS data transaction.  Populated with the pwrplant1.nextval sequence during import processing.';
comment on column ds_import_plant_arc.ds_data_account_xlate is 'Field provided by the user on the import file used to translate ds data account id.';
comment on column ds_import_plant_arc.ds_data_account_id is 'System-assigned identifier of the ds data account.';
comment on column ds_import_plant_arc.company_xlate is 'Field provided by the user on the import file used to translate company id.';
comment on column ds_import_plant_arc.company_id is 'System-assigned identifier of a company.';
comment on column ds_import_plant_arc.analysis_trans_xlate is 'Field provided by the user on the import file used to translate analysis trans id';
comment on column ds_import_plant_arc.analysis_trans_id is 'System-assigned identifier of an analysis transaction code.';
comment on column ds_import_plant_arc.vintage is 'Vintage of the plant transaction';
comment on column ds_import_plant_arc.activity_year is 'Activity year of the plant transaction';
comment on column ds_import_plant_arc.adjustment_year is 'Adjustment year of the plant transaction';
comment on column ds_import_plant_arc.effective_date is 'Effective date of the plant transaction.  Calculated as June of the activity year.';
comment on column ds_import_plant_arc.amount is 'Amount imported from file.';
comment on column ds_import_plant_arc.sum_factor is 'Sum factor populated from the analysis transaction code.';
comment on column ds_import_plant_arc.final_amount is 'Amount imported from file times the sum factor field from the corresponding analysis transaction code.';
comment on column ds_import_plant_arc.quantity is 'Quantity imported from file.';
comment on column ds_import_plant_arc.transaction_input_type_id is 'System-assigned identifier of the transaction input type.  Set to 2/"External" during import processing.';
comment on column ds_import_plant_arc.description is 'Description of the transaction being imported.';
comment on column ds_import_plant_arc.set_of_books_id is 'System-assigned identifier of a set of books.';
comment on column ds_import_plant_arc.set_of_books_id_xlate is 'Field provided by the user on the import file used to translate set of books id';
comment on column ds_import_plant_arc.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';

alter table ds_import_plant_arc
  add constraint ds_import_plant_arc_pk primary key (IMPORT_RUN_ID, LINE_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table ds_import_plant_arc
  add constraint ds_import_plant_run_arc_fk foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);

----ds_import_reserve
create table ds_import_reserve
(
  import_run_id             NUMBER(22,0) not null,
  line_id                   NUMBER(22,0) not null,
  time_stamp                DATE,
  user_id                   VARCHAR2(18),
  ds_trans_id               NUMBER(22,0),
  depr_group_xlate          VARCHAR2(254),
  depr_group_id             NUMBER(22,0),
  company_xlate             VARCHAR2(254),
  company_id                NUMBER(22,0),
  analysis_trans_xlate      VARCHAR2(254),
  analysis_trans_id         NUMBER(22,0),
  activity_year             VARCHAR2(35),
  effective_date            DATE,
  amount                    VARCHAR2(35), 
  quantity                  VARCHAR2(35),
  transaction_input_type_id NUMBER(22,0),
  description               VARCHAR2(254),
  set_of_books_id           NUMBER(22,0),
  set_of_books_id_xlate     VARCHAR2(254),
  is_modified               NUMBER(22,0),
  error_message             VARCHAR2(4000)
);

comment on table ds_import_reserve is '(O and C) [05] The DS Import Reserve table is a staging table into which data from a run of the DS Import Plant process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the DS Import Reserve Archive table.';
comment on column ds_import_reserve.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column ds_import_reserve.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column ds_import_reserve.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ds_import_reserve.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ds_import_reserve.ds_trans_id is 'Unique identifier for final table DS data transaction.  Populated with the pwrplant1.nextval sequence during import processing.';
comment on column ds_import_reserve.depr_group_xlate is 'Field provided by the user on the import file used to translate depr group.';
comment on column ds_import_reserve.depr_group_id is 'System-assigned identifier of the depr group.';
comment on column ds_import_reserve.company_xlate is 'Field provided by the user on the import file used to translate company id.';
comment on column ds_import_reserve.company_id is 'System-assigned identifier of a company.';
comment on column ds_import_reserve.analysis_trans_xlate is 'Field provided by the user on the import file used to translate analysis trans id';
comment on column ds_import_reserve.analysis_trans_id is 'System-assigned identifier of an analysis transaction code.';
comment on column ds_import_reserve.activity_year is 'Activity year of the reserve transaction';
comment on column ds_import_reserve.effective_date is 'Effective date of the plant transaction.  Calculated as June of the activity year.';
comment on column ds_import_reserve.amount is 'Amount imported from file.';
comment on column ds_import_reserve.quantity is 'Quantity imported from file.';
comment on column ds_import_reserve.transaction_input_type_id is 'System-assigned identifier of the transaction input type.  Set to 2/"External" during import processing.';
comment on column ds_import_reserve.description is 'Description of the transaction being imported.';
comment on column ds_import_reserve.set_of_books_id is 'System-assigned identifier of a set of books.';
comment on column ds_import_reserve.set_of_books_id_xlate is 'Field provided by the user on the import file used to translate set of books id';
comment on column ds_import_reserve.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column ds_import_reserve.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';

alter table ds_import_reserve
  add constraint ds_import_reserve_pk primary key (IMPORT_RUN_ID, LINE_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table ds_import_reserve
  add constraint ds_import_reserve_run_fk foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);

----ds_import_reserve_arc
create table ds_import_reserve_arc
(
  import_run_id             NUMBER(22,0) not null,
  line_id                   NUMBER(22,0) not null,
  time_stamp                DATE,
  user_id                   VARCHAR2(18),
  ds_trans_id               NUMBER(22,0),
  depr_group_xlate          VARCHAR2(254),
  depr_group_id             NUMBER(22,0),
  company_xlate             VARCHAR2(254),
  company_id                NUMBER(22,0),
  analysis_trans_xlate      VARCHAR2(254),
  analysis_trans_id         NUMBER(22,0),
  activity_year             VARCHAR2(35),
  effective_date            DATE,
  amount                    VARCHAR2(35),
  quantity                  VARCHAR2(35),
  transaction_input_type_id NUMBER(22,0),
  description               VARCHAR2(254),
  set_of_books_id           NUMBER(22,0),
  set_of_books_id_xlate     VARCHAR2(254),
  is_modified               NUMBER(22,0)
);

comment on table ds_import_reserve_arc is '(O and C) [05] The DS Import Reserve Archive table is where run data of the DS Import Reserve process is archived.';
comment on column ds_import_reserve_arc.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column ds_import_reserve_arc.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column ds_import_reserve_arc.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ds_import_reserve_arc.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ds_import_reserve_arc.ds_trans_id is 'Unique identifier for final table DS data transaction.  Populated with the pwrplant1.nextval sequence during import processing.';
comment on column ds_import_reserve_arc.depr_group_xlate is 'Field provided by the user on the import file used to translate depr group.';
comment on column ds_import_reserve_arc.depr_group_id is 'System-assigned identifier of the depr group.';
comment on column ds_import_reserve_arc.company_xlate is 'Field provided by the user on the import file used to translate company id.';
comment on column ds_import_reserve_arc.company_id is 'System-assigned identifier of a company.';
comment on column ds_import_reserve_arc.analysis_trans_xlate is 'Field provided by the user on the import file used to translate analysis trans id';
comment on column ds_import_reserve_arc.analysis_trans_id is 'System-assigned identifier of an analysis transaction code.';
comment on column ds_import_reserve_arc.activity_year is 'Activity year of the reserve transaction';
comment on column ds_import_reserve_arc.effective_date is 'Effective date of the plant transaction.  Calculated as June of the activity year.';
comment on column ds_import_reserve_arc.amount is 'Amount imported from file.';
comment on column ds_import_reserve_arc.quantity is 'Quantity imported from file.';
comment on column ds_import_reserve_arc.transaction_input_type_id is 'System-assigned identifier of the transaction input type.  Set to 2/"External" during import processing.';
comment on column ds_import_reserve_arc.description is 'Description of the transaction being imported.';
comment on column ds_import_reserve_arc.set_of_books_id is 'System-assigned identifier of a set of books.';
comment on column ds_import_reserve_arc.set_of_books_id_xlate is 'Field provided by the user on the import file used to translate set of books id';
comment on column ds_import_reserve_arc.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';

alter table ds_import_reserve_arc
  add constraint ds_import_reserve_arc_pk primary key (IMPORT_RUN_ID, LINE_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table ds_import_reserve_arc
  add constraint ds_import_reserve_arc_run_fk foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);

----ds_import_data_account
create table ds_import_data_account
(
  import_run_id             NUMBER(22,0) not null,
  line_id                   NUMBER(22,0) not null,
  time_stamp                DATE,
  user_id                   VARCHAR2(18),
  ds_data_account_xlate     VARCHAR2(254),
  ds_data_account_id        NUMBER(22,0),
  company_xlate             VARCHAR2(254),
  company_id                NUMBER(22,0),
  data_account_class_id     NUMBER(22,0),
  description               VARCHAR2(254),
  long_description          VARCHAR2(254),
  status_code_id            NUMBER(22,0),
  status_code_xlate         VARCHAR2(254),
  is_modified               NUMBER(22,0),
  error_message             VARCHAR2(4000)
);

comment on table ds_import_data_account is '(O and C) [05] The DS Import Data Account table is a staging table into which data from a run of the DS Import Data Account process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the DS Import Data Account Archive table.';
comment on column ds_import_data_account.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column ds_import_data_account.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column ds_import_data_account.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ds_import_data_account.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ds_import_data_account.ds_data_account_xlate is 'Field provided by the user on the import file used to translate ds data account id.';
comment on column ds_import_data_account.ds_data_account_id is 'System-assigned identifier of the ds data account.';
comment on column ds_import_data_account.company_xlate is 'Field provided by the user on the import file used to translate company id.';
comment on column ds_import_data_account.company_id is 'System-assigned identifier of a company.';
comment on column ds_import_data_account.data_account_class_id is 'System-assigned identifier of a ds data account class.  Automatically set to be -1 (Default).';
comment on column ds_import_data_account.description is 'Description of the ds data account being imported.';
comment on column ds_import_data_account.long_description is 'Long Description of the ds data account being imported.';
comment on column ds_import_data_account.status_code_id is 'System-assigned identifier of a status code.';
comment on column ds_import_data_account.status_code_xlate is 'Field provided by the user on the import file used to translate status code.';
comment on column ds_import_data_account.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column ds_import_data_account.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';

alter table ds_import_data_account
  add constraint ds_import_data_account_pk primary key (IMPORT_RUN_ID, LINE_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table ds_import_data_account
  add constraint ds_import_data_account_run_fk foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);


----ds_import_data_account_arc
create table ds_import_data_account_arc
(
  import_run_id             NUMBER(22,0) not null,
  line_id                   NUMBER(22,0) not null,
  time_stamp                DATE,
  user_id                   VARCHAR2(18),
  ds_data_account_xlate     VARCHAR2(254),
  ds_data_account_id        NUMBER(22,0),
  company_xlate             VARCHAR2(254),
  company_id                NUMBER(22,0),
  data_account_class_id     NUMBER(22,0),
  description               VARCHAR2(254),
  long_description          VARCHAR2(254),
  status_code_id            NUMBER(22,0),
  status_code_xlate         VARCHAR2(254),
  is_modified               NUMBER(22,0)
);

comment on table ds_import_data_account_arc is '(O and C) [05] The DS Import Data Account Archive table is where run data of the DS Import Data Account process is archived.';
comment on column ds_import_data_account_arc.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column ds_import_data_account_arc.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column ds_import_data_account_arc.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ds_import_data_account_arc.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ds_import_data_account_arc.ds_data_account_xlate is 'Field provided by the user on the import file used to translate ds data account id.';
comment on column ds_import_data_account_arc.ds_data_account_id is 'System-assigned identifier of the ds data account.';
comment on column ds_import_data_account_arc.company_xlate is 'Field provided by the user on the import file used to translate company id.';
comment on column ds_import_data_account_arc.company_id is 'System-assigned identifier of a company.';
comment on column ds_import_data_account_arc.data_account_class_id is 'System-assigned identifier of a ds data account class.  Automatically set to be -1 (Default).';
comment on column ds_import_data_account_arc.description is 'Description of the ds data account being imported.';
comment on column ds_import_data_account_arc.long_description is 'Long Description of the ds data account being imported.';
comment on column ds_import_data_account_arc.status_code_id is 'System-assigned identifier of a status code.';
comment on column ds_import_data_account_arc.status_code_xlate is 'Field provided by the user on the import file used to translate status code.';
comment on column ds_import_data_account_arc.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';

alter table ds_import_data_account_arc
  add constraint ds_import_data_account_arc_pk primary key (IMPORT_RUN_ID, LINE_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table ds_import_data_account_arc
  add constraint ds_import_data_acct_arc_run_fk foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);

----ds_import_depr_group
create table ds_import_depr_group
(
  import_run_id             NUMBER(22,0) not null,
  line_id                   NUMBER(22,0) not null,
  time_stamp                DATE,
  user_id                   VARCHAR2(18),
  depr_group_xlate          VARCHAR2(254),
  depr_group_id             NUMBER(22,0),
  company_xlate             VARCHAR2(254),
  company_id                NUMBER(22,0),
  subledger_type_id         NUMBER(22,0),
  mid_period_conv           NUMBER(22,0),
  mid_period_method         VARCHAR2(254),
  description               VARCHAR2(254),
  external_depr_code        VARCHAR2(254),
  is_modified               NUMBER(22,0),
  error_message             VARCHAR2(4000)
);

comment on table ds_import_depr_group is '(O and C) [05] The DS Import Depr Group table is a staging table into which data from a run of the DS Import Depr Group process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the DS Import Depr Group Archive table.';
comment on column ds_import_depr_group.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column ds_import_depr_group.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column ds_import_depr_group.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ds_import_depr_group.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ds_import_depr_group.depr_group_xlate is 'Field provided by the user on the import file used to translate depr group.';
comment on column ds_import_depr_group.depr_group_id is 'System-assigned identifier of the depr group.';
comment on column ds_import_depr_group.company_xlate is 'Field provided by the user on the import file used to translate company id.';
comment on column ds_import_depr_group.company_id is 'System-assigned identifier of a company.';
comment on column ds_import_depr_group.subledger_type_id is 'System-assigned identifier of a subledger type.  Automatically set to be 0.';
comment on column ds_import_depr_group.mid_period_conv is 'System-assigned identifier of a mid period convention.  Automatically set to be 0.5.';
comment on column ds_import_depr_group.mid_period_method is 'System-assigned identifier of a mid period method.  Automatically set to be "Monthly".';
comment on column ds_import_depr_group.description is 'Description of the depr group being imported.';
comment on column ds_import_depr_group.external_depr_code is 'External depr code of the depr group being imported.';
comment on column ds_import_depr_group.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column ds_import_depr_group.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';

alter table ds_import_depr_group
  add constraint ds_import_depr_group_pk primary key (IMPORT_RUN_ID, LINE_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table ds_import_depr_group
  add constraint ds_import_depr_group_run_fk foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);


----ds_import_depr_group_arc
create table ds_import_depr_group_arc
(
  import_run_id             NUMBER(22,0) not null,
  line_id                   NUMBER(22,0) not null,
  time_stamp                DATE,
  user_id                   VARCHAR2(18),
  depr_group_xlate          VARCHAR2(254),
  depr_group_id             NUMBER(22,0),
  company_xlate             VARCHAR2(254),
  company_id                NUMBER(22,0),
  subledger_type_id         NUMBER(22,0),
  mid_period_conv           NUMBER(22,0),
  mid_period_method         VARCHAR2(254),
  description               VARCHAR2(254),
  external_depr_code        VARCHAR2(254),
  is_modified               NUMBER(22,0)
);

comment on table ds_import_depr_group_arc is '(O and C) [05] The DS Import Depr Group Archive table is where run data of the DS Import Depr Group process is archived.';
comment on column ds_import_depr_group_arc.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column ds_import_depr_group_arc.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column ds_import_depr_group_arc.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ds_import_depr_group_arc.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ds_import_depr_group_arc.depr_group_xlate is 'Field provided by the user on the import file used to translate depr group.';
comment on column ds_import_depr_group_arc.depr_group_id is 'System-assigned identifier of the depr group.';
comment on column ds_import_depr_group_arc.company_xlate is 'Field provided by the user on the import file used to translate company id.';
comment on column ds_import_depr_group_arc.company_id is 'System-assigned identifier of a company.';
comment on column ds_import_depr_group_arc.subledger_type_id is 'System-assigned identifier of a subledger type.  Automatically set to be 0.';
comment on column ds_import_depr_group_arc.mid_period_conv is 'System-assigned identifier of a mid period convention.  Automatically set to be 0.5.';
comment on column ds_import_depr_group_arc.mid_period_method is 'System-assigned identifier of a mid period method.  Automatically set to be "Monthly".';
comment on column ds_import_depr_group_arc.description is 'Description of the depr group being imported.';
comment on column ds_import_depr_group_arc.external_depr_code is 'External depr code of the depr group being imported.';
comment on column ds_import_depr_group_arc.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';

alter table ds_import_depr_group_arc
  add constraint ds_import_depr_grp_arc_arc_pk primary key (IMPORT_RUN_ID, LINE_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table ds_import_depr_group_arc
  add constraint ds_import_depr_grp_arc_run_fk foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2162, 0, 2015, 1, 0, 0, 41990, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041990_ds_import_ddl_1.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;