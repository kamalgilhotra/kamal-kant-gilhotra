/*
||=============================================================================
|| Application: PowerPlant
|| File Name:   maint_044379_cr_pp_integration_2_ddl.sql
||=============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- -------------------------------------
|| 2015.2     08/11/2015 Kevin Dettbarn   Created tables for PP Integration
||                                        Import / Export tool.
||=============================================================================
*/

SET SERVEROUTPUT ON

CREATE TABLE pp_int_xml_export (
  batch_name         VARCHAR2(35)  NOT NULL,
  file_path          VARCHAR2(254) NULL,
  file_name          VARCHAR2(254) NULL,
  "SQL"                CLOB          NULL,
  rootNode           VARCHAR2(35)  NULL,
  definitionNode     VARCHAR2(35)  NULL,
  time_stamp         DATE          NULL,
  user_id            VARCHAR2(18)  NULL
);

ALTER TABLE pp_int_xml_export
  ADD CONSTRAINT pp_int_xml_export PRIMARY KEY (
    batch_name
  ) using index tablespace PWRPLANT_IDX
;

CREATE TABLE pp_int_xml_export_col (
  batch_name   VARCHAR2(35) NOT NULL,
  column_name  VARCHAR2(35) NOT NULL,
  column_order NUMBER(22,0) NOT NULL,
  column_type  VARCHAR2(12) NOT NULL,
  time_stamp   DATE         NULL,
  user_id      VARCHAR2(18) NULL
);

ALTER TABLE pp_int_xml_export_col
  ADD CONSTRAINT pp_int_xml_export_col_pk PRIMARY KEY (
    batch_name,
    column_name
  ) using index tablespace PWRPLANT_IDX;

ALTER TABLE pp_int_xml_export_col
  ADD CONSTRAINT pp_int_xml_export_col FOREIGN KEY (
    batch_name
  ) REFERENCES pp_int_xml_export (
    batch_name
  );

begin
   execute immediate 
'drop table pp_int_xml_import_col';
  exception
   when others then
   DBMS_OUTPUT.PUT_LINE('Table pp_int_xml_import_col does not exist or drop table priv not granted.');
end;
/

begin
   execute immediate 
'drop table pp_int_xml_import';
  exception
   when others then
   DBMS_OUTPUT.PUT_LINE('Table pp_int_xml_import does not exist or drop table priv not granted.');
end;
/

CREATE TABLE pp_int_xml_import (
  batch_name        VARCHAR2(35)  NOT NULL,
  file_path         VARCHAR2(254) NULL,
  file_name         VARCHAR2(254) NULL,
  table_name        VARCHAR2(35)  NULL,
  transaction_tag   VARCHAR2(35)  NULL,
  archive_file_path VARCHAR2(254) NULL,
  archive_file_name VARCHAR2(254) NULL,
  time_stamp        DATE          NULL,
  user_id           VARCHAR2(18)  NULL
);

ALTER TABLE pp_int_xml_import
  ADD CONSTRAINT pp_int_xml_import PRIMARY KEY (
    batch_name
  ) using index tablespace PWRPLANT_IDX
;

CREATE TABLE pp_int_xml_import_col (
  batch_name    VARCHAR2(35) NOT NULL,
  column_name   VARCHAR2(35) NOT NULL,
  xml_tag_name  VARCHAR2(35) NOT NULL,
  column_order  NUMBER(22,0) NOT NULL,
  time_stamp    DATE         NULL,
  user_id       VARCHAR2(18) NULL
);

ALTER TABLE pp_int_xml_import_col
  ADD CONSTRAINT pp_int_xml_import_col PRIMARY KEY (
    batch_name,
    column_name
  ) using index tablespace PWRPLANT_IDX;

ALTER TABLE pp_int_xml_import_col
  ADD CONSTRAINT pp_int_xml_import_col_fk FOREIGN KEY (
    batch_name
  ) REFERENCES pp_int_xml_import (
    batch_name
  );

comment on table pp_int_xml_export is 'Table for XML export in PP Integration';
comment on column pp_int_xml_export.batch_name is 'Name of batch in pp_integration';
comment on column pp_int_xml_export.file_path is 'Location to export file';
comment on column pp_int_xml_export.file_name is 'Name of export file';
comment on column pp_int_xml_export.sql is 'SQL to customize data being exported';
comment on column pp_int_xml_export.rootnode is 'Root node for XML export file';
comment on column pp_int_xml_export.definitionnode is 'Definition node for XML export file';

comment on table pp_int_xml_export_col is 'Columns to be exported in PP Integration';
comment on column pp_int_xml_export_col.batch_name is 'Name of batch in pp_integration';
comment on column pp_int_xml_export_col.column_name is 'Column name';
comment on column pp_int_xml_export_col.column_order is 'Position of column in table';
comment on column pp_int_xml_export_col.column_type is 'Data type of field';

comment on table pp_int_xml_import is 'Table for XML import in PP Integration';
comment on column pp_int_xml_import.batch_name is 'Name of batch in pp_integration';
comment on column pp_int_xml_import.file_path is 'Location of XML file to import';
comment on column pp_int_xml_import.file_name is 'Name of file to import';
comment on column pp_int_xml_import.table_name is 'Staging table';
comment on column pp_int_xml_import.transaction_tag is 'Transaction tag';
comment on column pp_int_xml_import.archive_file_path is 'Location of archive file';
comment on column pp_int_xml_import.archive_file_name is 'Name of archive file';

comment on table pp_int_xml_import_col is 'Columns being imported for XML files in PP Integration';
comment on column pp_int_xml_import_col.batch_name is 'Name of batch in pp_integration';
comment on column pp_int_xml_import_col.column_name is 'Name of column';
comment on column pp_int_xml_import_col.xml_tag_name is 'Name of column in XML tag';
comment on column pp_int_xml_import_col.column_order is 'Position of column in table';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2768, 0, 2015, 2, 0, 0, 044379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044379_cr_pp_integration_2_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;