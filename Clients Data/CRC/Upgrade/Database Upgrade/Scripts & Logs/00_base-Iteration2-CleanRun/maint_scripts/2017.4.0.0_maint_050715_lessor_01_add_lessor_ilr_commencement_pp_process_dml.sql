/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050715_lessor_01_add_lessor_ilr_commencement_pp_process_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/18/2018 Jared Watkins    Add Lessor - ILR Commencement Process to PP Processes
||============================================================================
*/

MERGE INTO pp_processes a
USING (SELECT max_id + ROWNUM AS process_id, DESCRIPTION, long_description
      FROM (SELECT 'Lessor - ILR Commencement' AS DESCRIPTION, 'Process to send Commencement JEs for a Lessor ILR' AS long_description
            FROM dual)
      CROSS JOIN (SELECT MAX(process_id) AS max_id
                  FROM pp_processes)) b ON (UPPER(TRIM(A.DESCRIPTION)) = UPPER(TRIM(b.DESCRIPTION)))
WHEN MATCHED THEN
  UPDATE SET A.long_description = b.long_description
  where decode(a.long_description, b.long_description, 1, 0) = 0
WHEN NOT MATCHED THEN
  INSERT(A.process_id, A.DESCRIPTION, A.long_description)
  values(b.process_id, b.description, b.long_description);
  
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(5702, 0, 2017, 4, 0, 0, 50715, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050715_lessor_01_add_lessor_ilr_commencement_pp_process_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;