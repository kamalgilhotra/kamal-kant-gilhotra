/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046876_pwrtax_map_menu_to_new_ws_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 01/27/2017 Charlie Shilling Modify menu structure to point to new base gui workspaces instead of old windows.
||============================================================================
*/
UPDATE ppbase_workspace
SET workspace_uo_name = 'uo_tax_rate',
	object_type_id = 1
WHERE workspace_identifier = 'depr_table_rates'
;

UPDATE ppbase_workspace
SET workspace_uo_name = 'uo_tax_convention',
	object_type_id = 1
WHERE workspace_identifier = 'depr_table_conventions'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3362, 0, 2017, 1, 0, 0, 46876, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_046876_pwrtax_map_menu_to_new_ws_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;