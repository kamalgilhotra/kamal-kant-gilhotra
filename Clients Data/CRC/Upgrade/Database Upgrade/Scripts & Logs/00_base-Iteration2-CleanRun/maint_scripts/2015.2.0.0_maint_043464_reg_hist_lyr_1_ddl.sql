/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043464_reg_hist_lyr_1_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/03/2015 Andrew Scott   Tables creates for reg hist layer 
||                                      book depr calc (modified from IFA)
||============================================================================
*/


CREATE TABLE INCREMENTAL_PROCESS 
(
   INCREMENTAL_PROCESS_ID NUMBER(22,0) NOT NULL,
   USER_ID VARCHAR2(18),
   TIME_STAMP DATE,
   FUNDING_WO_ID NUMBER(22,0),
   REVISION NUMBER(22,0),
   HIST_LAYER_ID NUMBER(22,0)
);

ALTER TABLE INCREMENTAL_PROCESS 
ADD CONSTRAINT INCREMENTAL_PROCESS_PK
PRIMARY KEY (INCREMENTAL_PROCESS_ID)
USING INDEX TABLESPACE PWRPLANT_IDX;

ALTER TABLE INCREMENTAL_PROCESS
ADD CONSTRAINT FUNDING_PROJECT_ID_FK
FOREIGN KEY (FUNDING_WO_ID)
REFERENCES WORK_ORDER_CONTROL (WORK_ORDER_ID);

ALTER TABLE INCREMENTAL_PROCESS
ADD CONSTRAINT HIST_LAYER_ID_FK
FOREIGN KEY (HIST_LAYER_ID)
REFERENCES INCREMENTAL_HIST_LAYER;

COMMENT ON TABLE INCREMENTAL_PROCESS IS 'The Incremental Process table is used to indicate whether the Incremental Forecast Depreciation and Incremental PowerTax calculations are performed for Historic Layers or Forecast Layers (FP + Revision).';

COMMENT ON COLUMN INCREMENTAL_PROCESS.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN INCREMENTAL_PROCESS.USER_ID IS 'Standard system-assigned user id used for audit purposes.' ;
COMMENT ON COLUMN INCREMENTAL_PROCESS.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.' ;
COMMENT ON COLUMN INCREMENTAL_PROCESS.FUNDING_WO_ID IS 'System assigned identifier for a funding project.';
COMMENT ON COLUMN INCREMENTAL_PROCESS.REVISION IS 'The revision of the funding project being forecasted.';
COMMENT ON COLUMN INCREMENTAL_PROCESS.HIST_LAYER_ID IS 'System assigned identifier for a historic layer.';




CREATE TABLE INCREMENTAL_PROCESS_RUN_LOG 
(
   INCREMENTAL_RUN_ID NUMBER(22,0) NOT NULL,
   USER_ID VARCHAR2(18),
   TIME_STAMP DATE,
   INCREMENTAL_PROCESS_ID NUMBER(22,0),
   BUDGET_VERSION_ID NUMBER(22,0),
   FCST_DEPR_VERSION_ID NUMBER(22,0),
   VERSION_ID NUMBER(22,0),
   DATE_RUN DATE,
   RUN_USER_ID VARCHAR2(18)
);

ALTER TABLE INCREMENTAL_PROCESS_RUN_LOG 
ADD CONSTRAINT INCREMENTAL_PROCESS_RUN_LOG_PK
PRIMARY KEY (INCREMENTAL_RUN_ID)
USING INDEX TABLESPACE PWRPLANT_IDX;

ALTER TABLE INCREMENTAL_PROCESS_RUN_LOG
ADD CONSTRAINT INCREMENTAL_PROCESS_ID_FK
FOREIGN KEY (INCREMENTAL_PROCESS_ID)
REFERENCES INCREMENTAL_PROCESS;

ALTER TABLE INCREMENTAL_PROCESS_RUN_LOG
ADD CONSTRAINT BUDGET_VERSION_ID_FK
FOREIGN KEY (BUDGET_VERSION_ID)
REFERENCES BUDGET_VERSION_CONTROL;

ALTER TABLE INCREMENTAL_PROCESS_RUN_LOG
ADD CONSTRAINT FCST_DEPR_VERSION_ID_FK
FOREIGN KEY (FCST_DEPR_VERSION_ID)
REFERENCES FCST_DEPR_VERSION;

ALTER TABLE INCREMENTAL_PROCESS_RUN_LOG
ADD CONSTRAINT VERSION_ID_FK
FOREIGN KEY (VERSION_ID)
REFERENCES VERSION;

COMMENT ON TABLE INCREMENTAL_PROCESS_RUN_LOG IS 'The Incremental Process table is used to indicate whether the Incremental Forecast Depreciation and Incremental PowerTax calculations are performed for Historic Layers or Forecast Layers (FP + Revision).';

COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.INCREMENTAL_RUN_ID IS 'System assigned identifier for an incremental run.';
COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.USER_ID IS 'Standard system-assigned user id used for audit purposes.' ;
COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.' ;
COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.BUDGET_VERSION_ID IS 'System assigned identifier for a budget version.';
COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.FCST_DEPR_VERSION_ID IS 'System assigned identifier for a forecast depreciation version.';
COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.VERSION_ID IS 'System assigned identifier for a tax case.';
COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.DATE_RUN IS 'Date and time when the process was completed.';
COMMENT ON COLUMN INCREMENTAL_PROCESS_RUN_LOG.RUN_USER_ID IS 'The user id that initiated the run.';


----add incremental process id columns to 
----FCST_DEPR_LEDGER_FP
----FCST_CPR_DEPR_FP
----INCREMENTAL_FP_RUN_LOG
alter table FCST_DEPR_LEDGER_FP add  INCREMENTAL_PROCESS_ID NUMBER(22,0);
alter table FCST_CPR_DEPR_FP add  INCREMENTAL_PROCESS_ID NUMBER(22,0);
alter table INCREMENTAL_FP_RUN_LOG add  INCREMENTAL_PROCESS_ID NUMBER(22,0);
----next dml script will populate the 2 newly created tables + 
----backfill incremental process id to the 3 fp tables

----3rd ddl script will rename the 3 fp tables and rebuild the pkeys

COMMENT ON COLUMN FCST_DEPR_LEDGER_FP.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN FCST_CPR_DEPR_FP.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2582, 0, 2015, 2, 0, 0, 043464, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043464_reg_hist_lyr_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;