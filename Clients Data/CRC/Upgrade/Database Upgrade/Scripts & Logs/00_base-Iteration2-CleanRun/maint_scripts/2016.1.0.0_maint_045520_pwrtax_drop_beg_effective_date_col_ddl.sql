/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045520_pwrtax_drop_beg_effective_date_col_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 09/05/2016 Charlie Shilling maint-45520 - drop beg_effective_date column from tax_plant_recon_form_rates table
||============================================================================
*/
ALTER TABLE tax_plant_recon_form_rates
DROP CONSTRAINT tax_plant_recon_form_rates_fk3;

ALTER TABLE tax_plant_recon_form_rates
DROP COLUMN beg_effective_date;

ALTER TABLE tax_plant_recon_form_rates
RENAME COLUMN end_effective_date TO effective_date;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3102, 0, 2016, 1, 0, 0, 045520, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045520_pwrtax_drop_beg_effective_date_col_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;