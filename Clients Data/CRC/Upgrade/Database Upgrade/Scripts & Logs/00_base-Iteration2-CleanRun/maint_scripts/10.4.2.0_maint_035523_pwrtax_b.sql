/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035523_pwrtax_b.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/04/2014 Julia Breuer   Point Release
||============================================================================
*/

--
-- Update the filter for company consolidation.
--
update pwrplant.pp_reports_filter set filter_uo_name = 'uo_tax_selecttabs_rpts_rollup_cons' where pp_report_filter_id = 53;

--
-- Correct the filter for tax rollups.
--
update pwrplant.pp_dynamic_filter set sqls_column_expression = 'tax_record_control.tax_class_id in (select distinct tax_class_id from tax_class_rollups where tax_rollup_detail_id' where filter_id = 110;

--
-- Create additional reports.
--
insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw ) values (  403008, sysdate, user, 'Rollforward Report', 'Depreciation Rollforward Report', '', 'dw_tax_rpt_roll_forward', '', '', '', 'PwrTax - 30', '', '', '', 1, 130, 50, 53, 1, 3, '', '', null, '', '', 0 );
insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw ) values (  403010, sysdate, user, 'Gain Loss Report', 'Displays the detailed gain/loss calculation showing adjusted basis, salvage, extraordinary and ordinary dispositions.', '', 'dw_tax_rpt_gain_loss', '', '', '', 'PwrTax - 50', '', '', '', 1, 130, 50, 53, 1, 3, '', '', null, '', '', 0 );
insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw ) values (  403012, sysdate, user, 'Gain Loss Analysis Report', 'Analyzes the gain/loss calculation.', '', 'dw_tax_rpt_gain_loss_analysis', '', '', '', 'PwrTax - 52', '', '', '', 1, 130, 50, 53, 1, 3, '', '', null, '', '', 0 );
insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw ) values (  403001, sysdate, user, '4562 by Company', 'Individual company tax depreciation summarized in the format of form 4562 of the federal income tax return.  Company and tax year are the only effective selections on this report.  The Form 4562 is generated for the entire company for the tax year - sele', 'PowerTax', 'dw_tax_rpt_4562', '', '', '', 'PwrTax - 1', '', '', '', 1, 130, 50, 53, 1, 3, '', '', null, '', '', 0 );

--
-- Enable all PowerTax menu items.
--
update pwrplant.ppbase_menu_items set enable_yn = 1 where module = 'powertax';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (941, 0, 10, 4, 2, 0, 35523, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035523_pwrtax_b.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;