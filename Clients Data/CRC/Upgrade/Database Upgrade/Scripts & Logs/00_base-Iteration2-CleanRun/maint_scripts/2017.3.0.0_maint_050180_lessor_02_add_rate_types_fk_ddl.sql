/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050180_lessor_02_add_rate_types_fk_ddl.sql
|| Description:	Add new rate types to rate type table
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/2/2018 Jared Watkins  Add missing FK from lsr_ilr_rates to lsr_ilr_approval
||============================================================================
*/

alter table lsr_ilr_rates 
add constraint lsr_ilr_rates_ilr_rev_fk 
foreign key (ilr_id, revision) 
references lsr_ilr_options (ilr_id, revision);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4167, 0, 2017, 3, 0, 0, 50180, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050180_lessor_02_add_rate_types_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
