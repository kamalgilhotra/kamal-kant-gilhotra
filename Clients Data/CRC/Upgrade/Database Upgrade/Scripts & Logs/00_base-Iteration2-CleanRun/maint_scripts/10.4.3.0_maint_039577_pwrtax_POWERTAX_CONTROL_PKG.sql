/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039577_pwrtax_POWERTAX_CONTROL_PKG.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/14/2014 Alex P.
||============================================================================
*/

create or replace package POWERTAX_CONTROL_PKG as
--||============================================================================
--|| Application: PowerPlant
--|| Object Name: POWERTAX_CONTROL_PKG
--|| Description: Modifies and retrieves PowerTax information for the current database
--||              Also used to hold general tax utilities.
--||============================================================================
--|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| PP Version  Version    Date           Revised By     Reason for Change
--|| ----------  ---------  -------------  -------------  -------------------------
--|| 10.4.2.0    1.00       01/17/2014     Julia Breuer   Initial Creation
--|| 10.4.3.0    1.01       05/29/2014     Andrew Scott   Added function for case management
--||                                                      wksp to find the earliest range of
--||                                                      trids to use for the new tax records
--|| 10.4.3.0    1.02       08/14/2014     Alex P.        Performance: Use Powerplant Context
--||                                                      to keep track of TAX VERSION ID
--||============================================================================

   -- Sets the global version_id variable
   procedure P_SET_VERSION_ID(A_VERSION_ID in VERSION.VERSION_ID%type);

   -- For Copied Cases and Adding Vintages, finds a range of tax record ids to use
   -- and applies it to the renumbering table.
   function F_RENUMBER_TRIDS return number;

end POWERTAX_CONTROL_PKG;
/

create or replace package body POWERTAX_CONTROL_PKG as

   G_TAX_VERSION_ID VERSION.VERSION_ID%type;

   --**************************************************************************************
   -- p_set_version_id
   --**************************************************************************************
   procedure P_SET_VERSION_ID(A_VERSION_ID in VERSION.VERSION_ID%type) is
     
     RTN_CODE   number(22, 0);
   
   begin
      G_TAX_VERSION_ID := A_VERSION_ID;
      RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('G_TAX_VERSION_ID', NVL(A_VERSION_ID, 0));
   end P_SET_VERSION_ID;

   --**************************************************************************************
   -- f_renumber_trids
   --**************************************************************************************
   function F_RENUMBER_TRIDS return number is

      L_VERSION_COUNT pls_integer := 0;
      L_VERSION_KEY   pls_integer := 0;
      CUR_ITER        pls_integer := 0;
      CUR_TRID        pls_integer;
      PRIOR_TRID      pls_integer;

      cursor L_TRID_CUR is
         select TAX_RECORD_ID from TAX_RECORD_CONTROL order by TAX_RECORD_ID;

      L_TRID_REC L_TRID_CUR%rowtype;
      RTN_CODE   number(22, 0);

   begin

      select count(*) into L_VERSION_COUNT from TEMP_RENUMBER_TRIDS;

      if L_VERSION_COUNT is null or L_VERSION_COUNT = 0 then
         ----this function should only be called immediately after the refreshing of contents of
         RAISE_APPLICATION_ERROR(-20001, 'Unable to retrieve the number of rows in temp_renumber_trids. ');
         return - 1;
      end if;

      open L_TRID_CUR;

      loop
         fetch L_TRID_CUR
            into L_TRID_REC;
         exit when L_TRID_CUR%notfound;

         CUR_ITER := CUR_ITER + 1;

         if CUR_ITER is null or CUR_ITER = 0 then
            ----this function should only be called immediately after the refreshing of contents of
            RAISE_APPLICATION_ERROR(-20002, 'Cursor iteration variable not correctly initiated');
            return - 1;
         elsif CUR_ITER = 1 then
            ----grab the trid for loop 2 to use as the prior trid
            CUR_TRID := L_TRID_REC.TAX_RECORD_ID;
            ----skip the first row
            CONTINUE;
         else
            ----save off the prior loop iteration trid
            PRIOR_TRID := CUR_TRID;
         end if;

         CUR_TRID := L_TRID_REC.TAX_RECORD_ID;

         if CUR_TRID - PRIOR_TRID > L_VERSION_COUNT then
            ----this means that a range has been found to renumber
            ----the trids.  So set the version key and exit the loop.
            L_VERSION_KEY := PRIOR_TRID;
            exit;
         end if;

      end loop;

      close L_TRID_CUR;

      if L_VERSION_KEY = 0 then
         ----this means that there was no available range found.  so just
         ----use the max iteration as the version key (update statement will do
         ----rownum + version key, so this should not error.
         L_VERSION_KEY := CUR_TRID;
      end if;

      ----one last check to make sure a version key was found.
      if L_VERSION_KEY is null or L_VERSION_KEY = 0 then
         RAISE_APPLICATION_ERROR(-20003, 'Failed to find the version key to use for the update.');
         return - 1;
      end if;

      RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      execute immediate 'update temp_renumber_trids set rnum = rnum + ' || L_VERSION_KEY;

      return 0;

   exception
      when others then
         RAISE_APPLICATION_ERROR(-20000, 'Failed Renumber Tax Record IDs. Error: ' || sqlerrm);
         return - 1;

   end F_RENUMBER_TRIDS;

end POWERTAX_CONTROL_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1355, 0, 10, 4, 3, 0, 39577, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039577_pwrtax_POWERTAX_CONTROL_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
