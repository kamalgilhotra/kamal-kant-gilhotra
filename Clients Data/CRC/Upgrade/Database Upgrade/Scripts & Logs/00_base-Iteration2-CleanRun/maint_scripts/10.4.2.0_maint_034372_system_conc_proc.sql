/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034372_system_conc_proc.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/29/2014 Stephen Motter
||============================================================================
*/

create table PP_PROCESSES_RUNNING
(
 SID                 number(22) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 PROCESS_NAME_STRING clob
);

alter table PP_PROCESSES_RUNNING
   add constraint PK_PP_PROCESSES_RUNNING
       primary key (SID)
       using index tablespace PWRPLANT_IDX;

comment on table PP_PROCESSES_RUNNING is '(S) [10] This table holds a list of the processes running in each session for concurrency locks.';

comment on column PP_PROCESSES_RUNNING.SID is 'The session id.';
comment on column PP_PROCESSES_RUNNING.TIME_STAMP is 'System-assigned time stamp.';
comment on column PP_PROCESSES_RUNNING.USER_ID is 'System-assigned user id.';
comment on column PP_PROCESSES_RUNNING.PROCESS_NAME_STRING is 'Semicolon delimited list of locked processes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (926, 0, 10, 4, 2, 0, 34372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034372_system_conc_proc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;