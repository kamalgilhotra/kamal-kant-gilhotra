/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047261_lease_add_formula_run_order_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 03/09/2017 Jared Watkins    Add the column used to track dependencies between variable payments assigned to an ILR
||============================================================================
*/
ALTER TABLE ls_ilr_payment_term_var_paymnt
ADD run_order number(3);

comment on column ls_ilr_payment_term_var_paymnt.run_order is 'A value denoting the order in which variable payments associated to the given ILR should be evaluated.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3384, 0, 2017, 1, 0, 0, 47261, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047261_lease_add_formula_run_order_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;