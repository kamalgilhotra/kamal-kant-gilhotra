SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033030_projects_cu_fkeys.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/24/2013 Stephen Motter
||============================================================================
*/

declare
   FK_EXISTS exception;
   pragma exception_init(FK_EXISTS, -02275);

begin
   begin
      execute immediate 'alter table COMPATIBLE_UNIT
                         add constraint FK_CU_STATUS_CODE
                             foreign key(STATUS_CODE_ID)
                             references STATUS_CODE(STATUS_CODE_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CU_STATUS_CODE already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CU_STATUS_CODE.');
   end;
   begin
      execute immediate 'alter table COMPATIBLE_UNIT
                            add constraint FK_CU_UOM_ID
                                foreign key(UNIT_OF_MEASURE_ID)
                                references UNIT_OF_MEASURE(UNIT_OF_MEASURE_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CU_UOM_ID already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CU_UOM_ID.');
   end;
   begin
      execute immediate 'alter table COMPATIBLE_UNIT
                         add constraint FK_CU_CREW_TYPE
                             foreign key(CREW_TYPE_ID)
                             references CREW_TYPE(CREW_TYPE_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CU_CREW_TYPE already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CU_CREW_TYPE.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_HIERARCHY
                         add constraint FK_CUH_COMP_UNIT
                             foreign key(COMP_UNIT_ID)
                             references COMPATIBLE_UNIT(COMP_UNIT_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUH_COMP_UNIT already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUH_COMP_UNIT.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_HIERARCHY
                            add constraint FK_CUH_COMP_UNIT_PARENT
                                foreign key(PARENT_COMP_UNIT_ID)
                                references COMPATIBLE_UNIT(COMP_UNIT_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUH_COMP_UNIT_PARENT already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUH_COMP_UNIT_PARENT.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_ACTION_CODE_STD
                            add constraint FK_CUAC_COMP_UNIT
                                foreign key(COMP_UNIT_ID)
                                references COMPATIBLE_UNIT(COMP_UNIT_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUAC_COMP_UNIT already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUAC_COMP_UNIT.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_ACTION_CODE_STD
                            add constraint FK_CUAC_WORK_TYPE
                                foreign key(WORK_TYPE_ID)
                                references WORK_TYPE(WORK_TYPE_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUAC_WORK_TYPE already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUAC_WORK_TYPE.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_WORK_TYPE
                            add constraint FK_CUWT_COMP_UNIT
                                foreign key(COMP_UNIT_ID)
                                references COMPATIBLE_UNIT(COMP_UNIT_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUWT_COMP_UNIT already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUWT_COMP_UNIT.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_WORK_TYPE
                            add constraint FK_CUWS_WORK_TYPE
                                foreign key(WORK_TYPE_ID)
                                references WORK_TYPE(WORK_TYPE_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUWS_WORK_TYPE already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUWS_WORK_TYPE.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_WORK_TYPE
                            add constraint FK_CUWS_BUS_SEGMENT
                                foreign key(BUS_SEGMENT_ID)
                                references BUSINESS_SEGMENT(BUS_SEGMENT_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUWS_BUS_SEGMENT already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUWS_BUS_SEGMENT.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_WORK_TYPE
                            add constraint FK_CUWS_UTILITY_ACCOUNT
                                foreign key(UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID)
                                references UTILITY_ACCOUNT(UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUWS_UTILITY_ACCOUNT already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUWS_UTILITY_ACCOUNT.');
   end;
   begin
      execute immediate 'alter table COMP_UNIT_WORK_TYPE
                            add constraint FK_CUWS_SUB_ACCOUNT
                                foreign key(BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID)
                                references SUB_ACCOUNT(BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID)';
   exception
      when FK_EXISTS then
         DBMS_OUTPUT.PUT_LINE('The foreign key FK_CUWS_SUB_ACCOUNT already exists.');
      when others then
         DBMS_OUTPUT.PUT_LINE('Unhandled ORA error occured adding foreign key FK_CUWS_SUB_ACCOUNT.');
   end;

end;
/


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (705, 0, 10, 4, 2, 0, 33030, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033030_projects_cu_fkeys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
