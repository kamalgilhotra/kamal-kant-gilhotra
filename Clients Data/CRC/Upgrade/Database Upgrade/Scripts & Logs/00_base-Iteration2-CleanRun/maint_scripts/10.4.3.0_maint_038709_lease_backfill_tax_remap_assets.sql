/*
||==============================================================================================
|| Application: PowerPlant
|| File Name:   maint_038709_lease_backfill_tax_remap_assets.sql
||==============================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==============================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------------
|| 10.4.3.0 07/08/2014 Daniel Motter        Creation
|| 10.4.3.0 07/11/2014 Daniel Motter        Added support for multiple MLA revisions to backfill
||==============================================================================================
*/

/* Defaults the tax summary on all assets that are assigned to an MLA (via an ILR). */
update LS_ASSET A
   set TAX_SUMMARY_ID =
        (select LO.TAX_SUMMARY_ID
           from LS_LEASE_OPTIONS LO, LS_ILR I, LS_LEASE L
          where A.ILR_ID = I.ILR_ID
            and I.LEASE_ID = L.LEASE_ID
            and L.LEASE_ID = LO.LEASE_ID
            and L.CURRENT_REVISION = LO.REVISION
            and LO.TAX_SUMMARY_ID <> 0
            and LO.TAX_SUMMARY_ID is not null)
 where A.ILR_ID is not null;


/* Build the mapping between the assets and taxes. */
declare
   LS_ASSETS            PKG_LEASE_CALC.NUM_ARRAY;
   DISABLED_LOCAL_TAXES PKG_LEASE_CALC.NUM_ARRAY;
begin
   select LS_ASSET_ID bulk collect
     into LS_ASSETS
     from LS_ASSET A, LS_ILR I, LS_LEASE_OPTIONS LO
    where A.ILR_ID = I.ILR_ID
      and I.LEASE_ID = LO.LEASE_ID
      and LO.TAX_SUMMARY_ID <> 0;

   /* This is logic from PKG_LEASE_ASSET_POST.P_GETTAXES
   It needs to be called like this instead of using the package because the
   package will not be updated when this script is run. */
   for I in 1 .. LS_ASSETS.COUNT
   loop
      --create an array of local taxes that are disabled under this asset
      select TAX_LOCAL_ID bulk collect
        into DISABLED_LOCAL_TAXES
        from LS_ASSET_TAX_MAP
       where STATUS_CODE_ID = 0
         and LS_ASSET_ID = LS_ASSETS(I);

      --delete old records from ls_asset_tax_map
      delete from LS_ASSET_TAX_MAP where LS_ASSET_ID = LS_ASSETS(I);

      --insert new records into ls_asset_tax_map
      insert into LS_ASSET_TAX_MAP
         (LS_ASSET_ID, TAX_LOCAL_ID, STATUS_CODE_ID)
         (select LS_ASSETS(I) LS_ASSET_ID, TL.TAX_LOCAL_ID TAX_LOCAL_ID, 1 STATUS_CODE_ID
            from LS_ASSET A, LS_TAX_LOCAL TL
           where LS_ASSETS(I) = A.LS_ASSET_ID
             and A.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID);

      --the insert statement defaults all local taxes to enabled, so disable the ones that need to be
      for J in 1 .. DISABLED_LOCAL_TAXES.COUNT
      loop
         update LS_ASSET_TAX_MAP
            set STATUS_CODE_ID = 0
          where LS_ASSET_ID = LS_ASSETS(I)
            and TAX_LOCAL_ID = DISABLED_LOCAL_TAXES(J);
      end loop;

   end loop;
   /* End logic from PKG_LEASE_ASSET_POST.P_GETTAXES */

end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1256, 0, 10, 4, 3, 0, 38709, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038709_lease_backfill_tax_remap_assets.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
