/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_041179_jobserver_inc_20141209_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/03/2014 Paul Cordero    	 Updating executable descriptions to match PP table
||==========================================================================================
*/

UPDATE pp_job_executable 
 SET EXECUTABLE = 'ssp_budget.exe AFUDC'
WHERE JOB_TYPE = 'SSP'
  AND JOB_NAME = 'Budget AFUDC'
  AND EXECUTABLE = 'ssp_budget.exe';
  
-- UPDATE pp_job_executable 
-- SET EXECUTABLE = 'ssp_budget.exe XXXXX'
--WHERE JOB_TYPE = 'SSP'
--  AND JOB_NAME = 'Budget Automatic Processing'
--  AND EXECUTABLE = 'ssp_budget.exe';

UPDATE pp_job_executable 
 SET EXECUTABLE = 'ssp_budget.exe Esc'
WHERE JOB_TYPE = 'SSP'
  AND JOB_NAME = 'Budget Escalations'
  AND EXECUTABLE = 'ssp_budget.exe';

UPDATE pp_job_executable 
 SET EXECUTABLE = 'ssp_budget.exe OH'
WHERE JOB_TYPE = 'SSP'
  AND JOB_NAME = 'Budget Overheads'
  AND EXECUTABLE = 'ssp_budget.exe';

UPDATE pp_job_executable 
 SET EXECUTABLE = 'ssp_budget.exe To CR'
WHERE JOB_TYPE = 'SSP'
  AND JOB_NAME = 'Budget To CR'
  AND EXECUTABLE = 'ssp_budget.exe';

UPDATE pp_job_executable 
 SET EXECUTABLE = 'ssp_budget.exe UWA'	
WHERE JOB_TYPE = 'SSP'
  AND JOB_NAME = 'Budget Update with Actuals'
  AND EXECUTABLE = 'ssp_budget.exe';

UPDATE pp_job_executable 
 SET EXECUTABLE = 'ssp_budget.exe WIP'
WHERE JOB_TYPE = 'SSP'
  AND JOB_NAME = 'Budget WIP Computations'
  AND EXECUTABLE = 'ssp_budget.exe';

--UPDATE pp_job_executable 
-- SET EXECUTABLE = 'ssp_budget.exe XXXXX'
--WHERE JOB_TYPE = 'SSP'
--  AND JOB_NAME = 'Incremental Forecast by FP'
--  AND EXECUTABLE = 'ssp_budget.exe';

UPDATE pp_job_executable 
 SET EXECUTABLE = 'ssp_budget.exe Load'
WHERE JOB_TYPE = 'SSP'
  AND JOB_NAME = 'Load Budget Dollars from FP'
  AND EXECUTABLE = 'ssp_budget.exe';

--UPDATE pp_job_executable 
-- SET EXECUTABLE = 'ssp_budget.exe XXXXX'
--WHERE JOB_TYPE = 'SSP'
--  AND JOB_NAME = 'Depreciation Forecast: Individual'
--  AND EXECUTABLE = 'SSP_budget.exe';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2346, 0, 2015, 1, 0, 0, 041179, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041179_jobserver_inc_20141209_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;