/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048364_lease_create_buckets_retire_new_term_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/09/2017 Anand R          PP-48364 Add contingent and executory buckets for lease retirement term table
||============================================================================
*/

ALTER TABLE LS_RETIREMENT_NEW_TERMS
ADD 
(
   C_BUCKET_1       number(22,2),
   C_BUCKET_2       number(22,2),
   C_BUCKET_3       number(22,2),
   C_BUCKET_4       number(22,2),
   C_BUCKET_5       number(22,2),
   C_BUCKET_6       number(22,2),
   C_BUCKET_7       number(22,2),
   C_BUCKET_8       number(22,2),
   C_BUCKET_9       number(22,2),
   C_BUCKET_10      number(22,2),
   E_BUCKET_1       number(22,2),
   E_BUCKET_2       number(22,2),
   E_BUCKET_3       number(22,2),
   E_BUCKET_4       number(22,2),
   E_BUCKET_5       number(22,2),
   E_BUCKET_6       number(22,2),
   E_BUCKET_7       number(22,2),
   E_BUCKET_8       number(22,2),
   E_BUCKET_9       number(22,2),
   E_BUCKET_10      number(22,2)
);   

comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_1 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_2 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_3 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_4 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_5 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_6 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_7 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_8 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_9 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.C_BUCKET_10 is 'Payment Amount for Contingent Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_1 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_2 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_3 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_4 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_5 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_6 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_7 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_8 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_9 is 'Payment Amount for Executory Bucket.';
comment on column LS_RETIREMENT_NEW_TERMS.E_BUCKET_10 is 'Payment Amount for Executory Bucket.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3635, 0, 2017, 1, 0, 0, 48364, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048364_lease_create_buckets_retire_new_term_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;