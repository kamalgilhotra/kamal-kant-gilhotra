/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030685_lease_status_new_revision.sql
|| Description: Adding a lease status of 'New Revision'
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/10/2013 Ryan Oliveria   Point Release
||============================================================================
*/

insert into LS_LEASE_STATUS
   (LEASE_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (7, sysdate, user, 'New Revision', 'New Revision');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (469, 0, 10, 4, 1, 0, 30685, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030685_lease_status_new_revision.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
