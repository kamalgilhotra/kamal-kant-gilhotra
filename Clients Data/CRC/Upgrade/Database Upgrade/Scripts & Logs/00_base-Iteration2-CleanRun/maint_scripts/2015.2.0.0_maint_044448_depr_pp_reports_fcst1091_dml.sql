/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044448_depr_pp_reports_fcst1091_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2     07/22/2015 Kevin Dettbarn   maint 44448
||============================================================================
*/

UPDATE pp_reports SET pp_report_filter_id = 10 WHERE Lower(trim(report_number)) = 'fcst - 1091';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2703, 0, 2015, 2, 0, 0, 044448, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044448_depr_pp_reports_fcst1091_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;