/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_039280_lease_import_invoices_2.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 10.4.3.0 08/11/2014 Daniel Motter  Re-add the LS_PAYMENT_TYPE table and
 ||                                    update PP_IMPORT_COLUMN appropriately
 ||============================================================================
 */

create table LS_PAYMENT_TYPE
(
 PAYMENT_TYPE_ID number(2,0),
 DESCRIPTION     varchar2(35),
 USER_ID         varchar2(18),
 TIME_STAMP      date
);

alter table LS_PAYMENT_TYPE
   add constraint PK_LS_PAYMENT_TYPE
       primary key (PAYMENT_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

insert into LS_PAYMENT_TYPE
   (PAYMENT_TYPE_ID, DESCRIPTION, USER_ID, TIME_STAMP)
values
   (1, 'Principal', 'PWRPLANT', sysdate);

insert into LS_PAYMENT_TYPE
   (PAYMENT_TYPE_ID, DESCRIPTION, USER_ID, TIME_STAMP)
values
   (2, 'Interest', 'PWRPLANT', sysdate);

insert into LS_PAYMENT_TYPE
   (PAYMENT_TYPE_ID, DESCRIPTION, USER_ID, TIME_STAMP)
values
   (3, 'Executory', 'PWRPLANT', sysdate);

insert into LS_PAYMENT_TYPE
   (PAYMENT_TYPE_ID, DESCRIPTION, USER_ID, TIME_STAMP)
values
   (13, 'Contingent', 'PWRPLANT', sysdate);

update PP_IMPORT_COLUMN
   set PARENT_TABLE = 'ls_payment_type',
       PARENT_TABLE_PK_COLUMN = 'payment_type_id'
 where COLUMN_NAME = 'payment_type_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1321, 0, 10, 4, 3, 0, 39280, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039280_lease_import_invoices_2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;