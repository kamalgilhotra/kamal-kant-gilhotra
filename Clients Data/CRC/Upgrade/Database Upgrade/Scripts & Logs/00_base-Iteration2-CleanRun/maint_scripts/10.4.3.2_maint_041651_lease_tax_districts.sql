/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Lessee
|| File Name:   maint_041651_lease_tax_districts.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3 01/02/2015 	B.Beck    	 		Allow multiple tax districts to be tied to an asset location
||==========================================================================================
*/

alter table asset_location
drop column ls_tax_district_id;

create table ls_location_tax_district
(
	asset_location_id number(22,0),
	tax_district_id number(22,0),
	time_stamp date,
	user_id varchar2(18),
	CONSTRAINT pk_ls_loc_tax_dist 
	PRIMARY KEY (asset_location_id, tax_district_id),
	CONSTRAINT fk_ls_loc_asset_loc
    FOREIGN KEY (asset_location_id)
    REFERENCES asset_location (asset_location_id),
	CONSTRAINT fk_ls_loc_tax_dist
    FOREIGN KEY (tax_district_id)
    REFERENCES tax_district (tax_district_id)
);

comment on table ls_location_tax_district is 'This table holds the asset location to tax district mappings for leased asset tax processing';

comment on column ls_location_tax_district.asset_location_id is 'The asset location that gets spread to multiple tax districts';
comment on column ls_location_tax_district.tax_district_id is 'The tax district for leased asset tax calculation';

alter table ls_monthly_tax
add
(
	tax_district_id number(22,0),
	CONSTRAINT fk_ls_tax_dist_id
    FOREIGN KEY (tax_district_id)
    REFERENCES tax_district (tax_district_id)
);

comment on column ls_monthly_tax.tax_district_id is 'The tax district that gets paid for the  for leased asset tax.  Null means paid to state';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2132, 0, 10, 4, 3, 2, 041651, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041651_lease_tax_districts.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;