/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036411_reg_REG_CASE_NET_OPER_INCOME_WF_V.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/11/2014 Ryan Oliveria
||============================================================================
*/

create or replace view REG_CASE_NET_OPER_INCOME_WF_V
as
select X.CASE_NAME, X.LABEL, nvl(X.AMOUNT,0) AMOUNT, X.SORT_ORDER, X.REG_CASE_ID, X.YEAR
  from (select RC.CASE_NAME CASE_NAME,
               'System Per Books' LABEL,
               -1 * sum(DECODE(A.REG_ACCT_TYPE_ID, 5, L.TOTAL_CO_INCLUDED_AMOUNT, 0)) -
               sum(DECODE(A.REG_ACCT_TYPE_ID, 3, L.TOTAL_CO_INCLUDED_AMOUNT)) AMOUNT,
               1 SORT_ORDER,
               RC.REG_CASE_ID,
               L.CASE_YEAR YEAR
          from REG_CASE RC, REG_CASE_SUMMARY_LEDGER L, REG_CASE_ACCT A
         where RC.REG_CASE_ID = L.REG_CASE_ID
           and L.REG_CASE_ID = A.REG_CASE_ID
           and L.REG_ACCT_ID = A.REG_ACCT_ID
           and L.REG_CASE_ID = 19
           and A.REG_ACCT_TYPE_ID in (3, 5, 11)
         group by RC.CASE_NAME, RC.REG_CASE_ID, L.CASE_YEAR
        union
        select RC.CASE_NAME,
               A.DESCRIPTION,
               -1 * sum(DECODE(T.REG_ACCT_TYPE_ID, 5, D.ADJUST_AMOUNT, 0)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 3, D.ADJUST_AMOUNT)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 11, DECODE(T.SUB_ACCT_TYPE_ID, 2, D.ADJUST_AMOUNT, 0))),
               2,
               RC.REG_CASE_ID,
               D.TEST_YR_ENDED YEAR
          from REG_CASE RC, REG_CASE_ADJUSTMENT A, REG_CASE_ADJUST_DETAIL D, REG_CASE_ACCT T
         where RC.REG_CASE_ID = A.REG_CASE_ID
           and A.REG_CASE_ID = D.REG_CASE_ID
           and A.ADJUSTMENT_ID = D.ADJUSTMENT_ID
           and D.REG_CASE_ID = T.REG_CASE_ID
           and D.REG_ACCT_ID = T.REG_ACCT_ID
           and T.REG_ACCT_TYPE_ID in (3, 5, 11)
           and D.REG_CASE_ID = 19
         group by RC.CASE_NAME, A.DESCRIPTION, RC.REG_CASE_ID, D.TEST_YR_ENDED) X
union
select X.CASE_NAME, 'System Adjusted', sum(X.AMOUNT), 3, X.REG_CASE_ID, X.YEAR
  from (select RC.CASE_NAME CASE_NAME,
               'System Per Books' LABEL,
               -1 * sum(DECODE(A.REG_ACCT_TYPE_ID, 5, L.TOTAL_CO_INCLUDED_AMOUNT, 0)) -
               sum(DECODE(A.REG_ACCT_TYPE_ID, 3, L.TOTAL_CO_INCLUDED_AMOUNT)) AMOUNT,
               1 SORT_ORDER,
               RC.REG_CASE_ID,
               L.CASE_YEAR YEAR
          from REG_CASE RC, REG_CASE_SUMMARY_LEDGER L, REG_CASE_ACCT A
         where RC.REG_CASE_ID = L.REG_CASE_ID
           and L.REG_CASE_ID = A.REG_CASE_ID
           and L.REG_ACCT_ID = A.REG_ACCT_ID
           and L.REG_CASE_ID = 19
           and A.REG_ACCT_TYPE_ID in (3, 5, 11)
         group by RC.CASE_NAME, RC.REG_CASE_ID, L.CASE_YEAR
        union
        select RC.CASE_NAME,
               A.DESCRIPTION,
               -1 * sum(DECODE(T.REG_ACCT_TYPE_ID, 5, D.ADJUST_AMOUNT, 0)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 3, D.ADJUST_AMOUNT)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 11, DECODE(T.SUB_ACCT_TYPE_ID, 2, D.ADJUST_AMOUNT, 0))),
               2,
               RC.REG_CASE_ID,
               D.TEST_YR_ENDED YEAR
          from REG_CASE RC, REG_CASE_ADJUSTMENT A, REG_CASE_ADJUST_DETAIL D, REG_CASE_ACCT T
         where RC.REG_CASE_ID = A.REG_CASE_ID
           and A.REG_CASE_ID = D.REG_CASE_ID
           and A.ADJUSTMENT_ID = D.ADJUSTMENT_ID
           and D.REG_CASE_ID = T.REG_CASE_ID
           and D.REG_ACCT_ID = T.REG_ACCT_ID
           and T.REG_ACCT_TYPE_ID in (3, 5, 11)
           and D.REG_CASE_ID = 19
         group by RC.CASE_NAME, A.DESCRIPTION, RC.REG_CASE_ID, D.TEST_YR_ENDED) X
 group by X.CASE_NAME, X.REG_CASE_ID, X.YEAR
 order by SORT_ORDER, YEAR;

 --**************************
 -- Log the run of the script
 --**************************

 insert into PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
     SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
 values
    (1030, 0, 10, 4, 2, 0, 36411, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036411_reg_REG_CASE_NET_OPER_INCOME_WF_V.sql', 1,
     SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
     SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;