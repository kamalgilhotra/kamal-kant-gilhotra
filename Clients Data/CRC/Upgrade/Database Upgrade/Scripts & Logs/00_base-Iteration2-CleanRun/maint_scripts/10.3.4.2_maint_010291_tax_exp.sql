/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010291_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.2   07/02/2012 Sunjin Cone    Point Release
||============================================================================
*/

create table REPAIR_BLANKET_PROCESS_REPORT
(
 BATCH_ID                   number(22,0) not null,
 REPAIR_SCHEMA_ID           number(22,0) not null,
 WORK_ORDER_ID              number(22,0) not null,
 WORK_ORDER_NUMBER          varchar2(35),
 COMPANY_ID                 number(22,0),
 BLANKET_METHOD             varchar2(35) not null,
 REPAIR_LOCATION_ID         number(22,0) not null,
 REPAIR_UNIT_CODE_ID        number(22,0) not null,
 CLOSING_OPTION_ID          number(22,0),
 REPLACEMENT_QTY            number(22,2),
 WO_UOP_QTY                 number(22,0),
 WO_UOP_RETIRE_QTY          number(22,0),
 WO_UOP_COST                number(22,2),
 WO_UOP_RETIRE_COST         number(22,2),
 WO_TOTAL_PERIOD_COST       number(22,2),
 EXPENSE_PERCENTAGE         number(22,8),
 NUMBER_OF_CIRCUITS         number(22,0),
 PRORATA_TEST_QTY           number(22,2),
 PRORATA_THRESHOLD          number(22,8),
 PRORATA_PASS_FAIL          varchar2(35),
 PROP_TOTAL_UOP_QTY         number(22,2),
 PROP_TOTAL_CIRCUIT_PERCENT number(22,8),
 PROP_QTY_PER_CIRCUIT       number(22,2),
 PROP_THRESHOLD             number(22,8),
 PROP_PASS_FAIL             varchar2(35),
 COST_UOP_REPLACEMENT_COST  number(22,2),
 COST_UOP_TEST_QTY          number(22,0),
 COST_THRESHOLD             number(22,8),
 COST_PASS_FAIL             varchar2(35),
 ALL_COST_REPLACEMENT_COST  number(22,2),
 ALL_COST_THRESHOLD         number(22,8),
 ALL_COST_PASS_FAIL         varchar2(35)
);

alter table REPAIR_BLANKET_PROCESS_REPORT
   add constraint PK_REPAIR_BLANKET_REPORT
       primary key (BATCH_ID, REPAIR_SCHEMA_ID, COMPANY_ID,
                    WORK_ORDER_NUMBER,  REPAIR_UNIT_CODE_ID,
                    REPAIR_LOCATION_ID, BLANKET_METHOD)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (154, 0, 10, 3, 4, 2, 10291, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.2_maint_010291_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
