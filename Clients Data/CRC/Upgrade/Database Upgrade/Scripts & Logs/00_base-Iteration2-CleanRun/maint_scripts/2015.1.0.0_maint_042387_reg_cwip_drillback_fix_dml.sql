/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042387_reg_cwip_drillback_fix_dml.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   01/22/2015 Shane Ward    		    Element Id's being populated causing error,
||                                            should be null
||========================================================================================
*/

--Reg Drillbacks should not use the element_id column (CR ELEMENTS)
UPDATE cr_dd_sources_criteria_fields
	 SET column_case = 'Any', element_id = NULL
 WHERE id IN (SELECT reg_query_id FROM reg_query_drillback);

UPDATE reg_budget_member_element SET interface_table = 'work_order_class_code work_order_class_code1' WHERE reg_member_element_id = 7;
UPDATE reg_budget_member_element SET interface_table = 'work_order_class_code work_order_class_code2' WHERE reg_member_element_id = 8;

UPDATE reg_cwip_member_element SET interface_table = 'work_order_class_code work_order_class_code1' WHERE reg_member_element_id = 7;
UPDATE reg_cwip_member_element SET interface_table = 'work_order_class_code work_order_class_code2' WHERE reg_member_element_id = 8;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2208, 0, 2015, 1, 0, 0, 42387, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042387_reg_cwip_drillback_fix_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;