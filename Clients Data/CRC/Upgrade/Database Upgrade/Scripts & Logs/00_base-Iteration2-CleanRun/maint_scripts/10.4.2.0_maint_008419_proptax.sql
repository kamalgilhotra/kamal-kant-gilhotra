SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008419_proptax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------
|| 10.4.2.0 01/07/2014 Andrew Scott         Table for payment reports when
||                                          over 1000 payments are selected
||============================================================================
*/

begin
   execute immediate 'drop table PT_PYMT_CNTR_1000_IDS';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table PT_PYMT_CNTR_1000_IDS does not exist or drop table priv not granted.');
end;
/

create table PT_PYMT_CNTR_1000_IDS
(
 PAYMENT_ID number(22,0) not null,
 USER_ID    varchar2(18),
 TIME_STAMP date
);

alter table PT_PYMT_CNTR_1000_IDS
   add constraint PT_PYMT_CNTR_1000_IDS_PK
       primary key ( PAYMENT_ID )
       using index tablespace PWRPLANT_IDX;

comment on table PT_PYMT_CNTR_1000_IDS is '(C) [07] The PT Payment Center 1000 IDs table is used for payment center reports when more than 1000 payment ids are selected.';
comment on column PT_PYMT_CNTR_1000_IDS.PAYMENT_ID is 'Standard system-assigned payment id for the payment selected.';
comment on column PT_PYMT_CNTR_1000_IDS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column PT_PYMT_CNTR_1000_IDS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (838, 0, 10, 4, 2, 0, 8419, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_008419_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;