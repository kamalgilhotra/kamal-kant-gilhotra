/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010858_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/21/2012 Sunjin Cone    Point Release
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Annual Additions by Batch',
          'Annual CPR Additions and Tax Expense Activity for Prior Years for WOs in a Tax Repairs Batch (Only Posted status batches will show Tax Expense dollars in the CPR Tax Expense column.)',
          'dw_repairs_106_101_batch_report',
          'REPAIRS',
          'REPRS - 1204',
          'dw_repair_batch_control',
          13,
          13,
          0,
          1,
          1,
          'REPRS - 1204',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Annual Additions by Tax Exp Test',
          'Annual CPR Additions and Tax Expense Activity for WOs Assigned to Tax Repairs Testing',
          'dw_repairs_106_101_taxtest_report',
          'REPAIRS',
          'REPRS - 1205',
          'dw_wo_tax_exp_test_select',
          13,
          13,
          2,
          3,
          1,
          'REPRS - 1205',
          3,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Annual Additions for All',
          'Annual CPR Additions and Tax Expense Activity for a Time Span',
          'dw_repairs_106_101_activity_report',
          'REPAIRS',
          'REPRS - 1206',
          '',
          13,
          13,
          2,
          3,
          1,
          'REPRS - 1206',
          3,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (203, 0, 10, 3, 5, 0, 10858, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010858_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
