/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_050962_lessor_01_types_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 03/12/2019 B. Beck    	Adding fields to the LSR_ILR_SALES_SCH_INFO
||										and rebuild the dependent Types.
||============================================================================
*/


CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO FORCE AUTHID CURRENT_USER AS OBJECT (carrying_cost NUMBER,
                                      carrying_cost_company_curr number,
                                      fair_market_value NUMBER,
                                      fair_market_value_company_curr number,
                                      guaranteed_residual NUMBER,
                                      estimated_residual NUMBER,
                                      days_in_year NUMBER,
                                      purchase_option_amount NUMBER,
                                      termination_amount NUMBER,
                                      remeasurement_date DATE,
                                      investment_amount NUMBER,
                                      original_profit_loss NUMBER,
                                      new_beg_receivable NUMBER,
                                      prior_end_unguaran_residual NUMBER,
                                      remeasure_month_fixed_payment NUMBER,
                                      include_idc_sw NUMBER,
                                      has_classification_change NUMBER
                                      );
/

create or replace TYPE t_lsr_ilr_sales_df_prelims AUTHID current_user AS OBJECT ( payment_info lsr_ilr_op_sch_pay_info_tab,
                                                              rates t_lsr_ilr_schedule_all_rates,
                                                              initial_direct_costs lsr_init_direct_cost_info_tab,
                                                              sales_type_info lsr_ilr_sales_sch_info);
/


CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO_TAB as table of lsr_ilr_sales_sch_info;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15962, 0, 2018, 2, 1, 0, 50962, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.1.0_maint_050962_lessor_01_types_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
