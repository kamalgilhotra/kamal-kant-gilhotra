/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049516_lessor_01_import_ilr_idc_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/15/2017 Shane "C" Ward    Create tables for Initial Direct Cost Import
||============================================================================
*/

CREATE TABLE LSR_IMPORT_ILR_INIT_DIRECT
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     error_message              VARCHAR2(4000),
     time_stamp                 DATE,
     user_id                    VARCHAR2(18),
     ilr_initial_direct_cost_id NUMBER(22, 0),
     ilr_id                     NUMBER(22, 0),
     ilr_id_xlate               VARCHAR2(254),
     revision                   NUMBER(22, 0),
     idc_group_id               NUMBER(22, 0),
     idc_group_id_xlate         VARCHAR2(254),
     date_incurred              VARCHAR2(35),
     amount                     NUMBER(22, 2),
     description                VARCHAR2(254)
  );

COMMENT ON TABLE LSR_IMPORT_ILR_INIT_DIRECT IS '(S) [06] The LSR Import ILR Initial Direct Cost table is an API table used to import Lessor ILR Initial Direct Costs.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.ilr_initial_direct_cost_id IS 'System-assigned identifier of a Lessor ILR Initial Direct Cost Line.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.ilr_id IS 'System-assigned identifier of a Lessor ILR.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.ilr_id IS 'Translate field for System-assigned identifier of a Lessor ILR.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.revision IS 'System-assigned identifier of a Lessor ILR revision.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.idc_group_id IS 'System-assigned identifier of an indirect cost group.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.idc_group_id_xlate IS 'Translate field for System-assigned identifier of an indirect cost group.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.date_incurred IS 'The date on which the cost incurred. Entered in MM/DD/YYYY format';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.amount IS 'Cost incurred';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT.description IS 'Description of the ILR indirect cost group assoiation.';

CREATE TABLE LSR_IMPORT_ILR_INIT_DIRECT_ARC
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     error_message              VARCHAR2(4000),
     time_stamp                 DATE,
     user_id                    VARCHAR2(18),
     ilr_initial_direct_cost_id NUMBER(22, 0),
     ilr_id                     NUMBER(22, 0),
     ilr_id_xlate               VARCHAR2(254),
     revision                   NUMBER(22, 0),
     idc_group_id               NUMBER(22, 0),
     idc_group_id_xlate         VARCHAR2(254),
     date_incurred              VARCHAR2(35),
     amount                     NUMBER(22, 2),
     description                VARCHAR2(254)
  );

COMMENT ON TABLE LSR_IMPORT_ILR_INIT_DIRECT_ARC IS '(S) [06] The LSR Import ILR Initial Direct Cost table is an API table used to import Lessor ILR Initial Direct Costs.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.ilr_initial_direct_cost_id IS 'System-assigned identifier of a Lessor ILR Initial Direct Cost Line.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.ilr_id IS 'System-assigned identifier of a Lessor ILR.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.ilr_id IS 'Translate field for System-assigned identifier of a Lessor ILR.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.revision IS 'System-assigned identifier of a Lessor ILR revision.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.idc_group_id IS 'System-assigned identifier of an indirect cost group.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.idc_group_id_xlate IS 'Translate field for System-assigned identifier of an indirect cost group.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.date_incurred IS 'The date on which the cost incurred. Entered in MM/DD/YYYY format';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.amount IS 'Cost incurred';

COMMENT ON COLUMN LSR_IMPORT_ILR_INIT_DIRECT_ARC.description IS 'Description of the ILR indirect cost group assoiation.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3972, 0, 2017, 1, 0, 0, 49516, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049516_lessor_01_import_ilr_idc_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;