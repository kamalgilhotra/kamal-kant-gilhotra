/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030393_lease_ilr_search.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Kyle Peterson  Point release
||============================================================================
*/

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_ilrcntr_wksp_search'
 where WORKSPACE_IDENTIFIER = 'search_ilr'
 and MODULE = 'LESSEE';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (438, 0, 10, 4, 1, 0, 30393, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030393_lease_ilr_search.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
