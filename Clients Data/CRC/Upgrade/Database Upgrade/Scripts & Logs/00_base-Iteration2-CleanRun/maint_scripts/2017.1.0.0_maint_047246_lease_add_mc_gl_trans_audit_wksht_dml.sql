/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047246_lease_add_mc_gl_trans_audit_wksht_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 07/11/2017 Jared Watkins  add new entry to the pp_worksheets table for the new
||                                      lease multicurrency audit table, so it is visible in the front-end
||============================================================================
*/

insert into pp_worksheets(worksheet_id, description, datawindow, title, add_button, delete_button)
select max(worksheet_id) + 1, 'Lease Multicurrency GL Trans Audit', 
'dw_ls_mc_gl_trans_audit', 'Lease Multicurrency GL Transaction Audit', 'No', 'No'
from pp_worksheets
where not exists (select 1 from pp_worksheets 
                  where title = 'Lease Multicurrency GL Transaction Audit' 
                  and datawindow = 'dw_ls_mc_gl_trans_audit'
                 )
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3567, 0, 2017, 1, 0, 0, 47246, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047246_lease_add_mc_gl_trans_audit_wksht_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
