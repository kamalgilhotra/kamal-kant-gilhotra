/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_050826_lessee_01_forecast_remain_terms_ddl.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0   05/07/2018   K. Powers      PP-50826 - Allow calculations to use
 ||                                        total or remaining terms on a lease.
 ||============================================================================
 */ 
 ALTER TABLE LS_FORECAST_VERSION ADD (TOTAL_TERMS_ID NUMBER(22,0));
 
 COMMENT ON COLUMN "PWRPLANT"."LS_FORECAST_VERSION"."TOTAL_TERMS_ID" IS 'User selected forecast rate option to use total or remaining terms.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5542, 0, 2017, 4, 0, 0, 50826, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050826_lessee_01_forecast_remain_terms_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

