/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032400_cpr_PP_CPR_PKG.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   09/12/2013 Charlie Shilling maint-32400
||============================================================================
*/

create or replace package PP_CPR_PKG
/*
||============================================================================
|| Application: PowerPlan
|| Object Name: PP_CPR_PKG
|| Description: CPR functions and procedures for PowerPlan application.
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0     08/13/2013 Joseph King    Create
|| 1.1     09/11/2013 C Shilling     Add p_lock_asset_tables procedure and edit f_get_asset_activity_id
||============================================================================
*/
 as
   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: f_get_asset_activity_id
   || Description: Finds the next asset activity id to be used by an asset.
   ||   Also calls P_LOCK_ASSET_DEPR_TABLES to lock core CPR and Depr tables
   ||   and prevent any concurrency issues.
   || Arguments:   Asset ID - Asset ID of the Asset having the activity added to
   ||               For new assets.  The CPR_LEDGER record should be created
   ||               before calling the function.
   || Return Values: >=1 - Asset Activity ID to be created
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     08/13/2013 Joseph King    Create
   || 1.1     09/11/2013 C Shilling  Add p_lock_asset_tables procedure and edit f_get_asset_activity_id
   ||============================================================================
   */
   function F_GET_ASSET_ACTIVITY_ID(A_ASSET_ID CPR_LEDGER.ASSET_ID%type) return number;

   function F_GET_ASSET_ACTIVITY_ID(A_ASSET_ID      CPR_LEDGER.ASSET_ID%type,
                                    A_DEPR_GROUP_ID CPR_LEDGER.DEPR_GROUP_ID%type) return number;

   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: p_lock_asset_depr_tables
   || Description: Locks core CPR and Depr tables to prevent any concurrency issues.
   || Arguments:   Asset ID - Asset ID of the Asset having the activity added to
   ||               For new assets.  The CPR_LEDGER record should be created
   ||               before calling the function.
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     09/11/2013 C Shilling     Create
   ||============================================================================
   */
   procedure P_LOCK_ASSET_DEPR_TABLES(A_ASSET_ID      CPR_LEDGER.ASSET_ID%type,
                                      A_DEPR_GROUP_ID CPR_LEDGER.DEPR_GROUP_ID%type);

end PP_CPR_PKG;
/


--**************************************************************************
--                            Package Body
--**************************************************************************
create or replace package body PP_CPR_PKG as

   type ASSET_ACTIVITY_ARR is table of CPR_ACTIVITY.ASSET_ACTIVITY_ID%type index by string(22);
   type NUMBER_ARR is table of number;
   ASSET_ACTIVITIES ASSET_ACTIVITY_ARR;

   --**************************************************************************
   --                            P_LOCK_ASSET_DEPR_TABLES
   --**************************************************************************
   procedure P_LOCK_ASSET_DEPR_TABLES(A_ASSET_ID      CPR_LEDGER.ASSET_ID%type,
                                      A_DEPR_GROUP_ID CPR_LEDGER.DEPR_GROUP_ID%type) is

      L_COUNTER2      NUMBER_ARR;

   begin
      -- Always lock the depr_ledger regardless of whether the asset_id is null
      begin
         select DEPR_GROUP_ID bulk collect
           into L_COUNTER2
           from DEPR_LEDGER
          where DEPR_GROUP_ID = A_DEPR_GROUP_ID
            for update;
      exception
         when NO_DATA_FOUND then
            L_COUNTER2.DELETE;
            -- continue
      end;

      if A_ASSET_ID is not null then
         -- Lock the key CPR Tables
         begin
            select ASSET_ID bulk collect
              into L_COUNTER2
              from CPR_LEDGER
             where ASSET_ID = A_ASSET_ID
               for update;
         exception
            when NO_DATA_FOUND then
               L_COUNTER2.DELETE;
               -- continue
         end;

         begin
            select ASSET_ID bulk collect
              into L_COUNTER2
              from CPR_ACTIVITY
             where ASSET_ID = A_ASSET_ID
               for update;
         exception
            when NO_DATA_FOUND then
               L_COUNTER2.DELETE;
               -- continue
         end;

         begin
            select ASSET_ID bulk collect
              into L_COUNTER2
              from CPR_DEPR
             where ASSET_ID = A_ASSET_ID
               for update;
         exception
            when NO_DATA_FOUND then
               L_COUNTER2.DELETE;
               -- continue
         end;
      end if;
   end P_LOCK_ASSET_DEPR_TABLES;

   --**************************************************************************
   --                            F_GET_ASSET_ACTIVITY_ID
   --**************************************************************************
   function F_GET_ASSET_ACTIVITY_ID(A_ASSET_ID CPR_LEDGER.ASSET_ID%type) return number is

   begin
      return F_GET_ASSET_ACTIVITY_ID(A_ASSET_ID, -1);
   end F_GET_ASSET_ACTIVITY_ID;

   --**************************************************************************
   --                            F_GET_ASSET_ACTIVITY_ID
   --**************************************************************************
   function F_GET_ASSET_ACTIVITY_ID(A_ASSET_ID      CPR_LEDGER.ASSET_ID%type,
                                    A_DEPR_GROUP_ID CPR_LEDGER.DEPR_GROUP_ID%type) return number is

      LS_ASSET_ID     string(22);
      L_COUNTER       number;
      L_COUNTER2      NUMBER_ARR;
      L_ACTIVITY_ID   CPR_ACTIVITY.ASSET_ACTIVITY_ID%type;
      L_DEPR_GROUP_ID CPR_LEDGER.DEPR_GROUP_ID%type;

   begin
      LS_ASSET_ID := TO_CHAR(A_ASSET_ID);

      select count(*), min(DEPR_GROUP_ID)
        into L_COUNTER, L_DEPR_GROUP_ID
        from CPR_LEDGER
       where ASSET_ID = A_ASSET_ID;

      --If asset not found, use depr_group from input argument.
      if L_COUNTER = 0 then
         L_DEPR_GROUP_ID := A_DEPR_GROUP_ID;
      end if;

      if L_COUNTER > 0 then
         -- Lock tables
         P_LOCK_ASSET_DEPR_TABLES(A_ASSET_ID, L_DEPR_GROUP_ID);

         -- Have not already pulled the max.  Get it, increment, set collection and return
         select NVL(max(ASSET_ACTIVITY_ID), 0)
           into L_ACTIVITY_ID
           from CPR_ACTIVITY
          where ASSET_ID = A_ASSET_ID;
      else
         -- Lock tables
         P_LOCK_ASSET_DEPR_TABLES(null, L_DEPR_GROUP_ID);

         -- Record does not exist in CPR_LEDGER.  Cannot lock CPR tables, but we can lock the depr_tables.
         L_ACTIVITY_ID := 0;
      end if;

      if ASSET_ACTIVITIES.EXISTS(LS_ASSET_ID) then
         L_ACTIVITY_ID := GREATEST(ASSET_ACTIVITIES(LS_ASSET_ID), L_ACTIVITY_ID);
      end if;
      L_ACTIVITY_ID := L_ACTIVITY_ID + 1;
      if LS_ASSET_ID is not null then
         ASSET_ACTIVITIES(LS_ASSET_ID) := L_ACTIVITY_ID;
      end if;

      return L_ACTIVITY_ID;
   end F_GET_ASSET_ACTIVITY_ID;

--**************************************************************************
--                            Initialize Package
--**************************************************************************
/*
begin
   Initialization procedure (Called the first time this package is opened in a session)
    Initialize the VALIDATE DEPR BUSINESS SEGMENT variable
*/
end PP_CPR_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (614, 0, 10, 4, 1, 0, 32400, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032400_cpr_PP_CPR_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
