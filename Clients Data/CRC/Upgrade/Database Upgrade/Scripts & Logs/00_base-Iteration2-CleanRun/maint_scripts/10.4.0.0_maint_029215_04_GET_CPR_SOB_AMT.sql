/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029215_04_GET_CPR_SOB_AMT.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- --------------   ------------------------------------
|| 10.4.0.0   02/05/2013 Charlie Shilling Point Release
||============================================================================
*/

create or replace function GET_CPR_SOB_AMT(P_ASSET_ID        number,
                                           P_SET_OF_BOOKS_ID number,
                                           P_FACTOR          number) return number is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: GET_CPR_SOB_AMT
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   ||============================================================================
   */

   V_RTN      number(22, 2);
   V_NUMBASIS number;
   V_SOB_AMT  number(22, 2);
   V_SQLS     varchar2(4000);
   type CUR_TYP is ref cursor;
   C CUR_TYP;

begin
   --Check that Pend Transaction exists
   select count(1) into V_RTN from CPR_LEDGER where ASSET_ID = P_ASSET_ID;

   if V_RTN < 1 then
      RAISE_APPLICATION_ERROR(-10801,
                              'No Asset Found in Get CPR SOB AMT Function (asset_id: ' ||
                              P_ASSET_ID || ')');
   end if;

   select max(BOOK_SUMMARY_ID) into V_NUMBASIS from BOOK_SUMMARY;

   V_SOB_AMT := 0;

   for I in 1 .. V_NUMBASIS
   loop
      V_SQLS := 'select sum(nvl(basis_' || TO_CHAR(I) || ',0) * nvl(b.basis_' || TO_CHAR(I) ||
                '_indicator,0)) * ' || P_FACTOR ||
                ' basis_amount from cpr_ldg_basis a, set_of_books b where a.asset_id = ' ||
                TO_CHAR(P_ASSET_ID) || ' and b.set_of_books_id = ' || TO_CHAR(P_SET_OF_BOOKS_ID);
      --DBMS_OUTPUT.PUT_LINE(V_SQLS);
      open C for V_SQLS;
      loop
         fetch C
            into V_RTN;
         exit when C%notfound;
         -- process row here
      end loop;
      close C;
      V_SOB_AMT := V_SOB_AMT + V_RTN;
   end loop;

   return V_SOB_AMT;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (286, 0, 10, 4, 0, 0, 29215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029215_04_GET_CPR_SOB_AMT.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;