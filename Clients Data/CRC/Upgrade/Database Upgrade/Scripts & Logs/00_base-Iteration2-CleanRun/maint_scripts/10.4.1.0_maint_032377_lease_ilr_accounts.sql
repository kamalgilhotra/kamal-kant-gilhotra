/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032377_lease_ilr_accounts.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/12/2013 Brandon Beck   Point Release
||============================================================================
*/

create table LS_ILR_ACCOUNT
(
 ILR_ID                  number(22,0),
 INT_ACCRUAL_ACCOUNT_ID  number(22,0),
 INT_EXPENSE_ACCOUNT_ID  number(22,0),
 EXEC_ACCRUAL_ACCOUNT_ID number(22,0),
 EXEC_EXPENSE_ACCOUNT_ID number(22,0),
 CONT_ACCRUAL_ACCOUNT_ID number(22,0),
 CONT_EXPENSE_ACCOUNT_ID number(22,0),
 USER_ID                 varchar2(18),
 TIME_STAMP              date
);

comment on table LS_ILR_ACCOUNT is '(S) [06] This table holds the accounts used in JE creation for a given ILR.  These accounts are defaulted from the ILR Group and can be overriden on a specific ILR';
comment on column LS_ILR_ACCOUNT.INT_ACCRUAL_ACCOUNT_ID is 'The interest accrual account';
comment on column LS_ILR_ACCOUNT.INT_EXPENSE_ACCOUNT_ID is 'The interest expense account';
comment on column LS_ILR_ACCOUNT.EXEC_ACCRUAL_ACCOUNT_ID is 'The executory accrual account';
comment on column LS_ILR_ACCOUNT.EXEC_EXPENSE_ACCOUNT_ID is 'The executory expense account';
comment on column LS_ILR_ACCOUNT.CONT_ACCRUAL_ACCOUNT_ID is 'The contingent accrual account';
comment on column LS_ILR_ACCOUNT.CONT_EXPENSE_ACCOUNT_ID is 'The contingent expense account';

alter table LS_ILR_GROUP
   add (INT_ACCRUAL_ACCOUNT_ID  number(22,0),
        INT_EXPENSE_ACCOUNT_ID  number(22,0),
        EXEC_ACCRUAL_ACCOUNT_ID number(22,0),
        EXEC_EXPENSE_ACCOUNT_ID number(22,0),
        CONT_ACCRUAL_ACCOUNT_ID number(22,0),
        CONT_EXPENSE_ACCOUNT_ID number(22,0));

comment on column LS_ILR_GROUP.INT_ACCRUAL_ACCOUNT_ID is 'The interest accrual account';
comment on column LS_ILR_GROUP.INT_EXPENSE_ACCOUNT_ID is 'The interest expense account';
comment on column LS_ILR_GROUP.EXEC_ACCRUAL_ACCOUNT_ID is 'The executory accrual account';
comment on column LS_ILR_GROUP.EXEC_EXPENSE_ACCOUNT_ID is 'The executory expense account';
comment on column LS_ILR_GROUP.CONT_ACCRUAL_ACCOUNT_ID is 'The contingent accrual account';
comment on column LS_ILR_GROUP.CONT_EXPENSE_ACCOUNT_ID is 'The contingent expense account';

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_PK
       primary key (ILR_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK1
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK2
       foreign key (INT_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK3
       foreign key (INT_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK4
       foreign key (EXEC_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK5
       foreign key (EXEC_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK6
       foreign key (CONT_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK7
       foreign key (CONT_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK2
       foreign key (INT_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK3
       foreign key (INT_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK4
       foreign key (EXEC_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK5
       foreign key (EXEC_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK6
       foreign key (CONT_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK7
       foreign key (CONT_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (615, 0, 10, 4, 1, 0, 32377, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032377_lease_ilr_accounts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
