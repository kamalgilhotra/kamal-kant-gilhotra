/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032815_sys_table_doc.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/14/2013 Ron Ferentini  Point Release
||============================================================================
*/

comment on column UTILITY_ACCOUNT.OVH_TO_UDG is 'Indicates whether Overhead/Underground conductor applies to the utility account.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (701, 0, 10, 4, 2, 0, 32815, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032815_sys_table_doc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
