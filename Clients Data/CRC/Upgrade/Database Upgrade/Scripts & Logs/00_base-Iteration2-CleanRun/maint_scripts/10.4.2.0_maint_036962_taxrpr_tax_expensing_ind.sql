/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036962_taxrpr_tax_expensing_ind.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/05/2014 Alex P.
||============================================================================
*/

update CLASS_CODE set CPR_INDICATOR = 1 where LOWER(DESCRIPTION) = 'tax expensing';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1011, 0, 10, 4, 2, 0, 36962, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036962_taxrpr_tax_expensing_ind.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;