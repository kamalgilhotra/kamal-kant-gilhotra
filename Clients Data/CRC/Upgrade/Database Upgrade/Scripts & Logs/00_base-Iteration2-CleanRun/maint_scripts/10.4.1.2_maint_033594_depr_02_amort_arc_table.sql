/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033594_depr_02_amort_arc_table.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.2   11/25/2013 Charlie Shilling Create archive table for amortization
||============================================================================
*/

create table DEPR_CALC_AMORT_ARC
(
 DEPR_GROUP_ID      number(22,0),
 SET_OF_BOOKS_ID    number(22,0),
 GL_POST_MO_YR      date,
 VINTAGE            number(22,0),
 COR_TREATMENT      varchar2(35),
 COR_LIFE           number(22,0),
 PRIOR_COR_BALANCE  number(22,2),
 PRIOR_COR_RESERVE  number(22,2),
 COR_BALANCE        number(22,2),
 COR_AMORT          number(22,2),
 COR_RESERVE        number(22,2),
 SALV_TREATMENT     varchar2(35),
 SALV_LIFE          number(22,0),
 PRIOR_SALV_BALANCE number(22,2),
 PRIOR_SALV_RESERVE number(22,2),
 SALV_BALANCE       number(22,2),
 SALV_AMORT         number(22,2),
 SALV_RESERVE       number(22,2)
);

comment on table DEPR_CALC_AMORT_ARC is '(C) [01][05] {A}
The Depr Calc Amort Arc table is an archive table for the Depr Calc Amort Stg table.';

comment on column DEPR_CALC_AMORT_ARC.DEPR_GROUP_ID is 'System-assigned identifier of a particular depreciation group.';
comment on column DEPR_CALC_AMORT_ARC.SET_OF_BOOKS_ID is 'System-assigned identifier of a unique set of books maintained by the utility in PowerPlan.';
comment on column DEPR_CALC_AMORT_ARC.GL_POST_MO_YR is 'Records the accounting month and year of activity the entry represents for a given depreciation group.';
comment on column DEPR_CALC_AMORT_ARC.VINTAGE is 'Year the cost of removal and gross salvage was closed to the depreciation ledger.';
comment on column DEPR_CALC_AMORT_ARC.COR_TREATMENT is 'The cor_treatment from DEPR_METHOD_RATES used to calculate the month''s cost of removal amortization.';
comment on column DEPR_CALC_AMORT_ARC.COR_LIFE is 'The life, in months, to amortize cost_of_removal charges over. Used to calculate the amortization rate.';
comment on column DEPR_CALC_AMORT_ARC.PRIOR_COR_BALANCE IS 'The cost of removal balance from the prior month.';
comment on column DEPR_CALC_AMORT_ARC.COR_BALANCE is 'The cost of removal balance for the current month.';
comment on column DEPR_CALC_AMORT_ARC.COR_AMORT is 'The cost of removal amortization amount for the current month.';
comment on column DEPR_CALC_AMORT_ARC.COR_RESERVE is 'The cumulative total cost of removal amortization accrued for the vintage.';
comment on column DEPR_CALC_AMORT_ARC.SALV_TREATMENT is 'The gross salvage_treatment from DEPR_METHOD_RATES used to calculate the month''s gross salvage amortization.';
comment on column DEPR_CALC_AMORT_ARC.SALV_LIFE is 'The life, in months, to amortize cost_of_removal charges over. Used to calculate the amortization rate.';
comment on column DEPR_CALC_AMORT_ARC.PRIOR_SALV_BALANCE is 'The salvage balance from the prior month.';
comment on column DEPR_CALC_AMORT_ARC.SALV_BALANCE is 'The salvage balance for the current month.';
comment on column DEPR_CALC_AMORT_ARC.SALV_AMORT is 'The salvage amortization amount for the current month.';
comment on column DEPR_CALC_AMORT_ARC.SALV_RESERVE is 'The cumulative total cost of removal amortization accrued for the vintage.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (790, 0, 10, 4, 1, 2, 33594, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_033594_depr_02_amort_arc_table.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;