/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008779_deprfcst_update.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/27/2013 Sunjin Cone    Point Release
||============================================================================
*/

--JIRA 8879
--Extend external depr code to 254 in fcst

alter table FCST_DEPR_GROUP         modify EXTERNAL_DEPR_CODE varchar2(254);
alter table FCST_DEPR_GROUP_VERSION modify EXTERNAL_DEPR_CODE varchar2(254);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (333, 0, 10, 4, 1, 0, 8779, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_008779_deprfcst_update.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;