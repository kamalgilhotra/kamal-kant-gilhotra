SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_006364_projects.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/01/2011 Luis Kramarz   Point Release
|| 10.4.2.0   04/05/2014 Lee Quinn      Added PL/SQL to ignore if it already exists.
||============================================================================
*/

-- May already exist.
begin
   execute immediate 'alter table WO_EST_TEMPLATE_WO_TYPE
                        add constraint FK_WO_TYPE_ID
                            foreign key (WORK_ORDER_TYPE_ID)
                            references WORK_ORDER_TYPE(WORK_ORDER_TYPE_ID)';
   DBMS_OUTPUT.PUT_LINE('Constraint added.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Constraint already exists.');
end;
/


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (33, 0, 10, 3, 3, 0, 6364, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_006364_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
