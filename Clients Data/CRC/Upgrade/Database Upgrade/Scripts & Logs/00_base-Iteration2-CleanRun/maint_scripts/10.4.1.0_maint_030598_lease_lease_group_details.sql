/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030598_lease_lease_group_details.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/09/2013 Kyle Peterson  Point Release
||============================================================================
*/

alter table LS_LEASE_GROUP rename column LOND_DESCRIPTION to LONG_DESCRIPTION;

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'admin_lease_groups', 'Lease Groups', 'uo_ls_lgcntr_wksp_details');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_lease_groups', 2, 5, 'Lease Groups', 'menu_wksp_admin', 'admin_lease_groups',
    1);

create sequence LS_LEASE_GROUP_SEQ
   minvalue 1
   start with 1
   increment by 1
   nocache;

alter table LS_ILR drop column LEASE_GROUP_ID;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (458, 0, 10, 4, 1, 0, 30598, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030598_lease_lease_group_details.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
