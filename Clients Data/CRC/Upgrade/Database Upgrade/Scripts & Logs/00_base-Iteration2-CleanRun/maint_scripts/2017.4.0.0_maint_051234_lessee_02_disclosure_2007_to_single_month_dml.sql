/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051234_lessee_02_disclosure_2007_to_single_month_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2017.4.0.0 5/15/2018  Sarah Byers    MC - Change date criteria on Statement of Financial Position to use a single month
||============================================================================
*/

update pp_reports
set pp_report_time_option_id = 207
where report_number = 'Lessee - 2007';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5522, 0, 2017, 4, 0, 0, 51234, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051234_lessee_02_disclosure_2007_to_single_month_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;