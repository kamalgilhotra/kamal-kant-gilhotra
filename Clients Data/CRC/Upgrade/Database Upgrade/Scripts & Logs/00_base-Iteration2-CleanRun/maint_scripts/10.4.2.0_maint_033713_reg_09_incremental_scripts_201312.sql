/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033713_reg_09_incremental_scripts_201312.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014 Sarah Byers
||============================================================================
*/

-- All incremental scripts for Dec 2013
--Add Month char and month date
--SCW 12/02/2013
insert into REG_EXT_STG_FIELD (FIELD_NAME) values ('MONTH_CHAR');
insert into REG_EXT_STG_FIELD (FIELD_NAME) values ('MONTH_DATE');

-- add reg_source_id to reg_history_ledger and reg_forecast_ledger
alter table REG_HISTORY_LEDGER  add REG_SOURCE_ID number(22,0);
alter table REG_FORECAST_LEDGER add REG_SOURCE_ID number(22,0);

-- add foreign keys
alter table REG_HISTORY_LEDGER
   add constraint R_REG_HISTORY_LEDGER4
       foreign key (REG_SOURCE_ID)
       references REG_SOURCE (REG_SOURCE_ID);

alter table REG_FORECAST_LEDGER
   add constraint R_REG_FORECAST_LEDGER4
       foreign key (REG_SOURCE_ID)
       references REG_SOURCE (REG_SOURCE_ID);

-- comments
comment on column REG_HISTORY_LEDGER.REG_SOURCE_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY SOURCE.';
comment on column REG_FORECAST_LEDGER.REG_SOURCE_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY SOURCE.';

-- update the history and forecast ledgers with reg source based on the reg account
update REG_HISTORY_LEDGER L
   set REG_SOURCE_ID =
        (select M.REG_SOURCE_ID from REG_ACCT_MASTER M where L.REG_ACCT_ID = M.REG_ACCT_ID)
 where exists (select 1 from REG_ACCT_MASTER M where L.REG_ACCT_ID = M.REG_ACCT_ID);

update REG_FORECAST_LEDGER L
   set REG_SOURCE_ID =
        (select M.REG_SOURCE_ID from REG_ACCT_MASTER M where L.REG_ACCT_ID = M.REG_ACCT_ID)
 where exists (select 1 from REG_ACCT_MASTER M where L.REG_ACCT_ID = M.REG_ACCT_ID);

-- make the reg_source_id field non-nullable
alter table REG_HISTORY_LEDGER  modify REG_SOURCE_ID number(22,0) not null;
alter table REG_FORECAST_LEDGER modify REG_SOURCE_ID number(22,0) not null;

alter table REG_FORECAST_LEDGER_ADJUST add REG_SOURCE_ID number(22,0);

alter table REG_FORECAST_LEDGER_ADJUST
   add constraint R_REG_FORECAST_LEDGER_ADJUST2
       foreign key (REG_SOURCE_ID)
       references REG_SOURCE (REG_SOURCE_ID);

alter table REG_HISTORY_LEDGER_ADJUST add REG_SOURCE_ID number(22,0);

alter table REG_HISTORY_LEDGER_ADJUST
   add constraint R_REG_HISTORY_LEDGER_ADJUST2
       foreign key (REG_SOURCE_ID)
       references REG_SOURCE (REG_SOURCE_ID);

-- drop the foreign keys to reg_history_ledger and reg_forecast_ledger
alter table REG_HISTORY_LEDGER_ADJUST drop constraint R_REG_HISTORY_LEDGER_ADJUST1;
alter table REG_FORECAST_LEDGER_ADJUST drop constraint R_REG_FORECAST_LEDGER_ADJUST1;

-- drop the foreign keys to reg_history_activity and reg_forecast_activity
alter table REG_HISTORY_ACTIVITY drop constraint R_REG_HISTORY_ACTIVITY1;
alter table REG_FORECAST_ACTIVITY drop constraint R_REG_FORECAST_ACTIVITY1;

-- drop the primary keys
alter table REG_HISTORY_LEDGER drop primary key drop index;
alter table REG_FORECAST_LEDGER drop primary key drop index;

-- add reg source id to the primary keys
alter table REG_HISTORY_LEDGER
   add constraint PK_REG_HISTORY_LEDGER
       primary key (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH, REG_SOURCE_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_FORECAST_LEDGER
   add constraint PK_REG_FORECAST_LEDGER
       primary key (REG_COMPANY_ID, REG_ACCT_ID, FORECAST_VERSION_ID, GL_MONTH, REG_SOURCE_ID)
       using index tablespace PWRPLANT_IDX;

-- add the foreign keys for the ledger adjust tables back
alter table REG_HISTORY_LEDGER_ADJUST
   add constraint R_REG_HISTORY_LEDGER_ADJUST1
       foreign key (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH, REG_SOURCE_ID)
       references REG_HISTORY_LEDGER (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH, REG_SOURCE_ID);

alter table REG_FORECAST_LEDGER_ADJUST
   add constraint R_REG_FORECAST_LEDGER_ADJUST1
       foreign key (REG_COMPANY_ID, REG_ACCT_ID, FORECAST_VERSION_ID, GL_MONTH, REG_SOURCE_ID)
       references REG_FORECAST_LEDGER (REG_COMPANY_ID, REG_ACCT_ID, FORECAST_VERSION_ID, GL_MONTH, REG_SOURCE_ID);

-- add reg_source_id to reg_history_activity and reg_forecast_activity
alter table REG_HISTORY_ACTIVITY  add REG_SOURCE_ID number(22,0);
alter table REG_FORECAST_ACTIVITY add REG_SOURCE_ID number(22,0);

update REG_HISTORY_ACTIVITY  set REG_SOURCE_ID = 1;
update REG_FORECAST_ACTIVITY set REG_SOURCE_ID = 1;

alter table REG_HISTORY_ACTIVITY modify  REG_SOURCE_ID number(22,0) not null;
alter table REG_FORECAST_ACTIVITY modify REG_SOURCE_ID number(22,0) not null;

alter table REG_HISTORY_ACTIVITY
   add constraint R_REG_HISTORY_ACTIVITY1
       foreign key (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH, REG_SOURCE_ID)
       references REG_HISTORY_LEDGER (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH, REG_SOURCE_ID);

alter table REG_FORECAST_ACTIVITY
   add constraint R_REG_FORECAST_ACTIVITY1
       foreign key (REG_COMPANY_ID, REG_ACCT_ID, FORECAST_VERSION_ID, GL_MONTH, REG_SOURCE_ID)
       references REG_FORECAST_LEDGER (REG_COMPANY_ID, REG_ACCT_ID, FORECAST_VERSION_ID, GL_MONTH, REG_SOURCE_ID);

-- add reg_acct_source_id to reg_ext_source
alter table REG_EXT_SOURCE add REG_ACCT_SOURCE_ID number(22,0);

alter table REG_EXT_SOURCE
   add constraint R_REG_EXT_SOURCE2
       foreign key (REG_ACCT_SOURCE_ID)
       references REG_SOURCE (REG_SOURCE_ID);

comment on column REG_EXT_SOURCE.REG_ACCT_SOURCE_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY SOURCE THAT IS USED AS THE SOURCE FOR CREATING AND ASSIGNING REG ACCOUNTS DURING THE TRANSLATE PROCESS.';

update REG_EXT_SOURCE set REG_ACCT_SOURCE_ID = REG_SOURCE_ID;

alter table REG_EXT_SOURCE modify REG_ACCT_SOURCE_ID not null;

-- populate the reg interface table
insert into REG_INTERFACE
   (REG_INTERFACE_ID, DESCRIPTION, ACT_OR_FCST_TYPE, LONG_DESCRIPTION, BUTTON_TEXT)
   select REG_SOURCE_ID, DESCRIPTION, 1, DESCRIPTION, SUBSTR('Run ' || DESCRIPTION, 1, 30)
     from REG_EXT_SOURCE
    where REG_SOURCE_ID > 100;

create or replace view REG_HISTORY_LEDGER_SV
(REG_COMPANY, REG_ACCOUNT, REG_ACCOUNT_TYPE, SUB_ACCOUNT_TYPE, REG_SOURCE, HISTORIC_LEDGER, GL_MONTH,
 ACT_AMOUNT, ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       HISTORIC_LEDGER,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH
  from (select C.DESCRIPTION      REG_COMPANY,
               M.DESCRIPTION      REG_ACCOUNT,
               T.DESCRIPTION      REG_ACCOUNT_TYPE,
               S.DESCRIPTION      SUB_ACCOUNT_TYPE,
               R.DESCRIPTION      REG_SOURCE,
               V.LONG_DESCRIPTION HISTORIC_LEDGER,
               L.GL_MONTH         GL_MONTH,
               L.ACT_AMOUNT       ACT_AMOUNT,
               L.ANNUALIZED_AMT   ANNUALIZED_AMT,
               L.ADJ_AMOUNT       ADJ_AMOUNT,
               L.ADJ_MONTH        ADJ_MONTH
          from REG_COMPANY_SV       C,
               REG_ACCT_MASTER      M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER   L,
               REG_ACCT_TYPE        T,
               REG_SUB_ACCT_TYPE    S,
               REG_SOURCE           R
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);


create or replace view REG_HISTORY_LEDGER_ID_SV
(REG_COMPANY, REG_ACCOUNT, REG_ACCOUNT_TYPE, SUB_ACCOUNT_TYPE, REG_SOURCE, HISTORIC_LEDGER, GL_MONTH, ACT_AMOUNT,
 ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH, REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_LEDGER_ID, REG_SOURCE_ID)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       HISTORIC_LEDGER,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       HISTORIC_LEDGER_ID,
       REG_SOURCE_ID
  from (select C.DESCRIPTION         REG_COMPANY,
               M.DESCRIPTION         REG_ACCOUNT,
               T.DESCRIPTION         REG_ACCOUNT_TYPE,
               S.DESCRIPTION         SUB_ACCOUNT_TYPE,
               R.DESCRIPTION         REG_SOURCE,
               V.LONG_DESCRIPTION    HISTORIC_LEDGER,
               L.GL_MONTH            GL_MONTH,
               L.ACT_AMOUNT          ACT_AMOUNT,
               L.ANNUALIZED_AMT      ANNUALIZED_AMT,
               L.ADJ_AMOUNT          ADJ_AMOUNT,
               L.ADJ_MONTH           ADJ_MONTH,
               L.REG_COMPANY_ID      REG_COMPANY_ID,
               L.REG_ACCT_ID         REG_ACCT_ID,
               L.HISTORIC_VERSION_ID HISTORIC_LEDGER_ID,
               L.REG_SOURCE_ID       REG_SOURCE_ID
          from REG_COMPANY_SV       C,
               REG_ACCT_MASTER      M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER   L,
               REG_ACCT_TYPE        T,
               REG_SUB_ACCT_TYPE    S,
               REG_SOURCE           R
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);

create or replace view REG_FORECAST_LEDGER_SV
(REG_COMPANY, REG_ACCOUNT, REG_SOURCE, FORECAST_LEDGER, GL_MONTH, FCST_AMOUNT, ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_SOURCE,
       FORECAST_LEDGER,
       GL_MONTH,
       FCST_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH
  from (select C.DESCRIPTION      REG_COMPANY,
               M.DESCRIPTION      REG_ACCOUNT,
               R.DESCRIPTION      REG_SOURCE,
               V.LONG_DESCRIPTION FORECAST_LEDGER,
               L.GL_MONTH         GL_MONTH,
               L.FCST_AMOUNT      FCST_AMOUNT,
               L.ANNUALIZED_AMT   ANNUALIZED_AMT,
               L.ADJ_AMOUNT       ADJ_AMOUNT,
               L.ADJ_MONTH        ADJ_MONTH
          from REG_COMPANY_SV       C,
               REG_ACCT_MASTER      M,
               REG_FORECAST_VERSION V,
               REG_FORECAST_LEDGER  L,
               REG_SOURCE           R
         where L.FORECAST_VERSION_ID = V.FORECAST_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID);


insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER,
    COLUMN_WIDTH, QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select C.ID, 'REG_SOURCE', NVL(M.MAX_COL_ORDER, 0) + 1, 0, 1, 'Reg Source', 674, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA C,
          (select max(F.COLUMN_ORDER) MAX_COL_ORDER
             from CR_DD_SOURCES_CRITERIA_FIELDS F, CR_DD_SOURCES_CRITERIA D
            where F.ID = D.ID
              and D.TABLE_NAME = 'reg_history_ledger_sv') M
    where C.TABLE_NAME = 'reg_history_ledger_sv';

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER,
    COLUMN_WIDTH, QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select C.ID, 'REG_SOURCE', NVL(M.MAX_COL_ORDER, 0) + 1, 0, 1, 'Reg Source', 674, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA C,
          (select max(F.COLUMN_ORDER) MAX_COL_ORDER
             from CR_DD_SOURCES_CRITERIA_FIELDS F, CR_DD_SOURCES_CRITERIA D
            where F.ID = D.ID
              and D.TABLE_NAME = 'reg_history_ledger_id_sv') M
    where C.TABLE_NAME = 'reg_history_ledger_id_sv';

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER,
    COLUMN_WIDTH, QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select C.ID,
          'REG_SOURCE_ID',
          NVL(M.MAX_COL_ORDER, 0) + 1,
          0,
          1,
          'Reg Source',
          674,
          0,
          'Any',
          0,
          0
     from CR_DD_SOURCES_CRITERIA C,
          (select max(F.COLUMN_ORDER) MAX_COL_ORDER
             from CR_DD_SOURCES_CRITERIA_FIELDS F, CR_DD_SOURCES_CRITERIA D
            where F.ID = D.ID
              and D.TABLE_NAME = 'reg_history_ledger_id_sv') M
    where C.TABLE_NAME = 'reg_history_ledger_id_sv';

insert into CR_DD_SOURCES_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, COLUMN_HEADER,
    COLUMN_WIDTH, QUANTITY_FIELD, COLUMN_CASE, REQUIRED_FILTER, TABLE_LOOKUP)
   select C.ID, 'REG_SOURCE', NVL(M.MAX_COL_ORDER, 0) + 1, 0, 1, 'Reg Source', 674, 0, 'Any', 0, 0
     from CR_DD_SOURCES_CRITERIA C,
          (select max(F.COLUMN_ORDER) MAX_COL_ORDER
             from CR_DD_SOURCES_CRITERIA_FIELDS F, CR_DD_SOURCES_CRITERIA D
            where F.ID = D.ID
              and D.TABLE_NAME = 'reg_forecast_ledger_sv') M
    where C.TABLE_NAME = 'reg_forecast_ledger_sv';

alter table REG_ANNUALIZE_WORK add REG_SOURCE_ID number(22,0);

create table REG_JUR_ADJ_ATTACHMENT
(
 ATTACH_ID           number(22,0)  not null,
 REG_JUR_TEMPLATE_ID number(22,0)  not null,
 ADJUSTMENT_ID       number(22,0)  not null,
 ATTACHMENT_DATA     blob          null,
 FILE_NAME           varchar2(100) null,
 FILE_SIZE           number(22,0)  null,
 USER_ID             varchar2(18)  null,
 TIME_STAMP          date          null
);

alter table REG_JUR_ADJ_ATTACHMENT
   add constraint PK_REG_JUR_ADJ_ATTACHMENT
       primary key (ATTACH_ID)
       using index tablespace PWRPLANT_IDX;

alter table reg_jur_adj_attachment
   add constraint R_REG_JUR_ADJ_ATTACHMENT1
       foreign key (REG_JUR_TEMPLATE_ID, ADJUSTMENT_ID)
       references REG_ADJUST_JUR_DEFAULT (REG_JUR_TEMPLATE_ID, ADJUSTMENT_ID);

--Drop FK's to reg...ledger in reg...activity tables
-- 12/09/2013 SCW
alter table REG_HISTORY_ACTIVITY drop constraint R_REG_HISTORY_ACTIVITY1;

alter table REG_FORECAST_ACTIVITY drop constraint R_REG_FORECAST_ACTIVITY1;

-- Revenue Req Keys
insert into REG_FIN_MONITOR_KEY
   (REG_FIN_MONITOR_KEY_ID, DESCRIPTION, PROCESS_FLAG, PARENT_KEY, KEY_LEVEL, SORT_ORDER)
values
   (950, 'Allowed Revenue', 9, 0, 1, 650);

insert into REG_FIN_MONITOR_KEY
   (REG_FIN_MONITOR_KEY_ID, DESCRIPTION, PROCESS_FLAG, PARENT_KEY, KEY_LEVEL, SORT_ORDER)
values
   (960, 'Revenue Difference', 9, 0, 1, 660);

--Alter datatypes for import_stg and arc to be varchar 254's
--SCW 12/13/2013
create table REG_IMPORT_EXT_STAGE_STG_B
(
 IMPORT_RUN_ID       number(22,0) not null,
 LINE_ID             number(22,0) not null,
 ERROR_MESSAGE       varchar2(4000),
 EXT_SOURCE_ID       number(22,0),
 EXT_SOURCE_ID_XLATE varchar2(254),
 STAGE_ID            number(22,0),
 COMPANY             varchar2(35),
 MONTH_NUMBER        number(22,0),
 MONTH_DATE          date,
 MONTH_CHAR          varchar2(20),
 AMOUNT              number(22,2),
 EXT_01              varchar2(254),
 EXT_02              varchar2(254),
 EXT_03              varchar2(254),
 EXT_04              varchar2(254),
 EXT_05              varchar2(254),
 EXT_06              varchar2(254),
 EXT_07              varchar2(254),
 EXT_08              varchar2(254),
 EXT_09              varchar2(254),
 EXT_10              varchar2(254),
 EXT_11              varchar2(254),
 EXT_12              varchar2(254),
 EXT_13              varchar2(254),
 EXT_14              varchar2(254),
 EXT_15              varchar2(254),
 EXT_16              varchar2(254),
 EXT_17              varchar2(254),
 EXT_18              varchar2(254),
 EXT_19              varchar2(254),
 EXT_20              varchar2(254),
 EXT_21              varchar2(254),
 EXT_22              varchar2(254),
 EXT_23              varchar2(254),
 EXT_24              varchar2(254),
 EXT_25              varchar2(254),
 EXT_26              varchar2(254),
 EXT_27              varchar2(254),
 EXT_28              varchar2(254),
 EXT_29              varchar2(254),
 EXT_30              varchar2(254),
 EXT_31              varchar2(254),
 EXT_32              varchar2(254),
 EXT_33              varchar2(254),
 EXT_34              varchar2(254),
 EXT_35              varchar2(254),
 EXT_36              varchar2(254),
 EXT_37              varchar2(254),
 EXT_38              varchar2(254),
 EXT_39              varchar2(254),
 EXT_40              varchar2(254),
 EXT_41              varchar2(254),
 EXT_42              varchar2(254),
 EXT_43              varchar2(254),
 EXT_44              varchar2(254),
 EXT_45              varchar2(254),
 EXT_46              varchar2(254),
 EXT_47              varchar2(254),
 EXT_48              varchar2(254),
 EXT_49              varchar2(254),
 EXT_50              varchar2(254),
 EXT_51              varchar2(254),
 EXT_52              varchar2(254),
 EXT_53              varchar2(254),
 EXT_54              varchar2(254),
 EXT_55              varchar2(254),
 EXT_56              varchar2(254),
 EXT_57              varchar2(254),
 EXT_58              varchar2(254),
 EXT_59              varchar2(254),
 EXT_60              varchar2(254)
);

insert into REG_IMPORT_EXT_STAGE_STG_B select * from REG_IMPORT_EXT_STAGE_STG;

delete from REG_IMPORT_EXT_STAGE_STG;

alter table REG_IMPORT_EXT_STAGE_STG modify AMOUNT       varchar2(254);
alter table REG_IMPORT_EXT_STAGE_STG modify MONTH_DATE   varchar2(254);
alter table REG_IMPORT_EXT_STAGE_STG modify MONTH_CHAR   varchar2(254);
alter table REG_IMPORT_EXT_STAGE_STG modify COMPANY      varchar2(254);
alter table REG_IMPORT_EXT_STAGE_STG modify MONTH_NUMBER varchar2(254);

insert into REG_IMPORT_EXT_STAGE_STG select * from REG_IMPORT_EXT_STAGE_STG_B;

create table REG_IMPORT_EXT_STAGE_ARC_B
(
 IMPORT_RUN_ID       number(22,0) not null,
 LINE_ID             number(22,0) not null,
 STAGE_ID            number(22,0),
 EXT_SOURCE_ID       number(22,0),
 EXT_SOURCE_ID_XLATE varchar2(254),
 COMPANY             varchar2(254),
 MONTH_NUMBER        varchar2(254),
 MONTH_DATE          varchar2(254),
 MONTH_CHAR          varchar2(254),
 AMOUNT              varchar2(254),
 EXT_01              varchar2(254),
 EXT_02              varchar2(254),
 EXT_03              varchar2(254),
 EXT_04              varchar2(254),
 EXT_05              varchar2(254),
 EXT_06              varchar2(254),
 EXT_07              varchar2(254),
 EXT_08              varchar2(254),
 EXT_09              varchar2(254),
 EXT_10              varchar2(254),
 EXT_11              varchar2(254),
 EXT_12              varchar2(254),
 EXT_13              varchar2(254),
 EXT_14              varchar2(254),
 EXT_15              varchar2(254),
 EXT_16              varchar2(254),
 EXT_17              varchar2(254),
 EXT_18              varchar2(254),
 EXT_19              varchar2(254),
 EXT_20              varchar2(254),
 EXT_21              varchar2(254),
 EXT_22              varchar2(254),
 EXT_23              varchar2(254),
 EXT_24              varchar2(254),
 EXT_25              varchar2(254),
 EXT_26              varchar2(254),
 EXT_27              varchar2(254),
 EXT_28              varchar2(254),
 EXT_29              varchar2(254),
 EXT_30              varchar2(254),
 EXT_31              varchar2(254),
 EXT_32              varchar2(254),
 EXT_33              varchar2(254),
 EXT_34              varchar2(254),
 EXT_35              varchar2(254),
 EXT_36              varchar2(254),
 EXT_37              varchar2(254),
 EXT_38              varchar2(254),
 EXT_39              varchar2(254),
 EXT_40              varchar2(254),
 EXT_41              varchar2(254),
 EXT_42              varchar2(254),
 EXT_43              varchar2(254),
 EXT_44              varchar2(254),
 EXT_45              varchar2(254),
 EXT_46              varchar2(254),
 EXT_47              varchar2(254),
 EXT_48              varchar2(254),
 EXT_49              varchar2(254),
 EXT_50              varchar2(254),
 EXT_51              varchar2(254),
 EXT_52              varchar2(254),
 EXT_53              varchar2(254),
 EXT_54              varchar2(254),
 EXT_55              varchar2(254),
 EXT_56              varchar2(254),
 EXT_57              varchar2(254),
 EXT_58              varchar2(254),
 EXT_59              varchar2(254),
 EXT_60              varchar2(254)
);

insert into REG_IMPORT_EXT_STAGE_ARC_B select * from REG_IMPORT_EXT_STAGE_ARC;

delete from REG_IMPORT_EXT_STAGE_ARC;

alter table REG_IMPORT_EXT_STAGE_ARC modify AMOUNT       varchar2(254);
alter table REG_IMPORT_EXT_STAGE_ARC modify MONTH_DATE   varchar2(254);
alter table REG_IMPORT_EXT_STAGE_ARC modify MONTH_CHAR   varchar2(254);
alter table REG_IMPORT_EXT_STAGE_ARC modify MONTH_NUMBER varchar2(254);
alter table REG_IMPORT_EXT_STAGE_ARC modify COMPANY      varchar2(254);

insert into REG_IMPORT_EXT_STAGE_ARC select * from REG_IMPORT_EXT_STAGE_ARC_B;

drop table REG_IMPORT_EXT_STAGE_ARC_B;
drop table REG_IMPORT_EXT_STAGE_STG_B;

alter table REG_DEPR_FAMILY_COMPONENTS add ACT_DEBIT_CREDIT_BALANCE number(22,0);

-- Single Month Query
INSERT INTO "CR_DD_SOURCES_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION","FEEDER_FIELD")
select (select max(nvl(id,0)) + 1 from cr_dd_sources_criteria),
       1002, 'none', 'dynamic view', 'Balance Sheet Roll-Forward Detail', 'none'
  from dual;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
       'ACTIVITY', 7, 0, 1, null, 'Activity', 395, 0, 'Upper', 34, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
       'AMOUNT', 9, 1, 1, null, 'Amount', 358, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
       'COMPONENT', 6, 0, 1, null, 'Component', 587, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
       'GL_MONTH', 8, 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
       'HISTORIC_LEDGER', 1, 0, 1, null, 'Historic Ledger', 518, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
       'REG_ACCOUNT', 3, 0, 1, null, 'Reg Account', 1501, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
      'REG_ACCT_TYPE', 4, 0, 1, null, 'Reg Acct Type', 509, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
      'REG_COMPANY', 2, 0, 1, null, 'Reg Company', 674, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward Detail'),
       'SUB_ACCT_TYPE', 5, 0, 1, null, 'Sub Acct Type', 587, 0, 'Any', null, null, 0, null, null, 0
  from dual;

-- Time Span Query
INSERT INTO "CR_DD_SOURCES_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "FEEDER_FIELD")
select (select max(nvl(id,0)) + 1 from cr_dd_sources_criteria),
        1002, 'none', 'dynamic view', 'Balance Sheet Roll-Forward TimeSpan', 'none'
  from dual;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
        'ACTIVITY', 7, 0, 1, null, 'Activity', 212, 0, 'Upper', 34, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'AMOUNT', 8, 1, 1, null, 'Amount', 212, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'COMPONENT', 6, 0, 1, null, 'Component', 308, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'END_MONTH', 10, 0, 1, null, 'End Month', 299, 0, 'Any', null, null, 1, 1, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'HISTORIC_LEDGER', 1, 0, 1, null, 'Historic Ledger', 413, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'REG_ACCOUNT', 3, 0, 1, null, 'Reg Account', 358, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'REG_ACCT_TYPE', 4, 0, 1, null, 'Reg Acct Type', 409, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'REG_COMPANY', 2, 0, 1, null, 'Reg Company', 377, 0, 'Any', null, null, 0, null, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'START_MONTH', 9, 0, 1, null, 'Start Month', 322, 0, 'Any', null, null, 1, 1, null, 0
  from dual;
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
select (select id from cr_dd_sources_criteria where description = 'Balance Sheet Roll-Forward TimeSpan'),
       'SUB_ACCT_TYPE', 5, 0, 1, null, 'Sub Acct Type', 404, 0, 'Any', null, null, 0, null, null, 0
  from dual;

update cr_dd_sources_criteria
   set sql = 'select l.long_description historic_ledger, c.description reg_company, m.description reg_account, t.description reg_acct_type, s.description sub_acct_type, f.description component, decode(a.reg_activity_id,-1,''Begin Balance'',0,''End Balance'',d.description) activity, a.gl_month, a.act_amount amount from ( select reg_company_id, reg_acct_id, historic_version_id, gl_month, reg_component_id, reg_activity_id, act_amount, reg_activity_id sort_order from reg_history_activity union select x.reg_company_id, x.reg_acct_id, x.historic_version_id,  to_number(to_char(add_months(to_date(to_char(x.gl_month),''YYYYMM''),1),''YYYYMM'')), y.reg_component_id, -1, x.act_amount, 0 from reg_history_ledger x, reg_acct_master y where x.reg_source_id = 1 and x.reg_acct_id = y.reg_acct_id union select x.reg_company_id, x.reg_acct_id, x.historic_version_id, x.gl_month, y.reg_component_id, 0, x.act_amount, 1000 from reg_history_ledger x, reg_acct_master y where x.reg_source_id = 1 and x.reg_acct_id = y.reg_acct_id) a, reg_acct_master m, reg_acct_type t, reg_sub_acct_type s, reg_company_sv c, reg_depr_component_activity d, reg_historic_version l, reg_depr_family_components f where a.reg_company_id = c.reg_company_id and a.reg_acct_id = m.reg_acct_id and m.reg_acct_type_default = t.reg_acct_type_id and m.reg_acct_type_default = s.reg_acct_type_id and m.sub_acct_type_id = s.sub_acct_type_id and a.historic_version_id = l.historic_version_id and a.reg_component_id = d.reg_component_id (+) and a.reg_activity_id = d.reg_activity_id (+) and a.reg_component_id = f.reg_component_id '
 where description = 'Balance Sheet Roll-Forward Detail'
   and source_id = 1002;

update cr_dd_sources_criteria
   set sql = 'SELECT l.long_description historic_ledger, c.description reg_company, m.description reg_account, t.description reg_acct_type, s.description sub_acct_type, f.description component, decode(a.reg_activity_id,-1,''Begin Balance'',0,''End Balance'',d.description) activity, a.act_amount amount, sm.filter_string start_month, em.filter_string end_month FROM ( SELECT reg_company_id, reg_acct_id, historic_version_id, reg_component_id, reg_activity_id, Sum(act_amount) act_amount, reg_activity_id sort_order FROM reg_history_activity, cr_dd_required_filter sm, cr_dd_required_filter em WHERE gl_month BETWEEN sm.filter_string AND em.filter_string AND Upper(sm.column_name) = ''START_MONTH'' AND Upper(em.column_name) = ''END_MONTH'' GROUP BY reg_company_id, reg_acct_id, historic_version_id, reg_component_id, reg_activity_id UNION SELECT x.reg_company_id, x.reg_acct_id, x.historic_version_id, y.reg_component_id, -1, x.act_amount, 0 FROM reg_history_ledger x, reg_acct_master y , cr_dd_required_filter sm WHERE x.reg_source_id = 1 and x.reg_acct_id = y.reg_acct_id AND gl_month = To_Number(To_Char(Add_Months(To_Date(sm.filter_string,''yyyymm''), -1),''yyyymm'')) AND Upper(sm.column_name) = ''START_MONTH'' UNION SELECT x.reg_company_id, x.reg_acct_id, x.historic_version_id, y.reg_component_id, 0, x.act_amount, 1000 FROM reg_history_ledger x, reg_acct_master y , cr_dd_required_filter sm , cr_dd_required_filter em WHERE x.reg_source_id = 1 and x.reg_acct_id = y.reg_acct_id AND gl_month = em.filter_string AND Upper(em.column_name) = ''END_MONTH'' ) a, reg_acct_master m, reg_acct_type t, reg_sub_acct_type s, reg_company_sv c, reg_depr_component_activity d, reg_historic_version l, reg_depr_family_components f , cr_dd_required_filter sm, cr_dd_required_filter em WHERE a.reg_company_id = c.reg_company_id and a.reg_acct_id = m.reg_acct_id and m.reg_acct_type_default = t.reg_acct_type_id and m.reg_acct_type_default = s.reg_acct_type_id and m.sub_acct_type_id = s.sub_acct_type_id and a.historic_version_id = l.historic_version_id and a.reg_component_id = d.reg_component_id (+) and a.reg_activity_id = d.reg_activity_id (+) and a.reg_component_id = f.reg_component_id AND Upper(sm.column_name) = ''START_MONTH'' AND Upper(em.column_name) = ''END_MONTH'''
 where description = 'Balance Sheet Roll-Forward TimeSpan'
   and source_id = 1002;

INSERT INTO "CR_DD_SOURCES_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "FEEDER_FIELD")
VALUES ((select max(nvl(id,0)) + 1 from cr_dd_sources_criteria), 1002, 'none', 'dynamic view', 'Case Adjustment Results by Account', 'none') ;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where description = 'Case Adjustment Results by Account'), 'AMOUNT', 5, 1, 1, null, 'Amount', 409, 0, 'Any', null, null, 0, null, null, 0) ;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where description = 'Case Adjustment Results by Account'), 'AMOUNT_DESCRIPTION', 3,  0, 1, null, 'Amount Description', 875, 0, 'Any', null, null, 0, null, null, 0) ;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where description = 'Case Adjustment Results by Account'), 'CASE_NAME', 1,  0, 1, null, 'Case Name', 646, 0, 'Any', null, null, 0, null, null, 0) ;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where description = 'Case Adjustment Results by Account'), 'CASE_YEAR', 6, 0, 1, null, 'Case Year', 285, 0, 'Any', null, null, 0, null, null, 0) ;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where description = 'Case Adjustment Results by Account'), 'ORDER_OF_PROCESS', 4, 0, 1, null, 'Order Of Process', 463, 0, 'Any', null, null, 0, null, null, 0) ;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where description = 'Case Adjustment Results by Account'), 'REG_ACCOUNT', 2, 0, 1, null, 'Reg Account', 1817, 0, 'Any', null, null, 0, null, null, 0) ;

update cr_dd_sources_criteria
   set sql = 'select c.case_name case_name, m.description reg_account, x.amount_description amount_description, x.process_order order_of_process, x.adjust_amount amount, x.case_year case_year from reg_acct_master m, reg_case c,  ( select d.reg_case_id reg_case_id, a.description amount_description,  to_char(a.order_of_process) process_order, d.reg_acct_id reg_acct_id,  d.test_yr_ended case_year, d.adjust_amount adjust_amount from reg_case_adjust_detail d, reg_case_adjustment a where d.reg_case_id =  a.reg_case_id and d.adjustment_id = a.adjustment_id union select l.reg_case_id, ''Total Co Included Amount'', ''-9999999'', l.reg_acct_id, l.case_year, l.total_co_included_amount from reg_case_summary_ledger l, reg_case_adjust_detail d where l.reg_case_id = d.reg_case_id and l.reg_acct_id = d.reg_acct_id union select l.reg_case_id, ''Total Co Final Amount'', ''A'',  l.reg_acct_id, l.case_year, l.total_co_final_amount from reg_case_summary_ledger l, reg_case_adjust_detail d where l.reg_case_id = d.reg_case_id and l.reg_acct_id = d.reg_acct_id ) x where x.reg_case_id = c.reg_case_id and x.reg_acct_id = m.reg_acct_id'
 where description = 'Case Adjustment Results by Account'
   and source_id = 1002;

-- fix to include dynamic allocator factors
update cr_dd_sources_criteria
   set sql = 'select y.case_name case_name, m.description reg_account, t.description reg_acct_type, s.description sub_acct_type, c.description tax_indicator, ac.description allocation_category, x.description allocation_target, r.case_year case_year, r.amount amount, f.annual_reg_factor, f.statistical_value, Nvl(al.description , ''<none>'') allocator, f.effective_date from reg_alloc_result r, reg_acct_type t, reg_sub_acct_type s, reg_tax_control c, reg_case_acct a, reg_acct_master m, reg_case_alloc_account b, reg_alloc_target x, reg_case y , reg_allocator al , reg_case_alloc_account p , reg_alloc_category ac , (SELECT reg_allocator_id, reg_case_id, reg_alloc_category_id, reg_alloc_target_id, effective_Date, annual_reg_factor, statistical_value FROM reg_case_alloc_factor af UNION SELECT reg_allocator_id, reg_case_id, -99, reg_alloc_target_id, test_yr_end, annual_reg_factor, statistical_value FROM  reg_case_allocator_dyn_factor daf ) f where r.reg_case_id = a.reg_case_id and r.reg_acct_id = a.reg_acct_id and r.reg_case_id = b.reg_case_id and r.reg_alloc_acct_id = b.reg_alloc_acct_id and r.reg_acct_id = m.reg_acct_id and b.reg_alloc_target_id = x.reg_alloc_target_id and a.reg_acct_type_id = t.reg_acct_type_id and a.reg_acct_type_id = s.reg_acct_type_id and a.sub_acct_type_id = s.sub_acct_type_id and s.tax_control_id = c.tax_control_id (+) and r.reg_case_id = y.reg_case_id AND p.reg_allocator_id = al.reg_allocator_id (+) AND b.parent_reg_alloc_acct_id = p.reg_alloc_acct_id AND b.reg_case_id = p.reg_case_id AND p.reg_allocator_id = f.reg_allocator_id AND p.reg_case_id = f.reg_case_id AND ac.reg_alloc_category_id=r.reg_alloc_category_id AND b.reg_alloc_target_id = f.reg_alloc_target_id AND f.effective_date = (SELECT Max(xx.effective_date) FROM (SELECT reg_allocator_id, reg_case_id, reg_alloc_category_id, reg_alloc_target_id, effective_Date, annual_reg_factor, statistical_value FROM reg_case_alloc_factor af UNION SELECT reg_allocator_id, reg_case_id, -99, reg_alloc_target_id, test_yr_end, annual_reg_factor, statistical_value FROM  reg_case_allocator_dyn_factor daf ) xx WHERE xx.reg_case_id=f.reg_case_id AND xx.reg_allocator_id=f.reg_allocator_id AND xx.reg_alloc_category_id=decode(f.reg_alloc_category_id, -99, xx.reg_alloc_category_id, f.reg_alloc_category_id) AND xx.reg_alloc_target_id=f.reg_alloc_target_id AND xx.effective_date <= r.case_year )'
 where description = 'Allocation Results with Tax Control'
   and source_id = 1002;

-- Alloc Factor Import Fixes
alter table reg_IMPORT_ALLO_FACTOR_STG_ARC drop column REG_CASE_ID;

alter table REG_IMPORT_ALLO_FACTOR_STG_ARC add REG_JUR_TEMPLATE_ID       number(22,0);
alter table REG_IMPORT_ALLO_FACTOR_STG_ARC add REG_JUR_TEMPLATE_ID_XLATE varchar2(254);
alter table REG_IMPORT_ALLO_FACTOR_STG_ARC add REG_CASE_ID               number(22,0);
alter table REG_IMPORT_ALLO_FACTOR_STG_ARC add REG_CASE_ID_XLATE         varchar2(254);

-- External Balances Temp Table
create global temporary table REG_EXT_LOAD_BAL_TEMP
(
 REG_COMPANY_ID      number(22,0) not null,
 REG_ACCT_ID         number(22,0) not null,
 HISTORIC_VERSION_ID number(22,0) not null,
 FORECAST_VERSION_ID number(22,0) not null,
 MONTH               number(22,1) not null,
 AMOUNT              number(22,2)
) on commit delete rows;

alter table REG_EXT_LOAD_BAL_TEMP
   add constraint PK_REG_EXT_LOAD_BAL_TEMP
       primary key (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, FORECAST_VERSION_ID, MONTH);

-- Fix Company Security View
create or replace view REG_COMPANY_SV
(VALID_USER_ID, REG_COMPANY_ID, DESCRIPTION, PP_COMPANY_ID, CR_COMPANY_ID,
 CR_EXTERNAL_COMPANY_ID, STATUS_CODE_ID)
as
select RCS.VALID_USER_ID,
       RC.REG_COMPANY_ID,
       RC.DESCRIPTION,
       I.PP_COMPANY_ID,
       CR_COMPANY_ID,
       CR_EXTERNAL_COMPANY_ID,
       RC.STATUS_CODE_ID
  from REG_COMPANY RC,
       REG_COMPANY_SECURITY RCS,
       (select PCS.USERS              VALID_USER_ID,
               CS.REG_COMPANY_ID      REG_COMPANY_ID,
               CS.COMPANY_ID          PP_COMPANY_ID,
               CC.CR_COMPANY_ID       CR_COMPANY_ID,
               CC.EXTERNAL_COMPANY_ID CR_EXTERNAL_COMPANY_ID
          from COMPANY_SETUP CS, PP_COMPANY_SECURITY PCS, CR_COMPANY CC, CR_COMPANY_SECURITY CCS
         where CS.COMPANY_ID = CC.POWERPLANT_COMPANY_ID
           and CS.COMPANY_ID = PCS.COMPANY_ID
           and CC.EXTERNAL_COMPANY_ID = CCS.VALID_VALUES
           and CCS.USERS = PCS.USERS
           and PCS.USERS = LOWER(user)) I
 where RC.REG_COMPANY_ID = RCS.VALID_COMPANY_ID
   and RC.REG_COMPANY_ID = I.REG_COMPANY_ID(+)
   and LOWER(RCS.VALID_USER_ID) = LOWER(user);

comment on column REG_COMPANY_SV.REG_COMPANY_ID is 'SYSTEM-ASSIGNED IDENTIFIER OF A REGULATORY COMPANY.';
comment on column REG_COMPANY_SV.DESCRIPTION is 'DESCRIPTION OF THE REGULATORY COMPANY.';
comment on column REG_COMPANY_SV.PP_COMPANY_ID is 'POWERPLAN COMPANY ID (FROM COMPANY SETUP)';
comment on column REG_COMPANY_SV.CR_EXTERNAL_COMPANY_ID is 'CR ELEMENT ID FROM LIST OF ELEMENTS USED FOR RECONCILIATION';
comment on column REG_COMPANY_SV.STATUS_CODE_ID is 'ACTIVE = 1; INACTIVE = 2';


create or replace view REG_HISTORY_LEDGER_SV
(REG_COMPANY, REG_ACCOUNT, REG_ACCOUNT_TYPE, SUB_ACCOUNT_TYPE, REG_SOURCE, HISTORIC_LEDGER,
 GL_MONTH, ACT_AMOUNT, ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       HISTORIC_LEDGER,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH
  from (select C.DESCRIPTION      REG_COMPANY,
               M.DESCRIPTION      REG_ACCOUNT,
               T.DESCRIPTION      REG_ACCOUNT_TYPE,
               S.DESCRIPTION      SUB_ACCOUNT_TYPE,
               R.DESCRIPTION      REG_SOURCE,
               V.LONG_DESCRIPTION HISTORIC_LEDGER,
               L.GL_MONTH         GL_MONTH,
               L.ACT_AMOUNT       ACT_AMOUNT,
               L.ANNUALIZED_AMT   ANNUALIZED_AMT,
               L.ADJ_AMOUNT       ADJ_AMOUNT,
               L.ADJ_MONTH        ADJ_MONTH
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER L,
               REG_ACCT_TYPE T,
               REG_SUB_ACCT_TYPE S,
               REG_SOURCE R
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);


create or replace view REG_HISTORY_LEDGER_ID_SV
(REG_COMPANY, REG_ACCOUNT, REG_ACCOUNT_TYPE, SUB_ACCOUNT_TYPE, REG_SOURCE, HISTORIC_LEDGER, GL_MONTH, ACT_AMOUNT,
 ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH, REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_LEDGER_ID, REG_SOURCE_ID)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       HISTORIC_LEDGER,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       HISTORIC_LEDGER_ID,
       REG_SOURCE_ID
  from (select C.DESCRIPTION         REG_COMPANY,
               M.DESCRIPTION         REG_ACCOUNT,
               T.DESCRIPTION         REG_ACCOUNT_TYPE,
               S.DESCRIPTION         SUB_ACCOUNT_TYPE,
               R.DESCRIPTION         REG_SOURCE,
               V.LONG_DESCRIPTION    HISTORIC_LEDGER,
               L.GL_MONTH            GL_MONTH,
               L.ACT_AMOUNT          ACT_AMOUNT,
               L.ANNUALIZED_AMT      ANNUALIZED_AMT,
               L.ADJ_AMOUNT          ADJ_AMOUNT,
               L.ADJ_MONTH           ADJ_MONTH,
               L.REG_COMPANY_ID      REG_COMPANY_ID,
               L.REG_ACCT_ID         REG_ACCT_ID,
               L.HISTORIC_VERSION_ID HISTORIC_LEDGER_ID,
               L.REG_SOURCE_ID       REG_SOURCE_ID
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER L,
               REG_ACCT_TYPE T,
               REG_SUB_ACCT_TYPE S,
               REG_SOURCE R
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);


create or replace view REG_FORECAST_LEDGER_SV
(REG_COMPANY, REG_ACCOUNT, REG_SOURCE, FORECAST_LEDGER, GL_MONTH,
 FCST_AMOUNT, ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH)
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_SOURCE,
       FORECAST_LEDGER,
       GL_MONTH,
       FCST_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH
  from (select C.DESCRIPTION      REG_COMPANY,
               M.DESCRIPTION      REG_ACCOUNT,
               R.DESCRIPTION      REG_SOURCE,
               V.LONG_DESCRIPTION FORECAST_LEDGER,
               L.GL_MONTH         GL_MONTH,
               L.FCST_AMOUNT      FCST_AMOUNT,
               L.ANNUALIZED_AMT   ANNUALIZED_AMT,
               L.ADJ_AMOUNT       ADJ_AMOUNT,
               L.ADJ_MONTH        ADJ_MONTH
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_ACCT_MASTER M,
               REG_FORECAST_VERSION V,
               REG_FORECAST_LEDGER L,
               REG_SOURCE R
         where L.FORECAST_VERSION_ID = V.FORECAST_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and L.REG_SOURCE_ID = R.REG_SOURCE_ID);

-- REG_HISTORIC_RECON_LEDGER_SV
create or replace view REG_HISTORIC_RECON_LEDGER_SV
as
select HISTORIC_LEDGER, RECON_ITEM, GL_MONTH, REG_COMPANY, CR_BALANCES_AMT, HIST_REG_ACCT_AMT
  from (select V.LONG_DESCRIPTION  HISTORIC_LEDGER,
               R.DESCRIPTION       RECON_ITEM,
               L.GL_MONTH          GL_MONTH,
               C.DESCRIPTION       REG_COMPANY,
               L.CR_BALANCES_AMT   CR_BALANCES_AMT,
               L.HIST_REG_ACCT_AMT HIST_REG_ACCT_AMT
          from (select distinct REG_COMPANY_ID, DESCRIPTION from REG_COMPANY_SV) C,
               REG_HIST_RECON_ITEMS R,
               REG_HISTORIC_VERSION V,
               REG_HISTORIC_RECON_LEDGER L
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.RECON_ID = R.RECON_ID);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (890, 0, 10, 4, 2, 0, 33713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033713_reg_09_incremental_scripts_201312.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
