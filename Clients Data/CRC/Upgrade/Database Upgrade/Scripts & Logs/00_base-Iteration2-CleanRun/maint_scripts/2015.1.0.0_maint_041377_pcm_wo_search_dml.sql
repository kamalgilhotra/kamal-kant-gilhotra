/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041377_pcm_wo_search_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	12/02/2014 Ryan Oliveria  Set up WO Searching
||============================================================================
*/

--* Workspace reference
update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_pcm_wo_wksp_search'
 where MODULE = 'pcm'
   and WORKSPACE_IDENTIFIER = 'wo_search';


--* Grid Selection System Options
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
values
	('Work Order Search - Grid View', 'The default way to display work orders in a search grid.', 0, '1 - Standard WO Grid', null, 1, 0);

insert into PPBASE_SYSTEM_OPTIONS_MODULE (SYSTEM_OPTION_ID, MODULE) values ('Work Order Search - Grid View', 'pcm');

insert into PPBASE_SYSTEM_OPTIONS_WKSP (SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER) values ('Work Order Search - Grid View', 'wo_search');

insert into PPBASE_SYSTEM_OPTIONS_VALUES (SYSTEM_OPTION_ID, OPTION_VALUE) values ('Work Order Search - Grid View', '1 - Standard WO Grid');
insert into PPBASE_SYSTEM_OPTIONS_VALUES (SYSTEM_OPTION_ID, OPTION_VALUE) values ('Work Order Search - Grid View', '2 - WO Main Sets of Books');
insert into PPBASE_SYSTEM_OPTIONS_VALUES (SYSTEM_OPTION_ID, OPTION_VALUE) values ('Work Order Search - Grid View', '3 - Simple WO Grid');



--* Reports Filters
update PP_REPORTS_FILTER
   set FILTER_UO_NAME = 'uo_pcm_fp_wo_tab_search'
 where PP_REPORT_FILTER_ID = 86;

insert into PP_REPORTS_FILTER
	(PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
	(87, 'PCM - Work Orders', null, 'uo_pcm_fp_wo_tab_search');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2087, 0, 2015, 1, 0, 0, 041377, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041377_pcm_wo_search_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;