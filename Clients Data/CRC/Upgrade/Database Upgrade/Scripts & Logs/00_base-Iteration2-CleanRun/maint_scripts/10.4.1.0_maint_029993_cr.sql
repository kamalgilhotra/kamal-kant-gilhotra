/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_029993_cr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   05/13/2013 Stephen Bynoe  Point Release
||============================================================================
*/

-- Drop column exclude_negatives bdg on cr_allocation_control

alter table CR_ALLOCATION_CONTROL drop column EXCLUDE_NEGATIVES_BDG;


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (382, 0, 10, 4, 1, 0, 29993, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029993_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

