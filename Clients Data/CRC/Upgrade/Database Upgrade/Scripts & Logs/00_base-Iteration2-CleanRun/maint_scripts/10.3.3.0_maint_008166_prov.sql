/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008166_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/03/2011 Blake Andrews  Point Release
||============================================================================
*/

--###Patch(8166)
insert into TAX_ACCRUAL_TRUEUP
   (TRUEUP_ID, DESCRIPTION, M_ID, BY_OPER_IND, SPREAD_REMAINING_ESTIMATE, TRUEUP_INCLUDES_INPUT,
    TRUEUP_INCLUDES_INPUT_DT)
values
   (101, 'GL Override - Hold Annual Estimate', -2, 0, 1, 1, 0);

insert into TAX_ACCRUAL_TRUEUP
   (TRUEUP_ID, DESCRIPTION, M_ID, BY_OPER_IND, SPREAD_REMAINING_ESTIMATE, TRUEUP_INCLUDES_INPUT,
    TRUEUP_INCLUDES_INPUT_DT)
values
   (102, 'GL Override - Hold Estimated Months', -1, 0, 0, 0, 0);

insert into TAX_ACCRUAL_TRUEUP
   (TRUEUP_ID, DESCRIPTION, M_ID, BY_OPER_IND, SPREAD_REMAINING_ESTIMATE, TRUEUP_INCLUDES_INPUT,
    TRUEUP_INCLUDES_INPUT_DT)
values
   (103, 'GL Override', -2, 0, 0, 0, 0);

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select MONTH_TYPE_ID,
          TRUEUP_ID + 100,
          FROM_PT_IND,
          CURRENT_MONTH_TRUEUP,
          OUT_MONTH_TRUEUP,
          CURRENT_MONTH_TRUEUP_DT,
          OUT_MONTH_TRUEUP_DT,
          PULL_ACTUALS
     from TAX_ACCRUAL_MONTH_TYPE_TRUEUP
    where TRUEUP_ID in (1, 2, 3);

update TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   set PULL_ACTUALS = 1
 where TRUEUP_ID in (101, 102, 103)
   and MONTH_TYPE_ID = 100;

update TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   set PULL_ACTUALS = 0
 where FROM_PT_IND = 3
   and MONTH_TYPE_ID = 100
   and TRUEUP_ID not in (101, 102, 103);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (6, 0, 10, 3, 3, 0, 8166, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008166_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
