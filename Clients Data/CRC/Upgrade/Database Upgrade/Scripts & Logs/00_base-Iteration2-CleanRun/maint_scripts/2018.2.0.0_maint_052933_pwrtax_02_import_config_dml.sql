/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052933_pwrtax_02_import_config_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/05/2019 Shane "C" Ward   Updates to include Asset ID on PowerTax Imports
||============================================================================
*/

insert into pp_import_column
  (import_type_id, column_name, description, import_column_name,
   is_required, processing_order, column_type, is_on_table, default_value)
values
  (212, 'asset_id', 'Asset ID', 'asset_xlate', 0, 1, 'number(22,0)', 1, 0);
  
insert into pp_import_column
  (import_type_id, column_name, description, import_column_name,
   is_required, processing_order, column_type, is_on_table, default_value)
values
  (213, 'asset_id', 'Asset ID', 'asset_xlate', 0, 1, 'number(22,0)', 1, 0);
  
insert into pp_import_column
  (import_type_id, column_name, description, import_column_name,
   is_required, processing_order, column_type, is_on_table, default_value)
values
  (214, 'asset_id', 'Asset ID', 'asset_xlate', 0, 1, 'number(22,0)', 1, 0);
 
insert into pp_import_column
  (import_type_id, column_name, description, import_column_name,
   is_required, processing_order, column_type, is_on_table, default_value)
values
  (224, 'asset_id', 'Asset ID', 'asset_xlate', 0, 1, 'number(22,0)', 1, 0);

insert into pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
values
  (212, 'asset_id', 187);
insert into pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
values
  (213, 'asset_id', 187);
insert into pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
values
  (214, 'asset_id', 187);
insert into pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
values
  (224, 'asset_id', 187);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15684, 0, 2018, 2, 0, 0, 52933, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052933_pwrtax_02_import_config_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;