/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036053_reg_calc_tax_accts.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/31/2014 Sarah Byers
||============================================================================
*/

insert into REG_SUB_ACCT_TYPE
   (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER, TAX_CONTROL_ID)
values
   (11, 1, 'Rate Base Tax Effect', 'Rate Base Tax Effect', 1, 7);
insert into REG_SUB_ACCT_TYPE
   (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER, TAX_CONTROL_ID)
values
   (11, 2, 'Net Operating Income Tax Effect', 'Net Operating Income Tax Effect', 2, 8);
insert into REG_SUB_ACCT_TYPE
   (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER, TAX_CONTROL_ID)
values
   (11, 3, 'Non-Utility Rate Base Tax Effect', 'Non-Utility Rate Base Tax Effect', 3, 7);
insert into REG_SUB_ACCT_TYPE
   (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SORT_ORDER, TAX_CONTROL_ID)
values
   (11, 4, 'Non-Utility NOI Tax Effect', 'Non-Utility NOI Tax Effect', 4, 8);

insert into REG_ACCT_MASTER
   (REG_ACCT_ID, DESCRIPTION, LONG_DESCRIPTION, REG_SOURCE_ID, REG_ACCT_TYPE_DEFAULT,
    SUB_ACCT_TYPE_ID, REG_ANNUALIZATION_ID, ACCT_GOOD_FOR)
   select PPSEQ_REG_ACCT_ID.NEXTVAL,
          'Calc Tax Effect: Rate Base',
          'Calculated Tax Effect for Utility Rate Base',
          0,
          11,
          1,
          1,
          4
     from DUAL;
insert into REG_ACCT_MASTER
   (REG_ACCT_ID, DESCRIPTION, LONG_DESCRIPTION, REG_SOURCE_ID, REG_ACCT_TYPE_DEFAULT,
    SUB_ACCT_TYPE_ID, REG_ANNUALIZATION_ID, ACCT_GOOD_FOR)
   select PPSEQ_REG_ACCT_ID.NEXTVAL,
          'Calc Tax Effect: Operating Income',
          'Calculated Tax Effect for Utilty Operating Income',
          0,
          11,
          2,
          1,
          4
     from DUAL;
insert into REG_ACCT_MASTER
   (REG_ACCT_ID, DESCRIPTION, LONG_DESCRIPTION, REG_SOURCE_ID, REG_ACCT_TYPE_DEFAULT,
    SUB_ACCT_TYPE_ID, REG_ANNUALIZATION_ID, ACCT_GOOD_FOR)
   select PPSEQ_REG_ACCT_ID.NEXTVAL,
          'Calc Tax Effect: Non-Util Rate Base',
          'Calculated Tax Effect for Non-Utility Rate Base',
          0,
          11,
          3,
          1,
          4
     from DUAL;
insert into REG_ACCT_MASTER
   (REG_ACCT_ID, DESCRIPTION, LONG_DESCRIPTION, REG_SOURCE_ID, REG_ACCT_TYPE_DEFAULT,
    SUB_ACCT_TYPE_ID, REG_ANNUALIZATION_ID, ACCT_GOOD_FOR)
   select PPSEQ_REG_ACCT_ID.NEXTVAL,
          'Calc Tax Effect: Non-Util Operating Income',
          'Calculated Tax Effect for Non-Utility Operating Income',
          0,
          11,
          4,
          1,
          4
     from DUAL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (934, 0, 10, 4, 2, 0, 36053, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036053_reg_calc_tax_accts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;