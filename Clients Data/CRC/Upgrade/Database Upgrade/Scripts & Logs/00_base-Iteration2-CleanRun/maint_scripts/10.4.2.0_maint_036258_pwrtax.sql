/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036258_pwrtax.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/05/2014 Julia Breuer
||============================================================================
*/

--
-- Add CPR Ledger fields to the Asset Grid for Individual Depreciation.
--
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (95, 'Asset Location', 'dw', 'tax_record_control.asset_id in (select asset_id from cpr_ledger where asset_location_id', '[selected_values])', 'dw_pp_asset_location_filter', 'asset_location_id', 'long_description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (96, 'Business Segment', 'dw', 'tax_record_control.asset_id in (select asset_id from cpr_ledger where bus_segment_id', '[selected_values])', 'dw_pp_business_segment_filter', 'bus_segment_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (97, 'Utility Account', 'dw', 'tax_record_control.asset_id in (select asset_id from cpr_ledger where utility_account_id', '[selected_values])', 'dw_pp_utility_account_filter', 'utility_account_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (98, 'Sub Account', 'dw', 'tax_record_control.asset_id in (select asset_id from cpr_ledger where sub_account_id', '[selected_values])', 'dw_pp_sub_account_filter', 'sub_account_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );

insert into pwrplant.pp_dynamic_filter_restrictions ( restriction_id, filter_id, restrict_by_filter_id, sqls_column_expression, sqls_where_clause, user_id, time_stamp ) values ( 50, 97, 96, 'utility_account.bus_segment_id', '', user, sysdate );
insert into pwrplant.pp_dynamic_filter_restrictions ( restriction_id, filter_id, restrict_by_filter_id, sqls_column_expression, sqls_where_clause, user_id, time_stamp ) values ( 51, 98, 96, 'sub_account.bus_segment_id', '', user, sysdate );
insert into pwrplant.pp_dynamic_filter_restrictions ( restriction_id, filter_id, restrict_by_filter_id, sqls_column_expression, sqls_where_clause, user_id, time_stamp ) values ( 52, 98, 97, 'sub_account.utility_account_id', '', user, sysdate );

insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 49, 95, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 49, 96, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 49, 97, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 49, 98, user, sysdate );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (946, 0, 10, 4, 2, 0, 36258, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036258_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;