/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037945_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 05/07/2014 Julia Breuer
||============================================================================
*/

--
-- Add the Archive workspace to PowerTax.
--
insert into pwrplant.ppbase_workspace ( module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp ) values ( 'powertax', 'case_archive', sysdate, user, 'Archive', 'uo_tax_case_wksp_archive', 'Archive' );
update pwrplant.ppbase_menu_items set workspace_identifier = 'case_archive' where module = 'powertax' and menu_identifier = 'case_archive';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1151, 0, 10, 4, 3, 0, 37945, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037945_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
