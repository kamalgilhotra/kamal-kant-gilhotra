/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053087_lessee_01_ilr_acct_month_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.1.0.2 02/11/2019 C. Yura        backfill new ls_ilr_approval fields
||============================================================================
*/

BEGIN
   EXECUTE IMMEDIATE 'alter table LS_ILR_APPROVAL add acct_month_approved date';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table LS_ILR_APPROVAL add revision_app_order number(22,0)';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

comment on column LS_ILR_APPROVAL.acct_month_approved is 'Accounting Month that ILR Revision Was Approved/Posted';
comment on column LS_ILR_APPROVAL.revision_app_order is 'Order that Revision was Approved';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15004, 0, 2018, 2, 0, 0, 53087, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053087_lessee_01_ilr_acct_month_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;



