/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047384_lease_add_iso_code_currency_col_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/10/2017 Jared Watkins    add new column to store the 3-character ISO code for a currency 
||============================================================================
*/
alter table currency 
add iso_code varchar2(3);

comment on column currency.iso_code is 'The 3-character ISO standard code used to denote this currency';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3427, 0, 2017, 1, 0, 0, 47384, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047384_lease_add_iso_code_currency_col_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
