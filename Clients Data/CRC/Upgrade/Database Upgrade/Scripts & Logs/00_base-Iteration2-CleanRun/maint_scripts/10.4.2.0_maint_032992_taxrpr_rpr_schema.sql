/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032992_taxrpr_rpr_schema.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/04/2014 Alex P.
||============================================================================
*/

alter table REPAIR_CALC_CWIP  modify REPAIR_SCHEMA_ID number(22,0) not null;
alter table REPAIR_CALC_ASSET modify REPAIR_SCHEMA_ID number(22,0) not null;

alter table REPAIR_MAJOR_UNIT_PCT         add constraint FK_RMUP_REPAIR_SCHEMA foreign key (REPAIR_SCHEMA_ID) references REPAIR_SCHEMA;
alter table REPAIR_BLANKET_PROCESS_REPORT add constraint FK_RBPR_REPAIR_SCHEMA foreign key (REPAIR_SCHEMA_ID) references REPAIR_SCHEMA;
alter table REPAIR_WO_SEG_REPORTING       add constraint FK_RWSR_REPAIR_SCHEMA foreign key (REPAIR_SCHEMA_ID) references REPAIR_SCHEMA;
alter table REPAIR_CALC_CWIP              add constraint FK_RCC_REPAIR_SCHEMA  foreign key (REPAIR_SCHEMA_ID) references REPAIR_SCHEMA;
alter table REPAIR_CALC_ASSET             add constraint FK_RCA_REPAIR_SCHEMA  foreign key (REPAIR_SCHEMA_ID) references REPAIR_SCHEMA;
alter table REPAIR_BATCH_CONTROL          add constraint FK_RBC_REPAIR_SCHEMA  foreign key (REPAIR_SCHEMA_ID) references REPAIR_SCHEMA;

alter table WO_TAX_STATUS drop constraint WO_TAX_STATUS_REPAIR_SCHEMA;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1008, 0, 10, 4, 2, 0, 32992, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032992_taxrpr_rpr_schema.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
