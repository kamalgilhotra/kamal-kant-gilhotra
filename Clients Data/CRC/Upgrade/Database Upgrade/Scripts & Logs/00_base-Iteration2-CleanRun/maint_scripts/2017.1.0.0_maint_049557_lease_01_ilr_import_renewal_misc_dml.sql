/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049557_lease_01_ilr_import_renewal_misc_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/10/2017 Shane "C" Ward		Set up Lessee ILR Renewal Import Tables
||============================================================================
*/

UPDATE PP_IMPORT_COLUMN
SET    is_required = 1
WHERE  column_name IN ( 'ilr_id', 'ilr_renewal_option_id', 'ilr_renewal_prob_id' ) AND
       import_type_id = 515;

UPDATE PP_IMPORT_COLUMN
SET    is_required = 0
WHERE  column_name NOT IN ( 'ilr_id', 'ilr_renewal_option_id', 'ilr_renewal_prob_id' ) AND
       import_type_id = 515;

UPDATE PP_IMPORT_COLUMN
SET    is_required = 1
WHERE  column_name IN ( 'ilr_id', 'ilr_renewal_option_id', 'ilr_renewal_prob_id' ) AND
       import_type_id = 265;

UPDATE PP_IMPORT_COLUMN
SET    is_required = 0
WHERE  column_name NOT IN ( 'ilr_id', 'ilr_renewal_option_id', 'ilr_renewal_prob_id' ) AND
       import_type_id = 265;

UPDATE PP_IMPORT_COLUMN
SET    column_type = 'date mmddyyyy format'
WHERE  import_type_id = 265 AND
       column_name = 'renewal_start_date';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(3981, 0, 2017, 1, 0, 0, 49557, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049557_lease_01_ilr_import_renewal_misc_dml.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
