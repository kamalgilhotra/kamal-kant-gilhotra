/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008656_sys_wkflow_tab_update.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/06/2013 David Nichols  Point Release
||============================================================================
*/

/*
Adding user_id and time_stamp columns and triggers to:
   Workflow
   Workflow Detail
   Workflow Subsystem
   Workflow Type
   Workflow Rule
   Workflow Type Rule
   Workflow Amount SQL
*/

alter table WORKFLOW            add (USER_ID varchar2(18), TIME_STAMP date);
alter table WORKFLOW_DETAIL     add (USER_ID varchar2(18), TIME_STAMP date);
alter table WORKFLOW_SUBSYSTEM  add (USER_ID varchar2(18), TIME_STAMP date);
alter table WORKFLOW_TYPE       add (USER_ID varchar2(18), TIME_STAMP date);
alter table WORKFLOW_RULE       add (USER_ID varchar2(18), TIME_STAMP date);
alter table WORKFLOW_TYPE_RULE  add (USER_ID varchar2(18), TIME_STAMP date);
alter table WORKFLOW_AMOUNT_SQL add (USER_ID varchar2(18), TIME_STAMP date);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (495, 0, 10, 4, 1, 0, 8656, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_008656_sys_wkflow_tab_update.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

