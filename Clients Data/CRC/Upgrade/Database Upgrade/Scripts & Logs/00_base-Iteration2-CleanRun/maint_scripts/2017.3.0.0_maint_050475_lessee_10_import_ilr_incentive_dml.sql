/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_10_import_ilr_incentive_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/25/2018 Shane "C" Ward    Create import setup for Lessee Incentive Import
||============================================================================
*/
--New Import Type
INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name,
             archive_additional_columns)
VALUES      (267,
             'Add: ILR Incentives',
             'Lessee ILR Incentives',
             'LS_IMPORT_ILR_INCENTIVE',
             'LS_IMPORT_ILR_INCENTIVE_ARC',
             1,
             'nvo_ls_logic_import',
             NULL);

--Associate new Import Type
INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_type_id,
             import_subsystem_id)
VALUES      (267,
             8);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 267,
             'ilr_id',
             'ILR ID',
             'ilr_id_xlate',
             1,
             1,
             'number(22,0)',
             'ls_ilr',
             1,
             'ilr_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 267,
             'revision',
             'Revision',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 267,
             'description',
             'description',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 267,
             'incentive_group_id',
             'Incentive Group ID',
             'incentive_group_id_xlate',
             1,
             1,
             'number(22,0)',
             'ls_incentive_group',
             1,
             'incentive_group_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 267,
             'date_incurred',
             'Date Incurred',
             NULL,
             0,
             1,
             'date mm/dd/yyyy format',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 267,
             'amount',
             'Amount',
             NULL,
             1,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

--New Lookups IDCG
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1115,
             'Ls Incentive Group.Description',
             'Lessee Incentive Group Description',
             'incentive_group_id',
             '( select distinct b.incentive_group_id from ls_incentive_group b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )',
             0,
             'ls_incentive_group',
             'description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1116,
             'Ls IDC Group.Long Description',
             'Lessee Initial Direct Cost Group Long Description',
             'incentive_group_id',
             '( select distinct b.incentive_group_id from ls_incentive_group b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )',
             0,
             'ls_incentive_group',
             'long_description');

--Associate Lookups
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (267,
             'incentive_group_id',
             1115);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (267,
             'incentive_group_id',
             1116);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             import_lookup_id,
             column_name)
VALUES      (267,
             1096,
             'ilr_id');

--Default Template
INSERT INTO PP_IMPORT_TEMPLATE
            (import_template_id,
             import_type_id,
             description,
             long_description,
             do_update_with_add,
             is_autocreate_template)
VALUES      (( SELECT Max(import_template_id) + 1
               FROM   PP_IMPORT_TEMPLATE ),
             267,
             'Add: ILR Incentives',
             'Default Template for Adding Initial Direct Costs to ILRs',
             1,
             0);

--Default Template Fields
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 267 AND
                      description = 'Add: ILR Incentives' ),
             1,
             267,
             'ilr_id',
             1096);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 267 AND
                      description = 'Add: ILR Incentives' ),
             2,
             267,
             'revision',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 267 AND
                      description = 'Add: ILR Incentives' ),
             3,
             267,
             'incentive_group_id',
             1115);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 267 AND
                      description = 'Add: ILR Incentives' ),
             4,
             267,
             'description',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 267 AND
                      description = 'Add: ILR Incentives' ),
             5,
             267,
             'date_incurred',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 267 AND
                      description = 'Add: ILR Incentives' ),
             6,
             267,
             'amount',
             NULL);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4627, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_10_import_ilr_incentive_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;