/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041861_prov_etr_compare_ddl.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 12/23/2014 Blake Andrews    Create new ETR Comparison report
||============================================================================
*/

comment on column TAX_ACCRUAL_REP_CONS_TYPE.BALANCES_IND
is '2 = The final column is a difference instead of total column.  1 = Display a beginning balance and ending balance column.  0 = Do not display a beginning balance and ending balance column.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2138, 0, 2015, 1, 0, 0, 041861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041861_prov_etr_compare_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;