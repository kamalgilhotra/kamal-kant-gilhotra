/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034553_sys_GET_PEND_SOB_ACTV_CODE.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.2.0   01/07/2014 Charlie Shilling add error handling to depr method rates query
||============================================================================
*/
create or replace function GET_PEND_SOB_ACTV_CODE(P_LDG_ASSET_ID       PEND_TRANSACTION.LDG_ASSET_ID%type,
                                                  P_SET_OF_BOOKS_ID    SET_OF_BOOKS.SET_OF_BOOKS_ID%type,
                                                  P_GL_POSTING_MO_YR   PEND_TRANSACTION.GL_POSTING_MO_YR%type,
                                                  P_ACTIVITY_CODE      PEND_TRANSACTION.ACTIVITY_CODE%type,
                                                  P_POSTING_AMOUNT     PEND_TRANSACTION.POSTING_AMOUNT%type,
                                                  P_GAIN_LOSS_REVERSAL PEND_TRANSACTION.GAIN_LOSS_REVERSAL%type)
   return PEND_TRANSACTION.ACTIVITY_CODE%type is

   V_ACTIVITY_CODE       PEND_TRANSACTION.ACTIVITY_CODE%type;
   V_GL_DEFAULT          DEPR_METHOD_RATES.GAIN_LOSS_DEFAULT%type;
   V_DEPR_GROUP_ID       DEPR_GROUP.DEPR_GROUP_ID%type;
   V_DEPR_INDICATOR      SUBLEDGER_CONTROL.DEPRECIATION_INDICATOR%type;
   V_SUBLEDGER_INDICATOR CPR_LEDGER.SUBLEDGER_INDICATOR%type;
   V_CHECK_COUNT         number(22, 0);

begin
   V_ACTIVITY_CODE := trim(UPPER(P_ACTIVITY_CODE));

   if V_ACTIVITY_CODE = 'URET' or V_ACTIVITY_CODE = 'URGL' or V_ACTIVITY_CODE = 'SALE' or
      V_ACTIVITY_CODE = 'SAGL' then

      -- protect against RWIP Closeout (ldg_asset_id -999) and COR transactions with ldg_asset_id = NULL,
      --   neither of which will never be gain_loss transactions
      if P_LDG_ASSET_ID = -999 or P_LDG_ASSET_ID is null then
         if V_ACTIVITY_CODE = 'URET' or V_ACTIVITY_CODE = 'URGL' then
            return 'URET';
         else
            return 'SALE';
         end if;
      end if;

      --if posting_amount is zero, check if there is a gain_loss_reversal that we need to take into account
      if P_POSTING_AMOUNT = 0 then
         begin
            select count(1)
              into V_CHECK_COUNT
              from WO_ACCRUED_GAIN_LOSS
             where ASSET_ID = P_LDG_ASSET_ID
               and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID;
         exception
            when NO_DATA_FOUND then
               if V_ACTIVITY_CODE = 'URET' or V_ACTIVITY_CODE = 'URGL' then
                  return 'URET';
               else
                  return 'SALE';
               end if;
         end;
      end if;

      --these transactions might be GL or not. get depr_group, subledger_indicator, and depr_indicator
      begin
         select A.DEPR_GROUP_ID, B.DEPRECIATION_INDICATOR, A.SUBLEDGER_INDICATOR
           into V_DEPR_GROUP_ID, V_DEPR_INDICATOR, V_SUBLEDGER_INDICATOR
           from CPR_LEDGER A, SUBLEDGER_CONTROL B
          where A.SUBLEDGER_INDICATOR = B.SUBLEDGER_TYPE_ID
            and A.ASSET_ID = P_LDG_ASSET_ID;
      exception
         when NO_DATA_FOUND then
            RAISE_APPLICATION_ERROR(-20006,
                                    'No CPR asset found for ldg_asset_id ' ||
                                    'on pend_transaction. ldg_asset_id: ' || P_LDG_ASSET_ID);
      end;

      if V_SUBLEDGER_INDICATOR = 0 then
         --group depreciated - check GL default on depr_method_Rates
         begin
            select NVL(B.GAIN_LOSS_DEFAULT, 0)
              into V_GL_DEFAULT
              from DEPR_GROUP A, DEPR_METHOD_RATES B
             where A.DEPR_GROUP_ID = V_DEPR_GROUP_ID
               and A.DEPR_METHOD_ID = B.DEPR_METHOD_ID
               and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
               and B.EFFECTIVE_DATE =
                   (select max(EFFECTIVE_DATE)
                      from DEPR_METHOD_RATES M
                     where M.DEPR_METHOD_ID = B.DEPR_METHOD_ID
                       and M.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
                       and M.EFFECTIVE_DATE <= P_GL_POSTING_MO_YR);
         exception
            when NO_DATA_FOUND then
               RAISE_APPLICATION_ERROR(-20007,
                                       'No record found in DEPR_METHOD_RATES for depr_group_id ' ||
                                       V_DEPR_GROUP_ID || ' set_of_books_id ' || P_SET_OF_BOOKS_ID ||
                                       ' and gl_posting_mo_yr ' || P_GL_POSTING_MO_YR || '.');
         end;
      else
         --individually depreciated - check depreciation_indicator to make sure this isn't an old-style subledger
         if V_DEPR_INDICATOR <> 3 then
            V_GL_DEFAULT := 0;
         else
            V_GL_DEFAULT := 1;
         end if;
      end if;

      --set activity_code based on pend_trans activity_code and gain loss default
      if V_ACTIVITY_CODE = 'URET' or V_ACTIVITY_CODE = 'URGL' then
         if V_GL_DEFAULT = 0 then
            return 'URET';
         else
            return 'URGL';
         end if;
      else
         if V_GL_DEFAULT = 0 then
            return 'SALE';
         else
            return 'SAGL';
         end if;
      end if;
   else
      --all other transactions will not change activity code
      return V_ACTIVITY_CODE;
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (843, 0, 10, 4, 2, 0, 34553, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034553_sys_GET_PEND_SOB_ACTV_CODE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;