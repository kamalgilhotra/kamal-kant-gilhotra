/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045559_reg_schedule_enhance_020_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 04/04/2016  Shane Ward		Improve Reg Schedule Builder
||============================================================================
*/


DELETE FROM reg_Schedule_col_type;

INSERT INTO reg_schedule_col_type (col_type_id, description) VALUES (1, 'Line Number');
INSERT INTO reg_schedule_col_type (col_type_id, description) VALUES (2, 'Line Item (Description)');
INSERT INTO reg_schedule_col_type (col_type_id, description) VALUES (3, 'Results');
INSERT INTO reg_schedule_col_type (col_type_id, description) VALUES (4, 'Formula');
INSERT INTO reg_schedule_col_type (col_type_id, description) VALUES (5, 'Filtered');

 UPDATE ppbase_menu_items SET item_order = item_order + 1 WHERE item_order > 3 AND MODULE = 'REG' AND parent_menu_identifier = 'REPORTS';
 UPDATE ppbase_menu_items SET item_order = 4 WHERE menu_identifier = 'ANALYSIS' AND  MODULE = 'REG' AND parent_menu_identifier = 'REPORTS';

--UPDATE REG_SCHEDULE_LINE_TYPE
DELETE FROM reg_schedule_line_type;

-- Was Default
INSERT into Reg_schedule_line_type (line_type_id, description) VALUES (1, 'Results');
-- Was Filtered
INSERT into Reg_schedule_line_type (line_type_id, description) VALUES (2, 'Formula');
-- Was formula
INSERT into Reg_schedule_line_type (line_type_id, description) VALUES (3, 'Blank Line');
-- Was Definition
INSERT into Reg_schedule_line_type (line_type_id, description) VALUES (4, 'Column Header');
-- New
INSERT into Reg_schedule_line_type (line_type_id, description) VALUES (5, 'Column Specific Formula');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3131, 0, 2016, 1, 0, 0, 45559, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045559_reg_schedule_enhance_020_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 