/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051580_lessee_01_add_payment_escalations_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/11/2018 Josh Sandler     Add escalations to payment terms
||============================================================================
*/

alter table ls_variable_payment
add escalation_sw NUMBER(1) null;

ALTER TABLE ls_variable_payment
ADD CONSTRAINT ls_var_pay_esc_sw_chk CHECK (escalation_sw IN(0,1));

COMMENT ON COLUMN ls_variable_payment.escalation_sw IS 'Whether the VP is valid for contingent bucket formulas or payment escalations. Is 0 for Contingent formulas and 1 for escalations.';



alter table ls_ilr_payment_term
add (escalation NUMBER(22) NULL,
escalation_freq_id NUMBER(22) NULL,
escalation_pct NUMBER(22,12) NULL);

ALTER TABLE ls_ilr_payment_term
  ADD CONSTRAINT fk_pymtterms_escalation FOREIGN KEY (
    escalation
  ) REFERENCES ls_variable_payment (
    variable_payment_id
  )
;

ALTER TABLE ls_ilr_payment_term
  ADD CONSTRAINT fk_pymtterms_escal_freq FOREIGN KEY (
    escalation_freq_id
  ) REFERENCES ls_payment_freq (
    payment_freq_id
  )
;

ALTER TABLE ls_ilr_payment_term
ADD CONSTRAINT ls_pymtterms_escal_pymt_chk CHECK (escalation_freq_id <= payment_freq_id);

ALTER TABLE ls_ilr_payment_term
ADD CONSTRAINT ls_pymtterms_escal_pct_chk CHECK (escalation is null or (escalation is not null and escalation_pct is null));

COMMENT ON COLUMN ls_ilr_payment_term.escalation IS 'The Variable Payment ID of VP based escalation or null for fixed escalation.';
COMMENT ON COLUMN ls_ilr_payment_term.escalation_freq_id IS 'The frequency that the escalation is compounded';
COMMENT ON COLUMN ls_ilr_payment_term.escalation_pct IS 'The escalation percent for a fixed escalation. Stored as a decimal.';



alter table ls_ilr_payment_term_stg
add (escalation NUMBER(22) NULL,
escalation_freq_id NUMBER(22) NULL,
escalation_pct NUMBER(22,12) NULL);

COMMENT ON COLUMN ls_ilr_payment_term_stg.escalation IS 'The Variable Payment ID of VP based escalation or null for fixed escalation.';
COMMENT ON COLUMN ls_ilr_payment_term_stg.escalation_freq_id IS 'The frequency that the escalation is compounded';
COMMENT ON COLUMN ls_ilr_payment_term_stg.escalation_pct IS 'The escalation percent for a fixed escalation. Stored as a decimal.';



alter table ls_ilr_schedule_stg
add (payment_term_id NUMBER(22) NULL,
escalation NUMBER(22) NULL,
escalation_month NUMBER(1) NULL,
escalation_pct NUMBER(22,12) NULL,
months_to_escalate NUMBER(22) NULL);

COMMENT ON COLUMN ls_ilr_schedule_stg.payment_term_id IS 'Sequence number used to maintain uniqueness among the ILR payment term records.';
COMMENT ON COLUMN ls_ilr_schedule_stg.escalation IS 'The Variable Payment ID of VP based escalation or null for fixed escalation.';
COMMENT ON COLUMN ls_ilr_schedule_stg.escalation_month IS '1 means it is an escalation month.  0 means it is not an escalation month.';
COMMENT ON COLUMN ls_ilr_schedule_stg.escalation_pct IS 'The escalation percent for a fixed escalation. Stored as a decimal.';
COMMENT ON COLUMN ls_ilr_schedule_stg.months_to_escalate IS 'The number of months between escalations based on the escalation_freq_id.';



alter table ls_import_ilr
add (escalation_xlate varchar2(254),
     escalation number(22),
     escalation_freq_xlate varchar2(254),
     escalation_freq_id number(22),
     escalation_pct number(22,12)
)
;

COMMENT ON COLUMN ls_import_ilr.escalation_xlate IS 'Translation field for determining the escalation.';
COMMENT ON COLUMN ls_import_ilr.escalation IS 'The variable payment ID of a variable escalation or null for fixed or no escalation.';
COMMENT ON COLUMN ls_import_ilr.escalation_freq_xlate IS 'Translation field for determining the escalation frequency.';
COMMENT ON COLUMN ls_import_ilr.escalation_freq_id IS 'The escalation frequency on this ILR payment term. (1: Annual, 2: Semi-Annual, 3: Quarterly, 4: Monthly).';
COMMENT ON COLUMN ls_import_ilr.escalation_pct IS 'The fixed escalation percent entered as a decimal (e.g. 100% = "1.0".';




alter table ls_import_ilr_archive
add (escalation_xlate varchar2(254),
     escalation number(22),
     escalation_freq_xlate varchar2(254),
     escalation_freq_id number(22),
     escalation_pct number(22,12)
)
;

COMMENT ON COLUMN ls_import_ilr_archive.escalation_xlate IS 'Translation field for determining the escalation.';
COMMENT ON COLUMN ls_import_ilr_archive.escalation IS 'The variable payment ID of a variable escalation or null for fixed or no escalation.';
COMMENT ON COLUMN ls_import_ilr_archive.escalation_freq_xlate IS 'Translation field for determining the escalation frequency.';
COMMENT ON COLUMN ls_import_ilr_archive.escalation_freq_id IS 'The escalation frequency on this ILR payment term. (1: Annual, 2: Semi-Annual, 3: Quarterly, 4: Monthly).';
COMMENT ON COLUMN ls_import_ilr_archive.escalation_pct IS 'The fixed escalation percent entered as a decimal (e.g. 100% = "1.0".';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6762, 0, 2017, 4, 0, 0, 51580, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051580_lessee_01_add_payment_escalations_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;