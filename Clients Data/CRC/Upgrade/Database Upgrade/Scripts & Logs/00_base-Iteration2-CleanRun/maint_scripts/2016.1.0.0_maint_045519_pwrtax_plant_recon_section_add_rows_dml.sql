/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045519_pwrtax_plant_recon_add_tables_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 03/03/2016 Anand R			 Add records to TAX_PLANT_RECON_SECTION table
||
||============================================================================
*/ 

insert into TAX_PLANT_RECON_SECTION (TAX_PR_SECTION_ID, DESCRIPTION) values (1, 'Rate Information');

insert into TAX_PLANT_RECON_SECTION (TAX_PR_SECTION_ID, DESCRIPTION) values (2, 'GL Account Information');

insert into TAX_PLANT_RECON_SECTION (TAX_PR_SECTION_ID, DESCRIPTION) values (3, 'Asset Balance to PPE Recon');

insert into TAX_PLANT_RECON_SECTION (TAX_PR_SECTION_ID, DESCRIPTION) values (4, 'Book Basis Federal and State');

insert into TAX_PLANT_RECON_SECTION (TAX_PR_SECTION_ID, DESCRIPTION) values (5, 'Reserve Balance to PPE Recon');

insert into TAX_PLANT_RECON_SECTION (TAX_PR_SECTION_ID, DESCRIPTION) values (6, 'Net Book vs Net Tax Tie Out - Federal and State');

insert into TAX_PLANT_RECON_SECTION (TAX_PR_SECTION_ID, DESCRIPTION) values (7, 'Additional Reconciliation');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3090, 0, 2016, 1, 0, 0, 045519, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045519_pwrtax_plant_recon_section_add_rows_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;