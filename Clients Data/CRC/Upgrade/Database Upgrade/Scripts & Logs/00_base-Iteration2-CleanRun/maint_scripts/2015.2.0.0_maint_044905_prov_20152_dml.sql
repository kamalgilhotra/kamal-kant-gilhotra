
 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044905_prov_20152_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2 09/08/2015 	Jarrett Skov   Corrects incorrect difference line in 54517 report
 ||============================================================================
 */ 

 -- 54517 difference line Fix
update tax_accrual_rep_cons_rows 
set total_col_fieldname = 'SPREADSHEETDIFF(A-B)'
where rep_cons_type_id = 45
  and lower(row_value) = 'comp_dtl_tte_b4_discretes_tt';
  

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2852, 0, 2015, 2, 0, 0, 044905, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044905_prov_20152_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;