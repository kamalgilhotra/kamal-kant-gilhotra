/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049317_lease_01_field_est_residual_import_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

alter table ls_import_asset add estimated_residual_xlate number(22,8);
alter table ls_import_asset_archive add estimated_residual_xlate number(22,8);

COMMENT ON COLUMN ls_import_asset.estimated_residual_xlate IS 'Translation field for determining the estimated residual percentage.';
COMMENT ON COLUMN ls_import_asset_archive.estimated_residual_xlate IS 'Translation field for determining the estimated residual percentage.';
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4014, 0, 2017, 1, 0, 0, 49317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049317_lease_01_field_est_residual_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;