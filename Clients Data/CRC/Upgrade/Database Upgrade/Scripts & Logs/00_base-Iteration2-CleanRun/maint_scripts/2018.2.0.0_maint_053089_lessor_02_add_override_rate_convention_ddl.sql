/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053089_lessor_02_add_override_rate_convention_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/21/2019 David Conway     Add lessor override rate convention
||============================================================================
*/

CREATE OR REPLACE VIEW v_lsr_ilr_rates_compounded AS
SELECT  lsr_ilr_rates.ilr_id,
        lsr_ilr_rates.revision,
        lsr_ilr_rates.rate_type_id,
        lsr_ilr_rate_types.DESCRIPTION AS rate_type_description,
        pkg_lessor_schedule.f_annual_to_implicit_rate(lsr_ilr_rates.rate,2) AS rate_implicit,
        rate AS annual_discount_rate
FROM lsr_ilr_rates
JOIN lsr_ilr_rate_types ON lsr_ilr_rates.rate_type_id = lsr_ilr_rate_types.rate_type_id;

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15443, 0, 2018, 2, 0, 0, 53089, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053089_lessor_02_add_override_rate_convention_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;