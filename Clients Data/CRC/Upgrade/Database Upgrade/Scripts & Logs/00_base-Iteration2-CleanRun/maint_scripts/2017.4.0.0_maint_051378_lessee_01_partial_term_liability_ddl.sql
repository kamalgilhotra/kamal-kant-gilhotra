/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051378_lessee_01_partial_term_liability_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/29/2018 Shane "C" Ward   Add columns for Lessee Partial Termination (Liability method)
||============================================================================
*/

ALTER TABLE LS_ILR_OPTIONS
  ADD remeasurement_type NUMBER(22, 0) NULL;

COMMENT ON COLUMN LS_ILR_OPTIONS.remeasurement_type IS 'Flag for type of remeasurement for ILR (1: Liability Percent Change, 2: Quantity % Change)';

ALTER TABLE LS_ILR_OPTIONS
  ADD partial_termination NUMBER(22, 0) NULL;

COMMENT ON COLUMN LS_ILR_OPTIONS.partial_termination IS 'Flag for type of Partial Termination for ILR Remeasurement (1: Partial Termination Remeasuremnet, 0: Not a Partial Termination)';

ALTER TABLE LS_ASSET_SCHEDULE
  ADD rou_asset_remeasurement NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ASSET_SCHEDULE.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';

ALTER TABLE LS_ASSET_SCHEDULE
  ADD partial_term_gain_loss NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ASSET_SCHEDULE.partial_term_gain_loss IS 'Gain/Loss calculated for partial termination of assets';

ALTER TABLE LS_ILR_SCHEDULE
  ADD rou_asset_remeasurement NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_SCHEDULE.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';

ALTER TABLE LS_ILR_SCHEDULE
  ADD partial_term_gain_loss NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_SCHEDULE.partial_term_gain_loss IS 'Gain/Loss calculated for partial termination of assets';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD rou_asset_remeasurement NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD partial_term_gain_loss NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.partial_term_gain_loss IS 'Gain/Loss calculated for partial termination of assets';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD rou_asset_remeasurement NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD partial_term_gain_loss NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.partial_term_gain_loss IS 'Gain/Loss calculated for partial termination of assets';

ALTER TABLE LS_SUMMARY_FORECAST
  ADD rou_asset_remeasurement NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_SUMMARY_FORECAST.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';

ALTER TABLE LS_SUMMARY_FORECAST
  ADD partial_term_gain_loss NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_SUMMARY_FORECAST.partial_term_gain_loss IS 'Gain/Loss calculated for partial termination of assets'; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6122, 0, 2017, 4, 0, 0, 51378, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051378_lessee_01_partial_term_liability_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;