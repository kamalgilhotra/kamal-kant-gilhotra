/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009577_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/02/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Create a new table to store valid property tax types for prop tax companies.
--
create table PWRPLANT.PT_COMPANY_TAX_TYPE
(
 PROP_TAX_COMPANY_ID  number(22,0)  not null,
 PROPERTY_TAX_TYPE_ID number(22,0)  not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

alter table PWRPLANT.PT_COMPANY_TAX_TYPE
   add constraint PTCO_TAX_TYPE_PK
       primary key (PROP_TAX_COMPANY_ID, PROPERTY_TAX_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_COMPANY_TAX_TYPE
   add constraint PTCO_TAX_TYPE_PTCO_FK
       foreign key (PROP_TAX_COMPANY_ID)
       references PWRPLANT.PT_COMPANY;

alter table PWRPLANT.PT_COMPANY_TAX_TYPE
   add constraint PTCO_TAX_TYPE_PTT_FK
       foreign key (PROPERTY_TAX_TYPE_ID)
       references PWRPLANT.PROPERTY_TAX_TYPE_DATA;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (104, 0, 10, 3, 4, 0, 9577, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009577_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
