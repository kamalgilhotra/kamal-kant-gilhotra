/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035717_aro_reg_report.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/17/2014 Ryan Oliveria  Report for archived Reg ARO transactions
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER,
    OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'ARO Regulated Activity',
          'ARO Regulated Activity',
          null,
          'dw_aro_pend_reg_activity_arc_rpt',
          'already has report time',
          null,
          null,
          'ARO - 1018',
          'dw_company_select',
          null,
          null,
          9,
          37,
          1,
          17,
          1,
          1,
          null,
          null,
          null,
          null,
          null,
          null
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (878, 0, 10, 4, 2, 0, 35717, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035717_aro_reg_report.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

