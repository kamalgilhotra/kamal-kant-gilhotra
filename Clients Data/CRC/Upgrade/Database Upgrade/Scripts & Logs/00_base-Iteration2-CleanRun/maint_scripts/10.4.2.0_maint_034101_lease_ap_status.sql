/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034101_lease_ap_status.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/17/2014 Kyle Peterson
||============================================================================
*/

alter table LS_PAYMENT_HDR rename column SENT_TO_AP to AP_STATUS_ID;

create table LS_AP_STATUS
(
 AP_STATUS_ID number(22,0) not null,
 DESCRIPTION varchar2(35)
);

alter table LS_AP_STATUS
   add constraint PK_LS_AP_STATUS
       primary key (AP_STATUS_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_PAYMENT_HDR
   add constraint FK_LS_PAYMENT_HDR4
       foreign key (AP_STATUS_ID)
       references LS_AP_STATUS(AP_STATUS_ID);

insert into LS_AP_STATUS (AP_STATUS_ID, DESCRIPTION) values (1, 'Not sent to AP');

update LS_PAYMENT_HDR set AP_STATUS_ID = 1;

comment on table LS_AP_STATUS is '(F) [06] The LS_AP_STATUS table holds description mappings for AP_STATUS_IDs.';

comment on column LS_PAYMENT_HDR.AP_STATUS_ID is 'A status column for a payment''s progress through AP. Should be updated by an AP interface only.';

comment on column LS_AP_STATUS.AP_STATUS_ID is 'A system generated ID for an AP status.';
comment on column LS_AP_STATUS.DESCRIPTION is 'A description for a given AP status.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (872, 0, 10, 4, 2, 0, 34101, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034101_lease_ap_status.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;