/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042668_pcm_fcst_options_version_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/02/2015 Sarah Byers      New table to support sorting comparison BVs
||============================================================================
*/
-- Populate the new table with the existing values from each of the templates
declare
	l_delimiter varchar2(3) := '---';
	l_delimiter_index number(22,0) := 1;
	l_while_index number(22,0);
	l_str_to_parse varchar2(2000);
	l_value varchar2(254);
	l_index number(22,0) := 1;
	l_sort_order number(2,0);
	l_count number(22,0);

	cursor c1 is
		select users, template_name, template_level, previous_version_id
		  from wo_est_forecast_options
		 where previous_version_id is not null;
begin
	for record in c1
	loop
		l_str_to_parse := record.previous_version_id;
		l_sort_order := 0;
		l_while_index := 1;
		l_index := 1;
		while l_while_index != 0
		loop
			l_delimiter_index := INSTR(l_str_to_parse, l_delimiter, l_index);
			
			if l_delimiter_index = 0 then
				l_value := substr(l_str_to_parse, l_index);
			else
				l_value := substr(l_str_to_parse, l_index, l_delimiter_index - l_index);
			end if;

			l_sort_order := l_sort_order + 1;

			insert into wo_est_fcst_options_version (
				users, template_name, template_level, description, sort_order)
			select users, template_name, template_level, l_value, l_sort_order
			  from wo_est_forecast_options
			 where users = record.users
				and template_name = record.template_name
				and template_level = record.template_level;

			l_index := l_delimiter_index + 3;
			l_while_index := l_delimiter_index;
		end loop;
	end loop;
exception
   when others then
      RAISE_APPLICATION_ERROR(-20000, 'ERROR: Converting wo_est_forecast_options.previous_version_id into wo_est_fcst_options_version: ' || sqlerrm);
end;
/

-- Update the budget version field
update wo_est_fcst_options_version a
	set budget_version_id = (
		select b.budget_version_id from budget_version b
		 where a.description = b.description)
 where exists (
		select 1 from budget_version b
		 where a.description = b.description);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2246, 0, 2015, 1, 0, 0, 042668, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042668_pcm_fcst_options_version_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;