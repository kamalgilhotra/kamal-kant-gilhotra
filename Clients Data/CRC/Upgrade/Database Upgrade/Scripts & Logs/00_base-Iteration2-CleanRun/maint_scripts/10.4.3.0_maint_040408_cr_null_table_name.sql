/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040408_cr_null_table_name.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/27/2014 Daniel Motter
||============================================================================
*/

alter table PP_PROCESSES_KICKOUT_TABLE modify TABLE_NAME NULL;

delete from PP_PROCESSES_KICKOUT_COLUMNS;

delete from PP_PROCESSES_KICKOUT_TABLE;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1583, 0, 10, 4, 3, 0, 40408, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040408_cr_null_table_name.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;