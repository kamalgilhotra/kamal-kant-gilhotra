/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037678_reg_cr_combos_lists.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.5 04/18/2014 Sarah Byers
||============================================================================
*/

-- Add CR_LIST_ID to REG_CR_COMBOS_FIELDS
alter table REG_CR_COMBOS_FIELDS add CR_LIST_ID number(22,0) null;

comment on column REG_CR_COMBOS_FIELDS.CR_LIST_ID is 'System assigned identifier of a CR Combo List.';

-- Create table REG_CR_COMBOS_LISTS
create table REG_CR_COMBOS_LISTS
(
 CR_LIST_ID    number(22,0) not null,
 DESCRIPTION   varchar2(50) not null,
 CR_ELEMENT_ID number(22,0) not null,
 STRUCTURE_ID  number(22,0) not null,
 ELEMENT_LIST  varchar2(77) not null,
 USER_ID       varchar2(18),
 TIME_STAMP    date
);

alter table REG_CR_COMBOS_LISTS
   add constraint PK_REG_CR_COMBOS_LISTS
       primary key (CR_LIST_ID)
       using index tablespace PWRPLANT_IDX;

-- Foreign Key
alter table REG_CR_COMBOS_FIELDS
   add constraint R_REG_CR_COMBOS_FIELDS3
       foreign key (CR_LIST_ID)
       references REG_CR_COMBOS_LISTS (CR_LIST_ID);

-- Comments
comment on table REG_CR_COMBOS_LISTS is '(O) [19] The Reg CR Combos Lists table contains unique CR elements lists.';
comment on column REG_CR_COMBOS_LISTS.CR_LIST_ID is 'System assigned identifier of a CR Combo List.';
comment on column REG_CR_COMBOS_LISTS.DESCRIPTION is 'User defined description of the CR Combo List.';
comment on column REG_CR_COMBOS_LISTS.CR_ELEMENT_ID is 'System assigned identifier of the CR Element.';
comment on column REG_CR_COMBOS_LISTS.STRUCTURE_ID is 'The STRUCTURE_ID from the CR used by Regulatory for the CR Element.';
comment on column REG_CR_COMBOS_LISTS.ELEMENT_LIST is 'The comma separated list of element values that are assigned to the list.';
comment on column REG_CR_COMBOS_LISTS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_CR_COMBOS_LISTS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';

-- Convert existing list uses
insert into REG_CR_COMBOS_LISTS
   (CR_LIST_ID, DESCRIPTION, CR_ELEMENT_ID, STRUCTURE_ID, ELEMENT_LIST)
   select F.CR_COMBO_ID, C.DESCRIPTION, F.CR_ELEMENT_ID, F.STRUCTURE_ID, F.LOWER_VALUE
     from REG_CR_COMBOS C, REG_CR_COMBOS_FIELDS F
    where STRUCTURE_ID is not null
      and C.CR_COMBO_ID = F.CR_COMBO_ID;

update REG_CR_COMBOS_FIELDS set CR_LIST_ID = CR_COMBO_ID where STRUCTURE_ID is not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1133, 0, 10, 4, 2, 5, 37678, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.5_maint_037678_reg_cr_combos_lists.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
