/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045412_jobserver_02add_company_security_module_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 05/27/2016 Jared Watkins		 Fill in the security modules on the rows with a company argument
||============================================================================
*/ 
SET DEFINE OFF;

update pp_job_executable set company_security_module = 'cr' where job_type = 'PowerBuilder' and job_name = 'CR Batch Derivation Budget';
update pp_job_executable set company_security_module = 'cr' where job_type = 'PowerBuilder' and job_name = 'CR Delete Budget Allocations';
update pp_job_executable set company_security_module = 'cr' where job_type = 'PowerBuilder' and job_name = 'CR Manual Journal Reversals';
update pp_job_executable set company_security_module = 'pp' where job_type = 'PowerBuilder' and job_name = 'PP to CR';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Create Funding Project';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Load Unit Est';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Create Job Task';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Create Work Order';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Load Monthly Estimates';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Accrual Calculation';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'WO Retirements';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Failed Unitization';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'AFUDC Calculation Only - No Overheads';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Overhead Calculation Before AFUDC';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Overhead Calculation After AFUDC';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'AFUDC Approval Only - No Overheads';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Overhead Approval Before AFUDC';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Overhead Approval After AFUDC';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'AFUDC & Overheads Approval';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'AFUDC & Overheads Calculation';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'ARO Approval';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'ARO Calculation';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Accrual Approval';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Auto Non-Unitization';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Auto Unitization';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'CPR Balance PowerPlan';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Close CPR';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'WO Close Charge Collection';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'CPR Close PowerPlan';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'WO Close Month';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Depreciation Approval';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Depreciation Calculation';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Depreciation Calculation: Lease';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'CPR GLReconciliation';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'WO GL Reconciliation';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'WO New Month';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'CPR New Month';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Re-Open CPR';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'CPR Release Journal Entries';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'WO Release Journal Entries';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Preview Journal Entries';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Approve Lease Depr';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Lease Approve Accruals';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Lease Approve Payments';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Lease Auto Retirements';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Lease Calc Accruals';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Lease Calc Payments';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Lease Open Next';
update pp_job_executable set company_security_module = 'pp' where job_type = 'SSP' and job_name = 'Lock Lease';

SET DEFINE ON;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3202, 0, 2016, 1, 0, 0, 045412, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045412_jobserver_02add_company_security_module_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;