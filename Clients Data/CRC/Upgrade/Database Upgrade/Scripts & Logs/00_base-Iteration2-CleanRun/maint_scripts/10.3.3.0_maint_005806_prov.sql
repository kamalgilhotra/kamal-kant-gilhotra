/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_005806_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/02/2011 Blake Andrews  Point Release
||============================================================================
*/

--###PATCH(5806)
alter table TAX_ACCRUAL_M_TYPE
   add POST_APPORT_IND number(22) default 0;

update TAX_ACCRUAL_M_TYPE set POST_APPORT_IND = 1 where M_TYPE_ID = 80;

insert into TAX_ACCRUAL_REP_SPECIAL_NOTE
   (SPECIAL_NOTE_ID, DESCRIPTION, REPORT_OPTION_ID, MONTH_REQUIRED, COMPARE_CASE, M_ROLLUP_REQUIRED,
    CALC_RATES, REPORT_TYPE, WINDOW_NAME, DW_PARAMETER_LIST, JE_TEMP_TABLE_IND,
    NS2FAS109_TEMP_TABLE_IND, REP_ROLLUP_GROUP_ID, DEFAULT_ACCT_ROLLUP, REP_CONS_CATEGORY_ID)
   select 71,
          'CONSOLIDATED-OPER-STATE',
          18,
          MONTH_REQUIRED,
          COMPARE_CASE,
          M_ROLLUP_REQUIRED,
          CALC_RATES,
          REPORT_TYPE,
          WINDOW_NAME,
          DW_PARAMETER_LIST,
          JE_TEMP_TABLE_IND,
          NS2FAS109_TEMP_TABLE_IND,
          REP_ROLLUP_GROUP_ID,
          DEFAULT_ACCT_ROLLUP,
          REP_CONS_CATEGORY_ID
     from TAX_ACCRUAL_REP_SPECIAL_NOTE
    where DESCRIPTION = 'CONSOLIDATED-OPER';


delete from pp_reports
where report_id = 51013;
commit;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER)
values
   (51013, 'Current State Provision-All',
    'Current State Provision Report that starts with pre-tax income (not Federal Taxable Income)',
    'Tax Accrual', 'dw_tax_accrual_state_provision_all', 'CONSOLIDATED-OPER-STATE',
    'Tax Accrual - 51013');

insert into TAX_ACCRUAL_REPORT_OPTION
   (REPORT_OPTION_ID, DESCRIPTION)
values
   (31, 'NEW_STATE_REPORT');

insert into TAX_ACCRUAL_REPORT_OPTION_DTL
   (REPORT_OPTION_ID, REPORT_OBJECT_ID)
   select 31, REPORT_OBJECT_ID from TAX_ACCRUAL_REPORT_OPTION_DTL where REPORT_OPTION_ID = 18;

delete from TAX_ACCRUAL_REPORT_OPTION_DTL
 where REPORT_OPTION_ID = 31
   and REPORT_OBJECT_ID in (2, 5);

update TAX_ACCRUAL_REP_SPECIAL_NOTE
   set REPORT_OPTION_ID = 31
 where DESCRIPTION = 'CONSOLIDATED-OPER-STATE';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (39, 0, 10, 3, 3, 0, 5806, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_005806_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
