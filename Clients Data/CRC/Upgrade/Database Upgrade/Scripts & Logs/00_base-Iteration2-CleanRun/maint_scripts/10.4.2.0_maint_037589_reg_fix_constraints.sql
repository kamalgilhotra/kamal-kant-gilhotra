set serveroutput on
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_037589_reg_fix_constraints.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 04/07/2014 Lee Quinn      Patch Release
||============================================================================
*/

alter table REG_COMPANY enable constraint R_REG_COMPANY1;
alter table REG_COMPANY enable constraint R_REG_COMPANY2;

-- These FK's where created without a name.
--alter table REG_BUDGET_COMP_MEMBER_MAP
--  add foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
--  references REG_CWIP_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);
--alter table REG_BUDGET_COMP_MEMBER_MAP
--  add foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
--  references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

--alter table REG_BUDGET_COMP_WHERE_CLAUSE
--  add foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
--  references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

--alter table REG_BUDGET_FAMILY_MEMBER_MAP
--  add foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
--  references REG_CWIP_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);
--alter table REG_BUDGET_FAMILY_MEMBER_MAP
--  add foreign key (REG_MEMBER_RULE_ID)
--  references REG_BUDGET_MEMBER_RULE (REG_MEMBER_RULE_ID);

--alter table REG_BUDGET_MEMBER_RULE_VALUES
--  add foreign key (REG_MEMBER_RULE_ID)
--  references REG_BUDGET_MEMBER_RULE (REG_MEMBER_RULE_ID);
--alter table REG_BUDGET_MEMBER_RULE_VALUES
--  add foreign key (REG_MEMBER_ELEMENT_ID)
--  references REG_BUDGET_MEMBER_ELEMENT (REG_MEMBER_ELEMENT_ID);

declare
   RTN number;

   function RENAME_CONSTRAINT(V_TABLE_NAME           varchar2,
                              V_REFERENCE_TABLE_NAME varchar2,
                              V_NEW_FK_NAME          varchar2) return number as
      OLD_FK_NAME varchar2(30);
      LS_SQL      varchar2(128);
   begin
      select CONSTRAINT_NAME
        into OLD_FK_NAME
        from USER_CONSTRAINTS
       where TABLE_NAME = V_TABLE_NAME
         and CONSTRAINT_TYPE = 'R'
         and R_CONSTRAINT_NAME = (select CONSTRAINT_NAME
                                    from USER_CONSTRAINTS
                                   where TABLE_NAME = V_REFERENCE_TABLE_NAME
                                     and CONSTRAINT_TYPE = 'P');
      if OLD_FK_NAME = V_NEW_FK_NAME then
         DBMS_OUTPUT.PUT_LINE('FK already renamed.');
      else
         LS_SQL := 'alter table ' || V_TABLE_NAME || ' rename constraint ' || OLD_FK_NAME || ' to ' ||
                   V_NEW_FK_NAME;
         execute immediate LS_SQL;
         DBMS_OUTPUT.PUT_LINE(LS_SQL || ';');
         DBMS_OUTPUT.PUT_LINE('FK Renamed.');
         return 1;
      end if;
      return - 1;
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('SQL Error: ' || sqlerrm);
         return - 1;
   end RENAME_CONSTRAINT;

begin
   RTN := RENAME_CONSTRAINT('REG_BUDGET_COMP_MEMBER_MAP',
                            'REG_CWIP_FAMILY_MEMBER',
                            'FK1_REG_BUDGET_COMP_MEMBER_MAP');
   RTN := RENAME_CONSTRAINT('REG_BUDGET_COMP_MEMBER_MAP',
                            'REG_CWIP_FAMILY_COMPONENT',
                            'FK2_REG_BUDGET_COMP_MEMBER_MAP');

   RTN := RENAME_CONSTRAINT('REG_BUDGET_COMP_WHERE_CLAUSE',
                            'REG_CWIP_FAMILY_COMPONENT',
                            'FK1_REG_BUDGET_COMP_WHERE_CLAU');

   RTN := RENAME_CONSTRAINT('REG_BUDGET_FAMILY_MEMBER_MAP',
                            'REG_CWIP_FAMILY_MEMBER',
                            'FK1_REG_BUDGET_FAMILY_MEMBER_M');
   RTN := RENAME_CONSTRAINT('REG_BUDGET_FAMILY_MEMBER_MAP',
                            'REG_BUDGET_MEMBER_RULE',
                            'FK2_REG_BUDGET_FAMILY_MEMBER_M');

   RTN := RENAME_CONSTRAINT('REG_BUDGET_MEMBER_RULE_VALUES',
                            'REG_BUDGET_MEMBER_RULE',
                            'FK1_REG_BUDGET_MEMBER_RULE_VAL');
   RTN := RENAME_CONSTRAINT('REG_BUDGET_MEMBER_RULE_VALUES',
                            'REG_BUDGET_MEMBER_ELEMENT',
                            'FK2_REG_BUDGET_MEMBER_RULE_VAL');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1097, 0, 10, 4, 2, 0, 37589, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037589_reg_fix_constraints.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
