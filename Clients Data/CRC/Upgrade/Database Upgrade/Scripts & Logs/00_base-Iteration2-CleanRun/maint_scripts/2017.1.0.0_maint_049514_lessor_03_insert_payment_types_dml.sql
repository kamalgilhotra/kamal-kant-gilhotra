/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049514_lessor_03_insert_payment_types_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/30/2017 JSisouphanh      Fill out ls_payment_type
||============================================================================
*/


-- Fill out Executory
update ls_payment_type
set description = 'Executory 1'
where payment_type_id =3 ;

insert into ls_payment_type(payment_type_id, description) values (4,  'Executory 2');
insert into ls_payment_type(payment_type_id, description) values (5,  'Executory 3');
insert into ls_payment_type(payment_type_id, description) values (6,  'Executory 4');
insert into ls_payment_type(payment_type_id, description) values (7,  'Executory 5');
insert into ls_payment_type(payment_type_id, description) values (8,  'Executory 6');
insert into ls_payment_type(payment_type_id, description) values (9,  'Executory 7');
insert into ls_payment_type(payment_type_id, description) values (10, 'Executory 8');
insert into ls_payment_type(payment_type_id, description) values (11, 'Executory 9');
insert into ls_payment_type(payment_type_id, description) values (12, 'Executory 10');

--Fill Contigent
update ls_payment_type
set description = 'Contingent 1'
where payment_type_id = 13 ;

insert into ls_payment_type(payment_type_id, description) values (14, 'Contingent 2');
insert into ls_payment_type(payment_type_id, description) values (15, 'Contingent 3');
insert into ls_payment_type(payment_type_id, description) values (16, 'Contingent 4');
insert into ls_payment_type(payment_type_id, description) values (17, 'Contingent 5');
insert into ls_payment_type(payment_type_id, description) values (18, 'Contingent 6');
insert into ls_payment_type(payment_type_id, description) values (19, 'Contingent 7');
insert into ls_payment_type(payment_type_id, description) values (20, 'Contingent 8');
insert into ls_payment_type(payment_type_id, description) values (21, 'Contingent 9');
insert into ls_payment_type(payment_type_id, description) values (22, 'Contingent 10');

insert into ls_payment_type(payment_type_id, description) values (-99, 'State Tax Offset');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3879, 0, 2017, 1, 0, 0, 49514, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049514_lessor_03_insert_payment_types_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;