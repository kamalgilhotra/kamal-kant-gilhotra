/*
||=====================================================================================
|| Application: PowerPlan
|| File Name:   maint_047549_lease_pp_any_query_mc_dml.sql
||=====================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||=====================================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.1.0.0 05/05/2017 David Haupt       Update query to include currency and symbols
||=====================================================================================
*/

declare
	query_id number;

begin
	select id into query_id from pp_any_query_criteria where description = 'ILR by Set of Books (CURRENT Revision)'; 

	update pp_any_query_criteria_fields set COLUMN_ORDER =  COLUMN_ORDER + 2 where id = query_id;

	insert into pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, display_field, display_table, column_type, quantity_field, data_field, required_filter, required_one_mult, sort_col, hide_from_results, hide_from_filters) 
    values (query_id, 'currency', 1, 0, 0, null, 'Currency', 300, null, null, 'VARCHAR2', 0, null, null, null, null, 0, 1)
	;
	
	insert into pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, display_field, display_table, column_type, quantity_field, data_field, required_filter, required_one_mult, sort_col, hide_from_results, hide_from_filters) 
    values (query_id, 'currency_display_symbol', 2, 0, 0, null, 'Currency Symbol', 300, null, null, 'VARCHAR2', 0, null, null, null, null, 1, 1)
	;	
    
    insert into pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, default_value,
	column_header, column_width, display_field, display_table, column_type, quantity_field, data_field, required_filter, required_one_mult, sort_col, hide_from_results, hide_from_filters) 
    values (query_id, 'currency_type', 12, 0, 1, null, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'description', 1, 2, null, 1, 0)
	;
end;
/	

update pp_any_query_criteria
set SQL = 'select cur.iso_code as currency, lct.description as currency_type, ll.ilr_id as ilr_id,    ll.ilr_number as ilr_number,   ll.revision as revision,   
 (select a.description from approval_status a, ls_ilr_approval la where la.approval_status_id = a.approval_status_id and ll.ilr_id = la.ilr_id and ll.revision = la.revision) as revision_status,
(select s.description from set_of_books s where ll.set_of_books_id = s.set_of_books_id) as set_of_books,   
c.description as company,   ll.net_present_value as net_present_value, ll.capital_cost as capital_cost,    ll.current_lease_cost as fair_market_value,
(select count(1) from ls_asset la where la.ilr_id = ll.ilr_id) as qty_number_of_assets,
cur.currency_display_symbol
from v_ls_ilr_header_fx_vw ll, company c, ls_lease ls, currency cur, ls_lease_currency_type lct  
where ll.company_id = c.company_id    
and ls.lease_id = ll.lease_id
and ll.currency_id = cur.currency_id
and lct.ls_currency_type_id = ll.ls_cur_type
and ll.revision = ll.current_revision
order by 2, 3'
where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3483, 0, 2017, 1, 0, 0, 47549, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047549_lease_pp_any_query_mc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	
