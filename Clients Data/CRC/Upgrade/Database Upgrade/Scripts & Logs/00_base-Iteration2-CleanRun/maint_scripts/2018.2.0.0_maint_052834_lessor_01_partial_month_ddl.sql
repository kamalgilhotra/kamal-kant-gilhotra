/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052834_lessor_01_partial_month_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 03/22/2019  Sarah Byers      Add Partial Month Payment Term Type functionality to Lessor
||============================================================================
*/

CREATE OR REPLACE TYPE t_number_22_8_tab as table of number(22,8);
/

drop type LSR_ILR_SALES_SCH_RESULT_TAB;

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER,
                                                          interest_income_received     NUMBER,
                                                          interest_income_accrued      NUMBER,
                                                          principal_accrued            NUMBER,
                                                          begin_receivable         NUMBER,
                                                          end_receivable         NUMBER,
                                                          begin_lt_receivable          NUMBER,
                                                          end_lt_receivable            NUMBER,
                                                          initial_direct_cost          NUMBER,
                                                          executory_accrual1           NUMBER,
                                                          executory_accrual2           NUMBER,
                                                          executory_accrual3           NUMBER,
                                                          executory_accrual4           NUMBER,
                                                          executory_accrual5           NUMBER,
                                                          executory_accrual6           NUMBER,
                                                          executory_accrual7           NUMBER,
                                                          executory_accrual8           NUMBER,
                                                          executory_accrual9           NUMBER,
                                                          executory_accrual10          NUMBER,
                                                          executory_paid1              NUMBER,
                                                          executory_paid2              NUMBER,
                                                          executory_paid3              NUMBER,
                                                          executory_paid4              NUMBER,
                                                          executory_paid5              NUMBER,
                                                          executory_paid6              NUMBER,
                                                          executory_paid7              NUMBER,
                                                          executory_paid8              NUMBER,
                                                          executory_paid9              NUMBER,
                                                          executory_paid10             NUMBER,
                                                          contingent_accrual1          NUMBER,
                                                          contingent_accrual2          NUMBER,
                                                          contingent_accrual3          NUMBER,
                                                          contingent_accrual4          NUMBER,
                                                          contingent_accrual5          NUMBER,
                                                          contingent_accrual6          NUMBER,
                                                          contingent_accrual7          NUMBER,
                                                          contingent_accrual8          NUMBER,
                                                          contingent_accrual9          NUMBER,
                                                          contingent_accrual10         NUMBER,
                                                          contingent_paid1             NUMBER,
                                                          contingent_paid2             NUMBER,
                                                          contingent_paid3             NUMBER,
                                                          contingent_paid4             NUMBER,
                                                          contingent_paid5             NUMBER,
                                                          contingent_paid6             NUMBER,
                                                          contingent_paid7             NUMBER,
                                                          contingent_paid8             NUMBER,
                                                          contingent_paid9             NUMBER,
                                                          contingent_paid10            NUMBER,
                                                          begin_unguaranteed_residual NUMBER,
                                                          int_on_unguaranteed_residual NUMBER,
                                                          end_unguaranteed_residual NUMBER,
                                                          begin_net_investment NUMBER,
                                                          int_on_net_investment NUMBER,
                                                          end_net_investment NUMBER,
                                                          receivable_remeasurement number,
                                                          lt_receivable_remeasurement  NUMBER,
                                                          partial_month_percent NUMBER(22,8),
                                                          unguaran_residual_remeasure number,
                                                          rate_implicit FLOAT,
                                                          discount_rate FLOAT,
                                                          rate_implicit_ni FLOAT,
                                                          discount_rate_ni FLOAT,
                                                          begin_lease_receivable number,
                                                          original_net_investment number,
                                                          npv_lease_payments NUMBER,
                                                          npv_guaranteed_residual NUMBER,
                                                          npv_unguaranteed_residual NUMBER,
                                                          selling_profit_loss NUMBER,
                                                          cost_of_goods_sold NUMBER,
                                                          schedule_rates t_lsr_ilr_schedule_all_rates)
/

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT_TAB AS TABLE OF lsr_ilr_sales_sch_result;
/

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16202, 0, 2018, 2, 0, 0, 52834, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052834_lessor_01_partial_month_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
