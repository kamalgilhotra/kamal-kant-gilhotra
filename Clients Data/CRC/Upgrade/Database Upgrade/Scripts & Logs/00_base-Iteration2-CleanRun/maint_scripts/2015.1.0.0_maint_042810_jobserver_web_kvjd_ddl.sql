/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_042810_jobserver_web_kvjd_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/03/2014 Paul Cordero    	 Creating new table 'PP_WEB_KVJD'
||==========================================================================================
*/

SET serveroutput ON size 30000;

DECLARE 
  doesTableExist NUMBER := 0;
  BEGIN
       BEGIN
          doesTableExist := SYS_DOES_TABLE_EXIST('PP_WEB_KVJD','PWRPLANT');
          
          IF doesTableExist = 0 THEN
            BEGIN
              dbms_output.put_line('Creating table PP_WEB_KVJD ');
                
              EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_WEB_KVJD" 
              (
                "ID" NUMBER(16,0) CONSTRAINT "PP_WEB_KVJD01" NOT NULL ENABLE, 
                "GUID_KEY" VARCHAR2(36 BYTE) CONSTRAINT "PP_WEB_KVJD02" NOT NULL ENABLE,
                "JSON_DATA_VALUE" VARCHAR2(4000 BYTE) CONSTRAINT "PP_WEB_KVJD03" NOT NULL ENABLE,
                "KVJD_PARENT_GUID" VARCHAR2(36 BYTE),
                TIME_STAMP DATE CONSTRAINT "PP_WEB_KVJD04" NOT NULL,
                CONSTRAINT PP_WEB_KVJD_PK PRIMARY KEY ("ID"),
                CONSTRAINT PP_WEB_KVJD_UK UNIQUE ("GUID_KEY")
              ) TABLESPACE PWRPLANT';    
    
              EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_KVJD"."ID" IS ''Primary record identifier''';
              EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_KVJD"."GUID_KEY" IS ''Unique key when saving/fetching record as GUID''';
              EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_KVJD"."JSON_DATA_VALUE" IS ''JSON data string''';
              EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_KVJD"."KVJD_PARENT_GUID" IS ''Link to parent record when Parent/Child relationship exists''';
              EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_KVJD"."TIME_STAMP" IS ''Timestamp of record''';
      
              EXECUTE IMMEDIATE 'CREATE SEQUENCE "PWRPLANT"."PP_WEB_KVJD_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE';
      
              EXECUTE IMMEDIATE 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_KVJD for pwrplant.PP_WEB_KVJD';
            
              EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_KVJD_AUDIT" before
                UPDATE OR
                INSERT ON PP_WEB_KVJD FOR EACH row BEGIN :new.TIME_STAMP := SYSDATE;
              END;';
      
              EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_WEB_KVJD_AUDIT" ENABLE';
      
              EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_KVJD_ID" BEFORE
                INSERT ON PP_WEB_KVJD FOR EACH ROW BEGIN
                SELECT PP_WEB_KVJD_SEQ.nextval INTO :new.ID FROM dual;
              END;';
      
              EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_WEB_KVJD_ID" ENABLE';
      
              EXECUTE IMMEDIATE 'GRANT ALL on PP_WEB_KVJD to pwrplant_role_dev';
            
            END;
          ELSE
			BEGIN
			  dbms_output.put_line('Table PP_WEB_KVJD exists');
			END;
          END IF;
       END;
  END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2343, 0, 2015, 1, 0, 0, 042810, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042810_jobserver_web_kvjd_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;