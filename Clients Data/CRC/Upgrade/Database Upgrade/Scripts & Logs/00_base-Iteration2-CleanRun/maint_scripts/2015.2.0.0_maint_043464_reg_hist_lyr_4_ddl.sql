/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043464_reg_hist_lyr_4_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/09/2015 Andrew Scott   add inc process id to stg and arc tbls
||============================================================================
*/

alter table DEPR_CALC_STG add  INCREMENTAL_PROCESS_ID NUMBER(22,0);
alter table CPR_DEPR_CALC_STG add  INCREMENTAL_PROCESS_ID NUMBER(22,0);
alter table DEPR_CALC_STG_ARC add  INCREMENTAL_PROCESS_ID NUMBER(22,0);
alter table FCST_DEPR_CALC_STG_ARC add  INCREMENTAL_PROCESS_ID NUMBER(22,0);
alter table FCST_DEPR_LEDGER_BLEND_STG_ARC add  INCREMENTAL_PROCESS_ID NUMBER(22,0);
alter table DEPR_LEDGER_BLENDING_STG add  INCREMENTAL_PROCESS_ID NUMBER(22,0);

COMMENT ON COLUMN DEPR_CALC_STG.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN CPR_DEPR_CALC_STG.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN FCST_DEPR_CALC_STG_ARC.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN FCST_DEPR_LEDGER_BLEND_STG_ARC.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN DEPR_LEDGER_BLENDING_STG.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2585, 0, 2015, 2, 0, 0, 043464, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043464_reg_hist_lyr_4_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;