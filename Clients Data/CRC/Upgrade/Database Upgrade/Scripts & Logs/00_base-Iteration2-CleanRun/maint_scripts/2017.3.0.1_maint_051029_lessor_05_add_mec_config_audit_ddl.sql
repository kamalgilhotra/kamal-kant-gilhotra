/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051029_lessor_05_add_mec_config_audit_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.1 04/27/2018 Crystal Yura     Add Lessor MEC Config Table Audit trigger
||============================================================================
*/
CREATE OR REPLACE TRIGGER 
PP_AU_LSR_MEC_CONFIG AFTER 
 INSERT  OR  UPDATE  OR  DELETE  OF ACCRUALS, AUTO_TERMINATION, COMPANY_ID, CURRENCY_GAIN_LOSS, INVOICES
 ON LSR_MEC_CONFIG FOR EACH ROW 
DECLARE  
  prog varchar2(60);  
  osuser varchar2(60);  
  machine varchar2(60);  
  terminal varchar2(60);  
  old_lookup varchar2(250);  
  new_lookup varchar2(250);  
  pk_lookup varchar2(1500);  
  pk_temp varchar2(250);  
  window varchar2(60);  
  windowtitle varchar2(250);  
  comments varchar2(250);  
  trans varchar2(35);  
  ret number(22,0);  
BEGIN  
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then  
   return;  
end if; 
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window'); 
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),''); 
  trans        := nvl(sys_context('powerplant_ctx','process'     ),''); 
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),''); 
  prog         := nvl(sys_context('powerplant_ctx','program'     ),''); 
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),''); 
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),''); 
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),''); 
  comments     := substr( 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal), 1 , 250 );
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF; 
IF UPDATING THEN 
   IF :old.ACCRUALS <> :new.ACCRUALS OR 
     (:old.ACCRUALS is null AND :new.ACCRUALS is not null) OR 
     (:new.ACCRUALS is null AND :old.ACCRUALS is not null) THEN 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'ACCRUALS', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :old.ACCRUALS, :new.ACCRUALS, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.AUTO_TERMINATION <> :new.AUTO_TERMINATION OR 
     (:old.AUTO_TERMINATION is null AND :new.AUTO_TERMINATION is not null) OR 
     (:new.AUTO_TERMINATION is null AND :old.AUTO_TERMINATION is not null) THEN 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'AUTO_TERMINATION', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :old.AUTO_TERMINATION, :new.AUTO_TERMINATION, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.COMPANY_ID <> :new.COMPANY_ID OR 
     (:old.COMPANY_ID is null AND :new.COMPANY_ID is not null) OR 
     (:new.COMPANY_ID is null AND :old.COMPANY_ID is not null) THEN 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'COMPANY_ID', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :old.COMPANY_ID, :new.COMPANY_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.CURRENCY_GAIN_LOSS <> :new.CURRENCY_GAIN_LOSS OR 
     (:old.CURRENCY_GAIN_LOSS is null AND :new.CURRENCY_GAIN_LOSS is not null) OR 
     (:new.CURRENCY_GAIN_LOSS is null AND :old.CURRENCY_GAIN_LOSS is not null) THEN 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'CURRENCY_GAIN_LOSS', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :old.CURRENCY_GAIN_LOSS, :new.CURRENCY_GAIN_LOSS, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.INVOICES <> :new.INVOICES OR 
     (:old.INVOICES is null AND :new.INVOICES is not null) OR 
     (:new.INVOICES is null AND :old.INVOICES is not null) THEN 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'INVOICES', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :old.INVOICES, :new.INVOICES, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
END IF; 
IF INSERTING THEN 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'ACCRUALS', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :new.ACCRUALS, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'AUTO_TERMINATION', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :new.AUTO_TERMINATION, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'COMPANY_ID', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :new.COMPANY_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'CURRENCY_GAIN_LOSS', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :new.CURRENCY_GAIN_LOSS, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'INVOICES', 
        ''|| 'COMPANY_ID=' || :new.COMPANY_ID||'; ', pk_lookup, :new.INVOICES, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
END IF; 
IF DELETING THEN 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'ACCRUALS', 
        ''|| 'COMPANY_ID=' || :old.COMPANY_ID||'; ', pk_lookup, :old.ACCRUALS, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'AUTO_TERMINATION', 
        ''|| 'COMPANY_ID=' || :old.COMPANY_ID||'; ', pk_lookup, :old.AUTO_TERMINATION, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'COMPANY_ID', 
        ''|| 'COMPANY_ID=' || :old.COMPANY_ID||'; ', pk_lookup, :old.COMPANY_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'CURRENCY_GAIN_LOSS', 
        ''|| 'COMPANY_ID=' || :old.COMPANY_ID||'; ', pk_lookup, :old.CURRENCY_GAIN_LOSS, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_LEASE_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'LSR_MEC_CONFIG', 'INVOICES', 
        ''|| 'COMPANY_ID=' || :old.COMPANY_ID||'; ', pk_lookup, :old.INVOICES, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
END IF; 
END; 

/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6846, 0, 2017, 3, 0, 1, 51029, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051029_lessor_05_add_mec_config_audit_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;