/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029321_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   02/25/2013 Julia Breuer   Point Release
||============================================================================
*/

--
-- Create a new reserve method that allows unit cost amounts to have reserve factors applied.
--
insert into PWRPLANT.PT_RESERVE_METHOD ( RESERVE_METHOD_ID, DESCRIPTION ) values ( 7, 'Unit Cost Calculated Reserve' );

--
-- Create a table to store the amounts by vintage.
--
create table PWRPLANT.PT_UNIT_COST_AMOUNT_VINTAGE
(
 UNIT_COST_ID number(22,0)   not null,
 TAX_YEAR     number(22,0)   not null,
 VINTAGE      number(22,0)   not null,
 TIME_STAMP   date,
 USER_ID      varchar2(18),
 UNIT_AMOUNT  number(22,2)   not null
);

alter table PWRPLANT.PT_UNIT_COST_AMOUNT_VINTAGE
   add constraint PT_UNIT_COST_AMT_VINT_PK
       primary key ( UNIT_COST_ID, TAX_YEAR, VINTAGE)
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_UNIT_COST_AMOUNT_VINTAGE
   add constraint PT_UNIT_COST_AMT_VINT_TY_FK
       foreign key ( TAX_YEAR )
       references PWRPLANT.PROPERTY_TAX_YEAR;

alter table PWRPLANT.PT_UNIT_COST_AMOUNT_VINTAGE
   add constraint PT_UNIT_COST_AMT_VINT_UC_FK
       foreign key ( UNIT_COST_ID )
       references PWRPLANT.PT_UNIT_COST;

--
-- Create a table to store the different unit cost types.
--
create table PWRPLANT.PT_UNIT_COST_TYPE
(
 UNIT_COST_TYPE_ID number(22,0)   not null,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(35)   not null,
 LONG_DESCRIPTION  varchar2(254)
);

alter table PWRPLANT.PT_UNIT_COST_TYPE
   add constraint PT_UNIT_COST_TYPE_PK
       primary key (UNIT_COST_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

insert into PWRPLANT.PT_UNIT_COST_TYPE ( UNIT_COST_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION ) values ( 1, 'Uniform', 'Uniform - one amount for all property' );
insert into PWRPLANT.PT_UNIT_COST_TYPE ( UNIT_COST_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION ) values ( 2, 'By Vintage', 'By Vintage - one amount for each vintage' );

--
-- Add unit cost type to the unit cost table.
--
alter table PWRPLANT.PT_UNIT_COST add UNIT_COST_TYPE_ID number(22,0);

alter table PWRPLANT.PT_UNIT_COST
   add constraint PT_UNIT_COST_UCTYPE_FK
       foreign key ( UNIT_COST_TYPE_ID )
       references PWRPLANT.PT_UNIT_COST_TYPE;

update PWRPLANT.PT_UNIT_COST set UNIT_COST_TYPE_ID = 1 where UNIT_COST_TYPE_ID is null;

alter table PWRPLANT.PT_UNIT_COST modify UNIT_COST_TYPE_ID not null;

--
-- Remove the unit cost tables from Table Maintenance.  They will now be maintained via a workspace in the Admin Center.
--
delete from PWRPLANT.PP_TABLE_GROUPS    where TABLE_NAME in ( 'pt_unit_cost', 'pt_unit_cost_amount' );
delete from PWRPLANT.POWERPLANT_COLUMNS where TABLE_NAME in ( 'pt_unit_cost', 'pt_unit_cost_amount' );
delete from PWRPLANT.POWERPLANT_TABLES  where TABLE_NAME in ( 'pt_unit_cost', 'pt_unit_cost_amount' );

--
-- Create an import type for unit cost amounts.
--
insert into PWRPLANT.PP_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION ) values ( 43, sysdate, user, 'Add/Update : Unit Cost Amounts', 'Import Unit Cost Amounts', 'pt_import_unit_cost', 'pt_import_unit_cost_archive', null, 0, 'uo_ptc_logic_import', '', 'Unit Cost Amount' );

insert into PWRPLANT.PP_IMPORT_TYPE_SUBSYSTEM ( IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID ) values ( 43, 1, sysdate, user );

insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 43, 'unit_cost_id', sysdate, user, 'Unit Cost Definition', 'unit_cost_xlate', 1, 1, 'number(22,0)', 'pt_unit_cost', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 43, 'tax_year', sysdate, user, 'Tax Year', 'tax_year_xlate', 1, 1, 'number(22,0)', 'property_tax_year', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 43, 'vintage', sysdate, user, 'Vintage', '', 0, 1, 'number(22,0)', '', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 43, 'unit_amount', sysdate, user, 'Unit Amount', '', 1, 1, 'number(22,2)', '', '', 1, null );

insert into PWRPLANT.PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN ) values ( 201, sysdate, user, 'PT Unit Cost.Description', 'The passed in value corresponds to the PT Unit Cost: Description field.  Translate to the Unit Cost ID using the Description column on the PT Unit Cost table.', 'unit_cost_id', '( select uc.unit_cost_id from pt_unit_cost uc where upper( trim( <importfield> ) ) = upper( trim( uc.description ) ) )', 0, 'pt_unit_cost', 'description', '', '', null );
insert into PWRPLANT.PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN ) values ( 202, sysdate, user, 'PT Unit Cost.Long Description', 'The passed in value corresponds to the PT Unit Cost: Long Description field.  Translate to the Unit Cost ID using the Long Description column on the PT Unit Cost table.', 'unit_cost_id', '( select uc.unit_cost_id from pt_unit_cost uc where upper( trim( <importfield> ) ) = upper( trim( uc.long_description ) ) )', 0, 'pt_unit_cost', 'long_description', '', '', null );

insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 43, 'unit_cost_id', 201, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 43, 'unit_cost_id', 202, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 43, 'tax_year', 35, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 43, 'tax_year', 36, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 43, 'tax_year', 37, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 43, 'tax_year', 38, sysdate, user );

create table PWRPLANT.PT_IMPORT_UNIT_COST
(
 IMPORT_RUN_ID     number(22,0)   not null,
 LINE_ID           number(22,0)   not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 TAX_YEAR_XLATE    varchar2(254),
 TAX_YEAR          number(22,0),
 UNIT_COST_XLATE   varchar2(254),
 UNIT_COST_ID      number(22,0),
 VINTAGE           varchar2(35),
 UNIT_AMOUNT       varchar2(35),
 ERROR_MESSAGE     varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_UNIT_COST
   add constraint PT_IMPORT_UNIT_COST_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_UNIT_COST
   add constraint PT_IMPT_UC_IMPT_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_UNIT_COST_ARCHIVE
(
 IMPORT_RUN_ID    number(22,0)   not null,
 LINE_ID          number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 TAX_YEAR_XLATE   varchar2(254),
 TAX_YEAR         number(22,0),
 UNIT_COST_XLATE  varchar2(254),
 UNIT_COST_ID     number(22,0),
 VINTAGE          varchar2(35),
 UNIT_AMOUNT      varchar2(35)
);

alter table PWRPLANT.PT_IMPORT_UNIT_COST_ARCHIVE
   add constraint PT_IMPORT_UNIT_COST_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_UNIT_COST_ARCHIVE
   add constraint PT_IMPT_UC_ARC_IMPT_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PP_IMPORT_RUN;

--
-- Create new Returns Center steps that allow importing of unit cost amounts and check for missing unit cost amounts.
--

-- Increment default step numbers for steps after these new steps.
update PWRPLANT.PT_PROCESS set DEFAULT_STEP_NUMBER = DEFAULT_STEP_NUMBER + 2 where DEFAULT_STEP_NUMBER >= 27;

-- Add new steps.
insert into PWRPLANT.PT_PROCESS ( PT_PROCESS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SHORT_DESCRIPTION, IDENTIFIER, PT_PROCESS_TYPE_ID, IS_BASE_PROCESS, DEFAULT_STEP_NUMBER, OBJECT_NAME, DOCUMENTATION ) values ( 53, sysdate, user, 'Import Unit Cost Amounts', 'Import unit cost amounts.', 'Import Unit Cost Amts', 'import_unit_cost_amts', 1, 1, 27, 'uo_ptc_rtncntr_wksp_import_unit_cost', null );
insert into PWRPLANT.PT_PROCESS ( PT_PROCESS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SHORT_DESCRIPTION, IDENTIFIER, PT_PROCESS_TYPE_ID, IS_BASE_PROCESS, DEFAULT_STEP_NUMBER, OBJECT_NAME, DOCUMENTATION ) values ( 54, sysdate, user, 'Unit Cost Audit', 'Check for any ledger items missing unit cost amounts.', 'Unit Cost Audit', 'unit_cost_audit', 1, 1, 28, 'uo_ptc_rtncntr_wksp_audit_unit_cost', null );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (309, 0, 10, 4, 1, 0, 29321, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029321_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
