/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049656_lessor_01_create_accrual_stg_table_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/09/2017 Josh Sandler     Create lsr_monthly_accrual_stg
||============================================================================
*/

CREATE TABLE lsr_monthly_accrual_stg (
  accrual_id       NUMBER(22,0) NOT NULL,
  accrual_type_id  NUMBER(22,0) NULL,
  ilr_id           NUMBER(22,0) NULL,
  amount           NUMBER(22,2) NULL,
  gl_posting_mo_yr DATE         NULL,
  set_of_books_id  NUMBER(22,0) NULL
)
/

ALTER TABLE lsr_monthly_accrual_stg
  ADD CONSTRAINT pk_lsr_monthly_accrual_stg PRIMARY KEY (
    accrual_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE lsr_monthly_accrual_stg
  ADD CONSTRAINT fk_lsr_monthly_accrual_stg1 FOREIGN KEY (
    ilr_id
  ) REFERENCES lsr_ilr (
    ilr_id
  )
/

ALTER TABLE lsr_monthly_accrual_stg
  ADD CONSTRAINT fk_lsr_monthly_accrual_stg2 FOREIGN KEY (
    accrual_type_id
  ) REFERENCES ls_payment_type (
    payment_type_id
  )
/

COMMENT ON TABLE lsr_monthly_accrual_stg IS '(S)  [06]
The LSR Monthly Accrual Stg table holds accrual amounts at an ILR level for a given month and set of books.';

COMMENT ON COLUMN lsr_monthly_accrual_stg.accrual_id IS 'Internal system generated id.';
COMMENT ON COLUMN lsr_monthly_accrual_stg.accrual_type_id IS 'The internal leased accrual type id within PowerPlant.';
COMMENT ON COLUMN lsr_monthly_accrual_stg.ilr_id IS 'The internal ILR id within PowerPlant.';
COMMENT ON COLUMN lsr_monthly_accrual_stg.amount IS 'The accrued amount for this period.';
COMMENT ON COLUMN lsr_monthly_accrual_stg.gl_posting_mo_yr IS 'The accounting period.';
COMMENT ON COLUMN lsr_monthly_accrual_stg.set_of_books_id IS 'The internal set of books id within PowerPlant.';



CREATE SEQUENCE lsr_monthly_accrual_stg_seq;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3920, 0, 2017, 1, 0, 0, 49656, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049656_lessor_01_create_accrual_stg_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	sys_context('USERENV', 'SERVICE_NAME'));
COMMIT;