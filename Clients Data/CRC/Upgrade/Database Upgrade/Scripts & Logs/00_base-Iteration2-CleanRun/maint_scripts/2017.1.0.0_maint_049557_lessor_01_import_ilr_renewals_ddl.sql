/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049557_lessor_01_ilr_renewal_import_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/10/2017 Shane "C" Ward    Updates to Lessor Renewal Options
||============================================================================
*/

ALTER TABLE LSR_IMPORT_ILR_RENEWAL
  ADD renewal_start_date VARCHAR2(35);

COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.renewal_start_date IS 'Date of Renewal Start in ''MMDDYYYY'' format';

COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.ilr_renewal_id IS 'System-assigned ID of Renewal for ILR and Revision combination';

COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL.ilr_renewal_option_id IS 'User defined value to group renewal options together';


ALTER TABLE LSR_IMPORT_ILR_RENEWAL_ARCHIVE
  ADD renewal_start_date VARCHAR2(35);

COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_start_date IS 'Date of Renewal Start in ''MMDDYYYY'' format';

COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_renewal_id IS 'System-assigned ID of Renewal for ILR and Revision combination';

COMMENT ON COLUMN LSR_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_renewal_option_id IS 'User defined value to group renewal options together';

  --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3925, 0, 2017, 1, 0, 0, 49557, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049557_lessor_01_import_ilr_renewals_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;