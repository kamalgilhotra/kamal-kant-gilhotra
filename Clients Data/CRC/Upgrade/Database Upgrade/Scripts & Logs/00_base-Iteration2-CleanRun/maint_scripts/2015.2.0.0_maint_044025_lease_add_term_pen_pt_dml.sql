/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044025_lease_add_term_pen_pt.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/15/2015 Will Davis    Add termination penalty payment type
||============================================================================
*/

insert into ls_payment_type(payment_type_id, description)
select 23, 'Termination Penalty' from dual;

insert into ls_payment_type(payment_type_id, description)
select 24, 'Sales Proceeds' from dual;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2610, 0, 2015, 2, 0, 0, 044025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044025_lease_add_term_pen_pt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;