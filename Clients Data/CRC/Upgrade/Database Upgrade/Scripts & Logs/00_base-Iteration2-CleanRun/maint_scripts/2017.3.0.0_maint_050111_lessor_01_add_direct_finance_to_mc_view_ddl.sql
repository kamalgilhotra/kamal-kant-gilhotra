/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050111_lessor_01_add_direct_finance_to_mc_view_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 02/18/2018 Jared Watkins    Add the Direct Finance columns to the Lessor MC View
||============================================================================
*/

CREATE OR REPLACE VIEW "PWRPLANT"."V_LSR_ILR_MC_SCHEDULE" ("ILR_ID", "ILR_NUMBER", "LEASE_ID", "LEASE_NUMBER", "CURRENT_REVISION", "REVISION", "SET_OF_BOOKS_ID", "MONTH", "COMPANY_ID", "OPEN_MONTH", "LS_CUR_TYPE", "EXCHANGE_DATE", "PREV_EXCHANGE_DATE", "CONTRACT_CURRENCY_ID", "DISPLAY_CURRENCY_ID", "RATE", "CALCULATED_RATE", "PREVIOUS_CALCULATED_RATE", "ISO_CODE", "CURRENCY_DISPLAY_SYMBOL", "INTEREST_INCOME_RECEIVED", "INTEREST_INCOME_ACCRUED", "BEG_DEFERRED_REV", "DEFERRED_REV_ACTIVITY", "END_DEFERRED_REV", "BEG_RECEIVABLE", "END_RECEIVABLE", "BEG_LONG_TERM_RECEIVABLE", "END_LONG_TERM_RECEIVABLE", "INITIAL_DIRECT_COST", "EXECUTORY_ACCRUAL1", "EXECUTORY_ACCRUAL2", "EXECUTORY_ACCRUAL3", "EXECUTORY_ACCRUAL4", "EXECUTORY_ACCRUAL5", "EXECUTORY_ACCRUAL6", "EXECUTORY_ACCRUAL7", "EXECUTORY_ACCRUAL8", "EXECUTORY_ACCRUAL9", "EXECUTORY_ACCRUAL10", "EXECUTORY_PAID1", "EXECUTORY_PAID2", "EXECUTORY_PAID3", "EXECUTORY_PAID4", "EXECUTORY_PAID5", "EXECUTORY_PAID6", "EXECUTORY_PAID7", "EXECUTORY_PAID8", "EXECUTORY_PAID9", "EXECUTORY_PAID10", "CONTINGENT_ACCRUAL1", "CONTINGENT_ACCRUAL2", "CONTINGENT_ACCRUAL3", "CONTINGENT_ACCRUAL4", "CONTINGENT_ACCRUAL5", "CONTINGENT_ACCRUAL6", "CONTINGENT_ACCRUAL7", "CONTINGENT_ACCRUAL8", "CONTINGENT_ACCRUAL9", "CONTINGENT_ACCRUAL10", "CONTINGENT_PAID1", "CONTINGENT_PAID2", "CONTINGENT_PAID3", "CONTINGENT_PAID4", "CONTINGENT_PAID5", "CONTINGENT_PAID6", "CONTINGENT_PAID7", "CONTINGENT_PAID8", "CONTINGENT_PAID9", "CONTINGENT_PAID10", "PRINCIPAL_RECEIVED", "PRINCIPAL_ACCRUED", "BEG_UNGUARANTEED_RESIDUAL", "INTEREST_UNGUARANTEED_RESIDUAL", "ENDING_UNGUARANTEED_RESIDUAL", "BEG_NET_INVESTMENT", "INTEREST_NET_INVESTMENT", "ENDING_NET_INVESTMENT", "BEGINNING_DEFERRED_PROFIT", "RECOGNIZED_PROFIT", "ENDING_DEFERRED_PROFIT", "GAIN_LOSS_FX", "RATES_EXCHANGE_DATE", "RATES_RATE", "RATES_LAST_EXCHANGE_DATE", "RATES_LAST_RATE") AS 
  WITH cur AS ( SELECT ls_currency_type_id AS ls_cur_type,
       currency_id,
       currency_display_symbol,
       iso_code,
         CASE
      ls_currency_type_id
      WHEN
        1
      THEN
        1
      ELSE
        NULL
    END
  AS contract_approval_rate
FROM currency
  CROSS JOIN ls_lease_currency_type
),open_month AS ( SELECT company_id,
       MIN(gl_posting_mo_yr) open_month
FROM lsr_process_control WHERE open_next IS NULL GROUP BY
  company_id
),calc_rate AS ( SELECT a.company_id,
       a.contract_currency_id,
       a.company_currency_id,
       a.accounting_month,
       a.exchange_date,
       a.rate,
       b.rate prev_rate
FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = add_months(b.accounting_month,1)
),rate_now AS ( SELECT currency_from,
       currency_to,
       rate
FROM ( SELECT currency_from,
         currency_to,
         rate,
         ROW_NUMBER() OVER(PARTITION BY
      currency_from,
      currency_to
      ORDER BY
        exchange_date
      DESC
    ) AS rn
  FROM currency_rate_default_dense WHERE trunc(exchange_date,'MONTH') <= trunc(SYSDATE,'MONTH')
    AND exchange_rate_type_id = 1
  )
WHERE rn = 1 ) 
SELECT schedule.ilr_id ilr_id,
  schedule.ilr_number,
  lease.lease_id,
  lease.lease_number,
  schedule.current_revision,
  schedule.revision revision,
  schedule.set_of_books_id set_of_books_id,
  schedule.month month,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  rates.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  lease.contract_currency_id,
  cur.currency_id display_currency_id,
  rates.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  schedule.interest_income_received * nvl(
    calc_rate.rate,
    rates.rate
  ) interest_income_received,
  schedule.interest_income_accrued * nvl(
    calc_rate.rate,
    rates.rate
  ) interest_income_accrued,
  schedule.beg_deferred_rev * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_deferred_rev,
  schedule.deferred_rev_activity * nvl(
    calc_rate.rate,
    rates.rate
  ) deferred_rev_activity,
  schedule.end_deferred_rev * nvl(
    calc_rate.rate,
    rates.rate
  ) end_deferred_rev,
  schedule.beg_receivable * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_receivable,
  schedule.end_receivable * nvl(
    calc_rate.rate,
    rates.rate
  ) end_receivable,
  schedule.beg_lt_receivable * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_long_term_receivable,
  schedule.end_lt_receivable * nvl(
    calc_rate.rate,
    rates.rate
  ) end_long_term_receivable,
  schedule.initial_direct_cost * nvl(
    calc_rate.rate,
    rates.rate
  ) initial_direct_cost,
  schedule.executory_accrual1 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual1,
  schedule.executory_accrual2 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual2,
  schedule.executory_accrual3 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual3,
  schedule.executory_accrual4 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual4,
  schedule.executory_accrual5 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual5,
  schedule.executory_accrual6 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual6,
  schedule.executory_accrual7 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual7,
  schedule.executory_accrual8 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual8,
  schedule.executory_accrual9 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual9,
  schedule.executory_accrual10 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_accrual10,
  schedule.executory_paid1 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid1,
  schedule.executory_paid2 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid2,
  schedule.executory_paid3 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid3,
  schedule.executory_paid4 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid4,
  schedule.executory_paid5 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid5,
  schedule.executory_paid6 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid6,
  schedule.executory_paid7 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid7,
  schedule.executory_paid8 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid8,
  schedule.executory_paid9 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid9,
  schedule.executory_paid10 * nvl(
    calc_rate.rate,
    rates.rate
  ) executory_paid10,
  schedule.contingent_accrual1 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual1,
  schedule.contingent_accrual2 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual2,
  schedule.contingent_accrual3 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual3,
  schedule.contingent_accrual4 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual4,
  schedule.contingent_accrual5 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual5,
  schedule.contingent_accrual6 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual6,
  schedule.contingent_accrual7 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual7,
  schedule.contingent_accrual8 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual8,
  schedule.contingent_accrual9 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual9,
  schedule.contingent_accrual10 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_accrual10,
  schedule.contingent_paid1 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid1,
  schedule.contingent_paid2 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid2,
  schedule.contingent_paid3 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid3,
  schedule.contingent_paid4 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid4,
  schedule.contingent_paid5 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid5,
  schedule.contingent_paid6 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid6,
  schedule.contingent_paid7 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid7,
  schedule.contingent_paid8 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid8,
  schedule.contingent_paid9 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid9,
  schedule.contingent_paid10 * nvl(
    calc_rate.rate,
    rates.rate
  ) contingent_paid10,
  schedule.principal_received * nvl(
    calc_rate.rate,
    rates.rate
  ) principal_received,
  schedule.principal_accrued * nvl(
    calc_rate.rate,
    rates.rate
  ) principal_accrued,
  schedule.beg_unguaranteed_residual * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_unguaranteed_residual,
  schedule.interest_unguaranteed_residual * nvl(
    calc_rate.rate,
    rates.rate
  ) interest_unguaranteed_residual,
  schedule.ending_unguaranteed_residual * nvl(
    calc_rate.rate,
    rates.rate
  ) ending_unguaranteed_residual,
  schedule.beg_net_investment * nvl(
    calc_rate.rate,
    rates.rate
  ) beg_net_investment,
  schedule.interest_net_investment * nvl(
    calc_rate.rate,
    rates.rate
  ) interest_net_investment,
  schedule.ending_net_investment * nvl(
    calc_rate.rate,
    rates.rate
  ) ending_net_investment,
  schedule.begin_deferred_profit * nvl(
    calc_rate.rate,
    rates.rate
  ) beginning_deferred_profit,
  schedule.recognized_profit * nvl(
    calc_rate.rate,
    rates.rate
  ) recognized_profit,
  schedule.end_deferred_profit * nvl(
    calc_rate.rate,
    rates.rate
  ) ending_deferred_profit,
  decode(calc_rate.rate, NULL, 0, ( schedule.beg_receivable * ( calc_rate.rate - coalesce(calc_rate.prev_rate, rates_last.rate, calc_rate.rate, 0 ) ) ) ) gain_loss_fx,
  rates.exchange_date,
  rates.rate,
  rates_last.exchange_date,
  rates_last.rate
FROM (SELECT schedule.ilr_id,
			 ilr.ilr_number,
			 ilr.current_revision,
			 schedule.revision,
			 schedule.set_of_books_id,
			 schedule.month,
			 schedule.interest_income_received,
			 schedule.interest_income_accrued,
			 schedule.beg_deferred_rev,
			 schedule.deferred_rev_activity,
			 schedule.end_deferred_rev,
			 schedule.beg_receivable,
			 schedule.end_receivable,
			 schedule.beg_lt_receivable,
			 schedule.end_lt_receivable,
			 schedule.initial_direct_cost,
			 schedule.executory_accrual1,
			 schedule.executory_accrual2,
			 schedule.executory_accrual3,
			 schedule.executory_accrual4,
			 schedule.executory_accrual5,
			 schedule.executory_accrual6,
			 schedule.executory_accrual7,
			 schedule.executory_accrual8,
			 schedule.executory_accrual9,
			 schedule.executory_accrual10,
			 schedule.executory_paid1,
			 schedule.executory_paid2,
			 schedule.executory_paid3,
			 schedule.executory_paid4,
			 schedule.executory_paid5,
			 schedule.executory_paid6,
			 schedule.executory_paid7,
			 schedule.executory_paid8,
			 schedule.executory_paid9,
			 schedule.executory_paid10,
			 schedule.contingent_accrual1,
			 schedule.contingent_accrual2,
			 schedule.contingent_accrual3,
			 schedule.contingent_accrual4,
			 schedule.contingent_accrual5,
			 schedule.contingent_accrual6,
			 schedule.contingent_accrual7,
			 schedule.contingent_accrual8,
			 schedule.contingent_accrual9,
			 schedule.contingent_accrual10,
			 schedule.contingent_paid1,
			 schedule.contingent_paid2,
			 schedule.contingent_paid3,
			 schedule.contingent_paid4,
			 schedule.contingent_paid5,
			 schedule.contingent_paid6,
			 schedule.contingent_paid7,
			 schedule.contingent_paid8,
			 schedule.contingent_paid9,
			 schedule.contingent_paid10,
			 st_schedule.principal_received,
			 st_schedule.principal_accrued,
			 st_schedule.beg_unguaranteed_residual,
			 st_schedule.interest_unguaranteed_residual,
			 st_schedule.ending_unguaranteed_residual,
			 st_schedule.beg_net_investment,
			 st_schedule.interest_net_investment,
			 st_schedule.ending_net_investment,
			 df_schedule.begin_deferred_profit,
			 df_schedule.recognized_profit,
			 df_schedule.end_deferred_profit,
			 ilr.lease_id,
			 ilr.company_id,
			 options.in_service_exchange_rate,
			 options.purchase_option_amt,
			 options.termination_amt,
			 schedule.rowid AS lisrowid,
			 options.rowid AS optrowid,
			 ilr.rowid AS ilrrowid,
			 lease.rowid AS leaserowid,
			 st_schedule.rowid AS salesrowid
		FROM lsr_ilr_schedule schedule,
			 lsr_ilr_options options,
			 lsr_ilr ilr,
			 lsr_lease lease,
			 lsr_ilr_schedule_sales_direct st_schedule,
			 lsr_ilr_schedule_direct_fin df_schedule
		WHERE schedule.ilr_id = options.ilr_id
		  AND schedule.revision = options.revision
		  AND schedule.ilr_id = ilr.ilr_id
		  AND ilr.lease_id = lease.lease_id
		  AND st_schedule.ilr_id (+) = schedule.ilr_id
		  AND st_schedule.revision (+) = schedule.revision
		  AND st_schedule.month (+) = schedule.month
		  AND st_schedule.set_of_books_id (+) = schedule.set_of_books_id
		  AND df_schedule.ilr_id (+) = schedule.ilr_id
		  AND df_schedule.revision (+) = schedule.revision
		  AND df_schedule.month (+) = schedule.month
		  AND df_schedule.set_of_books_id (+) = schedule.set_of_books_id
  ) schedule
  INNER JOIN lsr_lease lease ON schedule.lease_id = lease.lease_id
  INNER JOIN currency_schema cs ON schedule.company_id = cs.company_id
  INNER JOIN cur ON cur.currency_id =
    CASE
      cur.ls_cur_type
      WHEN
        1
      THEN
        lease.contract_currency_id
      WHEN
        2
      THEN
        cs.currency_id
    END
  INNER JOIN open_month ON schedule.company_id = open_month.company_id
  INNER JOIN currency_rate_default_dense rates ON cur.currency_id = rates.currency_to
  AND lease.contract_currency_id = rates.currency_from
  AND trunc(rates.exchange_date,'MONTH') = trunc(schedule.month,'MONTH')
  INNER JOIN currency_rate_default_dense rates_last ON cur.currency_id = rates_last.currency_to
  AND lease.contract_currency_id = rates_last.currency_from
  AND trunc(rates_last.exchange_date,'MONTH') = trunc(Add_Months(schedule.MONTH, -1),'MONTH')
  INNER JOIN rate_now ON cur.currency_id = rate_now.currency_to
  AND lease.contract_currency_id = rate_now.currency_from
  LEFT OUTER JOIN calc_rate ON lease.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND schedule.company_id = calc_rate.company_id
  AND schedule.month = calc_rate.accounting_month
WHERE cs.currency_type_id = 1
  AND rates.exchange_rate_type_id = 1
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4146, 0, 2017, 3, 0, 0, 50111, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050111_lessor_01_add_direct_finance_to_mc_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	