/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052459_lessee_01_impair_wksp_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/23/2018 K Powers       Impairment Workspace
||============================================================================
*/

-- New Table to hold working impairments

create table LS_ILR_IMPAIR
(ilr_id   number(22,0),
revision  number(22,0),
month     date,
set_of_books_id number(22,0),
impairment_activity number,
processed_flag  number(22,0));

ALTER TABLE LS_ILR_IMPAIR 
ADD PRIMARY KEY(ilr_id, month, revision, set_of_books_id) using index tablespace pwrplant_idx;

ALTER TABLE LS_ILR_IMPAIR 
ADD CONSTRAINT FK_LS_ILR_IMPAIR_1 
  FOREIGN KEY (ILR_ID)
  REFERENCES LS_ILR(ILR_ID);
  
ALTER TABLE LS_ILR_IMPAIR 
ADD CONSTRAINT FK_LS_ILR_IMPAIR_2 
  FOREIGN KEY (SET_OF_BOOKS_ID)
  REFERENCES SET_OF_BOOKS(SET_OF_BOOKS_ID);

-- Add Impairment Fields to the Schedule tables

alter table ls_ilr_schedule add BEGIN_ACCUM_IMPAIR NUMBER(22,2);
alter table ls_ilr_schedule add IMPAIRMENT_ACTIVITY NUMBER(22,2);
alter table ls_ilr_schedule add END_ACCUM_IMPAIR NUMBER(22,2);

alter table ls_asset_schedule add BEGIN_ACCUM_IMPAIR NUMBER(22,2);
alter table ls_asset_schedule add IMPAIRMENT_ACTIVITY NUMBER(22,2);
alter table ls_asset_schedule add END_ACCUM_IMPAIR NUMBER(22,2);

-- Add Table comments

COMMENT ON COLUMN LS_ASSET_SCHEDULE.BEGIN_ACCUM_IMPAIR IS 'Beginning accumulated impairment amount for the Asset.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.IMPAIRMENT_ACTIVITY IS 'Impairment amount for the Asset.';
COMMENT ON COLUMN LS_ASSET_SCHEDULE.END_ACCUM_IMPAIR IS 'Ending accumulated impairment amount for the Asset.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.BEGIN_ACCUM_IMPAIR IS 'Beginning accumulated impairment amount for the ILR.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.IMPAIRMENT_ACTIVITY IS 'Impairment amount for the ILR.';
COMMENT ON COLUMN LS_ILR_SCHEDULE.END_ACCUM_IMPAIR IS 'Ending accumulated impairment amount for the ILR.';

COMMENT ON TABLE LS_ILR_IMPAIR IS 'Table for ILR Impairments for the current month.';
COMMENT ON COLUMN LS_ILR_IMPAIR.ILR_ID IS 'The unique ILR identifier.';
COMMENT ON COLUMN LS_ILR_IMPAIR.REVISION IS 'The ILR current revision.';
COMMENT ON COLUMN LS_ILR_IMPAIR.MONTH IS 'The current open month.';
COMMENT ON COLUMN LS_ILR_IMPAIR.SET_OF_BOOKS_ID IS 'Unique Set of Books identifier';
COMMENT ON COLUMN LS_ILR_IMPAIR.IMPAIRMENT_ACTIVITY IS 'Impairment dollar amount. A negative value is an impairment reversal.';
COMMENT ON COLUMN LS_ILR_IMPAIR.PROCESSED_FLAG IS 'Details status of ILR impairment: 0-Unprocessed; 1=Processed; 2=Approved.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11085, 0, 2018, 1, 0, 0, 52459, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052459_lessee_01_impair_wksp_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;