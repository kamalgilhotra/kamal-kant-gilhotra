/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045081_budget_module_mypp_shortcuts_2_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2.0 10/14/2015 Chris Mardis   Re-work the MyPowerPlan shortcuts for the Budget Module
 ||============================================================================
 */

SET SERVEROUTPUT ON
 
declare
   new_task_id number;
   task_budget_mgmt number;
   task_proj_budget number;
   task_budget_versions number;
   task_bv_saved_search number;
   task_bv_select_all number;
   task_bv_details number;
   task_budget_items number;
   task_bi_saved_search number;
   task_bi_select_all number;
   task_bi_analysis number;
   task_dept_budget number;
begin
   delete from bv_pp_mypp_task;
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows deleted from bv_pp_mypp_task');
   
   delete from bv_pp_mypp_task_step;
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows deleted from bv_pp_mypp_task_step');
   
   
   select nvl(max(task_id),0) into new_task_id
   from pp_mypp_task;
   
   
   --Budget Management
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Management', 'Budget Management', null, 0, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_top_frame', 'm_top_frame.m_application.m_budget', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_budget_mgmt := new_task_id;
   
   
   
   --Budget Management--Project Budgets
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Project Budgets', 'Budgets > Project', task_budget_mgmt, 0, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_main', 'm_budget_main.m_application.m_capital', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_proj_budget := new_task_id;
   
   
   
   --Budget Management--Project Budgets--Budget Versions
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Select Tabs', 'Budgets > Project > Versions', task_proj_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_cap_main', 'm_budget_cap_main.m_application.m_versions', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_budget_versions := new_task_id;
   
   
   --Budget Management--Project Budgets--Budget Versions--Saved Search
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Saved Search - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Select Tabs Saved Search', 'Budget Version Select Tabs Query', task_budget_versions, 1, 'uo_mypp_pop_up_bv', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'cb_save_query', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_bv_saved_search := new_task_id;
   
   
   --Budget Management--Project Budgets--Budget Versions--Saved Search--Select All
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Saved Search--Select All - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Saved Query Select All', 'Budgets > Project > Versions > Select All', task_bv_saved_search, 0, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'cb_select_all', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_bv_select_all := new_task_id;
   
   
   --Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Details
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Details - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Details', 'Budgets > Project > Versions > Select All > Details', task_bv_select_all, 1, 'uo_mypp_pop_up_bv', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'm_budget_version.m_application.m_details', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_bv_details := new_task_id;
   
   
   --Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Details--Options
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Details--Options - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Detail Options', 'Budgets > Project > Versions > Select All > Details > Options', task_bv_details, 1, 'uo_mypp_pop_up_bv', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_version_control', 'cb_options', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Funding Projects
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Funding Projects - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Funding Projects', 'Budgets > Project > Versions > Select All > Funding Projects', task_bv_select_all, 1, 'uo_mypp_pop_up_bv', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'm_budget_version.m_application.m_fundingprojects', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Process
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Process - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Processing', 'Budgets > Project > Versions > Select All > Process', task_bv_select_all, 1, 'uo_mypp_pop_up_bv', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'm_budget_version.m_application.m_processing', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Dashboard
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Dashboard - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Dashboard', 'Budgets > Project > Versions > Select All > Dashboard', task_bv_select_all, 1, 'uo_mypp_pop_up_bv', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'm_budget_version.m_application.m_dashboard', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Forecast
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Saved Search--Select All--Forecast - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Forecasting', 'Budgets > Project > Versions > Select All > Forecast', task_bv_select_all, 1, 'uo_mypp_pop_up_bv', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'm_budget_version.m_activity.m_customestimates', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Versions--Add
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Add - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Create a Budget Version', 'Budgets > Project > Versions > Add', task_budget_versions, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'm_budget_version.m_application.m_add', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Versions--Compare
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Compare - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Compare', 'Budgets > Project > Versions > Compare', task_budget_versions, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'm_budget_version.m_application.m_compareto', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Versions--Approval Statuses
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Versions--Approval Statuses - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Version Project Approval Statuses', 'Budgets > Project > Versions > Statuses', task_budget_versions, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_version_select_tabs', 'm_budget_version.m_application.m_approvalstatuses', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   
   --Budget Management--Project Budgets--Budget Items
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Items - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Select Tabs', 'Budgets > Project > Select', task_proj_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_cap_main', 'm_budget_cap_main.m_application.m_3', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_budget_items := new_task_id;
   
   
   --Budget Management--Project Budgets--Budget Items--Saved Search
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Items--Saved Search - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Select Tabs Saved Search', 'Budget Item Select Tabs Query', task_budget_items, 1, 'uo_mypp_pop_up_bi', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_select_tabs', 'cb_save_query', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_bi_saved_search := new_task_id;
   
   
   --Budget Management--Project Budgets--Budget Items--Saved Search--Select All
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Items--Saved Search--Select All - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Saved Query Select All', 'Budgets > Project > Select > Select All', task_bi_saved_search, 0, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_select_tabs', 'cb_select_all', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_bi_select_all := new_task_id;
   
   
   --Budget Management--Project Budgets--Budget Items--Saved Search--Select All--Details
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Items--Saved Search--Select All--Details - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Details', 'Budgets > Project > Select > Select All > Details', task_bi_select_all, 1, 'uo_mypp_pop_up_bi', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_select_tabs', 'm_budget_select.m_application.m_detail', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Items--Saved Search--Select All--Justifications
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Items--Saved Search--Select All--Justifications - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Justifications', 'Budgets > Project > Select > Select All > Justifications', task_bi_select_all, 1, 'uo_mypp_pop_up_bi', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_select_tabs', 'm_budget_select.m_application.m_justification', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Items--Saved Search--Select All--Estimates
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Items--Saved Search--Select All--Estimates - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Estimates', 'Budgets > Project > Select > Select All > Estimates', task_bi_select_all, 1, 'uo_mypp_pop_up_bi', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_select_tabs', 'm_budget_select.m_application.m_estimates.m_estimate', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Items--Add
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Items--Add - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Create a Budget Item', 'Budgets > Project > Select > Add', task_budget_items, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_select_tabs', 'm_budget_select.m_application.m_addlike', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   
   --Budget Management--Project Budgets--Budget Item Analysis
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Item Analysis - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Analysis', 'Budgets > Project > Analysis', task_proj_budget, 0, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_cap_main', 'm_budget_cap_main.m_application.m_drilldown', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_bi_analysis := new_task_id;
   
   
   --Budget Management--Project Budgets--Budget Item Analysis--Amounts
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Item Analysis--Amounts - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Amounts', 'Budgets > Project > Analysis > Amounts', task_bi_analysis, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_review_options', 'm_budget_analysis.m_application.m_budgetedamounts', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Item Analysis--Act vs Bud
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Item Analysis--Act vs Bud - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Act vs Bud', 'Budgets > Project > Analysis > Actuals vs Budget', task_bi_analysis, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_review_options', 'm_budget_analysis.m_application.m_actualsvsbudgeted', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Item Analysis--Query
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Item Analysis--Query - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Query', 'Budgets > Project > Analysis > Query', task_bi_analysis, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_review_options', 'm_budget_analysis.m_application.m_query', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Item Analysis--Graphic Analysis
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Item Analysis--Graphic Analysis - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Graphic Analysis', 'Budgets > Project > Analysis > Graphic Analysis', task_bi_analysis, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_review_options', 'm_budget_analysis.m_application.m_graphicanalysis', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Item Approval
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Item Approval - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Approval', 'Budgets > Project > Approval', task_proj_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_cap_main', 'm_budget_cap_main.m_application.m_6', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   --Budget Management--Project Budgets--Budget Item Ranking
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Item Ranking - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Ranking', 'Budgets > Project > Ranking', task_proj_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_cap_main', 'm_budget_cap_main.m_application.m_ranking', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Project Budgets--Budget Item Reporting
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Project Budgets--Budget Item Reporting - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Budget Item Reporting', 'Budgets > Project > Reports', task_proj_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_cap_main', 'm_budget_cap_main.m_application.m_reports', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   
   --Budget Management--Departmental Budgets
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Departmental Budgets - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Departmental Budgets', 'Budgets > Dept', task_budget_mgmt, 0, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_budget_main', 'm_budget_main.m_application.m_om', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   task_dept_budget := new_task_id;
   
   
   --Budget Management--Departmental Budgets--Approve
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Departmental Budgets--Approve - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Departmental Budget Approval', 'Budgets > Dept > Approve', task_dept_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_cr_budgets', 'm_cr_budgets.m_activity.m_approval.m_approveandpost', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Departmental Budgets--Versions
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Departmental Budgets--Versions - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Departmental Budget Versions', 'Budgets > Dept > Versions', task_dept_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_cr_budgets', 'm_cr_budgets.m_activity.m_version.m_versions', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Departmental Budgets--Entry
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Departmental Budgets--Entry - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Departmental Budget Entry', 'Budgets > Dept > Entry', task_dept_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_cr_budgets', 'm_cr_budgets.m_activity.m_entry', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Departmental Budgets--Queries
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Departmental Budgets--Queries - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Departmental Budget Queries', 'Budgets > Dept > Queries', task_dept_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_cr_budgets', 'm_cr_budgets.m_activity.m_select', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Budget Management--Departmental Budgets--Processing
   new_task_id := new_task_id + 1;
   DBMS_OUTPUT.put_line('Budget Management--Departmental Budgets--Processing - new_task_id = '||TO_CHAR(new_task_id));
   
   insert into bv_pp_mypp_task (task_id, description, long_description, precedent_task_id, allow_as_shortcut_yn, pop_up_object, help_window_name)
   values (new_task_id, 'Departmental Budget Processing', 'Budgets > Dept > Processing', task_dept_budget, 1, null, null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task');
   
   insert into bv_pp_mypp_task_step (task_id, step_id, window, path, event, interface_id)
   values (new_task_id, 1, 'w_cr_budgets', 'm_cr_budgets.m_activity.m_closing', 'clicked', null);
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_step');
   
   
   --Commit
   commit;
   
   
   
   --Translate the old task ids to the new ones
   DBMS_OUTPUT.put_line('Task Id Translation');
   
   delete from bv_pp_mypp_task_xlat;
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows deleted from bv_pp_mypp_task_xlat');
   
   insert into bv_pp_mypp_task_xlat (old_task_id, new_task_id)
   select distinct step.task_id old_task_id, b_step.task_id new_task_id
   from
      (
      select step.task_id, step.window || '.' || step.path || '.' || step.event dot_path
      from pp_mypp_task_step step
      where step.window is not null
      and step.task_id in (
         select task.task_id
         from pp_mypp_task task
         start with task_id in (select task_id from pp_mypp_task where description = 'Budget Management' and task_id <> task_budget_mgmt)
         connect by prior task_id = precedent_task_id
         )
      ) step
   inner join
      (
      select b_step.task_id, b_step.window || '.' || b_step.path || '.' || b_step.event dot_path
      from bv_pp_mypp_task_step b_step
      where b_step.window is not null
      ) b_step
   on (  step.dot_path = b_step.dot_path
         or
         replace(step.dot_path,'w_budget_main.m_budget_main.','w_budget_cap_main.m_budget_cap_main.') = b_step.dot_path
      );
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into bv_pp_mypp_task_xlat');
   
   --Insert the new task ids
   insert into pp_mypp_task
   select * from bv_pp_mypp_task;
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into pp_mypp_task');
   
   insert into pp_mypp_task_step
   select * from bv_pp_mypp_task_step;
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows inserted into pp_mypp_task_step');
   
   --Loop through the contraints and disable in order to update the ids...then re-enable
   for i in (
      select constraint_name, table_name
      from all_constraints
      where owner = 'PWRPLANT'
      and r_constraint_name = (
         select constraint_name from all_constraints
         where table_name = 'PP_MYPP_TASK'
         and constraint_type = 'P'
         )
      )
   loop
      --Disable the contraints
      execute immediate 'alter table '||i.table_name||' disable constraint '||i.constraint_name||'';
      DBMS_OUTPUT.put_line('  disabled constraint '||i.table_name||'.'||i.constraint_name);
      
      --Update the task ids
      if i.table_name <> 'PP_MYPP_TASK_STEP' and i.table_name <> 'PP_MYPP_TASK' then
         execute immediate
            'update '||i.table_name||' a '||
            'set a.task_id = ( '||
            '  select b.new_task_id '||
            '  from bv_pp_mypp_task_xlat b '||
            '  where b.old_task_id = a.task_id '||
            '  ) '||
            'where a.task_id in ( '||
            '  select b.old_task_id '||
            '  from bv_pp_mypp_task_xlat b '||
            '  ) ';
         DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows updated in '||i.table_name);
      end if;
      
      --Re-enable the constraints
      execute immediate 'alter table '||i.table_name||' enable constraint '||i.constraint_name||'';
      DBMS_OUTPUT.put_line('  enabled constraint '||i.table_name||'.'||i.constraint_name);
      
   end loop;
   
   --Delete any user tasks for items that were not translated
   delete from pp_mypp_user_task
   where task_id in (
      select task.task_id
      from pp_mypp_task task
      start with task_id in (select task_id from pp_mypp_task where description = 'Budget Management' and task_id <> task_budget_mgmt)
      connect by prior task_id = precedent_task_id
      );
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows deleted from pp_mypp_user_task');
   
   --Delete the old Budget task ids
   delete from pp_mypp_task_step step
   where step.task_id in (
      select task.task_id
      from pp_mypp_task task
      start with task_id in (select task_id from pp_mypp_task where description = 'Budget Management' and task_id <> task_budget_mgmt)
      connect by prior task_id = precedent_task_id
      );
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows deleted from pp_mypp_task_step');
   
   --Delete the old Budget task ids
   delete from pp_mypp_task task
   where task.task_id in (
      select task.task_id
      from pp_mypp_task task
      start with task_id in (select task_id from pp_mypp_task where description = 'Budget Management' and task_id <> task_budget_mgmt)
      connect by prior task_id = precedent_task_id
      );
   DBMS_OUTPUT.put_line('  '||TO_CHAR(SQL%ROWCOUNT)||' rows deleted from pp_mypp_task');
   
   --Commit
   commit;
   
end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2923, 0, 2015, 2, 0, 0, 45081, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045081_budget_module_mypp_shortcuts_2_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
