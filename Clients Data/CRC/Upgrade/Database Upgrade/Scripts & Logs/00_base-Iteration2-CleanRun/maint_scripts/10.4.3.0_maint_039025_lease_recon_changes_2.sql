/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039025_lease_recon_changes_2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.1 07/30/2014 Daniel Motter    Misc changes regarding payment recon
||============================================================================
*/

--* Fill in reconcile type table
insert into LS_RECONCILE_TYPE (LS_RECONCILE_TYPE_ID, DESCRIPTION) values (1, 'Asset');
insert into LS_RECONCILE_TYPE (LS_RECONCILE_TYPE_ID, DESCRIPTION) values (2, 'ILR');
insert into LS_RECONCILE_TYPE (LS_RECONCILE_TYPE_ID, DESCRIPTION) values (3, 'MLA');


--* Add column to lease group for reconcile type
alter table LS_LEASE_GROUP add LS_RECONCILE_TYPE_ID number(22,0);

alter table LS_LEASE_GROUP
   add constraint FK_LS_RECONCILE_GROUP
       foreign key (LS_RECONCILE_TYPE_ID)
       references LS_RECONCILE_TYPE (LS_RECONCILE_TYPE_ID);

comment on column LS_LEASE_GROUP.LS_RECONCILE_TYPE_ID is 'The level at which payment recon will occur by default for the leases under this lease group. Available options are MLA, ILR, or Asset.';

--* Add column to lease options for reconcile type
alter table LS_LEASE_OPTIONS add LS_RECONCILE_TYPE_ID number(22,0);

alter table LS_LEASE_OPTIONS
   add constraint FK_LS_RECONCILE_OPTIONS
       foreign key (LS_RECONCILE_TYPE_ID)
       references LS_RECONCILE_TYPE (LS_RECONCILE_TYPE_ID);

comment on column LS_LEASE_OPTIONS.LS_RECONCILE_TYPE_ID is 'The level at which payment recon will occur for this lease. Available options are MLA, ILR, or Asset.';

--* Add columns to invoice headers table for asset_id and ilr_id
alter table LS_INVOICE add (LS_ASSET_ID   number(22,0) default -1,
                            ILR_ID        number(22,0) default -1);

alter table LS_INVOICE modify LEASE_ID default -1;

comment on column LS_INVOICE.LS_ASSET_ID is 'Holds the asset ID this header is for if reconciliation level is asset, -1 otherwise';
comment on column LS_INVOICE.ILR_ID is 'Holds the ILR ID this header is for if reconciliation level is ILR, -1 otherwise';

--* Add columns to payment headers for asset_id and ilr_id
alter table LS_PAYMENT_HDR add (LS_ASSET_ID   number(22,0) default -1,
                                ILR_ID        number(22,0) default -1);

alter table LS_PAYMENT_HDR modify LEASE_ID default -1;

comment on column LS_PAYMENT_HDR.LS_ASSET_ID is 'Holds the asset ID this header is for if reconciliation level is asset, -1 otherwise';
comment on column LS_PAYMENT_HDR.ILR_ID is 'Holds the ILR ID this header is for if reconciliation level is ILR, -1 otherwise';

--* Backfill ls_lease_group. Default reconcile type to MLA where it's not already set
update LS_LEASE_GROUP set LS_RECONCILE_TYPE_ID = 3 where LS_RECONCILE_TYPE_ID is null;

--* Backfill ls_lease_options. Inherit reconcile type from ls_lease_group
update LS_LEASE_OPTIONS O
   set O.LS_RECONCILE_TYPE_ID =
        (select G.LS_RECONCILE_TYPE_ID
           from LS_LEASE L, LS_LEASE_GROUP G
          where G.LEASE_GROUP_ID = L.LEASE_GROUP_ID
            and L.LEASE_ID = O.LEASE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1303, 0, 10, 4, 3, 0, 39025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039025_lease_recon_changes_2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
