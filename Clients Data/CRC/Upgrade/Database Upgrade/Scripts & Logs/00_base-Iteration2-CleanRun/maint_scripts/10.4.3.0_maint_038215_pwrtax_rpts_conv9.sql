/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038215_pwrtax_rpts_conv9.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 08/28/2014 Anand Rajashekar    script changes to for report conversion
||============================================================================
*/

--Report 120, 257, 259. Add filters to normalization schema

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (198, 'Jurisdiction', 'dw', 'normalization_schema.jurisdiction_id',
    'deferred_income_tax.normalization_id in ( select distinct normalization_id from normalization_schema where [selected_values])',
    'dw_jurisdiction_filter', 'jurisdiction_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT',
    TO_DATE('26-AUG-14', 'DD-MON-RR'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (199, 'Amortization Type', 'dw', 'normalization_schema.amortization_type_id',
    'deferred_income_tax.normalization_id in ( select distinct normalization_id from normalization_schema where [selected_values])',
    'dw_amortization_type_filter', 'amortization_type_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT',
    TO_DATE('26-AUG-14', 'DD-MON-RR'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (200, 'Deferred Rates', 'dw', 'normalization_schema.def_income_tax_rate_id',
    'deferred_income_tax.normalization_id in ( select distinct normalization_id from normalization_schema where [selected_values])',
    'dw_deferred_rates_filter', 'def_income_tax_rate_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT',
    TO_DATE('26-AUG-14', 'DD-MON-RR'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (59, 106, 198, 'jurisdiction_id', null, 'PWRPLANT', TO_DATE('22-AUG-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (60, 106, 199, 'amortization_type_id', null, 'PWRPLANT', TO_DATE('22-AUG-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (61, 106, 200, 'def_income_tax_rate_id', null, 'PWRPLANT', TO_DATE('22-AUG-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (81, 198, 'PWRPLANT', TO_DATE('28-JUL-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (81, 199, 'PWRPLANT', TO_DATE('25-JUL-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (81, 200, 'PWRPLANT', TO_DATE('25-JUL-14', 'DD-MON-RR'));


--Report 216 changes Add filters to normalization schema

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (195, 'Jurisdiction', 'dw', 'normalization_schema.jurisdiction_id',
    'deferred_income_tax.normalization_id in ( select distinct normalization_id from normalization_schema where [selected_values])',
    'dw_jurisdiction_filter', 'jurisdiction_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT',
    TO_DATE('26-AUG-14', 'DD-MON-RR'), 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (196, 'Amortization Type', 'dw', 'normalization_schema.amortization_type_id',
    'deferred_income_tax.normalization_id in ( select distinct normalization_id from normalization_schema where [selected_values])',
    'dw_amortization_type_filter', 'amortization_type_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT',
    TO_DATE('26-AUG-14', 'DD-MON-RR'), 1, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (197, 'Deferred Rates', 'dw', 'normalization_schema.def_income_tax_rate_id',
    'deferred_income_tax.normalization_id in ( select distinct normalization_id from normalization_schema where [selected_values])',
    'dw_deferred_rates_filter', 'def_income_tax_rate_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT',
    TO_DATE('26-AUG-14', 'DD-MON-RR'), 1, 0, 0);

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (62, 189, 195, 'jurisdiction_id', null, 'PWRPLANT', TO_DATE('22-AUG-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (63, 189, 196, 'amortization_type_id', null, 'PWRPLANT', TO_DATE('22-AUG-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (64, 189, 197, 'def_income_tax_rate_id', null, 'PWRPLANT', TO_DATE('22-AUG-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (80, 195, 'PWRPLANT', TO_DATE('28-JUL-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (80, 196, 'PWRPLANT', TO_DATE('25-JUL-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (80, 197, 'PWRPLANT', TO_DATE('25-JUL-14', 'DD-MON-RR'));

--Report 248 and 249 Add additional filters to normalization schema

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (82, 195, 'PWRPLANT', TO_DATE('28-JUL-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (82, 196, 'PWRPLANT', TO_DATE('25-JUL-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (82, 197, 'PWRPLANT', TO_DATE('25-JUL-14', 'DD-MON-RR'));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1370, 0, 10, 4, 3, 0, 38215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038215_pwrtax_rpts_conv9.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
