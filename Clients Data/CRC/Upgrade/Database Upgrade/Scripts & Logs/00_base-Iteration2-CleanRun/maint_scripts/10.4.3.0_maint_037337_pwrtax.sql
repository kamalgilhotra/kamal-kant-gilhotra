/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037337_pwrtax.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.3.0 04/15/2014 Julia Breuer
||=================================================================================
*/

--
-- Create PowerTax imports.  First insert into the base tables.
--
insert into pwrplant.pp_import_subsystem ( import_subsystem_id, time_stamp, user_id, description, long_description ) values ( 9, sysdate, user, 'PowerTax', 'PowerTax' );

insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 201, sysdate, user, 'Depreciation Adjustments', 'Import Depreciation and Reserve Adjustments', 'tax_import_depr_adjust', 'tax_import_depr_adjust_arc', null, 0, 'nvo_tax_logic_import', 'version_id', 'Depr Adjustments', '' );
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 202, sysdate, user, 'Book Activity', 'Import Book Activity', 'tax_import_basis_amounts', 'tax_import_basis_amounts_arc', null, 1, 'nvo_tax_logic_import', 'version_id', 'Book Activity', '' );
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 203, sysdate, user, 'Book-to-Tax Basis Differences', 'Import Book-to-Tax Basis Differences', 'tax_import_book_reconcile', 'tax_import_book_reconcile_arc', null, 1, 'nvo_tax_logic_import', 'version_id', 'Book-to-Tax Diff', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 201, 9, sysdate, user );
insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 202, 9, sysdate, user );
insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 203, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'tax_record_id', sysdate, user, 'Tax Record', 'tax_record_xlate', 1, 2, 'number(22,0)', 'tax_record_control', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'tax_year', sysdate, user, 'Tax Year', '', 1, 1, 'number(22,0)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'tax_book_id', sysdate, user, 'Tax Book', 'tax_book_xlate', 1, 1, 'number(22,0)', 'tax_book', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'tax_class_id', sysdate, user, 'Tax Class', 'tax_class_xlate', 0, 1, 'number(22,0)', 'tax_class', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'vintage_id', sysdate, user, 'Vintage', 'vintage_xlate', 0, 1, 'number(22,0)', 'vintage', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'in_service_month', sysdate, user, 'In Service Month', '', 0, 1, 'date', '', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'book_balance_adjust', sysdate, user, 'Book Balance Adjustment', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'accum_reserve_adjust', sysdate, user, 'Accum Reserve Adjustment', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'depreciable_base_adjust', sysdate, user, 'Depreciable Base Adjustment', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'depreciation_adjust', sysdate, user, 'Depreciation Adjustment', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'gain_loss_adjust', sysdate, user, 'Gain Loss Adjustment', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 201, 'cap_gain_loss_adjust', sysdate, user, 'Cap Gain Loss Adjustment', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'tax_record_id', sysdate, user, 'Tax Record', 'tax_record_xlate', 1, 2, 'number(22,0)', 'tax_record_control', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'tax_year', sysdate, user, 'Tax Year', '', 1, 1, 'number(22,0)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'tax_activity_code_id', sysdate, user, 'Tax Activity Code', 'tax_activity_code_xlate', 1, 1, 'number(22,0)', 'tax_activity_code', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'tax_include_id', sysdate, user, 'Tax Include', 'tax_include_xlate', 1, 1, 'number(22,0)', 'tax_include', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'tax_class_id', sysdate, user, 'Tax Class', 'tax_class_xlate', 0, 1, 'number(22,0)', 'tax_class', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'vintage_id', sysdate, user, 'Vintage', 'vintage_xlate', 0, 1, 'number(22,0)', 'vintage', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'in_service_month', sysdate, user, 'In Service Month', '', 0, 1, 'date', '', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'amount', sysdate, user, 'Amount', '', 1, 1, 'number(22,2)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 202, 'tax_summary_id', sysdate, user, 'Tax Summary', 'tax_summary_xlate', 0, 1, 'number(22,0)', 'tax_summary_control', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'tax_include_id', sysdate, user, 'Tax Include', 'tax_include_xlate', 1, 1, 'number(22,0)', 'tax_include', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'tax_year', sysdate, user, 'Tax Year', '', 1, 1, 'number(22,0)', '', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'tax_record_id', sysdate, user, 'Tax Record', 'tax_record_xlate', 1, 2, 'number(22,0)', 'tax_record_control', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'reconcile_item_id', sysdate, user, 'Tax Reconcile Item', 'reconcile_item_xlate', 1, 1, 'number(22,0)', 'tax_reconcile_item', '', 1, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'tax_class_id', sysdate, user, 'Tax Class', 'tax_class_xlate', 0, 1, 'number(22,0)', 'tax_class', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'vintage_id', sysdate, user, 'Vintage', 'vintage_xlate', 0, 1, 'number(22,0)', 'vintage', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'in_service_month', sysdate, user, 'In Service Month', '', 0, 1, 'date', '', '', 0, null );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id ) values ( 203, 'basis_amount_activity', sysdate, user, 'Activity Amount', '', 1, 1, 'number(22,2)', '', '', 1, null );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2001, sysdate, user, 'Tax Book.Description', 'The passed in value corresponds to the Tax Book: Description field.  Translate to the Tax Book ID using the Description column on the Tax Book table.', 'tax_book_id', '( select tb.tax_book_id from tax_book tb where upper( trim( <importfield> ) ) = upper( trim( tb.description ) ) )', 0, 'tax_book', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2002, sysdate, user, 'Tax Activity Code.Description', 'The passed in value corresponds to the Tax Activity: Description field.  Translate to the Tax Activity Code ID using the Description column on the Tax Activity Code table.', 'tax_activity_code_id', '( select tac.tax_activity_code_id from tax_activity_code tac where upper( trim( <importfield> ) ) = upper( trim( tac.description ) ) )', 0, 'tax_activity_code', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2003, sysdate, user, 'Tax Include.Description', 'The passed in value corresponds to the Tax Include: Description field.  Translate to the Tax Include ID using the Description column on the Tax Include table.', 'tax_include_id', '( select ti.tax_include_id from tax_include ti where upper( trim( <importfield> ) ) = upper( trim( ti.description ) ) )', 0, 'tax_include', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2004, sysdate, user, 'Tax Summary Control.Description', 'The passed in value corresponds to the Tax Summary Control: Description field.  Translate to the Tax Summary ID using the Description column on the Tax Summary Control table.', 'tax_summary_id', '( select tsc.tax_summary_id from tax_summary_control tsc where upper( trim( <importfield> ) ) = upper( trim( tsc.description ) ) )', 0, 'tax_summary_control', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2005, sysdate, user, 'Tax Reconcile Item.Description', 'The passed in value corresponds to the Tax Reconcile Item: Description field.  Translate to the Reconcile Item ID using the Description column on the Tax Reconcile Item table.', 'reconcile_item_id', '( select tri.reconcile_item_id from tax_reconcile_item tri where upper( trim( <importfield> ) ) = upper( trim( tri.description ) ) )', 0, 'tax_reconcile_item', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2006, sysdate, user, 'Derive from Case, Company, Tax Class, Vintage, and Month', 'The Tax Record ID will be derived from the given Case, Company, Tax Class, Vintage, and Month.', 'tax_record_id', '( select trc.tax_record_id from tax_record_control trc where <importtable>.company_id = trc.company_id and <importtable>.tax_class_id = trc.tax_class_id and <importtable>.vintage_id = trc.vintage_id and <importtable>.version_id = trc.version_id and nvl( to_char( to_date( <importtable>.in_service_month, ''mm/dd/yyyy'' ), ''MM'' ), ''00'' ) = nvl( to_char( trc.in_service_month, ''MM'' ), ''00'' ) )', 1, '', '', 'company_id, tax_class_id, vintage_id, in_service_month', '', 0 );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2007, sysdate, user, 'Tax Class.Description', 'The passed in value corresponds to the Tax Class: Description field.  Translate to the Tax Class ID using the Description column on the Tax Class table.', 'tax_class_id', '( select tc.tax_class_id from tax_class tc where upper( trim( <importfield> ) ) = upper( trim( tc.description ) ) )', 0, 'tax_class', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2008, sysdate, user, 'Vintage.Description', 'The passed in value corresponds to the Vintage: Description field.  Translate to the Vintage ID using the Description column on the Vintage table.', 'vintage_id', '( select v.vintage_id from vintage v where upper( trim( <importfield> ) ) = upper( trim( v.description ) ) )', 0, 'vintage', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2009, sysdate, user, 'Vintage.Year', 'The passed in value corresponds to the Vintage: Year field.  Translate to the Vintage ID using the Year column on the Vintage table.', 'vintage_id', '( select v.vintage_id from vintage v where upper( trim( <importfield> ) ) = to_char( v.year ) )', 0, 'vintage', 'year', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 201, 'tax_book_id', 2001, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 201, 'tax_record_id', 2006, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 201, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 201, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 201, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 201, 'tax_class_id', 2007, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 201, 'vintage_id', 2008, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 201, 'vintage_id', 2009, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'tax_activity_code_id', 2002, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'tax_include_id', 2003, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'tax_summary_id', 2004, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'tax_record_id', 2006, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'tax_class_id', 2007, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'vintage_id', 2008, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 202, 'vintage_id', 2009, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'tax_include_id', 2003, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'reconcile_item_id', 2005, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'tax_record_id', 2006, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'tax_class_id', 2007, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'vintage_id', 2008, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 203, 'vintage_id', 2009, sysdate, user );

--
-- Now create the staging and archive tables.
--
create table PWRPLANT.TAX_IMPORT_DEPR_ADJUST
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_RECORD_XLATE        varchar2(254),
 TAX_RECORD_ID           number(22,0),
 TAX_BOOK_XLATE          varchar2(254),
 TAX_BOOK_ID             number(22,0),
 TAX_YEAR                varchar2(35),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 TAX_CLASS_XLATE         varchar2(254),
 TAX_CLASS_ID            number(22,0),
 VINTAGE_XLATE           varchar2(254),
 VINTAGE_ID              number(22,0),
 IN_SERVICE_MONTH        varchar2(35),
 BOOK_BALANCE_ADJUST     varchar2(35),
 ACCUM_RESERVE_ADJUST    varchar2(35),
 DEPRECIABLE_BASE_ADJUST varchar2(35),
 DEPRECIATION_ADJUST     varchar2(35),
 GAIN_LOSS_ADJUST        varchar2(35),
 CAP_GAIN_LOSS_ADJUST    varchar2(35),
 VERSION_ID              number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_DEPR_ADJUST add constraint TAX_IMP_DEPR_ADJ_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_DEPR_ADJUST add constraint TAX_IMP_DEPR_ADJ_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_DEPR_ADJUST_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_RECORD_XLATE        varchar2(254),
 TAX_RECORD_ID           number(22,0),
 TAX_BOOK_XLATE          varchar2(254),
 TAX_BOOK_ID             number(22,0),
 TAX_YEAR                varchar2(35),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 TAX_CLASS_XLATE         varchar2(254),
 TAX_CLASS_ID            number(22,0),
 VINTAGE_XLATE           varchar2(254),
 VINTAGE_ID              number(22,0),
 IN_SERVICE_MONTH        varchar2(35),
 BOOK_BALANCE_ADJUST     varchar2(35),
 ACCUM_RESERVE_ADJUST    varchar2(35),
 DEPRECIABLE_BASE_ADJUST varchar2(35),
 DEPRECIATION_ADJUST     varchar2(35),
 GAIN_LOSS_ADJUST        varchar2(35),
 CAP_GAIN_LOSS_ADJUST    varchar2(35),
 VERSION_ID              number(22,0)
);

alter table PWRPLANT.TAX_IMPORT_DEPR_ADJUST_ARC add constraint TAX_IMP_DEPR_ADJ_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_DEPR_ADJUST_ARC add constraint TAX_IMP_DEPR_ADJ_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_BASIS_AMOUNTS
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_RECORD_XLATE        varchar2(254),
 TAX_RECORD_ID           number(22,0),
 TAX_ACTIVITY_CODE_XLATE varchar2(254),
 TAX_ACTIVITY_CODE_ID    number(22,0),
 TAX_INCLUDE_XLATE       varchar2(254),
 TAX_INCLUDE_ID          number(22,0),
 TAX_YEAR                varchar2(35),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 TAX_CLASS_XLATE         varchar2(254),
 TAX_CLASS_ID            number(22,0),
 VINTAGE_XLATE           varchar2(254),
 VINTAGE_ID              number(22,0),
 IN_SERVICE_MONTH        varchar2(35),
 TAX_SUMMARY_XLATE       varchar2(254),
 TAX_SUMMARY_ID          number(22,0),
 AMOUNT                  varchar2(35),
 VERSION_ID              number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_BASIS_AMOUNTS add constraint TAX_IMP_BASIS_AMT_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_BASIS_AMOUNTS add constraint TAX_IMP_BASIS_AMT_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_BASIS_AMOUNTS_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_RECORD_XLATE        varchar2(254),
 TAX_RECORD_ID           number(22,0),
 TAX_ACTIVITY_CODE_XLATE varchar2(254),
 TAX_ACTIVITY_CODE_ID    number(22,0),
 TAX_INCLUDE_XLATE       varchar2(254),
 TAX_INCLUDE_ID          number(22,0),
 TAX_YEAR                varchar2(35),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 TAX_CLASS_XLATE         varchar2(254),
 TAX_CLASS_ID            number(22,0),
 VINTAGE_XLATE           varchar2(254),
 VINTAGE_ID              number(22,0),
 IN_SERVICE_MONTH        varchar2(35),
 TAX_SUMMARY_XLATE       varchar2(254),
 TAX_SUMMARY_ID          number(22,0),
 AMOUNT                  varchar2(35),
 VERSION_ID              number(22,0)
);

alter table PWRPLANT.TAX_IMPORT_BASIS_AMOUNTS_ARC add constraint TAX_IMP_BASIS_AMT_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_BASIS_AMOUNTS_ARC add constraint TAX_IMP_BASIS_AMT_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_BOOK_RECONCILE
(
 IMPORT_RUN_ID         number(22,0) not null,
 LINE_ID               number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 TAX_RECORD_XLATE      varchar2(254),
 TAX_RECORD_ID         number(22,0),
 TAX_INCLUDE_XLATE     varchar2(254),
 TAX_INCLUDE_ID        number(22,0),
 RECONCILE_ITEM_XLATE  varchar2(254),
 RECONCILE_ITEM_ID     number(22,0),
 TAX_YEAR              varchar2(35),
 COMPANY_XLATE         varchar2(254),
 COMPANY_ID            number(22,0),
 TAX_CLASS_XLATE       varchar2(254),
 TAX_CLASS_ID          number(22,0),
 VINTAGE_XLATE         varchar2(254),
 VINTAGE_ID            number(22,0),
 IN_SERVICE_MONTH      varchar2(35),
 BASIS_AMOUNT_ACTIVITY varchar2(35),
 VERSION_ID            number(22,0),
 IS_MODIFIED           number(22,0),
 ERROR_MESSAGE         varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_BOOK_RECONCILE add constraint TAX_IMP_BK_RECON_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_BOOK_RECONCILE add constraint TAX_IMP_BK_RECON_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_BOOK_RECONCILE_ARC
(
 IMPORT_RUN_ID         number(22,0) not null,
 LINE_ID               number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 TAX_RECORD_XLATE      varchar2(254),
 TAX_RECORD_ID         number(22,0),
 TAX_INCLUDE_XLATE     varchar2(254),
 TAX_INCLUDE_ID        number(22,0),
 RECONCILE_ITEM_XLATE  varchar2(254),
 RECONCILE_ITEM_ID     number(22,0),
 TAX_YEAR              varchar2(35),
 COMPANY_XLATE         varchar2(254),
 COMPANY_ID            number(22,0),
 TAX_CLASS_XLATE       varchar2(254),
 TAX_CLASS_ID          number(22,0),
 VINTAGE_XLATE         varchar2(254),
 VINTAGE_ID            number(22,0),
 IN_SERVICE_MONTH      varchar2(35),
 BASIS_AMOUNT_ACTIVITY varchar2(35),
 VERSION_ID            number(22,0)
);

alter table PWRPLANT.TAX_IMPORT_BOOK_RECONCILE_ARC add constraint TAX_IMP_BK_RECON_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_BOOK_RECONCILE_ARC add constraint TAX_IMP_BK_RECON_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Tax Class.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 204, sysdate, user, 'Tax Class', 'Import Tax Classes', 'tax_import_tax_class', 'tax_import_tax_class_arc', null, 1, 'nvo_tax_logic_import', 'tax_class_id', 'Tax Class', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 204, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 204, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 204, 'oper_ind', sysdate, user, 'Oper Ind', 'oper_ind_xlate', 0, 1, 'number(22,0)', 'tax_accrual_oper_ind', '', 1, null, '' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2010, sysdate, user, 'Tax Accrual Oper Ind.Description', 'The passed in value corresponds to the Tax Accrual Oper Ind: Description field.  Translate to the Oper Ind using the Description column on the Tax Accrual Oper Ind table.', 'oper_ind', '( select tao.oper_ind from tax_accrual_oper_ind tao where upper( trim( <importfield> ) ) = upper( trim( tao.description ) ) )', 0, 'tax_accrual_oper_ind', 'description', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 204, 'oper_ind', 2010, sysdate, user );

insert into pwrplant.pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 204, 2007, sysdate, user );

create table PWRPLANT.TAX_IMPORT_TAX_CLASS
(
 IMPORT_RUN_ID  number(22,0) not null,
 LINE_ID        number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(254),
 OPER_IND_XLATE varchar2(254),
 OPER_IND       number(22,0),
 TAX_CLASS_ID   number(22,0),
 IS_MODIFIED    number(22,0),
 ERROR_MESSAGE  varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_TAX_CLASS add constraint TAX_IMP_CLASS_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_TAX_CLASS add constraint TAX_IMP_CLASS_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_TAX_CLASS_ARC
(
 IMPORT_RUN_ID  number(22,0) not null,
 LINE_ID        number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(254),
 OPER_IND_XLATE varchar2(254),
 OPER_IND       number(22,0),
 TAX_CLASS_ID   number(22,0)
);

alter table pwrplant.tax_import_tax_class_arc add constraint tax_imp_class_arc_pk primary key ( import_run_id, line_id ) using index tablespace pwrplant_idx;
alter table pwrplant.tax_import_tax_class_arc add constraint tax_imp_class_arc_run_fk foreign key ( import_run_id ) references pwrplant.pp_import_run;

--
-- Create an import for Tax Reconcile Item.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 205, sysdate, user, 'Tax Reconcile Item', 'Import Tax Reconcile Items', 'tax_import_rec_item', 'tax_import_rec_item_arc', null, 1, 'nvo_tax_logic_import', 'reconcile_item_id', 'Tax Reconcile Item', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 205, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 205, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 205, 'type', sysdate, user, 'Type', '', 0, 1, 'char(18)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 205, 'input_retire_ind', sysdate, user, 'Input Retire Indicator', 'input_retire_ind_xlate', 0, 1, 'number(1,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 205, 'default_tax_include_id', sysdate, user, 'Default Tax Include', 'default_tax_include_xlate', 0, 1, 'number(22,0)', 'tax_include', '', 1, null, 'tax_include_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 205, 'tax_type_of_property_id', sysdate, user, 'Tax Type of Property', 'tax_type_of_property_xlate', 0, 1, 'number(22,0)', 'tax_type_of_property', '', 1, null, 'type_of_property_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 205, 'net_basis_rpt', sysdate, user, 'Net Basis Report', 'net_basis_rpt_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 205, 'calced', sysdate, user, 'Calculated by Code', 'calced_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 205, 'depr_deduction', sysdate, user, 'Depreciation Deduction', 'depr_deduction_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2014, sysdate, user, 'Tax Type of Property.Description', 'The passed in value corresponds to the Tax Type of Property: Description field.  Translate to the Type of Property ID using the Description column on the Tax Type of Property table.', 'type_of_property_id', '( select tp.type_of_property_id from tax_type_of_property tp where upper( trim( <importfield> ) ) = upper( trim( tp.description ) ) )', 0, 'tax_type_of_property', 'description', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'input_retire_ind', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'input_retire_ind', 78, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'default_tax_include_id', 2003, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'tax_type_of_property_id', 2014, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'net_basis_rpt', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'net_basis_rpt', 78, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'calced', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'calced', 78, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'depr_deduction', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 205, 'depr_deduction', 78, sysdate, user );

insert into pwrplant.pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 205, 2005, sysdate, user );

create table PWRPLANT.TAX_IMPORT_REC_ITEM
(
 IMPORT_RUN_ID              number(22,0) not null,
 LINE_ID                    number(22,0) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 DESCRIPTION                varchar2(254),
 TYPE                       varchar2(254),
 INPUT_RETIRE_IND_XLATE     varchar2(254),
 INPUT_RETIRE_IND           number(22,0),
 DEFAULT_TAX_INCLUDE_XLATE  varchar2(254),
 DEFAULT_TAX_INCLUDE_ID     number(22,0),
 TAX_TYPE_OF_PROPERTY_XLATE varchar2(254),
 TAX_TYPE_OF_PROPERTY_ID    number(22,0),
 NET_BASIS_RPT_XLATE        varchar2(254),
 NET_BASIS_RPT              number(22,0),
 CALCED_XLATE               varchar2(254),
 CALCED                     number(22,0),
 DEPR_DEDUCTION_XLATE       varchar2(254),
 DEPR_DEDUCTION             number(22,0),
 RECONCILE_ITEM_ID          number(22,0),
 IS_MODIFIED                number(22,0),
 ERROR_MESSAGE              varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_REC_ITEM add constraint TAX_IMP_REC_ITEM_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_REC_ITEM add constraint TAX_IMP_REC_ITEM_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_REC_ITEM_ARC
(
 IMPORT_RUN_ID              number(22,0) not null,
 LINE_ID                    number(22,0) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 DESCRIPTION                varchar2(254),
 TYPE                       varchar2(254),
 INPUT_RETIRE_IND_XLATE     varchar2(254),
 INPUT_RETIRE_IND           number(22,0),
 DEFAULT_TAX_INCLUDE_XLATE  varchar2(254),
 DEFAULT_TAX_INCLUDE_ID     number(22,0),
 TAX_TYPE_OF_PROPERTY_XLATE varchar2(254),
 TAX_TYPE_OF_PROPERTY_ID    number(22,0),
 NET_BASIS_RPT_XLATE        varchar2(254),
 NET_BASIS_RPT              number(22,0),
 CALCED_XLATE               varchar2(254),
 CALCED                     number(22,0),
 DEPR_DEDUCTION_XLATE       varchar2(254),
 DEPR_DEDUCTION             number(22,0),
 RECONCILE_ITEM_ID          number(22,0)
);

alter table PWRPLANT.TAX_IMPORT_REC_ITEM_ARC add constraint TAX_IMP_REC_ITEM_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_REC_ITEM_ARC add constraint TAX_IMP_REC_ITEM_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Vintage.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 206, sysdate, user, 'Vintage', 'Import Vintages', 'tax_import_vintage', 'tax_import_vintage_arc', null, 1, 'nvo_tax_logic_import', 'vintage_id', 'Vintage', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 206, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 206, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 206, 'year', sysdate, user, 'Year', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 206, 'start_month', sysdate, user, 'Start Month', '', 1, 1, 'number(2,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 206, 'end_month', sysdate, user, 'End Month', '', 1, 1, 'number(2,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 206, 'vintage_convention_id', sysdate, user, 'Vintage Convention', 'vintage_convention_xlate', 0, 1, 'number(22,0)', 'tax_vintage_convention', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 206, 'ext_class_code_value', sysdate, user, 'Ext Class Code Value', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 206, 'bonus_start_month', sysdate, user, 'Bonus Start Month', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 206, 'bonus_end_month', sysdate, user, 'Bonus End Month', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2015, sysdate, user, 'Tax Vintage Convention.Description', 'The passed in value corresponds to the Tax Vintage Convention: Description field.  Translate to the Vintage Convention ID using the Description column on the Tax Vintage Convention table.', 'vintage_convention_id', '( select tvc.vintage_convention_id from tax_vintage_convention tvc where upper( trim( <importfield> ) ) = upper( trim( tvc.description ) ) )', 0, 'tax_vintage_convention', 'description', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 206, 'vintage_convention_id', 2015, sysdate, user );

insert into pwrplant.pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 206, 2008, sysdate, user );
insert into pwrplant.pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 206, 2009, sysdate, user );

create table PWRPLANT.TAX_IMPORT_VINTAGE
(
 IMPORT_RUN_ID            number(22,0) not null,
 LINE_ID                  number(22,0) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 DESCRIPTION              varchar2(254),
 YEAR                     varchar2(35),
 START_MONTH              varchar2(35),
 END_MONTH                varchar2(35),
 VINTAGE_CONVENTION_XLATE varchar2(254),
 VINTAGE_CONVENTION_ID    number(22,0),
 EXT_CLASS_CODE_VALUE     varchar2(254),
 BONUS_START_MONTH        varchar2(35),
 BONUS_END_MONTH          varchar2(35),
 VINTAGE_ID               number(22,0),
 IS_MODIFIED              number(22,0),
 ERROR_MESSAGE            varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_VINTAGE add constraint TAX_IMP_VINT_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_VINTAGE add constraint TAX_IMP_VINT_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_VINTAGE_ARC
(
 IMPORT_RUN_ID            number(22,0) not null,
 LINE_ID                  number(22,0) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 DESCRIPTION              varchar2(254),
 YEAR                     varchar2(35),
 START_MONTH              varchar2(35),
 END_MONTH                varchar2(35),
 VINTAGE_CONVENTION_XLATE varchar2(254),
 VINTAGE_CONVENTION_ID    number(22,0),
 EXT_CLASS_CODE_VALUE     varchar2(254),
 BONUS_START_MONTH        varchar2(35),
 BONUS_END_MONTH          varchar2(35),
 VINTAGE_ID               number(22,0)
);

alter table PWRPLANT.TAX_IMPORT_VINTAGE_ARC add constraint TAX_IMP_VINT_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_VINTAGE_ARC add constraint TAX_IMP_VINT_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Tax Book Translate.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 207, sysdate, user, 'Tax Book Translate', 'Import Tax Book Translate Records', 'tax_import_bk_translate', 'tax_import_bk_translate_arc', null, 1, 'nvo_tax_logic_import', '', 'Tax Book Translate', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 207, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'tax_book_translate_id', sysdate, user, 'Tax Book Translate ID', 'tax_book_translate_xlate', 0, 1, 'number(22,0)', 'tax_book_translate', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'utility_account_id', sysdate, user, 'Utility Account', 'utility_account_xlate', 1, 3, 'number(22,0)', 'utility_account', 'bus_segment_id', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'bus_segment_id', sysdate, user, 'Business Segment', 'bus_segment_xlate', 1, 2, 'number(22,0)', 'business_segment', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'sub_account_id', sysdate, user, 'Sub Account', 'sub_account_xlate', 1, 4, 'number(22,0)', 'sub_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'tax_location_id', sysdate, user, 'Tax Location', 'tax_location_xlate', 0, 1, 'number(22,0)', 'tax_location', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'class_code_id', sysdate, user, 'Class Code', 'class_code_xlate', 0, 1, 'number(22,0)', 'class_code', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'start_eff_book_vintage', sysdate, user, 'Start Vintage', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'end_eff_book_vintage', sysdate, user, 'End Vintage', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'tax_class_id', sysdate, user, 'Tax Class', 'tax_class_xlate', 1, 1, 'number(22,0)', 'tax_class', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'active', sysdate, user, 'Active', '', 1, 1, 'number(24,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'class_code_value', sysdate, user, 'Class Code Value', '', 0, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'monthly_adds', sysdate, user, 'Monthly Adds', 'monthly_adds_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'gl_account_id', sysdate, user, 'GL Account', 'gl_account_xlate', 0, 2, 'number(22,0)', 'gl_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'quarterly_adds', sysdate, user, 'Quarterly Adds', 'quarterly_adds_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'tax_depr_schema_id', sysdate, user, 'Tax Depr Schema', 'tax_depr_schema_xlate', 0, 1, 'number(22,0)', 'tax_depr_schema', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'tax_distinction_id', sysdate, user, 'Tax Distinction', 'tax_distinction_xlate', 0, 1, 'number(22,0)', 'retire_unit_tax_distinction', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'mortality_curve_id', sysdate, user, 'Mortality Curve', 'mortality_curve_xlate', 0, 1, 'number(22,0)', 'mortality_curve', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 207, 'life', sysdate, user, 'Life', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2011, sysdate, user, 'Tax Depr Schema.Description', 'The passed in value corresponds to the Tax Depr Schema: Description field.  Translate to the Tax Depr Schema ID using the Description column on the Tax Depr Schema table.', 'tax_depr_schema_id', '( select tds.tax_depr_schema_id from tax_depr_schema tds where upper( trim( <importfield> ) ) = upper( trim( tds.description ) ) )', 0, 'tax_depr_schema', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2012, sysdate, user, 'Retire Unit Tax Distinction.Description', 'The passed in value corresponds to the Retire Unit Tax Distinction: Description field.  Translate to the Tax Distinction ID using the Description column on the Retire Unit Tax Distinction table.', 'tax_distinction_id', '( select rutd.tax_distinction_id from retire_unit_tax_distinction rutd where upper( trim( <importfield> ) ) = upper( trim( rutd.description ) ) )', 0, 'retire_unit_tax_distinction', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2013, sysdate, user, 'Retire Unit Tax Distinction.Long Description', 'The passed in value corresponds to the Retire Unit Tax Distinction: Long Description field.  Translate to the Tax Distinction ID using the Long Description column on the Retire Unit Tax Distinction table.', 'tax_distinction_id', '( select rutd.tax_distinction_id from retire_unit_tax_distinction rutd where upper( trim( <importfield> ) ) = upper( trim( rutd.long_description ) ) )', 0, 'retire_unit_tax_distinction', 'long_description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2016, sysdate, user, 'Mortality Curve.Description', 'The passed in value corresponds to the Mortality Curve: Description field.  Translate to the Mortality Curve ID using the Description column on the Mortality Curve table.', 'mortality_curve_id', '( select mc.mortality_curve_id from mortality_curve mc where upper( trim( <importfield> ) ) = upper( trim( mc.description ) ) )', 0, 'mortality_curve', 'description', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'utility_account_id', 27, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'utility_account_id', 28, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'utility_account_id', 29, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'utility_account_id', 30, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'bus_segment_id', 25, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'bus_segment_id', 26, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'bus_segment_id', 604, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'bus_segment_id', 605, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'sub_account_id', 612, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'sub_account_id', 613, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'sub_account_id', 614, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'tax_location_id', 145, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'class_code_id', 199, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'class_code_id', 200, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'tax_class_id', 2007, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'gl_account_id', 22, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'gl_account_id', 23, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'gl_account_id', 24, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'gl_account_id', 601, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'gl_account_id', 602, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'gl_account_id', 603, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'tax_depr_schema_id', 2011, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'tax_distinction_id', 2012, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'tax_distinction_id', 2013, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'mortality_curve_id', 2016, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'monthly_adds', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'monthly_adds', 78, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'quarterly_adds', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 207, 'quarterly_adds', 78, sysdate, user );

create table PWRPLANT.TAX_IMPORT_BK_TRANSLATE
(
 IMPORT_RUN_ID            number(22,0) not null,
 LINE_ID                  number(22,0) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 TAX_BOOK_TRANSLATE_XLATE varchar2(254),
 TAX_BOOK_TRANSLATE_ID    number(22,0),
 UTILITY_ACCOUNT_XLATE    varchar2(254),
 UTILITY_ACCOUNT_ID       number(22,0),
 BUS_SEGMENT_XLATE        varchar2(254),
 BUS_SEGMENT_ID           number(22,0),
 SUB_ACCOUNT_XLATE        varchar2(254),
 SUB_ACCOUNT_ID           number(22,0),
 TAX_LOCATION_XLATE       varchar2(254),
 TAX_LOCATION_ID          number(22,0),
 CLASS_CODE_XLATE         varchar2(254),
 CLASS_CODE_ID            number(22,0),
 START_EFF_BOOK_VINTAGE   varchar2(35),
 END_EFF_BOOK_VINTAGE     varchar2(35),
 TAX_CLASS_XLATE          varchar2(254),
 TAX_CLASS_ID             number(22,0),
 ACTIVE                   varchar2(35),
 COMPANY_XLATE            varchar2(254),
 COMPANY_ID               number(22,0),
 CLASS_CODE_VALUE         varchar2(254),
 MONTHLY_ADDS_XLATE       varchar2(254),
 MONTHLY_ADDS             number(22,0),
 GL_ACCOUNT_XLATE         varchar2(254),
 GL_ACCOUNT_ID            number(22,0),
 QUARTERLY_ADDS_XLATE     varchar2(254),
 QUARTERLY_ADDS           number(22,0),
 TAX_DEPR_SCHEMA_XLATE    varchar2(254),
 TAX_DEPR_SCHEMA_ID       number(22,0),
 TAX_DISTINCTION_XLATE    varchar2(254),
 TAX_DISTINCTION_ID       number(22,0),
 MORTALITY_CURVE_XLATE    varchar2(254),
 MORTALITY_CURVE_ID       number(22,0),
 LIFE                     varchar2(35),
 IS_MODIFIED              number(22,0),
 ERROR_MESSAGE            varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_BK_TRANSLATE add constraint TAX_IMP_BKTRANS_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_BK_TRANSLATE add constraint TAX_IMP_BKTRANS_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_BK_TRANSLATE_ARC
(
 IMPORT_RUN_ID            number(22,0) not null,
 LINE_ID                  number(22,0) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 TAX_BOOK_TRANSLATE_XLATE varchar2(254),
 TAX_BOOK_TRANSLATE_ID    number(22,0),
 UTILITY_ACCOUNT_XLATE    varchar2(254),
 UTILITY_ACCOUNT_ID       number(22,0),
 BUS_SEGMENT_XLATE        varchar2(254),
 BUS_SEGMENT_ID           number(22,0),
 SUB_ACCOUNT_XLATE        varchar2(254),
 SUB_ACCOUNT_ID           number(22,0),
 TAX_LOCATION_XLATE       varchar2(254),
 TAX_LOCATION_ID          number(22,0),
 CLASS_CODE_XLATE         varchar2(254),
 CLASS_CODE_ID            number(22,0),
 START_EFF_BOOK_VINTAGE   varchar2(35),
 END_EFF_BOOK_VINTAGE     varchar2(35),
 TAX_CLASS_XLATE          varchar2(254),
 TAX_CLASS_ID             number(22,0),
 ACTIVE                   varchar2(35),
 COMPANY_XLATE            varchar2(254),
 COMPANY_ID               number(22,0),
 CLASS_CODE_VALUE         varchar2(254),
 MONTHLY_ADDS_XLATE       varchar2(254),
 MONTHLY_ADDS             number(22,0),
 GL_ACCOUNT_XLATE         varchar2(254),
 GL_ACCOUNT_ID            number(22,0),
 QUARTERLY_ADDS_XLATE     varchar2(254),
 QUARTERLY_ADDS           number(22,0),
 TAX_DEPR_SCHEMA_XLATE    varchar2(254),
 TAX_DEPR_SCHEMA_ID       number(22,0),
 TAX_DISTINCTION_XLATE    varchar2(254),
 TAX_DISTINCTION_ID       number(22,0),
 MORTALITY_CURVE_XLATE    varchar2(254),
 MORTALITY_CURVE_ID       number(22,0),
 LIFE                     varchar2(35)
);

alter table PWRPLANT.TAX_IMPORT_BK_TRANSLATE_ARC add constraint TAX_IMP_BKTRANS_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_BK_TRANSLATE_ARC add constraint TAX_IMP_BKTRANS_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Normalization Schema.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 208, sysdate, user, 'Normalization Schema', 'Import Normalization Schemas', 'tax_import_norm_schema', 'tax_import_norm_schema_arc', null, 1, 'nvo_tax_logic_import', 'normalization_id', 'Normalization Schema', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 208, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'jurisdiction_id', sysdate, user, 'Jurisdiction', 'jurisdiction_xlate', 1, 1, 'number(22,0)', 'jurisdiction', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'norm_from_tax', sysdate, user, 'From Book', 'norm_from_tax_xlate', 0, 1, 'number(22,0)', 'tax_book', '', 1, null, 'tax_book_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'norm_to_tax', sysdate, user, 'To Book', 'norm_to_tax_xlate', 0, 1, 'number(22,0)', 'tax_book', '', 1, null, 'tax_book_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'def_income_tax_rate_id', sysdate, user, 'Deferred Tax Rate', 'def_income_tax_rate_xlate', 1, 1, 'number(22,0)', 'deferred_rates', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'amortization_type_id', sysdate, user, 'Amortization Type', 'amortization_type_xlate', 1, 1, 'number(22,0)', 'amortization_type', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'reconcile_item_id', sysdate, user, 'Normalized Basis Difference', 'reconcile_item_xlate', 0, 1, 'number(22,0)', 'tax_reconcile_item', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'book_depr_alloc_ind', sysdate, user, 'Allocate Book Depreciation', 'book_depr_alloc_xlate', 1, 1, 'number(1,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'gl_account_id', sysdate, user, 'GL Account', 'gl_account_xlate', 0, 1, 'number(22,0)', 'gl_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'basis_diff_retire_reversal', sysdate, user, 'Net Basis Diff Retire Reversal', 'basis_diff_ret_reversal_xlate', 0, 1, 'number(1,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'cap_depr_ind', sysdate, user, 'Include Cap Depr in DIT Calculation', 'cap_depr_xlate', 0, 1, 'number(1,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'no_zero_check', sysdate, user, 'Allow Deferreds to Over Reverse', 'no_zero_check_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'norm_type_id', sysdate, user, 'Norm Type', 'norm_type_xlate', 0, 1, 'number(22,0)', 'norm_type', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 208, 'basis_diff_activity_split', sysdate, user, 'Net Basis Difference Activity', 'basis_diff_act_split_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2017, sysdate, user, 'Amortization Type.Description', 'The passed in value corresponds to the Amortization Type: Description field.  Translate to the Amortization Type ID using the Description column on the Amortization Type table.', 'amortization_type_id', '( select at.amortization_type_id from amortization_type at where upper( trim( <importfield> ) ) = upper( trim( at.description ) ) )', 0, 'amortization_type', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2018, sysdate, user, 'Deferred Rates.Description', 'The passed in value corresponds to the Deferred Rates: Description field.  Translate to the Def Income Tax Rate ID using the Description column on the Deferred Rates table.', 'def_income_tax_rate_id', '( select dr.def_income_tax_rate_id from deferred_rates dr where upper( trim( <importfield> ) ) = upper( trim( dr.description ) ) )', 0, 'deferred_rates', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2019, sysdate, user, 'Deferred Rates.Long Description', 'The passed in value corresponds to the Deferred Rates: Long Description field.  Translate to the Def Income Tax Rate ID using the Long Description column on the Deferred Rates table.', 'def_income_tax_rate_id', '( select dr.def_income_tax_rate_id from deferred_rates dr where upper( trim( <importfield> ) ) = upper( trim( dr.long_description ) ) )', 0, 'deferred_rates', 'long_description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2020, sysdate, user, 'Jurisdiction.Description', 'The passed in value corresponds to the Jurisdiction: Description field.  Translate to the Jurisdiction ID using the Description column on the Jurisdiction table.', 'jurisdiction_id', '( select j.jurisdiction_id from jurisdiction j where upper( trim( <importfield> ) ) = upper( trim( j.description ) ) )', 0, 'jurisdiction', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2021, sysdate, user, 'Normalization Schema.Description', 'The passed in value corresponds to the Normalization Schema: Description field.  Translate to the Normalization ID using the Description column on the Normalization Schema table.', 'normalization_id', '( select ns.normalization_id from normalization_schema ns where upper( trim( <importfield> ) ) = upper( trim( ns.description ) ) )', 0, 'normalization_schema', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2022, sysdate, user, 'Norm Type.Description', 'The passed in value corresponds to the Norm Type: Description field.  Translate to the Norm Type ID using the Description column on the Norm Type table.', 'norm_type_id', '( select nt.norm_type_id from norm_type nt where upper( trim( <importfield> ) ) = upper( trim( nt.description ) ) )', 0, 'norm_type', 'description', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'amortization_type_id', 2017, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'def_income_tax_rate_id', 2018, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'def_income_tax_rate_id', 2019, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'jurisdiction_id', 2020, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'norm_from_tax', 2001, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'norm_to_tax', 2001, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'reconcile_item_id', 2005, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'book_depr_alloc_ind', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'book_depr_alloc_ind', 78, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'gl_account_id', 22, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'gl_account_id', 23, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'gl_account_id', 24, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'basis_diff_retire_reversal', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'basis_diff_retire_reversal', 78, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'cap_depr_ind', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'cap_depr_ind', 78, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'no_zero_check', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'no_zero_check', 78, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'norm_type_id', 2022, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'basis_diff_activity_split', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 208, 'basis_diff_activity_split', 78, sysdate, user );

insert into pwrplant.pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 208, 2021, sysdate, user );

create table PWRPLANT.TAX_IMPORT_NORM_SCHEMA
(
 IMPORT_RUN_ID                 number(22,0) not null,
 LINE_ID                       number(22,0) not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 DESCRIPTION                   varchar2(254),
 JURISDICTION_XLATE            varchar2(254),
 JURISDICTION_ID               number(22,0),
 NORM_FROM_TAX_XLATE           varchar2(254),
 NORM_FROM_TAX                 number(22,0),
 NORM_TO_TAX_XLATE             varchar2(254),
 NORM_TO_TAX                   number(22,0),
 DEF_INCOME_TAX_RATE_XLATE     varchar2(254),
 DEF_INCOME_TAX_RATE_ID        number(22,0),
 AMORTIZATION_TYPE_XLATE       varchar2(254),
 AMORTIZATION_TYPE_ID          number(22,0),
 RECONCILE_ITEM_XLATE          varchar2(254),
 RECONCILE_ITEM_ID             number(22,0),
 BOOK_DEPR_ALLOC_XLATE         varchar2(254),
 BOOK_DEPR_ALLOC_IND           number(22,0),
 GL_ACCOUNT_XLATE              varchar2(254),
 GL_ACCOUNT_ID                 number(22,0),
 BASIS_DIFF_RET_REVERSAL_XLATE varchar2(254),
 BASIS_DIFF_RETIRE_REVERSAL    number(22,0),
 CAP_DEPR_XLATE                varchar2(254),
 CAP_DEPR_IND                  number(22,0),
 NO_ZERO_CHECK_XLATE           varchar2(254),
 NO_ZERO_CHECK                 number(22,0),
 NORM_TYPE_XLATE               varchar2(254),
 NORM_TYPE_ID                  number(22,0),
 BASIS_DIFF_ACT_SPLIT_XLATE    varchar2(254),
 BASIS_DIFF_ACTIVITY_SPLIT     number(22,0),
 NORMALIZATION_ID              number(22,0),
 IS_MODIFIED                   number(22,0),
 ERROR_MESSAGE                 varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_NORM_SCHEMA add constraint TAX_IMP_NORM_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_NORM_SCHEMA add constraint TAX_IMP_NORM_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_NORM_SCHEMA_ARC
(
 IMPORT_RUN_ID                 number(22,0) not null,
 LINE_ID                       number(22,0) not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 DESCRIPTION                   varchar2(254),
 JURISDICTION_XLATE            varchar2(254),
 JURISDICTION_ID               number(22,0),
 NORM_FROM_TAX_XLATE           varchar2(254),
 NORM_FROM_TAX                 number(22,0),
 NORM_TO_TAX_XLATE             varchar2(254),
 NORM_TO_TAX                   number(22,0),
 DEF_INCOME_TAX_RATE_XLATE     varchar2(254),
 DEF_INCOME_TAX_RATE_ID        number(22,0),
 AMORTIZATION_TYPE_XLATE       varchar2(254),
 AMORTIZATION_TYPE_ID          number(22,0),
 RECONCILE_ITEM_XLATE          varchar2(254),
 RECONCILE_ITEM_ID             number(22,0),
 BOOK_DEPR_ALLOC_XLATE         varchar2(254),
 BOOK_DEPR_ALLOC_IND           number(22,0),
 GL_ACCOUNT_XLATE              varchar2(254),
 GL_ACCOUNT_ID                 number(22,0),
 BASIS_DIFF_RET_REVERSAL_XLATE varchar2(254),
 BASIS_DIFF_RETIRE_REVERSAL    number(22,0),
 CAP_DEPR_XLATE                varchar2(254),
 CAP_DEPR_IND                  number(22,0),
 NO_ZERO_CHECK_XLATE           varchar2(254),
 NO_ZERO_CHECK                 number(22,0),
 NORM_TYPE_XLATE               varchar2(254),
 NORM_TYPE_ID                  number(22,0),
 BASIS_DIFF_ACT_SPLIT_XLATE    varchar2(254),
 BASIS_DIFF_ACTIVITY_SPLIT     number(22,0),
 NORMALIZATION_ID              number(22,0)
);

alter table PWRPLANT.TAX_IMPORT_NORM_SCHEMA_ARC add constraint TAX_IMP_NORM_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_NORM_SCHEMA_ARC add constraint TAX_IMP_NORM_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Tax Depr Schema.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 209, sysdate, user, 'Tax Depr Schema', 'Import Tax Depr Schemas', 'tax_import_depr_schema', 'tax_import_depr_schema_arc', null, 0, 'nvo_tax_logic_import', 'tax_depr_schema_id', 'Tax Depr Schema', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 209, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 209, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );

create table PWRPLANT.TAX_IMPORT_DEPR_SCHEMA
(
 IMPORT_RUN_ID      number(22,0) not null,
 LINE_ID            number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(254),
 TAX_DEPR_SCHEMA_ID number(22,0),
 IS_MODIFIED        number(22,0),
 ERROR_MESSAGE      varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_DEPR_SCHEMA add constraint TAX_IMP_DEPR_SCHM_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_DEPR_SCHEMA add constraint TAX_IMP_DEPR_SCHM_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_DEPR_SCHEMA_ARC
(
 IMPORT_RUN_ID      number(22,0) not null,
 LINE_ID            number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(254),
 TAX_DEPR_SCHEMA_ID number(22,0)
);

alter table PWRPLANT.TAX_IMPORT_DEPR_SCHEMA_ARC add constraint TAX_IMP_DEPR_SCHM_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_DEPR_SCHEMA_ARC add constraint TAX_IMP_DEPR_SCHM_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Tax Depr Schema Control.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 210, sysdate, user, 'Tax Depr Schema Control', 'Import Tax Depr Schema Control Records', 'tax_import_depr_sch_ctrl', 'tax_import_depr_sch_ctrl_arc', null, 1, 'nvo_tax_logic_import', 'tax_depr_schema_control_id', 'Tax Depr Schema Control', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 210, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'tax_depr_schema_id', sysdate, user, 'Tax Depr Schema', 'tax_depr_schema_xlate', 1, 1, 'number(22,0)', 'tax_depr_schema', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'tax_book_id', sysdate, user, 'Tax Book', 'tax_book_xlate', 1, 1, 'number(22,0)', 'tax_book', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'ext_class_code_value', sysdate, user, 'Ext Class Code Value', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'vintage_convention_id', sysdate, user, 'Vintage Convention', 'vintage_convention_xlate', 1, 1, 'number(22,0)', 'tax_vintage_convention', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'tax_rate_id', sysdate, user, 'Tax Rate', 'tax_rate_xlate', 1, 1, 'number(22,0)', 'tax_rate_control', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'convention_id', sysdate, user, 'Convention', 'convention_xlate', 1, 1, 'number(22,0)', 'tax_convention', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'extraordinary_convention', sysdate, user, 'Extraordinary Convention', 'extraordinary_conv_xlate', 1, 1, 'number(22,0)', 'tax_convention', '', 1, null, 'convention_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'tax_limit_id', sysdate, user, 'Tax Limit', 'tax_limit_xlate', 0, 1, 'number(22,0)', 'tax_limit', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'tax_credit_id', sysdate, user, 'Tax Credit', 'tax_credit_xlate', 0, 1, 'number(22,0)', 'tax_credit', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'deferred_tax_schema_id', sysdate, user, 'Deferred Tax Schema', 'deferred_tax_schema_xlate', 0, 1, 'number(22,0)', 'deferred_tax_schema', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'summary_4562_id', sysdate, user, 'Summary 4562', 'summary_4562_xlate', 0, 1, 'number(22,0)', 'summary_4562', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'recovery_period_id', sysdate, user, 'Recovery Period', 'recovery_period_xlate', 0, 1, 'number(22,0)', 'recovery_period', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'in_service_month', sysdate, user, 'Month', '', 0, 1, 'date', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'def_gain_reconcile_item_id', sysdate, user, 'Def Gain Reconcile Item', 'def_gain_rec_item_xlate', 0, 1, 'number(22,0)', 'tax_reconcile_item', '', 1, null, 'reconcile_item_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'def_gain_tax_include_id', sysdate, user, 'Def Gain Tax Include', 'def_gain_tax_inc_xlate', 0, 1, 'number(22,0)', 'tax_include', '', 1, null, 'tax_include_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 210, 'type_of_property_id', sysdate, user, 'Type of Property', 'type_of_property_xlate', 0, 1, 'number(22,0)', 'tax_type_of_property', '', 1, null, '' );

insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2023, sysdate, user, 'Tax Rate Control.Description', 'The passed in value corresponds to the Tax Rate Control: Description field.  Translate to the Tax Rate ID using the Description column on the Tax Rate Control table.', 'tax_rate_id', '( select tr.tax_rate_id from tax_rate_control tr where upper( trim( <importfield> ) ) = upper( trim( tr.description ) ) )', 0, 'tax_rate_control', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2024, sysdate, user, 'Tax Convention.Description', 'The passed in value corresponds to the Tax Convention: Description field.  Translate to the Convention ID using the Description column on the Tax Convention table.', 'convention_id', '( select tc.convention_id from tax_convention tc where upper( trim( <importfield> ) ) = upper( trim( tc.description ) ) )', 0, 'tax_convention', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2025, sysdate, user, 'Tax Limit.Description', 'The passed in value corresponds to the Tax Limit: Description field.  Translate to the Tax Limit ID using the Description column on the Tax Limit table.', 'tax_limit_id', '( select tl.tax_limit_id from tax_limit tl where upper( trim( <importfield> ) ) = upper( trim( tl.description ) ) )', 0, 'tax_limit', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2026, sysdate, user, 'Tax Credit.Description', 'The passed in value corresponds to the Tax Credit: Description field.  Translate to the Tax Credit ID using the Description column on the Tax Credit table.', 'tax_credit_id', '( select tc.tax_credit_id from tax_credit tc where upper( trim( <importfield> ) ) = upper( trim( tc.description ) ) )', 0, 'tax_credit', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2027, sysdate, user, 'Deferred Tax Schema.Description', 'The passed in value corresponds to the Deferred Tax Schema: Description field.  Translate to the Deferred Tax Schema ID using the Description column on the Deferred Tax Schema table.', 'deferred_tax_schema_id', '( select dts.deferred_tax_schema_id from deferred_tax_schema dts where upper( trim( <importfield> ) ) = upper( trim( dts.description ) ) )', 0, 'deferred_tax_schema', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2028, sysdate, user, 'Summary 4562.Description', 'The passed in value corresponds to the Summary 4562: Description field.  Translate to the Summary 4562 ID using the Description column on the Summary 4562 table.', 'summary_4562_id', '( select s.summary_4562_id from summary_4562 s where upper( trim( <importfield> ) ) = upper( trim( s.description ) ) )', 0, 'summary_4562', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2029, sysdate, user, 'Recovery Period.Description', 'The passed in value corresponds to the Recovery Period: Description field.  Translate to the Recovery Period ID using the Description column on the Recovery Period table.', 'recovery_period_id', '( select rp.recovery_period_id from recovery_period rp where upper( trim( <importfield> ) ) = upper( trim( rp.description ) ) )', 0, 'recovery_period', 'description', '', '', null );
insert into pwrplant.pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 2030, sysdate, user, 'Recovery Period.Period', 'The passed in value corresponds to the Recovery Period: Period field.  Translate to the Recovery Period ID using the Period column on the Recovery Period table.', 'recovery_period_id', '( select rp.recovery_period_id from recovery_period rp where upper( trim( <importfield> ) ) = upper( trim( rp.period ) ) )', 0, 'recovery_period', 'period', '', '', null );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'tax_depr_schema_id', 2011, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'tax_book_id', 2001, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'vintage_convention_id', 2015, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'tax_rate_id', 2023, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'convention_id', 2024, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'extraordinary_convention', 2024, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'tax_limit_id', 2025, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'tax_credit_id', 2026, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'deferred_tax_schema_id', 2027, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'summary_4562_id', 2028, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'recovery_period_id', 2029, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'recovery_period_id', 2030, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'def_gain_reconcile_item_id', 2005, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'def_gain_tax_include_id', 2003, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 210, 'type_of_property_id', 2014, sysdate, user );

create table PWRPLANT.TAX_IMPORT_DEPR_SCH_CTRL
(
 IMPORT_RUN_ID              number(22,0) not null,
 LINE_ID                    number(22,0) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 TAX_DEPR_SCHEMA_XLATE      varchar2(254),
 TAX_DEPR_SCHEMA_ID         number(22,0),
 TAX_BOOK_XLATE             varchar2(254),
 TAX_BOOK_ID                number(22,0),
 EXT_CLASS_CODE_VALUE       varchar2(254),
 VINTAGE_CONVENTION_XLATE   varchar2(254),
 VINTAGE_CONVENTION_ID      number(22,0),
 TAX_RATE_XLATE             varchar2(254),
 TAX_RATE_ID                number(22,0),
 CONVENTION_XLATE           varchar2(254),
 CONVENTION_ID              number(22,0),
 EXTRAORDINARY_CONV_XLATE   varchar2(254),
 EXTRAORDINARY_CONVENTION   number(22,0),
 TAX_LIMIT_XLATE            varchar2(254),
 TAX_LIMIT_ID               number(22,0),
 TAX_CREDIT_XLATE           varchar2(254),
 TAX_CREDIT_ID              number(22,0),
 DEFERRED_TAX_SCHEMA_XLATE  varchar2(254),
 DEFERRED_TAX_SCHEMA_ID     number(22,0),
 SUMMARY_4562_XLATE         varchar2(254),
 SUMMARY_4562_ID            number(22,0),
 RECOVERY_PERIOD_XLATE      varchar2(254),
 RECOVERY_PERIOD_ID         number(22,0),
 IN_SERVICE_MONTH           varchar2(35),
 DEF_GAIN_REC_ITEM_XLATE    varchar2(254),
 DEF_GAIN_RECONCILE_ITEM_ID number(22,0),
 DEF_GAIN_TAX_INC_XLATE     varchar2(254),
 DEF_GAIN_TAX_INCLUDE_ID    number(22,0),
 TYPE_OF_PROPERTY_XLATE     varchar2(254),
 TYPE_OF_PROPERTY_ID        number(22,0),
 TAX_DEPR_SCHEMA_CONTROL_ID number(22,0),
 IS_MODIFIED                number(22,0),
 ERROR_MESSAGE              varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_DEPR_SCH_CTRL add constraint TAX_IMP_DPRSCHMCTRL_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_DEPR_SCH_CTRL add constraint TAX_IMP_DPRSCHMCTRL_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_DEPR_SCH_CTRL_ARC
(
 IMPORT_RUN_ID              number(22,0) not null,
 LINE_ID                    number(22,0) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 TAX_DEPR_SCHEMA_XLATE      varchar2(254),
 TAX_DEPR_SCHEMA_ID         number(22,0),
 TAX_BOOK_XLATE             varchar2(254),
 TAX_BOOK_ID                number(22,0),
 EXT_CLASS_CODE_VALUE       varchar2(254),
 VINTAGE_CONVENTION_XLATE   varchar2(254),
 VINTAGE_CONVENTION_ID      number(22,0),
 TAX_RATE_XLATE             varchar2(254),
 TAX_RATE_ID                number(22,0),
 CONVENTION_XLATE           varchar2(254),
 CONVENTION_ID              number(22,0),
 EXTRAORDINARY_CONV_XLATE   varchar2(254),
 EXTRAORDINARY_CONVENTION   number(22,0),
 TAX_LIMIT_XLATE            varchar2(254),
 TAX_LIMIT_ID               number(22,0),
 TAX_CREDIT_XLATE           varchar2(254),
 TAX_CREDIT_ID              number(22,0),
 DEFERRED_TAX_SCHEMA_XLATE  varchar2(254),
 DEFERRED_TAX_SCHEMA_ID     number(22,0),
 SUMMARY_4562_XLATE         varchar2(254),
 SUMMARY_4562_ID            number(22,0),
 RECOVERY_PERIOD_XLATE      varchar2(254),
 RECOVERY_PERIOD_ID         number(22,0),
 IN_SERVICE_MONTH           varchar2(35),
 DEF_GAIN_REC_ITEM_XLATE    varchar2(254),
 DEF_GAIN_RECONCILE_ITEM_ID number(22,0),
 DEF_GAIN_TAX_INC_XLATE     varchar2(254),
 DEF_GAIN_TAX_INCLUDE_ID    number(22,0),
 TYPE_OF_PROPERTY_XLATE     varchar2(254),
 TYPE_OF_PROPERTY_ID        number(22,0),
 TAX_DEPR_SCHEMA_CONTROL_ID number(22,0)
);

alter table PWRPLANT.TAX_IMPORT_DEPR_SCH_CTRL_ARC add constraint TAX_IMP_DPRSCHMCTRL_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_DEPR_SCH_CTRL_ARC add constraint TAX_IMP_DPRSCHMCTRL_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Tax Retire Rules.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 211, sysdate, user, 'Tax Retirement/Transfer Rules', 'Import Tax Retirement/Transfer Rules', 'tax_import_retire_rules', 'tax_import_retire_rules_arc', null, 1, 'nvo_tax_logic_import', '', 'Tax Retire Rules', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 211, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'iteration', sysdate, user, 'Iteration', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'vintage_forward', sysdate, user, 'Vintage Forward', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'vintage_back', sysdate, user, 'Vintage Back', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'sort_order', sysdate, user, 'Sort Order', '', 1, 1, 'varchar2(100)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'gformat_string', sysdate, user, 'Filter for Retirement Records', '', 0, 1, 'varchar2(100)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'fformat_string', sysdate, user, 'Filter for Tax Records', '', 0, 1, 'varchar2(100)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'fifo', sysdate, user, 'FIFO', 'fifo_xlate', 1, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'tolerance', sysdate, user, 'Tolerance', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 211, 'min_vintage', sysdate, user, 'Minimum Vintage', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 211, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 211, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 211, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 211, 'fifo', 77, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 211, 'fifo', 78, sysdate, user );

create table PWRPLANT.TAX_IMPORT_RETIRE_RULES
(
 IMPORT_RUN_ID   number(22,0) not null,
 LINE_ID         number(22,0) not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 COMPANY_XLATE   varchar2(254),
 COMPANY_ID      number(22,0),
 ITERATION       varchar2(35),
 VINTAGE_FORWARD varchar2(35),
 VINTAGE_BACK    varchar2(35),
 SORT_ORDER      varchar2(254),
 GFORMAT_STRING  varchar2(254),
 FFORMAT_STRING  varchar2(254),
 FIFO_XLATE      varchar2(254),
 FIFO            number(22,0),
 TOLERANCE       varchar2(35),
 MIN_VINTAGE     varchar2(35),
 IS_MODIFIED     number(22,0),
 ERROR_MESSAGE   varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_RETIRE_RULES add constraint TAX_IMP_RET_RULES_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_RETIRE_RULES add constraint TAX_IMP_RET_RULES_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_RETIRE_RULES_ARC
(
 IMPORT_RUN_ID   number(22,0) not null,
 LINE_ID         number(22,0) not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 COMPANY_XLATE   varchar2(254),
 COMPANY_ID      number(22,0),
 ITERATION       varchar2(35),
 VINTAGE_FORWARD varchar2(35),
 VINTAGE_BACK    varchar2(35),
 SORT_ORDER      varchar2(254),
 GFORMAT_STRING  varchar2(254),
 FFORMAT_STRING  varchar2(254),
 FIFO_XLATE      varchar2(254),
 FIFO            number(22,0),
 TOLERANCE       varchar2(35),
 MIN_VINTAGE     varchar2(35)
);

alter table PWRPLANT.TAX_IMPORT_RETIRE_RULES_ARC add constraint TAX_IMP_RET_RULES_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_RETIRE_RULES_ARC add constraint TAX_IMP_RET_RULES_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Tax Book Trans Adds.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 212, sysdate, user, 'Tax Additions', 'Import Tax Additions', 'tax_import_adds', 'tax_import_adds_arc', null, 1, 'nvo_tax_logic_import', '', 'Tax Additions', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 212, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'trans_id', sysdate, user, 'Transaction ID', 'trans_xlate', 0, 1, 'number(22,0)', 'tax_book_trans_adds', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'year', sysdate, user, 'Year', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'start_month', sysdate, user, 'Start Month', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'end_month', sysdate, user, 'End Month', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'utility_account_id', sysdate, user, 'Utility Account', 'utility_account_xlate', 1, 3, 'number(22,0)', 'utility_account', 'bus_segment_id', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'sub_account_id', sysdate, user, 'Sub Account', 'sub_account_xlate', 1, 4, 'number(22,0)', 'sub_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'bus_segment_id', sysdate, user, 'Business Segment', 'bus_segment_xlate', 1, 2, 'number(22,0)', 'business_segment', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'in_service_year', sysdate, user, 'In Service Year', '', 1, 1, 'date', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'gl_posting_mo_yr', sysdate, user, 'Gl Posting Mo Yr', '', 1, 1, 'date', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'tax_location_id', sysdate, user, 'Tax Location', 'tax_location_xlate', 0, 1, 'number(22,0)', 'tax_location', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'class_code_id', sysdate, user, 'Class Code', 'class_code_xlate', 0, 1, 'number(22,0)', 'class_code', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'cc_tax_indicator', sysdate, user, 'Class Code Tax Indicator', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'cc_value', sysdate, user, 'Class Code Value', '', 0, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'gl_account_id', sysdate, user, 'GL Account', 'gl_account_xlate', 0, 2, 'number(22,0)', 'gl_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'bonus_value', sysdate, user, 'Bonus Value', '', 0, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'ext_class_code_value', sysdate, user, 'Ext Class Code Value', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'month_number', sysdate, user, 'Month Number', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'tax_distinction_id', sysdate, user, 'Tax Distinction', 'tax_distinction_xlate', 0, 1, 'number(22,0)', 'retire_unit_tax_distinction', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'work_order_number', sysdate, user, 'Work Order Number', '', 0, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'tax_load_id', sysdate, user, 'Tax Load ID', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'amount', sysdate, user, 'Amount', '', 1, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_1', sysdate, user, 'Basis 1', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_2', sysdate, user, 'Basis 2', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_3', sysdate, user, 'Basis 3', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_4', sysdate, user, 'Basis 4', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_5', sysdate, user, 'Basis 5', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_6', sysdate, user, 'Basis 6', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_7', sysdate, user, 'Basis 7', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_8', sysdate, user, 'Basis 8', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_9', sysdate, user, 'Basis 9', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_10', sysdate, user, 'Basis 10', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_11', sysdate, user, 'Basis 11', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_12', sysdate, user, 'Basis 12', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_13', sysdate, user, 'Basis 13', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_14', sysdate, user, 'Basis 14', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_15', sysdate, user, 'Basis 15', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_16', sysdate, user, 'Basis 16', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_17', sysdate, user, 'Basis 17', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_18', sysdate, user, 'Basis 18', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_19', sysdate, user, 'Basis 19', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_20', sysdate, user, 'Basis 20', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_21', sysdate, user, 'Basis 21', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_22', sysdate, user, 'Basis 22', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_23', sysdate, user, 'Basis 23', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_24', sysdate, user, 'Basis 24', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_25', sysdate, user, 'Basis 25', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_26', sysdate, user, 'Basis 26', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_27', sysdate, user, 'Basis 27', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_28', sysdate, user, 'Basis 28', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_29', sysdate, user, 'Basis 29', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_30', sysdate, user, 'Basis 30', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_31', sysdate, user, 'Basis 31', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_32', sysdate, user, 'Basis 32', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_33', sysdate, user, 'Basis 33', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_34', sysdate, user, 'Basis 34', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_35', sysdate, user, 'Basis 35', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_36', sysdate, user, 'Basis 36', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_37', sysdate, user, 'Basis 37', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_38', sysdate, user, 'Basis 38', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_39', sysdate, user, 'Basis 39', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_40', sysdate, user, 'Basis 40', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_41', sysdate, user, 'Basis 41', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_42', sysdate, user, 'Basis 42', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_43', sysdate, user, 'Basis 43', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_44', sysdate, user, 'Basis 44', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_45', sysdate, user, 'Basis 45', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_46', sysdate, user, 'Basis 46', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_47', sysdate, user, 'Basis 47', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_48', sysdate, user, 'Basis 48', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_49', sysdate, user, 'Basis 49', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_50', sysdate, user, 'Basis 50', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_51', sysdate, user, 'Basis 51', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_52', sysdate, user, 'Basis 52', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_53', sysdate, user, 'Basis 53', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_54', sysdate, user, 'Basis 54', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_55', sysdate, user, 'Basis 55', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_56', sysdate, user, 'Basis 56', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_57', sysdate, user, 'Basis 57', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_58', sysdate, user, 'Basis 58', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_59', sysdate, user, 'Basis 59', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_60', sysdate, user, 'Basis 60', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_61', sysdate, user, 'Basis 61', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_62', sysdate, user, 'Basis 62', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_63', sysdate, user, 'Basis 63', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_64', sysdate, user, 'Basis 64', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_65', sysdate, user, 'Basis 65', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_66', sysdate, user, 'Basis 66', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_67', sysdate, user, 'Basis 67', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_68', sysdate, user, 'Basis 68', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_69', sysdate, user, 'Basis 69', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 212, 'basis_70', sysdate, user, 'Basis 70', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'utility_account_id', 27, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'utility_account_id', 28, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'utility_account_id', 29, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'utility_account_id', 30, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'sub_account_id', 612, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'sub_account_id', 613, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'sub_account_id', 614, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'bus_segment_id', 25, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'bus_segment_id', 26, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'bus_segment_id', 604, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'bus_segment_id', 605, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'tax_location_id', 145, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'class_code_id', 199, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'class_code_id', 200, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'gl_account_id', 22, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'gl_account_id', 23, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'gl_account_id', 24, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'gl_account_id', 601, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'gl_account_id', 602, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'gl_account_id', 603, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'tax_distinction_id', 2012, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 212, 'tax_distinction_id', 2013, sysdate, user );

create table PWRPLANT.TAX_IMPORT_ADDS
(
 IMPORT_RUN_ID         number(22,0) not null,
 LINE_ID               number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 TRANS_XLATE           varchar2(254),
 TRANS_ID              number(22,0),
 YEAR                  varchar2(35),
 START_MONTH           varchar2(35),
 END_MONTH             varchar2(35),
 UTILITY_ACCOUNT_XLATE varchar2(254),
 UTILITY_ACCOUNT_ID    number(22,0),
 SUB_ACCOUNT_XLATE     varchar2(254),
 SUB_ACCOUNT_ID        number(22,0),
 COMPANY_XLATE         varchar2(254),
 COMPANY_ID            number(22,0),
 BUS_SEGMENT_XLATE     varchar2(254),
 BUS_SEGMENT_ID        number(22,0),
 IN_SERVICE_YEAR       varchar2(35),
 GL_POSTING_MO_YR      varchar2(35),
 TAX_LOCATION_XLATE    varchar2(254),
 TAX_LOCATION_ID       number(22,0),
 CLASS_CODE_XLATE      varchar2(254),
 CLASS_CODE_ID         number(22,0),
 CC_TAX_INDICATOR      varchar2(35),
 CC_VALUE              varchar2(254),
 GL_ACCOUNT_XLATE      varchar2(254),
 GL_ACCOUNT_ID         number(22,0),
 BONUS_VALUE           varchar2(254),
 EXT_CLASS_CODE_VALUE  varchar2(254),
 MONTH_NUMBER          varchar2(35),
 TAX_DISTINCTION_XLATE varchar2(254),
 TAX_DISTINCTION_ID    number(22,0),
 WORK_ORDER_NUMBER     varchar2(254),
 TAX_LOAD_ID           varchar2(35),
 AMOUNT                varchar2(35),
 BASIS_1               varchar2(35),
 BASIS_2               varchar2(35),
 BASIS_3               varchar2(35),
 BASIS_4               varchar2(35),
 BASIS_5               varchar2(35),
 BASIS_6               varchar2(35),
 BASIS_7               varchar2(35),
 BASIS_8               varchar2(35),
 BASIS_9               varchar2(35),
 BASIS_10              varchar2(35),
 BASIS_11              varchar2(35),
 BASIS_12              varchar2(35),
 BASIS_13              varchar2(35),
 BASIS_14              varchar2(35),
 BASIS_15              varchar2(35),
 BASIS_16              varchar2(35),
 BASIS_17              varchar2(35),
 BASIS_18              varchar2(35),
 BASIS_19              varchar2(35),
 BASIS_20              varchar2(35),
 BASIS_21              varchar2(35),
 BASIS_22              varchar2(35),
 BASIS_23              varchar2(35),
 BASIS_24              varchar2(35),
 BASIS_25              varchar2(35),
 BASIS_26              varchar2(35),
 BASIS_27              varchar2(35),
 BASIS_28              varchar2(35),
 BASIS_29              varchar2(35),
 BASIS_30              varchar2(35),
 BASIS_31              varchar2(35),
 BASIS_32              varchar2(35),
 BASIS_33              varchar2(35),
 BASIS_34              varchar2(35),
 BASIS_35              varchar2(35),
 BASIS_36              varchar2(35),
 BASIS_37              varchar2(35),
 BASIS_38              varchar2(35),
 BASIS_39              varchar2(35),
 BASIS_40              varchar2(35),
 BASIS_41              varchar2(35),
 BASIS_42              varchar2(35),
 BASIS_43              varchar2(35),
 BASIS_44              varchar2(35),
 BASIS_45              varchar2(35),
 BASIS_46              varchar2(35),
 BASIS_47              varchar2(35),
 BASIS_48              varchar2(35),
 BASIS_49              varchar2(35),
 BASIS_50              varchar2(35),
 BASIS_51              varchar2(35),
 BASIS_52              varchar2(35),
 BASIS_53              varchar2(35),
 BASIS_54              varchar2(35),
 BASIS_55              varchar2(35),
 BASIS_56              varchar2(35),
 BASIS_57              varchar2(35),
 BASIS_58              varchar2(35),
 BASIS_59              varchar2(35),
 BASIS_60              varchar2(35),
 BASIS_61              varchar2(35),
 BASIS_62              varchar2(35),
 BASIS_63              varchar2(35),
 BASIS_64              varchar2(35),
 BASIS_65              varchar2(35),
 BASIS_66              varchar2(35),
 BASIS_67              varchar2(35),
 BASIS_68              varchar2(35),
 BASIS_69              varchar2(35),
 BASIS_70              varchar2(35),
 IS_MODIFIED           number(22,0),
 ERROR_MESSAGE         varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_ADDS add constraint TAX_IMP_ADDS_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_ADDS add constraint TAX_IMP_ADDS_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_ADDS_ARC
(
 IMPORT_RUN_ID         number(22,0) not null,
 LINE_ID               number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 TRANS_XLATE           varchar2(254),
 TRANS_ID              number(22,0),
 YEAR                  varchar2(35),
 START_MONTH           varchar2(35),
 END_MONTH             varchar2(35),
 UTILITY_ACCOUNT_XLATE varchar2(254),
 UTILITY_ACCOUNT_ID    number(22,0),
 SUB_ACCOUNT_XLATE     varchar2(254),
 SUB_ACCOUNT_ID        number(22,0),
 COMPANY_XLATE         varchar2(254),
 COMPANY_ID            number(22,0),
 BUS_SEGMENT_XLATE     varchar2(254),
 BUS_SEGMENT_ID        number(22,0),
 IN_SERVICE_YEAR       varchar2(35),
 GL_POSTING_MO_YR      varchar2(35),
 TAX_LOCATION_XLATE    varchar2(254),
 TAX_LOCATION_ID       number(22,0),
 CLASS_CODE_XLATE      varchar2(254),
 CLASS_CODE_ID         number(22,0),
 CC_TAX_INDICATOR      varchar2(35),
 CC_VALUE              varchar2(254),
 GL_ACCOUNT_XLATE      varchar2(254),
 GL_ACCOUNT_ID         number(22,0),
 BONUS_VALUE           varchar2(254),
 EXT_CLASS_CODE_VALUE  varchar2(254),
 MONTH_NUMBER          varchar2(35),
 TAX_DISTINCTION_XLATE varchar2(254),
 TAX_DISTINCTION_ID    number(22,0),
 WORK_ORDER_NUMBER     varchar2(254),
 TAX_LOAD_ID           varchar2(35),
 AMOUNT                varchar2(35),
 BASIS_1               varchar2(35),
 BASIS_2               varchar2(35),
 BASIS_3               varchar2(35),
 BASIS_4               varchar2(35),
 BASIS_5               varchar2(35),
 BASIS_6               varchar2(35),
 BASIS_7               varchar2(35),
 BASIS_8               varchar2(35),
 BASIS_9               varchar2(35),
 BASIS_10              varchar2(35),
 BASIS_11              varchar2(35),
 BASIS_12              varchar2(35),
 BASIS_13              varchar2(35),
 BASIS_14              varchar2(35),
 BASIS_15              varchar2(35),
 BASIS_16              varchar2(35),
 BASIS_17              varchar2(35),
 BASIS_18              varchar2(35),
 BASIS_19              varchar2(35),
 BASIS_20              varchar2(35),
 BASIS_21              varchar2(35),
 BASIS_22              varchar2(35),
 BASIS_23              varchar2(35),
 BASIS_24              varchar2(35),
 BASIS_25              varchar2(35),
 BASIS_26              varchar2(35),
 BASIS_27              varchar2(35),
 BASIS_28              varchar2(35),
 BASIS_29              varchar2(35),
 BASIS_30              varchar2(35),
 BASIS_31              varchar2(35),
 BASIS_32              varchar2(35),
 BASIS_33              varchar2(35),
 BASIS_34              varchar2(35),
 BASIS_35              varchar2(35),
 BASIS_36              varchar2(35),
 BASIS_37              varchar2(35),
 BASIS_38              varchar2(35),
 BASIS_39              varchar2(35),
 BASIS_40              varchar2(35),
 BASIS_41              varchar2(35),
 BASIS_42              varchar2(35),
 BASIS_43              varchar2(35),
 BASIS_44              varchar2(35),
 BASIS_45              varchar2(35),
 BASIS_46              varchar2(35),
 BASIS_47              varchar2(35),
 BASIS_48              varchar2(35),
 BASIS_49              varchar2(35),
 BASIS_50              varchar2(35),
 BASIS_51              varchar2(35),
 BASIS_52              varchar2(35),
 BASIS_53              varchar2(35),
 BASIS_54              varchar2(35),
 BASIS_55              varchar2(35),
 BASIS_56              varchar2(35),
 BASIS_57              varchar2(35),
 BASIS_58              varchar2(35),
 BASIS_59              varchar2(35),
 BASIS_60              varchar2(35),
 BASIS_61              varchar2(35),
 BASIS_62              varchar2(35),
 BASIS_63              varchar2(35),
 BASIS_64              varchar2(35),
 BASIS_65              varchar2(35),
 BASIS_66              varchar2(35),
 BASIS_67              varchar2(35),
 BASIS_68              varchar2(35),
 BASIS_69              varchar2(35),
 BASIS_70              varchar2(35)
);

alter table PWRPLANT.TAX_IMPORT_ADDS_ARC add constraint TAX_IMP_ADDS_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_ADDS_ARC add constraint TAX_IMP_ADDS_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Tax Book Transactions.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 213, sysdate, user, 'Tax Retirements', 'Import Tax Retirements', 'tax_import_rets', 'tax_import_rets_arc', null, 1, 'nvo_tax_logic_import', '', 'Tax Retirements', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 213, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'trans_id', sysdate, user, 'Transaction ID', 'trans_xlate', 0, 1, 'number(22,0)', 'tax_book_transactions', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'year', sysdate, user, 'Year', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'start_month', sysdate, user, 'Start Month', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'end_month', sysdate, user, 'End Month', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'tax_activity_code_id', sysdate, user, 'Tax Activity Code', 'tax_activity_code_xlate', 1, 1, 'number(22,0)', 'tax_activity_code', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'utility_account_id', sysdate, user, 'Utility Account', 'utility_account_xlate', 1, 3, 'number(22,0)', 'utility_account', 'bus_segment_id', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'sub_account_id', sysdate, user, 'Sub Account', 'sub_account_xlate', 1, 4, 'number(22,0)', 'sub_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'bus_segment_id', sysdate, user, 'Business Segment', 'bus_segment_xlate', 1, 2, 'number(22,0)', 'business_segment', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'in_service_year', sysdate, user, 'In Service Year', '', 1, 1, 'date', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'gl_posting_mo_yr', sysdate, user, 'Gl Posting Mo Yr', '', 1, 1, 'date', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'tax_location_id', sysdate, user, 'Tax Location', 'tax_location_xlate', 0, 1, 'number(22,0)', 'tax_location', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'class_code_id', sysdate, user, 'Class Code', 'class_code_xlate', 0, 1, 'number(22,0)', 'class_code', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'cc_tax_indicator', sysdate, user, 'Class Code Tax Indicator', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'cc_value', sysdate, user, 'Class Code Value', '', 0, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'gl_account_id', sysdate, user, 'GL Account', 'gl_account_xlate', 0, 2, 'number(22,0)', 'gl_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'bonus_value', sysdate, user, 'Bonus Value', '', 0, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'ext_class_code_value', sysdate, user, 'Ext Class Code Value', '', 1, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'month_number', sysdate, user, 'Month Number', '', 1, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'tax_distinction_id', sysdate, user, 'Tax Distinction', 'tax_distinction_xlate', 0, 1, 'number(22,0)', 'retire_unit_tax_distinction', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 213, 'amount', sysdate, user, 'Amount', '', 1, 1, 'number(22,2)', '', '', 1, null, '' );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'tax_activity_code_id', 2002, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'utility_account_id', 27, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'utility_account_id', 28, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'utility_account_id', 29, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'utility_account_id', 30, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'sub_account_id', 612, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'sub_account_id', 613, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'sub_account_id', 614, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'bus_segment_id', 25, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'bus_segment_id', 26, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'bus_segment_id', 604, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'bus_segment_id', 605, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'tax_location_id', 145, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'class_code_id', 199, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'class_code_id', 200, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'gl_account_id', 22, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'gl_account_id', 23, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'gl_account_id', 24, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'gl_account_id', 601, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'gl_account_id', 602, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'gl_account_id', 603, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'tax_distinction_id', 2012, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 213, 'tax_distinction_id', 2013, sysdate, user );

create table PWRPLANT.TAX_IMPORT_RETS
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TRANS_XLATE             varchar2(254),
 TRANS_ID                number(22,0),
 YEAR                    varchar2(35),
 START_MONTH             varchar2(35),
 END_MONTH               varchar2(35),
 TAX_ACTIVITY_CODE_XLATE varchar2(254),
 TAX_ACTIVITY_CODE_ID    number(22,0),
 UTILITY_ACCOUNT_XLATE   varchar2(254),
 UTILITY_ACCOUNT_ID      number(22,0),
 SUB_ACCOUNT_XLATE       varchar2(254),
 SUB_ACCOUNT_ID          number(22,0),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 BUS_SEGMENT_XLATE       varchar2(254),
 BUS_SEGMENT_ID          number(22,0),
 IN_SERVICE_YEAR         varchar2(35),
 GL_POSTING_MO_YR        varchar2(35),
 TAX_LOCATION_XLATE      varchar2(254),
 TAX_LOCATION_ID         number(22,0),
 CLASS_CODE_XLATE        varchar2(254),
 CLASS_CODE_ID           number(22,0),
 CC_TAX_INDICATOR        varchar2(35),
 CC_VALUE                varchar2(254),
 GL_ACCOUNT_XLATE        varchar2(254),
 GL_ACCOUNT_ID           number(22,0),
 BONUS_VALUE             varchar2(254),
 EXT_CLASS_CODE_VALUE    varchar2(254),
 MONTH_NUMBER            varchar2(35),
 TAX_DISTINCTION_XLATE   varchar2(254),
 TAX_DISTINCTION_ID      number(22,0),
 AMOUNT                  varchar2(35),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_RETS add constraint TAX_IMP_RETS_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_RETS add constraint TAX_IMP_RETS_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_RETS_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TRANS_XLATE             varchar2(254),
 TRANS_ID                number(22,0),
 YEAR                    varchar2(35),
 START_MONTH             varchar2(35),
 END_MONTH               varchar2(35),
 TAX_ACTIVITY_CODE_XLATE varchar2(254),
 TAX_ACTIVITY_CODE_ID    number(22,0),
 UTILITY_ACCOUNT_XLATE   varchar2(254),
 UTILITY_ACCOUNT_ID      number(22,0),
 SUB_ACCOUNT_XLATE       varchar2(254),
 SUB_ACCOUNT_ID          number(22,0),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 BUS_SEGMENT_XLATE       varchar2(254),
 BUS_SEGMENT_ID          number(22,0),
 IN_SERVICE_YEAR         varchar2(35),
 GL_POSTING_MO_YR        varchar2(35),
 TAX_LOCATION_XLATE      varchar2(254),
 TAX_LOCATION_ID         number(22,0),
 CLASS_CODE_XLATE        varchar2(254),
 CLASS_CODE_ID           number(22,0),
 CC_TAX_INDICATOR        varchar2(35),
 CC_VALUE                varchar2(254),
 GL_ACCOUNT_XLATE        varchar2(254),
 GL_ACCOUNT_ID           number(22,0),
 BONUS_VALUE             varchar2(254),
 EXT_CLASS_CODE_VALUE    varchar2(254),
 MONTH_NUMBER            varchar2(35),
 TAX_DISTINCTION_XLATE   varchar2(254),
 TAX_DISTINCTION_ID      number(22,0),
 AMOUNT                  varchar2(35)
);

alter table PWRPLANT.TAX_IMPORT_RETS_ARC add constraint TAX_IMP_RETS_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_RETS_ARC add constraint TAX_IMP_RETS_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Create an import for Tax Book Transfers.
--
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 214, sysdate, user, 'Tax Transfers', 'Import Tax Transfers', 'tax_import_trans', 'tax_import_trans_arc', null, 0, 'nvo_tax_logic_import', '', 'Tax Transfers', '' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 214, 9, sysdate, user );

insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'trans_id', sysdate, user, 'Transaction ID', 'trans_xlate', 1, 1, 'number(22,0)', 'tax_book_transfers', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'year', sysdate, user, 'Year', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'start_month', sysdate, user, 'Start Month', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'end_month', sysdate, user, 'End Month', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'tax_activity_code_id', sysdate, user, 'Tax Activity Code', 'tax_activity_code_xlate', 0, 1, 'number(22,0)', 'tax_activity_code', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'utility_account_id', sysdate, user, 'Utility Account', 'utility_account_xlate', 0, 3, 'number(22,0)', 'utility_account', 'bus_segment_id', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'sub_account_id', sysdate, user, 'Sub Account', 'sub_account_xlate', 0, 4, 'number(22,0)', 'sub_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'bus_segment_id', sysdate, user, 'Business Segment', 'bus_segment_xlate', 0, 2, 'number(22,0)', 'business_segment', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'in_service_year', sysdate, user, 'In Service Year', '', 0, 1, 'date', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'gl_posting_mo_yr', sysdate, user, 'Gl Posting Mo Yr', '', 0, 1, 'date', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'tax_location_id', sysdate, user, 'Tax Location', 'tax_location_xlate', 0, 1, 'number(22,0)', 'tax_location', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'class_code_id', sysdate, user, 'Class Code', 'class_code_xlate', 0, 1, 'number(22,0)', 'class_code', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'cc_tax_indicator', sysdate, user, 'Class Code Tax Indicator', '', 0, 1, 'number(22,0)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'cc_value', sysdate, user, 'Class Code Value', '', 0, 1, 'varchar2(35)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'gl_account_id', sysdate, user, 'GL Account', 'gl_account_xlate', 0, 2, 'number(22,0)', 'gl_account', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'tax_distinction_id', sysdate, user, 'Tax Distinction', 'tax_distinction_xlate', 0, 1, 'number(22,0)', 'retire_unit_tax_distinction', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'amount', sysdate, user, 'Amount', '', 1, 1, 'number(22,2)', '', '', 1, null, '' );
insert into pwrplant.pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column ) values ( 214, 'book_sale_amount', sysdate, user, 'Book Sale Amount', '', 0, 1, 'number(22,2)', '', '', 1, null, '' );

insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'tax_activity_code_id', 2002, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'utility_account_id', 27, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'utility_account_id', 28, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'utility_account_id', 29, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'utility_account_id', 30, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'sub_account_id', 612, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'sub_account_id', 613, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'sub_account_id', 614, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'company_id', 19, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'company_id', 20, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'company_id', 21, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'bus_segment_id', 25, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'bus_segment_id', 26, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'bus_segment_id', 604, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'bus_segment_id', 605, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'tax_location_id', 145, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'class_code_id', 199, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'class_code_id', 200, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'gl_account_id', 22, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'gl_account_id', 23, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'gl_account_id', 24, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'gl_account_id', 601, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'gl_account_id', 602, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'gl_account_id', 603, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'tax_distinction_id', 2012, sysdate, user );
insert into pwrplant.pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 214, 'tax_distinction_id', 2013, sysdate, user );

create table PWRPLANT.TAX_IMPORT_TRANS
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TRANS_XLATE             varchar2(254),
 TRANS_ID                number(22,0),
 YEAR                    varchar2(35),
 START_MONTH             varchar2(35),
 END_MONTH               varchar2(35),
 TAX_ACTIVITY_CODE_XLATE varchar2(254),
 TAX_ACTIVITY_CODE_ID    number(22,0),
 UTILITY_ACCOUNT_XLATE   varchar2(254),
 UTILITY_ACCOUNT_ID      number(22,0),
 SUB_ACCOUNT_XLATE       varchar2(254),
 SUB_ACCOUNT_ID          number(22,0),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 BUS_SEGMENT_XLATE       varchar2(254),
 BUS_SEGMENT_ID          number(22,0),
 IN_SERVICE_YEAR         varchar2(35),
 GL_POSTING_MO_YR        varchar2(35),
 TAX_LOCATION_XLATE      varchar2(254),
 TAX_LOCATION_ID         number(22,0),
 CLASS_CODE_XLATE        varchar2(254),
 CLASS_CODE_ID           number(22,0),
 CC_TAX_INDICATOR        varchar2(35),
 CC_VALUE                varchar2(254),
 GL_ACCOUNT_XLATE        varchar2(254),
 GL_ACCOUNT_ID           number(22,0),
 TAX_DISTINCTION_XLATE   varchar2(254),
 TAX_DISTINCTION_ID      number(22,0),
 AMOUNT                  varchar2(35),
 BOOK_SALE_AMOUNT        varchar2(35),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table PWRPLANT.TAX_IMPORT_TRANS add constraint TAX_IMP_TRANS_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_TRANS add constraint TAX_IMP_TRANS_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.TAX_IMPORT_TRANS_ARC
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TRANS_XLATE             varchar2(254),
 TRANS_ID                number(22,0),
 YEAR                    varchar2(35),
 START_MONTH             varchar2(35),
 END_MONTH               varchar2(35),
 TAX_ACTIVITY_CODE_XLATE varchar2(254),
 TAX_ACTIVITY_CODE_ID    number(22,0),
 UTILITY_ACCOUNT_XLATE   varchar2(254),
 UTILITY_ACCOUNT_ID      number(22,0),
 SUB_ACCOUNT_XLATE       varchar2(254),
 SUB_ACCOUNT_ID          number(22,0),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 BUS_SEGMENT_XLATE       varchar2(254),
 BUS_SEGMENT_ID          number(22,0),
 IN_SERVICE_YEAR         varchar2(35),
 GL_POSTING_MO_YR        varchar2(35),
 TAX_LOCATION_XLATE      varchar2(254),
 TAX_LOCATION_ID         number(22,0),
 CLASS_CODE_XLATE        varchar2(254),
 CLASS_CODE_ID           number(22,0),
 CC_TAX_INDICATOR        varchar2(35),
 CC_VALUE                varchar2(254),
 GL_ACCOUNT_XLATE        varchar2(254),
 GL_ACCOUNT_ID           number(22,0),
 TAX_DISTINCTION_XLATE   varchar2(254),
 TAX_DISTINCTION_ID      number(22,0),
 AMOUNT                  varchar2(35),
 BOOK_SALE_AMOUNT        varchar2(35)
);

alter table PWRPLANT.TAX_IMPORT_TRANS_ARC add constraint TAX_IMP_TRANS_ARC_PK primary key ( IMPORT_RUN_ID, LINE_ID ) using index tablespace PWRPLANT_IDX;
alter table PWRPLANT.TAX_IMPORT_TRANS_ARC add constraint TAX_IMP_TRANS_ARC_RUN_FK foreign key ( IMPORT_RUN_ID ) references PWRPLANT.PP_IMPORT_RUN;

--
-- Change the Import Tool to be under Admin on the menu.
--
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'depr_input_import';
update pwrplant.ppbase_menu_items set item_order = 2 where module = 'powertax' and menu_identifier = 'admin_systemoptions';

insert into pwrplant.ppbase_workspace ( module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp ) values ( 'powertax', 'admin_import_tool', sysdate, user, 'Import Tool', 'uo_tax_admin_wksp_import', 'Import Tool' );
insert into pwrplant.ppbase_menu_items ( module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn ) values ( 'powertax', 'admin_import_tool', sysdate, user, 2, 1, 'Import Tool', '', 'admin', 'admin_import_tool', 1 );

--
-- Add a short description column to the yes_no table to prevent errors when exporting valid values.
--
alter table pwrplant.yes_no add short_description varchar2(1);
update pwrplant.yes_no set short_description = 'N' where yes_no_id = 0;
update pwrplant.yes_no set short_description = 'Y' where yes_no_id = 1;

--
-- Set up defaulting for imports.
--
alter table pwrplant.pp_import_column add default_value varchar2(100);

update pwrplant.pp_import_column set default_value = '0' where import_type_id = 201 and column_name = 'accum_reserve_adjust';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 201 and column_name = 'book_balance_adjust';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 201 and column_name = 'cap_gain_loss_adjust';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 201 and column_name = 'depreciable_base_adjust';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 201 and column_name = 'depreciation_adjust';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 201 and column_name = 'gain_loss_adjust';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 202 and column_name = 'tax_summary_id';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 205 and column_name = 'calced';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 205 and column_name = 'default_tax_include_id';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 205 and column_name = 'depr_deduction';
update pwrplant.pp_import_column set default_value = '12' where import_type_id = 206 and column_name = 'end_month';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 206 and column_name = 'ext_class_code_value';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 206 and column_name = 'start_month';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 207 and column_name = 'class_code_id';
update pwrplant.pp_import_column set default_value = '2100' where import_type_id = 207 and column_name = 'end_eff_book_vintage';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 207 and column_name = 'gl_account_id';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 207 and column_name = 'monthly_adds';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 207 and column_name = 'quarterly_adds';
update pwrplant.pp_import_column set default_value = '1800' where import_type_id = 207 and column_name = 'start_eff_book_vintage';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 207 and column_name = 'tax_distinction_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 207 and column_name = 'tax_location_id';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 208 and column_name = 'basis_diff_activity_split';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 208 and column_name = 'basis_diff_retire_reversal';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 208 and column_name = 'cap_depr_ind';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 208 and column_name = 'no_zero_check';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 211 and column_name = 'tolerance';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_1';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_10';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_11';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_12';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_13';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_14';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_15';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_16';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_17';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_18';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_19';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_2';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_20';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_21';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_22';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_23';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_24';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_25';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_26';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_27';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_28';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_29';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_3';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_30';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_31';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_32';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_33';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_34';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_35';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_36';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_37';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_38';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_39';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_4';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_40';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_41';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_42';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_43';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_44';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_45';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_46';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_47';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_48';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_49';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_5';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_50';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_51';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_52';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_53';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_54';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_55';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_56';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_57';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_58';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_59';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_6';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_60';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_61';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_62';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_63';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_64';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_65';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_66';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_67';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_68';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_69';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_7';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_70';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_8';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'basis_9';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 212 and column_name = 'cc_tax_indicator';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'class_code_id';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 212 and column_name = 'gl_account_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 212 and column_name = 'tax_distinction_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 212 and column_name = 'tax_load_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 212 and column_name = 'tax_location_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 213 and column_name = 'cc_tax_indicator';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 213 and column_name = 'class_code_id';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 213 and column_name = 'gl_account_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 213 and column_name = 'tax_distinction_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 213 and column_name = 'tax_location_id';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 214 and column_name = 'book_sale_amount';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 214 and column_name = 'cc_tax_indicator';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 214 and column_name = 'class_code_id';
update pwrplant.pp_import_column set default_value = '0' where import_type_id = 214 and column_name = 'gl_account_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 214 and column_name = 'tax_distinction_id';
update pwrplant.pp_import_column set default_value = '1' where import_type_id = 214 and column_name = 'tax_location_id';
update pwrplant.pp_import_column set default_value = ' ' where import_type_id = 207 and column_name = 'class_code_value';
update pwrplant.pp_import_column set default_value = ' ' where import_type_id = 212 and column_name = 'cc_value';
update pwrplant.pp_import_column set default_value = ' ' where import_type_id = 212 and column_name = 'work_order_number';
update pwrplant.pp_import_column set default_value = ' ' where import_type_id = 213 and column_name = 'cc_value';
update pwrplant.pp_import_column set default_value = ' ' where import_type_id = 214 and column_name = 'cc_value';

--
-- Add table and column comments.
--
comment on table tax_import_depr_adjust is '(O and C) [09] The Tax Import Depr Adjust table is a staging table into which data from a run of the Depreciation Adjustments Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Depr Adjust Archive table.';
comment on table tax_import_basis_amounts is '(O and C) [09] The Tax Import Basis Amounts table is a staging table into which data from a run of the Book Activity Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Basis Amounts Archive table.';
comment on table tax_import_book_reconcile is '(O and C) [09] The Tax Import Book Reconcile table is a staging table into which data from a run of the Book-to-Tax Basis Differences Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Book Reconcile Archive table.';
comment on table tax_import_tax_class is '(O and C) [09] The Tax Import Tax Class table is a staging table into which data from a run of the Tax Class Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Tax Class Archive table.';
comment on table tax_import_rec_item is '(O and C) [09] The Tax Import Rec Item table is a staging table into which data from a run of the Tax Reconcile Item Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Rec Item Archive table.';
comment on table tax_import_vintage is '(O and C) [09] The Tax Import Vintage table is a staging table into which data from a run of the Vintage Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Vintage Archive table.';
comment on table tax_import_bk_translate is '(O and C) [09] The Tax Import Bk Translate table is a staging table into which data from a run of the Tax Book Translate Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Bk Translate Archive table.';
comment on table tax_import_norm_schema is '(O and C) [09] The Tax Import Norm Schema table is a staging table into which data from a run of the Normalization Schema Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Norm Schema Archive table.';
comment on table tax_import_depr_schema is '(O and C) [09] The Tax Import Depr Schema table is a staging table into which data from a run of the Tax Depr Schema Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Depr Schema Archive table.';
comment on table tax_import_depr_sch_ctrl is '(O and C) [09] The Tax Import Depr Sch Ctrl table is a staging table into which data from a run of the Tax Depr Schema Control Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Depr Sch Ctrl Archive table.';
comment on table tax_import_retire_rules is '(O and C) [09] The Tax Import Retire Rules table is a staging table into which data from a run of the Tax Retirement/Transfer Rules Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Retire Rules Archive table.';
comment on table tax_import_adds is '(O and C) [09] The Tax Import Adds table is a staging table into which data from a run of the Tax Additions Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Adds Archive table.';
comment on table tax_import_rets is '(O and C) [09] The Tax Import Rets table is a staging table into which data from a run of the Tax Retirements Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Rets Archive table.';
comment on table tax_import_trans is '(O and C) [09] The Tax Import Trans table is a staging table into which data from a run of the Tax Transfers Import process is loaded and, then, processed.  Once the import process completes successfully, the data for a successful run is moved to the Tax Import Trans Archive table.';

comment on table tax_import_depr_adjust_arc is '(O and C) [09] The Tax Import Depr Adjust Archive table maintains an archive of all successful runs of the Depreciation Adjustments Import process.';
comment on table tax_import_basis_amounts_arc is '(O and C) [09] The Tax Import Basis Amounts Archive table maintains an archive of all successful runs of the Book Activity Import process.';
comment on table tax_import_book_reconcile_arc is '(O and C) [09] The Tax Import Book Reconcile Archive table maintains an archive of all successful runs of the Book-to-Tax Basis Differences Import process.';
comment on table tax_import_tax_class_arc is '(O and C) [09] The Tax Import Tax Class Archive table maintains an archive of all successful runs of the Tax Class Import process.';
comment on table tax_import_rec_item_arc is '(O and C) [09] The Tax Import Rec Item Archive table maintains an archive of all successful runs of the Tax Reconcile Item Import process.';
comment on table tax_import_vintage_arc is '(O and C) [09] The Tax Import Vintage Archive table maintains an archive of all successful runs of the Vintage Import process.';
comment on table tax_import_bk_translate_arc is '(O and C) [09] The Tax Import Bk Translate Archive table maintains an archive of all successful runs of the Tax Book Translate Import process.';
comment on table tax_import_norm_schema_arc is '(O and C) [09] The Tax Import Norm Schema Archive table maintains an archive of all successful runs of the Normalization Schema Import process.';
comment on table tax_import_depr_schema_arc is '(O and C) [09] The Tax Import Depr Schema Archive table maintains an archive of all successful runs of the Tax Depr Schema Import process.';
comment on table tax_import_depr_sch_ctrl_arc is '(O and C) [09] The Tax Import Depr Sch Ctrl Archive table maintains an archive of all successful runs of the Tax Depr Schema Control Import process.';
comment on table tax_import_retire_rules_arc is '(O and C) [09] The Tax Import Retire Rules Archive table maintains an archive of all successful runs of the Tax Retirement/Transfer Rules Import process.';
comment on table tax_import_adds_arc is '(O and C) [09] The Tax Import Adds Archive table maintains an archive of all successful runs of the Tax Additions Import process.';
comment on table tax_import_rets_arc is '(O and C) [09] The Tax Import Rets Archive table maintains an archive of all successful runs of the Tax Retirements Import process.';
comment on table tax_import_trans_arc is '(O and C) [09] The Tax Import Trans Archive table maintains an archive of all successful runs of the Tax Transfers Import process.';

comment on column tax_import_depr_adjust.accum_reserve_adjust is 'User-input dollar amount adjustment to accumulated tax depreciation reserve balance of the tax asset.';
comment on column tax_import_depr_adjust.book_balance_adjust is 'User-input dollar amount adjustment to book balance of the tax asset.';
comment on column tax_import_depr_adjust.cap_gain_loss_adjust is 'User-input dollar amount adjustment to capital gain/loss of the tax asset.';
comment on column tax_import_depr_adjust.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_depr_adjust.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_depr_adjust.depreciable_base_adjust is 'User-input dollar amount adjustment to depreciable base of the tax asset.';
comment on column tax_import_depr_adjust.depreciation_adjust is 'User-input dollar amount adjustment to current year depreciation expense for the tax asset.';
comment on column tax_import_depr_adjust.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_depr_adjust.gain_loss_adjust is 'User-input dollar amount adjustment to ordinary gain/loss.';
comment on column tax_import_depr_adjust.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_depr_adjust.in_service_month is 'Month/year field that is optional in the tax record key.  It would be used for certain depreciation methods (such as for buildings), where the in-service month determines the depreciation rate for each tax year.';
comment on column tax_import_depr_adjust.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_depr_adjust.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_depr_adjust.tax_book_id is 'System-assigned identifier of a particular Tax Book.';
comment on column tax_import_depr_adjust.tax_book_xlate is 'Value from the import file translated to obtain the tax_book_id.';
comment on column tax_import_depr_adjust.tax_class_id is 'System-assigned identifier of a particular Tax Class.';
comment on column tax_import_depr_adjust.tax_class_xlate is 'Value from the import file translated to obtain the tax_class_id.';
comment on column tax_import_depr_adjust.tax_record_id is 'System-assigned identifier of a particular Tax Record.';
comment on column tax_import_depr_adjust.tax_record_xlate is 'Value from the import file translated to obtain the tax_record_id.';
comment on column tax_import_depr_adjust.tax_year is 'Calendar tax year associated with the tax record''s depreciation calculation and stored balances.  The fraction is a sequential number for dealing with short tax years.';
comment on column tax_import_depr_adjust.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_depr_adjust.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_depr_adjust.version_id is 'System-assigned identifier of a PowerTax version.';
comment on column tax_import_depr_adjust.vintage_id is 'System-assigned identifier of a particular Vintage.';
comment on column tax_import_depr_adjust.vintage_xlate is 'Value from the import file translated to obtain the vintage_id.';
comment on column tax_import_basis_amounts.amount is 'The book amount of the transaction in dollars.';
comment on column tax_import_basis_amounts.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_basis_amounts.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_basis_amounts.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_basis_amounts.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_basis_amounts.in_service_month is 'Month/year field that is optional in the tax record key.  It would be used for certain depreciation methods (such as for buildings), where the in-service month determines the depreciation rate for each tax year.';
comment on column tax_import_basis_amounts.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_basis_amounts.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_basis_amounts.tax_activity_code_id is 'System-assigned identifier of a particular Tax Activity Code.';
comment on column tax_import_basis_amounts.tax_activity_code_xlate is 'Value from the import file translated to obtain the tax_activity_code_id.';
comment on column tax_import_basis_amounts.tax_class_id is 'System-assigned identifier of a particular Tax Class.';
comment on column tax_import_basis_amounts.tax_class_xlate is 'Value from the import file translated to obtain the tax_class_id.';
comment on column tax_import_basis_amounts.tax_include_id is 'System-assigned identifier of a particular Tax Include.';
comment on column tax_import_basis_amounts.tax_include_xlate is 'Value from the import file translated to obtain the tax_include_id.';
comment on column tax_import_basis_amounts.tax_record_id is 'System-assigned identifier of a particular Tax Record.';
comment on column tax_import_basis_amounts.tax_record_xlate is 'Value from the import file translated to obtain the tax_record_id.';
comment on column tax_import_basis_amounts.tax_summary_id is 'System-assigned identifier of a particular Tax Summary.';
comment on column tax_import_basis_amounts.tax_summary_xlate is 'Value from the import file translated to obtain the tax_summary_id.';
comment on column tax_import_basis_amounts.tax_year is 'Calendar tax year associated with the tax record''s depreciation calculation and stored balances.  The fraction is a sequential number for dealing with short tax years.';
comment on column tax_import_basis_amounts.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_basis_amounts.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_basis_amounts.version_id is 'System-assigned identifier of a PowerTax version.';
comment on column tax_import_basis_amounts.vintage_id is 'System-assigned identifier of a particular Vintage.';
comment on column tax_import_basis_amounts.vintage_xlate is 'Value from the import file translated to obtain the vintage_id.';
comment on column tax_import_book_reconcile.basis_amount_activity is 'Changes in book-to-tax basis differences in dollars, generally for additions since this is used as an originating difference for deferred taxes.  Note that it does not reconcile basis_amount_beg with basis_amount_end, since pro-rated retirement differences are excluded as are input amounts for retirements in basis_amount_input_retire.';
comment on column tax_import_book_reconcile.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_book_reconcile.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_book_reconcile.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_book_reconcile.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_book_reconcile.in_service_month is 'Month/year field that is optional in the tax record key.  It would be used for certain depreciation methods (such as for buildings), where the in-service month determines the depreciation rate for each tax year.';
comment on column tax_import_book_reconcile.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_book_reconcile.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_book_reconcile.reconcile_item_id is 'System-assigned identifier of a particular Tax Reconcile Item.';
comment on column tax_import_book_reconcile.reconcile_item_xlate is 'Value from the import file translated to obtain the reconcile_item_id.';
comment on column tax_import_book_reconcile.tax_class_id is 'System-assigned identifier of a particular Tax Class.';
comment on column tax_import_book_reconcile.tax_class_xlate is 'Value from the import file translated to obtain the tax_class_id.';
comment on column tax_import_book_reconcile.tax_include_id is 'System-assigned identifier of a particular Tax Include.';
comment on column tax_import_book_reconcile.tax_include_xlate is 'Value from the import file translated to obtain the tax_include_id.';
comment on column tax_import_book_reconcile.tax_record_id is 'System-assigned identifier of a particular Tax Record.';
comment on column tax_import_book_reconcile.tax_record_xlate is 'Value from the import file translated to obtain the tax_record_id.';
comment on column tax_import_book_reconcile.tax_year is 'Calendar tax year associated with the tax record''s depreciation calculation and stored balances.  The fraction is a sequential number for dealing with short tax years.';
comment on column tax_import_book_reconcile.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_book_reconcile.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_book_reconcile.version_id is 'System-assigned identifier of a PowerTax version.';
comment on column tax_import_book_reconcile.vintage_id is 'System-assigned identifier of a particular Vintage.';
comment on column tax_import_book_reconcile.vintage_xlate is 'Value from the import file translated to obtain the vintage_id.';
comment on column tax_import_tax_class.description is 'Description is the user-assigned name associated with each tax class.';
comment on column tax_import_tax_class.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_tax_class.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_tax_class.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_tax_class.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_tax_class.oper_ind is 'System-assigned identifier of a particular Oper Ind.';
comment on column tax_import_tax_class.oper_ind_xlate is 'Value from the import file translated to obtain the oper_ind.';
comment on column tax_import_tax_class.tax_class_id is 'System-assigned identifier of a particular Tax Class.';
comment on column tax_import_tax_class.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_tax_class.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_rec_item.calced is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_rec_item.calced_xlate is 'Value from the import file translated to obtain the calced.';
comment on column tax_import_rec_item.default_tax_include_id is 'System-assigned identifier of a particular Tax Include.';
comment on column tax_import_rec_item.default_tax_include_xlate is 'Value from the import file translated to obtain the default_tax_include_id.';
comment on column tax_import_rec_item.depr_deduction is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_rec_item.depr_deduction_xlate is 'Value from the import file translated to obtain the depr_deduction.';
comment on column tax_import_rec_item.description is 'Description is the user-assigned name associated with each tax reconcile item.';
comment on column tax_import_rec_item.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_rec_item.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_rec_item.input_retire_ind is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_rec_item.input_retire_ind_xlate is 'Value from the import file translated to obtain the input_retire_ind.';
comment on column tax_import_rec_item.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_rec_item.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_rec_item.net_basis_rpt is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_rec_item.net_basis_rpt_xlate is 'Value from the import file translated to obtain the net_basis_rpt.';
comment on column tax_import_rec_item.reconcile_item_id is 'System-assigned identifier of a particular Tax Reconcile Item.';
comment on column tax_import_rec_item.tax_type_of_property_id is 'System-assigned identifier of a particular Type of Property.';
comment on column tax_import_rec_item.tax_type_of_property_xlate is 'Value from the import file translated to obtain the tax_type_of_property_id.';
comment on column tax_import_rec_item.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_rec_item.type is 'Book (if it is a book item in the sense of being eligible for an allocation of book depreciation and is part of the financial set of books, e.g., AFUDC), Tax (not a part of the financial set of books, e.g., CIAC, CPI, Repair Allowance), or Credit (applies to Bonus Depreciation or Intangible Drilling Costs, where the item is actually a special first year depreciation; when the asset is retired, this item is added back to basis to determine if the salvage is in excess of basis for the determination of capital gain).  If a book item, the sign is reversed from the one on the CPR basis (book summary).  If a tax type, the same sign on the CPR is used on the adjustment.';
comment on column tax_import_rec_item.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_vintage.bonus_end_month is 'End month of the bonus period in the vintage.';
comment on column tax_import_vintage.bonus_start_month is 'Start month of the bonus period in the vintage.';
comment on column tax_import_vintage.description is 'Description is the user-assigned name associated with each vintage.';
comment on column tax_import_vintage.end_month is 'End month of the vintage in month number format YYYYMM.';
comment on column tax_import_vintage.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_vintage.ext_class_code_value is 'The external class code field for the bonus class code.';
comment on column tax_import_vintage.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_vintage.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_vintage.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_vintage.start_month is 'Start month of the vintage in month number format YYYYMM.';
comment on column tax_import_vintage.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_vintage.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_vintage.vintage_convention_id is 'System-assigned identifier of a particular Vintage Convention.';
comment on column tax_import_vintage.vintage_convention_xlate is 'Value from the import file translated to obtain the vintage_convention_id.';
comment on column tax_import_vintage.vintage_id is 'System-assigned identifier of a particular Vintage.';
comment on column tax_import_vintage.year is 'Year associated with the vintage, e.g., ''1994''.';
comment on column tax_import_bk_translate.active is 'A value of ''1'' indicates that this is the current translate to be used for additions for this book key.  Each book key (which can translate to multiple classes) should have one and only one active or ''1'' indicator.';
comment on column tax_import_bk_translate.bus_segment_id is 'System-assigned identifier of a particular Business Segment.';
comment on column tax_import_bk_translate.bus_segment_xlate is 'Value from the import file translated to obtain the bus_segment_id.';
comment on column tax_import_bk_translate.class_code_id is 'System-assigned identifier of a particular Class Code.';
comment on column tax_import_bk_translate.class_code_value is 'Value for the classification code.';
comment on column tax_import_bk_translate.class_code_xlate is 'Value from the import file translated to obtain the class_code_id.';
comment on column tax_import_bk_translate.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_bk_translate.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_bk_translate.end_eff_book_vintage is 'Four digit year giving the ending (inclusive) book vintage year for which the translate is valid.  (This can be a future year.)';
comment on column tax_import_bk_translate.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_bk_translate.gl_account_id is 'System-assigned identifier of a particular GL Account.';
comment on column tax_import_bk_translate.gl_account_xlate is 'Value from the import file translated to obtain the gl_account_id.';
comment on column tax_import_bk_translate.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_bk_translate.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_bk_translate.life is 'Average life in years.  Used only when retirements are not vintaged coming into PowerTax.';
comment on column tax_import_bk_translate.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_bk_translate.monthly_adds is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_bk_translate.monthly_adds_xlate is 'Value from the import file translated to obtain the monthly_adds.';
comment on column tax_import_bk_translate.mortality_curve_id is 'System-assigned identifier of a particular Mortality Curve.';
comment on column tax_import_bk_translate.mortality_curve_xlate is 'Value from the import file translated to obtain the mortality_curve_id.';
comment on column tax_import_bk_translate.quarterly_adds is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_bk_translate.quarterly_adds_xlate is 'Value from the import file translated to obtain the quarterly_adds.';
comment on column tax_import_bk_translate.start_eff_book_vintage is 'Four digit years giving the beginning (inclusive) book vintage year for which the tax book translate is valid.  (This can be before any data exists.)';
comment on column tax_import_bk_translate.sub_account_id is 'System-assigned identifier of a particular Sub Account.';
comment on column tax_import_bk_translate.sub_account_xlate is 'Value from the import file translated to obtain the sub_account_id.';
comment on column tax_import_bk_translate.tax_book_translate_id is 'System-assigned identifier of a particular Tax Book Translate ID.';
comment on column tax_import_bk_translate.tax_book_translate_xlate is 'Value from the import file translated to obtain the tax_book_translate_id.';
comment on column tax_import_bk_translate.tax_class_id is 'System-assigned identifier of a particular Tax Class.';
comment on column tax_import_bk_translate.tax_class_xlate is 'Value from the import file translated to obtain the tax_class_id.';
comment on column tax_import_bk_translate.tax_depr_schema_id is 'System-assigned identifier of a particular Tax Depr Schema.';
comment on column tax_import_bk_translate.tax_depr_schema_xlate is 'Value from the import file translated to obtain the tax_depr_schema_id.';
comment on column tax_import_bk_translate.tax_distinction_id is 'System-assigned identifier of a particular Tax Distinction.';
comment on column tax_import_bk_translate.tax_distinction_xlate is 'Value from the import file translated to obtain the tax_distinction_id.';
comment on column tax_import_bk_translate.tax_location_id is 'System-assigned identifier of a particular Tax Location.';
comment on column tax_import_bk_translate.tax_location_xlate is 'Value from the import file translated to obtain the tax_location_id.';
comment on column tax_import_bk_translate.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_bk_translate.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_bk_translate.utility_account_id is 'System-assigned identifier of a particular Utility Account.';
comment on column tax_import_bk_translate.utility_account_xlate is 'Value from the import file translated to obtain the utility_account_id.';
comment on column tax_import_norm_schema.amortization_type_id is 'System-assigned identifier of a particular Amortization Type.';
comment on column tax_import_norm_schema.amortization_type_xlate is 'Value from the import file translated to obtain the amortization_type_id.';
comment on column tax_import_norm_schema.basis_diff_act_split_xlate is 'Value from the import file translated to obtain the basis_diff_activity_split.';
comment on column tax_import_norm_schema.basis_diff_activity_split is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_norm_schema.basis_diff_ret_reversal_xlate is 'Value from the import file translated to obtain the basis_diff_retire_reversal.';
comment on column tax_import_norm_schema.basis_diff_retire_reversal is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_norm_schema.book_depr_alloc_ind is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_norm_schema.book_depr_alloc_xlate is 'Value from the import file translated to obtain the book_depr_alloc_ind.';
comment on column tax_import_norm_schema.cap_depr_ind is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_norm_schema.cap_depr_xlate is 'Value from the import file translated to obtain the cap_depr_ind.';
comment on column tax_import_norm_schema.def_income_tax_rate_id is 'System-assigned identifier of a particular Deferred Tax Rate.';
comment on column tax_import_norm_schema.def_income_tax_rate_xlate is 'Value from the import file translated to obtain the def_income_tax_rate_id.';
comment on column tax_import_norm_schema.description is 'Records a short description of the deferred tax difference, e.g., ''Method'', ''Method Life'', ''AFUDC'', etc.';
comment on column tax_import_norm_schema.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_norm_schema.gl_account_id is 'System-assigned identifier of a particular GL Account.';
comment on column tax_import_norm_schema.gl_account_xlate is 'Value from the import file translated to obtain the gl_account_id.';
comment on column tax_import_norm_schema.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_norm_schema.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_norm_schema.jurisdiction_id is 'System-assigned identifier of a particular Jurisdiction.';
comment on column tax_import_norm_schema.jurisdiction_xlate is 'Value from the import file translated to obtain the jurisdiction_id.';
comment on column tax_import_norm_schema.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_norm_schema.no_zero_check is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_norm_schema.no_zero_check_xlate is 'Value from the import file translated to obtain the no_zero_check.';
comment on column tax_import_norm_schema.norm_from_tax is 'System-assigned identifier of a particular Tax Book.';
comment on column tax_import_norm_schema.norm_from_tax_xlate is 'Value from the import file translated to obtain the norm_from_tax.';
comment on column tax_import_norm_schema.norm_to_tax is 'System-assigned identifier of a particular Tax Book.';
comment on column tax_import_norm_schema.norm_to_tax_xlate is 'Value from the import file translated to obtain the norm_to_tax.';
comment on column tax_import_norm_schema.norm_type_id is 'System-assigned identifier of a particular Norm Type.';
comment on column tax_import_norm_schema.norm_type_xlate is 'Value from the import file translated to obtain the norm_type_id.';
comment on column tax_import_norm_schema.normalization_id is 'System-assigned identifier of a particular Normalization Schema.';
comment on column tax_import_norm_schema.reconcile_item_id is 'System-assigned identifier of a particular Tax Reconcile Item.';
comment on column tax_import_norm_schema.reconcile_item_xlate is 'Value from the import file translated to obtain the reconcile_item_id.';
comment on column tax_import_norm_schema.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_norm_schema.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_depr_schema.description is 'Description of a tax depreciation schema.';
comment on column tax_import_depr_schema.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_depr_schema.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_depr_schema.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_depr_schema.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_depr_schema.tax_depr_schema_id is 'Value from the import file translated to obtain the .';
comment on column tax_import_depr_schema.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_depr_schema.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_depr_sch_ctrl.convention_id is 'System-assigned identifier of a particular Convention.';
comment on column tax_import_depr_sch_ctrl.convention_xlate is 'Value from the import file translated to obtain the convention_id.';
comment on column tax_import_depr_sch_ctrl.def_gain_rec_item_xlate is 'Value from the import file translated to obtain the def_gain_reconcile_item_id.';
comment on column tax_import_depr_sch_ctrl.def_gain_reconcile_item_id is 'System-assigned identifier of a particular Tax Reconcile Item.';
comment on column tax_import_depr_sch_ctrl.def_gain_tax_inc_xlate is 'Value from the import file translated to obtain the def_gain_tax_include_id.';
comment on column tax_import_depr_sch_ctrl.def_gain_tax_include_id is 'System-assigned identifier of a particular Tax Include.';
comment on column tax_import_depr_sch_ctrl.deferred_tax_schema_id is 'System-assigned identifier of a particular Deferred Tax Schema.';
comment on column tax_import_depr_sch_ctrl.deferred_tax_schema_xlate is 'Value from the import file translated to obtain the deferred_tax_schema_id.';
comment on column tax_import_depr_sch_ctrl.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_depr_sch_ctrl.ext_class_code_value is 'System-assigned identifier of the class code used for bonus depreciation: 0 = blanket, 1 = Non Qualifying, 2 = 30%, 3 = 50%.';
comment on column tax_import_depr_sch_ctrl.extraordinary_conv_xlate is 'Value from the import file translated to obtain the extraordinary_convention.';
comment on column tax_import_depr_sch_ctrl.extraordinary_convention is 'System-assigned identifier of a particular Convention.';
comment on column tax_import_depr_sch_ctrl.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_depr_sch_ctrl.in_service_month is 'Optional column used for certain depreciation methods (such as for buildings), where the in-service month determines the depreciation rate for each tax year.';
comment on column tax_import_depr_sch_ctrl.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_depr_sch_ctrl.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_depr_sch_ctrl.recovery_period_id is 'System-assigned identifier of a particular Recovery Period.';
comment on column tax_import_depr_sch_ctrl.recovery_period_xlate is 'Value from the import file translated to obtain the recovery_period_id.';
comment on column tax_import_depr_sch_ctrl.summary_4562_id is 'System-assigned identifier of a particular Summary 4562.';
comment on column tax_import_depr_sch_ctrl.summary_4562_xlate is 'Value from the import file translated to obtain the summary_4562_id.';
comment on column tax_import_depr_sch_ctrl.tax_book_id is 'System-assigned identifier of a particular Tax Book.';
comment on column tax_import_depr_sch_ctrl.tax_book_xlate is 'Value from the import file translated to obtain the tax_book_id.';
comment on column tax_import_depr_sch_ctrl.tax_credit_id is 'System-assigned identifier of a particular Tax Credit.';
comment on column tax_import_depr_sch_ctrl.tax_credit_xlate is 'Value from the import file translated to obtain the tax_credit_id.';
comment on column tax_import_depr_sch_ctrl.tax_depr_schema_control_id is 'Value from the import file translated to obtain the .';
comment on column tax_import_depr_sch_ctrl.tax_depr_schema_id is 'System-assigned identifier of a particular Tax Depr Schema.';
comment on column tax_import_depr_sch_ctrl.tax_depr_schema_xlate is 'Value from the import file translated to obtain the tax_depr_schema_id.';
comment on column tax_import_depr_sch_ctrl.tax_limit_id is 'System-assigned identifier of a particular Tax Limit.';
comment on column tax_import_depr_sch_ctrl.tax_limit_xlate is 'Value from the import file translated to obtain the tax_limit_id.';
comment on column tax_import_depr_sch_ctrl.tax_rate_id is 'System-assigned identifier of a particular Tax Rate.';
comment on column tax_import_depr_sch_ctrl.tax_rate_xlate is 'Value from the import file translated to obtain the tax_rate_id.';
comment on column tax_import_depr_sch_ctrl.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_depr_sch_ctrl.type_of_property_id is 'System-assigned identifier of a particular Type of Property.';
comment on column tax_import_depr_sch_ctrl.type_of_property_xlate is 'Value from the import file translated to obtain the type_of_property_id.';
comment on column tax_import_depr_sch_ctrl.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_depr_sch_ctrl.vintage_convention_id is 'System-assigned identifier of a particular Vintage Convention.';
comment on column tax_import_depr_sch_ctrl.vintage_convention_xlate is 'Value from the import file translated to obtain the vintage_convention_id.';
comment on column tax_import_retire_rules.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_retire_rules.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_retire_rules.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_retire_rules.fformat_string is 'User filter for the book retirements.';
comment on column tax_import_retire_rules.fifo is 'System-assigned identifier of a particular Yes/No.';
comment on column tax_import_retire_rules.fifo_xlate is 'Value from the import file translated to obtain the fifo.';
comment on column tax_import_retire_rules.gformat_string is 'User filter for the tax class/vintage.';
comment on column tax_import_retire_rules.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_retire_rules.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_retire_rules.iteration is 'Sequence in the retirement processing iteration.';
comment on column tax_import_retire_rules.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_retire_rules.min_vintage is 'Earliest vintage to which to apply a retirement, e.g. 1953.';
comment on column tax_import_retire_rules.sort_order is 'Defaults to largest dollar vintage first.';
comment on column tax_import_retire_rules.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_retire_rules.tolerance is 'Dollar amount of tolerance of retiring below a zero balance.';
comment on column tax_import_retire_rules.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_retire_rules.vintage_back is 'Number of vintages before the book vintage to allow the dollars to roll back.';
comment on column tax_import_retire_rules.vintage_forward is 'Number of vintages after the book vintage to allow the dollars to roll forward.';
comment on column tax_import_adds.amount is 'Book dollar amount of the addition.';
comment on column tax_import_adds.basis_1 is 'Dollar amount of basis 1 (defined in book summary).';
comment on column tax_import_adds.basis_10 is 'Dollar amount of basis 10 (defined in book summary).';
comment on column tax_import_adds.basis_11 is 'Dollar amount of basis 11 (defined in book summary).';
comment on column tax_import_adds.basis_12 is 'Dollar amount of basis 12 (defined in book summary).';
comment on column tax_import_adds.basis_13 is 'Dollar amount of basis 13 (defined in book summary).';
comment on column tax_import_adds.basis_14 is 'Dollar amount of basis 14 (defined in book summary).';
comment on column tax_import_adds.basis_15 is 'Dollar amount of basis 15 (defined in book summary).';
comment on column tax_import_adds.basis_16 is 'Dollar amount of basis 16 (defined in book summary).';
comment on column tax_import_adds.basis_17 is 'Dollar amount of basis 17 (defined in book summary).';
comment on column tax_import_adds.basis_18 is 'Dollar amount of basis 18 (defined in book summary).';
comment on column tax_import_adds.basis_19 is 'Dollar amount of basis 19 (defined in book summary).';
comment on column tax_import_adds.basis_2 is 'Dollar amount of basis 2 (defined in book summary).';
comment on column tax_import_adds.basis_20 is 'Dollar amount of basis 20 (defined in book summary).';
comment on column tax_import_adds.basis_21 is 'Dollar amount of basis 21 (defined in book summary).';
comment on column tax_import_adds.basis_22 is 'Dollar amount of basis 22 (defined in book summary).';
comment on column tax_import_adds.basis_23 is 'Dollar amount of basis 23 (defined in book summary).';
comment on column tax_import_adds.basis_24 is 'Dollar amount of basis 24 (defined in book summary).';
comment on column tax_import_adds.basis_25 is 'Dollar amount of basis 25 (defined in book summary).';
comment on column tax_import_adds.basis_26 is 'Dollar amount of basis 26 (defined in book summary).';
comment on column tax_import_adds.basis_27 is 'Dollar amount of basis 27 (defined in book summary).';
comment on column tax_import_adds.basis_28 is 'Dollar amount of basis 28 (defined in book summary).';
comment on column tax_import_adds.basis_29 is 'Dollar amount of basis 29 (defined in book summary).';
comment on column tax_import_adds.basis_3 is 'Dollar amount of basis 3 (defined in book summary).';
comment on column tax_import_adds.basis_30 is 'Dollar amount of basis 30 (defined in book summary).';
comment on column tax_import_adds.basis_31 is 'Dollar amount of basis 31 (defined in book summary).';
comment on column tax_import_adds.basis_32 is 'Dollar amount of basis 32 (defined in book summary).';
comment on column tax_import_adds.basis_33 is 'Dollar amount of basis 33 (defined in book summary).';
comment on column tax_import_adds.basis_34 is 'Dollar amount of basis 34 (defined in book summary).';
comment on column tax_import_adds.basis_35 is 'Dollar amount of basis 35 (defined in book summary).';
comment on column tax_import_adds.basis_36 is 'Dollar amount of basis 36 (defined in book summary).';
comment on column tax_import_adds.basis_37 is 'Dollar amount of basis 37 (defined in book summary).';
comment on column tax_import_adds.basis_38 is 'Dollar amount of basis 38 (defined in book summary).';
comment on column tax_import_adds.basis_39 is 'Dollar amount of basis 39 (defined in book summary).';
comment on column tax_import_adds.basis_4 is 'Dollar amount of basis 4 (defined in book summary).';
comment on column tax_import_adds.basis_40 is 'Dollar amount of basis 40 (defined in book summary).';
comment on column tax_import_adds.basis_41 is 'Dollar amount of basis 41 (defined in book summary).';
comment on column tax_import_adds.basis_42 is 'Dollar amount of basis 42 (defined in book summary).';
comment on column tax_import_adds.basis_43 is 'Dollar amount of basis 43 (defined in book summary).';
comment on column tax_import_adds.basis_44 is 'Dollar amount of basis 44 (defined in book summary).';
comment on column tax_import_adds.basis_45 is 'Dollar amount of basis 45 (defined in book summary).';
comment on column tax_import_adds.basis_46 is 'Dollar amount of basis 46 (defined in book summary).';
comment on column tax_import_adds.basis_47 is 'Dollar amount of basis 47 (defined in book summary).';
comment on column tax_import_adds.basis_48 is 'Dollar amount of basis 48 (defined in book summary).';
comment on column tax_import_adds.basis_49 is 'Dollar amount of basis 49 (defined in book summary).';
comment on column tax_import_adds.basis_5 is 'Dollar amount of basis 5 (defined in book summary).';
comment on column tax_import_adds.basis_50 is 'Dollar amount of basis 50 (defined in book summary).';
comment on column tax_import_adds.basis_51 is 'Dollar amount of basis 51 (defined in book summary).';
comment on column tax_import_adds.basis_52 is 'Dollar amount of basis 52 (defined in book summary).';
comment on column tax_import_adds.basis_53 is 'Dollar amount of basis 53 (defined in book summary).';
comment on column tax_import_adds.basis_54 is 'Dollar amount of basis 54 (defined in book summary).';
comment on column tax_import_adds.basis_55 is 'Dollar amount of basis 55 (defined in book summary).';
comment on column tax_import_adds.basis_56 is 'Dollar amount of basis 56 (defined in book summary).';
comment on column tax_import_adds.basis_57 is 'Dollar amount of basis 57 (defined in book summary).';
comment on column tax_import_adds.basis_58 is 'Dollar amount of basis 58 (defined in book summary).';
comment on column tax_import_adds.basis_59 is 'Dollar amount of basis 59 (defined in book summary).';
comment on column tax_import_adds.basis_6 is 'Dollar amount of basis 6 (defined in book summary).';
comment on column tax_import_adds.basis_60 is 'Dollar amount of basis 60 (defined in book summary).';
comment on column tax_import_adds.basis_61 is 'Dollar amount of basis 61 (defined in book summary).';
comment on column tax_import_adds.basis_62 is 'Dollar amount of basis 62 (defined in book summary).';
comment on column tax_import_adds.basis_63 is 'Dollar amount of basis 63 (defined in book summary).';
comment on column tax_import_adds.basis_64 is 'Dollar amount of basis 64 (defined in book summary).';
comment on column tax_import_adds.basis_65 is 'Dollar amount of basis 65 (defined in book summary).';
comment on column tax_import_adds.basis_66 is 'Dollar amount of basis 66 (defined in book summary).';
comment on column tax_import_adds.basis_67 is 'Dollar amount of basis 67 (defined in book summary).';
comment on column tax_import_adds.basis_68 is 'Dollar amount of basis 68 (defined in book summary).';
comment on column tax_import_adds.basis_69 is 'Dollar amount of basis 69 (defined in book summary).';
comment on column tax_import_adds.basis_7 is 'Dollar amount of basis 7 (defined in book summary).';
comment on column tax_import_adds.basis_70 is 'Dollar amount of basis 70 (defined in book summary).';
comment on column tax_import_adds.basis_8 is 'Dollar amount of basis 8 (defined in book summary).';
comment on column tax_import_adds.basis_9 is 'Dollar amount of basis 9 (defined in book summary).';
comment on column tax_import_adds.bonus_value is '0, 30, or 50.';
comment on column tax_import_adds.bus_segment_id is 'System-assigned identifier of a particular Business Segment.';
comment on column tax_import_adds.bus_segment_xlate is 'Value from the import file translated to obtain the bus_segment_id.';
comment on column tax_import_adds.cc_tax_indicator is 'Not used.';
comment on column tax_import_adds.cc_value is 'Value of the classification code.';
comment on column tax_import_adds.class_code_id is 'System-assigned identifier of a particular Class Code.';
comment on column tax_import_adds.class_code_xlate is 'Value from the import file translated to obtain the class_code_id.';
comment on column tax_import_adds.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_adds.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_adds.end_month is 'End month to which transactions apply.';
comment on column tax_import_adds.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_adds.ext_class_code_value is 'For bonus depreciation: 2 = 30%; 3 = 50%; 1 = Nonqualifying; 0 = blanket.';
comment on column tax_import_adds.gl_account_id is 'System-assigned identifier of a particular GL Account.';
comment on column tax_import_adds.gl_account_xlate is 'Value from the import file translated to obtain the gl_account_id.';
comment on column tax_import_adds.gl_posting_mo_yr is 'Book posting month year of the addition.';
comment on column tax_import_adds.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_adds.in_service_year is 'Book in-service year.';
comment on column tax_import_adds.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_adds.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_adds.month_number is 'Month of the transaction in YYYYMM format.';
comment on column tax_import_adds.start_month is 'Start month to which transactions apply.';
comment on column tax_import_adds.sub_account_id is 'System-assigned identifier of a particular Sub Account.';
comment on column tax_import_adds.sub_account_xlate is 'Value from the import file translated to obtain the sub_account_id.';
comment on column tax_import_adds.tax_distinction_id is 'System-assigned identifier of a particular Tax Distinction.';
comment on column tax_import_adds.tax_distinction_xlate is 'Value from the import file translated to obtain the tax_distinction_id.';
comment on column tax_import_adds.tax_load_id is 'The type of additions load.  1 = Regular Addition, 2 = Reverse Early In-Service, 3 = Forward Early In-Service.';
comment on column tax_import_adds.tax_location_id is 'System-assigned identifier of a particular Tax Location.';
comment on column tax_import_adds.tax_location_xlate is 'Value from the import file translated to obtain the tax_location_id.';
comment on column tax_import_adds.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_adds.trans_id is 'System-assigned identifier of a particular Transaction ID.';
comment on column tax_import_adds.trans_xlate is 'Value from the import file translated to obtain the trans_id.';
comment on column tax_import_adds.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_adds.utility_account_id is 'System-assigned identifier of a particular Utility Account.';
comment on column tax_import_adds.utility_account_xlate is 'Value from the import file translated to obtain the utility_account_id.';
comment on column tax_import_adds.work_order_number is 'Work order number.  Can be used in the audit trail.';
comment on column tax_import_adds.year is 'Vintage (in-service year for tax).';
comment on column tax_import_rets.amount is 'Book dollar amount of the retirement transaction.';
comment on column tax_import_rets.bonus_value is '0, 30, or 50.';
comment on column tax_import_rets.bus_segment_id is 'System-assigned identifier of a particular Business Segment.';
comment on column tax_import_rets.bus_segment_xlate is 'Value from the import file translated to obtain the bus_segment_id.';
comment on column tax_import_rets.cc_tax_indicator is 'Yes/No Indicator designating whether or not the class code is applicable for tax.';
comment on column tax_import_rets.cc_value is 'Value of the classification code for these assets.';
comment on column tax_import_rets.class_code_id is 'System-assigned identifier of a particular Class Code.';
comment on column tax_import_rets.class_code_xlate is 'Value from the import file translated to obtain the class_code_id.';
comment on column tax_import_rets.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_rets.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_rets.end_month is 'End month to which transactions apply.';
comment on column tax_import_rets.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_rets.ext_class_code_value is 'For bonus depreciation: 2 = 30%; 3 = 50%; 1 = Nonqualifying; 0 = blanket.';
comment on column tax_import_rets.gl_account_id is 'System-assigned identifier of a particular GL Account.';
comment on column tax_import_rets.gl_account_xlate is 'Value from the import file translated to obtain the gl_account_id.';
comment on column tax_import_rets.gl_posting_mo_yr is 'Book posting month and year of the retirement.';
comment on column tax_import_rets.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_rets.in_service_year is 'Book in-service year.';
comment on column tax_import_rets.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_rets.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_rets.month_number is 'Month of the transaction in YYYYMM format.';
comment on column tax_import_rets.start_month is 'Start month to which transactions apply.';
comment on column tax_import_rets.sub_account_id is 'System-assigned identifier of a particular Sub Account.';
comment on column tax_import_rets.sub_account_xlate is 'Value from the import file translated to obtain the sub_account_id.';
comment on column tax_import_rets.tax_activity_code_id is 'System-assigned identifier of a particular Tax Activity Code.';
comment on column tax_import_rets.tax_activity_code_xlate is 'Value from the import file translated to obtain the tax_activity_code_id.';
comment on column tax_import_rets.tax_distinction_id is 'System-assigned identifier of a particular Tax Distinction.';
comment on column tax_import_rets.tax_distinction_xlate is 'Value from the import file translated to obtain the tax_distinction_id.';
comment on column tax_import_rets.tax_location_id is 'System-assigned identifier of a particular Tax Location.';
comment on column tax_import_rets.tax_location_xlate is 'Value from the import file translated to obtain the tax_location_id.';
comment on column tax_import_rets.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_rets.trans_id is 'System-assigned identifier of a particular Transaction ID.';
comment on column tax_import_rets.trans_xlate is 'Value from the import file translated to obtain the trans_id.';
comment on column tax_import_rets.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_rets.utility_account_id is 'System-assigned identifier of a particular Utility Account.';
comment on column tax_import_rets.utility_account_xlate is 'Value from the import file translated to obtain the utility_account_id.';
comment on column tax_import_rets.year is 'Vintage (In-Service year for tax).';
comment on column tax_import_trans.amount is 'Book dollar amount of the transfer transaction.';
comment on column tax_import_trans.book_sale_amount is 'Amount of transfer allocated to the tax records.';
comment on column tax_import_trans.bus_segment_id is 'System-assigned identifier of a particular Business Segment.';
comment on column tax_import_trans.bus_segment_xlate is 'Value from the import file translated to obtain the bus_segment_id.';
comment on column tax_import_trans.cc_tax_indicator is 'Yes/No Indicator designating whether or not the class code is applicable for tax.';
comment on column tax_import_trans.cc_value is 'Value of the classification code for these assets.';
comment on column tax_import_trans.class_code_id is 'System-assigned identifier of a particular Class Code.';
comment on column tax_import_trans.class_code_xlate is 'Value from the import file translated to obtain the class_code_id.';
comment on column tax_import_trans.company_id is 'System-assigned identifier of a particular Company.';
comment on column tax_import_trans.company_xlate is 'Value from the import file translated to obtain the company_id.';
comment on column tax_import_trans.end_month is 'End month to which transactions apply.';
comment on column tax_import_trans.error_message is 'Error message encountered while importing / translating / loading this row of data during the import process.';
comment on column tax_import_trans.gl_account_id is 'System-assigned identifier of a particular GL Account.';
comment on column tax_import_trans.gl_account_xlate is 'Value from the import file translated to obtain the gl_account_id.';
comment on column tax_import_trans.gl_posting_mo_yr is 'Book posting month and year of the transfer.';
comment on column tax_import_trans.import_run_id is 'System-assigned identifier of an import run occurrence.';
comment on column tax_import_trans.in_service_year is 'Book in-service year.';
comment on column tax_import_trans.is_modified is 'Yes (1) / No (0) switch indicating if the data for this record has been changed in the file, compared to what is currently in the database.';
comment on column tax_import_trans.line_id is 'Line number corresponding to the row of this data in the original import file.';
comment on column tax_import_trans.start_month is 'Start month to which transactions apply.';
comment on column tax_import_trans.sub_account_id is 'System-assigned identifier of a particular Sub Account.';
comment on column tax_import_trans.sub_account_xlate is 'Value from the import file translated to obtain the sub_account_id.';
comment on column tax_import_trans.tax_activity_code_id is 'System-assigned identifier of a particular Tax Activity Code.';
comment on column tax_import_trans.tax_activity_code_xlate is 'Value from the import file translated to obtain the tax_activity_code_id.';
comment on column tax_import_trans.tax_distinction_id is 'System-assigned identifier of a particular Tax Distinction.';
comment on column tax_import_trans.tax_distinction_xlate is 'Value from the import file translated to obtain the tax_distinction_id.';
comment on column tax_import_trans.tax_location_id is 'System-assigned identifier of a particular Tax Location.';
comment on column tax_import_trans.tax_location_xlate is 'Value from the import file translated to obtain the tax_location_id.';
comment on column tax_import_trans.time_stamp is 'Standard System-assigned timestamp used for audit purposes.';
comment on column tax_import_trans.trans_id is 'System-assigned identifier of a particular Transaction ID.';
comment on column tax_import_trans.trans_xlate is 'Value from the import file translated to obtain the trans_id.';
comment on column tax_import_trans.user_id is 'Standard System-assigned user id used for audit purposes.';
comment on column tax_import_trans.utility_account_id is 'System-assigned identifier of a particular Utility Account.';
comment on column tax_import_trans.utility_account_xlate is 'Value from the import file translated to obtain the utility_account_id.';
comment on column tax_import_trans.year is 'Vintage (In-Service year for tax).';

comment on column tax_import_depr_adjust_arc.accum_reserve_adjust is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.book_balance_adjust is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.cap_gain_loss_adjust is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.company_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.company_xlate is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.depreciable_base_adjust is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.depreciation_adjust is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.gain_loss_adjust is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.import_run_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.in_service_month is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.line_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.tax_book_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.tax_book_xlate is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.tax_class_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.tax_class_xlate is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.tax_record_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.tax_record_xlate is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.tax_year is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.time_stamp is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.user_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.version_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.vintage_id is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_depr_adjust_arc.vintage_xlate is 'Archive of same column from tax_import_depr_adjust.';
comment on column tax_import_basis_amounts_arc.amount is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.company_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.company_xlate is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.import_run_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.in_service_month is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.line_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_activity_code_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_activity_code_xlate is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_class_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_class_xlate is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_include_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_include_xlate is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_record_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_record_xlate is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_summary_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_summary_xlate is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.tax_year is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.time_stamp is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.user_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.version_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.vintage_id is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_basis_amounts_arc.vintage_xlate is 'Archive of same column from tax_import_basis_amounts.';
comment on column tax_import_book_reconcile_arc.basis_amount_activity is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.company_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.company_xlate is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.import_run_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.in_service_month is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.line_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.reconcile_item_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.reconcile_item_xlate is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.tax_class_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.tax_class_xlate is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.tax_include_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.tax_include_xlate is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.tax_record_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.tax_record_xlate is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.tax_year is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.time_stamp is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.user_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.version_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.vintage_id is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_book_reconcile_arc.vintage_xlate is 'Archive of same column from tax_import_book_reconcile.';
comment on column tax_import_tax_class_arc.description is 'Archive of same column from tax_import_tax_class.';
comment on column tax_import_tax_class_arc.import_run_id is 'Archive of same column from tax_import_tax_class.';
comment on column tax_import_tax_class_arc.line_id is 'Archive of same column from tax_import_tax_class.';
comment on column tax_import_tax_class_arc.oper_ind is 'Archive of same column from tax_import_tax_class.';
comment on column tax_import_tax_class_arc.oper_ind_xlate is 'Archive of same column from tax_import_tax_class.';
comment on column tax_import_tax_class_arc.tax_class_id is 'Archive of same column from tax_import_tax_class.';
comment on column tax_import_tax_class_arc.time_stamp is 'Archive of same column from tax_import_tax_class.';
comment on column tax_import_tax_class_arc.user_id is 'Archive of same column from tax_import_tax_class.';
comment on column tax_import_rec_item_arc.calced is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.calced_xlate is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.default_tax_include_id is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.default_tax_include_xlate is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.depr_deduction is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.depr_deduction_xlate is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.description is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.import_run_id is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.input_retire_ind is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.input_retire_ind_xlate is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.line_id is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.net_basis_rpt is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.net_basis_rpt_xlate is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.reconcile_item_id is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.tax_type_of_property_id is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.tax_type_of_property_xlate is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.time_stamp is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.type is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_rec_item_arc.user_id is 'Archive of same column from tax_import_rec_item.';
comment on column tax_import_vintage_arc.bonus_end_month is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.bonus_start_month is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.description is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.end_month is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.ext_class_code_value is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.import_run_id is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.line_id is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.start_month is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.time_stamp is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.user_id is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.vintage_convention_id is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.vintage_convention_xlate is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.vintage_id is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_vintage_arc.year is 'Archive of same column from tax_import_vintage.';
comment on column tax_import_bk_translate_arc.active is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.bus_segment_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.bus_segment_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.class_code_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.class_code_value is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.class_code_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.company_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.company_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.end_eff_book_vintage is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.gl_account_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.gl_account_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.import_run_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.life is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.line_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.monthly_adds is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.monthly_adds_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.mortality_curve_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.mortality_curve_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.quarterly_adds is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.quarterly_adds_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.start_eff_book_vintage is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.sub_account_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.sub_account_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_book_translate_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_book_translate_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_class_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_class_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_depr_schema_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_depr_schema_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_distinction_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_distinction_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_location_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.tax_location_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.time_stamp is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.user_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.utility_account_id is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_bk_translate_arc.utility_account_xlate is 'Archive of same column from tax_import_bk_translate.';
comment on column tax_import_norm_schema_arc.amortization_type_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.amortization_type_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.basis_diff_act_split_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.basis_diff_activity_split is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.basis_diff_ret_reversal_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.basis_diff_retire_reversal is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.book_depr_alloc_ind is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.book_depr_alloc_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.cap_depr_ind is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.cap_depr_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.def_income_tax_rate_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.def_income_tax_rate_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.description is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.gl_account_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.gl_account_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.import_run_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.jurisdiction_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.jurisdiction_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.line_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.no_zero_check is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.no_zero_check_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.norm_from_tax is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.norm_from_tax_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.norm_to_tax is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.norm_to_tax_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.norm_type_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.norm_type_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.normalization_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.reconcile_item_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.reconcile_item_xlate is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.time_stamp is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_norm_schema_arc.user_id is 'Archive of same column from tax_import_norm_schema.';
comment on column tax_import_depr_schema_arc.description is 'Archive of same column from tax_import_depr_schema.';
comment on column tax_import_depr_schema_arc.import_run_id is 'Archive of same column from tax_import_depr_schema.';
comment on column tax_import_depr_schema_arc.line_id is 'Archive of same column from tax_import_depr_schema.';
comment on column tax_import_depr_schema_arc.tax_depr_schema_id is 'Archive of same column from tax_import_depr_schema.';
comment on column tax_import_depr_schema_arc.time_stamp is 'Archive of same column from tax_import_depr_schema.';
comment on column tax_import_depr_schema_arc.user_id is 'Archive of same column from tax_import_depr_schema.';
comment on column tax_import_depr_sch_ctrl_arc.convention_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.convention_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.def_gain_rec_item_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.def_gain_reconcile_item_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.def_gain_tax_inc_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.def_gain_tax_include_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.deferred_tax_schema_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.deferred_tax_schema_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.ext_class_code_value is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.extraordinary_conv_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.extraordinary_convention is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.import_run_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.in_service_month is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.line_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.recovery_period_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.recovery_period_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.summary_4562_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.summary_4562_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_book_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_book_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_credit_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_credit_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_depr_schema_control_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_depr_schema_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_depr_schema_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_limit_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_limit_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_rate_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.tax_rate_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.time_stamp is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.type_of_property_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.type_of_property_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.user_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.vintage_convention_id is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_depr_sch_ctrl_arc.vintage_convention_xlate is 'Archive of same column from tax_import_depr_sch_ctrl.';
comment on column tax_import_retire_rules_arc.company_id is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.company_xlate is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.fformat_string is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.fifo is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.fifo_xlate is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.gformat_string is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.import_run_id is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.iteration is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.line_id is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.min_vintage is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.sort_order is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.time_stamp is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.tolerance is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.user_id is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.vintage_back is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_retire_rules_arc.vintage_forward is 'Archive of same column from tax_import_retire_rules.';
comment on column tax_import_adds_arc.amount is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_1 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_10 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_11 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_12 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_13 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_14 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_15 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_16 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_17 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_18 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_19 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_2 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_20 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_21 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_22 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_23 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_24 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_25 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_26 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_27 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_28 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_29 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_3 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_30 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_31 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_32 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_33 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_34 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_35 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_36 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_37 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_38 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_39 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_4 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_40 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_41 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_42 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_43 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_44 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_45 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_46 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_47 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_48 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_49 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_5 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_50 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_51 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_52 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_53 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_54 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_55 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_56 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_57 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_58 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_59 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_6 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_60 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_61 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_62 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_63 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_64 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_65 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_66 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_67 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_68 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_69 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_7 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_70 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_8 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.basis_9 is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.bonus_value is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.bus_segment_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.bus_segment_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.cc_tax_indicator is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.cc_value is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.class_code_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.class_code_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.company_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.company_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.end_month is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.ext_class_code_value is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.gl_account_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.gl_account_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.gl_posting_mo_yr is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.import_run_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.in_service_year is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.line_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.month_number is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.start_month is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.sub_account_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.sub_account_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.tax_distinction_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.tax_distinction_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.tax_load_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.tax_location_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.tax_location_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.time_stamp is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.trans_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.trans_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.user_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.utility_account_id is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.utility_account_xlate is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.work_order_number is 'Archive of same column from tax_import_adds.';
comment on column tax_import_adds_arc.year is 'Archive of same column from tax_import_adds.';
comment on column tax_import_rets_arc.amount is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.bonus_value is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.bus_segment_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.bus_segment_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.cc_tax_indicator is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.cc_value is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.class_code_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.class_code_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.company_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.company_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.end_month is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.ext_class_code_value is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.gl_account_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.gl_account_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.gl_posting_mo_yr is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.import_run_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.in_service_year is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.line_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.month_number is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.start_month is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.sub_account_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.sub_account_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.tax_activity_code_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.tax_activity_code_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.tax_distinction_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.tax_distinction_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.tax_location_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.tax_location_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.time_stamp is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.trans_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.trans_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.user_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.utility_account_id is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.utility_account_xlate is 'Archive of same column from tax_import_rets.';
comment on column tax_import_rets_arc.year is 'Archive of same column from tax_import_rets.';
comment on column tax_import_trans_arc.amount is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.book_sale_amount is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.bus_segment_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.bus_segment_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.cc_tax_indicator is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.cc_value is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.class_code_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.class_code_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.company_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.company_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.end_month is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.gl_account_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.gl_account_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.gl_posting_mo_yr is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.import_run_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.in_service_year is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.line_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.start_month is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.sub_account_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.sub_account_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.tax_activity_code_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.tax_activity_code_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.tax_distinction_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.tax_distinction_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.tax_location_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.tax_location_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.time_stamp is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.trans_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.trans_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.user_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.utility_account_id is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.utility_account_xlate is 'Archive of same column from tax_import_trans.';
comment on column tax_import_trans_arc.year is 'Archive of same column from tax_import_trans.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1115, 0, 10, 4, 3, 0, 37337, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037337_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;