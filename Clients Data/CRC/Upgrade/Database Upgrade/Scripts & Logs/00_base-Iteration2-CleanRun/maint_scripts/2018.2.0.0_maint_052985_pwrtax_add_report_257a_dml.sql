/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052985_pwrtax_add_report_257a_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 01/31/2019 David Conway   DML to Add report 257a - FAS109 by type with provision and reversal
||============================================================================
*/

insert into pp_reports ( report_id,
  description, long_description, subsystem, datawindow, special_note,
  report_type, report_number, filter_option, pp_report_subsystem_id, report_type_id,
  pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, dynamic_dw
)
select 
 (SELECT NVL(Max(report_id),0) FROM pp_reports WHERE report_id >= 400000 and report_id <= 499999 ) + 1 REPORT_ID, 
  'FAS109  by type (Alternate)' description,
  'FAS109  by type with provision and reversal' long_description,
  'PowerTax' subsystem,
  'dw_tax_rpt_dfit_fas109_type_a_1043_be' datawindow,
  'dfit' special_note,
  'PowerTax_Rollup' report_type,
  'PwrTax - 257a' report_number,
  'DETAIL' filter_option,
  1 pp_report_subsystem_id,
  129 report_type_id,
  52 pp_report_time_option_id,
  81 pp_report_filter_id,
  1 pp_report_status_id,
  3 pp_report_envir_id,
  0 dynamic_dw
from dual
  left join pp_reports on pp_reports.report_number = 'PwrTax - 257a'
where pp_reports.report_number is null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14502, 0, 2018, 2, 0, 0, 52985, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052985_pwrtax_add_report_257a_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

