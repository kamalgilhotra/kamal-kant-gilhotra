/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049518_lessor_hide_unused_menu_items_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/26/2017 JSisouphanh      Remove unused menu items
||============================================================================
*/

update ppbase_menu_items
set enable_yn = 0
where module = 'LESSOR'
and menu_identifier in ('control_residual', 'initiate_asset', 'search_asset', 'control_invoices', 'admin_lessor_pend_transaction')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3836, 0, 2017, 1, 0, 0, 49518, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049518_lessor_hide_unused_menu_items_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;