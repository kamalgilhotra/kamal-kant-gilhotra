/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043270_cap_bud_subs_to_reason_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 3/14/2015  Chris Mardis     Add "to_reason" to budget subs
||============================================================================
*/

alter table budget_substitutions add (
   to_reason varchar2(2000)
   );

comment on column budget_substitutions.reason is 'Notes about the substitution. Why the source project(s) is being defunded.';
comment on column budget_substitutions.to_reason is 'Notes about the substitution. Why the receiving project needs the funds.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2383, 0, 2015, 1, 0, 0, 43270, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043270_cap_bud_subs_to_reason_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;