/*
||==========================================================================================
|| Application: Drill
|| Module: 		PCM
|| File Name:   maint_042870_pcm_change_263.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 	  2/17/2015  LAlston     	      Update from 'Work Order Number(s)' to 'Work Order ID(s)'
||==========================================================================================
*/

UPDATE  PP_DYNAMIC_FILTER
SET LABEL='Work Order ID(s)', 
	INPUT_TYPE='operation', 
	SQLS_COLUMN_EXPRESSION='work_order_control.work_order_id', 
	SQLS_WHERE_CLAUSE='work_order_control.work_order_id in (select funding_wo_id from work_order_control where funding_wo_indicator = 0 and [selected_values])'
WHERE FILTER_ID = 263;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2306, 0, 2015, 1, 0, 0, 042870, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042870_pcm_change_263.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;