SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030564_depr_over_check.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/09/2013 B. Beck        Point release
||============================================================================
*/

begin
   execute immediate 'drop table DEPR_CALC_OVER_STG';
   DBMS_OUTPUT.PUT_LINE('DEPR_CALC_OVER_STG table dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('DEPR_CALC_OVER_STG table does not exist to drop.');
end;
/

create global temporary table DEPR_CALC_OVER_STG
(
 DEPR_GROUP_ID          number(22,0) not null,
 GL_POST_MO_YR          date not null,
 SET_OF_BOOKS_ID        number(22,0) not null,
 TYPE                   varchar2(35),
 END_BALANCE            number(22,2),
 END_RESERVE            number(22,2),
 EXPENSE                number(22,2),
 RETRO_EXPENSE          number(22,2),
 NET_SALVAGE_PCT        number(22,8),
 SIGN_BALANCE           number(22,0),
 SIGN_RESERVE           number(22,0),
 SIGN_EXPENSE           number(22,0),
 OVER_DEPR_AMOUNT       number(22,2),
 OVER_DEPR_CHECK_AMOUNT number(22,2)
) on commit preserve rows;

comment on table "DEPR_CALC_OVER_STG" is '(T) [01] A global temporary table used to hold over depreciation checks during Depreciation Calculation';

comment on column "DEPR_CALC_OVER_STG"."DEPR_GROUP_ID" is 'The internal powerplant id for the depr group going through over depreciation check';
comment on column "DEPR_CALC_OVER_STG"."GL_POST_MO_YR" is 'The month going through over depreciation check';
comment on column "DEPR_CALC_OVER_STG"."SET_OF_BOOKS_ID" is 'The set of books going through over depreciation check';
comment on column "DEPR_CALC_OVER_STG"."TYPE" is 'The type of over depreciation check.  Values are LIFE, COR, COMB';
comment on column "DEPR_CALC_OVER_STG"."END_BALANCE" is 'The ending balance to use for over depreciation check';
comment on column "DEPR_CALC_OVER_STG"."END_RESERVE" is 'The ending reserve to use for the over depreciation check';
comment on column "DEPR_CALC_OVER_STG"."EXPENSE" is 'The expense calculated.  This is used to determine maximum depreciation adjustment allowed if over depreciated';
comment on column "DEPR_CALC_OVER_STG"."RETRO_EXPENSE" is 'The expense adjustment calculated as a result of retro rate change.  This is used to determine maximum depreciation adjustment allowed if over depreciated';
comment on column "DEPR_CALC_OVER_STG"."NET_SALVAGE_PCT" is 'The net salvage percent (or cor percent if COR)';
comment on column "DEPR_CALC_OVER_STG"."SIGN_BALANCE" is '1 if end balance is positive, -1 if negative, 0 if zero';
comment on column "DEPR_CALC_OVER_STG"."SIGN_RESERVE" is '1 if end reserve is positive, -1 if negative, 0 if zero';
comment on column "DEPR_CALC_OVER_STG"."SIGN_EXPENSE" is '1 if expense is positive, -1 if negative, 0 if zero';
comment on column "DEPR_CALC_OVER_STG"."OVER_DEPR_AMOUNT" is 'The amount of the adjustment due to over depreciation';
comment on column "DEPR_CALC_OVER_STG"."OVER_DEPR_CHECK_AMOUNT" is 'Used to store the minimum amount of expense or expense + retro adj.  Used to compare total over deprecation to the maximum allowed';

create index NDX_DEPR_CALC_OVER_STG on DEPR_CALC_OVER_STG (DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID);

alter table DEPR_CALC_STG
   add (OVER_DEPR_ADJ number(22,2),
        OVER_DEPR_ADJ_COR number(22,2),
        OVER_DEPR_ADJ_SALV number(22,2));

comment on column DEPR_CALC_STG.OVER_DEPR_ADJ is 'The over depr adjustment';
comment on column DEPR_CALC_STG.OVER_DEPR_ADJ_COR is 'The over depr adjustment from COR';
comment on column DEPR_CALC_STG.OVER_DEPR_ADJ_SALV is 'The over depr adjustment from Salvage';

alter table DEPR_CALC_STG modify MID_PERIOD_CONV number(22,2);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (669, 0, 10, 4, 1, 1, 30564, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_030564_depr_over_check.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
