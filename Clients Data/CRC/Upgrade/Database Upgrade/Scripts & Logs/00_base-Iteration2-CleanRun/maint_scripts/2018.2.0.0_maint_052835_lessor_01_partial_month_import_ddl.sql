/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052835_lessor_01_partial_month_import_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 03/26/2019  Sarah Byers      Add Partial Month Payment Term Type functionality to Lessor
||============================================================================
*/

alter table lsr_import_ilr add (
payment_term_type_id number(22,0),
payment_term_type_id_xlate varchar(254));

comment on column lsr_import_ilr.payment_term_type_id is 'System-assigned identifier of a particular lease payment term type.';
comment on column lsr_import_ilr.payment_term_type_id_xlate is 'Translation field for determining the Payment Term Type; "Normal" and "Partial Month" are supported.';

alter table lsr_import_ilr_archive add (
payment_term_type_id number(22,0),
payment_term_type_id_xlate varchar(254));

comment on column lsr_import_ilr_archive.payment_term_type_id is 'System-assigned identifier of a particular lease payment term type.';
comment on column lsr_import_ilr_archive.payment_term_type_id_xlate is 'Translation field for determining the Payment Term Type; "Normal" and "Partial Month" are supported.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16244, 0, 2018, 2, 0, 0, 52835, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052835_lessor_01_partial_month_import_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;