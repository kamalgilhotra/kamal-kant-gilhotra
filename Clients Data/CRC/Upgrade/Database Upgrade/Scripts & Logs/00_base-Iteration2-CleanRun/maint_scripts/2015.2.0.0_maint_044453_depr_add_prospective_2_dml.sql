/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044453_depr_add_prospective_2_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/30/2015 Daniel Motter  Add prospective column to table maintenance
||============================================================================
*/

insert into powerplant_columns (column_name, table_name, dropdown_name, pp_edit_type_id, description, column_rank, read_only)
select 'prospective', 'depreciation_method', 'yes_no', 'p', 'Prospective', 7, 0
from dual where not exists (select 1 from powerplant_columns where lower(trim(column_name)) = 'prospective' and lower(trim(table_name)) = 'depreciation_method');

insert into powerplant_columns (column_name, table_name, dropdown_name, pp_edit_type_id, description, column_rank, read_only)
select 'prospective', 'fcst_depreciation_method', 'yes_no', 'p', 'Prospective', 7, 0
from dual where not exists (select 1 from powerplant_columns where lower(trim(column_name)) = 'prospective' and lower(trim(table_name)) = 'fcst_depreciation_method');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2735, 0, 2015, 2, 0, 0, 044453, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044453_depr_add_prospective_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;