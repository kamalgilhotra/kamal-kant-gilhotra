/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053055_pwrtax_01_def_rate_tax_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 03/05/2019 K Powers	  PowerTax deferred tax rate usability window
|| 2018.2.0.0 03/05/2019 K Powers	  Modified for specific ID's from MasterDB
||============================================================================
*/

-- Remove any previous runs

-- PowerTax ID's are hardcoded and specific and generated from MasterDB
delete from pp_dynamic_filter_mapping where pp_report_filter_id = 107 and filter_id = 290;
delete from pp_dynamic_filter where filter_id = 290;
delete from pp_reports_filter where pp_report_filter_id = 107;

-- Set up the workspace

UPDATE ppbase_menu_items
SET enable_yn       = 1
WHERE module        = 'powertax'
AND menu_identifier = 'deferred_setup_rates';

UPDATE ppbase_workspace
SET workspace_uo_name    = 'uo_tax_dfit_wksp_dit_rates',
  object_type_id         = 1
WHERE module             = 'powertax'
AND workspace_identifier = 'deferred_setup_rates';

-- Insert report filters/mappings

INSERT
INTO pp_reports_filter
  (
    pp_report_filter_id,
    description,
    filter_uo_name
  )
  VALUES
  (
    107, 
    'PowerTax - Deferred Rates',
    'uo_ppbase_tab_filter_dynamic'
  );
  
INSERT
INTO pp_dynamic_filter 
(filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required,
single_select_only,valid_operators,data,exclude_from_where,not_retrieve_immediate,invisible)
VALUES
  (
    290 ,	
    'Deferred Rates',
    'dw',
    'deferred_rates.def_income_tax_rate_id',
    NULL,
    'dw_deferred_rates_no_accrual_filter',
    'def_income_tax_rate_id',
    'description',
    'N',
    0,0,
    NULL,
    NULL,
    0,0,0
  );
  
INSERT
INTO pp_dynamic_filter_mapping
  (
    pp_report_filter_id,
    filter_id
  )
  VALUES
  (107,290);

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15686, 0, 2018, 2, 0, 0, 53055, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053055_pwrtax_01_def_rate_tax_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
