 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046196_02_prov_add_table_values_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 09/02/2016 Jared Watkins  Harvest into base; add necessary items to
 ||											TBBS tables and remove deprecated entries
 ||============================================================================
 */ 

--------------------------------------
--add missing data and remove old data
--data is required so constraints can
--	be applied
--------------------------------------

DECLARE
  new_id NUMBER;
BEGIN
  SELECT MAX(process_id) + 1 INTO new_id
  from pp_processes;

  MERGE INTO pp_processes A
  USING (SELECT new_id AS process_id, 'TBBS - Manage Schemata' AS description, 'Uses PKG_TBBS_SCHEMA to maintain TBBS schemata' AS long_description,
  		0 AS allow_concurrent, 0 AS system_lock_enabled, 0 AS system_lock, 0 AS async_email_on_complete FROM dual
  	 UNION ALL
	 SELECT new_id + 1, 'TBBS - Run Report', 'Runs PKG_TBBS_REPORT to process the Tax Basis Balance Sheet', 1, 0, 0, 0 FROM dual
  	 UNION ALL
  	 SELECT new_id + 2, 'TBBS - Process Book Accounts', 'Runs PKG_TBBS_ACCOUNT to process the Book Accounts for the Tax Basis Balance Sheet', 0, 0, 0, 0 FROM dual) b
  ON (a.description = b.description)
  WHEN NOT MATCHED then
       INSERT (a.process_id, a.description, a.long_description, a.allow_concurrent, a.system_lock_enabled, a.system_lock, a.async_email_on_complete)
       VALUES (b.process_id, b.description, b.long_description, b.allow_concurrent, b.system_lock_enabled, b.system_lock, b.async_email_on_complete);
END;
/

DELETE FROM ppbase_menu_items WHERE Lower(MODULE) = 'provision';
DELETE FROM ppbase_workspace WHERE Lower(MODULE) = 'provision';

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_account_setup',to_date('30-MAR-16','DD-MON-RR'),'PWRPLANT','TBBS Account Setup','uo_tbbs_account_setup_workspace','Setup TBBS Accounts',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_assignment_view',to_date('20-JUN-16','DD-MON-RR'),'PWRPLANT','TBBS Assignments','uo_tbbs_assignment_view_workspace','View TBBS Assignments',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_book_adjust',to_date('27-JUN-16','DD-MON-RR'),'PWRPLANT','TBBS Book Adjustments','uo_tbbs_book_adjust_workspace','Setup TBBS Book Adjustments',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_column_close',to_date('25-MAY-16','DD-MON-RR'),'PWRPLANT','TBBS Column Closing Setup','uo_tbbs_close_column_workspace',
       'Setup Closing Column Totals to Line Item',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_line_item_assignment',to_date('30-MAR-16','DD-MON-RR'),'PWRPLANT','TBBS Line Item Assignment','uo_tbbs_line_item_assignment_workspace',
       'Assign Accounts and M-Items to Line Items',1);
       
INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_line_item_setup',to_date('30-MAR-16','DD-MON-RR'),'PWRPLANT','TBBS Line Item Setup','uo_tbbs_line_item_setup_workspace','Setup Line Items',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_line_item_tree_setup',to_date('06-APR-16','DD-MON-RR'),'PWRPLANT','TBBS Structure Setup','uo_tbbs_line_item_tree_workspace','Setup TBBS Structure',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_report_display',to_date('29-APR-16','DD-MON-RR'),'PWRPLANT','TBBS Report','uo_tbbs_report_display_workspace','Tax Basis Balance Sheet',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_reports',to_date('30-MAR-16','DD-MON-RR'),'PWRPLANT','Reports','uo_tbbs_report_workspace','Run Reports',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_schema_setup',to_date('30-MAR-16','DD-MON-RR'),'PWRPLANT','TBBS Schema Setup','uo_tbbs_schema_setup_workspace','Setup TBBS Schemas',1);

INSERT INTO PPBASE_WORKSPACE (MODULE,WORKSPACE_IDENTIFIER,TIME_STAMP,USER_ID,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
VALUES ('provision','tbbs_tax_adjust',to_date('27-JUN-16','DD-MON-RR'),'PWRPLANT','TBBS Tax Adjustments','uo_tbbs_tax_adjust_workspace','Setup TBBS Tax Adjustments',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_reports',to_date('30-MAR-16','DD-MON-RR'),'PWRPLANT',1,1,'Reports','Run Reports',null,'tbbs_reports',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_book_adjust',to_date('27-JUN-16','DD-MON-RR'),'PWRPLANT',1,2,'Book Adjustments','Configure Book Adjustments',null,'tbbs_book_adjust',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_tax_adjust',to_date('27-JUN-16','DD-MON-RR'),'PWRPLANT',1,3,'Tax Adjustments','Configure Tax Adjustments',null,'tbbs_tax_adjust',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_admin',to_date('30-MAR-16','DD-MON-RR'),'PWRPLANT',1,4,'Admin','TBBS Administration',null,null,1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_account_setup',to_date('30-MAR-16','DD-MON-RR'),'PWRPLANT',2,1,'Account Setup','Setup TBBS Accounts','tbbs_admin','tbbs_account_setup',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_column_close',to_date('25-MAY-16','DD-MON-RR'),'PWRPLANT',2,2,'Column Closing Setup','Setup Closing Column Totals to Line Items','tbbs_admin',
       'tbbs_column_close',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_line_item_setup',to_date('25-MAY-16','DD-MON-RR'),'PWRPLANT',2,3,'Line Item Setup','Setup Line Items','tbbs_admin','tbbs_line_item_setup',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_line_item_assignment',to_date('25-MAY-16','DD-MON-RR'),'PWRPLANT',2,4,'Line Item Assignment','Assign Accounts and M-Items to Line Items','tbbs_admin',
       'tbbs_line_item_assignment',1);
       
INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_schema_setup',to_date('25-MAY-16','DD-MON-RR'),'PWRPLANT',2,5,'Schema Setup','Setup TBBS Schemas','tbbs_admin','tbbs_schema_setup',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_line_item_tree_setup',to_date('25-MAY-16','DD-MON-RR'),'PWRPLANT',2,6,'Structure Setup','Setup TBBS Structure','tbbs_admin',
       'tbbs_line_item_tree_setup',1);

INSERT INTO PPBASE_MENU_ITEMS (MODULE,MENU_IDENTIFIER,TIME_STAMP,USER_ID,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
VALUES ('provision','tbbs_assignment_view',to_date('20-JUN-16','DD-MON-RR'),'PWRPLANT',2,7,'View Assignments','View Line Item Assignemnts','tbbs_admin','tbbs_assignment_view',1);

DELETE FROM tbbs_treatment_type;

INSERT INTO TBBS_TREATMENT_TYPE (TREATMENT_TYPE_ID,TIME_STAMP,USER_ID,DESCRIPTION,LONG_DESCRIPTION,TREATMENT_SQL)
VALUES (1,null,null,'Standard TBBS Treatment',
       'Use this option for most line items. Calculated Tax basis will equal Adjusted Book Basis + Adjusted Cumulative Timing Difference',NULL);

INSERT INTO TBBS_TREATMENT_TYPE (TREATMENT_TYPE_ID,TIME_STAMP,USER_ID,DESCRIPTION,LONG_DESCRIPTION,TREATMENT_SQL)
VALUES (2,null,null,'Eliminate Deferred Tax Accounts',
       'This treatment type should be used for line items that will contain DTA and DTL accounts such as 190, 281, 282 and 283. The Adjusted Book Basis will be reversed through the ' || 
       'Eliminate Deferred Taxes column so it has no impact on the tax basis','SET book_elim = -1 * (coalesce(book_rc, 0) + coalesce(book_adj,0))');
       
INSERT INTO TBBS_TREATMENT_TYPE (TREATMENT_TYPE_ID,TIME_STAMP,USER_ID,DESCRIPTION,LONG_DESCRIPTION,TREATMENT_SQL)
VALUES (3,null,null,'Reg Asset / Liability accounts',
       'This treatment type should be used for line items that will contain Regulatory Asset and Liability accounts such as 182 and 254. The Adjusted Book Basis will be reversed ' ||
       'through the Adjusted Timing Difference column so it does not impact tax basis.', 'SET tax_m_elim = -1 * (coalesce(book_amount, 0) + coalesce(book_rc, 0) + coalesce(book_adj,0))');

DELETE FROM tbbs_system_control;

INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (1,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','Balance Table Name', NULL,'Table containing balances for the TBBS.',
       'Name of the table containing account balances to be used by the TBBS. If a Dynamic query is chosen, the ID of the query from pp_any_query_criteria is saved to another system control.');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (2,to_date('10-DEC-15','DD-MON-RR'),'PWRPLANT','Balance Table Query ID', NULL,'Query ID for the balances table.',
       'If a dynamic query is chosen as the account balances table, this control contains the ID of the query selected from pp_any_query_criteria.');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (3,to_date('05-JAN-16','DD-MON-RR'),'PWRPLANT','Account Table Name', NULL,'Table containing the book accounts for the TBBS.',
       'Name of the table containing accounts to be used by the TBBS. This can be the same as the balances table. If a Dynamic query is chosen, the ID of the query ' ||
       'from pp_any_query_criteria is saved to another system control.');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (4,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','Account Table Query ID', NULL,'Query ID for the book account table.',
       'If a dynamic query is chosen as the book account table, this control contains the ID of the query selected from pp_any_query_criteria.');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (5,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','GL COMPANY Column', NULL,'Company column name in the balances table.','Column name of the Company field in the balances table.');

INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (6,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','MONTH NUMBER Column', NULL,'Month Number column name in the balances table.',
       'Column name of the Month Number field in the balances table.');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (7,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','Balance Source', 'table','Source of balance information--"table" or "query"',
       'Defines the source of balances to be used by the TBBS, either a database table or a query defined in pp_any_query_criteria');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (8,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','ACCOUNT ID Column', NULL,'Balance table column that contains main account id',
       'Column on the balances table that contains the main account identifier');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (9,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','Account Source', 'table','Source of acct description info-"table" or "query"',
       'Defines the source of balances to be used by the TBBS, either a database table or a query defined in pp_any_query_criteria');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (10,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','Balance Table Account ID Column',NULL,'Field with acct descrip. in source(query or table)',
       'Column in the query defined in Account Description Query ID or table in Account Description Table Name (Depending on Account Description Source Value) containing account descriptions');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (11,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','Account Description Column', NULL,'Field with acct descrip. in source(query or table)',
       'Column in the query defined in Account Table Query ID or table in Account Table Name (Depending on Account Description Source Value) containing account descriptions');
       
INSERT INTO TBBS_SYSTEM_CONTROL (CONTROL_ID,TIME_STAMP,USER_ID,CONTROL_NAME,CONTROL_VALUE,DESCRIPTION,LONG_DESCRIPTION)
VALUES (13,to_date('17-JUN-16','DD-MON-RR'),'PWRPLANT','Amount Column',NULL,'Field with amounts in balance source',
       'Column in the PP any query w/ id = Account Table Query ID or table in Account Table Name (see Account Description Source Value) containing account balances');
       
INSERT INTO tbbs_system_control (control_id,time_stamp,user_id,control_name,control_value,description,long_description)
VALUES (12,to_date('05-JAN-16','DD-MON-RR'),'PWRPLANT','INCLUDE_UNBOOKED_RTA',NULL,'Field with acct descrip. in source(query or table)',
       'Column in the query defined in Account Table Query ID or table in Account Table Name (Depending on Account Description Source Value) containing account descriptions');

merge INTO tax_accrual_gl_month_def d
USING (
SELECT 
                mon.ta_version_id, 
                mon.gl_month, 
                ver.YEAR || LPad(mon.period, 2,0) tbbs_month
FROM tax_accrual_gl_month_def mon
JOIN tax_accrual_version ver
                ON mon.ta_version_id = ver.ta_version_id
) s
ON (s.ta_version_id = d.ta_version_id AND s.gl_month = d.gl_month)
WHEN MATCHED THEN UPDATE SET d.tbbs_month = s.tbbs_month;

MERGE INTO pp_tree_category A
USING (SELECT 4 AS pp_tree_category_id, 'TBBS Line Item' AS description, 'tax_accrual_line_item' AS table_name,
              'line_item_id' AS column_name, 'description' AS column_description
        FROM dual) b ON (a.pp_tree_category_id = b.pp_tree_category_id)
WHEN NOT MATCHED THEN
  INSERT (A.pp_tree_category_id, A.description, A.table_name, A.column_name, A.column_description)
  VALUES (b.pp_tree_category_id, b.description, b.table_name, b.column_name, b.column_description);

MERGE INTO tbbs_line_type A
USING (SELECT 1 AS line_type_id, 'Summary - Tree Node' AS line_type_ind, 1 AS line_type_order, 'Summary Consolidating node in the TBBS Tree structure' AS description
        FROM dual
        UNION
        SELECT 2, 'Summary - Line Item',2,'TBBS Line item summary which consolidates Book Accounts and M Items'
        FROM dual
        UNION
        SELECT 3, 'Detail - Column Close',3,'Column Closing adjustment which balances the total of a TBBS column'
        FROM dual
        UNION
        SELECT 4, 'Detail - Book Account',4,'Single line book account detail from the trial balance'
        FROM dual
        UNION
        SELECT 5, 'Detail - M Item',5,'Single line M Item detail from Provision'
        from dual
        UNION
        SELECT 6, 'Detail - Collapsed',6,'Single line of Collapsed Book account and M Item amounts'
        FROM dual
        UNION
        SELECT 7, 'Total',7,'Single line Grand Total calculation'
        FROM dual) b ON (A.line_type_id = b.line_type_id)
WHEN MATCHED THEN
  UPDATE SET A.line_type_ind = b.line_type_ind, A.line_type_order = b.line_type_order, A.description = b.description
  where a.line_type_ind <> b.line_type_ind or a.line_type_order <> b.line_type_order or a.description <> b.description
WHEN NOT MATCHED THEN
  INSERT (A.line_type_id, A.line_type_ind, A.line_type_order, A.description)
  VALUES (b.line_type_id, b.line_type_ind, b.line_type_order, b.description);

  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3283, 0, 2016, 1, 0, 0, 046196, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046196_02_prov_add_table_values_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;