 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_006674_prov_20161_supermap_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0  08/09/2016 Jarrett Skov   Script that changes the length of External_Key columns to be consistent. 35 in most places, 10 in others.
 ||============================================================================
 */ 

-- Supermap
alter table TAX_ACCRUAL_DATA_SOURCES modify external_key varchar2(35);
alter table TAX_ACCRUAL_CONSOL_DRILL modify external_key varchar2(35);

-- Also found that the alloc_drill table has a field set to 10.
alter table TAX_ACCRUAL_ALLOC_DRILL modify external_key_from varchar2(35);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3253, 0, 2016, 1, 0, 0, 006674, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_006674_prov_20161_supermap_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;