/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Lesse
|| File Name:   maint_041655_lease_mla_status.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3 12/15/2014 		B.Beck				Add MLA statused
||==========================================================================================
*/

insert into ppbase_actions_windows
( id, module, action_identifier, action_text, action_order, action_event )
values
( 20, 'LESSEE', 'close_mla', 'Close MLA', 9, 'ue_closemla' );

insert into ppbase_actions_windows
( id, module, action_identifier, action_text, action_order, action_event )
values
( 21, 'LESSEE', 'retire_mla', 'Retire MLA', 10, 'ue_retiremla' );

commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2113, 0, 10, 4, 3, 2, 041655, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041655_lease_mla_status.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;