/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036992_depr_default_columns.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 03/07/2014 Brandon Beck
||============================================================================
*/

alter table DEPR_CALC_STG modify COMBINED_DEPR_ADJ number(22,2) default 0;
alter table DEPR_CALC_STG modify COMBINED_SALV_ADJ number(22,2) default 0;
alter table DEPR_CALC_STG modify COMBINED_COR_ADJ  number(22,2) default 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1013, 0, 10, 4, 2, 0, 36992, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036992_depr_default_columns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;