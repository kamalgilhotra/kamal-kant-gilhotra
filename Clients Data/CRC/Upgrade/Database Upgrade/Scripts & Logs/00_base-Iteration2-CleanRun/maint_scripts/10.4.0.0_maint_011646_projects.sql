/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011646_projects.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version   Date       Revised By     Reason for Change
|| --------- ---------- -------------- --------------------------------------
|| 10.4.0.0  01/17/2013 Chris Mardis   Point Release
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'WO CR Derivations',
          'never',
          'never;on update;on approval;always',
          'Controls when WO CR Derivations are run. "Never"-never run WO CR Derivations; "On Update"-anytime the monthly estimate or unit estimate is updated; "On Approval"-anytime the revision is fully approved; "Always"-always run WO CR Derivations',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (276, 0, 10, 4, 0, 0, 11646, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011646_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
