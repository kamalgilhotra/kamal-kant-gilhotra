/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051707_lessee_02_add_depr_calc_method_import_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/19/2018 Sarah Byers      Add depr calc method to ILR import
||============================================================================
*/

INSERT INTO pp_import_column
(IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,IS_ON_TABLE)
SELECT   import_type_id,
         'depr_calc_method',
         'Depr Calc Method',
         0,
         1,
         'number(12,0)',
         1
from pp_import_type a where lower(import_table_name) = 'ls_import_ilr'
and not exists (select 1 from pp_import_column b where a.import_type_id = b.import_type_id and lower(b.column_name) = 'depr_calc_method');

insert into pp_import_template_fields
(import_template_id, field_id, import_type_id, column_name)
select t.import_template_id, max(nvl(f.field_id,0)) + 1, a.import_type_id, 'depr_calc_method'
  from pp_import_type a, pp_import_template t, pp_import_template_fields f
 where lower(a.import_table_name) = 'ls_import_ilr'
   and a.import_type_id = t.import_type_id
   and t.import_type_id = f.import_type_id
   and t.import_template_id = f.import_template_id
   and not exists (select 1 from pp_import_template_fields b where a.import_type_id = b.import_type_id and lower(b.column_name) = 'depr_calc_method')
 group by t.import_template_id, a.import_type_id;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6824, 0, 2017, 4, 0, 0, 51707, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051707_lessee_02_add_depr_calc_method_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;