 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048593_reg_import_allo_wipe_replace_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 07/18/2017 Shane "C" Ward	Add column and ability to Wipe and Replace Allo Factors RMS
 ||============================================================================
 */

INSERT INTO pp_import_column (import_type_Id, column_name, description, is_required, processing_order, column_type, is_on_table)
VALUES (150, 'wipe_and_replace', 'Replace Existing (Yes or No)', 0, 1, 'varchar2(30)', 1);


INSERT INTO pp_import_column (import_type_Id, column_name, description, is_required, processing_order, column_type, is_on_table)
VALUES (151, 'wipe_and_replace', 'Replace Existing (Yes or No)', 0, 1, 'varchar2(30)', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3592, 0, 2017, 1, 0, 0, 48593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048593_reg_import_allo_wipe_replace_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;