/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044611_taxrpr_escalate_rets_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   08/19/2015 Alex P.        Escalate Retirement Dollars for pretests.
||============================================================================
*/

alter table REPAIR_WORK_ORDER_TEMP_ORIG add (HW_RETS_BASE_YEAR number(22,0));
alter table REPAIR_WORK_ORDER_TEMP add (HW_RETS_BASE_YEAR number(22,0));
alter table REPAIR_WORK_ORDER_SEGMENTS add (HW_RETS_BASE_YEAR number(22,0));
alter table REPAIR_WORK_ORDER_SEGMENTS add (HW_RETS_BASE_RATE number(22,8));
alter table REPAIR_WORK_ORDER_SEGMENTS add (HW_RETS_COST number(22,2));
alter table REPAIR_WORK_ORDER_SEGMENTS add (HW_RELATION_RETS_COST number(22,2));
alter table REPAIR_WO_SEG_REPORTING add (HW_RETS_COST number(22,2));
alter table REPAIR_WO_SEG_REPORTING add (HW_RELATION_RETS_COST number(22,2));

comment on column REPAIR_WORK_ORDER_TEMP_ORIG.HW_RETS_BASE_YEAR IS 'Retirement Year for a given tax unit of property/repair location combination. The retirement year is calculated as a weighted average of the retirement vintages found in cpr_ledger.eng_in_service_year.';
comment on column REPAIR_WORK_ORDER_TEMP.HW_RETS_BASE_YEAR IS 'Retirement Year for a given tax unit of property/repair location combination. The retirement year is calculated as a weighted average of the retirement vintages found in cpr_ledger.eng_in_service_year.';
comment on column REPAIR_WORK_ORDER_SEGMENTS.HW_RETS_BASE_YEAR IS 'Retirement Year for a given tax unit of property/repair location combination. The retirement year is calculated as a weighted average of the retirement vintages found in cpr_ledger.eng_in_service_year.';
comment on column REPAIR_WORK_ORDER_SEGMENTS.HW_RETS_BASE_RATE IS 'Handy Whitman rate for retirement base year of the tax unit of property.';
comment on column REPAIR_WORK_ORDER_SEGMENTS.HW_RETS_COST IS 'Retirement Cost escalated using Handy Whitman tables.';
comment on column REPAIR_WORK_ORDER_SEGMENTS.HW_RELATION_RETS_COST IS 'Retirement Cost for a set of related work orders escalated using Handy Whitman tables. This is the sum of HW_RETS_COST values within a given relation.';
comment on column REPAIR_WO_SEG_REPORTING.HW_RETS_COST IS 'Retirement Cost escalated using Handy Whitman tables.';
comment on column REPAIR_WO_SEG_REPORTING.HW_RELATION_RETS_COST IS 'Retirement Cost for a set of related work orders escalated using Handy Whitman tables. This is the sum of HW_RETS_COST values within a given relation.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2810, 0, 2015, 2, 0, 0, 044611, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044611_taxrpr_escalate_rets_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;