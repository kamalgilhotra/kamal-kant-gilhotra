/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047241_lease_admin_xchg_rate_menu_dml.sql
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2017.1.0.0 05/31/2017  Anand R        Create new menu item under Lease -> Admin menu for exchange rate view
||============================================================================
*/

insert into ppbase_workspace (MODULE, WORKSPACE_IDENTIFIER, LABEL, 
       WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID)
select 'LESSEE', 'admin_view_exchange_rates', 'View Exchange Rates', 
       'uo_ls_admincntr_view_exchange_rate_wksp', null, 1
from dual
where not exists (select 1 from ppbase_workspace
                  where module = 'LESSEE'
                  and workspace_identifier = 'admin_view_exchange_rates');

insert into ppbase_menu_items (MODULE, MENU_IDENTIFIER, MENU_LEVEL, 
       ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, 
       WORKSPACE_IDENTIFIER, ENABLE_YN)
select 'LESSEE', 'admin_view_exchange_rates', 2, 
       9, 'View Exchange Rates', null, 'menu_wksp_admin', 
       'admin_view_exchange_rates', 1
from dual
where not exists (select 1 from ppbase_menu_items
                  where module = 'LESSEE'
                  and menu_level = 2
                  and parent_menu_identifier = 'menu_wksp_admin'
                  and item_order = 9);
				  
	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3519, 0, 2017, 1, 0, 0, 47241, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047241_lease_admin_xchg_rate_menu_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;