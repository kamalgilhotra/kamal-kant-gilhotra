/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041319_pcm_fp_search_ddl2.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	12/01/2014 Ryan Oliveria		Altering dynamic filters for FP Search
||========================================================================================
*/
 
alter table PP_DYNAMIC_FILTER_SAVED_VALUES drop primary key drop index;

alter table PP_DYNAMIC_FILTER_SAVED_VALUES
	add constraint PP_DYNAMIC_FILTER_SAVED_VAL_PK
		primary key (SAVED_FILTER_ID, LABEL, FILTER_TYPE_ID)
		using index tablespace PWRPLANT_IDX;



alter table PP_DYN_FILTER_SAVED_VALUES_DW drop primary key drop index;

alter table PP_DYN_FILTER_SAVED_VALUES_DW
	add constraint PK_DYN_FILTER_SAVED_VALUES_DW
		primary key (SAVED_FILTER_ID, LABEL, VALUE_ID, FILTER_TYPE_ID)
		using index tablespace PWRPLANT_IDX;



alter table PP_DYNAMIC_FILTER_VALUES drop primary key drop index;

alter table PP_DYNAMIC_FILTER_VALUES
	add constraint PK_DYNAMIC_FILTER_VALUES
		primary key (LABEL, USERS, FILTER_TYPE_ID);



alter table PP_DYNAMIC_FILTER_VALUES_DW drop primary key drop index;

alter table PP_DYNAMIC_FILTER_VALUES_DW
	add constraint PK_DYNAMIC_FILTER_VALUES_DW
		primary key (LABEL, USERS, VALUE_ID, FILTER_TYPE_ID);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2077, 0, 2015, 1, 0, 0, 041319, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041319_pcm_fp_search_ddl2.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;