/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043641_cpr_ssp_depr_calc_allow_concurrent_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 04/15/2015 Sarah Byers		Set ALLOW_CONCURRENT for Server Side Processes
||============================================================================
*/

update pp_processes 
	set ALLOW_CONCURRENT = 1 
 where lower(executable_file) like 'ssp_depr_calc.exe%';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2502, 0, 2015, 1, 0, 0, 43641, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043641_cpr_ssp_depr_calc_allow_concurrent_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;