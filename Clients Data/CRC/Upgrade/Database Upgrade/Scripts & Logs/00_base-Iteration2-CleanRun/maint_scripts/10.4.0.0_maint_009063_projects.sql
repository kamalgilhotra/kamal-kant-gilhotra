/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009063_projects.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   01/09/2013 Chris Mardis   Point Release
||============================================================================
*/

alter table WO_DOC_JUSTIFICATION_RULES add USEROBJECT_NAME varchar2(35);
alter table WO_DOC_JUSTIFICATION_RULES add DATAWINDOW_SYNTAX clob;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (272, 0, 10, 4, 0, 0, 9063, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_009063_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;