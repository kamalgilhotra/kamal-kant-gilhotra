/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048575_lease_add_lessor_menu_items_and_workspaces_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------  ------------------------------------
|| 2017.1.0.0 08/21/2017 Jared Watkins   Add the workspaces and menu items required for Lessor to the PP base tables
||============================================================================
*/
--Remove any workspaces or menu items added for the Lessor prototype
delete from ppbase_menu_items where module = 'LESSOR';
delete from ppbase_workspace where module = 'LESSOR';

--Add the workspaces for the Initiate menu items
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'menu_wksp_initiate', 'Initiate', ' ', NULL, 1);

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'initiate_mla', 'Initiate MLA', 'uo_ls_lscntr_wksp_init', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'initiate_asset', 'Initiate Asset', 'uo_ls_assetcntr_wksp_wizard', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'initiate_ilr', 'Initiate ILR', 'uo_ls_ilrcntr_wksp_init_lease', NULL, 1);

--Add the Initiate menu items
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_initiate', 1, 1, 'Initiate', NULL, NULL, NULL, 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'initiate_mla', 2, 1, 'MLA', NULL, 'menu_wksp_initiate', 'initiate_mla', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'initiate_asset', 2, 2, 'Asset', NULL, 'menu_wksp_initiate', 'initiate_asset', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'initiate_ilr', 2, 3, 'ILR', NULL, 'menu_wksp_initiate', 'initiate_ilr', 1);

--Add the workspaces for the Details menu items
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'menu_wksp_details', 'Details', ' ', NULL, 1);

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'search_mla', 'Search MLA', 'uo_ls_lscntr_wksp_search', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'search_asset', 'Search Asset', 'uo_ls_assetcntr_wksp_search', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'search_ilr', 'Search ILR', 'uo_ls_ilrcntr_wksp_search', NULL, 1);

--Add the Details menu items
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_details', 1, 2, 'Details', NULL, NULL, NULL, 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'search_mla', 2, 1, 'MLA', NULL, 'menu_wksp_details', 'search_mla', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'search_asset', 2, 2, 'Asset', NULL, 'menu_wksp_details', 'search_asset', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'search_ilr', 2, 3, 'ILR', NULL, 'menu_wksp_details', 'search_ilr', 1);

--Add the workspaces for the Approval menu items
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'menu_wksp_approval', 'Approvals', ' ', NULL, 1);

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'approval_mla', 'MLA', 'uo_ls_lscntr_wksp_approvals', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'approval_assets', 'Assets and Schedules', 'uo_ls_ilrcntr_wksp_approvals', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'approval_payments', 'Payments', 'uo_ls_pymntcntr_wksp_approvals_open', NULL, 1);

--Add the Approval menu items
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_approval', 1, 3, 'Approvals', NULL, NULL, NULL, 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'approval_mla', 2, 1, 'MLA', NULL, 'menu_wksp_approval', 'approval_mla', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'approval_assets', 2, 2, 'Assets and Schedules', NULL, 'menu_wksp_approval', 'approval_assets', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'approval_payments', 2, 3, 'Payments', NULL, 'menu_wksp_approval', 'approval_payments', 1);

--Add the workspaces for the Monthly Control menu items
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'menu_wksp_control', 'Monthly Control', ' ', NULL, 1);

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'control_monthend', 'Month End', 'uo_ls_mecntr_wksp', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'control_invoices', 'Detailed Invoices', 'uo_ls_invoicecntr_wksp', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'control_residual', 'Residual Processing', 'uo_ls_mecntr_wksp_residual', NULL, 1);

--Add the Monthly Control menu items
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_control', 1, 4, 'Monthly Control', NULL, NULL, NULL, 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'control_monthend', 2, 1, 'Month End', NULL, 'menu_wksp_control', 'control_monthend', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'control_invoices', 2, 2, 'Detailed Invoices', NULL, 'menu_wksp_control', 'control_invoices', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'control_residual', 2, 3, 'Residual Processing', NULL, 'menu_wksp_control', 'control_residual', 1);

--Add the workspace for the Reports menu item
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'menu_wksp_reports', 'Reports', 'uo_ls_rptcntr_wksp', NULL, 1);

--Add the Reports menu item
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_reports', 1, 5, 'Reports', NULL, NULL, 'menu_wksp_reports', 1);

--Add the workspace for the Query menu item
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'menu_wksp_query', 'Query', 'uo_ls_querycntr_wksp', NULL, 1);

--Add the Query menu item
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_query', 1, 6, 'Query', NULL, NULL, 'menu_wksp_query', 1);

--Add the workspaces for the Admin menu items
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'menu_wksp_admin', 'Admin', ' ', NULL, 1);

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'admin_lease_groups', 'Lease Groups', 'uo_ls_lgcntr_wksp_details', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'admin_ilr_groups', 'ILR Groups', 'uo_ls_igcntr_wksp_details', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'admin_lessees', 'Lessees', 'uo_ls_admincntr_lessor_wksp', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'admin_table_maint', 'Table Maintenance', 'uo_ls_admincntr_wksp_table_maint', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'admin_lessor_pend_transaction', 'Pending Transactions', 'w_ls_trans_manage', NULL, 2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'admin_set_of_books_fasb_type', 'Set of Books FASB Type', 'uo_ls_admincntr_set_of_books_fasb_type_wksp', NULL, 1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'admin_view_exchange_rates', 'View Exchange Rates', 'uo_ls_admincntr_view_exchange_rate_wksp', NULL, 1);

--Add the Admin menu items
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_admin', 1, 7, 'Admin', NULL, NULL, NULL, 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_lease_groups', 2, 1, 'Lease Groups', NULL, 'menu_wksp_admin', 'admin_lease_groups', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_ilr_groups', 2, 2, 'ILR Groups', NULL, 'menu_wksp_admin', 'admin_ilr_groups', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_lessees', 2, 3, 'Lessees', NULL, 'menu_wksp_admin', 'admin_lessees', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_table_maint', 2, 4, 'Table Maintenance', NULL, 'menu_wksp_admin', 'admin_table_maint', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_lessor_pend_transaction', 2, 5, 'Pending Transactions', NULL, 'menu_wksp_admin', 'admin_lessor_pend_transaction', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_set_of_books_fasb_type', 2, 6, 'Set of Books by FASB Type', NULL, 'menu_wksp_admin', 'admin_set_of_books_fasb_type', 1);
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_view_exchange_rates', 2, 7, 'View Exchange Rates', NULL, 'menu_wksp_admin', 'admin_view_exchange_rates', 1);

--Add the workspaces for the Import menu items
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'menu_wksp_import', 'Import', ' ', NULL, 1);

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'import_new', 'Import New', 'uo_ls_importcntr_wksp', NULL, 1);

--Add the Import menu items
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_import', 1, 8, 'Import', NULL, NULL, NULL, 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_import_new', 2, 1, 'New Import', NULL, 'menu_wksp_import', 'import_new', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3668, 0, 2017, 1, 0, 0, 48575, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048575_lease_add_lessor_menu_items_and_workspaces_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;