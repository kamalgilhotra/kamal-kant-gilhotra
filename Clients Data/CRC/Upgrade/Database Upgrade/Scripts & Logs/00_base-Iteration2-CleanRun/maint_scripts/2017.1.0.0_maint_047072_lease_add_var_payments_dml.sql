/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047072_lease_add_var_payments_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 02/17/2017 Anand R          Add new menu and a workspace
||============================================================================
*/


INSERT INTO ppbase_workspace (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID) 
VALUES ('LESSEE', 'manage_var_payments', 'Manage Variable Payments', 'uo_ls_var_payments_wksp', 'Manage Variable Payments',	1);


INSERT INTO ppbase_menu_items (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN) VALUES (
	'LESSEE','menu_wksp_var_payments', 1, 10, 'Variable Payments', 'Variable Payments', NULL, NULL, 1);
	
INSERT INTO ppbase_menu_items (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN) VALUES (
	'LESSEE','manage_var_payments', 2, 1, 'Manage Variable Payments', 'Manage Variable Payments', 'menu_wksp_var_payments', 'manage_var_payments', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3372, 0, 2017, 1, 0, 0, 47072, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047072_lease_add_var_payments_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;