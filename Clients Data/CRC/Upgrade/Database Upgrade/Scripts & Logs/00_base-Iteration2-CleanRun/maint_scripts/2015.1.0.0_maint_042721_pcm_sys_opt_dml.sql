/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042721_pcm_sys_opt_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/05/2015 Alex P.          WO/FP system options for companies <> -1
||                                        Tweak required fields information screen
||============================================================================
*/

-- Set up new System Options based on System Controls for companies <> -1

-- Budget Initiate Messagebox
insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Budget Initiate Messagebox', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'budget initiate messagebox'
	and company_id <> -1;
	
-- CapBud - WOT to Budget Summary
insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'CapBud - WOT to Budget Summary', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'capbud - wot to budget summary'
	and company_id <> -1;

-- FPInt-Validate WO Type/Bud Item Co
insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'FPInt-Validate WO Type/Bud Item Co', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'fpint-validate wo type/bud item co'
	and company_id <> -1;
	
-- Funding Project Generation
insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Funding Project Generation', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'funding project generation'
	and company_id <> -1;
	
-- Override Edit Auto Gen FP Num
insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Override Edit Auto Gen FP Num', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'override edit auto gen fp num'
	and company_id <> -1;

-- WOInit - Open Projects Only
insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WOInit - Open Projects Only', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'woinit - open projects only'
	and company_id <> -1;

-- Work Order Type Default
insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Work Order Type Default', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'work order type default'
	and company_id <> -1;

 -- Work Order Type Default-Many
 insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Work Order Type Default-Many', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'work order type default-many'
	and company_id <> -1;
	
 -- Work Order Generation
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Work Order Generation', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'work order generation'
	and company_id <> -1;
	
-- Work Order Initiation
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'Work Order Initiation', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'work order initiation'
	and company_id <> -1;
	
---------------- WO/FP Detail System options --------------------------
-- FPEst - Validate vs FP Type
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'FPEst - Validate vs FP Type', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'fpest - validate vs fp type'
	and company_id <> -1;
	
-- WODetail - Audit WO Dates vs. FP
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WODetail - Audit WO Dates vs. FP', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'wodetail - audit wo dates vs. fp'
	and company_id <> -1;

-- WODetail - Dept/Div/Bus Seg
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WODetail - Dept/Div/Bus Seg', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'wodetail - dept/div/bus seg'
	and company_id <> -1;
	
-- WODetail - Dept/Div/Major Loc
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WODetail - Dept/Div/Major Loc', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'wodetail - dept/div/major loc'
	and company_id <> -1;
	
-- WODetail - Edit FP Budget
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WODetail - Edit FP Budget', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'wodetail - edit fp budget'
	and company_id <> -1;
	
-- WODetail - Edit Funding Project
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WODetail - Edit Funding Project', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'wodetail - edit funding project'
	and company_id <> -1;
	
-- WODetail - Edit Work Order Type
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WODetail - Edit Work Order Type', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'wodetail - edit work order type'
	and company_id <> -1;

-- WOEst - FP Date Change
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WOEst - FP Date Change', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'woest - fp date change'
	and company_id <> -1;

	
-- WOEst - WO Date Change
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WOEst - WO Date Change', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'woest - wo date change'
	and company_id <> -1;
	
-- WOInit - Require Location
  insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select 'WOInit - Require Location', company_id, control_value
  from pp_system_control_company
 where lower(control_name) = 'woinit - require location'
	and company_id <> -1;
	
	
------------------------ Modify Required Fields due to change on the Information Workspace ------------------------------

update pp_required_table_column
set objectpath = 'uo_pcm_maint_info.dw_detail'
where objectpath = 'uo_pcm_maint_wksp_info.dw_detail';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2262, 0, 2015, 1, 0, 0, 042721, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042721_pcm_sys_opt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;