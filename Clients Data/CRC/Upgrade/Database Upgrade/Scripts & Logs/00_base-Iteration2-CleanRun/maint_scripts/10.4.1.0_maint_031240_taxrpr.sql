SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_031240_taxrpr.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   08/08/2013 Nathan Hollis   Point Release
||============================================================================
*/

--* INSERT SCRIPT FOR UOP-2380

insert into PP_REPORTS
   (REPORT_ID,
    DESCRIPTION,
    LONG_DESCRIPTION,
    DATAWINDOW,
    REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID,
    PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID,
    PP_REPORT_NUMBER,
    DYNAMIC_DW
   )
 values
   ((select max(REPORT_ID) + 1 from PP_REPORTS) /* report_id */,
    'Repairs: Retirement Reversal Amt' /*description*/,
    'T&D Network Repairs: Displays any retirement reversal dollars that have been recorded for a repair method for related work orders.' /*long_description*/,
    'dw_rpr_rpt_netwk_process_rets_related' /*datawindow*/,
    'UOP - 2380' /*report_number*/,
    '13' /*pp_report_subsystem_id*/,
    '102' /*report_type_id*/,
    '41' /*pp_report_time_option_id*/,
    '30' /*pp_report_filter_id*/,
    '1' /*pp_report_status_id*/,
    '3' /*pp_report_envir_id*/,
    'NETWK - 0038' /*pp_report_number*/,
    '0' /*dynamic_dw*/);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (500, 0, 10, 4, 1, 0, 31240, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031240_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON