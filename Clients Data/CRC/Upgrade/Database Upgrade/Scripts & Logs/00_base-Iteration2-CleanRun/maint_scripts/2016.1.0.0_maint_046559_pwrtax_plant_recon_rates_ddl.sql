 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046559_pwrtax_plant_recon_rates_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0  10/06/2016 Anand R       Script to drop and recreate tax_plant_recon_form_rates
 ||============================================================================
 */ 

drop table tax_plant_recon_form_rates;
 
create table tax_plant_recon_form_rates 
(tax_pr_form_id number(22,0) not null, 
jurisdiction_id number(22,0) not null, 
def_income_tax_rate_id number(22,0) not null, 
effective_date date not null, 
prev_def_income_tax_rate_id number(22,0), 
prev_effective_date date, 
time_stamp date, 
user_id varchar2(18)) ;

alter table tax_plant_recon_form_rates 
add constraint tax_plant_recon_form_rates_pk 
primary key ( tax_pr_form_id, jurisdiction_id, def_income_tax_rate_id )
using index tablespace pwrplant_idx ;

alter table tax_plant_recon_form_rates 
add constraint tax_plant_recon_form_rates_fk 
foreign key ( jurisdiction_id ) 
references pwrplant.jurisdiction;

alter table tax_plant_recon_form_rates 
add constraint tax_plant_recon_form_rates_fk1 
foreign key ( tax_pr_form_id ) 
references pwrplant.tax_plant_recon_form;

alter table tax_plant_recon_form_rates 
add constraint tax_plant_recon_form_rates_fk2 
foreign key ( def_income_tax_rate_id ) 
references pwrplant.deferred_rates;

alter table tax_plant_recon_form_rates 
add constraint tax_plant_recon_form_rates_fk4 
foreign key ( def_income_tax_rate_id, effective_date ) 
references pwrplant.deferred_income_tax_rates;

alter table tax_plant_recon_form_rates 
add constraint tax_plant_recon_form_rates_fk5 
foreign key ( prev_def_income_tax_rate_id, prev_effective_date ) 
references pwrplant.deferred_income_tax_rates;

comment on table pwrplant.tax_plant_recon_form_rates    is '(S)[09] the tax plant recon form rates table stores the beginning and ending deferred income tax rates used for a plant reconciliation form.';

comment on column tax_plant_recon_form_rates.tax_pr_form_id is 'system assigned identifier for a tax plant reconciliation form.';
comment on column tax_plant_recon_form_rates.jurisdiction_id is 'system assigned identifier for a jurisdiction.';
comment on column tax_plant_recon_form_rates.def_income_tax_rate_id is 'system assigned identifier for a deferred tax rate.';
comment on column tax_plant_recon_form_rates.effective_date is 'effective date used to identify the beginning of year rate.';
comment on column tax_plant_recon_form_rates.prev_def_income_tax_rate_id is 'system assigned identifier for previous deferred tax rate.';
comment on column tax_plant_recon_form_rates.prev_effective_date is 'effective date used to identify the previous ending year rate.';
comment on column tax_plant_recon_form_rates.time_stamp is 'standard system-assigned timestamp used for audit purposes.';
comment on column tax_plant_recon_form_rates.user_id is 'standard system-assigned user id used for audit purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3306, 0, 2016, 1, 0, 0, 46559, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046559_pwrtax_plant_recon_rates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
