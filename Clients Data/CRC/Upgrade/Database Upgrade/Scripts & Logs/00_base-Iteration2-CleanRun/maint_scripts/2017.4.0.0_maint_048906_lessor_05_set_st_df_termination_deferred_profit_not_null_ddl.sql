/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048906_lessor_05_set_st_df_termination_deferred_profit_not_null_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/19/2018 Jared Watkins    Add new column to the ST/DF termination table for Deferred Profit so we can send JEs
||============================================================================
*/

alter table lsr_ilr_termination_st_df modify deferred_profit number(22,2) not null;

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6923, 0, 2017, 4, 0, 0, 48906, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_048906_lessor_05_set_st_df_termination_deferred_profit_not_null_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;