/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008737_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.1   01/11/2012 Elhadj Bah     Point Release
||============================================================================
*/

--Maint 8737: Turn Repair_work_order_segments into a global temp table

drop table REPAIR_WORK_ORDER_SEGMENTS;

create global temporary table REPAIR_WORK_ORDER_SEGMENTS
(
 REPAIR_SCHEMA_ID              number(22,0) not null,
 COMPANY_ID                    number(22,0) not null,
 WORK_ORDER_NUMBER             varchar2(35) not null,
 REPAIR_UNIT_CODE_ID           number(22,0) not null,
 REPAIR_LOCATION_ID            number(22,0) not null,
 REPAIR_METHOD_ID              number(22,0) not null,
 ADD_QUANTITY                  number(22,0),
 RETIRE_QUANTITY               number(22,0),
 ADD_COST                      number(22,2),
 RETIRE_COST                   number(22,2),
 MIN_VINTAGE                   number(22,0),
 MAX_VINTAGE                   number(22,0),
 PRE81_QUANTITY                number(22,0),
 POST80_QUANTITY               number(22,0),
 MY_CONDITION                  varchar2(35),
 MY_EVALUATE                   number(1,0),
 MY_USE_VINTAGE                number(1,0),
 MY_ADD_TEST                   number(1,0),
 MY_RETIRE_TEST                number(1,0),
 MY_FACTOR                     number(22,8),
 MY_QTY_FACTOR                 number(22,8),
 MY_USE_RATIO                  number(1,0),
 MY_USE_REPLACE                number(1,0),
 MY_QTY_OR_DOLLARS             number(1,0),
 MY_ADD_AND_RETIRE_TEST        number(1,0),
 MY_PRE81_RETIREMENT_RATIO     number(22,8),
 MY_TEST_QUANTITY              number(22,2),
 MY_TEST_QUANTITY2             number(22,2),
 MY_ADD_THRESHOLD              number(22,0),
 MY_RETIRE_THRESHOLD           number(22,0),
 MY_UNIT_CODE_RATIO            number(22,8),
 MY_REPAIR_ELIGIBLE_AMOUNT     number(22,2),
 USER_ID                       varchar2(18),
 TIME_STAMP                    date,
 WORK_REQUEST                  varchar2(35) not null,
 CPI_ADD_COST                  number(22,2),
 RETIREMENT_UNIT_ID            number(22,0) default 0 not null,
 MY_CPI_ELIGIBLE_AMOUNT        number(22,2),
 MY_EXPENSE_REPLACE            number(22,0),
 MY_RETIREMENT_ELIGIBLE_AMOUNT number(22,2),
 REPAIR_QTY_INCLUDE            number(22,2) default 1 not null,
 RELATION_ID                   number(22,0),
 RELATION_ADD_QTY              number(22,2),
 RELATION_RETIRE_QTY           number(22,2),
 TE_AGGREGATION_ID             number(22,0),
 ADD_RETIRE_TOLERANCE_PCT      number(22,2),
 ADD_RETIRE_RATIO              number(22,2),
 TOTAL_PERIOD_ADDS_EXPENSE     number(22,2),
 TOTAL_PERIOD_RETS_EXPENSE     number(22,2),
 ADDS_TAX_EXPENSE              number(22,2),
 RETS_TAX_EXPENSE              number(22,2)
) on commit preserve rows;

alter table REPAIR_WORK_ORDER_SEGMENTS
   add constraint PK_REPAIR_WORK_ORDER_SEGMENTS
       primary key (REPAIR_SCHEMA_ID, COMPANY_ID, WORK_ORDER_NUMBER, WORK_REQUEST,
                    REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID, REPAIR_METHOD_ID, RETIREMENT_UNIT_ID, REPAIR_QTY_INCLUDE);

--** Alter table below didn't work. Yelling about referential constraint now that it's a global temp table
--alter table REPAIR_WORK_ORDER_SEGMENTS
--   add constraint FK_TEAGGREGATE
--       foreign key (TE_AGGREGATION_ID)
--       references TE_AGGREGATION;

--** Adding accounting_month to repair_batch_control
alter table REPAIR_BATCH_CONTROL add MONTH_NUMBER number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (76, 0, 10, 3, 3, 1, 8737, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.1_maint_008737_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
