/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_038671_pwrtax_sysoptions.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/30/2014 Andrew Scott         Clean up some system options.  This covers maint
||                                          37787 and 38671.
||========================================================================================
*/

----delete these potential system option duplicates
delete from PP_SYSTEM_CONTROL_COMPANY
 where (CONTROL_NAME, COMPANY_ID) in (select CONTROL_NAME, COMPANY_ID
                                        from PP_SYSTEM_CONTROL_COMPANY
                                       where UPPER(trim(CONTROL_NAME)) = 'TAX ADD KEEP WORK ORDER'
                                       group by CONTROL_NAME, COMPANY_ID
                                      having count(*) > 1)
   and (CONTROL_ID, COMPANY_ID) not in
       (select min(CONTROL_ID) CONTROL_ID, COMPANY_ID
          from PP_SYSTEM_CONTROL_COMPANY
         where (CONTROL_NAME, COMPANY_ID) in (select CONTROL_NAME, COMPANY_ID
                                                from PP_SYSTEM_CONTROL_COMPANY
                                               where UPPER(trim(CONTROL_NAME)) = UPPER(trim('TAX ADD KEEP WORK ORDER'))
                                               group by CONTROL_NAME, COMPANY_ID
                                              having count(*) > 1)
         group by COMPANY_ID);

delete from PP_SYSTEM_CONTROL_COMPANY
 where (CONTROL_NAME, COMPANY_ID) in (select CONTROL_NAME, COMPANY_ID
                                        from PP_SYSTEM_CONTROL_COMPANY
                                       where UPPER(trim(CONTROL_NAME)) = 'Treat Reserve_Credits as Salvage'
                                       group by CONTROL_NAME, COMPANY_ID
                                      having count(*) > 1)
   and (CONTROL_ID, COMPANY_ID) not in
       (select min(CONTROL_ID) CONTROL_ID, COMPANY_ID
          from PP_SYSTEM_CONTROL_COMPANY
         where (CONTROL_NAME, COMPANY_ID) in
               (select CONTROL_NAME, COMPANY_ID
                  from PP_SYSTEM_CONTROL_COMPANY
                 where UPPER(trim(CONTROL_NAME)) = UPPER(trim('Treat Reserve_Credits as Salvage'))
                 group by CONTROL_NAME, COMPANY_ID
                having count(*) > 1)
         group by COMPANY_ID);

delete from PP_SYSTEM_CONTROL_COMPANY
 where (CONTROL_NAME, COMPANY_ID) in (select CONTROL_NAME, COMPANY_ID
                                        from PP_SYSTEM_CONTROL_COMPANY
                                       where UPPER(trim(CONTROL_NAME)) = 'Treat Salvage_Cash as Salvage'
                                       group by CONTROL_NAME, COMPANY_ID
                                      having count(*) > 1)
   and (CONTROL_ID, COMPANY_ID) not in
       (select min(CONTROL_ID) CONTROL_ID, COMPANY_ID
          from PP_SYSTEM_CONTROL_COMPANY
         where (CONTROL_NAME, COMPANY_ID) in
               (select CONTROL_NAME, COMPANY_ID
                  from PP_SYSTEM_CONTROL_COMPANY
                 where UPPER(trim(CONTROL_NAME)) = UPPER(trim('Treat Salvage_Cash as Salvage'))
                 group by CONTROL_NAME, COMPANY_ID
                having count(*) > 1)
         group by COMPANY_ID);

delete from PP_SYSTEM_CONTROL_COMPANY
 where (CONTROL_NAME, COMPANY_ID) in (select CONTROL_NAME, COMPANY_ID
                                        from PP_SYSTEM_CONTROL_COMPANY
                                       where UPPER(trim(CONTROL_NAME)) = 'Treat Salvage_Returns as Salvage'
                                       group by CONTROL_NAME, COMPANY_ID
                                      having count(*) > 1)
   and (CONTROL_ID, COMPANY_ID) not in
       (select min(CONTROL_ID) CONTROL_ID, COMPANY_ID
          from PP_SYSTEM_CONTROL_COMPANY
         where (CONTROL_NAME, COMPANY_ID) in
               (select CONTROL_NAME, COMPANY_ID
                  from PP_SYSTEM_CONTROL_COMPANY
                 where UPPER(trim(CONTROL_NAME)) = UPPER(trim('Treat Salvage_Returns as Salvage'))
                 group by CONTROL_NAME, COMPANY_ID
                having count(*) > 1)
         group by COMPANY_ID);


---clean up the system options (older ones may have had "yes/no" descriptions, which did not work).
update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = '0',
       DESCRIPTION = '1;0',
       LONG_DESCRIPTION = 'Specifies whether Salvage_Cash will be factored into salvage or depreciation calculation. A "1" means Salvage_Cash will be treated as Salvage and used in the tax gain/loss calculation. A "0" means the Salvage_Cash will be spread in the book depreciation allocation.'
 where CONTROL_NAME = 'Treat Salvage_Cash as Salvage';

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = '0',
       DESCRIPTION = '1;0',
       LONG_DESCRIPTION = 'Specifies whether Salvage_Returns will be factored into salvage or depreciation calculation. A "1" means Salvage_Returns will be treated as Salvage and used in the tax gain/loss calculation. A "0" means the Salvage_Returns will be spread in the book depreciation allocation.'
 where CONTROL_NAME = 'Treat Salvage_Returns as Salvage';

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = '0',
       DESCRIPTION = '1;0',
       LONG_DESCRIPTION = 'Specifies whether Reserve_Credits will be factored into salvage or depreciation calculation. A "1" means Reserve_Credits will be treated as Salvage and used in the tax gain/loss calculation. A "0" means the Reserve_Credits will be spread in the book depreciation allocation.'
 where CONTROL_NAME = 'Treat Reserve_Credits as Salvage';

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = decode(upper(trim(control_value)), 'YES', '1', '0'),
       DESCRIPTION = '1;0',
       LONG_DESCRIPTION = 'Yes (1) means to keep the work order number when loading the additions interface. The work order number will be visible through the audit button. No (0) means that the loaded data will not keep work order number.'
 where upper(trim(control_name)) = 'TAX ADD KEEP WORK ORDER';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1463, 0, 10, 4, 3, 0, 38671, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038671_pwrtax_sysoptions.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;