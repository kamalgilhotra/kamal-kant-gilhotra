/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050691_lessee_02_add_deferred_rent_switch_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/16/2018 Josh Sandler 	  Add deferred rent switch
||============================================================================
*/

ALTER TABLE LS_ILR_OPTIONS
  ADD deferred_rent_sw NUMBER(1, 0);

ALTER TABLE LS_ILR_OPTIONS
  ADD CONSTRAINT fk_deferred_rent_sw FOREIGN KEY ( deferred_rent_sw ) REFERENCES YES_NO ( yes_no_id );

COMMENT ON COLUMN LS_ILR_OPTIONS.deferred_rent_sw IS 'A flag identifying whether this ILR should calculated deferred rent.';

ALTER TABLE LS_ILR_STG
  ADD deferred_rent_sw NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_STG.deferred_rent_sw IS 'A flag identifying whether this ILR should calculated deferred rent.';

ALTER TABLE LS_ILR_SCHEDULE_STG
  ADD deferred_rent_sw NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_SCHEDULE_STG.deferred_rent_sw IS 'A flag identifying whether this ILR should calculated deferred rent.';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD deferred_rent_sw NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.deferred_rent_sw IS 'A flag identifying whether this ILR should calculated deferred rent.';

ALTER TABLE LS_ILR_ASSET_STG
  ADD deferred_rent_sw NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_ASSET_STG.deferred_rent_sw IS 'A flag identifying whether this ILR should calculated deferred rent.'; 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(4298, 0, 2017, 3, 0, 0, 50691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050691_lessee_02_add_deferred_rent_switch_ddl.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
