/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030996_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/13/2013 Andrew Scott   New Parcel Type/Flex Field Control Workspace
|| 10.4.1.0   09/11/2013 Andrew Scott   added a minus clause to the PT_PARCEL_FLEX_FIELD_CONTROL insert
||============================================================================
*/

--
-- Put the new Parcel Types workspace in the menu.
--
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('proptax', 'parcel_types', sysdate, user, 'Parcel Type Maintenance',
    'uo_ptc_prclcntr_wksp_prcl_types', 'Add / Update Parcel Types');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('proptax', 'parcel_types', sysdate, user, 2, 4, 'Parcel Type Maintenance',
    'Add / Update Parcel Types', 'parcel_center', 'parcel_types', 1);

--
-- Delete pt_parcel_type from Table Maintenance.
--
delete from PP_TABLE_GROUPS where TABLE_NAME = 'pt_parcel_type';
delete from POWERPLANT_COLUMNS where TABLE_NAME = 'pt_parcel_type';
delete from POWERPLANT_TABLES where TABLE_NAME = 'pt_parcel_type';


----the flex field/parcel type config workspace being added
----needs a record to exist in the control table.  The script
----below adds a row if not currently populated.
----this assumes that they have the ledger populated.  If they do not,
----then they likely do not have pt_parcel_type populated, and this
----workspace will create everything needed.

insert into PT_PARCEL_FLEX_FIELD_CONTROL
   (FLEX_FIELD_ID, PARCEL_TYPE_ID, TIME_STAMP, USER_ID, FLEX_FIELD_LABEL, FLEX_USAGE_ID,
    VALIDATE_VALUE)
   select FLEX_FIELD_ID, PARCEL_TYPE_ID, sysdate, LOWER(user), '', 1 /*not used*/, 0
   from ( 
     select FLEX_FIELD_ID, PARCEL_TYPE_ID
       from (select PARCEL_TYPE_ID from PT_PARCEL_TYPE where PARCEL_TYPE_ID > 0),
            (select ROWNUM FLEX_FIELD_ID
               from PT_LEDGER /*this assumes that they have the ledger populated*/
              where ROWNUM < 51)
     minus
     select FLEX_FIELD_ID, PARCEL_TYPE_ID
     from PT_PARCEL_FLEX_FIELD_CONTROL
   );

update PT_PARCEL_FLEX_FIELD_CONTROL
   set COLUMN_TYPE = 'Number(22,8)'
 where FLEX_FIELD_ID in (1, 2, 3, 4, 5, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33)
   and trim(COLUMN_TYPE) is null;

update PT_PARCEL_FLEX_FIELD_CONTROL
   set COLUMN_TYPE = 'Date'
 where FLEX_FIELD_ID in (6, 7, 8, 9, 10, 34, 35, 36, 37, 38)
   and trim(COLUMN_TYPE) is null;

update PT_PARCEL_FLEX_FIELD_CONTROL
   set COLUMN_TYPE = 'Varchar2(35)'
 where FLEX_FIELD_ID in (11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
   and trim(COLUMN_TYPE) is null;

update PT_PARCEL_FLEX_FIELD_CONTROL
   set COLUMN_TYPE = 'Varchar2(254)'
 where FLEX_FIELD_ID in (21, 22, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50)
   and trim(COLUMN_TYPE) is null;

update PT_PARCEL_FLEX_FIELD_CONTROL
   set COLUMN_TYPE = 'Varchar2(2000)'
 where FLEX_FIELD_ID in (23)
   and trim(COLUMN_TYPE) is null;

update PT_PARCEL_FLEX_FIELD_CONTROL set VALIDATE_VALUE = 0 where VALIDATE_VALUE is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (514, 0, 10, 4, 1, 0, 30996, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030996_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
