/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_043452_lease_020_make_gl_account_id_fk_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1.0.0 04/08/2015 Charlie Shilling    backfill gl_account_id so we can make it NOT NULL
||==========================================================================================
*/

DECLARE
	null_count INTEGER;
BEGIN
	SELECT Count(*)
	INTO null_count
	FROM ls_asset
	WHERE gl_account_id IS NULL;

	IF null_count > 0 THEN
		Raise_Application_Error(-20001, 'Upgrade script "maint_043452_lease_020_make_gl_account_id_fk_ddl.sql" '
			 || 'is attempting to make the column LS_ASEET.GL_ACCOUNT_ID into a NOT NULL column, but NULL value '
			 || 'exist in that column. Please populate the column and run the NOT NULL alter table statement. See '
			 || 'script comments for details.');
	END IF;

	/*
		The following statement will fail if there are any NULL values in the gl_account column on the LS_ASSET table,
		 though the above "Raise_Application_Error" call should prevent this statement from erroring.

		After talking with the lease SMEs, very few clients should have data in this table prior to the 2015.1 upgrade,
		so this should be a rare circumstance.
	*/

	--make gl_account_id not null
	EXECUTE IMMEDIATE 'ALTER TABLE ls_asset
	MODIFY gl_account_id NOT NULL';
END;
/


/*
	This statement might fail if there are any GL_ACCOUNTs assigned on this table that are not in the
	base GL_ACCOUNT table.\
*/
--foreign key to GL_ACCOUNT table
ALTER TABLE ls_asset
  ADD CONSTRAINT r_ls_asset_gl_acct FOREIGN KEY (
    gl_account_id
  ) REFERENCES gl_account (
    gl_account_id
  )
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2496, 0, 2015, 1, 0, 0, 043452, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043452_lease_020_make_gl_account_id_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;