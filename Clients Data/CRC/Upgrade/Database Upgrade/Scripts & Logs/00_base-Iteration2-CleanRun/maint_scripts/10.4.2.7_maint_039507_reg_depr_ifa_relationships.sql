/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039507_reg_depr_ifa_relationships.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/18/2014 Ryan Oliveria    Renaming Menu Items and Workspaces
||============================================================================
*/

delete from PPBASE_MENU_ITEMS
 where MENU_IDENTIFIER = 'IFA_CREATE'
   and MODULE = 'REG';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_inc_ifa_fp_review', 'IFA - Review', 'uo_reg_inc_review_fp_ifa', 'IFA - Review');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'IFA_REVIEW', 3, 3, 'IFA - Review', 'IFA - Review', 'IFA_TOP', 'uo_reg_inc_ifa_fp_review', 1);

update PPBASE_MENU_ITEMS
   set LABEL = 'Incremental Fcst Adj'
 where MENU_IDENTIFIER = 'IFA_TOP'
   and MODULE = 'REG';

update PPBASE_MENU_ITEMS
   set LABEL = 'IFA - FP Select', MINIHELP = 'IFA - FP Select'
 where MENU_IDENTIFIER = 'IFA_SELECT'
   and MODULE = 'REG';

update PPBASE_MENU_ITEMS
   set LABEL = 'IFA - Review', MINIHELP = 'IFA - Review', ITEM_ORDER = 999
 where MENU_IDENTIFIER = 'IFA_REVIEW'
   and MODULE = 'REG';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 3
 where MENU_IDENTIFIER = 'ifa_depr_select'
   and MODULE = 'REG';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 4
 where MENU_IDENTIFIER = 'IFA_REVIEW'
   and MODULE = 'REG';

update PPBASE_WORKSPACE
   set LABEL = 'IFA - FP Select', MINIHELP = 'IFA - FP Select'
 where WORKSPACE_IDENTIFIER = 'uo_reg_inc_adj_ws'
   and MODULE = 'REG';

update PPBASE_WORKSPACE
   set LABEL = 'IFA - Review', MINIHELP = 'IFA - Review'
 where WORKSPACE_IDENTIFIER = 'uo_reg_inc_ifa_fp_review'
   and MODULE = 'REG';

update PPBASE_WORKSPACE
   set MINIHELP = 'IFA - Forecast Depreciation Select'
 where WORKSPACE_IDENTIFIER = 'uo_reg_fcst_depr_inc_adj_ws'
   and MODULE = 'REG';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1337, 0, 10, 4, 2, 7, 39507, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039507_reg_depr_ifa_relationships.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
