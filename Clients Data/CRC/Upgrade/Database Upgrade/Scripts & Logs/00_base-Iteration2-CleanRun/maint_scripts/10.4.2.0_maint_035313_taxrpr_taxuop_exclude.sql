/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035313_taxrpr_taxuop_exclude.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/13/2014 Alex P.        Point Release
||============================================================================
*/

update PPBASE_MENU_ITEMS
   set LABEL = 'WO-TaxUOP Exclusions', MINIHELP = 'Work Order - Tax Unit of Property Exclusions'
 where MODULE = 'REPAIRS'
   and LABEL = 'WO-RUC Exeptions';

update PPBASE_WORKSPACE
   set LABEL = 'WO-TaxUOP Exclusions', MINIHELP = 'Work Order - Tax Unit of Property Exclusions'
 where MODULE = 'REPAIRS'
   and LABEL = 'WO-RUC Exceptions';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (848, 0, 10, 4, 2, 0, 35313, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035313_taxrpr_taxuop_exclude.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

