SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   02/11/2013 Blake Andrews
|| 10.4.0.0   05/30/2013 Lee Quinn       Changed insert into COST_ELEMENT
||                                       added NVL to max(COST_ELEMENT_ID)
||============================================================================
*/

-- System control is no longer used in the code
delete PP_SYSTEM_CONTROL_COMPANY where CONTROL_NAME = 'Tax Expense - Threshold Date Check';

-- New columns used for reporting
alter table REPAIR_CALC_ASSET
   add (TOTAL_ADD_COST number(22,2),
        AFUDC_EQUITY   number(22,2),
        AFUDC_DEBT     number(22,2),
        OTHER_BOOK     number(22,2),
        CPI            number(22,2),
        OTHER_TAX      number(22,2));

-- Create sequences.
--*
--|| Dynamic SQL to create RPR_REPAIR_SCHEMA_SEQ sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);
   LOOPING_CHAIN_SYN exception;
   pragma exception_init(LOOPING_CHAIN_SYN, -01775);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select nvl(max(repair_schema_id), 0) from repair_schema')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS or LOOPING_CHAIN_SYN then
         DBMS_OUTPUT.PUT_LINE('Table REPAIR_SCHEMA doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 100;

   SQLS := 'create SEQUENCE PWRPLANT.RPR_REPAIR_SCHEMA_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--*
--|| Dynamic SQL to create RPR_TAX_STATUS_SEQ sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);
   LOOPING_CHAIN_SYN exception;
   pragma exception_init(LOOPING_CHAIN_SYN, -01775);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select nvl(max(tax_status_id), 0) from wo_tax_status')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS or LOOPING_CHAIN_SYN then
         DBMS_OUTPUT.PUT_LINE('Table WO_TAX_STATUS doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 100;

   SQLS := 'create SEQUENCE PWRPLANT.RPR_TAX_STATUS_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--*
--|| Dynamic SQL to create RPR_RANGE_TEST_SEQ sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);
   LOOPING_CHAIN_SYN exception;
   pragma exception_init(LOOPING_CHAIN_SYN, -01775);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select nvl(max(range_test_id), 0) from repair_range_test_header')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS or LOOPING_CHAIN_SYN then
         DBMS_OUTPUT.PUT_LINE('Table REPAIR_RANGE_TEST_HEADER doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 100;

   SQLS := 'create SEQUENCE PWRPLANT.RPR_RANGE_TEST_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--*
--|| Dynamic SQL to create RPR_REPAIR_UNIT_CODE_SEQ sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);
   LOOPING_CHAIN_SYN exception;
   pragma exception_init(LOOPING_CHAIN_SYN, -01775);


   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select nvl(max(repair_unit_code_id), 0) from repair_unit_code')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS or LOOPING_CHAIN_SYN then
         DBMS_OUTPUT.PUT_LINE('Table REPAIR_UNIT_CODE doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 100;

   SQLS := 'create SEQUENCE PWRPLANT.RPR_REPAIR_UNIT_CODE_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--*
--|| Dynamic SQL to create RPR_REPAIR_LOCATION_SEQ sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);
   LOOPING_CHAIN_SYN exception;
   pragma exception_init(LOOPING_CHAIN_SYN, -01775);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select nvl(max(repair_location_id), 0) from repair_location')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS or LOOPING_CHAIN_SYN then
         DBMS_OUTPUT.PUT_LINE('Table REPAIR_LOCATION doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 100;

   SQLS := 'create SEQUENCE PWRPLANT.RPR_REPAIR_LOCATION_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--*
--|| Dynamic SQL to create RPR_REPAIR_LOC_ROLLUP_SEQ sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);
   LOOPING_CHAIN_SYN exception;
   pragma exception_init(LOOPING_CHAIN_SYN, -01775);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select nvl(max(repair_loc_rollup_id), 0) from repair_loc_rollup')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS or LOOPING_CHAIN_SYN then
         DBMS_OUTPUT.PUT_LINE('Table REPAIR_LOC_ROLLUP doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 100;

   SQLS := 'create SEQUENCE PWRPLANT.RPR_REPAIR_LOC_ROLLUP_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--*
--|| Dynamic SQL to create RPR_TAX_EXPENSE_TEST_SEQ sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);
   LOOPING_CHAIN_SYN exception;
   pragma exception_init(LOOPING_CHAIN_SYN, -01775);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select nvl(max(tax_expense_test_id), 0) from wo_tax_expense_test')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS or LOOPING_CHAIN_SYN then
         DBMS_OUTPUT.PUT_LINE('Table WO_TAX_EXPENSE_TEST doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 100;

   SQLS := 'create SEQUENCE PWRPLANT.RPR_TAX_EXPENSE_TEST_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/



-- Create a table to store the range test header information.  This is currently stored on repair_range_test, but there may be multiple rows for one range test.
create table REPAIR_RANGE_TEST_HEADER
(
 RANGE_TEST_ID number(22,0)   not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 DESCRIPTION   varchar2(35)   not null
);

alter table REPAIR_RANGE_TEST_HEADER
   add constraint REPAIR_RANGE_TEST_HDR_PK
       primary key (RANGE_TEST_ID)
       using index tablespace PWRPLANT_IDX;

-- Insert into the header table for any existing range tests.
insert into PWRPLANT.REPAIR_RANGE_TEST_HEADER
   (RANGE_TEST_ID, DESCRIPTION)
   (select distinct RANGE_TEST_ID, DESCRIPTION from PWRPLANT.REPAIR_RANGE_TEST);

-- Add a foreign key to this table from other tables with range_test_id.
alter table REPAIR_RANGE_TEST
   add constraint REP_RANGE_TEST_RT_ID
       foreign key (RANGE_TEST_ID)
       references REPAIR_RANGE_TEST_HEADER;

alter table REPAIR_UNIT_CODE
   add constraint REP_UNIT_CODE_RT_ID
       foreign key (RANGE_TEST_ID)
       references REPAIR_RANGE_TEST_HEADER;

-- Drop the description column on repair_range_test, as this field is now on the header table.
alter table REPAIR_RANGE_TEST drop column DESCRIPTION;


-- ***************************************** Blake: repairs_prototype_base_data.sql **********************************************************
-- Changed 05302013 - NVL to max(BOOK_SUMMARY_ID)
insert into BOOK_SUMMARY
   (BOOK_SUMMARY_ID, SUMMARY_NAME, RECONCILE_ITEM_ID, BOOK_SUMMARY_TYPE, CURRENCY_TYPE_ID,
    LONG_DESCRIPTION, CPI_RECALC_ALLOC_BASIS, EXCLUDE_FROM_TREND)
   select BS_MAX.MAX_ID + ROWNUM BOOK_SUMMARY_ID,
          SUMMARY_NAME,
          null RECONCILE_ITEM_ID,
          'Standard' BOOK_SUMMARY_TYPE,
          1 CURRENCY_TYPE_ID,
          replace(SUMMARY_NAME, 'xx', null) LONG_DESCRIPTION,
          DECODE(SUMMARY_NAME, 'xxTax Repairsxx', 1, 'xxCPI - Reversalxx', 1, null) CPI_RECALC_ALLOC_BASIS,
          0 EXCLUDE_FROM_TREND
     from (select NVL(max(BOOK_SUMMARY_ID), 0) MAX_ID from BOOK_SUMMARY) BS_MAX,
          (select 'xxTax Repairsxx' SUMMARY_NAME
             from DUAL
            where not exists
            (select 1 from REPAIR_BOOK_SUMMARY where LOWER(UPDATE_METHOD) = 'add qualified')
              and not exists (select 1 from BOOK_SUMMARY where SUMMARY_NAME = 'Tax Repairs')
           union all
           select 'xxTax Repairs Reversalxx' SUMMARY_NAME
             from DUAL
            where not exists
            (select 1 from REPAIR_BOOK_SUMMARY where LOWER(UPDATE_METHOD) = 'reverse')
              and not exists
            (select 1 from BOOK_SUMMARY where SUMMARY_NAME = 'Tax Repairs Reversal')
           union all
           select 'xxRetirement Reversalxx' SUMMARY_NAME
             from DUAL
            where not exists (select 1
                     from REPAIR_BOOK_SUMMARY
                    where LOWER(UPDATE_METHOD) = 'retirement reversal')
              and not exists
            (select 1 from BOOK_SUMMARY where SUMMARY_NAME = 'Retirement Reversal'));
commit;

--Possibly need to enter a value in tax_reconcile_item here.
--Also need to update book_summary.reconcile_item_id for 'Tax Repairs' to be equal to the Tax Repairs reconcile_item_id in tax_reconcile_item

-- Changed 05302013 - NVL to max(CHARGE_TYPE_ID)
insert into CHARGE_TYPE
   (CHARGE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, BOOK_SUMMARY_ID, GROUP_INDICATOR,
    EXCLUDE_FROM_TOTAL_CHARGES, PROCESSING_TYPE_ID, TRIGGER_FOR_AFUDC, TRIGGER_FOR_CPI,
    CURRENCY_TYPE_ID, IDLE_AFUDC_DIRECT, IDLE_CPI_DIRECT)
   select MAX_ID + ROWNUM,
          LONG_DESCRIPTION,
          LONG_DESCRIPTION,
          BOOK_SUMMARY_ID,
          1,
          0,
          5,
          0,
          0,
          1,
          0,
          0
     from BOOK_SUMMARY, (select NVL(max(CHARGE_TYPE_ID), 0) MAX_ID from CHARGE_TYPE)
    where SUMMARY_NAME in
          ('xxTax Repairsxx', 'xxTax Repairs Reversalxx', 'xxRetirement Reversalxx');
commit;

insert into CHARGE_TYPE_DATA
   (CHARGE_TYPE_ID, EFFECTIVE_DATE, PERCENT_TAX_BASIS, PERCENT_TAX_REV_EXP, PERCENT_BOOK_BASIS,
    AFUDC_ELIG_INDICATOR, CO_TENANCY_ELIGIBLE, CPI_ELIG_INDICATOR)
   select CHARGE_TYPE_ID, '01-JAN-1970', 1, 1, 1, 0, 0, 1
     from CHARGE_TYPE
    where BOOK_SUMMARY_ID in
          (select BOOK_SUMMARY_ID
             from BOOK_SUMMARY
            where SUMMARY_NAME in
                  ('xxTax Repairsxx', 'xxTax Repairs Reversalxx', 'xxRetirement Reversalxx'));

-- Changed 05302013 - NVL to max(COST_ELEMENT_ID)
insert into COST_ELEMENT
   (COST_ELEMENT_ID, CHARGE_TYPE_ID, EXTERNAL_COST_ELEMENT, LONG_DESCRIPTION, DESCRIPTION,
    CURRENCY_TYPE_ID)
   select MAX_ID + ROWNUM, CHARGE_TYPE_ID, MAX_ID + ROWNUM, LONG_DESCRIPTION, DESCRIPTION, 1
     from CHARGE_TYPE, (select NVL(max(COST_ELEMENT_ID), 0) MAX_ID from COST_ELEMENT)
    where BOOK_SUMMARY_ID in
          (select BOOK_SUMMARY_ID
             from BOOK_SUMMARY
            where SUMMARY_NAME in
                  ('xxTax Repairsxx', 'xxTax Repairs Reversalxx', 'xxRetirement Reversalxx'));
commit;

update BOOK_SUMMARY
   set SUMMARY_NAME = replace(SUMMARY_NAME, 'xx', null)
 where SUMMARY_NAME in ('xxTax Repairsxx', 'xxTax Repairs Reversalxx', 'xxRetirement Reversalxx');
commit;

--If the charge types, book summaries, and cost elements already existed, we need to update the descriptions to be consistent at all clients
update BOOK_SUMMARY
   set SUMMARY_NAME = 'Tax Repairs'
 where BOOK_SUMMARY_ID = (select distinct BOOK_SUMMARY_ID_OUTPUT
                            from REPAIR_BOOK_SUMMARY
                           where LOWER(UPDATE_METHOD) = 'add qualified');
commit;

update BOOK_SUMMARY
   set SUMMARY_NAME = 'Tax Repairs Reversal'
 where BOOK_SUMMARY_ID = (select distinct BOOK_SUMMARY_ID_OUTPUT
                            from REPAIR_BOOK_SUMMARY
                           where LOWER(UPDATE_METHOD) = 'reverse');
commit;

update BOOK_SUMMARY
   set SUMMARY_NAME = 'Retirement Reversal'
 where BOOK_SUMMARY_ID = (select distinct BOOK_SUMMARY_ID_OUTPUT
                            from REPAIR_BOOK_SUMMARY
                           where LOWER(UPDATE_METHOD) = 'retirement reversal');
commit;

update CHARGE_TYPE A
   set DESCRIPTION =
        (select B.SUMMARY_NAME
           from BOOK_SUMMARY B
          where A.BOOK_SUMMARY_ID = B.BOOK_SUMMARY_ID
            and B.SUMMARY_NAME in ('Tax Repairs', 'Tax Repairs Reversal', 'Retirement Reversal'))
 where exists
 (select 1
          from BOOK_SUMMARY B
         where A.BOOK_SUMMARY_ID = B.BOOK_SUMMARY_ID
           and B.SUMMARY_NAME in ('Tax Repairs', 'Tax Repairs Reversal', 'Retirement Reversal'));
commit;

update COST_ELEMENT A
   set DESCRIPTION =
        (select B.DESCRIPTION
           from CHARGE_TYPE B
          where A.CHARGE_TYPE_ID = B.CHARGE_TYPE_ID
            and B.DESCRIPTION in ('Tax Repairs', 'Tax Repairs Reversal', 'Retirement Reversal'))
 where exists
 (select 1
          from CHARGE_TYPE B
         where A.CHARGE_TYPE_ID = B.CHARGE_TYPE_ID
           and B.DESCRIPTION in ('Tax Repairs', 'Tax Repairs Reversal', 'Retirement Reversal'));
commit;


-- ************************************************* Blake: repairs_prototype_posting *****************************************************
insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('TAXREP POST UPDATE CWIP CHARGE', 1, '/*+ INDEX ( a pk_repair_calc_asset )  */');
commit;

insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('TAXREP UPDATE INSERT_ACTIVITY_ID', 0, ' ');
commit;

insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('TAXREP UPDATE CPR_ACTIVITY - CAPITAL', 0, ' ');
commit;

insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('TAXREP UPDATE CPR_ACTIVITY - EXPENSE', 0, ' ');
commit;

insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('TAXREP INSERT CLASS_CODE_CPR_LEDGER', 0, ' ');
commit;

insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('TAXREP DELETE CLASS_CODE_CPR_LEDGER', 0, ' ');

--************************************************************ Blake: repairs_prototype_work_order_tax_repairs.sql ****************************************
create table WORK_ORDER_TAX_REPAIRS
(
 WORK_ORDER_ID              number(22) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 TAX_EXPENSE_TEST_ID        number(22),
 REPAIR_LOCATION_METHOD_ID  number(22),
 REPAIR_LOCATION_ID         number(22), -- <-- This will be a repair_location_rollup_id if tax_expense_test is a blanket test, otherwise, it's a repair location (or something that says use asset location or wms location)
 TE_AGGREGATION_ID          number(22)  -- <-- Specifies whether or not we're going to aggregate work orders under a specific funding project to do our tax repairs test
);

alter table WORK_ORDER_TAX_REPAIRS
   add constraint WO_TAX_REPAIRS_PK
       primary key (WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

create table WO_REPAIR_LOCATION_METHOD
(
 REPAIR_LOCATION_METHOD_ID number(22),
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 DESCRIPTION               varchar2(35)
);

alter table WO_REPAIR_LOCATION_METHOD
   add constraint WO_RPR_LOC_METH_PK
       primary key (REPAIR_LOCATION_METHOD_ID)
       using index tablespace PWRPLANT_IDX;

insert into WO_REPAIR_LOCATION_METHOD
   (REPAIR_LOCATION_METHOD_ID, DESCRIPTION)
   select 1, 'Work Order Repair Location' from DUAL union all
   select 2, 'Asset Repair Location' from DUAL union all
   select 3, 'WMS Repair Location' from DUAL;

alter table WORK_ORDER_TAX_REPAIRS
   add constraint WO_TAX_REP_TEST_FK
       foreign key(TAX_EXPENSE_TEST_ID)
       references WO_TAX_EXPENSE_TEST(TAX_EXPENSE_TEST_ID);

alter table WORK_ORDER_TAX_REPAIRS
   add constraint WO_TAX_REP_WO_FK
       foreign KEY(WORK_ORDER_ID)
       references WORK_ORDER_CONTROL(WORK_ORDER_ID);

alter table WORK_ORDER_TAX_REPAIRS
   add constraint WO_TAX_REP_LOC_FK
       foreign KEY(REPAIR_LOCATION_METHOD_ID)
       references WO_REPAIR_LOCATION_METHOD(REPAIR_LOCATION_METHOD_ID);

alter table WORK_ORDER_TAX_REPAIRS
   add constraint WO_TAX_REP_AGG_FK
      foreign key(TE_AGGREGATION_ID)
      references TE_AGGREGATION(TE_AGGREGATION_ID);

alter table WORK_ORDER_TYPE add REPAIR_LOCATION_METHOD_ID number(22);

alter table WORK_ORDER_TYPE
   add constraint WO_TYPE_REP_LOC_METH_FK
       foreign KEY(REPAIR_LOCATION_METHOD_ID)
       references WO_REPAIR_LOCATION_METHOD(REPAIR_LOCATION_METHOD_ID);

alter table REPAIR_WORK_ORDER_TEMP_ORIG add ASSET_REPAIR_LOCATION_ID number(22);
alter table REPAIR_WORK_ORDER_TEMP_ORIG add REPAIR_LOCATION_METHOD_ID number(22);
alter table REPAIR_WORK_ORDER_TEMP_ORIG add TOTAL_PERIOD_REVERSE_EXPENSE number(22,2);

alter table REPAIR_WORK_ORDER_TEMP add TOTAL_PERIOD_REVERSE_EXPENSE number(22,2);

alter table REPAIR_WORK_ORDER_SEGMENTS add TOTAL_PERIOD_REVERSE_EXPENSE number(22,2);

-- **************************************** Blake: repairs_testing_script_05.sql ************************************************
alter table REPAIR_WORK_ORDER_TEMP_ORIG
   add (ADD_RETIRE_TOLERANCE_PCT number(22,2),
        EXPENSE_PERCENT          number(22,8),
        ADD_RETIRE_CIRCUIT_PCT   number(22,8));

alter table REPAIR_WORK_ORDER_TEMP
   add (ADD_RETIRE_TOLERANCE_PCT number(22,2),
        EXPENSE_PERCENT          number(22,8),
        ADD_RETIRE_CIRCUIT_PCT   number(22,8));

-- ******************************************** Blake: repairs_testing_script_06.sql **********************************************
alter table REPAIR_WORK_ORDER_TEMP_ORIG
   add (
        TAX_ONLY_BASIS_DIFFS   number(22,2),  --This is the amount of tax-only basis difference activity on the record
        AFUDC_EQUITY_ACTIVITY  number(22,2),  --The amount of AFUDC Equity on the record (this is only stored for reporting purposes)
        AFUDC_DEBT_ACTIVITY    number(22,2),  --The amount of AFUDC Debt on the record (this is only stored for reporting purposes)
        CPI_ACTIVITY           number(22,2),  --The amount of CPI activity on the record (this is only stored for reporting purposes)
        OTHER_BOOK_BASIS_DIFFS number(22,2),  --The amount of other tax-only basis differences (excluding CPI) on the record (this is only stored for reporting purposes)
        OTHER_TAX_BASIS_DIFFS  number(22,2),  --The amount of other book basis differences (excluding AFUDC) activity on the record (this is only stored for reporting purposes)
        GROSS_BOOK_COST        number(22,2)   --The total book cost of the record, including AFUDC and other basis differences
       );

alter table REPAIR_WORK_ORDER_TEMP
   add (
        TAX_ONLY_BASIS_DIFFS   number(22,2),  --This is the amount of tax-only basis difference activity on the record
        AFUDC_EQUITY_ACTIVITY  number(22,2),  --The amount of AFUDC Equity on the record (this is only stored for reporting purposes)
        AFUDC_DEBT_ACTIVITY    number(22,2),  --The amount of AFUDC Debt on the record (this is only stored for reporting purposes)
        CPI_ACTIVITY           number(22,2),  --The amount of CPI activity on the record (this is only stored for reporting purposes)
        OTHER_BOOK_BASIS_DIFFS number(22,2),  --The amount of other tax-only basis differences (excluding CPI) on the record (this is only stored for reporting purposes)
        OTHER_TAX_BASIS_DIFFS  number(22,2),  --The amount of other book basis differences (excluding AFUDC) activity on the record (this is only stored for reporting purposes)
        GROSS_BOOK_COST        number(22,2)   --The total book cost of the record, including AFUDC and other basis differences
       );

alter table REPAIR_WORK_ORDER_SEGMENTS
   add (
        TAX_ONLY_BASIS_DIFFS   number(22,2),  --This is the amount of tax-only basis difference activity on the record
        AFUDC_EQUITY_ACTIVITY  number(22,2),  --The amount of AFUDC Equity on the record (this is only stored for reporting purposes)
        AFUDC_DEBT_ACTIVITY    number(22,2),  --The amount of AFUDC Debt on the record (this is only stored for reporting purposes)
        CPI_ACTIVITY           number(22,2),  --The amount of CPI activity on the record (this is only stored for reporting purposes)
        OTHER_BOOK_BASIS_DIFFS number(22,2),  --The amount of other tax-only basis differences (excluding CPI) on the record (this is only stored for reporting purposes)
        OTHER_TAX_BASIS_DIFFS  number(22,2),  --The amount of other book basis differences (excluding AFUDC) activity on the record (this is only stored for reporting purposes)
        GROSS_BOOK_COST        number(22,2)   --The total book cost of the record, including AFUDC and other basis differences
       );

alter table REPAIR_MAJOR_UNIT_PCT
   add (
        MAJOR_CPI_TAKEN   number(22,2),
        MAJOR_TOTAL_CPI   number(22,2),
        MAJOR_CPI_PCT     number(22,2)
       );

-- **************************************************************************************************************************************

-- Create menu and links tables and populate with repairs data.

create table PPBASE_MENU_ITEMS
(
 MODULE            varchar2(35) not null,
 IDENTIFIER        varchar2(35) not null,
 PARENT_IDENTIFIER varchar2(35) not null,
 ITEM_ORDER        number(22,0),
 LABEL             varchar2(100),
 INFO              varchar2(254),
 ENABLE_YN         number(22,0),
 MENU_LEVEL        number(22,0),
 PICTURE_NAME      varchar2(254),
 UO_WKSP_NAME      varchar2(40),
 SPLIT_YN          number(22,0),
 IDENTIFIER_2      varchar2(35),
 UO_WKSP_NAME_2    varchar2(35),
 SPLIT_DIRECTION   varchar2(35)
);

alter table PPBASE_MENU_ITEMS
   add constraint PPBASE_MENU_ITEMS_PK
       primary key (MODULE, IDENTIFIER, PARENT_IDENTIFIER)
       using index tablespace PWRPLANT_IDX;

create table PPBASE_WORKSPACE_LINKS
(
 MODULE VARCHAR2(35) NOT NULL,
 IDENTIFIER VARCHAR2(35) NOT NULL,
 PARENT_IDENTIFIER VARCHAR2(35) NOT NULL,
 ITEM_ORDER NUMBER(22,0),
 LABEL VARCHAR2(100),
 UO_WKSP_NAME VARCHAR2(35),
 SPLIT_YN NUMBER(22,0),
 IDENTIFIER_2 VARCHAR2(35),
 UO_WKSP_NAME_2 VARCHAR2(35),
 SPLIT_DIRECTION VARCHAR2(35)
);

alter table PPBASE_WORKSPACE_LINKS
   add constraint PPBASE_WORKSPACE_LINKS_PK
      primary key (MODULE, IDENTIFIER, PARENT_IDENTIFIER)
      using index tablespace PWRPLANT_IDX;

insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_tax_status', 'menu_wksp_work_order', 1, 'Tax Status',
    'Tax Status Configuration', 1, 3, null, 'uo_rpr_config_wksp_tax_status', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_range_test', 'menu_wksp_work_order', 2, 'Range Test',
    'Range Test Configuration', 1, 3, null, 'uo_rpr_config_wksp_range_test', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_tax_loc_rollups', 'menu_wksp_asset', 2, 'Repair Location Rollups',
    'Repair Location Rollup Configuration for Blanket Allocations', 1, 3, null,
    'uo_rpr_config_wksp_rpr_loc_rollup', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_tax_units_work_order', 'menu_wksp_work_order', 3,
    'Tax Units of Property (Pre)', 'Tax Units of Property Configuration', 1, 3, null,
    'uo_rpr_config_wksp_unit_code', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_book_summary', 'menu_wksp_config', 1, 'Book Summary',
    'Repair Book Summary Configuration', 1, 2, null, 'uo_rpr_config_wksp_book_summary', null, null,
    null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_repair_schema', 'menu_wksp_config', 2, 'Repair Schema',
    'Repair Schema Configuration', 1, 2, null, 'uo_rpr_config_wksp_repair_schema', null, null, null,
    null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_tax_locations', 'menu_wksp_config', 3, 'Repair Locations',
    'Repair Location Configuration', 1, 2, null, 'uo_rpr_config_wksp_rpr_location', null, null, null,
    null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_threshold_estimate', 'menu_wksp_config', 4, 'Threshold Estimate',
    'Thresholds Estimate Configuration', 1, 2, null, 'uo_rpr_config_wksp_threshold_estimate', null,
    null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_tax_units_asset', 'menu_wksp_asset', 1, 'Tax Units of Property (Post)',
    'Tax Units of Property Configuration', 1, 3, null, 'uo_rpr_config_wksp_unit_code', null, null,
    null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_tax_exp_tests', 'menu_wksp_config', 5, 'Repair Tests',
    'Repair Test Configuration', 1, 2, null, 'uo_rpr_config_wksp_repair_test', null, null, null,
    null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_import', 'repairs_top', 3, 'Import', 'Import Center', 1, 1, null,
    'uo_rpr_import_wksp_main', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_tax_thresholds_asset', 'menu_wksp_asset', 3, 'Tax Thresholds (Post)',
    'Tax Threshold Configuration', 1, 3, null, 'uo_rpr_config_wksp_threshold', null, null, null,
    null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_tax_thresholds_work_order', 'menu_wksp_work_order', 4,
    'Tax Thresholds (Pre)', 'Tax Threshold Configuration', 1, 3, null,
    'uo_rpr_config_wksp_threshold', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_home', 'repairs_top', 1, 'Home', 'Home', 1, 1, null, null, null, null,
    null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_config', 'repairs_top', 2, 'Config', 'Configuration Center', 1, 1, null,
    null, null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_processing', 'repairs_top', 4, 'Processing', 'Processing Center', 1, 1,
    null, 'uo_rpr_process_wksp_main', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_reporting', 'repairs_top', 5, 'Reporting', 'Reporting Center', 1, 1, null,
    'uo_rpr_rptcntr_wksp_report', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_posting', 'repairs_top', 6, 'Posting', 'Posting Center', 1, 1, null,
    'uo_ayp_test2', null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_work_order', 'menu_wksp_config', 6, 'Pre in-service',
    'Work Order Configuration Workspaces', 1, 2, null, null, null, null, null, null);
insert into PPBASE_MENU_ITEMS
   (MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL,
    PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
values
   ('REPAIRS', 'menu_wksp_asset', 'menu_wksp_config', 7, 'Post in-service',
    'Asset Configuration Workspaces', 1, 2, null, null, null, null, null, null);

--************************************** Mark required fields ***************************************************
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select NVL(max(ID),0) + 1,
          'wo_tax_status',
          'priority',
          null,
          null,
          null,
          'uo_rpr_config_wksp_tax_status.dw_details',
          'Priority'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_range_test_header',
          'description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_range_test.dw_details',
          'Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_schema',
          'description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_schema.dw_details',
          'Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_schema',
          'status',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_schema.dw_details',
          'Status'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_schema',
          'usage_flag',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_schema.dw_details',
          'Usage Flag'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'repair_schema_id',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Repair Schema'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_loc_rollup',
          'description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_rpr_loc_rollup.dw_details',
          'Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_loc_rollup',
          'long_description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_rpr_loc_rollup.dw_details',
          'Long Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_location',
          'description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_rpr_location.dw_details',
          'Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_location',
          'long_description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_rpr_location.dw_details',
          'Long Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_unit_code',
          'description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_unit_code.dw_details',
          'Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'repair_unit_code',
          'long_description',
          null,
          null,
          null,
          'uo_rpr_config_wksp_unit_code.dw_details',
          'Long Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'tax_status_from_fp',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Tax Status From FP'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'estimate_dollars_vs_units',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Estimate Dollars vs. Units'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'est_revision',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Est Revision'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'replacement_check',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Replacement Check'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'repair_unit_code_priority',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Repair Unit Code Priority'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'replacement_flag_priority',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Replacement Flag Priority'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'tax_status_no_replacement',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Tax Status No Replacement'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'tax_status_fp_fully_qualifying',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Tax Status FP Fully Qualifying'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'tax_status_fp_non_qualifying',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Tax Status FP Non Qualifying'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'tax_status_minor_uop',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Tax Status Minor Unit of Property'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'tax_status_ovh_to_udg',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Tax Status Overhead to Underground'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'tax_status_default_capital',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Tax Status Default Capital'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_status',
          'description',
          null,
          TO_TIMESTAMP('1-12-2012 23:45:28', 'DD-MM-YYYY HH24:MI:SS'),
          'PWRPLANT',
          'uo_rpr_config_wksp_tax_status.dw_details',
          'Description'
     from PP_REQUIRED_TABLE_COLUMN;
insert into PP_REQUIRED_TABLE_COLUMN
   (ID, TABLE_NAME, COLUMN_NAME, COLOR_NUMBER, TIME_STAMP, USER_ID, OBJECTPATH, DESCRIPTION)
   select max(ID) + 1,
          'wo_tax_expense_test',
          'expense_percent',
          null,
          null,
          null,
          'uo_rpr_config_wksp_repair_test.dw_details',
          'Expense Percent'
     from PP_REQUIRED_TABLE_COLUMN;

create table TAX_REVERSAL_BOOK_SUMMARY
(
 BOOK_SUMMARY_ID          number(22) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 REVERSAL_BOOK_SUMMARY_ID number(22),
 TAX_REVERSAL_TYPE        varchar2(35)
);

alter table TAX_REVERSAL_BOOK_SUMMARY
   add constraint TAX_REV_BOOK_SUMMARY_PK
       primary key (BOOK_SUMMARY_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_REVERSAL_BOOK_SUMMARY
   add constraint TAX_REV_BOOK_SUMMARY_BS_FK
       foreign key (BOOK_SUMMARY_ID)
       references BOOK_SUMMARY(BOOK_SUMMARY_ID);

alter table TAX_REVERSAL_BOOK_SUMMARY
   add constraint TAX_REV_BOOK_SUMMARY_BS_FK2
       foreign key (REVERSAL_BOOK_SUMMARY_ID)
       references BOOK_SUMMARY(BOOK_SUMMARY_ID);

insert into TAX_REVERSAL_BOOK_SUMMARY
   (BOOK_SUMMARY_ID, TIME_STAMP, USER_ID, REVERSAL_BOOK_SUMMARY_ID, TAX_REVERSAL_TYPE)
   select distinct BOOK_SUMMARY_ID, sysdate, user, BOOK_SUMMARY_ID_OUTPUT, 'Repairs'
     from REPAIR_BOOK_SUMMARY
    where UPDATE_METHOD = 'reverse';

--********************************** Reporting scripts *****************************

insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (40, TO_TIMESTAMP('18-01-2013 15:01:59', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'PP - Single - DDDW');
insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (41, TO_TIMESTAMP('18-01-2013 15:01:59', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'PP - Span - DDDW');
insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (42, TO_TIMESTAMP('18-01-2013 15:01:59', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'PP - Single - DP');
insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (43, TO_TIMESTAMP('18-01-2013 15:01:59', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'PP - Span - DP');
insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (44, TO_TIMESTAMP('18-01-2013 15:01:59', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'PP - Multi - DW');
insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (45, TO_TIMESTAMP('18-01-2013 15:01:59', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 'PP - Single - DW');

insert into PP_REPORT_TYPE (REPORT_TYPE_ID, DESCRIPTION) values (100, 'Tax Repairs');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (30, 'Repairs - Netwk - Batch', 'repair_wo_seg_reporting');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (31, 'Repairs - Netwk - Company', 'repair_thresholds');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (32, 'Repairs - Blnkt - Batch', 'repair_blkt_results_reporting');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (33, 'Repairs - WMIS - Batch', 'repair_wo_seg_reporting');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (34, 'Repairs - WO', 'work_order_control');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (36, 'Repairs - WO - Processing', 'work_order_control');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (37, 'Repairs - CPR', 'cpr_ledger');

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (38, 'Repairs - CWIP - Company', 'repair_thresholds');

update PP_REPORTS set DATAWINDOW = 'dw_rep_basis_buckets_mapping', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0001';
update PP_REPORTS set DATAWINDOW = 'dw_rep_b2t_uapu', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0002';
update PP_REPORTS set DATAWINDOW = 'dw_rep_b2t_unit_map_pg', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0003';
update PP_REPORTS set DATAWINDOW = 'dw_rep_range_test', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0004';
update PP_REPORTS set DATAWINDOW = 'dw_rep_rel_wo_diff_tax_stat', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0005';
update PP_REPORTS set DATAWINDOW = 'dw_rep_repl_cost_act_est_ratio', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 34 where REPORT_NUMBER = 'REP-0007';
update PP_REPORTS set DATAWINDOW = 'dw_rep_repl_flag_no_ret_est', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 34 where REPORT_NUMBER = 'REP-0008';
update PP_REPORTS set DATAWINDOW = 'dw_rep_replacement_costs', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0009';
update PP_REPORTS set DATAWINDOW = 'dw_rep_tax_locations', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0010';
update PP_REPORTS set DATAWINDOW = 'dw_rep_tax_locations_rep_loc', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0011';
update PP_REPORTS set DATAWINDOW = 'dw_rep_tax_stat_test_tax_stat', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0012';
update PP_REPORTS set DATAWINDOW = 'dw_rep_tax_statuses', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0013';
update PP_REPORTS set DATAWINDOW = 'dw_rep_tax_unit_2_func_class', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0014';
update PP_REPORTS set DATAWINDOW = 'dw_rep_tax_units_detail', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0015';
update PP_REPORTS set DATAWINDOW = 'dw_rep_unassigned_tax_locs', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0016';
update PP_REPORTS set DATAWINDOW = 'dw_rep_work_order_type_map', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0017';
update PP_REPORTS set DATAWINDOW = 'dw_rep_cap_proj_in_period', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 34, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REP-0018';
update PP_REPORTS set DATAWINDOW = 'dw_rep_exp_amt_period', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 34, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REP-0019';
update PP_REPORTS set DATAWINDOW = 'dw_rep_exp_amt_period_printable', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 34, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REP-0020';
update PP_REPORTS set DATAWINDOW = 'dw_rep_irs_annual_audit', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 34, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REP-0021';
update PP_REPORTS set DATAWINDOW = 'dw_rep_irs_annual_audit_printable', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 34, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REP-0022';
update PP_REPORTS set DATAWINDOW = 'dw_rep_retirements_in_period', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 37, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REP-0023';
update PP_REPORTS set DATAWINDOW = 'dw_rep_unassigned_tax_units', REPORT_TYPE_ID = 100 where REPORT_NUMBER = 'REP-0024';
update PP_REPORTS set DATAWINDOW = 'dw_rep_tax_stat_tax_test_all_comp_1', REPORT_TYPE_ID = 100, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REP-0025';
update PP_REPORTS set DATAWINDOW = 'dw_rep_asset_amt_1', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 37, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REP-0026';

update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_blnkt_uop1', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 32, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_blnkt' where REPORT_NUMBER = 'RBLKT - 0001';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_blnkt_uop2', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 32, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_blnkt' where REPORT_NUMBER = 'RBLKT - 0002';

update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_cwip_wo_stat_hist', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 34, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'SITUS - 0033';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_cwip_repl_cost', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 38, PP_REPORT_TIME_OPTION_ID = 42 where REPORT_NUMBER = 'SITUS - 0035';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_cwip_comp_repl_cost', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 38, PP_REPORT_TIME_OPTION_ID = 42 where REPORT_NUMBER = 'SITUS - 0037';

update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_process', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 30, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_cpr' where REPORT_NUMBER = 'NETWK - 0027';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_process_related', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 30, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_cpr' where REPORT_NUMBER = 'NETWK - 0028';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_circuit_thres', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 30, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_cpr' where REPORT_NUMBER = 'NETWK - 0029';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_circuit_thres_related', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 30, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_cpr' where REPORT_NUMBER = 'NETWK - 0030';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_wo_thres', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 30, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_cpr' where REPORT_NUMBER = 'NETWK - 0031';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_wo_thres_related', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 30, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_cpr' where REPORT_NUMBER = 'NETWK - 0032';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_repl_qty', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 31, PP_REPORT_TIME_OPTION_ID = 42 where REPORT_NUMBER = 'NETWK - 0034';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_comp_repl_qty', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 31, PP_REPORT_TIME_OPTION_ID = 42 where REPORT_NUMBER = 'NETWK - 0036';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_netwk_process_rets', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 30, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_cpr' where REPORT_NUMBER = 'NETWK - 0038';

update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_wmis_uop1', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 33, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_wmis' where REPORT_NUMBER = 'WMIS - 0001';
update PP_REPORTS set DATAWINDOW = 'dw_rpr_rpt_wmis_uop2', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 33, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control_wmis' where REPORT_NUMBER = 'WMIS - 0002';

update PP_REPORTS set DATAWINDOW = 'dw_repairs_posted_orig_cpr_report', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 37, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REPRS - 1201';
update PP_REPORTS set DATAWINDOW = 'dw_repairs_posted_cpr_report', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 37, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REPRS - 1202';
update PP_REPORTS set DATAWINDOW = 'dw_repairs_posted_details_cpr_report', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 37, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REPRS - 1203';
update PP_REPORTS set DATAWINDOW = 'dw_repairs_106_101_batch_report', REPORT_TYPE_ID = 100, PP_REPORT_TIME_OPTION_ID = 45, INPUT_WINDOW = 'dw_rpr_batch_control' where REPORT_NUMBER = 'REPRS - 1204';
update PP_REPORTS set DATAWINDOW = 'dw_repairs_106_101_taxtest_report', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 37, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REPRS - 1205';
update PP_REPORTS set DATAWINDOW = 'dw_repairs_106_101_activity_report', REPORT_TYPE_ID = 100, PP_REPORT_FILTER_ID = 37, PP_REPORT_TIME_OPTION_ID = 43 where REPORT_NUMBER = 'REPRS - 1206';


--********************************************************* PP_DYNAMIC_FILTER *******************************************************************

INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   1,
   'Batch',
   'dw',
   'repair_batch_control.batch_id',
   NULL,
   'dw_rpr_batch_control_cpr',
   'id',
   'description',
   'N',
   1,
   1,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   2,
   'Repair Schema',
   'dw',
   'repair_schema.repair_schema_id',
   NULL,
   'dw_rpr_repair_schema_filter',
   'repair_schema_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   3,
   'Company',
   'dw',
   'company.company_id',
   NULL,
   'dw_rpr_company_filter',
   'company_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   4,
   'Work Order Number',
   'sle',
   'work_order_control.work_order_number',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   5,
   'Repair Location',
   'dw',
   'repair_location.repair_location_id',
   NULL,
   'dw_rpr_repair_location_filter',
   'repair_location_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   6,
   'Repair Unit Code',
   'dw',
   'repair_unit_code.repair_unit_code_id',
   NULL,
   'dw_rpr_repair_unit_code_filter',
   'repair_unit_code_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   7,
   'Quantity : Add',
   'operation',
   'repair_wo_seg_reporting.add_quantity',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   8,
   'Quantity : Retire',
   'operation',
   'repair_wo_seg_reporting.retire_quantity',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   9,
   'Cost : Add',
   'operation',
   'repair_wo_seg_reporting.add_cost',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   10,
   'Cost : Retire',
   'operation',
   'repair_wo_seg_reporting.retire_cost',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   11,
   'Work Request',
   'sle',
   'repair_wo_seg_reporting.work_request',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   12,
   'Ovh To Udg',
   'dw',
   'repair_wo_seg_reporting.ovh_to_udg',
   NULL,
   'dw_rpr_yes_no_filter',
   'yes_no_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   13,
   'Company',
   'dw',
   'company.company_id',
   NULL,
   'dw_rpr_company_filter',
   'company_id',
   'description',
   'N',
   1,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   14,
   'Month(s)',
   'daterange',
   'not_in_where_clause',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   1,
   NULL,
   NULL,
   NULL,
   NULL,
   '<=',
   1);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   15,
   'Batch',
   'dw',
   'repair_batch_control.batch_id',
   NULL,
   'dw_rpr_batch_control_blnkt',
   'id',
   'description',
   'N',
   1,
   1,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   16,
   'Quantity : Add',
   'operation',
   'repair_blkt_results_reporting.add_qty',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   17,
   'Quantity : Retire',
   'operation',
   'repair_blkt_results_reporting.retire_qty',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   18,
   'Cost : Add',
   'operation',
   'repair_blkt_results_reporting.add_cost',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   19,
   'Cost : Retire',
   'operation',
   'repair_blkt_results_reporting.retire_cost',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   20,
   'Blanket Method',
   'sle',
   'repair_blkt_results_reporting.blanket_method',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   21,
   'Expense Percent',
   'operation',
   'repair_blkt_results_reporting.expense_pct',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   22,
   'Batch',
   'dw',
   'repair_batch_control.batch_id',
   NULL,
   'dw_rpr_batch_control_wmis',
   'id',
   'description',
   'N',
   1,
   1,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   23,
   'Repair Location',
   'dw',
   'repair_wo_seg_reporting.repair_location_id',
   NULL,
   'dw_rpr_repair_location_filter',
   'repair_location_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   24,
   'Company',
   'dw',
   'work_order_control.company_id',
   NULL,
   'dw_rpr_company_filter',
   'company_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   25,
   'Work Order ID',
   'operation',
   'work_order_control.work_order_id',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   26,
   'Funding Project ID',
   'operation',
   'work_order_control.funding_wo_id',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   27,
   'Major Location',
   'dw',
   'work_order_control.major_location_id',
   NULL,
   'dw_rpr_major_location_filter',
   'major_location_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   28,
   'Asset Location',
   'dw',
   'work_order_control.asset_location_id',
   NULL,
   'dw_rpr_asset_location_filter',
   'asset_location_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   29,
   'Budget',
   'dw',
   'work_order_control.budget_id',
   NULL,
   'dw_rpr_budget_filter',
   'budget_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   30,
   'Budget Number',
   'dw',
   'work_order_control.budget_id',
   NULL,
   'dw_rpr_budget_number_filter',
   'budget_id',
   'budget_number',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   31,
   'Budget Version',
   'dw',
   'budget_version_fund_proj.budget_version_id',
   'work_order_control.work_order_id in (select budget_version_fund_proj.work_order_id from budget_version_fund_proj where active = 1 and [selected_values] )',
   'dw_rpr_budget_version_filter',
   'budget_version_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   32,
   'Work Order Group',
   'dw',
   'work_order_account.work_order_grp_id',
   'work_order_control.work_order_id in ( select distinct work_order_id from work_order_account where [selected_values] )',
   'dw_rpr_work_order_group_filter',
   'work_order_grp_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   33,
   'Work Order Status',
   'dw',
   'work_order_control.wo_status_id',
   NULL,
   'dw_rpr_work_order_status_filter',
   'wo_status_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   34,
   'Work Order Type',
   'dw',
   'work_order_control.work_order_type_id',
   NULL,
   'dw_rpr_work_order_type_filter',
   'work_order_type_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   35,
   'Month(s)',
   'daterange',
   'not_in_where_clause',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   1,
   NULL,
   NULL,
   NULL,
   NULL,
   'Between',
   1);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   36,
   'Repair Status',
   'dw',
   'nvl(wots.tax_status_id, -1)',
   NULL,
   'dw_rpr_tax_status_none_filter',
   'tax_status_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   37,
   'Funding Project Indicator',
   'dw',
   'work_order_control.funding_wo_indicator',
   NULL,
   'dw_rpr_yes_no_filter',
   'yes_no_id',
   'description',
   'N',
   1,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);

update PP_DYNAMIC_FILTER
   set INPUT_TYPE = 'dw',
       SQLS_COLUMN_EXPRESSION = 'work_order_control.work_order_id',
       DW = 'dw_rpr_work_order_number_filter',
       DW_ID = 'work_order_id',
       DW_DESCRIPTION = 'work_order_number',
       DW_ID_DATATYPE = 'N',
       NOT_RETRIEVE_IMMEDIATE = 1
 where FILTER_ID = 4;

INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   38,
   'Ferc Account',
   'dw',
   'not_in_where_clause',
   NULL,
   'dw_rpr_ferc_sys_of_accts_filter',
   'ferc_sys_of_accts_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   1,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   39,
   'GL Account',
   'dw',
   'cpr_ledger.gl_account_id',
   NULL,
   'dw_rpr_glacct_filter',
   'gl_account_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   40,
   'Business Segment',
   'dw',
   'cpr_ledger.bus_segment_id',
   NULL,
   'dw_rpr_busseg_filter',
   'bus_segment_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   41,
   'Utility Account',
   'dw',
   'cpr_ledger.utility_account_id',
   NULL,
   'dw_rpr_utilacct_filter',
   'utility_account_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   42,
   'Sub Account',
   'dw',
   'cpr_ledger.sub_account_id',
   NULL,
   'dw_rpr_subacct_filter',
   'sub_account_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   43,
   'Functional Class',
   'dw',
   'cpr_ledger.func_class_id',
   NULL,
   'dw_rpr_func_class_filter',
   'func_class_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   44,
   'Property Group',
   'dw',
   'cpr_ledger.property_group_id',
   NULL,
   'dw_rpr_prop_group_filter',
   'property_group_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   45,
   'Property Unit',
   'dw',
   'retirement_unit.property_unit_id',
   'cpr_ledger.retirement_unit_id in ( select retirement_unit_id from retirement_unit where [selected_values] )',
   'dw_rpr_prop_unit_filter',
   'property_unit_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   46,
   'Retirement Unit',
   'dw',
   'cpr_ledger.retirement_unit_id',
   NULL,
   'dw_rpr_retire_unit_filter',
   'retirement_unit_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   47,
   'Depreciation Group',
   'dw',
   'cpr_ledger.depr_group_id',
   NULL,
   'dw_rpr_depr_group_filter',
   'depr_group_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   48,
   'Asset Location',
   'dw',
   'cpr_ledger.asset_location_id',
   NULL,
   'dw_rpr_asset_location_filter',
   'asset_location_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   49,
   'Company',
   'dw',
   'cpr_ledger.company_id',
   NULL,
   'dw_rpr_company_filter',
   'company_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE) VALUES (
   50,
   'Major Location',
   'dw',
   'asset_location.major_location_id',
   'cpr_ledger.asset_location_id in (select asset_location_id from asset_location where [selected_values] )',
   'dw_rpr_major_location_filter',
   'major_location_id',
   'description',
   'N',
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);

update PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'gl_account.ferc_sys_of_accts_id',
       SQLS_WHERE_CLAUSE = 'cpr_ledger.gl_account_id in ( select gl_account_id from gl_account where [selected_values] )',
       EXCLUDE_FROM_WHERE = null
 where FILTER_ID = 38;

INSERT INTO PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP, VALID_OPERATORS, EXCLUDE_FROM_WHERE) VALUES (
   51,
   'Company',
   'dw',
   'c.company_id',
   'company.company_id in ( select c.company_id from company c where [selected_values] union select -1 from dual )',
   'dw_rpr_company_filter',
   'company_id',
   'description',
   'N',
   1,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL,
   NULL);

--************************************************************ PP_DYNAMIC_FILTER_MAPPING ********************************************************

INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   31,
   5,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   31,
   6,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   31,
   13,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   31,
   14,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   2,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   3,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   22,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   2,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   6,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   15,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   16,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   17,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   18,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   19,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   20,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   21,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   23,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   32,
   4,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   3,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   4,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   6,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   7,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   8,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   9,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   10,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   11,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   33,
   12,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   1,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   2,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   3,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   4,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   5,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   6,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   7,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   8,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   9,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   10,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   11,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   30,
   12,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   24,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   25,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   26,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   27,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   28,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   29,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   30,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   32,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   33,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   34,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   37,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   34,
   4,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   6,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   24,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   25,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   26,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   27,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   28,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   29,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   30,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   32,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   33,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   34,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   36,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   37,
   NULL,
   NULL);

delete PP_DYNAMIC_FILTER_MAPPING
 where PP_REPORT_FILTER_ID = 34
   and FILTER_ID = 37;

delete PP_DYNAMIC_FILTER_MAPPING
 where FILTER_ID in (select FILTER_ID
                       from PP_DYNAMIC_FILTER
                      where LOWER(LABEL) = 'batch'
                         or LOWER(LABEL) = 'month(s)');

INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   38,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   39,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   40,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   41,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   42,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   43,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   44,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   45,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   46,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   47,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   48,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   49,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   37,
   50,
   NULL,
   NULL);

update PP_REPORTS set PP_REPORT_FILTER_ID = 34 where PP_REPORT_FILTER_ID = 35;

delete PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 35;

delete PP_REPORTS_FILTER where PP_REPORT_FILTER_ID = 35;

update PP_REPORTS_FILTER set DESCRIPTION = 'Repairs - Netwk' where PP_REPORT_FILTER_ID = 30;

update PP_REPORTS_FILTER set DESCRIPTION = 'Repairs - Blnkt' where PP_REPORT_FILTER_ID = 32;

update PP_REPORTS_FILTER set DESCRIPTION = 'Repairs - WMIS' where PP_REPORT_FILTER_ID = 33;


INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   38,
   5,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   38,
   6,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   38,
   51,
   NULL,
   NULL);
INSERT INTO PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) VALUES (
   36,
   4,
   NULL,
   NULL);

insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   2,
   28,
   27,
   'asset_location.major_location_id',
   NULL,
   NULL,
   NULL);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   3,
   39,
   38,
   'gl_account.ferc_sys_of_accts_id',
   NULL,
   NULL,
   NULL);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   4,
   41,
   40,
   'utility_account.bus_segment_id',
   NULL,
   NULL,
   NULL);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   5,
   42,
   40,
   'sub_account.bus_segment_id',
   NULL,
   NULL,
   NULL);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   6,
   42,
   41,
   'sub_account.utility_account_id',
   NULL,
   NULL,
   NULL);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   7,
   45,
   44,
   'prop_group_prop_unit.property_unit_id',
   NULL,
   NULL,
   NULL);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   8,
   46,
   45,
   'retirement_unit.property_unit_id',
   NULL,
   NULL,
   NULL);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   9,
   46,
   44,
   'prop_group_prop_unit.property_group_id',
   NULL,
   NULL,
   NULL);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP) VALUES (
   10,
   48,
   50,
   'asset_location.major_location_id',
   NULL,
   NULL,
   NULL);

--******************************** End of reporting ************************************

create table REPAIR_WORK_ORDER_DEBUG
   as select * from REPAIR_WORK_ORDER_TEMP;

create table REPAIR_WORK_ORDER_ORIG_DEBUG
   as select * from REPAIR_WORK_ORDER_TEMP_ORIG;

create table REPAIR_AMOUNT_ALLOCATE_DEBUG
   as select * from REPAIR_AMOUNT_ALLOCATE;

create table REPAIR_BLANKET_PROC_DEBUG
   as select * from REPAIR_BLANKET_PROCESSING;

create table REPAIR_BLANKET_RESULTS_DEBUG
   as select * from REPAIR_BLANKET_RESULTS;

create table REPAIR_CALC_PRA_REVERSE_DEBUG
   as select * from REPAIR_CALC_PRA_REVERSE;

-- ***************************************************************
-- FXN: F_TAX_REPAIRS_DEBUG
-- POPULATES NON-TEMP TABLES WITH DATA FROM TEMPORARY TABLES
-- ***************************************************************
create or replace function F_TAX_REPAIRS_DEBUG(
                                               -- a_retrieve = 1 means we are storing data from temp tables
                                               -- a_retrieve = 2 means we are populating temp tables with data from non-temp tables
                                               A_RETRIEVE number) return number is

   pragma autonomous_transaction;

begin

   if A_RETRIEVE > 2 or A_RETRIEVE < 1 then
      return -1;
   end if;

   if A_RETRIEVE = 1 then

      delete from REPAIR_WORK_ORDER_DEBUG;
      insert into REPAIR_WORK_ORDER_DEBUG
         select * from REPAIR_WORK_ORDER_TEMP;

      delete from REPAIR_WORK_ORDER_ORIG_DEBUG;
      insert into REPAIR_WORK_ORDER_ORIG_DEBUG
         select * from REPAIR_WORK_ORDER_TEMP_ORIG;

      delete from REPAIR_AMOUNT_ALLOCATE_DEBUG;
      insert into REPAIR_AMOUNT_ALLOCATE_DEBUG
         select * from REPAIR_AMOUNT_ALLOCATE;

      delete from REPAIR_BLANKET_PROC_DEBUG;
      insert into REPAIR_BLANKET_PROC_DEBUG
         select * from REPAIR_BLANKET_PROCESSING;

      delete from REPAIR_BLANKET_RESULTS_DEBUG;
      insert into REPAIR_BLANKET_RESULTS_DEBUG
         select * from REPAIR_BLANKET_RESULTS;

      delete from REPAIR_CALC_PRA_REVERSE_DEBUG;
      insert into REPAIR_CALC_PRA_REVERSE_DEBUG
         select * from REPAIR_CALC_PRA_REVERSE;

   end if;

   if A_RETRIEVE = 2 then

      delete from REPAIR_WORK_ORDER_TEMP;
      insert into REPAIR_WORK_ORDER_TEMP
         select * from REPAIR_WORK_ORDER_DEBUG;

      delete from REPAIR_WORK_ORDER_TEMP_ORIG;
      insert into REPAIR_WORK_ORDER_TEMP_ORIG
         select * from REPAIR_WORK_ORDER_ORIG_DEBUG;

      delete from REPAIR_AMOUNT_ALLOCATE;
      insert into REPAIR_AMOUNT_ALLOCATE
         select * from REPAIR_AMOUNT_ALLOCATE_DEBUG;

      delete from REPAIR_BLANKET_PROCESSING;
      insert into REPAIR_BLANKET_PROCESSING
         select * from REPAIR_BLANKET_PROC_DEBUG;

      delete from REPAIR_BLANKET_RESULTS;
      insert into REPAIR_BLANKET_RESULTS
         select * from REPAIR_BLANKET_RESULTS_DEBUG;

      delete from REPAIR_CALC_PRA_REVERSE;
      insert into REPAIR_CALC_PRA_REVERSE
         select * from REPAIR_CALC_PRA_REVERSE_DEBUG;

   end if;

   commit;

   return 1;
end;
/

--Populate the new work_order_tax_repairs_table
insert into WORK_ORDER_TAX_REPAIRS
   (WORK_ORDER_ID, TAX_EXPENSE_TEST_ID, REPAIR_LOCATION_METHOD_ID, REPAIR_LOCATION_ID,
    TE_AGGREGATION_ID)
   select WOA.WORK_ORDER_ID, WOA.TAX_EXPENSE_TEST_ID, 2, null, WOA.TE_AGGREGATION_ID
     from WORK_ORDER_ACCOUNT WOA
    where TAX_EXPENSE_TEST_ID is not null;
commit;

update WORK_ORDER_TAX_REPAIRS A
   set REPAIR_LOCATION_ID =
        (select max(RL.REPAIR_LOCATION_ID)
           from REPAIR_LOCATION       RL,
                WORK_ORDER_CLASS_CODE WOCC,
                REPAIR_SCHEMA         RS,
                WO_TAX_EXPENSE_TEST   WOTEST
          where WOTEST.TAX_EXPENSE_TEST_ID = A.TAX_EXPENSE_TEST_ID
            and WOTEST.REPAIR_SCHEMA_ID = RS.REPAIR_SCHEMA_ID
            and RS.REPAIR_LOC_OVRRD_CLASS_CODE_ID = WOCC.CLASS_CODE_ID
            and WOCC.WORK_ORDER_ID = A.WORK_ORDER_ID
            and RL.DESCRIPTION = WOCC.VALUE
          group by A.WORK_ORDER_ID);
commit;

update WORK_ORDER_TAX_REPAIRS
   set REPAIR_LOCATION_METHOD_ID = 1
 where REPAIR_LOCATION_ID is not null;
commit;

alter table REPAIR_WORK_ORDER_TEMP_ORIG
   drop primary key drop index;

alter table REPAIR_WORK_ORDER_TEMP_ORIG
   add constraint PK_REPAIR_WORK_ORDER_TEMP_ORIG
       primary key (COMPANY_ID,
                    WORK_ORDER_NUMBER,
                    REPAIR_UNIT_CODE_ID,
                    REPAIR_LOCATION_ID,
                    ASSET_REPAIR_LOCATION_ID,
                    FERC_ACTIVITY_CODE,
                    UTILITY_ACCOUNT_ID,
                    BUS_SEGMENT_ID);

alter table REPAIR_WORK_ORDER_TEMP_ORIG modify VINTAGE null;

alter table REPAIR_WORK_ORDER_TEMP_ORIG modify RETIREMENT_UNIT_ID null;

alter table REPAIR_WORK_ORDER_TEMP
   drop primary key drop index;

alter table REPAIR_WORK_ORDER_TEMP modify VINTAGE null;

alter table REPAIR_WORK_ORDER_TEMP modify REPAIR_METHOD_ID null;

alter table REPAIR_WORK_ORDER_TEMP
   add constraint PK_REPAIR_WORK_ORDER_TEMP
       primary key (COMPANY_ID,
                    WORK_ORDER_NUMBER,
                    REPAIR_UNIT_CODE_ID,
                    REPAIR_LOCATION_ID,
                    FERC_ACTIVITY_CODE,
                    REPAIR_QTY_INCLUDE);

alter table REPAIR_AMOUNT_ALLOCATE
   drop primary key drop index;

alter table REPAIR_AMOUNT_ALLOCATE
   add constraint PK_REPAIR_AMOUNT_ALLOCATE
       primary key (ASSET_ID,
                    ASSET_ACTIVITY_ID,
                    BOOK_SUMMARY_ID,
                    WORK_REQUEST);

alter table REPAIR_AMT_ALLOC_REPORTING
   drop primary key drop index;

alter table REPAIR_AMT_ALLOC_REPORTING
   add constraint PK_REPAIR_AMT_ALLOC_REPORTING
       primary key (BATCH_ID,
                    ASSET_ID,
                    ASSET_ACTIVITY_ID,
                    BOOK_SUMMARY_ID,
                    WORK_REQUEST)
       using index tablespace PWRPLANT_IDX;

update PPBASE_MENU_ITEMS
   set UO_WKSP_NAME = 'uo_rpr_post_wksp_main'
 where MODULE = 'REPAIRS'
   and IDENTIFIER = 'menu_wksp_posting'
   and PARENT_IDENTIFIER = 'repairs_top';

create table REPAIR_CALC_CWIP
(
 CHARGE_ID             number(22) not null,
 CHARGE_TYPE_ID        number(22),
 WORK_ORDER_ID         number(22),
 DESCRIPTION           varchar2(35),
 AMOUNT                number(22,2),
 DEPARTMENT_ID         number(22),
 CHARGE_AUDIT_ID       number(22),
 COST_ELEMENT_ID       number(22),
 COMPANY_ID            number(22),
 TAX_ORIG_MONTH_NUMBER number(22),
 NOTES                 varchar2(2000),
 REPAIR_SCHEMA_ID      number(22),
 WORK_ORDER_AMOUNT     number(22,2),
 PRIOR_REPAIR_AMOUNT   number(22,2),
 BATCH_ID              number(22),
 STATUS                varchar2(35)
);

alter table REPAIR_CALC_CWIP
   add constraint REPAIR_CALC_CWIP_PK
       primary key (CHARGE_ID)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_CALC_ASSET
   add PRIOR_REVERSAL number(22,2);

create index REPAIR_CALC_CWIP_IDX
   on REPAIR_CALC_CWIP(BATCH_ID, COMPANY_ID, REPAIR_SCHEMA_ID)
      tablespace PWRPLANT_IDX;

--The purpose of this field is to allow us to set up some tax expense tests for users to use when they want to
--remove a work order from testing that was previously tested.
--This field should not be included in any configuration screens or updated in any circumstance.
alter table WO_TAX_EXPENSE_TEST
   add ALWAYS_CAPITAL number(22) default 0;

--Insert tests into WO Tax Expense Test that can be used when users want to exclude a work order that was previously tested using one of the Asset methods.
--We don't need a special "Always Capital" test for the CWIP method because the user can just update the work order tax status to "Capital - No Review" for CWIP.
insert into WO_TAX_EXPENSE_TEST
   (TAX_EXPENSE_TEST_ID, REPAIR_SCHEMA_ID, ALWAYS_CAPITAL, DESCRIPTION)
   select RPR_TAX_EXPENSE_TEST_SEQ.NEXTVAL, REPAIR_SCHEMA_ID, 1, 'Exclude from Test: ' || USAGE_FLAG
     from (select min(REPAIR_SCHEMA_ID) REPAIR_SCHEMA_ID, USAGE_FLAG
             from REPAIR_SCHEMA
            where UPPER(USAGE_FLAG) <> 'CWIP'
            group by USAGE_FLAG);

alter table REPAIR_WORK_ORDER_TEMP_ORIG
   drop primary key drop index;

alter table REPAIR_WORK_ORDER_TEMP_ORIG
   add constraint PK_REPAIR_WORK_ORDER_TEMP_ORIG
       primary key (COMPANY_ID,
                    WORK_ORDER_NUMBER,
                    REPAIR_UNIT_CODE_ID,
                    REPAIR_LOCATION_ID,
                    ASSET_REPAIR_LOCATION_ID,
                    FERC_ACTIVITY_CODE,
                    UTILITY_ACCOUNT_ID,
                    BUS_SEGMENT_ID,
                    REPAIR_QTY_INCLUDE);

INSERT INTO ppbase_menu_items(MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL, PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
VALUES (
   'REPAIRS',
   'menu_wksp_processing_wo_test',
   'menu_wksp_processing',
   1,
   'Set Work Order Tax Status',
   'Work Order Tax Status Update',
   1,
   3,
   NULL,
   'uo_rpr_process_wo_status',
   NULL,
   NULL,
   NULL,
   NULL);

update PPBASE_MENU_ITEMS set UO_WKSP_NAME = null where IDENTIFIER = 'menu_wksp_processing';

INSERT INTO ppbase_menu_items(MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL, PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION)
VALUES (
   'REPAIRS',
   'menu_wksp_processing_main',
   'menu_wksp_processing',
   2,
   'Calculate Dollars',
   'Calculate Tax Repairs Dollars',
   1,
   3,
   NULL,
   'uo_rpr_process_wksp_main',
   NULL,
   NULL,
   NULL,
   NULL);

update PPBASE_MENU_ITEMS
   set UO_WKSP_NAME = 'uo_rpr_process_wksp_wo_status'
 where MODULE = 'REPAIRS'
   and IDENTIFIER = 'menu_wksp_processing_wo_test';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 3
 where IDENTIFIER = 'menu_wksp_tax_exp_tests'
   and MODULE = 'REPAIRS';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 4, LABEL = 'Pre-unitized'
 where IDENTIFIER = 'menu_wksp_work_order'
   and MODULE = 'REPAIRS';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 5, LABEL = 'Post-unitized'
 where IDENTIFIER = 'menu_wksp_asset'
   and MODULE = 'REPAIRS';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 6
 where IDENTIFIER = 'menu_wksp_threshold_estimate'
   and MODULE = 'REPAIRS';

alter table WO_TAX_EXPENSE_TEST
   modify (ADD_RETIRE_TOLERANCE_PCT number(28,8),
           EXPENSE_PERCENT          number(28,8),
           TOLERANCE_PERCENT        number(28,8));

update PPBASE_MENU_ITEMS
   set UO_WKSP_NAME = null
 where MODULE = 'REPAIRS'
   and IDENTIFIER = 'menu_wksp_reporting';

INSERT INTO ppbase_menu_items(MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL, PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION) VALUES (
   'REPAIRS',
   'menu_wksp_reports',
   'menu_wksp_reporting',
   1,
   'Reports',
   'Reports',
   1,
   2,
   NULL,
   'uo_rpr_rptcntr_wksp_report',
   NULL,
   NULL,
   NULL,
   NULL);
INSERT INTO ppbase_menu_items(MODULE, IDENTIFIER, PARENT_IDENTIFIER, ITEM_ORDER, LABEL, INFO, ENABLE_YN, MENU_LEVEL, PICTURE_NAME, UO_WKSP_NAME, SPLIT_YN, IDENTIFIER_2, UO_WKSP_NAME_2, SPLIT_DIRECTION) VALUES (
   'REPAIRS',
   'menu_wksp_queries',
   'menu_wksp_reporting',
   2,
   'Queries',
   'Queries (launched in external window)',
   1,
   2,
   NULL,
   'uo_rpr_rptcntr_wksp_query',
   NULL,
   NULL,
   NULL,
   NULL);

update PPBASE_MENU_ITEMS
   set PICTURE_NAME = 'menu_arrow_right.png'
 where MODULE = 'REPAIRS'
   and IDENTIFIER in ('menu_wksp_config',
                      'menu_wksp_processing',
                      'menu_wksp_reporting',
                      'menu_wksp_work_order',
                      'menu_wksp_asset');

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'REPAIRS-Fail Zero Qty Minors',
          'Yes',
          'dw_yes_no;1',
          'Default is "yes". Minor items that have zero quantity will be automatically failed to avoid passing late charges. It works the same way as major items with zero quantity.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (298, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;