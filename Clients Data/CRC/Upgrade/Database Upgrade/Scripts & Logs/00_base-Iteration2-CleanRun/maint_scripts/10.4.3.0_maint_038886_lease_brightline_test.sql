/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038886_lease_brightline_test.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 07/22/2014 Daniel Motter       Creation
||========================================================================================
*/

/* Add row for bright line test */
insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
   (select 18, 'LESSEE', 'bright_line_test', 'Brightline Test', 7, 'ue_brightlinetest'
      from DUAL
     where not exists (select * from PPBASE_ACTIONS_WINDOWS where ID = 18));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1267, 0, 10, 4, 3, 0, 38886, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038886_lease_brightline_test.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;