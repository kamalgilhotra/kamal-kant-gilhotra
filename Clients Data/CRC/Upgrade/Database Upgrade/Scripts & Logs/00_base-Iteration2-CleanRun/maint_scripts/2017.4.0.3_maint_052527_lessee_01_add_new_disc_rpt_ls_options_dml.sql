/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052527_lessee_01_add_new_disc_rpt_ls_options_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.3 10/22/2018  C.Yura    Add new disclosure reports for displaying purchase option,renewal and term information. remove 2014 report.
||============================================================================
*/

delete from pp_reports 
where report_number like '%Lessee%' 
and report_number like '%2014%';

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
			 turn_off_multi_thread)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Lease Purchase Options',
             'Disclosure: Displays the lease options for purchase',
             'Lessee',
             'dw_ls_rpt_disclosure_purch_options',
             'Lessee - 2016',
             11,
             311,
             207,
             103,
             1,
             3,
			 1);

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
			 turn_off_multi_thread)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Lease Purchase Options',
             'Company Consolidation - Disclosure: Displays the lease options for purchase',
             'Lessee',
             'dw_ls_rpt_disclosure_purch_opt_consol',
             'Lessee - 2016 - Consolidated',
             11,
             312,
             206,
             104,
             1,
             3,
	         1);
			 
INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
			 turn_off_multi_thread)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Lease Termination Options',
             'Disclosure: Displays the lease options for termination',
             'Lessee',
             'dw_ls_rpt_disclosure_term_options',
             'Lessee - 2017',
             11,
             311,
             207,
             103,
             1,
             3,
			 1);

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
			 turn_off_multi_thread)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Lease Termination Options',
             'Company Consolidation - Disclosure: Displays the lease options for termination',
             'Lessee',
             'dw_ls_rpt_disclosure_term_opt_consol',
             'Lessee - 2017 - Consolidated',
             11,
             312,
             206,
             104,
             1,
             3,
	         1);

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
			 turn_off_multi_thread)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Lease Renewal Options',
             'Disclosure: Displays the lease options for renewal',
             'Lessee',
             'dw_ls_rpt_disclosure_renew_options',
             'Lessee - 2018',
             11,
             311,
             207,
             103,
             1,
             3,
			 1);

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
			 turn_off_multi_thread)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Lease Renewal Options',
             'Company Consolidation - Disclosure: Displays the lease options for renewal',
             'Lessee',
             'dw_ls_rpt_disclosure_renew_opt_consol',
             'Lessee - 2018 - Consolidated',
             11,
             312,
             206,
             104,
             1,
             3,
	         1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10702, 0, 2017, 4, 0, 3, 52527, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052527_lessee_01_add_new_disc_rpt_ls_options_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;