/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_sysweb_04_data.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/24/2014 Chad Theilman    Web Framework - Data
||============================================================================
*/

delete from PWRPLANT.PP_WEB_MODULE_COMPONENTS;
delete from PWRPLANT.PP_WEB_CONFIGURATION;
delete from PWRPLANT.PP_WEB_AUTHENTICATION;
delete from PWRPLANT.PP_WEB_MODULES;
delete from PWRPLANT.PP_WEB_LOCALIZATION_REVISION;
delete from PWRPLANT.PP_WEB_LOCALIZATION_STRING2;
delete from PWRPLANT.PP_WEB_COMPONENT;
delete from PWRPLANT.PP_WEB_AUTHENTICATION_USERS;
delete from PWRPLANT.PP_WEB_AUTHENTICATION_HISTORY;

INSERT INTO PWRPLANT.PP_WEB_MODULES ("MODULE_ID", "MODULE_NAME", "CLASSIFICATION", "LICENSABLE", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (0, 'ApplicationSvc', 'WEB', 0, 'Application Service', 'Admin_Install', sysdate);
  INSERT INTO PWRPLANT.PP_WEB_MODULES ("MODULE_ID", "MODULE_NAME", "CLASSIFICATION", "LICENSABLE", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (1, 'SYSTEM', 'WEB', 0, 'This entry represents the ''Web Administration'' module of the PowerPlan application suite.', 'Admin_Install', sysdate);
  INSERT INTO PWRPLANT.PP_WEB_MODULES ("MODULE_ID", "MODULE_NAME", "CLASSIFICATION", "LICENSABLE", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (2, 'MOBILE_PROJ', 'WEB', 1, 'This entry represents the ''Project Mobile Approvals'' module of the PowerPlan application suite.', 'Admin_Install', sysdate);
  INSERT INTO PWRPLANT.PP_WEB_MODULES ("MODULE_ID", "MODULE_NAME", "CLASSIFICATION", "LICENSABLE", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (3, 'MOBILE_PROP_TAX', 'WEB', 1, 'This entry represents the ''Property Tax Mobile Approvals'' module of the PowerPlan application suite.', 'Admin_Install', sysdate);
  INSERT INTO PWRPLANT.PP_WEB_MODULES ("MODULE_ID", "MODULE_NAME", "CLASSIFICATION", "LICENSABLE", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (4, 'MOBILE_LEASE', 'WEB', 1, 'This entry represents the ''Lease Mobile Approvals'' module of the PowerPlan application suite.', 'Admin_Install', sysdate);
  INSERT INTO PWRPLANT.PP_WEB_MODULES ("MODULE_ID", "MODULE_NAME", "CLASSIFICATION", "LICENSABLE", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (5, 'ANALYTICS_ASSET', 'WEB', 1, 'This entry represents the ''Asset Analytics'' module of the PowerPlan application suite.', 'Admin_Install', sysdate);
  INSERT INTO PWRPLANT.PP_WEB_MODULES ("MODULE_ID", "MODULE_NAME", "CLASSIFICATION", "LICENSABLE", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (6, 'PROJECTS', 'WEB', 1, 'This entry represents the ''Projects'' module of the PowerPlan application suite.', 'Admin_Install', sysdate);
  INSERT INTO PWRPLANT.PP_WEB_MODULES ("MODULE_ID", "MODULE_NAME", "CLASSIFICATION", "LICENSABLE", "DESCRIPTION", "USER_ID", "TIME_STAMP") VALUES (7, 'License', 'WEB', 1, 'License Service', 'Admin_Install', sysdate);

  
INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES   (-1,'MACurrentVersion','10.4.0','Version Number','This is the version number of the currently installed Mobile Approvals module.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'ShowLoggedInUser','YES','Show Logged in User','This is a binary flag the indicates whether the user that is currently logged into the system will have their ''name'' displayed on the various screens.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'SERVER_ENVIRONMENT','NONE','Server Environment','This represents a description of the server environment.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'PROJECTS_MODULE','ENABLED','''Projects'' Module Activation','This is a boolean flag that signals the system to activate or deactivate the ''Projects'' portion of Mobile Approvals.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'SHOW_PROJECTS_REJECTION_MESSAGE','YES','Show ''Projects'' Rejection Message','This is a boolean value indicating whether the ''Projects'' portion of Mobile Approvals will display a rejection message.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'PROJECTS_REJECTION_MESSAGE_REQUIRED','YES','''Projects'' Rejection Message Required','This is a boolean value indicating whether the ''Projects'' portion of Mobile approvals requires a rejection message.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'SHOW_LEASE_REJECTION_MESSAGE','YES','Show ''Lease'' Rejection Message','This is a boolean value indicating whether the ''Lease'' portion of Mobile Approvals will display a rejection message.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'LEASE_REJECTION_MESSAGE_REQUIRED','YES','''Lease'' Rejection Message Required','This is a boolean value indicating whether the ''Lease'' portion of Mobile approvals requires a rejection message.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'LEASE_APPROVAL_MESSAGE_REQUIRED','YES','''Lease'' Approval Message Required','This is a boolean value indicating whether the ''Lease'' portion of Mobile approvals requires an approval message.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'PROPERTY_TAX_MODULE','ENABLED','''Property Tax'' Module Activation','This is a boolean flag that signals the system to activate or deactivate the ''Property Tax'' portion of Mobile Approvals.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'SHOW_PROP_TAX_REJECTION_MESSAGE','YES','Show ''Property Tax'' Rejection Message','This is a boolean value indicating whether the ''Property Tax'' portion of Mobile Approvals will display a rejection message.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'PROP_TAX_REJECTION_MESSAGE_REQUIRED','YES','''Property Tax'' Rejection Message Required','This is a boolean value indicating whether the ''Property Tax'' portion of Mobile approvals requires a rejection message.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'USE_ANALYTICS_MODULE','YES','Enable the ''Analytics'' Module','The ''Analytics'' module is licensed and in use.',' ',sysdate,1);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'USE_MOBILE_MODULE','YES','Enable the ''Mobile'' Module','Mobile Module is licensed and in use.',' ',sysdate,1);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'APP_SERVER_URL','http://<hostName or IP>/Application.svc','Application Server URL','URL of Application Server Installation. May or may not be on the same server.',' ',sysdate,1);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'REPORT_SERVER_URL','http://<hostName or IP>/Application.svc','Reporting Server URL','URL of Reporting Server Installation. May or may not be on the same server.',' ',sysdate,1);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'WEB_SERVER_URL','http://<hostName or IP>/MobileApprovals/Index','Web Server URL','URL of Web Server Installation. May or may not be on the same server.',' ',sysdate,1);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_APPROVAL_EMAIL_TEXT','This is an automated email. You have an item that needs approval. Click the following link to approve or reject. {TOKEN_LINK}','Approval Email Text','Email text sent to users for approval email.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_RESET_PIN_EMAIL_TEXT','This is an automated email. You requested a pin reset. Click the following link to reset your pin. {TOKEN_LINK} If you didn''t make this request, click the following link. {TOKEN_LINK2}','Reset Pin Email Text','Email text sent to users for reset pin request',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_CONFIRM_RESET_PIN_EMAIL_TEXT','This is an automated email. Your pin number has been reset. If you didn''t reset your pin please click the following link. {TOKEN_LINK}','Reset Confirmation Email Text','Email text sent to users for reset confimation.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_GRID_ROWS_PER_PAGE','20','Grid Rows Per Page','Number of default rows displayed within grid.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_URL_USER_IMPORT_DOCUMENT','http://<hostName or IP>/Admin/Docs/UserImportTemplate.xls','User Import Template Location','Path to location of the .xls User Import Document.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_AUTO_GEN_UPPER_LIMIT','9999','Auto Gen Upper Limit','Ending number when auto generating Pin numbers.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_AUTO_GEN_LOWER_LIMIT','0','Auto Gen Lower Limit','Starting number when auto generating Pin numbers.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_IS_LOCK_USER_ATTEMPTS','YES','Lock User Attempts?','YES/NO Lock user out after N number of failed attempts?',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_LOCK_USER_FAILED_ATTEMPTS_NUMBER','4','Failed Attempts','Number of failed attempts before user lockout.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_IS_EXPIRE_PIN_DAYS','NO','Expire Pin?','YES/NO Expire a pin after N number of days.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_PIN_EXPIRATION_DAYS','0','Expiration Days','Number of days a pin is active. After this time period the pin expires and the user must request a new pin.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_IS_SET_LOCKOUT_TIME_LIMIT','YES','Lockout Time Limit?','YES/NO Lockout Time Limit',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'MOBILE_LOCKOUT_TIME_LIMIT_MINUTES','15','Lockout Minutes','Number of minutes a user must wait before logging in after being locked out.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'DEVICE_AUTHENTICATION_AUTO_APPROVE','NO','Device Registration Auto Approve','Whether or not to automatically approve a device when it is registered.',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'DEVICE_AUTHENTICATION_REGISTER_EMAIL','jkempema@pwrplan.com','Device Registration Email ','The email address to send a notification to when a device is registered',' ',sysdate,2);
  INSERT INTO PWRPLANT.PP_WEB_CONFIGURATION ("COMPANY_ID", "CONFIG_KEY", "CONFIG_VALUE", "CONFIG_TITLE", "CONFIG_DESC","CONFIG_TYPE", "TIME_STAMP", "MODULE_ID") VALUES (-1,'ADMIN_SITE','Administration','Administration site name','String used to identify the Web Admin site. Note: This must be present for the navigation on the Web Admin site to work correctly.',' ',sysdate,1);

  INSERT INTO PWRPLANT.PP_WEB_LOCALIZATION_REVISION (LOCALE, REVISION_DATE) VALUES('en-US', '01-AUG-13');
  INSERT INTO PWRPLANT.PP_WEB_LOCALIZATION_REVISION (LOCALE, REVISION_DATE) VALUES('es-MX', '01-AUG-13');
  
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Abort','es-MX','Aborto',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Retry','es-MX','Volver a Procesar',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Ignore','es-MX','Ignorar',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','OK','es-MX','Vale',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Cancel','es-MX','Cancelar',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Yes','es-MX','Sí',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','No','es-MX','No',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.LogOff','Log Off','es-MX','Finalizar la Sesión',to_date('31-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.License.Index','Clients','es-MX','Clientela',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.License.Modules','Modules','es-MX','Módulos',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.License.Versions','Versions','es-MX','Versiónes',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.System','Application Suite','es-MX','Suite de Aplicación',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.Analytics','Analytics','es-MX','Analítica',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.MobileApprovals','Mobile Approvals','es-MX','Aprobación Móvil',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.Projects','Projects','es-MX','Proyectos',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting','Regulatory','es-MX','Regulador',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.Index','Charts','es-MX','Gráficos',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.AnnualizedAcctTrend','Annualized Account Trend','es-MX','Moda de Cuenta Anualizado',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.AnnualizedSubAcctTrend','Annualized Sub-Account Trend','es-MX','Moda de Cuenta Anualizado Subordinado',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.CommonEquityReturn','Common Equity Return','es-MX','Retorno de Capital Ordinario',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.RevenueReqComparison','Revenue Req Comparison','es-MX','Comparación Requisito de Ingresos',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.RevenueReqSynthesis','Revenue Req Synthesis','es-MX','Síntesis Requisito de Ingresos',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Security.Index','Security','es-MX','Seguridad',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration','Administration','es-MX','Administración',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Home','Home','es-MX','Inicio',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Home.Index','Index','es-MX','índice',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Dashboard.Index','Dashboard','es-MX','Tablero de Instrumentos',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Reporting.Manager.Index','Report Manager','es-MX','Administrador de Informes',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Reporting.PowerQuery','Reporting','es-MX','Reportajes',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Reporting.PowerQuery.Index','PowerQuery','es-MX','Potencia de Consulta',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Home.Index','Home','es-MX','Inicio',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.Index','Administration','es-MX','Administración',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Modules','Modules','es-MX','Módulos',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Demo.ServerAlertsTest','Test','en-US','Test',to_date('31-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.LogOff','Log Off','en-US','Log Off',to_date('31-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Abort','en-US','Abort',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Retry','en-US','Retry',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Ignore','en-US','Ignore',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','OK','en-US','OK',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Cancel','en-US','Cancel',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','Yes','en-US','Yes',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Global.MessageBox','No','en-US','No',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.License.Index','Clients','en-US','Clients',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.License.Modules','Modules','en-US','Modules',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.License.Versions','Versions','en-US','Versions',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Demo.IndexTitle','Demo','en-US','Demo',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.System','Application Suite','en-US','Application Suite',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.Analytics','Analytics','en-US','Analytics',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.MobileApprovals','Mobile Approvals','en-US','Mobile Approvals',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.Projects','Projects','en-US','Projects',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting','Regulatory','en-US','Regulatory',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.Index','Charts','en-US','Charts',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.AnnualizedAcctTrend','Annualized Account Trend','en-US','Annualized Account Trend',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.AnnualizedSubAcctTrend','Annualized Sub-Account Trend','en-US','Annualized Sub-Account Trend',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.CommonEquityReturn','Common Equity Return','en-US','Common Equity Return',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.RevenueReqComparison','Revenue Req Comparison','en-US','Revenue Req Comparison',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Regulatory.Charting.RevenueReqSynthesis','Revenue Req Synthesis','en-US','Revenue Req Synthesis',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Forms.FormBuilder','Forms','en-US','Forms',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Forms.FormBuilder.Inline','Form Builder','en-US','FormBuilder',to_date('31-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Security.Index','Security','en-US','Security',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration','Administration','en-US','Administration',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Home','Home','en-US','Home',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Home.Index','Index','en-US','Index',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Dashboard.Index','Dashboard','en-US','Dashboard',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Reporting.Manager.Index','Report Manager','en-US','Report Manager',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Reporting.PowerQuery','Reporting','en-US','Reporting',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Reporting.PowerQuery.Index','PowerQuery','en-US','PowerQuery',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Home.Index','Home','en-US','Home',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('System.Administration.Index','Administration','en-US','Administration',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');
  Insert into PWRPLANT.PP_WEB_LOCALIZATION_STRING2 (OBJECTS,EN,LOCALE,VALUE,TIME_STAMP,USER_ID) values ('Modules','Modules','en-US','Modules',to_date('28-JAN-14','DD-MON-RR'),'PWRPLANT');

  
  INSERT INTO PWRPLANT.PP_WEB_COMPONENT ("COMPONENT_ID", "DESCRIPTION", "LONG_DESCRIPTION") VALUES (-1, 'NONE', 'No sub-components of a module.');
  INSERT INTO PWRPLANT.PP_WEB_COMPONENT ("COMPONENT_ID", "DESCRIPTION", "LONG_DESCRIPTION") VALUES (1, 'Projects', 'The PowerPlan Projects system component.');
  INSERT INTO PWRPLANT.PP_WEB_COMPONENT ("COMPONENT_ID", "DESCRIPTION", "LONG_DESCRIPTION") VALUES (2, 'Property Tax', 'The PowerPlan Property Tax system component.');
  INSERT INTO PWRPLANT.PP_WEB_COMPONENT ("COMPONENT_ID", "DESCRIPTION", "LONG_DESCRIPTION") VALUES (3, 'Lease', 'The PowerPlan Lease system component.');
  INSERT INTO PWRPLANT.PP_WEB_COMPONENT ("COMPONENT_ID", "DESCRIPTION", "LONG_DESCRIPTION") VALUES (4, 'Asset', 'The PowerPlan Asset system component.');
  INSERT INTO PWRPLANT.PP_WEB_COMPONENT ("COMPONENT_ID", "DESCRIPTION", "LONG_DESCRIPTION") VALUES (5, 'Regulatory', 'The PowerPlan Regulatory system component.');

  INSERT INTO PWRPLANT.PP_WEB_MODULE_COMPONENTS ("COMPONENT_ID", "MODULE_ID") VALUES (-1, 2);
  INSERT INTO PWRPLANT.PP_WEB_MODULE_COMPONENTS ("COMPONENT_ID", "MODULE_ID") VALUES (1, 2);
  INSERT INTO PWRPLANT.PP_WEB_MODULE_COMPONENTS ("COMPONENT_ID", "MODULE_ID") VALUES (2, 2);
  INSERT INTO PWRPLANT.PP_WEB_MODULE_COMPONENTS ("COMPONENT_ID", "MODULE_ID") VALUES (3, 2);
  INSERT INTO PWRPLANT.PP_WEB_MODULE_COMPONENTS ("COMPONENT_ID", "MODULE_ID") VALUES (-1, 3);
  INSERT INTO PWRPLANT.PP_WEB_MODULE_COMPONENTS ("COMPONENT_ID", "MODULE_ID") VALUES (4, 3);
  INSERT INTO PWRPLANT.PP_WEB_MODULE_COMPONENTS ("COMPONENT_ID", "MODULE_ID") VALUES (-1, 6);
  INSERT INTO PWRPLANT.PP_WEB_MODULE_COMPONENTS ("COMPONENT_ID", "MODULE_ID") VALUES (5, 6);
  
  INSERT INTO PWRPLANT.PP_WEB_AUTHENTICATION ("AUTHENTICATION_NAME", "AUTHENTICATION_TYPE", "IS_ACTIVE", "MAX_FAILED_ATTEMPTS", "EXPIRE_LOCKOUT_MINUTES", "MUST_CHANGE_PASSWORD_DAYS", "PASSWORD_TYPE", "PASSWORD_ALLOWED_CHARACTERS", "PASSWORD_CONTAINS_CHARACTERS", "PASSWORD_MINIMUM_LENGTH", "PASSWORD_MAXIMUM_LENGTH", "PASSWORD_REUSE_COUNT", "INTERFACE_DOT_NET_TYPE", "MODULE_ID") VALUES ('EMAIL-PIN', 'TABLE', 1, 5, 60, 0, 'TEXT', '0123456789', 'N:4', 4, 4, 0, '', 2);
  INSERT INTO PWRPLANT.PP_WEB_AUTHENTICATION ("AUTHENTICATION_NAME", "AUTHENTICATION_TYPE", "IS_ACTIVE", "MAX_FAILED_ATTEMPTS", "EXPIRE_LOCKOUT_MINUTES", "MUST_CHANGE_PASSWORD_DAYS", "PASSWORD_TYPE", "PASSWORD_ALLOWED_CHARACTERS", "PASSWORD_CONTAINS_CHARACTERS", "PASSWORD_MINIMUM_LENGTH", "PASSWORD_MAXIMUM_LENGTH", "PASSWORD_REUSE_COUNT", "INTERFACE_DOT_NET_TYPE", "MODULE_ID") VALUES ('DIRECT-DB', 'DATABASE', 1, 5, 60, 0, 'TEXT', ' ', ' ', 0, 0, 0, '', 2);
  
  
  INSERT INTO PWRPLANT.PP_WEB_AUTHENTICATION_USERS 
  (AUTHENTICATION_NAME, USERS, ACCOUNT, IS_ACTIVE, IS_LOCKED_OUT, FAILED_LOGIN_ATTEMPTS, TIME_STAMP) 
   VALUES ('DIRECT-DB', 'pwrplant', 'PWRPLANT', '1', '0', '0', TO_DATE('31-JUL-13', 'DD-MON-RR'));
   
  insert into pp_web_authentication_history(authentication_name,users,password,time_stamp)
  values('DIRECT-DB','PWRPLANT',null,TO_DATE('31-JUL-13', 'DD-MON-RR'));
  
--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1569, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_sysweb_04_data.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;