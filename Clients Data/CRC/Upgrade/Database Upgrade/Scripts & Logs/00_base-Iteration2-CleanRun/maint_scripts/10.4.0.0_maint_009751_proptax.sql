/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_009751_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.4.0.0    11/07/2012 Julia Breuer   Point Release
||============================================================================
*/
-- Increment default step numbers for steps after this new step
update PWRPLANT.PT_PROCESS
   set DEFAULT_STEP_NUMBER = DEFAULT_STEP_NUMBER + 1
 where DEFAULT_STEP_NUMBER >= 34;

-- Add new step.
insert into PWRPLANT.PT_PROCESS
   (PT_PROCESS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SHORT_DESCRIPTION,
    IDENTIFIER, PT_PROCESS_TYPE_ID, IS_BASE_PROCESS, DEFAULT_STEP_NUMBER, OBJECT_NAME, DOCUMENTATION)
values
   (52, sysdate, user, 'National Postallo CWIP Audit',
    'Compare the post-allocation property tax balances to the CWIP balances for multi-state property.',
    'National CWIP Audit', 'national_postallo_cwip', 1, 1, 34,
    'uo_ptc_rtncntr_wksp_audit_post_cwip_natl', null);

-- Change the CPR audit to have a similar description.
update PWRPLANT.PT_PROCESS
   set DESCRIPTION = 'National Postallo Audit', SHORT_DESCRIPTION = 'National Audit'
 where PT_PROCESS_ID = 50;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (247, 0, 10, 4, 0, 0, 9751, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_009751_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
