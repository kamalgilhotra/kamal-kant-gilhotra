/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011573_version-updates_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/30/2012 pcordero       10.4.0.0 version
||============================================================================
*/

update PP_VERSION set PROVISION_VERSION = '10.4.0';

update TAX_ACCRUAL_SYSTEM_CONTROL
   set CONTROL_VALUE = '10.4.0'
where CONTROL_NAME = 'Provision DB Version';

 --**************************
 -- Log the run of the script
 --**************************

 insert into PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
 values
    (257, 0, 10, 4, 0, 0, 11573, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011573_version-updates_prov.sql', 1,
     SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
     SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
 commit;
