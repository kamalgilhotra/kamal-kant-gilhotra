/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052073_lessee_01_asset_retire_info_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.1 8/23/2018  Shane "C" Ward Create table for preserving original leased asset information for reverting a retirement
||============================================================================
*/

CREATE TABLE ls_asset_retire_info
(ls_asset_id NUMBER(22,0) NOT NULL,
orig_quantity NUMBER(22,2),
orig_act_term_amt NUMBER(22,2),
orig_act_resid_amt NUMBER(22,2),
orig_est_residual NUMBER(22,2),
orig_guaranteed_resid_amt NUMBER(22,2),
orig_pay_term_penalty NUMBER(22,2),
orig_pay_sales_proceed NUMBER(22,2),
orig_pay_purch_option NUMBER(22,2),
orig_act_purch_amount NUMBER(22,2),
user_id VARCHAR2(18),
time_stamp DATE);

ALTER TABLE ls_asset_retire_info
  ADD CONSTRAINT ls_asset_retire_info_pk PRIMARY KEY (
    ls_asset_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

COMMENT ON TABLE ls_asset_retire_info IS '(S) [] Table used for holding Lessee Asset information as it was before being sent for retirement. The table is leveraged to revert the retirement if the pending transaction is deleted';

COMMENT ON COLUMN ls_asset_retire_info.ls_asset_id IS 'System Assigned Identifier of Leased Asset.';
COMMENT ON COLUMN ls_asset_retire_info.orig_quantity IS 'Original quantity of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.orig_act_term_amt IS 'Original Actual Term Amount of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.orig_act_resid_amt IS 'Original Actual Residual Amount of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.orig_est_residual IS 'Original Estimated Residual Percent of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.orig_guaranteed_resid_amt IS 'Original Guaranteed Residual Amount of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.orig_pay_term_penalty IS 'Original Payment Term Penalty of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.orig_pay_sales_proceed IS 'Original Pay Sales Proceeds of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.orig_pay_purch_option IS 'Original Pay Purchase Option of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.orig_act_purch_amount IS 'Original Actual Purchase Amount of Asset pre-retirement';
COMMENT ON COLUMN ls_asset_retire_info.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_asset_retire_info.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9222, 0, 2017, 4, 0, 1, 52073, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052073_lessee_01_asset_retire_info_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;