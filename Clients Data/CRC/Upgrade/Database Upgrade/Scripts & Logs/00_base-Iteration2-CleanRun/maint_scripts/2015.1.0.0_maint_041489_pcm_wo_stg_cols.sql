/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041489_add_columns_wo_interface_staging.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   12/01/2014 Anand Rajashekar    Add 2 new columns to wo_interface_staging table
||========================================================================================
*/

alter table WO_INTERFACE_STAGING add REPAIR_LOCATION_ID number(22,0);

alter table WO_INTERFACE_STAGING add EXT_REPAIR_LOCATION varchar2(254);

comment on column WO_INTERFACE_STAGING.REPAIR_LOCATION_ID     is 'The repair location id';
comment on column WO_INTERFACE_STAGING.EXT_REPAIR_LOCATION    is 'External repair location';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2078, 0, 2015, 1, 0, 0, 41489, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041489_pcm_wo_stg_cols.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;