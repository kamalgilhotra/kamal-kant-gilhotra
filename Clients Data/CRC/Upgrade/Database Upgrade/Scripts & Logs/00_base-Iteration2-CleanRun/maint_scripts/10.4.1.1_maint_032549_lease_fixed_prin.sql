/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032549_lease_fixed_prin.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Brandon Beck   Point Release
||============================================================================
*/

insert into LS_PAYMENT_TERM_TYPE
   (PAYMENT_TERM_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (3, 'Fixed Principal', 'Fixed Principal');

alter table LS_ILR_SCHEDULE_STG            add PAYMENT_TERM_TYPE_ID number(22,0);
comment on column LS_ILR_SCHEDULE_STG.PAYMENT_TERM_TYPE_ID            is 'The type of term.';

alter table LS_ILR_ASSET_SCHEDULE_STG      add PAYMENT_TERM_TYPE_ID number(22,0);
comment on column LS_ILR_ASSET_SCHEDULE_STG.PAYMENT_TERM_TYPE_ID      is 'The type of term.';

alter table LS_ILR_ASSET_SCHEDULE_CALC_STG add PAYMENT_TERM_TYPE_ID number(22,0);
comment on column LS_ILR_ASSET_SCHEDULE_CALC_STG.PAYMENT_TERM_TYPE_ID is 'The type of term.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (633, 0, 10, 4, 1, 1, 32549, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032549_lease_fixed_prin.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
