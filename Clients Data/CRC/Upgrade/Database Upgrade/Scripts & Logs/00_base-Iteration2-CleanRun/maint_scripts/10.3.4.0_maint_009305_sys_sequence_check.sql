/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009305_sys_sequence_check.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/20/2012 Lee Quinn      Point Release
||============================================================================
*/

-- Check all Sequences for Values larger the 2 Bil, also check to see if the increment is more than 1 and that we
-- are not caching too many values.
-- Print out all Sequences current values and raise exception if any of the above criteria are met.

SET SERVEROUTPUT ON

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';
   PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

   SEQUENCE_NUMBER_HIGH exception;
   INCREMENT_BY         exception;
   CACHE_SIZE           exception;

   L_SEQUENCE_NUMBER_HIGH_CNT  number default 0;
   L_INCREMENT_BY_CNT          number default 0;
   L_CACHE_SIZE_CNT            number default 0;
   L_SEQUENCE_NUMBER_HIGH_NAME varchar2(30);
   L_INCREMENT_BY_HIGH_NAME    varchar2(30);
   L_CACHE_SIZE_CNT_HIGH_NAME  varchar2(30);

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
   for CSR_SEQUENCE in (select SEQUENCE_NAME, LAST_NUMBER, INCREMENT_BY, CACHE_SIZE
                          from ALL_SEQUENCES
                         where SEQUENCE_OWNER = 'PWRPLANT')
   loop
      case
         when CSR_SEQUENCE.LAST_NUMBER > 2000000000 then
            if CSR_SEQUENCE.SEQUENCE_NAME = 'CRBUDGETS' then
               DBMS_OUTPUT.PUT_LINE(PPCMSG || RPAD(CSR_SEQUENCE.SEQUENCE_NAME, 30) ||
                                    ' - Last Number: ' || CSR_SEQUENCE.LAST_NUMBER || ' - CRBUDGETS sequence is valid to be larger than 2 billion.');
            else
               L_SEQUENCE_NUMBER_HIGH_NAME:= CSR_SEQUENCE.SEQUENCE_NAME;
               L_SEQUENCE_NUMBER_HIGH_CNT := L_SEQUENCE_NUMBER_HIGH_CNT + 1;
               DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA-20001 - Sequence ' || CSR_SEQUENCE.SEQUENCE_NAME ||
                                    ' is too high, value ' || CSR_SEQUENCE.LAST_NUMBER);
            end if;
         when CSR_SEQUENCE.INCREMENT_BY > 1 then
            L_INCREMENT_BY_HIGH_NAME := CSR_SEQUENCE.SEQUENCE_NAME;
            L_INCREMENT_BY_CNT := L_INCREMENT_BY_CNT + 1;
            DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA-20002 - Sequence ' || CSR_SEQUENCE.SEQUENCE_NAME ||
                                 ' Increment count too high, value ' || CSR_SEQUENCE.INCREMENT_BY);
         when CSR_SEQUENCE.CACHE_SIZE > 20 then
            L_CACHE_SIZE_CNT_HIGH_NAME := CSR_SEQUENCE.SEQUENCE_NAME;
            L_CACHE_SIZE_CNT := L_CACHE_SIZE_CNT + 1;
            DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA-20003 - Sequence ' || CSR_SEQUENCE.SEQUENCE_NAME ||
                                 ' Cache size is too high, value ' || CSR_SEQUENCE.CACHE_SIZE);
         else
            DBMS_OUTPUT.PUT_LINE(PPCMSG || RPAD(CSR_SEQUENCE.SEQUENCE_NAME, 30) ||
                                 ' - Last Number: ' || CSR_SEQUENCE.LAST_NUMBER);
      end case;
   end loop;

   case
      when L_SEQUENCE_NUMBER_HIGH_CNT > 0 then
         raise SEQUENCE_NUMBER_HIGH;
      when L_INCREMENT_BY_CNT > 0 then
         raise INCREMENT_BY;
      when L_CACHE_SIZE_CNT > 0 then
         raise CACHE_SIZE;
      else
         null;
   end case;

exception
   when SEQUENCE_NUMBER_HIGH then
      RAISE_APPLICATION_ERROR(-20001, 'Sequence Number too high - Sequence Name: ' || L_SEQUENCE_NUMBER_HIGH_NAME);
   when INCREMENT_BY then
      RAISE_APPLICATION_ERROR(-20002, 'Increment number too high - Sequence Name: ' || L_INCREMENT_BY_HIGH_NAME);
   when CACHE_SIZE then
      DBMS_OUTPUT.PUT_LINE(PPCORA ||'-20003, Cache size may be too high - Sequence Name: ' || L_CACHE_SIZE_CNT_HIGH_NAME);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (109, 0, 10, 3, 4, 0, 9305, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009305_sys_sequence_check.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
