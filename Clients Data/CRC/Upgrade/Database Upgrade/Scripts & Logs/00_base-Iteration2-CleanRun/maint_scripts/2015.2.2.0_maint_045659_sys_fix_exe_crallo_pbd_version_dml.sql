/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045659_sys_fix_exe_crallo_pbd_version_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2015.2.2.0 05/04/2016 David Haupt		    Fixing typod custom pbd version
||============================================================================
*/

UPDATE pp_custom_pbd_versions
SET pbd_name = 'exe_crallo_custom_dw.pbd'
WHERE pbd_name = 'exe_crallo_custom_dw,pbd';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3182, 0, 2015, 2, 2, 0, 045659, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045659_sys_fix_exe_crallo_pbd_version_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;