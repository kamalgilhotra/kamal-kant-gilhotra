/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045107_budgetcap_updated_bv_sv_views_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By            Reason for Change
|| -------- ---------- --------------------  ---------------------------------
|| 2015.2   10/26/2015 Chris Mardis          Add records for "First Approved" and "Selected Revision"
|| 2015.2.1 01/13/2016 Alex P.				 Changing the version to 2015.2.1 because the script was not released in 2015.2.0
||============================================================================
*/ 

create or replace view budget_version_sv as
select BUDGET_VERSION_ID,
       DESCRIPTION,
       LONG_DESCRIPTION,
       LOCKED,
       START_YEAR,
       CURRENT_YEAR,
       END_YEAR,
       ACTUALS_MONTH,
       DATE_FINALIZED,
       CURRENT_VERSION,
       BUDGET_ONLY,
       OPEN_FOR_ENTRY,
       EXTERNAL_BUDGET_VERSION,
       BRING_IN_SUBS,
       OUTLOOK_NUMBER,
       LEVEL_ID,
       REPORTING_CURRENCY_ID,
       BUDGET_VERSION_TYPE_ID,
       COMPANY_ID,
       5 SORT,
       'fp,bi' FP_BI,
       1 standard
  from BUDGET_VERSION BV
 where (BV.COMPANY_ID is null or BV.COMPANY_ID in (select COMPANY_ID from COMPANY))
union all
select -9 BUDGET_VERSION_ID,
       'Current Approved' DESCRIPTION,
       'Current Approved' LONG_DESCRIPTION,
       null LOCKED,
       null START_YEAR,
       null CURRENT_YEAR,
       null END_YEAR,
       null ACTUALS_MONTH,
       null DATE_FINALIZED,
       null CURRENT_VERSION,
       null BUDGET_ONLY,
       null OPEN_FOR_ENTRY,
       null EXTERNAL_BUDGET_VERSION,
       null BRING_IN_SUBS,
       null OUTLOOK_NUMBER,
       null LEVEL_ID,
       null REPORTING_CURRENCY_ID,
       null BUDGET_VERSION_TYPE_ID,
       null COMPANY_ID,
       1 SORT,
       'fp' FP_BI,
       1 standard
  from DUAL
union all
select -8 BUDGET_VERSION_ID,
       'Latest Revision' DESCRIPTION,
       'Latest Revision' LONG_DESCRIPTION,
       null LOCKED,
       null START_YEAR,
       null CURRENT_YEAR,
       null END_YEAR,
       null ACTUALS_MONTH,
       null DATE_FINALIZED,
       null CURRENT_VERSION,
       null BUDGET_ONLY,
       null OPEN_FOR_ENTRY,
       null EXTERNAL_BUDGET_VERSION,
       null BRING_IN_SUBS,
       null OUTLOOK_NUMBER,
       null LEVEL_ID,
       null REPORTING_CURRENCY_ID,
       null BUDGET_VERSION_TYPE_ID,
       null COMPANY_ID,
       3 SORT,
       'fp' FP_BI,
       1 standard
  from DUAL
union all
select -7 BUDGET_VERSION_ID,
       'First Approved' DESCRIPTION,
       'First Approved' LONG_DESCRIPTION,
       null LOCKED,
       null START_YEAR,
       null CURRENT_YEAR,
       null END_YEAR,
       null ACTUALS_MONTH,
       null DATE_FINALIZED,
       null CURRENT_VERSION,
       null BUDGET_ONLY,
       null OPEN_FOR_ENTRY,
       null EXTERNAL_BUDGET_VERSION,
       null BRING_IN_SUBS,
       null OUTLOOK_NUMBER,
       null LEVEL_ID,
       null REPORTING_CURRENCY_ID,
       null BUDGET_VERSION_TYPE_ID,
       null COMPANY_ID,
       2 SORT,
       'fp' FP_BI,
       1 standard
  from DUAL
union all
select -6 BUDGET_VERSION_ID,
       'Selected Revision' DESCRIPTION,
       'Selected Revision' LONG_DESCRIPTION,
       null LOCKED,
       null START_YEAR,
       null CURRENT_YEAR,
       null END_YEAR,
       null ACTUALS_MONTH,
       null DATE_FINALIZED,
       null CURRENT_VERSION,
       null BUDGET_ONLY,
       null OPEN_FOR_ENTRY,
       null EXTERNAL_BUDGET_VERSION,
       null BRING_IN_SUBS,
       null OUTLOOK_NUMBER,
       null LEVEL_ID,
       null REPORTING_CURRENCY_ID,
       null BUDGET_VERSION_TYPE_ID,
       null COMPANY_ID,
       4 SORT,
       'fp' FP_BI,
       0 standard
  from DUAL;


create or replace view budget_version_fund_proj_sv as
select BVFP.WORK_ORDER_ID, BVFP.REVISION, BVFP.BUDGET_VERSION_ID, BVFP.ACTIVE
  from BUDGET_VERSION_FUND_PROJ BVFP
union all
select WOC.WORK_ORDER_ID, WOC.CURRENT_REVISION, -9 BUDGET_VERSION_ID, 1 ACTIVE
  from WORK_ORDER_CONTROL WOC
 where WOC.FUNDING_WO_INDICATOR = 1
   and NVL(WOC.CURRENT_REVISION, 0) <> 0
union all
select WOC.WORK_ORDER_ID, max(WOA.REVISION), -8 BUDGET_VERSION_ID, 1 ACTIVE
  from WORK_ORDER_CONTROL WOC, WORK_ORDER_APPROVAL WOA
 where WOC.FUNDING_WO_INDICATOR = 1
   and WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
 group by WOC.WORK_ORDER_ID
union all
select WOC.WORK_ORDER_ID, min(WOA.REVISION), -7 BUDGET_VERSION_ID, 1 ACTIVE
  from WORK_ORDER_CONTROL WOC, WORK_ORDER_APPROVAL WOA
 where WOC.FUNDING_WO_INDICATOR = 1
   and WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
   and WOA.APPROVAL_STATUS_ID in (3,6)
 group by WOC.WORK_ORDER_ID
union all
select WOC.WORK_ORDER_ID, max(TWO.BATCH_REPORT_ID) REVISION, -6 BUDGET_VERSION_ID, 1 ACTIVE
  from WORK_ORDER_CONTROL WOC, TEMP_WORK_ORDER TWO
 where WOC.FUNDING_WO_INDICATOR = 1
   and WOC.WORK_ORDER_ID = TWO.WORK_ORDER_ID
   and TWO.BATCH_REPORT_ID > 0
 group by WOC.WORK_ORDER_ID;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2940, 0, 2015, 2, 1, 0, 45107, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045107_budgetcap_updated_bv_sv_views_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;