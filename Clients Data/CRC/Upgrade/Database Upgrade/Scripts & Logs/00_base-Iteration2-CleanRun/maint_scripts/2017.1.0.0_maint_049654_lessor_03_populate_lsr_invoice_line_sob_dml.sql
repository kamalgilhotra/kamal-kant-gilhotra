/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049654_lessor_03_populate_lsr_invoice_line_sob_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/09/2017 Charlie Shilling add default to set_of_books_id column so that we can NOT NULL it.
||============================================================================
*/
UPDATE lsr_invoice_line
SET set_of_books_id = 1
WHERE set_of_books_id IS NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3901, 0, 2017, 1, 0, 0, 49654, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049654_lessor_03_populate_lsr_invoice_line_sob_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	sys_context('USERENV', 'SERVICE_NAME'));
COMMIT;