/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050928_lessee_01_ls_ilr_load_balances_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 04/12/2018 Josh Sandler     Conversion Deferred/Prepaid Rent Table DDL
||============================================================================
*/

create table ls_ilr_load_balances (
  ilr_id              NUMBER(22,0) not null,
  revision            NUMBER(22,0) not null,
  set_of_books_id     NUMBER(22,0) not null,
  month               DATE not null,
  beg_deferred_rent   NUMBER(22,2) default 0,
  beg_prepaid_rent    NUMBER(22,2) default 0,
  user_id             VARCHAR2(18),
  time_stamp          DATE);

alter table ls_ilr_load_balances
  add constraint ls_ilr_load_balances_pk primary key (ilr_id, revision, set_of_books_id)
  using index tablespace PWRPLANT_IDX;

alter table ls_ilr_load_balances
  add constraint ls_ilr_load_balances_fk_ilrrev foreign key (ilr_id, revision)
  references ls_ilr_approval(ilr_id, revision);

alter table ls_ilr_load_balances
  add constraint ls_ilr_load_balances_fk_sob foreign key (set_of_books_id)
  references set_of_books(set_of_books_id);

comment on table ls_ilr_load_balances is '(S) [] The table stores conversion Deferred/Prepaid Rent Balances associated to an ILR.';
comment on column ls_ilr_load_balances.ilr_id is 'System-assigned identifier of a Lessee ILR.';
comment on column ls_ilr_load_balances.revision is 'System-assigned identifier of a Lessee ILR revision.';
COMMENT ON COLUMN ls_ilr_load_balances.set_of_books_id IS 'The internal set of books id within PowerPlant.';
comment on column ls_ilr_load_balances.month is 'The month which the beginning balances are effective.';
comment on column ls_ilr_load_balances.beg_deferred_rent is 'The beginning deferred rent balance.';
comment on column ls_ilr_load_balances.beg_prepaid_rent is 'The beginning prepaid rent balance.';
comment on column ls_ilr_load_balances.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_ilr_load_balances.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

ALTER TABLE ls_ilr_stg
  ADD conversion_balance_month DATE;

comment on column ls_ilr_stg.conversion_balance_month is 'The month which conversion beginning balances are effective.';


ALTER TABLE ls_ilr_asset_stg
  ADD conversion_balance_month DATE;

comment on column ls_ilr_asset_stg.conversion_balance_month is 'The month which conversion beginning balances are effective.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4865, 0, 2017, 4, 0, 0, 50928, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050928_lessee_01_ls_ilr_load_balances_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;