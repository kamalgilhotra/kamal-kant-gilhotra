/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029155_proptax_basegui_transition.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/18/2013 Stephen Wicks  Point Release
||============================================================================
*/
-- move report packages from ptc_report_package to ppbase_report_package
insert into PPBASE_REPORT_PACKAGE
   (PACKAGE_ID, TIME_STAMP, USER_ID, PACKAGE_NUMBER, DESCRIPTION, LONG_DESCRIPTION,
    SEPARATE_JOBS_ON, NAME_JOBS_ON)
   select PACKAGE_ID,
          TIME_STAMP,
          USER_ID,
          PACKAGE_NUMBER,
          DESCRIPTION,
          LONG_DESCRIPTION,
          SEPARATE_JOBS_ON,
          NAME_JOBS_ON
     from PTC_REPORT_PACKAGE;

-- move report package reports from ptc_report_package_reports to ppbase_report_package_reports
insert into PPBASE_REPORT_PACKAGE_REPORTS
   (PACKAGE_ID, REPORT_ID, TIME_STAMP, USER_ID, REPORT_ORDER, WHERE_CLAUSE)
   select PACKAGE_ID, REPORT_ID, TIME_STAMP, USER_ID, REPORT_ORDER, WHERE_CLAUSE
     from PTC_REPORT_PACKAGE_REPORTS;

-- drop the old property tax report package tables
drop table PTC_REPORT_PACKAGE_REPORTS;
drop table PTC_REPORT_PACKAGE;

-- update the filter user object names for the property tax filters
update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ptc_selecttabs_ledger'      where PP_REPORT_FILTER_ID = 7;
update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ptc_selecttabs_bills'       where PP_REPORT_FILTER_ID = 8;
update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ptc_selecttabs_parcels'     where PP_REPORT_FILTER_ID = 9;
update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ptc_selecttabs_payments'    where PP_REPORT_FILTER_ID = 15;
update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ptc_selecttabs_preallo'     where PP_REPORT_FILTER_ID = 16;
update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ptc_selecttabs_allocations' where PP_REPORT_FILTER_ID = 20;
update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ptc_selecttabs_allocations' where PP_REPORT_FILTER_ID = 21;
update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ptc_selecttabs_scenario'    where PP_REPORT_FILTER_ID = 22;

-- update the time option user object names and other settings for the property tax input parameters (i.e., time options)
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_date_single' where PP_REPORT_TIME_OPTION_ID in (7, 22);
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_date_span'   where PP_REPORT_TIME_OPTION_ID in (8, 23);
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_one',  DWNAME1 = 'dddw_pt_tax_year',        LABEL1 = 'Tax Year'        where PP_REPORT_TIME_OPTION_ID = 9;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_one',  DWNAME1 = 'dddw_pt_case',            LABEL1 = 'Case'            where PP_REPORT_TIME_OPTION_ID = 18;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_one',  DWNAME1 = 'dddw_pt_statement_year',  LABEL1 = 'Statement Year'  where PP_REPORT_TIME_OPTION_ID = 24;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_one',  DWNAME1 = 'dddw_pt_assessment_year', LABEL1 = 'Assessment Year' where PP_REPORT_TIME_OPTION_ID = 33;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_one',  DWNAME1 = 'dddw_pt_val_scenario',    LABEL1 = 'Scenario'        where PP_REPORT_TIME_OPTION_ID = 36;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_two',  DWNAME1 = 'dddw_pt_tax_year',        LABEL1 = 'Current Tax Year',       DWNAME2 = 'dddw_pt_tax_year',       LABEL2 = 'Prior Tax Year'       where PP_REPORT_TIME_OPTION_ID = 12;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_two',  DWNAME1 = 'dddw_pt_case',            LABEL1 = 'Current Case',           DWNAME2 = 'dddw_pt_case',           LABEL2 = 'Prior Case'           where PP_REPORT_TIME_OPTION_ID = 19;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_two',  DWNAME1 = 'dddw_pt_statement_year',  LABEL1 = 'Current Statement Year', DWNAME2 = 'dddw_pt_statement_year', LABEL2 = 'Prior Statement Year' where PP_REPORT_TIME_OPTION_ID = 27;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_two',  DWNAME1 = 'dddw_pt_val_scenario',    LABEL1 = 'Current Scenario',       DWNAME2 = 'dddw_pt_val_scenario',   LABEL2 = 'Prior Scenario'       where PP_REPORT_TIME_OPTION_ID = 37;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_date', DWNAME1 = 'dddw_pt_tax_year',        LABEL1 = 'Tax Year'        where PP_REPORT_TIME_OPTION_ID = 10;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_date', DWNAME1 = 'dddw_pt_statement_year',  LABEL1 = 'Statement Year'  where PP_REPORT_TIME_OPTION_ID = 25;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_date', DWNAME1 = 'dddw_pt_assessment_year', LABEL1 = 'Assessment Year' where PP_REPORT_TIME_OPTION_ID = 34;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_multigrid', DWNAME1 = 'dw_pt_tax_year_filter',   LABEL1 = 'Tax Years to Compare',       KEYCOLUMN1 = 'tax_year' where PP_REPORT_TIME_OPTION_ID = 15;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_multigrid', DWNAME1 = 'dw_pt_case_filter',       LABEL1 = 'Cases to Compare',           KEYCOLUMN1 = 'case_id' where PP_REPORT_TIME_OPTION_ID = 20;
update PP_REPORTS_TIME_OPTION set PARAMETER_UO_NAME = 'uo_ppbase_report_parms_multigrid', DWNAME1 = 'dw_pt_statement_filter',  LABEL1 = 'Statement Years to Compare', KEYCOLUMN1 = 'statement_year_id' where PP_REPORT_TIME_OPTION_ID = 30;

-- remove unused property tax time options (these were never implemented)
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 11;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 13;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 14;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 16;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 17;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 26;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 28;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 29;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 31;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 32;
delete from PP_REPORTS_TIME_OPTION where PP_REPORT_TIME_OPTION_ID = 35;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (415, 0, 10, 4, 1, 0, 29155, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029155_proptax_basegui_transition.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
