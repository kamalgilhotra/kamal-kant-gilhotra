/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049654_lessor_04_table_changes_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/09/2017 Charlie Shilling NOT NULL the SOB column on lsr_invoice_line
||											add FK to set_of_books table
||											Modify PK on lsr_invoice_line
||											rename invoice_amount to invoice_principal since it's being used differently than the LESSEE side
||											recreate view to account for changed column name
||============================================================================
*/
ALTER TABLE lsr_invoice_line
MODIFY set_of_books_id NOT NULL;

ALTER TABLE lsr_invoice_line
  ADD CONSTRAINT fk_lsr_invoice_sob FOREIGN KEY (
    set_of_books_id
  ) REFERENCES set_of_books (
    set_of_books_id
  )
;

ALTER TABLE lsr_invoice_line
DROP CONSTRAINT pk_lsr_invoice_line;

ALTER TABLE lsr_invoice_line
  ADD CONSTRAINT pk_lsr_invoice_line PRIMARY KEY (
    invoice_id,
    invoice_line_number,
    invoice_type_id,
	set_of_books_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
;

ALTER TABLE lsr_invoice
RENAME COLUMN invoice_amount TO invoice_principal;

CREATE OR REPLACE VIEW v_lsr_invoice_fx_vw (
  invoice_id,
  invoice_number,
  ilr_id,
  gl_posting_mo_yr,
  invoice_principal,
  invoice_interest,
  invoice_executory,
  invoice_contingent,
  lease_id,
  lease_number,
  ls_cur_type,
  exchange_date,
  contract_currency_id,
  display_currency_id,
  company_currency_id,
  iso_code,
  currency_display_symbol
) AS
WITH
cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
 invoice_id,
 invoice_number,
 inv.ilr_id,
 gl_posting_Mo_yr,
 invoice_principal * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_principal,
 invoice_interest * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_interest,
 invoice_executory * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_executory,
 invoice_contingent * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_contingent,
 lease.lease_id,
 lease.lease_number,
 cur.ls_cur_type ls_cur_type,
 cr.exchange_date,
 lease.contract_currency_id,
 cur.currency_id display_currency_id,
 cs.currency_id company_currency_id,
 cur.iso_code,
 cur.currency_display_symbol
 FROM lsr_invoice inv
 INNER JOIN lsr_ilr ilr
   on inv.ilr_id = ilr.ilr_id
 INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
 INNER JOIN lsr_lease lease
    ON ilr.lease_id = lease.lease_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 LEFT OUTER JOIN ls_lease_calculated_date_rates cr
    ON lease.contract_currency_id = cr.contract_currency_id
   AND cur.currency_id = cr.company_currency_id
   AND ilr.company_id = cr.company_id
   AND inv.gl_posting_mo_yr = cr.accounting_month
 WHERE Nvl(cr.exchange_rate_type_id, 1) = 1
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3902, 0, 2017, 1, 0, 0, 49654, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049654_lessor_04_table_changes_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	sys_context('USERENV', 'SERVICE_NAME'));
COMMIT;