/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052833_lessor_01_partial_month_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 03/20/2019  Sarah Byers      Add Partial Month Payment Term Type functionality to Lessor
||============================================================================
*/

-- LSR_ILR_SCHEDULE
alter table lsr_ilr_schedule add partial_month_percent number(22,8);
comment on column lsr_ilr_schedule.partial_month_percent is 'The percentage each schedule month to be considered from the payment term for the Partial Month type.  All months are equal to 1 except the first and last months.';

-- lsr_ilr_prior_schedule_temp
alter table lsr_ilr_prior_schedule_temp add partial_month_percent number(22,8);
comment on column lsr_ilr_prior_schedule_temp.partial_month_percent is 'The percentage each schedule month to be considered from the payment term for the Partial Month type.  All months are equal to 1 except the first and last months.';

-- drop t_lsr_ilr_sales_df_prelims which is dependent on lsr_ilr_op_sch_pay_info_tab
drop type t_lsr_ilr_sales_df_prelims;


-- LS_ILR_OP_SCH_PAY_INFO
CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_PAY_INFO FORCE AUTHID current_user AS OBJECT  (payment_month_frequency NUMBER,
                                                                                payment_term_start_date date,
                                                                                MONTH DATE,
                                                                                number_of_terms NUMBER,
                                                                                payment_amount NUMBER,
                                                                                group_fixed_payment_amount NUMBER,
                                                                                contingent_buckets lsr_bucket_amount_tab,
                                                                                executory_buckets lsr_bucket_amount_tab,
                                                                                is_prepay NUMBER(1,0),
                                                                                partial_month_percent NUMBER(22,8),
                                                                                iter NUMBER)
/

-- LSR_ILR_OP_SCH_PAY_INFO_TAB
CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_PAY_INFO_TAB as table of lsr_ilr_op_sch_pay_info;
/

-- t_lsr_ilr_sales_df_prelims which is dependent on lsr_ilr_op_sch_pay_info_tab
create or replace TYPE t_lsr_ilr_sales_df_prelims AUTHID current_user AS OBJECT ( payment_info lsr_ilr_op_sch_pay_info_tab,
                                                              rates t_lsr_ilr_schedule_all_rates,
                                                              initial_direct_costs lsr_init_direct_cost_info_tab,
                                                              sales_type_info lsr_ilr_sales_sch_info);
/

-- LSR_ILR_OP_SCH_PAY_TERM
CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_PAY_TERM FORCE AUTHID current_user AS OBJECT( payment_month_frequency NUMBER,
                                                                              payment_term_start_date date,
                                                                              number_of_terms NUMBER,
                                                                              payment_amount NUMBER,
                                                                              executory_buckets lsr_bucket_amount_tab,
                                                                              contingent_buckets lsr_bucket_amount_tab,
                                                                              is_prepay number(1,0),
                                                                              remeasurement_date date,
                                                                              payment_term_id NUMBER(22,0),
                                                                              payment_term_type_id NUMBER(22,0),
                                                                              first_partial_month_percent NUMBER(22,8),
                                                                              last_partial_month_percent NUMBER(22,8))
/

-- LSR_ILR_OP_SCH_PAY_TERM_TAB
CREATE OR REPLACE  TYPE LSR_ILR_OP_SCH_PAY_TERM_TAB as table of lsr_ilr_op_sch_pay_term;
/

-- LSR_SCHEDULE_PAYMENT_DEF
CREATE OR REPLACE TYPE LSR_SCHEDULE_PAYMENT_DEF FORCE AUTHID current_user AS OBJECT (payment_month_frequency NUMBER,
                                                            is_prepay number(1,0),
                                                            payment_group NUMBER, --Group months based on date payment takes place
                                                            MONTH DATE,
                                                            number_of_terms NUMBER,
                                                            iter NUMBER,
                                                            group_fixed_payment NUMBER,
                                                            payment_amount NUMBER,
                                                            partial_month_percent NUMBER(22,8));
/

-- LSR_SCHEDULE_PAYMENT_DEF_TAB
CREATE OR REPLACE TYPE LSR_SCHEDULE_PAYMENT_DEF_TAB as table of lsr_schedule_payment_def;
/

-- LSR_ILR_OP_SCH_RESULT
CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_RESULT FORCE AUTHID current_user IS OBJECT( MONTH                        DATE,
                                                                            interest_income_received     NUMBER,
                                                                            interest_income_accrued      NUMBER,
                                                                            interest_rental_recvd_spread NUMBER,
                                                                            begin_deferred_rev           NUMBER,
                                                                            deferred_rev                 NUMBER,
                                                                            end_deferred_rev             NUMBER,
                                                                            begin_receivable             NUMBER,
                                                                            end_receivable               NUMBER,
                                                                            begin_lt_receivable          NUMBER,
                                                                            end_lt_receivable            NUMBER,
                                                                            initial_direct_cost          NUMBER,
                                                                            executory_accrual1           NUMBER,
                                                                            executory_accrual2           NUMBER,
                                                                            executory_accrual3           NUMBER,
                                                                            executory_accrual4           NUMBER,
                                                                            executory_accrual5           NUMBER,
                                                                            executory_accrual6           NUMBER,
                                                                            executory_accrual7           NUMBER,
                                                                            executory_accrual8           NUMBER,
                                                                            executory_accrual9           NUMBER,
                                                                            executory_accrual10          NUMBER,
                                                                            executory_paid1              NUMBER,
                                                                            executory_paid2              NUMBER,
                                                                            executory_paid3              NUMBER,
                                                                            executory_paid4              NUMBER,
                                                                            executory_paid5              NUMBER,
                                                                            executory_paid6              NUMBER,
                                                                            executory_paid7              NUMBER,
                                                                            executory_paid8              NUMBER,
                                                                            executory_paid9              NUMBER,
                                                                            executory_paid10             NUMBER,
                                                                            contingent_accrual1          NUMBER,
                                                                            contingent_accrual2          NUMBER,
                                                                            contingent_accrual3          NUMBER,
                                                                            contingent_accrual4          NUMBER,
                                                                            contingent_accrual5          NUMBER,
                                                                            contingent_accrual6          NUMBER,
                                                                            contingent_accrual7          NUMBER,
                                                                            contingent_accrual8          NUMBER,
                                                                            contingent_accrual9          NUMBER,
                                                                            contingent_accrual10         NUMBER,
                                                                            contingent_paid1             NUMBER,
                                                                            contingent_paid2             NUMBER,
                                                                            contingent_paid3             NUMBER,
                                                                            contingent_paid4             NUMBER,
                                                                            contingent_paid5             NUMBER,
                                                                            contingent_paid6             NUMBER,
                                                                            contingent_paid7             NUMBER,
                                                                            contingent_paid8             NUMBER,
                                                                            contingent_paid9             NUMBER,
                                                                            contingent_paid10            NUMBER,
                                                                            begin_deferred_rent          NUMBER,
                                                                            deferred_rent                NUMBER,
                                                                            end_deferred_rent            NUMBER,
                                                                            begin_accrued_rent           NUMBER,
                                                                            accrued_rent                 NUMBER,
                                                                            end_accrued_rent             NUMBER,
                                                                            receivable_remeasurement     NUMBER,
                                                                            lt_receivable_remeasurement  NUMBER,
                                                                            partial_month_percent        NUMBER(22,8));
/

-- LSR_ILR_OP_SCH_RESULT_TAB
CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_RESULT_TAB AS TABLE OF lsr_ilr_op_sch_result;
/

-- Rebuild v_lsr_ilr_op_schedule_calc
CREATE OR REPLACE FORCE VIEW v_lsr_ilr_op_schedule_calc AS
SELECT  results.ilr_id,
        results.revision,
        results.set_of_books_id,
        cols.month,
        cols.interest_income_received,
        cols.interest_income_accrued,
        cols.interest_rental_recvd_spread,
        cols.begin_deferred_rev,
        cols.deferred_rev,
        cols.end_deferred_rev,
        cols.begin_receivable,
        cols.end_receivable,
        cols.begin_lt_receivable,
        cols.end_lt_receivable,
        cols.initial_direct_cost,
        cols.executory_accrual1,
        cols.executory_accrual2,
        cols.executory_accrual3,
        cols.executory_accrual4,
        cols.executory_accrual5,
        cols.executory_accrual6,
        cols.executory_accrual7,
        cols.executory_accrual8,
        cols.executory_accrual9,
        cols.executory_accrual10,
        cols.executory_paid1,
        cols.executory_paid2,
        cols.executory_paid3,
        cols.executory_paid4,
        cols.executory_paid5,
        cols.executory_paid6,
        cols.executory_paid7,
        cols.executory_paid8,
        cols.executory_paid9,
        cols.executory_paid10,
        cols.contingent_accrual1,
        cols.contingent_accrual2,
        cols.contingent_accrual3,
        cols.contingent_accrual4,
        cols.contingent_accrual5,
        cols.contingent_accrual6,
        cols.contingent_accrual7,
        cols.contingent_accrual8,
        cols.contingent_accrual9,
        cols.contingent_accrual10,
        cols.contingent_paid1,
        cols.contingent_paid2,
        cols.contingent_paid3,
        cols.contingent_paid4,
        cols.contingent_paid5,
        cols.contingent_paid6,
        cols.contingent_paid7,
        cols.contingent_paid8,
        cols.contingent_paid9,
        cols.contingent_paid10,
        t_lsr_ilr_schedule_all_rates( t_lsr_rates_implicit_in_lease(NULL, NULL), 
                                          t_lsr_rates_implicit_in_lease(NULL, NULL), 
                                          t_lsr_rates_implicit_in_lease(NULL, NULL)) as schedule_rates
FROM (SELECT  ilro.ilr_id,
              ilro.revision,
              fasb_sob.set_of_books_id,
              pkg_lessor_schedule.f_get_op_schedule(ilr_id,revision) sch
      FROM lsr_ilr_options ilro
      JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
      JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
      WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION) ) = 'operating') results, TABLE ( results.sch ) (+) cols;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16148, 0, 2018, 2, 0, 0, 52833, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052833_lessor_01_partial_month_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
