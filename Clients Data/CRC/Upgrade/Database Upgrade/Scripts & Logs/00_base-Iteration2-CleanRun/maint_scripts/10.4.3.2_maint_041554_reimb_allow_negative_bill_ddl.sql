/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041554_reimb_allow_negative_bill_ddl.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 01/08/2015 A. Rajashekar       Add new column ALLOW_NEGATIVE_BILL
||
||========================================================================================
*/

--	Add new column ALLOW_NEGATIVE_BILL to the following tables

alter table REIMB_METHOD add ALLOW_NEGATIVE_BILL number(22,0) ;

comment on column REIMB_METHOD.ALLOW_NEGATIVE_BILL is 'Yes (1) / No (0) indicator of whether negative bills are allowed.';

alter table REIMB_BILL_GROUP add ALLOW_NEGATIVE_BILL number(22,0) ;

comment on column REIMB_BILL_GROUP.ALLOW_NEGATIVE_BILL is 'Yes (1) / No (0) indicator of whether negative bills are allowed.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2153, 0, 10, 4, 3, 2, 41554, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041554_reimb_allow_negative_bill_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;