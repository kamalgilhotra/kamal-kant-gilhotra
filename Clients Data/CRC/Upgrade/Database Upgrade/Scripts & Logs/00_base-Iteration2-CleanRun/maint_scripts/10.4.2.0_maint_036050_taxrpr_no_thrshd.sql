/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036050_taxrpr_no_thrshd.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/03/2014 Alex Pivoshenko
||============================================================================
*/

insert into REPAIR_PRETEST_EXPLAIN
   (ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (11, 'No Threshold',
    'No tax threshold exists or tax threshold data is incomplete, for example no replacement cost for tax uop that is tested on cost');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (939, 0, 10, 4, 2, 0, 36050, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036050_taxrpr_no_thrshd.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;