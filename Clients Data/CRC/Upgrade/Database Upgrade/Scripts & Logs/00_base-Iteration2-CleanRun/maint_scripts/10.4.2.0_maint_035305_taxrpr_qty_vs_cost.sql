/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035305_taxrpr_qty_vs_cost.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.2.0   01/17/2014 Alex P.
||============================================================================
*/

alter table RPR_IMPORT_TEST     drop column QUANTITY_VS_DOLLARS;
alter table RPR_IMPORT_TEST_ARC drop column QUANTITY_VS_DOLLARS;
alter table WO_TAX_EXPENSE_TEST drop column QUANTITY_VS_DOLLARS

-- Remove quantity_vs_dollars from the wo_tax_expense_test import.
--
update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID + 100
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 105)
   and exists (select 'x'
          from PP_IMPORT_TEMPLATE_FIELDS INFLDS
         where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
           and COLUMN_NAME = 'quantity_vs_dollars')
   and FIELD_ID > (select FIELD_ID
                     from PP_IMPORT_TEMPLATE_FIELDS INFLDS
                    where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
                      and COLUMN_NAME = 'quantity_vs_dollars');

delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME = 'quantity_vs_dollars'
   and IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 105);

update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID - 101
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 105)
   and FIELD_ID > 100;

delete from PP_IMPORT_COLUMN
 where IMPORT_TYPE_ID = 105
   and COLUMN_NAME = 'quantity_vs_dollars';

 ----- Add  QUANTITY_VS_COST column to repair_unit_code table

alter table RPR_IMPORT_UNIT_CODE add QUANTITY_VS_COST number(2,0);
comment on column RPR_IMPORT_UNIT_CODE.QUANTITY_VS_COST is 'A flag indicating whether quantity or cost is used during Unit Of Property testing. The column is used by the import tool to import into repair_unit_code.quantity_vs_cost column.';

alter table RPR_IMPORT_UNIT_CODE add QUANTITY_VS_COST_XLATE varchar2(254);
comment on column RPR_IMPORT_UNIT_CODE.QUANTITY_VS_COST_XLATE is 'A flag used to translate quantity_vs_cost id into description. The column is used by the import tool to import into repair_unit_code.quantity_vs_cost column.';

alter table RPR_IMPORT_UNIT_CODE_ARC add QUANTITY_VS_COST number(2,0);
comment on column RPR_IMPORT_UNIT_CODE_ARC.QUANTITY_VS_COST is 'A flag indicating whether quantity or cost is used during Unit Of Property testing. The column is used by the import tool to import into repair_unit_code.quantity_vs_cost column.';

alter table RPR_IMPORT_UNIT_CODE_ARC add QUANTITY_VS_COST_XLATE varchar2(254);
comment on column RPR_IMPORT_UNIT_CODE_ARC.QUANTITY_VS_COST_XLATE is 'A flag used to translate quantity_vs_cost id into description. The column is used by the import tool to import into repair_unit_code.quantity_vs_cost column.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN)
values
   (109, 'quantity_vs_cost', sysdate, user, 'Quantity vs Cost', 'quantity_vs_cost_xlate', 1, 1,
    'number(2,0)', 'repair_lookup_qty_cost', '', 1, null, 'id');

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL,
    IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS)
values
   (1070, sysdate, user, 'Repair Lookup Qty vs Cost.Description',
    'The passed in value corresponds to the Qty vs Cost flag.  Translate to the ID using the Repair Lookup Qty Cost table.',
    'quantity_vs_cost',
    '( select r.id from repair_lookup_qty_cost r where upper( trim( <importfield> ) ) = upper( trim( r.description ) ) )',
    0, 'repair_lookup_qty_cost', 'description', '');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
   (select 109, 'quantity_vs_cost', IMPORT_LOOKUP_ID, sysdate, user
      from PP_IMPORT_LOOKUP
     where DESCRIPTION = 'Repair Lookup Qty vs Cost.Description');

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select IMPORT_TEMPLATE_ID, 6, IMPORT_TYPE_ID, 'quantity_vs_cost', PICL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TEMPLATE_FIELDS, PP_IMPORT_LOOKUP PICL
     where PICL.DESCRIPTION = 'Repair Lookup Qty vs Cost.Description'
       and IMPORT_TYPE_ID = 109
       and FIELD_ID = 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (876, 0, 10, 4, 2, 0, 35305, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035305_taxrpr_qty_vs_cost.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;