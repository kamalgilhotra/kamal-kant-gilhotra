/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_043809_jobserver_inc_20150506_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     05/06/2014 Ryan Oliveria    	 Creating new table 'PP_WEB_CONTROL_SETTINGS'
||==========================================================================================
*/

SET serveroutput ON size 30000;

DECLARE
	doesTableExist NUMBER := 0;
BEGIN

	--* TABLE: PP_WEB_CONTROL_SETTINGS
	BEGIN
		doesTableExist := SYS_DOES_TABLE_EXIST('PP_WEB_CONTROL_SETTINGS','PWRPLANT');

		IF doesTableExist = 0 THEN
			BEGIN
				dbms_output.put_line('Creating table PP_WEB_CONTROL_SETTINGS ');

				EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_WEB_CONTROL_SETTINGS"
				(
				"OBJECTS" VARCHAR2(100 BYTE) CONSTRAINT "PP_WEB_CONTROL_SETTINGS01" NOT NULL ENABLE,
				"USERS" VARCHAR2(18 BYTE) CONSTRAINT "PP_WEB_CONTROL_SETTINGS02" NOT NULL ENABLE,
				"SETTINGS" VARCHAR2(4000 BYTE),
				"TIME_STAMP" DATE,
				"USER_ID" VARCHAR2(18 BYTE),
				CONSTRAINT "PP_WEB_CONTROL_SETTINGS_PK" PRIMARY KEY ("OBJECTS", "USERS")
				) TABLESPACE PWRPLANT';

				EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_WEB_CONTROL_SETTINGS" IS ''User settings table''';

				EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_CONTROL_SETTINGS"."OBJECTS" IS ''The ID of the page this setting is used on''';
				EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_CONTROL_SETTINGS"."USERS" IS ''The ID of the user this value is being stored for''';
				EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_CONTROL_SETTINGS"."SETTINGS" IS ''The value of the setting''';
				EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_CONTROL_SETTINGS"."TIME_STAMP" IS ''Timestamp of record''';
				EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_CONTROL_SETTINGS"."USER_ID" IS ''User ID for auditing''';

				EXECUTE IMMEDIATE 'CREATE or REPLACE PUBLIC SYNONYM PP_WEB_CONTROL_SETTINGS for pwrplant.PP_WEB_CONTROL_SETTINGS';

				EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_WEB_CONTROL_SETTINGS_AUDIT"
				before update or insert on PP_WEB_CONTROL_SETTINGS
				for each row
				BEGIN
				:new.user_id := USER;
				:new.time_stamp := SYSDATE;
				END;';

				EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_WEB_CONTROL_SETTINGS_AUDIT" ENABLE';

				EXECUTE IMMEDIATE 'GRANT ALL on PP_WEB_CONTROL_SETTINGS to pwrplant_role_dev';

			END;
		ELSE
			BEGIN
				dbms_output.put_line('Table PP_WEB_CONTROL_SETTINGS exists');
			END;
		END IF;
	END;

END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2554, 0, 2015, 1, 0, 0, 43809, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043809_jobserver_inc_20150506_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;