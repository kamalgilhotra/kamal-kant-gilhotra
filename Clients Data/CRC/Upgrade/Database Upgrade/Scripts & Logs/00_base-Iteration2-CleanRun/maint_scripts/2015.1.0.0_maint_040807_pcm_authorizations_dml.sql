 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_040807_pcm_authorizations_dml.sql
|| Description: Add authorizations workspace to PCM port
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/08/2014 Ryan Oliveria  New Module
||============================================================================
*/

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_pcm_auth_wksp'
 where MODULE = 'pcm'
   and WORKSPACE_IDENTIFIER in ('fp_authorize', 'wo_authorize');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2156, 0, 2015, 1, 0, 0, 040807, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040807_pcm_authorizations_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;