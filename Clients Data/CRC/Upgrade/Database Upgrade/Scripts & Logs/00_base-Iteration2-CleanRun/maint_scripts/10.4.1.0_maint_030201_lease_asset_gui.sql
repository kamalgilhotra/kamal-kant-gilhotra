/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030201_lease_asset_gui.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Kyle Peterson  Point Release
||============================================================================
*/

alter table LS_ASSET
   add (FMV                        number(22,2),
        PROPERTY_GROUP_ID          number(22,0),
        UTILITY_ACCOUNT_ID         number(22,0),
        BUS_SEGMENT_ID             number(22,0),
        CAPITALIZED_COST           number(22,2),
        SUB_ACCOUNT_ID             number(22,0),
        RETIREMENT_UNIT_ID         number(22,0),
        WORK_ORDER_ID              number(22,0),
        FUNC_CLASS_ID              number(22,0),
        ASSET_LOCATION_ID          number(22,0),
        IN_SERVICE_DATE            date,
        SERIAL_NUMBER              varchar2(35),
        ACTUAL_RESIDUAL_AMOUNT     number(22,2),
        GUARANTEED_RESIDUAL_AMOUNT number(22,2),
        NOTES                      varchar2(4000));

alter table LS_ASSET
   drop (PROPERTY_TAX_MO_YR,
         PROPERTY_TAX_COST,
         EXTERNAL_SYS,
         EXTERNAL_SYS_ID,
         CURRENT_LEASE_COST,
         RESIDUAL_AMOUNT,
         TAG_NUMBER);

ALTER TABLE LS_ASSET
   add constraint FK_ASSET_SUB_ACCOUNT
       foreign key (UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID)
       references SUB_ACCOUNT(UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID);

alter table LS_ASSET
   add constraint FK_ASSET_PROPERTY_GROUP
       foreign key (PROPERTY_GROUP_ID)
       references PROPERTY_GROUP(PROPERTY_GROUP_ID);

alter table LS_ASSET
   add constraint FK_ASSET_RETIREMENT_UNIT
       foreign key (RETIREMENT_UNIT_ID)
       references RETIREMENT_UNIT(RETIREMENT_UNIT_ID);

alter table LS_ASSET
   add constraint FK_ASSET_WORK_ORDER
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL(WORK_ORDER_ID);

alter table LS_ASSET
   add constraint FK_ASSET_FUNC_CLASS
       foreign key (FUNC_CLASS_ID)
       references FUNC_CLASS(FUNC_CLASS_ID);

alter table LS_ASSET
   add constraint FK_ASSET_ASSET_LOCATION
       foreign key (ASSET_LOCATION_ID)
       references ASSET_LOCATION(ASSET_LOCATION_ID);

create table LS_ASSET_DOCUMENT
(
 LS_ASSET_ID   number(22,0) not null,
 DOCUMENT_DATA blob,
 DOCUMENT_ID   number(22,0) not null,
 NOTES         varchar2(2000),
 DESCRIPTION   varchar2(35),
 FILE_NAME     varchar2(100) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 FILESIZE      number(22,0),
 FILE_LINK     varchar2(2000)
);

alter table LS_ASSET_DOCUMENT
   add constraint PK_LS_ASSET_DOCUMENT
       primary key (LS_ASSET_ID, DOCUMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_ASSET_DOCUMENT
   add constraint FK1_LS_ASSET_DOCUMENT
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (434, 0, 10, 4, 1, 0, 30201, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030201_lease_asset_gui.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
