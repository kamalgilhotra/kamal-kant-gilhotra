/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037402_cwip_ocr_units.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.3.0 06/20/2014 Sunjin Cone    OCR for Unitization
||=================================================================================
*/

create table UNITIZE_CONVERT_OCR_STG
(
 COMPANY_ID         number(22,0),
 WORK_ORDER_ID      number(22,0) not null,
 UNIT_ITEM_ID       number(22,0) not null,
 CHARGE_GROUP_ID    number(22,0) not null,
 CHARGE_TYPE_ID     number(22,0),
 RETIREMENT_UNIT_ID number(22,0),
 UTILITY_ACCOUNT_ID number(22,0),
 BUS_SEGMENT_ID     number(22,0),
 SUB_ACCOUNT_ID     number(22,0),
 PROPERTY_GROUP_ID  number(22,0),
 ASSET_LOCATION_ID  number(22,0),
 CGC_QUANTITY       number(22,2),
 CGC_AMOUNT         number(22,2),
 TIME_STAMP         date,
 USER_ID            varchar2(18)
);

alter table UNITIZE_CONVERT_OCR_STG
   add constraint PK_UNITIZE_CONVERT_OCR_STG
       primary key (WORK_ORDER_ID, UNIT_ITEM_ID, CHARGE_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_CONVERT_OCR_STG is '(C) [04] The list of Original Cost Retirement Unit Items eligible to convert into summarized WO Estimates.';
comment on column UNITIZE_CONVERT_OCR_STG.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_CONVERT_OCR_STG.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_CONVERT_OCR_STG.UNIT_ITEM_ID is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_CONVERT_OCR_STG.CHARGE_GROUP_ID is 'System-assigned identifier of the the charge group.';
comment on column UNITIZE_CONVERT_OCR_STG.CHARGE_TYPE_ID is 'System-assigned identifier the OCR charge type.';
comment on column UNITIZE_CONVERT_OCR_STG.RETIREMENT_UNIT_ID is 'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_CONVERT_OCR_STG.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.';
comment on column UNITIZE_CONVERT_OCR_STG.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_CONVERT_OCR_STG.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_CONVERT_OCR_STG.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_CONVERT_OCR_STG.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_CONVERT_OCR_STG.CGC_QUANTITY is 'Quantity for the OCR entry.';
comment on column UNITIZE_CONVERT_OCR_STG.CGC_AMOUNT is 'Amount in dollars for the OCR entry.';
comment on column UNITIZE_CONVERT_OCR_STG.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_CONVERT_OCR_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_CONVERT_OCR
(
 COMPANY_ID         number(22,0),
 WORK_ORDER_ID      number(22,0) not null,
 UNIT_ITEM_ID       number(22,0) not null,
 CHARGE_GROUP_ID    number(22,0) not null,
 CHARGE_TYPE_ID     number(22,0),
 RETIREMENT_UNIT_ID number(22,0),
 UTILITY_ACCOUNT_ID number(22,0),
 BUS_SEGMENT_ID     number(22,0),
 SUB_ACCOUNT_ID     number(22,0),
 PROPERTY_GROUP_ID  number(22,0),
 ASSET_LOCATION_ID  number(22,0),
 CGC_QUANTITY       number(22,2),
 CGC_AMOUNT         number(22,2),
 TIME_STAMP         date,
 USER_ID            varchar2(18)
);

alter table UNITIZE_CONVERT_OCR
   add constraint PK_UNITIZE_CONVERT_OCR
       primary key (WORK_ORDER_ID, UNIT_ITEM_ID, CHARGE_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_CONVERT_OCR is '(C) [04] The list of Original Cost Retirement Unit Items converted into summarized WO Estimates.';
comment on column UNITIZE_CONVERT_OCR.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_CONVERT_OCR.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_CONVERT_OCR.UNIT_ITEM_ID is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_CONVERT_OCR.CHARGE_GROUP_ID is 'System-assigned identifier of the the charge group.';
comment on column UNITIZE_CONVERT_OCR.CHARGE_TYPE_ID is 'System-assigned identifier the OCR charge type.';
comment on column UNITIZE_CONVERT_OCR.RETIREMENT_UNIT_ID is 'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_CONVERT_OCR.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.';
comment on column UNITIZE_CONVERT_OCR.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_CONVERT_OCR.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_CONVERT_OCR.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_CONVERT_OCR.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_CONVERT_OCR.CGC_QUANTITY is 'Quantity for the OCR entry.';
comment on column UNITIZE_CONVERT_OCR.CGC_AMOUNT is 'Amount in dollars for the OCR entry.';
comment on column UNITIZE_CONVERT_OCR.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_CONVERT_OCR.USER_ID is 'Standard system-assigned user id used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1223, 0, 10, 4, 3, 0, 37402, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037402_cwip_ocr_units.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
