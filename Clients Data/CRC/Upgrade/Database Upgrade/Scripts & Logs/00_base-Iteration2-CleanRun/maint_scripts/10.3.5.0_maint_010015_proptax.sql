SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010015_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    04/27/2012 Julia Breuer   Point Release
|| 10.3.5.0    03/26/2013 Julia Breuer   ** This was added after the release
||                                       This will have to be done manually because clients
||                                       may not have all states and the id could be different.
|| 10.4.1.2    12/11/2013 Julia Breuer   Script was altered
||============================================================================
*/

--
-- Add a status code field to the following tables:
--    pt_type_rollup
--    pt_reserve_factors
--    pt_escalated_value_type
--
-- This will allow us to deliver all standard rows of data but hide the rows not needed by a specific client.
--
-- Set all current rows to active (status_code_id = 1) and make the field not nullable.
--
alter table PWRPLANT.PT_TYPE_ROLLUP          add STATUS_CODE_ID number(22,0);
alter table PWRPLANT.PT_RESERVE_FACTORS      add STATUS_CODE_ID number(22,0);
alter table PWRPLANT.PT_ESCALATED_VALUE_TYPE add STATUS_CODE_ID number(22,0);

alter table PWRPLANT.PT_TYPE_ROLLUP          add constraint PT_ROLLUP_STATUS_FK   foreign key ( STATUS_CODE_ID ) references PWRPLANT.STATUS_CODE;
alter table PWRPLANT.PT_RESERVE_FACTORS      add constraint PT_RSV_FCTR_STATUS_FK foreign key ( STATUS_CODE_ID ) references PWRPLANT.STATUS_CODE;
alter table PWRPLANT.PT_ESCALATED_VALUE_TYPE add constraint PT_ESCVAL_STATUS_FK   foreign key ( STATUS_CODE_ID ) references PWRPLANT.STATUS_CODE;

update PWRPLANT.PT_TYPE_ROLLUP          set STATUS_CODE_ID = 1;
update PWRPLANT.PT_RESERVE_FACTORS      set STATUS_CODE_ID = 1;
update PWRPLANT.PT_ESCALATED_VALUE_TYPE set STATUS_CODE_ID = 1;

alter table PWRPLANT.PT_TYPE_ROLLUP          modify STATUS_CODE_ID not null;
alter table PWRPLANT.PT_RESERVE_FACTORS      modify STATUS_CODE_ID not null;
alter table PWRPLANT.PT_ESCALATED_VALUE_TYPE modify STATUS_CODE_ID not null;

insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'status_code_id', 'pt_type_rollup', sysdate, user, 'status_code', 'p', null, 'Status Code', 'Status Code', 4, 'Status Code', null, null, null, null, null, 0 );
insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'status_code_id', 'pt_escalated_value_type', sysdate, user, 'status_code', 'p', null, 'Status Code', 'Status Code', 4, 'Status Code', null, null, null, null, null, 0 );

-- Add PT Reserve Factors back to Table Maint so that users can activate inactive factors.  Make reserve factor type read only.
insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'description', 'pt_reserve_factors', sysdate, user, null, 'e', null, 'Description', 'Description', 2, 'Description', null, null, null, null, null, 0 );
insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'long_description', 'pt_reserve_factors', sysdate, user, null, 'e', null, 'Long Description', 'Long Description', 3, 'Long Description', null, null, null, null, null, 0 );
insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'reserve_factor_id', 'pt_reserve_factors', sysdate, user, null, 's', null, 'Reserve Factor ID', 'Reserve Factor ID', 1, 'Reserve Factor ID', null, null, null, null, null, 0 );
insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'reserve_factor_type_id', 'pt_reserve_factors', sysdate, user, 'pt_reserve_factor_type_filter', 'p', null, 'Reserve Factor Type', 'Reserve Factor Type', 4, 'Reserve Factor Type', null, null, null, null, null, 1 );
insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'time_stamp', 'pt_reserve_factors', sysdate, user, null, 'e', null, 'Time Stamp', 'Time Stamp', 100, 'Time Stamp', null, null, null, null, null, 0 );
insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'user_id', 'pt_reserve_factors', sysdate, user, null, 'e', null, 'User ID', 'User ID', 101, 'User ID', null, null, null, null, null, 0 );
insert into PWRPLANT.POWERPLANT_COLUMNS ( COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY ) values ( 'status_code_id', 'pt_reserve_factors', sysdate, user, 'status_code', 'p', null, 'Status Code', 'Status Code', 5, 'Status Code', null, null, null, null, null, 0 );

--
-- Create a sequence to be used when creating new property tax types, rollups, reserve factors, and escalated value types.
-- This is needed so that users do not use IDs in the assigned ranges for standard values.  Start it above all ranges.
--
create sequence PWRPLANT.PT_TABLES_SEQ start with 100000;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (191, 0, 10, 3, 5, 0, 10015, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010015_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;


SET DEFINE ON