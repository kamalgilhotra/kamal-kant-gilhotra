/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038086_pwrtax_salvage_load_cor.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/09/2014 Anand Rajashekar    Adding workspace to power tax main menu and associating it to menu items
||
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'tax_depr_act_load_salvage_cor', 'Load Salvage / COR', 'uo_tax_depr_act_wksp_load_salvage_cor',
    'Load Salvage / COR');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'tax_depr_act_load_salvage_cor'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_salvage_load';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1241, 0, 10, 4, 3, 0, 38086, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038086_pwrtax_salvage_load_cor.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;