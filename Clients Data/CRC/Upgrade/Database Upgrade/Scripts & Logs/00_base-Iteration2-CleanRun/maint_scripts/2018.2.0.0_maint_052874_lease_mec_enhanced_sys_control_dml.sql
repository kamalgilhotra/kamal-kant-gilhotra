/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052874_lease_mec_enhanced_sys_control_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/18/19   Shane "C" Ward   New System Control for displaying Enhanced Lease MEC
||============================================================================
*/

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
select
   		max(control_id)+1 AS control_id,
   		'Lessee/Lessor MEC Enhancement' AS control_name,
   		'no' AS control_value,
   		'dw_yes_no;1' AS description,
   		'Set to YES to default the Lease Month End Close workspace to the enhanced workspace on open.' AS long_description,
   		-1 AS company_id
	from pp_system_control_company;
	
	
Update ppbase_workspace 
	set workspace_uo_name = 'uo_ls_mecntr_wksp_enhanced' 
where lower(workspace_identifier) = 'control_monthend'
and upper(module) in ('LESSEE', 'LESSOR');

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15202, 0, 2018, 2, 0, 0, 52874, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052874_lease_mec_enhanced_sys_control_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;