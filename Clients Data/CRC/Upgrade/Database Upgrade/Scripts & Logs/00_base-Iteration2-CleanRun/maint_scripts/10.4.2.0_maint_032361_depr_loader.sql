/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032361_depr_loader.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ---------------------------------
|| 10.4.2.0   12/31/2013 Sunjin Cone         table update
||============================================================================
*/

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select dg.depr_group_id from depr_group dg where upper( trim( <importfield> ) ) = upper( trim( dg.description ) ) and <importtable>.company_id = dg.company_id )'
 where IMPORT_LOOKUP_ID = 502;

update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select dg.depr_group_id from depr_group dg where upper( trim( <importfield> ) ) = upper( trim( dg.external_depr_code ) )  and <importtable>.company_id = dg.company_id )'
 where IMPORT_LOOKUP_ID = 503;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (827, 0, 10, 4, 2, 0, 32361, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032361_depr_loader.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;