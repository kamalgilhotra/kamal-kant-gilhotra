/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046429_prov_reorder_admin_menu_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 09/27/2016 Jared Watkins  Update TBBS Admin menu item ordering
||============================================================================
*/
update ppbase_menu_items set item_order = item_order + 10
where module = 'provision' and menu_level = 2 and parent_menu_identifier = 'tbbs_admin';
update ppbase_menu_items set item_order = 1
where module = 'provision' and menu_identifier = 'tbbs_account_setup';
update ppbase_menu_items set item_order = 2
where module = 'provision' and menu_identifier = 'tbbs_line_item_setup';
update ppbase_menu_items set item_order = 3
where module = 'provision' and menu_identifier = 'tbbs_schema_setup';
update ppbase_menu_items set item_order = 4
where module = 'provision' and menu_identifier = 'tbbs_line_item_tree_setup';
update ppbase_menu_items set item_order = 5
where module = 'provision' and menu_identifier = 'tbbs_line_item_assignment';
update ppbase_menu_items set item_order = 6
where module = 'provision' and menu_identifier = 'tbbs_column_close';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3298, 0, 2016, 1, 0, 0, 46429, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046429_prov_reorder_admin_menu_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
