/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045081_budget_module_mypp_shortcuts_3_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2.0 10/14/2015 Chris Mardis   Re-work the MyPowerPlan shortcuts for the Budget Module
 ||============================================================================
 */

--These are just tables used temporarily for processing. they do not need to be preserved
drop table bv_pp_mypp_task_step;

drop table bv_pp_mypp_task;

drop table bv_pp_mypp_task_xlat;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2924, 0, 2015, 2, 0, 0, 45081, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045081_budget_module_mypp_shortcuts_3_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;