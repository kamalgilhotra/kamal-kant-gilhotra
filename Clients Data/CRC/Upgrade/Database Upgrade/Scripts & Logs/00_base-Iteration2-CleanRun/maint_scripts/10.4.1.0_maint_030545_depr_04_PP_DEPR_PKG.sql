/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030545_depr_04_PP_DEPR_PKG.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/09/2013 B. Beck        Point release
||============================================================================
*/

create or replace package PP_DEPR_PKG
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PP_DEPR_PKG
|| Description: Depr functions and procedures for PowerPlant application.
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0     07/18/2011 David Liss   Create
|| 2.0      04/03/2013 Brandon Beck   Adding Depreciation Calculation Functions
||============================================================================
*/
 as
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS         PKG_PP_COMMON.DATE_TABTYPE,
                                 A_LOAD_RECURRING number);

   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH          date,
                                 A_LOAD_RECURRING number);

   procedure P_STAGEMONTHENDDEPR(A_MONTHS PKG_PP_COMMON.DATE_TABTYPE);

   procedure P_STAGEMONTHENDDEPR(A_MONTH date);

   function F_PREPDEPRCALC return number;

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_find_depr_group
   || Description: Finds the depr group under the depr group control lookup.
   || None of the parameters can be null before entering the function.
   || Make sure you check sqlca.sqlcode when calling this function.
   || Return Values: -1 - No data found
   ||             dg_id - Valid depr group ID
   ||                -9 - Invalid depr group
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     07/18/2011 David Liss   Create
   || 2.0      04/03/2013 Brandon Beck   Adding Depreciation Calculation Functions
   ||============================================================================
   */
   function F_FIND_DEPR_GROUP(A_COMPANY_ID         number,
                              A_GL_ACCOUNT_ID      number,
                              A_MAJOR_LOCATION_ID  number,
                              A_UTILITY_ACCOUNT_ID number,
                              A_BUS_SEGMENT_ID     number,
                              A_SUB_ACCOUNT_ID     number,
                              A_SUBLEDGER_TYPE_ID  number,
                              A_BOOK_VINTAGE       number,
                              A_ASSET_LOCATION_ID  number,
                              A_LOCATION_TYPE_ID   number,
                              A_RETIREMENT_UNIT_ID number,
                              A_PROPERTY_UNIT_ID   number,
                              A_CLASS_CODE_ID      number,
                              A_CC_VALUE           varchar2)

    return number;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_get_dg_cc_id
   || Description: Finds the depr group class code.
   || Make sure you check sqlca.sqlcode when calling this function.
   || Return Values: 0 - No data found
   ||                dg_cc_id - DEPR GROUP CONTROL class code id
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     08/12/2011 David Liss     Create
   ||============================================================================
   */
   function F_GET_DG_CC_ID return number;

   --Check to validate depr group status
   procedure P_DG_STATUS_CHECK(A_ENABLE boolean);

   --Check to validate depr group business segment
   procedure P_CHECK_DG_BS;

   G_DG_STATUS_CHECK boolean := true;
   G_CHECK_DG_BS     varchar2(10);

end PP_DEPR_PKG;
/

create or replace package body PP_DEPR_PKG as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --       VARIABLES
   --**************************************************************
   G_IGNOREINACTIVEYES PKG_PP_COMMON.NUM_TABTYPE;
   G_IGNOREINACTIVENO  PKG_PP_COMMON.NUM_TABTYPE;

   --**************************************************************
   --       PROCEDURES
   --**************************************************************

   --**************************************************************************
   --                            P_SET_FISCALYEARSTART
   --**************************************************************************
   -- Sets the beginning of the fiscal year
   procedure P_SET_FISCALYEARSTART is

   begin
      update DEPR_CALC_STG
         set FISCALYEARSTART = ADD_MONTHS(TO_DATE(TO_CHAR(ADD_MONTHS(GL_POST_MO_YR, FISCALYEAROFFSET),
                                                           'yyyy') || '01',
                                                   'yyyymm'),
                                           -1 * FISCALYEAROFFSET)
       where DEPR_CALC_STATUS = 1;
   end P_SET_FISCALYEARSTART;

   --**************************************************************************
   --                            P_CHECKINACTP_CORSALVAMORTVEDEPR
   --**************************************************************************
   --
   -- Get COR and SALV amort
   -- PREREQ: Staging table must be filled in
   --
   procedure P_CORSALVAMORT is

   begin
      update DEPR_CALC_STG Z
         set (DNSA_COR_BAL, DNSA_SALV_BAL) =
              (select sum(COST_OF_REMOVAL_BAL - COST_OF_REMOVAL_RESERVE),
                      sum(SALVAGE_BAL - SALVAGE_RESERVE)
                 from DEPR_NET_SALVAGE_AMORT D
                where D.DEPR_GROUP_ID = Z.DEPR_GROUP_ID
                  and D.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                  and D.GL_POST_MO_YR = ADD_MONTHS(Z.GL_POST_MO_YR, -1))
       where COR_TREATMENT + SALVAGE_TREATMENT > 0
         and DEPR_CALC_STATUS = 1;

      update DEPR_CALC_STG Z
         set (COR_YTD, SALV_YTD) =
              (select sum(L.COST_OF_REMOVAL), sum(L.SALVAGE_CASH - L.SALVAGE_RETURNS)
                 from DEPR_LEDGER L
                where L.DEPR_GROUP_ID = Z.DEPR_GROUP_ID
                  and L.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
                  and L.GL_POST_MO_YR between Z.FISCALYEARSTART and ADD_MONTHS(Z.GL_POST_MO_YR, -1))
       where COR_TREATMENT + SALVAGE_TREATMENT > 0
         and DEPR_CALC_STATUS = 1;
   end P_CORSALVAMORT;

   --**************************************************************************
   --                            P_CHECKINACTIVEDEPR
   --**************************************************************************
   --
   -- Set the companies that are ignoring inactive status
   --
   procedure P_CHECKINACTIVEDEPR(A_COMPANY_ID number,
                                 A_INDEX      number) is

   begin
      if LOWER(trim(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',
                                                                          A_COMPANY_ID),
                        'no'))) = 'no' then
         G_IGNOREINACTIVENO(A_INDEX) := A_COMPANY_ID;
      else
         G_IGNOREINACTIVEYES(A_INDEX) := A_COMPANY_ID;
      end if;
   end P_CHECKINACTIVEDEPR;

   --**************************************************************************
   --                            P_MISSINGSETSOFBOOKS
   --**************************************************************************
   --
   -- Checks for missing sets of books.
   -- Set the depr calc status to be -1 if a set of books is missing for the company
   -- PREREQ: Staging Table must be filled in
   --
   procedure P_MISSINGSETSOFBOOKS is

   begin
      update DEPR_CALC_STG Z
         set DEPR_CALC_STATUS = -1, DEPR_CALC_MESSAGE = 'Missing Sets of Books in Depr Ledger'
       where COMPANY_ID in (select YY.COMPANY_ID
                              from (select CS.COMPANY_ID, CS.SET_OF_BOOKS_ID
                                      from COMPANY_SET_OF_BOOKS CS
                                    minus
                                    select ZZ.COMPANY_ID, ZZ.SET_OF_BOOKS_ID
                                      from DEPR_CALC_STG ZZ) YY);
   end P_MISSINGSETSOFBOOKS;

   --**************************************************************************
   --                            P_STARTMONTHENDDEPR
   --**************************************************************************
   --
   -- WRAPPER for the start depr calc to allow single month depr calculations
   --
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTH          date,
                                 A_LOAD_RECURRING number) is

      MY_MONTHS PKG_PP_COMMON.DATE_TABTYPE;

   begin
      MY_MONTHS(1) := A_MONTH;
      P_STARTMONTHENDDEPR(A_COMPANY_IDS, MY_MONTHS, A_LOAD_RECURRING);
   end P_STARTMONTHENDDEPR;

   --**************************************************************************
   --                            P_STARTMONTHENDDEPR
   --**************************************************************************
   --
   --  Start a depr calc for an array of companies and an array of months
   -- Start Logging
   -- Load Global Temp Staging table
   -- Calculate Deprecation
   -- Backfill results to depr ledger
   -- End logging
   --
   procedure P_STARTMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                                 A_MONTHS         PKG_PP_COMMON.DATE_TABTYPE,
                                 A_LOAD_RECURRING number) is
      MY_RET number;
      MY_STR varchar2(2000);
   begin
      --
      -- START THE LOGGING and log what companys and months are being processed
      --
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_PP_DEPR_COMMON.F_GETDEPRPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('PROCESSING the following company_ids:');

      --
      -- LOOP over the companies and prep the company for processing
      -- Get company system controls to determine whether to include inactive depr groups
      --
      for I in 1 .. A_COMPANY_IDS.COUNT
      loop
         PKG_PP_LOG.P_WRITE_MESSAGE('   ' || TO_CHAR(A_COMPANY_IDS(I)));
         -- load the arrays for companies ignoring inactive depr groups
         P_CHECKINACTIVEDEPR(A_COMPANY_IDS(I), I);
      end loop;

      if A_LOAD_RECURRING = 1 then
         if PKG_PP_DEPR_ACTIVITY.F_RECURRINGACTIVITY(A_COMPANY_IDS, A_MONTHS, MY_STR) = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(MY_STR);
            return;
         end if;
      end if;

      P_STAGEMONTHENDDEPR(A_MONTHS);

      -- start depreciation calculation
      if F_PREPDEPRCALC() = -1 then
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR During Depreciation Calculation');
      end if;

      --
      --  END LOGGING
      --
      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');
      PKG_PP_LOG.P_END_LOG();
   end P_STARTMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  WRAPPER for the prep function to pass in an array of months.
   --
   procedure P_STAGEMONTHENDDEPR(A_MONTHS PKG_PP_COMMON.DATE_TABTYPE) is

   begin
      for I in A_MONTHS.FIRST .. A_MONTHS.LAST
      loop
         P_STAGEMONTHENDDEPR(A_MONTHS(I));
      end loop;
   end P_STAGEMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation.
   -- THE Load is based on depr ledger for a passed in array of company ids.
   -- AND a single month
   --
   procedure P_STAGEMONTHENDDEPR(A_MONTH date) is

   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the calculation staging table for month:' ||
                                 TO_CHAR(A_MONTH, 'yyyymm'));

      forall I in indices of G_IGNOREINACTIVENO
         insert into DEPR_CALC_STG
            (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
             DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, FISCALYEAROFFSET, DNSA_COR_BAL,
             DNSA_SALV_BAL, COR_YTD, SALV_YTD,
             COMPANY_ID, SUBLEDGER_TYPE_ID, DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV,
             MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS, TRUE_UP_CPR_DEPR,
             RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE, END_OF_LIFE,
             EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_PCT,
             DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT, SALVAGE_TREATMENT,
             NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE,
             DEPR_LEDGER_STATUS, BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR,
             SALVAGE_BALANCE, RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN,
             RESERVE_BAL_TRAN_OUT, RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS,
             RESERVE_ALLOC_FACTOR, BEGIN_BALANCE, ADDITIONS, RETIREMENTS, TRANSFERS_IN,
             TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE, END_BALANCE, DEPRECIATION_RATE,
             DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST, DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL,
             RESERVE_RETIREMENTS, SALVAGE_RETURNS, SALVAGE_CASH, RESERVE_CREDITS,
             RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT, GAIN_LOSS,
             VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE, CURRENT_NET_SALVAGE_AMORT,
             CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG, IMPAIRMENT_RESERVE_ACT,
             IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION, COR_BEG_RESERVE, COR_EXPENSE,
             COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN, COR_RES_TRAN_OUT, COR_RES_ADJUST,
             COR_END_RESERVE, COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL,
             RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS, RWIP_RESERVE_CREDITS, SALVAGE_RATE,
             SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST,
             RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT, RESERVE_BLENDING_TRANSFER,
             COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER, IMPAIRMENT_ASSET_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT)
            select DL.DEPR_GROUP_ID,
                   DL.SET_OF_BOOKS_ID,
                   DL.GL_POST_MO_YR,
                   DL.GL_POST_MO_YR,
                   1,
                   '',
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                               DG.COMPANY_ID),
                             'no')),
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                                               DG.COMPANY_ID),
                             'no')),
                   TO_NUMBER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('FISCAL YEAR OFFSET',
                                                                                   DG.COMPANY_ID),
                                 '0')),
                   0,
                   0,
                   0,
                   0,
                   DG.COMPANY_ID,
                   DG.SUBLEDGER_TYPE_ID,
                   DG.DESCRIPTION,
                   DG.DEPR_METHOD_ID,
                   DG.MID_PERIOD_CONV,
                   DG.MID_PERIOD_METHOD,
                   DG.EST_ANN_NET_ADDS,
                   DG.TRUE_UP_CPR_DEPR,
                   DMR.RATE,
                   DMR.NET_GROSS,
                   DMR.OVER_DEPR_CHECK,
                   DMR.NET_SALVAGE_PCT,
                   DMR.RATE_USED_CODE,
                   DMR.END_OF_LIFE,
                   DMR.EXPECTED_AVERAGE_LIFE,
                   DMR.RESERVE_RATIO_ID,
                   DMR.COST_OF_REMOVAL_RATE,
                   DMR.COST_OF_REMOVAL_PCT,
                   DMR.SALVAGE_RATE,
                   DMR.INTEREST_RATE,
                   DMR.AMORTIZABLE_LIFE,
                   DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1),
                   DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1),
                   DMR.NET_SALVAGE_AMORT_LIFE,
                   DMR.ALLOCATION_PROCEDURE,
                   DL.DEPR_LEDGER_STATUS,
                   DL.BEGIN_RESERVE,
                   DL.END_RESERVE,
                   DL.RESERVE_BAL_PROVISION,
                   DL.RESERVE_BAL_COR,
                   DL.SALVAGE_BALANCE,
                   DL.RESERVE_BAL_ADJUST,
                   DL.RESERVE_BAL_RETIREMENTS,
                   DL.RESERVE_BAL_TRAN_IN,
                   DL.RESERVE_BAL_TRAN_OUT,
                   DL.RESERVE_BAL_OTHER_CREDITS,
                   DL.RESERVE_BAL_GAIN_LOSS,
                   DL.RESERVE_ALLOC_FACTOR,
                   DL.BEGIN_BALANCE,
                   DL.ADDITIONS,
                   DL.RETIREMENTS,
                   DL.TRANSFERS_IN,
                   DL.TRANSFERS_OUT,
                   DL.ADJUSTMENTS,
                   DL.DEPRECIATION_BASE,
                   DL.END_BALANCE,
                   DL.DEPRECIATION_RATE,
                   DL.DEPRECIATION_EXPENSE,
                   DL.DEPR_EXP_ADJUST,
                   DL.DEPR_EXP_ALLOC_ADJUST,
                   DL.COST_OF_REMOVAL,
                   DL.RESERVE_RETIREMENTS,
                   DL.SALVAGE_RETURNS,
                   DL.SALVAGE_CASH,
                   DL.RESERVE_CREDITS,
                   DL.RESERVE_ADJUSTMENTS,
                   DL.RESERVE_TRAN_IN,
                   DL.RESERVE_TRAN_OUT,
                   DL.GAIN_LOSS,
                   DL.VINTAGE_NET_SALVAGE_AMORT,
                   DL.VINTAGE_NET_SALVAGE_RESERVE,
                   DL.CURRENT_NET_SALVAGE_AMORT,
                   DL.CURRENT_NET_SALVAGE_RESERVE,
                   DL.IMPAIRMENT_RESERVE_BEG,
                   DL.IMPAIRMENT_RESERVE_ACT,
                   DL.IMPAIRMENT_RESERVE_END,
                   DL.EST_ANN_NET_ADDS,
                   DL.RWIP_ALLOCATION,
                   DL.COR_BEG_RESERVE,
                   DL.COR_EXPENSE,
                   DL.COR_EXP_ADJUST,
                   DL.COR_EXP_ALLOC_ADJUST,
                   DL.COR_RES_TRAN_IN,
                   DL.COR_RES_TRAN_OUT,
                   DL.COR_RES_ADJUST,
                   DL.COR_END_RESERVE,
                   DL.COST_OF_REMOVAL_RATE,
                   DL.COST_OF_REMOVAL_BASE,
                   DL.RWIP_COST_OF_REMOVAL,
                   DL.RWIP_SALVAGE_CASH,
                   DL.RWIP_SALVAGE_RETURNS,
                   DL.RWIP_RESERVE_CREDITS,
                   DL.SALVAGE_RATE,
                   DL.SALVAGE_BASE,
                   DL.SALVAGE_EXPENSE,
                   DL.SALVAGE_EXP_ADJUST,
                   DL.SALVAGE_EXP_ALLOC_ADJUST,
                   DL.RESERVE_BAL_SALVAGE_EXP,
                   DL.RESERVE_BLENDING_ADJUSTMENT,
                   DL.RESERVE_BLENDING_TRANSFER,
                   DL.COR_BLENDING_ADJUSTMENT,
                   DL.COR_BLENDING_TRANSFER,
                   DL.IMPAIRMENT_ASSET_AMOUNT,
                   DL.IMPAIRMENT_EXPENSE_AMOUNT,
                   DL.RESERVE_BAL_IMPAIRMENT
              from DEPR_LEDGER DL,
                   DEPR_GROUP DG,
                   (select DD.*,
                           ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
                      from DEPR_METHOD_RATES DD
                     where DD.EFFECTIVE_DATE <= A_MONTH) DMR
             where DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
               and DMR.THE_ROW = 1
               and DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
               and DMR.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
               and DL.GL_POST_MO_YR = A_MONTH
               and DG.COMPANY_ID = G_IGNOREINACTIVENO(I);

      -- statement to load the groups that are ignoring inactive
      forall I in indices of G_IGNOREINACTIVEYES
         insert into DEPR_CALC_STG
            (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
             DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, FISCALYEAROFFSET, DNSA_COR_BAL,
             DNSA_SALV_BAL, COR_YTD, SALV_YTD,
             COMPANY_ID, SUBLEDGER_TYPE_ID, DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV,
             MID_PERIOD_METHOD, DG_EST_ANN_NET_ADDS, TRUE_UP_CPR_DEPR,
             RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE, END_OF_LIFE,
             EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_PCT,
             DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT, SALVAGE_TREATMENT,
             NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE,
             DEPR_LEDGER_STATUS, BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR,
             SALVAGE_BALANCE, RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN,
             RESERVE_BAL_TRAN_OUT, RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS,
             RESERVE_ALLOC_FACTOR, BEGIN_BALANCE, ADDITIONS, RETIREMENTS, TRANSFERS_IN,
             TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE, END_BALANCE, DEPRECIATION_RATE,
             DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST, DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL,
             RESERVE_RETIREMENTS, SALVAGE_RETURNS, SALVAGE_CASH, RESERVE_CREDITS,
             RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT, GAIN_LOSS,
             VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE, CURRENT_NET_SALVAGE_AMORT,
             CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG, IMPAIRMENT_RESERVE_ACT,
             IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION, COR_BEG_RESERVE, COR_EXPENSE,
             COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN, COR_RES_TRAN_OUT, COR_RES_ADJUST,
             COR_END_RESERVE, COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL,
             RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS, RWIP_RESERVE_CREDITS, SALVAGE_RATE,
             SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST,
             RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT, RESERVE_BLENDING_TRANSFER,
             COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER, IMPAIRMENT_ASSET_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT)
            select DL.DEPR_GROUP_ID,
                   DL.SET_OF_BOOKS_ID,
                   DL.GL_POST_MO_YR,
                   DL.GL_POST_MO_YR,
                   1,
                   '',
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                               DG.COMPANY_ID),
                             'no')),
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                                               DG.COMPANY_ID),
                             'no')),
                   TO_NUMBER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('FISCAL YEAR OFFSET',
                                                                                   DG.COMPANY_ID),
                                 '0')),
                   0,
                   0,
                   0,
                   0,
                   DG.COMPANY_ID,
                   DG.SUBLEDGER_TYPE_ID,
                   DG.DESCRIPTION,
                   DG.DEPR_METHOD_ID,
                   DG.MID_PERIOD_CONV,
                   DG.MID_PERIOD_METHOD,
                   DG.EST_ANN_NET_ADDS,
                   DG.TRUE_UP_CPR_DEPR,
                   DMR.RATE,
                   DMR.NET_GROSS,
                   DMR.OVER_DEPR_CHECK,
                   DMR.NET_SALVAGE_PCT,
                   DMR.RATE_USED_CODE,
                   DMR.END_OF_LIFE,
                   DMR.EXPECTED_AVERAGE_LIFE,
                   DMR.RESERVE_RATIO_ID,
                   DMR.COST_OF_REMOVAL_RATE,
                   DMR.COST_OF_REMOVAL_PCT,
                   DMR.SALVAGE_RATE,
                   DMR.INTEREST_RATE,
                   DMR.AMORTIZABLE_LIFE,
                   DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1),
                   DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1),
                   DMR.NET_SALVAGE_AMORT_LIFE,
                   DMR.ALLOCATION_PROCEDURE,
                   DL.DEPR_LEDGER_STATUS,
                   DL.BEGIN_RESERVE,
                   DL.END_RESERVE,
                   DL.RESERVE_BAL_PROVISION,
                   DL.RESERVE_BAL_COR,
                   DL.SALVAGE_BALANCE,
                   DL.RESERVE_BAL_ADJUST,
                   DL.RESERVE_BAL_RETIREMENTS,
                   DL.RESERVE_BAL_TRAN_IN,
                   DL.RESERVE_BAL_TRAN_OUT,
                   DL.RESERVE_BAL_OTHER_CREDITS,
                   DL.RESERVE_BAL_GAIN_LOSS,
                   DL.RESERVE_ALLOC_FACTOR,
                   DL.BEGIN_BALANCE,
                   DL.ADDITIONS,
                   DL.RETIREMENTS,
                   DL.TRANSFERS_IN,
                   DL.TRANSFERS_OUT,
                   DL.ADJUSTMENTS,
                   DL.DEPRECIATION_BASE,
                   DL.END_BALANCE,
                   DL.DEPRECIATION_RATE,
                   DL.DEPRECIATION_EXPENSE,
                   DL.DEPR_EXP_ADJUST,
                   DL.DEPR_EXP_ALLOC_ADJUST,
                   DL.COST_OF_REMOVAL,
                   DL.RESERVE_RETIREMENTS,
                   DL.SALVAGE_RETURNS,
                   DL.SALVAGE_CASH,
                   DL.RESERVE_CREDITS,
                   DL.RESERVE_ADJUSTMENTS,
                   DL.RESERVE_TRAN_IN,
                   DL.RESERVE_TRAN_OUT,
                   DL.GAIN_LOSS,
                   DL.VINTAGE_NET_SALVAGE_AMORT,
                   DL.VINTAGE_NET_SALVAGE_RESERVE,
                   DL.CURRENT_NET_SALVAGE_AMORT,
                   DL.CURRENT_NET_SALVAGE_RESERVE,
                   DL.IMPAIRMENT_RESERVE_BEG,
                   DL.IMPAIRMENT_RESERVE_ACT,
                   DL.IMPAIRMENT_RESERVE_END,
                   DL.EST_ANN_NET_ADDS,
                   DL.RWIP_ALLOCATION,
                   DL.COR_BEG_RESERVE,
                   DL.COR_EXPENSE,
                   DL.COR_EXP_ADJUST,
                   DL.COR_EXP_ALLOC_ADJUST,
                   DL.COR_RES_TRAN_IN,
                   DL.COR_RES_TRAN_OUT,
                   DL.COR_RES_ADJUST,
                   DL.COR_END_RESERVE,
                   DL.COST_OF_REMOVAL_RATE,
                   DL.COST_OF_REMOVAL_BASE,
                   DL.RWIP_COST_OF_REMOVAL,
                   DL.RWIP_SALVAGE_CASH,
                   DL.RWIP_SALVAGE_RETURNS,
                   DL.RWIP_RESERVE_CREDITS,
                   DL.SALVAGE_RATE,
                   DL.SALVAGE_BASE,
                   DL.SALVAGE_EXPENSE,
                   DL.SALVAGE_EXP_ADJUST,
                   DL.SALVAGE_EXP_ALLOC_ADJUST,
                   DL.RESERVE_BAL_SALVAGE_EXP,
                   DL.RESERVE_BLENDING_ADJUSTMENT,
                   DL.RESERVE_BLENDING_TRANSFER,
                   DL.COR_BLENDING_ADJUSTMENT,
                   DL.COR_BLENDING_TRANSFER,
                   DL.IMPAIRMENT_ASSET_AMOUNT,
                   DL.IMPAIRMENT_EXPENSE_AMOUNT,
                   DL.RESERVE_BAL_IMPAIRMENT
              from DEPR_LEDGER DL,
                   DEPR_GROUP DG,
                   (select DD.*,
                           ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
                      from DEPR_METHOD_RATES DD
                     where DD.EFFECTIVE_DATE <= A_MONTH) DMR
             where DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
               and DMR.THE_ROW = 1
               and DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
               and DMR.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
               and DL.GL_POST_MO_YR = A_MONTH
               and DG.COMPANY_ID = G_IGNOREINACTIVEYES(I)
               and (NVL(DG.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
                   DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
                   DL.COR_BEG_RESERVE <> 0);
   end P_STAGEMONTHENDDEPR;

   --**************************************************************************
   --                            P_DEPRCALC
   --**************************************************************************
   procedure P_DEPRCALC is

   begin
      pkg_pp_log.p_write_message( 'STARTING Depreciation Calculation' );

      --
      -- IF SMD and end of life provided, the net_gross has to be 1
      -- IF mid_period_method is 'End of Life', then end_of_life has to be provided and net_gross has to be 1
      -- IF mid_period_method is Monthly then end_of_life does not get used
      --
      merge into depr_calc_stg a using
      (
         select *
         from depr_calc_stg b
         where b.depr_calc_status = 1
         and b.subledger_type_id = 0
         model
         partition by (depr_group_id, set_of_books_id, gl_post_mo_yr)
         dimension by (mid_period_method)
         measures
         (
            additions, retirements, transfers_in, transfers_out,
            adjustments, impairment_asset_amount,
            salvage_cash, salvage_returns, current_net_salvage_amort,
            reserve_credits, reserve_tran_in, reserve_tran_out, reserve_adjustments,
            impairment_expense_amount,
            cost_of_removal, cor_res_tran_in, cor_res_tran_out, cor_res_adjust,
            mid_period_conv, rate, net_salvage_pct, cost_of_removal_pct,
            rwip_salvage_cash, rwip_salvage_returns, rwip_reserve_credits, rwip_cost_of_removal,
            cor_beg_reserve, begin_balance, begin_reserve,
            cor_treatment, salvage_treatment, cor_ytd, salv_ytd, dnsa_cor_bal, dnsa_salv_bal,
            end_balance, depreciation_rate, depreciation_base, depreciation_expense, calc_month,
            end_of_life, net_gross, trf_in_est_adds, include_rwip_in_net,
            salvage_base, salvage_rate, salvage_expense, dmr_salvage_rate,
            cost_of_removal_base, cost_of_removal_rate, cor_expense, dmr_cost_of_removal_rate,
            0 as plant_activity, 0 as reserve_activity, 0 as rwip_salvage, 0 as cor_activity,
            0 as remaining_months, 0 as rwip_cor
         )
         rules update
         (
            -- prep activity and begin balances based on flags and settings
            -- applies to ALL mid period methods.
            plant_activity[ANY] =
               case when trf_in_est_adds[CV(mid_period_method)] = 'no' then
                  additions[CV(mid_period_method)] + retirements[CV(mid_period_method)]
                  + adjustments[CV(mid_period_method)] + impairment_asset_amount[CV(mid_period_method)]
               else
                  additions[CV(mid_period_method)] + retirements[CV(mid_period_method)]
                  + transfers_in[CV(mid_period_method)] + transfers_out[CV(mid_period_method)]
                  + adjustments[CV(mid_period_method)] + impairment_asset_amount[CV(mid_period_method)]
               end,
            -- reserve activity needs to take into account salvage amort if it has any (multiply salvage by the salvage treatment
            --  So if salvage treatment is yes (1) the cash and returns are included.  If salvage treatment is no (0 they are not).
            --  Since 1 times anything is anything.  And 0 times anything is zero
            reserve_activity[ANY] =
               case when trf_in_est_adds[CV(mid_period_method)] = 'no' then
                  retirements[CV(mid_period_method)] + salvage_cash[CV(mid_period_method)] + salvage_returns[CV(mid_period_method)]
                  + reserve_credits[CV(mid_period_method)] + reserve_adjustments[CV(mid_period_method)] - current_net_salvage_amort[CV(mid_period_method)]
                  + impairment_asset_amount[CV(mid_period_method)]+ impairment_expense_amount[CV(mid_period_method)]
                  - ( salvage_treatment[CV(mid_period_method)] * ( salvage_cash[CV(mid_period_method)] + salvage_returns[CV(mid_period_method)] ) )
               else
                  retirements[CV(mid_period_method)] + salvage_cash[CV(mid_period_method)] + salvage_returns[CV(mid_period_method)]
                  + reserve_credits[CV(mid_period_method)] + reserve_tran_in[CV(mid_period_method)]+ reserve_tran_out[CV(mid_period_method)]
                  + reserve_adjustments[CV(mid_period_method)] - current_net_salvage_amort[CV(mid_period_method)]
                  + impairment_asset_amount[CV(mid_period_method)]+ impairment_expense_amount[CV(mid_period_method)]
                  - ( salvage_treatment[CV(mid_period_method)] * ( salvage_cash[CV(mid_period_method)] + salvage_returns[CV(mid_period_method)] ) )
               end,
            cor_activity[ANY] =
               case when trf_in_est_adds[CV(mid_period_method)] = 'no' then
                  cost_of_removal[CV(mid_period_method)] + cor_res_adjust[CV(mid_period_method)]
               else
                  cost_of_removal[CV(mid_period_method)] + cor_res_tran_in[CV(mid_period_method)]
                  + cor_res_tran_out[CV(mid_period_method)] + cor_res_adjust[CV(mid_period_method)]
               end,
            -- begin reserver needs to take into account cor and salvage treatment.
            begin_reserve[ANY] =
               case when trf_in_est_adds[CV(mid_period_method)] = 'no' then
                  begin_reserve[CV(mid_period_method)] + reserve_tran_in[CV(mid_period_method)] + reserve_tran_out[CV(mid_period_method)]
                  + ( cor_treatment[CV(mid_period_method)] * ( cor_beg_reserve[CV(mid_period_method)] - dnsa_cor_bal[CV(mid_period_method)] - cor_ytd[CV(mid_period_method)] ) )
                  - ( salvage_treatment[CV(mid_period_method)] * ( dnsa_salv_bal[CV(mid_period_method)] + salv_ytd[CV(mid_period_method)] ) )
               else
                  begin_reserve[CV(mid_period_method)]
                  + ( cor_treatment[CV(mid_period_method)] * ( cor_beg_reserve[CV(mid_period_method)] - dnsa_cor_bal[CV(mid_period_method)] - cor_ytd[CV(mid_period_method)] ) )
                  - ( salvage_treatment[CV(mid_period_method)] * ( dnsa_salv_bal[CV(mid_period_method)] + salv_ytd[CV(mid_period_method)] ) )
               end,
            begin_balance[ANY] =
               case when trf_in_est_adds[CV(mid_period_method)] = 'no' then
                  begin_balance[CV(mid_period_method)] + transfers_in[CV(mid_period_method)] + transfers_out[CV(mid_period_method)]
               else
                  begin_balance[CV(mid_period_method)]
               end,

            -- if including RWIP and net_gross is 1
            rwip_salvage[ANY] =
               case when include_rwip_in_net [CV(mid_period_method)] = 'yes'
               and net_gross[CV(mid_period_method)] = 1 then
                  rwip_salvage_cash[CV(mid_period_method)] + rwip_salvage_returns[CV(mid_period_method)] + rwip_reserve_credits[CV(mid_period_method)]
               else
                  0
               end,
            rwip_cor[ANY] =
               case when include_rwip_in_net [CV(mid_period_method)] = 'yes'
               and net_gross[CV(mid_period_method)] = 1 then
                  rwip_cost_of_removal[CV(mid_period_method)]
               else
                  0
               end,

            -- remaining months if end of life provided (min value of 1)
            -- ELSE use a 0
            remaining_months[ANY] =
               case when end_of_life[CV(mid_period_method)] IS NOT NULL then
                  greatest(1, 1 + months_between(to_date(to_char( end_of_life[CV(mid_period_method)] ), 'yyyymm'), calc_month[CV(mid_period_method)] ) )
               else
                  0
               end,

            -- GET The depreciation rates for MONTHLY.
            depreciation_rate['Monthly'] = rate['Monthly'],
            salvage_rate['Monthly'] = dmr_salvage_rate['Monthly'],
            cost_of_removal_rate['Monthly'] = dmr_cost_of_removal_rate['Monthly'],

            -- GET The depreciation rates for END OF LIFE.  Salv rate and cor rate are same
            depreciation_rate['End of Life'] = 12 / greatest(1, remaining_months['End of Life']),
            salvage_rate['End of Life'] = depreciation_rate['End of Life'],
            cost_of_removal_rate['End of Life'] = depreciation_rate['End of Life'],

            -- GET the depreciation rates for SMD
            depreciation_rate['SMD'] =
               case when end_of_life[CV(mid_period_method)] IS NULL then
                  rate[CV(mid_period_method)]
               else
                  12 * ( remaining_months['SMD'] / ( ( remaining_months['SMD'] * ( remaining_months['SMD'] + 1 ) ) / 2 ) )
               end,
            salvage_rate['SMD'] =
               case when end_of_life[CV(mid_period_method)] IS NULL then
                  dmr_salvage_rate[CV(mid_period_method)]
               else -- same as depreciation rate
                  depreciation_rate['SMD']
               end,
            cost_of_removal_rate['SMD'] =
               case when end_of_life[CV(mid_period_method)] IS NULL then
                  dmr_cost_of_removal_rate[CV(mid_period_method)]
               else -- same as depreciation rate
                  depreciation_rate['SMD']
               end,

            -- take into account net salvage now for end of life provided
            depreciation_rate[mid_period_method in ('End of Life', 'SMD')] =
               case when end_of_life[CV(mid_period_method)] IS NOT NULL and net_salvage_pct[CV(mid_period_method)] <> 1 then
                  depreciation_rate[CV(mid_period_method)] / ( 1 - net_salvage_pct[CV(mid_period_method)] )
               else
                  depreciation_rate[CV(mid_period_method)]
               end,
            salvage_rate[mid_period_method in ('End of Life', 'SMD')] =
               case when end_of_life[CV(mid_period_method)] IS NOT NULL then
                  case when net_salvage_pct[CV(mid_period_method)] <> 1 then
                     salvage_rate[CV(mid_period_method)] * net_salvage_pct[CV(mid_period_method)] * -1 / ( 1 - net_salvage_pct[CV(mid_period_method)] )
                  else
                     0
                  end
               else
                  salvage_rate[CV(mid_period_method)]
               end,


            depreciation_base[mid_period_method in ('Monthly', 'End of Life', 'SMD')] =
               case when net_gross[CV(mid_period_method)] = 1 then
                  ( begin_balance[CV(mid_period_method)] *
                     (1 - net_salvage_pct[CV(mid_period_method)]) - begin_reserve[CV(mid_period_method)] - rwip_salvage[CV(mid_period_method)]
                  ) + ( mid_period_conv[CV(mid_period_method)] * ( plant_activity[CV(mid_period_method)] *
                                             (1 - net_salvage_pct[CV(mid_period_method)] ) - reserve_activity[CV(mid_period_method)] )
                  )
               else
                  begin_balance[CV(mid_period_method)] +(   plant_activity[CV(mid_period_method)] * mid_period_conv[CV(mid_period_method)] )
               end,
            -- salvage base is the same as depreciation base
            salvage_base[mid_period_method in ('Monthly', 'End of Life', 'SMD')] = depreciation_base[CV(mid_period_method)],
            cost_of_removal_base[mid_period_method in ('Monthly', 'End of Life', 'SMD')] =
               case when net_gross[CV(mid_period_method)] = 1 then
                  ( begin_balance[CV(mid_period_method)] * cost_of_removal_pct[CV(mid_period_method)]
                     - cor_beg_reserve[CV(mid_period_method)] - rwip_cor[CV(mid_period_method)]
                  ) + ( mid_period_conv[CV(mid_period_method)]
                     * ( plant_activity[CV(mid_period_method)] * cost_of_removal_pct[CV(mid_period_method)] - cor_activity[CV(mid_period_method)] )
                  )
               else
                  begin_balance[CV(mid_period_method)] + (plant_activity[CV(mid_period_method)] * mid_period_conv[CV(mid_period_method)] )
               end,

            -- ALL Mid Period Methods
            end_balance[ANY] = begin_balance[CV(mid_period_method)] + plant_activity[CV(mid_period_method)],
            depreciation_expense[ANY] = depreciation_base[CV(mid_period_method)] * depreciation_rate[CV(mid_period_method)] / 12,
            salvage_expense[ANY] = salvage_base[CV(mid_period_method)] * salvage_rate[CV(mid_period_method)] / 12,
            cor_expense[ANY] = cost_of_removal_base[CV(mid_period_method)] * cost_of_removal_rate[CV(mid_period_method)] / 12
         )
      ) b
      on
      (
         a.depr_group_id = b.depr_group_id
         and a.set_of_books_id = b.set_of_books_id
         and a.gl_post_mo_yr = b.gl_post_mo_yr
      )
      when matched then update
      set
         end_balance = b.end_balance, depreciation_base = b.depreciation_base, depreciation_rate = b.depreciation_rate, depreciation_expense = b.depreciation_expense,
         salvage_base = b.salvage_base, salvage_rate = b.salvage_rate, salvage_expense = b.salvage_expense,
         cost_of_removal_base = b.cost_of_removal_base, cost_of_removal_rate = b.cost_of_removal_rate, cor_expense = b.cor_expense
      ;
      --'depr_exp_alloc_adjust', depr_exp_adj + retro_depr_exp_ad
      --'salvage_exp_alloc_adjust', salv_exp_adj + retro_salv_exp_adj
      --'cor_end_reserve', (begin_cor_month + cor_act + cor_prov + retro_cor_exp_adj + cor_exp_adj + cor_exp_adjust_in)
      --'cor_exp_alloc_adjust', retro_cor_exp_adj + cor_exp_adj
   end P_DEPRCALC;

   --**************************************************************************
   --                            F_PREPDEPRCALC
   --**************************************************************************
   --
   -- This function performs the depreciation calculation
   -- PREREQS: The staging table depr_calc_stg is already populated by some other means
   -- ALLOWING: this function to calculate depreciation independently of type of calc or what process initiated
   --

   function F_PREPDEPRCALC return number is

   begin
      P_MISSINGSETSOFBOOKS();
      P_SET_FISCALYEARSTART();
      P_CORSALVAMORT();
      P_DEPRCALC();
      return 1;

   exception when others then
         /* this catches all SQL errors, including no_data_found */
         return - 1;
   end F_PREPDEPRCALC;


   --**************************************************************************
   --                            F_FIND_DEPR_GROUP
   --**************************************************************************
   function F_FIND_DEPR_GROUP(A_COMPANY_ID         number,
                              A_GL_ACCOUNT_ID      number,
                              A_MAJOR_LOCATION_ID  number,
                              A_UTILITY_ACCOUNT_ID number,
                              A_BUS_SEGMENT_ID     number,
                              A_SUB_ACCOUNT_ID     number,
                              A_SUBLEDGER_TYPE_ID  number,
                              A_BOOK_VINTAGE       number,
                              A_ASSET_LOCATION_ID  number,
                              A_LOCATION_TYPE_ID   number,
                              A_RETIREMENT_UNIT_ID number,
                              A_PROPERTY_UNIT_ID   number,
                              A_CLASS_CODE_ID      number,
                              A_CC_VALUE           varchar2) return number is
      DG_ID  number;
      STATUS number;
      CC_VAL varchar2(35);

   begin

      if A_CC_VALUE is null then
         CC_VAL := 'NO CLASS CODE';
      else
         CC_VAL := A_CC_VALUE;
      end if;

      select DEPR_GROUP_ID, STATUS_ID
        into DG_ID, STATUS
        from (select DEPR_GROUP_CONTROL.DEPR_GROUP_ID DEPR_GROUP_ID, NVL(STATUS_ID, 1) STATUS_ID
                from DEPR_GROUP_CONTROL, DEPR_GROUP
               where DEPR_GROUP.COMPANY_ID = A_COMPANY_ID
                 and DEPR_GROUP_CONTROL.BUS_SEGMENT_ID = A_BUS_SEGMENT_ID
                 and DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID = A_UTILITY_ACCOUNT_ID
                 and NVL(DEPR_GROUP_CONTROL.GL_ACCOUNT_ID, A_GL_ACCOUNT_ID) = A_GL_ACCOUNT_ID
                 and NVL(DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID, A_MAJOR_LOCATION_ID) =
                     A_MAJOR_LOCATION_ID
                 and NVL(DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID, A_SUB_ACCOUNT_ID) = A_SUB_ACCOUNT_ID
                 and NVL(TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY'), A_BOOK_VINTAGE) =
                     A_BOOK_VINTAGE
                 and NVL(DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID, A_SUBLEDGER_TYPE_ID) =
                     A_SUBLEDGER_TYPE_ID
                 and NVL(DEPR_GROUP_CONTROL.ASSET_LOCATION_ID, A_ASSET_LOCATION_ID) =
                     A_ASSET_LOCATION_ID
                 and NVL(DEPR_GROUP_CONTROL.LOCATION_TYPE_ID, A_LOCATION_TYPE_ID) =
                     A_LOCATION_TYPE_ID
                 and NVL(DEPR_GROUP_CONTROL.PROPERTY_UNIT_ID, A_PROPERTY_UNIT_ID) =
                     A_PROPERTY_UNIT_ID
                 and NVL(DEPR_GROUP_CONTROL.RETIREMENT_UNIT_ID, A_RETIREMENT_UNIT_ID) =
                     A_RETIREMENT_UNIT_ID
                 and NVL(DEPR_GROUP_CONTROL.CLASS_CODE_ID, A_CLASS_CODE_ID) = A_CLASS_CODE_ID
                 and NVL(DEPR_GROUP_CONTROL.CC_VALUE, CC_VAL) = CC_VAL
                 and DEPR_GROUP_CONTROL.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID
                 and DECODE(G_CHECK_DG_BS,
                            'YES',
                            DEPR_GROUP_CONTROL.BUS_SEGMENT_ID,
                            'NO_BUS_SEG_CHECK') =
                     DECODE(G_CHECK_DG_BS, 'YES', DEPR_GROUP.BUS_SEGMENT_ID, 'NO_BUS_SEG_CHECK')
               order by DEPR_GROUP.COMPANY_ID,
                        DEPR_GROUP_CONTROL.BUS_SEGMENT_ID,
                        DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.GL_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID,
                        DEPR_GROUP_CONTROL.ASSET_LOCATION_ID,
                        DEPR_GROUP_CONTROL.LOCATION_TYPE_ID,
                        DEPR_GROUP_CONTROL.PROPERTY_UNIT_ID,
                        DEPR_GROUP_CONTROL.RETIREMENT_UNIT_ID,
                        TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY'),
                        DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID,
                        DEPR_GROUP_CONTROL.CLASS_CODE_ID,
                        DEPR_GROUP_CONTROL.CC_VALUE)
       where ROWNUM = 1;

      if STATUS = 1 then
         return DG_ID;
      else
         /* invalid depr group */
         if G_DG_STATUS_CHECK then
            return - 9;
         else
            return DG_ID;
         end if;
      end if;
   exception when others then
         /* this catches all SQL errors, including no_data_found */
         return -1;
   end;

   --**************************************************************************
   --                            F_GET_DG_CC_ID
   --**************************************************************************
   function F_GET_DG_CC_ID return number is
      DG_CC_DESC varchar2(35);
      DG_CC_ID   number;

   begin

      select CONTROL_VALUE
        into DG_CC_DESC
        from PP_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = 'DEPR GROUP CLASS CODE';

      select CLASS_CODE_ID
        into DG_CC_ID
        from CLASS_CODE
       where UPPER(trim(DESCRIPTION)) = UPPER(trim(DG_CC_DESC));

      return DG_CC_ID;

   exception when others then
         /* this catches all SQL errors, including no_data_found */
         return 0;
   end;

   --**************************************************************************
   --                            P_DG_STATUS_CHECK
   --**************************************************************************
   procedure P_DG_STATUS_CHECK(A_ENABLE boolean) is

   begin
      G_DG_STATUS_CHECK := A_ENABLE;
   end P_DG_STATUS_CHECK;

   --**************************************************************************
   --                            P_CHECK_DG_BS
   --**************************************************************************
   procedure P_CHECK_DG_BS is
      PP_CONTROL_VALUE varchar2(35);

   begin
      select UPPER(trim(CONTROL_VALUE))
        into PP_CONTROL_VALUE
        from PP_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = 'POST VALIDATE DEPR BUSINESS SEGMENT';

      if PP_CONTROL_VALUE = 'YES' then
         G_CHECK_DG_BS := 'YES';
      else
         G_CHECK_DG_BS := 'NO';
      end if;
   end P_CHECK_DG_BS;

--**************************************************************************************
-- Initialization procedure (Called the first time this package is opened in a session)
-- Initialize the VALIDATE DEPR BUSINESS SEGMENT variable
--**************************************************************************************
begin
   P_CHECK_DG_BS;

end PP_DEPR_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (452, 0, 10, 4, 1, 0, 30545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030545_depr_04_PP_DEPR_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
