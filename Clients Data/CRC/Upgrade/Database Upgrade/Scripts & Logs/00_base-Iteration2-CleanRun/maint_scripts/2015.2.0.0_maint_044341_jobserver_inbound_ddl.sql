/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_044341_jobserver_inbound_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2     07/22/2015 Chad Theilman    	 Creating new table 'PP_JOB_INBOUND_DATA'
||==========================================================================================
*/

SET serveroutput ON size 30000;

declare
	doesTableExist number := 0;
begin
	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_JOB_INBOUND_DATA','PWRPLANT');

	if doesTableExist = 0 then
        begin
			dbms_output.put_line('Creating table PP_JOB_INBOUND_DATA');

			EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_JOB_INBOUND_DATA"
								(
									"JOB_INPUT_DATA_ID" number(16,0) not null,
									"TIME_STAMP" date,
									"JOB_WORKFLOW_ID" number(16,0),
									"CREATED" date not null,
									"DATA" CLOB,
									CONSTRAINT PP_JOB_INBOUND_DATA PRIMARY KEY ("JOB_INPUT_DATA_ID"),
									CONSTRAINT FK_PP_JOB_INBOUND_DATA FOREIGN KEY ("JOB_WORKFLOW_ID") REFERENCES "PP_JOB_WORKFLOW" ("JOB_WORKFLOW_ID")
								) TABLESPACE PWRPLANT';

			EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_INBOUND_DATA" IS ''Temporary storage of data associated with Inbound Requests.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_INBOUND_DATA"."JOB_INPUT_DATA_ID" is ''Primary record identifier''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_INBOUND_DATA"."TIME_STAMP" is ''Timestamp of record''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_INBOUND_DATA"."JOB_WORKFLOW_ID" is ''The job flow that will be started when a new file is found''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_INBOUND_DATA"."CREATED" is ''The user that created this File Watcher''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_INBOUND_DATA"."DATA" is ''Integration Data that will be processed by a JobFlow Job triggered by an Inbound Request''';

			EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_INBOUND_DATA FOR PWRPLANT.PP_JOB_INBOUND_DATA';

			EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_INBOUND_DATA TO pwrplant_role_dev';

			EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_INBOUND_DATA_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';

			EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_INBOUND_DATA
								BEFORE INSERT ON PP_JOB_INBOUND_DATA
								FOR EACH ROW
								BEGIN
									:new.JOB_INPUT_DATA_ID := PP_JOB_INBOUND_DATA_SEQ.nextval;
								END;';

		end;
	end if;
end;

/

update PP_JOB_WORKFLOW set key = REGEXP_REPLACE(key,'[^a-zA-Z_]','_') 
where Key is not null;

/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2715, 0, 2015, 2, 0, 0, 044341, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044341_jobserver_inbound_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;