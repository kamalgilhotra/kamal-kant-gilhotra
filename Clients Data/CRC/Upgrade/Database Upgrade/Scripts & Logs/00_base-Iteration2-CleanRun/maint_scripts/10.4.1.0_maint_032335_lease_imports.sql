/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032335_lease_imports.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/09/2013 Ryan Oliveria  Point Release
||============================================================================
*/

alter table LS_IMPORT_INVOICE add INVOICE_NUMBER varchar2(35);
comment on column LS_IMPORT_INVOICE.INVOICE_NUMBER is 'User-defined number associated with an Invoice. This is usually a reference to an external numbering system.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE)
   select IMPORT_TYPE_ID,
          'invoice_number',
          'Invoice Number',
          null,
          0,
          1,
          'varchar2(35)',
          null,
          null,
          1
     from PP_IMPORT_TYPE type
    where TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice';

update PP_IMPORT_TEMPLATE_FIELDS
   set FIELD_ID = FIELD_ID + 1
 where IMPORT_TEMPLATE_ID =
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where DESCRIPTION = 'Lease Invoice Add');

insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select IMPORT_TEMPLATE_ID, 1, PP_IMPORT_TYPE.IMPORT_TYPE_ID, 'invoice_number', null
     from PP_IMPORT_TEMPLATE, PP_IMPORT_TYPE
    where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = PP_IMPORT_TYPE.IMPORT_TYPE_ID
      and PP_IMPORT_TYPE.IMPORT_TABLE_NAME = 'ls_import_invoice'
      and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Lease Invoice Add';

alter table LS_IMPORT_INVOICE_ARCHIVE add INVOICE_NUMBER varchar2(35);
comment on column LS_IMPORT_INVOICE_ARCHIVE.INVOICE_NUMBER is 'User-defined number associated with an Invoice. This is usually a reference to an external numbering system.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (607, 0, 10, 4, 1, 0, 32335, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032335_lease_imports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;