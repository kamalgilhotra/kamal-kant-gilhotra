/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029902_lease_LS_TEST_ILR_SCHEDULE.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   05/01/2013 Brandon Beck   Point Release
||============================================================================
*/

create or replace package LS_TEST_ILR_SCHEDULE as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LS_TEST_ILR_SCHEDULE
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.0 05/01/2013
   ||============================================================================
   */

   procedure SET_ILR_ID(A_ILR_ID number);

   function GET_ILR_ID return number;

   function VALIDATE_ILR return number;

   function ILR_INTEGRITY return number;

   function ILR_ASSET_INTEGRITY return number;

   function ILR_TOTALS return number;

   function ILR_ROLL return number;

   function ASSET_ROLL return number;
end LS_TEST_ILR_SCHEDULE;
/

create or replace package body LS_TEST_ILR_SCHEDULE as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: ARCHIVE_TAX
   || Description:
   ||============================================================================
   || Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.0 04/26/2013
   ||============================================================================
   */

   L_ILR_ID number;

   --**************************************************************************
   --                            Start Body
   --**************************************************************************

   --**************************************************************************
   --                            SET_ILR_ID
   --**************************************************************************

   procedure SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end SET_ILR_ID;

   --**************************************************************************
   --                            GET_ILR_ID
   --**************************************************************************

   function GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end GET_ILR_ID;

   --**************************************************************************
   --                            VALIDATE_ILR
   --**************************************************************************

   function VALIDATE_ILR return number is
      L_COUNT number;

   begin
      select count(*) into L_COUNT from LS_ILR_SCHEDULE where ILR_ID = L_ILR_ID;
      if L_COUNT > 0 then
         return 1;
      else
         return 0;
      end if;
   end VALIDATE_ILR;

   --**************************************************************************
   --                            ILR_INTEGRITY
   --**************************************************************************

   function ILR_INTEGRITY return number is
      L_DIFF number;

   begin
      select sum(ABS(INT_AMT_TO_ACC)) + sum(ABS(PRIN_AMT_TO_ACC)) + sum(ABS(INT_PRIN_ACC_TO_PAID)) +
             sum(ABS(INT_PRIN_AMT_TO_PAID)) + sum(ABS(EXEC_AMT_ACC)) + sum(ABS(CONT_AMT_ACC)) +
             sum(ABS(EXEC_AMT_PAID)) + sum(ABS(CONT_AMT_PAID)) as TEST
        into L_DIFF
        from (select (case
                        when RESIDUAL > 0 and INT_AMT <> 0 and PRE_PAYMENT_SW = 1 then
                         INT_AMT - RESIDUAL + PRIN_ACC -
                         NVL(LAG(INT_ACC, NVL(B.PRE_PAYMENT_SW, 0)) OVER(order by PERIOD), 0)
                        else
                         INT_AMT -
                         NVL(LAG(INT_ACC, NVL(B.PRE_PAYMENT_SW, 0)) OVER(order by PERIOD), 0)
                     end) as INT_AMT_TO_ACC,
                     (case
                        when RESIDUAL > 0 and INT_AMT <> 0 and PRE_PAYMENT_SW = 1 then
                         PRIN_AMT - RESIDUAL + INT_ACC -
                         NVL(LAG(PRIN_ACC, NVL(B.PRE_PAYMENT_SW, 0)) OVER(order by PERIOD), 0)
                        when PRIN_AMT = PAID_AMT and INT_AMT = 0 and PRE_PAYMENT_SW = 1 then
                         PRIN_AMT - PAID_AMT
                        else
                         PRIN_AMT -
                         NVL(LAG(PRIN_ACC, NVL(B.PRE_PAYMENT_SW, 0)) OVER(order by PERIOD), 0)
                     end) as PRIN_AMT_TO_ACC,
                     NVL(PAID_AMT - LAG(INT_ACC, NVL(B.PRE_PAYMENT_SW, 0))
                         OVER(order by PERIOD) - LAG(PRIN_ACC, NVL(B.PRE_PAYMENT_SW, 0))
                         OVER(order by PERIOD) +
                         DECODE(B.PRE_PAYMENT_SW, 0, DECODE(INT_AMT, 0, 0, RESIDUAL), 0),
                         0) as INT_PRIN_ACC_TO_PAID,
                     PAID_AMT - PRIN_AMT - INT_AMT + DECODE(INT_AMT, 0, 0, RESIDUAL) as INT_PRIN_AMT_TO_PAID,
                     EXEC_ACC - EXEC_AMT as EXEC_AMT_ACC,
                     CONT_ACC - CONT_AMT as CONT_AMT_ACC,
                     EXEC_AMT - EST_EXEC as EXEC_AMT_PAID,
                     CONT_AMT - EST_CONT as CONT_AMT_PAID
                from (select FLOOR((B.ROW_NUM - 1) / DECODE(C.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) as PERIOD,
                             sum(B.PRINCIPAL_AMOUNT) PRIN_AMT,
                             sum(B.PRINCIPAL_ACCRUAL) PRIN_ACC,
                             sum(B.INTEREST_AMOUNT) INT_AMT,
                             sum(B.INTEREST_ACCRUAL) INT_ACC,
                             sum(B.EXECUTORY_AMOUNT) EXEC_AMT,
                             sum(B.EXECUTORY_ACCRUAL) EXEC_ACC,
                             sum(B.CONTINGENT_AMOUNT) CONT_AMT,
                             sum(B.CONTINGENT_ACCRUAL) CONT_ACC,
                             max(NVL(C.PAID_AMOUNT, 0)) PAID_AMT,
                             max(NVL(C.EST_EXECUTORY_COST, 0)) EST_EXEC,
                             max(NVL(C.CONTINGENT_AMOUNT, 0)) EST_CONT,
                             sum(case
                                    when B.MAX_GL = B.GL_MONTH_NUMBER then
                                     ASSET_RESIDUAL.RESID
                                    else
                                     0
                                 end) as RESIDUAL
                        from ( -- grab the rownumber sorting by gl_month_number
                              select ROW_NUMBER() OVER(order by GL_MONTH_NUMBER) as ROW_NUM,
                                      max(GL_MONTH_NUMBER) OVER(partition by ILR_ID) as MAX_GL,
                                      LIS.*
                                from LS_ILR_SCHEDULE LIS
                               where ILR_ID = L_ILR_ID) B,
                             LS_ILR_PAYMENT_TERM C,
                             ( -- get the residual amount for the assets
                              select sum(nvl(RESIDUAL_AMOUNT,0)) as RESID
                                from LS_ASSET B
                               where ILR_ID = L_ILR_ID) ASSET_RESIDUAL
                       where C.ILR_ID = L_ILR_ID
                         and B.GL_MONTH_NUMBER >= TO_NUMBER(TO_CHAR(C.PAYMENT_TERM_DATE, 'yyyymm'))
                         and B.GL_MONTH_NUMBER <
                             TO_NUMBER(TO_CHAR(ADD_MONTHS(C.PAYMENT_TERM_DATE,
                                                          (DECODE(C.PAYMENT_FREQ_ID,
                                                                  1,
                                                                  12,
                                                                  2,
                                                                  6,
                                                                  3,
                                                                  3,
                                                                  1) * C.NUMBER_OF_TERMS)),
                                               'yyyymm'))
                       group by FLOOR((ROW_NUM - 1) / DECODE(C.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))) A,
                     (select PRE_PAYMENT_SW
                        from LS_LEASE A, LS_ILR B
                       where B.ILR_ID = L_ILR_ID
                         and A.LEASE_ID = B.LEASE_ID) B);

      return L_DIFF;
   end ILR_INTEGRITY;

   --**************************************************************************
   --                            ILR_ASSET_INTEGRITY
   --**************************************************************************

   function ILR_ASSET_INTEGRITY return number is
      L_DIFF number;

   begin
      select (sum(ABS(INT_ACC_DIFF)) + sum(ABS(PRIN_ACC_DIFF)) + sum(ABS(INT_AMT_DIFF)) +
             sum(ABS(PRIN_AMT_DIFF)) + sum(ABS(EXEC_ACC_DIFF)) + sum(ABS(EXEC_AMT_DIFF)) +
             sum(ABS(CONT_ACC_DIFF)) + sum(ABS(CONT_AMT_DIFF)))
        into L_DIFF
        from (select A.GL_MONTH_NUMBER,
                     A.INT_ACC - B.INT_ACC INT_ACC_DIFF,
                     A.PRIN_ACC - B.PRIN_ACC PRIN_ACC_DIFF,
                     DECODE(A.GL_MONTH_NUMBER,
                            C.MAX_GL_MN,
                            A.INT_AMT - B.INT_AMT + B.END_OBLIG,
                            A.INT_AMT - B.INT_AMT) INT_AMT_DIFF,
                     DECODE(A.GL_MONTH_NUMBER,
                            C.MAX_GL_MN,
                            A.PRIN_AMT - B.PRIN_AMT - B.END_OBLIG,
                            A.PRIN_AMT - B.PRIN_AMT) PRIN_AMT_DIFF,
                     A.EXEC_ACC - B.EXEC_ACC EXEC_ACC_DIFF,
                     A.EXEC_AMT - B.EXEC_AMT EXEC_AMT_DIFF,
                     A.CONT_ACC - B.CONT_ACC CONT_ACC_DIFF,
                     A.CONT_AMT - B.CONT_AMT CONT_AMT_DIFF,
                     A.CURR_COST - B.CURR_COST CURR_COST_DIFF,
                     A.CAP_COST - B.CAP_COST CAP_COST_DIFF,
                     A.BEG_OBLIG - B.BEG_OBLIG BEG_OBLIG_DIFF,
                     DECODE(A.GL_MONTH_NUMBER,
                            C.MAX_GL_MN,
                            A.END_OBLIG - B.END_OBLIG + B.END_OBLIG,
                            A.END_OBLIG - B.END_OBLIG) END_OBLIG_DIFF,
                     A.END_UNPAID - B.END_UNPAID END_UNPAID_DIFF
                from (select GL_MONTH_NUMBER,
                             sum(NVL(INTEREST_ACCRUAL, 0)) INT_ACC,
                             sum(NVL(PRINCIPAL_ACCRUAL, 0)) PRIN_ACC,
                             sum(NVL(INTEREST_AMOUNT, 0)) INT_AMT,
                             sum(NVL(PRINCIPAL_AMOUNT, 0)) PRIN_AMT,
                             sum(NVL(EXECUTORY_ACCRUAL, 0)) EXEC_ACC,
                             sum(NVL(EXECUTORY_AMOUNT, 0)) EXEC_AMT,
                             sum(NVL(CONTINGENT_ACCRUAL, 0)) CONT_ACC,
                             sum(NVL(CONTINGENT_AMOUNT, 0)) CONT_AMT,
                             sum(NVL(CURRENT_LEASE_COST, 0)) CURR_COST,
                             sum(NVL(END_CAPITALIZED_LEASE_COST, 0)) CAP_COST,
                             sum(NVL(BEG_LEASE_OBLIGATION, 0)) BEG_OBLIG,
                             sum(NVL(END_LEASE_OBLIGATION, 0)) END_OBLIG,
                             sum(NVL(UNPAID_BALANCE, 0)) END_UNPAID
                        from LS_ASSET_ACTIVITY
                       where LS_ASSET_ID in
                             (select distinct LS_ASSET_ID from LS_ASSET where ILR_ID = L_ILR_ID)
                         and LS_ACTIVITY_TYPE_ID = 100
                       group by GL_MONTH_NUMBER) A,
                     (select GL_MONTH_NUMBER,
                             NVL(INTEREST_ACCRUAL, 0) INT_ACC,
                             NVL(PRINCIPAL_ACCRUAL, 0) PRIN_ACC,
                             NVL(INTEREST_AMOUNT, 0) INT_AMT,
                             NVL(PRINCIPAL_AMOUNT, 0) PRIN_AMT,
                             NVL(EXECUTORY_ACCRUAL, 0) EXEC_ACC,
                             NVL(EXECUTORY_AMOUNT, 0) EXEC_AMT,
                             NVL(CONTINGENT_ACCRUAL, 0) CONT_ACC,
                             NVL(CONTINGENT_AMOUNT, 0) CONT_AMT,
                             NVL(CURRENT_LEASE_COST, 0) CURR_COST,
                             NVL(CAPITALIZED_COST, 0) CAP_COST,
                             NVL(BEG_LEASE_OBLIGATION, 0) BEG_OBLIG,
                             NVL(END_LEASE_OBLIGATION, 0) END_OBLIG,
                             NVL(END_UNPAID_BALANCE, 0) END_UNPAID
                        from LS_ILR_SCHEDULE
                       where ILR_ID = L_ILR_ID) B,
                     (select max(GL_MONTH_NUMBER) MAX_GL_MN
                        from LS_ILR_SCHEDULE
                       where ILR_ID = L_ILR_ID) C
               where A.GL_MONTH_NUMBER = B.GL_MONTH_NUMBER);

      return L_DIFF;
   end ILR_ASSET_INTEGRITY;

   --**************************************************************************
   --                            ILR_TOTALS
   --**************************************************************************

   function ILR_TOTALS return number is
      L_DIFF number;

   begin
      select DECODE(CEIL((D.CAPITAL_YN_SW + D.IFRS_CAPITAL_YN_SW) / 2),
                    1,
                    BEG_LEASE_OBLIGATION - SUM_PRIN_ACC - PREPAY_ADJ,
                    0) + DECODE(CEIL((D.CAPITAL_YN_SW + D.IFRS_CAPITAL_YN_SW) / 2),
                                1,
                                BEG_LEASE_OBLIGATION - SUM_PRIN_AMT,
                                0) + BEG_UNPAID_BALANCE - SUM_PRIN_AMT - SUM_INT_AMT +
             BEG_UNPAID_BALANCE - SUM_PRIN_ACC - SUM_INT_ACC - PREPAY_ADJ + SUM_PRIN_ACC -
             SUM_PRIN_AMT + PREPAY_ADJ + SUM_INT_ACC - SUM_INT_AMT
        into L_DIFF
        from (select ROW_NUMBER() OVER(order by GL_MONTH_NUMBER) as THE_ROW,
                     sum(INTEREST_ACCRUAL) OVER() SUM_INT_ACC,
                     sum(PRINCIPAL_ACCRUAL) OVER() SUM_PRIN_ACC,
                     sum(INTEREST_AMOUNT) OVER() SUM_INT_AMT,
                     sum(PRINCIPAL_AMOUNT) OVER() SUM_PRIN_AMT,
                     FIRST_VALUE(BEG_LEASE_OBLIGATION) OVER(partition by ILR_ID order by GL_MONTH_NUMBER) as BEG_LEASE_OBLIGATION,
                     FIRST_VALUE(BEG_UNPAID_BALANCE) OVER(PARTITION by ILR_ID order by GL_MONTH_NUMBER) as BEG_UNPAID_BALANCE,
                     DECODE(FIRST_VALUE(INTEREST_AMOUNT)
                            OVER(partition by ILR_ID order by GL_MONTH_NUMBER),
                            0,
                            FIRST_VALUE(PRINCIPAL_AMOUNT)
                            OVER(partition by ILR_ID order by GL_MONTH_NUMBER),
                            0) as PREPAY_ADJ
                from LS_ILR_SCHEDULE A
               where A.ILR_ID = L_ILR_ID) B,
             LS_ILR C,
             LS_LEASE D
       where B.THE_ROW = 1
         and C.LEASE_ID = D.LEASE_ID
         and C.ILR_ID = L_ILR_ID;

      return L_DIFF;
   end ILR_TOTALS;

   --**************************************************************************
   --                            ILR_ROLL
   --**************************************************************************

   function ILR_ROLL return number is
      L_DIFF number;

   begin
      select sum(ABS(MONTH_LO_ROLL)) + sum(ABS(MONTH_UP_ROLL)) + sum(ABS(MONTH_TO_MONTH_UP_ROLL)) +
             sum(ABS(MONTH_TO_MONTH_LO_ROLL)) TOTAL_TEST
        into L_DIFF
        from (select DECODE(A.BEG_LEASE_OBLIGATION,
                            0,
                            0,
                            (A.BEG_LEASE_OBLIGATION - A.END_LEASE_OBLIGATION - A.PRINCIPAL_AMOUNT)) MONTH_LO_ROLL,
                     (A.BEG_UNPAID_BALANCE - A.END_UNPAID_BALANCE - A.PRINCIPAL_AMOUNT -
                     A.INTEREST_AMOUNT) MONTH_UP_ROLL,
                     NVL((A.END_UNPAID_BALANCE - Z.BEG_UNPAID_BALANCE), 0) MONTH_TO_MONTH_UP_ROLL,
                     NVL((A.END_LEASE_OBLIGATION - Z.BEG_LEASE_OBLIGATION), 0) MONTH_TO_MONTH_LO_ROLL
                from LS_ILR_SCHEDULE A,
                     (select TO_NUMBER(TO_CHAR(ADD_MONTHS(TO_DATE(TO_CHAR(B.GL_MONTH_NUMBER),
                                                                  'yyyymm'),
                                                          -1),
                                               'yyyymm')) NEW_GL,
                             B.*
                        from LS_ILR_SCHEDULE B) Z
               where A.ILR_ID = L_ILR_ID
                 and A.ILR_ID = Z.ILR_ID(+)
                 and A.GL_MONTH_NUMBER = Z.NEW_GL(+));

      return L_DIFF;
   end ILR_ROLL;

   --**************************************************************************
   --                            ASSET_ROLL
   --**************************************************************************

   function ASSET_ROLL return number is
      L_DIFF number;

   begin
      select sum(ABS(TOTAL_OBLIG_EM_ROLL)) + sum(ABS(TOTAL_OBLIG_CM_ROLL)) + sum(ABS(CAP_CM_ROLL)) +
             sum(ABS(CAP_EM_ROLL)) + sum(ABS(CAP_EM_ROLL)) + sum(ABS(NONCURR_EM_ROLL)) +
             sum(ABS(NONCURR_CM_ROLL)) + sum(ABS(DEPR_CM_ROLL)) + sum(ABS(DEPR_EM_ROLL)) +
             sum(ABS(REG_CM_ROLL)) + sum(ABS(REG_EM_ROLL)) + sum(ABS(UNPAID_BAL_ROLL)) TEST
        into L_DIFF
        from (select DECODE(A.LS_ASSET_ID,
                            LEAD(A.LS_ASSET_ID, 1, 0) OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            END_LEASE_OBLIGATION - LEAD(BEG_LEASE_OBLIGATION, 1, 0)
                            OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            0) TOTAL_OBLIG_EM_ROLL,
                     DECODE(CEIL((D.CAPITAL_YN_SW + D.IFRS_CAPITAL_YN_SW) / 2),
                            1,
                            (BEG_LEASE_OBLIGATION - END_LEASE_OBLIGATION - PRINCIPAL_AMOUNT),
                            0) as TOTAL_OBLIG_CM_ROLL,
                     BEG_CAPITALIZED_LEASE_COST - END_CAPITALIZED_LEASE_COST as CAP_CM_ROLL,
                     DECODE(A.LS_ASSET_ID,
                            LEAD(A.LS_ASSET_ID, 1, 0) OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            END_CAPITALIZED_LEASE_COST - LEAD(BEG_CAPITALIZED_LEASE_COST, 1, 0)
                            OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            0) CAP_EM_ROLL,
                     DECODE(A.LS_ASSET_ID,
                            LEAD(A.LS_ASSET_ID, 1, 0) OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            END_NONCURR_LEASE_OBLIGATION - LEAD(BEG_NONCURR_LEASE_OBLIGATION, 1, 0)
                            OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            0) NONCURR_EM_ROLL,
                     BEG_NONCURR_LEASE_OBLIGATION - END_NONCURR_LEASE_OBLIGATION -
                     DECODE(CEIL((D.CAPITAL_YN_SW + D.IFRS_CAPITAL_YN_SW) / 2),
                            1,
                            DECODE(A.LS_ASSET_ID,
                                   LEAD(A.LS_ASSET_ID, 12, 0)
                                   OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                                   LEAD(PRINCIPAL_AMOUNT, 12, 0)
                                   OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                                   0),
                            0) as NONCURR_CM_ROLL,
                     BEG_DEPR_RESERVE + DEPR_EXPENSE - END_DEPR_RESERVE as DEPR_CM_ROLL,
                     DECODE(A.LS_ASSET_ID,
                            LEAD(A.LS_ASSET_ID, 1, 0) OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            END_DEPR_RESERVE - LEAD(BEG_DEPR_RESERVE, 1, 0)
                            OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            0) DEPR_EM_ROLL,
                     BEG_REGULATORY_AMORT + REGULATORY_AMORT - END_REGULATORY_AMORT as REG_CM_ROLL,
                     DECODE(A.LS_ASSET_ID,
                            LEAD(A.LS_ASSET_ID, 1, 0) OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            END_REGULATORY_AMORT - LEAD(BEG_REGULATORY_AMORT, 1, 0)
                            OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            0) REG_EM_ROLL,
                     DECODE(A.LS_ASSET_ID,
                            LAG(A.LS_ASSET_ID, 1, 0) OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER),
                            LAG(UNPAID_BALANCE, 1, 0)
                            OVER(order by A.LS_ASSET_ID, GL_MONTH_NUMBER) - INTEREST_AMOUNT -
                            PRINCIPAL_AMOUNT - UNPAID_BALANCE,
                            0) UNPAID_BAL_ROLL
                from LS_ASSET_ACTIVITY A, LS_ASSET B, LS_ILR C, LS_LEASE D
               where A.LS_ASSET_ID in
                     (select distinct LS_ASSET_ID from LS_ASSET where ILR_ID = L_ILR_ID)
                 and LS_ACTIVITY_TYPE_ID = 100
                 and A.LS_ASSET_ID = B.LS_ASSET_ID
                 and B.ILR_ID = C.ILR_ID
                 and C.LEASE_ID = D.LEASE_ID);

      return L_DIFF;
   end ASSET_ROLL;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end LS_TEST_ILR_SCHEDULE;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (373, 0, 10, 4, 0, 0, 29902, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029902_lease_LS_TEST_ILR_SCHEDULE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;