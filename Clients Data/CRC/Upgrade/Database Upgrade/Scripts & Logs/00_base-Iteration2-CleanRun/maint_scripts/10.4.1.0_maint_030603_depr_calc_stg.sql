/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030603_depr_calc_stg.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/06/2013 Daniel Motter  Point release
||============================================================================
*/

alter table DEPR_CALC_STG modify FISCALQTRSTART date;

alter table DEPR_CALC_STG
   add (SMOOTH_CURVE            varchar2(5),
        MIN_CALC                number(22,2),
        MIN_UOP_EXP             number(22,2),
        YTD_UOP_DEPR            number(22,2),
        YTD_UOP_DEPR_2          number(22,2),
        CURR_UOP_EXP            number(22,2),
        CURR_UOP_EXP_2          number(22,2),
        LESS_YEAR_UOP_DEPR      number(22,2),
        DEPRECIATION_UOP_RATE   number(22,8),
        DEPRECIATION_UOP_RATE_2 number(22,8),
        DEPRECIATION_BASE_UOP   number(22,2),
        DEPRECIATION_BASE_UOP_2 number(22,2),
        COR_ACTIVITY_2          number(22,2));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (497, 0, 10, 4, 1, 0, 30603, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030603_depr_calc_stg.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;