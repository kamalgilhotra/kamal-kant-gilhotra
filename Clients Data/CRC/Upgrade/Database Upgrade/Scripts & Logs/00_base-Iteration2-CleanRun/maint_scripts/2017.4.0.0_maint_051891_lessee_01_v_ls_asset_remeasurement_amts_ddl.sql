/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051891_lessee_01_v_ls_asset_remeasurement_amts_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/05/2018 Josh Sandler     Rebuild V_LS_ASSET_REMEASUREMENT_AMTS
||============================================================================
*/

CREATE OR REPLACE VIEW V_LS_ASSET_REMEASUREMENT_AMTS AS
SELECT ilr_id, revision, ls_asset_id, set_of_books_id, remeasurement_date, current_revision, om_to_cap_indicator,
prior_month_end_obligation,
prior_month_end_liability,
prior_month_end_lt_obligation,
prior_month_end_lt_liability,
prior_month_end_deferred_rent,
prior_month_end_prepaid_rent,
prior_month_end_depr_reserve,
prior_month_end_nbv,
prior_month_end_capital_cost,
prior_month_net_rou_asset,
prior_month_end_arrears_accr,
remeasurement_type,
partial_termination,
original_asset_quantity,
current_asset_quantity,
is_remeasurement,
Sum(unpaid_accrued_interest) AS unpaid_accrued_interest
FROM (
  SELECT stg.ilr_id, stg.revision, sch.ls_asset_id, sch.set_of_books_id, o.remeasurement_date, ilr.current_revision,
  CASE
    WHEN sch.is_om = 1 AND stg.is_om = 0 THEN
      1
    ELSE
      0
  END om_to_cap_indicator,
  sch.end_obligation AS prior_month_end_obligation, sch.end_liability AS prior_month_end_liability,
  sch.end_lt_obligation AS prior_month_end_lt_obligation, sch.end_lt_liability AS prior_month_end_lt_liability,
  sch.end_deferred_rent AS prior_month_end_deferred_rent,
  sch.end_prepaid_rent AS prior_month_end_prepaid_rent,
  depr.end_reserve AS prior_month_end_depr_reserve,
  sch.end_capital_cost - depr.end_reserve AS prior_month_end_nbv,
  sch.end_capital_cost AS prior_month_end_capital_cost,
  sch.end_net_rou_asset as prior_month_net_rou_asset,
  sch.end_arrears_accrual AS prior_month_end_arrears_accr,
  o.remeasurement_type as remeasurement_type,
  o.partial_termination as partial_termination,
  prior_opt.asset_quantity as original_asset_quantity,
  o.asset_quantity AS current_asset_quantity,
  1 is_remeasurement,
  CASE WHEN int_sch.MONTH >= Nvl(
                                  Add_Months(Max(Decode(int_sch.interest_paid, 0, NULL, int_sch.MONTH)) OVER(PARTITION BY int_sch.ls_asset_id, int_sch.revision, int_sch.set_of_books_id), Decode(mla.pre_payment_sw, 0, 1, 0)),
                                  First_Value(int_sch.MONTH) OVER(PARTITION BY int_sch.ls_asset_id, int_sch.revision, int_sch.set_of_books_id ORDER BY int_sch.month)
                                )
  THEN
    int_sch.interest_accrual
  ELSE
    0
  END unpaid_accrued_interest
  FROM ls_ilr_stg stg
    JOIN ls_ilr ilr ON stg.ilr_id = ilr.ilr_id
    JOIN ls_ilr_options o ON stg.ilr_id = o.ilr_id AND stg.revision = o.revision
    JOIN ls_ilr_options prior_opt ON stg.ilr_id = prior_opt.ilr_id AND prior_opt.revision = ilr.current_revision
    JOIN ls_ilr_asset_map map ON map.ilr_id = stg.ilr_id AND map.revision = ilr.current_revision
    JOIN ls_asset_schedule sch ON map.ls_asset_id = sch.ls_asset_id AND sch.revision = ilr.current_revision AND sch.set_of_books_id = stg.set_of_books_id
    left OUTER JOIN ls_depr_forecast depr ON depr.ls_asset_id = map.ls_asset_id AND depr.revision = ilr.current_revision AND depr.set_of_books_id = stg.set_of_books_id
    JOIN ls_lease mla ON mla.lease_id = ilr.lease_id
    JOIN ls_asset_schedule int_sch ON int_sch.ls_asset_id = sch.ls_asset_id AND int_sch.revision = ilr.current_revision AND int_sch.set_of_books_id = sch.set_of_books_id AND int_sch.MONTH < Trunc(o.remeasurement_date, 'month')
  WHERE ilr.current_revision <> stg.revision
  AND o.remeasurement_date IS NOT NULL
  AND Trunc(o.remeasurement_date, 'month') = Add_Months(sch.MONTH,1)
  AND (Trunc(o.remeasurement_date, 'month') = Add_Months(depr.MONTH,1) OR depr.MONTH IS NULL)
)
GROUP BY ilr_id, revision, ls_asset_id, set_of_books_id, remeasurement_date, current_revision, om_to_cap_indicator,
prior_month_end_obligation,
prior_month_end_liability,
prior_month_end_lt_obligation,
prior_month_end_lt_liability,
prior_month_end_deferred_rent,
prior_month_end_prepaid_rent,
prior_month_end_depr_reserve,
prior_month_end_nbv,
prior_month_end_capital_cost,
prior_month_net_rou_asset,
prior_month_end_arrears_accr,
remeasurement_type,
partial_termination,
original_asset_quantity,
current_asset_quantity,
is_remeasurement;


	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7622, 0, 2017, 4, 0, 0, 51891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051891_lessee_01_v_ls_asset_remeasurement_amts_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;