/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049002_2_sys_01_update_f_get_call_stack_format_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.2.0.0 02/22/2018 Andrew Hill    Update call stack formatting
||============================================================================
*/

create or replace FUNCTION f_get_call_stack RETURN VARCHAR2 IS
  call_stack VARCHAR2(32767);
  l_depth pls_integer;
BEGIN
  $IF dbms_db_version.VERSION > 11 $THEN
    --https://oracle-base.com/articles/12c/utl-call-stack-12cr1#call-stack
    l_depth := utl_call_stack.dynamic_depth;
    call_stack := '***** Call Stack *****' || CHR(10);
    --Log window uses proportional font. Alignment is wonky
    call_stack := call_stack || 'Depth   Lexical Line        Owner         Ed.     Name' || CHR(10);
    call_stack := call_stack || '.          Depth   Number' || CHR(10);
    call_stack := call_stack || '--------- ---------   ---------    ---------------   -----     --------------------';

    FOR I IN 1 .. l_depth LOOP
      call_stack := call_stack || CHR(10);
      call_stack := call_stack || rpad(I, 10) ||
                                  RPAD(UTL_CALL_STACK.lexical_depth(I), 10) ||
                                  RPAD(TO_CHAR(UTL_CALL_STACK.unit_line(I)), 12) ||
                                  RPAD(NVL(UTL_CALL_STACK.OWNER(I),' '), 20) ||
                                  RPAD(NVL(UTL_CALL_STACK.current_edition(i),' '), 3) ||
                                  UTL_CALL_STACK.concatenate_subprogram(UTL_CALL_STACK.subprogram(i));
    END LOOP;
  $ELSE
    call_stack := dbms_utility.format_call_stack;
  $END
  RETURN call_stack;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4155, 0, 2017, 2, 0, 0, 49002, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_049002_2_sys_01_update_f_get_call_stack_format_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;