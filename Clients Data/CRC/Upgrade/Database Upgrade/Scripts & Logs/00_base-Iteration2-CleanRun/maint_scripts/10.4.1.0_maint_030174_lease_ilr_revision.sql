/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030174_lease_ilr_revision.sql
|| Description: Updates to allow revisions on ILR's
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/12/2013 Ryan Oliveria   Point Release
||============================================================================
*/

--* Default revision, change primary key on ls_ilr_approval
update LS_ILR_APPROVAL set REVISION = 1 where REVISION is null;

insert into LS_ILR_APPROVAL
   (ILR_ID, REVISION, TIME_STAMP, USER_ID, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
   select ILR_ID,
          1,
          sysdate,
          user,
          (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where SUBSYSTEM = 'ilr_approval'),
          1
     from LS_ILR
    where not exists (select 1 from LS_ILR_APPROVAL where ILR_ID = LS_ILR.ILR_ID);

alter table LS_ILR_APPROVAL drop primary key drop index;

alter table LS_ILR_APPROVAL
   add constraint PK_LS_ILR_APPROVAL
       primary key (ILR_ID, REVISION)
       using index tablespace PWRPLANT_IDX;

--* Create ls_ILR_options table (include primary and foreign keys)
create table LS_ILR_OPTIONS
(
 ILR_ID                  number(22,0) not null,
 REVISION                number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 PURCHASE_OPTION_TYPE_ID number(22,0),
 PURCHASE_OPTION_AMT     number(22,2),
 RENEWAL_OPTION_TYPE_ID  number(22,0),
 CANCELABLE_TYPE_ID      number(22,0),
 ITC_SW                  number(1,0),
 PARTIAL_RETIRE_SW       number(1,0),
 SUBLET_SW               number(1,0),
 MUNI_BO_SW              number(1,0),
 INCEPTION_AIR           number(22,8),
 LEASE_CAP_TYPE_ID       number(22,0)
);

alter table LS_ILR_OPTIONS
  add constraint PK_LS_ILR_OPTIONS
    primary key (ILR_ID, REVISION)
    using index tablespace PWRPLANT_IDX;

alter table LS_ILR_OPTIONS
  add constraint FK_LS_ILR_OPTIONS
    foreign key (ILR_ID, REVISION)
    references LS_ILR_APPROVAL (ILR_ID, REVISION);

alter table ls_ilr_options
  add constraint FK_ILR_OPT_CAP_TYPE
    foreign key (LEASE_CAP_TYPE_ID)
    references LS_LEASE_CAP_TYPE (LS_LEASE_CAP_TYPE_ID);

--* Populate ls_ILR_options table
insert into LS_ILR_OPTIONS
   (ILR_ID, REVISION, TIME_STAMP, USER_ID, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT,
   RENEWAL_OPTION_TYPE_ID, CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW,
   LEASE_CAP_TYPE_ID, INCEPTION_AIR)
   select LS_ILR.ILR_ID,
          1,
          sysdate,
          user,
          LS_LEASE_OPTIONS.PURCHASE_OPTION_TYPE_ID,
          LS_LEASE_OPTIONS.PURCHASE_OPTION_AMT,
          LS_LEASE_OPTIONS.RENEWAL_OPTION_TYPE_ID,
          LS_LEASE_OPTIONS.CANCELABLE_TYPE_ID,
          LS_LEASE_OPTIONS.ITC_SW,
          LS_LEASE_OPTIONS.PARTIAL_RETIRE_SW,
          LS_LEASE_OPTIONS.SUBLET_SW,
          LS_LEASE_OPTIONS.MUNI_BO_SW,
          LS_ILR.LS_LEASE_CAP_TYPE_ID,
          LS_ILR.INCEPTION_AIR
     from LS_ILR, LS_LEASE_OPTIONS, LS_LEASE
    where LS_LEASE.LEASE_ID = LS_ILR.LEASE_ID
    and LS_LEASE_OPTIONS.LEASE_ID = LS_LEASE.LEASE_ID
    and LS_LEASE_OPTIONS.REVISION = LS_LEASE.CURRENT_REVISION
    and not exists (select 1 from LS_ILR_OPTIONS where ILR_ID = LS_ILR.ILR_ID);

--* Drop revision and options columns from ls_ILR
alter table LS_ILR drop column LS_LEASE_CAP_TYPE_ID;
alter table LS_ILR drop column INCEPTION_AIR;

--* Add current_revision column on ls_ILR
alter table LS_ILR add CURRENT_REVISION number(22,0);

update LS_ILR set CURRENT_REVISION = 1 where CURRENT_REVISION is null;

--* Add revision to ILR children
alter table LS_ILR_PAYMENT_TERM add REVISION number(22,0);

--* Default revision on ILR children
update LS_ILR_PAYMENT_TERM set REVISION = 1 where REVISION is null;

--* Drop foreign keys from ILR children
alter table LS_ILR_PAYMENT_TERM drop constraint FK_PYMTTERMS_ILR;

--* Drop primary keys from ILR children
alter table LS_ILR_PAYMENT_TERM drop primary key drop index;

--* Create primary key (ILR_id, revision) on ILR children
alter table LS_ILR_PAYMENT_TERM
  add constraint PK_LS_ILR_PAYMENT_TERM
      primary key (ILR_ID, REVISION, PAYMENT_TERM_ID)
      using index tablespace PWRPLANT_IDX;

--* Create foreign key (ILR_id, revision) on ILR children
alter table LS_ILR_PAYMENT_TERM
   add constraint FK_PYMTTERMS_ILR
       foreign key (ILR_ID, REVISION)
       references LS_ILR_APPROVAL (ILR_ID, REVISION);

--* Create "New Revision" ILR status
insert into LS_ILR_STATUS
   (ILR_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (7, sysdate, user, 'New Revision', 'New Revision');

--* Update workflow sql to include nvl()
update WORKFLOW_TYPE
   set SQL_APPROVAL_AMOUNT = 'select nvl(sum(beg_obligation),0) from ls_ilr_schedule where ilr_id = <<id_field1>> and month = (select min(month) from ls_ilr_schedule where ilr_id = <<id_field1>>)'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_TYPE
   set SQL_APPROVAL_AMOUNT = 'select nvl(sum(max_lease_line),0) from LS_LEASE_COMPANY where lease_id = <<id_field1>>'
 where SUBSYSTEM = 'mla_approval';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (466, 0, 10, 4, 1, 0, 30174, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030174_lease_ilr_revision.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
