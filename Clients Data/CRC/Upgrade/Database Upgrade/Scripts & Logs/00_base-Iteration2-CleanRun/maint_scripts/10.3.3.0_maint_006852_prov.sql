/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_006852_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/03/2011 Blake Andrews  Point Release
||============================================================================
*/

--###PATCH(6852)
create table PWRPLANT.TAX_ACCRUAL_MONTH_DROPDOWNS
(
 DROPDOWN_ID    number(22) not null,
 DROPDOWN_VALUE varchar2(35) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18)
);

alter table TAX_ACCRUAL_MONTH_DROPDOWNS
   add constraint TA_MONTH_DROPDOWNS_PK
       primary key (DROPDOWN_ID, DROPDOWN_VALUE)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_MONTH_DROPDOWNS
before update or insert on PWRPLANT.TAX_ACCRUAL_MONTH_DROPDOWNS
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

insert into TAX_ACCRUAL_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, COMPANY_ID)
values
   (6, 'Month GL Field 1 Dropdown', 0,
    'Dropdown indicator for custom dropdown gl_value1 on Months Manager: 0=No Dropdown, 1=Editable Dropdown, 2=Non-Editable Dropdown',
    -1);

insert into TAX_ACCRUAL_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, COMPANY_ID)
values
   (7, 'Month GL Field 2 Dropdown', 0,
    'Dropdown indicator for custom dropdown gl_value2 on Months Manager: 0=No Dropdown, 1=Editable Dropdown, 2=Non-Editable Dropdown',
    -1);

insert into TAX_ACCRUAL_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, COMPANY_ID)
values
   (8, 'Month GL Field 3 Dropdown', 0,
    'Dropdown indicator for custom dropdown gl_value3 on Months Manager: 0=No Dropdown, 1=Editable Dropdown, 2=Non-Editable Dropdown',
    -1);

insert into TAX_ACCRUAL_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, COMPANY_ID)
values
   (9, 'Month GL Field 4 Dropdown', 0,
    'Dropdown indicator for custom dropdown gl_value4 on Months Manager: 0=No Dropdown, 1=Editable Dropdown, 2=Non-Editable Dropdown',
    -1);

insert into TAX_ACCRUAL_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, COMPANY_ID)
values
   (10, 'Month GL Field 5 Dropdown', 0,
    'Dropdown indicator for custom dropdown gl_value5 on Months Manager: 0=No Dropdown, 1=Editable Dropdown, 2=Non-Editable Dropdown',
    -1);

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM_SCREEN,
    SUBSYSTEM_DISPLAY)
values
   ('tax_accrual_month_dropdowns', 'spl*', 'Provision GL Month Custom Dropdowns',
    'Labels and dropdown values on custom fields in Months Manager', 'tax',
    'w_tax_accrual_cust_month_fields');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (5, 0, 10, 3, 3, 0, 6852, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_006852_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
