/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010155_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.2   07/02/2012 Sunjin Cone    Point Release
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Added Asset Tax Repairs Orig Mo',
          'Shows Summary of added Assets and associated tax expense for a time span matching both the gl posting month and the tax original month.',
          'dw_repairs_posted_orig_cpr_report',
          'SHOW ZERO ASSETS, ALREADY HAS REPORT TIME, REPAIRS',
          'REPRS - 1201',
          '',
          13,
          13,
          2,
          3,
          1,
          'REPRS - 1201',
          1,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER, INPUT_WINDOW,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_NUMBER, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Added Asset Tax Repairs GL Mo',
          'Shows Summary of added Assets and associated tax expense for a time span matching the gl posting month only.',
          'dw_repairs_posted_cpr_report',
          'SHOW ZERO ASSETS, ALREADY HAS REPORT TIME, REPAIRS',
          'REPRS - 1202',
          '',
          13,
          13,
          2,
          3,
          1,
          'REPRS - 1202',
          7,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (158, 0, 10, 3, 4, 2, 10155, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.2_maint_010155_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;