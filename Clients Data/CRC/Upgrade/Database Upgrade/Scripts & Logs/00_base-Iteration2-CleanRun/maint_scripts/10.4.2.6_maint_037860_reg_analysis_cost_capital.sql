/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037860_reg_analysis_cost_capital.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/21/2014 Ryan Oliveria  Cost of Capital Reports
||========================================================================================
*/

insert into REG_ANALYSIS_PRESENTATION
   (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (8, 'Case to Case', 'Cost of Capital Comparison - Case to Case', 1);
insert into REG_ANALYSIS_PRESENTATION
   (RA_PRESENT_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID)
values
   (9, 'Year to Year', 'Cost of Capital Comparison - Year to Year', 1);

insert into REG_ANALYSIS_DETAIL
   (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
values
   (8, 'Case to Case', 'Cost of Capital Comparison - Case to Case', 1, 4, 8);

insert into REG_ANALYSIS_DETAIL
   (RA_DETAIL_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID, RA_GROUP_ID, RA_PRESENT_ID)
values
   (9, 'Year to Year', 'Cost of Capital Comparison - Year to Year', 1, 4, 9);

insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (8,2);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (8,3);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (9,1);
insert into REG_ANALYSIS_DETAIL_OPTION (RA_DETAIL_ID, RA_OPTION_ID) values (9,4);


insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER,
    OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Capital Structure Case Differences',
          'Capital Structure Case Differences',
          null SUBSYSTEM,
          'dw_reg_report_cap_struct_diff',
          'already has report time' SPECIAL_NOTE,
          null REPORT_TYPE,
          null TIME_OPTION,
          'Capital Structure Case Differences' REPORT_NUMBER,
          null INPUT_WINDOW,
          null FILTER_OPTION,
          null STATUS,
          101 PP_REPORT_SUBSYSTEM_ID,
          407 REPORT_TYPE_ID,
          null PP_REPORT_TIME_OPTION_ID,
          null PP_REPORT_FILTER_ID,
          1 PP_REPORT_STATUS_ID,
          7 PP_REPORT_ENVIR_ID, /*All Hide environment, so it will only appear in Reg Analysis Wksp*/
          null DOCUMENTATION,
          null USER_COMMENT,
          null LAST_APPROVED_DATE,
          null PP_REPORT_NUMBER,
          null OLD_REPORT_NUMBER,
          null DYNAMIC_DW
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER,
    OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Capital Structure Rate Differences',
          'Capital Structure Rate Differences',
          null SUBSYSTEM,
          'dw_reg_report_cap_struct_diff2',
          'already has report time' SPECIAL_NOTE,
          null REPORT_TYPE,
          null TIME_OPTION,
          'Capital Structure Rate Differences' REPORT_NUMBER,
          null INPUT_WINDOW,
          null FILTER_OPTION,
          null STATUS,
          101 PP_REPORT_SUBSYSTEM_ID,
          407 REPORT_TYPE_ID,
          null PP_REPORT_TIME_OPTION_ID,
          null PP_REPORT_FILTER_ID,
          1 PP_REPORT_STATUS_ID,
          7 PP_REPORT_ENVIR_ID, /*All Hide environment, so it will only appear in Reg Analysis Wksp*/
          null DOCUMENTATION,
          null USER_COMMENT,
          null LAST_APPROVED_DATE,
          null PP_REPORT_NUMBER,
          null OLD_REPORT_NUMBER,
          null DYNAMIC_DW
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER,
    OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Capital Structure Return Impact',
          'Capital Structure Return Impact',
          null SUBSYSTEM,
          'dw_reg_report_cap_struct_return',
          'already has report time' SPECIAL_NOTE,
          null REPORT_TYPE,
          null TIME_OPTION,
          'Capital Structure Return Impact' REPORT_NUMBER,
          null INPUT_WINDOW,
          null FILTER_OPTION,
          null STATUS,
          101 PP_REPORT_SUBSYSTEM_ID,
          407 REPORT_TYPE_ID,
          null PP_REPORT_TIME_OPTION_ID,
          null PP_REPORT_FILTER_ID,
          1 PP_REPORT_STATUS_ID,
          7 PP_REPORT_ENVIR_ID, /*All Hide environment, so it will only appear in Reg Analysis Wksp*/
          null DOCUMENTATION,
          null USER_COMMENT,
          null LAST_APPROVED_DATE,
          null PP_REPORT_NUMBER,
          null OLD_REPORT_NUMBER,
          null DYNAMIC_DW
     from PP_REPORTS;


--** Case to Case presentation
insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 8, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_cap_struct_diff';

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 8, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_cap_struct_diff2';

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 8, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_cap_struct_return';

--** Year to Year presentation
insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 9, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_cap_struct_diff';

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 9, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_cap_struct_diff2';

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 9, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_cap_struct_return';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1148, 0, 10, 4, 2, 6, 37860, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_037860_reg_analysis_cost_capital.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;