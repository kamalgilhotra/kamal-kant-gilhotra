SET DEFINE OFF
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033713_reg_01_all_tables.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014  Sarah Byers
||============================================================================
*/

prompt
prompt Creating table REG_ANNUALIZATION_TYPE
prompt =====================================
prompt
create table REG_ANNUALIZATION_TYPE
(
  reg_annualization_id NUMBER(22) not null,
  description          VARCHAR2(35) not null,
  long_description     VARCHAR2(254),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_ANNUALIZATION_TYPE
  is '(  )  [  ]
THE REG ANNUALIZATION TYPE TABLE LISTS THE ANNUALIZATION TYPES AVAILABLE FOR EACH REGULATORY ACCOUNT WHEN PRODUCING HISTORIC CASES, SURVEILLANCE MONITORING OR CERTAIN REPORTING.';
comment on column REG_ANNUALIZATION_TYPE.reg_annualization_id
  is 'SYSTEM-ASSIGNED IDENTIFIERS OF AN ANNUALIZTION TYPE.';
comment on column REG_ANNUALIZATION_TYPE.description
  is 'SHORT DESCRIPTION OF THE ANNUALIZATION TYPE.
1 = 13 MONTH SIMPLE AVERAGE
2 = 13 MONTH AVERAGE OF AVERAGES
3 = 12 MONTH AVERAGE
4 = 2 POINT ANNUAL AVERAGE
5 = ENDING BALANCE
6 = SUM OF 12 MONTHS
7 = ENDING MONTH * 12
8 = INPUT';
comment on column REG_ANNUALIZATION_TYPE.long_description
  is 'LONG DESCRIPTION OF THE TYPE.';
comment on column REG_ANNUALIZATION_TYPE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ANNUALIZATION_TYPE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ANNUALIZATION_TYPE
  add constraint PK_REG_ANNUALIZATION_TYPE primary key (REG_ANNUALIZATION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_FAMILY
prompt =========================
prompt
create table REG_FAMILY
(
  reg_family_id     NUMBER(22) not null,
  description       VARCHAR2(35) not null,
  table_for_members VARCHAR2(35),
  user_id           VARCHAR2(18),
  time_stamp        DATE
)
;
comment on table REG_FAMILY
  is '(  )  [  ]
THE REG FAMILY TABLE RECORDS THE RELATIONSHIPS BETWEEN REGULATORY ACCOUNTS, E.G. A SET OF REGULATORY ACCOUNTS MAY RELATE TO A DEPRECIATION GROUP (THE DEPRECIATION GROUPS BEING THE FAMILY).  THE MEMBER IDS WOULD INDICATE THE PARTICULAR MEMBER OF THE FAMILY, E.G. THE PARTICULAR DEPRECIATION GROUP.  FOR EACH REGULATORY FAMILY DEFINED IT WILL BE NECESSARY TO CREATE CORRESPONDING MEMBER AND COMPONENT TABLES.';
comment on column REG_FAMILY.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY.';
comment on column REG_FAMILY.description
  is 'DESCRIPTION OF THE FAMILY, FOR EXAMPLE “DEPRECIATION GROUPS.”';
comment on column REG_FAMILY.table_for_members
  is 'THIS IS THE TABLE ON WHICH THE MEMBERS OF THE FAMILY CAN BE OBTAINED, E.G. THE DEPR GROUP TABLE WOULD HAVE ALL OF THE DEPRECIATION GROUP IDS.';
comment on column REG_FAMILY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_FAMILY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_FAMILY
  add constraint PK_REG_FAMILY primary key (REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_FERC_ACCOUNT
prompt ===============================
prompt
create table REG_FERC_ACCOUNT
(
  ferc_acct            VARCHAR2(35) not null,
  service              VARCHAR2(1) not null,
  description          VARCHAR2(255) not null,
  bs_or_is             VARCHAR2(35) not null,
  classification       VARCHAR2(35) not null,
  user_id              VARCHAR2(18),
  time_stamp           DATE,
  ferc_sys_of_accts_id NUMBER(22)
)
;
comment on table REG_FERC_ACCOUNT
  is '(  )  [  ]
THE REG FERC ACCOUNT TABLE CONTAINS THE LIST OF FERC ACCOUNTS TO WHICH REG ACCOUNTS WILL BE ASSOCIATED.';
comment on column REG_FERC_ACCOUNT.ferc_acct
  is 'MAJOR FERC ACCOUNT (UNIFORM SYSTEM OF ACCOUNTS).';
comment on column REG_FERC_ACCOUNT.service
  is '''C'' FOR COMMON, ''E'' FOR ELECTRIC, ''G'' FOR GAS, ETC.';
comment on column REG_FERC_ACCOUNT.description
  is 'DESCRIPTION OF THE FERC ACCOUNT';
comment on column REG_FERC_ACCOUNT.bs_or_is
  is 'INDICATES WHETHER THE FERC ACCOUNT BELONGS TO THE BALANCE SHEET OR INCOME STATEMENT – VALUES ARE ''BALANCE SHEET'' AND ''INCOME STATEMENT''';
comment on column REG_FERC_ACCOUNT.classification
  is 'INDICATES THE CLASSIFICATION OF THE FERC ACCOUNT, E.G. ASSETS OR LIABILITIES FOR BALANCE SHEET';
comment on column REG_FERC_ACCOUNT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_FERC_ACCOUNT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_FERC_ACCOUNT
  add constraint PK_REG_FERC_ACCOUNT primary key (FERC_ACCT)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FERC_ACCOUNT
  add constraint R_REG_FERC_ACCOUNT1 foreign key (FERC_SYS_OF_ACCTS_ID)
  references FERC_SYS_OF_ACCTS (FERC_SYS_OF_ACCTS_ID);

prompt
prompt Creating table REG_SOURCE
prompt =========================
prompt
create table REG_SOURCE
(
  reg_source_id NUMBER(22) not null,
  description   VARCHAR2(35) not null,
  non_standard  NUMBER(22) not null,
  user_id       VARCHAR2(18),
  time_stamp    DATE
)
;
comment on table REG_SOURCE
  is '(F)  [  ]
THE REG SOURCE TABLE IS A FIXED TABLE IDENTIFYING THE VARIOUS SOURCES FOR THE DOLLAR DATA BOTH MAINLY WITHIN POWERPLANT BUT ALSO EXTERNAL SOURCES.';
comment on column REG_SOURCE.reg_source_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY SOURCE.';
comment on column REG_SOURCE.description
  is 'THE SHORT DESCRIPTION OF THE REGULATORY SOURCE.
0 = INPUT
1 = DEPR LEDGER
2 = CPR
3 = CWIP
4 = CWIP COMPUTATION
5 = CR SUMMARY
6 = CR DETAIL
7 = PROPERTY TAX
8 = POWERTAX
9 = PROPERTY TAX FORECAST
10 = POWERTAX FORECAST
11 = O&M BUDGET
12 = CAPITAL BUDGET
13 = DEPRECIATION FORECAST
14 = FORECAST TREND
15 – 100 = RESERVED';
comment on column REG_SOURCE.non_standard
  is '0 = ''NO – STANDARD'' (DEFAULT); ''1'' = ''YES – NON-STANDARD''';
comment on column REG_SOURCE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_SOURCE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_SOURCE
  add constraint PK_REG_SOURCE primary key (REG_SOURCE_ID) USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_ACCT_TYPE
prompt ============================
prompt
create table REG_ACCT_TYPE
(
  reg_acct_type_id NUMBER(22) not null,
  description      VARCHAR2(35) not null,
  long_description VARCHAR2(254),
  user_id          VARCHAR2(18),
  time_stamp       DATE,
  sort_order       NUMBER(22)
)
;
comment on table REG_ACCT_TYPE
  is '( F)  [  ]
THE REG ACCT TYPE TABLE IS A FIXED TABLE LISTING THE MAJOR CLASSIFICATIONS FOR REGULATORY ACCOUNTS, E.G. RATE BASE, OPERATING INCOME, ETC.';
comment on column REG_ACCT_TYPE.reg_acct_type_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT TYPE.';
comment on column REG_ACCT_TYPE.description
  is 'DESCRIPTION OF THE REGULATORY ACCOUNT TYPE.
1 = RATE BASE
2 = UTILITY INVESTMENT NOT IN RATE BASE (I.E. NOT EARNING A RETURN).
3 = OPERATING EXPENSE
4 = NON-UTILITY EXPENSE
5 = OPERATING REVENUE
6 = NON-UTILITY REVENUE
7 = NON-UTILITY INVESTMENT
8 = CAPITAL
9 = OTHER ADJUSTMENTS (E.G. MARGIN FOR WEATHER ADJUSTMENT)
10 = INCOME TAX MEMO';
comment on column REG_ACCT_TYPE.long_description
  is 'LONG DESCRIPTION OF THE REGULATORY ACCOUNT TYPE.';
comment on column REG_ACCT_TYPE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ACCT_TYPE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ACCT_TYPE
  add constraint PK_REG_ACCT_TYPE primary key (REG_ACCT_TYPE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CAPITAL_STRUCT_INDICATOR
prompt ===========================================
prompt
create table REG_CAPITAL_STRUCT_INDICATOR
(
  capital_struct_indicator_id NUMBER(22) not null,
  description                 VARCHAR2(35) not null,
  long_description            VARCHAR2(254),
  user_id                     VARCHAR2(18),
  time_stamp                  DATE,
  interest_sync_default       NUMBER(1)
)
;
alter table REG_CAPITAL_STRUCT_INDICATOR
  add constraint PK_REG_CAP_STRUCT_INDICATOR primary key (CAPITAL_STRUCT_INDICATOR_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CAPITAL_STRUCTURE_CONTROL
prompt ============================================
prompt
create table REG_CAPITAL_STRUCTURE_CONTROL
(
  capital_structure_id          NUMBER(22) not null,
  description                   VARCHAR2(35) not null,
  capital_struct_indicator_id   NUMBER(22) not null,
  user_id                       VARCHAR2(18),
  time_stamp                    DATE,
  related_expense_cap_struct_id NUMBER(22),
  sort_order                    NUMBER(22)
)
;
comment on table REG_CAPITAL_STRUCTURE_CONTROL
  is '(  )  [  ]
THE REG CAPITAL STRUCTURE CONTROL TABLE CONTROLS CAPITAL CLASSIFICATIONS (I.E. REG ACCOUNTS WHOSE ACCOUNT TYPE IS “CAPITAL”) FOR PROCESSING LOGIC AND CAN BE USED TO GENERATE HEADINGS AND SUMMARIES FOR REPORTING. THE ROWS ON REG CAPITAL STRUCTURE CONTROL ARE USER DEFINED.  THEY REFERENCE THE (REG) CAP STRUCTURE INDICATOR, WHICH IS SYSTEM DEFINED.  THESE ROWS ARE REFERENCED FROM THE REG SUB ACCOUNT TYPE.  ALSO SEE REG INTEREST SYNC CONTROL, WHICH SPECIFIES WHICH CAPITAL STRUCTURE ITEMS QUALIFY FOR THE INTEREST SYNC CALC.';
comment on column REG_CAPITAL_STRUCTURE_CONTROL.capital_structure_id
  is 'SYSTEM_ASSIGNED IDENTIFIER OF A REG CAPITAL STRUCTURE CONTROL (CAPTION AND COST RATES)';
comment on column REG_CAPITAL_STRUCTURE_CONTROL.description
  is 'THE CAPTION ON THE FOUR MAIN MONITORING REPORTS (E.G. “LONG TERM DEBT”, “CUSTOMER DEPOSITS”)';
comment on column REG_CAPITAL_STRUCTURE_CONTROL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CAPITAL_STRUCTURE_CONTROL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_CAPITAL_STRUCTURE_CONTROL.related_expense_cap_struct_id
  is 'THIS IS A REFERENCE TO THE ASSOCIATED EXPENSE CAP_STRUCTURE ITEM E.G. LONG TERM DEBT EXPENSE, PREFERRED DIVIDENDS';
create unique index PK_REG_FIN_MONITOR_TYPE on REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID) tablespace PWRPLANT_IDX;
alter table REG_CAPITAL_STRUCTURE_CONTROL
  add constraint PK_REG_CAPITAL_STRUCT_CONTROL primary key (CAPITAL_STRUCTURE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CAPITAL_STRUCTURE_CONTROL
  add constraint R_REG_CAPITAL_STRUCT_CONTROL1 foreign key (RELATED_EXPENSE_CAP_STRUCT_ID)
  references REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID);
alter table REG_CAPITAL_STRUCTURE_CONTROL
  add constraint R_REG_CAPITAL_STRUCT_CONTROL2 foreign key (CAPITAL_STRUCT_INDICATOR_ID)
  references REG_CAPITAL_STRUCT_INDICATOR (CAPITAL_STRUCT_INDICATOR_ID);

prompt
prompt Creating table REG_SUB_ACCT_TYPE
prompt ================================
prompt
create table REG_SUB_ACCT_TYPE
(
  reg_acct_type_id     NUMBER(22) not null,
  sub_acct_type_id     NUMBER(22) not null,
  description          VARCHAR2(35) not null,
  long_description     VARCHAR2(254),
  capital_structure_id NUMBER(22),
  user_id              VARCHAR2(18),
  time_stamp           DATE,
  sort_order           NUMBER(22),
  tax_control_id       NUMBER(22)
)
;
comment on table REG_SUB_ACCT_TYPE
  is '(  )  [  ]
THE REG SUB ACCT TYPE TABLE LISTS THOSE SUB ACCOUNT TYPES WHICH ARE VALID FOR THE ACCOUNT TYPES IN A JURISDICTION, FOR EXAMPLE, THE CAPITAL ACCOUNT TYPE COULD HAVE SUB ACCOUNTS OF PREFERRED, COMMON, DEBT, DEFERREDS, CUSTOMER DEPOSITS, OTHER.  RATE BASE COULD HAVE SUBACCOUNTS OF PLANT, RESERVE, M&S, NUCLEAR FUEL, FUTURE USE, ACCUMULATED DEFERRED TAXES, ETC. THESE SUB ACCOUNT TYPES ARE ALSO USED TO DRIVE THE MONITORING REPORTING.  THE REG SUB ACCT TYPE ASSIGNMENTS ARE CONTROLLED BY THE REG JUR TEMPLATES.';
comment on column REG_SUB_ACCT_TYPE.reg_acct_type_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT TYPE.';
comment on column REG_SUB_ACCT_TYPE.sub_acct_type_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A SUB ACCOUNT WHICH IS VALID FOR THE ABOVE ACCOUNT TYPE AND JURISDICTION.';
comment on column REG_SUB_ACCT_TYPE.description
  is 'SHORT DESCRIPTION OF THE REGULATORY SUB ACCOUNT.';
comment on column REG_SUB_ACCT_TYPE.long_description
  is 'LONG DESCRIPTION OF THE REGULATORY SUB ACCOUNT.
CAP_STRUCTURE _ID     CAPITAL STRUCTURE TYPE, E.G. LONG TERM DEBT BALANCE, LONG TERM INTEREST, ETC. FOR COMPUTING THE OVERALL RETURNS AND RETURN ON EQUITY.';
comment on column REG_SUB_ACCT_TYPE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_SUB_ACCT_TYPE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_SUB_ACCT_TYPE.sort_order
  is 'SORT ORDER FOR PRESENTATION, E.G. ON THE ALLOCATIONS WINDOWS ''1'' SORTS TO THE TOP.';
comment on column REG_SUB_ACCT_TYPE.tax_control_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF TAX USAGE, E.G. A PERM DIFFERENCE, ITC AMORTIZATION, ETC. (FOR TAX MEMO REG ACCT TYPES ONLY)';
alter table REG_SUB_ACCT_TYPE
  add constraint PK_REG_SUB_ACCT_TYPE primary key (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_SUB_ACCT_TYPE
  add constraint R_REG_SUB_ACCT_TYPE1 foreign key (REG_ACCT_TYPE_ID)
  references REG_ACCT_TYPE (REG_ACCT_TYPE_ID);
alter table REG_SUB_ACCT_TYPE
  add constraint R_REG_SUB_ACCT_TYPE2 foreign key (CAPITAL_STRUCTURE_ID)
  references REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID);

prompt
prompt Creating table REG_ACCT_MASTER
prompt ==============================
prompt
create table REG_ACCT_MASTER
(
  reg_acct_id           NUMBER(22) not null,
  description           VARCHAR2(75) not null,
  long_description      VARCHAR2(254),
  reg_source_id         NUMBER(22) not null,
  reg_acct_type_default NUMBER(22) not null,
  sub_acct_type_id      NUMBER(22) not null,
  reg_annualization_id  NUMBER(22) not null,
  acct_good_for         NUMBER(22) not null,
  reg_family_id         NUMBER(22),
  reg_member_id         NUMBER(22),
  reg_component_id      NUMBER(22),
  user_id               VARCHAR2(18),
  time_stamp            DATE,
  ferc_acct             VARCHAR2(10)
)
;
comment on table REG_ACCT_MASTER
  is '(S)  [  ]
THE REG ACCT MASTER TABLE CONTAINS THE BASIC DATA FOR THE REGULATORY ACCOUNTS WHICH ARE THE FUNDAMENTAL BUILDING BLOCK OF DATA WITHIN THE REGULATORY MODULES.  THE PROCESSING AGAINST THE REGULATORY ACCOUNTS IS CONTROLLED BY THIS TABLE, BY THE REG JUR ACCTS DEFAULTS TABLE, AND BY THE REG ACCOUNT CASE TABLE AS WELL AS SOME OF THE ADJUSTMENT AND ALLOCATION TABLES.  DOLLAR DATA IS MAINTAINED THROUGHOUT THE CASES AT THE REGULATORY ACCOUNT LEVEL. (NOTE THAT DEFAULTS ON THIS TABLE FOR REG ACCOUNT TYPE AND SUB ACCT TYPE ARE USED FOR FINANCIAL MONITORING)';
comment on column REG_ACCT_MASTER.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY ACCOUNT.';
comment on column REG_ACCT_MASTER.description
  is 'SHORT DESCRIPTION OF THE REGULATORY ACCOUNT.';
comment on column REG_ACCT_MASTER.long_description
  is 'LONG DESCRIPTION OF THE REGULATORY ACCOUNT.';
comment on column REG_ACCT_MASTER.reg_source_id
  is 'SOURCE OF THE DOLLAR DATA FOR THE ACCOUNT, E.G. DEPR LEDGER, CR, CWIP, POWERTAX, PROVISION, INPUT ETC.';
comment on column REG_ACCT_MASTER.reg_acct_type_default
  is '1 = RATE BASE
2 = UTILITY INVESTMENT NOT IN RATE BASE (I.E. NOT EARNING A RETURN)
3 = OPERATING EXPENSE
4 = NON-UTILITY EXPENSE
5 = OPERATING REVENUE
6 = NON-UTILITY REVENUE
7 = NON-UTILITY INVESTMENT
8 = CAPITAL
9 = OTHER ADJUSTMENT
10 = INCOME TAX MEMO';
comment on column REG_ACCT_MASTER.sub_acct_type_id
  is 'E.G. FOR ACCOUNT TYPE CAPITAL: PREFERRED, COMMON OR DEBT; FOR RATE BASE: WORKING CAPITAL, PP&E';
comment on column REG_ACCT_MASTER.reg_annualization_id
  is '1 = 13 MONTH SIMPLE AVERAGE
2 = 13 MONTH AVERAGE OF AVERAGES
3 = 12 MONTH AVERAGE
4 = 2 POINT ANNUAL AVERAGE
5 = ENDING BALANCE
6 = SUM OF 12 MONTHS
7 = ENDING MONTH * 12
8 = INPUT';
comment on column REG_ACCT_MASTER.acct_good_for
  is '1 = HISTORIC ACTUALS|2 = OTHER HISTORIC|3 = FORECAST|4 = HISTORIC ACTUALS AND FORECAST';
comment on column REG_ACCT_MASTER.reg_family_id
  is 'RELATES THE REGULATORY ACCOUNT TO A FAMILY OF DATA, E.G. A DEPRECIATION GROUP.';
comment on column REG_ACCT_MASTER.reg_member_id
  is 'THE PARTICULAR MEMBER OF THE FAMILY, E.G. THE PARTICULAR DEPRECIATION GROUP.';
comment on column REG_ACCT_MASTER.reg_component_id
  is 'THE PARTICULAR COMPONENT OF A FAMILY, FOR EXAMPLE “ACCUMULATED RESERVE BALANCE.”';
comment on column REG_ACCT_MASTER.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ACCT_MASTER.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_ACCT_MASTER.ferc_acct
  is 'RELATES THE REG ACCOUNT TO A FERC SYSTEM OF ACCOUNT (E.G. 101, ETC. BY ELECTRIC, GAS, COMMON).';
alter table REG_ACCT_MASTER
  add constraint PK_REG_ACCT_MASTER primary key (REG_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ACCT_MASTER
  add constraint R_REG_ACCT_MASTER1 foreign key (REG_SOURCE_ID)
  references REG_SOURCE (REG_SOURCE_ID);
alter table REG_ACCT_MASTER
  add constraint R_REG_ACCT_MASTER2 foreign key (REG_ACCT_TYPE_DEFAULT, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);
alter table REG_ACCT_MASTER
  add constraint R_REG_ACCT_MASTER4 foreign key (REG_ANNUALIZATION_ID)
  references REG_ANNUALIZATION_TYPE (REG_ANNUALIZATION_ID);
alter table REG_ACCT_MASTER
  add constraint R_REG_ACCT_MASTER5 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);
alter table REG_ACCT_MASTER
  add constraint R_REG_ACCT_MASTER6 foreign key (FERC_ACCT)
  references REG_FERC_ACCOUNT (FERC_ACCT);

prompt
prompt Creating table REG_STATUS
prompt =========================
prompt
create table REG_STATUS
(
  reg_status_id NUMBER(22) not null,
  description   VARCHAR2(35) not null,
  user_id       VARCHAR2(18),
  time_stamp    DATE
)
;
comment on table REG_STATUS
  is '(F)  [  ]
THE REGULATORY STATUS TABLE CONTAINS STATUS VALUES FOR REGULATORY CASES.';
comment on column REG_STATUS.reg_status_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT TYPE.';
comment on column REG_STATUS.description
  is 'SHORT DESCRIPTION OF THE REG STATUS
1 = ACTIVE
2 = LOCKED
3 = ARCHIVED';
comment on column REG_STATUS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_STATUS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_STATUS
  add constraint PK_REG_STATUS primary key (REG_STATUS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_HISTORIC_VERSION
prompt ===================================
prompt
create table REG_HISTORIC_VERSION
(
  historic_version_id NUMBER(22) not null,
  long_description    VARCHAR2(254) not null,
  reg_status_id       NUMBER(22),
  inherit_from        NUMBER(22),
  use_in_update       NUMBER(22),
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  start_date          NUMBER(6),
  end_date            NUMBER(6),
  comments            VARCHAR2(1000)
)
;
comment on table REG_HISTORIC_VERSION
  is '(  )  [  ]
THE REG HISTORIC VERSION TABLE ALLOWS FOR MULTIPLE VERSIONS FOR CHANGES IN REGULATORY ACCOUNTS OR FOR THE MAPPING TO THE REGULATORY ACCOUNTS.';
comment on column REG_HISTORIC_VERSION.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE HISTORIC VERSION';
comment on column REG_HISTORIC_VERSION.long_description
  is 'DESCRIPTION OF HISTORIC VERSION';
comment on column REG_HISTORIC_VERSION.reg_status_id
  is '1 = UNLOCKED; 2 = LOCKED (I..E. THE MAPPING CANNONT BE CHANGED, BUT A NEW ACCOUNT CAN BE ADDED); 3 = ARCHIVED';
comment on column REG_HISTORIC_VERSION.inherit_from
  is 'IF COPIED FROM A PREVIOUS VERSION, THIS IS THE PREVIOUS VERSION ID';
comment on column REG_HISTORIC_VERSION.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_HISTORIC_VERSION.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_HISTORIC_VERSION
  add constraint PK_REG_HISTORIC_VERSION primary key (HISTORIC_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_HISTORIC_VERSION
  add constraint R_REG_HISTORIC_VERSION1 foreign key (REG_STATUS_ID)
  references REG_STATUS (REG_STATUS_ID);

prompt
prompt Creating table REG_COMPANY
prompt ==========================
prompt
create table REG_COMPANY
(
  reg_company_id      NUMBER(22) not null,
  description         VARCHAR2(35),
  status_code_id      NUMBER(22),
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  historic_version_id NUMBER(22)
)
;
comment on table REG_COMPANY
  is '(  )  [  ]
THE REG COMPANY TABLE CONTAINS THE LIST OF COMPANIES USED IN THE REGULATORY MODULE.  (IT IS REFERENCED FROM THE POWERPLAN COMPANY SETUP TABLE.)';
comment on column REG_COMPANY.reg_company_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A REGULATORY COMPANY.';
comment on column REG_COMPANY.description
  is 'DESCRIPTION OF THE REGULATORY COMPANY.';
comment on column REG_COMPANY.status_code_id
  is 'ACTIVE = 1; INACTIVE = 2';
comment on column REG_COMPANY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_COMPANY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_COMPANY.historic_version_id
  is 'THE HISTORIC VERSION USED FOR FINANCIAL MONITORING.';
alter table REG_COMPANY
  add constraint PK_REG_COMPANY primary key (REG_COMPANY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_COMPANY
  add constraint R_REG_COMPANY1 foreign key (STATUS_CODE_ID)
  references STATUS_CODE (STATUS_CODE_ID)
  disable;
alter table REG_COMPANY
  add constraint R_REG_COMPANY2 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID)
  disable;

prompt
prompt Creating table REG_JURISDICTION
prompt ===============================
prompt
create table REG_JURISDICTION
(
  reg_jurisdiction_id NUMBER(22) not null,
  description         VARCHAR2(35) not null,
  status_code_id      NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  reg_company_id      NUMBER(22) not null
)
;
comment on table REG_JURISDICTION
  is '(  ) [  ]
THE REG JURISDICTION TABLE LISTS THE REGULATORY JURISDICTIONS THAT ARE USED IN THE REGULATORY MODULES.  NON-UTILITY WILL BE ITS OWN JURISDICTION. JURISDICTIONS CAN BE BROKEN DOWN INTO RECOVERY CLASSES. (E.G. FOR RIDERS, CLAUSES, OTHER RECOVERIES AND BASE RATES).  REG_JURISDICTION_ID OF ''0'' IS TOTAL COMPANY.  AT LEAST ONE ''NON-UTILITY'' JURISDICTION SHOULD BE ESTABLISHED.';
comment on column REG_JURISDICTION.reg_jurisdiction_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY JURISDICTION.';
comment on column REG_JURISDICTION.description
  is 'THE DESCRIPTION OF THE REGULATORY JURISDICTION.';
comment on column REG_JURISDICTION.status_code_id
  is '1 = ACTIVE; 2 = INACTIVE';
comment on column REG_JURISDICTION.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_JURISDICTION.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_JURISDICTION.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER FO A REG_COMPANY';
alter table REG_JURISDICTION
  add constraint PK_REG_JURISDICTION_ID primary key (REG_JURISDICTION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JURISDICTION
  add constraint R_REG_JURISDICTION1 foreign key (STATUS_CODE_ID)
  references STATUS_CODE (STATUS_CODE_ID);
alter table REG_JURISDICTION
  add constraint R_REG_JURISDICTION2 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);

prompt
prompt Creating table REG_JUR_TEMPLATE
prompt ===============================
prompt
create table REG_JUR_TEMPLATE
(
  reg_jur_template_id   NUMBER(22) not null,
  reg_jurisdiction_id   NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  long_description      VARCHAR2(1000) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE,
  cost_of_capital_level NUMBER(2)
)
;
comment on table REG_JUR_TEMPLATE
  is '(  ) [  ]
THE REG JUR TEMPLATE TABLES ALLOWS THE USER TO HAVE DIFFERENT SETS OF DEFAULTS FOR ALLOCATIONS AND ADJUSTMENTS BY JURISDICTION.  FOR EXAMPLE, A SEPARATE SET (I.E. TEMPLATE) COULD BE USED FOR EXTERNAL COMMISSION SURVEILLANCE, INTERNAL MONITORING, OR FULL RATE CASES; OR MULTIPLE TEMPLATES COULD BE USED FOR CASES.  THESE WILL ORDINARILY BE DESCRIBED, FOR EXAMPLE, “OHIO SURVEILLANCE”.';
comment on column REG_JUR_TEMPLATE.reg_jur_template_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A TEMPLATE.';
comment on column REG_JUR_TEMPLATE.reg_jurisdiction_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A JURISDICTION.';
comment on column REG_JUR_TEMPLATE.description
  is 'SHORT DESCRIPTION OF THE TEMPLATE FOR THE WINDOW';
comment on column REG_JUR_TEMPLATE.long_description
  is 'LONG DESCRIPTION OF THE TEMPLATE.';
comment on column REG_JUR_TEMPLATE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_JUR_TEMPLATE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_JUR_TEMPLATE
  add constraint PK_REG_JUR_TEMPLATE primary key (REG_JUR_TEMPLATE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_TEMPLATE
  add constraint R_REG_JUR_TEMPLATE1 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);

prompt
prompt Creating table REG_ACCT_ROLLUP
prompt ==============================
prompt
create table REG_ACCT_ROLLUP
(
  reg_tree_id         NUMBER(22) not null,
  description         VARCHAR2(35) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  reg_jur_template_id NUMBER(22)
)
;
comment on table REG_ACCT_ROLLUP
  is '(  )  [  ]
THE REG ACCT ROLLUP TABLE DESCRIBES ALL THE TREE STRUCTURE ROLL UPS FOR THE REGULATORY ACCOUNTS.  THESE CAN BE BY JURISDICTION.';
comment on column REG_ACCT_ROLLUP.reg_tree_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT ROLLUP TREE.';
comment on column REG_ACCT_ROLLUP.description
  is 'DESCRIPTION OF THE TREE ROLLUP.';
comment on column REG_ACCT_ROLLUP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ACCT_ROLLUP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_ACCT_ROLLUP.reg_jur_template_id
  is 'OPTIONAL. IF POPULATED RESTRICTS REG TREE TO USE IN CASES FOR THE SPECIFIC JURISDICTION AND TEMPLATE.';
alter table REG_ACCT_ROLLUP
  add constraint PK_REG_ACCT_ROLLUP primary key (REG_TREE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ACCT_ROLLUP
  add constraint R_REG_ACCT_ROLLUP1 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);

prompt
prompt Creating table REG_FIN_MONITOR_ALLOC
prompt ====================================
prompt
create table REG_FIN_MONITOR_ALLOC
(
  reg_monitor_alloc_id  NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  long_description      VARCHAR2(254),
  reg_fin_alloc_type_id NUMBER(22) not null,
  priority              NUMBER(22) not null,
  reg_tree_id           NUMBER(22),
  tree_value            VARCHAR2(52),
  update_frequency      NUMBER(22),
  user_id               VARCHAR2(18),
  time_stamp            DATE,
  use_for_column        VARCHAR2(30)
)
;
comment on table REG_FIN_MONITOR_ALLOC
  is '(  )  [  ]
THE REG FIN MONITOR ALLOC TABLE CONTAINS THE ALLOCATORS USED IN THE FINANCIAL (TO JURISDICTIONS AND NON-UTILITY) ALLOCATION';
comment on column REG_FIN_MONITOR_ALLOC.reg_monitor_alloc_id
  is 'SYSTEM_ASSIGNED_IDENTIFIER OF THE ALLOCATOR';
comment on column REG_FIN_MONITOR_ALLOC.description
  is 'DESCRIPTION OF THE ALLOCATOR (E.G. COINCIDENTAL PEAK, CUSTOMERS, ETC.)';
comment on column REG_FIN_MONITOR_ALLOC.long_description
  is 'LONG DESCRIPTION OF THE ALLOCATOR';
comment on column REG_FIN_MONITOR_ALLOC.update_frequency
  is 'OPTIONAL FREQUENCY IN WHICH THE FACTORS SHOULD BE UPDATED IN MONTHS, E.G. ''12'' MEANS ANNUAL, ''3'' MEANS QUARTERLY';
comment on column REG_FIN_MONITOR_ALLOC.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_FIN_MONITOR_ALLOC.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_FIN_MONITOR_ALLOC
  add constraint PK_REG_FIN_MONITOR_ALLOC primary key (REG_MONITOR_ALLOC_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FIN_MONITOR_ALLOC
  add constraint R_REG_FIN_MONITOR_ALLOC2 foreign key (REG_TREE_ID)
  references REG_ACCT_ROLLUP (REG_TREE_ID);

prompt
prompt Creating table REG_ACCT_COMPANY
prompt ===============================
prompt
create table REG_ACCT_COMPANY
(
  reg_acct_id          NUMBER(22) not null,
  reg_company_id       NUMBER(22) not null,
  reg_monitor_alloc_id NUMBER(22),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_ACCT_COMPANY
  is '(  )  [  ]
THE REG ACCOUNT COMPANY TABLE CONTAINS THE VALID REGULATORY ACCOUNTS FOR A COMPANY (CURRENT AND HISTORIC) AND THE CURRENT ALLOCATOR TO JURISDICTION (AND NON UTILITY) FOR FINANCIAL MONITORING.';
comment on column REG_ACCT_COMPANY.reg_acct_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A REGULATORY ACCOUNT.';
comment on column REG_ACCT_COMPANY.reg_company_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A REGULATORY COMPANY.';
comment on column REG_ACCT_COMPANY.reg_monitor_alloc_id
  is 'IDENTIFIER OF ALLOCATOR FOR FINANCIAL MONITORING.';
comment on column REG_ACCT_COMPANY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ACCT_COMPANY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ACCT_COMPANY
  add constraint PK_REG_ACCT_COMPANY primary key (REG_ACCT_ID, REG_COMPANY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ACCT_COMPANY
  add constraint R_REG_ACCT_COMPANY1 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_ACCT_COMPANY
  add constraint R_REG_ACCT_COMPANY2 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_ACCT_COMPANY
  add constraint R_REG_ACCT_COMPANY3 foreign key (REG_MONITOR_ALLOC_ID)
  references REG_FIN_MONITOR_ALLOC (REG_MONITOR_ALLOC_ID);

prompt
prompt Creating table REG_ACCT_ROLLUP_LEVELS
prompt =====================================
prompt
create table REG_ACCT_ROLLUP_LEVELS
(
  reg_tree_id       NUMBER(22) not null,
  level_id          NUMBER(22) not null,
  level_description VARCHAR2(50),
  level_type        NUMBER(22),
  picture_index     NUMBER(22),
  user_id           VARCHAR2(18),
  time_stamp        DATE
)
;
comment on table REG_ACCT_ROLLUP_LEVELS
  is '(  )  [  ]
THE REG ACCOUNT ROLLUP LEVELS TABLE CONTAINS THE REGULATORY ACCOUNT TREE HIERARCHIES.';
comment on column REG_ACCT_ROLLUP_LEVELS.reg_tree_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A REG ACCOUNT ROLLUP.';
comment on column REG_ACCT_ROLLUP_LEVELS.level_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A ROLLUP LEVEL';
comment on column REG_ACCT_ROLLUP_LEVELS.level_description
  is 'DESCRIPTION OF THE LEVEL';
comment on column REG_ACCT_ROLLUP_LEVELS.level_type
  is 'INDICATOR WHETHER REG ACCT TYPE, SUB ACCT TYPE, OR REG ACCT (OTHERWISE NO INDICATOR)';
comment on column REG_ACCT_ROLLUP_LEVELS.picture_index
  is 'PICTURE ICON ASSIGNED (IS DEFAULTED IF NOT ASSIGNED)';
comment on column REG_ACCT_ROLLUP_LEVELS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ACCT_ROLLUP_LEVELS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ACCT_ROLLUP_LEVELS
  add constraint PK_REG_ACCT_ROLLUP_LEVELS primary key (REG_TREE_ID, LEVEL_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ACCT_ROLLUP_LEVELS
  add constraint R_REG_ACCT_ROLLUP_LEVELS1 foreign key (REG_TREE_ID)
  references REG_ACCT_ROLLUP (REG_TREE_ID);

prompt
prompt Creating table REG_ACCT_ROLLUP_VALUES
prompt =====================================
prompt
create table REG_ACCT_ROLLUP_VALUES
(
  reg_tree_id      NUMBER(22) not null,
  child_value      VARCHAR2(75) not null,
  parent_value     VARCHAR2(75) not null,
  reg_acct_id      NUMBER(22),
  level_id         NUMBER(22),
  reg_acct_type_id NUMBER(22),
  sub_acct_type_id NUMBER(22),
  user_id          VARCHAR2(18),
  time_stamp       DATE
)
;
comment on table REG_ACCT_ROLLUP_VALUES
  is '(  )  [  ]
THE REG ACCT ROLLUP VALUES TABLE.  THE LEVELS MUST BE DENSE TO FACILITATE EASY “LEVEL” REPORTING.';
comment on column REG_ACCT_ROLLUP_VALUES.reg_tree_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A REG ACCOUNT ROLLUP.';
comment on column REG_ACCT_ROLLUP_VALUES.child_value
  is 'VALUE (OF CHILD) ON THE TREE.';
comment on column REG_ACCT_ROLLUP_VALUES.parent_value
  is 'VALUE OF PARENT THAT CHILD ROLLS UP TO.';
comment on column REG_ACCT_ROLLUP_VALUES.reg_acct_id
  is 'POPULATED WITH REG_ACCT_ID FOR THE MOST DETAILED LEVELS';
comment on column REG_ACCT_ROLLUP_VALUES.level_id
  is 'LEVEL NUMBER INTEGER; ''1'' IS HIGHEST LEVEL, ''N'' IS MOST DETAILED.';
comment on column REG_ACCT_ROLLUP_VALUES.reg_acct_type_id
  is 'USER ASSIGNED DESCRIPTION TO LEVELS';
comment on column REG_ACCT_ROLLUP_VALUES.sub_acct_type_id
  is 'CROSS REFERENCE TO TO REG_ACCT_TYPE_ID OR SUB_ACCT_TYPE_ID IF APPLICABLE.';
comment on column REG_ACCT_ROLLUP_VALUES.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ACCT_ROLLUP_VALUES.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ACCT_ROLLUP_VALUES
  add constraint PK_REG_ACCT_ROLLUP_VALUES primary key (REG_TREE_ID, CHILD_VALUE)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ACCT_ROLLUP_VALUES
  add constraint R_REG_ACCT_ROLLUP_VALUES1 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_ACCT_ROLLUP_VALUES
  add constraint R_REG_ACCT_ROLLUP_VALUES2 foreign key (REG_TREE_ID)
  references REG_ACCT_ROLLUP (REG_TREE_ID);
alter table REG_ACCT_ROLLUP_VALUES
  add constraint R_REG_ACCT_ROLLUP_VALUES3 foreign key (REG_TREE_ID, LEVEL_ID)
  references REG_ACCT_ROLLUP_LEVELS (REG_TREE_ID, LEVEL_ID);

prompt
prompt Creating table REG_ADJUSTMENT_DISPOSITION
prompt =========================================
prompt
create table REG_ADJUSTMENT_DISPOSITION
(
  disposition_id NUMBER(22) not null,
  description    VARCHAR2(35) not null,
  user_id        VARCHAR2(18),
  time_stamp     DATE
)
;
comment on table REG_ADJUSTMENT_DISPOSITION
  is '(  )  [  ]
THE REG ADJUSTMENT DISPOSITION TABLE CONTAINS DISPOSITION LABELS THAT CAN BE USED TO DOCUMENT ADJUSTMENTS.  DISPOSITION CODES ARE ''ACCEPTED'', ''COMMISSION MODIFIED'', ''DENIED'', ''NOT FILED'', OR ''BLACK BOX SETTLEMENT''.  THESE CAN BE MODIFIED BY THE USER.';
comment on column REG_ADJUSTMENT_DISPOSITION.disposition_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A DISPOSITION CODE';
comment on column REG_ADJUSTMENT_DISPOSITION.description
  is 'DESCRIPTION, E.G. ''ACCEPTED''';
comment on column REG_ADJUSTMENT_DISPOSITION.user_id
  is 'STANDARD SYSTEM ASSIGNED USER_ID USED FOR AUDIT PURPOSES';
comment on column REG_ADJUSTMENT_DISPOSITION.time_stamp
  is 'STANDARD SYSTEM ASSIGNED TIME_STAMP USED FOR AUDIT PURPOSES';
alter table REG_ADJUSTMENT_DISPOSITION
  add constraint PK_REG_ADJUSTMENT_DISPOSITION primary key (DISPOSITION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_ADJUSTMENT_STATUS
prompt ====================================
prompt
create table REG_ADJUSTMENT_STATUS
(
  adj_status_id      NUMBER(22) not null,
  description        VARCHAR2(35) not null,
  window_description VARCHAR2(15),
  window_icon        VARCHAR2(2000),
  user_id            VARCHAR2(18),
  time_stamp         DATE
)
;
comment on table REG_ADJUSTMENT_STATUS
  is '(  )  [  ]
USER CAN ASSIGN STATUS TO ADJUSTMENTS FOR EXAMPLE:  ''NEEDED'', ''DRAFT'', ''READY FOR REVIEW'', ''COMPLETE''';
comment on column REG_ADJUSTMENT_STATUS.adj_status_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF AN ADJUSTMENT STATUS';
comment on column REG_ADJUSTMENT_STATUS.description
  is 'DESCRIPTION OF THE STATUS
WINDOW DESCRIPTION     SHORT DESCRIPTION OF THE STATUS FOR WINDOWS REPORT';
comment on column REG_ADJUSTMENT_STATUS.window_icon
  is 'WINDOW ICON ASSOCIATED WITH THE STATUS';
comment on column REG_ADJUSTMENT_STATUS.user_id
  is 'STANDARD SYSTEM ASSIGNED USER_ID USED FOR AUDIT PURPOSES';
comment on column REG_ADJUSTMENT_STATUS.time_stamp
  is 'STANDARD SYSTEM ASSIGNED TIME_STAMP USED FOR AUDIT PURPOSES';
alter table REG_ADJUSTMENT_STATUS
  add constraint PK_REG_ADJUSTMENT_STATUS primary key (ADJ_STATUS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_COLUMN
prompt =========================
prompt
create table REG_COLUMN
(
  column_summary_id NUMBER(22) not null,
  column_name       VARCHAR2(30) not null,
  description       VARCHAR2(35) not null,
  report_dw         VARCHAR2(40),
  user_id           VARCHAR2(18),
  time_stamp        DATE
)
;
comment on table REG_COLUMN
  is '(  ) [  ]
THE REG COLUMN TABLE CONTAINS INFORMATION ABOUT ADJUSTING COLUMNS AVAILABLE ON SCHEDULES.';
comment on column REG_COLUMN.column_summary_id
  is 'THE SYSTEM ASSIGNED IDENTIFIER OF THE COLUMN.';
comment on column REG_COLUMN.column_name
  is 'NAME OF THE COLUMN';
comment on column REG_COLUMN.description
  is 'DESCRIPTION OF THE COLUMN FOR THE SCHEDULE.';
comment on column REG_COLUMN.report_dw
  is 'REPORT/WINDOW WHERE THE COLUMN IS USED';
comment on column REG_COLUMN.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_COLUMN.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_COLUMN
  add constraint PK_REG_COLUMN primary key (COLUMN_SUMMARY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_RECOVERY_CLASS
prompt =================================
prompt
create table REG_RECOVERY_CLASS
(
  recovery_class_id   NUMBER(22) not null,
  reg_jurisdiction_id NUMBER(22) not null,
  description         VARCHAR2(35) not null,
  jurisdiction_type   NUMBER(22) not null,
  long_description    VARCHAR2(254),
  reg_status_id       NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_RECOVERY_CLASS
  is '(  ) [  ]
THE REG RECOVERY CLASS TABLE LISTS THE RECOVERY CLASSES (I.E. SUB-JURISDICTIONS, E.G. RIDERS OR CLAUSES COLLECTING REVENUES OUTSIDE OF BASE RATES) USED IN THE REGULATORY MODULES.  THE GENERAL BASE RATES IS A RECOVERY CLASS.  NON-UTILITY WILL BE ITS OWN JURISDICTION.';
comment on column REG_RECOVERY_CLASS.recovery_class_id
  is 'EITHER THE BASE OR A PARTICULAR RIDER.';
comment on column REG_RECOVERY_CLASS.reg_jurisdiction_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY JURISDICTION.';
comment on column REG_RECOVERY_CLASS.description
  is 'THE DESCRIPTION OF THE REGULATORY RECOVERY CLASS';
comment on column REG_RECOVERY_CLASS.jurisdiction_type
  is 'THE GENERAL RATE CASE (0), OR SUB-JURISDICTION/RIDER (1).  ONLY ONE GENERAL RATE CASE IS ALLOWED PER JURISIDICTION.';
comment on column REG_RECOVERY_CLASS.long_description
  is 'THE LONG DESCRIPTION OF THE REGULATORY RECOVERY CLASS.';
comment on column REG_RECOVERY_CLASS.reg_status_id
  is '1 = ACTIVE; 2 = INACTIVE';
comment on column REG_RECOVERY_CLASS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_RECOVERY_CLASS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_RECOVERY_CLASS
  add constraint PK_REG_RECOVERY_CLASS primary key (RECOVERY_CLASS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_RECOVERY_CLASS
  add constraint R_REG_RECOVERY_CLASS1 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);
alter table REG_RECOVERY_CLASS
  add constraint R_REG_RECOVERY_CLASS2 foreign key (REG_STATUS_ID)
  references REG_STATUS (REG_STATUS_ID);

prompt
prompt Creating table REG_ADJUST_JUR_DEFAULT
prompt =====================================
prompt
create table REG_ADJUST_JUR_DEFAULT
(
  adjustment_id          NUMBER(22) not null,
  reg_jur_template_id    NUMBER(22) not null,
  recovery_class_id      NUMBER(22),
  description            VARCHAR2(35) not null,
  adjust_type_id         NUMBER(22) not null,
  historic               NUMBER(22) not null,
  forecast               NUMBER(22) not null,
  long_description       VARCHAR2(2000),
  column_summary_id      NUMBER(22),
  status_code_id         NUMBER(22),
  order_of_process       NUMBER(22),
  doc_link               VARCHAR2(2000),
  set_of_books_id        NUMBER(22),
  calc_dw                VARCHAR2(40),
  adjustment_timing      NUMBER(22),
  adjustment_periodicity NUMBER(22),
  user_id                VARCHAR2(18),
  time_stamp             DATE
)
;
comment on table REG_ADJUST_JUR_DEFAULT
  is '(  )  [  ]
THE REG ADJUST JUR DEFAULT TABLE SPECIFIES THOSE ADJUSTMENTS TO THE ACTUALS THAT ARE NEEDED BY JURISDICTION TO DETERMINE REGULATORY RESULTS, FOR EXAMPLE, THESE MAY INCLUDE ADJUSTMENTS FOR WORKING CAPITAL, KNOWN AND MEASURABLE CHANGES OR INTEREST SYNCHRONIZATION. MULTIPLE TEMPLATES ARE ALLOWED FOR EACH JURISDICTION.';
comment on column REG_ADJUST_JUR_DEFAULT.adjustment_id
  is 'A SEQUENTIAL NUMBER ASSIGNED TO THE ADJUSTMENT.';
comment on column REG_ADJUST_JUR_DEFAULT.reg_jur_template_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A JURISDICTIONAL TEMPLATE';
comment on column REG_ADJUST_JUR_DEFAULT.recovery_class_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE RECOVERY CLASS, I.E. JURISDICTION RECOVERY CLASS.';
comment on column REG_ADJUST_JUR_DEFAULT.description
  is 'SHORT DESCRIPTION OF THE ADJUSTMENT.';
comment on column REG_ADJUST_JUR_DEFAULT.adjust_type_id
  is 'TYPE OF ADJUSTMENT, E.G. INPUT PERCENTAGE, INPUT INCREMENT';
comment on column REG_ADJUST_JUR_DEFAULT.historic
  is 'IS THE APPROPRIATE FOR HISTORIC TEST YEARS CASES. 0 = NO; 1 = YES';
comment on column REG_ADJUST_JUR_DEFAULT.forecast
  is 'IS IT APPROPRIATE FOR FORECAST TEST YEAR CASES. 0 = NO; 1 = YES
LONG DESCRIPTION     LONG DESCRIPTION FOR THE ADJUSTMENT.';
comment on column REG_ADJUST_JUR_DEFAULT.column_summary_id
  is 'REFERENCES THE COLUMN ON THE SCHEDULE TO WHICH THE ADJUSTMENT APPLIES.';
comment on column REG_ADJUST_JUR_DEFAULT.status_code_id
  is '1 = ACTIVE; 2 = INACTIVE';
comment on column REG_ADJUST_JUR_DEFAULT.order_of_process
  is 'THE ORDER IN WHICH THE ADJUSTMENTS SHOULD BE MADE (STARTING WITH THE LOWEST NUMBER).';
comment on column REG_ADJUST_JUR_DEFAULT.doc_link
  is 'LINK TO THE SUPPORTING DOCUMENTATION.';
comment on column REG_ADJUST_JUR_DEFAULT.set_of_books_id
  is 'THIS IS NEEDED IF THE ADJUSTMENT TYPE IS DEPR SET OF BOOKS SUBSTITUTION.';
comment on column REG_ADJUST_JUR_DEFAULT.calc_dw
  is 'NULL UNLESS ''CALCULATION'' TYPE IS USED.  REFERENCES A DATAWINDOW.';
comment on column REG_ADJUST_JUR_DEFAULT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ADJUST_JUR_DEFAULT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ADJUST_JUR_DEFAULT
  add constraint PK_REG_ADJUST_JUR_DEFAULT primary key (ADJUSTMENT_ID, REG_JUR_TEMPLATE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ADJUST_JUR_DEFAULT
  add constraint R_REG_ADJUST_JUR_DEFAULT1 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);
alter table REG_ADJUST_JUR_DEFAULT
  add constraint R_REG_ADJUST_JUR_DEFAULT2 foreign key (RECOVERY_CLASS_ID)
  references REG_RECOVERY_CLASS (RECOVERY_CLASS_ID);
alter table REG_ADJUST_JUR_DEFAULT
  add constraint R_REG_ADJUST_JUR_DEFAULT3 foreign key (COLUMN_SUMMARY_ID)
  references REG_COLUMN (COLUMN_SUMMARY_ID);
alter table REG_ADJUST_JUR_DEFAULT
  add constraint R_REG_ADJUST_JUR_DEFAULT4 foreign key (STATUS_CODE_ID)
  references STATUS_CODE (STATUS_CODE_ID);
alter table REG_ADJUST_JUR_DEFAULT
  add constraint R_REG_ADJUST_JUR_DEFAULT5 foreign key (SET_OF_BOOKS_ID)
  references SET_OF_BOOKS (SET_OF_BOOKS_ID);

prompt
prompt Creating table REG_ALLOC_CATEGORY
prompt =================================
prompt
create table REG_ALLOC_CATEGORY
(
  reg_alloc_category_id NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  long_description      VARCHAR2(254),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_ALLOC_CATEGORY
  is '(  )  [  ]
THE REG ALLOC CATEGORY TABLE DEFINES ALLOCATION CATEGORIES (NOT SPECIFIC)';
comment on column REG_ALLOC_CATEGORY.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION CATEGORY.';
comment on column REG_ALLOC_CATEGORY.description
  is 'DESCRIPTION OF THE REGULATORY ALLOCATION CATEGORY.  E.G., FUNCTIONAL CLASS, SERVICE (GAS VS. ELECTRIC), CLASSIFICATION (ENERGY, DEMAND, CUSTOMER)';
comment on column REG_ALLOC_CATEGORY.long_description
  is 'LONG DESCRIPTION OF THE REGULATORY ALLOCATION CATEGORY';
comment on column REG_ALLOC_CATEGORY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ALLOC_CATEGORY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME STAMP USED FOR AUDIT PURPOSES.';
alter table REG_ALLOC_CATEGORY
  add constraint PK_REG_ALLOC_CATEGORY primary key (REG_ALLOC_CATEGORY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_ALLOC_TARGET
prompt ===============================
prompt
create table REG_ALLOC_TARGET
(
  reg_alloc_target_id NUMBER(22) not null,
  description         VARCHAR2(35) not null,
  long_description    VARCHAR2(254),
  ext_alloc_target    VARCHAR2(35),
  target_summary      VARCHAR2(35),
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  reg_jurisdiction_id NUMBER(22),
  recovery_class_id   NUMBER(22)
)
;
comment on table REG_ALLOC_TARGET
  is '(  )  [  ]
THE REG ALLOC TARGET TABLE DEFINES THE ALLOCATION TARGET LIST, E.G. JURISDICTION: NORTH CAROLINA, SOUTH CAROLINA, FERC; FUNCTIONAL: TRANSMISSION, DISTRIBUTION, GENERATION.';
comment on column REG_ALLOC_TARGET.reg_alloc_target_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION TARGET.';
comment on column REG_ALLOC_TARGET.description
  is 'DESCRIPTION OF THE REGULATORY ALLOCATION TARGET';
comment on column REG_ALLOC_TARGET.long_description
  is 'LONG DESCRIPTION OF THE REGULATORY ALLOCATION TARGET.';
comment on column REG_ALLOC_TARGET.ext_alloc_target
  is 'OPTIONAL EXTERNAL REFERENCE.
TARGET SUMMARY      OPTIONAL SUMMARY COLUMN FOR REPORTING';
comment on column REG_ALLOC_TARGET.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ALLOC_TARGET.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_ALLOC_TARGET.reg_jurisdiction_id
  is 'THE ALLOCATION TARGET CAN BE RESTRICTED TO THIS JURISDICTION.  NULL MEANS TARGET IS AVAILABLE FOR ALL JURISDICTIONS.';
comment on column REG_ALLOC_TARGET.recovery_class_id
  is 'THE RECOVERY CLASS (RIDER, CLAUSE) THAT THE TARGET REPRESENTS.';
alter table REG_ALLOC_TARGET
  add constraint PK_REG_ALLOC_TARGET primary key (REG_ALLOC_TARGET_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ALLOC_TARGET
  add constraint R_REG_ALLOC_TARGET1 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);
alter table REG_ALLOC_TARGET
  add constraint R_REG_ALLOC_TARGET2 foreign key (RECOVERY_CLASS_ID)
  references REG_RECOVERY_CLASS (RECOVERY_CLASS_ID);

prompt
prompt Creating table REG_JUR_ACCT_DEFAULT
prompt ===================================
prompt
create table REG_JUR_ACCT_DEFAULT
(
  reg_jur_template_id  NUMBER(22) not null,
  reg_acct_id          NUMBER(22) not null,
  reg_acct_type_id     NUMBER(22) not null,
  sub_acct_type_id     NUMBER(22) not null,
  reg_annualization_id NUMBER(22) not null,
  relation_id          NUMBER(22),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_JUR_ACCT_DEFAULT
  is '(S)  [  ]
THE REG JUR ACCOUNT DEFAULT TABLE ALLOWS THE USER TO DEFAULT REGULATORY ACCOUNT OPTIONS BY JURISDICTIONAL TEMPLATE.';
comment on column REG_JUR_ACCT_DEFAULT.reg_jur_template_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY JURISDICTIONAL TEMPLATE.';
comment on column REG_JUR_ACCT_DEFAULT.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_JUR_ACCT_DEFAULT.reg_acct_type_id
  is '1 = RATE BASE
2 = UTILITY INVESTMENT NOT IN RATE BASE (I.E. NOT EARNING A RETURN)
3 = OPERATING EXPENSE
4 = NON-UTILITY EXPENSE
5 = OPERATING REVENUE
6 = NON-UTILITY REVENUE
7 = NON-UTILITY INVESTMENT
8 = CAPITAL
9 = OTHER ADJUSTMENT
10 = INCOME TAX MEMO';
comment on column REG_JUR_ACCT_DEFAULT.sub_acct_type_id
  is 'E.G. FOR ACCOUNT TYPE CAPITAL: PREFERRED, COMMON OR DEBT; FOR RATE BASE: WORKING CAPITAL, PP&E';
comment on column REG_JUR_ACCT_DEFAULT.reg_annualization_id
  is '1 = 13 SIMPLE MONTH AVERAGE
2 = 13 MONTH AVERAGE OF AVERAGES
3 = 12 MONTH AVERAGE
4 = 2 POINT ANNUAL AVERAGE
5 = ENDING BALANCE
6 = SUM OF 12 MONTHS
7 = ENDING MONTH * 12
8 = INPUT';
comment on column REG_JUR_ACCT_DEFAULT.relation_id
  is 'THIS REFERENCES ANOTHER REGULATORY ACCOUNT TO WHICH IT IS ASSOCIATED, E.G. A NUMBER OF CURRENT ASSET AND LIABILITY ACCOUNTS COULD POINT TO A LEAD/LAG WORKING CAPITAL ACCOUNT.';
comment on column REG_JUR_ACCT_DEFAULT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_JUR_ACCT_DEFAULT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_JUR_ACCT_DEFAULT
  add constraint PK_REG_JUR_ACCT_DEFAULT primary key (REG_JUR_TEMPLATE_ID, REG_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_ACCT_DEFAULT
  add constraint R_REG_JUR_ACCT_DEFAULT1 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_JUR_ACCT_DEFAULT
  add constraint R_REG_JUR_ACCT_DEFAULT2 foreign key (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);
alter table REG_JUR_ACCT_DEFAULT
  add constraint R_REG_JUR_ACCT_DEFAULT3 foreign key (REG_ACCT_TYPE_ID)
  references REG_ACCT_TYPE (REG_ACCT_TYPE_ID);
alter table REG_JUR_ACCT_DEFAULT
  add constraint R_REG_JUR_ACCT_DEFAULT4 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);
alter table REG_JUR_ACCT_DEFAULT
  add constraint R_REG_JUR_ACCT_DEFAULT5 foreign key (REG_ANNUALIZATION_ID)
  references REG_ANNUALIZATION_TYPE (REG_ANNUALIZATION_ID);

prompt
prompt Creating table REG_ADJUST_JUR_DEFAULT_ACCT
prompt ==========================================
prompt
create table REG_ADJUST_JUR_DEFAULT_ACCT
(
  adjustment_id         NUMBER(22) not null,
  reg_jur_template_id   NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_acct_id           NUMBER(22) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_ADJUST_JUR_DEFAULT_ACCT
  is '(  )  [  ]
THE REG ADJUST JUR DEFAULT ACCOUNT TABLE CONTAINS THE DEFAULT LIST OF ACCOUNTS (AND REGULATORY ALLOCATION ACCOUNTS) ASSOCIATED WITH AN ADJUSTMENT, WHICH IS FOR A PARTICULAR JURISDICTION.';
comment on column REG_ADJUST_JUR_DEFAULT_ACCT.adjustment_id
  is 'SYSTEM  - ASSIGNED IDENTIFIER OF THE ADJUSTMENT';
comment on column REG_ADJUST_JUR_DEFAULT_ACCT.reg_jur_template_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A JURISDICTIONAL TEMPLATE.';
comment on column REG_ADJUST_JUR_DEFAULT_ACCT.reg_alloc_target_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A REGULATORY TARGET';
comment on column REG_ADJUST_JUR_DEFAULT_ACCT.reg_alloc_category_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A ALLOCATION CATEGORY';
comment on column REG_ADJUST_JUR_DEFAULT_ACCT.reg_acct_id
  is 'NOT NULLL SYSTEM – ASSIGNED IDENTIFIER OF A REGULATORY ACCOUNT';
comment on column REG_ADJUST_JUR_DEFAULT_ACCT.user_id
  is 'VARCHAR2 (18)   STANDARD SYSTEM – ASSIGNED USER ID FOR AUDIT PURPOSES';
comment on column REG_ADJUST_JUR_DEFAULT_ACCT.time_stamp
  is 'STANDARD SYSTEM – ASSIGNED TIME STAMP FOR AUDIT PURPOSES.';
alter table REG_ADJUST_JUR_DEFAULT_ACCT
  add constraint PK_REG_ADJUST_JUR_DEFAULT_ACCT primary key (ADJUSTMENT_ID, REG_JUR_TEMPLATE_ID, REG_ALLOC_TARGET_ID, REG_ALLOC_CATEGORY_ID, REG_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ADJUST_JUR_DEFAULT_ACCT
  add constraint R_REG_ADJUST_JUR_DEFAULT_ACCT1 foreign key (ADJUSTMENT_ID, REG_JUR_TEMPLATE_ID)
  references REG_ADJUST_JUR_DEFAULT (ADJUSTMENT_ID, REG_JUR_TEMPLATE_ID);
alter table REG_ADJUST_JUR_DEFAULT_ACCT
  add constraint R_REG_ADJUST_JUR_DEFAULT_ACCT2 foreign key (REG_JUR_TEMPLATE_ID, REG_ACCT_ID)
  references REG_JUR_ACCT_DEFAULT (REG_JUR_TEMPLATE_ID, REG_ACCT_ID);
alter table REG_ADJUST_JUR_DEFAULT_ACCT
  add constraint R_REG_ADJUST_JUR_DEFAULT_ACCT3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);
alter table REG_ADJUST_JUR_DEFAULT_ACCT
  add constraint R_REG_ADJUST_JUR_DEFAULT_ACCT4 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_ADJUST_JUR_DEFAULT_ACCT
  add constraint R_REG_ADJUST_JUR_DEFAULT_ACCT5 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);

prompt
prompt Creating table REG_ADJUST_TYPES
prompt ===============================
prompt
create table REG_ADJUST_TYPES
(
  adjustment_type_id NUMBER(22) not null,
  description        VARCHAR2(35) not null,
  user_id            VARCHAR2(18),
  time_stamp         DATE
)
;
comment on table REG_ADJUST_TYPES
  is '( F)  [  ]
THE REG ADJUST TYPES TABLE IS A FIXED TABLE IDENTIFYING THE TYPE OF ADJUSTMENT.';
comment on column REG_ADJUST_TYPES.adjustment_type_id
  is 'THE SYSTEM ASSIGNED IDENTIFIER OF THE ADJUSTMENT TYPE.';
comment on column REG_ADJUST_TYPES.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ADJUST_TYPES.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ADJUST_TYPES
  add constraint PK_REG_ADJUST_TYPES primary key (ADJUSTMENT_TYPE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_ALLOCATOR
prompt ============================
prompt
create table REG_ALLOCATOR
(
  reg_allocator_id     NUMBER(22) not null,
  description          VARCHAR2(35) not null,
  alloc_factor_code    VARCHAR2(40),
  factor_type          NUMBER(22),
  related_reg_alloc_id NUMBER(22),
  update_interval      NUMBER(22),
  capital_structure_id NUMBER(22,0),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_ALLOCATOR
  is '(  )  [  ]
THE REG ALLOCATOR TABLE DEFINES THE LIST OF ALLOCATORS, E.G. FOR JURISDICTIONAL AND CUSTOMER CLASS SEPARATIONS.';
comment on column REG_ALLOCATOR.reg_allocator_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATOR.  AN ALLOCATOR IS TIED DIRECTLY TO A SET OF TARGETS.  THUS AN ALLOCATOR THAT IS BASED ON 12-COIND-PEAK FOR JURISDICTIONAL IS A SEPARATE ALLOCATOR FROM THE ONE THAT IS BASED ON 12-COIND-PEAK FOR CUSTOMER CLASS ALLOCATION.  HOWEVER, THE ''ALLOCFACTOR CODE'' CAN BE THE SAME, AND THEY CAN BE RELATED SO THAT THE STATISTICAL AMOUNTS ARE RECONCILED.';
comment on column REG_ALLOCATOR.description
  is 'DESCRIPTION OF THE REGULATORY ALLOCATOR.';
comment on column REG_ALLOCATOR.alloc_factor_code
  is 'OPTIONAL EXTERNAL REFERENCE OR CODE FOR EXHIBIT REFERENCE AND SORTING';
comment on column REG_ALLOCATOR.factor_type
  is '1 = INPUT; 2 = IMPORTED, 3 = DYNAMIC/INTERNAL CALCULATED, 4 = DYNAMIC FROM EXTERNAL, 5 = FROM CUSTOMER TABLES, 6 = USE CALCULATED TAX EFFECT TO PRODUCE FACTORS';
comment on column REG_ALLOCATOR.related_reg_alloc_id
  is 'THIS IS ANOTHER REG ALLOCATOR WHOSE STATISTICAL TOTAL FOR THE SAME PERIOD, REMAINING  PREVIOUS LEVEL SHOULD EQUAL THIS REG ALLOCATOR';
comment on column REG_ALLOCATOR.update_interval
  is '1 = ANNUAL; 2 = QUARTERLY, 3 = MONTHLY, 4 = AS NECESSARY, 5 = INACTIVE.  UPDATE INTERNAL DOES NOT APPLY TO DYNAMIC ALLOCATORS.  DEFAULT IS ''AS NECESSARY''';
comment on column REG_ALLOCATOR.capital_structure_id is 'The system-assigned identifier of the capital structure item which is only populated for the system calculated dynamic allocator for cost of capital calculations.';
comment on column REG_ALLOCATOR.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ALLOCATOR.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ALLOCATOR
  add constraint PK_REG_ALLOCATOR primary key (REG_ALLOCATOR_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

alter table reg_allocator
   add constraint r_reg_allocator1 foreign key (capital_structure_id)
  references reg_capital_structure_control (capital_structure_id);


prompt
prompt Creating table REG_ALLOCATOR_DYN
prompt ================================
prompt
create table REG_ALLOCATOR_DYN
(
  reg_allocator_id    NUMBER(22) not null,
  row_number          NUMBER(22) not null,
  reg_acct_type_id    NUMBER(22),
  sub_acct_type_id    NUMBER(22),
  reg_acct_id         NUMBER(22),
  reg_alloc_target_id NUMBER(22),
  sign                VARCHAR2(1),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_ALLOCATOR_DYN
  is '(  )  [  ]
THE REG ALLOCATOR DYNAMIC TABLE SAVES DYNAMICALLY CALCULATED ALLOCATORS INVOLVING AN ARITHEMETIC COMBINATION OF ONE OR MORE PREVIOUS ALLOCATIONS.';
comment on column REG_ALLOCATOR_DYN.reg_allocator_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF AN ALLOCATOR THAT IS DYNAMICALLY CALCULATED';
comment on column REG_ALLOCATOR_DYN.row_number
  is 'SYSTEM ASSIGNED IDENTIFIER FOR EACH UNIQUE SELCTION MADE BY THE USER TO DEFINE THE DYNAMIC ALLOCATOR.';
comment on column REG_ALLOCATOR_DYN.reg_acct_type_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR A REG ACCOUNT TYPE, ALLOWING THE USER TO DESIGNATE THE ENTIRE REG ACCOUNT TYPE, E.G. ''RATE BASE'' IN THE CALCULATION OF THE ALLOCATOR.';
comment on column REG_ALLOCATOR_DYN.sub_acct_type_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR A SUB ACCOUNT TYPE, ALLOWING THE USER TO DESIGNATE THE ENTIRE SUB ACCOUNT TYPE IN THE CALCULATION OF THE ALLOCATOR.';
comment on column REG_ALLOCATOR_DYN.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR A REG ACCOUNT';
comment on column REG_ALLOCATOR_DYN.reg_alloc_target_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR A REG ALLOCATION TARGET';
comment on column REG_ALLOCATOR_DYN.sign
  is 'THE SIGN (''+'' OR ''-'') TO BE APPLIED TO THE CURRENT ROW TO BUILD THE DEFINITION OF THE DYNAMIC ALLOCATOR.';
comment on column REG_ALLOCATOR_DYN.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ALLOCATOR_DYN.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ALLOCATOR_DYN
  add constraint PK_REG_ALLOCATOR_DYN primary key (REG_ALLOCATOR_ID, ROW_NUMBER)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ALLOCATOR_DYN
  add constraint R_REG_ALLOCATOR_DYN1 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);
alter table REG_ALLOCATOR_DYN
  add constraint R_REG_ALLOCATOR_DYN2 foreign key (REG_ACCT_TYPE_ID)
  references REG_ACCT_TYPE (REG_ACCT_TYPE_ID);
alter table REG_ALLOCATOR_DYN
  add constraint R_REG_ALLOCATOR_DYN3 foreign key (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);
alter table REG_ALLOCATOR_DYN
  add constraint R_REG_ALLOCATOR_DYN4 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_ALLOCATOR_DYN
  add constraint R_REG_ALLOCATOR_DYN5 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_ALLOCATOR_DYN_EXT
prompt ====================================
prompt
create table REG_ALLOCATOR_DYN_EXT
(
  reg_allocator_id NUMBER(22) not null,
  computation      VARCHAR2(2000) not null
)
;
comment on table REG_ALLOCATOR_DYN_EXT
  is '(  )  [  ]
THE REG ALLOCATOR DYN EXT TABLE KEEPS TRACK OF FORMULAS USE TO CALCULATE ALLOCATORS FROM IMPORTED STATISTICS';
comment on column REG_ALLOCATOR_DYN_EXT.reg_allocator_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF THE ALLOCATOR BEING CALCULATED';
comment on column REG_ALLOCATOR_DYN_EXT.computation
  is 'ALGEBRAIC EXPRESSION NEED TO CALCULATE THE ALLOCATOR (THIS INVOLVES OTHER REG ALLOCATORS).';
alter table REG_ALLOCATOR_DYN_EXT
  add constraint PK_REG_ALLOCATOR_DYN_EXT primary key (REG_ALLOCATOR_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ALLOCATOR_DYN_EXT
  add constraint R_REG_ALLOCATOR_DYN_EXT1 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);

prompt
prompt Creating table REG_CASE
prompt =======================
prompt
create table REG_CASE
(
  reg_case_id            NUMBER(22) not null,
  case_name              VARCHAR2(35) not null,
  long_description       VARCHAR2(254),
  reg_jurisdiction_id    NUMBER(22) not null,
  reg_case_type          NUMBER(22) not null,
  reg_status_id          NUMBER(22),
  last_update            DATE,
  case_manager_user_id   VARCHAR2(18),
  parent_case_id         NUMBER(22),
  notes                  VARCHAR2(2000),
  user_id                VARCHAR2(18),
  time_stamp             DATE,
  jurisdictional_cos_yn  NUMBER(22),
  customer_cos_yn        NUMBER(22),
  period_start           NUMBER(6) default 0 not null,
  period_end             NUMBER(6) default 0 not null,
  reg_jur_template_id    NUMBER(22) not null,
  cost_of_capital_level  NUMBER(2),
  theoretical_cap_struct NUMBER(1)
)
;
comment on table REG_CASE
  is '(  ) [  ]
THE REG CASE TABLE MAINTAINS THE LIST OF ALL ACTUAL, JUR MONITORING, AND FORECAST REGULATORY CASES AND RELATED INFORMATION.';
comment on column REG_CASE.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_CASE.case_name
  is 'SHORT DESCRIPTION FOR REPORTING.';
comment on column REG_CASE.long_description
  is 'LONG DESCRIPTION OF THE CASE.';
comment on column REG_CASE.reg_jurisdiction_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY JURISDICTION OR SUB-JURISDICTION E.G. REVENUE RIDER.';
comment on column REG_CASE.reg_case_type
  is '1 = MONITORING (AUTOMATIC UPDATE OF TIME); 2 = NORMAL CASE';
comment on column REG_CASE.reg_status_id
  is '1 = ACTIVE; 2 = LOCKED; 3 = ARCHIVED';
comment on column REG_CASE.last_update
  is 'THE LAST TIME A VARIABLE IN THE CASE WAS UPDATED.';
comment on column REG_CASE.case_manager_user_id
  is 'THE MANAGER OF THE CASE.';
comment on column REG_CASE.notes
  is 'FOR MISCELLANEOUS NOTES.';
comment on column REG_CASE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_CASE.jurisdictional_cos_yn
  is 'YES/NO INDICATOR.  1 = YES – COST OF SERVICE IF RESULTS AND ALLOCATIONS ARE PERFORMED AT THE JURISDICTIONAL LEVEL,  0 = NO.';
comment on column REG_CASE.customer_cos_yn
  is 'YES/NO INDICATOR. 1 = YES – COST OF SERVICE RESULTS AND ALLOCATIONS ARE PERFORMED AT THE CUSTOMER LEVEL, 0 = NO.';
comment on column REG_CASE.period_start
  is 'STARTING MONTH OF THE TEST YEAR (INCLUSIVE) – YYYYMM FORMAT.';
comment on column REG_CASE.period_end
  is 'ENDING MONTH OF THE TEST YEAR (INCLUSIVE).  FOR ACTUAL CASES ONLY ONE YEAR IS ALLOWED.  FOR FORECAST (OR COMBINATION OF ACTUAL AND FORECAST) CASES, MULTIPLE YEARS ARE ALLOWED – YYYYMM FORMAT.';
comment on column REG_CASE.reg_jur_template_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY JURISDICTION TEMPLATE USED TO INITIALIZE THE CASE DATA.';
alter table REG_CASE
  add constraint PK_REG_CASE primary key (REG_CASE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE
  add constraint R_REG_CASE2 foreign key (PARENT_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE
  add constraint R_REG_CASE3 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);
alter table REG_CASE
  add constraint R_REG_CASE4 foreign key (REG_STATUS_ID)
  references REG_STATUS (REG_STATUS_ID);
alter table REG_CASE
  add constraint R_REG_CASE5 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);

prompt
prompt Creating table REG_CASE_ACCT
prompt ============================
prompt
create table REG_CASE_ACCT
(
  reg_case_id          NUMBER(22) not null,
  reg_acct_id          NUMBER(22) not null,
  reg_acct_type_id     NUMBER(22) not null,
  sub_acct_type_id     NUMBER(22) not null,
  status_code_id       NUMBER(22) not null,
  reg_annualization_id NUMBER(22) not null,
  note                 VARCHAR2(2000),
  relation_id          NUMBER(22),
  preparer             VARCHAR2(18),
  witness              VARCHAR2(18),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_CASE_ACCT
  is '(O)  [  ]
THE REG ACCT CASE TABLE RECORDS THE ACTUAL OPTIONS USED IN THE CREATION OF THE CASE.  THESE OPTIONS CAN BE DEFAULTED FROM THE REG JURISDICTIONAL DEFAULTS TABLE OR THE REG ACCOUNT MASTER TABLE.';
comment on column REG_CASE_ACCT.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_CASE_ACCT.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_CASE_ACCT.reg_acct_type_id
  is 'THE ACTUAL ACCOUNT TYPE GOING INTO THIS CASE.
1 = RATE BASE
2 = UTILITY INVESTMENT NOT IN RATE BASE (I.E. NOT EARNING A RETURN)
3 = OPERATING EXPENSE
4 = NON-UTILITY EXPENSE
5 = OPERATING REVENUE
6 = NON-UTILITY REVENUE
7 = NON-UTILITY INVESTMENT
8 = CAPITAL
9 = OTHER ADJUSTMENT
10 = INCOME TAX MEMO';
comment on column REG_CASE_ACCT.sub_acct_type_id
  is 'E.G. FOR ACCOUNT TYPE CAPITAL: PREFERRED, COMMON OR DEBT; FOR RATE BASE: WORKING CAPITAL, PP&E, ETC.';
comment on column REG_CASE_ACCT.status_code_id
  is 'ACTIVE/INACTIVE INDICATOR.  ACTIVE = 1: THIS REGULATORY ACCOUNT IS VALID FOR THIS JURISDICTION.  INACTIVE = 2: THIS REGULATORY ACCOUNT IS NOT VALID FOR THIS JURISDICTION.  DEFAULT IS ACTIVE.';
comment on column REG_CASE_ACCT.reg_annualization_id
  is '1 = 13 MONTH SIMPLE AVERAGE
2 = 13 MONTH AVERAGE OF AVERAGES
3 = 12 MONTH AVERAGE
4 = 2 POINT ANNUAL AVERAGE
5 = ENDING BALANCE
6 = SUM OF 12 MONTHS
7 = ENDING MONTH * 12
8 = INPUT';
comment on column REG_CASE_ACCT.note
  is 'DESCRIPTION FOR THIS REGULATORY ACCOUNT IN THIS CASE.';
comment on column REG_CASE_ACCT.relation_id
  is 'THIS REFERENCES ANOTHER REGULATORY ACCOUNT TO WHICH IT IS ASSOCIATED, E.G. A NUMBER OF CURRENT ASSET AND LIABILITY ACCOUNTS COULD POINT TO A LEAD/LAG WORKING CAPITAL ACCOUNT.';
comment on column REG_CASE_ACCT.preparer
  is 'OPTIONAL FIELD FOR RECORDING THE PREPARER RESPONSIBLE FOR THE ACCOUNT.';
comment on column REG_CASE_ACCT.witness
  is 'OPTIONAL FIELD FOR RECORDING THE WITNESS RESPONSIBLE FOR THE ACCOUNT.';
comment on column REG_CASE_ACCT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ACCT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ACCT
  add constraint PK_REG_CASE_ACCT primary key (REG_CASE_ID, REG_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ACCT
  add constraint R_REG_CASE_ACCT1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ACCT
  add constraint R_REG_CASE_ACCT3 foreign key (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);
alter table REG_CASE_ACCT
  add constraint R_REG_CASE_ACCT4 foreign key (STATUS_CODE_ID)
  references STATUS_CODE (STATUS_CODE_ID);
alter table REG_CASE_ACCT
  add constraint R_REG_CASE_ACCT5 foreign key (REG_ANNUALIZATION_ID)
  references REG_ANNUALIZATION_TYPE (REG_ANNUALIZATION_ID);
alter table REG_CASE_ACCT
  add constraint R_REG_CASE_ACCT8 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);

prompt
prompt Creating table REG_CASE_ALLOC_ACCOUNT
prompt =====================================
prompt
create table REG_CASE_ALLOC_ACCOUNT
(
  reg_case_id              NUMBER(22) not null,
  reg_alloc_acct_id        NUMBER(22) not null,
  reg_alloc_category_id    NUMBER(22) not null,
  long_description         VARCHAR2(254) not null,
  reg_calc_status_id       NUMBER(22) not null,
  reg_acct_id              NUMBER(22) not null,
  reg_alloc_target_id      NUMBER(22),
  reg_allocator_id         NUMBER(22),
  parent_reg_alloc_acct_id NUMBER(22),
  user_id                  VARCHAR2(18),
  time_stamp               DATE
)
;
comment on table REG_CASE_ALLOC_ACCOUNT
  is '(  )  [  ]
THE REG CASE ALLOC ACCOUNT TABLE DYNAMICALLY GENERATED REGULATORY ACCOUNT BREAKDOWNS WITHIN A CASE BASED ON THE ALLOCATION CATEGORIES (AND RELATED TARGETS).  THE DATA INITIALLY DEFAULTS FROM THE REG JUR ALLOC ACCOUNT TABLE.';
comment on column REG_CASE_ALLOC_ACCOUNT.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_CASE_ALLOC_ACCOUNT.reg_alloc_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOC ACCOUNT.  NOTE, WILL EQUAL REG_ACCT_ID FOR STEP 0.';
comment on column REG_CASE_ALLOC_ACCOUNT.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION CATEGORY THAT CREATES THE SUB ACCOUNT.';
comment on column REG_CASE_ALLOC_ACCOUNT.long_description
  is 'DESCRIPTION OF THE REGULATORY SUB ACCOUNT.';
comment on column REG_CASE_ALLOC_ACCOUNT.reg_calc_status_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY STATUS. E.G. SETUP/CREATED, RESULT CALCULATED, NO RESULT CALCULATED.';
comment on column REG_CASE_ALLOC_ACCOUNT.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_CASE_ALLOC_ACCOUNT.reg_alloc_target_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION TARGET THAT CREATES THIS SUB ACCOUNT.';
comment on column REG_CASE_ALLOC_ACCOUNT.reg_allocator_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATOR THAT THIS SUB ACCOUNT WILL USE IN THE NEXT ALLOCATION STEP.';
comment on column REG_CASE_ALLOC_ACCOUNT.parent_reg_alloc_acct_id
  is 'THE IMMEDIATE PREDECESSOR ALLOC ACCOUNT BEFORE THE ALLOCATION.';
comment on column REG_CASE_ALLOC_ACCOUNT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ALLOC_ACCOUNT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ALLOC_ACCOUNT
  add constraint PK_REG_CASE_ALLOC_ACCOUNT primary key (REG_CASE_ID, REG_ALLOC_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ALLOC_ACCOUNT
  add constraint R_REG_CASE_ALLOC_ACCOUNT1 foreign key (REG_CASE_ID, REG_ACCT_ID)
  references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);
alter table REG_CASE_ALLOC_ACCOUNT
  add constraint R_REG_CASE_ALLOC_ACCOUNT2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_ALLOC_ACCOUNT
  add constraint R_REG_CASE_ALLOC_ACCOUNT3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);
alter table REG_CASE_ALLOC_ACCOUNT
  add constraint R_REG_CASE_ALLOC_ACCOUNT4 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);

prompt
prompt Creating table REG_ALLOC_RESULT
prompt ===============================
prompt
create table REG_ALLOC_RESULT
(
  reg_alloc_result_id   NUMBER(22) not null,
  reg_case_id           NUMBER(22) not null,
  reg_alloc_acct_id     NUMBER(22) not null,
  reg_acct_id           NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  amount                NUMBER(22,2) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE,
  case_year             NUMBER(22) not null
)
;
comment on table REG_ALLOC_RESULT
  is '(  )  [  ]
THE REG ALLOC RESULT TABLE DISPLAYS THE ALLOCATION RESULTS.  THIS WILL BE LOADED TO THE OTHER REPORTING LEDGERS.';
comment on column REG_ALLOC_RESULT.reg_alloc_result_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION RESULT.';
comment on column REG_ALLOC_RESULT.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_ALLOC_RESULT.reg_alloc_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION ACCOUNT.';
comment on column REG_ALLOC_RESULT.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_ALLOC_RESULT.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION CATEGORY.';
comment on column REG_ALLOC_RESULT.amount
  is 'RESULT OF THE ALLOCATION IN DOLLARS. (THE ALLOCATION EXCLUDES THE DIRECTLY ASSIGNED DOLLARS.)';
comment on column REG_ALLOC_RESULT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ALLOC_RESULT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
create index REG_ALLOC_RESULT_ALLOC_ID_IX on REG_ALLOC_RESULT (REG_CASE_ID, REG_ALLOC_ACCT_ID) tablespace PWRPLANT_IDX;
alter table REG_ALLOC_RESULT
  add constraint PK_REG_ALLOC_RESULT primary key (REG_ALLOC_RESULT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_ALLOC_RESULT
  add constraint R_REG_ALLOC_RESULT1 foreign key (REG_CASE_ID, REG_ALLOC_ACCT_ID)
  references REG_CASE_ALLOC_ACCOUNT (REG_CASE_ID, REG_ALLOC_ACCT_ID);
alter table REG_ALLOC_RESULT
  add constraint R_REG_ALLOC_RESULT2 foreign key (REG_CASE_ID, REG_ACCT_ID)
  references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);
alter table REG_ALLOC_RESULT
  add constraint R_REG_ALLOC_RESULT4 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);

prompt
prompt Creating table REG_ANNUALIZE_WORK
prompt =================================
prompt
create global temporary table REG_ANNUALIZE_WORK
(
  reg_acct_id    NUMBER(22) not null,
  gl_month       NUMBER(6) not null,
  act_amount     NUMBER(22,2),
  version_id     NUMBER(22),
  annualized_amt NUMBER(22,2),
  reg_company_id NUMBER(22)
)
on commit delete rows;
alter table REG_ANNUALIZE_WORK
  add constraint PK_REG_ANNUALIZATE_WORK primary key (REG_ACCT_ID, GL_MONTH);

prompt
prompt Creating table REG_CWIP_SOURCE
prompt ==============================
prompt
create table REG_CWIP_SOURCE
(
  reg_cwip_source_id NUMBER(22) not null,
  description        VARCHAR2(35),
  table_name         VARCHAR2(30) not null,
  user_id            VARCHAR2(18),
  time_stamp         DATE
)
;
comment on table REG_CWIP_SOURCE
  is '(  )  [  ]
THE REG CWIP SOURCE TABLE CONTAINS THE TABLES USED IN THE CWIP INTERFACE';
comment on column REG_CWIP_SOURCE.reg_cwip_source_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP SOURCE';
comment on column REG_CWIP_SOURCE.description
  is 'DESCRIPTION OF THE CWIP SOURCE';
comment on column REG_CWIP_SOURCE.table_name
  is 'TABLE NAMES FOR THE CWIP SOURCE; VALUES ARE ''CWIP_CHARGE'', ''WO_GL_ACCOUNT_SUMMARY'', ''AFUDC_CALC'', ''CWIP_IN_RATE_BASE'', ''WIP_COMP_CALC'', ''WIP_COMP_CHARGES''';
comment on column REG_CWIP_SOURCE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES';
comment on column REG_CWIP_SOURCE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES';
alter table REG_CWIP_SOURCE
  add constraint PK_REG_CWIP_SOURCE primary key (REG_CWIP_SOURCE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CWIP_FAMILY_COMPONENT
prompt ========================================
prompt
create table REG_CWIP_FAMILY_COMPONENT
(
  reg_component_id      NUMBER(22) not null,
  reg_family_id         NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  reg_acct_type_default NUMBER(22),
  sub_acct_type_id      NUMBER(22),
  reg_annualization_id  NUMBER(22),
  acct_good_for         NUMBER(22),
  long_description      VARCHAR2(254),
  user_id               VARCHAR2(18),
  time_stamp            DATE,
  used_by_client        NUMBER(1) not null,
  reg_cwip_source_id    NUMBER(22) not null,
  hist_or_fcst          VARCHAR2(4)
)
;
alter table REG_CWIP_FAMILY_COMPONENT
  add constraint PK_REG_CWIP_FAMILY_COMPONENT primary key (REG_COMPONENT_ID, REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CWIP_FAMILY_COMPONENT
  add constraint R_REG_CWIP_FAMILY_COMPONENT1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);
alter table REG_CWIP_FAMILY_COMPONENT
  add constraint R_REG_CWIP_FAMILY_COMPONENT2 foreign key (REG_ACCT_TYPE_DEFAULT)
  references REG_ACCT_TYPE (REG_ACCT_TYPE_ID);
alter table REG_CWIP_FAMILY_COMPONENT
  add constraint R_REG_CWIP_FAMILY_COMPONENT3 foreign key (REG_ACCT_TYPE_DEFAULT, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);
alter table REG_CWIP_FAMILY_COMPONENT
  add constraint R_REG_CWIP_FAMILY_COMPONENT4 foreign key (REG_ANNUALIZATION_ID)
  references REG_ANNUALIZATION_TYPE (REG_ANNUALIZATION_ID);
alter table REG_CWIP_FAMILY_COMPONENT
  add constraint R_REG_CWIP_FAMILY_COMPONENT5 foreign key (REG_CWIP_SOURCE_ID)
  references REG_CWIP_SOURCE (REG_CWIP_SOURCE_ID);

prompt
prompt Creating table REG_CWIP_FAMILY_MEMBER
prompt =====================================
prompt
create table REG_CWIP_FAMILY_MEMBER
(
  reg_member_id    NUMBER(22) not null,
  reg_family_id    NUMBER(22) not null,
  description      VARCHAR2(35) not null,
  long_description VARCHAR2(254),
  user_id          VARCHAR2(18),
  time_stamp       DATE,
  hist_or_fcst     VARCHAR2(4)
)
;
comment on table REG_CWIP_FAMILY_MEMBER
  is '(  )  [  ]
THE REG CWIP FAMILY MEMBER TABLE CONTAINS THOSE GROUPING OF WORK ORDERS MAKING UP A ''FAMILY'' OF REG ACCOUNTS, FOR EXAMPLE ''DISTRIBUTION'', ''LOW VOLTAGE TRANSMISSION'', ''BACKBONE TRANSMISSION''.';
comment on column REG_CWIP_FAMILY_MEMBER.reg_member_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP FAMILY MEMBER';
comment on column REG_CWIP_FAMILY_MEMBER.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG FAMILY';
comment on column REG_CWIP_FAMILY_MEMBER.description
  is 'DESCRIPTION OF THE CWIP FAMILY MEMBER';
comment on column REG_CWIP_FAMILY_MEMBER.long_description
  is 'LONG DESCRIPTION';
comment on column REG_CWIP_FAMILY_MEMBER.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES';
comment on column REG_CWIP_FAMILY_MEMBER.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES';
alter table REG_CWIP_FAMILY_MEMBER
  add constraint PK_REG_CWIP_FAMILY_MEMBER primary key (REG_MEMBER_ID, REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CWIP_FAMILY_MEMBER
  add constraint R_REG_CWIP_FAMILY_MEMBER1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);

prompt
prompt Creating table REG_BUDGET_COMP_MEMBER_MAP
prompt =========================================
prompt
create table REG_BUDGET_COMP_MEMBER_MAP
(
  forecast_version_id NUMBER(22) not null,
  reg_member_id       NUMBER(22) not null,
  reg_family_id       NUMBER(22) not null,
  reg_component_id    NUMBER(22) not null,
  ferc_acct           VARCHAR2(10),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_BUDGET_COMP_MEMBER_MAP
  add constraint PK_REG_BUDGET_COMP_MEMBER_MAP primary key (FORECAST_VERSION_ID, REG_MEMBER_ID, REG_FAMILY_ID, REG_COMPONENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_BUDGET_COMP_MEMBER_MAP
  add foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);
alter table REG_BUDGET_COMP_MEMBER_MAP
  add foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

prompt
prompt Creating table REG_BUDGET_COMP_WHERE_CLAUSE
prompt ===========================================
prompt
create table REG_BUDGET_COMP_WHERE_CLAUSE
(
  reg_component_id NUMBER(22) not null,
  reg_family_id    NUMBER(22) not null,
  table_list       VARCHAR2(1000) not null,
  where_sql        VARCHAR2(4000) not null,
  user_id          VARCHAR2(18),
  time_stamp       DATE
)
;
alter table REG_BUDGET_COMP_WHERE_CLAUSE
  add constraint PK_REG_BDG_COMP_WHERE_CLAUSE primary key (REG_COMPONENT_ID, REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_BUDGET_COMP_WHERE_CLAUSE
  add foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

prompt
prompt Creating table REG_BUDGET_EXPENDITURE_TYPE
prompt ==========================================
prompt
create table REG_BUDGET_EXPENDITURE_TYPE
(
  expenditure_type_id NUMBER(22) not null,
  description         VARCHAR2(35) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_BUDGET_EXPENDITURE_TYPE
  add constraint PK_REG_BUDGET_EXPENDITURE_TYPE primary key (EXPENDITURE_TYPE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_BUDGET_MEMBER_RULE
prompt =====================================
prompt
create table REG_BUDGET_MEMBER_RULE
(
  reg_member_rule_id NUMBER(22) not null,
  description        VARCHAR2(35),
  user_id            VARCHAR2(18),
  time_stamp         DATE
)
;
alter table REG_BUDGET_MEMBER_RULE
  add constraint PK_REG_BUDGET_MEMBER_RULE primary key (REG_MEMBER_RULE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_BUDGET_FAMILY_MEMBER_MAP
prompt ===========================================
prompt
create table REG_BUDGET_FAMILY_MEMBER_MAP
(
  forecast_version_id NUMBER(22) not null,
  reg_member_id       NUMBER(22) not null,
  reg_family_id       NUMBER(22) not null,
  reg_member_rule_id  NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_BUDGET_FAMILY_MEMBER_MAP
  add constraint PK_REG_BDG_FAMILY_MEMBER_MAP primary key (FORECAST_VERSION_ID, REG_MEMBER_ID, REG_FAMILY_ID, REG_MEMBER_RULE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_BUDGET_FAMILY_MEMBER_MAP
  add foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);
alter table REG_BUDGET_FAMILY_MEMBER_MAP
  add foreign key (REG_MEMBER_RULE_ID)
  references REG_BUDGET_MEMBER_RULE (REG_MEMBER_RULE_ID);

prompt
prompt Creating table REG_BUDGET_MEMBER_ELEMENT
prompt ========================================
prompt
create table REG_BUDGET_MEMBER_ELEMENT
(
  reg_member_element_id NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  column_name           VARCHAR2(30) not null,
  description_table     VARCHAR2(30) not null,
  interface_table       VARCHAR2(30) not null,
  class_code_id         NUMBER(22),
  required              NUMBER(22) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
alter table REG_BUDGET_MEMBER_ELEMENT
  add constraint PK_REG_BUDGET_MEMBER_ELEMENT primary key (REG_MEMBER_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_BUDGET_MEMBER_RULE_VALUES
prompt ============================================
prompt
create table REG_BUDGET_MEMBER_RULE_VALUES
(
  forecast_version_id   NUMBER(22) not null,
  reg_member_rule_id    NUMBER(22) not null,
  reg_member_element_id NUMBER(22) not null,
  lower_value           NUMBER(22) not null,
  upper_value           NUMBER(22),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
alter table REG_BUDGET_MEMBER_RULE_VALUES
  add constraint PK_REG_BDG_MEMBER_RULE_VALUES primary key (FORECAST_VERSION_ID, REG_MEMBER_RULE_ID, REG_MEMBER_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_BUDGET_MEMBER_RULE_VALUES
  add foreign key (REG_MEMBER_RULE_ID)
  references REG_BUDGET_MEMBER_RULE (REG_MEMBER_RULE_ID);
alter table REG_BUDGET_MEMBER_RULE_VALUES
  add foreign key (REG_MEMBER_ELEMENT_ID)
  references REG_BUDGET_MEMBER_ELEMENT (REG_MEMBER_ELEMENT_ID);

prompt
prompt Creating table REG_BUDGET_SOURCE
prompt ================================
prompt
create table REG_BUDGET_SOURCE
(
  reg_budget_source_id NUMBER(22) not null,
  description          VARCHAR2(35),
  table_name           VARCHAR2(30),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
alter table REG_BUDGET_SOURCE
  add constraint PK_REG_BUDGET_SOURCE primary key (REG_BUDGET_SOURCE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_BUDGET_SOURCE_DETAIL
prompt =======================================
prompt
create table REG_BUDGET_SOURCE_DETAIL
(
  detail_id           NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  description         VARCHAR2(35) not null,
  column_name         VARCHAR2(35),
  expenditure_type_id NUMBER(22),
  activity_or_balance VARCHAR2(35)
)
;
alter table REG_BUDGET_SOURCE_DETAIL
  add constraint PK_REG_BUDGET_SOURCE_DETAIL primary key (DETAIL_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CALCULATIONS
prompt ===============================
prompt
create table REG_CALCULATIONS
(
  calc_id           NUMBER(22) not null,
  long_description  VARCHAR2(254) not null,
  base_vs_extension NUMBER(22),
  code_as_data      VARCHAR2(4000),
  user_id           VARCHAR2(18),
  time_stamp        DATE
)
;
comment on table REG_CALCULATIONS
  is '(  )  [  ]
THE REG CALCULATIONS TABLE DESCRIBES CALCULATIONS THAT ARE MADE FOR JURISDICTIONAL ADJUSTMENTS.  SOME OF THESE CALCULATIONS ARE IN BASE LOGIC FUNCTIONS, OTHERS CAN BE ADDED AS EXTENSIONS.';
comment on column REG_CALCULATIONS.calc_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CALCULATION.';
comment on column REG_CALCULATIONS.long_description
  is 'DESCRIPTION OF THE CALCULATION.';
comment on column REG_CALCULATIONS.base_vs_extension
  is '0 = BASE FUNCTIONALITY, 1 = EXTENSION FUNCTION';
comment on column REG_CALCULATIONS.code_as_data
  is 'THIS IS WHERE THE CALCULATING CODE IS FOUND FOR AN EXTENSION FUNCTION.';
comment on column REG_CALCULATIONS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CALCULATIONS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CALCULATIONS
  add constraint PK_REG_CALCULATIONS primary key (CALC_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CASE_ACTUALS_LOG
prompt ===================================
prompt
create table REG_CASE_ACTUALS_LOG
(
  reg_case_id             NUMBER(22) not null,
  fcst_depr_version_id    NUMBER(22),
  tax_forecast_version_id NUMBER(22),
  prior_case_id           NUMBER(22),
  set_of_books_id         NUMBER(22),
  user_id                 VARCHAR2(18),
  time_stamp              DATE
)
;
comment on table REG_CASE_ACTUALS_LOG
  is '(  ) [  ]
THE REG ACTUALS CASE LOG TABLE RECORDS DEPRECIATION FORECAST AND POWERTAX FORECAST CASES THAT WENT INTO THIS HISTORIC CASE TO CREATE A KNOWN AND MEASURABLE ADJUSTMENT.';
comment on column REG_CASE_ACTUALS_LOG.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR THIS CASE.';
comment on column REG_CASE_ACTUALS_LOG.fcst_depr_version_id
  is 'THIS IS THE HISTORIC DEPRECIATION FORECAST VERSION PROVIDING THE DEPRECIATION TO BE USED IN THE ACTUALS CASE.';
comment on column REG_CASE_ACTUALS_LOG.tax_forecast_version_id
  is 'THIS IS THE POWERTAX CASE CORRESPONDING TO THE DEPR_FCST_CASE';
comment on column REG_CASE_ACTUALS_LOG.prior_case_id
  is 'IF THIS HISTORIC CASE STARTED FROM ANOTHER HISTORIC CASE – THIS IS THE PREDECESSOR HISTORIC CASE.';
comment on column REG_CASE_ACTUALS_LOG.set_of_books_id
  is 'THIS IS THE SET OF BOOKS ASSOCIATED WITH THE DEPR_FCST_CASE';
comment on column REG_CASE_ACTUALS_LOG.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ACTUALS_LOG.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ACTUALS_LOG
  add constraint PK_REG_CASE_ACTUALS_LOG primary key (REG_CASE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ACTUALS_LOG
  add constraint R_REG_CASE_ACTUALS_LOG1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ACTUALS_LOG
  add constraint R_REG_CASE_ACTUALS_LOG2 foreign key (FCST_DEPR_VERSION_ID)
  references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);
alter table REG_CASE_ACTUALS_LOG
  add constraint R_REG_CASE_ACTUALS_LOG3 foreign key (TAX_FORECAST_VERSION_ID)
  references VERSION (VERSION_ID);
alter table REG_CASE_ACTUALS_LOG
  add constraint R_REG_CASE_ACTUALS_LOG4 foreign key (PRIOR_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ACTUALS_LOG
  add constraint R_REG_CASE_ACTUALS_LOG5 foreign key (SET_OF_BOOKS_ID)
  references SET_OF_BOOKS (SET_OF_BOOKS_ID);

prompt
prompt Creating table REG_CASE_ADJUSTMENT
prompt ==================================
prompt
create table REG_CASE_ADJUSTMENT
(
  reg_case_id            NUMBER(22) not null,
  adjustment_id          NUMBER(22) not null,
  recovery_class_id      NUMBER(22),
  description            VARCHAR2(35) not null,
  long_description       VARCHAR2(2000),
  responsible_preparer   VARCHAR2(18),
  witness_id             VARCHAR2(18),
  adj_status_id          NUMBER(22),
  adjustment_type_id     NUMBER(22),
  adjustment_timing      NUMBER(22) not null,
  adjustment_periodicity NUMBER(22) not null,
  column_summary_id      NUMBER(22),
  doc_link               VARCHAR2(2000),
  fcst_depr_version_id   NUMBER(22),
  set_of_books_id        NUMBER(22),
  tax_version_id         NUMBER(22),
  om_budget_version_id   NUMBER(22),
  pt_version_id          NUMBER(22),
  calc_dw                VARCHAR2(40),
  disposition_id         NUMBER(22),
  spreadsheet_backup     VARCHAR2(2000),
  order_of_process       NUMBER(22),
  user_id                VARCHAR2(18),
  time_stamp             DATE
)
;
comment on table REG_CASE_ADJUSTMENT
  is '(  ) [  ]
THE REG CASE ADJUSTMENT TABLE TRACKS THE ORDERING OF ADJUSTMENTS FOR A REGULATORY CASE.';
comment on column REG_CASE_ADJUSTMENT.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_CASE_ADJUSTMENT.adjustment_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF AN ADJUSTMENT.';
comment on column REG_CASE_ADJUSTMENT.recovery_class_id
  is 'SYSTEM ASSIGNED IDENTIFIER OR A RECOVERY CLASS.';
comment on column REG_CASE_ADJUSTMENT.description
  is 'DESCRIPTION OF THE REGULATORY CASE ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.long_description
  is 'LONG DESCRIPTION OF OR NOTE ON THE ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.responsible_preparer
  is 'PREPARER OF THE ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.witness_id
  is 'WITNESS FOR THE ADJSTMENT';
comment on column REG_CASE_ADJUSTMENT.adj_status_id
  is 'USER DEFINED ADJUSTMENT STATUS, E.G. ''READY FOR REVIEW''';
comment on column REG_CASE_ADJUSTMENT.adjustment_type_id
  is 'ADJUSTMENT TYPE, E.G. ''INPUT INCREMENT''';
comment on column REG_CASE_ADJUSTMENT.adjustment_timing
  is 'PRE ALLOCATION (TOTAL COMPANY); OR POST JURISDICTIONAL ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.column_summary_id
  is 'MEANS TO SUMMARIZE SEVERAL ADJUSTMENTS FOR PRESENTATION';
comment on column REG_CASE_ADJUSTMENT.doc_link
  is 'LINK TO SUPPORTING DOCUMENTATION';
comment on column REG_CASE_ADJUSTMENT.fcst_depr_version_id
  is 'SYSTEM – ASSIGNED ID OF A DEPRECIATION FORECAST USED FOR THE ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.set_of_books_id
  is 'SET OF BOOKS ID USED FOR A DEPRECIATION ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.tax_version_id
  is 'POWERTAX VERSION ID USED FOR A TAX DEFERRED TAX ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.om_budget_version_id
  is 'SYSTEM – ASSIGNED ID OF THE OM BUDGET VERSION USED FOR THE ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.pt_version_id
  is 'SYSTEM – ASSIGNED ID OF THE PROPERTY TAX FORECAST USED FOR THE ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.calc_dw
  is 'PREFERENCE TO THE CALCULATION USED IN THE ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.disposition_id
  is 'SYSTEM – ASSIGNED ID OF A DISPOSITION, E.G. ''ACCEPTED'' ''REJECTED''';
comment on column REG_CASE_ADJUSTMENT.spreadsheet_backup
  is 'CONTAINS SPREADSHEET USED TO CALCULATE THE ADJUSTMENT';
comment on column REG_CASE_ADJUSTMENT.order_of_process
  is 'THE ORDER IN WHICH THE ADJUSTMENTS SHOULD BE MADE (STARTING WITH THE LOWEST NUMBER).';
comment on column REG_CASE_ADJUSTMENT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ADJUSTMENT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ADJUSTMENT
  add constraint PK_REG_CASE_ADJUSTMENT primary key (REG_CASE_ID, ADJUSTMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT10 foreign key (TAX_VERSION_ID)
  references VERSION (VERSION_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT11 foreign key (OM_BUDGET_VERSION_ID)
  references CR_BUDGET_VERSION (CR_BUDGET_VERSION_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT2 foreign key (RECOVERY_CLASS_ID)
  references REG_RECOVERY_CLASS (RECOVERY_CLASS_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT4 foreign key (ADJ_STATUS_ID)
  references REG_ADJUSTMENT_STATUS (ADJ_STATUS_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT5 foreign key (ADJUSTMENT_TYPE_ID)
  references REG_ADJUST_TYPES (ADJUSTMENT_TYPE_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT6 foreign key (COLUMN_SUMMARY_ID)
  references REG_COLUMN (COLUMN_SUMMARY_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT7 foreign key (DISPOSITION_ID)
  references REG_ADJUSTMENT_DISPOSITION (DISPOSITION_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT8 foreign key (FCST_DEPR_VERSION_ID)
  references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);
alter table REG_CASE_ADJUSTMENT
  add constraint R_REG_CASE_ADJUSTMENT9 foreign key (SET_OF_BOOKS_ID)
  references SET_OF_BOOKS (SET_OF_BOOKS_ID);

prompt
prompt Creating table REG_CASE_ADJUST_ACCT
prompt ===================================
prompt
create table REG_CASE_ADJUST_ACCT
(
  adjustment_id     NUMBER(22) not null,
  reg_case_id       NUMBER(22) not null,
  reg_alloc_acct_id NUMBER(22) not null,
  reg_acct_id       NUMBER(22) not null,
  test_yr_ended     NUMBER(22) not null,
  input_amount      NUMBER(22,2),
  input_percentage  NUMBER(22,8),
  user_id           VARCHAR2(18),
  time_stamp        DATE
)
;
comment on table REG_CASE_ADJUST_ACCT
  is '(  )  [  ]
THE REG ACCOUNT ADJUST CASE TABLE SPECIFIES THOSE ADJUSTMENTS TO THE ACTUALS OR FORECAST DATA THAT ARE NEEDED BY JURISDICTION TO DETERMINE REGULATORY RESULTS, FOR EXAMPLE, THESE MAY INCLUDE ADJUSTMENTS FOR WORKING CAPITAL, KNOWN AND MEASURABLE CHANGES, INTEREST SYNCHRONIZATION, ETC. ALTHOUGH THE TABLE HAS REG_ALLOC_ACCT ON IT, CURRENTLY ALL INPUTS ARE FOR TOTAL COMPANY.';
comment on column REG_CASE_ADJUST_ACCT.adjustment_id
  is 'A SEQUENTIAL NUMBER ASSIGNED TO THE ADJUSTMENT.';
comment on column REG_CASE_ADJUST_ACCT.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE';
comment on column REG_CASE_ADJUST_ACCT.reg_alloc_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG ALLOCATION ACCOUNT.';
comment on column REG_CASE_ADJUST_ACCT.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_CASE_ADJUST_ACCT.test_yr_ended
  is 'DATE IN YYYYMM FORMAT';
comment on column REG_CASE_ADJUST_ACCT.input_amount
  is 'THIS IS APPROPRIATE IF THE ADJUSTMENT TYPE IS INPUT, SUBSTITUTION, OR INPUT INCREMENT.  THIS IS THE ANNUALIZED AMOUNT.';
comment on column REG_CASE_ADJUST_ACCT.input_percentage
  is 'THIS IS APPROPRIATE IF THE ADJUSTMENT TYPE IS INPUT PERCENT.';
comment on column REG_CASE_ADJUST_ACCT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ADJUST_ACCT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ADJUST_ACCT
  add constraint PK_REG_CASE_ACCT_ADJ primary key (ADJUSTMENT_ID, REG_CASE_ID, REG_ALLOC_ACCT_ID, TEST_YR_ENDED)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJUST_ACCT
  add constraint R_REG_CASE_ACCT_ADJ1 foreign key (REG_CASE_ID, REG_ACCT_ID)
  references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);
alter table REG_CASE_ADJUST_ACCT
  add constraint R_REG_CASE_ACCT_ADJ2 foreign key (REG_CASE_ID, ADJUSTMENT_ID)
  references REG_CASE_ADJUSTMENT (REG_CASE_ID, ADJUSTMENT_ID);
alter table REG_CASE_ADJUST_ACCT
  add constraint R_REG_CASE_ACCT_ADJ3 foreign key (REG_CASE_ID, REG_ALLOC_ACCT_ID)
  references REG_CASE_ALLOC_ACCOUNT (REG_CASE_ID, REG_ALLOC_ACCT_ID);

prompt
prompt Creating table REG_CASE_ADJUST_ACCT_MONTHLY
prompt ===========================================
prompt
create table REG_CASE_ADJUST_ACCT_MONTHLY
(
  adjustment_id     NUMBER(22) not null,
  reg_case_id       NUMBER(22) not null,
  reg_alloc_acct_id NUMBER(22) not null,
  reg_acct_id       NUMBER(22) not null,
  test_yr_ended     NUMBER(22) not null,
  month_year        NUMBER(6) not null,
  input_amount      NUMBER(22,2),
  user_id           VARCHAR2(18),
  time_stamp        DATE
)
;
comment on table REG_CASE_ADJUST_ACCT_MONTHLY
  is '(  )  [  ]
THE REG CASE ADJUST ACCT MONTHLY TABLE LISTS THOSE ADJUSTMENTS THAT WERE INPUT OR CALCULATED IN A PARTICULAR FORECAST OR HISTORIC JURISDICTIONAL CASE ON A MONTHLY BASIS.  THESE ADJUSTMENTS ARE ANNUALIZED AND MOVED TO THE REG CASE ADJUST DETAIL.';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.adjustment_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF AN ADJUSTMENT.';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY CASE.';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.reg_alloc_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG ALLOCATION ACCOUNT.';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.reg_acct_id
  is 'REGULATORY ACCOUNT TO WHICH THE ADJUSTMENT APPLIES.';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.test_yr_ended
  is 'CASE YEAR END IN WHICH THE MONTHLY ADJUSTMENTS FALL (YYYYMM)';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.month_year
  is 'MONTH YEAR OF THE ADJUSTMENT.  YYYYMM.  IF ANNUALIZED LAST MONTH OF RATE YEAR.';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.input_amount
  is 'MONTHLY AMOUNT OF THE ADJUSTMENT.';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ADJUST_ACCT_MONTHLY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ADJUST_ACCT_MONTHLY
  add constraint PK_REG_CASE_ADJ_ACCT_MONTHLY primary key (ADJUSTMENT_ID, REG_CASE_ID, REG_ALLOC_ACCT_ID, TEST_YR_ENDED, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJUST_ACCT_MONTHLY
  add constraint R_REG_CASE_ADJ_ACCT_MONTHLY1 foreign key (ADJUSTMENT_ID, REG_CASE_ID, REG_ALLOC_ACCT_ID, TEST_YR_ENDED)
  references REG_CASE_ADJUST_ACCT (ADJUSTMENT_ID, REG_CASE_ID, REG_ALLOC_ACCT_ID, TEST_YR_ENDED);

prompt
prompt Creating table REG_CASE_ADJUST_ALLOC_LEDGER
prompt ===========================================
prompt
create table REG_CASE_ADJUST_ALLOC_LEDGER
(
  reg_case_id                 NUMBER(22) not null,
  adjustment_id               NUMBER(22) not null,
  case_year                   NUMBER(22) not null,
  reg_acct_id                 NUMBER(22) not null,
  total_co_adjust_amount      NUMBER(22,2),
  jur_alloc_percent           NUMBER(22,8),
  jur_alloc_adjust_amount     NUMBER(22,2),
  rc_alloc_percent            NUMBER(22,8),
  jur_base_rev_req_adjust_amt NUMBER(22,2),
  user_id                     VARCHAR2(18),
  time_stamp                  DATE
)
;
alter table REG_CASE_ADJUST_ALLOC_LEDGER
  add constraint PK_REG_CASE_ADJUST_ALLOC_LGR primary key (REG_CASE_ID, ADJUSTMENT_ID, CASE_YEAR, REG_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJUST_ALLOC_LEDGER
  add constraint R_REG_CASE_ADJ_ALLOC_LEDGER1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ADJUST_ALLOC_LEDGER
  add constraint R_REG_CASE_ADJ_ALLOC_LEDGER2 foreign key (REG_CASE_ID, ADJUSTMENT_ID)
  references REG_CASE_ADJUSTMENT (REG_CASE_ID, ADJUSTMENT_ID);
alter table REG_CASE_ADJUST_ALLOC_LEDGER
  add constraint R_REG_CASE_ADJ_ALLOC_LEDGER3 foreign key (REG_CASE_ID, REG_ACCT_ID)
  references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);

prompt
prompt Creating table REG_CASE_ADJUST_CALC_EXT
prompt =======================================
prompt
create table REG_CASE_ADJUST_CALC_EXT
(
  reg_case_id     NUMBER(22) not null,
  adjustment_id   NUMBER(22) not null,
  case_year       NUMBER(22) not null,
  file_path       VARCHAR2(2000),
  file_name       VARCHAR2(254),
  document_data   BLOB,
  file_size       NUMBER(22),
  attachment_type VARCHAR2(20),
  user_id         VARCHAR2(18),
  time_stamp      DATE
)
;
alter table REG_CASE_ADJUST_CALC_EXT
  add constraint PK_REG_CASE_ADJUST_CALC_EXT primary key (REG_CASE_ID, ADJUSTMENT_ID, CASE_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJUST_CALC_EXT
  add constraint R_REG_CASE_ADJUST_CALC_EXT1 foreign key (REG_CASE_ID, ADJUSTMENT_ID)
  references REG_CASE_ADJUSTMENT (REG_CASE_ID, ADJUSTMENT_ID);

prompt
prompt Creating table REG_CASE_ADJUST_DETAIL
prompt =====================================
prompt
create table REG_CASE_ADJUST_DETAIL
(
  adjustment_id     NUMBER(22) not null,
  reg_case_id       NUMBER(22) not null,
  reg_alloc_acct_id NUMBER(22) not null,
  reg_acct_id       NUMBER(22) not null,
  test_yr_ended     NUMBER(22) not null,
  user_id           VARCHAR2(18),
  time_stamp        DATE,
  begin_amount      NUMBER(22,2),
  adjust_amount     NUMBER(22,2),
  final_amount      NUMBER(22,2)
)
;
comment on table REG_CASE_ADJUST_DETAIL
  is '(  )  [  ]
THE REG CASE ADJUST DETAIL TABLE LISTS THOSE ADJUSTMENTS THAT WERE INPUT OR CALCULATED IN A PARTICULAR FORECAST OR HISTORIC JURISDICTIONAL CASE ON A MONTHLY BASIS.';
comment on column REG_CASE_ADJUST_DETAIL.adjustment_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF AN ADJUSTMENT.';
comment on column REG_CASE_ADJUST_DETAIL.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY CASE.';
comment on column REG_CASE_ADJUST_DETAIL.reg_alloc_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG ALLOCATION ACCOUNT.';
comment on column REG_CASE_ADJUST_DETAIL.reg_acct_id
  is 'REGULATORY ACCOUNT TO WHICH THE ADJUSTMENT APPLIES.';
comment on column REG_CASE_ADJUST_DETAIL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ADJUST_DETAIL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ADJUST_DETAIL.begin_amount
  is 'THE ANNUALIZED BEGINNING AMOUNT FOR THE ACCOUNT BEFORE THIS ADJUSTMENT.  (NOTE THAT PROCESSING ORDER IS IMPLIED)';
comment on column REG_CASE_ADJUST_DETAIL.adjust_amount
  is 'THE ANNUALIZED (INCREMENTAL) ADJUSTMENT AMOUNT FOR THE ACCOUNT.';
comment on column REG_CASE_ADJUST_DETAIL.final_amount
  is 'THE ANNUALIZED ENDING AMOUNT FOR THE ACCOUNT AFTER THIS ADJUSTMENT.';
alter table REG_CASE_ADJUST_DETAIL
  add constraint PK_REG_CASE_ADJUST_DETAIL primary key (ADJUSTMENT_ID, REG_CASE_ID, REG_ALLOC_ACCT_ID, TEST_YR_ENDED)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJUST_DETAIL
  add constraint R_REG_CASE_ADJUST_DETAIL1 foreign key (REG_CASE_ID, ADJUSTMENT_ID)
  references REG_CASE_ADJUSTMENT (REG_CASE_ID, ADJUSTMENT_ID);
alter table REG_CASE_ADJUST_DETAIL
  add constraint R_REG_CASE_ADJUST_DETAIL2 foreign key (REG_CASE_ID, REG_ACCT_ID)
  references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);
alter table REG_CASE_ADJUST_DETAIL
  add constraint R_REG_CASE_ADJUST_DETAIL3 foreign key (REG_CASE_ID, REG_ALLOC_ACCT_ID)
  references REG_CASE_ALLOC_ACCOUNT (REG_CASE_ID, REG_ALLOC_ACCT_ID);

prompt
prompt Creating table REG_CASE_ADJUST_SUMMARY
prompt ======================================
prompt
create table REG_CASE_ADJUST_SUMMARY
(
  reg_case_id       NUMBER(22) not null,
  reg_alloc_acct_id NUMBER(22) not null,
  reg_acct_id       NUMBER(22) not null,
  recovery_class_id NUMBER(22) not null,
  test_yr_ended     NUMBER(22) not null,
  column_summary_id NUMBER(22) not null,
  user_id           VARCHAR2(18),
  time_stamp        DATE,
  begin_amount      NUMBER(22,2),
  adjust_amount     NUMBER(22,2),
  final_amount      NUMBER(22,2)
)
;
comment on table REG_CASE_ADJUST_SUMMARY
  is '(  )  [  ]
THE REG ADJUSTED MONTHLY LEDGER TABLE SUMMARIZES THE ADJUSTMENT AMOUNTS BY COLUMN, BY MONTH, BY RECOVERY CLASS.';
comment on column REG_CASE_ADJUST_SUMMARY.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_CASE_ADJUST_SUMMARY.reg_alloc_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY ACCOUNT.';
comment on column REG_CASE_ADJUST_SUMMARY.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_CASE_ADJUST_SUMMARY.recovery_class_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE RECOVERY CLASS.';
comment on column REG_CASE_ADJUST_SUMMARY.column_summary_id
  is 'THE COLUMN TO WHICH THE ADJUSTMENT IS SUMMARIZED.';
comment on column REG_CASE_ADJUST_SUMMARY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ADJUST_SUMMARY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ADJUST_SUMMARY
  add constraint PK_REG_ANNUAL_ADJUSTED_LEDGER primary key (REG_CASE_ID, REG_ALLOC_ACCT_ID, RECOVERY_CLASS_ID, TEST_YR_ENDED, COLUMN_SUMMARY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJUST_SUMMARY
  add constraint R_REG_CASE_ADJUST_SUMMARY1 foreign key (REG_CASE_ID, REG_ACCT_ID)
  references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);
alter table REG_CASE_ADJUST_SUMMARY
  add constraint R_REG_CASE_ADJUST_SUMMARY2 foreign key (RECOVERY_CLASS_ID)
  references REG_RECOVERY_CLASS (RECOVERY_CLASS_ID);
alter table REG_CASE_ADJUST_SUMMARY
  add constraint R_REG_CASE_ADJUST_SUMMARY3 foreign key (REG_CASE_ID, REG_ALLOC_ACCT_ID)
  references REG_CASE_ALLOC_ACCOUNT (REG_CASE_ID, REG_ALLOC_ACCT_ID);

prompt
prompt Creating table REG_CASE_ADJ_ATTACHMENT
prompt ======================================
prompt
create table REG_CASE_ADJ_ATTACHMENT
(
  attach_id       NUMBER(22) not null,
  reg_case_id     NUMBER(22) not null,
  adjustment_id   NUMBER(22) not null,
  attachment_data BLOB,
  file_name       VARCHAR2(100),
  file_size       NUMBER(22),
  user_id         VARCHAR2(18),
  time_stamp      DATE
)
;
alter table REG_CASE_ADJ_ATTACHMENT
  add constraint PK_REG_CASE_ADJ_ATTACHMENT primary key (ATTACH_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJ_ATTACHMENT
  add constraint R_REG_CASE_ADJ_ATTACHMENT1 foreign key (REG_CASE_ID, ADJUSTMENT_ID)
  references REG_CASE_ADJUSTMENT (REG_CASE_ID, ADJUSTMENT_ID);

prompt
prompt Creating table REG_CASE_ADJ_CALC_INPUT_ACCT
prompt ===========================================
prompt
create table REG_CASE_ADJ_CALC_INPUT_ACCT
(
  reg_case_id       NUMBER(22) not null,
  adjustment_id     NUMBER(22) not null,
  reg_acct_id       NUMBER(22) not null,
  reg_alloc_acct_id NUMBER(22) not null,
  user_id           VARCHAR2(18),
  time_stamp        DATE
)
;
alter table REG_CASE_ADJ_CALC_INPUT_ACCT
  add constraint PK_REG_CASE_ADJ_CALC_IN_ACCT primary key (REG_CASE_ID, ADJUSTMENT_ID, REG_ACCT_ID, REG_ALLOC_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ADJ_CALC_INPUT_ACCT
  add constraint R_REG_CASE_ADJ_CALC_IN_ACCT1 foreign key (REG_CASE_ID, ADJUSTMENT_ID)
  references REG_CASE_ADJUSTMENT (REG_CASE_ID, ADJUSTMENT_ID);
alter table REG_CASE_ADJ_CALC_INPUT_ACCT
  add constraint R_REG_CASE_ADJ_CALC_IN_ACCT2 foreign key (REG_CASE_ID, REG_ACCT_ID)
  references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);
alter table REG_CASE_ADJ_CALC_INPUT_ACCT
  add constraint R_REG_CASE_ADJ_CALC_IN_ACCT3 foreign key (REG_CASE_ID, REG_ALLOC_ACCT_ID)
  references REG_CASE_ALLOC_ACCOUNT (REG_CASE_ID, REG_ALLOC_ACCT_ID);

prompt
prompt Creating table REG_CASE_ALLOCATOR_DYN_FACTOR
prompt ============================================
prompt
create table REG_CASE_ALLOCATOR_DYN_FACTOR
(
  reg_case_id         NUMBER(22) not null,
  reg_allocator_id    NUMBER(22) not null,
  test_yr_end         NUMBER(22) not null,
  reg_alloc_target_id NUMBER(22) not null,
  annual_reg_factor   NUMBER(22,16) not null,
  statistical_value   NUMBER(22,8),
  last_updated_by     VARCHAR2(18) not null,
  last_updated        DATE not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CASE_ALLOCATOR_DYN_FACTOR
  is '(  )  [  ]
THE REG CASE ALLOCATOR DYN FACTOR TABLE HOLDS THE FACTORS FOR ALLOCATORS CALCULATED DYNAMICALLY FOR A JURISDICTIONAL CASE AND TEST YEAR, FOR EXAMPLE “RATE BASE” COULD BE A DYNAMIC ALLOCATOR.';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.reg_allocator_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATOR.';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.test_yr_end
  is 'MONTH AND YEAR OF THE TEST YEAR IN YYYYMM FORMAT (NOTE: A CASE CAN INCLUDE MULTIPLE TEST YEARS)';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.reg_alloc_target_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE ALLOCATION TARGET TO WHICH THE FACTOR APPLIES';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.annual_reg_factor
  is 'THE ACTUAL FACTOR IN DECIMAL FORMAT';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.statistical_value
  is 'THE DOLLARS FORMING THE BASIS OF THE DYNAMIC FACTOR';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.last_updated_by
  is 'THE USER ID OF THE PERSON LAST UPDATING THE FACTOR (BY RUNNING THE ALLOCATIONS PROCESS)';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.last_updated
  is 'RUN TIME OF LAST UPDATE.';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ALLOCATOR_DYN_FACTOR.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ALLOCATOR_DYN_FACTOR
  add constraint PK_REG_CASE_ALLOC_DYN_FACTOR primary key (REG_CASE_ID, REG_ALLOCATOR_ID, TEST_YR_END, REG_ALLOC_TARGET_ID, LAST_UPDATED, LAST_UPDATED_BY)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ALLOCATOR_DYN_FACTOR
  add constraint R_REG_CASE_ALLOC_DYN_FACTOR1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ALLOCATOR_DYN_FACTOR
  add constraint R_REG_CASE_ALLOC_DYN_FACTOR2 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);
alter table REG_CASE_ALLOCATOR_DYN_FACTOR
  add constraint R_REG_CASE_ALLOC_DYN_FACTOR3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_CASE_ALLOC_DIRECT_ASSIGN
prompt ===========================================
prompt
create table REG_CASE_ALLOC_DIRECT_ASSIGN
(
  reg_case_id           NUMBER(22) not null,
  reg_alloc_acct_id     NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  case_year             NUMBER(6) not null,
  direct_assign_amount  NUMBER(22,2) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_CASE_ALLOC_DIRECT_ASSIGN
  is '(  )  [  ]
THE REG CASE ALLOC DIRECT ASSIGN TABLE STORES AMOUNTS DIRECTLY ASSIGNED TO TARGETS FOR SPECIFIC “PARENT” ALLOCATION ACCOUNTS REGARDLESS OF THE ALLOCATOR ASSIGNED TO THE ACCOUNT.';
comment on column REG_CASE_ALLOC_DIRECT_ASSIGN.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_CASE_ALLOC_DIRECT_ASSIGN.reg_alloc_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION ACCOUNT.';
comment on column REG_CASE_ALLOC_DIRECT_ASSIGN.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION CATEGORY.';
comment on column REG_CASE_ALLOC_DIRECT_ASSIGN.reg_alloc_target_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION TARGET.';
comment on column REG_CASE_ALLOC_DIRECT_ASSIGN.case_year
  is 'LAST MONTH OF THE CASE YEAR IN YYYYMM FORMAT.';
comment on column REG_CASE_ALLOC_DIRECT_ASSIGN.direct_assign_amount
  is 'THE DOLLAR AMOUNT TO DIRECTLY ASSIGN TO THE ALLOCATION ACCOUNT GENERATED AS THE CHILD OF THE ALLOCATION ACCOUNT ON THE RECORD FOR THE TARGET REGARDLESS OF THE ALLOCATOR ASSIGNED.';
comment on column REG_CASE_ALLOC_DIRECT_ASSIGN.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ALLOC_DIRECT_ASSIGN.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ALLOC_DIRECT_ASSIGN
  add constraint PK_REG_CASE_ALLOC_DIRECT primary key (REG_CASE_ID, REG_ALLOC_ACCT_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID, CASE_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ALLOC_DIRECT_ASSIGN
  add constraint R_REG_CASE_ALLOC_DIRECT1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ALLOC_DIRECT_ASSIGN
  add constraint R_REG_CASE_ALLOC_DIRECT2 foreign key (REG_CASE_ID, REG_ALLOC_ACCT_ID)
  references REG_CASE_ALLOC_ACCOUNT (REG_CASE_ID, REG_ALLOC_ACCT_ID);
alter table REG_CASE_ALLOC_DIRECT_ASSIGN
  add constraint R_REG_CASE_ALLOC_DIRECT3 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_ALLOC_DIRECT_ASSIGN
  add constraint R_REG_CASE_ALLOC_DIRECT4 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_CASE_ALLOC_FACTOR
prompt ====================================
prompt
create table REG_CASE_ALLOC_FACTOR
(
  reg_case_id           NUMBER(22) not null,
  reg_allocator_id      NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  effective_date        NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  base_year_end         NUMBER(22),
  annual_reg_factor     NUMBER(22,16) not null,
  statistical_value     NUMBER(22,8),
  unit_of_measure       VARCHAR2(35),
  last_updated          DATE,
  last_updated_by       VARCHAR2(18),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
alter table REG_CASE_ALLOC_FACTOR
  add constraint PK_REG_CASE_ALLOC_FACTOR primary key (REG_CASE_ID, REG_ALLOCATOR_ID, EFFECTIVE_DATE, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ALLOC_FACTOR
  add constraint R_REG_CASE_ALLOC_FACTOR1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ALLOC_FACTOR
  add constraint R_REG_CASE_ALLOC_FACTOR2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_ALLOC_FACTOR
  add constraint R_REG_CASE_ALLOC_FACTOR3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);
alter table REG_CASE_ALLOC_FACTOR
  add constraint R_REG_CASE_ALLOC_FACTOR4 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);

prompt
prompt Creating table REG_CASE_ALLOC_STEPS
prompt ===================================
prompt
create table REG_CASE_ALLOC_STEPS
(
  reg_case_id                NUMBER(22) not null,
  reg_alloc_category_id      NUMBER(22) not null,
  step_order                 NUMBER(22) not null,
  jur_result_flag            NUMBER(22),
  class_cost_of_service_flag NUMBER(22),
  recovery_class_flag        NUMBER(22),
  user_id                    VARCHAR2(18),
  time_stamp                 DATE
)
;
comment on table REG_CASE_ALLOC_STEPS
  is '(  )  [  ]
THE REG CASE ALLOC STEP TABLE CONTROLS WHICH CATEGORY LEVELS ARE USED IN A REGULATORY CASE AND THEIR ORDER OF EXECUTION.  THE DATA CAN BE DEFAULTED FROM THE REG JUR ALLOC STEPS TABLE WHEN CREATING A CASE';
comment on column REG_CASE_ALLOC_STEPS.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY CASE.';
comment on column REG_CASE_ALLOC_STEPS.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION CATEGORY.';
comment on column REG_CASE_ALLOC_STEPS.step_order
  is 'INTEGER ORDER, E.G. ''1'' ALLOCATION WOULD PROCEED ''2''.  (DOES NOT NEED TO BE CONSECUTIVE).';
comment on column REG_CASE_ALLOC_STEPS.jur_result_flag
  is '0 = NO; 1 = YES, I.E. THIS STEP REPRESENTS JURISDICTIONAL RESULTS';
comment on column REG_CASE_ALLOC_STEPS.class_cost_of_service_flag
  is '0 = NO; 1 = YES, I.E. THIS STEP REPRESENTS CLASS COST OF SERVICE RESULTS';
comment on column REG_CASE_ALLOC_STEPS.recovery_class_flag
  is '0 = NO; 1 = YES, I.E. THIS STEP REPRESENTS RECOVERY CLASS RESULTS';
comment on column REG_CASE_ALLOC_STEPS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ALLOC_STEPS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ALLOC_STEPS
  add constraint PK_REG_CASE_ALLOC_STEPS primary key (REG_CASE_ID, REG_ALLOC_CATEGORY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ALLOC_STEPS
  add constraint R_REG_CASE_ALLOC_STEPS1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ALLOC_STEPS
  add constraint R_REG_CASE_ALLOC_STEPS2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);

prompt
prompt Creating table REG_CASE_ALLOC_TAX_CONTROL
prompt =========================================
prompt
create table REG_CASE_ALLOC_TAX_CONTROL
(
  reg_case_id           NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  calc_tax_on_adjust    NUMBER(22) not null,
  tax_calc_id           NUMBER(22) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_CASE_ALLOC_TAX_CONTROL
  is '(  )  [  ]
THE REG CASE ALLOC TAX CONTROL TABLE CONTAINS THE RULES FOR DETERMINING INCOME TAX EXPENSE FOR EACH LEVEL (CATEGORY) OF THE ALLOCATION AND FOR EACH TARGET IN A CASE.  IT IS DEFAULTED FROM THE TEMPLATE: REG_JUR_ALLOC_TAX_CONTROL TABLE.';
comment on column REG_CASE_ALLOC_TAX_CONTROL.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF AN ALLOCATION CATEGORY OR LEVEL.';
comment on column REG_CASE_ALLOC_TAX_CONTROL.calc_tax_on_adjust
  is 'DO WE CALCULATE TAXES FOR ADJUSTMENTS FOR THIS CATEGORY AND TARGET (1=YES, 2 = NO).';
comment on column REG_CASE_ALLOC_TAX_CONTROL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_ALLOC_TAX_CONTROL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_ALLOC_TAX_CONTROL
  add constraint PK_REG_CASE_ALLOC_TAX_CONTROL primary key (REG_CASE_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ALLOC_TAX_CONTROL
  add constraint R_REG_CASE_ALLOC_TAX_CTRL1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_ALLOC_TAX_CONTROL
  add constraint R_REG_CASE_ALLOC_TAX_CTRL2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_ALLOC_TAX_CONTROL
  add constraint R_REG_CASE_ALLOC_TAX_CTRL3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_CASE_ATTACHMENT
prompt ==================================
prompt
create table REG_CASE_ATTACHMENT
(
  attach_id       NUMBER(22) not null,
  reg_case_id     NUMBER(22) not null,
  attachment_data BLOB,
  file_name       VARCHAR2(100),
  file_size       NUMBER(22),
  user_id         VARCHAR2(18),
  time_stamp      DATE
)
;
alter table REG_CASE_ATTACHMENT
  add constraint PK_REG_CASE_ATTACHMENT primary key (ATTACH_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_ATTACHMENT
  add constraint R_REG_CASE_ATTACHMENT1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);

prompt
prompt Creating table REG_CASE_CALCULATION
prompt ===================================
prompt
create table REG_CASE_CALCULATION
(
  reg_case_id                 NUMBER(22) not null,
  reg_alloc_category_id       NUMBER(22) not null,
  reg_alloc_target_id         NUMBER(22) not null,
  adjustment_id               NUMBER(22) not null,
  case_year_end               NUMBER(22) not null,
  rate_base                   NUMBER(22,2),
  pretax_income               NUMBER(22,2),
  interest_sync               NUMBER(22,2),
  alloc_perms                 NUMBER(22,2),
  alloc_credits               NUMBER(22,2),
  alloc_itc_amort             NUMBER(22,2),
  alloc_fas109                NUMBER(22,2),
  calc_inc_tax_rate_base_sync NUMBER(22,2),
  calc_inc_tax_oper_income    NUMBER(22,2),
  income_tax_rate             NUMBER(22,8),
  user_id                     VARCHAR2(18),
  time_stamp                  DATE
)
;
comment on table REG_CASE_CALCULATION
  is '(  )  [  ]
THE REG CASE CALCULATION TABLE HOLDS THE DATA AND RESULTS OF INCOME TAX COMPUTATIONS FOR BOTH FINANCIAL ALLOCATED ENTITIES.';
comment on column REG_CASE_CALCULATION.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY JURISDICTION.';
comment on column REG_CASE_CALCULATION.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY ALLOCATION CATEGORY.';
comment on column REG_CASE_CALCULATION.reg_alloc_target_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY ALLOCATION TARGET.';
comment on column REG_CASE_CALCULATION.adjustment_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY ADJUSTMENT.';
comment on column REG_CASE_CALCULATION.case_year_end
  is 'MONTH END OF 12 MONTH ENDING ANALYSIS IN YYYYMM FORMAT.';
comment on column REG_CASE_CALCULATION.rate_base
  is 'AMOUNT OF RATE BASE IN DOLLARS.';
comment on column REG_CASE_CALCULATION.pretax_income
  is 'AMOUNT OF PRE-TAX BOOK INCOME (EXPENSE) IN DOLLARS';
comment on column REG_CASE_CALCULATION.interest_sync
  is 'AMOUNT OF DOLLARS OF INTEREST ALLOCATED (SYNCHRONIZATION) PRE TAX.  ALLOCATION IS BASED ON RATE BASE.';
comment on column REG_CASE_CALCULATION.alloc_perms
  is 'THE AMOUNT OF PRE-TAX PERM M-ITEMS ALLOCATED TO THIS TARGET OR IN THE ADJUSTMENT IN DOLLARS.  A ''+'' IS AN INCREASE IN TAXABLE INCOME.';
comment on column REG_CASE_CALCULATION.alloc_credits
  is 'THE AMOUNT OF AFTER TAX CREDIT TO BE APPLIED TO THE TARGET OR IN THE ADJUSTMENT IN DOLLARS.  A ''–'' IS A DECREASE IN TAX EXPENSE.';
comment on column REG_CASE_CALCULATION.alloc_itc_amort
  is 'THE AMOUNT OF ITC AMORTIZATION.  A ''_'' IS A THE NORMAL SIGN.';
comment on column REG_CASE_CALCULATION.alloc_fas109
  is 'THIS IS THE INCREASE (-1) IN THE ACCUMULATED CREDIT BALANCE OF THE INCREMENTAL FAS109 DEFERREDS OVER REGULATORY DEFERREDS BEFORE THE GROSS UP (TO REVENUE REQUIREMENT).  THIS WOULD EXIST FOR EITHER FLOW THROUGH OR EXCESS DEFERREDS.';
comment on column REG_CASE_CALCULATION.calc_inc_tax_rate_base_sync
  is 'THIS IS THE COMBINED FEDERAL AND STATE, CURRENT AND DEFERRED, INTEREST SYNC PORTION OF THE INCOME TAX.';
comment on column REG_CASE_CALCULATION.calc_inc_tax_oper_income
  is 'THIS IS THE COMBINED FEDERAL AND STATE, CURRENT AND DEFERRED, ALLOCATED INCOME TAX EXPENSE.';
comment on column REG_CASE_CALCULATION.income_tax_rate
  is 'THIS IS THE TOTAL FEDERAL AND STATE ''STATUTORY'' RATE USED IN THE CALCULATION, RECORDED HERE ONLY FOR EASY TRACKING.';
comment on column REG_CASE_CALCULATION.user_id
  is 'SYSTEM ASSIGNED USER ID USED FOR AUDIT PURPOSES';
comment on column REG_CASE_CALCULATION.time_stamp
  is 'SYSTEM ASSIGNED TIME STAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_CALCULATION
  add constraint PK_REG_CASE_CALCULATION primary key (REG_CASE_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID, ADJUSTMENT_ID, CASE_YEAR_END)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_CALCULATION
  add constraint R_REG_CASE_CALCULATION1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_CALCULATION
  add constraint R_REG_CASE_CALCULATION2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_CALCULATION
  add constraint R_REG_CASE_CALCULATION3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);
alter table REG_CASE_CALCULATION
  add constraint R_REG_CASE_CALCULATION4 foreign key (REG_CASE_ID, ADJUSTMENT_ID)
  references REG_CASE_ADJUSTMENT (REG_CASE_ID, ADJUSTMENT_ID);

prompt
prompt Creating table REG_CASE_CATEGORY_ALLOCATOR
prompt ==========================================
prompt
create table REG_CASE_CATEGORY_ALLOCATOR
(
  reg_case_id           NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_allocator_id      NUMBER(22) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_CASE_CATEGORY_ALLOCATOR
  is '(  )  [  ]
THESE ARE THE ALLOCATORS AVAILABLE FOR THAT CATEGORY FOR THAT CASE.';
comment on column REG_CASE_CATEGORY_ALLOCATOR.reg_case_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF A CASE.';
comment on column REG_CASE_CATEGORY_ALLOCATOR.reg_alloc_category_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF AN ALLOCATION CATEGORY.';
comment on column REG_CASE_CATEGORY_ALLOCATOR.reg_allocator_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF A REGULATORY ALLOCATOR APPROPRIATE FOR THAT JURISDICTION AND LEVEL.';
comment on column REG_CASE_CATEGORY_ALLOCATOR.user_id
  is 'SYSTEM – ASSIGNED USER_ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_CATEGORY_ALLOCATOR.time_stamp
  is 'SYSTEM – ASSIGNED TIME_STAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_CATEGORY_ALLOCATOR
  add constraint PK_REG_CASE_CATEGORY_ALLOCATOR primary key (REG_CASE_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOCATOR_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_CATEGORY_ALLOCATOR
  add constraint R_REG_CASE_CATEGORY_ALLOCATOR1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_CATEGORY_ALLOCATOR
  add constraint R_REG_CASE_CATEGORY_ALLOCATOR2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_CATEGORY_ALLOCATOR
  add constraint R_REG_CASE_CATEGORY_ALLOCATOR3 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);

prompt
prompt Creating table REG_CASE_COST_OF_CAPITAL
prompt =======================================
prompt
create table REG_CASE_COST_OF_CAPITAL
(
  reg_case_id                 NUMBER(22) not null,
  capital_structure_id        NUMBER(22) not null,
  month_year                  NUMBER(6) not null,
  annualized_balance          NUMBER(22,2),
  adjusted_balance            NUMBER(22,2),
  expense_cost                NUMBER(22,2),
  calculated_rate             NUMBER(22,8),
  input_rate                  NUMBER(22,8),
  cap_structure_percent       NUMBER(22,8),
  cap_structure_input_percent NUMBER(22,8),
  weighted_cofc_rate          NUMBER(22,8),
  weighted_cofc_actual        NUMBER(22,8),
  high_common_rate            NUMBER(22,8),
  low_common_rate             NUMBER(22,8),
  weighted_cofc_high          NUMBER(22,8),
  weighted_cofc_low           NUMBER(22,8),
  user_id                     VARCHAR2(18),
  time_stamp                  DATE
)
;
comment on table REG_CASE_COST_OF_CAPITAL
  is '(  )  [  ]
THE REG CASE COST OF CAPITAL TABLE IS USED TO CALCULATE THE COMPONENT AND OVERALL COST OF CAPITAL USED FOR INTREST SYNC AND TO CALCULATE TOTAL REVENUE REQUIREMENTS.  NOTE THAT THIS TABLE RECORDS THE COMPUTATION OF A SINGLE JURISDICTION COST OF CAPITAL FOR THE JURISDICTION, USING THE CAP STRUCTURE WHICH IS ASSOCIATED WITH JURISDICTIONAL BASE RATES.';
comment on column REG_CASE_COST_OF_CAPITAL.reg_case_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF A CASE.';
comment on column REG_CASE_COST_OF_CAPITAL.capital_structure_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CAPITAL STRUCTURE (CONTROL) ITEM, E.G. LONG TERM DEBT, PREFERRED STOCK, ETC.';
comment on column REG_CASE_COST_OF_CAPITAL.annualized_balance
  is 'ANNUALIZED ACCOUNT BALANCE (BASED ON THE CASE''S ANNUALIZATION METHODOLOGY BEFORE ADJUSTMENTS).  TOTAL COMPANY';
comment on column REG_CASE_COST_OF_CAPITAL.adjusted_balance
  is 'ANNUALIZED ADJUSTED CAPITAL STRUCTURE BALANCE INCORPORATES USER ADJUSTMENTS OR ALLOCATIONS, SYNCHRONIZED CAPITAL STRUCTURE, AT THE JURISDICTIONAL BASE RATES RECOVERY CLASS LEVEL.';
comment on column REG_CASE_COST_OF_CAPITAL.expense_cost
  is 'THIS IS THE ASSOCIATED EXPENSE (OR DIVIDEND) FOR THE CAPITAL STRUCTURE ITEM FOR THE PERIOD.';
comment on column REG_CASE_COST_OF_CAPITAL.calculated_rate
  is 'THIS IS THE CALCULATED RATE (EXPENSE COST/ANNUALIZED BALANCE)';
comment on column REG_CASE_COST_OF_CAPITAL.input_rate
  is 'THIS IS AN INPUT RATE FOR THOSE ITEMS (E.G. COMMON) NOT HAVING A CALCULATED RATE, OR AS AN OVERRIDE TO THE CALCULATED RATE, INPUT AS A DECIMAL.';
comment on column REG_CASE_COST_OF_CAPITAL.cap_structure_percent
  is 'CALCULATED PERCENT OF CAPITAL STRUCTURE FOR THE CAPITAL STRUCTURE ITEM FOR THE CASE YEAR USING THE CALCULATED_RATE.';
comment on column REG_CASE_COST_OF_CAPITAL.cap_structure_input_percent
  is 'CALCULATED PERCENT OF CAPITAL STRUCTURE FOR THE CAPITAL STRUCTURE ITEM FOR THE CASE YEAR USING THE INPUT_RATE.';
comment on column REG_CASE_COST_OF_CAPITAL.weighted_cofc_rate
  is 'THIS IS CALCULATED WEIGHTED COST OF CAPITAL FOR THIS ITEM USING THE THEORETICAL CAPITAL STRUCTURE OR ACTUAL AND CALCULATED (OR IF OVERRIDDEN BY INPUT) COSTS RATES.';
comment on column REG_CASE_COST_OF_CAPITAL.weighted_cofc_actual
  is 'THIS IS THE COST OF CAPITAL USING ACTUAL CALCULATED RATES WHEN AVAILABLE AND ACTUAL CAPITALIZATION STRUCTURE.';
comment on column REG_CASE_COST_OF_CAPITAL.high_common_rate
  is 'OPTIONAL INPUT FOR COMMON EQUITY CAPITAL STRUCTURE ITEM, REPRESENTING A HIGH BAND RETURN.';
comment on column REG_CASE_COST_OF_CAPITAL.low_common_rate
  is 'OPTIONAL INPUT FOR COMMON EQUITY CAPITAL STRUCTURE ITEM, REPRESENTING A LOW BAND RETURN.';
comment on column REG_CASE_COST_OF_CAPITAL.weighted_cofc_high
  is 'LIKE THE WEIGHTED_COFC_RATE, BUT USING THE HIGH_COMMON_RATE';
comment on column REG_CASE_COST_OF_CAPITAL.weighted_cofc_low
  is 'LIKE THE WEIGHTED_COFC_RATE, BUT USING THE LOW_COMMON_RATE';
comment on column REG_CASE_COST_OF_CAPITAL.user_id
  is 'SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_COST_OF_CAPITAL.time_stamp
  is 'SYSTEM-ASSIGNED TIME STAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_COST_OF_CAPITAL
  add constraint PK_REG_CASE_COST_OF_CAPITAL primary key (REG_CASE_ID, CAPITAL_STRUCTURE_ID, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_COST_OF_CAPITAL
  add constraint R_REG_CASE_COST_OF_CAPITAL1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_COST_OF_CAPITAL
  add constraint R_REG_CASE_COST_OF_CAPITAL2 foreign key (CAPITAL_STRUCTURE_ID)
  references REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID);

prompt
prompt Creating table REG_CASE_COST_OF_CAPITAL_RATES
prompt =============================================
prompt
create table REG_CASE_COST_OF_CAPITAL_RATES
(
  reg_case_id              NUMBER(22) not null,
  month_year               NUMBER(6) not null,
  capital_structure_id     NUMBER(22) not null,
  rate                     NUMBER(22,8),
  high_band_rate           NUMBER(22,8),
  low_band_rate            NUMBER(22,8),
  input_percent_of_cap_tme NUMBER(22,8),
  user_id                  VARCHAR2(18),
  time_stamp               DATE
)
;
comment on table REG_CASE_COST_OF_CAPITAL_RATES
  is '(  )  [  ]
THE REG COST OF CAPITAL RATES TABLE CONTAINS INPUT RATES FOR COST OF CAPITAL ITEMS.  THESE CAN BE ITEMS FOR WHICH THERE IS NO CALCULATION, (E.G. COMMON EQUITY, CUSTOMER DEPOSITS, ETC.) OR WHICH THE USER IS OVERRIDING THE RATE (E.G. ACCUMULATED ITC, DEFERRED TAXES).  IT CAN BE ENTERED BY CAPITAL STRUCTURE ID, FOR THOSE REG ACCOUNTS WITH A SUBACCOUNT TYPE REFERENCING A REG FIN MONITORING TYPE E.G. ''COMMON''.';
comment on column REG_CASE_COST_OF_CAPITAL_RATES.reg_case_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF A RATE CASE.';
comment on column REG_CASE_COST_OF_CAPITAL_RATES.capital_structure_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF REG COST OF CAPITAL IDENTIFIER';
comment on column REG_CASE_COST_OF_CAPITAL_RATES.rate
  is 'ANNUAL RATE OF RETURN EXPRESSED IN DECIMAL FORMAT.';
comment on column REG_CASE_COST_OF_CAPITAL_RATES.high_band_rate
  is 'FOR COMMON EQUITY – IF THE COMMISSION PRESCRIBES A HIGH END-RETURN ON COMMON EQUITY.';
comment on column REG_CASE_COST_OF_CAPITAL_RATES.low_band_rate
  is 'FOR COMMON EQUITY - IF THE COMMISSION PRESCRIBES A FLOOR RETURN ON COMMON EQUITY.';
comment on column REG_CASE_COST_OF_CAPITAL_RATES.input_percent_of_cap_tme
  is 'INPUT PERCENT OF CAPITAL FOR A THEORETICAL OR SYNTHETIC CAPITAL STRUCTURE.';
comment on column REG_CASE_COST_OF_CAPITAL_RATES.user_id
  is 'SYSTEM – ASSIGNED USER_ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_COST_OF_CAPITAL_RATES.time_stamp
  is 'SYSTEM – ASSIGNED TIME_STAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_COST_OF_CAPITAL_RATES
  add constraint PK_REG_CASE_COST_OF_CAP_RATES primary key (REG_CASE_ID, MONTH_YEAR, CAPITAL_STRUCTURE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_COST_OF_CAPITAL_RATES
  add constraint R_REG_CASE_COST_OF_CAP_RATES1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_COST_OF_CAPITAL_RATES
  add constraint R_REG_CASE_COST_OF_CAP_RATES2 foreign key (CAPITAL_STRUCTURE_ID)
  references REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID);

prompt
prompt Creating table REG_FORECAST_VERSION
prompt ===================================
prompt
create table REG_FORECAST_VERSION
(
  forecast_version_id       NUMBER(22) not null,
  description               VARCHAR2(35) not null,
  long_description          VARCHAR2(1000),
  reg_status_id             NUMBER(22),
  start_month               NUMBER(6) not null,
  end_month                 NUMBER(6) not null,
  actuals_thru_month        NUMBER(6),
  cr_budget_version_id      NUMBER(22),
  cr_budget_version_time    DATE,
  cr_budget_actuals_month   NUMBER(6),
  budget_version_id         NUMBER(22),
  budget_version_time       DATE,
  budget_actuals_month      NUMBER(6),
  fcst_depr_version_id      NUMBER(22),
  fcst_depr_version_time    DATE,
  fcst_depr_actuals_month   NUMBER(6),
  tax_version_id            NUMBER(22),
  tax_version_time          DATE,
  tax_version_actuals_month NUMBER(6),
  ta_version_id             NUMBER(22),
  ta_version_time           DATE,
  ta_version_actuals_month  NUMBER(6),
  validation_status         VARCHAR2(1000),
  copied_from_version_id    NUMBER(22),
  copied_from_date          DATE,
  user_id                   VARCHAR2(18),
  time_stamp                DATE,
  historic_version_id       NUMBER(22)
)
;
comment on table REG_FORECAST_VERSION
  is '(  )  [  ]
THE REG FORECAST VERSION CONTAINS A LIST OF THE REGULATORY FORECAST VERSIONS ON THE REG FORECAST LEDGER.  IT CONTAINS DATA ABOUT THE VERSION AND HOW IT WAS CREATED FROM VARIOUS BUDGETS VERSIONS AND CASES THROUGHOUT THE POWERPLANT SYSTEM FOR VERIFICATION AND VALIDATION.';
comment on column REG_FORECAST_VERSION.reg_status_id
  is '1 = ACTIVE (USED FOR MONTHLY PULL); 2 = LOCKED; 3 = ARCHIVED';
alter table REG_FORECAST_VERSION
  add constraint PK_REG_FORECAST_VERSION primary key (FORECAST_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FORECAST_VERSION
  add constraint R_REG_FORECAST_VERSION1 foreign key (REG_STATUS_ID)
  references REG_STATUS (REG_STATUS_ID);
alter table REG_FORECAST_VERSION
  add constraint R_REG_FORECAST_VERSION2 foreign key (CR_BUDGET_VERSION_ID)
  references CR_BUDGET_VERSION (CR_BUDGET_VERSION_ID);
alter table REG_FORECAST_VERSION
  add constraint R_REG_FORECAST_VERSION3 foreign key (BUDGET_VERSION_ID)
  references BUDGET_VERSION (BUDGET_VERSION_ID);
alter table REG_FORECAST_VERSION
  add constraint R_REG_FORECAST_VERSION4 foreign key (FCST_DEPR_VERSION_ID)
  references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);
alter table REG_FORECAST_VERSION
  add constraint R_REG_FORECAST_VERSION5 foreign key (TAX_VERSION_ID)
  references VERSION (VERSION_ID);
alter table REG_FORECAST_VERSION
  add constraint R_REG_FORECAST_VERSION6 foreign key (TA_VERSION_ID)
  references TAX_ACCRUAL_VERSION (TA_VERSION_ID);
alter table REG_FORECAST_VERSION
  add constraint R_REG_FORECAST_VERSION7 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

prompt
prompt Creating table REG_CASE_COVERAGE_VERSIONS
prompt =========================================
prompt
create table REG_CASE_COVERAGE_VERSIONS
(
  coverage_id       NUMBER(22) not null,
  reg_case_id       NUMBER(22) not null,
  hist_version_id   NUMBER(22) not null,
  fore_version_id   NUMBER(22) not null,
  user_id           VARCHAR2(18),
  time_stamp        DATE,
  start_date        NUMBER(6) default 0 not null,
  end_date          NUMBER(6) default 0 not null,
  test_yr_flag      NUMBER(1) default 0 not null,
  coverage_yr_ended NUMBER(6)
)
;
comment on table REG_CASE_COVERAGE_VERSIONS
  is '(  ) [  ]
THE REG CASE COVERAGE VERSIONS TABLE TRACKS WHICH HISTORIC AND FORECAST VERSIONS GO INTO A CASE.  FOR THE MOST PART ONLY ONE HISTORIC VERSION WILL BE USED IN A CASE.';
comment on column REG_CASE_COVERAGE_VERSIONS.coverage_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR COVERAGE.';
comment on column REG_CASE_COVERAGE_VERSIONS.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE CASE.';
comment on column REG_CASE_COVERAGE_VERSIONS.hist_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE HISTORIC VERSION.';
comment on column REG_CASE_COVERAGE_VERSIONS.fore_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE FORECAST VERSION';
comment on column REG_CASE_COVERAGE_VERSIONS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_COVERAGE_VERSIONS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_COVERAGE_VERSIONS.test_yr_flag
  is 'INDICATES WHETHER THE VERSION AND PERIOD BELONG TO A TEST YEAR FOR A RATE CASE. 0 – NOT A TEST YEAR; 1 – TEST YEAR.';
comment on column REG_CASE_COVERAGE_VERSIONS.coverage_yr_ended
  is 'THE YEAR END IN YYYYMM FORMAT.';
alter table REG_CASE_COVERAGE_VERSIONS
  add constraint PK_CASE_COVERAGE_VERSIONS primary key (COVERAGE_ID, HIST_VERSION_ID, FORE_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_COVERAGE_VERSIONS
  add constraint R_REG_CASE_COVERAGE_VERSIONS1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_COVERAGE_VERSIONS
  add constraint R_REG_CASE_COVERAGE_VERSIONS2 foreign key (HIST_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_CASE_COVERAGE_VERSIONS
  add constraint R_REG_CASE_COVERAGE_VERSIONS3 foreign key (FORE_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

prompt
prompt Creating table REG_CASE_FORECAST_LOG
prompt ====================================
prompt
create table REG_CASE_FORECAST_LOG
(
  reg_case_id            NUMBER(22) not null,
  last_actuals_mo        DATE,
  capital_budget_version NUMBER(22),
  om_budget_version_id   NUMBER(22),
  fcst_depr_version_id   NUMBER(22),
  set_of_books_id        NUMBER(22),
  pt_case_id             NUMBER(22),
  tax_version_id         NUMBER(22),
  prior_case_id          NUMBER(22),
  user_id                VARCHAR2(18),
  time_stamp             DATE
)
;
comment on table REG_CASE_FORECAST_LOG
  is '(  ) [  ]
THE REG FORECAST CASE LOG TABLE RECORDS THE VARIOUS POWERPLANT VERSIONS AND CASES BUILDING UP THE REGULATORY FORECAST CASE AS WELL AS THE STATUS OF COMPLETION STEPS IN BUILDING THE CASE.';
comment on column REG_CASE_FORECAST_LOG.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE CASE.';
comment on column REG_CASE_FORECAST_LOG.last_actuals_mo
  is 'LAST MONTH OF ACTUALS IN THE FORECAST CASE.';
comment on column REG_CASE_FORECAST_LOG.capital_budget_version
  is 'POWERPLANT CAPITAL BUDGET VERSION FEEDING INTO THIS FORECAST CASE.';
comment on column REG_CASE_FORECAST_LOG.om_budget_version_id
  is 'POWERPLANT O&M BUDGET VERSION FEEDING INTO THIS FORECAST CASE.';
comment on column REG_CASE_FORECAST_LOG.fcst_depr_version_id
  is 'POWERPLANT BOOK DEPRECIATION FORECAST FEEDING INTO THIS FORECAST CASE.';
comment on column REG_CASE_FORECAST_LOG.set_of_books_id
  is 'SET OF BOOKS RELATED TO THE DEPR_FORECAST_VERSION.';
comment on column REG_CASE_FORECAST_LOG.pt_case_id
  is 'POWERPLANT PROPERTY TAX CASE FEEDING INTO THIS FORECAST CASE.';
comment on column REG_CASE_FORECAST_LOG.tax_version_id
  is 'POWERTAX CASE FEEDING INTO THIS FORECAST CASE.';
comment on column REG_CASE_FORECAST_LOG.prior_case_id
  is 'IF THIS FORECAST CASE STARTED FROM ANOTHER FORECAST CASE – THIS IS THE PREDECESSOR CASE.';
comment on column REG_CASE_FORECAST_LOG.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_FORECAST_LOG.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_FORECAST_LOG
  add constraint PK_REG_CASE_FORECAST_LOG primary key (REG_CASE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_FORECAST_LOG
  add constraint R_REG_CASE_FORECAST_LOG1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_FORECAST_LOG
  add constraint R_REG_CASE_FORECAST_LOG2 foreign key (CAPITAL_BUDGET_VERSION)
  references BUDGET_VERSION (BUDGET_VERSION_ID);
alter table REG_CASE_FORECAST_LOG
  add constraint R_REG_CASE_FORECAST_LOG3 foreign key (FCST_DEPR_VERSION_ID)
  references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);
alter table REG_CASE_FORECAST_LOG
  add constraint R_REG_CASE_FORECAST_LOG4 foreign key (SET_OF_BOOKS_ID)
  references SET_OF_BOOKS (SET_OF_BOOKS_ID);
alter table REG_CASE_FORECAST_LOG
  add constraint R_REG_CASE_FORECAST_LOG5 foreign key (PT_CASE_ID)
  references PT_CASE (CASE_ID);
alter table REG_CASE_FORECAST_LOG
  add constraint R_REG_CASE_FORECAST_LOG6 foreign key (TAX_VERSION_ID)
  references VERSION (VERSION_ID);
alter table REG_CASE_FORECAST_LOG
  add constraint R_REG_CASE_FORECAST_LOG7 foreign key (PRIOR_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_FORECAST_LOG
  add constraint R_REG_CASE_FORECAST_LOG8 foreign key (OM_BUDGET_VERSION_ID)
  references CR_BUDGET_VERSION (CR_BUDGET_VERSION_ID);

prompt
prompt Creating table REG_CASE_PROCESS
prompt ===============================
prompt
create table REG_CASE_PROCESS
(
  reg_case_process_id NUMBER(22) not null,
  description         VARCHAR2(35) not null,
  order_num           NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CASE_PROCESS
  is '(  ) [  ]
THE REG CASE PROCESS TABLE CONTAINS LIST OF MAJOR PROCESSES FOR A REGULATORY CASE SUCH AS “TOTAL COMPANY ADJUSTMENTS,” “JURISDICTIONAL ALLOCATIONS,” ETC.';
comment on column REG_CASE_PROCESS.reg_case_process_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF REGULATORY PROCESSES.';
comment on column REG_CASE_PROCESS.description
  is 'DESCRIPTION OF THE REGULATORY PROCESS.';
comment on column REG_CASE_PROCESS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_PROCESS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_PROCESS
  add constraint PK_REG_CASE_PROCESS primary key (REG_CASE_PROCESS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CASE_PROCESS_STEPS
prompt =====================================
prompt
create table REG_CASE_PROCESS_STEPS
(
  reg_case_process_id NUMBER(22) not null,
  reg_process_step_id NUMBER(22) not null,
  description         VARCHAR2(35) not null,
  order_num           NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CASE_PROCESS_STEPS
  is '(  ) [  ]
THE REG CASE PROCESS STEPS TABLE CONTAINS LIST OF STEPS WITHIN EACH MAJOR PROCESS FOR A REGULATORY CASE.';
comment on column REG_CASE_PROCESS_STEPS.reg_case_process_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF REGULATORY PROCESS.';
comment on column REG_CASE_PROCESS_STEPS.reg_process_step_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF STEP WITHIN A REGULATORY MODULE PROCESS.';
comment on column REG_CASE_PROCESS_STEPS.description
  is 'DESCRIPTION OF THE REGULATORY PROCESS STEP.';
comment on column REG_CASE_PROCESS_STEPS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_PROCESS_STEPS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_PROCESS_STEPS
  add constraint PK_REG_CASE_PROCESS_STEPS primary key (REG_CASE_PROCESS_ID, REG_PROCESS_STEP_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_PROCESS_STEPS
  add constraint R_REG_CASE_PROCESS_STEPS1 foreign key (REG_CASE_PROCESS_ID)
  references REG_CASE_PROCESS (REG_CASE_PROCESS_ID);

prompt
prompt Creating table REG_CASE_HISTORY
prompt ===============================
prompt
create table REG_CASE_HISTORY
(
  reg_history_id      NUMBER(22) not null,
  reg_case_id         NUMBER(22) not null,
  reg_case_process_id NUMBER(22) not null,
  reg_process_step_id NUMBER(22) not null,
  message             VARCHAR2(2000) not null,
  entry_date          TIMESTAMP(0) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CASE_HISTORY
  is '(  ) [  ]
THE REG CASE HISTORY TABLE CONTAINS RECORD OF USER ACTIONS FOR A CASE.  ACTIONS INCLUDE DATA CONFIGURATION CHANGES OR EXECUTIONS OF PROCESS STEPS.';
comment on column REG_CASE_HISTORY.reg_history_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR CASE HISTORY ENTRIES.';
comment on column REG_CASE_HISTORY.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE CASE.';
comment on column REG_CASE_HISTORY.reg_case_process_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF REGULATORY PROCESS.';
comment on column REG_CASE_HISTORY.reg_process_step_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF STEP WITHIN A REGULATORY MODULE PROCESS.';
comment on column REG_CASE_HISTORY.message
  is 'DETAILED MESSAGE FOR THE CASE HISTORY ENTRY.';
comment on column REG_CASE_HISTORY.entry_date
  is 'DATE THE CASE HISTORY ENTRY WAS RECORDED.';
comment on column REG_CASE_HISTORY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_HISTORY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_HISTORY
  add constraint PK_REG_CASE_HISTORY primary key (REG_HISTORY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_HISTORY
  add constraint R_REG_CASE_HISTORY1 foreign key (REG_CASE_PROCESS_ID)
  references REG_CASE_PROCESS (REG_CASE_PROCESS_ID);
alter table REG_CASE_HISTORY
  add constraint R_REG_CASE_HISTORY2 foreign key (REG_CASE_PROCESS_ID, REG_PROCESS_STEP_ID)
  references REG_CASE_PROCESS_STEPS (REG_CASE_PROCESS_ID, REG_PROCESS_STEP_ID);
alter table REG_CASE_HISTORY
  add constraint R_REG_CASE_HISTORY3 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);

prompt
prompt Creating table REG_CASE_INTEREST_SYNC_CONTROL
prompt =============================================
prompt
create table REG_CASE_INTEREST_SYNC_CONTROL
(
  reg_case_id          NUMBER(22) not null,
  capital_structure_id NUMBER(22) not null,
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_CASE_INTEREST_SYNC_CONTROL
  is '(  ) [  ]
THE REG CASE INTEREST SYNCE CONTROL TABLE LISTS THE CAPITAL STRUCTURE ITEMS (E.G. LONG TERM DEBT, SHORT TERM DEBT, CUSTOMER DEPOSITS, ETC.) THAT ARE CONSIDERED IN THE INTEREST SYNCHRONIZATION CALCULATION FOR A CASE.  THIS IS DEFAULTED FROM THE CAPITAL STRUCTURE INDICATOR TABLE BASED ON CAPITAL STRUCTURE ITEMS VALID FOR THE CASE.  IT CAN BE MODIFIED BY THE USER WITHIN A SPECIFIC CASE IF NECESSARY.  NOTE THAT THE BASE ITEM “LONG TERM DEBT” AMOUNT IS USED, NOT THE CORRESPONDING EXPENSE AMOUNTS.';
comment on column REG_CASE_INTEREST_SYNC_CONTROL.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CASE.';
comment on column REG_CASE_INTEREST_SYNC_CONTROL.capital_structure_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CAPITAL STRUCTURE ITEM (E.G. LONG TERM DEBT) USED IN THE INTEREST SYNCHRONIZATION CALCULATION FOR THIS JURISDICTIONAL TEMPLATE.';
comment on column REG_CASE_INTEREST_SYNC_CONTROL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_INTEREST_SYNC_CONTROL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_INTEREST_SYNC_CONTROL
  add constraint PK_REG_CASE_INTER_SYNC_CONTROL primary key (REG_CASE_ID, CAPITAL_STRUCTURE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_INTEREST_SYNC_CONTROL
  add constraint R_REG_CASE_INTER_SYNC_CONTROL1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_INTEREST_SYNC_CONTROL
  add constraint R_REG_CASE_INTER_SYNC_CONTROL2 foreign key (CAPITAL_STRUCTURE_ID)
  references REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID);

prompt
prompt Creating table REG_CASE_INTEREST_SYNC_RATE
prompt ==========================================
prompt
create table REG_CASE_INTEREST_SYNC_RATE
(
  reg_case_id        NUMBER(22) not null,
  case_year          NUMBER(22) not null,
  interest_sync_rate NUMBER(22,8) not null,
  weighted_avg_cofc  NUMBER(22,8) not null,
  user_id            VARCHAR2(18),
  time_stamp         DATE
)
;
alter table REG_CASE_INTEREST_SYNC_RATE
  add constraint PK_REG_CASE_INTEREST_SYNC_RATE primary key (REG_CASE_ID, CASE_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_INTEREST_SYNC_RATE
  add constraint R_REG_CASE_INTEREST_SYNC_RATE1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);

prompt
prompt Creating table REG_CASE_MONITOR_CONTROL
prompt =======================================
prompt
create table REG_CASE_MONITOR_CONTROL
(
  reg_case_id           NUMBER(22) not null,
  reg_return_tag_id     NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  reg_adjustment_ids    VARCHAR2(100),
  subtotal_name         VARCHAR2(35),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_CASE_MONITOR_CONTROL
  is '(  ) [  ]
THE REG CASE MONITOR CONTROL TABLE LINKS THE ALLOCATION STEPS TREE CATAGORIES, TARGETS, AND ADJMENTS TO THE RECONCILIATION OF MARKET EQUITY RETURNS TO JURISDICTIONAL RETURNS.';
comment on column REG_CASE_MONITOR_CONTROL.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE MONITORING CASE FOR THIS JURISDICTION.';
comment on column REG_CASE_MONITOR_CONTROL.description
  is 'THE DESCRIPTION OF THE RECONCILING ITEM OR TOTAL COMPANY OR ENDING JURISDICTIONAL BASE RATE';
comment on column REG_CASE_MONITOR_CONTROL.subtotal_name
  is 'NAME OF SUBTOTAL USED AFTER THE COLUMN, E.G. “UTILITY”, “ELECTRIC NORTH DAKOTA”.  NOTE THAT IF NOT NULL, THE EQUITY DENOMINATOR GETS RECOMPUTED     (SO THAT THE ORDER OTHER THAN FOUR PRESENTATION WILL NOT MATTER UNTIL THE NEXT SUBTOTAL.)';
alter table REG_CASE_MONITOR_CONTROL
  add constraint PK_REG_CASE_MONITOR_CONTROL primary key (REG_CASE_ID, REG_RETURN_TAG_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_MONITOR_CONTROL
  add constraint R_REG_CASE_MONITOR_CONTROL1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_MONITOR_CONTROL
  add constraint R_REG_CASE_MONITOR_CONTROL2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_MONITOR_CONTROL
  add constraint R_REG_CASE_MONITOR_CONTROL3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_PARAMETER
prompt ============================
prompt
create table REG_PARAMETER
(
  reg_parm_id  NUMBER(22) not null,
  description  VARCHAR2(35) not null,
  type_of_data VARCHAR2(35) not null,
  time_stamp   DATE,
  user_id      VARCHAR2(8)
)
;
alter table REG_PARAMETER
  add constraint PK_REG_PARAMETER primary key (REG_PARM_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CASE_PARAMETER
prompt =================================
prompt
create table REG_CASE_PARAMETER
(
  reg_parm_id NUMBER(22) not null,
  reg_case_id NUMBER(22) not null,
  parm_value  VARCHAR2(35) not null,
  user_id     VARCHAR2(8),
  time_stamp  DATE
)
;
alter table REG_CASE_PARAMETER
  add constraint PK_REG_CASE_PARAMETER primary key (REG_CASE_ID, REG_PARM_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_PARAMETER
  add constraint R_REG_CASE_PARAMETER1 foreign key (REG_PARM_ID)
  references REG_PARAMETER (REG_PARM_ID);
alter table REG_CASE_PARAMETER
  add constraint R_REG_CASE_PARAMETER2 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);

prompt
prompt Creating table REG_CASE_PROCESS_STATUS
prompt ======================================
prompt
create table REG_CASE_PROCESS_STATUS
(
  reg_case_id          NUMBER(22) not null,
  case_year            NUMBER(6) not null,
  reg_case_process_id  NUMBER(22) not null,
  prior_process_status NUMBER(22) not null,
  process_status       NUMBER(22) not null,
  run_id               NUMBER(22),
  run_date             TIMESTAMP(0),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_CASE_PROCESS_STATUS
  is '(  ) [  ]
THE REG CASE PROCESS STATUS TABLE TRACKS THE STATUS FOR MAJOR PROCESSES, E.G. ADJUSTMENTS AND ALLOCATION STEPS, WITHIN A CASE.  TWO SEPARATE STATUSES ARE TRACKED FOR EACH PROCESS, 1) THE DATA CONFIGURATION COMPLETENESS AND 2) THE PROCESS EXECUTION COMPLETENESS.';
comment on column REG_CASE_PROCESS_STATUS.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE CASE.';
comment on column REG_CASE_PROCESS_STATUS.case_year
  is 'END OF ''TEST'' YEAR IN ''YYYYMM'' FORMAT.';
comment on column REG_CASE_PROCESS_STATUS.reg_case_process_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF REGULATORY PROCESSES.';
comment on column REG_CASE_PROCESS_STATUS.prior_process_status
  is 'INDICATES THE STATUS OF THE PRIOR PROCESSING STEP:
1 = GREEN – THIS STEP CONSISTENT WITH PRIOR STEP RESULT
2 = YELLOW – THIS STEP NOT CONSISTENT WITH PRIOR STEP RESULT
3 = RED – PRIOR STEP RESULTS NOT AVAILABLE';
comment on column REG_CASE_PROCESS_STATUS.process_status
  is 'INDICATES THE STATUS OF THIS PROCESSING STEP:
1 = GREEN – ALL DATA IS POPULATED AND EXECUTION IS UP TO DATE
2 = YELLOW – ALL DATA IS POPULATED, BUT EXECUTION IS OUT OF DATE
3 = RED – DATA IS MISSING OR INVALID AND CANNOT EXECUTE';
comment on column REG_CASE_PROCESS_STATUS.run_id
  is 'SYSTEM GENERATED RUN IDENTIFIER FOR THE LAST STATUS SWEEP EXECUTION.';
comment on column REG_CASE_PROCESS_STATUS.run_date
  is 'SYSTEM DATE FOR THE LAST STATUS SWEEP EXECUTION.';
comment on column REG_CASE_PROCESS_STATUS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_PROCESS_STATUS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_PROCESS_STATUS
  add constraint PK_REG_CASE_PROCESS_STATUS primary key (REG_CASE_ID, CASE_YEAR, REG_CASE_PROCESS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_PROCESS_STATUS
  add constraint R_REG_CASE_PROCESS_STATUS1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_PROCESS_STATUS
  add constraint R_REG_CASE_PROCESS_STATUS2 foreign key (REG_CASE_PROCESS_ID)
  references REG_CASE_PROCESS (REG_CASE_PROCESS_ID);

prompt
prompt Creating table REG_CASE_RESULTS_TYPE
prompt ====================================
prompt
create table REG_CASE_RESULTS_TYPE
(
  reg_case_id           NUMBER(22) not null,
  result_type           NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  set_of_adjustments    VARCHAR2(60),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_CASE_RESULTS_TYPE
  is '(  ) [  ]
THE REG CASE RESULTS TYPE TABLE TELLS THE SYSTEM WHERE TO CALCULATE OVERALL AND COMMON EQUITY RETURNS AND SUPPORTING DATA TO BE PLACED ON THE REG_CASE_RESULTS TABLE .  THESE OPTIONS ARE DEFAULTED FROM THE REG_RESULTS_TYPE_TEMPLATE TABLE.';
comment on column REG_CASE_RESULTS_TYPE.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A JURISDICTIONAL CASE.';
comment on column REG_CASE_RESULTS_TYPE.description
  is 'VARCHAR 2(35)  DESCRIPTION OF THE REG RESULT TYPE FOR THIS JURISDICTION, E.G. ''OHIO BASE RATES.''';
comment on column REG_CASE_RESULTS_TYPE.reg_alloc_category_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF AN “ALLOCATION” CATEGORY OR LEVEL.';
comment on column REG_CASE_RESULTS_TYPE.reg_alloc_target_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF THE TARGET FOR WHICH THE RETURNS WILL BE CALCULATED.
SETS OF ADJUSTMENTS     THE RETURNS CAN BE CALCULATED WITH AND WITHOUT THESE SETS OF ADJUSTMENTS.  A NULL MEANS THE RETURNS ARE CALCULATED AS ADJUSTMENTS.  THE ADJUSTMENTS ARE THE ID''S OF COMMON DELIMITED.';
comment on column REG_CASE_RESULTS_TYPE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_RESULTS_TYPE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_RESULTS_TYPE
  add constraint PK_REG_CASE_RESULTS_TYPE primary key (REG_CASE_ID, RESULT_TYPE)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_RESULTS_TYPE
  add constraint R_REG_CASE_RESULTS_TYPE1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_RESULTS_TYPE
  add constraint R_REG_CASE_RESULTS_TYPE2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_RESULTS_TYPE
  add constraint R_REG_CASE_RESULTS_TYPE3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_CASE_RESULTS
prompt ===============================
prompt
create table REG_CASE_RESULTS
(
  reg_case_id              NUMBER(22) not null,
  month_year               NUMBER(6) not null,
  result_type              NUMBER(22) not null,
  interest_sync_rate       NUMBER(22,8),
  rate_base                NUMBER(22,2),
  rate_base_adjustment     NUMBER(22,2),
  oper_income              NUMBER(22,2),
  oper_income_adjustment   NUMBER(22,2),
  current_revenues         NUMBER(22,2),
  revenue_requirement      NUMBER(22,2),
  overall_return           NUMBER(22,8),
  common_equity_return     NUMBER(22,8),
  weighted_non_equity_rate NUMBER(22,8),
  percent_common_equity    NUMBER(22,8),
  user_id                  VARCHAR2(18),
  time_stamp               DATE
)
;
comment on table REG_CASE_RESULTS
  is '(  ) [  ]
THE REG CASE RESULTS TABLE CONTAINS THE KEY CALCULATED RETURN ITEMS OR REVENUE REQUIREMENTS FOR MONITORING AND ACTUAL CASES.  THE RESULTS ARE BY RESULT TYPE, WHICH CAN INDICATE CERTAIN CATEGORY LEVELS AND TARGETS WITH AND WITHOUT ADJUSTMENTS.  THIS IS SPECIFIED ON THE REG RESULTS TYPE TEMPLATE TABLE.';
comment on column REG_CASE_RESULTS.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF';
comment on column REG_CASE_RESULTS.interest_sync_rate
  is 'THE WEIGHTED DEBT RATE USED IN THE INTEREST SYNC CALCULATIONS.';
comment on column REG_CASE_RESULTS.rate_base
  is 'THIS IS THE RATE BASE BEFORE ADJUSTMENTS IN DOLLARS';
comment on column REG_CASE_RESULTS.rate_base_adjustment
  is 'THIS IS THE TOTAL DOLLAR AMOUNT FOR THE RATE BASE ADJUSTMENTS INDICATED FOR THIS RESULT TYPE.  INCLUDES ANY ASSOCIATED COMPILED INCOME TAX ADJUSTMENTS.';
comment on column REG_CASE_RESULTS.oper_income
  is 'OPERATING INCOME FOR THIS RESULT TYPE IN DOLLARS BEFORE ADJUSTMENTS.';
comment on column REG_CASE_RESULTS.current_revenues
  is 'HISTORIC OR FORECAST REVENUES FOR THIS RESULT TYPE AT EXISTING RATES IN DOLLARS';
comment on column REG_CASE_RESULTS.overall_return
  is 'OVERALL (WEIGHTED OVERALL CAPITAL COMPONENTS – DEBT, EQUITY, ETC.) EXPRESSED AS A DECIMAL.';
comment on column REG_CASE_RESULTS.weighted_non_equity_rate
  is 'WEIGHTED COST OF CAPITAL FOR ALL NON-COMMON EQUITY CAPITAL COMPONENTS.';
comment on column REG_CASE_RESULTS.percent_common_equity
  is 'PERCENT COMMON EQUITY USED IN THE RETURN CALCULATIONS.';
comment on column REG_CASE_RESULTS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_RESULTS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_RESULTS
  add constraint PK_REG_CASE_RESULTS primary key (REG_CASE_ID, MONTH_YEAR, RESULT_TYPE)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_RESULTS
  add constraint R_REG_CASE_RESULTS1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_RESULTS
  add constraint R_REG_CASE_RESULTS2 foreign key (REG_CASE_ID, RESULT_TYPE)
  references REG_CASE_RESULTS_TYPE (REG_CASE_ID, RESULT_TYPE);

prompt
prompt Creating table REG_FIN_MONITOR_KEY
prompt ==================================
prompt
create table REG_FIN_MONITOR_KEY
(
  reg_fin_monitor_key_id NUMBER(22) not null,
  description            VARCHAR2(35) not null,
  process_flag           NUMBER(22) not null,
  parent_key             NUMBER(22) not null,
  key_level              NUMBER(22) not null,
  sort_order             NUMBER(22) not null,
  user_id                VARCHAR2(18),
  time_stamp             DATE
)
;
alter table REG_FIN_MONITOR_KEY
  add constraint PK_REG_FIN_MONITOR_KEY primary key (REG_FIN_MONITOR_KEY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CASE_RETURN_RESULT
prompt =====================================
prompt
create table REG_CASE_RETURN_RESULT
(
  reg_case_id            NUMBER(22) not null,
  reg_return_tag_id      NUMBER(22) not null,
  reg_fin_monitor_key_id NUMBER(22) not null,
  month_year             NUMBER(6) not null,
  key_value              NUMBER(22,8) not null,
  user_id                VARCHAR2(18),
  time_stamp             DATE
)
;
alter table REG_CASE_RETURN_RESULT
  add constraint PK_REG_CASE_RETURN_RESULT primary key (REG_CASE_ID, REG_RETURN_TAG_ID, REG_FIN_MONITOR_KEY_ID, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_RETURN_RESULT
  add constraint R_REG_CASE_RETURN_RESULT1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_RETURN_RESULT
  add constraint R_REG_CASE_RETURN_RESULT2 foreign key (REG_CASE_ID, REG_RETURN_TAG_ID)
  references REG_CASE_MONITOR_CONTROL (REG_CASE_ID, REG_RETURN_TAG_ID);
alter table REG_CASE_RETURN_RESULT
  add constraint R_REG_CASE_RETURN_RESULT3 foreign key (REG_FIN_MONITOR_KEY_ID)
  references REG_FIN_MONITOR_KEY (REG_FIN_MONITOR_KEY_ID);

prompt
prompt Creating table REG_FIN_MONITOR_SUMMARY
prompt ======================================
prompt
create table REG_FIN_MONITOR_SUMMARY
(
  reg_fin_monitor_summ_id NUMBER(22) not null,
  description             VARCHAR2(35) not null,
  reg_fin_monitor_key_id  NUMBER(22) not null,
  sort_order              NUMBER(22) not null,
  user_id                 VARCHAR2(18),
  time_stamp              DATE
)
;
alter table REG_FIN_MONITOR_SUMMARY
  add constraint PK_REG_FIN_MONITOR_SUMMARY primary key (REG_FIN_MONITOR_SUMM_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FIN_MONITOR_SUMMARY
  add constraint R_REG_FIN_MONITOR_SUMMARY1 foreign key (REG_FIN_MONITOR_KEY_ID)
  references REG_FIN_MONITOR_KEY (REG_FIN_MONITOR_KEY_ID);

prompt
prompt Creating table REG_CASE_RETURN_RESULT_SUMMARY
prompt =============================================
prompt
create table REG_CASE_RETURN_RESULT_SUMMARY
(
  reg_case_id             NUMBER(22) not null,
  reg_return_tag_id       NUMBER(22) not null,
  reg_fin_monitor_key_id  NUMBER(22) not null,
  reg_fin_monitor_summ_id NUMBER(22) not null,
  month_year              NUMBER(6) not null,
  key_value               NUMBER(22,8) not null,
  user_id                 VARCHAR2(18),
  time_stamp              DATE
)
;
alter table REG_CASE_RETURN_RESULT_SUMMARY
  add constraint PK_REG_CASE_RETURN_RESULT_SUMM primary key (REG_CASE_ID, REG_RETURN_TAG_ID, REG_FIN_MONITOR_SUMM_ID, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_RETURN_RESULT_SUMMARY
  add constraint R_REG_CASE_RETURN_RESULT_SUMM1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_RETURN_RESULT_SUMMARY
  add constraint R_REG_CASE_RETURN_RESULT_SUMM2 foreign key (REG_CASE_ID, REG_RETURN_TAG_ID)
  references REG_CASE_MONITOR_CONTROL (REG_CASE_ID, REG_RETURN_TAG_ID);
alter table REG_CASE_RETURN_RESULT_SUMMARY
  add constraint R_REG_CASE_RETURN_RESULT_SUMM3 foreign key (REG_FIN_MONITOR_SUMM_ID)
  references REG_FIN_MONITOR_SUMMARY (REG_FIN_MONITOR_SUMM_ID);

prompt
prompt Creating table REG_CASE_REV_REQ_RESULTS
prompt =======================================
prompt
create table REG_CASE_REV_REQ_RESULTS
(
  reg_case_id            NUMBER(22) not null,
  case_year_end          NUMBER(22) not null,
  rate_base              NUMBER(22,2),
  overall_return         NUMBER(22,8),
  user_id                VARCHAR2(18),
  time_stamp             DATE,
  taxes                  NUMBER(22,2),
  operating_expense      NUMBER(22,2),
  depreciation           NUMBER(22,2),
  revenue_allowed        NUMBER(22,2),
  noi_allowed            NUMBER(22,2),
  revenue_current        NUMBER(22,2),
  noi_current            NUMBER(22,2),
  noi_change             NUMBER(22,2),
  revenue_grossup_factor NUMBER(22,2),
  revenue_impact         NUMBER(22,2)
)
;
alter table REG_CASE_REV_REQ_RESULTS
  add constraint PK_REG_CASE_REV_REQ_RESULTS primary key (REG_CASE_ID, CASE_YEAR_END)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_STATUS_CHECK
prompt ===============================
prompt
create table REG_STATUS_CHECK
(
  reg_status_check_id NUMBER(22) not null,
  reg_case_process_id NUMBER(22) not null,
  reg_process_step_id NUMBER(22) not null,
  process_status      NUMBER(22) not null,
  check_sql           VARCHAR2(2000),
  detail_field        VARCHAR2(35) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  description         VARCHAR2(254),
  status_code_id      NUMBER(22) not null
)
;
alter table REG_STATUS_CHECK
  add constraint PK_REG_STATUS_CHECK primary key (REG_STATUS_CHECK_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_STATUS_CHECK
  add constraint R_REG_STATUS_CHECK1 foreign key (REG_CASE_PROCESS_ID)
  references REG_CASE_PROCESS (REG_CASE_PROCESS_ID);
alter table REG_STATUS_CHECK
  add constraint R_REG_STATUS_CHECK2 foreign key (REG_CASE_PROCESS_ID, REG_PROCESS_STEP_ID)
  references REG_CASE_PROCESS_STEPS (REG_CASE_PROCESS_ID, REG_PROCESS_STEP_ID);
alter table REG_STATUS_CHECK
  add constraint R_REG_STATUS_CHECK3 foreign key (STATUS_CODE_ID)
  references STATUS_CODE (STATUS_CODE_ID);

prompt
prompt Creating table REG_CASE_STATUS_DETAIL_ADJUST
prompt ============================================
prompt
create table REG_CASE_STATUS_DETAIL_ADJUST
(
  reg_case_status_id  NUMBER(22) not null,
  reg_case_id         NUMBER(22) not null,
  case_year           NUMBER(6) not null,
  reg_case_process_id NUMBER(22) not null,
  adjustment_id       NUMBER(22),
  reg_alloc_acct_id   NUMBER(22),
  reg_status_check_id NUMBER(22) not null,
  process_status      NUMBER(22) not null,
  run_id              NUMBER(22),
  run_date            DATE,
  message             VARCHAR2(2000),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CASE_STATUS_DETAIL_ADJUST
  is '(  ) [  ]
THE REG CASE STATUS DETAIL ADJUST TABLE CONTAINS DETAIL ENTRIES, E.G. PER ADJUSTMENT, FOR ANYTHING CAUSING AN ADJUSTMENT PROCESS TO HAVE A YELLOW OR RED STATUS.  NOTE THAT RESULTS FOR TOTAL COMPANY ADJUSTMENTS, JURISDICTIONAL ADJUSTMENTS AND RECOVERY CLASS ADJUSTMENTS WILL ALL BE RECORDED IN THIS TABLE.  ADJUSTMENT CONFIGURATION WITH GREEN STATUS WILL NOT BE RECORDED.
REG_CASE_STATUS_ID
REG_CASE_ID
CASE_YEAR
REG_CASE_PROCESS_ID
ADJUSTMENT_ID';
alter table REG_CASE_STATUS_DETAIL_ADJUST
  add constraint PK_REG_CASE_STATUS_DETAIL_ADJ primary key (REG_CASE_STATUS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_STATUS_DETAIL_ADJUST
  add constraint R_REG_CASE_STATUS_DETAIL_ADJ1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_STATUS_DETAIL_ADJUST
  add constraint R_REG_CASE_STATUS_DETAIL_ADJ2 foreign key (REG_CASE_PROCESS_ID)
  references REG_CASE_PROCESS (REG_CASE_PROCESS_ID);
alter table REG_CASE_STATUS_DETAIL_ADJUST
  add constraint R_REG_CASE_STATUS_DETAIL_ADJ3 foreign key (REG_STATUS_CHECK_ID)
  references REG_STATUS_CHECK (REG_STATUS_CHECK_ID);

prompt
prompt Creating table REG_CASE_STATUS_DETAIL_ALLOC
prompt ===========================================
prompt
create table REG_CASE_STATUS_DETAIL_ALLOC
(
  reg_case_status_id    NUMBER(22) not null,
  reg_case_id           NUMBER(22) not null,
  case_year             NUMBER(6) not null,
  reg_case_process_id   NUMBER(22) not null,
  reg_alloc_acct_id     NUMBER(22),
  reg_alloc_category_id NUMBER(22),
  reg_allocator_id      NUMBER(22),
  reg_status_check_id   NUMBER(22) not null,
  process_status        NUMBER(22) not null,
  run_id                NUMBER(22),
  run_date              DATE,
  message               VARCHAR2(2000),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_CASE_STATUS_DETAIL_ALLOC
  is '(  ) [  ]
THE REG CASE STATUS DETAIL ALLOC TABLE CONTAINS DETAIL ENTRIES, E.G. PER ALLOCATION ACCOUNT, FOR ANYTHING CAUSING AN ALLOCATION PROCESS TO HAVE A YELLOW OR RED STATUS.  NOTE THAT RESULTS FOR JURISDICTIONAL ALLOCATIONS, RECOVERY CLASS ALLOCATIONS AND CUSTOMER CLASS ALLOCATIONS WILL ALL BE RECORDED IN THIS TABLE.  ALLOCATION CONFIGURATION WITH GREEN STATUS WILL NOT BE RECORDED.
REG_CASE_STATUS_ID
REG_CASE_ID
CASE_YEAR
REG_CASE_PROCESS_ID
REG_ALLOC_ACCT_ID
REG_ALLOC_CATEGORY_ID';
alter table REG_CASE_STATUS_DETAIL_ALLOC
  add constraint PK_REG_CASE_STATUS_DTL_ALLOC primary key (REG_CASE_STATUS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_STATUS_DETAIL_ALLOC
  add constraint R_REG_CASE_STATUS_DTL_ALLOC1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_STATUS_DETAIL_ALLOC
  add constraint R_REG_CASE_STATUS_DTL_ALLOC2 foreign key (REG_CASE_PROCESS_ID)
  references REG_CASE_PROCESS (REG_CASE_PROCESS_ID);
alter table REG_CASE_STATUS_DETAIL_ALLOC
  add constraint R_REG_CASE_STATUS_DTL_ALLOC3 foreign key (REG_STATUS_CHECK_ID)
  references REG_STATUS_CHECK (REG_STATUS_CHECK_ID);

prompt
prompt Creating table REG_CASE_STATUS_DETAIL_CALC
prompt ==========================================
prompt
create table REG_CASE_STATUS_DETAIL_CALC
(
  reg_case_status_id  NUMBER(22) not null,
  reg_case_id         NUMBER(22) not null,
  case_year           NUMBER(6) not null,
  reg_case_process_id NUMBER(22) not null,
  reg_status_check_id NUMBER(22) not null,
  process_status      NUMBER(22) not null,
  run_id              NUMBER(22),
  run_date            DATE,
  message             VARCHAR2(2000),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_CASE_STATUS_DETAIL_CALC
  add constraint PK_REG_CASE_STATUS_DETAIL_CALC primary key (REG_CASE_STATUS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_STATUS_DETAIL_CALC
  add constraint R_REG_CASE_STATUS_DETAIL_CALC1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_STATUS_DETAIL_CALC
  add constraint R_REG_CASE_STATUS_DETAIL_CALC2 foreign key (REG_CASE_PROCESS_ID)
  references REG_CASE_PROCESS (REG_CASE_PROCESS_ID);
alter table REG_CASE_STATUS_DETAIL_CALC
  add constraint R_REG_CASE_STATUS_DETAIL_CALC3 foreign key (REG_STATUS_CHECK_ID)
  references REG_STATUS_CHECK (REG_STATUS_CHECK_ID);

prompt
prompt Creating table REG_CASE_STATUS_DETAIL_LEDGER
prompt ============================================
prompt
create table REG_CASE_STATUS_DETAIL_LEDGER
(
  reg_case_status_id  NUMBER(22) not null,
  reg_case_id         NUMBER(22) not null,
  case_year           NUMBER(6) not null,
  reg_case_process_id NUMBER(22) not null,
  reg_status_check_id NUMBER(22) not null,
  process_status      NUMBER(22) not null,
  run_id              NUMBER(22),
  run_date            DATE,
  message             VARCHAR2(2000),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CASE_STATUS_DETAIL_LEDGER
  is '(  ) [  ]
THE REG CASE STATUS DETAIL LEDGER TABLE CONTAINS DETAIL ENTRIES, E.G. PER, FOR ANYTHING CAUSING A LEDGER PROCESS TO HAVE A YELLOW OR RED STATUS.  NOTE THAT RESULTS FOR ALL HISTORIC AND FORECAST INTERFACES WILL BE RECORDED IN THIS TABLE.  LEDGER CONFIGURATION WITH GREEN STATUS WILL NOT BE RECORDED.
REG_CASE_STATUS_ID
REG_CASE_ID
CASE_YEAR
REG_CASE_PROCESS_ID
REG_STATUS_CHECK_ID
PROCESS_STATUS
RUN_ID
RUN_DATE';
alter table REG_CASE_STATUS_DETAIL_LEDGER
  add constraint PK_REG_CASE_STATUS_DETAIL_LED primary key (REG_CASE_STATUS_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_STATUS_DETAIL_LEDGER
  add constraint R_REG_CASE_STATUS_DETAIL_LED1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_STATUS_DETAIL_LEDGER
  add constraint R_REG_CASE_STATUS_DETAIL_LED2 foreign key (REG_CASE_PROCESS_ID)
  references REG_CASE_PROCESS (REG_CASE_PROCESS_ID);
alter table REG_CASE_STATUS_DETAIL_LEDGER
  add constraint R_REG_CASE_STATUS_DETAIL_LED3 foreign key (REG_STATUS_CHECK_ID)
  references REG_STATUS_CHECK (REG_STATUS_CHECK_ID);

prompt
prompt Creating table REG_CASE_SUMMARY_LEDGER
prompt ======================================
prompt
create table REG_CASE_SUMMARY_LEDGER
(
  reg_case_id               NUMBER(22) not null,
  reg_acct_id               NUMBER(22) not null,
  case_year                 NUMBER(22) not null,
  total_co_ledger_amount    NUMBER(22,2) not null,
  total_co_included_amount  NUMBER(22,2) not null,
  total_co_adjust_amount    NUMBER(22,2) not null,
  total_co_final_amount     NUMBER(22,2) not null,
  jur_allo_percent          NUMBER(22,16) not null,
  jur_allo_results_amount   NUMBER(22,2) not null,
  jur_adjust_amount         NUMBER(22,2) not null,
  jur_final_amount          NUMBER(22,2) not null,
  jur_recovery_class_amount NUMBER(22,2) not null,
  jur_base_rev_req_amount   NUMBER(22,2) not null,
  user_id                   VARCHAR2(18),
  time_stamp                DATE
)
;
comment on table REG_CASE_SUMMARY_LEDGER
  is '(  )  [  ]
THE REG CASE SUMMARY LEDGER MAINTAINS THE BASIC DOLLAR RESULTS DATA AND RESULTS AS A CASE MOVES FORWARD.';
comment on column REG_CASE_SUMMARY_LEDGER.reg_case_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A CASE.';
comment on column REG_CASE_SUMMARY_LEDGER.reg_acct_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A REGULATORY ACCOUNT.';
comment on column REG_CASE_SUMMARY_LEDGER.case_year
  is 'END OF ''TEST'' YEAR IN ''YYYYMM'' FORMAT.';
comment on column REG_CASE_SUMMARY_LEDGER.total_co_ledger_amount
  is 'TOTAL COMPANY AMOUNTS ON THE LEDGER';
comment on column REG_CASE_SUMMARY_LEDGER.total_co_included_amount
  is 'TOTAL COMPANY AMOUNT AFTER EXCLUDING ACCOUNTS';
comment on column REG_CASE_SUMMARY_LEDGER.total_co_adjust_amount
  is 'TOTAL COMPANY ADJUSTMENTS';
comment on column REG_CASE_SUMMARY_LEDGER.total_co_final_amount
  is 'FINAL COMPANY AMOUNT TO BE ALLOCATED';
comment on column REG_CASE_SUMMARY_LEDGER.jur_allo_percent
  is 'RESULTANT ALLOCATION % BETWEEN COMPANY AND JURISDICTIONAL RESULT';
comment on column REG_CASE_SUMMARY_LEDGER.jur_allo_results_amount
  is 'TOTAL JURISDICTIONAL RESULTS (INCLUDING CLAUSES, ETC)';
comment on column REG_CASE_SUMMARY_LEDGER.jur_adjust_amount
  is 'TOTAL ADJUSTMENTS APPLIED AT THE JURISDICTIONAL LEVEL';
comment on column REG_CASE_SUMMARY_LEDGER.jur_final_amount
  is 'JURISIDICTIONAL FINAL AMOUNTS (INCLUDING CLAUSES, ETC)';
comment on column REG_CASE_SUMMARY_LEDGER.jur_recovery_class_amount
  is 'JURISDICTIONAL AMOUNTS FOR THE SUM OF NON-BASE RATE RECOVERY CLASSES AFTER ADJUSTMENTS.';
comment on column REG_CASE_SUMMARY_LEDGER.jur_base_rev_req_amount
  is 'JURISDICTIONAL AMOUNTS FOR BASE RATES, AFTER ADJUSTMENTS.';
comment on column REG_CASE_SUMMARY_LEDGER.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_SUMMARY_LEDGER.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_SUMMARY_LEDGER
  add constraint PK_REG_CASE_SUMMARY_LEDGER primary key (REG_CASE_ID, REG_ACCT_ID, CASE_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_SUMMARY_LEDGER
  add constraint R_REG_CASE_SUMMARY_LEDGER1 foreign key (REG_CASE_ID, REG_ACCT_ID)
  references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);

prompt
prompt Creating table REG_CASE_TARGET_FORWARD
prompt ======================================
prompt
create table REG_CASE_TARGET_FORWARD
(
  reg_case_id           NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  carry_forward_dollars NUMBER(22) not null,
  create_alloc_accts    NUMBER(22) not null,
  targets_major_minor   NUMBER(22),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_CASE_TARGET_FORWARD
  is '(  ) [  ]
THE REG CASE TARGET FORWARD TABLE DESCRIBES HOW ALLOCATION RESULTS (TARGETS) ARE TO BE USED IN THE NEXT LEVEL OF ALLOCATION.';
comment on column REG_CASE_TARGET_FORWARD.reg_case_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF A CASE.';
comment on column REG_CASE_TARGET_FORWARD.reg_alloc_category_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF AN ALLOCATION LEVEL.';
comment on column REG_CASE_TARGET_FORWARD.reg_alloc_target_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF AN ALLOCATION TARGET.';
comment on column REG_CASE_TARGET_FORWARD.carry_forward_dollars
  is '1 = THE DOLLARS CARRY FORWARD
2 = THE DOLLARS DO NOT CARRY FORWARD E.G. IF IT WERE AN ELECTRIC CASE, AFTER THE SPLIT TO ELECTRIC AND GAS THE ELECTRIC WOULD CARRY FORWARD, BUT NOT THE GAS.';
comment on column REG_CASE_TARGET_FORWARD.create_alloc_accts
  is '1 = CREATE ALLOC ACCOUNTS, (SUBACCOUNTS,) USING THE TARGETS
2 = DON''T CREATE ALLOC ACCOUNTS';
comment on column REG_CASE_TARGET_FORWARD.targets_major_minor
  is 'IF SUBACCOUNTS ARE CREATED, ARE THEY:
1:  MAJOR – SORTED HIGH – E.G. THE REG ACCOUNTS ARE BELOW THE TARGET.
2:  MINOR – SORTED LOW – THE TARGETS ARE UNDER EACH REG ACCOUNT.
3:  THE TARGETS REPLACE THE REGULATORY ACCOUNTS, (E.G. THE REG ACCOUNTS ARE FULLY SUMMED UNDER EACH TARGET.)';
comment on column REG_CASE_TARGET_FORWARD.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CASE_TARGET_FORWARD.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CASE_TARGET_FORWARD
  add constraint PK_REG_CASE_TARGET_FORWARD primary key (REG_CASE_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CASE_TARGET_FORWARD
  add constraint R_REG_CASE_TARGET_FORWARD1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_CASE_TARGET_FORWARD
  add constraint R_REG_CASE_TARGET_FORWARD2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_CASE_TARGET_FORWARD
  add constraint R_REG_CASE_TARGET_FORWARD3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_CHART
prompt ========================
prompt
create table REG_CHART
(
  chart_id         NUMBER(22) not null,
  description      VARCHAR2(35) not null,
  long_description VARCHAR2(254),
  url              VARCHAR2(2000),
  sql_command      VARCHAR2(2000),
  time_stamp       DATE,
  user_id          VARCHAR2(18)
)
;
alter table REG_CHART
  add constraint PK_REG_CHART primary key (CHART_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_COMPANY_SECURITY
prompt ===================================
prompt
create table REG_COMPANY_SECURITY
(
  valid_user_id    VARCHAR2(18) not null,
  valid_company_id NUMBER(22) not null,
  user_id          VARCHAR2(18),
  time_stamp       DATE
)
;
alter table REG_COMPANY_SECURITY
  add constraint PK_REG_COMPANY_SECURITY primary key (VALID_USER_ID, VALID_COMPANY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_COMPANY_SECURITY
  add constraint R_REG_COMPANY_SECURITY1 foreign key (VALID_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);

prompt
prompt Creating table REG_CR_PULL_TYPE
prompt ===============================
prompt
create table REG_CR_PULL_TYPE
(
  pull_type_id     NUMBER(22) not null,
  description      VARCHAR2(35) not null,
  long_description VARCHAR2(254),
  default_amt_type VARCHAR2(10),
  user_id          VARCHAR2(18),
  time_stamp       DATE
)
;
comment on table REG_CR_PULL_TYPE
  is '(  )  [  ]
THE REG CR PULL TYPE TABLE IDENTIFIES HOW TO PULL DATA FROM CR, I.E. MONTHLY BALANCE VS. MONTHLY ACTIVITY';
comment on column REG_CR_PULL_TYPE.pull_type_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A PULL TYPE';
comment on column REG_CR_PULL_TYPE.description
  is 'DESCRIPTION O FTHE TYPE, E.G. ''BALANCES'', ''ACTIVITY''';
comment on column REG_CR_PULL_TYPE.long_description
  is 'LONG DESCRIPTION OF THE TYPE';
comment on column REG_CR_PULL_TYPE.default_amt_type
  is 'USED TO DEFAULT THE CR_ACCT_TYPE ON REG_CR_COMBOS';
comment on column REG_CR_PULL_TYPE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_PULL_TYPE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_PULL_TYPE
  add constraint PK_REG_CR_PULL_TYPE primary key (PULL_TYPE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CR_TABLES
prompt ============================
prompt
create table REG_CR_TABLES
(
  cr_table_id NUMBER(22) not null,
  description VARCHAR2(35) not null,
  user_id     VARCHAR2(18),
  time_stamp  DATE
)
;
comment on table REG_CR_TABLES
  is '(  )  [  ]
THE REG CR TABLES LISTS ALL CR TABLES THAT STORE TRANSACTIONS INCLUDING CR_BALANCE AND CR_COST_REPOSITORY';
comment on column REG_CR_TABLES.cr_table_id
  is '100 = CR BALANCES, 101 = CR_COST_REPOSITORY, ELSE = SOURCE_ID FROM CR_SOURCES, 200 = CR BUDGET BALANCES, 201 = CR BUDGET DATA';
comment on column REG_CR_TABLES.description
  is 'DESCRIPTION, E.G. “CR BALANCES”';
comment on column REG_CR_TABLES.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_TABLES.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_TABLES
  add constraint PK_REG_CR_TABLES primary key (CR_TABLE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CR_COMBOS
prompt ============================
prompt
create table REG_CR_COMBOS
(
  cr_combo_id   NUMBER(22) not null,
  description   VARCHAR2(35) not null,
  cr_table_id   NUMBER(22) not null,
  pull_type_id  NUMBER(22) not null,
  cr_amt_type   VARCHAR2(10),
  user_id       VARCHAR2(18),
  time_stamp    DATE,
  cr_company    VARCHAR2(35),
  hist_combo_id NUMBER(22)
)
;
comment on table REG_CR_COMBOS
  is '(  )  [  ]
THE REG CR COMBOS TABLE MAINTAINS THE “HEADER” RECORDS THAT KEEPS TRACK OF SETS OF CR DATA (COMPANY, RANGE OR SET OF ACCOUNTS, OTHER QUALIFIERS SUCH AS DEPARTMENT), I.E. THE “CR COMBO” AND WHICH CR TABLE TO PULL FROM AND HOW TO PULL THE DATA (I.E. BALANCE VS. ACTIVITY).';
comment on column REG_CR_COMBOS.cr_combo_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A “CR COMBO”';
comment on column REG_CR_COMBOS.description
  is 'DESCRIPTION OF CR COMBO';
comment on column REG_CR_COMBOS.cr_table_id
  is 'CR TABLE TO PULL FROM';
comment on column REG_CR_COMBOS.pull_type_id
  is 'TYPE OF PULL, I.E. BALANCE VS. ACTIVITY';
comment on column REG_CR_COMBOS.cr_amt_type
  is 'SEPARATED LIST OF AMOUNT TYPE IDS FROM CR';
comment on column REG_CR_COMBOS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS.cr_company
  is 'THE COMPANY VALUE FROM THE CR USED FOR THE MAPPING.';
comment on column REG_CR_COMBOS.hist_combo_id
  is 'POPULATED ON CR COMBOS USED FOR THE FORECAST INTERFACE TO INDICATE THAT THEY WERE AUTOMAPPED WITH THE SPECIFIED CR COMBO ID FROM THE HISTORIC MAPPINGS SO THAT THE AUTOMAPPING ONLY OCCURS ONCE.';
alter table REG_CR_COMBOS
  add constraint PK_REG_CR_COMBOS primary key (CR_COMBO_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_COMBOS
  add constraint R_REG_CR_COMBOS1 foreign key (CR_TABLE_ID)
  references REG_CR_TABLES (CR_TABLE_ID);
alter table REG_CR_COMBOS
  add constraint R_REG_CR_COMBOS2 foreign key (PULL_TYPE_ID)
  references REG_CR_PULL_TYPE (PULL_TYPE_ID);

prompt
prompt Creating table REG_CR_COMBOS_ELEMENTS
prompt =====================================
prompt
create table REG_CR_COMBOS_ELEMENTS
(
  cr_element_id        NUMBER(22) not null,
  description          VARCHAR2(35) not null,
  cr_account_field     NUMBER(22),
  element_order        NUMBER(22) not null,
  default_structure_id NUMBER(22),
  user_id              VARCHAR2(18),
  time_stamp           DATE,
  cr_company_field     NUMBER(22)
)
;
comment on table REG_CR_COMBOS_ELEMENTS
  is '(  )  [  ]
THE REG CR COMBOS ELEMENTS TABLE LISTS THE ELEMENTS AVAILABLE TO BE USED FOR BUILDING CR COMBOS.';
comment on column REG_CR_COMBOS_ELEMENTS.cr_element_id
  is 'SAME AS ELEMENT ID ON CR_ELEMENTS';
comment on column REG_CR_COMBOS_ELEMENTS.description
  is 'DESCRIPTION OF THE CR ELEMENT';
comment on column REG_CR_COMBOS_ELEMENTS.cr_account_field
  is 'SAME AS FIELD NOTED IN FERC AMOUNT FIELD CR SYSTEM CONTROL';
comment on column REG_CR_COMBOS_ELEMENTS.element_order
  is 'DISPLAY ORDER';
comment on column REG_CR_COMBOS_ELEMENTS.default_structure_id
  is 'STRUCTURE ID USED TO KEEP TRACK OF LISTS OF VALUES';
comment on column REG_CR_COMBOS_ELEMENTS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS_ELEMENTS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS_ELEMENTS.cr_company_field
  is 'SAME AS FIELD NOTED IN COMPANY CR SYSTEM CONTROL';
alter table REG_CR_COMBOS_ELEMENTS
  add constraint PK_REG_CR_COMBOS_ELEMENTS primary key (CR_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_COMBOS_ELEMENTS
  add constraint R_REG_CR_COMBOS_ELEMENTS1 foreign key (CR_ELEMENT_ID)
  references CR_ELEMENTS (ELEMENT_ID);

prompt
prompt Creating table REG_CR_COMBOS_ELEMENTS_BDG
prompt =========================================
prompt
create table REG_CR_COMBOS_ELEMENTS_BDG
(
  cr_element_id        NUMBER(22) not null,
  description          VARCHAR2(35) not null,
  cr_account_field     NUMBER(22),
  cr_company_field     NUMBER(22),
  element_order        NUMBER(22) not null,
  default_structure_id NUMBER(22),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_CR_COMBOS_ELEMENTS_BDG
  is '(  )  [  ]
THE REG CR COMBOS ELEMENTS BDG TABLE LISTS THE ELEMENTS AVAILABLE TO BE USED FOR BUILDING CR COMBOS FOR THE O&M FORECAST INTERFACE.';
comment on column REG_CR_COMBOS_ELEMENTS_BDG.cr_element_id
  is 'SAME AS ELEMENT ID ON CR_ELEMENTS';
comment on column REG_CR_COMBOS_ELEMENTS_BDG.description
  is 'DESCRIPTION OF THE CR ELEMENT';
comment on column REG_CR_COMBOS_ELEMENTS_BDG.cr_account_field
  is 'SAME AS FIELD NOTED IN FERC AMOUNT FIELD CR SYSTEM CONTROL';
comment on column REG_CR_COMBOS_ELEMENTS_BDG.cr_company_field
  is 'SAME AS FIELD NOTED IN COMPANY CR SYSTEM CONTROL';
comment on column REG_CR_COMBOS_ELEMENTS_BDG.element_order
  is 'DISPLAY ORDER';
comment on column REG_CR_COMBOS_ELEMENTS_BDG.default_structure_id
  is 'STRUCTURE ID USED TO KEEP TRACK OF LISTS OF VALUES';
comment on column REG_CR_COMBOS_ELEMENTS_BDG.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS_ELEMENTS_BDG.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_COMBOS_ELEMENTS_BDG
  add constraint PK_REG_CR_COMBOS_ELEMENTS_BDG primary key (CR_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_COMBOS_ELEMENTS_BDG
  add constraint R_REG_CR_COMBOS_ELEMENTS_BDG1 foreign key (CR_ELEMENT_ID)
  references CR_ELEMENTS (ELEMENT_ID);

prompt
prompt Creating table REG_CR_COMBOS_FIELDS
prompt ===================================
prompt
create table REG_CR_COMBOS_FIELDS
(
  cr_combo_id         NUMBER(22) not null,
  historic_version_id NUMBER(22) not null,
  cr_element_id       NUMBER(22) not null,
  cr_source_id        NUMBER(22) not null,
  cr_field_id         NUMBER(22) not null,
  structure_id        NUMBER(22),
  not_indicator       NUMBER(22),
  lower_value         VARCHAR2(77),
  upper_value         VARCHAR2(77),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CR_COMBOS_FIELDS
  is '(  )  [  ]
THE REG CR COMBOS FIELDS TABLE CONTAINS MAPPINGS PER ELEMENT, DETAILED ATTRIBUTE FOR A GIVEN CR COMBO';
comment on column REG_CR_COMBOS_FIELDS.cr_combo_id
  is 'SYSTEM ASSIGNED';
comment on column REG_CR_COMBOS_FIELDS.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR A HISTORIC VERSION';
comment on column REG_CR_COMBOS_FIELDS.cr_element_id
  is '0 – MAPPING IS FOR DETAILED ATTRIBUTE, OTHERWISE MATCHES ELEMENT ID';
comment on column REG_CR_COMBOS_FIELDS.cr_source_id
  is '0 – MAPPING IS FOR ELEMENT, OTHERWISE IS REAL SOURCE_ID';
comment on column REG_CR_COMBOS_FIELDS.cr_field_id
  is '0 – MAPPING FOR ELEMENT, OTHERWISE IS REAL FIELD_ID';
comment on column REG_CR_COMBOS_FIELDS.structure_id
  is 'POPULATED WITH A STRUCTURE_ID FROM THE CR IF THE COMBO CONSISTS OF A LIST, NULL OTHERWISE';
comment on column REG_CR_COMBOS_FIELDS.not_indicator
  is 'INDICATES WHETHER THE ''NOT'' KEYWORD SHOULD BE INCLUDED WHEN BUILDING THE WHERE CLAUSE. 0 – DO NOT INCLUDE, 1 – INCLUDE.';
comment on column REG_CR_COMBOS_FIELDS.lower_value
  is 'LOWER VALUE IN RANGE, SINGLE VALUE, OR STRUCTURE POINT';
comment on column REG_CR_COMBOS_FIELDS.upper_value
  is 'UPPER VALUE IN RANGE, NULL OTHERWISE';
comment on column REG_CR_COMBOS_FIELDS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS_FIELDS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_COMBOS_FIELDS
  add constraint PK_REG_CR_COMBOS_FIELDS primary key (CR_COMBO_ID, HISTORIC_VERSION_ID, CR_ELEMENT_ID, CR_SOURCE_ID, CR_FIELD_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_COMBOS_FIELDS
  add constraint R_REG_CR_COMBOS_FIELDS1 foreign key (CR_COMBO_ID)
  references REG_CR_COMBOS (CR_COMBO_ID);
alter table REG_CR_COMBOS_FIELDS
  add constraint R_REG_CR_COMBOS_FIELDS2 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

prompt
prompt Creating table REG_CR_COMBOS_FIELDS_BDG
prompt =======================================
prompt
create table REG_CR_COMBOS_FIELDS_BDG
(
  cr_combo_id         NUMBER(22) not null,
  forecast_version_id NUMBER(22) not null,
  cr_element_id       NUMBER(22) not null,
  cr_source_id        NUMBER(22) not null,
  cr_field_id         NUMBER(22) not null,
  structure_id        NUMBER(22),
  not_indicator       NUMBER(22),
  lower_value         VARCHAR2(77),
  upper_value         VARCHAR2(77),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CR_COMBOS_FIELDS_BDG
  is '(  )  [  ]
THE REG CR COMBOS FIELDS BDG TABLE CONTAINS MAPPINGS PER ELEMENT, DETAILED ATTRIBUTE FOR A GIVEN CR COMBO FOR THE O&M FORECAST INTERFACE.';
comment on column REG_CR_COMBOS_FIELDS_BDG.cr_combo_id
  is 'SYSTEM ASSIGNED';
comment on column REG_CR_COMBOS_FIELDS_BDG.forecast_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR A FORECAST VERSION';
comment on column REG_CR_COMBOS_FIELDS_BDG.cr_element_id
  is '0 – MAPPING IS FOR DETAILED ATTRIBUTE, OTHERWISE MATCHES ELEMENT ID';
comment on column REG_CR_COMBOS_FIELDS_BDG.cr_source_id
  is '0 – MAPPING IS FOR ELEMENT, OTHERWISE IS REAL SOURCE_ID';
comment on column REG_CR_COMBOS_FIELDS_BDG.cr_field_id
  is '0 – MAPPING FOR ELEMENT, OTHERWISE IS REAL FIELD_ID';
comment on column REG_CR_COMBOS_FIELDS_BDG.structure_id
  is 'POPULATED WITH A STRUCTURE_ID FROM THE CR IF THE COMBO CONSISTS OF A LIST, NULL OTHERWISE';
comment on column REG_CR_COMBOS_FIELDS_BDG.not_indicator
  is 'INDICATES WHETHER THE ''NOT'' KEYWORD SHOULD BE INCLUDED WHEN BUILDING THE WHERE CLAUSE. 0 – DO NOT INCLUDE, 1 – INCLUDE.';
comment on column REG_CR_COMBOS_FIELDS_BDG.lower_value
  is 'LOWER VALUE IN RANGE, SINGLE VALUE, OR STRUCTURE POINT';
comment on column REG_CR_COMBOS_FIELDS_BDG.upper_value
  is 'UPPER VALUE IN RANGE, NULL OTHERWISE';
comment on column REG_CR_COMBOS_FIELDS_BDG.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS_FIELDS_BDG.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_COMBOS_FIELDS_BDG
  add constraint PK_REG_CR_COMBOS_FIELDS_BDG primary key (CR_COMBO_ID, FORECAST_VERSION_ID, CR_ELEMENT_ID, CR_SOURCE_ID, CR_FIELD_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_COMBOS_FIELDS_BDG
  add constraint R_REG_CR_COMBOS_FIELDS_BDG1 foreign key (CR_COMBO_ID)
  references REG_CR_COMBOS (CR_COMBO_ID);
alter table REG_CR_COMBOS_FIELDS_BDG
  add constraint R_REG_CR_COMBOS_FIELDS_BDG2 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

prompt
prompt Creating table REG_CR_COMBOS_MAP
prompt ================================
prompt
create table REG_CR_COMBOS_MAP
(
  historic_version_id NUMBER(22) not null,
  cr_combo_id         NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CR_COMBOS_MAP
  is '(  )  [  ]
THE CR COMBOS MAP TABLE MAPS CR COMBOS TO REG ACCOUNTS.  MAPPINGS ARE MANY TO 1 FOR CR COMBOS TO REG ACCOUNTS';
comment on column REG_CR_COMBOS_MAP.historic_version_id
  is 'SYSTEM – IDENTIFIER OF A HISTORIC LEDGER VERSION';
comment on column REG_CR_COMBOS_MAP.cr_combo_id
  is 'SYSTEM – IDENTIFIER OF A CR COMBO';
comment on column REG_CR_COMBOS_MAP.reg_acct_id
  is 'SYSTEM – IDENTIFIER OF A REGULATORY ACCOUNT';
comment on column REG_CR_COMBOS_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_COMBOS_MAP
  add constraint PK_REG_CR_COMBOS_MAP primary key (HISTORIC_VERSION_ID, CR_COMBO_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_COMBOS_MAP
  add constraint R_REG_CR_COMBOS_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_CR_COMBOS_MAP
  add constraint R_REG_CR_COMBOS_MAP2 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_CR_COMBOS_MAP
  add constraint R_REG_CR_COMBOS_MAP4 foreign key (CR_COMBO_ID)
  references REG_CR_COMBOS (CR_COMBO_ID);

prompt
prompt Creating table REG_CR_COMBOS_MAP_BDG
prompt ====================================
prompt
create table REG_CR_COMBOS_MAP_BDG
(
  forecast_version_id NUMBER(22) not null,
  cr_combo_id         NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CR_COMBOS_MAP_BDG
  is '(  )  [  ]
THE CR COMBOS MAP BDG TABLE MAPS CR COMBOS TO REG ACCOUNTS FOR THE O&M FORECAST INTERFACE.  MAPPINGS ARE MANY TO 1 FOR CR COMBOS TO REG ACCOUNTS.';
comment on column REG_CR_COMBOS_MAP_BDG.forecast_version_id
  is 'SYSTEM – IDENTIFIER OF A FORECAST VERSION';
comment on column REG_CR_COMBOS_MAP_BDG.cr_combo_id
  is 'SYSTEM – IDENTIFIER OF A CR COMBO';
comment on column REG_CR_COMBOS_MAP_BDG.reg_acct_id
  is 'SYSTEM – IDENTIFIER OF A REGULATORY ACCOUNT';
comment on column REG_CR_COMBOS_MAP_BDG.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS_MAP_BDG.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_COMBOS_MAP_BDG
  add constraint PK_REG_CR_COMBOS_MAP_BDG primary key (FORECAST_VERSION_ID, CR_COMBO_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_COMBOS_MAP_BDG
  add constraint R_REG_CR_COMBOS_MAP_BDG1 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);
alter table REG_CR_COMBOS_MAP_BDG
  add constraint R_REG_CR_COMBOS_MAP_BDG2 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_CR_COMBOS_MAP_BDG
  add constraint R_REG_CR_COMBOS_MAP_BDG4 foreign key (CR_COMBO_ID)
  references REG_CR_COMBOS (CR_COMBO_ID);

prompt
prompt Creating table REG_CR_COMBOS_SOURCES_FIELDS
prompt ===========================================
prompt
create table REG_CR_COMBOS_SOURCES_FIELDS
(
  cr_source_id NUMBER(22) not null,
  cr_field_id  NUMBER(22) not null,
  description  VARCHAR2(35) not null,
  field_order  NUMBER(22) not null,
  user_id      VARCHAR2(18),
  time_stamp   DATE
)
;
comment on table REG_CR_COMBOS_SOURCES_FIELDS
  is '(  )  [  ]
THE REG CR COMBOS SOURCES FIELDS TABLE IDENTIFIES DETAILED ATTRIBUTES FROM THE CR THAT NEED TO BE USED FOR CR COMBOS.';
comment on column REG_CR_COMBOS_SOURCES_FIELDS.cr_source_id
  is 'SOURCE_ID FROM CR_SOURCES';
comment on column REG_CR_COMBOS_SOURCES_FIELDS.cr_field_id
  is 'FIELD_ID FROM CR_SOURCE_FIELDS';
comment on column REG_CR_COMBOS_SOURCES_FIELDS.description
  is 'DESCRIPTION OF THE FIELD';
comment on column REG_CR_COMBOS_SOURCES_FIELDS.field_order
  is 'DISPLAY ORDER';
comment on column REG_CR_COMBOS_SOURCES_FIELDS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_COMBOS_SOURCES_FIELDS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_COMBOS_SOURCES_FIELDS
  add constraint PK_REG_CR_COMBOS_S_FIELDS primary key (CR_SOURCE_ID, CR_FIELD_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_COMBOS_SOURCES_FIELDS
  add constraint R_REG_CR_COMBOS_S_FIELDS1 foreign key (CR_SOURCE_ID, CR_FIELD_ID)
  references CR_SOURCES_FIELDS (SOURCE_ID, FIELD_ID);

prompt
prompt Creating table REG_CR_WHERE
prompt ===========================
prompt
create table REG_CR_WHERE
(
  where_clause_id     NUMBER(22) not null,
  historic_version_id NUMBER(22) not null,
  forecast_version_id NUMBER(22) not null,
  cr_company          VARCHAR2(35) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CR_WHERE
  is '(  )  [  ]
THE REG CR WHERE TABLE CONTAINS A LIST OF WHERE CLAUSES ASSIGNED TO A CR OR O&M BUDGET PULL TO BE APPLIED TO ALL COMBOS WHEN PULLING FROM THE CR/O&M TABLES.';
comment on column REG_CR_WHERE.where_clause_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG CR WHERE CLAUSE';
comment on column REG_CR_WHERE.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A HISTORIC VERSION – THE VALUE WILL BE NON-ZERO FOR WHERE CLAUSES USED FOR HISTORIC MAPPINGS AND ZERO FOR FORECAST MAPPINGS';
comment on column REG_CR_WHERE.forecast_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FORECAST VERSION – THE VALUE WILL BE NON-ZERO FOR WHERE CLAUSES USED FOR FORECAST MAPPINGS AND ZERO FOR HISTORIC MAPPINGS';
comment on column REG_CR_WHERE.cr_company
  is 'CR COMPANY VALUE TO WHICH THE WHERE CLAUSE APPLIES';
comment on column REG_CR_WHERE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_WHERE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_WHERE
  add constraint PK_REG_CR_WHERE primary key (WHERE_CLAUSE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_WHERE
  add constraint R_REG_CR_WHERE1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_CR_WHERE
  add constraint R_REG_CR_WHERE2 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

prompt
prompt Creating table REG_CR_WHERE_CLAUSE
prompt ==================================
prompt
create table REG_CR_WHERE_CLAUSE
(
  where_clause_id NUMBER(22) not null,
  row_id          NUMBER(22) not null,
  left_paren      VARCHAR2(1),
  column_name     VARCHAR2(254),
  operator        VARCHAR2(35),
  value1          VARCHAR2(1000),
  between_and     VARCHAR2(3),
  value2          VARCHAR2(35),
  and_or          VARCHAR2(3),
  right_paren     VARCHAR2(1),
  user_id         VARCHAR2(18),
  time_stamp      DATE
)
;
comment on table REG_CR_WHERE_CLAUSE
  is '(  )  [  ]
THE REG CR WHERE CLAUSE CONTAINS THE CRITERIA FOR BUILDING EACH REG CR WHERE CLAUSE';
comment on column REG_CR_WHERE_CLAUSE.where_clause_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG CR WHERE CLAUSE';
comment on column REG_CR_WHERE_CLAUSE.row_id
  is 'SYSTEM ASSIGNED IDENTIFIER FOR EACH ROW IN THE WHERE CLAUSE CRITERIA';
comment on column REG_CR_WHERE_CLAUSE.left_paren
  is 'LEFT PARENTHESIS ADDED TO THE WHERE CLAUSE';
comment on column REG_CR_WHERE_CLAUSE.column_name
  is 'NAME OF CR COLUMN';
comment on column REG_CR_WHERE_CLAUSE.operator
  is 'OPERATOR TO USE FOR THE WHERE CLAUSE, E.G. ''='', ''IN'', ''BETWEEN'', ETC.';
comment on column REG_CR_WHERE_CLAUSE.value1
  is 'THE VALUE FOR THE SELECTED COLUMN NAME AS IT RELATES TO THE OPERATOR SELECTED';
comment on column REG_CR_WHERE_CLAUSE.between_and
  is 'VALUE IS ''AND'' IF ''BETWEEN'' OR ''NOT BETWEEN'' IS SELECTED AS THE OPERATOR';
comment on column REG_CR_WHERE_CLAUSE.value2
  is 'UPPER BOUND VALUE IF ''BETWEEN'' OR ''NOT BETWEEN'' IS SELECTED AS THE OPERATOR';
comment on column REG_CR_WHERE_CLAUSE.and_or
  is 'VALUE IS ''AND'' OR ''OR'' AND MUST BE POPULATED ON THE NUMBER OF ROWS ADDED MINUS 1 (ALL BUT THE LAST ROW OF THE WHERE CLAUSE)';
comment on column REG_CR_WHERE_CLAUSE.right_paren
  is 'RIGHT PARENTHESIS ADDED TO THE WHERE CLAUSE';
comment on column REG_CR_WHERE_CLAUSE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_CR_WHERE_CLAUSE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_CR_WHERE_CLAUSE
  add constraint PK_REG_CR_WHERE_CLAUSE primary key (WHERE_CLAUSE_ID, ROW_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CR_WHERE_CLAUSE
  add constraint R_REG_CR_WHERE_CLAUSE1 foreign key (WHERE_CLAUSE_ID)
  references REG_CR_WHERE (WHERE_CLAUSE_ID);

prompt
prompt Creating table REG_CWIP_COMPONENT_ELEMENT
prompt =========================================
prompt
create table REG_CWIP_COMPONENT_ELEMENT
(
  reg_component_element_id NUMBER(22) not null,
  description              VARCHAR2(35) not null,
  id_column_name           VARCHAR2(30) not null,
  description_column       VARCHAR2(30) not null,
  description_table        VARCHAR2(30) not null,
  user_id                  VARCHAR2(18),
  time_stamp               DATE
)
;
alter table REG_CWIP_COMPONENT_ELEMENT
  add constraint PK_REG_CWIP_COMPONENT_ELEMENT primary key (REG_COMPONENT_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CWIP_COMPONENT_VALUES
prompt ========================================
prompt
create table REG_CWIP_COMPONENT_VALUES
(
  reg_component_id         NUMBER(22) not null,
  reg_family_id            NUMBER(22) not null,
  reg_component_element_id NUMBER(22) not null,
  id_value                 NUMBER(22) not null,
  user_id                  VARCHAR2(18),
  time_stamp               DATE
)
;
alter table REG_CWIP_COMPONENT_VALUES
  add constraint PK_REG_CWIP_COMPONENT_VALUES primary key (REG_COMPONENT_ID, REG_FAMILY_ID, REG_COMPONENT_ELEMENT_ID, ID_VALUE)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CWIP_COMPONENT_VALUES
  add constraint R_REG_CWIP_COMPONENT_VALUES1 foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);
alter table REG_CWIP_COMPONENT_VALUES
  add constraint R_REG_CWIP_COMPONENT_VALUES2 foreign key (REG_COMPONENT_ELEMENT_ID)
  references REG_CWIP_COMPONENT_ELEMENT (REG_COMPONENT_ELEMENT_ID);

prompt
prompt Creating table REG_CWIP_COMP_MEMBER_MAP
prompt =======================================
prompt
create table REG_CWIP_COMP_MEMBER_MAP
(
  historic_version_id NUMBER(22) not null,
  reg_member_id       NUMBER(22) not null,
  reg_family_id       NUMBER(22) not null,
  reg_component_id    NUMBER(22) not null,
  ferc_acct           VARCHAR2(10),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CWIP_COMP_MEMBER_MAP
  is '(  )  [  ]
THE REG CWIP COMP MEMBER MAP TABLE MAPS CWIP COMPONENTS TO CWIP FAMILY MEMBERS FOR A GIVEN HISTORIC VERSION TO IDENTIFY THE CWIP REG ACCOUNTS TO BE CREATED.';
comment on column REG_CWIP_COMP_MEMBER_MAP.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP SOURCE';
comment on column REG_CWIP_COMP_MEMBER_MAP.reg_member_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP FAMILY MEMBER';
comment on column REG_CWIP_COMP_MEMBER_MAP.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG FAMILY';
comment on column REG_CWIP_COMP_MEMBER_MAP.reg_component_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP COMPONENT';
comment on column REG_CWIP_COMP_MEMBER_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES';
comment on column REG_CWIP_COMP_MEMBER_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES';
alter table REG_CWIP_COMP_MEMBER_MAP
  add constraint PK_REG_CWIP_COMP_MEMBER_MAP primary key (HISTORIC_VERSION_ID, REG_MEMBER_ID, REG_FAMILY_ID, REG_COMPONENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CWIP_COMP_MEMBER_MAP
  add constraint R_REG_CWIP_COMP_MEMBER_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_CWIP_COMP_MEMBER_MAP
  add constraint R_REG_CWIP_COMP_MEMBER_MAP2 foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);
alter table REG_CWIP_COMP_MEMBER_MAP
  add constraint R_REG_CWIP_COMP_MEMBER_MAP3 foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);
alter table REG_CWIP_COMP_MEMBER_MAP
  add constraint R_REG_CWIP_COMP_MEMBER_MAP4 foreign key (FERC_ACCT)
  references REG_FERC_ACCOUNT (FERC_ACCT);

prompt
prompt Creating table REG_CWIP_COMP_WHERE_CLAUSE
prompt =========================================
prompt
create table REG_CWIP_COMP_WHERE_CLAUSE
(
  reg_component_id NUMBER(22) not null,
  reg_family_id    NUMBER(22) not null,
  table_list       VARCHAR2(1000) not null,
  where_sql        VARCHAR2(4000) not null,
  user_id          VARCHAR2(18),
  time_stamp       DATE
)
;
alter table REG_CWIP_COMP_WHERE_CLAUSE
  add constraint PK_REG_CWIP_COMP_WHERE_CLAUSE primary key (REG_COMPONENT_ID, REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CWIP_COMP_WHERE_CLAUSE
  add constraint R_REG_CWIP_COMP_WHERE_CLAUSE1 foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

prompt
prompt Creating table REG_CWIP_MEMBER_RULE
prompt ===================================
prompt
create table REG_CWIP_MEMBER_RULE
(
  reg_member_rule_id NUMBER(22) not null,
  description        VARCHAR2(35),
  user_id            VARCHAR2(18),
  time_stamp         DATE
)
;
comment on table REG_CWIP_MEMBER_RULE
  is '(  )  [  ]
THE REG CWIP MEMBER RULE TABLE CONTAINS A LIST OF RULES USED TO IDENTIFY A CWIP FAMILY MEMBER.';
comment on column REG_CWIP_MEMBER_RULE.reg_member_rule_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP MEMBER RULE';
comment on column REG_CWIP_MEMBER_RULE.description
  is 'DESCRIPTION OF THE CWIP MEMBER RULE';
comment on column REG_CWIP_MEMBER_RULE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES';
comment on column REG_CWIP_MEMBER_RULE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES';
alter table REG_CWIP_MEMBER_RULE
  add constraint PK_REG_CWIP_MEMBER_RULE primary key (REG_MEMBER_RULE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CWIP_FAMILY_MEMBER_MAP
prompt =========================================
prompt
create table REG_CWIP_FAMILY_MEMBER_MAP
(
  historic_version_id NUMBER(22) not null,
  reg_member_id       NUMBER(22) not null,
  reg_family_id       NUMBER(22) not null,
  reg_member_rule_id  NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_CWIP_FAMILY_MEMBER_MAP
  is '(  )  [  ]
THE REG CWIP FAMILY MEMBER MAP TABLE CONTAINS THE RULES THAT IDENTIFY A CWIP FAMILY MEMBER FOR A GIVEN HISTORIC VERSION.';
comment on column REG_CWIP_FAMILY_MEMBER_MAP.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A HISTORIC VERSION';
comment on column REG_CWIP_FAMILY_MEMBER_MAP.reg_member_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP FAMILY MEMBER';
comment on column REG_CWIP_FAMILY_MEMBER_MAP.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG FAMILY';
comment on column REG_CWIP_FAMILY_MEMBER_MAP.reg_member_rule_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG MEMBER RULE';
comment on column REG_CWIP_FAMILY_MEMBER_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES';
comment on column REG_CWIP_FAMILY_MEMBER_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES';
alter table REG_CWIP_FAMILY_MEMBER_MAP
  add constraint PK_REG_CWIP_FAMILY_MEMBER_MAP primary key (HISTORIC_VERSION_ID, REG_MEMBER_ID, REG_FAMILY_ID, REG_MEMBER_RULE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CWIP_FAMILY_MEMBER_MAP
  add constraint R_REG_CWIP_FAMILY_MEMBER_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_CWIP_FAMILY_MEMBER_MAP
  add constraint R_REG_CWIP_FAMILY_MEMBER_MAP2 foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
  references REG_CWIP_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);
alter table REG_CWIP_FAMILY_MEMBER_MAP
  add constraint R_REG_CWIP_FAMILY_MEMBER_MAP3 foreign key (REG_MEMBER_RULE_ID)
  references REG_CWIP_MEMBER_RULE (REG_MEMBER_RULE_ID);

prompt
prompt Creating table REG_CWIP_GL_ACCOUNT_TYPE
prompt =======================================
prompt
create table REG_CWIP_GL_ACCOUNT_TYPE
(
  reg_cwip_gl_acct_type_id NUMBER(22) not null,
  description              VARCHAR2(35) not null,
  wo_account_column        VARCHAR2(30) not null,
  user_id                  VARCHAR2(18),
  time_stamp               DATE,
  amount_column            VARCHAR2(254)
)
;
alter table REG_CWIP_GL_ACCOUNT_TYPE
  add constraint PK_REG_CWIP_GL_ACCOUNT_TYPE primary key (REG_CWIP_GL_ACCT_TYPE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CWIP_MEMBER_ELEMENT
prompt ======================================
prompt
create table REG_CWIP_MEMBER_ELEMENT
(
  reg_member_element_id NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  column_name           VARCHAR2(30) not null,
  description_table     VARCHAR2(30) not null,
  interface_table       VARCHAR2(30) not null,
  class_code_id         NUMBER(22),
  required              NUMBER(22) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_CWIP_MEMBER_ELEMENT
  is '(  )  [  ]
THE REG CWIP MEMBER ELEMENT TABLE CONTAINS FIELDS AVAILABLE FOR DEFINING CWIP FAMILY MEMBERS.';
comment on column REG_CWIP_MEMBER_ELEMENT.reg_member_element_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP MEMBER ELEMENT';
comment on column REG_CWIP_MEMBER_ELEMENT.description
  is 'DESCRIPTION OF THE ELEMENT; VALUES ARE:
BUSINESS SEGMENT
WORK ORDER TYPE
FUNCTIONAL CLASS
MAJOR LOCATION
ASSET LOCATION
CLASS CODE 1
CLASS CODE 2';
comment on column REG_CWIP_MEMBER_ELEMENT.column_name
  is 'THE “ID” COLUMN NAME ON THE TABLE THAT STORES DESCRIPTIONS FOR THE ELEMENT';
comment on column REG_CWIP_MEMBER_ELEMENT.description_table
  is 'THE NAME OF THE TABLE THAT STORES DESCRIPTIONS FOR THE ELEMENT';
comment on column REG_CWIP_MEMBER_ELEMENT.class_code_id
  is 'THE CLASS CODE ID VALUE FOR CLASS CODE 1 OR CLASS CODE 2';
comment on column REG_CWIP_MEMBER_ELEMENT.required
  is 'DETERMINES WHETHER A VALUE IS REQUIRED FOR THIS ELEMENT TO DETERMINE THE MAPPING FOR THE COMPONENT; VALUES ARE 0 – NOT USED, 1 – REQUIRED, 2 - OPTIONAL';
comment on column REG_CWIP_MEMBER_ELEMENT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES';
comment on column REG_CWIP_MEMBER_ELEMENT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES';
alter table REG_CWIP_MEMBER_ELEMENT
  add constraint PK_REG_CWIP_MEMBER_ELEMENT primary key (REG_MEMBER_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_CWIP_MEMBER_RULE_VALUES
prompt ==========================================
prompt
create table REG_CWIP_MEMBER_RULE_VALUES
(
  historic_version_id   NUMBER(22) not null,
  reg_member_rule_id    NUMBER(22) not null,
  reg_member_element_id NUMBER(22) not null,
  lower_value           NUMBER(22) not null,
  upper_value           NUMBER(22),
  user_id               VARCHAR2(18),
  time_stamp            DATE,
  class_code_value      VARCHAR2(254)
)
;
comment on table REG_CWIP_MEMBER_RULE_VALUES
  is '(  )  [  ]
THE REG CWIP MEMBER RULE VALUES TABLE CONTAINS THE MAPPINGS USED TO BUILD THE WHERE CLAUSE IN THE CWIP INTERFACE TO IDENTIFY A CWIP FAMILY MEMBER.';
comment on column REG_CWIP_MEMBER_RULE_VALUES.reg_member_rule_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP MEMBER RULE';
comment on column REG_CWIP_MEMBER_RULE_VALUES.reg_member_element_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP MEMBER ELEMENT';
comment on column REG_CWIP_MEMBER_RULE_VALUES.lower_value
  is 'THE VALUE OF THE CWIP MEMBER ELEMENT USED TO IDENTIFY THE CWIP MEMBER IN THE INTERFACE; COULD BE THE LOWER VALUE OF A RANGE.';
comment on column REG_CWIP_MEMBER_RULE_VALUES.upper_value
  is 'UPPER VALUE IN A RANGE';
comment on column REG_CWIP_MEMBER_RULE_VALUES.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES';
comment on column REG_CWIP_MEMBER_RULE_VALUES.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES';
alter table REG_CWIP_MEMBER_RULE_VALUES
  add constraint PK_REG_CWIP_MEMBER_RULE_VALUES primary key (HISTORIC_VERSION_ID, REG_MEMBER_RULE_ID, REG_MEMBER_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_CWIP_MEMBER_RULE_VALUES
  add constraint R_REG_CWIP_MEMBER_RULE_VALUES1 foreign key (REG_MEMBER_RULE_ID)
  references REG_CWIP_MEMBER_RULE (REG_MEMBER_RULE_ID);
alter table REG_CWIP_MEMBER_RULE_VALUES
  add constraint R_REG_CWIP_MEMBER_RULE_VALUES2 foreign key (REG_MEMBER_ELEMENT_ID)
  references REG_CWIP_MEMBER_ELEMENT (REG_MEMBER_ELEMENT_ID);
alter table REG_CWIP_MEMBER_RULE_VALUES
  add constraint R_REG_CWIP_MEMBER_RULE_VALUES3 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

prompt
prompt Creating table REG_DEPR_FAMILY_COMPONENTS
prompt =========================================
prompt
create table REG_DEPR_FAMILY_COMPONENTS
(
  reg_component_id      NUMBER(22) not null,
  reg_family_id         NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  reg_acct_type_default NUMBER(22),
  sub_acct_type_id      NUMBER(22),
  reg_annualization_id  NUMBER(22),
  acct_good_for         NUMBER(22),
  long_description      VARCHAR2(254),
  user_id               VARCHAR2(18),
  time_stamp            DATE,
  ferc_acct_ref         NUMBER(22)
)
;
comment on table REG_DEPR_FAMILY_COMPONENTS
  is '(  )  [  ]
THESE ARE THE REGULATORY ACCOUNTS WHICH WILL BE CREATED FOR EACH DEPRECIATION GROUP OR ROLLUP TO A DEPR FAMILY MEMBER (SEE REG DEPR GROUP MAP).  NOTE DEPR LEDGER COLUMNS ARE MAPPED TO THESE COMPONENTS ON THE REG DEPR LEDGER MAP TABLE.';
comment on column REG_DEPR_FAMILY_COMPONENTS.reg_component_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A DEPR FAMILY COMPONENT, FOR EXAMPLE “ACCUMULATED RESERVE BALANCE.” UNIQUE ACROSS ALL ACCOUNT FAMILIES.';
comment on column REG_DEPR_FAMILY_COMPONENTS.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE DEPR FAMILY.';
comment on column REG_DEPR_FAMILY_COMPONENTS.description
  is 'DESCRIPTION OF THE FAMILY COMPONENT, FOR EXAMPLE “ACCUMULATED RESERVE BALANCE.”';
comment on column REG_DEPR_FAMILY_COMPONENTS.reg_acct_type_default
  is '1 = RATE BASE
2 = UTILITY INVESTMENT NOT IN RATE BASE (I.E. NOT EARNING A RETURN)
3 = OPERATING EXPENSE
4 = NON-UTILITY EXPENSE
5 = OPERATING REVENUE
6 = NON-UTILITY REVENUE
7 = NON-UTILITY INVESTMENT
8 = CAPITAL
9 = OTHER ADJUSTMENT
10 = INCOME TAX MEMO';
comment on column REG_DEPR_FAMILY_COMPONENTS.sub_acct_type_id
  is 'E.G. FOR ACCOUNT TYPE CAPITAL: PREFERRED, COMMON OR DEBT; FOR RATE BASE: WORKING CAPITAL, PP&E, ETC.';
comment on column REG_DEPR_FAMILY_COMPONENTS.reg_annualization_id
  is 'ANNUALIZATION METHOD:
1 = 13 MONTH SIMPLE AVERAGE
2 = 13 MONTH AVERAGE OF AVERAGES
3 = 12 MONTH AVERAGE
4 = 2 POINT ANNUAL AVERAGE
5 = ENDING BALANCE
6 = SUM OF 12 MONTHS
7 = ENDING MONTH * 12
8  = INPUT';
comment on column REG_DEPR_FAMILY_COMPONENTS.acct_good_for
  is 'APPLICABILITY INDICATOR:
1 = HISTORIC ACTUALS
2 = OTHER HISTORIC
3 = FORECAST
4 = HISTORIC ACTUALS AND FORECAST';
comment on column REG_DEPR_FAMILY_COMPONENTS.long_description
  is 'LONG DESCRIPTION OF THE COMPONENT.';
comment on column REG_DEPR_FAMILY_COMPONENTS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_DEPR_FAMILY_COMPONENTS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_DEPR_FAMILY_COMPONENTS
  add constraint PK_REG_DEPR_FAMILY_COMPONENTS primary key (REG_COMPONENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_DEPR_FAMILY_COMPONENTS
  add constraint R_REG_DEPR_FAMILY_COMPONENTS1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);
alter table REG_DEPR_FAMILY_COMPONENTS
  add constraint R_REG_DEPR_FAMILY_COMPONENTS2 foreign key (REG_ACCT_TYPE_DEFAULT)
  references REG_ACCT_TYPE (REG_ACCT_TYPE_ID);

prompt
prompt Creating table REG_DEPR_FAMILY_MEMBER
prompt =====================================
prompt
create table REG_DEPR_FAMILY_MEMBER
(
  reg_member_id    NUMBER(22) not null,
  reg_family_id    NUMBER(22) not null,
  description      VARCHAR2(35) not null,
  long_description VARCHAR2(254),
  user_id          VARCHAR2(18),
  time_stamp       DATE,
  ferc_acct        VARCHAR2(10)
)
;
comment on table REG_DEPR_FAMILY_MEMBER
  is '(  )  [  ]
THE REG DEPR FAMILY MEMBERS TABLE DEFINES THE MEMBERS WITHIN A FAMILY, E.G. THE SPECIFIC DEPRECIATION GROUPS IN THE “DEPRECIATION” FAMILY.';
comment on column REG_DEPR_FAMILY_MEMBER.reg_member_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY MEMBER, FOR EXAMPLE A SPECIFIC DEPRECIATION GROUP. UNIQUE ACROSS ALL ACCOUNT FAMILIES.';
comment on column REG_DEPR_FAMILY_MEMBER.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY.';
comment on column REG_DEPR_FAMILY_MEMBER.description
  is 'DESCRIPTION OF THE COLUMN – I.E. MEMBER OF THE DEPR FAMILY.';
comment on column REG_DEPR_FAMILY_MEMBER.long_description
  is 'LONG DESCRIPTION';
comment on column REG_DEPR_FAMILY_MEMBER.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_DEPR_FAMILY_MEMBER.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_DEPR_FAMILY_MEMBER
  add constraint PK_REG_DEPR_FAMILY_MEMBER primary key (REG_MEMBER_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_DEPR_FAMILY_MEMBER
  add constraint R_REG_DEPR_FAMILY_MEMBER1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);
alter table REG_DEPR_FAMILY_MEMBER
  add constraint R_REG_DEPR_FAMILY_MEMBER2 foreign key (FERC_ACCT)
  references REG_FERC_ACCOUNT (FERC_ACCT);

prompt
prompt Creating table REG_DEPR_FAMILY_MEMBER_MAP
prompt =========================================
prompt
create table REG_DEPR_FAMILY_MEMBER_MAP
(
  depr_group_id       NUMBER(22) not null,
  historic_version_id NUMBER(22) not null,
  reg_family_id       NUMBER(22) not null,
  reg_member_id       NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_DEPR_FAMILY_MEMBER_MAP
  is '(  )  [  ]
THE REG DEPR FAMILY MEMBER MAP TABLE HAS THE RELATIONSHIP BETWEEN THE SYSTEM DEPRECIATION GROUPS AND REGULATORY FAMILY MEMBERS.  THIS MAY BE 1-TO-1 OR MANY-TO-1, BUT NOT 1-TO-MANY.  THIS TABLE MAPS DEPRECIATION GROUPS TO A LEVEL (FAMILY MEMBER) THAT IS USED FOR ROWS ON THE REG ACCT MASTER, SO THAT FOR EACH DEPRECIATION GROUP ROLLUP TO MEMBER, THERE ARE REG ACCOUNTS FOR EACH DEPR GROUP FAMILY COMPONENT.';
comment on column REG_DEPR_FAMILY_MEMBER_MAP.depr_group_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A DEPRECIATION GROUP.';
comment on column REG_DEPR_FAMILY_MEMBER_MAP.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A HISTORIC VERSION';
comment on column REG_DEPR_FAMILY_MEMBER_MAP.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY.';
comment on column REG_DEPR_FAMILY_MEMBER_MAP.reg_member_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY MEMBER, FOR EXAMPLE A SPECIFIC DEPRECIATION GROUP.';
comment on column REG_DEPR_FAMILY_MEMBER_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_DEPR_FAMILY_MEMBER_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_DEPR_FAMILY_MEMBER_MAP
  add constraint PK_REG_DEPR_FAMILY_MEMBER_MAP primary key (DEPR_GROUP_ID, HISTORIC_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_DEPR_FAMILY_MEMBER_MAP
  add constraint R_REG_DEPR_FAMILY_MEMBER_MAP1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);
alter table REG_DEPR_FAMILY_MEMBER_MAP
  add constraint R_REG_DEPR_FAMILY_MEMBER_MAP2 foreign key (DEPR_GROUP_ID)
  references DEPR_GROUP (DEPR_GROUP_ID);
alter table REG_DEPR_FAMILY_MEMBER_MAP
  add constraint R_REG_DEPR_FAMILY_MEMBER_MAP3 foreign key (REG_MEMBER_ID)
  references REG_DEPR_FAMILY_MEMBER (REG_MEMBER_ID);
alter table REG_DEPR_FAMILY_MEMBER_MAP
  add constraint R_REG_DEPR_FAMILY_MEMBER_MAP4 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

prompt
prompt Creating table REG_DEPR_LEDGER_COLUMNS
prompt ======================================
prompt
create table REG_DEPR_LEDGER_COLUMNS
(
  column_name       VARCHAR2(30) not null,
  description       VARCHAR2(35),
  table_doc_descr   VARCHAR2(2000),
  sort_order        NUMBER(22),
  column_multiplier NUMBER(22)
)
;
comment on table REG_DEPR_LEDGER_COLUMNS
  is '(  )  [  ]
THE REG DEPR LEDGER COLUMNS TABLE CONTAINS THE DEPRECIATION LEDGER COLUMNS (AND DOCUMENTATION OF THE COLUMN ATTRIBUTES) THAT CAN BE MAPPED AS COMPONENTS INTO REGULATORY ACCOUNTS.';
comment on column REG_DEPR_LEDGER_COLUMNS.column_name
  is 'COLUMN (ATTRIBUTE) ON THE DEPR LEDGER TABLE';
comment on column REG_DEPR_LEDGER_COLUMNS.description
  is 'DESCRIPTION OF THE COLUMN';
comment on column REG_DEPR_LEDGER_COLUMNS.table_doc_descr
  is 'LONG DESCRIPTION OF THE COLUMN FROM THE TABLE DOCUMENTATION';
comment on column REG_DEPR_LEDGER_COLUMNS.sort_order
  is 'SORT ORDER FOR THE SELECTION WINDOW';
alter table REG_DEPR_LEDGER_COLUMNS
  add constraint PK_REG_DEPR_LEDGER_COLUMNS primary key (COLUMN_NAME)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_DEPR_LEDGER_MAP
prompt ==================================
prompt
create table REG_DEPR_LEDGER_MAP
(
  column_name         VARCHAR2(30) not null,
  historic_version_id NUMBER(22) not null,
  reg_component_id    NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_DEPR_LEDGER_MAP
  is '(  )  [  ]
THE REG DEPR LEDGER MAP TABLE IS USED FOR THE MAPPING OF DEPRECIATION LEDGER COLUMNS TO REG DEPR GROUP FAMILY COMPONENTS.  (E.G. DEPR EXPENSE, RESERVE, PLANT BALANCE, ETC.).';
comment on column REG_DEPR_LEDGER_MAP.column_name
  is 'THE EXACT DATABASE COLUMN NAME FOR AMOUNT COLUMNS ON DEPR_LEDGER TABLE.';
comment on column REG_DEPR_LEDGER_MAP.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A HISTORIC VERSION';
comment on column REG_DEPR_LEDGER_MAP.reg_component_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY COMPONENT.  ALSO VALID IS 0 = NOT MAPPED.';
comment on column REG_DEPR_LEDGER_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_DEPR_LEDGER_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_DEPR_LEDGER_MAP
  add constraint PK_REG_DEPR_LEDGER_MAP primary key (COLUMN_NAME, HISTORIC_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_DEPR_LEDGER_MAP
  add constraint R_REG_DEPR_LEDGER_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_DEPR_LEDGER_MAP
  add constraint R_REG_DEPR_LEDGER_MAP2 foreign key (COLUMN_NAME)
  references REG_DEPR_LEDGER_COLUMNS (COLUMN_NAME);
alter table REG_DEPR_LEDGER_MAP
  add constraint R_REG_DEPR_LEDGER_MAP3 foreign key (REG_COMPONENT_ID)
  references REG_DEPR_FAMILY_COMPONENTS (REG_COMPONENT_ID);

prompt
prompt Creating table REG_FCST_CAP_BDG_LOAD_TEMP
prompt =========================================
prompt
create table REG_FCST_CAP_BDG_LOAD_TEMP
(
  reg_company_id      NUMBER(22),
  reg_component_id    NUMBER(22),
  reg_member_id       NUMBER(22),
  reg_member_rule_id  NUMBER(22),
  forecast_version_id NUMBER(22),
  gl_month            NUMBER(22,1),
  amount              NUMBER(22,2)
)
;

prompt
prompt Creating table REG_FCST_CR_LOAD_TEMP
prompt ====================================
prompt
create global temporary table REG_FCST_CR_LOAD_TEMP
(
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  cr_combo_id         NUMBER(22) not null,
  forecast_version_id NUMBER(22) not null,
  gl_month            NUMBER(22,1) not null,
  amount              NUMBER(22,2)
)
on commit preserve rows;
alter table REG_FCST_CR_LOAD_TEMP
  add constraint PK_REG_FCST_CR_LOAD_TEMP primary key (REG_COMPANY_ID, REG_ACCT_ID, CR_COMBO_ID, FORECAST_VERSION_ID, GL_MONTH);

prompt
prompt Creating table REG_FCST_DEPR_FAM_MEM_MAP
prompt ========================================
prompt
create table REG_FCST_DEPR_FAM_MEM_MAP
(
  fcst_depr_group_id  NUMBER(22) not null,
  forecast_version_id NUMBER(22) not null,
  reg_family_id       NUMBER(22) not null,
  reg_member_id       NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_FCST_DEPR_FAM_MEM_MAP
  add constraint PK_REG_FCST_DEPR_FAM_MEM_MAP primary key (FCST_DEPR_GROUP_ID, FORECAST_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FCST_DEPR_FAM_MEM_MAP
  add constraint R_REG_FCST_DEPR_FAM_MEM_MAP1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);
alter table REG_FCST_DEPR_FAM_MEM_MAP
  add constraint R_REG_FCST_DEPR_FAM_MEM_MAP2 foreign key (FCST_DEPR_GROUP_ID)
  references FCST_DEPR_GROUP (FCST_DEPR_GROUP_ID);
alter table REG_FCST_DEPR_FAM_MEM_MAP
  add constraint R_REG_FCST_DEPR_FAM_MEM_MAP3 foreign key (REG_MEMBER_ID)
  references REG_DEPR_FAMILY_MEMBER (REG_MEMBER_ID);
alter table REG_FCST_DEPR_FAM_MEM_MAP
  add constraint R_REG_FCST_DEPR_FAM_MEM_MAP4 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

prompt
prompt Creating table REG_FCST_DEPR_LEDGER_MAP
prompt =======================================
prompt
create table REG_FCST_DEPR_LEDGER_MAP
(
  column_name         VARCHAR2(30) not null,
  forecast_version_id NUMBER(22) not null,
  reg_component_id    NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_FCST_DEPR_LEDGER_MAP
  is '(  )  [  ]
THE REG FCST DEPR LEDGER MAP TABLE IS USED FOR THE MAPPING OF FORECAST DEPRECIATION LEDGER COLUMNS TO REG DEPR GROUP FAMILY COMPONENTS.  (E.G. DEPR EXPENSE, RESERVE, PLANT BALANCE, ETC.).';
comment on column REG_FCST_DEPR_LEDGER_MAP.column_name
  is 'THE EXACT DATABASE COLUMN NAME FOR AMOUNT COLUMNS ON FCST_DEPR_LEDGER TABLE.';
comment on column REG_FCST_DEPR_LEDGER_MAP.forecast_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FORECAST VERSION';
comment on column REG_FCST_DEPR_LEDGER_MAP.reg_component_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY COMPONENT.  ALSO VALID IS 0 = NOT MAPPED.';
comment on column REG_FCST_DEPR_LEDGER_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_FCST_DEPR_LEDGER_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_FCST_DEPR_LEDGER_MAP
  add constraint PK_REG_FCST_DEPR_LEDGER_MAP primary key (COLUMN_NAME, FORECAST_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FCST_DEPR_LEDGER_MAP
  add constraint R_REG_FCST_DEPR_LEDGER_MAP1 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);
alter table REG_FCST_DEPR_LEDGER_MAP
  add constraint R_REG_FCST_DEPR_LEDGER_MAP2 foreign key (COLUMN_NAME)
  references REG_DEPR_LEDGER_COLUMNS (COLUMN_NAME);
alter table REG_FCST_DEPR_LEDGER_MAP
  add constraint R_REG_FCST_DEPR_LEDGER_MAP3 foreign key (REG_COMPONENT_ID)
  references REG_DEPR_FAMILY_COMPONENTS (REG_COMPONENT_ID);

prompt
prompt Creating table REG_FINANCIAL_MONITOR_RESULT
prompt ===========================================
prompt
create table REG_FINANCIAL_MONITOR_RESULT
(
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  reg_jurisdiction_id NUMBER(22) not null,
  month_year          DATE not null,
  book_amount         NUMBER(22,2) not null,
  jur_amount          NUMBER(22,2) not null,
  jur_percent         NUMBER(22,16) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_FINANCIAL_MONITOR_RESULT
  is '(  )  [  ]
THESE ARE THE RESULTS FOR OVERALL FINANCIAL MONITORING.  NOTE: REG_JURISDICTION_ID OF ''0'' INDICATES TOTAL COMPANY.';
comment on column REG_FINANCIAL_MONITOR_RESULT.reg_company_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF THE COMPANY';
comment on column REG_FINANCIAL_MONITOR_RESULT.reg_acct_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT';
comment on column REG_FINANCIAL_MONITOR_RESULT.reg_jurisdiction_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF THE JURISDICTION';
comment on column REG_FINANCIAL_MONITOR_RESULT.month_year
  is 'LAST MONTH AND YEAR OF THE TWELVE MONTH ENDED RESULT';
comment on column REG_FINANCIAL_MONITOR_RESULT.jur_amount
  is 'JURISDICTIONAL ALLOCATED AMOUNT IN DOLLARS – 13 MONTH AVERAGE OF AVERAGE (BALANCE SHEET); 12 MONTH SUM INCOME STATEMENT.';
comment on column REG_FINANCIAL_MONITOR_RESULT.jur_percent
  is 'PERCENT USED IN THE JURISDICTIONAL ALLOCATION';
comment on column REG_FINANCIAL_MONITOR_RESULT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_FINANCIAL_MONITOR_RESULT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

prompt
prompt Creating table REG_FINANCIAL_MONITOR_RES_BAK
prompt ============================================
prompt
create table REG_FINANCIAL_MONITOR_RES_BAK
(
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  reg_jurisdiction_id NUMBER(22) not null,
  month_year          DATE not null,
  book_amount         NUMBER(22,2) not null,
  jur_amount          NUMBER(22,2) not null,
  jur_percent         NUMBER(22,16) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;

prompt
prompt Creating table REG_FINANCIAL_MONITOR_SUMMARY
prompt ============================================
prompt
create table REG_FINANCIAL_MONITOR_SUMMARY
(
  reg_company_id       NUMBER(22) not null,
  reg_jurisdiction_id  NUMBER(22) not null,
  month_year           DATE not null,
  return_on_common     NUMBER(22,8),
  overall_return       NUMBER(22,8),
  adj_return_on_common NUMBER(22,8),
  adj_overall_return   NUMBER(22,8),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_FINANCIAL_MONITOR_SUMMARY
  is '(  )  [  ]
THESE ARE THE RESULTS FOR OVERALL FINANCIAL MONITORING.  NOTE: REG_JURISDICTION_ID OF ''0'' INDICATES TOTAL COMPANY.';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.reg_company_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A COMPANY';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.reg_jurisdiction_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A REGULATORY JURISDICTION';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.month_year
  is 'LAST ACCOUNTING MONTH OF THE TWELVE MONTH ENDED RESULTS';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.return_on_common
  is 'RETURN ON COMMON EQUITY FOR THE 12 MONTH ENDED PERIOD';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.overall_return
  is 'OVERALL RETURN ON CAPITAL STRUCTURE FOR THE 12 MONTH ENDED PERIOD';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.adj_return_on_common
  is 'WEATHER NORMALIZED ADJUSTED RETURN ON COMMON EQUITY FOR THE 12 MONTH ENDED PERIOD (OPTIONAL)';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.adj_overall_return
  is 'WEATHER NORMALIZED ADJUSTED RETURN ON CAPITAL STRUCTURE FOR THE 12 MONTH ENDED PERIOD (OPTIONAL)';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_FINANCIAL_MONITOR_SUMMARY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

prompt
prompt Creating table REG_FIN_COST_OF_CAPITAL
prompt ======================================
prompt
create table REG_FIN_COST_OF_CAPITAL
(
  reg_company_id       NUMBER(22) not null,
  reg_jurisdiction_id  NUMBER(22) not null,
  capital_structure_id NUMBER(22) not null,
  month_year           NUMBER(6) not null,
  ending_acct_bal      NUMBER(22,2),
  avg_daily_short_bal  NUMBER(22,2),
  avg_13_mo_acct_bal   NUMBER(22,2),
  month_cost           NUMBER(22,2),
  tme_cost             NUMBER(22,2),
  rate                 NUMBER(22,8),
  percent_of_cap_mo    NUMBER(22,8),
  percent_of_cap_tme   NUMBER(22,8),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_FIN_COST_OF_CAPITAL
  is '(  )  [  ]
THE REG FIN COST OF CAPITAL TABLE CONTAINS THOSE ITEMS USED FOR THE FINANCIAL MONITORING OF THE ALLOWED RATES OF RETURN AND CAPITAL MIX AGAINST THE ACTUAL RATES OF RETURN AND CAPITAL MIX ON THE BALANCE SHEET AND INCOME STATEMENT.';
comment on column REG_FIN_COST_OF_CAPITAL.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY COMPANY.';
comment on column REG_FIN_COST_OF_CAPITAL.reg_jurisdiction_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG JURISDICTION.  THIS IS THE MAIN JURISDICTIONAL BASE RECOVERY CLASS.';
comment on column REG_FIN_COST_OF_CAPITAL.capital_structure_id
  is 'THE VARIOUS MONITORING ACCOUNTS ASSOCIATED WITH THE CAPITAL ACCOUNT TYPE, E.G. PREFERRED, COMMON, DEBT, OR ACCUMULATED DEFERRED. (NOTE, 0 = OVERALL JURISDICTION CALCULATION.)';
comment on column REG_FIN_COST_OF_CAPITAL.avg_daily_short_bal
  is 'AVERAGE DAILY SHORT TERM BALANCE IF NEEDED (INPUT) IN DOLLARS.';
comment on column REG_FIN_COST_OF_CAPITAL.avg_13_mo_acct_bal
  is '13 MONTH AVERAGE OF AVERAGES ACCOUNT BALANCE.';
comment on column REG_FIN_COST_OF_CAPITAL.month_cost
  is 'THE MONITORING ACCOUNT EXPENSE FOR THE MONTHLY E.G. INTEREST EXPENSE.';
comment on column REG_FIN_COST_OF_CAPITAL.tme_cost
  is 'THE MONITORING ACCOUNT COST FOR THE TWELVE MONTH ENDED PERIOD.';
comment on column REG_FIN_COST_OF_CAPITAL.rate
  is 'EITHER THE CALCULATED ACTUAL EMBEDDED RATE OR IF DEFERRED TAX, 0.  (NOTE COMMON RATE IS CALCULATED)';
comment on column REG_FIN_COST_OF_CAPITAL.percent_of_cap_mo
  is 'THE MONTH END PERCENT OF CAPITAL FOR THE SUB ACCOUNT.';
comment on column REG_FIN_COST_OF_CAPITAL.percent_of_cap_tme
  is 'NUMBER(  PERCENT OF CAPITAL FOR THE LAST 12 MONTH ENDED PERIOD.
22,8)';
comment on column REG_FIN_COST_OF_CAPITAL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_FIN_COST_OF_CAPITAL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_FIN_COST_OF_CAPITAL
  add constraint PK_REG_FIN_COST_OF_CAPITAL primary key (REG_COMPANY_ID, REG_JURISDICTION_ID, CAPITAL_STRUCTURE_ID, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FIN_COST_OF_CAPITAL
  add constraint R_REG_FIN_COST_OF_CAPITAL1 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_FIN_COST_OF_CAPITAL
  add constraint R_REG_FIN_COST_OF_CAPITAL2 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);
alter table REG_FIN_COST_OF_CAPITAL
  add constraint R_REG_FIN_COST_OF_CAPITAL3 foreign key (CAPITAL_STRUCTURE_ID)
  references REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID);

prompt
prompt Creating table REG_FIN_MONITOR_ALLOC_FACTOR
prompt ===========================================
prompt
create table REG_FIN_MONITOR_ALLOC_FACTOR
(
  reg_monitor_alloc_id NUMBER(22) not null,
  reg_company_id       NUMBER(22) not null,
  reg_jurisdiction_id  NUMBER(22) not null,
  effective_date       NUMBER(22) not null,
  factor               NUMBER(22,8),
  jur_percent          NUMBER(22,16),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_FIN_MONITOR_ALLOC_FACTOR
  is '(  )  [  ]
THE REGULATORY FINANCIAL MONITOR ALLOCATION TABLE CONTAINS THE FACTORS USED IN THE ALLOCATION OF FULL FINANCIALS TO JURISDICTIONS AND NON UTILITY';
comment on column REG_FIN_MONITOR_ALLOC_FACTOR.reg_monitor_alloc_id
  is 'SYSTEM_ASSIGNED IDENTIFIER OF THE ALLOCATOR';
comment on column REG_FIN_MONITOR_ALLOC_FACTOR.reg_company_id
  is 'SYSTEM_ASSIGNED IDENTIFIER OF THE COMPANY';
comment on column REG_FIN_MONITOR_ALLOC_FACTOR.reg_jurisdiction_id
  is 'SYSTEM IDENTIFIER OF THE REGULATORY JURISDICTION TO WHICH THE FACTOR APPLIES';
comment on column REG_FIN_MONITOR_ALLOC_FACTOR.effective_date
  is 'THE EFFECTIVE DATE OF THE FACTOR (YYYYMM)';
comment on column REG_FIN_MONITOR_ALLOC_FACTOR.factor
  is 'THE RAW FACTOR, E.G. NUMBER OF CUSTOMERS, IF USED';
comment on column REG_FIN_MONITOR_ALLOC_FACTOR.jur_percent
  is 'THE ALLOCATION FACTOR APPLYING TO THE JURISDICTION EXPRESSED AS A DECIMAL PERCENT';
comment on column REG_FIN_MONITOR_ALLOC_FACTOR.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_FIN_MONITOR_ALLOC_FACTOR.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_FIN_MONITOR_ALLOC_FACTOR
  add constraint PK_REG_FIN_MONITOR_ALLOC_FACTO primary key (REG_MONITOR_ALLOC_ID, REG_COMPANY_ID, REG_JURISDICTION_ID, EFFECTIVE_DATE)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FIN_MONITOR_ALLOC_FACTOR
  add constraint R_REG_FIN_MON_ALLOC_FACTOR1 foreign key (REG_MONITOR_ALLOC_ID)
  references REG_FIN_MONITOR_ALLOC (REG_MONITOR_ALLOC_ID);
alter table REG_FIN_MONITOR_ALLOC_FACTOR
  add constraint R_REG_FIN_MON_ALLOC_FACTOR2 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);

prompt
prompt Creating table REG_FIN_MONITOR_CALC_KEY
prompt =======================================
prompt
create table REG_FIN_MONITOR_CALC_KEY
(
  reg_company_id         NUMBER(22) not null,
  reg_jurisdiction_id    NUMBER(22) not null,
  reg_fin_monitor_key_id NUMBER(22) not null,
  month_year             NUMBER(6) not null,
  key_value              NUMBER(22,8),
  user_id                VARCHAR2(18),
  time_stamp             DATE
)
;
alter table REG_FIN_MONITOR_CALC_KEY
  add constraint PK_REG_FIN_MONITOR_CALC_KEY primary key (REG_COMPANY_ID, REG_JURISDICTION_ID, REG_FIN_MONITOR_KEY_ID, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FIN_MONITOR_CALC_KEY
  add constraint R_REG_FIN_MONITOR_CALC_KEY1 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_FIN_MONITOR_CALC_KEY
  add constraint R_REG_FIN_MONITOR_CALC_KEY2 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);
alter table REG_FIN_MONITOR_CALC_KEY
  add constraint R_REG_FIN_MONITOR_CALC_KEY3 foreign key (REG_FIN_MONITOR_KEY_ID)
  references REG_FIN_MONITOR_KEY (REG_FIN_MONITOR_KEY_ID);

prompt
prompt Creating table REG_FIN_MONITOR_CALC_SUMMARY
prompt ===========================================
prompt
create table REG_FIN_MONITOR_CALC_SUMMARY
(
  reg_company_id          NUMBER(22) not null,
  reg_jurisdiction_id     NUMBER(22) not null,
  reg_fin_monitor_summ_id NUMBER(22) not null,
  month_year              NUMBER(6) not null,
  reg_fin_monitor_key_id  NUMBER(22) not null,
  amount                  NUMBER(22,2) not null,
  jur_percent             NUMBER(22,8) not null,
  user_id                 VARCHAR2(18),
  time_stamp              DATE
)
;
alter table REG_FIN_MONITOR_CALC_SUMMARY
  add constraint PK_REG_FIN_MONITOR_CALC_SUMM primary key (REG_COMPANY_ID, REG_JURISDICTION_ID, REG_FIN_MONITOR_SUMM_ID, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FIN_MONITOR_CALC_SUMMARY
  add constraint R_REG_FIN_MONITOR_CALC_SUMM1 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_FIN_MONITOR_CALC_SUMMARY
  add constraint R_REG_FIN_MONITOR_CALC_SUMM2 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);
alter table REG_FIN_MONITOR_CALC_SUMMARY
  add constraint R_REG_FIN_MONITOR_CALC_SUMM3 foreign key (REG_FIN_MONITOR_SUMM_ID)
  references REG_FIN_MONITOR_SUMMARY (REG_FIN_MONITOR_SUMM_ID);
alter table REG_FIN_MONITOR_CALC_SUMMARY
  add constraint R_REG_FIN_MONITOR_CALC_SUMM4 foreign key (REG_FIN_MONITOR_KEY_ID)
  references REG_FIN_MONITOR_KEY (REG_FIN_MONITOR_KEY_ID);

prompt
prompt Creating table REG_FIN_MONITOR_PROCESS_TEMP
prompt ===========================================
prompt
create global temporary table REG_FIN_MONITOR_PROCESS_TEMP
(
  reg_company_id         NUMBER(22) not null,
  reg_jurisdiction_id    NUMBER(22) not null,
  ptbi                   NUMBER(22,2),
  pre_tax_perm_items     NUMBER(22,2),
  net_state_tax          NUMBER(22,2),
  net_fed_tax            NUMBER(22,2),
  total_tax_expense      NUMBER(22,2),
  net_income             NUMBER(22,2),
  com_stock_ret_earnings NUMBER(22,2),
  interest               NUMBER(22,2),
  return_on_dollars      NUMBER(22,2)
)
on commit delete rows;
comment on table REG_FIN_MONITOR_PROCESS_TEMP
  is '(  )  [  ]
THE REG FIN MONITOR PROCESS TEMP TABLE IS A TEMPORARY TABLE USED IN DURING FINANCIAL MONITORING CALCULATIONS.';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG COMPANY';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.reg_jurisdiction_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG JURISDICTION';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.ptbi
  is 'THE VALUE FOR PRE TAX BOOK INCOME';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.pre_tax_perm_items
  is 'THE VALUE FOR PRE TAX PERMANENT ITEMS';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.net_state_tax
  is 'THE VALUE FOR NET STATE TAX';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.net_fed_tax
  is 'THE VALUE FOR NET FEDERAL TAX';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.total_tax_expense
  is 'TOTAL TAX EXPENSE';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.net_income
  is 'NET INCOME';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.com_stock_ret_earnings
  is 'COMMON STOCK RETAINED EARNINGS';
comment on column REG_FIN_MONITOR_PROCESS_TEMP.return_on_dollars
  is 'RETURN ON DOLLARS';
alter table REG_FIN_MONITOR_PROCESS_TEMP
  add constraint PK_REG_FIN_MONITOR_PROCESS_TMP primary key (REG_COMPANY_ID, REG_JURISDICTION_ID);

prompt
prompt Creating table REG_FIN_MONITOR_RESULT
prompt =====================================
prompt
create table REG_FIN_MONITOR_RESULT
(
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  reg_jurisdiction_id NUMBER(22) not null,
  month_year          NUMBER(6) not null,
  book_amount         NUMBER(22,2) not null,
  jur_amount          NUMBER(22,2) not null,
  jur_percent         NUMBER(22,16) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_FIN_MONITOR_RESULT
  add constraint PK_REG_FIN_MONITOR_RESULT primary key (REG_COMPANY_ID, REG_ACCT_ID, REG_JURISDICTION_ID, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FIN_MONITOR_RESULT
  add constraint R_REG_FIN_MONITOR_RESULT1 foreign key (REG_ACCT_ID, REG_COMPANY_ID)
  references REG_ACCT_COMPANY (REG_ACCT_ID, REG_COMPANY_ID);
alter table REG_FIN_MONITOR_RESULT
  add constraint R_REG_FIN_MONITOR_RESULT2 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_FIN_MONITOR_RESULT
  add constraint R_REG_FIN_MONITOR_RESULT3 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);

prompt
prompt Creating table REG_FIN_MONITOR_SUB_ACCT
prompt =======================================
prompt
create table REG_FIN_MONITOR_SUB_ACCT
(
  reg_company_id          NUMBER(22) not null,
  reg_acct_type_id        NUMBER(22) not null,
  sub_acct_type_id        NUMBER(22) not null,
  reg_fin_monitor_summ_id NUMBER(22) not null,
  user_id                 VARCHAR2(18),
  time_stamp              DATE
)
;
alter table REG_FIN_MONITOR_SUB_ACCT
  add constraint PK_REG_FIN_MONITOR_SUB_ACCT primary key (REG_COMPANY_ID, REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FIN_MONITOR_SUB_ACCT
  add constraint R_REG_FIN_MONITOR_SUB_ACCT1 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_FIN_MONITOR_SUB_ACCT
  add constraint R_REG_FIN_MONITOR_SUB_ACCT2 foreign key (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);

prompt
prompt Creating table REG_FIN_MONITOR_TYPE
prompt ===================================
prompt
create table REG_FIN_MONITOR_TYPE
(
  reg_fin_monitor_id             NUMBER(22) not null,
  description                    VARCHAR2(35) not null,
  cap_structure_indicator        NUMBER(22),
  user_id                        VARCHAR2(18),
  time_stamp                     DATE,
  associated_expense_fin_monitor NUMBER(22),
  use_for_net_asset              NUMBER(22),
  use_for_cap_struct             NUMBER(22),
  use_for_interest               NUMBER(22),
  use_for_ptbi                   NUMBER(22),
  use_for_taxes                  NUMBER(22),
  use_for_com_stock_ret_earnings NUMBER(22)
)
;
alter table REG_FIN_MONITOR_TYPE
  add constraint REG_FIN_MONITOR_TYPE_PK primary key (REG_FIN_MONITOR_ID);

prompt
prompt Creating table REG_FORECAST_LEDGER
prompt ==================================
prompt
create table REG_FORECAST_LEDGER
(
  forecast_version_id NUMBER(22) not null,
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  gl_month            NUMBER(22,1) not null,
  fcst_amount         NUMBER(22,2),
  annualized_amt      NUMBER(22,2),
  adj_amount          NUMBER(22,2),
  adj_month           NUMBER(22),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_FORECAST_LEDGER
  is '(  )  [  ]
THE REG FORECAST LEDGER CONTAINS THE RESULTS OF FORECASTS SUMMARIZED (OR EXPANDED) TO REGULATORY ACCOUNTS.  IT IS THE FUNDAMENTAL DATA FROM WHICH A REGULATORY CASE IS CONSTRUCTED.  (SEE ALSO THE REG FORECAST VERSION TABLE).';
comment on column REG_FORECAST_LEDGER.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FORECAST COMPANY';
comment on column REG_FORECAST_LEDGER.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FORECAST REGULATORY ACCOUNT';
comment on column REG_FORECAST_LEDGER.gl_month
  is '“GENERAL LEDGER” MONTH OF THE AMOUNT IN YYYYMM FORMAT';
comment on column REG_FORECAST_LEDGER.user_id
  is 'STANDARD SYSTEM ASSIGNED USER ID FOR AUDIT PURPOSES';
comment on column REG_FORECAST_LEDGER.time_stamp
  is 'STANDARD SYSTEM ASSIGNED TIME STAMP FOR AUDIT PURPOSES';
alter table REG_FORECAST_LEDGER
  add constraint PK_REG_FORECAST_LEDGER primary key (FORECAST_VERSION_ID, REG_COMPANY_ID, REG_ACCT_ID, GL_MONTH)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FORECAST_LEDGER
  add constraint R_REG_FORECAST_LEDGER1 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_FORECAST_LEDGER
  add constraint R_REG_FORECAST_LEDGER2 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_FORECAST_LEDGER
  add constraint R_REG_FORECAST_LEDGER3 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

prompt
prompt Creating table REG_FORECAST_LEDGER_ADJUST
prompt =========================================
prompt
create table REG_FORECAST_LEDGER_ADJUST
(
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  forecast_version_id NUMBER(22) not null,
  gl_month            NUMBER(22,1) not null,
  adjust_id           NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  "COMMENT"           VARCHAR2(2000) not null,
  adjust_amount       NUMBER(22,2) not null,
  adjust_month        NUMBER(22),
  reverse_flag        NUMBER(22) not null,
  reverse_month       NUMBER(22)
)
;
alter table REG_FORECAST_LEDGER_ADJUST
  add constraint PK_REG_FORECAST_LEDGER_ADJUST primary key (REG_COMPANY_ID, REG_ACCT_ID, FORECAST_VERSION_ID, GL_MONTH, ADJUST_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_FORECAST_LEDGER_ADJUST
  add constraint R_REG_FORECAST_LEDGER_ADJUST1 foreign key (FORECAST_VERSION_ID, REG_COMPANY_ID, REG_ACCT_ID, GL_MONTH)
  references REG_FORECAST_LEDGER (FORECAST_VERSION_ID, REG_COMPANY_ID, REG_ACCT_ID, GL_MONTH);

prompt
prompt Creating table REG_HIST_RECON_ITEMS
prompt ===================================
prompt
create table REG_HIST_RECON_ITEMS
(
  recon_id    NUMBER(22) not null,
  description VARCHAR2(35) not null,
  status      NUMBER(22) not null,
  user_id     VARCHAR2(18),
  time_stamp  DATE
)
;
comment on table REG_HIST_RECON_ITEMS
  is '(  )  [  ]
THE REG HIST RECON ITEMS TABLE STORES UNIQUE RECONCILIATION ITEMS USED TO IDENTIFY DISCREPANCIES BETWEEN THE HISTORIC LEDGER AND CR BALANCES';
comment on column REG_HIST_RECON_ITEMS.recon_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE RECONCILIATION ITEM.  TWO SPECIAL IDS ARE USED:  0 = NOT USED; -1 = NOT MAPPED';
comment on column REG_HIST_RECON_ITEMS.description
  is 'DESCRIPTION OF THE RECONCILIATION ITEM, E.G. ''101 PLANT IN SERVICE GAS''';
comment on column REG_HIST_RECON_ITEMS.status
  is '1 = ACTIVE, 0 = INACTIVE';
comment on column REG_HIST_RECON_ITEMS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_HIST_RECON_ITEMS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_HIST_RECON_ITEMS
  add constraint PK_REG_HIST_RECON_ITEMS primary key (RECON_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_HISTORIC_RECON_LEDGER
prompt ========================================
prompt
create table REG_HISTORIC_RECON_LEDGER
(
  historic_version_id NUMBER(22) not null,
  recon_id            NUMBER(22) not null,
  gl_month            NUMBER(22,1) not null,
  reg_company_id      NUMBER(22) not null,
  cr_balances_amt     NUMBER(22,2),
  hist_reg_acct_amt   NUMBER(22,2),
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  recon_adj_status    NUMBER(22),
  recon_comment       VARCHAR2(1000),
  recon_adj_amount    NUMBER(22,2)
)
;
alter table REG_HISTORIC_RECON_LEDGER
  add constraint PK_REG_HISTORIC_RECON_LEDGER primary key (HISTORIC_VERSION_ID, RECON_ID, GL_MONTH, REG_COMPANY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_HISTORIC_RECON_LEDGER
  add constraint R_REG_HISTORIC_RECON_LEDGER1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_HISTORIC_RECON_LEDGER
  add constraint R_REG_HISTORIC_RECON_LEDGER2 foreign key (RECON_ID)
  references REG_HIST_RECON_ITEMS (RECON_ID);
alter table REG_HISTORIC_RECON_LEDGER
  add constraint R_REG_HISTORIC_RECON_LEDGER3 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);

prompt
prompt Creating table REG_HISTORY_LEDGER
prompt =================================
prompt
create table REG_HISTORY_LEDGER
(
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  historic_version_id NUMBER(22) not null,
  gl_month            NUMBER(22,1) not null,
  act_amount          NUMBER(22,2) not null,
  annualized_amt      NUMBER(22,2),
  adj_amount          NUMBER(22,2),
  adj_month           NUMBER(22),
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  recon_adj_amount    NUMBER(22,2),
  recon_adj_comment   VARCHAR2(1000)
)
;
comment on table REG_HISTORY_LEDGER
  is '(  )  [  ]
THE REG HISTORY LEDGER TABLE CONTAINS THE COMPANY''S MONTHLY ACTUAL DATA SUMMARIZED BY REGULATORY ACCOUNT.';
comment on column REG_HISTORY_LEDGER.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE POWERPLANT COMPANY.';
comment on column REG_HISTORY_LEDGER.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_HISTORY_LEDGER.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE HISTORIC “VERSION” OR LEDGER.';
comment on column REG_HISTORY_LEDGER.gl_month
  is 'GENERAL LEDGER POSTING ACCOUNTING MONTH YYYYMM.S';
comment on column REG_HISTORY_LEDGER.act_amount
  is 'THE ACTUAL MONTH END ACCOUNT BALANCE OR MONTHLY AMOUNT.';
comment on column REG_HISTORY_LEDGER.annualized_amt
  is 'THE ANNUALIZED AMOUNT FOR FINANCIAL ALLOCATION';
comment on column REG_HISTORY_LEDGER.adj_amount
  is 'THE ADJUSTMENT TO CORRECT PRIOR PERIOD AMOUNTS ON THE GENERAL LEDGER.';
comment on column REG_HISTORY_LEDGER.adj_month
  is 'THE GENERAL LEDGER POSTING MONTH FROM WHERE THE ADJUSTMENT WAS MOVED.  YYYYMM FORMAT';
comment on column REG_HISTORY_LEDGER.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_HISTORY_LEDGER.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_HISTORY_LEDGER
  add constraint PK_REG_HISTORY_LEDGER primary key (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_HISTORY_LEDGER
  add constraint R_REG_HISTORY_LEDGER1 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_HISTORY_LEDGER
  add constraint R_REG_HISTORY_LEDGER2 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_HISTORY_LEDGER
  add constraint R_REG_HISTORY_LEDGER3 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

prompt
prompt Creating table REG_HISTORY_LEDGER_ADJUST
prompt ========================================
prompt
create table REG_HISTORY_LEDGER_ADJUST
(
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  historic_version_id NUMBER(22) not null,
  gl_month            NUMBER(22,1) not null,
  adjust_id           NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  "COMMENT"           VARCHAR2(2000) not null,
  adjust_amount       NUMBER(22,2) not null,
  adjust_month        NUMBER(22),
  reverse_flag        NUMBER(22) not null,
  reverse_month       NUMBER(22)
)
;
alter table REG_HISTORY_LEDGER_ADJUST
  add constraint PK_REG_HISTORY_LEDGER_ADJUST primary key (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH, ADJUST_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_HISTORY_LEDGER_ADJUST
  add constraint R_REG_HISTORY_LEDGER_ADJUST1 foreign key (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH)
  references REG_HISTORY_LEDGER (REG_COMPANY_ID, REG_ACCT_ID, HISTORIC_VERSION_ID, GL_MONTH);

prompt
prompt Creating table REG_HIST_CR_LOAD_TEMP
prompt ====================================
prompt
create global temporary table REG_HIST_CR_LOAD_TEMP
(
  reg_company_id      NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  cr_combo_id         NUMBER(22) not null,
  historic_version_id NUMBER(22) not null,
  gl_month            NUMBER(22,1) not null,
  act_amount          NUMBER(22,2)
)
on commit preserve rows;
alter table REG_HIST_CR_LOAD_TEMP
  add constraint PK_REG_HIST_CR_LOAD_TEMP primary key (REG_COMPANY_ID, REG_ACCT_ID, CR_COMBO_ID, HISTORIC_VERSION_ID, GL_MONTH);

prompt
prompt Creating table REG_HIST_RECON_CR_COMBOS
prompt =======================================
prompt
create table REG_HIST_RECON_CR_COMBOS
(
  cr_balances_combo_id NUMBER(22) not null,
  cr_element_id        NUMBER(22) not null,
  structure_id         NUMBER(22),
  lower_value          VARCHAR2(77),
  upper_value          VARCHAR2(77),
  description          VARCHAR2(35),
  pull_type_id         NUMBER(22),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_HIST_RECON_CR_COMBOS
  is '(  )  [  ]
THE REG HIST RECON CR COMBOS TABLE CONTAINS THE RESTRICTIONS PER ELEMENT FOR EACH CR BALANCES COMBO DEFINED FOR HISTORIC RECONCILIATION.';
comment on column REG_HIST_RECON_CR_COMBOS.cr_balances_combo_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A CR BALANCE COMBO';
comment on column REG_HIST_RECON_CR_COMBOS.cr_element_id
  is 'CR ELEMENT ID FROM LIST OF ELEMENTS USED FOR RECONCILIATION';
comment on column REG_HIST_RECON_CR_COMBOS.structure_id
  is 'POPULATED WITH CR STRUCTURE ID IF STRUCTURE IS USED FOR MAPPING';
comment on column REG_HIST_RECON_CR_COMBOS.lower_value
  is 'LOWER VALUE IN A RANGE; SINGLE VALUE; STRUCTURE NODE VALUE';
comment on column REG_HIST_RECON_CR_COMBOS.upper_value
  is 'UPPER VALUE IN A RANGE';
comment on column REG_HIST_RECON_CR_COMBOS.description
  is 'DESCRIPTION OF THE RECONCILIATION';
comment on column REG_HIST_RECON_CR_COMBOS.pull_type_id
  is 'HOW TO PULL THE AMOUNT, I.E. ''BALANCE'' OR ''ACTIVITY''';
comment on column REG_HIST_RECON_CR_COMBOS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_HIST_RECON_CR_COMBOS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_HIST_RECON_CR_COMBOS
  add constraint PK_REG_HIST_RECON_CR_COMBOS primary key (CR_BALANCES_COMBO_ID, CR_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_HIST_RECON_CR_COMBOS
  add constraint R_REG_HIST_RECON_CR_COMBOS1 foreign key (STRUCTURE_ID)
  references CR_STRUCTURES (STRUCTURE_ID);
alter table REG_HIST_RECON_CR_COMBOS
  add constraint R_REG_HIST_RECON_CR_COMBOS2 foreign key (PULL_TYPE_ID)
  references REG_CR_PULL_TYPE (PULL_TYPE_ID);

prompt
prompt Creating table REG_HIST_RECON_CR_ELEMENTS
prompt =========================================
prompt
create table REG_HIST_RECON_CR_ELEMENTS
(
  cr_element_id        NUMBER(22) not null,
  cr_account_field     NUMBER(22),
  element_order        NUMBER(22),
  default_structure_id NUMBER(22),
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_HIST_RECON_CR_ELEMENTS
  is '(  )  [  ]
THE REG HIST RECON CR ELEMENTS TABLE CONTAINS THE SUBSET OF CR ELEMENTS USED FOR HISTORIC RECONCILIATION.  THE GL ACCOUNT FIELD FROM THE CR MUST BE INCLUDED.';
comment on column REG_HIST_RECON_CR_ELEMENTS.cr_element_id
  is 'CR ELEMENT ID';
comment on column REG_HIST_RECON_CR_ELEMENTS.cr_account_field
  is '1 INDICATES THE ACCOUNT FIELD, 0 OTHERWISE';
comment on column REG_HIST_RECON_CR_ELEMENTS.element_order
  is 'DISPLAY ORDER';
comment on column REG_HIST_RECON_CR_ELEMENTS.default_structure_id
  is 'CR STRUCTURE ID USED TO STORE UNORDERED LISTS OF ELEMENT VALUES';
comment on column REG_HIST_RECON_CR_ELEMENTS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_HIST_RECON_CR_ELEMENTS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_HIST_RECON_CR_ELEMENTS
  add constraint PK_REG_HIST_RECON_CR_ELEMENTS primary key (CR_ELEMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_HIST_RECON_CR_MAP
prompt ====================================
prompt
create table REG_HIST_RECON_CR_MAP
(
  historic_version_id  NUMBER(22) not null,
  recon_id             NUMBER(22) not null,
  cr_balances_combo_id NUMBER(22) not null,
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_HIST_RECON_CR_MAP
  is '(  )  [  ]
THE REG HIST RECON CR MAP TABLE STORES THE MAPPING OF CR BALANCES COMBOS TO RECON ITEMS BY HISTORIC VERSION USED FOR RECONCILING REG ACCOUNTS TO THE GL.';
comment on column REG_HIST_RECON_CR_MAP.historic_version_id
  is 'SYSTEM_ID OF THE HISTORIC VERSION';
comment on column REG_HIST_RECON_CR_MAP.recon_id
  is 'ID USED TO IDENTIFY EACH UNIQUE RECONCILIATION ITEM';
comment on column REG_HIST_RECON_CR_MAP.cr_balances_combo_id
  is 'CR BALANCES COMBO ASSIGNED TO RECON MAPPING';
comment on column REG_HIST_RECON_CR_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_HIST_RECON_CR_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_HIST_RECON_CR_MAP
  add constraint PK_REG_HIST_RECON_CR_MAP primary key (HISTORIC_VERSION_ID, RECON_ID, CR_BALANCES_COMBO_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_HIST_RECON_CR_MAP
  add constraint R_REG_HIST_RECON_CR_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_HIST_RECON_CR_MAP
  add constraint R_REG_HIST_RECON_CR_MAP2 foreign key (RECON_ID)
  references REG_HIST_RECON_ITEMS (RECON_ID);

prompt
prompt Creating table REG_HIST_RECON_REGACCT_MAP
prompt =========================================
prompt
create table REG_HIST_RECON_REGACCT_MAP
(
  historic_version_id NUMBER(22) not null,
  recon_id            NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_HIST_RECON_REGACCT_MAP
  is '(  )  [  ]
STORES THE MAPPING OF REG ACCOUNTS TO RECON ITEMS BY HISTORIC VERSION USED FOR RECONCILING REG ACCOUNTS TO THE GL.';
comment on column REG_HIST_RECON_REGACCT_MAP.historic_version_id
  is 'SYSTEM IDENTIFIER OF AN HISTORIC VERSION';
comment on column REG_HIST_RECON_REGACCT_MAP.recon_id
  is 'ID USED TO IDENTIFY EACH UNIQUE RECONCILIATION ITEM';
comment on column REG_HIST_RECON_REGACCT_MAP.reg_acct_id
  is 'ID OF REG ACCOUNT ASSIGNED TO THE RECON ID';
comment on column REG_HIST_RECON_REGACCT_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_HIST_RECON_REGACCT_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_HIST_RECON_REGACCT_MAP
  add constraint PK_REG_HIST_RECON_REGACCT_MAP primary key (HISTORIC_VERSION_ID, RECON_ID, REG_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_HIST_RECON_REGACCT_MAP
  add constraint R_REG_HIST_RECON_REGACCT_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_HIST_RECON_REGACCT_MAP
  add constraint R_REG_HIST_RECON_REGACCT_MAP2 foreign key (RECON_ID)
  references REG_HIST_RECON_ITEMS (RECON_ID);
alter table REG_HIST_RECON_REGACCT_MAP
  add constraint R_REG_HIST_RECON_REGACCT_MAP3 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);

prompt
prompt Creating table REG_IMPORT_ALLOC_FACTOR
prompt ======================================
prompt
create table REG_IMPORT_ALLOC_FACTOR
(
  reg_import_id     NUMBER(22) not null,
  reg_allocator     VARCHAR2(35) not null,
  reg_alloc_target  VARCHAR2(35) not null,
  effective_date    NUMBER not null,
  statistical_value NUMBER(22,8),
  source_file       VARCHAR2(55),
  import_status     VARCHAR2(10),
  user_id           VARCHAR2(18),
  time_stamp        DATE
)
;
alter table REG_IMPORT_ALLOC_FACTOR
  add constraint PK_REG_ALLOC_FACTOR_IMPORT primary key (REG_IMPORT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_IMPORT_ALLO_FACTOR_STG
prompt =========================================
prompt
create table REG_IMPORT_ALLO_FACTOR_STG
(
  import_run_id               NUMBER(22) not null,
  line_id                     NUMBER(22) not null,
  error_message               VARCHAR2(4000),
  reg_allocator_id_xlate      VARCHAR2(254),
  reg_alloc_target_id_xlate   VARCHAR2(254),
  reg_allocator_id            VARCHAR2(35),
  effective_date              VARCHAR2(35),
  reg_alloc_target_id         VARCHAR2(35),
  base_year_end               VARCHAR2(35),
  annual_reg_factor           VARCHAR2(35),
  statistical_value           VARCHAR2(35),
  last_updated                VARCHAR2(35),
  last_updated_by             VARCHAR2(35),
  user_id                     VARCHAR2(35),
  time_stamp                  VARCHAR2(35),
  unit_of_measure             VARCHAR2(35),
  reg_jurisdiction_id_xlate   VARCHAR2(254),
  reg_jurisdiction_id         NUMBER(22),
  reg_alloc_category_id_xlate VARCHAR2(254),
  reg_alloc_category_id       NUMBER(22)
)
;
alter table REG_IMPORT_ALLO_FACTOR_STG
  add constraint REG_IMPORT_ALLOC_FACTOR_STG_PK primary key (IMPORT_RUN_ID, LINE_ID);

prompt
prompt Creating table REG_IMPORT_ALLO_FACTOR_STG_ARC
prompt =============================================
prompt
create table REG_IMPORT_ALLO_FACTOR_STG_ARC
(
  import_run_id               NUMBER,
  line_id                     NUMBER,
  reg_allocator_id_xlate      VARCHAR2(35),
  reg_alloc_target_id_xlate   VARCHAR2(35),
  reg_allocator_id            VARCHAR2(35),
  effective_date              VARCHAR2(35),
  reg_alloc_target_id         VARCHAR2(35),
  base_year_end               VARCHAR2(35),
  annual_reg_factor           VARCHAR2(35),
  statistical_value           VARCHAR2(35),
  last_updated                VARCHAR2(35),
  last_updated_by             VARCHAR2(35),
  user_id                     VARCHAR2(35),
  time_stamp                  VARCHAR2(35),
  unit_of_measure             VARCHAR2(35),
  reg_jurisdiction_id_xlate   VARCHAR2(254),
  reg_jurisdiction_id         NUMBER(22),
  reg_alloc_category_id_xlate VARCHAR2(254),
  reg_alloc_category_id       NUMBER(22)
)
;

prompt
prompt Creating table REG_IMPORT_HISTORY_LDG
prompt =====================================
prompt
create table REG_IMPORT_HISTORY_LDG
(
  import_run_id          NUMBER(22) not null,
  line_id                NUMBER(22) not null,
  time_stamp             DATE,
  user_id                VARCHAR2(18),
  reg_company_xlate      VARCHAR2(254),
  reg_company_id         NUMBER(22),
  reg_acct_xlate         VARCHAR2(254),
  reg_acct_id            NUMBER(22),
  historic_version_xlate VARCHAR2(254),
  historic_version_id    NUMBER(22),
  gl_month               VARCHAR2(35),
  act_amount             VARCHAR2(35),
  adj_amount             VARCHAR2(35),
  adj_month              VARCHAR2(35),
  is_modified            NUMBER(22),
  error_message          VARCHAR2(4000)
)
;
alter table REG_IMPORT_HISTORY_LDG
  add constraint REG_IMP_HIST_LDG_PK primary key (IMPORT_RUN_ID, LINE_ID);
alter table REG_IMPORT_HISTORY_LDG
  add constraint REG_IMP_HIST_LDG_RUN_FK foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);

prompt
prompt Creating table REG_IMPORT_HISTORY_LDG_ARC
prompt =========================================
prompt
create table REG_IMPORT_HISTORY_LDG_ARC
(
  import_run_id          NUMBER(22) not null,
  line_id                NUMBER(22) not null,
  time_stamp             DATE,
  user_id                VARCHAR2(18),
  reg_company_xlate      VARCHAR2(254),
  reg_company_id         NUMBER(22),
  reg_acct_xlate         VARCHAR2(254),
  reg_acct_id            NUMBER(22),
  historic_version_xlate VARCHAR2(254),
  historic_version_id    NUMBER(22),
  gl_month               VARCHAR2(35),
  act_amount             VARCHAR2(35),
  adj_amount             VARCHAR2(35),
  adj_month              VARCHAR2(35),
  is_modified            NUMBER(22)
)
;
alter table REG_IMPORT_HISTORY_LDG_ARC
  add constraint REG_IMP_HIST_LDG_ARC_PK primary key (IMPORT_RUN_ID, LINE_ID);
alter table REG_IMPORT_HISTORY_LDG_ARC
  add constraint REG_IMP_HIST_LDG_ARC_RUN_FK foreign key (IMPORT_RUN_ID)
  references PP_IMPORT_RUN (IMPORT_RUN_ID);

prompt
prompt Creating table REG_INTERFACE
prompt ============================
prompt
create table REG_INTERFACE
(
  reg_interface_id NUMBER(22) not null,
  description      VARCHAR2(35) not null,
  act_or_fcst_type NUMBER(22) not null,
  long_description VARCHAR2(254),
  user_id          VARCHAR2(18),
  time_stamp       DATE,
  process_order    NUMBER(22),
  button_text      VARCHAR2(35),
  action           NUMBER(22)
)
;
comment on table REG_INTERFACE
  is '(  )  [  ]
THE REG PROCESS TABLE TRACKS THE VARIOUS INTEGRATION PROCESSES TO BE COMPLETED.';
comment on column REG_INTERFACE.reg_interface_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY PROCESS.';
comment on column REG_INTERFACE.description
  is 'DESCRIPTION OF PROCESS';
comment on column REG_INTERFACE.act_or_fcst_type
  is '1 = ACTUAL HISTORIC LEDGER, 2 = FORECAST';
comment on column REG_INTERFACE.long_description
  is 'LONG DESCRIPTION OF THE PROCESS.';
comment on column REG_INTERFACE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_INTERFACE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_INTERFACE.process_order
  is 'ORDER OF BUTTON(S) TO APPEAR ON THE WINDOW (''1'' IS FIRST)';
comment on column REG_INTERFACE.button_text
  is 'TEXT FOR THE BUTTON ON THE WINDOW';
comment on column REG_INTERFACE.action
  is '(WAY TO TABLE DRIVE REACTION)';
create unique index PK_REG_PROCESS on REG_INTERFACE (REG_INTERFACE_ID) tablespace PWRPLANT_IDX;
alter table REG_INTERFACE
  add constraint PK_REG_INTERFACE primary key (REG_INTERFACE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_INTERFACE_CONTROL
prompt ====================================
prompt
create table REG_INTERFACE_CONTROL
(
  reg_interface_id    NUMBER(22) not null,
  historic_version_id NUMBER(22) not null,
  forecast_version_id NUMBER(22) not null,
  reg_company_id      NUMBER(22) not null,
  month_year          NUMBER(22,1) not null,
  last_run_date       DATE,
  last_run_by         VARCHAR2(18),
  reconciled          DATE,
  approved            DATE,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_INTERFACE_CONTROL
  is '(  )  [  ]
THE REG INTERFACE CONTROL TABLE STORES THE LIST OF INTERFACES USED FOR LOADING DATA INTO THE HISTORIC AND FORECAST LEDGERS.';
comment on column REG_INTERFACE_CONTROL.reg_interface_id
  is 'LIST OF INTERFACES TO LOAD DATA INTO THE HISTORIC OR FORECAST LEDGER';
comment on column REG_INTERFACE_CONTROL.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE HISTORIC VERSION';
comment on column REG_INTERFACE_CONTROL.forecast_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE FORECAST VERSION';
comment on column REG_INTERFACE_CONTROL.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REG COMPANY';
comment on column REG_INTERFACE_CONTROL.month_year
  is 'ACCOUNTING MONTH (YYYYMM.S FORMAT)';
comment on column REG_INTERFACE_CONTROL.last_run_date
  is 'LAST DATE THE PROCESS WAS RUN.';
comment on column REG_INTERFACE_CONTROL.last_run_by
  is 'LAST USER TO RUN THE PROCESS.';
comment on column REG_INTERFACE_CONTROL.reconciled
  is 'DATE THAT THE PROCESS WAS RECONCILED.';
comment on column REG_INTERFACE_CONTROL.approved
  is 'DATE THAT THE RECON OF THE PROCESS WAS REVIEWED AND APPROVED.';
comment on column REG_INTERFACE_CONTROL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_INTERFACE_CONTROL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_INTERFACE_CONTROL
  add constraint PK_REG_INTERFACE_CONTROL primary key (REG_INTERFACE_ID, HISTORIC_VERSION_ID, FORECAST_VERSION_ID, REG_COMPANY_ID, MONTH_YEAR)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_INTERFACE_CONTROL
  add constraint R_REG_INTERFACE_CONTROL1 foreign key (REG_INTERFACE_ID)
  references REG_INTERFACE (REG_INTERFACE_ID);
alter table REG_INTERFACE_CONTROL
  add constraint R_REG_INTERFACE_CONTROL2 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_INTERFACE_CONTROL
  add constraint R_REG_INTERFACE_CONTROL3 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);
alter table REG_INTERFACE_CONTROL
  add constraint R_REG_INTERFACE_CONTROL4 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);

prompt
prompt Creating table REG_ISSUE
prompt ========================
prompt
create table REG_ISSUE
(
  issue_id         NUMBER(22) not null,
  long_description VARCHAR2(254) not null,
  issue_cause      VARCHAR2(254),
  user_id          VARCHAR2(18),
  time_stamp       DATE
)
;
comment on table REG_ISSUE
  is '(  )  [  ]
THE REG ISSUE TABLE ALLOWS THE USER TO LINK MULTIPLE ADJUSTMENTS WITH A PARTICULAR ISSUE, FOR EXAMPLE, NORMALIZING OUT THE EFFECTS OF A PARTICULAR STORM.  ISSUES CAN BE ONE TIME OR RECURRING.';
comment on column REG_ISSUE.issue_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE ISSUE.';
comment on column REG_ISSUE.long_description
  is 'DESCRIPTION OF THE ISSUE.';
comment on column REG_ISSUE.issue_cause
  is 'OPTIONAL FIELD FOR TYPE OF ISSUE.';
comment on column REG_ISSUE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_ISSUE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_ISSUE
  add constraint PK_REG_ISSUE primary key (ISSUE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_JUR_ADJUST_CALC_EXT
prompt ======================================
prompt
create table REG_JUR_ADJUST_CALC_EXT
(
  reg_jur_template_id NUMBER(22) not null,
  adjustment_id       NUMBER(22) not null,
  file_path           VARCHAR2(2000),
  file_name           VARCHAR2(254),
  document_data       BLOB,
  file_size           NUMBER(22),
  attachment_type     VARCHAR2(20),
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_JUR_ADJUST_CALC_EXT
  add constraint PK_REG_JUR_ADJUST_CALC_EXT primary key (REG_JUR_TEMPLATE_ID, ADJUSTMENT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_ADJUST_CALC_EXT
  add constraint R_REG_JUR_ADJUST_CALC_EXT1 foreign key (ADJUSTMENT_ID, REG_JUR_TEMPLATE_ID)
  references REG_ADJUST_JUR_DEFAULT (ADJUSTMENT_ID, REG_JUR_TEMPLATE_ID);

prompt
prompt Creating table REG_JUR_ALLOC_ACCOUNT
prompt ====================================
prompt
create table REG_JUR_ALLOC_ACCOUNT
(
  reg_jur_template_id      NUMBER(22) not null,
  reg_alloc_acct_id        NUMBER(22) not null,
  long_description         VARCHAR2(254) not null,
  reg_calc_status_id       NUMBER(22) not null,
  reg_acct_id              NUMBER(22) not null,
  reg_alloc_category_id    NUMBER(22) not null,
  reg_alloc_target_id      NUMBER(22),
  reg_allocator_id         NUMBER(22),
  parent_reg_alloc_acct_id NUMBER(22),
  user_id                  VARCHAR2(18),
  time_stamp               DATE
)
;
comment on table REG_JUR_ALLOC_ACCOUNT
  is '(  )  [  ]
THE REG JUR ALLOC ACCOUNT TABLE CONTAINS THE DYNAMICALLY GENERATED REGULATORY ACCOUNT BREAKDOWNS WITHIN A JURISDICTIONAL TEMPLATE BASED ON THE ALLOCATION CATEGORIES (AND RELATED TARGETS).';
comment on column REG_JUR_ALLOC_ACCOUNT.reg_jur_template_id
  is 'SYSTEM ASSIGNED DEFAULT TEMPLATE FOR A JURISDICTION.  TEMPLATES ARE FOR A PARTICULAR JURISDICTION, BUT MULTIPLE TEMPLATES CAN BE DEFINED FOR A JURISDICTION.';
comment on column REG_JUR_ALLOC_ACCOUNT.reg_alloc_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION ACCOUNT.  NOTE, WILL EQUAL REG_ACCT_ID FOR STEP 0.';
comment on column REG_JUR_ALLOC_ACCOUNT.long_description
  is 'DESCRIPTION OF THE REGULATORY ALLOCATION ACCOUNT.';
comment on column REG_JUR_ALLOC_ACCOUNT.reg_calc_status_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY STATUS. E.G. SETUP/CREATED, RESULT CALCULATED, NO RESULT CALCULATED.';
comment on column REG_JUR_ALLOC_ACCOUNT.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_JUR_ALLOC_ACCOUNT.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION CATEGORY THAT CREATES THE ALLOC ACCOUNT.';
comment on column REG_JUR_ALLOC_ACCOUNT.reg_alloc_target_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION TARGET THAT CREATES THIS SUB ACCOUNT.';
comment on column REG_JUR_ALLOC_ACCOUNT.reg_allocator_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATOR THAT THIS REGULATORY ALLOCATION ACCOUNT WILL USE IN THE NEXT ALLOCATION STEP.';
comment on column REG_JUR_ALLOC_ACCOUNT.parent_reg_alloc_acct_id
  is 'THE PROCEDING ACCOUNT OR ALLOCATION ACCOUNT FROM WHICH THE ALLOCATION WAS DERIVED';
comment on column REG_JUR_ALLOC_ACCOUNT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_JUR_ALLOC_ACCOUNT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_JUR_ALLOC_ACCOUNT
  add constraint PK_REG_JUR_ALLOC_ACCOUNT primary key (REG_JUR_TEMPLATE_ID, REG_ALLOC_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_ALLOC_ACCOUNT
  add constraint R_REG_JUR_ALLOC_ACCOUNT2 foreign key (REG_JUR_TEMPLATE_ID, REG_ACCT_ID)
  references REG_JUR_ACCT_DEFAULT (REG_JUR_TEMPLATE_ID, REG_ACCT_ID);
alter table REG_JUR_ALLOC_ACCOUNT
  add constraint R_REG_JUR_ALLOC_ACCOUNT3 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_JUR_ALLOC_ACCOUNT
  add constraint R_REG_JUR_ALLOC_ACCOUNT4 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);
alter table REG_JUR_ALLOC_ACCOUNT
  add constraint R_REG_JUR_ALLOC_ACCOUNT5 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);

prompt
prompt Creating table REG_JUR_ADJ_CALC_INPUT_ACCT
prompt ==========================================
prompt
create table REG_JUR_ADJ_CALC_INPUT_ACCT
(
  reg_jur_template_id NUMBER(22) not null,
  adjustment_id       NUMBER(22) not null,
  reg_acct_id         NUMBER(22) not null,
  reg_alloc_acct_id   NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_JUR_ADJ_CALC_INPUT_ACCT
  add constraint PK_REG_JUR_ADJ_CALC_INPUT_ACCT primary key (REG_JUR_TEMPLATE_ID, ADJUSTMENT_ID, REG_ACCT_ID, REG_ALLOC_ACCT_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_ADJ_CALC_INPUT_ACCT
  add constraint R_REG_JUR_ADJ_CALC_INPUT_ACCT1 foreign key (ADJUSTMENT_ID, REG_JUR_TEMPLATE_ID)
  references REG_ADJUST_JUR_DEFAULT (ADJUSTMENT_ID, REG_JUR_TEMPLATE_ID);
alter table REG_JUR_ADJ_CALC_INPUT_ACCT
  add constraint R_REG_JUR_ADJ_CALC_INPUT_ACCT2 foreign key (REG_JUR_TEMPLATE_ID, REG_ACCT_ID)
  references REG_JUR_ACCT_DEFAULT (REG_JUR_TEMPLATE_ID, REG_ACCT_ID);
alter table REG_JUR_ADJ_CALC_INPUT_ACCT
  add constraint R_REG_JUR_ADJ_CALC_INPUT_ACCT3 foreign key (REG_JUR_TEMPLATE_ID, REG_ALLOC_ACCT_ID)
  references REG_JUR_ALLOC_ACCOUNT (REG_JUR_TEMPLATE_ID, REG_ALLOC_ACCT_ID);

prompt
prompt Creating table REG_JUR_ALLOC_FACTOR
prompt ===================================
prompt
create table REG_JUR_ALLOC_FACTOR
(
  reg_jur_template_id   NUMBER(22) not null,
  reg_allocator_id      NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  effective_date        NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  base_year_end         NUMBER(22),
  annual_reg_factor     NUMBER(22,16) not null,
  statistical_value     NUMBER(22,8),
  unit_of_measure       VARCHAR2(35),
  last_updated          DATE,
  last_updated_by       VARCHAR2(18),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
alter table REG_JUR_ALLOC_FACTOR
  add constraint PK_REG_JUR_ALLOC_FACTOR primary key (REG_JUR_TEMPLATE_ID, REG_ALLOCATOR_ID, EFFECTIVE_DATE, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_ALLOC_FACTOR
  add constraint R_REG_JUR_ALLOC_FACTOR1 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);
alter table REG_JUR_ALLOC_FACTOR
  add constraint R_REG_JUR_ALLOC_FACTOR2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_JUR_ALLOC_FACTOR
  add constraint R_REG_JUR_ALLOC_FACTOR3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);
alter table REG_JUR_ALLOC_FACTOR
  add constraint R_REG_JUR_ALLOC_FACTOR4 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);

prompt
prompt Creating table REG_JUR_ALLOC_STEPS
prompt ==================================
prompt
create table REG_JUR_ALLOC_STEPS
(
  reg_jur_template_id        NUMBER(22) not null,
  reg_alloc_category_id      NUMBER(22) not null,
  step_order                 NUMBER(22) not null,
  jur_result_flag            NUMBER(22),
  class_cost_of_service_flag NUMBER(22),
  recovery_class_flag        NUMBER(22),
  user_id                    VARCHAR2(18),
  time_stamp                 DATE
)
;
comment on table REG_JUR_ALLOC_STEPS
  is '(  )  [  ]
THE REG JUR ALLOC STEP TABLE SPECIFIES THE DEFAULTS CONTROLLING WHICH ALLOCATION CATEGORIES TO BE USED AND THEIR ORDER OF EXECUTION FOR A PARTICULAR JURISDICTIONAL TEMPLATE.';
comment on column REG_JUR_ALLOC_STEPS.reg_jur_template_id
  is 'SYSTEM ASSIGNED DEFAULT TEMPLATE FOR A JURISDICTION.  TEMPLATES ARE FOR A PARTICULAR JURISDICTION, BUT MULTIPLE TEMPLATES CAN BE DEFERRED FOR A JURISDICTION.';
comment on column REG_JUR_ALLOC_STEPS.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ALLOCATION LEVEL.';
comment on column REG_JUR_ALLOC_STEPS.step_order
  is 'INTEGER ORDER, E.G. ''1'' ALLOCATION WOULD PROCEED ''2''.  (DOES NOT NEED TO BE CONSECUTIVE).';
comment on column REG_JUR_ALLOC_STEPS.jur_result_flag
  is '0 = NO; 1 = YES, I.E. THIS STEP REPRESENTS JURISDICTIONAL RESULTS';
comment on column REG_JUR_ALLOC_STEPS.class_cost_of_service_flag
  is '0 = NO; 1 = YES, I.E. THIS STEP REPRESENTS CLASS COST OF SERVICE RESULTS';
comment on column REG_JUR_ALLOC_STEPS.recovery_class_flag
  is '0 = NO; 1 = YES, I.E. THIS STEP REPRESENTS RECOVERY CLASS RESULTS';
comment on column REG_JUR_ALLOC_STEPS.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_JUR_ALLOC_STEPS.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_JUR_ALLOC_STEPS
  add constraint PK_REG_JUR_ALLOC_STEPS primary key (REG_JUR_TEMPLATE_ID, REG_ALLOC_CATEGORY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_ALLOC_STEPS
  add constraint R_REG_JUR_ALLOC_STEPS1 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_JUR_ALLOC_STEPS
  add constraint R_REG_JUR_ALLOC_STEPS2 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);

prompt
prompt Creating table REG_JUR_ALLOC_TAX_CONTROL
prompt ========================================
prompt
create table REG_JUR_ALLOC_TAX_CONTROL
(
  reg_jur_template_id   NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  calc_tax_on_adjust    NUMBER(22) not null,
  tax_calc_id           NUMBER(22) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_JUR_ALLOC_TAX_CONTROL
  is '(  )  [  ]
THE REG JUR ALLOC TAX CONTROL TABLE CONTAINS THE DEFAULT RULES FOR DETERMINING INCOME TAX EXPENSE FOR EACH STEP (CATEGORY) OF THE ALLOCATION AND FOR EACH TARGET.';
comment on column REG_JUR_ALLOC_TAX_CONTROL.reg_jur_template_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A JURISDICTIONAL TEMPLATE';
comment on column REG_JUR_ALLOC_TAX_CONTROL.reg_alloc_category_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE ALLOCATION CATEGORY';
comment on column REG_JUR_ALLOC_TAX_CONTROL.reg_alloc_target_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE ALLOCATION TARGET';
comment on column REG_JUR_ALLOC_TAX_CONTROL.calc_tax_on_adjust
  is 'DETERMINES WHETHER TAXES ARE CALCULATED FOR ADJUSTMENTS FOR THIS CATEGORY AND TARGET (1 = YES, 2 = NO)';
comment on column REG_JUR_ALLOC_TAX_CONTROL.tax_calc_id
  is 'TYPE OF TAX CALCULATION TO PERFORM FOR THE ALLOCATION CATEGORY AND TARGET RESULTS:
1 = SKIP (NO CALCULATION IS PERFORMED FOR THIS CATEGORY, E.G. TOTAL COMPANY)
2 = CALCULATION BASED ON TAXABLE INCOME, PERM ITEMS, ETC.
3 = CALCULATION FOR ALLOCATION – DO THE TAX CALCULATION, BUT USE IT ONLY AS AN ALLOCATOR (THIS WOULD NEED TO BE ASSIGNED TO ALL TARGETS FOR THIS CATEGORY)
4 = ALLOCATION – USE AN INPUT OR DYNAMIC ALLOCATOR TO ALLOCATE TAXES FROM THE PREVIOUS STEP.  (AGAIN, THIS WOULD BE FOR ALL TARGETS ASSIGNED TO THE CATEGORY)
5 = “GROSS UP” – CALCULATE THE TAXES USING THE STATUTORY RATES TO GROSS UP TAXES, FOR EXAMPLE FOR A CLAUSE
6 = “NO TAX” – CALCULATE NO INCOME TAX FOR THIS TARGET.  THIS MAY BE APPROPRIATE WHERE THERE IS A CLAUSE OR RIDER FOR WHICH TAXES HAVE BEEN STATUTORIALLY IGNORED.
7 = “ABSORB” – ABSORBS THE TAXES FROM THE PREVIOUS CATEGORY, SUBTRACTING OUT THE TAXES ASSIGNED TO OTHER TARGETS FOR THIS CATEGORY.  THIS MAY BE APPROPRIATE WHERE SOME OF THE TARGETS USE ''GROSS UP'' AND/OR ''NO TAX'' AND WE WANT TO MAINTAIN THE TOTAL INCOME TAX.
THERE ARE OTHER SET UP RULES ENFORCED BY THE SYSTEM, E.G.
-          THERE CAN  BE NO “SKIP” FOR JUR, RECOVERY CLASS, OR CLASS COST OF SERVICE STEPS
-          CANNOT ASSIGN ''ALLOCATION'' OR ''CALCULATION FOR ALLOCATION'' WHEN THE PRIOR CATEGORY''S ASSIGNMENT WAS ''SKIP'' EXCEPT TOTAL COMPANY.
-          ''ABSORB'' CAN ONLY BE FOR 1 TARGET WHERE THE OTHER TARGETS ARE ''GROSS UP'', ''NO TAX'', OR ''CALCULATION''';
comment on column REG_JUR_ALLOC_TAX_CONTROL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID USED FOR AUDIT PURPOSES.';
comment on column REG_JUR_ALLOC_TAX_CONTROL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP USED FOR AUDIT PURPOSES.';
alter table REG_JUR_ALLOC_TAX_CONTROL
  add constraint PK_REG_JUR_ALLOC_TAX_CONTROL primary key (REG_JUR_TEMPLATE_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_ALLOC_TAX_CONTROL
  add constraint R_REG_JUR_ALLOC_TAX_CTRL1 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);
alter table REG_JUR_ALLOC_TAX_CONTROL
  add constraint R_REG_JUR_ALLOC_TAX_CTRL2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_JUR_ALLOC_TAX_CONTROL
  add constraint R_REG_JUR_ALLOC_TAX_CTRL3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_JUR_CATEGORY_ALLOCATOR
prompt =========================================
prompt
create table REG_JUR_CATEGORY_ALLOCATOR
(
  reg_jur_template_id   NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_allocator_id      NUMBER(22) not null,
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_JUR_CATEGORY_ALLOCATOR
  is '(  )  [  ]
THESE ARE THE ALLOCATORS AVAILABLE FOR THAT CATEGORY FOR THAT JURISDICTION.';
comment on column REG_JUR_CATEGORY_ALLOCATOR.reg_jur_template_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF A JURISDICTIONAL TEMPLATE';
comment on column REG_JUR_CATEGORY_ALLOCATOR.reg_alloc_category_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF AN ALLOCATION CATEGORY.';
comment on column REG_JUR_CATEGORY_ALLOCATOR.reg_allocator_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF A REGULATORY ALLOCATOR APPROPRIATE FOR THAT JURISDICTION AND LEVEL.';
comment on column REG_JUR_CATEGORY_ALLOCATOR.user_id
  is 'SYSTEM – ASSIGNED USER_ID USED FOR AUDIT PURPOSES.';
comment on column REG_JUR_CATEGORY_ALLOCATOR.time_stamp
  is 'SYSTEM – ASSIGNED TIME_STAMP USED FOR AUDIT PURPOSES.';
alter table REG_JUR_CATEGORY_ALLOCATOR
  add constraint PK_REG_JUR_CATEGORY_ALLOCATOR primary key (REG_JUR_TEMPLATE_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOCATOR_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_CATEGORY_ALLOCATOR
  add constraint R_REG_JUR_CATEGORY_ALLOCATOR1 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);
alter table REG_JUR_CATEGORY_ALLOCATOR
  add constraint R_REG_JUR_CATEGORY_ALLOCATOR2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_JUR_CATEGORY_ALLOCATOR
  add constraint R_REG_JUR_CATEGORY_ALLOCATOR3 foreign key (REG_ALLOCATOR_ID)
  references REG_ALLOCATOR (REG_ALLOCATOR_ID);

prompt
prompt Creating table REG_JUR_MONITOR_CONTROL
prompt ======================================
prompt
create table REG_JUR_MONITOR_CONTROL
(
  reg_jur_template_id   NUMBER(22) not null,
  reg_return_tag_id     NUMBER(22) not null,
  description           VARCHAR2(35) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  reg_adjustment_ids    VARCHAR2(100),
  subtotal_name         VARCHAR2(35),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
alter table REG_JUR_MONITOR_CONTROL
  add constraint PK_REG_JUR_MONITOR_CONTROL primary key (REG_JUR_TEMPLATE_ID, REG_RETURN_TAG_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_MONITOR_CONTROL
  add constraint R_REG_JUR_MONITOR_CONTROL1 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);
alter table REG_JUR_MONITOR_CONTROL
  add constraint R_REG_JUR_MONITOR_CONTROL2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_JUR_MONITOR_CONTROL
  add constraint R_REG_JUR_MONITOR_CONTROL3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_JUR_TARGET_FORWARD
prompt =====================================
prompt
create table REG_JUR_TARGET_FORWARD
(
  reg_jur_template_id   NUMBER(22) not null,
  reg_alloc_category_id NUMBER(22) not null,
  reg_alloc_target_id   NUMBER(22) not null,
  carry_forward_dollars NUMBER(22) not null,
  create_alloc_accts    NUMBER(22) not null,
  targets_major_minor   NUMBER(22),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_JUR_TARGET_FORWARD
  is '(  ) [  ]
THE REG JUR TARGET FORWARD TABLE DESCRIBES HOW ALLOCATION RESULTS (TARGETS) ARE TO BE USED IN THE NEXT LEVEL OF ALLOCATION FOR EACH JURISDICTION TEMPLATE.';
comment on column REG_JUR_TARGET_FORWARD.reg_jur_template_id
  is 'SYSTEM - ASSIGNED DEFAULT TEMPLATE FOR A JURISDICTION.  TEMPLATES ARE FOR A PARTICULAR JURISDICTION, BUT MULTIPLE TEMPLATES CAN BE DEFINED FOR JURISDICTION.';
comment on column REG_JUR_TARGET_FORWARD.reg_alloc_category_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF AN ALLOCATION LEVEL.';
comment on column REG_JUR_TARGET_FORWARD.reg_alloc_target_id
  is 'SYSTEM - ASSIGNED IDENTIFIER OF AN ALLOCATION TARGET.';
comment on column REG_JUR_TARGET_FORWARD.carry_forward_dollars
  is '1 = THE DOLLARS CARRY FORWARD
2 = THE DOLLARS DO NOT CARRY FORWARD E.G. IF IT WERE AN ELECTRIC CASE, AFTER THE SPLIT TO ELECTRIC AND GAS THE ELECTRIC WOULD CARRY FORWARD, BUT NOT THE GAS.';
comment on column REG_JUR_TARGET_FORWARD.create_alloc_accts
  is '1 = CREATE ALLOC ACCOUNTS, (SUBACCOUNTS,) USING THE TARGETS
2 = DON''T CREATE ALLOC ACCOUNTS';
comment on column REG_JUR_TARGET_FORWARD.targets_major_minor
  is 'IF SUBACCOUNTS ARE CREATED, ARE THEY:
1:  MAJOR – SORTED HIGH – E.G. THE REG ACCOUNTS ARE BELOW THE TARGET.
2:  MINOR – SORTED LOW – THE TARGETS ARE UNDER EACH REG ACCOUNT.
3:  THE TARGETS REPLACE THE REGULATORY ACCOUNTS, (E.G. THE REG ACCOUNTS ARE FULLY SUMMED UNDER EACH TARGET.)';
comment on column REG_JUR_TARGET_FORWARD.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_JUR_TARGET_FORWARD.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_JUR_TARGET_FORWARD
  add constraint PK_REG_JUR_TARGET_FORWARD primary key (REG_JUR_TEMPLATE_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_JUR_TARGET_FORWARD
  add constraint R_REG_JUR_TARGET_FORWARD1 foreign key (REG_JUR_TEMPLATE_ID)
  references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);
alter table REG_JUR_TARGET_FORWARD
  add constraint R_REG_JUR_TARGET_FORWARD2 foreign key (REG_ALLOC_CATEGORY_ID)
  references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);
alter table REG_JUR_TARGET_FORWARD
  add constraint R_REG_JUR_TARGET_FORWARD3 foreign key (REG_ALLOC_TARGET_ID)
  references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

prompt
prompt Creating table REG_MONITOR_JUR_CASE
prompt ===================================
prompt
create table REG_MONITOR_JUR_CASE
(
  reg_company_id            NUMBER(22) not null,
  reg_jurisdiction_id       NUMBER(22) not null,
  reg_monitoring_case_id    NUMBER(22) not null,
  reg_last_approved_case_id NUMBER(22) not null,
  user_id                   VARCHAR2(18),
  time_stamp                DATE
)
;
comment on table REG_MONITOR_JUR_CASE
  is '(  )  [  ]
THE REG MONITOR JUR CASE REFERENCES JURISDICTIONAL CASES TO BE USED IN COMPARISON TO THE OVERALL FINANCIAL MONITORING.';
comment on column REG_MONITOR_JUR_CASE.reg_company_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A REGULATORY COMPANY';
comment on column REG_MONITOR_JUR_CASE.reg_jurisdiction_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A REGULATORY JURISDICTION';
comment on column REG_MONITOR_JUR_CASE.reg_monitoring_case_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF A JURISDICTIONAL MONITORING CASE';
comment on column REG_MONITOR_JUR_CASE.reg_last_approved_case_id
  is 'SYSTEM – ASSIGNED IDENTIFIER OF THE LAST APPROVED JURISDICTIONAL CASE.';
comment on column REG_MONITOR_JUR_CASE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_MONITOR_JUR_CASE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_MONITOR_JUR_CASE
  add constraint PK_REG_MONITOR_JUR_CASE primary key (REG_COMPANY_ID, REG_JURISDICTION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_MONITOR_JUR_CASE
  add constraint R_REG_MONITOR_JUR_CASE1 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_MONITOR_JUR_CASE
  add constraint R_REG_MONITOR_JUR_CASE2 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);
alter table REG_MONITOR_JUR_CASE
  add constraint R_REG_MONITOR_JUR_CASE3 foreign key (REG_MONITORING_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_MONITOR_JUR_CASE
  add constraint R_REG_MONITOR_JUR_CASE4 foreign key (REG_LAST_APPROVED_CASE_ID)
  references REG_CASE (REG_CASE_ID);

prompt
prompt Creating table REG_QUERY_DRILLBACK
prompt ==================================
prompt
create table REG_QUERY_DRILLBACK
(
  reg_source_id  NUMBER(22) not null,
  hist_fcst_flag NUMBER(22) not null,
  reg_query_id   NUMBER(22) not null,
  description    VARCHAR2(35) not null,
  default_flag   NUMBER(1),
  user_id        VARCHAR2(18),
  time_stamp     DATE,
  table_name     VARCHAR2(35)
)
;
comment on table REG_QUERY_DRILLBACK
  is '(  )  [  ]
THE REG QUERY DRILLBACK TABLE CONTROLS WHICH QUERY ISSUED TO PROVIDE DETAILS FOR HISTORIC AND FORECAST LEDGER TRANSACTIONS.';
comment on column REG_QUERY_DRILLBACK.reg_source_id
  is 'SYSTEM ASSIGNED IDENTIFIERS OF POWERPLAN SOURCES (MODULES) TO REGULATORY MODULE';
comment on column REG_QUERY_DRILLBACK.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_QUERY_DRILLBACK.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_QUERY_DRILLBACK
  add constraint PK_REG_QUERY_DRILLBACK primary key (REG_SOURCE_ID, HIST_FCST_FLAG, REG_QUERY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_QUERY_DRILLBACK
  add constraint R_REG_QUERY_DRILLBACK1 foreign key (REG_SOURCE_ID)
  references REG_SOURCE (REG_SOURCE_ID);
alter table REG_QUERY_DRILLBACK
  add constraint R_REG_QUERY_DRILLBACK2 foreign key (REG_QUERY_ID)
  references CR_DD_SOURCES_CRITERIA (ID);

prompt
prompt Creating table REG_QUERY_LIST
prompt =============================
prompt
create table REG_QUERY_LIST
(
  id           NUMBER(22) not null,
  description  VARCHAR2(254),
  table_name   VARCHAR2(30),
  any_query_id NUMBER(22)
)
;
alter table REG_QUERY_LIST
  add constraint PK_REG_QUERY_LIST primary key (ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_RELATED_CASES
prompt ================================
prompt
create table REG_RELATED_CASES
(
  related_key NUMBER(22) not null,
  reg_case_id NUMBER(22) not null,
  user_id     VARCHAR2(18),
  time_stamp  DATE
)
;
comment on table REG_RELATED_CASES
  is '(  ) [  ]
THE REG RELATED CASES TABLE RELATES CASES FROM MULTIPLE JURISDICTIONS TOGETHER FOR THE SAME COMPANY TO GET TOTAL COMPANY RESULTS.  NOTE THAT RELATED CASES MUST COVER THE SAME TIME PERIOD (I.E. PERIOD START, PERIOD END) AND MUST BE FOR THE SAME COMPANY.';
comment on column REG_RELATED_CASES.related_key
  is 'SYSTEM ASSIGNED DUMB KEY FOR THIS TABLE.';
comment on column REG_RELATED_CASES.reg_case_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF ONE OF THE RELATED CASES.';
comment on column REG_RELATED_CASES.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_RELATED_CASES.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_RELATED_CASES
  add constraint PK_REG_RELATED_CASES primary key (RELATED_KEY, REG_CASE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_RELATED_CASES
  add constraint R_REG_RELATED_CASES1 foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);

prompt
prompt Creating table REG_TAX_COMPONENT
prompt ================================
prompt
create table REG_TAX_COMPONENT
(
  reg_component_id     NUMBER(22) not null,
  reg_family_id        NUMBER(22) not null,
  description          VARCHAR2(35) not null,
  long_description     VARCHAR2(254) not null,
  reg_acct_type_id     NUMBER(22),
  sub_acct_type_id     NUMBER(22),
  reg_annualization_id NUMBER(22),
  acct_good_for        NUMBER(22) not null,
  used_by_client       NUMBER(22) not null,
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_TAX_COMPONENT
  is '(  )  [  ]~r~nThe Reg Tax Component table defines the components (types of data) that can be loaded from PowerTax.  There is fixed list of components for use by the PowerTax integration.';
comment on column REG_TAX_COMPONENT.reg_component_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A FAMILY COMPONENT.';
comment on column REG_TAX_COMPONENT.reg_family_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A REG FAMILY.';
comment on column REG_TAX_COMPONENT.description
  is 'DESCRIPTION OF THE POWERTAX COMPONENT.';
comment on column REG_TAX_COMPONENT.long_description
  is 'LONG DESCRIPTION OF THE POWERTAX COMPONENT.';
comment on column REG_TAX_COMPONENT.reg_acct_type_id
  is '1 = RATE BASE~r~n2 = UTILITY ASSETS/LIABS NOT IN RATE BASE~r~n3 = OPERATING EXPENSE~r~n4 = OTHER INCOME (DEDUCTIONS)~r~n5 = OPERATING REVENUE~r~n6 = INTEREST EXPENSE~r~n7 = NON-UTILITY ASSETS (LIABS)~r~n8 = CAPITAL STRUCTURE~r~n9 = OTHER ADJUSTMENTS~r~n10 = INCOME TAX MEMO~r~n11 = CALCULATED ACCOUNTS';
comment on column REG_TAX_COMPONENT.sub_acct_type_id
  is 'E.G. FOR ACCOUNT TYPE CAPITAL: PREFERRED, COMMON OR DEBT; FOR RATE BASE: WORKING CAPITAL, PP&E, ETC.';
comment on column REG_TAX_COMPONENT.reg_annualization_id
  is 'ANNUALIZATION METHOD:~r~n1 = 13 MONTH SIMPLE AVERAGE~r~n2 = 13 MONTH AVERAGE OF AVERAGES~r~n3 = 12 MONTH AVERAGE~r~n4 = 2 POINT ANNUAL AVERAGE~r~n5 = ENDING BALANCE~r~n6 = SUM OF 12 MONTHS~r~n7 = ENDING MONTH x 12~r~n8  = INPUT';
comment on column REG_TAX_COMPONENT.acct_good_for
  is 'APPLICABILITY INDICATOR:~r~n1 = HISTORIC ACTUALS~r~n2 = OTHER HISTORIC~r~n3 = FORECAST~r~n4 = HISTORIC ACTUALS AND FORECAST';
comment on column REG_TAX_COMPONENT.used_by_client
  is '1 = COMPONENT IS ACTIVE AND WILL BE USED TO DEFINE REGULATORY ACCOUNTS; 0 = COMPONENT WILL NOT BE USED';
comment on column REG_TAX_COMPONENT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TAX_COMPONENT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_TAX_COMPONENT
  add constraint PK_REG_TAX_COMPONENT primary key (REG_COMPONENT_ID, REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TAX_COMPONENT
  add constraint R_REG_TAX_COMPONENT1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);
alter table REG_TAX_COMPONENT
  add constraint R_REG_TAX_COMPONENT2 foreign key (REG_ACCT_TYPE_ID)
  references REG_ACCT_TYPE (REG_ACCT_TYPE_ID);
alter table REG_TAX_COMPONENT
  add constraint R_REG_TAX_COMPONENT3 foreign key (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);
alter table REG_TAX_COMPONENT
  add constraint R_REG_TAX_COMPONENT4 foreign key (REG_ANNUALIZATION_ID)
  references REG_ANNUALIZATION_TYPE (REG_ANNUALIZATION_ID);

prompt
prompt Creating table REG_TAX_CONTROL
prompt ==============================
prompt
create table REG_TAX_CONTROL
(
  tax_control_id   NUMBER(22) not null,
  description      VARCHAR2(35) not null,
  long_description VARCHAR2(1000),
  user_id          VARCHAR2(18),
  time_stamp       DATE
)
;
comment on table REG_TAX_CONTROL
  is '(  )  [  ]
THE REG TAX CONTROL TABLE IS A FIXED TABLE CONTROLLING THE PROCESSING OF THE REG ACCOUNTS WITH A REG ACCT TYPE OF TAX MEMO.  IT IS REFERENCED FOR THESE ITEMS WHEN APPLIED DOLLARS TO THE REG ACCOUNTS FROM THE REG SUB ACCT TYPES.';
comment on column REG_TAX_CONTROL.tax_control_id
  is 'SYSTEM-ASSIGNED IDENTIFIER OF A TAX CONTROL IDENTIFIER';
comment on column REG_TAX_CONTROL.description
  is 'CAPTION ON THE MONITORING REPORTS. (EACH ITEM IS PROCESSED DIFFERENTLY IN THE TAX COMPUTATION)  NOTE THE ASSUMED SIGNS.
1)       PERM M-ITEM ACTIVITY (''+'' INCREASES TAXABLE INCOME)
2)       NON-STATUTORY RATE ACTIVITY (I.E. THE DELTA IN INCREMENTAL 109 ACCUMULATED DEFERREDS BEFORE GROSS UP) (AN INCREASE IN THE CREDIT BALANCE IS A ''-'')
3)       STATE CREDITS (CURRENT) (''-'' IS A CREDIT)
4)       FEDERAL CREDITS (CURRENT) (''-'' IS A CREDIT)
5)       ITC AMORTIZATION (NORMALLY A ''-'')
6)       TAX EXPENSE ON INCOME STATEMENT (TO DERIVE PRE TAX INCOME).
7)       CALCULATED TAX EXPENSE FOR RARE BASE/INTEREST SYNC
8)       CALCULATED TAX EXPENSE FOR INCOME ACTIVITY
9)       BASIS REDUCTION ASSOCIATED WITH STATE CREDITS ABOVE
10)   BASIS REDUCTION ASSOCIATED WITH FEDERAL CREDITS ABOVE';
comment on column REG_TAX_CONTROL.long_description
  is 'ALTERNATIVE LONG DESCRIPTION';
comment on column REG_TAX_CONTROL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES';
comment on column REG_TAX_CONTROL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES';
alter table REG_TAX_CONTROL
  add constraint PK_REG_TAX_CONTROL primary key (TAX_CONTROL_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;

prompt
prompt Creating table REG_TAX_FAMILY_MEMBER
prompt ====================================
prompt
create table REG_TAX_FAMILY_MEMBER
(
  reg_member_id NUMBER(22) not null,
  reg_family_id NUMBER(22) not null,
  description   VARCHAR2(35) not null,
  user_id       VARCHAR2(18),
  time_stamp    DATE
)
;
comment on table REG_TAX_FAMILY_MEMBER
  is '(  )  [  ]~r~nThe Reg Tax Family Member table defines the different detail items that can be loaded from PowerTax.  These are client defined usually based on mappings from tax rollups, tax classes and normalization schemas.';
comment on column REG_TAX_FAMILY_MEMBER.reg_member_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY MEMBER.';
comment on column REG_TAX_FAMILY_MEMBER.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG FAMILY.';
comment on column REG_TAX_FAMILY_MEMBER.description
  is 'DESCRIPTION OF THE POWERTAX FAMILY MEMBER.';
comment on column REG_TAX_FAMILY_MEMBER.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TAX_FAMILY_MEMBER.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_TAX_FAMILY_MEMBER
  add constraint PK_REG_TAX_FAMILY_MEMBER primary key (REG_MEMBER_ID, REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TAX_FAMILY_MEMBER
  add constraint R_REG_TAX_FAMILY_MEMBER1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);

prompt
prompt Creating table REG_TAX_FAMILY_MEMBER_MAP
prompt ========================================
prompt
create table REG_TAX_FAMILY_MEMBER_MAP
(
  reg_tax_member_map_id NUMBER(22) not null,
  historic_version_id   NUMBER(22) not null,
  company_id            NUMBER(22) not null,
  tax_rollup_detail_id  NUMBER(22) not null,
  tax_class_id          NUMBER(22),
  normalization_id      NUMBER(22) not null,
  reg_member_id         NUMBER(22),
  reg_family_id         NUMBER(22),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_TAX_FAMILY_MEMBER_MAP
  is '(  )  [  ]~r~nThe Reg Tax Family Member Map table stores the mapping from PowerTax attributes to Reg Tax Family Members.';
comment on column REG_TAX_FAMILY_MEMBER_MAP.reg_tax_member_map_id
  is 'System-assigned identifier of tax member mapping';
comment on column REG_TAX_FAMILY_MEMBER_MAP.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A HISTORIC VERSION';
comment on column REG_TAX_FAMILY_MEMBER_MAP.company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A COMPANY';
comment on column REG_TAX_FAMILY_MEMBER_MAP.tax_rollup_detail_id
  is 'System-assigned identifier of a particular rollup.';
comment on column REG_TAX_FAMILY_MEMBER_MAP.tax_class_id
  is 'System-assigned identifier for each individual tax class defined by the user.';
comment on column REG_TAX_FAMILY_MEMBER_MAP.normalization_id
  is 'System-assigned identifier of a normalization schema.';
comment on column REG_TAX_FAMILY_MEMBER_MAP.reg_member_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY MEMBER.';
comment on column REG_TAX_FAMILY_MEMBER_MAP.reg_family_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REG FAMILY.';
comment on column REG_TAX_FAMILY_MEMBER_MAP.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TAX_FAMILY_MEMBER_MAP.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_TAX_FAMILY_MEMBER_MAP
  add constraint PK_REG_TAX_FAMILY_MEMBER_MAP primary key (REG_TAX_MEMBER_MAP_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TAX_FAMILY_MEMBER_MAP
  add constraint R_REG_TAX_FAMILY_MEMBER_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TAX_FAMILY_MEMBER_MAP
  add constraint R_REG_TAX_FAMILY_MEMBER_MAP2 foreign key (TAX_ROLLUP_DETAIL_ID)
  references TAX_ROLLUP_DETAIL (TAX_ROLLUP_DETAIL_ID);
alter table REG_TAX_FAMILY_MEMBER_MAP
  add constraint R_REG_TAX_FAMILY_MEMBER_MAP3 foreign key (TAX_CLASS_ID)
  references TAX_CLASS (TAX_CLASS_ID);
alter table REG_TAX_FAMILY_MEMBER_MAP
  add constraint R_REG_TAX_FAMILY_MEMBER_MAP4 foreign key (NORMALIZATION_ID)
  references NORMALIZATION_SCHEMA (NORMALIZATION_ID);
alter table REG_TAX_FAMILY_MEMBER_MAP
  add constraint R_REG_TAX_FAMILY_MEMBER_MAP5 foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
  references REG_TAX_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);

prompt
prompt Creating table REG_TAX_LOAD_HISTORY
prompt ===================================
prompt
create table REG_TAX_LOAD_HISTORY
(
  version_id                  NUMBER(22) not null,
  tax_year                    NUMBER(22,2) not null,
  reg_company_id              NUMBER(22) not null,
  historic_version_id         NUMBER(22) not null,
  forecast_version_id         NUMBER(22) not null,
  load_ledger                 NUMBER(22),
  load_ledger_start_month     NUMBER(22),
  load_ledger_end_month       NUMBER(22),
  load_ledger_status          NUMBER(22),
  split_provision             NUMBER(22),
  split_provision_start_month NUMBER(22),
  split_provision_end_month   NUMBER(22),
  split_provision_status      NUMBER(22),
  user_id                     VARCHAR2(18),
  time_stamp                  DATE,
  factor_id                   NUMBER(22)
)
;
comment on table REG_TAX_LOAD_HISTORY
  is '(  )  [  ]~r~nThe Reg Tax Load History table records how specific PowerTax data is being used in the regulatory system.  This includes key attributes of where in reg ledger or split provision and other audit trail fields such as when the load was done.  Note that the same PowerTax data can be used in multiple ways.';
comment on column REG_TAX_LOAD_HISTORY.version_id
  is 'System-assigned identifier of a PowerTax version.';
comment on column REG_TAX_LOAD_HISTORY.tax_year
  is 'Tax filing year, e.g., "1996". If there is more than one tax year in a year due to short tax years, they should be sequentially identified with a decimal, e.g. 1999.1, 1999.2, where 1999.1 precedes 1999.2.';
comment on column REG_TAX_LOAD_HISTORY.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY COMPANY.';
comment on column REG_TAX_LOAD_HISTORY.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A HISTORIC LEDGER (VERSION).';
comment on column REG_TAX_LOAD_HISTORY.forecast_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FORECAST LEDGER (VERSION).';
comment on column REG_TAX_LOAD_HISTORY.load_ledger
  is 'Indicator if the amounts for the tax version and year will be loaded directly to reg history ledger. 1 = Yes; 0 = No. Default is No.';
comment on column REG_TAX_LOAD_HISTORY.load_ledger_start_month
  is 'The month number value of first GL Month field loaded to reg history ledger.';
comment on column REG_TAX_LOAD_HISTORY.load_ledger_end_month
  is 'The month number value of last GL Month field loaded to reg history ledger.';
comment on column REG_TAX_LOAD_HISTORY.load_ledger_status
  is 'Status of loading Reg Ledger. 0 = not loaded; 1 = loaded; 2 = pending.';
comment on column REG_TAX_LOAD_HISTORY.split_provision
  is 'Indicator if the amounts for the tax version and year will be used to split provision regulatory accounts. 1 = Yes; 0 = No. Default is No.';
comment on column REG_TAX_LOAD_HISTORY.split_provision_start_month
  is 'The month number value of first effective month of the split provision rules.';
comment on column REG_TAX_LOAD_HISTORY.split_provision_end_month
  is 'The month number value of first effective month of the split provision rules.';
comment on column REG_TAX_LOAD_HISTORY.split_provision_status
  is 'Status of loading Split Provision Rules. 0 = not loaded; 1 = loaded; 2 = pending';
comment on column REG_TAX_LOAD_HISTORY.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TAX_LOAD_HISTORY.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_TAX_LOAD_HISTORY.factor_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A SPREAD FACTOR.';
alter table REG_TAX_LOAD_HISTORY
  add constraint PK_REG_TAX_LOAD_HISTORY primary key (VERSION_ID, TAX_YEAR, REG_COMPANY_ID, HISTORIC_VERSION_ID, FORECAST_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TAX_LOAD_HISTORY
  add constraint R_REG_TAX_LOAD_HISTORY1 foreign key (VERSION_ID)
  references VERSION (VERSION_ID);
alter table REG_TAX_LOAD_HISTORY
  add constraint R_REG_TAX_LOAD_HISTORY2 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_TAX_LOAD_HISTORY
  add constraint R_REG_TAX_LOAD_HISTORY3 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TAX_LOAD_HISTORY
  add constraint R_REG_TAX_LOAD_HISTORY4 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);
alter table REG_TAX_LOAD_HISTORY
  add constraint R_REG_TAX_LOAD_HISTORY5 foreign key (FACTOR_ID)
  references SPREAD_FACTOR (FACTOR_ID);

prompt
prompt Creating table REG_TAX_RATE
prompt ===========================
prompt
create table REG_TAX_RATE
(
  reg_company_id      NUMBER(22) not null,
  reg_jurisdiction_id NUMBER(22) not null,
  effective_date      DATE not null,
  statutory_tax_rate  NUMBER(22,8) not null,
  stat_fed_rate       NUMBER(22,8) not null,
  stat_state_rate     NUMBER(22,8) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_TAX_RATE
  is '(  )  [  ]
THE REG TAX RATE TABLE CONTAINS THE INPUT STATUTORY TAX RATES USED BY EACH COMPANY AND JURISDICTION.';
comment on column REG_TAX_RATE.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A REGULATORY COMPANY.';
comment on column REG_TAX_RATE.reg_jurisdiction_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE JURISDICTION.';
comment on column REG_TAX_RATE.effective_date
  is 'THE STARTING EFFECTIVE DATE OF THE INPUT TAX RATE.  THE RATE WILL BE IN EFFECT UNTIL THE NEXT EFFECTIVE DATE FOR THE COMPANY AND JURISDICTION.';
comment on column REG_TAX_RATE.statutory_tax_rate
  is 'THE STATUTORY COMBINED FEDERAL AND STATE TAX RATE USED FOR TAX CALCULATIONS, EXPRESSED AS A DECIMAL.';
comment on column REG_TAX_RATE.stat_fed_rate
  is 'THE STATUTORY TAX RATE USED FOR FEDERAL';
comment on column REG_TAX_RATE.stat_state_rate
  is 'THE STATUTORY TAX RATE USED FOR STATE';
comment on column REG_TAX_RATE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER_ID FOR AUDIT PURPOSES.';
comment on column REG_TAX_RATE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIME_STAMP FOR AUDIT PURPOSES.';
alter table REG_TAX_RATE
  add constraint PK_REG_TAX_RATE primary key (REG_COMPANY_ID, REG_JURISDICTION_ID, EFFECTIVE_DATE)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TAX_RATE
  add constraint R_REG_TAX_RATE1 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_TAX_RATE
  add constraint R_REG_TAX_RATE2 foreign key (REG_JURISDICTION_ID)
  references REG_JURISDICTION (REG_JURISDICTION_ID);

prompt
prompt Creating table REG_TAX_STAGE
prompt ============================
prompt
create table REG_TAX_STAGE
(
  reg_tax_stage_id      NUMBER(22) not null,
  version_id            NUMBER(22) not null,
  tax_year              NUMBER(22,2) not null,
  company_id            NUMBER(22) not null,
  tax_rollup_detail_id  NUMBER(22) not null,
  tax_class_id          NUMBER(22),
  normalization_id      NUMBER(22) not null,
  reg_company_id        NUMBER(22) not null,
  historic_version_id   NUMBER(22) not null,
  reg_acct_id           NUMBER(22) not null,
  act_amount            NUMBER(22,2) not null,
  load_ledger           NUMBER(22),
  load_ledger_month     NUMBER(22),
  split_provision       NUMBER(22),
  split_provision_month NUMBER(22),
  user_id               VARCHAR2(18),
  time_stamp            DATE
)
;
comment on table REG_TAX_STAGE
  is '(  )  [  ]~r~nThe Reg Tax Stage table is used by the PowerTax-to-Regulatory interface logic.  It is single point for translating PowerTax attributes to Regulatory Account that can then be loaded to various locations within the Regulatory system.  ';
comment on column REG_TAX_STAGE.reg_tax_stage_id
  is 'System-assigned identifier of a Regulatory PowerTax Stage record.';
comment on column REG_TAX_STAGE.version_id
  is 'System-assigned identifier of a PowerTax version.';
comment on column REG_TAX_STAGE.tax_year
  is 'Tax filing year, e.g., "1996". If there is more than one tax year in a year due to short tax years, they should be sequentially identified with a decimal, e.g. 1999.1, 1999.2, where 1999.1 precedes 1999.2.';
comment on column REG_TAX_STAGE.company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A COMPANY';
comment on column REG_TAX_STAGE.tax_rollup_detail_id
  is 'System-assigned identifier of a particular rollup.';
comment on column REG_TAX_STAGE.tax_class_id
  is 'System-assigned identifier for each individual tax class defined by the user.';
comment on column REG_TAX_STAGE.normalization_id
  is 'System-assigned identifier of a normalization schema.';
comment on column REG_TAX_STAGE.reg_company_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY COMPANY.';
comment on column REG_TAX_STAGE.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE HISTORIC "VERSION" OR LEDGER.';
comment on column REG_TAX_STAGE.reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE REGULATORY ACCOUNT.';
comment on column REG_TAX_STAGE.act_amount
  is 'THE ACTUAL MONTH END ACCOUNT BALANCE OR MONTHLY AMOUNT.';
comment on column REG_TAX_STAGE.load_ledger
  is 'Indicate if the record was loaded directly to reg history ledger.';
comment on column REG_TAX_STAGE.load_ledger_month
  is 'The month number value used for GL Month field when loading reg history ledger.';
comment on column REG_TAX_STAGE.split_provision
  is 'Indicate if the record was loaded to reg TA split rules table.';
comment on column REG_TAX_STAGE.split_provision_month
  is 'The month number value to use for recorded effective date of the split factors. The powertax amounts will be used to split provision amounts loaded for months greater than or equal to the effective date until another effective date is encountered.';
comment on column REG_TAX_STAGE.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TAX_STAGE.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_TAX_STAGE
  add constraint PK_REG_TAX_STAGE primary key (REG_TAX_STAGE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TAX_STAGE
  add constraint R_REG_TAX_STAGE1 foreign key (VERSION_ID)
  references VERSION (VERSION_ID);
alter table REG_TAX_STAGE
  add constraint R_REG_TAX_STAGE2 foreign key (TAX_ROLLUP_DETAIL_ID)
  references TAX_ROLLUP_DETAIL (TAX_ROLLUP_DETAIL_ID);
alter table REG_TAX_STAGE
  add constraint R_REG_TAX_STAGE3 foreign key (TAX_CLASS_ID)
  references TAX_CLASS (TAX_CLASS_ID);
alter table REG_TAX_STAGE
  add constraint R_REG_TAX_STAGE4 foreign key (NORMALIZATION_ID)
  references NORMALIZATION_SCHEMA (NORMALIZATION_ID);
alter table REG_TAX_STAGE
  add constraint R_REG_TAX_STAGE5 foreign key (REG_COMPANY_ID)
  references REG_COMPANY (REG_COMPANY_ID);
alter table REG_TAX_STAGE
  add constraint R_REG_TAX_STAGE6 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TAX_STAGE
  add constraint R_REG_TAX_STAGE7 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);

prompt
prompt Creating table REG_TAX_VERSION_CONTROL
prompt ======================================
prompt
create table REG_TAX_VERSION_CONTROL
(
  historic_version_id NUMBER(22) not null,
  forecast_version_id NUMBER(22) not null,
  version_id          NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
comment on table REG_TAX_VERSION_CONTROL
  is '(  )  [  ]~r~nThe Reg Tax Version Control table defines which tax versions (cases) use the mapping for the specified regulatory ledger.';
comment on column REG_TAX_VERSION_CONTROL.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A HISTORIC VERSION';
comment on column REG_TAX_VERSION_CONTROL.forecast_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A FORECAST VERSION';
comment on column REG_TAX_VERSION_CONTROL.version_id
  is 'System-assigned identifier of a PowerTax version.';
comment on column REG_TAX_VERSION_CONTROL.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TAX_VERSION_CONTROL.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_TAX_VERSION_CONTROL
  add constraint PK_REG_TAX_VERSION_CONTROL primary key (HISTORIC_VERSION_ID, FORECAST_VERSION_ID, VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TAX_VERSION_CONTROL
  add constraint R_REG_TAX_VERSION_CONTROL1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TAX_VERSION_CONTROL
  add constraint R_REG_TAX_VERSION_CONTROL2 foreign key (FORECAST_VERSION_ID)
  references REG_FORECAST_VERSION (FORECAST_VERSION_ID);
alter table REG_TAX_VERSION_CONTROL
  add constraint R_REG_TAX_VERSION_CONTROL3 foreign key (VERSION_ID)
  references VERSION (VERSION_ID);

prompt
prompt Creating table REG_TA_COMPONENT
prompt ===============================
prompt
create table REG_TA_COMPONENT
(
  reg_component_id     NUMBER(22) not null,
  reg_family_id        NUMBER(22) not null,
  description          VARCHAR2(35),
  reg_acct_type_id     NUMBER(22),
  sub_acct_type_id     NUMBER(22),
  reg_annualization_id NUMBER(22),
  acct_good_for        NUMBER(22),
  long_description     VARCHAR2(254),
  used_by_client       NUMBER(1),
  user_id              VARCHAR2(18),
  time_stamp           DATE,
  federal              NUMBER(1),
  load_offsets         NUMBER(22)
)
;
alter table REG_TA_COMPONENT
  add constraint PK_REG_TA_COMPONENT primary key (REG_COMPONENT_ID, REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_COMPONENT
  add constraint R_REG_TA_COMPONENT1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);
alter table REG_TA_COMPONENT
  add constraint R_REG_TA_COMPONENT2 foreign key (REG_ACCT_TYPE_ID)
  references REG_ACCT_TYPE (REG_ACCT_TYPE_ID);
alter table REG_TA_COMPONENT
  add constraint R_REG_TA_COMPONENT3 foreign key (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);
alter table REG_TA_COMPONENT
  add constraint R_REG_TA_COMPONENT4 foreign key (REG_ANNUALIZATION_ID)
  references REG_ANNUALIZATION_TYPE (REG_ANNUALIZATION_ID);

prompt
prompt Creating table REG_TA_FAMILY_MEMBER
prompt ===================================
prompt
create table REG_TA_FAMILY_MEMBER
(
  reg_member_id NUMBER(22) not null,
  reg_family_id NUMBER(22) not null,
  description   VARCHAR2(35) not null,
  user_id       VARCHAR2(18),
  time_stamp    DATE
)
;
alter table REG_TA_FAMILY_MEMBER
  add constraint PK_REG_TA_FAMILY_MEMBER primary key (REG_MEMBER_ID, REG_FAMILY_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_FAMILY_MEMBER
  add constraint R_REG_FAMILY_MEMBER1 foreign key (REG_FAMILY_ID)
  references REG_FAMILY (REG_FAMILY_ID);

prompt
prompt Creating table REG_TA_COMP_MEMBER_MAP
prompt =====================================
prompt
create table REG_TA_COMP_MEMBER_MAP
(
  historic_version_id NUMBER(22) not null,
  reg_component_id    NUMBER(22) not null,
  reg_family_id       NUMBER(22) not null,
  reg_member_id       NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_TA_COMP_MEMBER_MAP
  add constraint PK_REG_TA_COMP_MEMBER_MAP primary key (HISTORIC_VERSION_ID, REG_COMPONENT_ID, REG_FAMILY_ID, REG_MEMBER_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_COMP_MEMBER_MAP
  add constraint R_REG_TA_COMP_MEMBER_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TA_COMP_MEMBER_MAP
  add constraint R_REG_TA_COMP_MEMBER_MAP2 foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
  references REG_TA_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);
alter table REG_TA_COMP_MEMBER_MAP
  add constraint R_REG_TA_COMP_MEMBER_MAP3 foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
  references REG_TA_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);

prompt
prompt Creating table REG_TA_CONTROL_MAP
prompt =================================
prompt
create table REG_TA_CONTROL_MAP
(
  reg_ta_control_id   NUMBER(22) not null,
  historic_version_id NUMBER(22) not null,
  company_id          NUMBER(22) not null,
  oper_ind            NUMBER(22) not null,
  m_item_id           NUMBER(22) not null,
  reg_acct_id         NUMBER(22),
  user_id             VARCHAR2(18),
  time_stamp          DATE,
  tax_control_id      NUMBER(22)
)
;
alter table REG_TA_CONTROL_MAP
  add constraint PK_REG_TA_CONTROL_MAP primary key (REG_TA_CONTROL_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_CONTROL_MAP
  add constraint R_REG_TA_CONTROL_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TA_CONTROL_MAP
  add constraint R_REG_TA_CONTROL_MAP2 foreign key (REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_TA_CONTROL_MAP
  add constraint R_REG_TA_CONTROL_MAP3 foreign key (COMPANY_ID)
  references COMPANY_SETUP (COMPANY_ID);
alter table REG_TA_CONTROL_MAP
  add constraint R_REG_TA_CONTROL_MAP4 foreign key (OPER_IND)
  references TAX_ACCRUAL_OPER_IND (OPER_IND);
alter table REG_TA_CONTROL_MAP
  add constraint R_REG_TA_CONTROL_MAP5 foreign key (M_ITEM_ID)
  references TAX_ACCRUAL_M_MASTER (M_ITEM_ID);

prompt
prompt Creating table REG_TA_FAMILY_MEMBER_MAP
prompt =======================================
prompt
create table REG_TA_FAMILY_MEMBER_MAP
(
  reg_ta_member_map_id NUMBER(22) not null,
  historic_version_id  NUMBER(22) not null,
  company_id           NUMBER(22) not null,
  oper_ind             NUMBER(22) not null,
  m_item_id            NUMBER(22) not null,
  reg_member_id        NUMBER(22) not null,
  reg_family_id        NUMBER(22) not null,
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
alter table REG_TA_FAMILY_MEMBER_MAP
  add constraint PK_REG_TA_FAMILY_MEMBER_MAP primary key (REG_TA_MEMBER_MAP_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_FAMILY_MEMBER_MAP
  add constraint R_REG_TA_FAMILY_MEMBER_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TA_FAMILY_MEMBER_MAP
  add constraint R_REG_TA_FAMILY_MEMBER_MAP2 foreign key (COMPANY_ID)
  references COMPANY_SETUP (COMPANY_ID);
alter table REG_TA_FAMILY_MEMBER_MAP
  add constraint R_REG_TA_FAMILY_MEMBER_MAP3 foreign key (OPER_IND)
  references TAX_ACCRUAL_OPER_IND (OPER_IND);
alter table REG_TA_FAMILY_MEMBER_MAP
  add constraint R_REG_TA_FAMILY_MEMBER_MAP4 foreign key (M_ITEM_ID)
  references TAX_ACCRUAL_M_MASTER (M_ITEM_ID);
alter table REG_TA_FAMILY_MEMBER_MAP
  add constraint R_REG_TA_FAMILY_MEMBER_MAP5 foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
  references REG_TA_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);

prompt
prompt Creating table REG_TA_M_TYPE_ENTITY_MAP
prompt =======================================
prompt
create table REG_TA_M_TYPE_ENTITY_MAP
(
  historic_version_id NUMBER(22) not null,
  m_type_id           NUMBER(22) not null,
  entity_id           NUMBER(22) not null,
  tax_control_id      NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_TA_M_TYPE_ENTITY_MAP
  add constraint PK_REG_TA_M_TYPE_ENTITY_MAP primary key (HISTORIC_VERSION_ID, M_TYPE_ID, ENTITY_ID, TAX_CONTROL_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_M_TYPE_ENTITY_MAP
  add constraint R_REG_TA_M_TYPE_ENTITY_MAP1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TA_M_TYPE_ENTITY_MAP
  add constraint R_REG_TA_M_TYPE_ENTITY_MAP2 foreign key (TAX_CONTROL_ID)
  references REG_TAX_CONTROL (TAX_CONTROL_ID);
alter table REG_TA_M_TYPE_ENTITY_MAP
  add constraint R_REG_TA_M_TYPE_ENTITY_MAP3 foreign key (M_TYPE_ID)
  references TAX_ACCRUAL_M_TYPE (M_TYPE_ID);
alter table REG_TA_M_TYPE_ENTITY_MAP
  add constraint R_REG_TA_M_TYPE_ENTITY_MAP4 foreign key (ENTITY_ID)
  references TAX_ACCRUAL_ENTITY (ENTITY_ID);

prompt
prompt Creating table REG_TA_SPLIT_RULES
prompt =================================
prompt
create table REG_TA_SPLIT_RULES
(
  reg_ta_split_rule_id NUMBER(22) not null,
  prov_reg_acct_id     NUMBER(22) not null,
  tax_reg_acct_id      NUMBER(22) not null,
  historic_version_id  NUMBER(22) not null,
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_TA_SPLIT_RULES
  is '(  )  [  ]~r~nThe Reg TA Split Rules table is used to split a single provision based reg account to multiple powertax based reg accounts.  Provision usually just has high level "plant related" m-items but details like functional class are useful for cost of service analysis but only known in PowerTax.';
comment on column REG_TA_SPLIT_RULES.reg_ta_split_rule_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A TAX PROVISION SPLIT RULE.';
comment on column REG_TA_SPLIT_RULES.prov_reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE PROVISION REGULATORY ACCOUNT.';
comment on column REG_TA_SPLIT_RULES.tax_reg_acct_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE POWERTAX REGULATORY ACCOUNT.';
comment on column REG_TA_SPLIT_RULES.historic_version_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF THE HISTORIC "VERSION" OR LEDGER.';
comment on column REG_TA_SPLIT_RULES.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TA_SPLIT_RULES.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_TA_SPLIT_RULES
  add constraint PK_REG_TA_SPLIT_RULES primary key (REG_TA_SPLIT_RULE_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_SPLIT_RULES
  add constraint R_REG_TA_SPLIT_RULES1 foreign key (PROV_REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_TA_SPLIT_RULES
  add constraint R_REG_TA_SPLIT_RULES2 foreign key (TAX_REG_ACCT_ID)
  references REG_ACCT_MASTER (REG_ACCT_ID);
alter table REG_TA_SPLIT_RULES
  add constraint R_REG_TA_SPLIT_RULES3 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

prompt
prompt Creating table REG_TA_SPLIT_RULE_AMOUNT
prompt =======================================
prompt
create table REG_TA_SPLIT_RULE_AMOUNT
(
  reg_ta_split_rule_id NUMBER(22) not null,
  month                NUMBER(6) not null,
  amount               NUMBER(22,2) not null,
  user_id              VARCHAR2(18),
  time_stamp           DATE
)
;
comment on table REG_TA_SPLIT_RULE_AMOUNT
  is '(  )  [  ]~r~nThe Reg TA Split Rule Amount table is used to store the amounts used split a single provision based reg account to multiple powertax based reg accounts per effective month.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.reg_ta_split_rule_id
  is 'SYSTEM ASSIGNED IDENTIFIER OF A TAX PROVISION SPLIT RULE.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.month
  is 'Effective Month (YYYYMM) that the PowerTax amounts should be used when splitting provision reg accounts.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.amount
  is 'THE POWERTAX AMOUNT LOADED PER REG ACCOUNT TO USE AS PRO-RATA FACTOR IN SPLITTING PROVISION AMOUNT.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.user_id
  is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TA_SPLIT_RULE_AMOUNT.time_stamp
  is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
alter table REG_TA_SPLIT_RULE_AMOUNT
  add constraint PK_REG_TA_SPLIT_RULE_AMOUNT primary key (REG_TA_SPLIT_RULE_ID, MONTH)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_SPLIT_RULE_AMOUNT
  add constraint R_REG_TA_SPLIT_RULE_AMOUNT1 foreign key (REG_TA_SPLIT_RULE_ID)
  references REG_TA_SPLIT_RULES (REG_TA_SPLIT_RULE_ID);

prompt
prompt Creating table REG_TA_VERSION_CONTROL
prompt =====================================
prompt
create table REG_TA_VERSION_CONTROL
(
  historic_version_id NUMBER(22) not null,
  ta_version_id       NUMBER(22) not null,
  user_id             VARCHAR2(18),
  time_stamp          DATE
)
;
alter table REG_TA_VERSION_CONTROL
  add constraint PK_REG_TA_VERSION_CONTROL primary key (HISTORIC_VERSION_ID, TA_VERSION_ID)
  USING INDEX
    TABLESPACE pwrplant_idx;
alter table REG_TA_VERSION_CONTROL
  add constraint R_REG_TA_VERSION_CONTROL1 foreign key (HISTORIC_VERSION_ID)
  references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);
alter table REG_TA_VERSION_CONTROL
  add constraint R_REG_TA_VERSION_CONTROL2 foreign key (TA_VERSION_ID)
  references TAX_ACCRUAL_VERSION (TA_VERSION_ID);

-- Temp table for loading PowerTax data
create global temporary table REG_HIST_TAX_LOAD_TEMP
(
 TAX_LOAD_TEMP_ID     number(22,0) not null,
 VERSION_ID           number(22,0) not null,
 TAX_YEAR             number(22,2) not null,
 COMPANY_ID           number(22,0) not null,
 TAX_ROLLUP_DETAIL_ID number(22,0) not null,
 TAX_CLASS_ID         number(22,0),
 NORMALIZATION_ID     number(22,0) not null,
 REG_COMPANY_ID       number(22,0) not null,
 HISTORIC_VERSION_ID  number(22,0) not null,
 REG_ACCT_ID          number(22,0) not null,
 ACT_AMOUNT           number(22,2) not null
) on commit preserve rows;

alter table REG_HIST_TAX_LOAD_TEMP
   add constraint PK_REG_HIST_TAX_LOAD_TEMP
       primary key (TAX_LOAD_TEMP_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (744, 0, 10, 4, 2, 0, 33713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033713_reg_01_all_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON