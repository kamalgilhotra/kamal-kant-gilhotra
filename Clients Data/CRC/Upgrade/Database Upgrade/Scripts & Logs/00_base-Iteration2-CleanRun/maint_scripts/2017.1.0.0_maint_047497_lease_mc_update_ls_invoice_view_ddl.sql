/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047497_lease_mc_update_ls_invoice_view_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 06/12/2017 Anand R        Update v_ls_invoice_fx_vw to add ilr_id
||============================================================================
*/

CREATE OR REPLACE VIEW v_ls_invoice_fx_vw
AS
WITH
cur AS (
  select /*+ materialize */ ls_cur_type, currency_id, currency_display_symbol, iso_code, contract_approval_rate
  from (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
      contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 contract_approval_rate
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id,
      company_cur.currency_display_symbol, company_cur.iso_code, NULL
    FROM currency company_cur
  )
),
open_month AS (
  SELECT /*+ materialize */ company_id, Min(gl_posting_mo_yr) open_month
  FROM ls_process_control
  WHERE open_next IS NULL
  GROUP BY company_id
),
cr AS (
  SELECT /*+ materialize */ exchange_date, currency_from, currency_to, rate
  FROM currency_rate_default a
  WHERE exchange_date = (select max(exchange_date)
                         from currency_rate_default b
                         where to_char(a.exchange_date, 'yyyymm') = to_char(b.exchange_date, 'yyyymm')
                         and a.currency_from = b.currency_from and a.currency_to = b.currency_to)
),
calc_rate AS (
  SELECT /*+ materialize */ a.company_id, a.contract_currency_id, a.company_currency_id, a.accounting_month, a.exchange_date, a.rate, b.rate prev_rate
  FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b
  ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = Add_Months(b.accounting_month, 1)
)
SELECT /*+ no_merge */
 invoice_id,
 vendor_id,
 invoice_status,
 invoice_number,
 inv.ls_asset_Id,
 inv.ilr_id,
 inv.ilr_Id tax_local_id,
 gl_posting_Mo_yr,
 invoice_amount * Nvl(calc_rate.rate, cr.rate) invoice_amount,
 invoice_interest * Nvl(calc_rate.rate, cr.rate) invoice_interest,
 invoice_executory * Nvl(calc_rate.rate, cr.rate) invoice_executory,
 invoice_contingent * Nvl(calc_rate.rate, cr.rate) invoice_contingent,
 lease.lease_id,
 lease.lease_number,
 open_month.company_id,
 open_month.open_month,
 cur.ls_cur_type ls_cur_type,
 cr.exchange_date,
 calc_rate.exchange_date prev_exchange_date,
 lease.contract_currency_id,
 cur.currency_id display_currency_id,
 cs.currency_id company_currency_id,
 cr.rate,
 calc_rate.rate calculated_rate,
 calc_rate.prev_rate previous_calculated_rate,
 cur.iso_code,
 cur.currency_display_symbol
  FROM ls_Invoice inv
 INNER JOIN currency_schema cs
    ON inv.company_id = cs.company_id
 INNER JOIN ls_lease lease
    ON inv.lease_id = lease.lease_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 INNER JOIN open_month
    ON inv.company_id = open_month.company_id
 INNER JOIN cr
    ON cur.currency_id = cr.currency_to
   AND lease.contract_currency_id = cr.currency_from
   AND cr.exchange_date < Add_Months(inv.gl_posting_mo_yr, 1)
  LEFT OUTER JOIN calc_rate
    ON lease.contract_currency_id = calc_rate.contract_currency_id
   AND cur.currency_id = calc_rate.company_currency_id
   AND inv.company_id = calc_rate.company_id
   AND inv.gl_posting_mo_yr = calc_rate.accounting_month
 WHERE cr.exchange_date =
       (SELECT Max(exchange_date)
          FROM cr cr2
         WHERE cr.currency_from = cr2.currency_from
           AND cr.currency_to = cr2.currency_to
           AND cr2.exchange_date < Add_Months(inv.gl_posting_mo_yr, 1))
   AND cs.currency_type_id = 1
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3523, 0, 2017, 1, 0, 0, 47497, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047497_lease_mc_update_ls_invoice_view_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
