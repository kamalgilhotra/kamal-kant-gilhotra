/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029823_prov.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/02/2013 Ron Ferentini  Point Release 
|| 10.4.2.0   2/26/2014	Nathan Hollis	Disabling this report - it's not going to be included in 10.4.2.  Be sure
||												to look at the bottom of this script - it changes the report record
||												in pp_reports so that it is disabled when the script is run. Fix this in 
||												another maint script to re-enable the report.
||============================================================================
*/

insert into TAX_ACCRUAL_REPORT_OPTION
   (REPORT_OPTION_ID, DESCRIPTION)
   select 34, 'CONSOLIDATED-OPER-CONS' from DUAL;

insert into TAX_ACCRUAL_REPORT_OPTION_DTL
   (REPORT_OPTION_ID, REPORT_OBJECT_ID)
   select 34, REPORT_OBJECT_ID
     from TAX_ACCRUAL_REPORT_OPTION_DTL
    where REPORT_OPTION_ID = 31
      and REPORT_OBJECT_ID not in (61, 62);

insert into TAX_ACCRUAL_REP_SPECIAL_NOTE
   (SPECIAL_NOTE_ID, DESCRIPTION, REPORT_OPTION_ID, MONTH_REQUIRED, COMPARE_CASE, M_ROLLUP_REQUIRED,
    CALC_RATES, REPORT_TYPE, DW_PARAMETER_LIST, JE_TEMP_TABLE_IND, NS2FAS109_TEMP_TABLE_IND,
    REP_ROLLUP_GROUP_ID, REP_CONS_CATEGORY_ID)
   select 76,
          'CONSOLIDATED-OPER-CONS',
          34,
          MONTH_REQUIRED,
          COMPARE_CASE,
          M_ROLLUP_REQUIRED,
          CALC_RATES,
          REPORT_TYPE,
          DW_PARAMETER_LIST,
          JE_TEMP_TABLE_IND,
          NS2FAS109_TEMP_TABLE_IND,
          REP_ROLLUP_GROUP_ID,
          6 REP_CONS_CATEGORY_ID
     from TAX_ACCRUAL_REP_SPECIAL_NOTE
    where DESCRIPTION = 'CONSOLIDATED-OPER-STATE';

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID)
   select 51014,
          'Current Provision-All - Grid',
          'Current Provision Report that starts with pre-tax income (not Federal Taxable Income) for all entities - Components across the top of the grid.',
          'Tax Accrual',
          'dw_tax_accrual_state_prov_all_cons',
          'CONSOLIDATED-OPER-CONS',
          'Tax Accrual - 51014',
          3,
          0,
          1
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, SORT_SPEC, REP_CONS_CATEGORY_ID, BALANCES_IND)
   select 41,
          'Current By Oper',
          'dw_tax_accrual_state_prov_all_cons',
          'oper_ind, m_type_id, tax_return_key, detail_descr',
          6,
          0
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, SORT_SPEC, REP_CONS_CATEGORY_ID, BALANCES_IND)
   select 42, 'Current By Month', 'dw_tax_accrual_state_prov_all_cons', null, 6, 0 from DUAL;

insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, SORT_SPEC, REP_CONS_CATEGORY_ID, BALANCES_IND)
   select 43, 'Current By Month Type', 'dw_tax_accrual_state_prov_all_cons', null, 6, 0 from DUAL;

insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, SORT_SPEC, REP_CONS_CATEGORY_ID, BALANCES_IND)
   select 44, 'Current By Company', 'dw_tax_accrual_state_prov_all_cons', null, 6, 0 from DUAL;

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 41, 'oper_ind', 1, 'number', 'oper_descr', 0
     from DUAL
   union
   select 41, 'detail_description', 1, 'string', 'detail_description', 1
     from DUAL
   union
   select 41, 'm_type_id', 1, 'number', 'rollup_descr', 2
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 42, 'month_id', 1, 'number', 'month', 0
     from DUAL
   union
   select 42, 'detail_description', 1, 'string', 'detail_description', 1
     from DUAL
   union
   select 42, 'm_type_id', 1, 'number', 'rollup_descr', 2
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 43, 'month_id', 1, 'number', 'month', 0
     from DUAL
   union
   select 43, 'detail_description', 1, 'string', 'detail_description', 1
     from DUAL
   union
   select 43, 'm_type_id', 1, 'number', 'rollup_descr', 2
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 44, 'consol_desc', 1, 'string', 'consol_desc', 0
     from DUAL
   union
   select 44, 'detail_description', 1, 'string', 'detail_description', 1
     from DUAL
   union
   select 44, 'm_type_id', 1, 'number', 'rollup_descr', 2
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          10,
          'D',
          1,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'm_amount_total',
          'COL_VALUE_TEXT',
          'detail_description',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          12,
          'F',
          2,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          'm_rollup_amount_total',
          'COL_VALUE_TEXT',
          'rollup_descr',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 13, 'F', 2, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          14,
          'F',
          99,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          null,
          'TEXT',
          'Total Adjustments',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          15,
          'F',
          99,
          2,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_b4_deductions_total',
          'TEXT',
          'Taxable Income Before Deductions',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          16,
          'F',
          99,
          3,
          0,
          'COL_VALUE_NUM',
          0,
          'deductions_total',
          'TEXT',
          '   Deduction for Fed/Other States',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          17,
          'F',
          99,
          4,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_b4_apport_total',
          'TEXT',
          'Taxable Income Before Apportionment',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          18,
          'F',
          99,
          5,
          0,
          'COL_VALUE_NUM',
          0,
          'apportionment_total',
          'TEXT',
          '   Apportionment Factor',
          'PA',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          19,
          'F',
          99,
          6,
          1,
          'COL_VALUE_NUM',
          0,
          'apport_ti_total',
          'TEXT',
          'Taxable Income After Apportioment',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          20,
          'F',
          99,
          7,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_with_nols_total',
          'TEXT',
          'Taxable Income After Other Adjustments',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          21,
          'F',
          99,
          8,
          0,
          'COL_VALUE_NUM',
          0,
          'calc_stat_rate_total',
          'TEXT',
          '   Statutory Tax Rate',
          'P',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          22,
          'F',
          99,
          9,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_calc_one_rate_total',
          'TEXT',
          'Calculated Tax Before Credits',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          23,
          'F',
          99,
          10,
          0,
          'COL_VALUE_NUM',
          0,
          'rate_diff_total',
          'TEXT',
          '   Rate Differential',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          24,
          'F',
          99,
          11,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_calc_individual_rates_total',
          'TEXT',
          'Calculated Tax After Rate Differential',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          25,
          'F',
          99,
          12,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_trueup_total',
          'TEXT',
          'Current Period True-Up',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          26,
          'F',
          99,
          13,
          0,
          'COL_VALUE_NUM',
          0,
          'tax_based_on_ytd_total',
          'TEXT',
          'Tax Before Credits',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          27,
          'F',
          99,
          14,
          1,
          'COL_VALUE_NUM',
          0,
          'current_tax_total',
          'TEXT',
          'Current Tax',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 28, 'F', 99, 15, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          1,
          'H',
          -1,
          1,
          1,
          'TEXT',
          1,
          'Current Consolidating Report by Oper',
          null,
          null,
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'consol_desc', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'entity_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 6, 'H', -1, 6, 1, 'COL_VALUE_TEXT', 0, 'month_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 7, 'H', -1, 7, 0, 'FILLER', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41,
          8,
          'H',
          0,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'bi_total',
          'TEXT',
          'Book Income',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 9, 'H', 0, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 41, 11, 'H', 2, 1, 0, 'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_descr', 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          10,
          'D',
          1,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'm_amount_total',
          'COL_VALUE_TEXT',
          'detail_description',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          12,
          'F',
          2,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          'm_rollup_amount_total',
          'COL_VALUE_TEXT',
          'rollup_descr',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 13, 'F', 2, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          14,
          'F',
          99,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          null,
          'TEXT',
          'Total Adjustments',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          15,
          'F',
          99,
          2,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_b4_deductions_total',
          'TEXT',
          'Taxable Income Before Deductions',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          16,
          'F',
          99,
          3,
          0,
          'COL_VALUE_NUM',
          0,
          'deductions_total',
          'TEXT',
          '   Deduction for Fed/Other States',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          17,
          'F',
          99,
          4,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_b4_apport_total',
          'TEXT',
          'Taxable Income Before Apportionment',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          18,
          'F',
          99,
          5,
          0,
          'COL_VALUE_NUM',
          0,
          'apportionment_total',
          'TEXT',
          '   Apportionment Factor',
          'PA',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          19,
          'F',
          99,
          6,
          1,
          'COL_VALUE_NUM',
          0,
          'apport_ti_total',
          'TEXT',
          'Taxable Income After Apportioment',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          20,
          'F',
          99,
          7,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_with_nols_total',
          'TEXT',
          'Taxable Income After Other Adjustments',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          21,
          'F',
          99,
          8,
          0,
          'COL_VALUE_NUM',
          0,
          'calc_stat_rate_total',
          'TEXT',
          '   Statutory Tax Rate',
          'P',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          22,
          'F',
          99,
          9,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_calc_one_rate_total',
          'TEXT',
          'Calculated Tax Before Credits',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          23,
          'F',
          99,
          10,
          0,
          'COL_VALUE_NUM',
          0,
          'rate_diff_total',
          'TEXT',
          '   Rate Differential',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          24,
          'F',
          99,
          11,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_calc_individual_rates_total',
          'TEXT',
          'Calculated Tax After Rate Differential',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          25,
          'F',
          99,
          12,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_trueup_total',
          'TEXT',
          'Current Period True-Up',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          26,
          'F',
          99,
          13,
          0,
          'COL_VALUE_NUM',
          0,
          'tax_based_on_ytd_total',
          'TEXT',
          'Tax Before Credits',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          27,
          'F',
          99,
          14,
          1,
          'COL_VALUE_NUM',
          0,
          'current_tax_total',
          'TEXT',
          'Current Tax',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 28, 'F', 99, 15, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          1,
          'H',
          -1,
          1,
          1,
          'TEXT',
          1,
          'Current Consolidating Report by Month',
          null,
          null,
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'consol_desc', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'entity_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 6, 'H', -1, 6, 1, 'COL_VALUE_TEXT', 0, 'month_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 7, 'H', -1, 7, 0, 'FILLER', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42,
          8,
          'H',
          0,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'bi_total',
          'TEXT',
          'Book Income',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 9, 'H', 0, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 42, 11, 'H', 2, 1, 0, 'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_descr', 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          10,
          'D',
          1,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'm_amount_total',
          'COL_VALUE_TEXT',
          'detail_description',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          12,
          'F',
          2,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          'm_rollup_amount_total',
          'COL_VALUE_TEXT',
          'rollup_descr',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 13, 'F', 2, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          14,
          'F',
          99,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          null,
          'TEXT',
          'Total Adjustments',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          15,
          'F',
          99,
          2,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_b4_deductions_total',
          'TEXT',
          'Taxable Income Before Deductions',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          16,
          'F',
          99,
          3,
          0,
          'COL_VALUE_NUM',
          0,
          'deductions_total',
          'TEXT',
          '   Deduction for Fed/Other States',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          17,
          'F',
          99,
          4,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_b4_apport_total',
          'TEXT',
          'Taxable Income Before Apportionment',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          18,
          'F',
          99,
          5,
          0,
          'COL_VALUE_NUM',
          0,
          'apportionment_total',
          'TEXT',
          '   Apportionment Factor',
          'PA',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          19,
          'F',
          99,
          6,
          1,
          'COL_VALUE_NUM',
          0,
          'apport_ti_total',
          'TEXT',
          'Taxable Income After Apportioment',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          20,
          'F',
          99,
          7,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_with_nols_total',
          'TEXT',
          'Taxable Income After Other Adjustments',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          21,
          'F',
          99,
          8,
          0,
          'COL_VALUE_NUM',
          0,
          'calc_stat_rate_total',
          'TEXT',
          '   Statutory Tax Rate',
          'P',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          22,
          'F',
          99,
          9,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_calc_one_rate_total',
          'TEXT',
          'Calculated Tax Before Credits',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          23,
          'F',
          99,
          10,
          0,
          'COL_VALUE_NUM',
          0,
          'rate_diff_total',
          'TEXT',
          '   Rate Differential',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          24,
          'F',
          99,
          11,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_calc_individual_rates_total',
          'TEXT',
          'Calculated Tax After Rate Differential',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          25,
          'F',
          99,
          12,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_trueup_total',
          'TEXT',
          'Current Period True-Up',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          26,
          'F',
          99,
          13,
          0,
          'COL_VALUE_NUM',
          0,
          'tax_based_on_ytd_total',
          'TEXT',
          'Tax Before Credits',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          27,
          'F',
          99,
          14,
          1,
          'COL_VALUE_NUM',
          0,
          'current_tax_total',
          'TEXT',
          'Current Tax',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 28, 'F', 99, 15, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          1,
          'H',
          -1,
          1,
          1,
          'TEXT',
          1,
          'Current Consolidating Report by Month Type',
          null,
          null,
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'consol_desc', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'entity_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 6, 'H', -1, 6, 1, 'COL_VALUE_TEXT', 0, 'month_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 7, 'H', -1, 7, 0, 'FILLER', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43,
          8,
          'H',
          0,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'bi_total',
          'TEXT',
          'Book Income',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 9, 'H', 0, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 43, 11, 'H', 2, 1, 0, 'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_descr', 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          10,
          'D',
          1,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'm_amount_total',
          'COL_VALUE_TEXT',
          'detail_description',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          12,
          'F',
          2,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          'm_rollup_amount_total',
          'COL_VALUE_TEXT',
          'rollup_descr',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 13, 'F', 2, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          14,
          'F',
          99,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          null,
          'TEXT',
          'Total Adjustments',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          15,
          'F',
          99,
          2,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_b4_deductions_total',
          'TEXT',
          'Taxable Income Before Deductions',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          16,
          'F',
          99,
          3,
          0,
          'COL_VALUE_NUM',
          0,
          'deductions_total',
          'TEXT',
          '   Deduction for Fed/Other States',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          17,
          'F',
          99,
          4,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_b4_apport_total',
          'TEXT',
          'Taxable Income Before Apportionment',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          18,
          'F',
          99,
          5,
          0,
          'COL_VALUE_NUM',
          0,
          'apportionment_total',
          'TEXT',
          '   Apportionment Factor',
          'PA',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          19,
          'F',
          99,
          6,
          1,
          'COL_VALUE_NUM',
          0,
          'apport_ti_total',
          'TEXT',
          'Taxable Income After Apportioment',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          20,
          'F',
          99,
          7,
          1,
          'COL_VALUE_NUM',
          0,
          'ti_with_nols_total',
          'TEXT',
          'Taxable Income After Other Adjustments',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          21,
          'F',
          99,
          8,
          0,
          'COL_VALUE_NUM',
          0,
          'calc_stat_rate_total',
          'TEXT',
          '   Statutory Tax Rate',
          'P',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          22,
          'F',
          99,
          9,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_calc_one_rate_total',
          'TEXT',
          'Calculated Tax Before Credits',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          23,
          'F',
          99,
          10,
          0,
          'COL_VALUE_NUM',
          0,
          'rate_diff_total',
          'TEXT',
          '   Rate Differential',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          24,
          'F',
          99,
          11,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_calc_individual_rates_total',
          'TEXT',
          'Calculated Tax After Rate Differential',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          25,
          'F',
          99,
          12,
          0,
          'COL_VALUE_NUM',
          0,
          'ct_trueup_total',
          'TEXT',
          'Current Period True-Up',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          26,
          'F',
          99,
          13,
          0,
          'COL_VALUE_NUM',
          0,
          'tax_based_on_ytd_total',
          'TEXT',
          'Tax Before Credits',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          27,
          'F',
          99,
          14,
          1,
          'COL_VALUE_NUM',
          0,
          'current_tax_total',
          'TEXT',
          'Current Tax',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 28, 'F', 99, 15, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          1,
          'H',
          -1,
          1,
          1,
          'TEXT',
          1,
          'Current Consolidating Report by Company',
          null,
          null,
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 3, 'H', -1, 3, 1, 'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'consol_desc', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 5, 'H', -1, 5, 1, 'COL_VALUE_TEXT', 0, 'entity_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 6, 'H', -1, 6, 1, 'COL_VALUE_TEXT', 0, 'month_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 7, 'H', -1, 7, 0, 'FILLER', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44,
          8,
          'H',
          0,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'bi_total',
          'TEXT',
          'Book Income',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 9, 'H', 0, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 44, 11, 'H', 2, 1, 0, 'TEXT', 0, null, 'COL_VALUE_TEXT', 'rollup_descr', 'T', null, null
     from DUAL;
     
     
 --here for now until the report is actually finished
 update PP_REPORTS
   set SUBSYSTEM = 'Tax Accrual_d'
   where lower(report_number) = 'tax accrual - 51014';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (681, 0, 10, 4, 2, 0, 29823, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029823_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
