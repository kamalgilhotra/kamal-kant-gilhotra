/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_029768_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   05/18/2013 Julia Breuer   Point Release
||============================================================================
*/

--
-- Add a column to pt_accrual_type to indicate if partially paid bills should be accrued on actuals or estimates.
--
alter table PT_ACCRUAL_TYPE add ACCRUE_ON_ACTUALS number(22,0);

--
-- Update the column based on the current system option value.
--
update PWRPLANT.PT_ACCRUAL_TYPE
   set ACCRUE_ON_ACTUALS =
        (select DECODE(UPPER(trim(NVL(OPTION_VALUE, PP_DEFAULT_VALUE))), 'NO', 0, 1)
           from PTC_SYSTEM_OPTIONS
          where SYSTEM_OPTION_ID = 'Accruals - Accrue on Actuals for Partially Paid Bills');

--
-- Make the column not nullable.
--
alter table PT_ACCRUAL_TYPE modify ACCRUE_ON_ACTUALS not null;

--
-- Add the column to table maintenance.
--
update PWRPLANT.POWERPLANT_COLUMNS
   set COLUMN_RANK = COLUMN_RANK + 1
 where TABLE_NAME = 'pt_accrual_type'
   and COLUMN_RANK between 14 and 99;

insert into PWRPLANT.POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('accrue_on_actuals', 'pt_accrual_type', 'pt_yes_no_filter', 'p', 'Accrue on Actuals',
    'Accrue on Actuals for Partially Paid Bills', 14, 'Accrue on Actuals', 0);

--
-- Delete the system option.
--
delete from PTC_SYSTEM_OPTIONS_VALUES
 where SYSTEM_OPTION_ID = 'Accruals - Accrue on Actuals for Partially Paid Bills';

delete from PTC_SYSTEM_OPTIONS
 where SYSTEM_OPTION_ID = 'Accruals - Accrue on Actuals for Partially Paid Bills';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (384, 0, 10, 4, 1, 0, 29768, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029768_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

