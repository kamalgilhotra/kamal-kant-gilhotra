/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031807_sys_01_PP_PROCESS_PKG.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 10.4.1.0   08/29/2013 Charlie Shilling maint-31807
||============================================================================
*/

create or replace package PP_PROCESS_PKG is
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: pp_process_pkg
   --|| Description:
   --||============================================================================
   --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By        Reason for Change
   --|| -------- ---------- ------------------- ----------------------------------------
   --|| 1.0      03/20/2013 Charlie Shilling    Original Version
   --||============================================================================

   function PP_SET_PROCESS(A_ARG varchar2) return varchar2;

   function PP_CHECK_PROCESS(A_ARG varchar2) return varchar2;

   function PP_CHECK_PROCESS_PREFIX(A_ARG varchar2) return varchar2;

end PP_PROCESS_PKG;
/

create or replace package body PP_PROCESS_PKG is
   -- =============================================================================
   --  Function PP_SET_PROCESS
   -- =============================================================================
   function PP_SET_PROCESS(A_ARG varchar2) return varchar2 as
      /*
      ||============================================================================
      || Application: PowerPlant
      || Object Name: PP_SET_PROCESS
      || Description:
      ||============================================================================
      || Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
      ||============================================================================
      || Version Date       Revised By       Reason for Change
      || ------- ---------- ---------------- -----------------------------------------
      || 1.1                                 Added a check for null argument
      || 1.2     08/30/2013 Charlie Shilling maint 31807
      ||============================================================================
      */

      SID      varchar(60);
      USERNAME varchar(60);
      TERMINAL varchar2(60);
      CODE     number(22, 0);

   begin
      begin
         if A_ARG = '' then
            DBMS_APPLICATION_INFO.SET_CLIENT_INFO('');
            return 'Removed Client Info';
         end if;
      exception
         when others then
            CODE := sqlcode;
            RAISE_APPLICATION_ERROR(-20041,
                                    'Error remove Client Infor ! Msg = ' || sqlerrm(CODE) ||
                                    ' code=' || TO_CHAR(CODE));
            return 'Error';
      end;

      begin
         CODE := 1;
         select AUDSID, USERNAME, NVL(TERMINAL, ' ')
           into SID, USERNAME, TERMINAL
           from GV$SESSION
          where CLIENT_INFO = A_ARG
            and STATUS <> 'KILLED'
            and ROWNUM = 1;
      exception
         when NO_DATA_FOUND then
            -- No one is running the process
            CODE := 0;
         when others then
            CODE := sqlcode;
            RAISE_APPLICATION_ERROR(-20041,
                                    'Selecting from gv$session ! Msg = ' || sqlerrm(CODE) ||
                                    ' code=' || TO_CHAR(CODE));
            return 'Error';
      end;

      if CODE = 1 then
         return 'Sid:' || TO_CHAR(SID) || ' User:' || USERNAME || ' Term:' || TERMINAL;
      end if;

      DBMS_APPLICATION_INFO.SET_CLIENT_INFO(A_ARG);

      begin
         select AUDSID, USERNAME, NVL(TERMINAL, ' ')
           into SID, USERNAME, TERMINAL
           from GV$SESSION
          where CLIENT_INFO = A_ARG
            and STATUS <> 'KILLED'
            and ROWNUM = 1;
      exception
         when NO_DATA_FOUND then
            CODE := sqlcode;
            RAISE_APPLICATION_ERROR(-20041,
                                    'Error setting client info ! Msg = ' || sqlerrm(CODE) ||
                                    ' code=' || TO_CHAR(CODE));
            return 'Error';
         when others then
            CODE := sqlcode;
            RAISE_APPLICATION_ERROR(-20041,
                                    'Error selecting from gv$session ! Msg = ' || sqlerrm(CODE) ||
                                    ' code=' || TO_CHAR(CODE));
            return 'Error';
      end;

      if USERENV('SESSIONID') <> SID then
         -- Someone set the client info before this user
         DBMS_APPLICATION_INFO.SET_CLIENT_INFO('');
         return 'Sid:' || TO_CHAR(SID) || ' User:' || USERNAME || ' Term:' || TERMINAL;
      end if;

      return 'OK';

   exception
      when others then
         CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20041,
                                 'Error verifing client info ! Msg = ' || sqlerrm(CODE) || ' code=' ||
                                 TO_CHAR(CODE));
         return 'Error';
   end;

   -- =============================================================================
   --  Function PP_CHECK_PROCESS
   -- =============================================================================
   function PP_CHECK_PROCESS(A_ARG varchar2) return varchar2 as

      /*
      ||============================================================================
      || Application: PowerPlant
      || Object Name: PP_CHECK_PROCESS
      || Description:
      ||============================================================================
      || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
      ||============================================================================
      || Version Date       Revised By      Reason for Change
      || ------- ---------- --------------- ----------------------------------------
      || 1.0     03/13/2013 Alex Pivoshenko Create
      || 1.1    08/29/2013 C. Shilling    maint 31807
      ||============================================================================
      */

      SID      varchar(60);
      USERNAME varchar(60);
      TERMINAL varchar2(60);
      CODE     number(22, 0);

   begin
      select AUDSID, USERNAME, NVL(TERMINAL, ' ')
        into SID, USERNAME, TERMINAL
        from GV$SESSION
       where CLIENT_INFO = A_ARG
         and STATUS <> 'KILLED'
         and ROWNUM = 1;

      return 'Sid:' || TO_CHAR(SID) || ' User:' || USERNAME || ' Term:' || TERMINAL;

   exception
      when NO_DATA_FOUND then
         return 'OK';
      when others then
         CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20041,
                                 'Error getting client info ! Msg = ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 'Error';
   end PP_CHECK_PROCESS;

   -- =============================================================================
   --  Function PP_CHECK_PROCESS_PREFIX
   -- =============================================================================
   function PP_CHECK_PROCESS_PREFIX(A_ARG varchar2) return varchar2 as

      /*
      ||============================================================================
      || Application: PowerPlant
      || Object Name: PP_CHECK_PROCESS_PREFIX
      || Description:
      ||============================================================================
      || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
      ||============================================================================
      || Version   Date       Revised By      Reason for Change
      || -------   ---------- --------------- ----------------------------------------
      || 10.4.1.0  08/01/2013 Charlie Shilling Create
      ||============================================================================
      */

      SID      varchar(60);
      USERNAME varchar(60);
      TERMINAL varchar2(60);
      CODE     number(22, 0);

   begin
      select AUDSID, USERNAME, NVL(TERMINAL, ' ')
        into SID, USERNAME, TERMINAL
        from GV$SESSION
       where CLIENT_INFO like A_ARG || '%'
         and STATUS <> 'KILLED';

      return 'Sid:' || TO_CHAR(SID) || ' User:' || USERNAME || ' Term:' || TERMINAL;

   exception
      when NO_DATA_FOUND then
         return 'OK';
      when TOO_MANY_ROWS then
         return 'Multiple sessions match input prefix.';
      when others then
         CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20041,
                                 'Error getting client info ! Msg = ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 'Error';
   end PP_CHECK_PROCESS_PREFIX;
end PP_PROCESS_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (559, 0, 10, 4, 1, 0, 31807, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031807_sys_01_PP_PROCESS_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
